<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Reports_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $type)
    {
        $limit = $this->Settings->max_num_results_display;
        $this->db->select('id, code, name')
                 ->where('((name LIKE "%'.$term.'%") or (code LIKE "%'.$term.'%"))');
        if ($type != "") {
            $this->db->where('type', $type);
        }
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariantCodeName($term, $type)
    {
        $limit = $this->Settings->max_num_results_display;
        $this->db->select('id, code, name')
                 ->where('((name LIKE "%'.$term.'%") or (code LIKE "%'.$term.'%"))');
        if ($type != "") {
            $this->db->where('type', $type);
        }
        $this->db->limit($limit);
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaff()
    {
        if ($this->Admin) {
            $this->db->where('group_id !=', 1);
        }
        $this->db->where('group_id !=', 3)->where('group_id !=', 4);
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSalesTotals($customer_id)
    {

        $this->db->select('SUM(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid', FALSE)
            ->where('customer_id', $customer_id);
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCustomerSales($customer_id)
    {
        $this->db->from('sales')->where('customer_id', $customer_id);
        return $this->db->count_all_results();
    }

    public function getCustomerQuotes($customer_id)
    {
        $this->db->from('quotes')->where('customer_id', $customer_id);
        return $this->db->count_all_results();
    }

    public function getCustomerReturns($customer_id)
    {
        $this->db->from('sales')->where('customer_id', $customer_id)->where('sale_status', 'returned');
        return $this->db->count_all_results();
    }

    public function getStockValue()
    {
        $q = $this->db->query("SELECT SUM(by_price) as stock_by_price, SUM(by_cost) as stock_by_cost FROM ( Select COALESCE(sum(" . $this->db->dbprefix('warehouses_products') . ".quantity), 0)*price as by_price, COALESCE(sum(" . $this->db->dbprefix('warehouses_products') . ".quantity), 0)*cost as by_cost FROM " . $this->db->dbprefix('products') . " JOIN " . $this->db->dbprefix('warehouses_products') . " ON " . $this->db->dbprefix('warehouses_products') . ".product_id=" . $this->db->dbprefix('products') . ".id GROUP BY " . $this->db->dbprefix('products') . ".id )a");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseStockValue($id)
    {
        $q = $this->db->query("SELECT SUM(by_price) as stock_by_price, SUM(by_cost) as stock_by_cost FROM ( Select sum(COALESCE(" . $this->db->dbprefix('warehouses_products') . ".quantity, 0))*price as by_price, sum(COALESCE(" . $this->db->dbprefix('warehouses_products') . ".quantity, 0))*cost as by_cost FROM " . $this->db->dbprefix('products') . " JOIN " . $this->db->dbprefix('warehouses_products') . " ON " . $this->db->dbprefix('warehouses_products') . ".product_id=" . $this->db->dbprefix('products') . ".id WHERE " . $this->db->dbprefix('warehouses_products') . ".warehouse_id = ? GROUP BY " . $this->db->dbprefix('products') . ".id )a", array($id));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    // public function getmonthlyPurchases()
    // {
    //     $myQuery = "SELECT (CASE WHEN date_format( date, '%b' ) Is Null THEN 0 ELSE date_format( date, '%b' ) END) as month, SUM( COALESCE( total, 0 ) ) AS purchases FROM purchases WHERE date >= date_sub( now( ) , INTERVAL 12 MONTH ) GROUP BY date_format( date, '%b' ) ORDER BY date_format( date, '%m' ) ASC";
    //     $q = $this->db->query($myQuery);
    //     if ($q->num_rows() > 0) {
    //         foreach (($q->result()) as $row) {
    //             $data[] = $row;
    //         }
    //         return $data;
    //     }
    //     return FALSE;
    // }

    public function getChartData()
    {
        $myQuery = "SELECT S.month,
        COALESCE(S.sales, 0) as sales,
        COALESCE( P.purchases, 0 ) as purchases,
        COALESCE(S.tax1, 0) as tax1,
        COALESCE(S.tax2, 0) as tax2,
        COALESCE( P.ptax, 0 ) as ptax
        FROM (  SELECT  date_format(date, '%Y-%m') Month,
                SUM(total) Sales,
                SUM(product_tax) tax1,
                SUM(order_tax) tax2
                FROM " . $this->db->dbprefix('sales') . "
                WHERE date >= date_sub( now( ) , INTERVAL 12 MONTH )
                GROUP BY date_format(date, '%Y-%m')) S
            LEFT JOIN ( SELECT  date_format(date, '%Y-%m') Month,
                        SUM(product_tax) ptax,
                        SUM(order_tax) otax,
                        SUM(total) purchases
                        FROM " . $this->db->dbprefix('purchases') . "
                        GROUP BY date_format(date, '%Y-%m')) P
            ON S.Month = P.Month
            ORDER BY S.Month";
        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getDailySales($year, $month, $biller_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date,
                    SUM( COALESCE( IF(sale_status != 'returned', product_tax, 0), 0 ) ) AS tax1,
                    SUM( COALESCE( IF(sale_status != 'returned', order_tax, 0), 0 ) ) AS tax2,
                    SUM( COALESCE( IF(sale_status != 'returned', grand_total, 0), 0 ) ) AS total,
                    SUM( COALESCE( IF(sale_status != 'returned', order_discount, 0), 0 ) ) AS discount,
                    SUM( COALESCE( IF(sale_status != 'returned', shipping, 0), 0 ) ) AS shipping,
                    SUM( COALESCE( IF(sale_status != 'returned', tip_amount, 0), 0 ) ) AS tip,
                    SUM( COALESCE( IF(sale_status != 'returned', total, 0), 0 ) ) AS sub_total,
                    SUM( COALESCE( IF(sale_status = 'returned', grand_total, 0), 0 ) ) AS returned_total
            FROM " . $this->db->dbprefix('sales') . " WHERE ";
        if ($biller_id) {
            $myQuery .= " biller_id = {$biller_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
            GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getMonthlySales($year, $biller_id = NULL)
    {
        $myQuery = "SELECT
                        DATE_FORMAT( date,  '%c' ) AS date,
                        SUM( COALESCE( IF(sale_status != 'returned', product_tax, 0), 0 ) ) AS tax1,
                        SUM( COALESCE( IF(sale_status != 'returned', order_tax, 0), 0 ) ) AS tax2,
                        SUM( COALESCE( IF(sale_status != 'returned', grand_total, 0), 0 ) ) AS total,
                        SUM( COALESCE( IF(sale_status != 'returned', order_discount, 0), 0 ) ) AS discount,
                        SUM( COALESCE( IF(sale_status != 'returned', shipping, 0), 0 ) ) AS shipping,
                        SUM( COALESCE( IF(sale_status != 'returned', tip_amount, 0), 0 ) ) AS tip,
                        SUM( COALESCE( IF(sale_status != 'returned', total, 0), 0 ) ) AS sub_total,
                        SUM( COALESCE( IF(sale_status = 'returned', grand_total, 0), 0 ) ) AS returned_total
            FROM " . $this->db->dbprefix('sales') . " WHERE ";
        if ($biller_id) {
            $myQuery .= " biller_id = {$biller_id} AND ";
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id')) {
                $myQuery .= ' (created_by = '.$this->session->userdata('user_id').' OR seller_id = '.$this->session->userdata('company_id').')'.' AND';
            } else {
                $myQuery .= ' created_by = '.$this->session->userdata('user_id').' AND';
            }
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y' ) =  '{$year}'
            GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaffDailySales($user_id, $year, $month, $biller_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date,
                    SUM( COALESCE( IF(sale_status != 'returned', product_tax, 0), 0 ) ) AS tax1,
                    SUM( COALESCE( IF(sale_status != 'returned', order_tax, 0), 0 ) ) AS tax2,
                    SUM( COALESCE( IF(sale_status != 'returned', grand_total, 0), 0 ) ) AS total,
                    SUM( COALESCE( IF(sale_status != 'returned', order_discount, 0), 0 ) ) AS discount,
                    SUM( COALESCE( IF(sale_status != 'returned', shipping, 0), 0 ) ) AS shipping,
                    SUM( COALESCE( IF(sale_status != 'returned', tip_amount, 0), 0 ) ) AS tip,
                    SUM( COALESCE( IF(sale_status != 'returned', total, 0), 0 ) ) AS sub_total,
                    SUM( COALESCE( IF(sale_status = 'returned', grand_total, 0), 0 ) ) AS returned_total
            FROM " . $this->db->dbprefix('sales')." WHERE ";
        if ($biller_id) {
            $myQuery .= " biller_id = {$biller_id} AND ";
        }
        $myQuery .= " created_by = {$user_id} AND DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
            GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaffMonthlySales($user_id, $year, $biller_id = NULL)
    {
        $myQuery = "SELECT
                        DATE_FORMAT( date,  '%c' ) AS date,
                        SUM( COALESCE( IF(sale_status != 'returned', product_tax, 0) ,0 ) ) AS tax1,
                        SUM( COALESCE( IF(sale_status != 'returned', order_tax, 0) ,0 ) ) AS tax2,
                        SUM( COALESCE( IF(sale_status != 'returned', grand_total, 0) ,0 ) ) AS total,
                        SUM( COALESCE( IF(sale_status != 'returned', total_discount, 0) ,0 ) ) AS discount,
                        SUM( COALESCE( IF(sale_status != 'returned', tip_amount, 0) ,0 ) ) AS tip,
                        SUM( COALESCE( IF(sale_status != 'returned', shipping, 0) ,0 ) ) AS shipping,
                        SUM( COALESCE( IF(sale_status = 'returned', grand_total, 0) ,0 ) ) AS returned_total
            FROM " . $this->db->dbprefix('sales') . " WHERE ";
        if ($biller_id) {
            $myQuery .= " biller_id = {$biller_id} AND ";
        }
        $myQuery .= " created_by = {$user_id} AND DATE_FORMAT( date,  '%Y' ) =  '{$year}'
            GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasesTotals($supplier_id)
    {
        $this->db->select('SUM(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid', FALSE)
            ->where('supplier_id', $supplier_id);
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSupplierPurchases($supplier_id)
    {
        $this->db->from('purchases')->where('supplier_id', $supplier_id);
        return $this->db->count_all_results();
    }

    public function getStaffPurchases($user_id)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid', FALSE)
            ->where('created_by', $user_id);
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getStaffSales($user_id)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid', FALSE)
            ->where('created_by', $user_id);
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalSales($start, $end, $warehouse_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', FALSE)
            ->where('sale_status !=', 'pending')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReturnSales($start, $end, $warehouse_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', FALSE)
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('returns');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalPurchases($start, $end, $warehouse_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', FALSE)
            ->where('status !=', 'pending')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalExpenses($start, $end, $warehouse_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        // if ($warehouse_id) {
        //     $this->db->where('warehouse_id', $warehouse_id);
        // }
        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalPaidAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'sent')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedCashAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'cash')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedCCAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'CC')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedChequeAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'Cheque')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedPPPAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'ppp')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedStripeAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'stripe')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReturnedAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'returned')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseTotals($warehouse_id = NULL)
    {
        $this->db->select('sum(quantity) as total_quantity, count(id) as total_items', FALSE);
        $this->db->where('quantity !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('warehouses_products');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCosting($date, $biller_id = NULL, $year = NULL, $month = NULL)
    {
        $this->db->select('
                            SUM( COALESCE(IF('.$this->db->dbprefix('sales').'.sale_status != "returned", IF( costing.total_cost > 0, costing.total_cost , '.$this->db->dbprefix('sale_items').'.avg_net_unit_cost * '.$this->db->dbprefix('sale_items').'.quantity), 0 ), 0)) AS cost,
                            SUM( COALESCE( IF('.$this->db->dbprefix('sales').'.sale_status != "returned", '.$this->db->dbprefix('sale_items').'.unit_price, 0), 0 ) * '.$this->db->dbprefix('sale_items').'.quantity ) AS sales,
                            SUM( COALESCE( IF('.$this->db->dbprefix('sales').'.sale_status != "returned", '.$this->db->dbprefix('sale_items').'.net_unit_price, 0), 0 ) * '.$this->db->dbprefix('sale_items').'.quantity ) AS net_sales,
                            SUM( COALESCE( IF('.$this->db->dbprefix('sales').'.sale_status = "returned", '.$this->db->dbprefix('sale_items').'.unit_price, 0), 0 ) * '.$this->db->dbprefix('sale_items').'.quantity ) AS net_return_sales,
                            SUM( COALESCE( IF('.$this->db->dbprefix('sales').'.sale_status != "returned", '.$this->db->dbprefix('sale_items').'.item_tax, 0), 0 ) ) AS item_tax
                        ', FALSE);
        if ($date) {
            $this->db->where('sales.date >=', $date.' 00:00:00');
            $this->db->where('sales.date <=', $date.' 23:59:59');
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('sales.date >=', $year.'-'.$month.'-01 00:00:00');
            $this->db->where('sales.date <=', $year.'-'.$month.'-'.$last_day.' 23:59:59');
        }

        if ($biller_id) {
            $this->db->where('sales.biller_id', $biller_id);
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id')) {
                $this->db->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.$this->session->userdata('company_id').')');
            } else {
                $this->db->where('sales.created_by', $this->session->userdata('user_id'));
            }
        } 
        $this->db->join('sales', 'sales.id=sale_items.sale_id', 'inner');
        $this->db->join('(SELECT sale_item_id, SUM(COALESCE(purchase_net_unit_cost * quantity, 0)) as total_cost FROM '.$this->db->dbprefix('costing').' GROUP BY sale_item_id) costing', 'costing.sale_item_id = '.$this->db->dbprefix('sale_items').'.id', 'left');
        $q = $this->db->get('sale_items');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getExpenses($date, $biller_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date.' 00:00:00';
        $edate = $date.' 23:59:59';
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE);
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year.'-'.$month.'-01 00:00:00');
            $this->db->where('date <=', $year.'-'.$month.'-'.$last_day.' 23:59:59');
        }


        if ($biller_id) {
            $this->db->where('biller_id', $biller_id);
        }

        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getReturns($date, $biller_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date.' 00:00:00';
        $edate = $date.' 23:59:59';
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total', FALSE)
        ->where('sale_status', 'returned');
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year.'-'.$month.'-01 00:00:00');
            $this->db->where('date <=', $year.'-'.$month.'-'.$last_day.' 23:59:59');
        }

        if ($biller_id) {
            $this->db->where('biller_id', $biller_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getOrderDiscount($date, $biller_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date.' 00:00:00';
        $edate = $date.' 23:59:59';
        $this->db->select('SUM( COALESCE( order_discount, 0 ) ) AS order_discount', FALSE);
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year.'-'.$month.'-01 00:00:00');
            $this->db->where('date <=', $year.'-'.$month.'-'.$last_day.' 23:59:59');
        }

        if ($biller_id) {
            $this->db->where('biller_id', $biller_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getExpenseCategories()
    {
        $q = $this->db->get('expense_categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getDailyPurchases($year, $month, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('purchases') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
            GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getMonthlyPurchases($year, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%c' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('purchases') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y' ) =  '{$year}'
            GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaffDailyPurchases($user_id, $year, $month, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('purchases')." WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " created_by = {$user_id} AND DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
            GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaffMonthlyPurchases($user_id, $year, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%c' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('purchases') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " created_by = {$user_id} AND DATE_FORMAT( date,  '%Y' ) =  '{$year}'
            GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getBestSeller($start_date, $end_date, $warehouse_id = NULL)
    {
        $this->db
            ->select("product_name, product_code")->select_sum('quantity')
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->where('date >=', $start_date)->where('date <=', $end_date)
            ->group_by('product_name, product_code')->order_by('sum(quantity)', 'desc')->limit(10);
        if ($warehouse_id) {
            $this->db->where('sale_items.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('sale_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getPOSSetting()
    {
        $q = $this->db->get('pos_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getSalesTax($start_date = null, $end_date = null, $taxRate = null, $biller = null, $warehouse = null, $document_type = null)
    {

        if ($taxRate) {

            $this->db->select(" SUM({$this->db->dbprefix('sales')}.igst) AS igst,
                            SUM({$this->db->dbprefix('sales')}.cgst) AS cgst,
                            SUM({$this->db->dbprefix('sales')}.sgst) AS sgst,
                            SUM({$this->db->dbprefix('sale_items')}.net_unit_price * {$this->db->dbprefix('sale_items')}.quantity) AS net_price,
                            SUM(({$this->db->dbprefix('sale_items')}.item_tax + {$this->db->dbprefix('sale_items')}.item_tax_2)) AS product_tax,
                            SUM({$this->db->dbprefix('sales')}.order_tax) AS order_tax,
                            SUM({$this->db->dbprefix('sale_items')}.subtotal) AS grand_total,
                            SUM({$this->db->dbprefix('sales')}.shipping) AS shipping,
                            SUM({$this->db->dbprefix('sales')}.order_discount) AS order_discount,
                            SUM({$this->db->dbprefix('sales')}.paid) AS paid")
          ->from('sale_items')
          ->join('sales', 'sales.id = sale_items.sale_id', 'left')
          ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
          ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left');
          $this->db->where('sale_items.tax_rate_id', $taxRate);
          if ($biller) {
            $this->db->where('sales.biller_id', $biller);
          }
          if ($warehouse) {
            $this->db->where('sales.warehouse_id', $warehouse);
          }
          if ($document_type) {
            $this->db->where('sales.document_type_id', $document_type);
          }
          if ($start_date) {
            $this->db->where('sales.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
          }

          $q = $this->db->get();

        }else{

            $this->db->select_sum('igst')
                ->select_sum('cgst')
                ->select_sum('sgst')
                ->select_sum('total', 'net_price')
                ->select_sum('product_tax')
                ->select_sum('order_tax')
                ->select_sum('shipping')
                ->select_sum('grand_total')
                ->select_sum('order_discount')
                ->select_sum('paid');
            if ($start_date) {
                $this->db->where('date >=', $start_date);
            }
            if ($end_date) {
                $this->db->where('date <=', $end_date);
            }

            if ($biller) {
                $this->db->where('biller_id =', $biller);
            }

            if ($warehouse) {
                $this->db->where('warehouse_id =', $warehouse);
            }

            if ($document_type) {
                $this->db->where('document_type_id', $document_type);
            }

            $q = $this->db->get('sales');

        }

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getSalesTaxDetail($start_date, $end_date, $taxRate, $biller, $warehouse, $document_type){
        $this->db->select("
                            {$this->db->dbprefix('tax_rates')}.name as tax_rate_name ,
                            SUM(({$this->db->dbprefix('sale_items')}.net_unit_price * {$this->db->dbprefix('sale_items')}.quantity)) AS net_price,
                            SUM({$this->db->dbprefix('sale_items')}.item_tax) AS product_tax,
                            SUM({$this->db->dbprefix('sales')}.order_tax) AS order_tax,
                            SUM({$this->db->dbprefix('sale_items')}.subtotal)  AS grand_total,
                            SUM({$this->db->dbprefix('sales')}.order_discount) AS order_discount,
                            SUM({$this->db->dbprefix('sales')}.paid) AS paid")
        ->from('sale_items')
        ->join('sales', 'sales.id = sale_items.sale_id', 'left')
        ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
        ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
        ->group_by('sale_items.tax_rate_id');

        if ($taxRate) {
            $this->db->where('sale_items.tax_rate_id', $taxRate);
        }

        if ($biller) {
            $this->db->where('sales.biller_id', $biller);
        }

        if ($warehouse) {
            $this->db->where('sales.warehouse_id', $warehouse);
        }

        if ($document_type) {
            $this->db->where('sales.document_type_id', $document_type);
        }

        if ($start_date) {
            $this->db->where('sales.date >=', $start_date);
        }

        if ($end_date) {
            $this->db->where('sales.date <=', $end_date);
        }

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }

    function getSalesIpoconsumoDetail($start_date, $end_date, $taxRate, $biller, $warehouse, $document_type){
        $this->db->select("
                            SUM(({$this->db->dbprefix('sale_items')}.net_unit_price * {$this->db->dbprefix('sale_items')}.quantity)) AS net_price,
                            SUM({$this->db->dbprefix('sale_items')}.item_tax_2) AS product_tax,
                            SUM({$this->db->dbprefix('sale_items')}.subtotal)  AS grand_total")
        ->from('sale_items')
        ->join('sales', 'sales.id = sale_items.sale_id', 'left')
        ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
        ->where('item_tax_2 >', 0);

        if ($taxRate) {
            $this->db->where('sale_items.tax_rate_id', $taxRate);
        }

        if ($biller) {
            $this->db->where('sales.biller_id', $biller);
        }

        if ($warehouse) {
            $this->db->where('sales.warehouse_id', $warehouse);
        }

        if ($document_type) {
            $this->db->where('sales.document_type_id', $document_type);
        }

        if ($start_date) {
            $this->db->where('sales.date >=', $start_date);
        }

        if ($end_date) {
            $this->db->where('sales.date <=', $end_date);
        }

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }

    function getPurchasesTax($start_date = null, $end_date = null, $taxRate = null, $biller = null, $warehouse = null, $document_type = null)
    {

        if ($taxRate) {
            $this->db->select("SUM({$this->db->dbprefix('purchases')}.igst) AS igst,
                              SUM({$this->db->dbprefix('purchases')}.cgst) AS cgst,
                              SUM({$this->db->dbprefix('purchases')}.sgst) AS sgst,
                              SUM({$this->db->dbprefix('purchase_items')}.net_unit_cost * {$this->db->dbprefix('purchase_items')}.quantity) AS net_price,
                              SUM({$this->db->dbprefix('purchase_items')}.item_tax) AS product_tax,
                              SUM({$this->db->dbprefix('purchases')}.order_tax) AS order_tax,
                              SUM({$this->db->dbprefix('purchases')}.shipping) AS shipping,
                              sum({$this->db->dbprefix('purchase_items')}.unit_cost * {$this->db->dbprefix('purchase_items')}.quantity) AS grand_total,
                              SUM({$this->db->dbprefix('purchases')}.order_discount) AS order_discount,
                              SUM({$this->db->dbprefix('purchases')}.paid) AS paid", FALSE )
            ->from('purchase_items')
            ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'left')
            ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
            ->join('tax_rates', 'tax_rates.id=purchase_items.tax_rate_id', 'left');

            $this->db->where('purchase_items.tax_rate_id', $taxRate);

            if ($biller) {
              $this->db->where('purchases.biller_id', $biller);
            }
            if ($warehouse) {
              $this->db->where('purchases.warehouse_id', $warehouse);
            }
            if ($document_type) {
              $this->db->where('purchases.document_type_id', $document_type);
            }
            if ($start_date) {
              $this->db->where('purchases.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();

            // var_dump($q->row());

        } else {
            $this->db->select_sum('igst')
                ->select_sum('cgst')
                ->select_sum('sgst')
                ->select_sum('total', 'net_price')
                ->select_sum('product_tax')
                ->select_sum('order_tax')
                ->select_sum('grand_total')
                ->select_sum('shipping')
                ->select_sum('order_discount')
                ->select_sum('paid');
            if ($start_date) {
                $this->db->where('date >=', $start_date);
            }
            if ($end_date) {
                $this->db->where('date <=', $end_date);
            }
            if ($document_type) {
                $this->db->where('document_type_id', $document_type);
            }
            $q = $this->db->get('purchases');

        }

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getPurchasesTaxDetail($start_date = null, $end_date = null, $taxRate = null, $biller = null, $warehouse = null, $document_type = null)
    {

        $this->db->select("
                        {$this->db->dbprefix('tax_rates')}.name as tax_rate_name ,
                          SUM({$this->db->dbprefix('purchase_items')}.net_unit_cost * {$this->db->dbprefix('purchase_items')}.quantity) AS net_price,
                          SUM({$this->db->dbprefix('purchase_items')}.item_tax) AS product_tax,
                          SUM({$this->db->dbprefix('purchases')}.order_tax) AS order_tax,
                          sum({$this->db->dbprefix('purchase_items')}.subtotal) AS grand_total,
                          SUM({$this->db->dbprefix('purchases')}.order_discount) AS order_discount,
                          SUM({$this->db->dbprefix('purchases')}.paid) AS paid", FALSE )
        ->from('purchase_items')
        ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'left')
        ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
        ->join('tax_rates', 'tax_rates.id=purchase_items.tax_rate_id', 'left')
        ->group_by('purchase_items.tax_rate_id');

        if ($taxRate) {
            $this->db->where('purchase_items.tax_rate_id', $taxRate);
        }
        if ($biller) {
            $this->db->where('purchases.biller_id', $biller);
        }
        if ($warehouse) {
            $this->db->where('purchases.warehouse_id', $warehouse);
        }
        if ($document_type) {
            $this->db->where('purchases.document_type_id', $document_type);
        }
        if ($start_date) {
            $this->db->where('purchases.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getPurchasesIpoconsumoDetail($start_date = null, $end_date = null, $taxRate = null, $biller = null, $warehouse = null, $document_type = null)
    {

        $this->db->select("
                          SUM({$this->db->dbprefix('purchase_items')}.net_unit_cost * {$this->db->dbprefix('purchase_items')}.quantity) AS net_price,
                          SUM({$this->db->dbprefix('purchase_items')}.item_tax) AS product_tax,
                          sum({$this->db->dbprefix('purchase_items')}.subtotal) AS grand_total", FALSE )
        ->from('purchase_items')
        ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'left')
        ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
        ->join('tax_rates', 'tax_rates.id=purchase_items.tax_rate_id', 'left')
        ->where('purchase_items.item_tax_2 > ', 0);

        if ($taxRate) {
            $this->db->where('purchase_items.tax_rate_id', $taxRate);
        }
        if ($biller) {
            $this->db->where('purchases.biller_id', $biller);
        }
        if ($warehouse) {
            $this->db->where('purchases.warehouse_id', $warehouse);
        }
        if ($document_type) {
            $this->db->where('purchases.document_type_id', $document_type);
        }
        if ($start_date) {
            $this->db->where('purchases.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    //INFORMES JAIME INICIO

    //INFORME DE CARTERA
    public function get_sales_biller()
    {
        $payment_status = array('pending', 'due', 'partial');
        $this->db->select($this->db->dbprefix('companies').'.id as cod_companie');
        $this->db->select_max($this->db->dbprefix('companies').'.name', 'nombre');
        $this->db->from('sales');
        $this->db->join('companies', 'companies.id = sales.biller_id');
        $this->db->where('sale_status','completed');
        $this->db->where('(grand_total-paid) > 0 ');
        $this->db->where_in('payment_status', $payment_status);
        $this->db->group_by("companies.id");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_sales_seller($biller = '')
    {
        if($biller=='all')  $biller = '';
        $payment_status = array('pending', 'due', 'partial');
        $this->db->select('IF('.$this->db->dbprefix("sales").'.seller_id!='."'NUll'".', '.$this->db->dbprefix("sales").'.seller_id,"0") as cod_seller');
        $this->db->select('IF('.$this->db->dbprefix("companies").'.vat_no!='."'NUll'".','.$this->db->dbprefix("companies").'.name,"Sin Vendedor") as nombre ');
        $this->db->select('IF('.$this->db->dbprefix("companies").'.vat_no!='."'NUll'".','.$this->db->dbprefix("companies").'.vat_no,"0") as Document ');
        $this->db->from('sales');
        $this->db->join('companies', 'sales.seller_id = companies.id','left');
        $this->db->where('sale_status','completed');
        $this->db->where('(grand_total-paid) > 0 ');
        if($biller!='') $this->db->where('sales.biller_id',$biller);
        $this->db->where_in('payment_status', $payment_status);
        $this->db->group_by("companies.vat_no");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_sales_customer($biller = '',$seller = '')
    {
        if($biller=='all')  $biller = '';
        if($seller=='all')  $seller = '';
        $payment_status = array('pending', 'due', 'partial');
        $this->db->select($this->db->dbprefix("companies").'.vat_no as cod_id');
        $this->db->select_max('customer', 'nombre');
        $this->db->from('sales');
        $this->db->join('companies', 'companies.id = sales.customer_id');
        $this->db->join('companies as companies_seller', 'sales.seller_id = companies_seller.id','left');
        $this->db->where('sale_status','completed');
        $this->db->where('(grand_total-paid) > 0 ');
        if($biller!='')
        {
            $this->db->where('companies_seller.vat_no',$biller);
            $this->db->or_where($this->db->dbprefix('sales').'.biller_id', $biller);
        }
        if($seller!='')
        {
            if($seller=='0') $this->db->where('sales.seller_id',$seller);
            else $this->db->where('companies_seller.vat_no',$seller);
        }
        $this->db->where_in('payment_status', $payment_status);
        $this->db->group_by("sales.customer_id");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_sales($data)
    {
        $filter_year = isset($data['filter_year']) ? $data['filter_year'] : false;
        $biller = isset($data['biller']) ? $data['biller'] : false;
        $seller = isset($data['seller']) ? $data['seller'] : false;
        $customer = isset($data['customer']) ? $data['customer'] : false;
        $order = isset($data['order']) ? $data['order'] : false;
        $group_by_biller = isset($data['group_by_biller']) ? $data['group_by_biller'] : false;
        $group_by_seller = isset($data['group_by_seller']) ? $data['group_by_seller'] : false;
        $group_by_city = isset($data['group_by_city']) ? $data['group_by_city'] : false;
        $group_by_customer = isset($data['group_by_customer']) ? $data['group_by_customer'] : false;
        $group_by_customer_address = isset($data['group_by_customer_address']) ? $data['group_by_customer_address'] : false;
        $order_by = isset($data['order_by']) ? $data['order_by'] : false;
        $end_date = isset($data['end_date']) ? $data['end_date'] : false;
        if($biller=='all')  $biller = '';
        if($seller=='all')  $seller = '';
        if($customer=='all')  $customer = '';
        $payment_status = array('pending', 'due', 'partial');
        if (!$end_date) {
            $end_date = date('Y-m-d H:i:s');
        } else {
            $end_date .= " 23:59:59";
        }
        $this->db->query('CREATE TEMPORARY TABLE '.$this->db->dbprefix("portfolio_payments").' AS (SELECT sale_id, SUM(amount) as amount FROM '.$this->db->dbprefix("payments").' WHERE date <= "'.$end_date.'" AND sale_id IS NOT NULL GROUP BY sale_id)');
        $this->db->select($this->db->dbprefix("sales").'.id as cod_fact');
        $this->db->select($this->db->dbprefix("sales").'.due_date');
        $this->db->select($this->db->dbprefix("sales").'.date as Fecha_Creacion');
        $this->db->select('reference_no as Referencia');
        $this->db->select('COALESCE('.$this->db->dbprefix("portfolio_payments").'.amount, 0) as val_pago');
        $this->db->select('payment_status as estado');
        $this->db->select($this->db->dbprefix("sales").'.payment_term as dias');
        $this->db->select('grand_total as total');
        $this->db->select('(grand_total-COALESCE('.$this->db->dbprefix("portfolio_payments").'.amount, 0)) as Saldo ');
        $this->db->select($this->db->dbprefix("companies").'.id as cod_companie');
        $this->db->select($this->db->dbprefix("companies").'.name as nom_companie ');
        $this->db->select($this->db->dbprefix("companies").'.vat_no as documento ');
        $this->db->select($this->db->dbprefix("companies").'.phone ');
        $this->db->select($this->db->dbprefix("companies").'.city ');
        $this->db->select($this->db->dbprefix("sales").'.customer as nom_client');
        $this->db->select($this->db->dbprefix("sales").'.biller_id as cod_Sucursal');
        $this->db->select($this->db->dbprefix("sales").'.biller as Sucursal');
        $this->db->select($this->db->dbprefix("sales").'.seller_id as Cod_vendedor');
        $this->db->select('IF('.$this->db->dbprefix("sales").'.seller_id!='."'NUll'".','.$this->db->dbprefix("sales").'.seller_id,"0") as cod_seller ');
        $this->db->select('IF(companies_seller.name!='."'NUll'".', companies_seller.name,"Sin Vendedor") as nom_seller');
        $this->db->select($this->db->dbprefix("addresses").'.sucursal as customer_sucursal');
        $this->db->select($this->db->dbprefix("addresses").'.state as customer_state');
        $this->db->select($this->db->dbprefix("addresses").'.city as customer_city');
        $this->db->select($this->db->dbprefix("addresses").'.direccion as customer_address');
        $this->db->from('sales');
        $this->db->join('companies', 'companies.id = sales.customer_id', 'inner');
        $this->db->join('companies as companies_seller', 'sales.seller_id = companies_seller.id','inner');
        $this->db->join('addresses', 'addresses.id = '.$this->db->dbprefix("sales").'.address_id', 'LEFT');
        $this->db->join(''.$this->db->dbprefix("portfolio_payments"), ''.$this->db->dbprefix("portfolio_payments").'.sale_id = '.$this->db->dbprefix("sales").'.id', 'left');
        $this->db->where('sale_status','completed');
        $this->db->where('grand_total >',0);
        $this->db->where('(grand_total-COALESCE('.$this->db->dbprefix("portfolio_payments").'.amount, 0)) > 0');
        if($biller!='')
        {
            $this->db->where('(companies_seller.vat_no = '.$biller.' OR '.$this->db->dbprefix('sales').'.biller_id = '.$biller.')');
        }
        if($seller!='')
        {
            if ($seller=='0') {
                $this->db->where('('.$this->db->dbprefix('sales').'.seller_id = '.$seller.' or companies_seller.vat_no = '.$seller.' or '.$this->db->dbprefix('sales').'.seller_id = NULL)');
            } else{
                $this->db->where('('.$this->db->dbprefix('sales').'.seller_id = '.$seller.' or companies_seller.vat_no = '.$seller.')');
            }
        }
        if($customer!='') $this->db->where('companies.vat_no',$customer);
        // $this->db->where_in('payment_status', $payment_status);
        if ($filter_year) {
            if ($filter_year != date('Y', strtotime($end_date))) {
                $this->db->where($this->db->dbprefix('sales').'.date >=', $filter_year."-01-01 00:00:00");
                $this->db->where($this->db->dbprefix('sales').'.date <=', $filter_year."-12-31 23:59:59");
                $this->db->where("(".$this->db->dbprefix('sales').'.portfolio_end_date > "'.$filter_year.'-12-31 23:59:59" OR '.$this->db->dbprefix('sales').'.portfolio_end_date IS NULL)');
            } else {
                $this->db->where($this->db->dbprefix('sales').'.date >=', $filter_year."-01-01 00:00:00");
                $this->db->where($this->db->dbprefix('sales').'.date <=', $end_date);
                $this->db->where("(".$this->db->dbprefix('sales').'.portfolio_end_date > "'.$end_date.'" OR '.$this->db->dbprefix('sales').'.portfolio_end_date IS NULL)');
            }
        } else {
            $this->db->where($this->db->dbprefix('sales').'.date <=', $end_date);
            $this->db->where("(".$this->db->dbprefix('sales').'.portfolio_end_date > "'.$end_date.'" OR '.$this->db->dbprefix('sales').'.portfolio_end_date IS NULL )');
        }
        $gpb_sql = $this->db->dbprefix('sales').'.biller_id ASC, ';
        if ($group_by_city || $group_by_seller || $group_by_customer) {
            if ($group_by_seller || $group_by_city) {
                if (($group_by_seller && !$group_by_city) || ($group_by_seller && $group_by_city)) {
                    $this->db->order_by(($group_by_biller ? $gpb_sql : '').'companies_seller.name asc, addresses.state asc, addresses.city asc, companies.name asc, sales.address_id asc, sales.reference_no asc');
                } else if (!$group_by_seller && $group_by_city) {
                    $this->db->order_by(($group_by_biller ? $gpb_sql : '').'addresses.state asc, addresses.city asc, companies.name asc, sales.address_id asc, sales.reference_no asc');
                }
            } else if ($group_by_customer) {
                $this->db->order_by(($group_by_biller ? $gpb_sql : '').'companies.name asc, sales.address_id asc, sales.reference_no asc');
            }
        } else {
            if ($order_by != "") {
                if ($order_by == 'customer_asc') {
                    $this->db->order_by('companies.name asc, sales.reference_no desc');
                } else if ($order_by == 'customer_desc') {
                    $this->db->order_by('companies.name desc, sales.reference_no desc');
                } else if ($order_by == 'customer_city_asc') {
                    $this->db->order_by('addresses.city asc, sales.reference_no desc');
                } else if ($order_by == 'customer_city_asc') {
                    $this->db->order_by('addresses.city desc, sales.reference_no desc');
                } else if ($order_by == 'seller_asc') {
                    $this->db->order_by('companies_seller.name asc, sales.reference_no desc');
                } else if ($order_by == 'seller_desc') {
                    $this->db->order_by('companies_seller.name desc, sales.reference_no desc');
                }
            } else {
                if ($group_by_seller == false && $group_by_customer == false && $group_by_city == false) {
                    $this->db->order_by('companies.name asc, sales.reference_no desc');
                } else if ($group_by_city) {
                    $this->db->order_by(($group_by_biller ? $gpb_sql : '').'addresses.state asc, addresses.city asc, companies.name asc, sales.address_id asc, sales.reference_no asc');
                }
            }
        }
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        $this->db->query('DROP TEMPORARY TABLE IF EXISTS '.$this->db->dbprefix("portfolio_payments"));
        return $resultado ;
    }

    public function get_sales_big_data($data)
    {
        $filter_year = isset($data['filter_year']) ? $data['filter_year'] : false;
        $biller = isset($data['biller']) ? $data['biller'] : false;
        $seller = isset($data['seller']) ? $data['seller'] : false;
        $customer = isset($data['customer']) ? $data['customer'] : false;
        $order = isset($data['order']) ? $data['order'] : false;
        $group_by_biller = isset($data['group_by_biller']) ? $data['group_by_biller'] : false;
        $group_by_seller = isset($data['group_by_seller']) ? $data['group_by_seller'] : false;
        $group_by_city = isset($data['group_by_city']) ? $data['group_by_city'] : false;
        $group_by_customer = isset($data['group_by_customer']) ? $data['group_by_customer'] : false;
        $group_by_customer_address = isset($data['group_by_customer_address']) ? $data['group_by_customer_address'] : false;
        $order_by = isset($data['order_by']) ? $data['order_by'] : false;
        if($biller=='all')  $biller = '';
        if($seller=='all')  $seller = '';
        if($customer=='all')  $customer = '';
        $payment_status = array('pending', 'due', 'partial');
        $result_data = [];
        $end_filter_year = date('Y', strtotime($this->session->userdata('portfolio_temp_end_date')));
        foreach ($this->filter_year_options as $key => $value) {
            if ($key > $end_filter_year) {
                break;
            }
            $this->db->select($this->db->dbprefix("portfolio_temp_".$key).'.id as cod_fact');
            $this->db->select($this->db->dbprefix("portfolio_temp_".$key).'.due_date');
            $this->db->select($this->db->dbprefix("portfolio_temp_".$key).'.date as Fecha_Creacion');
            $this->db->select('reference_no as Referencia');
            $this->db->select('paid as val_pago');
            $this->db->select('payment_status as estado');
            $this->db->select($this->db->dbprefix("portfolio_temp_".$key).'.payment_term as dias');
            $this->db->select('grand_total as total');
            $this->db->select('(grand_total-paid) as Saldo ');
            $this->db->select($this->db->dbprefix("companies").'.id as cod_companie');
            $this->db->select($this->db->dbprefix("companies").'.name as nom_companie ');
            $this->db->select($this->db->dbprefix("companies").'.vat_no as documento ');
            $this->db->select($this->db->dbprefix("companies").'.phone ');
            $this->db->select($this->db->dbprefix("companies").'.city ');
            $this->db->select($this->db->dbprefix("portfolio_temp_".$key).'.customer as nom_client');
            $this->db->select($this->db->dbprefix("portfolio_temp_".$key).'.biller_id as cod_Sucursal');
            $this->db->select($this->db->dbprefix("portfolio_temp_".$key).'.biller as Sucursal');
            $this->db->select($this->db->dbprefix("portfolio_temp_".$key).'.seller_id as Cod_vendedor');
            $this->db->select('IF('.$this->db->dbprefix("portfolio_temp_".$key).'.seller_id!='."'NUll'".','.$this->db->dbprefix("portfolio_temp_".$key).'.seller_id,"0") as cod_seller ');
            $this->db->select('IF(companies_seller.name!='."'NUll'".', companies_seller.name,"Sin Vendedor") as nom_seller');
            $this->db->select($this->db->dbprefix("addresses").'.sucursal as customer_sucursal');
            $this->db->select($this->db->dbprefix("addresses").'.state as customer_state');
            $this->db->select($this->db->dbprefix("addresses").'.city as customer_city');
            $this->db->select($this->db->dbprefix("addresses").'.direccion as customer_address');
            $this->db->from('portfolio_temp_'.$key);
            $this->db->join('companies', 'companies.id = '.$this->db->dbprefix("portfolio_temp_".$key).'.customer_id', 'left');
            $this->db->join('companies as companies_seller', ''.$this->db->dbprefix("portfolio_temp_".$key).'.seller_id = companies_seller.id','left');
            $this->db->join('addresses', 'addresses.id = '.$this->db->dbprefix("portfolio_temp_".$key).'.address_id', 'left');
            if($biller!='')
            {
                $this->db->where('(companies_seller.vat_no = '.$biller.' OR '.$this->db->dbprefix("portfolio_temp_".$key).'.biller_id = '.$biller.')');
            }
            if($seller!='')
            {
                if ($seller=='0') {
                    $this->db->where('('.$this->db->dbprefix("portfolio_temp_".$key).'.seller_id = '.$seller.' or companies_seller.vat_no = '.$seller.' or '.$this->db->dbprefix("portfolio_temp_".$key).'.seller_id = NULL)');
                } else{
                    $this->db->where('('.$this->db->dbprefix("portfolio_temp_".$key).'.seller_id = '.$seller.' or companies_seller.vat_no = '.$seller.')');
                }
            }
            $gpb_sql = $this->db->dbprefix("portfolio_temp_".$key).'.biller_id ASC, ';
            if($customer!='') $this->db->where('companies.vat_no',$customer);
            // $this->db->where_in('payment_status', $payment_status);
            if ($group_by_city || $group_by_seller || $group_by_customer) {
                if ($group_by_seller || $group_by_city) {
                    if (($group_by_seller && !$group_by_city) || ($group_by_seller && $group_by_city)) {
                        $this->db->order_by(($group_by_biller ? $gpb_sql : '')."companies_seller.name asc, addresses.state asc, addresses.city asc, companies.name asc, ".$this->db->dbprefix("portfolio_temp_".$key).".address_id asc, ".$this->db->dbprefix("portfolio_temp_".$key).".reference_no asc");
                    } else if (!$group_by_seller && $group_by_city) {
                        $this->db->order_by(($group_by_biller ? $gpb_sql : '')."addresses.state asc, addresses.city asc, companies.name asc, ".$this->db->dbprefix("portfolio_temp_".$key).".address_id asc, ".$this->db->dbprefix("portfolio_temp_".$key).".reference_no asc");
                    }
                } else if ($group_by_customer) {
                    $this->db->order_by(($group_by_biller ? $gpb_sql : '')."companies.name asc, ".$this->db->dbprefix("portfolio_temp_".$key).".address_id asc, ".$this->db->dbprefix("portfolio_temp_".$key).".reference_no asc");
                }
            } else {
                if ($order_by != "") {
                    if ($order_by == 'customer_asc') {
                        $this->db->order_by('companies.name asc, '.$this->db->dbprefix("portfolio_temp_".$key).'.reference_no desc');
                    } else if ($order_by == 'customer_desc') {
                        $this->db->order_by('companies.name desc, '.$this->db->dbprefix("portfolio_temp_".$key).'.reference_no desc');
                    } else if ($order_by == 'customer_city_asc') {
                        $this->db->order_by('addresses.city asc, '.$this->db->dbprefix("portfolio_temp_".$key).'.reference_no desc');
                    } else if ($order_by == 'customer_city_asc') {
                        $this->db->order_by('addresses.city desc, '.$this->db->dbprefix("portfolio_temp_".$key).'.reference_no desc');
                    } else if ($order_by == 'seller_asc') {
                        $this->db->order_by('companies_seller.name asc, '.$this->db->dbprefix("portfolio_temp_".$key).'.reference_no desc');
                    } else if ($order_by == 'seller_desc') {
                        $this->db->order_by('companies_seller.name desc, '.$this->db->dbprefix("portfolio_temp_".$key).'.reference_no desc');
                    }
                } else {
                    if ($group_by_seller == false && $group_by_customer == false && $group_by_city == false) {
                        $this->db->order_by('companies.name asc, '.$this->db->dbprefix("portfolio_temp_".$key).'.reference_no desc');
                    } else if ($group_by_city) {
                        $this->db->order_by(($group_by_biller ? $gpb_sql : '').'addresses.state asc, addresses.city asc, companies.name asc, '.$this->db->dbprefix("portfolio_temp_".$key).'.address_id asc, '.$this->db->dbprefix("portfolio_temp_".$key).'.reference_no asc');
                    }
                }
            }
            $consulta = $this->db->get();
            $resultado = $consulta->result_array();
            $result_data = array_merge($result_data, $resultado);
        }
        return $result_data;
    }
    //INFORME DE CARTERA

    //INFORME CUENTAS POR PAGAR JAIME

    public function get_provider()
    {
        $payment_status = array('pending', 'partial', 'due');
        $this->db->select($this->db->dbprefix('companies').'.id as cod_id');
        $this->db->select_max($this->db->dbprefix('companies').'.name', 'nombre');
        $this->db->from('purchases');
        $this->db->join('companies', 'purchases.supplier_id = companies.id');
        $this->db->where('group_name','supplier');
        $this->db->where('(grand_total-paid) > 0 ');
        $this->db->where_in('payment_status', $payment_status);
        $this->db->group_by("companies.id");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_purchases($provider = '', $biller = '', $end_date = NULL)
    {
        if($provider=='all')  $provider = '';
        if($biller=='all')  $biller = '';
        if (!$end_date) {
            $end_date = date('Y-m-d H:i:s');
        } else {
            $end_date .= " 23:59:59";
        }
        $payment_status = array('pending', 'partial', 'due');
        $this->db->select('payment_status as estado');
        $this->db->select('(grand_total-COALESCE(tpayments.amount, 0)) as Saldo');
        $this->db->select($this->db->dbprefix('companies').'.id as cod_companie');
        $this->db->select('grand_total as total');
        $this->db->select('reference_no as Referencia');
        $this->db->select('purchases.date as Fecha');
        $this->db->select('COALESCE(tpayments.amount, 0) as val_pago');
        $this->db->select('due_date');
        $this->db->select('supplier');
        $this->db->from('purchases');
        $this->db->join('companies', 'purchases.supplier_id = companies.id');
        $this->db->join('(SELECT purchase_id, SUM(amount) AS amount FROM '.$this->db->dbprefix("payments").' WHERE date <= "'.$end_date.'" GROUP BY purchase_id) as tpayments', 'tpayments.purchase_id = '.$this->db->dbprefix("purchases").'.id', 'left');
        $this->db->where('group_name','supplier');
        $this->db->where('(grand_total-COALESCE(tpayments.amount, 0)) > 0 ');
        // $this->db->where_in('payment_status', $payment_status);
        if($provider!='') $this->db->where('companies.id',$provider);
        if($biller!='') $this->db->where('purchases.biller_id',$biller);

        if($end_date)
        {
            $this->db->where($this->db->dbprefix('purchases').'.date <=', $end_date);
        }

        $this->db->order_by('purchases.supplier_id ASC, supplier ASC, purchases.date asc, purchases.reference_no asc');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    //INFORME CUENTAS POR PAGAR JAIME


    /* Informe Zeta */

    public function get_zeta_sales_biller()
    {
        $this->db->select($this->db->dbprefix('companies').'.id as cod_companie');
        $this->db->select_max('name', 'nombre');
        $this->db->from('sales');
        $this->db->join('companies', 'companies.id = sales.biller_id');
        $this->db->where('sale_status','completed');
        $this->db->group_by($this->db->dbprefix('companies').".id");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_biller()
    {
        $this->db->select($this->db->dbprefix('companies').'.id as cod_companie');
        $this->db->select_max('name', 'nombre');
        $this->db->from('sales');
        $this->db->join('companies', 'companies.id = sales.biller_id');
        $this->db->where('sale_status','completed');
        $this->db->group_by($this->db->dbprefix('companies').".id");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_billing($biller = NULL, $fech_ini = NULL, $fech_fin = NULL, $tipo = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $this->db->select($this->db->dbprefix('sales').'.id');
        $this->db->select_max($this->db->dbprefix('sales').'.reference_no');
        $this->db->select_max($this->db->dbprefix('sales').'.customer');
        $this->db->select_max($this->db->dbprefix('sales').'.date','fecha');
        $this->db->select_max('total');
        $this->db->select_max('tip_amount');
        $this->db->select_max('shipping');
        $this->db->select_max('total_tax');
        $this->db->select_max('grand_total');
        $this->db->select_max('paid');
        $this->db->select('IF('.$this->db->dbprefix('payment_methods').'.name IS NOT NULL,'.$this->db->dbprefix('payment_methods').'.name,"Credito") as name ');
        $this->db->select('count('.$this->db->dbprefix('payments').'.sale_id) as cantidad ');
        $this->db->select('(grand_total-SUM(COALESCE('.$this->db->dbprefix('payments').'.amount , 0))) As saldo ');
        $this->db->select_min($this->db->dbprefix('payments').'.date','fecha_pay');
        $this->db->from('sales');
        $this->db->join('payments', "payments.sale_id = sales.id AND payments.date BETWEEN '".$fech_ini."' AND '".$fech_fin."'",'left');
        $this->db->join('payment_methods', 'payments.paid_by = payment_methods.code','left');
        $this->db->where($this->db->dbprefix('sales').".date BETWEEN '".$fech_ini."' AND '".$fech_fin."'");
        if(!$tipo) {
            $this->db->where('grand_total > 0 ');

            if ($document_type_id) {
                $this->db->where($this->db->dbprefix('sales').'.document_type_id', $document_type_id);
            }

        } else {
            $this->db->where('grand_total < 0 ');
        }

        if ($module_id) {
            if ($module_id == 1) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 1);
            } else if ($module_id == 2) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 0);
            }
        }

        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id',$biller);
        }
        $this->db->group_by($this->db->dbprefix('sales').".id");
        $this->db->order_by($this->db->dbprefix('sales').'.document_type_id ASC, '.$this->db->dbprefix('sales').'.date ASC');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        // $this->sma->print_arrays($resultado);
        return $resultado ;
    }

    public function get_total_payment($biller = NULL,$fech_ini = NULL,$fech_fin = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $this->db->select('IF(
                                '.$this->db->dbprefix('payment_methods').'.name IS NOT NULL, '.$this->db->dbprefix('payment_methods').'.name,
                                IF('.$this->db->dbprefix('payments').'.paid_by = "retencion", \'Retenciones aplicadas\', "N/R")
                            ) as name');
        $this->db->select(' sum(IF('.$this->db->dbprefix('payments').'.amount > 0, '.$this->db->dbprefix('payments').'.amount, 0)) as total_payment ');
        $this->db->select(' sum(IF('.$this->db->dbprefix('payments').'.amount < 0, '.$this->db->dbprefix('payments').'.amount, 0)) as total_return_payment ');
        $this->db->from('sales');
        $this->db->join('payments', 'payments.sale_id = sales.id  AND  payments.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        $this->db->join('payment_methods', 'payments.paid_by = payment_methods.code', 'left');
        $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if ($module_id) {
            if ($module_id == 1) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 1);
            } else if ($module_id == 2) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 0);
            }
        }
        if ($document_type_id) {
            $this->db->where($this->db->dbprefix('sales').'.document_type_id', $document_type_id);
        }
        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id',$biller);
        }
        $this->db->group_by($this->db->dbprefix('payment_methods').'.name');
        $this->db->order_by($this->db->dbprefix('payment_methods').'.name ASC');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_total_tax_rates($biller = NULL,$fech_ini = NULL,$fech_fin = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $this->db->select($this->db->dbprefix('tax_rates').'.name');
        $this->db->select(' sum(net_unit_price*quantity) as unisubtotal ');
        $this->db->select_sum('item_tax');
        $this->db->select_sum('subtotal');
        $this->db->select('SUM('.$this->db->dbprefix('sales').'.order_discount * ('.$this->db->dbprefix('sale_items').'.subtotal / ('.$this->db->dbprefix('sales').'.grand_total + '.$this->db->dbprefix('sales').'.order_discount))) as order_discount');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id');
        $this->db->join('tax_rates', 'tax_rates.id = sale_items.tax_rate_id');
        $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id',$biller);
        }
        if ($module_id) {
            if ($module_id == 1) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 1);
            } else if ($module_id == 2) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 0);
            }
        }
        if ($document_type_id) {
            $this->db->where($this->db->dbprefix('sales').'.document_type_id', $document_type_id);
        }
        $this->db->group_by('tax_rates.name');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_total_categories_tax($biller = NULL,$fech_ini = NULL,$fech_fin = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $query = $this->db->query("
            CREATE TEMPORARY TABLE sma_Temp
            SELECT
            sma_sale_items.quantity as total_items,
            net_unit_price*sma_sale_items.quantity as unisubtotal,
            item_tax AS item_tax,
            subtotal AS subtotal,
            sma_sale_items.product_id
            FROM sma_sales
            inner JOIN sma_sale_items ON sma_sales.id = sma_sale_items.sale_id
            WHERE sma_sales.date BETWEEN '$fech_ini' AND '$fech_fin'
            ".($biller ? "AND sma_sales.biller_id = '$biller'" : "")."
            ".($module_id ? "AND sma_sales.pos = ".($module_id == 2 ? 0 : 1) : "")."
            ".($document_type_id ? "AND sma_sales.document_type_id = ".$document_type_id : "")."
        ");

        $query = $this->db->query("
            CREATE TEMPORARY TABLE sma_Temp2
            select
                sma_categories.code AS codigo,
                sma_categories.name AS Nombre,
                unisubtotal,
                item_tax,
                subtotal,
                product_id,
                total_items
            from sma_Temp
            LEFT join sma_products on sma_Temp.product_id = sma_products.id
            LEFT join sma_categories on sma_categories.id = sma_products.category_id
        ");
        $this->db->select('COUNT(unisubtotal) as trans');
        $this->db->select_sum('total_items');
        $this->db->select_sum('unisubtotal');
        $this->db->select_sum('item_tax');
        $this->db->select_sum('subtotal');
        $this->db->select_MAX('Nombre');
        $this->db->select_MAX('codigo');
        $this->db->from('Temp2');
        $this->db->group_by('codigo');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        // $this->sma->print_arrays($resultado);
        return $resultado ;
    }

    public function get_total_users_tax($biller = NULL,$fech_ini = NULL,$fech_fin = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $this->db->select_max('first_name');
        $this->db->select_max('last_name');
        $this->db->select_max('user_pc_serial');
        $this->db->select_sum('total');
        $this->db->select_sum('total_items');
        $this->db->select_sum('total_tax');
        $this->db->select_sum('grand_total');
        $this->db->select('count('.$this->db->dbprefix('sales').'.id) AS Cantidad');
        $this->db->from('sma_sales');
        $this->db->join('sma_users', 'sma_sales.created_by = sma_users.id');
        $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id',$biller);
        }
        if ($module_id) {
            if ($module_id == 1) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 1);
            } else if ($module_id == 2) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 0);
            }
        }
        if ($document_type_id) {
            $this->db->where($this->db->dbprefix('sales').'.document_type_id', $document_type_id);
        }
        $this->db->group_by('created_by');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }
    public function get_total_sellers_tax($biller = NULL,$fech_ini = NULL,$fech_fin = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $this->db->select_max('first_name');
        $this->db->select_max('first_lastname');
        $this->db->select_sum('total');
        $this->db->select_sum('total_tax');
        $this->db->select_sum('total_items');
        $this->db->select_sum('grand_total');
        $this->db->select('count('.$this->db->dbprefix('sales').'.id) AS Cantidad');
        $this->db->from('sma_sales');
        $this->db->join('sma_companies', 'sma_sales.seller_id = sma_companies.id');
        $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id',$biller);
        }
        if ($module_id) {
            if ($module_id == 1) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 1);
            } else if ($module_id == 2) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 0);
            }
        }
        if ($document_type_id) {
            $this->db->where($this->db->dbprefix('sales').'.document_type_id', $document_type_id);
        }
        $this->db->order_by('sma_sales.seller_id ASC');
        $this->db->group_by('sma_companies.id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_total_customers_tax($biller = NULL,$fech_ini = NULL,$fech_fin = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $this->db->select_max('name');
        $this->db->select_sum('total');
        $this->db->select_sum('total_tax');
        $this->db->select_sum('grand_total');
        $this->db->select('count('.$this->db->dbprefix('sales').'.id) AS Cantidad');
        $this->db->from('sma_sales');
        $this->db->join('sma_companies', 'sma_sales.customer_id = sma_companies.id');
        $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id',$biller);
        }
        if ($module_id) {
            if ($module_id == 1) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 1);
            } else if ($module_id == 2) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 0);
            }
        }
        if ($document_type_id) {
            $this->db->where($this->db->dbprefix('sales').'.document_type_id', $document_type_id);
        }
        $this->db->group_by('sma_companies.id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }
    /* End Informe Zeta */

    /* Informe ventas vendedores */
    public function get_seller($biller = NULL)
    {
        $this->db->select('IF('.$this->db->dbprefix('companies').'.vat_no!='."'NUll'".','.$this->db->dbprefix('companies').'.vat_no,"0") as cod_seller');
        $this->db->select('IF('.$this->db->dbprefix('companies').'.vat_no!='."'NUll'".','.$this->db->dbprefix('companies').'.name,"Sin Vendedor") as nombre ');
        $this->db->select('IF('.$this->db->dbprefix('companies').'.vat_no!='."'NUll'".','.$this->db->dbprefix('companies').'.vat_no,"0") as Document ');
        $this->db->from('sales');
        $this->db->join('companies', 'sales.seller_id = companies.id','left');
        if($biller)
        {
            $this->db->where($this->db->dbprefix('companies').'.vat_no',$biller);
            $this->db->or_where($this->db->dbprefix('sales').'.biller_id', $biller);
        }
        $this->db->where('status =', 1);
        $this->db->group_by($this->db->dbprefix('companies').".vat_no");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_customer($biller = '',$seller = '')
    {
        if($biller=='all')  $biller = '';
        if($seller=='all')  $seller = '';
        $this->db->select($this->db->dbprefix('companies').'.vat_no as cod_id');
        $this->db->select_max('customer', 'nombre');
        $this->db->from('sales');
        $this->db->join('companies', 'companies.id = sales.customer_id');
        $this->db->join('companies as companies_seller', 'companies_seller.id = sales.seller_id','left');
        if($biller!='') $this->db->where($this->db->dbprefix('sales').'.biller_id',$biller);
        if($seller!='')
        {
            $this->db->where($this->db->dbprefix('sales').'.seller_id',$seller);
            if($seller=='0')
                $this->db->or_where($this->db->dbprefix('sales').'.seller_id',NULL);
        }
        $this->db->group_by($this->db->dbprefix('sales').".customer_id");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_seller_bills($biller = '',$seller = '',$customer = '',$fech_ini = '',$fech_fin = '',$zone = '',$subzone = '')
    {
        if($biller=='all')  $biller = '';
        if($seller=='all')  $seller = '';
        if($customer=='all')  $customer = '';
        if($zone=='false')  $zone = '';
        if($subzone=='false')  $subzone = '';
        $this->db->select($this->db->dbprefix('sales').'.id ');
        $this->db->select($this->db->dbprefix('sales').'.date');
        $this->db->select('return_sale_ref');
        $this->db->select('reference_no');
        $this->db->select('paid');
        $this->db->select('payment_status');
        $this->db->select('zones.zone_code AS cod_zone');
        $this->db->select('zones.zone_name AS nom_zone');
        $this->db->select('subzones.subzone_code AS cod_subzone');
        $this->db->select('subzones.subzone_name AS nom_subzone');
        $this->db->select('grand_total');
        $this->db->select('(total-order_discount) As total');
        $this->db->select('total_tax');
        $this->db->select('customer as customer_name');
        $this->db->select($this->db->dbprefix('sales').'.customer as nom_client');
        $this->db->select($this->db->dbprefix('sales').'.biller_id as cod_Sucursal');
        $this->db->select($this->db->dbprefix('sales').'.biller as Sucursal');
        $this->db->select('IF('.$this->db->dbprefix('sales').'.seller_id!='."'NUll'".','.$this->db->dbprefix('sales').'.seller_id,"0") as cod_seller ');
        $this->db->select('IF(companies_seller.name!='."'NUll'".',companies_seller.name,"Sin Vendedor") as nom_seller ');
        $this->db->from('sales');
        if($fech_fin!='') $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        $this->db->join('companies', 'companies.id = sales.customer_id');
        $this->db->join('addresses', 'addresses.id = sales.address_id');
        $this->db->join('zones', 'addresses.location = zones.id', 'left');
        $this->db->join('subzones', 'addresses.subzone = subzones.id', 'left');
        $this->db->join('companies as companies_seller', 'sales.seller_id = companies_seller.id','left');
        if ($zone) {
            $this->db->where('addresses.location', $zone);
        }
        if ($subzone) {
            $this->db->where('addresses.subzone', $subzone);
        }
        if($biller!='')
        {
            $this->db->where('('.$this->db->dbprefix('companies').'.vat_no = "'.$biller.'" OR '.$this->db->dbprefix('sales').'.biller_id = "'.$biller.'")');
            // $this->db->where($this->db->dbprefix('companies').'.vat_no',$biller);
            // $this->db->or_where($this->db->dbprefix('sales').'.biller_id', $biller);
        }
        if($seller!='')
        {
            $this->db->where('(companies_seller.vat_no = "'.$seller.'" OR '.$this->db->dbprefix('sales').'.seller_id = "'.$seller.'")');
            // $this->db->where($this->db->dbprefix('sales').'.seller_id',$seller);
            // $this->db->or_where('companies_seller.vat_no',$seller);
            // if($seller=='0')
                // $this->db->or_where($this->db->dbprefix('sales').'.seller_id',NULL);
        }
        if($customer!='') $this->db->where($this->db->dbprefix('companies').'.vat_no',$customer);
        $this->db->order_by($this->db->dbprefix('sales').'.seller_id ASC, '.$this->db->dbprefix('addresses').'.location ASC, '.$this->db->dbprefix('sales').'.date DESC, '.$this->db->dbprefix('sales').'.reference_no ASC');
        $this->db->limit(15000);
        $consulta = $this->db->get();
        // exit(($this->db->last_query()));
        $resultado = $consulta->result_array();
        return $resultado ;
    }
    /* End Informe Zeta */

    /* Informe Rentabilidad documento  */
    public function get_profitability_doct($tipo_doc = '',$fech_ini = '',$fech_fin = '', $biller = '')
    {
        if($tipo_doc=='all')  $tipo_doc = '';
        if($biller=='all')  $biller = '';

        if ($this->Settings->cost_to_profit_and_validation == 2) {
            $cost_profit = 'COALESCE(avg_net_unit_cost, 0) + COALESCE(consumption_sales_costing, 0)';
        } else {
            $cost_profit = $this->db->dbprefix('products').'.cost + COALESCE('.$this->db->dbprefix('products').'.consumption_purchase_tax, 0)';
        }

        $this->db->select_max('date');
        $this->db->select_max('reference_no');
        $this->db->select_max('return_sale_ref');
        $this->db->select_max('customer_id');
        $this->db->select_max('document_type_id');
        $this->db->select_max('customer');
        $this->db->select_max('documents_types.sales_prefix');
        $this->db->select('sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity) as total');
        $this->db->select('sum(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity) as costo ');
        $this->db->select('(sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity)-sum(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity)) as utilidad ');
        $this->db->select('(((sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity)-sum(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity))/ sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity))*100) as margen');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id');
        $this->db->join('products', 'products.id = sale_items.product_id');
        $this->db->join('documents_types', 'documents_types.id = sales.document_type_id');
        if($fech_fin!='') $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if($tipo_doc!='') $this->db->like('reference_no', $tipo_doc, 'after');
        if($biller!='') $this->db->where('sales.biller_id', $biller);
        $this->db->group_by($this->db->dbprefix('sales').".id");
        $this->db->order_by($this->db->dbprefix('sales').".document_type_id ASC, ".$this->db->dbprefix('sales').".reference_no ASC");
        $this->db->limit(15000);
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_cod_customer()
    {
        $this->db->select('customer_id');
        $this->db->select_max('name');
        $this->db->from('sales');
        $this->db->join('sma_companies', 'sma_sales.customer_id = sma_companies.id');
        $this->db->group_by($this->db->dbprefix('sales').".customer_id");
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    /* End Rentabilidad documento */

    /* Informe Rentabilidad Cliente  */
    public function get_profitability_detall_customer($customer = '',$fech_ini = '',$fech_fin = '', $biller = '')
    {
        if($customer=='all')  $customer = '';
        if($biller=='all')  $biller = '';
        $this->db->select_max('date');
        $this->db->select_max('reference_no');
        $this->db->select_max('return_sale_ref');
        $this->db->select_max('customer_id');
        $this->db->select_max('customer');
        $this->db->select_max('companies.vat_no');
        $this->db->select('sum(net_unit_price * quantity) as total');
        $this->db->select('sum(avg_net_unit_cost * quantity) as costo ');
        $this->db->select('(sum(net_unit_price * quantity)-sum(avg_net_unit_cost * quantity)) as utilidad ');
        $this->db->select('(((sum(net_unit_price * quantity)-sum(avg_net_unit_cost * quantity))/ sum(net_unit_price * quantity))*100) as margen');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id');
        $this->db->join('companies', 'companies.id = sales.customer_id');
        if($fech_fin!='') $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if($customer!='') $this->db->where('customer_id', $customer);
        if($biller!='') $this->db->where('sales.biller_id', $biller);
        $this->db->group_by($this->db->dbprefix('sales').".id");
        $this->db->order_by($this->db->dbprefix('sales').".customer_id, ".$this->db->dbprefix('sales').".date ASC");
        $this->db->limit(15000);
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_profitability_customer($customer = '',$fech_ini = '',$fech_fin = '', $biller = '')
    {
        if($customer=='all')  $customer = '';
        if($biller=='all')  $biller = '';
        if ($this->Settings->cost_to_profit_and_validation == 2) {
            $cost_profit = '(avg_net_unit_cost + COALESCE(sma_sale_items.consumption_sales_costing, 0))';
        } else {
            $cost_profit = "(".$this->db->dbprefix('products').'.cost + COALESCE('.$this->db->dbprefix('products').'.consumption_purchase_tax, 0))';
        }
        $query = $this->db->query("
        CREATE TEMPORARY TABLE sma_Temp
        SELECT
        MIN(date) As Fecha,
        MAX(customer_id) AS customer_id,
        sma_sales.biller_id AS biller_id,
        MAX(customer) AS customer,
        sum((net_unit_price + COALESCE(sma_sale_items.consumption_sales, 0)) * sma_sale_items.quantity) AS total,
        sum(".$cost_profit." * sma_sale_items.quantity) as costo
        FROM sma_sales
        JOIN sma_sale_items ON sma_sales.id = sma_sale_items.sale_id
        JOIN sma_products ON sma_products.id = sma_sale_items.product_id
        ".($biller != '' ? " WHERE sma_sales.biller_id = ".$biller : "")."
        ".($biller != '' && $fech_ini != '' ? " AND sma_sales.date >= '".$fech_ini."' AND sma_sales.date <= '".$fech_fin."'" : "")."
        ".($biller == '' && $fech_ini != '' ? " WHERE sma_sales.date >= '".$fech_ini."' AND sma_sales.date <= '".$fech_fin."'" : "")."
        GROUP BY sma_sales.id ");

        $this->db->select_MIN('Fecha');
        $this->db->select_MAX('customer');
        $this->db->select_sum('total');
        $this->db->select_sum('costo');
        $this->db->select('(sum(total)-sum(costo)) as Utilidad ');
        $this->db->select('((sum(total)-sum(costo))/sum(total))*100 as margen  ');
        $this->db->from('Temp');
        if($customer!='') $this->db->where('customer_id', $customer);
        $this->db->group_by('customer_id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    /* End Rentabilidad Cliente */

    /* Informe Rentabilidad Producto  */

    public function get_cod_producto_sale()
    {
        $this->db->select($this->db->dbprefix('products').'.id , '.$this->db->dbprefix('products').'.code');
        $this->db->select_MAX('name');
        $this->db->from('products');
        $this->db->join('sale_items', 'sale_items.product_id = products.id');
        $this->db->group_by($this->db->dbprefix('products').'.id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_profitability_detall_producto($producto = '',$fech_ini = '',$fech_fin = '',$biller_id = '')
    {
        if ($this->Settings->cost_to_profit_and_validation == 2) {
            $cost_profit = 'avg_net_unit_cost + COALESCE(consumption_sales_costing, 0)';
        } else {
            $cost_profit = $this->db->dbprefix('products').'.cost + COALESCE('.$this->db->dbprefix('products').'.consumption_purchase_tax, 0)';
        }

        if($producto=='all')  $producto = '';
        if($biller_id=='all')  $biller_id = '';
        $this->db->select($this->db->dbprefix('sales').".id");
        $this->db->select('date');
        $this->db->select('reference_no');
        $this->db->select('name');
        $this->db->select('return_sale_ref');
        $this->db->select('customer_id');
        $this->db->select('customer');
        $this->db->select('total');
        $this->db->select('(net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) AS net_unit_price');
        $this->db->select('('.$cost_profit.') AS avg_net_unit_cost');
        $this->db->select($this->db->dbprefix('sale_items').'.quantity');
        $this->db->select($this->db->dbprefix('sale_items').".id");
        $this->db->from('sales');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id');
        $this->db->join('products', 'sale_items.product_id = products.id');
        if($fech_fin!='') $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if($producto!='') $this->db->where($this->db->dbprefix('products').'.id', $producto);
        if($biller_id!='') $this->db->where($this->db->dbprefix('sales').'.biller_id', $biller_id);
        $this->db->order_by($this->db->dbprefix('sales').".id ASC, ".$this->db->dbprefix('products').".name ASC");
        $this->db->limit(15000);
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_profitability_producto($producto = '',$fech_ini = '',$fech_fin = '', $biller_id = '')
    {
        if($producto=='all')  $producto = '';
        if($biller_id=='all')  $biller_id = '';

        if ($this->Settings->cost_to_profit_and_validation == 2) {
            $cost_profit = 'COALESCE(avg_net_unit_cost, 0) + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales_costing, 0)';
        } else {
            $cost_profit = $this->db->dbprefix('products').'.cost + COALESCE('.$this->db->dbprefix('products').'.consumption_purchase_tax, 0)';
        }

        $this->db->select_MIN('date');
        $this->db->select($this->db->dbprefix('products').'.id AS cod_product');
        $this->db->select_MAX('name','name');
        $this->db->select('SUM((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity) AS total');
        $this->db->select('SUM(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity) AS costo');
        $this->db->select('(sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity)-sum(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity)) as utilidad ');
        $this->db->select('(((sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity)-sum(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity))/ sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity))*100) as margen');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id ');
        $this->db->join('products', 'sale_items.product_id = products.id');
        if($fech_ini!='') $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if($producto!='') $this->db->where($this->db->dbprefix('products').'.id', $producto);
        if ($biller_id!='') {
            $this->db->where($this->db->dbprefix('sales').'.biller_id', $biller_id);
        }
        $this->db->group_by($this->db->dbprefix('products').'.id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function get_profitability_producto_xls($producto = '',$fech_ini = null, $fech_fin = null, $biller_id = '')
    {
        if($producto=='all')  $producto = '';
        if($biller_id=='all')  $biller_id = '';

        if ($this->Settings->cost_to_profit_and_validation == 2) {
            $cost_profit = 'COALESCE(avg_net_unit_cost, 0) + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales_costing, 0)';
        } else {
            $cost_profit = $this->db->dbprefix('products').'.cost + COALESCE('.$this->db->dbprefix('products').'.consumption_purchase_tax, 0)';
        }

        $this->db->select_MIN('date');
        $this->db->select($this->db->dbprefix('products').'.code AS cod_product');
        $this->db->select_MAX('name','name');
        $this->db->select('SUM((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity) AS total');
        $this->db->select('SUM(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity) AS costo');
        $this->db->select('(sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity)-sum(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity)) as utilidad ');
        $this->db->select('(((sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity)-sum(('.$cost_profit.') * '.$this->db->dbprefix('sale_items').'.quantity))/ sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('sale_items').'.quantity))*100) as margen');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id ');
        $this->db->join('products', 'sale_items.product_id = products.id');
        if($fech_ini != null) $this->db->where($this->db->dbprefix('sales').'.date > '."'$fech_ini'");
        if($fech_fin != null) $this->db->where($this->db->dbprefix('sales').'.date < '."'$fech_fin'");
        if($producto!='') $this->db->where($this->db->dbprefix('products').'.id', $producto);
        if ($biller_id!='') {
            $this->db->where($this->db->dbprefix('sales').'.biller_id', $biller_id);
        }
        $this->db->group_by($this->db->dbprefix('products').'.id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    /* End Rentabilidad Producto */

    //INFORMES JAIME FIN


    public function get_movements($company_id, $address_id = NULL, $start_date = NULL, $end_date = NULL){
        $and_sales = '';
        $and_deposits = '';
        $and_payments = '';

        if ($start_date) {
            $and_sales .= " AND {$this->db->dbprefix('sales')}.date >= '".$start_date."' AND {$this->db->dbprefix('sales')}.date <= '".$end_date."'";
            $and_deposits .= " AND {$this->db->dbprefix('deposits')}.date >= '".$start_date."' AND {$this->db->dbprefix('deposits')}.date <= '".$end_date."'";
            $and_payments .= " AND {$this->db->dbprefix('payments')}.date >= '".$start_date."' AND {$this->db->dbprefix('payments')}.date <= '".$end_date."'";
        }

        if ($address_id) {
            $and_sales .= " AND {$this->db->dbprefix('sales')}.address_id = ".$address_id;
            $and_payments .= " AND {$this->db->dbprefix('sales')}.address_id = ".$address_id;
        }


            $and_payments .= " AND {$this->db->dbprefix('payments')}.multi_payment = 1";

        $sql = "SELECT
                    {$this->db->dbprefix('sales')}.date,
                    {$this->db->dbprefix('sales')}.reference_no,
                    {$this->db->dbprefix('addresses')}.sucursal,
                    IF({$this->db->dbprefix('sales')}.grand_total < 0, ({$this->db->dbprefix('sales')}.grand_total * -1), 0) as incoming,
                    IF({$this->db->dbprefix('sales')}.grand_total > 0, {$this->db->dbprefix('sales')}.grand_total, 0) as outcoming
                FROM {$this->db->dbprefix('sales')}
                    INNER JOIN {$this->db->dbprefix('addresses')} ON {$this->db->dbprefix('addresses')}.id = {$this->db->dbprefix('sales')}.address_id
                WHERE {$this->db->dbprefix('sales')}.customer_id = {$company_id} {$and_sales}

                UNION ALL

                SELECT
                    {$this->db->dbprefix('deposits')}.date,
                    {$this->db->dbprefix('deposits')}.reference_no,
                    NULL,
                    IF({$this->db->dbprefix('deposits')}.amount > 0, {$this->db->dbprefix('deposits')}.amount, 0) as incoming,
                    IF({$this->db->dbprefix('deposits')}.amount < 0, ({$this->db->dbprefix('deposits')}.amount * -1), 0) as outcoming
                FROM {$this->db->dbprefix('deposits')}
                WHERE {$this->db->dbprefix('deposits')}.company_id = {$company_id} {$and_deposits}

                UNION ALL

                SELECT
                    {$this->db->dbprefix('payments')}.date,
                    {$this->db->dbprefix('payments')}.reference_no,
                    {$this->db->dbprefix('addresses')}.sucursal,
                    IF({$this->db->dbprefix('payments')}.amount > 0, {$this->db->dbprefix('payments')}.amount, 0) as incoming,
                    IF({$this->db->dbprefix('payments')}.amount < 0, ({$this->db->dbprefix('payments')}.amount * -1), 0) as outcoming
                FROM {$this->db->dbprefix('payments')}
                    INNER JOIN {$this->db->dbprefix('sales')} ON {$this->db->dbprefix('sales')}.id = {$this->db->dbprefix('payments')}.sale_id
                    INNER JOIN {$this->db->dbprefix('companies')} ON {$this->db->dbprefix('companies')}.id = {$this->db->dbprefix('sales')}.customer_id
                    INNER JOIN {$this->db->dbprefix('addresses')} ON {$this->db->dbprefix('addresses')}.id = {$this->db->dbprefix('sales')}.address_id
                WHERE {$this->db->dbprefix('companies')}.id = {$company_id} {$and_payments}

                    ORDER BY date ASC
                ;";

        $q = $this->db->query($sql);
        if (($q->num_rows() > 0)) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }


    public function get_movements_SI($company_id, $address_id = NULL, $start_date = NULL){
        $and_sales = '';
        $and_deposits = '';
        $and_payments = '';

        if ($start_date) {
            $and_sales .= " AND {$this->db->dbprefix('sales')}.date < '".$start_date."'";
            $and_deposits .= " AND {$this->db->dbprefix('deposits')}.date < '".$start_date."'";
            $and_payments .= " AND {$this->db->dbprefix('payments')}.date < '".$start_date."'";

            if ($address_id) {
                $and_sales .= " AND {$this->db->dbprefix('sales')}.address_id = ".$address_id;
                $and_payments .= " AND {$this->db->dbprefix('sales')}.address_id = ".$address_id;
            }

            $sql = "SELECT
                        SUM({$this->db->dbprefix('sales')}.grand_total) as balance
                    FROM {$this->db->dbprefix('sales')}
                        INNER JOIN {$this->db->dbprefix('addresses')} ON {$this->db->dbprefix('addresses')}.id = {$this->db->dbprefix('sales')}.address_id
                    WHERE {$this->db->dbprefix('sales')}.customer_id = {$company_id} {$and_sales}

                    UNION ALL

                    SELECT
                        SUM({$this->db->dbprefix('deposits')}.amount) as balance
                    FROM {$this->db->dbprefix('deposits')}
                    WHERE {$this->db->dbprefix('deposits')}.company_id = {$company_id} {$and_deposits}

                    UNION ALL

                    SELECT
                        SUM({$this->db->dbprefix('payments')}.amount) as balance
                    FROM {$this->db->dbprefix('payments')}
                        INNER JOIN {$this->db->dbprefix('sales')} ON {$this->db->dbprefix('sales')}.id = {$this->db->dbprefix('payments')}.sale_id
                        INNER JOIN {$this->db->dbprefix('companies')} ON {$this->db->dbprefix('companies')}.id = {$this->db->dbprefix('sales')}.customer_id
                        INNER JOIN {$this->db->dbprefix('addresses')} ON {$this->db->dbprefix('addresses')}.id = {$this->db->dbprefix('sales')}.address_id
                    WHERE {$this->db->dbprefix('companies')}.id = {$company_id} {$and_payments}

                    ;";

            $q = $this->db->query($sql);
            $balance = 0;
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $balance += $row->balance;
                }
            }
            return $balance;
        } else {
            return 0;
        }
    }

    public function get_supplier_movements($company_id, $start_date = NULL, $end_date = NULL){
        $and_purchases = '';
        $and_payments = '';

        if ($start_date) {
            $and_purchases .= " AND {$this->db->dbprefix('purchases')}.date >= '".$start_date."' AND {$this->db->dbprefix('purchases')}.date <= '".$end_date."'";
            $and_payments .= " AND {$this->db->dbprefix('payments')}.date >= '".$start_date."' AND {$this->db->dbprefix('payments')}.date <= '".$end_date."'";
        }

        $sql = "SELECT
                    {$this->db->dbprefix('purchases')}.date,
                    {$this->db->dbprefix('purchases')}.reference_no,
                    IF({$this->db->dbprefix('purchases')}.grand_total < 0, ({$this->db->dbprefix('purchases')}.grand_total * -1), 0) as incoming,
                    IF({$this->db->dbprefix('purchases')}.grand_total > 0, {$this->db->dbprefix('purchases')}.grand_total, 0) as outcoming
                FROM {$this->db->dbprefix('purchases')}
                WHERE {$this->db->dbprefix('purchases')}.supplier_id = {$company_id} {$and_purchases}

                UNION ALL

                SELECT
                    {$this->db->dbprefix('payments')}.date,
                    {$this->db->dbprefix('payments')}.reference_no,
                    IF({$this->db->dbprefix('payments')}.amount > 0, {$this->db->dbprefix('payments')}.amount, 0) as incoming,
                    IF({$this->db->dbprefix('payments')}.amount < 0, ({$this->db->dbprefix('payments')}.amount * -1), 0) as outcoming
                FROM {$this->db->dbprefix('payments')}
                    INNER JOIN {$this->db->dbprefix('purchases')} ON {$this->db->dbprefix('purchases')}.id = {$this->db->dbprefix('payments')}.purchase_id
                    INNER JOIN {$this->db->dbprefix('companies')} ON {$this->db->dbprefix('companies')}.id = {$this->db->dbprefix('purchases')}.supplier_id
                WHERE {$this->db->dbprefix('companies')}.id = {$company_id} {$and_payments}

                    ORDER BY date ASC
                ;";

        $q = $this->db->query($sql);
        if (($q->num_rows() > 0)) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_supplier_movements_SI($company_id, $start_date = NULL){
        $and_purchases = '';
        $and_payments = '';

        if ($start_date) {
            $and_purchases .= " AND {$this->db->dbprefix('purchases')}.date < '".$start_date."'";
            $and_payments .= " AND {$this->db->dbprefix('payments')}.date < '".$start_date."'";

            $sql = "SELECT
                        SUM({$this->db->dbprefix('purchases')}.grand_total) as balance
                    FROM {$this->db->dbprefix('purchases')}
                    WHERE {$this->db->dbprefix('purchases')}.supplier_id = {$company_id} {$and_purchases}

                    UNION ALL

                    SELECT
                        SUM({$this->db->dbprefix('payments')}.amount) as balance
                    FROM {$this->db->dbprefix('payments')}
                        INNER JOIN {$this->db->dbprefix('purchases')} ON {$this->db->dbprefix('purchases')}.id = {$this->db->dbprefix('payments')}.purchase_id
                        INNER JOIN {$this->db->dbprefix('companies')} ON {$this->db->dbprefix('companies')}.id = {$this->db->dbprefix('purchases')}.supplier_id
                    WHERE {$this->db->dbprefix('companies')}.id = {$company_id} {$and_payments}

                    ;";

            $q = $this->db->query($sql);
            $balance = 0;
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $balance += $row->balance;
                }
            }
            return $balance;
        } else {
            return 0;
        }
    }

    public function get_total_ico($biller = NULL,$fech_ini = NULL,$fech_fin = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $this->db->select('"Impoconsumo (L&C)" as name');
        $this->db->select(' 0 as unisubtotal ');
        $this->db->select('SUM(item_tax_2) as item_tax');
        $this->db->select('0 as subtotal');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id');
        $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id',$biller);
        }
        if ($module_id) {
            if ($module_id == 1) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 1);
            } else if ($module_id == 2) {
                $this->db->where($this->db->dbprefix('sales').'.pos', 0);
            }
        }
        if ($document_type_id) {
            $this->db->where($this->db->dbprefix('sales').'.document_type_id', $document_type_id);
        }
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    public function production_order_report(){
        $products = [];
        $q = $this->db->query("
                    SELECT
                        por_data.product_id,
                        por_data.product_code,
                        por_data.category_name,
                        por_data.brand_name,
                        por_data.wh_quantity,
                        SUM(por_data.complete_quantity) AS complete_quantity,
                        SUM(por_data.quantity) AS quantity,
                        SUM(por_data.complete_cutting_quantity) AS complete_cutting_quantity,
                        SUM(por_data.complete_cutting_quantity_completed) AS complete_cutting_quantity_completed,
                        SUM(por_data.cutting_quantity) AS cutting_quantity,
                        SUM(por_data.cutting_finished_quantity) AS cutting_finished_quantity,
                        SUM(por_data.complete_assembling_quantity) AS complete_assembling_quantity,
                        SUM(por_data.complete_assembling_quantity_completed) AS complete_assembling_quantity_completed,
                        SUM(por_data.assembling_quantity) AS assembling_quantity,
                        SUM(por_data.assembling_finished_quantity) AS assembling_finished_quantity,
                        SUM(por_data.complete_packing_quantity) AS complete_packing_quantity,
                        SUM(por_data.complete_packing_quantity_completed) AS complete_packing_quantity_completed,
                        SUM(por_data.packing_quantity) AS packing_quantity,
                        SUM(por_data.packing_finished_quantity) AS packing_finished_quantity
                    FROM
                        (SELECT
                            pfinished_product.id AS product_id,
                            pfinished_product.code AS product_code,
                            pfinished_product.quantity AS wh_quantity,
                            category.name AS category_name,
                            brand.name AS brand_name,
                                por_pfinished.quantity AS complete_quantity,
                                SUM({$this->db->dbprefix('production_order_detail')}.quantity) AS quantity,
                                ciq_pfinished_complete_cutting.complete_cutting_quantity,
                                (cutting.cutting_quantity - cutting.finished_quantity - cutting.fault_quantity) AS cutting_quantity,
                                ciq_pfinished_complete_cutting.complete_cutting_quantity_completed,
                                cutting.finished_quantity AS cutting_finished_quantity,
                                ciq_pfinished_complete_assemble.complete_assembling_quantity,
                                (assemble.assembling_quantity - assemble.finished_quantity - assemble.fault_quantity) AS assembling_quantity,
                                ciq_pfinished_complete_assemble.complete_assembling_quantity_completed,
                                assemble.finished_quantity AS assembling_finished_quantity,
                                ciq_pfinished_complete_packing.complete_packing_quantity,
                                (packing.packing_quantity - packing.finished_quantity - packing.fault_quantity) AS packing_quantity,
                                ciq_pfinished_complete_packing.complete_packing_quantity_completed,
                                packing.finished_quantity AS packing_finished_quantity
                        FROM
                            {$this->db->dbprefix('production_order')}
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} AS por_pfinished ON por_pfinished.production_order_id = {$this->db->dbprefix('production_order')}.id
                            AND por_pfinished.production_order_pfinished_item_id IS NULL
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} ON {$this->db->dbprefix('production_order_detail')}.production_order_id = {$this->db->dbprefix('production_order')}.id
                            AND {$this->db->dbprefix('production_order_detail')}.production_order_pfinished_item_id IS NOT NULL
                        INNER JOIN {$this->db->dbprefix('products')} pfinished_product ON pfinished_product.id = por_pfinished.product_id
                        INNER JOIN {$this->db->dbprefix('categories')} category ON category.id = pfinished_product.category_id
                        INNER JOIN {$this->db->dbprefix('brands')} brand ON brand.id = pfinished_product.brand
                        INNER JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id = {$this->db->dbprefix('production_order_detail')}.product_id
                        INNER JOIN {$this->db->dbprefix('companies')} AS employee ON employee.id = {$this->db->dbprefix('production_order')}.employee_id
                        LEFT JOIN (
                            SELECT
                                production_order_pfinished_item_id,
                                IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(cutting_composition_complete_quantity)) AS complete_cutting_quantity,
                                IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(cutting_composition_complete_quantity_completed)) AS complete_cutting_quantity_completed,
                                COUNT(production_order_pfinished_item_id),
                                pfinished_num_composition_items
                        FROM
                            (SELECT
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                POD.product_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.cutting_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_cutting_quantity,
                                (SUM(CD.cutting_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS cutting_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_cutting_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS cutting_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('cutting_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS cutting_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_cutting ON ciq_pfinished_complete_cutting.production_order_pfinished_item_id = por_pfinished.id
                        LEFT JOIN (
                            SELECT
                                production_order_pfinished_item_id,

                                IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity)) AS complete_assembling_quantity,
                                IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity_completed)) AS complete_assembling_quantity_completed,
                                COUNT(production_order_pfinished_item_id),
                                pfinished_num_composition_items
                        FROM
                            (SELECT
                            POD.production_order_id,
                                POD.product_id,
                                POD.production_order_pfinished_item_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_assembling_quantity,
                                (SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS assemble_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_assembling_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS assemble_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('assemble_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS assemble_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_assemble ON ciq_pfinished_complete_assemble.production_order_pfinished_item_id = por_pfinished.id
                        LEFT JOIN (SELECT
                            production_order_pfinished_item_id,
                                MIN(packing_composition_complete_quantity) AS complete_packing_quantity,
                                MIN(packing_composition_complete_quantity_completed) AS complete_packing_quantity_completed,
                                COUNT(production_order_pfinished_item_id),
                                pfinished_num_composition_items
                        FROM
                            (SELECT
                            POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_packing_quantity,
                                (SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS packing_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_packing_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS packing_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('packing_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS packing_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_packing ON ciq_pfinished_complete_packing.production_order_pfinished_item_id = por_pfinished.id
                        LEFT JOIN (SELECT
                            pfinished_POD.id AS pfinished_POD_id,
                                SUM(CD.cutting_quantity) AS cutting_quantity,
                                SUM(CD.finished_quantity) AS finished_quantity,
                                SUM(CD.fault_quantity) AS fault_quantity,
                                COUNT(DISTINCT CD.product_id) AS cutting_comp_qty
                        FROM
                            {$this->db->dbprefix('cutting_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD_id) cutting ON cutting.pfinished_POD_id = por_pfinished.id
                        LEFT JOIN (SELECT
                            pfinished_POD.id AS pfinished_POD_id,
                                SUM(CD.assembling_quantity) AS assembling_quantity,
                                SUM(CD.finished_quantity) AS finished_quantity,
                                SUM(CD.fault_quantity) AS fault_quantity,
                                COUNT(DISTINCT CD.product_id) AS assembling_comp_qty
                        FROM
                            {$this->db->dbprefix('assemble_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD.id) assemble ON assemble.pfinished_POD_id = por_pfinished.id
                        LEFT JOIN (SELECT
                            pfinished_POD.id AS pfinished_POD_id,
                                SUM(CD.packing_quantity) AS packing_quantity,
                                SUM(CD.finished_quantity) AS finished_quantity,
                                SUM(CD.fault_quantity) AS fault_quantity,
                                COUNT(DISTINCT CD.product_id) AS packing_comp_qty
                        FROM
                            {$this->db->dbprefix('packing_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD.id) packing ON packing.pfinished_POD_id = por_pfinished.id
                        GROUP BY {$this->db->dbprefix('production_order')}.id) AS por_data
                    GROUP BY por_data.product_id
                        ");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            // $this->sma->print_arrays($data);
        }
        return $data;
    }

    function reset_portfolio_temp_tables(){
        $q = $this->db->query('show tables like "%portfolio_temp%";');
        $return = false;
        if ($q->num_rows() > 0) {
            $table_name = false;
            $temp_tables = [];
            if ($this->session->userdata('portfolio_temp_end_date')) {
                foreach (($q->result()) as $key => $tblnm) {
                    foreach ($tblnm as $key => $tvl) {
                        $temp_tables[] = $tvl;
                        if (!$table_name) {
                            $q2 = $this->db->get($tvl, 1);
                            if ($q2->num_rows() > 0) {
                                $table_name = $tvl;
                                $q2 = $q2->result();
                                $start_date = new DateTime($q2[0]->registration_date);
                                $since_start = $start_date->diff(new DateTime(date('Y-m-d H:i:s')));
                                if ($since_start->m == 0 && $since_start->d == 0 && $since_start->h == 0 && $since_start->i <= 60) {
                                    $return = true;
                                }
                            }
                        }
                    }
                }
            }
            if ($return == false) {
                foreach ($temp_tables as $key => $value) {
                    $this->db->query("DROP TABLE ".$value);
                }
            }
        }
        return $return;
    }

    function create_portfolio_temp_tables($end_date, $year_to_execute, $start_month, $end_month){
        $end_date_year = date('Y', strtotime($end_date));
        $system_start_date_year = date('Y', strtotime($this->Settings->system_start_date));
        $system_end_date_year = date('Y');
        if ($year_to_execute > $end_date_year) {
            return false;
        }
        $q = $this->db->query("SHOW TABLES LIKE '%sma_portfolio_temp_{$year_to_execute}%';");
        $end_date_to_execute = $year_to_execute.$end_month;
        if ($end_date < $end_date_to_execute) {
            $end_month = substr($end_date, 4);
            // exit('End month >>> '.$end_month);
        }
        if ($q->num_rows() > 0) {
        } else {
            $creating = $this->db->query("
              CREATE TABLE `sma_portfolio_temp_{$year_to_execute}` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `date` datetime DEFAULT NULL,
              `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
              `customer_id` int(11) NOT NULL,
              `customer` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
              `biller_id` int(11) NOT NULL,
              `biller` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
              `warehouse_id` int(11) DEFAULT NULL,
              `total` decimal(25,4) NOT NULL,
              `product_discount` decimal(25,4) DEFAULT 0.0000,
              `order_discount_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `total_discount` decimal(25,4) DEFAULT 0.0000,
              `order_discount` decimal(25,4) DEFAULT 0.0000,
              `product_tax` decimal(25,4) DEFAULT 0.0000,
              `order_tax_id` int(11) DEFAULT NULL,
              `order_tax` decimal(25,4) DEFAULT 0.0000,
              `total_tax` decimal(25,4) DEFAULT 0.0000,
              `shipping` decimal(25,4) DEFAULT 0.0000,
              `grand_total` decimal(25,4) NOT NULL,
              `sale_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Completado pendiente',
              `payment_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Pendiente Debido Parcial Pagado',
              `payment_term` tinyint(4) DEFAULT NULL,
              `due_date` date DEFAULT NULL,
              `created_by` int(11) DEFAULT NULL,
              `total_items` smallint(6) DEFAULT NULL,
              `pos` tinyint(1) NOT NULL DEFAULT 0,
              `paid` decimal(25,4) DEFAULT 0.0000,
              `return_id` int(11) DEFAULT NULL,
              `surcharge` decimal(25,4) NOT NULL DEFAULT 0.0000,
              `return_sale_ref` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `sale_id` int(11) DEFAULT NULL,
              `return_sale_total` decimal(25,4) NOT NULL DEFAULT 0.0000,
              `rounding` decimal(10,4) DEFAULT NULL,
              `address_id` int(11) DEFAULT NULL,
              `seller_id` int(11) DEFAULT NULL,
              `rete_fuente_percentage` decimal(6,3) DEFAULT 0.000,
              `rete_fuente_total` decimal(25,4) DEFAULT 0.0000,
              `rete_fuente_account` int(11) DEFAULT 0,
              `rete_fuente_base` decimal(25,4) DEFAULT NULL,
              `rete_iva_percentage` decimal(6,3) DEFAULT 0.000,
              `rete_iva_total` decimal(25,4) DEFAULT 0.0000,
              `rete_iva_account` int(11) DEFAULT 0,
              `rete_iva_base` decimal(25,4) DEFAULT NULL,
              `rete_ica_percentage` decimal(6,3) DEFAULT 0.000,
              `rete_ica_total` decimal(25,4) DEFAULT 0.0000,
              `rete_ica_account` int(11) DEFAULT 0,
              `rete_ica_base` decimal(25,4) DEFAULT NULL,
              `rete_other_percentage` decimal(6,3) DEFAULT 0.000,
              `rete_other_total` decimal(25,4) DEFAULT 0.0000,
              `rete_other_account` int(11) DEFAULT 0,
              `rete_other_base` decimal(25,4) DEFAULT NULL,
              `resolucion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `sale_currency` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `sale_currency_trm` decimal(25,4) DEFAULT NULL,
              `cost_center_id` int(11) DEFAULT NULL,
              `document_type_id` int(11) DEFAULT NULL,
              `tip_amount` decimal(25,4) DEFAULT 0.0000,
              `shipping_in_grand_total` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
              `sale_origin` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `sale_origin_reference_no` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
              `rete_fuente_id` int(11) DEFAULT NULL,
              `rete_iva_id` int(11) DEFAULT NULL,
              `rete_ica_id` int(11) DEFAULT NULL,
              `rete_other_id` int(11) DEFAULT NULL,
              `sale_comm_perc` decimal(5,2) DEFAULT 0.00,
              `collection_comm_perc` decimal(5,2) DEFAULT 0.00,
              `sale_comm_amount` decimal(25,4) DEFAULT 0.0000,
              `sale_comm_payment_status` int(1) DEFAULT 0,
              `debit_note_id` int(11) DEFAULT NULL COMMENT 'Identificador de la nota débito de referencia.',
              `reference_debit_note` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'referencia de la nota débito',
              `reference_invoice_id` int(11) DEFAULT NULL COMMENT 'Identificador de la factura de referencia.',
              `consumption_sales` decimal(25,4) DEFAULT NULL,
              `self_withholding_amount` decimal(25,4) DEFAULT 0.0000,
              `self_withholding_percentage` decimal(25,4) DEFAULT 0.0000,
              `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
              `return_other_concepts` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No; 1: Si;',
              `rete_bomberil_percentage` decimal(6,3) DEFAULT 0.000,
              `rete_bomberil_total` decimal(25,4) DEFAULT 0.0000,
              `rete_bomberil_account` int(11) DEFAULT 0,
              `rete_bomberil_base` decimal(25,4) DEFAULT NULL,
              `rete_autoaviso_percentage` decimal(6,3) DEFAULT 0.000,
              `rete_autoaviso_total` decimal(25,4) DEFAULT 0.0000,
              `rete_autoaviso_account` int(11) DEFAULT 0,
              `rete_autoaviso_base` decimal(25,4) DEFAULT NULL,
              `rete_autoica_percentage` decimal(6,3) DEFAULT 0.000,
              `rete_autoica_total` decimal(25,4) DEFAULT 0.0000,
              `rete_autoica_account` int(11) DEFAULT 0,
              `rete_autoica_base` decimal(25,4) DEFAULT NULL,
              `rete_autoica_account_counterpart` int(11) DEFAULT 0,
              `rete_autoaviso_account_counterpart` int(11) DEFAULT 0,
              `rete_bomberil_account_counterpart` int(11) DEFAULT 0,
              `rete_bomberil_id` int(11) DEFAULT NULL,
              `rete_autoaviso_id` int(11) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `id` (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            ");
            if (!$creating) {
                return false;
            }
        }
        // $inserting = $this->db->query("INSERT INTO sma_portfolio_temp_{$year_to_execute} (date, reference_no, customer_id, customer, biller_id, biller, warehouse_id, total, product_discount, order_discount_id, total_discount, order_discount, product_tax, order_tax_id, order_tax, total_tax, shipping, grand_total, sale_status, payment_status, payment_term, due_date, created_by, total_items, pos, paid, return_id, surcharge, return_sale_ref, sale_id, return_sale_total, rounding, address_id, seller_id, rete_fuente_percentage, rete_fuente_total, rete_fuente_account, rete_fuente_base, rete_iva_percentage, rete_iva_total, rete_iva_account, rete_iva_base, rete_ica_percentage, rete_ica_total, rete_ica_account, rete_ica_base, rete_other_percentage, rete_other_total, rete_other_account, rete_other_base, resolucion, sale_currency, sale_currency_trm, cost_center_id, document_type_id, tip_amount, shipping_in_grand_total, sale_origin, sale_origin_reference_no, rete_fuente_id, rete_iva_id, rete_ica_id, rete_other_id, sale_comm_perc, collection_comm_perc, sale_comm_amount, sale_comm_payment_status, debit_note_id, reference_debit_note, reference_invoice_id, consumption_sales, self_withholding_amount, self_withholding_percentage, return_other_concepts, rete_bomberil_percentage, rete_bomberil_total, rete_bomberil_account, rete_bomberil_base, rete_autoaviso_percentage, rete_autoaviso_total, rete_autoaviso_account, rete_autoaviso_base, rete_autoica_percentage, rete_autoica_total, rete_autoica_account, rete_autoica_base, rete_autoica_account_counterpart, rete_autoaviso_account_counterpart, rete_bomberil_account_counterpart, rete_bomberil_id, rete_autoaviso_id)
        //     SELECT
        //         S.date, S.reference_no, S.customer_id, S.customer, S.biller_id, S.biller, S.warehouse_id, S.total, S.product_discount, S.order_discount_id, S.total_discount, S.order_discount, S.product_tax, S.order_tax_id, S.order_tax, S.total_tax, S.shipping, S.grand_total, S.sale_status, IF(SUM(COALESCE(P.amount, 0)) > 0, IF(SUM(COALESCE(P.amount, 0)) < S.grand_total, 'partial', 'paid'), 'pending') AS payment_status, S.payment_term, S.due_date, S.created_by, S.total_items, S.pos, SUM(COALESCE(P.amount, 0)) AS paid, S.return_id, S.surcharge, S.return_sale_ref, S.sale_id, S.return_sale_total, S.rounding, S.address_id, S.seller_id, S.rete_fuente_percentage, S.rete_fuente_total, S.rete_fuente_account, S.rete_fuente_base, S.rete_iva_percentage, S.rete_iva_total, S.rete_iva_account, S.rete_iva_base, S.rete_ica_percentage, S.rete_ica_total, S.rete_ica_account, S.rete_ica_base, S.rete_other_percentage, S.rete_other_total, S.rete_other_account, S.rete_other_base, S.resolucion, S.sale_currency, S.sale_currency_trm, S.cost_center_id, S.document_type_id, S.tip_amount, S.shipping_in_grand_total, S.sale_origin, S.sale_origin_reference_no, S.rete_fuente_id, S.rete_iva_id, S.rete_ica_id, S.rete_other_id, S.sale_comm_perc, S.collection_comm_perc, S.sale_comm_amount, S.sale_comm_payment_status, S.debit_note_id, S.reference_debit_note, S.reference_invoice_id, S.consumption_sales, S.self_withholding_amount, S.self_withholding_percentage, S.return_other_concepts, S.rete_bomberil_percentage, S.rete_bomberil_total, S.rete_bomberil_account, S.rete_bomberil_base, S.rete_autoaviso_percentage, S.rete_autoaviso_total, S.rete_autoaviso_account, S.rete_autoaviso_base, S.rete_autoica_percentage, S.rete_autoica_total, S.rete_autoica_account, S.rete_autoica_base, S.rete_autoica_account_counterpart, S.rete_autoaviso_account_counterpart, S.rete_bomberil_account_counterpart, S.rete_bomberil_id, S.rete_autoaviso_id
        //     FROM sma_sales S
        //         LEFT JOIN sma_payments P ON P.sale_id = S.id AND P.sale_id IS NOT NULL AND P.date >= '{$year_to_execute}{$start_month} 00:00:00'
        //     WHERE S.date >= '{$year_to_execute}{$start_month} 00:00:00' AND S.date <= '{$year_to_execute}{$end_month} 23:59:59'
        //     GROUP BY S.id
        //     HAVING (S.grand_total - SUM(COALESCE(P.amount, 0))) > 0
        //     ");
        // if (!$inserting) {
        //     return false;
        // }
        $this->db->select("
            {$this->db->dbprefix('sales')}.date,
            {$this->db->dbprefix('sales')}.reference_no,
            {$this->db->dbprefix('sales')}.customer_id,
            {$this->db->dbprefix('sales')}.customer,
            {$this->db->dbprefix('sales')}.biller_id,
            {$this->db->dbprefix('sales')}.biller,
            {$this->db->dbprefix('sales')}.warehouse_id,
            {$this->db->dbprefix('sales')}.total,
            {$this->db->dbprefix('sales')}.product_discount,
            {$this->db->dbprefix('sales')}.order_discount_id,
            {$this->db->dbprefix('sales')}.total_discount,
            {$this->db->dbprefix('sales')}.order_discount,
            {$this->db->dbprefix('sales')}.product_tax,
            {$this->db->dbprefix('sales')}.order_tax_id,
            {$this->db->dbprefix('sales')}.order_tax,
            {$this->db->dbprefix('sales')}.total_tax,
            {$this->db->dbprefix('sales')}.shipping,
            {$this->db->dbprefix('sales')}.grand_total,
            {$this->db->dbprefix('sales')}.sale_status, IF(SUM(COALESCE({$this->db->dbprefix('payments')}.amount, 0)) > 0, IF(SUM(COALESCE({$this->db->dbprefix('payments')}.amount, 0)) < {$this->db->dbprefix('sales')}.grand_total, 'partial', 'paid'), 'pending') AS payment_status,
            {$this->db->dbprefix('sales')}.payment_term,
            {$this->db->dbprefix('sales')}.due_date,
            {$this->db->dbprefix('sales')}.created_by,
            {$this->db->dbprefix('sales')}.total_items,
            {$this->db->dbprefix('sales')}.pos, SUM(COALESCE({$this->db->dbprefix('payments')}.amount, 0)) AS paid,
            {$this->db->dbprefix('sales')}.return_id,
            {$this->db->dbprefix('sales')}.surcharge,
            {$this->db->dbprefix('sales')}.return_sale_ref,
            {$this->db->dbprefix('sales')}.sale_id,
            {$this->db->dbprefix('sales')}.return_sale_total,
            {$this->db->dbprefix('sales')}.rounding,
            {$this->db->dbprefix('sales')}.address_id,
            {$this->db->dbprefix('sales')}.seller_id,
            {$this->db->dbprefix('sales')}.rete_fuente_percentage,
            {$this->db->dbprefix('sales')}.rete_fuente_total,
            {$this->db->dbprefix('sales')}.rete_fuente_account,
            {$this->db->dbprefix('sales')}.rete_fuente_base,
            {$this->db->dbprefix('sales')}.rete_iva_percentage,
            {$this->db->dbprefix('sales')}.rete_iva_total,
            {$this->db->dbprefix('sales')}.rete_iva_account,
            {$this->db->dbprefix('sales')}.rete_iva_base,
            {$this->db->dbprefix('sales')}.rete_ica_percentage,
            {$this->db->dbprefix('sales')}.rete_ica_total,
            {$this->db->dbprefix('sales')}.rete_ica_account,
            {$this->db->dbprefix('sales')}.rete_ica_base,
            {$this->db->dbprefix('sales')}.rete_other_percentage,
            {$this->db->dbprefix('sales')}.rete_other_total,
            {$this->db->dbprefix('sales')}.rete_other_account,
            {$this->db->dbprefix('sales')}.rete_other_base,
            {$this->db->dbprefix('sales')}.resolucion,
            {$this->db->dbprefix('sales')}.sale_currency,
            {$this->db->dbprefix('sales')}.sale_currency_trm,
            {$this->db->dbprefix('sales')}.cost_center_id,
            {$this->db->dbprefix('sales')}.document_type_id,
            {$this->db->dbprefix('sales')}.tip_amount,
            {$this->db->dbprefix('sales')}.shipping_in_grand_total,
            {$this->db->dbprefix('sales')}.sale_origin,
            {$this->db->dbprefix('sales')}.sale_origin_reference_no,
            {$this->db->dbprefix('sales')}.rete_fuente_id,
            {$this->db->dbprefix('sales')}.rete_iva_id,
            {$this->db->dbprefix('sales')}.rete_ica_id,
            {$this->db->dbprefix('sales')}.rete_other_id,
            {$this->db->dbprefix('sales')}.sale_comm_perc,
            {$this->db->dbprefix('sales')}.collection_comm_perc,
            {$this->db->dbprefix('sales')}.sale_comm_amount,
            {$this->db->dbprefix('sales')}.sale_comm_payment_status,
            {$this->db->dbprefix('sales')}.debit_note_id,
            {$this->db->dbprefix('sales')}.reference_debit_note,
            {$this->db->dbprefix('sales')}.reference_invoice_id,
            {$this->db->dbprefix('sales')}.consumption_sales,
            {$this->db->dbprefix('sales')}.self_withholding_amount,
            {$this->db->dbprefix('sales')}.self_withholding_percentage,
            {$this->db->dbprefix('sales')}.return_other_concepts,
            {$this->db->dbprefix('sales')}.rete_bomberil_percentage,
            {$this->db->dbprefix('sales')}.rete_bomberil_total,
            {$this->db->dbprefix('sales')}.rete_bomberil_account,
            {$this->db->dbprefix('sales')}.rete_bomberil_base,
            {$this->db->dbprefix('sales')}.rete_autoaviso_percentage,
            {$this->db->dbprefix('sales')}.rete_autoaviso_total,
            {$this->db->dbprefix('sales')}.rete_autoaviso_account,
            {$this->db->dbprefix('sales')}.rete_autoaviso_base,
            {$this->db->dbprefix('sales')}.rete_autoica_percentage,
            {$this->db->dbprefix('sales')}.rete_autoica_total,
            {$this->db->dbprefix('sales')}.rete_autoica_account,
            {$this->db->dbprefix('sales')}.rete_autoica_base,
            {$this->db->dbprefix('sales')}.rete_autoica_account_counterpart,
            {$this->db->dbprefix('sales')}.rete_autoaviso_account_counterpart,
            {$this->db->dbprefix('sales')}.rete_bomberil_account_counterpart,
            {$this->db->dbprefix('sales')}.rete_bomberil_id,
            {$this->db->dbprefix('sales')}.rete_autoaviso_id");
        $this->db->join('payments', "{$this->db->dbprefix('payments')}.sale_id = {$this->db->dbprefix('sales')}.id AND {$this->db->dbprefix('payments')}.sale_id IS NOT NULL AND {$this->db->dbprefix('payments')}.date >= '{$year_to_execute}{$start_month} 00:00:00' AND {$this->db->dbprefix('payments')}.date <= '{$end_date} 23:59:59'", 'INNER');
        $this->db->where("{$this->db->dbprefix('sales')}.date >= '{$year_to_execute}{$start_month} 00:00:00' AND {$this->db->dbprefix('sales')}.date <= '{$year_to_execute}{$end_month} 23:59:59'");
        $this->db->where("{$this->db->dbprefix('sales')}.grand_total > 0");
        $this->db->where("{$this->db->dbprefix('sales')}.sale_status", 'completed');
        $this->db->group_by("{$this->db->dbprefix('sales')}.id");
        $this->db->having("({$this->db->dbprefix('sales')}.grand_total - SUM(COALESCE({$this->db->dbprefix('payments')}.amount, 0))) > 0");
        $q = $this->db->get("sales");
        foreach (($q->result()) as $row) {
            $this->db->insert("sma_portfolio_temp_{$year_to_execute}", $row);
        }

        $this->db->select("
            {$this->db->dbprefix('sales')}.date,
            {$this->db->dbprefix('sales')}.reference_no,
            {$this->db->dbprefix('sales')}.customer_id,
            {$this->db->dbprefix('sales')}.customer,
            {$this->db->dbprefix('sales')}.biller_id,
            {$this->db->dbprefix('sales')}.biller,
            {$this->db->dbprefix('sales')}.warehouse_id,
            {$this->db->dbprefix('sales')}.total,
            {$this->db->dbprefix('sales')}.product_discount,
            {$this->db->dbprefix('sales')}.order_discount_id,
            {$this->db->dbprefix('sales')}.total_discount,
            {$this->db->dbprefix('sales')}.order_discount,
            {$this->db->dbprefix('sales')}.product_tax,
            {$this->db->dbprefix('sales')}.order_tax_id,
            {$this->db->dbprefix('sales')}.order_tax,
            {$this->db->dbprefix('sales')}.total_tax,
            {$this->db->dbprefix('sales')}.shipping,
            {$this->db->dbprefix('sales')}.grand_total,
            'pending' AS payment_status,
            {$this->db->dbprefix('sales')}.payment_term,
            {$this->db->dbprefix('sales')}.due_date,
            {$this->db->dbprefix('sales')}.created_by,
            {$this->db->dbprefix('sales')}.total_items,
            {$this->db->dbprefix('sales')}.pos,
            0 AS paid,
            {$this->db->dbprefix('sales')}.return_id,
            {$this->db->dbprefix('sales')}.surcharge,
            {$this->db->dbprefix('sales')}.return_sale_ref,
            {$this->db->dbprefix('sales')}.sale_id,
            {$this->db->dbprefix('sales')}.return_sale_total,
            {$this->db->dbprefix('sales')}.rounding,
            {$this->db->dbprefix('sales')}.address_id,
            {$this->db->dbprefix('sales')}.seller_id,
            {$this->db->dbprefix('sales')}.rete_fuente_percentage,
            {$this->db->dbprefix('sales')}.rete_fuente_total,
            {$this->db->dbprefix('sales')}.rete_fuente_account,
            {$this->db->dbprefix('sales')}.rete_fuente_base,
            {$this->db->dbprefix('sales')}.rete_iva_percentage,
            {$this->db->dbprefix('sales')}.rete_iva_total,
            {$this->db->dbprefix('sales')}.rete_iva_account,
            {$this->db->dbprefix('sales')}.rete_iva_base,
            {$this->db->dbprefix('sales')}.rete_ica_percentage,
            {$this->db->dbprefix('sales')}.rete_ica_total,
            {$this->db->dbprefix('sales')}.rete_ica_account,
            {$this->db->dbprefix('sales')}.rete_ica_base,
            {$this->db->dbprefix('sales')}.rete_other_percentage,
            {$this->db->dbprefix('sales')}.rete_other_total,
            {$this->db->dbprefix('sales')}.rete_other_account,
            {$this->db->dbprefix('sales')}.rete_other_base,
            {$this->db->dbprefix('sales')}.resolucion,
            {$this->db->dbprefix('sales')}.sale_currency,
            {$this->db->dbprefix('sales')}.sale_currency_trm,
            {$this->db->dbprefix('sales')}.cost_center_id,
            {$this->db->dbprefix('sales')}.document_type_id,
            {$this->db->dbprefix('sales')}.tip_amount,
            {$this->db->dbprefix('sales')}.shipping_in_grand_total,
            {$this->db->dbprefix('sales')}.sale_origin,
            {$this->db->dbprefix('sales')}.sale_origin_reference_no,
            {$this->db->dbprefix('sales')}.rete_fuente_id,
            {$this->db->dbprefix('sales')}.rete_iva_id,
            {$this->db->dbprefix('sales')}.rete_ica_id,
            {$this->db->dbprefix('sales')}.rete_other_id,
            {$this->db->dbprefix('sales')}.sale_comm_perc,
            {$this->db->dbprefix('sales')}.collection_comm_perc,
            {$this->db->dbprefix('sales')}.sale_comm_amount,
            {$this->db->dbprefix('sales')}.sale_comm_payment_status,
            {$this->db->dbprefix('sales')}.debit_note_id,
            {$this->db->dbprefix('sales')}.reference_debit_note,
            {$this->db->dbprefix('sales')}.reference_invoice_id,
            {$this->db->dbprefix('sales')}.consumption_sales,
            {$this->db->dbprefix('sales')}.self_withholding_amount,
            {$this->db->dbprefix('sales')}.self_withholding_percentage,
            {$this->db->dbprefix('sales')}.return_other_concepts,
            {$this->db->dbprefix('sales')}.rete_bomberil_percentage,
            {$this->db->dbprefix('sales')}.rete_bomberil_total,
            {$this->db->dbprefix('sales')}.rete_bomberil_account,
            {$this->db->dbprefix('sales')}.rete_bomberil_base,
            {$this->db->dbprefix('sales')}.rete_autoaviso_percentage,
            {$this->db->dbprefix('sales')}.rete_autoaviso_total,
            {$this->db->dbprefix('sales')}.rete_autoaviso_account,
            {$this->db->dbprefix('sales')}.rete_autoaviso_base,
            {$this->db->dbprefix('sales')}.rete_autoica_percentage,
            {$this->db->dbprefix('sales')}.rete_autoica_total,
            {$this->db->dbprefix('sales')}.rete_autoica_account,
            {$this->db->dbprefix('sales')}.rete_autoica_base,
            {$this->db->dbprefix('sales')}.rete_autoica_account_counterpart,
            {$this->db->dbprefix('sales')}.rete_autoaviso_account_counterpart,
            {$this->db->dbprefix('sales')}.rete_bomberil_account_counterpart,
            {$this->db->dbprefix('sales')}.rete_bomberil_id,
            {$this->db->dbprefix('sales')}.rete_autoaviso_id");
        $this->db->where("sma_sales.id NOT IN (SELECT sma_sales.id FROM sma_sales INNER JOIN sma_payments ON sma_payments.sale_id = sma_sales.id WHERE {$this->db->dbprefix('payments')}.date >= '{$year_to_execute}{$start_month} 00:00:00' AND {$this->db->dbprefix('payments')}.date <= '{$end_date} 23:59:59')");

        $this->db->where("{$this->db->dbprefix('sales')}.date >= '{$year_to_execute}{$start_month} 00:00:00' AND {$this->db->dbprefix('sales')}.date <= '{$year_to_execute}{$end_month} 23:59:59'");
        $this->db->where("{$this->db->dbprefix('sales')}.grand_total > 0");
        $this->db->where("{$this->db->dbprefix('sales')}.sale_status", 'completed');
        $this->db->group_by("{$this->db->dbprefix('sales')}.id");
        $q = $this->db->get("sales");
        foreach (($q->result()) as $row) {
            $this->db->insert("sma_portfolio_temp_{$year_to_execute}", $row);
        }
        return true;
    }

    //  INICIO INFORME CANTIDADES BODEGA POR VARIANTES
    public function getAllWarehouses(){
        $this->db->select($this->db->dbprefix('warehouses').'.id AS ID');
        $this->db->select($this->db->dbprefix('warehouses').'.name AS nombre');
        $this->db->from('warehouses');
        $this->db->where($this->db->dbprefix('warehouses').'.status = 1');
        $this->db->order_by($this->db->dbprefix('warehouses').'.id ASC');
        $consulta = $this->db->get();
        $respuesta = $consulta->result_array();
        return $respuesta;
    }

    public function getWarehouseProducts($bodega){
        $this->db->select(" {$this->db->dbprefix('products')}.id AS id");
        $this->db->select(" {$this->db->dbprefix('products')}.name AS text ");
        $this->db->from("   {$this->db->dbprefix('products')} ");
        if ($bodega) {
            $condition = "  {$this->db->dbprefix('products')}.id IN (SELECT {$this->db->dbprefix('warehouses_products')}.product_id
                            FROM {$this->db->dbprefix('warehouses_products')}
                            WHERE {$this->db->dbprefix('warehouses_products')}.`warehouse_id` = $bodega)" ;
            $this->db->where($condition);
        }
        $this->db->order_by("{$this->db->dbprefix('products')}.name ASC");
        $consulta = $this->db->get();
        if ($consulta->num_rows() > 0) {
            foreach (($consulta->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariants($producto){
        $this->db->select($this->db->dbprefix('product_variants').'.id AS id');
        $this->db->select($this->db->dbprefix('product_variants').'.name AS text');
        $this->db->from('product_variants');
        $this->db->where($this->db->dbprefix('product_variants').'.product_id' , $producto);
        $this->db->order_by($this->db->dbprefix('product_variants').'.name ASC');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function get_purchases_request_report($biller_id = NULL, $group_by_preferences = false){
        $this->db->select('
                                categories.id as category_id,
                                categories.name as category_name,
                                subcategory.id as subcategory_id,
                                subcategory.name as subcategory_name,
                                units.code as main_unit_code,
                                COALESCE('.$this->db->dbprefix('products').'.quantity'.', 0) as product_quantity,
                                SUM(COALESCE('.$this->db->dbprefix('order_sale_items').'.quantity, 0) - COALESCE('.$this->db->dbprefix('order_sale_items').'.quantity_delivered, 0)) as quantity_request,
                                order_sale_items.*
                             ')
                 ->join('order_sale_items', 'order_sale_items.sale_id = '.$this->db->dbprefix('order_sales').'.id', 'inner')
                 ->join('products', 'products.id = order_sale_items.product_id', 'inner')
                 ->join('units', 'units.id = products.unit', 'inner')
                 ->join('categories', 'categories.id = '.$this->db->dbprefix('products').'.category_id', 'inner')
                 ->join('categories as subcategory', 'subcategory.id = '.$this->db->dbprefix('products').'.subcategory_id', 'left')
                 ->where('('.$this->db->dbprefix('order_sales').'.sale_status = "pending" OR '.$this->db->dbprefix('order_sales').'.sale_status = "enlistment" OR '.$this->db->dbprefix('order_sales').'.sale_status = "partial")');

        if ($biller_id) {
            $this->db->where('order_sales.biller_id', $biller_id);
        }
        if ($group_by_preferences) {
            $this->db->order_by('categories.name ASC, subcategory.name ASC, products.name ASC, order_sale_items.preferences ASC');
            $this->db->group_by('order_sale_items.product_id, order_sale_items.preferences');
        } else {
            $this->db->order_by('categories.name ASC, subcategory.name ASC, products.name ASC');
            $this->db->group_by('order_sale_items.product_id');
        }
        $q = $this->db->having('quantity_request > 0')->get('order_sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $row->all_units = $this->get_product_all_units($row->product_id);
                $data[] = $row;

            }
            return $data;
        }
        return false;
    }

    public function get_product_all_units($product_id){
        $q = $this->db->query("
                SELECT 1 as num, U2.name, P.price as valor_unitario, 1 as cantidad, 0 as id, U2.id as product_unit_id, U2.code as unit_code, U2.operation_value as operation_value, U2.operator as operator FROM {$this->db->dbprefix('products')} AS P
                    INNER JOIN {$this->db->dbprefix('units')} AS U2 ON U2.id = P.sale_unit
                WHERE P.id = {$product_id}
                    UNION
                SELECT 2 as num, U.name, UP.valor_unitario, UP.cantidad, UP.id, U.id as product_unit_id, U.code as unit_code, U.operation_value as operation_value, U.operator as operator FROM {$this->db->dbprefix('unit_prices')} AS UP
                    INNER JOIN {$this->db->dbprefix('units')} AS U ON U.id = UP.unit_id
                WHERE UP.id_product = {$product_id}
                ORDER BY num DESC, cantidad DESC
                ");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_all_products($discontinued = false, $only_with_stock = false, $group = false, $only_view_products_featured = false){
        $this->db->select('categories.name as category_name, subcategories.name as subcategory_name, units.name as unit_name, products.*')
                ->join('categories', 'categories.id = products.category_id', 'inner')
                ->join('units', 'units.id = products.unit', 'inner')
                ->join('categories as subcategories', 'subcategories.id = products.subcategory_id', 'left');
        if (!$discontinued) {
            $this->db->where('products.discontinued', '0');
        }
        if ($only_view_products_featured) {
            $this->db->where('products.featured', '1');
        }
        if ($only_with_stock) {
            $this->db->where('products.quantity >', '0');
        }
        if ($group) {
            $this->db->order_by('categories.name asc, subcategories.name asc, products.name asc');
        } else {
            $this->db->order_by('products.name asc');
        }
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_order_sales_report($filter_data){

        $group_by_order = $filter_data['group_by_order'];
        $group_by_zone  = $filter_data['group_by_zone'];
        $reference_no   = $filter_data['reference_no'];
        $filter_date    = $filter_data['filter_date'];
        $start_date     = $filter_data['start_date'];
        $warehouse      = $filter_data['warehouse'];
        $customer       = $filter_data['customer'];
        $end_date       = $filter_data['end_date'];
        $country        = $filter_data['country'];
        $subzone        = $filter_data['subzone'];
        $biller         = $filter_data['biller'];
        $seller         = $filter_data['seller'];
        $status         = $filter_data['status'];
        $state          = $filter_data['state'];
        $city           = $filter_data['city'];
        $zone           = $filter_data['zone'];

        
        $this->db->select('
            order_sale_items.*,
            categories.id as category_id,
            order_sales.reference_no as reference_no,
            categories.name as category_name,
            subcategory.id as subcategory_id,
            subcategory.name as subcategory_name,
            units.code as main_unit_code,
            COALESCE('.($warehouse ? $this->db->dbprefix('warehouses_products').'.quantity' : $this->db->dbprefix('products').'.quantity').', 0) as product_quantity,
            SUM(COALESCE('.$this->db->dbprefix('order_sale_items').'.quantity, 0)) as quantity_request,
            SUM(COALESCE('.$this->db->dbprefix('order_sale_items').'.quantity_delivered, 0)) as quantity_delivered,
            companies.name AS companyName,
            companies.company AS company,
            order_sales.date AS date,
            addresses.direccion,
            IF('. $this->db->dbprefix("zones") .'.zone_name = \'\' or '. $this->db->dbprefix("zones") .'.zone_name IS NULL, \'Sin Zona\','. $this->db->dbprefix("zones") .'.zone_name) AS zone,
            IF('. $this->db->dbprefix("addresses") .'.subzone = \'\' or '. $this->db->dbprefix("addresses") .'.subzone IS NULL, \'Sin Subzona\','. $this->db->dbprefix("addresses") .'.subzone) AS subzone,
        ');
        $this->db->from('order_sales');
        $this->db->join('order_sale_items', 'order_sale_items.sale_id = '.$this->db->dbprefix('order_sales').'.id', 'inner');
        $this->db->join('products', 'products.id = order_sale_items.product_id', 'inner');
        $this->db->join('units', 'units.id = products.unit', 'inner');
        $this->db->join('categories', 'categories.id = '.$this->db->dbprefix('products').'.category_id', 'inner');
        $this->db->join('categories as subcategory', 'subcategory.id = '.$this->db->dbprefix('products').'.subcategory_id', 'left');
        $this->db->join('addresses', 'addresses.id = order_sales.address_id', 'inner');
        $this->db->join('companies', 'companies.id = order_sales.customer_id', 'inner');
        $this->db->join('zones', 'zones.id = addresses.location', 'left');

        $this->db->where('('.$this->db->dbprefix('order_sales').'.sale_status = "pending" OR '.$this->db->dbprefix('order_sales').'.sale_status = "enlistment" OR '.$this->db->dbprefix('order_sales').'.sale_status = "partial")');


        if ($biller) {
            $this->db->where('order_sales.biller_id', $biller);
        }
        if ($warehouse) {
            $this->db->join('warehouses_products', 'warehouses_products.product_id = products.id AND warehouses_products.warehouse_id = '.$warehouse, 'left');
            $this->db->where('order_sales.warehouse_id', $warehouse);
        }
        if ($customer) {
            $this->db->where('order_sales.customer_id', $customer);
        }
        if ($seller) {
            $this->db->where('order_sales.seller_id', $seller);
        }
        if ($status) {
            $this->db->where('order_sales.status', $status);
        }
        if ($country) {
            $this->db->where('addresses.country', $country);
        }
        if ($state) {
            $this->db->where('addresses.state', $state);
        }
        if ($city) {
            $this->db->where('addresses.city', $city);
        }
        if ($zone) {
            $this->db->where('addresses.location', $zone);
        }
        if ($subzone) {
            $this->db->where('addresses.subzone', $subzone);
        }
        $date_to_filter = 'date';
        if ($filter_date) {
            if ($filter_date == 2) {
                $date_to_filter = 'delivery_day';
            }
        }
        if ($start_date) {
            $this->db->where('order_sales.'.$date_to_filter.' >=', $start_date);
        }
        if ($end_date) {
            $this->db->where('order_sales.'.$date_to_filter.' <=', $end_date);
        }
        if ($group_by_order) {
            $this->db->group_by('order_sale_items.sale_id, order_sale_items.product_id');
            $this->db->order_by('order_sale_items.sale_id ASC, categories.name ASC, products.name ASC');
        } else if ($group_by_zone) {
            $this->db->group_by('zones.id, order_sale_items.sale_id, order_sale_items.product_id');
            $this->db->order_by('zones.id ASC, order_sale_items.sale_id ASC, categories.name ASC, products.name ASC');
        } else {
            $this->db->group_by('order_sale_items.product_id');
            $this->db->order_by('categories.name ASC, subcategory.name ASC, products.name ASC');
        }
        if ($reference_no) {
            $this->db->where('order_sales.reference_no', $reference_no);
        }
        // $this->sma->print_arrays($this->db->get_compiled_select());
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $pr_units = $this->get_product_all_units($row->product_id);
                if ($warehouse) {
                    $other_wh_qty = $this->get_other_wh_quantity($row->product_id, $warehouse);
                    $row->ow_quantity = $other_wh_qty;
                }
                $arr['min'] = 0;
                $arr['max'] = 0;
                $min_unit = [];
                $max_unit = [];
                if ($pr_units) {
                    foreach ($pr_units as $pr_units_row) {
                        if ($arr['min'] == 0 || $pr_units_row->operation_value < $arr['min']) {
                            $min_unit[0] = $pr_units_row;
                            $arr['min'] = $pr_units_row->operation_value;
                        }
                        if ($arr['max'] == 0 || $pr_units_row->operation_value > $arr['max']) {
                            $max_unit[0] = $pr_units_row;
                            $arr['max'] = $pr_units_row->operation_value;
                        }
                    }
                    $row->all_units[] = $max_unit[0];
                    $row->all_units[] = $min_unit[0];
                }

                $data[] = $row;

                // $row->all_units = $this->get_product_all_units($row->product_id);
                // $data[] = $row;
            }
            return $data;
        }
        return false;
    }


    public function get_other_wh_quantity($pr_id, $wh_id){
        $q = $this->db->select('SUM(COALESCE(quantity, 0)) AS quantity')
                ->where('warehouse_id !=', $wh_id)
                ->where('product_id', $pr_id)
                ->group_by('product_id')
                ->get('warehouses_products');
        if ($q->num_rows() > 0) {
            return $q->row()->quantity;
        } else {
            return 0;
        }
    }

    public function get_other_wh($pr_id, $wh_id){
        $q = $this->db->select('warehouses_products.quantity, warehouses_products.warehouse_id')
                ->where('warehouse_id !=', $wh_id)
                ->where('product_id', $pr_id)
                ->get('warehouses_products');
        if ($q->num_rows() > 0) {
            $data = [];
            foreach (($q->result()) as $row) {
                $data[$row->warehouse_id] = $row->quantity;
            }
        } else {
            return 0;
        }
    }

    public function get_pos_register_movements($Sucursal = NULL,$fech_ini = NULL,$fech_fin = NULL, $module_id = NULL, $document_type_id = NULL)
    {
        $this->db->select('pos_register_movements.*', FALSE);
        if ($Sucursal) {
            $this->db->where('biller_id', $Sucursal);
        }
        if ($document_type_id) {
            $this->db->where('document_type_id', $document_type_id);
        }
        if ($fech_ini) {
            $this->db->where('date >=', $fech_ini)->where('date <=', $fech_fin);
        }
        $q = $this->db->get('pos_register_movements');
        $resultado = $q->result_array();
        return $resultado ;
    }

    public function get_warehouses($warehouse = NULL){
        if ($warehouse) {
            $q = $this->db->get_where('warehouses', ['id =' => $warehouse]);
        }else{
            $q = $this->db->get('warehouses');
        }
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function get_subcategories(){
        $this->db->select('id');
        $this->db->select('name');
        $this->db->where('parent_id !=', NULL);
        $consulta = $this->db->get('categories');
        $data = [];
        foreach (($consulta->result()) as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    public function get_brands(){
        $this->db->select('id');
        $this->db->select('name');
        $consulta = $this->db->get('brands');
        $data = [];
        foreach (($consulta->result()) as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    public function get_colors(){
        $this->db->select('id');
        $this->db->select('name');
        $consulta = $this->db->get('colors');
        $data = [];
        foreach (($consulta->result()) as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    public function get_materials(){
        $this->db->select('id');
        $this->db->select('name');
        $consulta = $this->db->get('materials');
        $data = [];
        foreach (($consulta->result()) as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    public function get_units(){
        $this->db->select('id');
        $this->db->select('name');
        $consulta = $this->db->get('units');
        $data = [];
        foreach (($consulta->result()) as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    public function get_warehouses_products($warehouse){
        $this->db->select('product_id');
        $this->db->select('quantity');
        $this->db->where('warehouse_id =', $warehouse);
        $consulta = $this->db->get('warehouses_products');
        $data = [];
        foreach (($consulta->result()) as $row) {
            $data[$row->product_id] = $row->quantity;
        }
        return $data;
    }

    public function get_warehouses_products_variants($warehouse){
        $this->db->select('option_id');
        $this->db->select('quantity');
        $this->db->where('warehouse_id =', $warehouse);
        $consulta = $this->db->get('warehouses_products_variants');
        $data = [];
        foreach (($consulta->result()) as $row) {
            $data[$row->option_id] = $row->quantity;
        }
        return $data;
    }

    function get_deposits($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id)
    {
        $this->db->select($this->db->dbprefix('deposits').'.reference_no');
        $this->db->select("(SELECT company FROM {$this->db->dbprefix('companies')} WHERE id = {$this->db->dbprefix('deposits')}.company_id) AS customer");
        $this->db->select($this->db->dbprefix('deposits').'.date AS fecha');
        $this->db->select($this->db->dbprefix('deposits').'.amount');
        $this->db->select($this->db->dbprefix('deposits').'.origen_reference_no');
        $this->db->select($this->db->dbprefix('payment_methods').'.name');
        $this->db->select($this->db->dbprefix('payment_methods').'.code');
        $this->db->select($this->db->dbprefix('deposits').'.origen');
        $this->db->select($this->db->dbprefix('deposits').'.paid_by');
        $this->db->from('deposits');
        $this->db->join('payment_methods', 'deposits.paid_by = payment_methods.code','left');
        $this->db->where($this->db->dbprefix('deposits').".date BETWEEN '".$fech_ini."' AND '".$fech_fin."'");
        if ($Sucursal) {
            $this->db->where($this->db->dbprefix('deposits').'.biller_id', $Sucursal);
        }
        $this->db->group_by($this->db->dbprefix('deposits').".id");
        $this->db->order_by($this->db->dbprefix('deposits').'.document_type_id ASC, '.$this->db->dbprefix('deposits').'.date ASC');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;
    }

    function validate_variants(){
        $this->db->select('*');
        $consulta = $this->db->get('product_variants');
        if ($consulta->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_award_points_detail_sales($biller = NULL, $customer = NULL, $start_date_times_format = NULL, $end_date_times_format = NULL){
        $this->db->reset_query();
        $this->db->select('sales.date');
        $this->db->select('companies.vat_no');
        $this->db->select("UPPER({$this->db->dbprefix('companies')}.name) AS name");
        $this->db->select("'sale' AS type_movement");
        $this->db->select("sales.reference_no");
        $this->db->select("sales.grand_total");
        $this->db->select("COALESCE( SUM( FLOOR( ( grand_total / {$this->Settings->each_spent} ) * {$this->Settings->ca_point} ) ), 0) AS total");
        $this->db->from('sales');
        $this->db->join('companies', 'sales.customer_id = companies.id','inner');
        $this->db->where($this->db->dbprefix('sales').".grand_total > 0");
        $this->db->where($this->db->dbprefix('sales').".date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59' ");
        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id', $biller);
        }
        if ($customer) {
            $this->db->where($this->db->dbprefix('sales').'.customer_id', $customer);
        }
        $this->db->group_by($this->db->dbprefix('sales').'.id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado;
    }

    public function get_award_points_detail_returns($biller = NULL, $customer = NULL, $start_date_times_format = NULL, $end_date_times_format = NULL){
        $this->db->reset_query();
        $this->db->select('sales.date');
        $this->db->select('companies.vat_no');
        $this->db->select("UPPER({$this->db->dbprefix('companies')}.name) AS name");
        $this->db->select("'return' AS type_movement");
        $this->db->select("sales.reference_no");
        $this->db->select("sales.grand_total");
        $this->db->select("COALESCE( SUM( CEIL( ( grand_total / {$this->Settings->each_spent} ) * {$this->Settings->ca_point} ) ), 0) AS total");
        $this->db->from('sales');
        $this->db->join('companies', 'sales.customer_id = companies.id','inner');
        $this->db->where($this->db->dbprefix('sales').".grand_total < 0");
        $this->db->where($this->db->dbprefix('sales').".date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59' ");
        if ($biller) {
            $this->db->where($this->db->dbprefix('sales').'.biller_id', $biller);
        }
        if ($customer) {
            $this->db->where($this->db->dbprefix('sales').'.customer_id', $customer);
        }
        $this->db->group_by($this->db->dbprefix('sales').'.id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado;
    }

    public function get_award_points_detail_gitCards($biller = NULL, $customer = NULL, $start_date_times_format = NULL, $end_date_times_format = NULL){
        $this->db->reset_query();
        $this->db->select('gift_cards.date');
        $this->db->select('companies.vat_no');
        $this->db->select("UPPER({$this->db->dbprefix('companies')}.name) AS name");
        $this->db->select("'gift_card' AS type_movement");
        $this->db->select("gift_cards.card_no AS reference_no");
        $this->db->select("gift_cards.value AS grand_total");
        $this->db->select("({$this->db->dbprefix('gift_cards')}.ca_points_redeemed * -1) AS total");
        $this->db->from('gift_cards');
        $this->db->join('companies', 'gift_cards.customer_id = companies.id','inner');
        $this->db->where($this->db->dbprefix('gift_cards').".date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59' ");
        $this->db->where($this->db->dbprefix('gift_cards').".status != 3");
        if ($customer) {
            $this->db->where($this->db->dbprefix('gift_cards').'.customer_id', $customer);
        }
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado;
    }

    public function get_product_profitability($producto = '',$fech_ini = '',$fech_fin = '', $biller_id = '')
    {
        if($producto=='all')  $producto = '';
        if($biller_id=='all')  $biller_id = '';

        // if ($this->Settings->cost_to_profit_and_validation == 2) {
        //     $cost_profit = 'COALESCE(avg_net_unit_cost, 0) + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales_costing, 0)';
        // } else {
            $cost_profit = $this->db->dbprefix('purchase_items').'.net_unit_cost + COALESCE(('.$this->db->dbprefix('purchase_items').'.item_tax_2 / '.$this->db->dbprefix('purchase_items').'.quantity), 0)';
        // }

        $this->db->select_MIN($this->db->dbprefix('sales').'.date');
        $this->db->select($this->db->dbprefix('products').'.code AS cod_product');
        $this->db->select_MAX('name','name');
        $this->db->select('SUM((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('costing').'.quantity) AS total');
        $this->db->select('SUM('.$this->db->dbprefix('costing').'.quantity) AS quantity');
        $this->db->select('SUM(('.$cost_profit.') * '.$this->db->dbprefix('costing').'.quantity) AS costo');
        $this->db->select('(sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('costing').'.quantity)-sum(('.$cost_profit.') * '.$this->db->dbprefix('costing').'.quantity)) as utilidad ');
        $this->db->select('(((sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('costing').'.quantity)-sum(('.$cost_profit.') * '.$this->db->dbprefix('costing').'.quantity))/ sum((net_unit_price + COALESCE('.$this->db->dbprefix('sale_items').'.consumption_sales, 0)) * '.$this->db->dbprefix('costing').'.quantity))*100) as margen');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id');
        $this->db->join('costing', 'costing.sale_item_id = sale_items.id', 'left');
        $this->db->join('purchase_items', 'purchase_items.id = costing.purchase_item_id', 'left');
        $this->db->join('products', 'sale_items.product_id = products.id', 'left');
        if($fech_ini!='') $this->db->where($this->db->dbprefix('sales').'.date BETWEEN '."'$fech_ini'".' AND '."'$fech_fin'".' ');
        if($producto!='') $this->db->where($this->db->dbprefix('products').'.id', $producto);
        if ($biller_id!='') {
            $this->db->where($this->db->dbprefix('sales').'.biller_id', $biller_id);
        }
        $this->db->group_by($this->db->dbprefix('products').'.id');
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado ;

    }

    public function getSalesTotalsWhioutReturns($customer_id)
    {
        $this->db->select(
            "SUM(COALESCE(grand_total, 0)) as total_amount,
            SUM(
                COALESCE(
                    (SELECT SUM(amount) FROM {$this->db->dbprefix('payments')} WHERE sale_id = {$this->db->dbprefix('sales')}.id AND multi_payment = 1 )
                , 0)
            ) as paid", FALSE)
            ->where('customer_id', $customer_id);
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}
