<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo sprintf(lang('add_category'), lang('category')); ?></h4>
        </div>
        <?php $attrib = ['id'=>'add_category_form'];
        echo admin_form_open_multipart("system_settings/add_category", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?= lang('category_code', 'code'); ?>
                <div class="input-group">
                    <?= form_input('code', set_value('code'), 'class="form-control validate_code" data-module="category" id="code" required="required"'); ?>
                    <span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('random_code_text') ?>">
                        <i class="fa fa-random"></i>
                    </span>
                    <span class="input-group-addon pointer" id="consecutive_code" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('consecutive_code_text') ?>">
                        <b>123</b>
                    </span>
                </div>
                <input type="hidden" name="code_consecutive" class="code_consecutive_setted">
            </div>
            <div class="form-group">
                <?= lang('category_name', 'name'); ?> 
                <?= form_input('name', set_value('name'), 'class="form-control gen_slug" id="name" required="required"'); ?>
            </div>
            <div class="form-group all">
                <?= lang('slug', 'slug'); ?>
                <?= form_input('slug', set_value('slug'), 'class="form-control tip" id="slug" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("category_image", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>
            <div class="col-md-12">
                <label>Imágen</label>
                <br>
                <img class="main_image" style="width: 180px;">
            </div>
            <div class="form-group">
                <?= lang("parent_category", "parent") ?>
                <?php
                $cat[''] = lang('select').' '.lang('parent_category');
                foreach ($categories as $pcat) {
                    $cat[$pcat->id] = $pcat->name;
                }
                echo form_dropdown('parent', $cat, (isset($_POST['parent']) ? $_POST['parent'] : ''), 'class="form-control select" id="parent" style="width:100%"')
                ?>
            </div>
            <div class="form-group">
                <?= lang("subcategory", "subcategory") ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Si diligencia este dato, se creará cómo subcategoría de segundo nivel" style="cursor: pointer"></i>
                <?php
                $scat[''] = lang('select').' '.lang('subcategory');
                echo form_dropdown('subcategory', $scat, (isset($_POST['subcategory']) ? $_POST['subcategory'] : ''), 'class="form-control select" id="subcategory" style="width:100%"')
                ?>
            </div>
            <?php if ($printers): ?>
                <div class="form-group">
                    <?= lang('select_printer', 'printer') ?>
                    <?= form_dropdown('printer', $printers, '', 'class="form-control" required="required" id="printer"') ?>
                </div>
            <?php endif ?>
            <?php if ($users_preparations): ?>
                <div class="form-group">
                    <?= lang('select_user_preparation', 'user_preparation') ?>
                    <?= form_dropdown('user_preparation', $users_preparations, '', 'class="form-control" required="required" id="user_preparation"') ?>
                </div>
            <?php endif ?>
            <div class="form-group all">
                <?= lang('profitability_margin_price_validation', 'profitability_margin'); ?>
                <?= form_input('profitability_margin', (isset($_POST['profitability_margin']) ? $_POST['profitability_margin'] : ''), 'class="form-control tip only_number" id="profitability_margin"') ?>
            </div>
            <div class="form-group all">
                <?= lang('profitability_margin_price_base', 'profitability_margin_price_base'); ?>
                <?= form_input('profitability_margin_price_base', (isset($_POST['profitability_margin_price_base']) ? $_POST['profitability_margin_price_base'] : ''), 'class="form-control tip only_number" id="profitability_margin_price_base"') ?>
            </div>
            <div class="form-group">
                <?= lang('hide_category_products', 'hide_category_products') ?><br>
                <label>
                    <input type="checkbox" name="hide_category_products" id="hide_category_products" value="1">
                    <?= lang('hide') ?>
                </label>
            </div>
            <div class="form-group">
                <?= lang('except_category_taxes', 'except_category_taxes') ?><br>
                <label>
                    <input type="checkbox" name="except_category_taxes" id="except_category_taxes" value="1">
                    <?= lang('yes') ?>
                </label>
            </div>
            <div class="form-group">
                <?= lang('hide_ecommerce', 'hide_ecommerce') ?><br>
                <label>
                    <input type="checkbox" name="hide_ecommerce" id="hide_ecommerce" value="1">
                    <?= lang('yes') ?>
                </label>
            </div>
            <div class="form-group">
                <?= lang("suggested_tax_rate", "tax_rate") ?>
                <?php
                $tr[''] = lang('select').' '.lang('tax_rate');
                foreach ($tax_rates as $tax_rate) {
                    $tr[$tax_rate->id] = $tax_rate->name;
                }
                echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ''), 'class="form-control select" id="tax_rate" style="width:100%"')
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="add_category" value="1">
            <button class="btn btn-primary submit" type="button"><?= lang('submit') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
    $(document).ready(function() {
        $(document).on('click', '.submit', function(){
            if ($('#add_category_form').valid()) {
                $('#add_category_form').submit();
            }
        });
        $('[data-toggle="tooltip"]').tooltip();
        $('.gen_slug').change(function(e) {
            getSlug($(this).val(), 'category');
        });
        var prev_src = $('.main_image').prop('src');
        $(document).on("change", "#image", function(){
            $('.main_image').prop('src', prev_src);
            input = this;
            var file, img;
            var _URL = window.URL || window.webkitURL;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    _URL.revokeObjectURL(objectUrl);
                    if (this.height > site.settings.iheight) {
                        command: toastr.warning('La imagen no puede tener más de '+site.settings.iheight+'px de altura.', '¡Atención!', {
                                "showDuration": "500",
                                "hideDuration": "1000",
                                "timeOut": "4000",
                                "extendedTimeOut": "1000",
                            });
                        return false;
                    } else {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                          $('.main_image').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                };
                img.src = objectUrl;
            }
        });
    });
    $(document).on('change', '#parent', function(){
        $.ajax({
            url : site.base_url+"system_settings/get_subcategories/"+$('#parent').val(),
            type : "GET",
            dataType : "JSON"
        }).done(function(data){
            $('#subcategory').html(data.html).select2();
        });
    });
</script>