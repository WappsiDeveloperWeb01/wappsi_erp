<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entries extends Admin_Controller {
	public function __construct() {
        parent::__construct();
    }

	public function index() {

		// render page
		$p_entrytype = $this->DB1->where('origin', 1)->order_by('entrytypes'.$this->DB1->dbsuffix.'.id', 'asc')->limit(1)->get('entrytypes'.$this->DB1->dbsuffix);
		if ($p_entrytype->num_rows() > 0) {
			$p_entrytype = $p_entrytype->row_array();
			$p_entrytype_label = $p_entrytype['label'];
		} else {
			$p_entrytype_label = false;
			$this->session->set_flashdata('warning', 'No hay tipo de asientos contables con origen de contabilidad.');
		}

		$this->data['entry_types'] = $this->DB1->get('entrytypes'.$this->DB1->dbsuffix)->result_array();
		if ($this->mAccountSettings->cost_center) {
			if ($this->user_cost_centers) {
				foreach ($this->user_cost_centers as $key => $value) {
					if ($key == 0) {
						$this->DB1->where('id', $value);
					} else {
						$this->DB1->or_where('id', $value);
					}
				}
			}
			$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			if ($cost_centers_data->num_rows() > 0) {
				$cost_centers_data = $cost_centers_data->result_array();
				foreach ($cost_centers_data as $row => $cost_center_data) {
					$cost_centers[] = $cost_center_data;
				}
			} else {
				$cost_centers = false;
			}
			$this->data['cost_centers'] = $cost_centers;
		}

		$this->data['p_entrytype_label'] = $p_entrytype_label;
		$this->render('entries/index');
	}


	public function getEntries(){

		// select all entries

		$fecha_inicio = $_POST['fecha_inicio'];
		$fecha_fin = $_POST['fecha_fin'];
		$tipo_nota = $_POST['tipo_nota'];
		$cost_center = isset($_POST['cost_center']) ? $_POST['cost_center'] : NULL;
		$numero_nota = $_POST['numero_nota'];

		$this->DB1->select('entries'.$this->DB1->dbsuffix.'.*, entries'.$this->DB1->dbsuffix.'.id as entryNumber, entrytypes'.$this->DB1->dbsuffix.'.label as entryLabel, entrytypes'.$this->DB1->dbsuffix.'.name as entryName, '.$this->DB1->dbprefix('companies').'.name as company_name');
		$this->DB1->join('entrytypes'.$this->DB1->dbsuffix, 'entrytypes'.$this->DB1->dbsuffix.'.id = entries'.$this->DB1->dbsuffix.'.entrytype_id');
		$this->DB1->join('entryitems'.$this->DB1->dbsuffix, 'entryitems'.$this->DB1->dbsuffix.'.entry_id = entries'.$this->DB1->dbsuffix.'.id');
		$this->DB1->join('companies', 'companies.id = entryitems'.$this->DB1->dbsuffix.'.companies_id');

		$condiciones = [];

		$condiciones['date >='] = $fecha_inicio;
		$condiciones['date <='] = $fecha_fin;

		$condiciones['entries'.$this->DB1->dbsuffix.'.state !='] = 3;

		if (!empty($tipo_nota)) {
			$condiciones['entrytype_id'] = $tipo_nota;
		}

		if (!empty($numero_nota)) {
			$condiciones['number'] = $numero_nota;
		}

		if (!empty($cost_center)) {
			$condiciones['entryitems'.$this->DB1->dbsuffix.'.cost_center_id'] = $cost_center;
		}

		$this->DB1->where($condiciones);
		if ($this->user_cost_centers && !$cost_center) {
			$sql_where = '(';
			foreach ($this->user_cost_centers as $key => $ucc) {
				if ($key == 0) {
					$sql_where .= 'entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc.' ';
				} else {
					$sql_where .= 'OR entryitems'.$this->DB1->dbsuffix.'.cost_center_id = '.$ucc.' ';
				}
			}
			$sql_where .= ')';
			$this->DB1->where($sql_where);
		}
		$this->DB1->order_by('entries'.$this->DB1->dbsuffix.'.date DESC, entries'.$this->DB1->dbsuffix.'.id DESC')
		->group_by('entries'.$this->DB1->dbsuffix.'.id');
		$query = $this->DB1->get('entries'.$this->DB1->dbsuffix);

		// pass an array of all entries to view page
		$entries = $query->result_array();

		$data = [];

		$num = 0;

		foreach ($entries as $entry) {

			$entryTypeName = $entry['entryName'];
			$entryTypeLabel = $entry['entryLabel'];


			$data[$num]['date'] = $entry['date'];
			$data[$num]['number'] = $this->functionscore->toEntryNumber($entry['number'], $entry['entrytype_id']).(!empty($entry['consecutive_supplier']) ? " (".$entry['consecutive_supplier'].")":"");
			$data[$num]['note'] = $entry['notes'];
			$data[$num]['type'] = $entryTypeName;
			$data[$num]['third'] = $entry['company_name'] ;
			$data[$num]['amount'] = ($entry['dr_total'] == $entry['cr_total']) ? $this->functionscore->toCurrency('', $entry['cr_total']) : "Eror";

			if ($entry['state'] == '1') {

				$data[$num]['state'] = '<p><span class="label label-primary">Aprobado</span><p>';

			} else if ($entry['state'] == '2') {

				$data[$num]['state'] = '<p><span class="label">Desaprobado</span></p>';

			} else if ($entry['state'] == '0') {

				$data[$num]['state'] = '<p><span class="label label-warning">Anulada</span></p>';

			}

			$data[$num]['actions'] = '<div class="btn-group">
								  <div class="dropdown">
								    <button class="btn btn-success btn-sm" type="button" id="accionesProducto" data-toggle="dropdown" aria-haspopup="true">
								      Acciones
								      <span class="caret"></span>
								    </button>
								    <ul class="dropdown-menu pull-right" aria-labelledby="accionesProducto" style="width:220%;">
									  	<li>
									  		<a href="'. base_url().'entries/view/'. ($entryTypeLabel) .'/'. $entry['id'] .'" class="no-hover" escape="false"><i class="fa fa-sign-in"></i>'. lang('entries_views_index_th_actions_view_btn') .'</a>
									  	</li>';
								if (!empty($entry['consecutive_supplier'])) {
									$data[$num]['actions'] .='<li>
									  	<a href="'. base_url().'entries/group_view/'. ($entryTypeLabel) .'/'. urlencode($entry['consecutive_supplier']) .'/'. $entry['companies_id'] .'" class="no-hover" escape="false"><i class="fa fa-sign-in"></i> '. lang('entry_group_view') .'</a>
									 </li>';
								}
								if ($entry['origin'] == 1) {
									if ($entry['state'] == 1) {
										$data[$num]['actions'] .='
							    		<li class="cambiarEstado">
									    	<a href="#delete_entry_modal" data-toggle="modal" data-entry="'. $entry['id'] .'" data-entrytype="'. ($entryTypeLabel) .'">
									    	'.lang('entries_views_views_td_actions_state').'
									    		<input type="checkbox" checked data-identry="'. $entry['id'] .'" data-toggle="toggle" data-on="Aprobado" data-off="Anulado" data-size="mini" data-width="70%" />
									    	</a>
									    </li>';
									} else if ($entry['state'] == 2) {
										$data[$num]['actions'] .= '
									  	<li>
											<a href="'. base_url().'entries/edit/'. ($entryTypeLabel) .'/'. $entry['id'] .'" class="no-hover" escape="false"><i class="fa fa-pencil"></i>'. lang('entries_views_index_th_actions_edit_btn') .'</a>
									    </li>
									    <li class="cambiarEstado">
											<a href="#approve_entry_modal" data-toggle="modal" data-entrytype="'. ($entryTypeLabel) .'" data-entry="'. $entry['id'] .'">
											'.lang('entries_views_views_td_actions_state').'
												<input type="checkbox" data-toggle="toggle" data-on="Aprobado" data-off="Desaprobado" data-size="mini" data-width="70%" />
											</a>
									    </li>';
									}
									$data[$num]['actions'] .= '
									  	<li>
									  		<a href="'. base_url().'entries/add/'. ($entryTypeLabel) .'/'. $entry['id'].'" class="no-hover" escape="false"><i class="fa fa-copy"></i>'. lang('entries_views_index_th_actions_duplicate_btn') .'</a>
									  	</li>';
								}
								if ($entry['origin'] == 1 && $entry['state'] == 2) {

								}

							    if ($entry['origin'] == 1 && $entry['state'] == 1) {

							    }

			$data[$num]['actions'] .= '</ul>
								  </div>
								</div>';
			$data[$num]['entryLabel'] = $entry['entryLabel'];
			$data[$num]['entryNumber'] = $entry['entryNumber'];

			$num++;
		}


	   $output = [
		'sEcho' => 1,
		'iTotalRecords' => count($data),
		'iTotalDisplayRecords' => count($data),
		'aaData' => $data
		];

		echo json_encode($output);
	}

	/**
	* add method
	*
	* @param string $entrytypeLabel
	* @return void
	*/
	public function add($entrytypeLabel = null, $id_entry_duplicate = NULL) {

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}
		// collapse sidebar on page render
        $this->mBodyClass .= ' sidebar-collapse';

		/* Check for valid entry type */
		if (!$entrytypeLabel)
		{
			// show 404 error page
			show_404();
		}
		// load entry model
		$this->load->model('entry_model');
		$this->load->model('companies_model');

		// Select from entrytypes table in DB1 where label = $entrytypeLabel
		$entrytype = $this->DB1->query("SELECT * FROM ".$this->DB1->dbprefix('entrytypes'.$this->DB1->dbsuffix)." WHERE label='$entrytypeLabel'");
		// create array of select data from DB1 - [entrytypes] table
		$entrytype = $entrytype->row_array();
		// check if entry type exists
		if (!$entrytype)
		{
			// set error message if entry type do not exist
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}
		// get allowed decimal place from account settings
		$allowed = $this->mAccountSettings->decimal_places;

		// form validation rules
		$this->form_validation->set_rules('number', lang('entries_cntrler_add_form_validation_number_label'), 'is_numeric');
		$this->form_validation->set_rules('date', lang('entries_cntrler_add_form_validation_date_label'), 'required');
		$this->form_validation->set_rules('tag_id', lang('entries_cntrler_add_form_validation_tag_label'), 'required');

		$dc_valid = false; 	// valid debit or credit ledger
		$dr_total = 0;		// total dr amount initially 0
		$cr_total = 0;		// total cr amount initially 0
		// check if $_POST['Entryitem'] is set and is an array
		if (isset($_POST['Entryitem']) && is_array($_POST['Entryitem']))
		{
			// loop for all $_POST['Entryitem']
		    foreach ($_POST['Entryitem'] as $key => $value)
		    {
		    	// check if $value['ledger_id'] less then or equal to 0
		    	if (!isset($value['ledger_id']) || $value['ledger_id'] <= 0)
		    	{
		    		// continue to next Entryitem
					continue;
				}
				// array of selected ledger
		    	$ledger = $this->DB1->get_where('ledgers'.$this->DB1->dbsuffix, array('id' => $value['ledger_id']))->row_array();
		    	// check if $ledger is Empty
				if (!$ledger)
				{
					// set form validation for Entryitem to be required with error alert
    				$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_invalid_ledger_form_validation_alert')));
				}
				// check if Only Bank or Cash account is present on both Debit and Credit side
				if ($entrytype['restriction_bankcash'] == 4)
				{
					// check if ledger is [NOT] a Bank or Cash Account
					if ($ledger['type'] != 1) {
    					$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_restriction_bankcash_4_form_validation_alert')));
					}
				}
				// check if Only NON Bank or Cash account is present on both Debit and Credit side
				if ($entrytype['restriction_bankcash'] == 5)
				{
					// check if ledger is a Bank or Cash Account
					if ($ledger['type'] == 1) {
    					$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_restriction_bankcash_5_form_validation_alert')));
					}
				}
				// check if ledger is Debit
				if ($value['dc'] == 'D')
				{
					// check if Atleast one Bank or Cash account must be present on Debit side
					if ($entrytype['restriction_bankcash'] == 2)
					{
						// check if ledger is a Bank or Cash Account
						if ($ledger['type'] == 1)
						{
							// set dc_valid = true
							$dc_valid = true;
						}
					}
				} else if ($value['dc'] == 'C') // check if ledger is Credit
				{
					// check if Atleast 1 Bank or Cash account is present on Credit side
					if ($entrytype['restriction_bankcash'] == 3)
					{
						// check if ledger is Bank or Cash Account
						if ($ledger['type'] == 1)
						{
							// set dc_valid = true
							$dc_valid = true;
						}
					}
				}
				// some more form validation rules
		        $this->form_validation->set_rules('Entryitem['.$key.'][dc]', lang('entries_cntrler_add_form_validation_entryitem_dc_label'), 'required'); // Any validation you need
		        $this->form_validation->set_rules('Entryitem['.$key.'][ledger_id]', lang('entries_cntrler_add_form_validation_entryitem_ledger_id_label'), 'required'); // Any validation you need

		        // if Debit selected
		        if ($value['dc'] == 'D')
		        {
		        	// if dr_amount not empty
		        	if (!empty($value['dr_amount']))
		        	{
		        		// set form validation rules form dr_amount
		        		$this->form_validation->set_rules('Entryitem['.$key.'][dr_amount]', '', "greater_than[0]|amount_okay[$allowed]");

		        		// calculate total debit amount
						$dr_total = $this->functionscore->calculate($dr_total, $value['dr_amount'], '+');
		        	}
		        }else // if credit selected
		        {
		        	// if cr_amount if not empty
		        	if (!empty($value['cr_amount']))
		        	{
		        		// set form validation rules form cr_amount
			        	$this->form_validation->set_rules('Entryitem['.$key.'][cr_amount]', '', "greater_than[0]|amount_okay[$allowed]");

			        	// calculate total credit amount
						$cr_total = $this->functionscore->calculate($cr_total, $value['cr_amount'], '+');
		        	}
		        }
		    }
		    // check if total dr or cr amount is not equal
		    if ($this->functionscore->calculate($dr_total, $cr_total, '!='))
		    {
		    	// set form validation error
        		$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_dr_cr_total_not_equal_form_validation_alert')));
			}
		}

		// check if restriction_bankcash = 2
		if ($entrytype['restriction_bankcash'] == 2)
		{
			// check if Atleast one Bank or Cash account is present on Debit side
			if (!$dc_valid)
			{
				// set form validation error
        		$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_restriction_bankcash_2_not_valid_dc_form_validation_alert')));
			}
		}

		// check if Atleast one Bank or Cash account is present on Credit side
		if ($entrytype['restriction_bankcash'] == 3)
		{
			// check if no Bank or Cash account is present on Credit side
			if (!$dc_valid) {
				// set form validation error
        		$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_restriction_bankcash_3_not_valid_dc_form_validation_alert')));
			}
		}

		/***** Check if entry type numbering is auto ******/
		if ($entrytype['numbering'] == 1)
		{
			/* check if $_POST['number'] is empty */
			if (empty($this->input->post('number')))
			{
				// set entry number to next entry number
				$number = $this->entry_model->nextNumber($entrytype['id']);
			}else // if not empty
			{
				// set entry number to $_POST['number']
				$number = $this->input->post('number');
			}
		}else if ($entrytype['numbering'] == 2) // Check if entry type numbering is manual and required
		{
			/* Manual + Required - Check if $_POST['number'] is empty */
			if (empty($this->input->post('number')))
			{
				//  set form validation rule
        		$this->form_validation->set_rules('number', '', 'required', array('required' => lang('entries_cntrler_entry_number_required_form_validation_alert')));
			} else // if not empty
			{
				// set entry number to $_POST['number']
				$number = $this->input->post('number');
			}
		} else // if entry type numbering is manual and not required
		{
			/* Manual + Optional - set entry number to $_POST['number'] */
			$number = $this->input->post('number');
		}

		// check if form is NOT Validated
		if ($this->form_validation->run() == FALSE)
		{

			$date = $this->DB1->where('entrytype_id', $entrytype['id'])->where('state !=', '0')->order_by('date', 'desc')->limit(1)->get('entries'.$this->DB1->dbsuffix);

			if ($date->num_rows() > 0) {
				$date = $date->row_array();
				$this->data['limit_date'] = $date['date'];
			}

			$this->data['entrytype'] = $entrytype; // pass entrytype array to view page
			// pass page title to view page
			$this->data['title'] = sprintf(lang('entries_cntrler_add_title'), $entrytype['name']);
			// pass tag_options array to view page
			$this->data['tag_options'] = $this->DB1->select('id, title')->get('tags'.$this->DB1->dbsuffix)->result_array();

			/*  Check if input method is post */
			if ($this->input->method() == 'post' || $id_entry_duplicate != NULL)
			{
				// initilize current entry items array
				$curEntryitems = array();

				if (isset($_POST['Entryitem']) && !empty($_POST['Entryitem'])) {
					// loop to save post data to current entry items array
					foreach ($_POST['Entryitem'] as $row => $entryitem)
					{

						$company_data = $this->companies_model->get_company_by_id($entryitem['companies_id']);
						if (isset($entryitem['ledger_balance'])) {
							$curEntryitems[$row] = array
							(
								'dc' => $entryitem['dc'],
								'ledger_id' => $entryitem['ledger_id'],
								'dr_amount' => isset($entryitem['dr_amount']) ? $entryitem['dr_amount'] : '',
								'cr_amount' => isset($entryitem['cr_amount']) ? $entryitem['cr_amount'] : '',
								'narration' => $entryitem['narration'],
								'ledger_balance' => $entryitem['ledger_balance'],
								'companies_id' => $entryitem['companies_id'],
								'company_name' => $company_data['company'],
								'ledgername' => $this->ledger_model->getName($entryitem['ledger_id'])
							);
						}else{
							$curEntryitems[$row] = array
							(
								'dc' => $entryitem['dc'],
								'ledger_id' => $entryitem['ledger_id'],
								'dr_amount' => isset($entryitem['dr_amount']) ? $entryitem['dr_amount'] : '',
								'cr_amount' => isset($entryitem['cr_amount']) ? $entryitem['cr_amount'] : '',
								'narration' => $entryitem['narration'],
								'companies_id' => $entryitem['companies_id'],
								'company_name' => $company_data['company'],
								'ledgername' => $this->ledger_model->getName($entryitem['ledger_id'])
							);
						}
					}
				}

				if ($id_entry_duplicate != NULL) {
					$entry_duplicate = $this->DB1->where('id', $id_entry_duplicate)->get('entries'.$this->DB1->dbsuffix)->row_array();
					$entryitems_duplicate = $this->DB1->where('entry_id', $id_entry_duplicate)->get('entryitems'.$this->DB1->dbsuffix)->result_array();

					$cnt = count($curEntryitems);

					$cost_center_duplicate = null;

					foreach ($entryitems_duplicate as $row => $eitem) {
						$company_data = $this->companies_model->get_company_by_id($eitem['companies_id']);
						// if ($eitem['companies_id'] == 519) {
						// 	exit(var_dump($company_data));
						// }
						$curEntryitems[$cnt] = array
							(
								'dc' => $eitem['dc'],
								'ledger_id' => $eitem['ledger_id'],
								'dr_amount' => $this->functionscore->formatDecimal($eitem['dc'] == "D" ? $eitem['amount'] : ''),
								'cr_amount' => $this->functionscore->formatDecimal($eitem['dc'] == "C" ? $eitem['amount'] : ''),
								'narration' => $eitem['narration'],
								'base' => $eitem['base'],
								'companies_id' => $eitem['companies_id'],
								'ledgername' => $this->ledger_model->getName($eitem['ledger_id']),
								'company_name' => $company_data['company'] ? $company_data['company'] : $company_data['name'],
								'cost_center_id' => $eitem['cost_center_id'],
							);
						if (!$cost_center_duplicate) {
							$cost_center_duplicate = $eitem['cost_center_id'];
						}
						$cnt++;
					}

					$company_data = $this->companies_model->get_company_by_id($entry_duplicate['companies_id']);
					$this->data['third_duplicate'] = $entry_duplicate['companies_id'];
					$this->data['cost_center_duplicate'] = $cost_center_duplicate;
					$this->data['third_name_duplicate'] = $company_data ? $company_data['company'] : NULL;
					$this->data['state_duplicate'] = $entry_duplicate['state'];
					$this->data['notes_duplicate'] = $entry_duplicate['notes'];


				} else if ($_POST['company_id']) {
					$company_data = $this->companies_model->get_company_by_id($_POST['company_id']);
					$this->data['third_duplicate'] = $_POST['company_id'];
					$this->data['third_name_duplicate'] = $company_data ? $company_data['company'] : NULL;
				}

				// pass current entry items array to view page
				$this->data['curEntryitems'] = $curEntryitems;

			}else // if method is NOT post
			{
				$curEntryitems = array(); // initilize current entry items array

				/* Special case if atleast one Bank or Cash on credit side (3) then 1st item is Credit */
				if ($entrytype['restriction_bankcash'] == 3){
					$curEntryitems[0] = array('dc' => 'C');
					$curEntryitems[1] = array('dc' => 'D');
				}else /* else 1st item is Debit */
				{
					$curEntryitems[0] = array('dc' => 'D');
					$curEntryitems[1] = array('dc' => 'C');
				}

				// pass current entry items array to view page
				$this->data['curEntryitems'] = $curEntryitems;
			}

			$this->data['consecutivoEntry'] = $this->entry_model->nextNumber($entrytype['id']);
			$this->data['isNumbering'] = $entrytype['numbering'];

			if ($this->mAccountSettings->cost_center) {
				$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
				if ($cost_centers_data->num_rows() > 0) {
					$cost_centers_data = $cost_centers_data->result_array();
					foreach ($cost_centers_data as $row => $cost_center) {
						$cost_centers[] = $cost_center;
					}
				} else {
					$cost_centers = false;
				}

				$this->data['cost_centers'] = $cost_centers;
			}


			// render page
			if ($this->mSettings->entry_form) {
				$this->render('entries/add2');
			}else{
				$this->render('entries/add');
			}

		}else // if form is Validated
		{
			/***************************************************************************/
			/*********************************** ENTRY *********************************/
			/***************************************************************************/

			$entrydata = null; // create entry data array to insert in [entries] table - DB1
			$entrydata['Entry']['number'] = $number; // set entry number in entry data array


			$entrydata['Entry']['entrytype_id'] = $entrytype['id']; // set entrytype_id in entry data array

			// check if $_POST['tag_id'] is empty
			if (empty($this->input->post('tag_id')))
			{
				// set entry tag id in entry data array to [NULL]
				$entrydata['Entry']['tag_id'] = null;

			}else // if $_POST['tag_id'] is NOT empty
			{
				// set entry tag id in entry data array to $_POST['tag_id']
				$entrydata['Entry']['tag_id'] = $this->input->post('tag_id');
			}

			/***** Check if $_POST['notes'] is empty *****/
			if (empty($this->input->post('notes')))
			{
				// set entry note in entry data array to [NULL]
				$entrydata['Entry']['notes'] = '';
			}else // if NOT empty
			{
				// set entry note in entry data array to $_POST['notes']
				$entrydata['Entry']['notes'] = $this->input->post('notes');
			}

			if ($this->input->post('entry_approved'))
			{
				// Si se indicó que la nota entra aprobada, se coloca estado 1 a la misma.
				$entrydata['Entry']['state'] = 1;
			}

			/***** Set entry date to $_POST['date'] after converting to sql format(dateToSql function) *****/
			$entrydata['Entry']['date'] = $this->functionscore->dateToSql($this->input->post('date'));


			/***************************************************************************/
			/***************************** ENTRY ITEMS *********************************/
			/***************************************************************************/
			/* Check ledger restriction */

			$entrydata['Entry']['dr_total'] = $dr_total; // set entry dr_total in entry data array as $dr_total
			$entrydata['Entry']['cr_total'] = $cr_total; // set entry cr_total in entry data array as $cr_total

			/* Add item to entry item data array if everything is ok */
			$entryitemdata = array(); // create entry items data array to insert in [entryitems] table - DB1

			// loop for all Entryitems from post data
			foreach ($this->input->post('Entryitem') as $row => $entryitem)
			{
				// check if $entryitem['ledger_id'] less then or equal to 0
				if ($entryitem['ledger_id'] <= 0)
				{
					// continue to next entryitem
					continue;
				}

				// if entryitem is debit
				if ($entryitem['dc'] == 'D')
				{
					// save entry item data array with dr_amount
					$entryitemdata[] = array(
						'Entryitem' => array(
							'dc' => $entryitem['dc'],
							'ledger_id' => $entryitem['ledger_id'],
							'amount' => $this->functionscore->formatDecimal($entryitem['dr_amount']),
							'narration' => $entryitem['narration'],
							'companies_id' => $entryitem['companies_id'],
							'cost_center_id' => isset($entryitem['cost_center_id']) ? $entryitem['cost_center_id'] : NULL,
							'base' => $entryitem['base']
						)
					);
				}else // if entrytype is credit
				{
					// save entry item data array with cr_amount
					$entryitemdata[] = array(
						'Entryitem' => array(
							'dc' => $entryitem['dc'],
							'ledger_id' => $entryitem['ledger_id'],
							'amount' => $this->functionscore->formatDecimal($entryitem['cr_amount']),
							'narration' => $entryitem['narration'],
							'companies_id' => $entryitem['companies_id'],
							'cost_center_id' => isset($entryitem['cost_center_id']) ? $entryitem['cost_center_id'] : NULL,
							'base' => $entryitem['base']

						)
					);
				}
			}

			$entrydata['Entry']['origin'] = 1;
			$entrydata['Entry']['companies_id'] = $this->input->post('company_id');
			//$entrydata['Entry']['cost_center_id'] = $this->input->post('cost_center_id');

			/* insert entry data array to entries table - DB1 */
			$add  = $this->DB1->insert('entries'.$this->DB1->dbsuffix, $entrydata['Entry']);

			// if entry data is inserted
			if ($add)
			{
			   	$insert_id = $this->DB1->insert_id(); // get inserted entry id

			   	// loop for inserting entry item data array to [entryitems] table - DB1
				foreach ($entryitemdata as $row => $itemdata)
				{
					// entry_id for each entry item as id of last entry
					$itemdata['Entryitem']['entry_id'] = $insert_id;

					// insert item data to entryitems table - DB1
					$this->DB1->insert('entryitems'.$this->DB1->dbsuffix ,$itemdata['Entryitem']);
				}

				// set entry number as per prefix, suffix and zero padding for that entry type for logging
				$entryNumber = $this->functionscore->toEntryNumber($entrydata['Entry']['number'], $entrytype['id']);

				// insert log if logging is enabled
				$this->settings_model->add_log(sprintf(lang('entries_cntrler_add_log'),$entrytype['name'], $entryNumber), 1);

				// set success alert message
				$this->session->set_flashdata('message', sprintf(lang('entries_cntrler_add_entry_created_successfully'),$entrytype['name'], $entryNumber));
				// redirect to index page
				$this->entry_model->update_consecutive($entrytype['id'], $number, $insert_id);
				redirect('entries/add/'.$entrytypeLabel);
			}else
			{
				// set error alert message
				$this->session->set_flashdata('error', lang('entries_cntrler_add_entry_not_created_error'));
				// redirect to index page
				redirect('entries/index');
			}
		}
	}


	public function manualconsecutive($entrytype = NULL, $number = NULL){
		$numbering_exists = $this->DB1->where(array('entrytype_id' => $entrytype, 'number' => $number))->get('entries'.$this->DB1->dbsuffix);
		if ($numbering_exists->num_rows() > 0) {
			echo "1";
		} else {
			echo "0";
		}
	}

	/**
	* edit method
	*
	* @param string $entrytypeLabel
	* @param string $id
	* @return void
	*/
	public function edit($entrytypeLabel = null, $id = null)
	{
		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

		// load model - entry_model
		$this->load->model('entry_model');

		/* Check for valid entry type */
		if (!$entrytypeLabel)
		{
			// show 404 error page
			show_404();
		}

		// create entry type array where label = [$entrytypeLabel]
		$entrytype = $this->DB1->query("SELECT * FROM ".$this->DB1->dbprefix('entrytypes'.$this->DB1->dbsuffix)." WHERE label='".$entrytypeLabel."'")->row_array();

		// if no entry type found
		if (!$entrytype)
		{
			// set error message
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// get allowed decimal place from account settings
		$allowed = $this->mAccountSettings->decimal_places;

		// form validation rules
		$this->form_validation->set_rules('number', lang('entries_cntrler_edit_form_validarion_number'), 'is_numeric');
		$this->form_validation->set_rules('date', lang('entries_cntrler_edit_form_validarion_date'), 'required');
		$this->form_validation->set_rules('tag_id', lang('entries_cntrler_edit_form_validarion_tag'), 'required');

		$dc_valid = false; 	// valid Debit or Credit
		$dr_total = 0;		// total Debit amount
		$cr_total = 0;		// total credit amount

		// if Entryitem present in post data and is an array
		if (isset($_POST['Entryitem']) && is_array($_POST['Entryitem']))
		{
			// loop for all entry items
		    foreach ($_POST['Entryitem'] as $key => $value)
		    {
		    	// check if $value['ledger_id'] less then or equal to 0
		    	if ($value['ledger_id'] <= 0)
		    	{
		    		// continue to next Entry item
					continue;
				}

				// ledgers array where id = selected entry items ledger id
		    	$ledger = $this->DB1->get_where('ledgers'.$this->DB1->dbsuffix, array('id' => $value['ledger_id']))->row_array();

		    	// if ledger not found
				if (!$ledger)
				{
					// set form validation for Entryitem to be required with error alert
    				$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_invalid_ledger_form_validation_alert')));
				}
				// check if Only Bank or Cash account is present on both Debit and Credit side
				if ($entrytype['restriction_bankcash'] == 4)
				{
					// check if ledger is [NOT] a Bank or Cash Account
					if ($ledger['type'] != 1)
					{
						// set form validation for Entryitem to be required with error alert
    					$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_restriction_bankcash_4_form_validation_alert')));
					}
				}

				// check if Only NON Bank or Cash account is present on both Debit and Credit side
				if ($entrytype['restriction_bankcash'] == 5)
				{
					if ($ledger['type'] == 1)
					{
						// set form validation for Entryitem to be required with error alert
    					$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_restriction_bankcash_5_form_validation_alert')));
					}
				}

				// check if ledger is Debit
				if ($value['dc'] == 'D') {
					// check if Atleast one Bank or Cash account must be present on Debit side
					if ($entrytype['restriction_bankcash'] == 2)
					{
						// check if ledger is a Bank or Cash Account
						if ($ledger['type'] == 1)
						{
							// set dc_valid = true
							$dc_valid = true;
						}
					}
				} else if ($value['dc'] == 'C') // check if ledger is Credit
				{
					// check if Atleast 1 Bank or Cash account is present on Credit side
					if ($entrytype['restriction_bankcash'] == 3)
					{
						// check if ledger is Bank or Cash Account
						if ($ledger['type'] == 1)
						{
							// set dc_valid = true
							$dc_valid = true;
						}
					}
				}

				// some more form validation rules
		        $this->form_validation->set_rules('Entryitem['.$key.'][dc]', lang('entries_cntrler_edit_form_validation_entryitem_dc_label'), 'required'); // Any validation you need
		        $this->form_validation->set_rules('Entryitem['.$key.'][ledger_id]', lang('entries_cntrler_edit_form_validation_entryitem_ledger_id_label'), 'required'); // Any validation you need

		        // if Debit selected
		        if ($value['dc'] == 'D')
		        {
		        	// if dr_amount if not empty
		        	if (!empty($value['dr_amount']))
		        	{
						// set form validation rules form dr_amount
		        		$this->form_validation->set_rules('Entryitem['.$key.'][dr_amount]', '', "greater_than[0]"); // Any validation you need

		        		// calculate total debit amount
						$dr_total = $this->functionscore->calculate($dr_total, $value['dr_amount'], '+');
		        	}
		        }else // if credit selected
		        {
		        	// if cr_amount if not empty
		        	if (!empty($value['cr_amount']))
		        	{
						// set form validation rules form cr_amount
			        	$this->form_validation->set_rules('Entryitem['.$key.'][cr_amount]', '', "greater_than[0]"); // Any validation you need

			        	// calculate total credit amount
						$cr_total = $this->functionscore->calculate($cr_total, $value['cr_amount'], '+');
		        	}
		        }
		    }

		   	// check if total dr or cr amount is not equal
		    if ($this->functionscore->calculate($dr_total, $cr_total, '!='))
		    {
		    	// set form validation error
        		$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_dr_cr_total_not_equal_form_validation_alert')));
			}

		}

		// check if one Bank or Cash account is present on Debit side
		if ($entrytype['restriction_bankcash'] == 2)
		{
			// check if dc_valid is [NOT] true
			if (!$dc_valid)
			{
				// set form validation error
        		$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_restriction_bankcash_2_not_valid_dc_form_validation_alert')));
			}
		}

		// check if Atleast one Bank or Cash account is present on Credit side
		if ($entrytype['restriction_bankcash'] == 3)
		{
			// check if dc_valid is [NOT] true
			if (!$dc_valid)
			{
				// set form validation error
        		$this->form_validation->set_rules('Entryitem', '', 'required', array('required' => lang('entries_cntrler_restriction_bankcash_3_not_valid_dc_form_validation_alert')));
			}
		}

		/***** Check if entry type numbering is auto ******/
		if ($entrytype['numbering'] == 1)
		{
			$entrada = "A";
			/* check if $_POST['number'] is empty */
			if (empty($this->input->post('number')))
			{
				$entrada = "A.1";
				// set entry number to next entry number
				$number = $this->entry_model->nextNumber($entrytype['id']);
			}else // if not empty
			{

				$entrada = "A.2";
				// set entry number to $_POST['number']
				$number = $this->input->post('number');
			}
		}else if ($entrytype['numbering'] == 2) // Check if entry type numbering is manual and required
		{
			$entrada = "B";
			/* Manual + Required - Check if $_POST['number'] is empty */
			if (empty($this->input->post('number')))
			{
				$entrada = "B.1";
				// set form validation rule with error
        		$this->form_validation->set_rules('number', '', 'required', array('required' => lang('entries_cntrler_entry_number_required_form_validation_alert')));
			}else // if not empty
			{
				$entrada = "B.2";
				// set entry number to $_POST['number']
				$number = $this->input->post('number');
			}
		}else// if entry type numbering is manual and not required
		{
			$entrada = "C";
			/* Manual + Optional - set entry number to $_POST['number'] */
			$number = $this->input->post('number');
		}

		// exit($entrada." ".$number);

		// check if form is NOT Validated
		if ($this->form_validation->run() == FALSE)
		{

			// pass page title to view page
			$this->data['title'] = sprintf(lang('entries_cntrler_edit_title'), $entrytype['name']);
			$this->data['entrytype'] = $entrytype; // pass entrytype array to view page
			// pass tag_options array to view page
			$this->data['tag_options'] = $this->DB1->select('id, title')->get('tags'.$this->DB1->dbsuffix)->result_array();

			/* Ledger selection */
			// $ledgers = new LedgerTree(); // initilize ledgers array - LedgerTree Lib
			// $ledgers->Group = &$this->Group; // initilize selected ledger groups in ledgers array
			// $ledgers->Ledger = &$this->Ledger; // initilize selected ledgers in ledgers array
			// $ledgers->current_id = -1; // initilize current group id
			// // set restriction_bankcash from entrytype
			// $ledgers->restriction_bankcash = $entrytype['restriction_bankcash'];
			// $ledgers->build(0); // set ledger id to [NULL] and ledger name to [None]
			// $ledgers->toList($ledgers, -1);	// create a list of ledgers array
			// $this->data['ledger_options'] = $ledgers->ledgerList; // pass ledger list to view page

			// if ($this->mSettings->entry_form) {
			// 	$this->DB1->where('id', $id);
			// 	$this->data['ledger'] = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
			// }
			/* Check for valid entry id */
			if (!$entrytypeLabel)
			{
				// set error alert
				$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_specified_error'));
				// redirect to index page
				redirect('entries/index');
			}

			// select data from entries table where id equals $id(passed id to edit function) and create array
			$entry = $this->DB1
							->select('entries'.$this->DB1->dbsuffix.'.*, companies.name as company_name')
							->join('companies', 'companies.id = entries'.$this->DB1->dbsuffix.'.companies_id')
							->where('entries'.$this->DB1->dbsuffix.'.id', $id)
							->get('entries'.$this->DB1->dbsuffix)
							->row_array();


			$date = $this->DB1->where(array('entrytype_id' => $entrytype['id'], 'id !=' => $id))->order_by('number', 'desc')->limit(2)->get('entries'.$this->DB1->dbsuffix);


			if ($date->num_rows() > 0) {
				$entriesDate = $date->result_array();
				$banderaEntrySiguiente = 0;

				foreach ($entriesDate as $row => $entrydate) {

					if ($entrydate['number'] > $entry['number']) {
						$endDateLimit = $entrydate['date'];
					} else {
						$startDateLimit = $entrydate['date'];
					}
				}
			}

			if (!isset($startDateLimit)) {
				$startDateLimit = $this->mAccountSettings->fy_start;
			}

			if (!isset($endDateLimit)) {
				$endDateLimit = $this->mAccountSettings->fy_end;
			}

			$this->data['startDateLimit'] = $startDateLimit;
			$this->data['endDateLimit'] = $endDateLimit;

			$fecha1 = new DateTime($startDateLimit);
			$fecha2 = new DateTime($endDateLimit);

			$interval = $fecha1->diff($fecha2);
			$diasdiff = $interval->format('%d');
			$mesdiff = $interval->format('%m');
			$anosdiff = $interval->format('%y');

			// var_dump($this->data['startDateLimit']);
			// var_dump($this->data['endDateLimit']);
			// var_dump($anosdiff);

			if ($diasdiff < 1 || $anosdiff > 0) {
				$this->data['edit_entry_date'] = FALSE;
			}

			// var_dump($this->data['limit_date']);
			// var_dump($this->functionscore->dateToSql($entry['date']));

			// if ($this->data['limit_date'] > $this->functionscore->dateToSql($entry['date'])) {
			// 	$this->data['edit_entry_date'] = FALSE;
			// }

			// if no entries found
			if (!$entry)
			{
				// set error alert
				$this->session->set_flashdata('error', lang('entries_cntrler_entry_not_found_error'));
				// redirect to index page
				redirect('entries/index');
			}

			//verificamos si la nota NO es de origen de Contabilidad.
			if ($entry['origin'] != 1) {
				// set error alert
				$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_no_from_conta'));
				// redirect to index page
				redirect('entries/index');
			}

			/* Check if input method is post */
			if ($this->input->method() == 'post')
			{
				// initilize current entry items array
				$curEntryitems = array();

				// loop to save post data to current entry items array
				foreach ($this->input->post('Entryitem') as $row => $entryitem)
				{
					if($this->mSettings->entry_form){
						$curEntryitems[$row] = array
						(
							'dc' => $entryitem['dc'],
							'ledger_id' => $entryitem['ledger_id'],
							// if dr_amount isset save it else save empty string
							'dr_amount' => isset($entryitem['dr_amount']) ? $entryitem['dr_amount'] : '',
							 // if cr_amount isset save it else save empty string
							'cr_amount' => isset($entryitem['cr_amount']) ? $entryitem['cr_amount'] : '',
							'narration' => $entryitem['narration'],
							'ledger_balance' => $entryitem['ledger_balance'],
							'ledgername' => $this->ledger_model->getName($entryitem['ledger_id'])
						);
					}else{
						$curEntryitems[$row] = array
						(
							'dc' => $entryitem['dc'],
							'ledger_id' => $entryitem['ledger_id'],
							// if dr_amount isset save it else save empty string
							'dr_amount' => isset($entryitem['dr_amount']) ? $entryitem['dr_amount'] : '',
							// if cr_amount isset save it else save empty string
							'cr_amount' => isset($entryitem['cr_amount']) ? $entryitem['cr_amount'] : '',
							'narration' => $entryitem['narration']
						);
					}


				}
				// pass current entry items array to view page
				$this->data['curEntryitems'] = $curEntryitems;
			}else // if method is [NOT] post
			{
				$curEntryitems = array(); // initilize current entry items array

				// get entry items where entry_id equals $id(passed id to edit function) and store to [curEntryitemsData] array
				$curEntryitemsData = $this->DB1->select('entryitems'.$this->DB1->dbsuffix.'.*, companies.name as company_name')
										->join('companies', 'companies.id = entryitems'.$this->DB1->dbsuffix.'.companies_id')
										->where('entry_id', $id)
										->get('entryitems'.$this->DB1->dbsuffix)->result_array();
				// loop for storing current entry items in current entry items array
				foreach ($curEntryitemsData as $row => $data)
				{
					if($this->mSettings->entry_form){
						$ledger_balance = $this->curLedgerBalance($data['ledger_id']);

						// if entry item is debit
						if ($data['dc'] == 'D')
						{
							$curEntryitems[$row] = array
							(
								'dc' => $data['dc'],
								'ledger_id' => $data['ledger_id'],
								'dr_amount' => $data['amount'],
								'cr_amount' => '',
								'narration' => $data['narration'],
								'ledgername' => $this->ledger_model->getName($data['ledger_id']),
								'ledger_balance' => $ledger_balance,
								'base' => $data['base'],
								'companies_id' => $data['companies_id'],
								'company_name' => $data['company_name'],
								'cost_center_id' => $data['cost_center_id'],

							);
						}else // if entry item is credit
						{
							$curEntryitems[$row] = array
							(
								'dc' => $data['dc'],
								'ledger_id' => $data['ledger_id'],
								'dr_amount' => '',
								'cr_amount' => $data['amount'],
								'narration' => $data['narration'],
								'ledgername' => $this->ledger_model->getName($data['ledger_id']),
								'ledger_balance' => $ledger_balance,
								'base' => $data['base'],
								'companies_id' => $data['companies_id'],
								'company_name' => $data['company_name'],
								'cost_center_id' => $data['cost_center_id'],
							);
						}
					}else{
						// if entry item is debit
						if ($data['dc'] == 'D')
						{
							$curEntryitems[$row] = array
							(
								'dc' => $data['dc'],
								'ledger_id' => $data['ledger_id'],
								'dr_amount' => $data['amount'],
								'cr_amount' => '',
								'narration' => $data['narration'],
								'base' => $data['base'],
								'companies_id' => $data['companies_id'],
								'company_name' => $data['company_name'],
								'cost_center_id' => $data['cost_center_id'],
							);
						}else // if entry item is credit
						{
							$curEntryitems[$row] = array
							(
								'dc' => $data['dc'],
								'ledger_id' => $data['ledger_id'],
								'dr_amount' => '',
								'cr_amount' => $data['amount'],
								'narration' => $data['narration'],
								'base' => $data['base'],
								'companies_id' => $data['companies_id'],
								'company_name' => $data['company_name'],
								'cost_center_id' => $data['cost_center_id'],
							);
						}
					}
				}

				// pass current entry items array to view page
				$this->data['curEntryitems'] = $curEntryitems;
				$ledgers = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix)->result_array();
				$ledger_options = [];

				foreach ($ledgers as $ledger) {
					$ledger_options[$ledger['id']] = $ledger['code']." - ".ucfirst(mb_strtolower($ledger['name']));
				}

				$this->data['ledger_options'] = $ledger_options;


			}

			/***** store entry date after converting from sql format(dateFromSql function) *****/
			$entry['date'] = $this->functionscore->dateFromSql($entry['date']);
			// pass entry array to view page
			$this->data['entry'] = $entry;

			if ($this->mAccountSettings->cost_center) {
				$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
				if ($cost_centers_data->num_rows() > 0) {
					$cost_centers_data = $cost_centers_data->result_array();
					foreach ($cost_centers_data as $row => $cost_center) {
						$cost_centers[] = $cost_center;
					}
				} else {
					$cost_centers = false;
				}

				$this->data['cost_centers'] = $cost_centers;
			}


			// echo "<pre>";
			// print_r($this->data);
			// echo "</pre>";
			// die();

			// render page
			if ($this->mSettings->entry_form) {
				$this->render('entries/edit2');
			}else{
				$this->render('entries/edit');
			}
		}else // if form is Validated
		{
			/* Check if acccount is locked */
			if ($this->mAccountSettings->account_locked == 1)
			{
				// set error alert
				$this->session->set_flashdata('error', lang('entries_cntrler_edit_account_locked_error'));
				// redirect to index page
				redirect('entries');
			}

			/***************************************************************************/
			/*********************************** ENTRY *********************************/
			/***************************************************************************/

			$entrydata = null; // entry data array to insert into entries table - DB1

			/***** Entry number ******/
			$entrydata['Entry']['number'] = $number;

			/***** Entry id ******/
			$entrydata['Entry']['id'] = $id;

			/****** Entrytype remains the same *****/
			$entrydata['Entry']['entrytype_id'] = $entrytype['id'];

			/****** Check tag ******/
			if (empty($this->input->post('tag_id')))
			{
				// null if empty
				$entrydata['Entry']['tag_id'] = null;
			} else
			{
				// else $_POST['tag_id']
				$entrydata['Entry']['tag_id'] = $this->input->post('tag_id');
			}

			/***** Notes *****/
			$entrydata['Entry']['notes'] = $this->input->post('notes');

			/***** Date after converting to sql format *****/
			$entrydata['Entry']['date'] = $this->functionscore->dateToSql($this->input->post('date'));
			$entrydata['Entry']['companies_id'] = $this->input->post('company_id');


			/***************************************************************************/
			/***************************** ENTRY ITEMS *********************************/
			/***************************************************************************/


			$entrydata['Entry']['dr_total'] = $dr_total; // total debit amount
			$entrydata['Entry']['cr_total'] = $cr_total; // total credit amount

			/* Add item to entryitemdata array if everything is ok */
			$entryitemdata = array();

			// loop for entry items array according to debit or credit
			foreach ($this->input->post('Entryitem') as $row => $entryitem)
			{
				// check if $entryitem['ledger_id'] less then or equal to 0
				if ($entryitem['ledger_id'] <= 0)
				{
					// continue to next entryitem
					continue;
				}

				// if entry item is debit
				if ($entryitem['dc'] == 'D')
				{
					$entryitemdata[] = array
					(
						'Entryitem' => array(
							'dc' => $entryitem['dc'],
							'ledger_id' => $entryitem['ledger_id'],
							'amount' => $entryitem['dr_amount'],
							'narration' => $entryitem['narration'],
							'base' => $entryitem['base'],
							'companies_id' => $entryitem['companies_id'],
							'cost_center_id' => isset($entryitem['cost_center_id']) ? $entryitem['cost_center_id'] : NULL,

						)
					);
				} else // if entry item is credit
				{
					$entryitemdata[] = array
					(
						'Entryitem' => array(
							'dc' => $entryitem['dc'],
							'ledger_id' => $entryitem['ledger_id'],
							'amount' => $entryitem['cr_amount'],
							'narration' => $entryitem['narration'],
							'base' => $entryitem['base'],
							'companies_id' => $entryitem['companies_id'],
							'cost_center_id' => isset($entryitem['cost_center_id']) ? $entryitem['cost_center_id'] : NULL,
						)
					);
				}
			}

			// select where id from [entries] table equals passed id
			$this->DB1->where('id', $id);
			// update entries table
			// exit(var_dump($entrydata['Entry']));
			$update = $this->DB1->update('entries'.$this->DB1->dbsuffix, $entrydata['Entry']);

			// if update successfull
			if ($update)
			{
			   	/* Delete all original entryitems */
				$this->DB1->where('entry_id', $id); // select all entry items where entry_id equals passed id
				$this->DB1->delete('entryitems'.$this->DB1->dbsuffix); // delete selected entry items

				// loop to insert entry item data to entryitems table
				foreach ($entryitemdata as $row => $itemdata)
				{
					$itemdata['Entryitem']['entry_id'] = $id; // entry_id equals passed id
					$this->DB1->insert('entryitems'.$this->DB1->dbsuffix ,$itemdata['Entryitem']); // insert data to entryitems table
				}

				// set entry number as per prefix, suffix and zero padding for that entry type for logging
				$entryNumber = ($this->functionscore->toEntryNumber($entrydata['Entry']['number'], $entrydata['Entry']['entrytype_id']));

				// insert log if logging is enabled
				$this->settings_model->add_log(sprintf(lang('entries_cntrler_edit_log'),$entrytype['name'], $entryNumber), 1);

				// set success alert message
				$this->session->set_flashdata('message', sprintf(lang('entries_cntrler_edit_entry_updated_successfully'), $entrytype['name'], $entryNumber));
				// redirect to index page
				redirect('entries/index');
			} else {
				// set error alert message
				$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_updated_error'));
				// redirect to index page
				redirect('entries/index');
			}
		}
	}

	private function curLedgerBalance($id)
	{
		$this->DB1->where('id', $id);
		$this->data['curledger'] = $this->DB1->get('ledgers'.$this->DB1->dbsuffix)->row_array();
		$cl = $this->ledger_model->closingBalance($id);
		$status = 'ok';
		$ledger_balance = '';
		if ($this->data['curledger']['type'] == 1) {
			if ($cl['dc'] == 'C') {
				$status = 'neg';
			}
		}

		/* Return closing balance */
		$cl = array('cl' =>
				array(
					'dc' => $cl['dc'],
					'amount' => $cl['amount'],
					'status' => $status,
				)
		);

		$ledger_bal = $cl['cl']['amount'];
		$prefix = '';
		$suffix = '';
		if ($cl['cl']['status'] == 'neg') {
			$this->data['prefix'] = '<span class="error-text">';
			$this->data['suffix'] = '</span>';
		}
		if ($cl['cl']['dc'] == 'D') {
			$ledger_balance = "Dr " . $ledger_bal;
		} else if ($cl['cl']['dc'] == 'C') {
			$ledger_balance = "Cr " . $ledger_bal;
		} else {
			$ledger_balance = '-';
		}
		return $ledger_balance;
	}


	/**
	* delete method
	*
	* @throws MethodNotAllowedException
	* @param string $entrytypeLabel
	* @param string $id
	* @return void
	*/
	public function delete($entrytypeLabel = null, $id = null)
	{

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

		/* Check for valid entry type */
		if (empty($entrytypeLabel))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_specified_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// select entry type where label equals $entrytypeLabel and store to array
		$entrytype = $this->DB1->where('label',$entrytypeLabel)->get('entrytypes'.$this->DB1->dbsuffix)->row_array();

		// if entry type [NOT] found
		if (!$entrytype)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}

		/* Check if valid id */
		if (empty($id))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries');
		}

		// select entry where id equals $id and store to array
		$entry = $this->DB1->where('id',$id)->get('entries'.$this->DB1->dbsuffix)->row_array();

		/* if entry [NOT] found */
		if (!$entry)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries');
		}

		if ($entry['origin'] == 1) {
			/* Delete entry items */
			// $this->DB1->delete('entryitems'.$this->DB1->dbsuffix, array('entry_id' => $id));
			$data = array(
               'amount' => '0.00'
            );

			$this->DB1->where('entry_id', $id);
			$this->DB1->update('entryitems'.$this->DB1->dbsuffix, $data);

			$data = array(
               'notes' => '[Anulada]  '.$entry['notes'],
               'state' => '0',
               'dr_total' => '0',
               'cr_total' => '0'
            );

			$this->DB1->where('id', $id);
			$this->DB1->update('entries'.$this->DB1->dbsuffix, $data);

			/* Delete entry */
			// $this->DB1->delete('entries'.$this->DB1->dbsuffix, array('id' => $id));

			// set entry number as per prefix, suffix and zero padding for that entry type for logging
			$entryNumber = ($this->functionscore->toEntryNumber($entry['number'], $entrytype['id']));

			// set success alert
			$this->session->set_flashdata('message', sprintf(lang('entries_cntrler_delete_entry_deleted_successfully'), $entrytype['name'], $entryNumber));

			// insert log if logging is enabled
			$this->settings_model->add_log(sprintf(lang('entries_cntrler_delete_log'),$entrytype['name'], $entryNumber), 1);
			// redirect to index page
			redirect('entries/index');
		} else {
			$this->session->set_flashdata('error', lang('entries_cntrler_delete_entry_no_from_conta'));
			// redirect to index page
			redirect('entries');
		}

	}

	public function approve($entrytypeLabel = null, $id = null)
	{

		if ($this->mAccountSettings->account_locked == 1) {
			$this->session->set_flashdata('warning', lang('account_settings_cntrler_main_account_locked_warning'));
			redirect('dashboard');
		}

		/* Check for valid entry type */
		if (empty($entrytypeLabel))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_specified_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// select entry type where label equals $entrytypeLabel and store to array
		$entrytype = $this->DB1->where('label',$entrytypeLabel)->get('entrytypes'.$this->DB1->dbsuffix)->row_array();

		// if entry type [NOT] found
		if (!$entrytype)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}

		/* Check if valid id */
		if (empty($id))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries');
		}

		// select entry where id equals $id and store to array
		$entry = $this->DB1->where('id',$id)->get('entries'.$this->DB1->dbsuffix)->row_array();

		/* if entry [NOT] found */
		if (!$entry)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries');
		}

		if ($entry['origin'] == 1) {

			$data = array(
               'state' => '1'
            );

			$this->DB1->where('id', $id);
			$this->DB1->update('entries'.$this->DB1->dbsuffix, $data);

			$entryNumber = ($this->functionscore->toEntryNumber($entry['number'], $entrytype['id']));

			// set success alert
			$this->session->set_flashdata('message', sprintf(lang('entries_cntrler_delete_entry_approved_successfully'), $entrytype['name'], $entryNumber));

			// insert log if logging is enabled
			$this->settings_model->add_log(sprintf(lang('entries_cntrler_delete_log'),$entrytype['name'], $entryNumber), 1);
			// redirect to index page
			redirect('entries/index');
		} else {
			$this->session->set_flashdata('error', lang('entries_cntrler_approve_entry_no_from_conta'));
			// redirect to index page
			redirect('entries');
		}

	}

	/**
	* view method
	*
	* @param string $entrytypeLabel
	* @param string $id
	* @return void
	*/
	public function view($entrytypeLabel = null, $id = null) {

		/* Check for valid entry type */
		if (empty($entrytypeLabel))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_specified_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// select entry type where label equals $entrytypeLabel and store to array
		$entrytype = $this->DB1
					->where('label',$entrytypeLabel)
					->get('entrytypes'.$this->DB1->dbsuffix)->row_array();

		// if entry type [NOT] found
		if (!$entrytype)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}
		// pass entrytype to view page
		$this->data['entrytype'] = $entrytype;
		$this->data['entrytypeLabel'] = $entrytypeLabel;
		/* Check if valid id */
		if (empty($id))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// select entry where id equals $id and store to array
		$entry = $this->DB1
					->select('entries'.$this->DB1->dbsuffix.'.*, '.$this->DB1->dbprefix('companies').'.name as company_name')
					->join('companies', 'companies.id = entries'.$this->DB1->dbsuffix.'.companies_id')
					->where('entries'.$this->DB1->dbsuffix.'.id',$id)
					->get('entries'.$this->DB1->dbsuffix)->row_array();
		/* if entry [NOT] found */
		if (!$entry)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}
		/* Initial data */
		$curEntryitems = array(); // initilize current entry items array
		$curEntryitemsData = $this->DB1->select('entryitems'.$this->DB1->dbsuffix.'.*, '.$this->DB1->dbprefix('companies').'.name as company_name')
								->join('companies', 'companies.id = entryitems'.$this->DB1->dbsuffix.'.companies_id')
								->where('entry_id', $id)
								->get('entryitems'.$this->DB1->dbsuffix)
								->result_array();

		$cost_centers = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
		$cost_centers_id = [];
		if ($cost_centers->num_rows() > 0) {
			foreach (($cost_centers->result_array()) as $cost_center) {
				$cost_centers_id[$cost_center['id']] = $cost_center['name'];
			}
		}

		// loop to store selected entry items to current entry items array
		foreach ($curEntryitemsData as $row => $data)
		{
			// if debit entry
			if ($data['dc'] == 'D')
			{
				$curEntryitems[$row] = array
				(
					'dc' => $data['dc'],
					'ledger_id' => $data['ledger_id'],
					'ledger_name' => $this->ledger_model->getName($data['ledger_id']),
					'dr_amount' => $data['amount'],
					'cr_amount' => '',
					'narration' => $data['narration'],
					'base' => $data['base'],
					'companies_id' => $data['companies_id'],
					'company_name' => $data['company_name'],
					'cost_center_id' => isset($cost_centers_id[$data['cost_center_id']]) ? $cost_centers_id[$data['cost_center_id']] : "N/A",
				);
			}else // if credit entry
			{
				$curEntryitems[$row] = array
				(
					'dc' => $data['dc'],
					'ledger_id' => $data['ledger_id'],
					'ledger_name' => $this->ledger_model->getName($data['ledger_id']),
					'dr_amount' => '',
					'cr_amount' => $data['amount'],
					'narration' => $data['narration'],
					'base' => $data['base'],
					'companies_id' => $data['companies_id'],
					'company_name' => $data['company_name'],
					'cost_center_id' => isset($cost_centers_id[$data['cost_center_id']]) ? $cost_centers_id[$data['cost_center_id']] : "N/A",
				);
			}
		}

		$this->data['curEntryitems'] = $curEntryitems; // pass current entry items to view
		$this->data['allTags'] = $this->DB1->get('tags'.$this->DB1->dbsuffix)->result_array(); // fetch all tags and pass to view
		$this->data['entry'] = $entry; // pass entry to view

		// render page
		$this->render('entries/view');
	}

	public function group_view($entrytypeLabel = null, $id = null, $company_id = null) {
		/* Check for valid entry type */
		$id = urldecode($id);
		if (empty($entrytypeLabel))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_specified_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// select entry type where label equals $entrytypeLabel and store to array
		$entrytype = $this->DB1->where('label',$entrytypeLabel)->get('entrytypes'.$this->DB1->dbsuffix)->row_array();

		// if entry type [NOT] found
		if (!$entrytype)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// pass entrytype to view page
		$this->data['entrytype'] = $entrytype;
		$this->data['entrytypeLabel'] = $entrytypeLabel;

		/* Check if valid id */
		if (empty($id))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// select entry where id equals $id and store to array
		$entries = $this->DB1
						->where('consecutive_supplier',$id)
						->where('companies_id',$company_id)
						->get('entries'.$this->DB1->dbsuffix)
						->result_array();

		/* if entry [NOT] found */
		if (!$entries)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}
		$curEntryitems = array(); // initilize current entry items array
		$total_d = 0;
		$total_c = 0;
		$note = "";
		foreach ($entries as $entry) {
			/* Initial data */
			$this->DB1->where('entry_id', $entry['id']); // select where entry_id equals $id

			// store selected data to $curEntryitemsData
			$curEntryitemsData = $this->DB1->get('entryitems'.$this->DB1->dbsuffix)->result_array();

			$cost_centers = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
			$cost_centers_id = [];
			if ($cost_centers->num_rows() > 0) {
				foreach (($cost_centers->result_array()) as $cost_center) {
					$cost_centers_id[$cost_center['id']] = $cost_center['name'];
				}
			}
			// loop to store selected entry items to current entry items array
			foreach ($curEntryitemsData as $row => $data)
			{
				$total_d += $data['dc'] == 'D' ? $data['amount'] : 0;
				$total_c += $data['dc'] == 'C' ? $data['amount'] : 0;
				if (isset($curEntryitems[$data['ledger_id']])) {
					$curEntryitems[$data['ledger_id']]['dr_amount'] += $data['dc'] == 'D' ? $data['amount'] : 0;
					$curEntryitems[$data['ledger_id']]['cr_amount'] += $data['dc'] == 'C' ? $data['amount'] : 0;
					$curEntryitems[$data['ledger_id']]['narration'] .= $data['narration'] != $curEntryitems[$data['ledger_id']]['narration'] ? " || ".$data['narration'] : "";
				} else {
					$curEntryitems[$data['ledger_id']] = array
					(
						'dc' => $data['dc'],
						'ledger_id' => $data['ledger_id'],
						'ledger_name' => $this->ledger_model->getName($data['ledger_id']),
						'dr_amount' => $data['dc'] == 'D' ? $data['amount'] : 0,
						'cr_amount' => $data['dc'] == 'C' ? $data['amount'] : 0,
						'narration' => $data['narration'],
						'base' => $data['base'],
						'companies_id' => $data['companies_id'],
						'cost_center_id' => isset($cost_centers_id[$data['cost_center_id']]) ? $cost_centers_id[$data['cost_center_id']] : "N/A",
					);
				}
			}
			$note .= $entry['notes']." || ";
		}
		$entries[0]['notes'] = trim($note, " || ");
		$entries[0]['dr_total'] = $total_d;
		$entries[0]['cr_total'] = $total_c;
		$companies = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'companies ORDER BY group_name ASC')->result_array();
		$company_data = [];
		foreach ($companies as $company) {
			$company_data[$company['id']] = $company;
		}

		$this->data['companies'] = $company_data;

		$this->data['curEntryitems'] = $curEntryitems; // pass current entry items to view
		$this->data['allTags'] = $this->DB1->get('tags'.$this->DB1->dbsuffix)->result_array(); // fetch all tags and pass to view
		$this->data['entries'] = $entries[0]; // pass entry to view

		// render page
		$this->render('entries/group_view');
	}

	/**
	 * Add a row in the entry via ajax
	 *
	 * @param string $addType
	 * @return void
	 */
	function addrow($restriction_bankcash, $third) {

		// $this->layout = null;

		/* Ledger selection */
		// $ledgers = new LedgerTree(); // initilize ledgers array - LedgerTree Lib
		// $ledgers->Group = &$this->Group; // initilize selected ledger groups in ledgers array
		// $ledgers->Ledger = &$this->Ledger; // initilize selected ledgers in ledgers array
		// $ledgers->current_id = -1; // initilize current group id
		// // set restriction_bankcash from entrytype
		// $ledgers->restriction_bankcash = $restriction_bankcash;
		// $ledgers->build(0); // set ledger id to [NULL] and ledger name to [None]
		// $ledgers->toList($ledgers, -1); // create a list of ledgers array
		// $data['ledger_options'] = $ledgers->ledgerList; // pass ledger list to view
		// $data['companies'] = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'companies ORDER BY group_name ASC')->result_array();
		$data['third'] = $third;

		$cost_centers_data = $this->DB1->get('cost_centers'.$this->DB1->dbsuffix);
		if ($cost_centers_data->num_rows() > 0) {
			$cost_centers_data = $cost_centers_data->result_array();
			foreach ($cost_centers_data as $row => $cost_center) {
				$cost_centers[] = $cost_center;
			}
		} else {
			$cost_centers = false;
		}

		$data['cost_centers'] = $cost_centers;

		$this->load->view('entries/addrow', $data); // load view
	}

	function addrow2() {
		$restriction_bankcash = $_POST['restriction_bankcash'];
		$third = $_POST['third'];
		// $this->layout = null;
		/* Ledger selection */
		$ledgers = new LedgerTree(); // initilize ledgers array - LedgerTree Lib
		$ledgers->Group = &$this->Group; // initilize selected ledger groups in ledgers array
		$ledgers->Ledger = &$this->Ledger; // initilize selected ledgers in ledgers array
		$ledgers->current_id = -1; // initilize current group id
		// set restriction_bankcash from entrytype
		$ledgers->restriction_bankcash = $restriction_bankcash;
		$ledgers->build(0); // set ledger id to [NULL] and ledger name to [None]
		$ledgers->toList($ledgers, -1); // create a list of ledgers array
		$data['ledger_options'] = $ledgers->ledgerList; // pass ledger list to view
		$data['companies'] = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'companies ORDER BY group_name ASC')->result_array();
		$data['third'] = $third;

		$eItems = json_decode($_POST['entryItems']);

		$entryItems = [];

		foreach ($eItems as $row => $row2) {
			foreach ($row2 as $id => $value) {
				foreach ($value as $key => $value2) {
					$entryItems[$row][$key] = $value2;
				}
			}
		}

		$data['entryitems'] = $entryItems;

		$this->load->view('entries/addrow', $data); // load view
	}

	/**
	 * Add a row in the entry via ajax
	 *
	 * @param string $addType
	 * @return void
	 */
	function addentry() {
		if (isset($_POST) && !empty($_POST)) {
			$data['entryitem'] = $_POST;
			$this->load->view('entries/addentry', $data); // load view
		}else{
			return FALSE;
		}
	}

	public function export($entrytypeLabel, $id, $type='xls')
	{

		if (!is_file($_SERVER['DOCUMENT_ROOT'] ."/contabilidad/assets/uploads/companies/". $this->mAccountSettings->logo)) {
			$this->session->set_flashdata('error', lang('invalid_company_logo'));
			redirect('account_settings/main');
		}

		/* Check for valid entry type */
		if (empty($entrytypeLabel))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_specified_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// select entry type where label equals $entrytypeLabel and store to array
		$entrytype = $this->DB1->where('label',$entrytypeLabel)->get('entrytypes'.$this->DB1->dbsuffix)->row_array();

		// if entry type [NOT] found
		if (!$entrytype)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_entrytype_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// pass entrytype to view page
		$this->data['entrytype'] = $entrytype;

		/* Check if valid id */
		if (empty($id))
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}

		// select entry where id equals $id and store to array
		$entry = $this->DB1->where('id',$id)->get('entries'.$this->DB1->dbsuffix)->row_array();

		/* if entry [NOT] found */
		if (!$entry)
		{
			// set error alert
			$this->session->set_flashdata('error', lang('entries_cntrler_edit_entry_not_found_error'));
			// redirect to index page
			redirect('entries/index');
		}


		/* Initial data */
		$curEntryitems = array(); // initilize current entry items array
		$this->DB1->where('entry_id', $id); // select where entry_id equals $id

		// store selected data to $curEntryitemsData
		$curEntryitemsData = $this->DB1->get('entryitems'.$this->DB1->dbsuffix)->result_array();

		// loop to store selected entry items to current entry items array
		foreach ($curEntryitemsData as $row => $data)
		{


			$q = $this->DB1->where(array('id' => $data['companies_id']))->get('companies');
			$thirdItem = $q->row_array();

			// if debit entry
			if ($data['dc'] == 'D')
			{
				$curEntryitems[$row] = array
				(
					'dc' => $data['dc'],
					'ledger_id' => $data['ledger_id'],
					'ledger_name' => $this->ledger_model->getName($data['ledger_id']),
					'dr_amount' => $data['amount'],
					'cr_amount' => '',
					'base' => $data['base'],
					'third_name' => $thirdItem['name'],
					'third_nit' => $thirdItem['vat_no'],
					'narration' => $data['narration']
				);
			}else // if credit entry
			{
				$curEntryitems[$row] = array
				(
					'dc' => $data['dc'],
					'ledger_id' => $data['ledger_id'],
					'ledger_name' => $this->ledger_model->getName($data['ledger_id']),
					'dr_amount' => '',
					'cr_amount' => $data['amount'],
					'base' => $data['base'],
					'third_name' => $thirdItem['name'],
					'third_nit' => $thirdItem['vat_no'],
					'narration' => $data['narration']

				);
			}
		}


        if (!empty($data)) {

            if ($type=='pdf') {
            	$q = $this->DB1->where(array('id' => $entry['companies_id']))->get('companies');
            	$third = $q->row_array();

            	$entry['label'] = $entrytypeLabel;

            	$entry['third_name'] = $third['name'];
            	$entry['third_nit'] = $third['vat_no'];

            	$this->load->view('entries/entry_pdf.php', array('datos' => $entry, 'entrytype' => $entrytype,  'entryitems' => $curEntryitems, 'settings' => $this->mAccountSettings));

            }

            if ($type=='xls') {

            	$this->load->library('excel');
	            $this->excel->setActiveSheetIndex(0);
	            if ($type=='pdf') {
	                $styleArray = array(
	                    'borders' => array(
	                        'allborders' => array(
	                            'style' => PHPExcel_Style_Border::BORDER_THIN
	                        )
	                    )
	                );
	                $this->excel->getDefaultStyle()->applyFromArray($styleArray);
	            }
				if ($this->mSettings->drcr_toby == 'toby') {
					$drcr_toby = lang('entries_views_views_th_to_by');
				} else {
					$drcr_toby = lang('entries_views_views_th_dr_cr');
				}
	            $this->excel->getActiveSheet()->setTitle(ucfirst($entrytypeLabel).lang('entry_title')."  #".$entry['number']);

	            $this->excel->getActiveSheet()->SetCellValue('A1', ucfirst($entrytypeLabel).lang('entry_title')."  #".$entry['number']);
	            $this->excel->getActiveSheet()->mergeCells('A1:E1');

	            $this->excel->getActiveSheet()->SetCellValue('A2', lang('entries_views_add_label_date').": ".$entry['date']);
	            $this->excel->getActiveSheet()->mergeCells('A2:E2');


	            $this->excel->getActiveSheet()->SetCellValue('A3', $drcr_toby);
	            $this->excel->getActiveSheet()->SetCellValue('B3', lang('entries_views_views_th_ledger'));
	            $this->excel->getActiveSheet()->SetCellValue('C3', lang('entries_views_views_th_dr_amount'));
	            $this->excel->getActiveSheet()->SetCellValue('D3', lang('entries_views_views_th_cr_amount'));
	            $this->excel->getActiveSheet()->SetCellValue('E3', lang('entries_views_views_th_narration') );

	            $row = 4;
	            $ttotal = 0;
	            $ttotal_tax = 0;
	            $tgrand_total = 0;
	            foreach ($curEntryitems as $entryitem) {
	                $ir = $row + 1;
	                if ($ir % 2 == 0) {
	                    $style_header = array(
	                        'fill' => array(
	                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                            'color' => array('rgb'=>'CCCCCC'),
	                        ),
	                    );
	                    $this->excel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray( $style_header );
	                }

	                if ($this->mSettings->drcr_toby == 'toby') {
						if ($entryitem['dc'] == 'D') {
							$dr_cr_rows = lang('entries_views_views_toby_D');
						} else {
							$dr_cr_rows = lang('entries_views_views_toby_C');
						}
					} else {
						if ($entryitem['dc'] == 'D') {
							$dr_cr_rows = lang('entries_views_views_drcr_D');
						} else {
							$dr_cr_rows = lang('entries_views_views_drcr_C');
						}
					}



	                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $dr_cr_rows);
	                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $entryitem['ledger_name']);
	                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $entryitem['dc'] == 'D' ? $entryitem['dr_amount'] : '');
	                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $entryitem['dc'] == 'C' ? $entryitem['cr_amount'] : '');
	                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $entryitem['narration']);
	                $row++;
	            }
	            $style_header = array(
	                'fill' => array(
	                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                    'color' => array('rgb'=>'fdbf2d'),
	                ),
	            );


	            $this->excel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray( $style_header );

	            $this->excel->getActiveSheet()->SetCellValue("A$row", lang('entries_views_views_td_total'));
	            $this->excel->getActiveSheet()->mergeCells("A$row:B$row");
	            $this->excel->getActiveSheet()->SetCellValue("C$row", $this->functionscore->toCurrency('D', $entry['dr_total']));
	            $this->excel->getActiveSheet()->SetCellValue("D$row", $this->functionscore->toCurrency('C', $entry['cr_total']));


	            if ($this->functionscore->calculate($entry['dr_total'], $entry['cr_total'], '==')) {
					/* Do nothing */
				} else {
					if ($this->functionscore->calculate($entry['dr_total'], $entry['cr_total'], '>')) {
						$this->excel->getActiveSheet()->SetCellValue("A$row", lang('entries_views_views_td_diff'));
			            $this->excel->getActiveSheet()->mergeCells("A$row:B$row");
			            $this->excel->getActiveSheet()->SetCellValue("C$row",  $this->functionscore->toCurrency('D', $this->functionscore->calculate($entry['dr_total'], $entry['cr_total'], '-')));
					} else {
						$this->excel->getActiveSheet()->SetCellValue("A$row", lang('entries_views_views_td_diff'));
			            $this->excel->getActiveSheet()->mergeCells("A$row:C$row");
			            $this->excel->getActiveSheet()->SetCellValue("D$row", $this->functionscore->toCurrency('C', $this->functionscore->calculate($entry['cr_total'], $entry['dr_total'], '-')));
					}
				}

	            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
	            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
	            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(60);

	            $filename = 'entry_print';
	            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


	            $this->excel->getActiveSheet()->getStyle('C2:C' . ($row))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
	            $this->excel->getActiveSheet()->getStyle('D2:D' . ($row))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

	            $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

	            $header = 'A1:E1';
	            $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('94ce58');
	            $style = array(
	                'font' => array('bold' => true,),
	                'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
	            );
	            $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);

	            $header = 'A2:E2';
	            $this->excel->getActiveSheet()->getStyle($header)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fdbf2d');
	            $style = array(
	                'font' => array('bold' => true,),
	                'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
	            );
	            $this->excel->getActiveSheet()->getStyle($header)->applyFromArray($style);


                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                $objWriter->save('php://output');
                exit();
            }
        }
	}

	// public function companiesoptions(){

	// 	$group_names_spanish = array('biller' => 'Facturador', 'customer' => 'Cliente', 'Partner' => 'Cliente', 'supplier' => 'Proveedor', 'creditor' => 'Acreedor');

	// 	$companies = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'companies ORDER BY group_name ASC')->result_array();

	// 	$options = "<option value=''>".lang('select')."</option>";

	// 	foreach ($companies as $company) {
	// 		$options.="<option value='".$company['id']."'>"
	// 			.'('.(isset($group_names_spanish[$company['group_name']]) ? $group_names_spanish[$company['group_name']] : "No registra").') '.$company['name'].' - '.$company['vat_no'].
	// 			"</option>";
	// 	}

	// 	echo $options;

	// }

	function companiesoptions($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term');
            $term = $term['term'];
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit');

        $group_names_spanish = array('biller' => 'Facturador', 'customer' => 'Cliente', 'Partner' => 'Cliente', 'supplier' => 'Proveedor', 'creditor' => 'Acreedor', 'employee' => 'Empleado');

		$result = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'companies WHERE (name LIKE "'.$term.'%" OR company LIKE "'.$term.'%" OR name LIKE "%'.$term.'%" OR company LIKE "%'.$term.'%" OR vat_no LIKE "%'.$term.'%" ) ORDER BY group_name ASC LIMIT 15')->result('object');

		$datos = [];

		$cnt = 0;
		foreach ($result as $row) {
			$datos[$cnt]['id'] = $row->id;
			$datos[$cnt]['text'] = "(".(isset($group_names_spanish[$row->group_name]) ? $group_names_spanish[$row->group_name] : $row->group_name).") ".$row->company .($row->name != $row->company ? " - ".$row->name : "");
			$cnt++;
		}

        if ($a) {
            $this->sma->send_json($datos);
        }
        $rows['results'] = $datos;

        exit(json_encode($rows));
    }

	function ledgersoptions($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term');
            $term = $term['term'];
        }
        // exit(var_dump($term));
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit');

		$result = $this->DB1->query('SELECT * FROM '.$this->DB1->dbprefix.'ledgers'.$this->DB1->dbsuffix.' WHERE code LIKE "'.$term.'%" or name LIKE "'.$term.'%" ORDER BY code ASC LIMIT 25')->result('object');

		$datos = [];

		$cnt = 0;
		foreach ($result as $row) {
			$datos[$cnt]['id'] = $row->id;
			$datos[$cnt]['text'] = $row->code." - ".ucfirst(mb_strtolower($row->name));
			$cnt++;
		}

        if ($a) {
            $this->sma->send_json($datos);
        }
        $rows['results'] = $datos;

        exit(json_encode($rows));
    }

}


