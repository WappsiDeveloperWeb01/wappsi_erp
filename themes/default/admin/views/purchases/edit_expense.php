<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_expense'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("purchases/edit_expense/" . $expense->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <?php if ($Owner || $Admin) { ?>

                <div class="form-group">
                    <?= lang("date", "date"); ?>
                    <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : $expense->date), 'class="form-control datetime" id="date" readonly'); ?>
                </div>
            <?php } ?>

            <div class="form-group">
                <?= lang("reference", "reference"); ?>
                <?= form_input('reference', (isset($_POST['reference']) ? $_POST['reference'] : $expense->reference), 'class="form-control tip" id="reference" readonly'); ?>
            </div>

            <div class="form-group">
                <?= lang('category', 'category'); ?>
                <select name="category" id="category" class="form-control tip" required>
                    <option value=""><?= lang('select') ?></option>
                    <?php foreach ($categories as $category): ?>
                        <option value="<?= $category->id ?>" data-tax1="<?= $category->tax_rate_id ?>" data-tax2="<?= $category->tax_rate_2_id ?>" <?= $expense->category_id == $category->id ? 'selected="selected"' : ''  ?>><?= $category->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <div class="form-group">
                <?= lang("biller", "biller"); ?>
                <?php
                $bl[""] = "";
                foreach ($billers as $biller) {
                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                }
                echo form_dropdown('biller', $bl, $expense->biller_id, 'id="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                ?>
            </div>

            <?php if (isset($cost_centers)): ?>
                <div class="form-group">
                    <?= lang('cost_center', 'cost_center_id') ?>
                    <?php
                    $ccopts[''] = lang('select');
                    foreach ($cost_centers as $cost_center) {
                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                    }
                     ?>
                     <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                </div>
            <?php endif ?>

            <div class="form-group">
                <?= lang("value", "amount"); ?>
                <input name="amount" type="text" id="amount" value="<?= $this->sma->formatDecimal($this->sma->remove_tax_from_amount($expense->tax_rate_id, $expense->amount)); ?>"
                       class="pa form-control kb-pad amount" required="required"/>
            </div>

            <div class="form-group">
                <label><?= lang('tax_1') ?></label>
                <div class="row">
                    <div class="col-xs-6">
                        <select name="exp_ptax" id="exp_ptax" class="form-control exp_ptax">
                            <option value=""><?= lang('n/a') ?></option>
                            <?php foreach ($tax_rates as $tax): ?>
                                <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <?= form_input('exp_ptax_value', '', 'id="exp_ptax_value" class="form-control"'); ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label><?= lang('tax_2') ?></label>
                <div class="row">
                    <div class="col-xs-6">
                        <select name="exp_ptax_2" id="exp_ptax_2" class="form-control exp_ptax_2">
                            <option value=""><?= lang('n/a') ?></option>
                            <?php foreach ($tax_rates as $tax): ?>
                                <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <?= form_input('exp_ptax_2_value', '', 'id="exp_ptax_2_value" class="form-control"'); ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <table class="table table-bordered">
                    <tr>
                        <th style="width:16.66%;"><?= lang('expense_cost'); ?></th>
                        <th style="width:16.66%;"><span id="net_cost"></span></th>
                        <th style="width:16.66%;"><?= lang('expense_tax'); ?></th>
                        <th style="width:16.66%;"><span id="pro_tax"></span></th>
                        <th style="width:16.66%;"><?= lang('total'); ?></th>
                        <th style="width:16.66%;"><span id="pro_total"></span></th>
                    </tr>
                </table>
            </div>

            <div class="form-group">
                <label onclick="showRetention()" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención</label>
                <input type="text" name="retencion" id="rete" class="form-control text-right" readonly>
            </div>
            <div class="section-retentions" style="display: none;">
                  <div class="row">
                    <div class="col-sm-2">
                      <label>Retención</label>
                    </div>
                    <div class="col-sm-5">
                      <label>Opción</label>
                    </div>
                    <div class="col-sm-2">
                      <label>posexcentaje</label>
                    </div>
                    <div class="col-sm-3">
                      <label>Valor</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                        <option value="">Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                      <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                      <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                        <option value="">Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                      <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                      <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                        <option value="">Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                      <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                      <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                        <option value="">Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                      <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                      <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 text-right">
                      <label>Total : </label>
                    </div>
                    <div class="col-md-4 text-right">
                      <label id="total_rete_amount"> 0.00 </label>
                    </div>
                    <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                  </div>
            </div>

            <div class="form-group">
                <?= lang("attachment", "attachment") ?>
                <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>

            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : $expense->note), 'class="form-control" id="note"'); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_expense', lang('edit_expense'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">

  $(document).ready(function(){
    recalcular_posexetenciones("<?= $expense->rete_fuente_id ?>", "<?= $expense->rete_iva_id ?>", "<?= $expense->rete_ica_id ?>", "<?= $expense->rete_other_id ?>");
  });

    var tax_rates = <?php echo json_encode($tax_rates); ?>;
    $('#category').on('change', function(){
        category = $(this);
        var tax_1 = $('#category option:selected').data('tax1');
        var tax_2 = $('#category option:selected').data('tax2');
        $.ajax({
            type:'POST',
            url:'<?= admin_url("purchases/getTaxInfoJson") ?>',
            data : {
                'tax_1' : tax_1,
                'tax_2' : tax_2,
                '<?=$this->security->get_csrf_token_name()?>': "<?=$this->security->get_csrf_hash()?>"
            }
        }).done(function(data){
            if (data != false) {
                tax_info = data;
                tax_rate = tax_info.tax_1;
                tax_rate_2 = tax_info.tax_2;
                net_cost = $('#amount').val() > 0 ? $('#amount').val() : 0;
                var tax_method = 1;
                $('#exp_ptax').select2('val', tax_1);
                $('#exp_ptax_2').select2('val', tax_2);
                if (tax_rate.type == 1) {
                    $('#exp_ptax_value').prop('readonly', 'readonly');
                }
                if (tax_rate_2.type == 1) {
                    $('#exp_ptax_2_value').prop('readonly', 'readonly');
                } //segundo iva
                if (tax_rate == false) {
                    $('#exp_ptax').select2('val', '').select2('readonly', 'readonly');
                    $('#exp_ptax_value').prop('readonly', 'readonly');
                } else {
                    $('#exp_ptax').select2('readonly', false);
                }
                if (tax_rate_2 == false) {
                    $('#exp_ptax_2').select2('val', '').select2('readonly', 'readonly');
                    $('#exp_ptax_2_value').prop('readonly', 'readonly');
                } else {
                    $('#exp_ptax_2').select2('readonly', false);
                }
                var pr_tax = tax_rate, pr_tax_val = 0;
                if (pr_tax !== null && pr_tax != 0) {
                    $.each(tax_rates, function () {
                        if(this.id == tax_1){
                            if (this.type == 1) {
                                if (tax_method == 0) {
                                    pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))), 4);
                                    pr_tax_rate = formatDecimal(this.rate) + '%';
                                    net_cost -= pr_tax_val;
                                } else {
                                    pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                                    pr_tax_rate = formatDecimal(this.rate) + '%';
                                }
                                $('#exp_ptax_value').prop('readonly', 'readonly');
                            } else if (this.type == 2) {
                                pr_tax_val = parseFloat(this.rate);
                                pr_tax_rate = this.rate;
                                $('#exp_ptax_value').removeAttr('readonly');
                            }
                            $('#exp_ptax_value').val(formatDecimal(pr_tax_val));
                        }
                    });
                }

                var pr_tax_2 = tax_rate_2, pr_tax_2_val = 0;
                if (pr_tax_2 !== null && pr_tax_2 != 0) {
                    $.each(tax_rates, function () {
                        if(this.id == tax_2){
                            if (this.type == 1) {
                                if (tax_method == 0) {
                                    pr_tax_2_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))), 4);
                                    pr_tax_2_rate = formatDecimal(this.rate) + '%';
                                    net_cost -= pr_tax_2_val;
                                } else {
                                    pr_tax_2_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                                    pr_tax_2_rate = formatDecimal(this.rate) + '%';
                                }
                                $('#exp_ptax_2_value').prop('readonly', 'readonly');
                            } else if (this.type == 2) {
                                pr_tax_2_val = parseFloat(this.rate);
                                pr_tax_2_rate = this.rate;
                                $('#exp_ptax_2_value').removeAttr('readonly');
                            }
                            $('#exp_ptax_2_value').val(formatDecimal(pr_tax_2_val));
                        }
                    });
                }
                $('#net_cost').text(formatMoney(net_cost));
                $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
                $('#pro_total').text(formatMoney(parseFloat(net_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
            } else {
                console.log(data);
            }
        });
    });

    $(document).on('change', '#amount, #exp_ptax, #exp_ptax_2, #exp_ptax_value, #exp_ptax_2_value', function(){
        var tax_1 = $('#exp_ptax').val();
        var tax_2 = $('#exp_ptax_2').val();
        var item_tax_method = 1;
        var net_cost = $('#amount').val() > 0 ? $('#amount').val() : 0;
        var pr_tax_val = 0;
        var pr_tax_2_val = 0;
        $.each(tax_rates, function () {
            type_tax = this.type;
            if(this.id == tax_1){
                if (this.type == 1) {
                    if (item_tax_method == 0) {
                        pr_tax_val = formatDecimal(((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)), 4);
                        pr_tax_rate = formatDecimal(this.rate) + '%';
                        net_cost -= pr_tax_val;
                    } else {
                        pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                        pr_tax_rate = formatDecimal(this.rate) + '%';
                    }
                    $('#exp_ptax_value').val(pr_tax_val).prop('readonly', 'readonly');
                } else if (this.type == 2) {
                    $('#exp_ptax_value').removeAttr('readonly');
                    rate_tax_c = $('#exp_ptax_value').val();
                    pr_tax_val = parseFloat(rate_tax_c);
                    pr_tax_rate = rate_tax_c;
                }
            }

            if(this.id == tax_2){
                if (this.type == 1) {
                    if (item_tax_method == 0) {
                        pr_tax_2_val = formatDecimal(((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)), 4);
                        pr_tax_2_rate = formatDecimal(this.rate) + '%';
                        net_cost -= pr_tax_2_val;
                    } else {
                        pr_tax_2_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                        pr_tax_2_rate = formatDecimal(this.rate) + '%';
                    }
                    $('#exp_ptax_2_value').val(pr_tax_2_val).prop('readonly', 'readonly');
                } else if (this.type == 2) {
                    $('#exp_ptax_2_value').removeAttr('readonly');
                    rate_tax_c = $('#exp_ptax_2_value').val();
                    pr_tax_2_val = parseFloat(rate_tax_c);
                    pr_tax_2_rate = rate_tax_c;
                }
            }
        });
        $('#net_cost').text(formatMoney(net_cost));
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
        $('#pro_total').text(formatMoney(parseFloat(net_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
        recalcular_posexetenciones();
    });


    $(document).ready(function () {
        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $('#category').trigger('change');
    });


function showRetention(){
    sr = $('.section-retentions');
    if (sr.hasClass('active')) {
        $('.section-retentions').fadeOut().removeClass('active');
        $('#rete_fuente').attr('checked', true).trigger('click');
        $('#rete_iva').attr('checked', true).trigger('click');
        $('#rete_ica').attr('checked', true).trigger('click');
        $('#rete_otros').attr('checked', true).trigger('click');
    } else {
        $('.section-retentions').fadeIn().addClass('active');
    }
}

// JS posexetenciones

$(document).on('click', '#rete_fuente', function(){
    if ($(this).is(':checked')) {
         $.ajax({
            url: site.base_url+"purchases/opcionesWithHolding/FUENTE"
        }).done(function(data){
            $('#rete_fuente_option').html(data);
            $('#rete_fuente_option').select2('val', '<?= $expense->rete_fuente_id ?>').trigger('change');
            $('#rete_fuente_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#rete_fuente_option').val('').attr('disabled', true).select();
        $('#rete_fuente_tax').val('');
        $('#rete_fuente_valor').val('');
        setReteTotalAmount(rete_fuente_amount, '-');
    }
});

$(document).on('click', '#rete_iva', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"purchases/opcionesWithHolding/IVA"
        }).done(function(data){
            $('#rete_iva_option').html(data);
            $('#rete_iva_option').select2('val', '<?= $expense->rete_iva_id ?>').trigger('change');
            $('#rete_iva_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#rete_iva_option').val('').attr('disabled', true).select();
        $('#rete_iva_tax').val('');
        $('#rete_iva_valor').val('');
        setReteTotalAmount(rete_iva_amount, '-');
    }
});

$(document).on('click', '#rete_ica', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"purchases/opcionesWithHolding/ICA"
        }).done(function(data){
            $('#rete_ica_option').html(data);
            $('#rete_ica_option').select2('val', '<?= $expense->rete_ica_id ?>').trigger('change');
            $('#rete_ica_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#rete_ica_option').val('').attr('disabled', true).select();
        $('#rete_ica_tax').val('');
        $('#rete_ica_valor').val('');
        setReteTotalAmount(rete_ica_amount, '-');
    }
});

$(document).on('click', '#rete_otros', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"purchases/opcionesWithHolding/OTRA"
        }).done(function(data){
            $('#rete_otros_option').html(data);
            $('#rete_otros_option').select2('val', '<?= $expense->rete_other_id ?>').trigger('change');
            $('#rete_otros_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#rete_otros_option').val('').attr('disabled', true).select();
        $('#rete_otros_tax').val('');
        $('#rete_otros_valor').val('');
        setReteTotalAmount(rete_otros_amount, '-');
    }
});

var rete_fuente_amount = 0;
var rete_ica_amount = 0;
var rete_iva_amount = 0;
var rete_otros_amount = 0;

$(document).on('change', '#rete_fuente_option', function(){
    rete_fuente_id = $(this).val();
    $('#rete_fuente_id').val(rete_fuente_id);
    trmrate = 1;
    prevamnt = $('#rete_fuente_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_fuente_option option:selected').data('percentage');
    apply = $('#rete_fuente_option option:selected').data('apply');
    account = $('#rete_fuente_option option:selected').data('account');
    $('#rete_fuente_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    $('#rete_fuente_base').val(amount);
    cAmount = amount * (percentage / 100);
    cAmount = Math.round(cAmount);
    $('#rete_fuente_tax').val(percentage);
    $('#rete_fuente_valor_show').val(formatDecimal(trmrate * cAmount));
    $('#rete_fuente_valor').val(cAmount);
    rete_fuente_amount = formatMoney(cAmount);
    setReteTotalAmount(rete_fuente_amount, '+');
});

$(document).on('change', '#rete_iva_option', function(){
    rete_iva_id = $(this).val();
    $('#rete_iva_id').val(rete_iva_id);
    trmrate = 1;
    prevamnt = $('#rete_iva_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_iva_option option:selected').data('percentage');
    apply = $('#rete_iva_option option:selected').data('apply');
    account = $('#rete_iva_option option:selected').data('account');
    $('#rete_iva_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    $('#rete_iva_base').val(amount);
    cAmount = amount * (percentage / 100);
    cAmount = Math.round(cAmount);
    $('#rete_iva_tax').val(percentage);
    $('#rete_iva_valor_show').val(formatDecimal(trmrate * cAmount));
    $('#rete_iva_valor').val(cAmount);
    rete_iva_amount = formatMoney(cAmount);
    setReteTotalAmount(rete_iva_amount, '+');
});

$(document).on('change', '#rete_ica_option', function(){
    rete_ica_id = $(this).val();
    $('#rete_ica_id').val(rete_ica_id);
    trmrate = 1;
    prevamnt = $('#rete_ica_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_ica_option option:selected').data('percentage');
    apply = $('#rete_ica_option option:selected').data('apply');
    account = $('#rete_ica_option option:selected').data('account');
    $('#rete_ica_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    $('#rete_ica_base').val(amount);
    cAmount = amount * (percentage / 100);
    cAmount = Math.round(cAmount);
    $('#rete_ica_tax').val(percentage);
    $('#rete_ica_valor_show').val(formatDecimal(trmrate * cAmount));
    $('#rete_ica_valor').val(cAmount);
    rete_ica_amount = formatMoney(cAmount);
    setReteTotalAmount(rete_ica_amount, '+');
});

$(document).on('change', '#rete_otros_option', function(){
    rete_otros_id = $(this).val();
    $('#rete_otros_id').val(rete_otros_id);
    trmrate = 1;
    prevamnt = $('#rete_otros_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_otros_option option:selected').data('percentage');
    apply = $('#rete_otros_option option:selected').data('apply');
    account = $('#rete_otros_option option:selected').data('account');
    $('#rete_otros_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    $('#rete_otros_base').val(amount);
    cAmount = amount * (percentage / 100);
    cAmount = Math.round(cAmount);
    $('#rete_otros_tax').val(percentage);
    $('#rete_otros_valor_show').val(formatDecimal(trmrate * cAmount));
    $('#rete_otros_valor').val(cAmount);
    rete_otros_amount = formatMoney(cAmount);
    setReteTotalAmount(rete_otros_amount, '+');
});

function getReteAmount(apply){
    amount = 0;
    if (apply == "ST") {
        amount = $('#net_cost').text();
    } else if (apply == "TX") {
        amount = $('#pro_tax').text();
    } else if (apply == "TO") {
        amount = $('#pro_total').text();
    }
    if (othercurrency = localStorage.getItem('othercurrency')) {
        othercurrencytrm = localStorage.getItem('othercurrencytrm');
        if (othercurrencytrm > 0) {
            amount = amount * othercurrencytrm;
        }
    }
    return amount;
}

function setReteTotalAmount(amount, action){
    if (action == "+") {
        tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) + formatDecimal(amount);
    } else if (action == "-") {
        tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) - formatDecimal(amount);
    }
    if (tra < 0) {
        tra = 0;
    }
    trmrate = 1;
        
    if (tra > 0) {
        $('#rete_applied').val(1);
    } else {
        $('#rete_applied').val(0);
    }

    $('#rete').val(formatMoney(trmrate * tra));
    $('#total_rete_amount').text(formatMoney(tra));
}

function recalcular_posexetenciones(id_retefuente = null, id_reteiva = null, id_reteica = null, id_reteother = null){
    // console.log(id_retefuente);
    if (id_retefuente != null && id_retefuente != "") {
      setTimeout(function() {
        if (!$('#rete_fuente').is(':checked')) {
            $('#rete_fuente').trigger('click');
        }
        sr = $('.section-retentions');
        if (!sr.hasClass('active')) {
          showRetention();
        }
      }, 850);
        
    } else {
        $('#rete_fuente_option').trigger('change');
    }

    if (id_reteiva != null && id_reteiva != "") {
      setTimeout(function() {
        if (!$('#rete_iva').is(':checked')) {
            $('#rete_iva').trigger('click');
        }
        sr = $('.section-retentions');
        if (!sr.hasClass('active')) {
          showRetention();
        }
      }, 850);
    } else {
        $('#rete_iva_option').trigger('change');
    }

    if (id_reteica != null && id_reteica != "") {
      setTimeout(function() {
        if (!$('#rete_ica').is(':checked')) {
            $('#rete_ica').trigger('click');
        }
        sr = $('.section-retentions');
        if (!sr.hasClass('active')) {
          showRetention();
        }
      }, 850);
    } else {
        $('#rete_ica_option').trigger('change');
    }

    if (id_reteother != null && id_reteother != "") {
      setTimeout(function() {
        if (!$('#rete_other').is(':checked')) {
            $('#rete_other').trigger('click');
        }
        sr = $('.section-retentions');
        if (!sr.hasClass('active')) {
          showRetention();
        }
      }, 850);
    } else {
        $('#rete_otros_option').trigger('change');
    }
}
</script>
