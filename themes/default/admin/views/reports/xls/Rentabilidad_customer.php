<?php
require_once APPPATH.'../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

$i = $j = 0 ;

$j++;
$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(30);

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1:H1")->getFont()->setBold(true);

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, 'Fecha')
			->setCellValue('B'.$j, 'Refencia')
			->setCellValue('C'.$j, 'Doc. Afec')
			->setCellValue('D'.$j, 'Cliente')
			->setCellValue('E'.$j, 'Total')
			->setCellValue('F'.$j, 'Costo')
			->setCellValue('G'.$j, 'Utilidad')
			->setCellValue('H'.$j, 'Utilidad');

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(16);

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultColumnDimension()
			->setWidth(16);

foreach ($profitabilitys as $profitability) 
{

	$j++;
	$objPHPExcel->setActiveSheetIndex(0)->getStyle('E'.$j.':H'.$j)->getAlignment()->setHorizontal(
		PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	);

	$fecha = substr($profitability['date'],0,10);

    $utilidad = number_format($profitability['utilidad'],2,',','.');
    $avg_net_unit_cost = number_format($profitability['costo'],2,',','.'); 
    $margen = number_format($profitability['margen'],2,',',''); 
	$total = number_format($profitability['total'],2,',','.');
	
	$referencia = ($profitability['return_sale_ref']!='') ? $profitability['return_sale_ref'] : $profitability['reference_no'] ;
	$afec = ($profitability['return_sale_ref']!='') ? $profitability['reference_no'] : '' ;

	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, $fecha)
				->setCellValue('B'.$j, $referencia)
				->setCellValue('C'.$j, $afec)
				->setCellValue('D'.$j, $profitability['customer'])
				->setCellValue('E'.$j, $total)
				->setCellValue('F'.$j, $avg_net_unit_cost) 
				->setCellValue('G'.$j, $utilidad)
				->setCellValue('H'.$j, $margen) ;
}

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Rentabilidad por cliente');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Rentabilidad por cliente.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
