<?php

/***********

FORMATO PARA VISUALIZACIÓN DE RECIBOS DE CAJA MÚLTIPLES

************/

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';



#[\AllowDynamicProperties]
class PDF extends FPDF
{
    
    

    function Header()
    {
        $cx = 13;
        $cy = 7;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(15, 4, $this->sma->utf8Decode('Fecha : '),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(20, 4, $this->sma->utf8Decode(date('Y-m-d', strtotime($this->encabezado->date))),0,0,'L');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(15, 4, $this->sma->utf8Decode(' Hora : '),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(20, 4, $this->sma->utf8Decode(date('h:i A', strtotime($this->encabezado->date))),0,1,'L');
        $cx = 83;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(28.1, 4, $this->sma->utf8Decode('Forma de pago :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(40, 4, $this->sma->utf8Decode($this->encabezado->paid_by),0,0,'L');

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(($this->sale_payment ? 33 : 42), 4, $this->sma->utf8Decode('N° '.($this->sale_payment ? 'Recibo original' : 'Comprobante original').' :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->setX($this->getX()+3);
        $this->Cell(48.9, 4, $this->sma->utf8Decode($this->encabezado->consecutive_payment),'',1,'L');

        $cx = 176;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cx = 13;
        $cy = 11;
        $this->setXY($cx, $cy);

        $third_name = '';
        if ($this->sale_payment == 1) {
            $third_name = 'Proveedor';
        } else if ($this->sale_payment == 2) {
            $third_name = 'Cliente';
        } else if ($this->sale_payment == 3) {
            $third_name = 'Vendedor';
        }
        $this->Cell(25, 4, $this->sma->utf8Decode(($third_name).' : '),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->encabezado->customer),0,0,'L');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(20, 4, $this->sma->utf8Decode('Nit / CC :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(40, 4, $this->sma->utf8Decode($this->encabezado->vat_no.($this->encabezado->digito_verificacion ? '-'.$this->encabezado->digito_verificacion : '')),0,1,'L');

        $this->ln(4);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(24.5, 5, $this->sma->utf8Decode('Afecta a'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('Valor'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('ReteFuente'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('ReteIVA'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('ReteICA'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode(lang('rete_other')),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('Descuentos'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('Valor pagado'),'B',1,'C');

    }

    function Footer()
    {
        // Print centered page number
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 805) {
            $text = substr($text, 0, 800);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

    function print_payments($pmnts, $print_discounts = false){
        foreach ($pmnts as $pmnt) {
            if ($pmnt->type == 'discount' && $print_discounts == false) {
                continue;
            }
            unset($this->payments_with_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]);
            if ($this->getY() > 240) {
                $this->AddPage();
            }
            $sale_discount = isset($this->sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $this->sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] : 0;
            if (isset($this->sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id])) {
                unset($this->sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]);
            }
            $this->total_rete_fuente += !isset($pmnt->rete_fuente_assumed) || $pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0;
            $this->total_rete_iva += !isset($pmnt->rete_iva_assumed) || $pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0;
            $this->total_rete_ica += !isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0;
            $this->total_rete_other += (!isset($pmnt->rete_other_assumed) || $pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0) + (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total + $pmnt->rete_autoaviso_total : 0);
            $this->total_received += ($pmnt->reference_no != NULL ? $pmnt->amount : 0 ) + $sale_discount;
            $total_retentions =
                        (!isset($pmnt->rete_fuente_assumed) || $pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0) + 
                        (!isset($pmnt->rete_iva_assumed) || $pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0) + 
                        (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0) + 
                        (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total : 0) + 
                        (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_autoaviso_total : 0) + 
                        (!isset($pmnt->rete_other_assumed) || $pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0);
            $this->total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
            $text = $pmnt->reference_no;
            if (!$pmnt->reference_no) {
                $text = $pmnt->note;
            }
            if ($this->getY() > 240) {
                $this->AddPage();
            }
            $columns = [];
            $inicio_fila_X = $this->getX();
            $inicio_fila_Y = $this->getY();
            $columns[0]['inicio_x'] = $this->getX();
            $cX = $this->getX();
            $cY = $this->getY();
            $inicio_altura_fila = $cY;
            $this->setY($cY+1);
                $this->MultiCell(26.5, 2.5, $this->sma->utf8Decode($text),'','L');
            $columns[0]['fin_x'] = $cX+24.5;
            $fin_altura_fila = $this->getY();
            $this->setXY($cX+24.5, $cY);
            $columns[1]['inicio_x'] = $this->getX();
                $this->Cell(22.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->reference_no != NULL ? $pmnt->amount + $sale_discount : 0)),'',0,'R');
            $columns[1]['fin_x'] = $this->getX();
            $columns[2]['inicio_x'] = $this->getX();
                $this->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney(!isset($pmnt->rete_fuente_assumed) || $pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0)),'',0,'R');
            $columns[2]['fin_x'] = $this->getX();
            $columns[3]['inicio_x'] = $this->getX();
                $this->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney(!isset($pmnt->rete_iva_assumed) || $pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0)),'',0,'R');
            $columns[3]['fin_x'] = $this->getX();
            $columns[4]['inicio_x'] = $this->getX();
                $this->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney(!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0)),'',0,'R');
            $columns[4]['fin_x'] = $this->getX();
            $columns[5]['inicio_x'] = $this->getX();
            $rete_other = (!isset($pmnt->rete_other_assumed) || $pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0) + (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total + $pmnt->rete_autoaviso_total : 0);
            $this->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($rete_other)),'',0,'R');
            $columns[5]['fin_x'] = $this->getX();
            $columns[6]['inicio_x'] = $this->getX();

            $this->total_discount += $sale_discount;
                $this->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($sale_discount)),'',0,'R');
            $columns[6]['fin_x'] = $this->getX();
            $columns[7]['inicio_x'] = $this->getX();
                $this->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->amount - $total_retentions)),'',0,'R');
            $columns[7]['fin_x'] = $this->getX();
            $fin_fila_X = $this->getX();
            $fin_fila_Y = $this->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = ($fin_altura_fila - $inicio_altura_fila)+1;
            $this->setXY($inicio_fila_X, $inicio_fila_Y);
            foreach ($columns as $key => $data) {
                $ancho_column = ($data['fin_x'] - $data['inicio_x']);
                if ($altura_fila < 5) {
                    $altura_fila = 5;
                }
                $this->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'B',0,'R');
            }
            $this->ln($altura_fila);
            if (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0) {
                if ($pmnt->rete_bomberil_total > 0) {
                    // $bomb_tabl_text .= "Tasa bomberil (".$text.") ".$this->sma->formatMoney($pmnt->rete_bomberil_total).", ";
                }
                if ($pmnt->rete_autoaviso_total > 0) {
                    // $bomb_tabl_text .= "Tasa Auto Avisos y Tableros (".$text.") ".$this->sma->formatMoney($pmnt->rete_autoaviso_total).", ";
                }
            }
        }

    }
}
$pdf = new PDF('P', 'mm', array(216, 279));
$number_convert = new number_convert();
$pdf->total_amount = 0;
$pdf->total_rete_fuente = 0;
$pdf->total_rete_iva = 0;
$pdf->total_rete_ica = 0;
$pdf->total_rete_other = 0;
$pdf->total_received = 0;
$pdf->total_discount = 0;
$pdf->sales_discount = [];
$sale_payment = 1;
if ($encabezado->supplier_id) {
    $pdf->setTitle($this->sma->utf8Encode('Comprobante egreso'));
} else if ($encabezado->customer_id) {
    $sale_payment = 2;
    $pdf->setTitle($this->sma->utf8Encode('Recibo de caja'));
} else {
    $sale_payment = 3;
}
$pdf->sale_payment = $sale_payment;
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);
$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();
$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->customer = $customer;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->document_type = $document_type;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->Settings = $this->Settings;
$pdf->encabezado = $encabezado;
$pdf->sma = $this->sma;
$pdf->payments_with_discount = [];
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}
$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->biller_logo = $biller_logo;

$enc_total_amount = 0;
$total_payment_by_invoice = [];
foreach ($pmnts as $pmnt) {
    if ($pmnt->type == 'discount') {
        $pdf->payments_with_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] = $pmnt;
        continue;
    }
    $total_retentions = 
                    (!isset($pmnt->rete_fuente_assumed) || $pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0) + 
                    (!isset($pmnt->rete_iva_assumed) || $pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0) + 
                    (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0) + 
                    (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total : 0) + 
                    (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_autoaviso_total : 0) + 
                    (!isset($pmnt->rete_other_assumed) || $pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0);
    $enc_total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
    if (!isset($total_payment_by_invoice[$pmnt->sale_id])) {
        $total_payment_by_invoice[$pmnt->sale_id] = $pmnt->amount;
    } else {
        $total_payment_by_invoice[$pmnt->sale_id] += $pmnt->amount;
    }
}
$pdf->enc_total_amount = $enc_total_amount;
$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();
$maximo_footer = 235;
$pdf->SetFont('Arial','',$fuente+1);
// $this->sma->print_arrays($pmnts);
foreach ($pmnts as $pmnt) {

    if ($pmnt->type == 'discount') {
        if (isset($pdf->sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id])) {
            $pdf->sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] += $pmnt->amount;
        } else {
            $pdf->sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]= $pmnt->amount;
        }
    } else {
        continue;
    }
    
}

$bomb_tabl_text = "";

$pdf->print_payments($pmnts);

$sd_pmnts = [];

if (count($pdf->payments_with_discount) > 0) {
    foreach ($pdf->payments_with_discount as $sp_id => $dis_pmnt) {
        $sd_pmnts[$sp_id] = $dis_pmnt;
        $sd_pmnts[$sp_id]->amount = 0;
    }
    $pdf->print_payments($sd_pmnts, true);
}


$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(24.5, 5, $this->sma->utf8Decode('TOTAL '),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pdf->total_received)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pdf->total_rete_fuente)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pdf->total_rete_iva)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pdf->total_rete_ica)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pdf->total_rete_other)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pdf->total_discount)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pdf->total_amount)),'B',1,'R');

if ($pdf->getY() > 180) {
    $pdf->AddPage();
}


$pdf->ln(2);
$pdf->SetFont('Arial','',$fuente+1);
$pdf->Cell(196, 5, $this->sma->utf8Decode('Valor (En letras)'),'',1,'L');
$pdf->setX($pdf->getX()+5);
$pdf->MultiCell(191, 3, $this->sma->utf8Decode($number_convert->convertir($enc_total_amount)), 0, 'L');
$pdf->ln(5);


$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
$pdf->setXY($cx+185, $cy);
$pdf->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$pdf->PageNo()),'',1,'C');


// $pdf->Cell(28.9,3, $this->sma->utf8Decode($sma->formatMoney($total)),'',1,'C');
$download = false;
if ($download || $for_email) {
    if ($download) {
        $pdf->Output("factura_venta.pdf", "D");
    } else if ($for_email) {
        $pdf->Output("assets/uploads/". $inv->reference_no .".pdf", "F");
    }
} else {
  $pdf->Output("factura_venta.pdf", "I");
}