<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('compatibilities_edit_compatibility'); ?></h4>
        </div>
        <?= admin_form_open_multipart("system_settings/edit_compatibility/". $compatibility->id, ['data-toggle' => 'validator', 'role' => 'form']); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>

            <div class="form-group">
                <?= lang('compatibilities_brand', 'marca'); ?>
                <select name="marca" id="marca" class="form-control" required="required">
                    <option value=""><?= lang("select"); ?></option>
                    <?php foreach ($brands as $brand) { ?>
                        <option value="<?= $brand->id; ?>" <?= ($compatibility->marca_id == $brand->id) ? "selected" : ""; ?>><?= $brand->name; ?></option>
                    <?php } ?>
                </select>
                <input type="hidden" name="nombre_marca" value="<?= $compatibility->nombre_marca; ?>">
            </div>

            <div class="form-group">
                <?= lang('compatibilities_name', 'nombre'); ?>
                <?= form_input('nombre', $compatibility->nombre, 'class="form-control" id="nombre" required="required"'); ?>
            </div>

            <?= form_hidden('id', $compatibility->id); ?>
        </div>
        <div class="modal-footer">
            <?= form_submit('edit_compatibility', lang('save'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
    $(document).ready(function(){
        $(document).on('change', '#marca', obtener_nombre_marca);
    });

    function obtener_nombre_marca()
    {
        var marca = $("#marca").val();
        var nombre_marca = $("#marca option:selected").text();

        if (marca != '') {
            $('input[name="nombre_marca"]').val(nombre_marca);
        } else {
            $('input[name="nombre_marca"]').val('');
        }
    }
</script>