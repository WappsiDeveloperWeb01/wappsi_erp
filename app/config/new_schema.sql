START TRANSACTION;

CREATE TABLE `%_PREFIX_%addresses` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `direccion` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sucursal` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `city_code` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '''''',
  `customer_address_seller_id_assigned` int(11) DEFAULT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  `price_group_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_sale_comision` decimal(25,4) DEFAULT 0.0000,
  `seller_collection_comision` decimal(25,4) DEFAULT 0.0000,
  `code` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `line1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `line2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%adjustments` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `count_id` int(11) DEFAULT NULL,
  `companies_id` int(11) DEFAULT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `cost_center_id` int(11) DEFAULT NULL,
  `type_adjustment` tinyint(1) DEFAULT 0 COMMENT '0. Ajuste normal, 1. Orden de producción, 2. Transformación de productos, 3. Orden de confección',
  `document_type_id` int(11) DEFAULT NULL,
  `origin_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin_document_type_id` int(11) DEFAULT 0,
  `ignore_accountant` int(1) DEFAULT 0,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `destination_biller_id` int(11) DEFAULT NULL,
  `destination_cost_center_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%adjustment_items` (
  `id` int(11) NOT NULL,
  `adjustment_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adjustment_cost` decimal(25,4) DEFAULT NULL,
  `avg_cost` decimal(25,4) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%api_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reference` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%api_limits` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%api_logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_key` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_code` smallint(3) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%assemble` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `est_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `supplier_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Completado pendiente',
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `cutting_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cutting_id` int(11) DEFAULT NULL,
  `production_order_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%assemble_detail` (
  `id` int(11) NOT NULL,
  `assemble_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `production_order_pid` int(11) NOT NULL,
  `cutting_id_pid` int(11) DEFAULT NULL COMMENT 'Relacion al detalle de la orden de corte',
  `assembling_quantity` decimal(15,4) NOT NULL,
  `finished_quantity` decimal(15,4) DEFAULT 0.0000,
  `fault_quantity` decimal(15,4) DEFAULT 0.0000,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Completado pendiente',
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%biller_categories_concessions` (
  `id` int(11) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `concession_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `concession_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%biller_data` (
  `id` int(11) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `pos_document_type_default` int(11) DEFAULT NULL,
  `detal_document_type_default` int(11) DEFAULT NULL,
  `default_price_group` int(11) DEFAULT NULL,
  `default_warehouse_id` int(11) DEFAULT NULL,
  `default_customer_id` int(11) DEFAULT NULL,
  `default_seller_id` int(11) DEFAULT NULL,
  `key_log` int(1) DEFAULT 1,
  `product_order` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: Por defecto, 1: Categoría, 2: Marca',
  `purchases_document_type_default` int(11) DEFAULT NULL,
  `pin_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_type` tinyint(1) DEFAULT 1 COMMENT '1. Física, 2. Virtual',
  `coverage_country` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coverage_state` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coverage_city` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coverage_zone` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prioridad_precios_producto` int(11) DEFAULT NULL,
  `language` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_shipping_cost` int(1) DEFAULT NULL COMMENT '0. Sin costo, 1. Según ubicación, 2. Según volumen y peso, 3. Costo fijo\n',
  `shipping_cost` decimal(25,4) DEFAULT NULL,
  `max_total_shipping_cost` decimal(25,4) DEFAULT NULL,
  `concession_status` int(1) DEFAULT 0,
  `default_pos_section` int(11) DEFAULT NULL,
  `preparation_adjustment_document_type_id` int(11) DEFAULT NULL,
  `default_cost_center_id` int(11) DEFAULT NULL,
  `rete_bomberil_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_bomberil_account` int(11) DEFAULT 0,
  `rete_bomberil_account_counterpart` int(11) DEFAULT 0,
  `rete_autoaviso_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_autoaviso_account` int(11) DEFAULT 0,
  `rete_autoaviso_account_counterpart` int(11) DEFAULT 0,
  `rete_autoica_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_autoica_account` int(11) DEFAULT 0,
  `rete_autoica_account_counterpart` int(11) DEFAULT 0,
  `cash_payment_method_account` int(11) DEFAULT NULL,
  `pin_code_request` int(1) DEFAULT 3 COMMENT '1. Control de márgenes bajos   2. Control de borrado de producto y cancelar factura 3. Todas las anteriores',
  `pin_code_method` int(1) DEFAULT 1 COMMENT '1. Manual, 2. Aleatorio',
  `random_pin_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '98987878747471424454745',
  `random_pin_code_date` datetime DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%biller_documents_types` (
  `id` int(11) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%biller_seller` (
  `id` int(11) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `zone` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%branch_areas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sucursal_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Áreas para las mesas de las sucursales';
CREATE TABLE `%_PREFIX_%brands` (
  `id` int(11) NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `color` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 4 COMMENT '1: Festivos, 2: Fechas Especiales, 3: Cumpleaños, 4: Recordatorios, 5: Tributarios, 6: Otros,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `word` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%cart` (
  `id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `printer_id` int(11) DEFAULT NULL,
  `preparer_id` int(11) DEFAULT NULL,
  `hide` int(1) DEFAULT 0,
  `profitability_margin` decimal(5,2) DEFAULT 0.00,
  `profitability_margin_price_base` decimal(5,2) DEFAULT 0.00,
  `except_category_taxes` int(1) DEFAULT 0 COMMENT '0. No, 1. Si',
  `preparation_area_id` int(11) DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%ciiu_codes` (
  `id` int(11) NOT NULL,
  `code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%cities` (
  `PAIS` varchar(3) NOT NULL DEFAULT '""',
  `CODDEPARTAMENTO` varchar(5) NOT NULL DEFAULT '',
  `DEPARTAMENTO` varchar(50) DEFAULT NULL,
  `CODIGO` varchar(9) NOT NULL DEFAULT '',
  `DESCRIPCION` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%collection_discounts` (
  `id` int(11) NOT NULL,
  `apply_to` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ledger_id` int(11) DEFAULT NULL,
  `purchase_ledger_id` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%combo_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%commisions_soc` (
  `id` int(10) NOT NULL,
  `category` int(11) NOT NULL,
  `subcategory` int(11) DEFAULT 0,
  `price_list` int(11) NOT NULL,
  `percentage` decimal(5,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%companies` (
  `id` int(11) NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_person` smallint(1) DEFAULT NULL COMMENT '1 => Natural   0 => Juridico',
  `name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commercial_register` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` int(3) DEFAULT NULL,
  `document_code` int(6) NOT NULL,
  `vat_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `digito_verificacion` int(3) DEFAULT NULL COMMENT 'En caso de ser tipo de documento NIT, se llena el campo, caso contrario se deja vacío',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subzone` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cf1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf3` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf4` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf5` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf6` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_footer` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_term` int(11) DEFAULT 0,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT 0,
  `deposit_amount` decimal(25,4) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  `price_group_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_partner` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_regimen` int(3) DEFAULT NULL COMMENT '1. Simplificado, 2. Común, 0. No Aplica',
  `city_code` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) DEFAULT 1 COMMENT '1. Activo, 2. Inactivo',
  `birth_month` int(2) DEFAULT 0,
  `birth_day` int(2) DEFAULT 0,
  `customer_only_for_pos` int(1) DEFAULT NULL COMMENT '1. SI, 0. No',
  `customer_seller_id_assigned` int(11) DEFAULT NULL,
  `customer_special_discount` tinyint(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `fuente_retainer` int(1) DEFAULT 0,
  `iva_retainer` int(1) DEFAULT 0,
  `ica_retainer` int(1) DEFAULT 0,
  `default_rete_fuente_id` int(11) DEFAULT NULL,
  `default_rete_iva_id` int(11) DEFAULT NULL,
  `default_rete_ica_id` int(11) DEFAULT NULL,
  `default_rete_other_id` int(11) DEFAULT NULL,
  `customer_payment_type` tinyint(1) DEFAULT 0 COMMENT '0. Credito, 1. Contado',
  `customer_credit_limit` decimal(25,4) DEFAULT 0.0000,
  `customer_payment_term` int(11) DEFAULT 0,
  `customer_validate_min_base_retention` tinyint(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `gender` int(1) DEFAULT NULL COMMENT '1. Masculino, 2. Femenino',
  `logo_square` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'logo.png',
  `tax_exempt_customer` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `initial_accounting_balance_transferred` int(1) DEFAULT 0 COMMENT '0. No, 1. Si',
  `customer_profile_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier_type` int(1) DEFAULT 1 COMMENT ' 1. Productos y Gastos 2. Productos 3. Gastos 4. Acreedores 5. Entidad Financiera 6. Entidad Promotora de Salud (EPS) 7. Fondo de Pensiones y Cesantías 8. Administradora de Riesgos Laborales (ARL) 9. Caja de compensación\n 0. Servicio Nacional de Aprendizaje (SENA)',
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark_as_seller` tinyint(1) NOT NULL DEFAULT 0,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%compatibilidades` (
  `id` int(11) NOT NULL,
  `marca_id` int(11) NOT NULL COMMENT 'Relación con la marca',
  `nombre` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca_nombre` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Concatenación de marca y nombre'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%console_service_data` (
  `id` int(11) NOT NULL,
  `update_date` date NOT NULL,
  `current_version` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `%_PREFIX_%costing` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `purchase_net_unit_cost` decimal(25,4) DEFAULT 0.0000,
  `purchase_unit_cost` decimal(25,4) DEFAULT 0.0000,
  `sale_net_unit_price` decimal(25,4) DEFAULT 0.0000,
  `sale_unit_price` decimal(25,4) DEFAULT 0.0000,
  `quantity_balance` decimal(15,4) DEFAULT NULL,
  `inventory` tinyint(1) DEFAULT 0,
  `overselling` tinyint(1) DEFAULT 0,
  `option_id` int(11) DEFAULT NULL,
  `preparation_id_origin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%cost_per_month` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `M1` decimal(25,4) DEFAULT 0.0000,
  `M2` decimal(25,4) DEFAULT 0.0000,
  `M3` decimal(25,4) DEFAULT 0.0000,
  `M4` decimal(25,4) DEFAULT 0.0000,
  `M5` decimal(25,4) DEFAULT 0.0000,
  `M6` decimal(25,4) DEFAULT 0.0000,
  `M7` decimal(25,4) DEFAULT 0.0000,
  `M8` decimal(25,4) DEFAULT 0.0000,
  `M9` decimal(25,4) DEFAULT 0.0000,
  `M10` decimal(25,4) DEFAULT 0.0000,
  `M11` decimal(25,4) DEFAULT 0.0000,
  `M12` decimal(25,4) DEFAULT 0.0000,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%countries` (
  `CODIGO` varchar(3) NOT NULL DEFAULT '',
  `NOMBRE` varchar(60) NOT NULL DEFAULT '',
  `INDICATIVO` varchar(3) NOT NULL DEFAULT '',
  `MONEDA` int(11) NOT NULL DEFAULT 0,
  `codigo_iso` varchar(2) NOT NULL COMMENT 'Estándar ISO 3166-1. Máximo 2 dígitos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%currencies` (
  `id` int(11) NOT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT 0,
  `symbol` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%customer_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percent` int(11) NOT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%custom_fields` (
  `id` int(11) NOT NULL,
  `module` int(1) NOT NULL COMMENT '1. Customers, 2. Productos',
  `cf_code` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cf_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cf_type` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'select, multiple, date, input'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%custom_field_values` (
  `id` int(11) NOT NULL,
  `cf_id` int(11) NOT NULL,
  `cf_value` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%cutting` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `est_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `employee_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Completado pendiente',
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `production_order_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `production_order_id` int(11) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `employee_2_id` int(11) DEFAULT NULL,
  `tender_id` int(11) DEFAULT NULL COMMENT '%_PREFIX_%companies, group_name = employee',
  `tender_2_id` int(11) DEFAULT NULL COMMENT '%_PREFIX_%companies, group_name = employee',
  `packer_id` int(11) DEFAULT NULL COMMENT '%_PREFIX_%companies, group_name = employee',
  `packer_2_id` int(11) DEFAULT NULL COMMENT '%_PREFIX_%companies, group_name = employee',
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `embroiderer_id` int(11) DEFAULT NULL,
  `stamper_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%cutting_detail` (
  `id` int(11) NOT NULL,
  `cutting_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `production_order_pid` int(11) NOT NULL COMMENT 'Relacion al detalle de la orden de produccion',
  `cutting_quantity` decimal(15,4) NOT NULL,
  `finished_quantity` decimal(15,4) DEFAULT 0.0000,
  `fault_quantity` decimal(15,4) DEFAULT 0.0000,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Completado pendiente',
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%dashboards` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT 1 COMMENT '1. Activo, 0. Inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `php` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sql` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%debit_credit_notes_concepts` (
  `id` int(11) NOT NULL,
  `type` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dian_code` tinyint(2) NOT NULL,
  `dian_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(240) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_id` tinyint(2) NOT NULL,
  `tax_amount_ledger_account` int(11) DEFAULT NULL,
  `tax_base_ledger_account` int(11) DEFAULT NULL,
  `view` tinyint(1) NOT NULL COMMENT '0: No, 1: Si,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%deliveries` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `sale_id` int(11) NOT NULL,
  `do_reference_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale_reference_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivered_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `received_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%deposits` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `balance` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `paid_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `cost_center_id` int(11) DEFAULT NULL,
  `third_type` int(11) DEFAULT 1 COMMENT '1. Cliente, 2. Proveedor',
  `origen` int(11) DEFAULT 1 COMMENT '1. Manual, 2. Automático',
  `origen_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origen_document_type_id` int(11) DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  `affects_pos_register` int(1) DEFAULT 1 COMMENT '1. Si afecta caja, 0. No afecta caja'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%dian_note_concept` (
  `id` int(11) NOT NULL,
  `code` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` char(2) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Concepto de Corrección para Notas crédito y Notas Débito';
CREATE TABLE `%_PREFIX_%documents_types` (
  `id` int(11) NOT NULL,
  `module` int(1) NOT NULL COMMENT '1. Pos, 2. Detal, 3. DEV POS, 4. DEV DETAL, 5. Compras, 6. DEV Compras, 7. Cotizaciones, 8. Órden de pedido, 9. Órden de compra, 10. Órden de gasto, 11. Ajustes, 12. Transferencias, 13. Pagos a Ventas, 14. Recibos de caja, 15. Anticipos, 16. Pagos a Compras, 17. Gastos ',
  `nombre` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factura_electronica` tinyint(1) DEFAULT 0 COMMENT '0: No, 1: Si,',
  `factura_contingencia` tinyint(1) DEFAULT NULL COMMENT '0: No, 1: Si,',
  `num_resolucion` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `palabra_resolucion` int(1) DEFAULT 1 COMMENT '1. Habilita, 2. Autoriza',
  `sales_prefix` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales_consecutive` bigint(20) DEFAULT NULL,
  `inicio_resolucion` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fin_resolucion` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emision_resolucion` date DEFAULT NULL,
  `vencimiento_resolucion` date DEFAULT NULL,
  `clave_tecnica` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Se genera por cada serie de numeración',
  `fe_work_environment` tinyint(1) DEFAULT 0 COMMENT '0: Producción, 1: Contingencia, 2: Pruebas,',
  `fe_testid` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Set TestId para realizar pruebas.',
  `invoice_footer` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_log` int(1) DEFAULT 1,
  `module_invoice_format_id` int(11) DEFAULT NULL,
  `save_resolution_in_sale` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
  `word_type_sale` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fe_transaction_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Campo valido para DELCOP',
  `not_accounting` int(1) DEFAULT 0 COMMENT '1. No contabiliza, 0. Si contabiliza',
  `invoice_header` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_consecutive_left_zeros` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%documentypes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abreviacion` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo_doc` int(3) NOT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%employee_contacts` (
  `id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `relationship` tinyint(1) NOT NULL COMMENT '1: Conyuge, 2: Padre, 3: Madre, 4: Hijo, 5: Hija, 6: Abuelo, 7: Abuela, 8: Otros,',
  `phone` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `main_contact` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%employee_data` (
  `id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `home_phone` varchar(30) DEFAULT NULL,
  `marital_status` tinyint(1) NOT NULL COMMENT '1: Union Libre, 2: Casado, 3: Separado, 4: Soltero, 5: Viudo,',
  `blood_type` tinyint(1) NOT NULL COMMENT '1: A+, 2: A-, 3: B+, 4: B-, 5: O+, 6: O-, 7: AB+, 8: AB-,',
  `military_card` varchar(30) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `educational_level` tinyint(1) NOT NULL COMMENT '1: Básica primaria, 2: Básica Secundaria, 3:Educación Media técnica, 4: Técnico profesional, 5: Tecnología, 6: Profesional Universitario, 7: Especialización, 8: Maestría, 9: Doctorado, 10: Postdoctorado,',
  `profession` varchar(45) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%entryitems_con` (
  `id` bigint(18) NOT NULL,
  `entry_id` bigint(18) NOT NULL,
  `ledger_id` bigint(18) NOT NULL,
  `amount` decimal(25,2) NOT NULL DEFAULT 0.00,
  `dc` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `narration` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `base` decimal(25,2) NOT NULL DEFAULT 0.00,
  `companies_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `%_PREFIX_%expenses` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `cost_center_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax_val` decimal(25,4) DEFAULT 0.0000,
  `tax_rate_2_id` int(11) DEFAULT NULL,
  `tax_val_2` decimal(25,4) DEFAULT 0.0000,
  `supplier_id` int(11) DEFAULT NULL,
  `document_type_id` int(11) NOT NULL,
  `rete_fuente_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_fuente_total` decimal(25,4) DEFAULT 0.0000,
  `rete_fuente_account` int(11) DEFAULT 0,
  `rete_fuente_base` decimal(25,4) DEFAULT NULL,
  `rete_fuente_id` int(11) DEFAULT NULL,
  `rete_iva_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_iva_total` decimal(25,4) DEFAULT 0.0000,
  `rete_iva_account` int(11) DEFAULT 0,
  `rete_iva_base` decimal(25,4) DEFAULT NULL,
  `rete_iva_id` int(11) DEFAULT NULL,
  `rete_ica_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_ica_total` decimal(25,4) DEFAULT 0.0000,
  `rete_ica_account` int(11) DEFAULT 0,
  `rete_ica_base` decimal(25,4) DEFAULT NULL,
  `rete_ica_id` int(11) DEFAULT NULL,
  `rete_other_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_other_total` decimal(25,4) DEFAULT 0.0000,
  `rete_other_account` int(11) DEFAULT 0,
  `rete_other_base` decimal(25,4) DEFAULT NULL,
  `rete_other_id` int(11) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%expense_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ledger_id` int(11) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax_ledger_id` int(11) DEFAULT NULL,
  `tax_rate_2_id` int(11) DEFAULT NULL,
  `tax_2_ledger_id` int(11) DEFAULT NULL,
  `creditor_ledger_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%gift_cards` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `card_no` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%gift_card_topups` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `card_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relationed_companies` int(1) DEFAULT 0,
  `group_name_relationed` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%groups_con` (
  `id` bigint(18) NOT NULL,
  `parent_id` bigint(18) DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `affects_gross` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `%_PREFIX_%invoice_formats` (
  `id` int(11) NOT NULL,
  `module` int(2) NOT NULL COMMENT '1. Pos, 2. Detal, 3. DEV POS, 4. DEV DETAL, 5. Compras, 6. DEV Compras, 7. Cotizaciones, 8. Órden de pedido, 9. Órden de compra, 10. Órden de gasto, 11. Ajustes, 12. Transferencias, 13. Pagos a Ventas, 14. Recibos de caja, 15. Anticipos, 16. Pagos a Compras, 17. Gastos',
  `format_url` varchar(450) COLLATE utf8mb4_unicode_ci NOT NULL,
  `format_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_item_tax` int(1) NOT NULL,
  `tax_inc` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '0. Inactivo, 1. Activo',
  `qty_decimals` int(2) DEFAULT 2,
  `value_decimals` int(2) DEFAULT 2,
  `logo` int(1) DEFAULT 2 COMMENT '1. Rectangular, 2. Cuadrado',
  `tax_indicator` int(1) DEFAULT 0,
  `product_detail_discount` int(1) DEFAULT 0,
  `product_detail_promo` int(1) NOT NULL DEFAULT 0,
  `product_show_code` int(1) DEFAULT 1 COMMENT '1. Codigo  2. Referencia',
  `totalize_quantity` int(1) NOT NULL COMMENT '1. Si, 0. No',
  `show_award_points` int(1) DEFAULT 0,
  `show_document_type_header` int(1) DEFAULT 1,
  `group_by_product_reference` int(1) DEFAULT 0 COMMENT 'Sólo aplica para formatos que se hicieron con agrupamiento por referencia',
  `show_product_preferences` int(1) DEFAULT 0 COMMENT '1 Si, 0 No',
  `product_order` int(1) DEFAULT NULL COMMENT '1. Código producto, 2. Nom producto ASC, 3. Nom producto DESC, 4. Según parámetro ordenamiento de diligenciamiento, 5. Según diligenciamiento sin tener en cuenta parámetro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%invoice_notes` (
  `id` int(11) NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` int(1) DEFAULT NULL,
  `module` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%ledgers_con` (
  `id` bigint(18) NOT NULL,
  `group_id` bigint(18) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `op_balance` decimal(25,2) NOT NULL DEFAULT 0.00,
  `op_balance_dc` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(2) NOT NULL DEFAULT 0,
  `reconciliation` int(1) NOT NULL DEFAULT 0,
  `notes` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%new_year_database_connection` (
  `id` int(11) NOT NULL,
  `hostname` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `database` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dbprefix` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%notifications` (
  `id` int(11) NOT NULL,
  `comment` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `from_date` datetime DEFAULT NULL,
  `till_date` datetime DEFAULT NULL,
  `scope` tinyint(1) NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%order_ref` (
  `ref_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `so` int(11) NOT NULL DEFAULT 1,
  `qu` int(11) NOT NULL DEFAULT 1,
  `qup` int(11) NOT NULL DEFAULT 1,
  `os` int(11) NOT NULL DEFAULT 1,
  `po` int(11) NOT NULL DEFAULT 1,
  `to` int(11) NOT NULL DEFAULT 1,
  `pos` int(11) NOT NULL DEFAULT 1,
  `do` int(11) NOT NULL DEFAULT 1,
  `pay` int(11) NOT NULL DEFAULT 1,
  `re` int(11) NOT NULL DEFAULT 1,
  `rep` int(11) NOT NULL DEFAULT 1,
  `ex` int(11) NOT NULL DEFAULT 1,
  `ppay` int(11) NOT NULL DEFAULT 1,
  `qa` int(11) DEFAULT 1,
  `rc` int(11) NOT NULL DEFAULT 1,
  `dp` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%order_sales` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT 0.0000,
  `order_discount_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT 0.0000,
  `order_discount` decimal(25,4) DEFAULT 0.0000,
  `product_tax` decimal(25,4) DEFAULT 0.0000,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT 0.0000,
  `total_tax` decimal(25,4) DEFAULT 0.0000,
  `shipping` decimal(25,4) DEFAULT 0.0000,
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Completado pendiente',
  `payment_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Pendiente Debido Parcial Pagado',
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT 0,
  `paid` decimal(25,4) DEFAULT 0.0000,
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_sale_ref` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `return_sale_total` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `rounding` decimal(10,4) DEFAULT NULL,
  `suspend_note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api` tinyint(1) DEFAULT 0,
  `shop` tinyint(1) DEFAULT 0,
  `seller_id` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `reserve_id` int(11) DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manual_payment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `payment_method` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT '''''' COMMENT 'Ordenes de pedido tienda online',
  `pay_partner` smallint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 =PENDIENTE  1 = PAGADO',
  `rete_fuente_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_fuente_total` decimal(25,4) DEFAULT 0.0000,
  `rete_fuente_account` int(11) DEFAULT 0,
  `rete_fuente_base` decimal(25,4) DEFAULT NULL,
  `rete_iva_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_iva_total` decimal(25,4) DEFAULT 0.0000,
  `rete_iva_account` int(11) DEFAULT 0,
  `rete_iva_base` decimal(25,4) DEFAULT NULL,
  `rete_ica_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_ica_total` decimal(25,4) DEFAULT 0.0000,
  `rete_ica_account` int(11) DEFAULT 0,
  `rete_ica_base` decimal(25,4) DEFAULT NULL,
  `rete_other_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_other_total` decimal(25,4) DEFAULT 0.0000,
  `rete_other_account` int(11) DEFAULT 0,
  `rete_other_base` decimal(25,4) DEFAULT NULL,
  `resolucion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  `destination_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `wms_picking_status` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wms_packing_status` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%order_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `quantity_to_bill` decimal(15,4) DEFAULT 0.0000,
  `quantity_delivered` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_tax_2` decimal(25,4) DEFAULT NULL,
  `tax_rate_2_id` int(11) DEFAULT NULL,
  `tax_2` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `unit_order_discount` decimal(25,4) DEFAULT NULL,
  `price_before_tax` decimal(25,4) DEFAULT 0.0000,
  `preferences` varchar(245) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%packing` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `est_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `warehouse_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Completado pendiente',
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `assemble_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assemble_id` int(11) DEFAULT NULL,
  `production_order_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adjustment_doc_type_id` int(11) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%packing_detail` (
  `id` int(11) NOT NULL,
  `packing_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `production_order_pid` int(11) NOT NULL,
  `assemble_id_pid` int(11) NOT NULL COMMENT 'Relacion al detalle de la orden de ensamble',
  `packing_quantity` decimal(15,4) NOT NULL,
  `finished_quantity` decimal(15,4) DEFAULT 0.0000,
  `fault_quantity` decimal(15,4) DEFAULT 0.0000,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Completado pendiente',
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%pages` (
  `id` int(11) NOT NULL,
  `name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `order_no` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%partners_soc` (
  `id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `partner_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `partner_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_type` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` int(20) UNSIGNED NOT NULL,
  `swift_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred` smallint(1) UNSIGNED NOT NULL COMMENT '1= Bank  2= Paypal',
  `active` smallint(1) NOT NULL COMMENT '0= Inactive 1= Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT current_timestamp(),
  `sale_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `expense_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_by` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cheque_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_holder` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_month` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_year` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT 0.0000,
  `pos_balance` decimal(25,4) DEFAULT 0.0000,
  `approval_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `affect` smallint(1) NOT NULL DEFAULT 0 COMMENT '1= Facturación   0= Otros',
  `trm_difference` decimal(25,4) DEFAULT 0.0000,
  `trm_difference_ledger_id` int(11) DEFAULT NULL,
  `tax_rate_traslate` decimal(25,4) DEFAULT 0.0000,
  `tax_rate_traslate_ledger_id` int(11) DEFAULT NULL,
  `cost_center_id` int(11) DEFAULT NULL,
  `rete_fuente_total` decimal(25,4) DEFAULT 0.0000,
  `rete_iva_total` decimal(25,4) DEFAULT 0.0000,
  `rete_ica_total` decimal(25,4) DEFAULT 0.0000,
  `rete_other_total` decimal(25,4) DEFAULT 0.0000,
  `multi_payment` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `mean_payment_code_fe` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código empleado para facturación electrónica',
  `payment_date` timestamp NULL DEFAULT NULL,
  `consecutive_payment` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  `rete_fuente_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_fuente_account` int(11) DEFAULT 0,
  `rete_fuente_base` decimal(25,4) DEFAULT NULL,
  `rete_iva_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_iva_account` int(11) DEFAULT 0,
  `rete_iva_base` decimal(25,4) DEFAULT NULL,
  `rete_ica_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_ica_account` int(11) DEFAULT 0,
  `rete_ica_base` decimal(25,4) DEFAULT NULL,
  `rete_other_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_other_account` int(11) DEFAULT 0,
  `rete_other_base` decimal(25,4) DEFAULT NULL,
  `rete_fuente_id` int(11) DEFAULT NULL,
  `rete_iva_id` int(11) DEFAULT NULL,
  `rete_ica_id` int(11) DEFAULT NULL,
  `rete_other_id` int(11) DEFAULT NULL,
  `discount_ledger_id` int(11) DEFAULT NULL,
  `comm_perc` decimal(7,4) DEFAULT 0.0000,
  `comm_base` decimal(25,4) DEFAULT 0.0000,
  `comm_amount` decimal(25,4) DEFAULT 0.0000,
  `seller_id` int(11) DEFAULT NULL,
  `comm_payment_status` int(1) DEFAULT 0,
  `comm_payment_date` timestamp NULL DEFAULT NULL,
  `comm_payment_reference_no` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `concept_ledger_id` int(11) DEFAULT NULL,
  `affected_deposit_id` int(11) DEFAULT NULL,
  `return_reference_no` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pm_commision_value` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pm_retefuente_value` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pm_reteiva_value` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pm_reteica_value` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `rete_fuente_assumed` int(1) DEFAULT NULL,
  `rete_iva_assumed` int(1) DEFAULT NULL,
  `rete_ica_assumed` int(1) DEFAULT NULL,
  `rete_other_assumed` int(1) DEFAULT NULL,
  `rete_fuente_assumed_account` int(11) DEFAULT NULL,
  `rete_iva_assumed_account` int(11) DEFAULT NULL,
  `rete_ica_assumed_account` int(11) DEFAULT NULL,
  `rete_other_assumed_account` int(11) DEFAULT NULL,
  `concept_base` decimal(25,4) DEFAULT NULL,
  `concept_company_id` int(11) DEFAULT NULL,
  `rete_bomberil_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_bomberil_total` decimal(25,4) DEFAULT 0.0000,
  `rete_bomberil_account` int(11) DEFAULT 0,
  `rete_bomberil_base` decimal(25,4) DEFAULT NULL,
  `rete_autoaviso_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_autoaviso_total` decimal(25,4) DEFAULT 0.0000,
  `rete_autoaviso_account` int(11) DEFAULT 0,
  `rete_autoaviso_base` decimal(25,4) DEFAULT NULL,
  `rete_bomberil_id` int(11) DEFAULT NULL,
  `rete_autoaviso_id` int(11) DEFAULT NULL,
  `rete_bomberil_assumed_account` int(11) DEFAULT NULL,
  `rete_autoaviso_assumed_account` int(11) DEFAULT NULL,
  `comm_paid_payments_ids` int(11) DEFAULT NULL,
  `comm_paid_sale_reference` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%payments_soc` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_partner` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Identificador (alfanumérico) del socio de negocio de quien realiza la petición de pago.',
  `datetime_request` datetime NOT NULL,
  `req_amount` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `status` smallint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0=pendiente  1=pagado 2= en proceso',
  `receipt_number` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `datetime_receipt` datetime NOT NULL,
  `bank_paypal` smallint(1) NOT NULL DEFAULT 1 COMMENT '1 => bank     2 => paypal',
  `withdraw_amount` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `total_payment` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `id_user` int(10) UNSIGNED NOT NULL COMMENT 'Identificador del usuario quién realiza el pago.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%payment_mean_code_fe` (
  `code` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%payment_methods` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_sale` int(1) NOT NULL DEFAULT 1 COMMENT 'Estado en ventas : 1. Activo, 0. Inactivo',
  `state_purchase` int(1) NOT NULL DEFAULT 1 COMMENT 'Estado en compras : 1. Activo, 0. Inactivo',
  `code_fe` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código empleado para facturación electrónica',
  `cash_payment` int(1) DEFAULT 0 COMMENT 'Pago de contado : 1. Si, 0. No',
  `due_payment` int(1) NOT NULL DEFAULT 0 COMMENT '1. Si, 0. No',
  `commision_value` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commision_ledger_id` int(11) DEFAULT NULL,
  `retefuente_value` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `retefuente_ledger_id` int(11) DEFAULT NULL,
  `reteiva_value` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reteiva_ledger_id` int(11) DEFAULT NULL,
  `reteica_value` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reteica_ledger_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'default.png',
  `biller_accountant_parametrization` int(1) DEFAULT 0 COMMENT '1. SI, 0. No',
  `billers` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paypal_currency` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT 2.0000,
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT 3.9000,
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT 4.4000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%payroll` (
  `id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL COMMENT 'month - payment_frequency - number',
  `creation_date` date NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `frequency` tinyint(1) NOT NULL,
  `number` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `employee_no` int(11) NOT NULL DEFAULT 0,
  `earnings` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `deductions` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total_payment` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1: En preparación, 2: Aprobada, 3: Pagada,',
  `paid_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_adjustment_documents` (
  `id` int(11) NOT NULL,
  `electronic_payroll_id` int(11) NOT NULL,
  `electronic_payroll_adjustment_id` int(11) DEFAULT NULL,
  `electronic_payroll_delete_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%payroll_areas` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_arl_risk_classes` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `percentage` decimal(12,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%payroll_concepts` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `concept_type_id` int(11) NOT NULL,
  `payment_frequency_concept` tinyint(1) NOT NULL DEFAULT 0,
  `dian_code` int(11) DEFAULT NULL,
  `percentage` decimal(10,2) NOT NULL DEFAULT 0.00,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Inactivo, 1: Activo,',
  `order_json` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_concept_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0: No aplica, 1: Devengado, 2: Deducido,',
  `fixed` tinyint(1) NOT NULL COMMENT '0: No, 1: Si,',
  `automatic` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1:Si,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_contracts` (
  `id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `internal_code` varchar(30) DEFAULT NULL,
  `contract_type` tinyint(1) NOT NULL COMMENT '1:Término fijo, 2:Término indefinido, 3:Obra o labor, 4:Aprendizaje, 5:Prácticas o pasantías,',
  `employee_type` int(11) NOT NULL,
  `creation_date` date NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `settlement_date` date NOT NULL,
  `workday` int(11) NOT NULL,
  `daily_hours` decimal(3,2) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `professional_position` varchar(45) NOT NULL,
  `retired_risk` tinyint(1) NOT NULL COMMENT '1:SI, 2:NO,',
  `contract_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0:Inactivo,1:Activo,',
  `base_amount` decimal(12,2) NOT NULL,
  `integral_salary` tinyint(1) DEFAULT 0 COMMENT '0: No, 1: Si,',
  `trans_allowance` tinyint(1) DEFAULT 0 COMMENT '0: No, 1: Si,',
  `payment_frequency` int(11) NOT NULL COMMENT '1: Semanal, 2: Decenal, 3: Catorcenal, 4: quencenal, 5: Mensual, 6: Otros,',
  `extra_allowance` tinyint(1) DEFAULT NULL,
  `withholding_method` tinyint(1) DEFAULT 0 COMMENT '0: No aplica, 1:Método uno, 2:Método dos,',
  `withholding_percentage` decimal(12,2) DEFAULT 0.00,
  `payment_method` int(11) NOT NULL,
  `bank` int(11) DEFAULT NULL,
  `account_type` tinyint(1) DEFAULT NULL COMMENT '1:Cuenta de Ahorro, 2:Cuenta Corriente, 3:Billetera Virtual,',
  `account_no` varchar(30) DEFAULT NULL,
  `afp_id` int(11) DEFAULT NULL,
  `eps_id` int(11) DEFAULT NULL,
  `arl_id` int(11) DEFAULT NULL,
  `arl_risk_classes` int(11) NOT NULL,
  `caja_id` int(11) DEFAULT NULL,
  `icbf_id` int(11) DEFAULT NULL,
  `sena_id` int(11) DEFAULT NULL,
  `cesantia_id` int(11) DEFAULT NULL,
  `salary_bonus` int(11) NOT NULL DEFAULT 0,
  `non_salary_bonus` int(11) NOT NULL DEFAULT 0,
  `pants_size` varchar(12) DEFAULT NULL,
  `shirt_size` varchar(12) DEFAULT NULL,
  `shoes_size` varchar(12) DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_contract_concepts` (
  `contract_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `concept_id` int(11) NOT NULL,
  `concept_type_id` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT 0.00,
  `applied` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%payroll_electronic_employee` (
  `id` int(11) NOT NULL,
  `electronic_payroll_id` int(11) NOT NULL,
  `creation_date` date NOT NULL,
  `creation_time` time NOT NULL,
  `biller_id` int(11) NOT NULL,
  `reference_no` varchar(45) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `earneds` decimal(12,4) NOT NULL,
  `deductions` decimal(12,4) NOT NULL,
  `payment_total` decimal(12,4) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No enviado, 1: Pendiente, 2: Aceptado, 3: Enviado,',
  `generation_date` date DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `trackId` varchar(45) DEFAULT NULL,
  `cune` text DEFAULT NULL,
  `statusMessage` varchar(45) DEFAULT NULL,
  `statusDescription` text DEFAULT NULL,
  `warnings` text DEFAULT NULL,
  `errorMessage` varchar(45) DEFAULT NULL,
  `errorReason` text DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: Documento NE, 1: Documento de Ajuste, 2: Documento de Eliminación,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%payroll_futures_items` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `concept_type_id` int(11) NOT NULL,
  `concept_id` int(11) NOT NULL,
  `creation_date` date DEFAULT NULL,
  `start_date` date NOT NULL,
  `due_date` date DEFAULT NULL,
  `frequency` tinyint(1) NOT NULL COMMENT '1: Recurrente, 2: Permanente,',
  `amount` decimal(12,2) NOT NULL COMMENT 'Saldo',
  `quantity` int(11) NOT NULL COMMENT 'Cantidad de cuotas faltantes',
  `fee_no` int(11) NOT NULL COMMENT 'Número de la cuota',
  `fee_amount` decimal(12,2) NOT NULL,
  `term` varchar(45) NOT NULL,
  `term_no` int(11) NOT NULL,
  `applied` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1: Si,',
  `from_contract` tinyint(1) NOT NULL DEFAULT 0,
  `loan_code` varchar(10) DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_items` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `payroll_concept_type` int(11) NOT NULL,
  `payroll_concept_id` int(11) NOT NULL,
  `earned_deduction` tinyint(1) NOT NULL COMMENT '1: Devengado, 2:Deducido,',
  `amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `creation_date` date NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `days_quantity` int(11) DEFAULT 0,
  `hours_quantity` decimal(12,2) DEFAULT 0.00,
  `frequency` tinyint(1) NOT NULL COMMENT '1: Recurrente, 2: Permanente, 3: Once,',
  `prorroga` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1: Si,',
  `future_item_id` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_period` (
  `id` int(11) NOT NULL,
  `period` varchar(45) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Inactivo, 1: Activo,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_professional_position` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `area_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_provisions` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `concept_id` int(11) NOT NULL,
  `base_salary` decimal(12,4) NOT NULL,
  `percentage` decimal(12,4) NOT NULL,
  `amount` decimal(12,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%payroll_settings` (
  `id` int(11) NOT NULL,
  `work_environment` tinyint(1) NOT NULL DEFAULT 2 COMMENT '1:Producción,\n2:Pruebas,',
  `set_test_id` varchar(45) DEFAULT NULL,
  `payment_frequency` int(11) NOT NULL,
  `payment_schedule` tinyint(1) NOT NULL COMMENT '1:Mes Comercial 30 días, \n2:Mes Variable,',
  `weekly_working_hours` decimal(12,2) NOT NULL DEFAULT 0.00,
  `enable_saturday_vacation` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0:No,\n1:Si,',
  `minimum_salary_value` decimal(12,2) NOT NULL DEFAULT 0.00,
  `integral_salary_value` decimal(12,2) NOT NULL DEFAULT 0.00,
  `transportation_allowance_value` decimal(12,2) NOT NULL DEFAULT 0.00,
  `withholding_base_value` decimal(12,2) NOT NULL DEFAULT 0.00,
  `prima_payment` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1:Último dia de semestre,\n2:Ultimo día de quincena,',
  `bonus_payment` tinyint(1) NOT NULL COMMENT '1:Quincenal,\n2:Mensual,',
  `payment_note` varchar(45) DEFAULT NULL,
  `arl` int(11) DEFAULT NULL,
  `ccf` int(11) DEFAULT NULL,
  `compensation_fund_percentage` decimal(12,2) NOT NULL,
  `pension_percentage` decimal(12,2) NOT NULL,
  `high_risk_pension_percentage` decimal(12,2) NOT NULL,
  `exempt_parafiscal_health_payments` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0:No,\n1:Si,',
  `small_business` tinyint(1) DEFAULT 0 COMMENT '0:No,\n1:Si,',
  `illness_2days` decimal(12,2) DEFAULT 0.00,
  `illness_90days` decimal(12,2) DEFAULT 0.00,
  `illness_91` decimal(12,2) DEFAULT 0.00,
  `bank_id` int(11) DEFAULT 0,
  `bank_account_number` varchar(45) DEFAULT NULL,
  `bank_account_type` tinyint(1) DEFAULT NULL COMMENT '1:Cuenta de Ahorro, 2:Cuenta Corriente, 3:Billetera Virtual,',
  `file_format` tinyint(1) DEFAULT NULL COMMENT '1:SAP,\n2:PAB,',
  `ss_operators` int(11) DEFAULT NULL,
  `UVT_value` decimal(12,2) DEFAULT 0.00,
  `bonus_limit_percentage` decimal(12,2) NOT NULL,
  `generate_payroll_per_branch` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1: S,i'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_social_security_parafiscal` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `base_salary` decimal(12,4) NOT NULL,
  `percentage` decimal(12,4) NOT NULL,
  `amount` decimal(12,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%payroll_ss_operators` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_types_contracts` (
  `id` int(11) NOT NULL,
  `description` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_types_employees` (
  `id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `description` varchar(120) NOT NULL,
  `afp` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Deshabilita, 1: Habilita,',
  `eps` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Deshabilita, 1: Habilita,',
  `arl` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Deshabilita, 1: Habilita,',
  `caja` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Deshabilita, 1: Habilita,',
  `icbf` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Deshabilita, 1: Habilita,',
  `sena` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Deshabilita, 1: Habilita,',
  `cesantias` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: Deshabilita, 1: Habilita,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_weeks` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `week` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%payroll_workday` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `products-index` tinyint(1) DEFAULT 0,
  `products-add` tinyint(1) DEFAULT 0,
  `products-edit` tinyint(1) DEFAULT 0,
  `products-delete` tinyint(1) DEFAULT 0,
  `products-cost` tinyint(1) DEFAULT 0,
  `products-price` tinyint(1) DEFAULT 0,
  `quotes-index` tinyint(1) DEFAULT 0,
  `quotes-add` tinyint(1) DEFAULT 0,
  `quotes-edit` tinyint(1) DEFAULT 0,
  `quotes-pdf` tinyint(1) DEFAULT 0,
  `quotes-email` tinyint(1) DEFAULT 0,
  `quotes-delete` tinyint(1) DEFAULT 0,
  `sales-index` tinyint(1) DEFAULT 0,
  `sales-fe_index` tinyint(1) DEFAULT 0,
  `sales-add` tinyint(1) DEFAULT 0,
  `sales-edit` tinyint(1) DEFAULT 0,
  `sales-pdf` tinyint(1) DEFAULT 0,
  `sales-email` tinyint(1) DEFAULT 0,
  `sales-delete` tinyint(1) DEFAULT 0,
  `purchases-index` tinyint(1) DEFAULT 0,
  `purchases-add` tinyint(1) DEFAULT 0,
  `purchases-edit` tinyint(1) DEFAULT 0,
  `purchases-pdf` tinyint(1) DEFAULT 0,
  `purchases-email` tinyint(1) DEFAULT 0,
  `purchases-delete` tinyint(1) DEFAULT 0,
  `transfers-index` tinyint(1) DEFAULT 0,
  `transfers-add` tinyint(1) DEFAULT 0,
  `transfers-edit` tinyint(1) DEFAULT 0,
  `transfers-pdf` tinyint(1) DEFAULT 0,
  `transfers-email` tinyint(1) DEFAULT 0,
  `transfers-delete` tinyint(1) DEFAULT 0,
  `customers-index` tinyint(1) DEFAULT 0,
  `customers-add` tinyint(1) DEFAULT 0,
  `customers-edit` tinyint(1) DEFAULT 0,
  `customers-delete` tinyint(1) DEFAULT 0,
  `suppliers-index` tinyint(1) DEFAULT 0,
  `suppliers-add` tinyint(1) DEFAULT 0,
  `suppliers-edit` tinyint(1) DEFAULT 0,
  `suppliers-delete` tinyint(1) DEFAULT 0,
  `sales-deliveries` tinyint(1) DEFAULT 0,
  `sales-add_delivery` tinyint(1) DEFAULT 0,
  `sales-edit_delivery` tinyint(1) DEFAULT 0,
  `sales-delete_delivery` tinyint(1) DEFAULT 0,
  `sales-email_delivery` tinyint(1) DEFAULT 0,
  `sales-pdf_delivery` tinyint(1) DEFAULT 0,
  `sales-gift_cards` tinyint(1) DEFAULT 0,
  `sales-add_gift_card` tinyint(1) DEFAULT 0,
  `sales-edit_gift_card` tinyint(1) DEFAULT 0,
  `sales-delete_gift_card` tinyint(1) DEFAULT 0,
  `pos-index` tinyint(1) DEFAULT 0,
  `pos-sales` tinyint(1) DEFAULT 0,
  `sales-return_sales` tinyint(1) DEFAULT 0,
  `reports-index` tinyint(1) DEFAULT 0,
  `reports-warehouse_stock` tinyint(1) DEFAULT 0,
  `reports-quantity_alerts` tinyint(1) DEFAULT 0,
  `reports-expiry_alerts` tinyint(1) DEFAULT 0,
  `reports-products` tinyint(1) DEFAULT 0,
  `reports-daily_sales` tinyint(1) DEFAULT 0,
  `reports-monthly_sales` tinyint(1) DEFAULT 0,
  `reports-sales` tinyint(1) DEFAULT 0,
  `reports-payments` tinyint(1) DEFAULT 0,
  `reports-purchases` tinyint(1) DEFAULT 0,
  `reports-profit_loss` tinyint(1) DEFAULT 0,
  `reports-customers` tinyint(1) DEFAULT 0,
  `reports-suppliers` tinyint(1) DEFAULT 0,
  `reports-staff` tinyint(1) DEFAULT 0,
  `reports-register` tinyint(1) DEFAULT 0,
  `reports-payment_term_expired` tinyint(1) DEFAULT 0,
  `sales-payments` tinyint(1) DEFAULT 0,
  `purchases-payments` tinyint(1) DEFAULT 0,
  `purchases-expenses` tinyint(1) DEFAULT 0,
  `products-adjustments` tinyint(1) NOT NULL DEFAULT 0,
  `bulk_actions` tinyint(1) NOT NULL DEFAULT 0,
  `customers-list_deposits` tinyint(1) NOT NULL DEFAULT 0,
  `customers-delete_deposit` tinyint(1) NOT NULL DEFAULT 0,
  `products-barcode` tinyint(1) NOT NULL DEFAULT 0,
  `purchases-return_purchases` tinyint(1) NOT NULL DEFAULT 0,
  `reports-expenses` tinyint(1) NOT NULL DEFAULT 0,
  `reports-daily_purchases` tinyint(1) DEFAULT 0,
  `reports-monthly_purchases` tinyint(1) DEFAULT 0,
  `products-stock_count` tinyint(1) DEFAULT 0,
  `edit_price` tinyint(1) DEFAULT 0,
  `returns-index` tinyint(1) DEFAULT 0,
  `returns-add` tinyint(1) DEFAULT 0,
  `returns-edit` tinyint(1) DEFAULT 0,
  `returns-delete` tinyint(1) DEFAULT 0,
  `returns-email` tinyint(1) DEFAULT 0,
  `returns-pdf` tinyint(1) DEFAULT 0,
  `reports-tax` tinyint(1) DEFAULT 0,
  `payments-index` tinyint(1) DEFAULT 0,
  `payments-add` tinyint(1) DEFAULT 0,
  `system_settings-categories` tinyint(1) DEFAULT 0,
  `system_settings-add_category` tinyint(1) DEFAULT 0,
  `system_settings-edit_category` tinyint(1) DEFAULT 0,
  `system_settings-delete_category` tinyint(1) DEFAULT 0,
  `system_settings-units` tinyint(1) DEFAULT 0,
  `system_settings-add_unit` tinyint(1) DEFAULT 0,
  `system_settings-edit_unit` tinyint(1) DEFAULT 0,
  `system_settings-delete_unit` tinyint(1) DEFAULT 0,
  `system_settings-brands` tinyint(1) DEFAULT 0,
  `system_settings-add_brand` tinyint(1) DEFAULT 0,
  `system_settings-edit_brand` tinyint(1) DEFAULT 0,
  `system_settings-delete_brand` tinyint(1) DEFAULT 0,
  `system_settings-tax_rates` tinyint(1) DEFAULT 0,
  `system_settings-add_tax_rate` tinyint(1) DEFAULT 0,
  `system_settings-edit_tax_rate` tinyint(1) DEFAULT 0,
  `system_settings-delete_tax_rate` tinyint(1) DEFAULT 0,
  `system_settings-payment_methods` tinyint(1) DEFAULT 0,
  `system_settings-add_payment_method` tinyint(1) DEFAULT 0,
  `system_settings-edit_payment_method` tinyint(1) DEFAULT 0,
  `system_settings-delete_payment_method` tinyint(1) DEFAULT 0,
  `reports-best_sellers` tinyint(1) DEFAULT 0,
  `reports-valued_products` tinyint(1) DEFAULT 0,
  `reports-adjustments` tinyint(1) DEFAULT 0,
  `reports-categories` tinyint(1) DEFAULT 0,
  `reports-brands` tinyint(1) DEFAULT 0,
  `reports-users` tinyint(1) DEFAULT 0,
  `reports-portfolio` tinyint(1) DEFAULT 0,
  `reports-portfolio_report` tinyint(1) DEFAULT 0,
  `reports-debts_to_pay_report` tinyint(1) DEFAULT 0,
  `reports-load_zeta` tinyint(1) DEFAULT 0,
  `reports-load_bills` tinyint(1) DEFAULT 0,
  `reports-load_rentabilidad_doc` tinyint(1) DEFAULT 0,
  `reports-load_rentabilidad_customer` tinyint(1) DEFAULT 0,
  `reports-load_rentabilidad_producto` tinyint(1) DEFAULT 0,
  `shop_settings-index` tinyint(1) DEFAULT 0,
  `shop_settings-slider` tinyint(1) DEFAULT 0,
  `shop_settings-pages` tinyint(1) DEFAULT 0,
  `shop_settings-add_page` tinyint(1) DEFAULT 0,
  `shop_settings-sms_settings` tinyint(1) DEFAULT 0,
  `shop_settings-send_sms` tinyint(1) DEFAULT 0,
  `shop_settings-sms_log` tinyint(1) DEFAULT 0,
  `shop_settings-edit_page` tinyint(1) DEFAULT 0,
  `shop_settings-delete_page` tinyint(1) DEFAULT 0,
  `sales-orders` tinyint(1) DEFAULT 0,
  `sales-add_order` tinyint(1) DEFAULT 0,
  `sales-edit_order` tinyint(1) DEFAULT 0,
  `products-production_orders` tinyint(1) DEFAULT 0,
  `products-add_production_order` tinyint(1) DEFAULT 0,
  `products-edit_production_order` tinyint(1) DEFAULT 0,
  `products-delete_production_order` tinyint(1) DEFAULT 0,
  `pos-add_wholesale` tinyint(1) DEFAULT 0,
  `payments-pindex` tinyint(1) DEFAULT 0,
  `payments-padd` tinyint(1) DEFAULT 0,
  `system_settings-update_products_group_prices` tinyint(1) DEFAULT 0,
  `products-edit_group_prices` tinyint(1) DEFAULT 0,
  `pos-pos_register_movements` tinyint(1) NOT NULL DEFAULT 0,
  `pos-pos_register_add_movement` tinyint(1) NOT NULL DEFAULT 0,
  `payments-cancel` tinyint(1) DEFAULT 0,
  `payments-pcancel` tinyint(1) DEFAULT 0,
  `pos_print_server` tinyint(1) NOT NULL DEFAULT 0,
  `products-product_transformations` tinyint(1) DEFAULT 0,
  `products-add_product_transformation` tinyint(1) DEFAULT 0,
  `returns-credit_note_other_concepts` tinyint(1) DEFAULT 0,
  `payroll_management-index` tinyint(1) DEFAULT 0,
  `payroll_management-add` tinyint(1) DEFAULT 0,
  `payroll_management-update` tinyint(1) DEFAULT 0,
  `payroll_management-approve` tinyint(1) DEFAULT 0,
  `payroll_management-add_novelty` tinyint(1) DEFAULT 0,
  `production_order-index` tinyint(1) DEFAULT NULL,
  `production_order-add` tinyint(1) DEFAULT NULL,
  `production_order-edit` tinyint(1) DEFAULT NULL,
  `production_order-cutting_orders` tinyint(1) DEFAULT NULL,
  `production_order-add_cutting_order` tinyint(1) DEFAULT NULL,
  `production_order-assemble_orders` tinyint(1) DEFAULT NULL,
  `production_order-add_assemble_order` tinyint(1) DEFAULT NULL,
  `production_order-packing_orders` tinyint(1) DEFAULT NULL,
  `production_order-add_packing_order` tinyint(1) DEFAULT NULL,
  `production_order-change_cutting_order_items_status` tinyint(1) DEFAULT NULL,
  `production_order-change_assemble_order_items_status` tinyint(1) DEFAULT NULL,
  `production_order-change_packing_order_items_status` tinyint(1) DEFAULT NULL,
  `production_order-production_order_report` tinyint(1) DEFAULT NULL,
  `customers-add_deposit` tinyint(1) DEFAULT NULL,
  `suppliers-list_deposits` tinyint(1) DEFAULT NULL,
  `suppliers-add_deposit` tinyint(1) DEFAULT NULL,
  `reports-collection_commissions` tinyint(1) DEFAULT NULL,
  `sales-pay_commissions` tinyint(1) DEFAULT NULL,
  `purchases-return_other_concepts` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%pos_register` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,4) NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_cash` decimal(25,4) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,4) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL,
  `refunds` decimal(25,4) DEFAULT 0.0000,
  `expenses` decimal(25,4) DEFAULT 0.0000,
  `purchases_payments` decimal(25,4) DEFAULT 0.0000,
  `deposits` decimal(25,4) DEFAULT 0.0000,
  `deposits_other_methods` decimal(25,4) DEFAULT 0.0000,
  `suppliers_deposits` decimal(25,4) DEFAULT 0.0000,
  `tips_cash` decimal(25,4) DEFAULT 0.0000,
  `tips_other_methods` decimal(25,4) DEFAULT 0.0000,
  `tips_due` decimal(25,4) DEFAULT 0.0000,
  `shipping_cash` decimal(25,4) DEFAULT 0.0000,
  `shipping_other_methods` decimal(25,4) DEFAULT 0.0000,
  `shipping_due` decimal(25,4) DEFAULT 0.0000,
  `movements_in` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `movements_out` decimal(25,4) DEFAULT 0.0000,
  `total_retention` decimal(25,4) NOT NULL,
  `total_return_retention` decimal(25,4) NOT NULL,
  `total_rc_retention` decimal(25,4) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%pos_register_items` (
  `id` int(11) NOT NULL,
  `pos_register_id` int(11) NOT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `payment_counted` int(1) DEFAULT 0 COMMENT '1. Es de contado, 0. No es de contado',
  `description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payments_amount` decimal(25,4) DEFAULT 0.0000,
  `sales_amount` decimal(25,4) DEFAULT 0.0000,
  `devolutions_amount` decimal(25,4) DEFAULT 0.0000,
  `total_amount` decimal(25,4) DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%pos_register_movements` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `reference_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biller_id` int(11) NOT NULL,
  `origin_paid_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination_paid_by` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movement_type` int(1) NOT NULL COMMENT '1. Entrada, 2. Salida, 3. Transferencia de caja',
  `amount` decimal(25,4) NOT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `cost_center_id` int(11) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `destination_biller_id` int(11) DEFAULT NULL,
  `destination_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%pos_settings` (
  `pos_id` int(1) NOT NULL,
  `cat_limit` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `default_section` int(11) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_biller` int(11) NOT NULL,
  `display_time` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `cf_title1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf_title2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf_value1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf_value2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_printer` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cash_drawer_codes` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `focus_add_item` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_manual_product` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_selection` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_customer` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toggle_category_slider` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toggle_subcategory_slider` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancel_sale` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suspend_sale` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_items_list` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finalize_sale` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `today_sale` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_hold_bills` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `close_register` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `pos_printers` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `product_button_color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `tooltips` tinyint(1) DEFAULT 1,
  `paypal_pro` tinyint(1) DEFAULT 0,
  `stripe` tinyint(1) DEFAULT 0,
  `rounding` tinyint(1) DEFAULT 0,
  `char_per_line` tinyint(4) DEFAULT 42,
  `pin_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'purchase_code',
  `envato_username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'envato_username',
  `version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '3.3.5',
  `after_sale_page` tinyint(1) DEFAULT 0,
  `item_order` tinyint(1) DEFAULT 0,
  `authorize` tinyint(1) DEFAULT 0,
  `toggle_brands_slider` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remote_printing` tinyint(1) DEFAULT 1,
  `printer` int(11) DEFAULT NULL,
  `order_printers` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_print` tinyint(1) DEFAULT 0,
  `customer_details` tinyint(1) DEFAULT NULL,
  `local_printers` tinyint(1) DEFAULT NULL,
  `balance_settings` int(1) DEFAULT 0 COMMENT '0. Sin balanza, 1. Balanza, 2. Balanza con etiqueta.',
  `balance_label_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_focus_quantity` decimal(6,4) DEFAULT 4.0000,
  `restobar_mode` int(1) DEFAULT 0 COMMENT '0. Inactivo, 1. Activo',
  `table_service` int(1) DEFAULT 0 COMMENT '0. Inactivo, 1. Activo',
  `order_print_mode` int(1) DEFAULT 0 COMMENT '0. A pantalla, 1. A impresora',
  `apply_suggested_tip` decimal(25,4) DEFAULT 0.0000,
  `apply_suggested_home_delivery_amount` decimal(25,4) DEFAULT 0.0000,
  `home_delivery_in_invoice` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
  `cloud_print_mail` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cloud_print_service_name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cloud_print_json_name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `printer_selected_direct_printing` int(11) DEFAULT 0,
  `detail_sale_by_category` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `show_suspended_bills_automatically` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `print_voucher_delivery` int(1) DEFAULT 0,
  `allow_print_command` int(1) DEFAULT 0,
  `express_payment` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `search_product_price` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `except_tip_restobar` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `required_shipping_restobar` int(1) DEFAULT 1,
  `mobile_print_automatically_invoice` int(1) DEFAULT 1 COMMENT '1. Si, 2. No',
  `use_barcode_scanner` int(1) NOT NULL DEFAULT 1 COMMENT '1. Si, 0. No',
  `product_clic_action` int(1) NOT NULL DEFAULT 1,
  `show_variants_and_preferences` int(1) NOT NULL DEFAULT 0,
  `order_to_table_default_restobar` int(1) NOT NULL DEFAULT 0 COMMENT '0:Inactivo, 1:Activo,',
  `mobile_print_wait_time` decimal(6,4) DEFAULT 0.0000,
  `show_client_modal_on_select` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `last_update` datetime NOT NULL DEFAULT current_timestamp(),
  `max_net_sale` decimal(25,4) DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%preferences` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%preferences_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%preparation_areas` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%price_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_group_base` int(1) DEFAULT 0,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%printers` (
  `id` int(11) NOT NULL,
  `title` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `char_per_line` tinyint(3) UNSIGNED DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varbinary(45) DEFAULT NULL,
  `port` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_cloud_print_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%production_order` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biller_id` int(11) NOT NULL,
  `est_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `warehouse_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Completado pendiente',
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%production_order_detail` (
  `id` int(11) NOT NULL,
  `production_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `production_order_pfinished_item_id` int(11) DEFAULT NULL COMMENT 'Relación al producto finalizado',
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `finished_quantity` decimal(15,4) DEFAULT 0.0000,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Completado pendiente',
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%production_order_notices` (
  `id` int(11) NOT NULL,
  `detail` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(1) NOT NULL COMMENT '1. Activo, 0. Inactivo',
  `register_id` int(11) NOT NULL,
  `register_type` int(1) NOT NULL COMMENT '1. Production order, 2. Cutting, 3. Assemble, 4. Packing'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%production_order_receipts` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ok_quantity` decimal(15,4) DEFAULT 0.0000,
  `fault_quantity` decimal(15,4) DEFAULT 0.0000,
  `type` int(1) NOT NULL COMMENT '1. Cutting, 2. Assemble, 3. Packing',
  `detail_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `packing_adjustment_id` int(11) DEFAULT NULL,
  `packing_warehouse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` int(11) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `avg_cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `alert_quantity` decimal(15,4) DEFAULT 20.0000,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cf6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT 0.0000,
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT 1,
  `details` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'code128',
  `file` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_details` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT 0,
  `type` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `promotion` tinyint(1) DEFAULT 0,
  `promo_price` decimal(25,4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `supplier1_part_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier2_part_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier3_part_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier4_part_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier5_part_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale_unit` int(11) DEFAULT NULL,
  `purchase_unit` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `slug` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `weight` decimal(10,4) DEFAULT NULL,
  `hsn_code` int(11) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `hide_detal` tinyint(1) NOT NULL DEFAULT 0,
  `hide_pos` tinyint(1) NOT NULL DEFAULT 0,
  `hide_online_store` tinyint(1) NOT NULL DEFAULT 0,
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_multiple_units` int(1) DEFAULT NULL COMMENT '1. Si, 2. No',
  `profitability_margin` decimal(5,2) DEFAULT 0.00,
  `ignore_hide_parameters` int(1) DEFAULT 0,
  `consumption_sale_tax` int(11) DEFAULT 0,
  `consumption_purchase_tax` int(11) DEFAULT 0,
  `discontinued` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `attributes` int(1) NOT NULL DEFAULT 0,
  `supplier1price_date` datetime DEFAULT NULL,
  `supplier2price_date` datetime DEFAULT NULL,
  `supplier3price_date` datetime DEFAULT NULL,
  `supplier4price_date` datetime DEFAULT NULL,
  `supplier5price_date` datetime DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%products_billers` (
  `biller_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%products_brands` (
  `product_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%products_recosting` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `warehouse_id` int(11) NOT NULL,
  `calculated_avg_cost` decimal(25,4) NOT NULL,
  `calculated_quantity` decimal(15,4) NOT NULL,
  `updated_by_csv` int(1) NOT NULL DEFAULT 0 COMMENT '1. Si, 0. No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%product_photos` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `photo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%product_preferences` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preference_id` int(11) DEFAULT NULL,
  `preference_category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%product_prices` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_group_id` int(11) NOT NULL,
  `price` decimal(25,4) NOT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT NULL,
  `suffix` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%purchases` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT 0.0000,
  `shipping` decimal(25,4) DEFAULT 0.0000,
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `status` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'received, pending',
  `payment_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_term` int(11) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `return_purchase_ref` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `return_purchase_total` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `closed_year` smallint(1) DEFAULT 0 COMMENT '0= Abierto  1=cerrado',
  `rete_fuente_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_fuente_total` decimal(25,4) DEFAULT 0.0000,
  `rete_fuente_account` int(11) DEFAULT 0,
  `rete_fuente_base` decimal(25,4) DEFAULT NULL,
  `rete_iva_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_iva_total` decimal(25,4) DEFAULT 0.0000,
  `rete_iva_account` int(11) DEFAULT 0,
  `rete_iva_base` decimal(25,4) DEFAULT NULL,
  `rete_ica_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_ica_total` decimal(25,4) DEFAULT 0.0000,
  `rete_ica_account` int(11) DEFAULT 0,
  `rete_ica_base` decimal(25,4) DEFAULT NULL,
  `rete_other_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_other_total` decimal(25,4) DEFAULT 0.0000,
  `rete_other_account` int(11) DEFAULT 0,
  `rete_other_base` decimal(25,4) DEFAULT NULL,
  `order_discount_method` int(1) DEFAULT 2 COMMENT '1. Aplicar a cada producto, 2. Aplicar al total de la factura',
  `purchase_currency` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_currency_trm` decimal(25,4) DEFAULT NULL,
  `purchase_type` int(1) DEFAULT 1 COMMENT '1. Productos, 2. Gastos',
  `credit_ledger_id` int(11) DEFAULT NULL COMMENT 'Histórico del ledger parametrizado para la forma de pago escogida.',
  `payment_affects_register` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
  `cost_center_id` int(11) DEFAULT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `consecutive_supplier` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  `rete_fuente_id` int(11) DEFAULT NULL,
  `rete_iva_id` int(11) DEFAULT NULL,
  `rete_ica_id` int(11) DEFAULT NULL,
  `rete_other_id` int(11) DEFAULT NULL,
  `purchase_origin` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_origin_reference_no` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prorated_shipping_cost` decimal(25,4) DEFAULT 0.0000,
  `consumption_purchase` decimal(25,4) DEFAULT NULL,
  `resolucion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due_payment_method_id` int(11) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `expense_causation` int(11) DEFAULT NULL COMMENT '1. Si, 0. No',
  `return_other_concepts` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1: Si,',
  `rete_fuente_assumed` int(1) DEFAULT NULL,
  `rete_iva_assumed` int(1) DEFAULT NULL,
  `rete_ica_assumed` int(1) DEFAULT NULL,
  `rete_other_assumed` int(1) DEFAULT NULL,
  `rete_fuente_assumed_account` int(11) DEFAULT NULL,
  `rete_iva_assumed_account` int(11) DEFAULT NULL,
  `rete_ica_assumed_account` int(11) DEFAULT NULL,
  `rete_other_assumed_account` int(11) DEFAULT NULL,
  `rete_bomberil_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_bomberil_total` decimal(25,4) DEFAULT 0.0000,
  `rete_bomberil_account` int(11) DEFAULT 0,
  `rete_bomberil_base` decimal(25,4) DEFAULT NULL,
  `rete_autoaviso_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_autoaviso_total` decimal(25,4) DEFAULT 0.0000,
  `rete_autoaviso_account` int(11) DEFAULT 0,
  `rete_autoaviso_base` decimal(25,4) DEFAULT NULL,
  `rete_bomberil_id` int(11) DEFAULT NULL,
  `rete_autoaviso_id` int(11) DEFAULT NULL,
  `rete_bomberil_assumed_account` int(11) DEFAULT NULL,
  `rete_autoaviso_assumed_account` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%purchases_return` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `purchase_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_id` int(11) NOT NULL,
  `return_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_tax_2` decimal(25,4) DEFAULT NULL,
  `tax_rate_2_id` int(11) DEFAULT NULL,
  `tax_2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT 0.0000,
  `date` date NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `quantity_received` decimal(15,4) DEFAULT NULL,
  `supplier_part_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `product_unit_id_selected` int(11) DEFAULT NULL,
  `shipping_unit_cost` decimal(25,4) DEFAULT 0.0000,
  `profitability_margin` decimal(5,2) DEFAULT 0.00,
  `consumption_purchase` decimal(25,4) DEFAULT NULL,
  `returned_quantity` decimal(15,4) DEFAULT 0.0000,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `expense_category_creditor_ledger_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%quotes` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal_note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT 0.0000,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT 0.0000,
  `product_tax` decimal(25,4) DEFAULT 0.0000,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT 0.0000,
  `grand_total` decimal(25,4) NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `quote_currency` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quote_currency_trm` decimal(25,4) DEFAULT NULL,
  `quote_type` int(1) DEFAULT 1 COMMENT '1. Productos, 2. Gastos',
  `rete_fuente_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_fuente_total` decimal(25,4) DEFAULT 0.0000,
  `rete_fuente_account` int(11) DEFAULT 0,
  `rete_fuente_base` decimal(25,4) DEFAULT NULL,
  `rete_iva_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_iva_total` decimal(25,4) DEFAULT 0.0000,
  `rete_iva_account` int(11) DEFAULT 0,
  `rete_iva_base` decimal(25,4) DEFAULT NULL,
  `rete_ica_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_ica_total` decimal(25,4) DEFAULT 0.0000,
  `rete_ica_account` int(11) DEFAULT 0,
  `rete_ica_base` decimal(25,4) DEFAULT NULL,
  `rete_other_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_other_total` decimal(25,4) DEFAULT 0.0000,
  `rete_other_account` int(11) DEFAULT 0,
  `rete_other_base` decimal(25,4) DEFAULT NULL,
  `rete_fuente_id` int(11) DEFAULT NULL,
  `rete_iva_id` int(11) DEFAULT NULL,
  `rete_ica_id` int(11) DEFAULT NULL,
  `rete_other_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  `destination_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` int(1) NOT NULL DEFAULT 0 COMMENT '1. Contado, 0. Credito',
  `payment_term` int(4) NOT NULL,
  `payment_method` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_tax_2` decimal(25,4) DEFAULT NULL,
  `tax_rate_2_id` int(11) DEFAULT NULL,
  `tax_2` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `price_before_tax` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%returns` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT 0.0000,
  `order_discount_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT 0.0000,
  `order_discount` decimal(25,4) DEFAULT 0.0000,
  `product_tax` decimal(25,4) DEFAULT 0.0000,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT 0.0000,
  `total_tax` decimal(25,4) DEFAULT 0.0000,
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `paid` decimal(25,4) DEFAULT 0.0000,
  `surcharge` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%return_items` (
  `id` int(11) NOT NULL,
  `return_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%sales` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT 0.0000,
  `order_discount_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT 0.0000,
  `order_discount` decimal(25,4) DEFAULT 0.0000,
  `product_tax` decimal(25,4) DEFAULT 0.0000,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT 0.0000,
  `total_tax` decimal(25,4) DEFAULT 0.0000,
  `shipping` decimal(25,4) DEFAULT 0.0000,
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Completado pendiente',
  `payment_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Pendiente Debido Parcial Pagado',
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT 0,
  `paid` decimal(25,4) DEFAULT 0.0000,
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_sale_ref` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `return_sale_total` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `rounding` decimal(10,4) DEFAULT NULL,
  `suspend_note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api` tinyint(1) DEFAULT 0,
  `shop` tinyint(1) DEFAULT 0,
  `address_id` int(11) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `reserve_id` int(11) DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manual_payment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `payment_method` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT '''''' COMMENT 'Ordenes de pedido tienda online',
  `pay_partner` smallint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 =PENDIENTE  1 = PAGADO',
  `rete_fuente_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_fuente_total` decimal(25,4) DEFAULT 0.0000,
  `rete_fuente_account` int(11) DEFAULT 0,
  `rete_fuente_base` decimal(25,4) DEFAULT NULL,
  `rete_iva_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_iva_total` decimal(25,4) DEFAULT 0.0000,
  `rete_iva_account` int(11) DEFAULT 0,
  `rete_iva_base` decimal(25,4) DEFAULT NULL,
  `rete_ica_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_ica_total` decimal(25,4) DEFAULT 0.0000,
  `rete_ica_account` int(11) DEFAULT 0,
  `rete_ica_base` decimal(25,4) DEFAULT NULL,
  `rete_other_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_other_total` decimal(25,4) DEFAULT 0.0000,
  `rete_other_account` int(11) DEFAULT 0,
  `rete_other_base` decimal(25,4) DEFAULT NULL,
  `resolucion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fe_correo_enviado` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1: Si,',
  `fe_aceptado` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Confirmación de la DIAN. 0: No enviado, 1: Pendiente, 2: Aceptado, 3: Enviado,',
  `fe_recibido` tinyint(1) DEFAULT NULL COMMENT '0: No, 1: Si, NULL: Pendiente,',
  `cufe` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Cufe devuelto por la DIAN',
  `codigo_qr` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Código QR base 64',
  `fe_id_transaccion` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Código empleado para facturación electrónica',
  `fe_mensaje` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Código empleado para facturación electrónica',
  `fe_mensaje_soporte_tecnico` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mensaje de ayuda para soporte técnico',
  `fe_xml` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Archivo XML en base 64',
  `fe_debit_credit_note_concept_dian_code` int(11) DEFAULT NULL COMMENT 'Código DIAN para el concepto de la nota crédito o débito.',
  `sale_currency` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale_currency_trm` decimal(25,4) DEFAULT NULL,
  `cost_center_id` int(11) DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  `payment_method_fe` int(1) NOT NULL COMMENT '1: Contado, 2: Crédito,',
  `payment_mean_fe` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código empleado para facturación electrónica',
  `tip_amount` decimal(25,4) DEFAULT 0.0000,
  `shipping_in_grand_total` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
  `sale_origin` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale_origin_reference_no` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rete_fuente_id` int(11) DEFAULT NULL,
  `rete_iva_id` int(11) DEFAULT NULL,
  `rete_ica_id` int(11) DEFAULT NULL,
  `rete_other_id` int(11) DEFAULT NULL,
  `sale_comm_perc` decimal(5,2) DEFAULT 0.00,
  `collection_comm_perc` decimal(5,2) DEFAULT 0.00,
  `sale_comm_amount` decimal(25,4) DEFAULT 0.0000,
  `sale_comm_payment_status` int(1) DEFAULT 0,
  `debit_note_id` int(11) DEFAULT NULL COMMENT 'Identificador de la nota débito de referencia.',
  `reference_debit_note` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'referencia de la nota débito',
  `reference_invoice_id` int(11) DEFAULT NULL COMMENT 'Identificador de la factura de referencia.',
  `restobar_table_id` int(11) DEFAULT NULL,
  `consumption_sales` decimal(25,4) DEFAULT NULL,
  `self_withholding_amount` decimal(25,4) DEFAULT 0.0000,
  `self_withholding_percentage` decimal(25,4) DEFAULT 0.0000,
  `printed` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1: Si,',
  `due_payment_method_id` int(11) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `return_other_concepts` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1: Si,',
  `rete_bomberil_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_bomberil_total` decimal(25,4) DEFAULT 0.0000,
  `rete_bomberil_account` int(11) DEFAULT 0,
  `rete_bomberil_base` decimal(25,4) DEFAULT NULL,
  `rete_autoaviso_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_autoaviso_total` decimal(25,4) DEFAULT 0.0000,
  `rete_autoaviso_account` int(11) DEFAULT 0,
  `rete_autoaviso_base` decimal(25,4) DEFAULT NULL,
  `rete_autoica_percentage` decimal(6,3) DEFAULT 0.000,
  `rete_autoica_total` decimal(25,4) DEFAULT 0.0000,
  `rete_autoica_account` int(11) DEFAULT 0,
  `rete_autoica_base` decimal(25,4) DEFAULT NULL,
  `rete_autoica_account_counterpart` int(11) DEFAULT 0,
  `rete_autoaviso_account_counterpart` int(11) DEFAULT 0,
  `rete_bomberil_account_counterpart` int(11) DEFAULT 0,
  `rete_bomberil_id` int(11) DEFAULT NULL,
  `rete_autoaviso_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%sales_brands_view` (
`month` int(2)
,`biller_id` int(11)
,`brand` int(11)
,`name` varchar(50)
,`subtotal` decimal(48,4)
,`number_transactions` bigint(21)
);
CREATE TABLE `%_PREFIX_%sales_budget` (
  `id` int(11) NOT NULL,
  `year` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `month` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biller_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `amount` decimal(25,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%sales_categories_view` (
`month` int(2)
,`biller_id` int(11)
,`category_id` int(11)
,`name` varchar(55)
,`subtotal` decimal(48,4)
,`number_transactions` bigint(21)
);
CREATE TABLE `%_PREFIX_%sales_customers_view` (
`month` int(2)
,`biller_id` int(11)
,`customer_id` int(11)
,`name` varchar(55)
,`subtotal` decimal(48,4)
,`number_transactions` bigint(21)
);
CREATE TABLE `%_PREFIX_%sales_debit_notes` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `sale_reference_no` varchar(45) NOT NULL,
  `debit_note_id` int(11) NOT NULL,
  `debit_note_reference_no` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Relación entre facturas y notas débito';
CREATE TABLE `%_PREFIX_%sales_document_type_view` (
`month` int(2)
,`document_type_id` int(11)
,`biller_id` int(11)
,`sales_prefix` varchar(45)
,`subtotal` decimal(47,4)
,`tax` decimal(47,4)
);
CREATE TABLE `%_PREFIX_%sales_mean_payments_view` (
`month` int(2)
,`biller_id` int(11)
,`code` varchar(45)
,`name` varchar(45)
,`subtotal` decimal(48,4)
);
CREATE TABLE `%_PREFIX_%sales_products_view` (
`month` int(2)
,`biller_id` int(11)
,`product_id` int(11) unsigned
,`name` varchar(255)
,`subtotal` decimal(48,4)
,`number_transactions` bigint(21)
);
CREATE TABLE `%_PREFIX_%sales_returns` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `sale_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_id` int(11) NOT NULL,
  `return_reference_no` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%sales_sellers_view` (
`month` int(2)
,`biller_id` int(11)
,`seller_id` int(11)
,`name` varchar(55)
,`subtotal` decimal(48,4)
,`number_transactions` bigint(21)
);
CREATE TABLE `%_PREFIX_%sales_tax_view` (
`month` int(2)
,`biller_id` int(11)
,`tax_rate_id` int(11)
,`tax_name` varchar(55)
,`total_tax` decimal(47,4)
);
CREATE TABLE `%_PREFIX_%sales_view` (
`number_transactions` bigint(21)
,`biller_id` int(11)
,`year` int(4)
,`month` int(2)
,`day` int(2)
,`subtotal` decimal(47,4)
,`tax` decimal(47,4)
,`shipping` decimal(47,4)
,`discount` decimal(47,4)
,`returned_total` decimal(47,4)
,`cost_subtotal` decimal(62,8)
,`cost_total` decimal(62,8)
);
CREATE TABLE `%_PREFIX_%sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_tax_2` decimal(25,4) DEFAULT NULL,
  `tax_rate_2_id` int(11) DEFAULT NULL,
  `tax_2` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `unit_order_discount` decimal(25,4) DEFAULT NULL,
  `avg_net_unit_cost` decimal(25,4) DEFAULT NULL,
  `state_readiness` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1: Pedido, 2. Preparación, 3: Cancelado, 4: Despachado',
  `preferences` varchar(245) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_before_tax` decimal(25,4) DEFAULT 0.0000,
  `product_unit_id_selected` int(11) DEFAULT NULL,
  `consumption_sales` decimal(25,4) DEFAULT NULL,
  `consumption_sales_costing` decimal(25,4) DEFAULT NULL,
  `returned_quantity` decimal(15,4) DEFAULT NULL,
  `price_before_promo` decimal(25,4) DEFAULT 0.0000,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `under_cost_authorized` int(1) DEFAULT 0 COMMENT '1. Si, 0. No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%sequential_count` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_names` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brands` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_names` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `apply` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 = No aplicado 1 = aplicado',
  `biller_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
CREATE TABLE `%_PREFIX_%sequential_count_items` (
  `id` int(11) NOT NULL,
  `adjustment_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
CREATE TABLE `%_PREFIX_%sessions` (
  `id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_warehouse` int(2) NOT NULL,
  `accounting_method` tinyint(4) NOT NULL DEFAULT 0,
  `default_currency` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `version` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1.0',
  `default_tax_rate2` int(11) NOT NULL DEFAULT 0,
  `dateformat` int(11) NOT NULL,
  `sales_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quote_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quote_purchase_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_sale_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfer_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `returnp_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expense_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_addition` tinyint(1) NOT NULL DEFAULT 0,
  `theme` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_serial` tinyint(4) NOT NULL,
  `default_discount` int(11) NOT NULL,
  `product_discount` tinyint(1) NOT NULL DEFAULT 0,
  `discount_method` tinyint(4) NOT NULL,
  `tax1` tinyint(4) NOT NULL,
  `tax2` tinyint(4) NOT NULL,
  `tax3` tinyint(4) NOT NULL,
  `overselling` tinyint(1) NOT NULL DEFAULT 0,
  `restrict_user` tinyint(4) NOT NULL DEFAULT 0,
  `restrict_calendar` tinyint(4) NOT NULL DEFAULT 0,
  `timezone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iwidth` int(11) NOT NULL DEFAULT 0,
  `iheight` int(11) NOT NULL,
  `twidth` int(11) NOT NULL,
  `theight` int(11) NOT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `reg_ver` tinyint(1) DEFAULT NULL,
  `allow_reg` tinyint(1) DEFAULT NULL,
  `reg_notification` tinyint(1) DEFAULT NULL,
  `auto_reg` tinyint(1) DEFAULT NULL,
  `protocol` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'mail',
  `mailpath` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT '/usr/sbin/sendmail',
  `smtp_host` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_user` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_pass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_port` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '25',
  `smtp_crypto` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `corn` datetime DEFAULT NULL,
  `customer_group` int(11) NOT NULL,
  `default_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mmode` tinyint(1) NOT NULL,
  `bc_fix` tinyint(4) NOT NULL DEFAULT 0,
  `auto_detect_barcode` tinyint(1) NOT NULL DEFAULT 0,
  `captcha` tinyint(1) NOT NULL DEFAULT 1,
  `reference_format` tinyint(1) NOT NULL DEFAULT 1,
  `racks` tinyint(1) DEFAULT 0,
  `attributes` tinyint(1) NOT NULL DEFAULT 0,
  `product_expiry` tinyint(1) NOT NULL DEFAULT 0,
  `decimals` tinyint(2) NOT NULL DEFAULT 2,
  `qty_decimals` tinyint(2) NOT NULL DEFAULT 2,
  `decimals_sep` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '.',
  `thousands_sep` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ',',
  `invoice_view` tinyint(1) DEFAULT 0,
  `default_biller` int(11) DEFAULT NULL,
  `envato_username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rtl` tinyint(1) DEFAULT 0,
  `each_spent` decimal(15,4) DEFAULT NULL,
  `ca_point` tinyint(4) DEFAULT NULL,
  `each_sale` decimal(15,4) DEFAULT NULL,
  `sa_point` tinyint(4) DEFAULT NULL,
  `update` tinyint(1) DEFAULT 0,
  `sac` tinyint(1) DEFAULT 0,
  `display_all_products` tinyint(1) DEFAULT 0,
  `display_symbol` tinyint(1) DEFAULT NULL,
  `symbol` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remove_expired` tinyint(1) DEFAULT 0,
  `barcode_separator` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_',
  `set_focus` tinyint(1) NOT NULL DEFAULT 0,
  `price_group` int(11) DEFAULT NULL,
  `barcode_img` tinyint(1) NOT NULL DEFAULT 1,
  `ppayment_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'POP',
  `disable_editing` smallint(6) DEFAULT 90,
  `qa_prefix` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_cost` tinyint(1) DEFAULT NULL,
  `apis` tinyint(1) NOT NULL DEFAULT 0,
  `pdf_lib` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `%_PREFIX_%payment_prefix` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deposit_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cashier_close` tinyint(1) DEFAULT 0 COMMENT '1. Si, 0. No, 2. No se usa POS',
  `alert_sale_expired` int(3) DEFAULT 0 COMMENT 'Mínimo de días para avisar cuando el plazo de una factura esté por vencerse.',
  `tipo_regimen` int(1) DEFAULT 1 COMMENT '1: Simplificado, 2: Común ,',
  `purchase_tax_rate` int(11) DEFAULT 1,
  `modulary` int(1) DEFAULT 0,
  `resolucion_porc_aviso` decimal(4,2) DEFAULT 0.00 COMMENT 'Porcentaje de aviso para vencimiento de resolución por numeración.',
  `resolucion_dias_aviso` int(3) DEFAULT 0 COMMENT 'Días de aviso para vencimiento de resolución por fecha de vencimiento.',
  `prioridad_precios_producto` int(1) DEFAULT 1 COMMENT '1. Lista precio base + promociones, 2. Lista de precios sucursal, 3. Lista de precios cliente, 4. Lista de precios sucursal o cliente con descuentos y promociones, 5. Precios por unidad',
  `descuento_orden` int(1) DEFAULT 1 COMMENT '1. Afectando IVA, 2. Sin afectar IVA',
  `allow_change_sale_iva` int(1) DEFAULT 0,
  `tax_rate_traslate` int(1) DEFAULT 0 COMMENT '1. Si, 2. No',
  `get_companies_check_digit` int(1) DEFAULT 1 COMMENT '1. Si, 2. No',
  `tipo_persona` tinyint(1) NOT NULL COMMENT '1: Jurídica, 2: Natural,',
  `tipo_documento` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `digito_verificacion` tinyint(1) DEFAULT NULL COMMENT 'Digito de verificación del NIT',
  `numero_documento` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Sin puntos. Sin comas. Sin dígito de verificación.',
  `razon_social` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_comercial` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primer_nombre` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundo_nombre` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primer_apellido` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundo_apellido` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `matricula_mercantil` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `localidad` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_code` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departamento` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allow_advanced_search` int(1) NOT NULL DEFAULT 0 COMMENT '1: SI, 0: No,',
  `rounding` int(1) DEFAULT 0,
  `purchase_payment_affects_cash_register` int(1) DEFAULT 2 COMMENT '0. No, 1. Si, 2. Preguntar siempre',
  `digital_signature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_num_results_display` int(3) DEFAULT 15,
  `cost_center_selection` int(1) DEFAULT 2 COMMENT '0. Definido en Sucursal, 1. Definir en cada registro, 2. Desactivar',
  `hide_products_in_zero_price` int(1) DEFAULT 0,
  `default_cost_center` int(11) DEFAULT NULL,
  `tax_rate_translate` int(1) DEFAULT 0 COMMENT '1. Si, 2. No',
  `electronic_billing` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: No, 1: Si,',
  `fe_technology_provider` int(11) DEFAULT NULL,
  `fe_user` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Usuario para facturación electrónica',
  `fe_password` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Contraseña para la facturación electronica',
  `tax_method` int(1) DEFAULT 2 COMMENT '0. IVA incluído, 1. Antes de IVA, 2. Escoger en cada producto',
  `show_alert_sale_expired` int(1) DEFAULT 1 COMMENT '0. No mostrar, 1. Mostrar',
  `customer_default_country` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_default_state` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_default_city` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `set_adjustment_cost` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `default_records_filter` int(1) DEFAULT 0 COMMENT '0. Todos los registros, 1. Registros de Hoy, 2. Registros del mes, 3. Registros del último trimestre, 4. Registros del año actual, 5. Filtrado por rango de fechas',
  `show_brand_in_product_search` int(1) DEFAULT 0,
  `close_year_step` int(1) DEFAULT 0 COMMENT '1. Paso de tablas y datos, 2. Paso de inventario, 3. Paso de cuentas por cobrar, 4. Paso de Cuentas por pagar',
  `closed_from_another_year` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `precios_por_unidad_presentacion` int(1) DEFAULT NULL COMMENT '1. Unidad, 2. Presentación',
  `lock_cost_field_in_product` int(1) DEFAULT 1,
  `fuente_retainer` int(1) DEFAULT 0,
  `iva_retainer` int(1) DEFAULT 0,
  `ica_retainer` int(1) DEFAULT 0,
  `export_to_csv_each_new_customer` int(1) DEFAULT 0,
  `commision_payment_method` int(1) DEFAULT 1 COMMENT '1. Facturas completamente pagas, 2. Facturas parciales',
  `default_expense_id_to_pay_commisions` int(11) DEFAULT NULL,
  `margin_price_formula` int(1) DEFAULT 0,
  `users_can_view_price_groups` int(1) DEFAULT 0,
  `users_can_view_warehouse_quantitys` int(1) DEFAULT 0,
  `product_variant_language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variants_language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_electronic_invoice_to` int(1) DEFAULT 1 COMMENT '1. Correo principal del cliente, 2. Correo de la sucursal del cliente',
  `copy_mail_to_sender` int(1) NOT NULL DEFAULT 0,
  `days_before_current_date` int(11) NOT NULL DEFAULT 0,
  `days_after_current_date` int(11) NOT NULL DEFAULT 0,
  `add_individual_attachments` tinyint(1) NOT NULL DEFAULT 0,
  `product_crud_validate_cost` int(1) DEFAULT NULL,
  `alert_zero_cost_sale` int(1) DEFAULT NULL COMMENT '1. Si, 0. No',
  `customer_branch_language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_branches_language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barcode_reader_exact_search` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `which_cost_margin_validation` int(1) DEFAULT 1 COMMENT '1. último costo, 2. costo promedio',
  `max_minutes_random_pin_code` int(10) DEFAULT 5,
  `cron_job_db_backup` int(1) DEFAULT 1,
  `cron_job_images_backup` int(1) DEFAULT 1,
  `cron_job_ebilling_backup` int(1) DEFAULT 1,
  `cron_job_send_mail` int(1) DEFAULT 1,
  `cron_job_db_backup_partitions` int(2) DEFAULT 1,
  `purchase_send_advertisement` int(1) DEFAULT NULL COMMENT '1 Si, 0 No',
  `ignore_purchases_edit_validations` int(1) DEFAULT 0,
  `big_data_limit_reports` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `fe_work_environment` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1: Producción, 2: Pruebas,',
  `fe_technical_annex_version` tinyint(1) NOT NULL DEFAULT 1,
  `management_consecutive_suppliers` int(1) DEFAULT 0,
  `url_web` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profitability_margin_validation` int(1) DEFAULT NULL,
  `prorate_shipping_cost` int(1) DEFAULT 0,
  `session_expiration_time` int(3) DEFAULT 15 COMMENT 'Valor en minutos',
  `product_order` int(1) DEFAULT 1 COMMENT '1. Ascendente, 2. Descendente',
  `keep_seller_from_user` int(1) DEFAULT 0 COMMENT '0. No, 1. Si',
  `invoice_values_greater_than_zero` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
  `except_category_taxes` int(1) DEFAULT 0 COMMENT '0. No, 1. Si',
  `category_tax_exception_start_date` date DEFAULT NULL,
  `category_tax_exception_end_date` date DEFAULT NULL,
  `category_tax_exception_tax_rate_id` int(11) DEFAULT NULL,
  `customer_territorial_decree_tax_rate_id` int(11) DEFAULT NULL,
  `ipoconsumo` int(11) DEFAULT NULL,
  `default_return_payment_method` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `manual_payment_reference` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `self_withholding_percentage` decimal(25,4) DEFAULT 0.0000,
  `item_discount_apply_to` int(1) NOT NULL DEFAULT 2 COMMENT '1. Base IVA, 2. Total precio',
  `gift_card_language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `automatic_update` int(1) NOT NULL DEFAULT 0,
  `wappsi_rest_url` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dropbox_key` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dropbox_secret` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dropbox_token` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail_due_sales` int(1) DEFAULT 0,
  `detail_partial_sales` int(1) DEFAULT 0,
  `detail_paid_sales` int(1) DEFAULT 0,
  `detail_products_expirated` int(1) DEFAULT 0,
  `detail_products_promo_expirated` int(1) DEFAULT 0,
  `detail_sales_per_biller` int(1) DEFAULT 0,
  `detail_pos_registers` int(1) DEFAULT 0,
  `detail_zeta_report` int(1) DEFAULT 0,
  `great_contributor` int(1) NOT NULL DEFAULT 0,
  `annual_closure` int(1) NOT NULL DEFAULT 0,
  `product_variant_per_serial` int(1) NOT NULL DEFAULT 0,
  `product_search_show_quantity` int(1) DEFAULT 1 COMMENT '1. No 0. Si',
  `suspended_sales_to_retail` int(1) DEFAULT 1 COMMENT '0. No, 1. Si',
  `payments_methods_retcom` int(11) NOT NULL DEFAULT 0 COMMENT 'Retenciones y comisiones en formas de pago 1. Si, 0. No',
  `product_default_exempt_tax_rate` int(11) NOT NULL,
  `ciiu_code` int(11) DEFAULT NULL,
  `production_order_one_reference_limit` int(1) DEFAULT NULL COMMENT '1. Si, 0. No',
  `system_start_date` date DEFAULT '2018-01-01',
  `set_product_variant_suffix` int(1) DEFAULT 0 COMMENT '0. No, 1. Si',
  `disable_product_price_under_cost` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
  `wms_picking_filter_biller` int(1) DEFAULT 0 COMMENT '1. Si, 0. No',
  `accounting_year_suffix` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_customer_tax_exemption` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
  `block_movements_from_another_year` int(1) DEFAULT 0,
  `product_transformation_validate_quantity` int(1) DEFAULT 1 COMMENT '1. Si, 0. No',
  `rete_other_language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp(),
  `customer_default_payment_type` int(1) DEFAULT 0 COMMENT '1. Efectivo, 0. Credito',
  `customer_default_payment_term` decimal(25,4) DEFAULT 0.0000,
  `customer_default_credit_limit` decimal(25,4) DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%settings_soc` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `base_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `max_paytime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `max_comperc` decimal(5,2) NOT NULL DEFAULT 0.00,
  `max_wtihdraw` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `withdraw_perc` decimal(5,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
CREATE TABLE `%_PREFIX_%shop_settings` (
  `shop_id` int(11) NOT NULL,
  `description` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biller` int(11) NOT NULL,
  `about_link` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `terms_link` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `privacy_link` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_link` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_text` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `follow_text` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cookie_message` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cookie_link` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` int(11) DEFAULT NULL,
  `purchase_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'purchase_code',
  `envato_username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'envato_username',
  `version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '3.3.5',
  `bank_details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products_page` tinyint(1) DEFAULT NULL,
  `hide0` tinyint(1) DEFAULT 0,
  `show_product_quantity` tinyint(1) DEFAULT 0 COMMENT '0. No mostrar existencia, 1. Mostrar sólo de la bodega de la tienda, 2. Mostrar la suma de todas las bodegas',
  `show_product_zero_price` tinyint(1) DEFAULT 1 COMMENT '0. Ocultar, 1. Mostrar',
  `show_product_zero_quantity` tinyint(1) DEFAULT 1 COMMENT '0. Ocultar, 1. Mostrar',
  `show_categories_without_products` tinyint(1) DEFAULT 0,
  `overselling` tinyint(1) DEFAULT 0 COMMENT '0. No, 1. Si',
  `use_product_variants` tinyint(1) DEFAULT 1 COMMENT '0. No, 1. Si',
  `use_product_preferences` tinyint(1) DEFAULT 1 COMMENT '0. No, 1. Si',
  `primary_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bar_menu_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `font_text_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondary_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `font_family` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT 'Roboto',
  `background_page_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header_message_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_view` tinyint(1) DEFAULT 1 COMMENT '1. Horizontal, 2. Vertical',
  `shop_favicon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'icon.png',
  `navbar_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `navbar_text_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bar_menu_text_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_text_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_text_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favorite_color` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'mbtest',
  `skrill_currency` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%sms_settings` (
  `id` int(11) NOT NULL,
  `auto_send` tinyint(1) DEFAULT NULL,
  `config` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%states` (
  `CODDEPARTAMENTO` varchar(5) NOT NULL DEFAULT '',
  `DEPARTAMENTO` varchar(50) NOT NULL DEFAULT '',
  `PAIS` varchar(3) NOT NULL DEFAULT '',
  `DESADICIONAL` varchar(20) DEFAULT '' COMMENT 'DESCRIPCION ADICIONAL'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%stock_counts` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `initial_file` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `final_file` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brands` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_names` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_names` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products` int(11) DEFAULT NULL,
  `rows` int(11) DEFAULT NULL,
  `differences` int(11) DEFAULT NULL,
  `matches` int(11) DEFAULT NULL,
  `missing` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `finalized` tinyint(1) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `automatic_adjustment` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%stock_count_items` (
  `id` int(11) NOT NULL,
  `stock_count_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variant` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variant_id` int(11) DEFAULT NULL,
  `expected` decimal(15,4) NOT NULL,
  `counted` decimal(15,4) NOT NULL,
  `cost` decimal(25,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%subzones` (
  `zone_code` varchar(50) NOT NULL,
  `subzone_name` varchar(55) NOT NULL,
  `subzone_code` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%suspended_bills` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` int(11) NOT NULL,
  `order_discount_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `suspend_note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` decimal(15,4) DEFAULT 0.0000,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `table_id` int(11) DEFAULT NULL COMMENT 'Restobar: Mesa asociada a la cuenta.',
  `state` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1: Ordenado, 2: Preparación,',
  `seller_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `consumption_sales` decimal(25,4) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%suspended_items` (
  `id` int(11) NOT NULL,
  `suspend_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT 0.0000,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_tax_2` decimal(25,4) DEFAULT NULL,
  `tax_rate_2_id` int(11) DEFAULT NULL,
  `tax_2` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `state_readiness` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1: Pedido, 2. Preparación, 3: Cancelado, 4: Despachado',
  `preferences` varchar(245) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_before_tax` decimal(25,4) DEFAULT 0.0000,
  `product_unit_id_selected` int(11) DEFAULT NULL,
  `consumption_sales` decimal(25,4) DEFAULT NULL,
  `consumption_sales_costing` decimal(25,4) DEFAULT NULL,
  `price_before_promo` decimal(25,4) DEFAULT 0.0000,
  `browser_readiness_status` tinyint(1) DEFAULT NULL COMMENT '1: Pedido, 2. Preparación, 3: Cancelado, 4: Despachado',
  `under_cost_authorized` int(1) DEFAULT 0 COMMENT '1. Si, 0. No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%system_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%tables` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL COMMENT 'Número que aparecerá en la interfaz gráfica',
  `forma_mesa` int(11) NOT NULL COMMENT 'Figura geométrica de la mesa',
  `numero_asientos` int(11) NOT NULL COMMENT 'Cantidad de puestos, asientos o sillas',
  `imagen` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ruta de la imagen',
  `estado` int(1) NOT NULL DEFAULT 1 COMMENT '1: Disponible, 2. No disponible, 3: Ocupado, 4: Reservado',
  `ancho` int(11) DEFAULT NULL COMMENT 'Determina el ancho de la imagen',
  `alto` int(11) DEFAULT NULL COMMENT 'Determina el alto de la imagen',
  `tamano` int(11) DEFAULT NULL COMMENT 'Determina el tamaño de la imagen cuadrada',
  `radio` int(11) DEFAULT NULL COMMENT 'Determinar el radio de la imagen circular',
  `posicion_X` int(11) NOT NULL COMMENT 'Coordenada de la abscisa de la imagen',
  `posicion_Y` int(11) NOT NULL COMMENT 'Coordenada de la ordenada de la imagen',
  `rotacion` int(11) NOT NULL DEFAULT 0 COMMENT 'Determina la rotación de la imagen',
  `area_id` int(11) NOT NULL COMMENT 'Área donde pertenece la mesa',
  `tipo` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'M: Mesa, D: Domicilio, LL: Llevar,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Mesas para la app de restobar';
CREATE TABLE `%_PREFIX_%tax_class` (
  `id` int(11) NOT NULL,
  `codigo` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código asignado por la DIAN',
  `descripcion` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%tax_code_fe` (
  `codigo` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indicador` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Indicador si es impuesto o retención. 0: Impuesto, 1: Retención,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%tax_percentages_fe` (
  `id` int(11) NOT NULL,
  `percentage` double NOT NULL COMMENT 'Porcentajes dados por la Dian',
  `tax_code` char(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código de impuesto dados por la DIAN'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` decimal(12,4) NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_class` int(11) NOT NULL DEFAULT 1 COMMENT 'Identifica la clase de impuesto segun estandar dada por la DIAN',
  `code_fe` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código empleado para facturación electrónica',
  `tax_indicator` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
CREATE TABLE `%_PREFIX_%technology_providers` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enlist_customer_necessary` int(1) DEFAULT 0,
  `status` int(1) DEFAULT NULL COMMENT '1. Activo, 0. Inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%technology_provider_configuration` (
  `id` int(11) NOT NULL,
  `technology_provider_id` int(11) NOT NULL,
  `work_enviroment` tinyint(2) NOT NULL COMMENT '1: Producción, 2: Pruebas,',
  `security_token` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Token utilizado para CADENA',
  `url` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Url para consumo de WS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%transfers` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `from_warehouse_id` int(11) NOT NULL,
  `from_warehouse_code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_warehouse_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_warehouse_id` int(11) NOT NULL,
  `to_warehouse_code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_warehouse_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `grand_total` decimal(25,4) DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `shipping` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `attachment` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `destination_biller_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `cost_center_id` int(11) DEFAULT NULL,
  `destination_cost_center_id` int(11) DEFAULT NULL,
  `companies_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%transfer_items` (
  `id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) DEFAULT NULL,
  `quantity_balance` decimal(15,4) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gst` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%types_customer_obligations` (
  `customer_id` int(11) NOT NULL,
  `types_obligations_id` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation` tinyint(1) NOT NULL COMMENT '1: Emisor, 2: Adquiriente,'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%types_obligations-responsabilities` (
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código DIAN',
  `description` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%types_person` (
  `id` int(11) NOT NULL,
  `description` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%types_vat_regime` (
  `id` int(11) NOT NULL,
  `codigo` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código determinado por DIAN',
  `description` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%ubication_shipping_cost` (
  `id` int(11) NOT NULL,
  `codigo_ciudad` varchar(9) NOT NULL,
  `codigo_zona` varchar(55) DEFAULT NULL,
  `codigo_subzona` varchar(55) DEFAULT NULL,
  `costo` decimal(25,4) DEFAULT NULL,
  `biller_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `%_PREFIX_%units` (
  `id` int(11) NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_unit` int(11) DEFAULT NULL,
  `operator` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_value` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operation_value` decimal(25,4) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
CREATE TABLE `%_PREFIX_%unit_prices` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL DEFAULT 0,
  `valor_unitario` decimal(25,4) NOT NULL DEFAULT 0.0000,
  `label` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unidad` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `cantidad` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `unit_id` int(11) NOT NULL DEFAULT 0,
  `id_product` int(11) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%users` (
  `id` int(11) UNSIGNED NOT NULL,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED DEFAULT NULL,
  `biller_id` int(10) UNSIGNED DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `show_cost` tinyint(1) DEFAULT 0,
  `show_price` tinyint(1) DEFAULT 0,
  `award_points` int(11) DEFAULT 0,
  `view_right` tinyint(1) NOT NULL DEFAULT 0,
  `edit_right` tinyint(1) NOT NULL DEFAULT 0,
  `allow_discount` tinyint(1) DEFAULT 0,
  `login_hour` timestamp NULL DEFAULT NULL,
  `login_status` tinyint(1) DEFAULT 0 COMMENT '1. Logueado, 0. Inactivo',
  `seller_id` int(11) DEFAULT NULL,
  `login_code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dashboard_id` int(11) DEFAULT NULL,
  `register_cash_movements_with_another_user` int(1) DEFAULT NULL,
  `cant_finish_pos_invoice` int(1) DEFAULT 0 COMMENT '1. No finalizar facturas en POS, 0. Si puede finalizar facturas en POS',
  `user_pc_serial` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_administrator` int(1) DEFAULT 0,
  `terms_and_conditions_accepted` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
CREATE TABLE `%_PREFIX_%user_activities` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `type_id` tinyint(1) NOT NULL COMMENT '1 = Edición, 2 = Eliminación, 3 = Agregar, 4 = consulta',
  `user_id` int(11) NOT NULL,
  `module_name` varchar(45) NOT NULL,
  `table_name` varchar(45) NOT NULL,
  `record_id` int(11) DEFAULT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Información del log de actividades del usuario';
CREATE TABLE `%_PREFIX_%user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%variants` (
  `id` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'no_image.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%warehouses` (
  `id` int(11) NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT 1 COMMENT '0. Inactivo, 1. Activo',
  `type` int(1) DEFAULT 1 COMMENT '1. Comercial, 2. Picking, 3. Packing',
  `preffix` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suffix` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%warehouses_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avg_cost` decimal(25,4) NOT NULL,
  `closed_year` smallint(1) DEFAULT 0 COMMENT '''0= Abierto  1= Cerrado',
  `warehouse_location` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%warehouses_products_variants` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `closed_year` smallint(1) DEFAULT 0 COMMENT '0= Abierto  1= Cerrado',
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%wishlist` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%withholdings` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `min_base` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `percentage` decimal(6,3) NOT NULL DEFAULT 0.000,
  `account_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apply` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ST = SubTotal, TX = Valor Iva, TO = Total, IC = ICA',
  `affects` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'S = Ventas, P = Compras',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'FUENTE, IVA, ICA, OTRA, BOMB, TABL',
  `code_fe` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Código empleado para facturación electrónica',
  `assumed_account_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `%_PREFIX_%wms_default_notices` (
  `id` int(11) NOT NULL,
  `text` varchar(200) NOT NULL,
  `type` int(1) NOT NULL COMMENT '1. Picking, 2. Packing',
  `status` int(1) NOT NULL COMMENT '1. Activo, 0. Inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%wms_notices` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL ,
  `type` int(1) NOT NULL COMMENT '1. Picking, 2 Packing',
  `picking_id` int(11) DEFAULT NULL,
  `packing_id` int(11) DEFAULT NULL,
  `default_notice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%wms_picking` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL ,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `document_type_id` int(11) NOT NULL,
  `packing_warehouse_id` int(11) NOT NULL,
  `invoiced_status` varchar(55) DEFAULT 'pending',
  `packed_status` varchar(55) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%wms_picking_detail` (
  `id` int(11) NOT NULL,
  `picking_id` int(11) NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `order_sale_id` int(11) NOT NULL,
  `order_sale_item_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `packed_quantity` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `packing_warehouse_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `%_PREFIX_%zones` (
  `codigo_ciudad` varchar(50) NOT NULL,
  `zone_name` varchar(55) NOT NULL,
  `zone_code` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `%_PREFIX_%sales_brands_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_brands_view`  AS SELECT month(`s`.`date`) AS `month`, `s`.`biller_id` AS `biller_id`, `p`.`brand` AS `brand`, `b`.`name` AS `name`, sum(`s`.`total` + `s`.`product_tax`) AS `subtotal`, count(`p`.`brand`) AS `number_transactions` FROM (((`%_PREFIX_%sales` `s` join `%_PREFIX_%sale_items` `si` on(`si`.`sale_id` = `s`.`id`)) join `%_PREFIX_%products` `p` on(`p`.`id` = `si`.`product_id`)) join `%_PREFIX_%brands` `b` on(`b`.`id` = `p`.`brand`)) WHERE year(`s`.`date`) = 2021 AND `s`.`sale_status` <> 'returned' GROUP BY month(`s`.`date`), `s`.`biller_id`, `p`.`brand`;

DROP TABLE IF EXISTS `%_PREFIX_%sales_categories_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_categories_view`  AS SELECT month(`s`.`date`) AS `month`, `s`.`biller_id` AS `biller_id`, `p`.`category_id` AS `category_id`, `c`.`name` AS `name`, sum(`s`.`total` + `s`.`product_tax`) AS `subtotal`, count(`p`.`brand`) AS `number_transactions` FROM (((`%_PREFIX_%sales` `s` join `%_PREFIX_%sale_items` `si` on(`si`.`sale_id` = `s`.`id`)) join `%_PREFIX_%products` `p` on(`p`.`id` = `si`.`product_id`)) join `%_PREFIX_%categories` `c` on(`c`.`id` = `p`.`category_id`)) WHERE year(`s`.`date`) = 2021 AND `s`.`sale_status` <> 'returned' GROUP BY month(`s`.`date`), `s`.`biller_id`, `p`.`category_id`;

DROP TABLE IF EXISTS `%_PREFIX_%sales_customers_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_customers_view`  AS SELECT month(`%_PREFIX_%sales`.`date`) AS `month`, `%_PREFIX_%sales`.`biller_id` AS `biller_id`, `%_PREFIX_%sales`.`customer_id` AS `customer_id`, `%_PREFIX_%sales`.`customer` AS `name`, sum(`%_PREFIX_%sales`.`total` + `%_PREFIX_%sales`.`product_tax`) AS `subtotal`, count(`%_PREFIX_%sales`.`customer_id`) AS `number_transactions` FROM `%_PREFIX_%sales` WHERE year(`%_PREFIX_%sales`.`date`) = 2021 AND `%_PREFIX_%sales`.`sale_status` <> 'returned' GROUP BY month(`%_PREFIX_%sales`.`date`), `%_PREFIX_%sales`.`biller_id`, `%_PREFIX_%sales`.`customer_id`;

DROP TABLE IF EXISTS `%_PREFIX_%sales_document_type_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_document_type_view`  AS SELECT month(`s`.`date`) AS `month`, `s`.`document_type_id` AS `document_type_id`, `s`.`biller_id` AS `biller_id`, `dt`.`sales_prefix` AS `sales_prefix`, sum(`s`.`total`) AS `subtotal`, sum(`s`.`product_tax`) AS `tax` FROM (`%_PREFIX_%sales` `s` join `%_PREFIX_%documents_types` `dt` on(`dt`.`id` = `s`.`document_type_id`)) WHERE year(`s`.`date`) = 2021 AND `s`.`sale_status` <> 'returned' GROUP BY `s`.`biller_id`, `dt`.`id`, month(`s`.`date`);

DROP TABLE IF EXISTS `%_PREFIX_%sales_mean_payments_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_mean_payments_view`  AS SELECT month(`s`.`date`) AS `month`, `s`.`biller_id` AS `biller_id`, `pm`.`code` AS `code`, `pm`.`name` AS `name`, sum(`s`.`total` + `s`.`product_tax`) AS `subtotal` FROM ((`%_PREFIX_%sales` `s` join `%_PREFIX_%payments` `p` on(`p`.`sale_id` = `s`.`id`)) join `%_PREFIX_%payment_methods` `pm` on(`pm`.`code` = `p`.`paid_by`)) WHERE year(`s`.`date`) = 2021 AND `s`.`sale_status` <> 'returned' GROUP BY month(`s`.`date`), `s`.`biller_id`, `pm`.`code`;

DROP TABLE IF EXISTS `%_PREFIX_%sales_products_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_products_view`  AS SELECT month(`s`.`date`) AS `month`, `s`.`biller_id` AS `biller_id`, `si`.`product_id` AS `product_id`, `p`.`name` AS `name`, sum(`s`.`total` + `s`.`product_tax`) AS `subtotal`, count(`s`.`id`) AS `number_transactions` FROM ((`%_PREFIX_%sales` `s` join `%_PREFIX_%sale_items` `si` on(`si`.`sale_id` = `s`.`id`)) join `%_PREFIX_%products` `p` on(`p`.`id` = `si`.`product_id`)) WHERE year(`s`.`date`) = 2021 AND `s`.`sale_status` <> 'returned' GROUP BY month(`s`.`date`), `s`.`biller_id`, `si`.`product_id`;

DROP TABLE IF EXISTS `%_PREFIX_%sales_sellers_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_sellers_view`  AS SELECT month(`s`.`date`) AS `month`, `s`.`biller_id` AS `biller_id`, `s`.`seller_id` AS `seller_id`, `c`.`name` AS `name`, sum(`s`.`total` + `s`.`product_tax`) AS `subtotal`, count(`s`.`seller_id`) AS `number_transactions` FROM (`%_PREFIX_%sales` `s` join `%_PREFIX_%companies` `c` on(`c`.`id` = `s`.`seller_id`)) WHERE year(`s`.`date`) = 2021 AND `s`.`sale_status` <> 'returned' GROUP BY month(`s`.`date`), `s`.`biller_id`, `s`.`seller_id`;

DROP TABLE IF EXISTS `%_PREFIX_%sales_tax_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_tax_view`  AS SELECT month(`s`.`date`) AS `month`, `s`.`biller_id` AS `biller_id`, `si`.`tax_rate_id` AS `tax_rate_id`, `t`.`name` AS `tax_name`, sum(`si`.`item_tax`) AS `total_tax` FROM ((`%_PREFIX_%sales` `s` join `%_PREFIX_%sale_items` `si` on(`si`.`sale_id` = `s`.`id`)) join `%_PREFIX_%tax_rates` `t` on(`t`.`id` = `si`.`tax_rate_id`)) WHERE year(`s`.`date`) = 2021 AND `s`.`sale_status` <> 'returned' GROUP BY `s`.`biller_id`, month(`s`.`date`), `si`.`tax_rate_id`;

DROP TABLE IF EXISTS `%_PREFIX_%sales_view`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `%_PREFIX_%sales_view`  AS SELECT count(0) AS `number_transactions`, `s`.`biller_id` AS `biller_id`, year(`s`.`date`) AS `year`, month(`s`.`date`) AS `month`, dayofmonth(`s`.`date`) AS `day`, sum(coalesce(if(`s`.`sale_status` <> 'returned',`s`.`total`,0),0)) AS `subtotal`, sum(coalesce(if(`s`.`sale_status` <> 'returned',`s`.`product_tax`,0),0)) AS `tax`, sum(coalesce(if(`s`.`sale_status` <> 'returned',`s`.`shipping`,0),0)) AS `shipping`, sum(coalesce(if(`s`.`sale_status` <> 'returned',`s`.`order_discount`,0),0)) AS `discount`, sum(coalesce(if(`s`.`sale_status` = 'returned',`s`.`grand_total`,0),0)) AS `returned_total`, 0 AS `cost_subtotal`, 0 AS `cost_total` FROM `%_PREFIX_%sales` AS `s` WHERE year(`s`.`date`) = '2021' GROUP BY `s`.`biller_id`, month(`s`.`date`), dayofmonth(`s`.`date`);

ALTER TABLE `%_PREFIX_%addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

ALTER TABLE `%_PREFIX_%adjustments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

ALTER TABLE `%_PREFIX_%adjustment_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adjustment_id` (`adjustment_id`);

ALTER TABLE `%_PREFIX_%api_keys`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%api_limits`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%api_logs`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%assemble`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%assemble_detail`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%biller_categories_concessions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%biller_data`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%biller_documents_types`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%biller_seller`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_companies_id_companies` (`companies_id`);

ALTER TABLE `%_PREFIX_%branch_areas`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

ALTER TABLE `%_PREFIX_%calendar`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

ALTER TABLE `%_PREFIX_%cart`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%ciiu_codes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%cities`
  ADD PRIMARY KEY (`CODIGO`),
  ADD KEY `CODIGO` (`CODIGO`),
  ADD KEY `DEPARTAMENTO` (`DEPARTAMENTO`),
  ADD KEY `CODDEPARTAMENTO` (`CODDEPARTAMENTO`);

ALTER TABLE `%_PREFIX_%collection_discounts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%combo_items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%commisions_soc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Index_2` (`category`,`subcategory`,`price_list`),
  ADD KEY `FK_%_PREFIX_%commisions_soc_2` (`price_list`);

ALTER TABLE `%_PREFIX_%companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_id_2` (`group_id`);

ALTER TABLE `%_PREFIX_%compatibilidades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_marca_brands_id` (`marca_id`);

ALTER TABLE `%_PREFIX_%console_service_data`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%costing`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%cost_per_month`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%countries`
  ADD PRIMARY KEY (`CODIGO`);

ALTER TABLE `%_PREFIX_%currencies`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%customer_groups`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%custom_fields`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%custom_field_values`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%cutting`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%cutting_detail`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%dashboards`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%date_format`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%debit_credit_notes_concepts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%deliveries`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%deposits`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%dian_note_concept`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%documents_types`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%documentypes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%employee_contacts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%employee_data`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%entryitems_con`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `entry_id` (`entry_id`),
  ADD KEY `ledger_id` (`ledger_id`),
  ADD KEY `companies_id` (`companies_id`);

ALTER TABLE `%_PREFIX_%expenses`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%expense_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%gift_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

ALTER TABLE `%_PREFIX_%gift_card_topups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `card_id` (`card_id`);

ALTER TABLE `%_PREFIX_%groups`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%groups_con`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `parent_id` (`parent_id`);

ALTER TABLE `%_PREFIX_%invoice_formats`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%invoice_notes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%ledgers_con`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `id` (`id`),
  ADD KEY `group_id` (`group_id`);

ALTER TABLE `%_PREFIX_%login_attempts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%new_year_database_connection`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%notifications`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%order_ref`
  ADD PRIMARY KEY (`ref_id`);

ALTER TABLE `%_PREFIX_%order_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%order_sale_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`sale_id`),
  ADD KEY `sale_id_2` (`sale_id`,`product_id`);

ALTER TABLE `%_PREFIX_%packing`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%packing_detail`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%pages`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%partners_soc`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payments`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payments_soc`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payment_methods`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%paypal`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_adjustment_documents`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_areas`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_arl_risk_classes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_concepts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_concept_type`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_contracts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_contract_concepts`
  ADD PRIMARY KEY (`contract_id`,`concept_id`),
  ADD KEY `fk_concept` (`concept_id`);

ALTER TABLE `%_PREFIX_%payroll_electronic_employee`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_futures_items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_period`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_professional_position`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_provisions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_settings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_social_security_parafiscal`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_ss_operators`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_types_contracts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_types_employees`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_weeks`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%payroll_workday`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%permissions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%pos_register`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%pos_register_items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%pos_register_movements`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%pos_settings`
  ADD PRIMARY KEY (`pos_id`);

ALTER TABLE `%_PREFIX_%preferences`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%preferences_categories`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%preparation_areas`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%price_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

ALTER TABLE `%_PREFIX_%printers`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%production_order`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%production_order_detail`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%production_order_notices`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%production_order_receipts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `category_id_2` (`category_id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `brand` (`brand`);

ALTER TABLE `%_PREFIX_%products_brands`
  ADD PRIMARY KEY (`product_id`,`brand_id`);

ALTER TABLE `%_PREFIX_%products_recosting`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%product_photos`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%product_preferences`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%product_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `price_group_id` (`price_group_id`);

ALTER TABLE `%_PREFIX_%product_variants`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%purchases_return`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%purchase_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

ALTER TABLE `%_PREFIX_%quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%quote_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quote_id` (`quote_id`),
  ADD KEY `product_id` (`product_id`);

ALTER TABLE `%_PREFIX_%returns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `return_id` (`return_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`return_id`),
  ADD KEY `return_id_2` (`return_id`,`product_id`);

ALTER TABLE `%_PREFIX_%sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%sales_budget`
  ADD PRIMARY KEY (`id`,`amount`);

ALTER TABLE `%_PREFIX_%sales_debit_notes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%sales_returns`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%sale_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`sale_id`),
  ADD KEY `sale_id_2` (`sale_id`,`product_id`);

ALTER TABLE `%_PREFIX_%sequential_count`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

ALTER TABLE `%_PREFIX_%sequential_count_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adjustment_id` (`adjustment_id`);

ALTER TABLE `%_PREFIX_%sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

ALTER TABLE `%_PREFIX_%settings`
  ADD PRIMARY KEY (`setting_id`);

ALTER TABLE `%_PREFIX_%settings_soc`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%shop_settings`
  ADD PRIMARY KEY (`shop_id`);

ALTER TABLE `%_PREFIX_%skrill`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%sms_settings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%states`
  ADD PRIMARY KEY (`PAIS`,`CODDEPARTAMENTO`),
  ADD KEY `CODDEPARTAMENTO` (`CODDEPARTAMENTO`);

ALTER TABLE `%_PREFIX_%stock_counts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

ALTER TABLE `%_PREFIX_%stock_count_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_count_id` (`stock_count_id`),
  ADD KEY `product_id` (`product_id`);

ALTER TABLE `%_PREFIX_%subzones`
  ADD PRIMARY KEY (`subzone_code`),
  ADD KEY `fk_zone_subzone_idx` (`zone_code`);

ALTER TABLE `%_PREFIX_%suspended_bills`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%suspended_items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%system_logs`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%tables`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%tax_class`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%tax_code_fe`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `%_PREFIX_%tax_percentages_fe`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%tax_rates`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%technology_providers`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%technology_provider_configuration`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%transfer_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transfer_id` (`transfer_id`),
  ADD KEY `product_id` (`product_id`);

ALTER TABLE `%_PREFIX_%types_customer_obligations`
  ADD PRIMARY KEY (`customer_id`,`types_obligations_id`,`relation`);

ALTER TABLE `%_PREFIX_%types_obligations-responsabilities`
  ADD PRIMARY KEY (`code`);

ALTER TABLE `%_PREFIX_%types_person`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%types_vat_regime`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%ubication_shipping_cost`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_shipping_cost_zone_idx` (`codigo_zona`),
  ADD KEY `fk_shipping_cost_subzone_idx` (`codigo_subzona`),
  ADD KEY `fk_shipping_cost_ciudad_idx` (`codigo_ciudad`),
  ADD KEY `fk_shipping_cost_biller_idx` (`biller_id`);

ALTER TABLE `%_PREFIX_%units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `base_unit` (`base_unit`);

ALTER TABLE `%_PREFIX_%unit_prices`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`,`warehouse_id`,`biller_id`),
  ADD KEY `group_id_2` (`group_id`,`company_id`);

ALTER TABLE `%_PREFIX_%user_activities`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%user_logins`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%variants`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%warehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `%_PREFIX_%warehouses_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

ALTER TABLE `%_PREFIX_%warehouses_products_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

ALTER TABLE `%_PREFIX_%wishlist`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%withholdings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%wms_default_notices`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%wms_notices`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%wms_picking`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%wms_picking_detail`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `%_PREFIX_%zones`
  ADD PRIMARY KEY (`zone_code`),
  ADD KEY `fk_city_zone_idx` (`codigo_ciudad`);

ALTER TABLE `%_PREFIX_%addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%adjustment_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%api_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%api_limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%api_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%assemble`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%assemble_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%biller_categories_concessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%biller_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%biller_documents_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%biller_seller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%branch_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%ciiu_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%collection_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%combo_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%commisions_soc`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%compatibilidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%console_service_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%costing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%cost_per_month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%customer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%custom_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%custom_field_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%cutting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%cutting_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%dashboards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%date_format`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%debit_credit_notes_concepts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%deposits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%dian_note_concept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%documents_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%documentypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%employee_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%employee_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%entryitems_con`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%expense_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%gift_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%gift_card_topups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%groups_con`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%invoice_formats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%invoice_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%ledgers_con`
  MODIFY `id` bigint(18) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%new_year_database_connection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%order_ref`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%order_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%order_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%packing_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payments_soc`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payment_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_adjustment_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_arl_risk_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_concepts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_concept_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_electronic_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_futures_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_professional_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_provisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_social_security_parafiscal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_ss_operators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_types_contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_types_employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_weeks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%payroll_workday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%pos_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%pos_register_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%pos_register_movements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%preferences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%preferences_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%preparation_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%price_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%printers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%production_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%production_order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%production_order_notices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%production_order_receipts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%products_recosting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%product_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%product_preferences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%product_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%product_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%purchases_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%quote_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%sales_budget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%sales_debit_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%sales_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%sequential_count`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%sequential_count_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%settings_soc`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%sms_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%stock_counts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%stock_count_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%suspended_bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%suspended_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%system_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%tax_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%tax_percentages_fe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%technology_providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%technology_provider_configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%transfer_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%ubication_shipping_cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%unit_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%user_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%warehouses_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%warehouses_products_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%withholdings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%wms_default_notices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%wms_notices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%wms_picking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%wms_picking_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `%_PREFIX_%cities`
  ADD CONSTRAINT `ciudades_fk` FOREIGN KEY (`CODDEPARTAMENTO`) REFERENCES `%_PREFIX_%states` (`CODDEPARTAMENTO`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `%_PREFIX_%commisions_soc`
  ADD CONSTRAINT `FK_%_PREFIX_%commisions_soc_1` FOREIGN KEY (`category`) REFERENCES `%_PREFIX_%categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_%_PREFIX_%commisions_soc_2` FOREIGN KEY (`price_list`) REFERENCES `%_PREFIX_%price_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `%_PREFIX_%payroll_contract_concepts`
  ADD CONSTRAINT `fk_concept` FOREIGN KEY (`concept_id`) REFERENCES `%_PREFIX_%payroll_concepts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contract` FOREIGN KEY (`contract_id`) REFERENCES `%_PREFIX_%payroll_contracts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `%_PREFIX_%states`
  ADD CONSTRAINT `DEPARTAMENTOS_PAISES_FK1` FOREIGN KEY (`PAIS`) REFERENCES `%_PREFIX_%countries` (`CODIGO`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `%_PREFIX_%subzones`
  ADD CONSTRAINT `fk_zone_subzone` FOREIGN KEY (`zone_code`) REFERENCES `%_PREFIX_%zones` (`zone_code`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `%_PREFIX_%ubication_shipping_cost`
  ADD CONSTRAINT `fk_shipping_cost_ciudad` FOREIGN KEY (`codigo_ciudad`) REFERENCES `%_PREFIX_%cities` (`CODIGO`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_shipping_cost_subzone` FOREIGN KEY (`codigo_subzona`) REFERENCES `%_PREFIX_%subzones` (`subzone_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_shipping_cost_zone` FOREIGN KEY (`codigo_zona`) REFERENCES `%_PREFIX_%zones` (`zone_code`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `%_PREFIX_%zones`
  ADD CONSTRAINT `fk_city_zone` FOREIGN KEY (`codigo_ciudad`) REFERENCES `%_PREFIX_%cities` (`CODIGO`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
