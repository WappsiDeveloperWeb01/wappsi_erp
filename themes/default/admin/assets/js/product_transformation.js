$(document).ready(function () {
    if (!localStorage.getItem('ptrref')) {
        localStorage.setItem('ptrref', '');
    }

    $('.bootbox').on('hidden.bs.modal', function (e) {
        $('#add_item').focus();
    });
    $('body a, body button').attr('tabindex', -1);
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }
    if (localStorage.getItem('ptrdestinationitems') || localStorage.getItem('ptroriginitems')) {
        loadItems();
    }
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('ptroriginitems')) {
                    localStorage.removeItem('ptroriginitems');
                }
                if (localStorage.getItem('ptrdestinationitems')) {
                    localStorage.removeItem('ptrdestinationitems');
                }
                if (localStorage.getItem('ptrref')) {
                    localStorage.removeItem('ptrref');
                }
                if (localStorage.getItem('ptrwarehouseorigin')) {
                    localStorage.removeItem('ptrwarehouseorigin');
                }
                if (localStorage.getItem('ptrwarehousedestination')) {
                    localStorage.removeItem('ptrwarehousedestination');
                }
                if (localStorage.getItem('ptrnote')) {
                    localStorage.removeItem('ptrnote');
                }
                if (localStorage.getItem('ptrdate')) {
                    localStorage.removeItem('ptrdate');
                }

                if (localStorage.getItem('trnf_companies_id')) {
                    localStorage.removeItem('trnf_companies_id');
                }
                if (localStorage.getItem('trnf_document_type_id')) {
                    localStorage.removeItem('trnf_document_type_id');
                }
                if (localStorage.getItem('ptrbiller')) {
                    localStorage.removeItem('ptrbiller');
                }
                if (localStorage.getItem('ptrdestination_biller')) {
                    localStorage.removeItem('ptrdestination_biller');
                }
                if (localStorage.getItem('reference_restricted')) {
                    localStorage.removeItem('reference_restricted');
                }
                $('#modal-loading').show();
                location.reload();
            }
        });
    });

    // save and load the fields in and/or from localStorage
    $('#ptrref').change(function (e) {
        localStorage.setItem('ptrref', $(this).val());
    });
    if (ptrref = localStorage.getItem('ptrref')) {
        $('#ptrref').val(ptrref);
    }
    $('#ptrwarehouseorigin').change(function (e) {
        localStorage.setItem('ptrwarehouseorigin', $(this).val());
        loadItems();
    });
    if (ptrwarehouseorigin = localStorage.getItem('ptrwarehouseorigin')) {
        $('#ptrwarehouseorigin').select2("val", ptrwarehouseorigin);
    }
    $('#ptrwarehousedestination').change(function (e) {
        localStorage.setItem('ptrwarehousedestination', $(this).val());
    });
    if (ptrwarehousedestination = localStorage.getItem('ptrwarehousedestination')) {
        $('#ptrwarehousedestination').select2("val", ptrwarehousedestination);
    }
    if (reference_restricted = localStorage.getItem('reference_restricted')) {
        $('#restrict_by_product_reference').select2("val", 1).select2('readonly', true);
        $('#restricted_reference').val(reference_restricted);
        $('.div_restricted_reference').fadeIn();
    }
    //$(document).on('change', '#ptrnote', function (e) {
        $('#ptrnote').redactor('destroy');
        $('#ptrnote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('ptrnote', v);
            }
        });
        if (ptrnote = localStorage.getItem('ptrnote')) {
            $('#ptrnote').redactor('set', ptrnote);
        }

    // prevent default action upon enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });


    /* ----------------------
     * Delete Row Method
     * ---------------------- */

    $(document).on('click', '.ptrdel_origin', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        ptroriginitems = JSON.parse(localStorage.getItem('ptroriginitems'));
        delete ptroriginitems[item_id];
        row.remove();
        if(ptroriginitems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('ptroriginitems', JSON.stringify(ptroriginitems));
            loadItems();
            return;
        }
    });

     $(document).on('click', '.ptrdel_destination', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        ptrdestinationitems = JSON.parse(localStorage.getItem('ptrdestinationitems'));
        delete ptrdestinationitems[item_id];
        row.remove();
        if(ptrdestinationitems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('ptrdestinationitems', JSON.stringify(ptrdestinationitems));
            loadItems();
            return;
        }
    });

    /* --------------------------
     * Edit Row Quantity Method
     -------------------------- */

    $(document).on("change", '.rquantityorigin', function () {
        var row = $(this).closest('tr');
        ptroriginitems = JSON.parse(localStorage.getItem('ptroriginitems'));
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        ptroriginitems[item_id].row.qty = new_qty;
        localStorage.setItem('ptroriginitems', JSON.stringify(ptroriginitems));
        loadItems();
    });

    $(document).on("change", '.rquantitydestination', function () {
        var row = $(this).closest('tr');
        ptrdestinationitems = JSON.parse(localStorage.getItem('ptrdestinationitems'));
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        ptrdestinationitems[item_id].row.qty = new_qty;
        localStorage.setItem('ptrdestinationitems', JSON.stringify(ptrdestinationitems));
        loadItems();
    });

    $(document).on("change", '.rcost_origin', function () {
        var new_cost = parseFloat($(this).val());
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        ptroriginitems = JSON.parse(localStorage.getItem('ptroriginitems'));
        var prev_cost = ptroriginitems[item_id].row.cost;
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(prev_cost);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        ptroriginitems[item_id].row.price = new_cost;
        localStorage.setItem('ptroriginitems', JSON.stringify(ptroriginitems));
        loadItems();
    });

    $(document).on("change", '.rcost_destination', function () {
        var new_cost = parseFloat($(this).val());
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        ptrdestinationitems = JSON.parse(localStorage.getItem('ptrdestinationitems'));
        var prev_cost = ptrdestinationitems[item_id].row.cost;
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(prev_cost);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        ptrdestinationitems[item_id].row.cost = new_cost;
        localStorage.setItem('ptrdestinationitems', JSON.stringify(ptrdestinationitems));
        loadItems();
    });

    if (ptrbiller = localStorage.getItem('ptrbiller')) {
        $('#ptrbiller').select2("val", ptrbiller).trigger('change');
    } else {
        $('#ptrbiller').trigger('change');
    }

    if (ptrdestination_biller = localStorage.getItem('ptrdestination_biller')) {
        $('#ptrdestination_biller').select2("val", ptrdestination_biller).trigger('change');
    } else {
        $('#ptrdestination_biller').trigger('change');
    }

    loadItems();

	$("#add_transformation").validate({
		ignore: []
	});

    $(document).on('change', '#document_type_id', function(){
        localStorage.setItem('trnf_document_type_id', $(this).val());
    });

    $(document).on('change', '#companies_id', function(){
        localStorage.setItem('trnf_companies_id', $(this).val());
    });

    if (trnf_companies_id = localStorage.getItem('trnf_companies_id')) {
        $('#companies_id').select2('val', trnf_companies_id);
    }

    if (trnf_document_type_id = localStorage.getItem('trnf_document_type_id')) {
        $('#document_type_id').select2('val', trnf_document_type_id);
    }

});
/* -----------------------
 * Load Items to table
 ----------------------- */
 //ACA LOAD
function loadItems() {

    countdestination = 1;
    countorigin = 1;
    total_origin = 0;
    total_destination = 0;
    if (localStorage.getItem('ptroriginitems')) {
        an = 1;
        Ttotal = 0;
        $("#ptrTableOrigin tbody").empty();
        ptroriginitems = JSON.parse(localStorage.getItem('ptroriginitems'));
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(ptroriginitems, function(o){return [parseInt(o.order)];}) : ptroriginitems;
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_qty = item.row.qty, item_price = item.row.price, item_total = formatDecimal(item.row.qty * item.row.price), item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var type = item.row.type ? item.row.type : '';
            var item_cost = item.row.price;

            Ttotal += item_total;
            var row_no = item.id;

            wh_quantity = (item.row.wh_data[$('#ptrwarehouseorigin').val()] !== undefined ? item.row.wh_data[$('#ptrwarehouseorigin').val()].quantity : 0);
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + (parseFloat(wh_quantity) <= 0 || parseFloat(wh_quantity) < parseFloat(item_qty) ? ' danger' : '') +'" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id_origin[]" type="hidden" class="rid" value="' + product_id + '">'+
                            '<input name="product_name_origin[]" type="hidden" value="' + item_name + '">'+
                            '<span class="sname" id="name_' + row_no + '">' + item_name +' ('+item_code+') '+' '+
                            (item.row.serialModal_serial !== undefined ? '('+item.row.serialModal_serial+')' : '')+
                            '<input name="product_serial_option_id_origin[]" type="hidden" value="' + (item.row.serialModal_serial !== undefined ? item.row.option : '') + '"><input name="product_serial_no_origin[]" type="hidden" value="' + (item.row.serialModal_serial !== undefined ? item.row.serialModal_serial : '') + '"></span>'+
                        '</td>';
            tr_html += '<td>'+item.unit.code+'</td>';
            tr_html += '<td '+(show_cost ? '' : 'style="display:none;"')+'>'+
                            '<input type="text" name="unit_cost_origin[]" class="rcost_origin form-control text-right" value="' + formatDecimal(item_cost) + '" '+(site.settings.set_adjustment_cost == 1 && (is_admin || is_owner || user_permissions.products_cost) ? '' : 'readonly')+'>'+
                        '</td>';
            tr_html += '<td>'+lang[item.row.type]+'</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantityorigin" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity_origin[]" type="text" value="' + formatDecimal(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+(item.row.serialModal_serial !== undefined ? 'readonly' : '')+'>'+
                        '</td>';
            tr_html += '<td class="text-right" '+(show_cost ? '' : 'style="display:none;"')+'>'+formatMoney(item_total)+
                            '<input class="form-control text-center" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="total_cost_origin[]" type="hidden" value="' + item_total + '" readonly>'+
                            '<input name="cost_origin[]" type="hidden" value="'+item_total+'">'+
                        '</td>'; //unitario
            tr_html += '<td class="text-center"><i class="fa fa-times tip ptrdel_origin" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#ptrTableOrigin");
            countorigin += parseFloat(item_qty);
            an++;

        });

        var col = (show_cost ? 3 : 2);
        if (site.settings.product_serial == 1) {
                col++;
            }
        var tfoot = '<tr id="tfoot" class="tfoot active">'+(site.settings.set_adjustment_cost ? '<th></th>' : '')+
                        '<th colspan="'+col+'">Total</th><th class="text-center total_qty_origin_tfoot" >' + formatQuantity2(parseFloat(countorigin) - 1) + '</th>'+
                        '<th class="text-center total_cost_origin_tfoot" '+(show_cost ? '' : 'style="display:none;"')+'>' + formatMoney(parseFloat(Ttotal)) + '</th>';
        if (site.settings.product_serial == 1) { tfoot += '<th></th>'; }
        tfoot +=        '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                    '</tr>';
        $('#ptrTableOrigin tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        total_origin = Ttotal;
    }
    if (ptrdestinationitems = JSON.parse(localStorage.getItem('ptrdestinationitems'))) {
        an = 1;
        Ttotal = 0;
        $("#ptrTableDestination tbody").empty();
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(ptrdestinationitems, function(o){return [parseInt(o.order)];}) : ptrdestinationitems;
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_qty = item.row.qty, item_price = item.row.price, item_total = formatDecimal(item.row.qty * item.row.cost), item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var type = item.row.type ? item.row.type : '';
            var item_cost = item.row.cost;
            Ttotal += item_total;
            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id_destination[]" type="hidden" class="rid" value="' + product_id + '">'+
                            '<input name="product_name_destination[]" type="hidden" value="' + item_name + '">'+
                            '<span class="sname" id="name_' + row_no + '">' + item_name +' ('+item_code+') '+' '+
                            (item.row.serialModal_serial !== undefined ? '('+item.row.serialModal_serial+')' : '')+
                            '<input name="product_serial_option_id_destination[]" type="hidden" value="' + (item.row.serialModal_serial !== undefined ? item.row.option : '') + '"><input name="product_serial_no_destination[]" type="hidden" value="' + (item.row.serialModal_serial !== undefined ? item.row.serialModal_serial : '') + '"></span>'+
                        '</td>';
            tr_html += '<td>'+item.unit.code+'</td>';
            tr_html += '<td '+(show_cost ? '' : 'style="display:none;"')+'>'+
                            '<input type="text" name="unit_cost_destination[]" class="rcost_destination form-control text-right" value="' + formatDecimal(item_cost) + '" '+(site.settings.set_adjustment_cost == 1 && (is_admin || is_owner || user_permissions.products_cost) ? '' : 'readonly')+'>'+
                        '</td>';
            tr_html += '<td>'+lang[item.row.type]+'</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantitydestination" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity_destination[]" type="text" value="' + formatDecimal(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                        '</td>';
            tr_html += '<td class="text-right" '+(show_cost ? '' : 'style="display:none;"')+'>'+formatMoney(item_total)+
                            '<input class="form-control text-center" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="total_cost_destination[]" type="hidden" value="' + item_total + '" readonly>'+
                            '<input name="cost_destination[]" type="hidden" value="'+item_total+'">'+
                        '</td>'; //unitario
            tr_html += '<td class="text-center"><i class="fa fa-times tip ptrdel_destination" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#ptrTableDestination");
            countdestination += parseFloat(item_qty);
            an++;

        });

        var col = (show_cost ? 3 : 2);
        if (site.settings.product_serial == 1) {
                col++;
            }
        var tfoot = '<tr id="tfoot" class="tfoot active">'+(site.settings.set_adjustment_cost ? '<th></th>' : '')+
                        '<th colspan="'+col+'">Total</th><th class="text-center total_qty_destination_tfoot">' + formatQuantity2(parseFloat(countdestination) - 1) + '</th>'+
                        '<th class="text-center total_cost_destination_tfoot" '+(show_cost ? '' : 'style="display:none;"')+'>' + formatMoney(parseFloat(Ttotal)) + '</th>';
        if (site.settings.product_serial == 1) { tfoot += '<th></th>'; }
        tfoot +=        '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                    '</tr>';
        $('#ptrTableDestination tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        total_destination = Ttotal;
    }
    $('#submit').prop('disabled', false);
    $('.quantity_error').fadeOut();
    $('.cost_error').fadeOut();
    if (formatDecimal(total_origin, 2) != formatDecimal(total_destination, 2)) {
    	$('#submit').prop('disabled', true);
    	$('.cost_error').fadeIn();
    } else if (site.settings.product_transformation_validate_quantity == 1 && parseFloat(formatDecimal(countorigin)) != parseFloat(formatDecimal(countdestination)) && ptrdestinationitems && Object.keys(ptrdestinationitems).length > 0) 
    {
        if (site.settings.automatic_loading_of_existence_in_transformation == 0) {
            $('#submit').prop('disabled', true);
            $('.quantity_error').fadeIn();
        }
    }

    if (ptrdestinationitems && countorigin != countdestination && Object.keys(ptrdestinationitems).length > 0) {

        // bootbox.confirm({
        //     message: "La cantidad total en los productos de destino es diferente al total de los productos origen",
        //     buttons: {
        //         confirm: {
        //             label: 'Corregir cantidad',
        //             className: 'btn-success btn-full send-submit-sale'
        //         },
        //         cancel: {
        //             label: 'Continuar proceso',
        //             className: 'btn-danger btn-full btn-outline'
        //         }
        //     },
        //     callback: function (result) {
        //         if (result) {
        //             setTimeout(function() {
        //                 $('.rquantitydestination').last().focus().select();
        //             }, 850);
        //         } else {
        //             sended = false;
        //         }
        //     }
        // });
    }

    if (countorigin > 1) {
        $('#restrict_by_product_reference').select2('readonly', true);
    }
}

/* -----------------------------
 * Add Purchase Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
var reference_restricted = localStorage.getItem('reference_restricted') ? localStorage.getItem('reference_restricted') : null;
function add_adjustment_origin_item(item, modal = false, cont = false) {
    if (countorigin == 1) {
        ptroriginitems = {};
    }
    if (item == null)
        return;

    restrict_by_product_reference = $('#restrict_by_product_reference').val();
    if (countorigin != 1 && restrict_by_product_reference == 1 && item.row.reference != reference_restricted) {
        command: toastr.error('No puede llamar un producto con referencia diferente a <b>'+reference_restricted+'</b>.', '¡Error!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        return;
    }

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (ptroriginitems[item_id]) {
        var new_qty = parseFloat(ptroriginitems[item_id].row.qty) + 1;
        ptroriginitems[item_id].row.base_quantity = new_qty;
        if(ptroriginitems[item_id].row.unit != ptroriginitems[item_id].row.base_unit) {
            $.each(ptroriginitems[item_id].units, function(){
                if (this.id == ptroriginitems[item_id].row.unit) {
                    ptroriginitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        ptroriginitems[item_id].row.qty = new_qty;
    } else {
        ptroriginitems[item_id] = item;
    }
    ptroriginitems[item_id].order = new Date().getTime();
    if (site.settings.product_variant_per_serial == 1 && modal === false && item.row.attributes == 1) {
        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(ptroriginitems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal_option_id').val('');
        $('#serialModal').modal('show');
        localStorage.setItem('modalSerial_item', JSON.stringify(ptroriginitems[item_id]));
        delete ptroriginitems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
            $('#serialModal_type').val(1);
        }, 850);
    } else {
        if (site.settings.product_variant_per_serial == 1) {
            localStorage.removeItem('modalSerial_item');
            if (site.settings.product_variant_per_serial == 1 && cont == false) {
                $('#serialModal').modal('hide');
                $('#add_item').val('').focus();
            } else {
                item = ptroriginitems[item_id];
                $.ajax({
                    url : site.base_url + "products/get_random_id"
                }).done(function(data){
                    item.id = data;
                    localStorage.setItem('modalSerial_item', JSON.stringify(item));
                    $('#serialModal_serial').val('');
                    $('#serialModal_meters').val('');
                });
            }
        }
        if (countorigin == 1 && restrict_by_product_reference == 1) {
            reference_restricted = item.row.reference;
            localStorage.setItem('reference_restricted', reference_restricted);
        }
        if (countorigin == 1) {
            $('#restrict_by_product_reference').select2('readonly', true);
        }
        localStorage.setItem('ptroriginitems', JSON.stringify(ptroriginitems));
        loadItems();
        validate_products_cost();
        return true;
    }
}

function add_adjustment_destination_item(item, modal = false, cont = false) {

    if (countdestination == 1) {
        ptrdestinationitems = {};
    }
    if (item == null)
        return;

    restrict_by_product_reference = $('#restrict_by_product_reference').val();
    if (countdestination == 1 && countorigin == 1 && restrict_by_product_reference == 1) {
        command: toastr.error('Debe llamar un primer producto de origen para establecer la referencia restringida', '¡Error!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        return;
    }
    if (restrict_by_product_reference == 1 && item.row.reference != reference_restricted) {
        command: toastr.error('No puede llamar un producto con referencia diferente a <b>'+reference_restricted+'</b>.', '¡Error!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        return;
    }

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (ptrdestinationitems[item_id]) {

        var new_qty = parseFloat(ptrdestinationitems[item_id].row.qty) + 1;
        ptrdestinationitems[item_id].row.base_quantity = new_qty;
        if(ptrdestinationitems[item_id].row.unit != ptrdestinationitems[item_id].row.base_unit) {
            $.each(ptrdestinationitems[item_id].units, function(){
                if (this.id == ptrdestinationitems[item_id].row.unit) {
                    ptrdestinationitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        ptrdestinationitems[item_id].row.qty = new_qty;

    } else {
        ptrdestinationitems[item_id] = item;
    }
    ptrdestinationitems[item_id].order = new Date().getTime();

    //NUEVO

    if (site.settings.product_variant_per_serial == 1 && modal === false && item.row.attributes == 1) {
        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(ptrdestinationitems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal_option_id').val('');
        $('#serialModal').modal('show');
        localStorage.setItem('modalSerial_item', JSON.stringify(ptrdestinationitems[item_id]));
        delete ptrdestinationitems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
            $('#serialModal_type').val(2);
        }, 850);
    } else {
        if (site.settings.product_variant_per_serial == 1) {
            localStorage.removeItem('modalSerial_item');
            if (site.settings.product_variant_per_serial == 1 && cont == false) {
                $('#serialModal').modal('hide');
                $('#add_item').val('').focus();
            } else {
                item = ptrdestinationitems[item_id];
                $.ajax({
                    url : site.base_url + "products/get_random_id"
                }).done(function(data){
                    item.id = data;
                    localStorage.setItem('modalSerial_item', JSON.stringify(item));
                    $('#serialModal_serial').val('');
                    $('#serialModal_meters').val('');
                });
            }
        }
        if (countdestination == 1) {
            $('#restrict_by_product_reference').select2('readonly', true);
        }
        localStorage.setItem('ptrdestinationitems', JSON.stringify(ptrdestinationitems));
        loadItems();
        validate_products_cost();
        return true;
    }
    //NUEVO
}

function validate_products_cost(){
    ptrdestinationitems = JSON.parse(localStorage.getItem('ptrdestinationitems'));
    ptroriginitems = JSON.parse(localStorage.getItem('ptroriginitems'));
    if (ptrdestinationitems && Object.keys(ptrdestinationitems).length > 1 && Object.keys(ptroriginitems).length) {
        setTimeout(function() {
            bootbox.confirm('Al seleccionar más de un producto de destino los costos se prorratearán, por favor confirmar antes de finalizar el proceso', function (result) {

            });
        }, 800);
    }
}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (countorigin > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}


$(document).on('click', '#equal_costs', function(){
    ptrdestinationitems = JSON.parse(localStorage.getItem('ptrdestinationitems'));
    total_qty_destination_tfoot = $('.total_qty_destination_tfoot').text();
    $.each(ptrdestinationitems, function(index, item){
    	prev_cost = parseFloat(ptrdestinationitems[index].row.cost);
    	prev_total_cost = parseFloat(ptrdestinationitems[index].row.cost) * ptrdestinationitems[index].row.qty;
    	remaining_cost = pcalculateRemainingCost(prev_total_cost, formatDecimal($('.total_cost_destination_tfoot').text()), formatDecimal($('.total_cost_origin_tfoot').text()), ptrdestinationitems[index].row.qty, total_qty_destination_tfoot);
    	new_cost = prev_cost + (remaining_cost / ptrdestinationitems[index].row.qty);
    	ptrdestinationitems[index].row.prev_cost = prev_cost;
    	ptrdestinationitems[index].row.cost = new_cost;
    });
    localStorage.setItem('ptrdestinationitems', JSON.stringify(ptrdestinationitems));
    loadItems();
});

$(document).on('click', '#reset_costs', function(){
    ptrdestinationitems = JSON.parse(localStorage.getItem('ptrdestinationitems'));
    $.each(ptrdestinationitems, function(index, item){
    	last_cost = parseFloat(ptrdestinationitems[index].row.last_cost);
    	ptrdestinationitems[index].row.cost = last_cost;
    });
    localStorage.setItem('ptrdestinationitems', JSON.stringify(ptrdestinationitems));
    loadItems();
});

$(document).on('click', '#submit', function(){
	if ($('#add_transformation').valid()) {
        $(this).prop('disabled', true);
		$('.submit_form').click();
	}
});

///SERIAL MODAL


$(document).on('click', '.send_serial_modal', function(){
    validar_modal_serial();
});

$(document).on('click', '.continue_serial_modal', function(){
    validar_modal_serial(true);
});


function validar_modal_serial(cont = false){
    if ($('#serialModal_form').valid()) {
        serial = $('#serialModal_serial').val();
        meters = $('#serialModal_meters').val();
        item_id = $('#serialModal_product_id').val();
        option_id = $('#serialModal_option_id').val();
        serialModal_type = $('#serialModal_type').val();
        item = JSON.parse(localStorage.getItem('modalSerial_item'));
        item.row.serialModal_serial = serial;
        item.row.qty = meters;
        item.row.quantity = meters;
        item.row.base_quantity = meters;
        item.row.delivered_qty = 0;
        item.row.pending_qty = meters;
        item.row.bill_qty = 0;
        item.row.option = option_id;
        if (serialModal_type == 1) {
            add_adjustment_origin_item(item, true, cont);
        } else if (serialModal_type == 2) {
            add_adjustment_destination_item(item, true, cont);
        }
    }
}

$(document).on('keyup', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal, e);
});

$(document).on('change', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal);
});

$(document).on('keyup', '#serialModal_meters', function(e){
    if (e.keyCode == 13) {
        $('.send_serial_modal').trigger('click');
    }
});

function validate_serial(serialModal, e = null, get_qty = false){
    serialModal_type = $('#serialModal_type').val();
    if (serialModal_type == 1) {
        if (e == null || e != null && e.keyCode == 13) {
            if (get_qty == false) {
                var get_qty = true;
                if (ptroriginitems = JSON.parse(localStorage.getItem('ptroriginitems'))) {
                    $.each(ptroriginitems, function(index, item){
                        if (item.row.serialModal_serial == $('#serialModal_serial').val()) {
                            setTimeout(function() {
                                command: toastr.warning('El serial ingresado ya se registró en este formulario.', '¡Atención!', {
                                    "showDuration": "500",
                                    "hideDuration": "1000",
                                    "timeOut": "4000",
                                    "extendedTimeOut": "1000",
                                });
                                $('#serialModal_meters').val('');
                                $('#serialModal_serial').val('').focus();
                            }, 800);
                            get_qty = false;
                        }
                    });
                }
                if (get_qty == false) {
                    return false;
                }
                setTimeout(function() {
                    validate_serial(serialModal, e, true);
                }, 1000);
            }
            if (get_qty == true) {
                serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
                ptrwarehouseorigin = $('#ptrwarehouseorigin').val();
                $.ajax({
                    url : site.base_url + 'products/get_product_serial_variant_quantity/'+serial_item.row.id+'/'+serialModal+'/'+ptrwarehouseorigin,
                    dataType : 'JSON',
                }).done(function(data){
                    if (data.id !== undefined) {
                        $('#serialModal_meters').val(data.quantity);
                        $('#serialModal_meters').prop('max', data.quantity);
                        $('#serialModal_option_id').val(data.option_id);
                        $('#serialModal_meters').focus();
                    } else {
                        $('#serialModal_serial').val('');
                        $('#serialModal_option_id').val('');
                         command: toastr.warning('El serial indicado no está registrado para este producto', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                });
            }
        }
    } else if (serialModal_type == 2) {
        serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
        $.ajax({
            url : site.base_url + 'products/get_product_serial_variant/'+serial_item.row.id+'/'+serialModal
        }).done(function(data){
            if (data == '1') {
                if (!localStorage.getItem('validated_serial')) {
                    localStorage.setItem('validated_serial', true);
                    bootbox.confirm('El serial ingresado ya existe para este producto. ¿Desea registrar más cantidades?', function (result) {
                        if (result) {
                            setTimeout(function() {
                                $('#serialModal').modal('show');
                            }, 400);
                            setTimeout(function() {
                                $('#serialModal_meters').focus();
                                localStorage.removeItem('validated_serial');
                            }, 1000);
                        } else {
                            setTimeout(function() {
                                $('#serialModal').modal('show');
                            }, 400);
                            setTimeout(function() {
                                $('#serialModal_serial').val('').focus();
                                localStorage.removeItem('validated_serial');
                            }, 1000);
                        }
                    });
                }
            } else {
                if (e != null && e.keyCode == 13) {
                    $('#serialModal_meters').focus();
                }
            }
        });
    } else {
        if (e != null && e.keyCode == 13) {
            $('#serialModal_meters').focus();
        }
    }
}

$(document).on('click', '#random_num', function(e){
    $('#serialModal_serial').select();
});