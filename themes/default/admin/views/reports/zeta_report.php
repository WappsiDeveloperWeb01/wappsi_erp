<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
	<div class="row">
	    <div class="col-lg-12">
	        <div class="ibox">
	            <div class="ibox-content">
					<?php echo validation_errors(); ?>
					<?php $attributes = array('target' => '_blank'); ?>
					<?php echo admin_form_open('reports/Zeta_billing_search',$attributes ); ?>
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<div class="form-group">
									<label for="inputEmail3" class="control-label"> <?= lang('biller') ?></label>
									<select class="form-control " onchange="" name="Sucursal" id="Sucursal">
										<option value="" >Todos</option>
										<?php foreach ($billers as $biller): ?>
											<option value="<?php echo $biller['cod_companie']; ?>"><?php echo $biller['nombre']; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-md-3">
								<div class="form-group">
									<label for="inputEmail3" class="control-label"> <?= lang('module') ?></label>
									<select class="form-control" name="module_id" id="module_id">
										<option value="all" >Todos</option>
										<option value="1"><?= lang('modules')[1] ?></option>
										<option value="2"><?= lang('modules')[2] ?></option>
									</select>
								</div>
							</div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <?= lang("reference_no", "slref"); ?>
                                    <select name="document_type_id" class="form-control" id="document_type_id">
										<option value="" >Todos</option>
                                    </select>
                                </div>
                            </div>
							<div class="col-xs-12 col-md-3">
								<div class="form-group">
									<label for="fech_ini" class="control-label"> <?= lang('filter_date') ?> </label>
									<input type="date" class="form-control" id="fech_ini" name="fech_ini" required >
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<label for="inputEmail3" class="control-label"> <?= lang('print_type') ?> </label><br>
								<label class="radio-inline">
									<input type="radio" name="Informe" id="Informe1" value="noPos" checked> <?= lang('letter') ?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="Informe" id="Informe2" value="Pos" > <?= lang('strip') ?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="Informe" id="Informe3" value="Excel" > <?= lang('excel') ?>
								</label>
							</div>
							<div class="col-xs-12 col-md-3">
								<label for="inputEmail3" class="control-label"> Tipo </label><br>
								<label class="radio-inline">
									<input type="radio" name="Tipo" id="Tipo1" value="Detallado" checked> <?= lang('detailed') ?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="Tipo" id="Tipo2" value="Resumido" > <?= lang('summarized') ?>
								</label>
							</div>
							<div class="col-xs-12 col-md-6">
                                <button type="submit" class="btn btn-primary new-button" style="margin-top: 10px" data-toggle="tooltip" data-placement="right" title="<?= lang('save') ?>"><i class="fa fa-check"></i></button>
							</div>
						</div>
						<div class="row">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		<?php if ($this->session->userdata('biller_id')): ?>
			setTimeout(function() {
				$('#Sucursal').select2('val', '<?= $this->session->userdata('biller_id') ?>').select2('readonly', true).trigger('change');
			}, 850);
		<?php endif ?>
	});
	$(document).on('change', '#Sucursal, #module_id', function(){
		var biller_id = $('#Sucursal').val();
		var module_id = $('#module_id').val();
		$.ajax({
	      url:'<?= admin_url("billers/getBillersDocumentTypes/") ?>'+module_id+"/"+biller_id,
          type:'POST',
          dataType:'JSON',
          data: {
            '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash(); ?>',
            'module': module_id,
            'biller_id': biller_id,
            'empty_word': 1
          }
	    }).done(function(data){
	      response = data;

	      $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }

	      if (response.status == 0) {
	        $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
	        localStorage.setItem('locked_for_biller_resolution', 1);
	      } else {
	        if (localStorage.getItem('locked_for_biller_resolution')) {
	            localStorage.removeItem('locked_for_biller_resolution');
	        }
	      }
	        $('#document_type_id').trigger('change');
	    });
	});
</script>