<?php
/*
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'vendor/fpdf/fpdf.php';
*/
require_once APPPATH.'../vendor/fpdf/fpdf.php';
#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {     
        $this->SetTitle($this->sma->utf8Decode('Informe de cartera por edades '.date('Y-m-d H:i:s')));
        $this->SetFont('Arial','B',12);
        $this->Cell(100);        
        $this->Cell(30,4,'ANALISIS DE CARTERA POR EDADES',0,0,'C');
        $this->Ln();
        $this->Cell(37); 
        $this->SetFont('Arial','',6);
        $this->Cell(0,4,'Fecha : '.date('d-m-Y h:i A'),0,0,'C');
        if (isset($this->biller_name)) {
            $this->Ln();
            $this->SetFont('Arial','B',12);
            $this->Cell(60,4,"Sucursal : ".$this->biller_name,'',0,'L');
        }
        $this->Ln(8);
        $this->SetFont('Arial','B',$this->fuente);
        if ($this->group_data) {
            $this->Cell(40,8, 'DOCUMENTO','TB',0,'C');
            $this->Cell(20,8, 'FECHA','TB',0,'C');
            $this->Cell(10,8, 'DIAS','TB',0,'C');
            $this->SetXY($this->GetX(),$this->GetY()); 
            $this->MultiCell(17,4, 'SALDO '.chr(10).' 0 - 30','TB','C');
            $this->SetXY($this->GetX()+87,$this->GetY()-8); 
            $this->MultiCell(17,4, 'SALDO '.chr(10).' 31 - 60','TB','C');
            $this->SetXY($this->GetX()+104,$this->GetY()-8); 
            $this->MultiCell(17,4, 'SALDO '.chr(10).' 61 - 90','TB','C');
            $this->SetXY($this->GetX()+121,$this->GetY()-8); 
            $this->MultiCell(17,4, 'SALDO '.chr(10).' 91 - 120','TB','C');
            $this->SetXY($this->GetX()+138,$this->GetY()-8); 
            $this->MultiCell(17,4, 'SALDO '.chr(10).' 121 o mas','TB','C');
            $this->SetXY($this->GetX()+155,$this->GetY()-8); 
            $this->Cell(26,8, 'SALDO TOTAL','TB',0,'C');
            $this->Cell(15,8, 'VENCE','TB',0,'C');
            $this->Ln();
        } else {
            $this->Cell(15,8, 'DOCUMENTO','TB',0,'C');
            $this->Cell(56,8, 'CLIENTE','TB',0,'C');
            $this->Cell(15,8, 'FECHA','TB',0,'C');
            $this->Cell(10,8, 'DIAS','TB',0,'C');
            $this->SetXY($this->GetX(),$this->GetY()); 
            $this->MultiCell(13,4, 'SALDO '.chr(10).' 0 - 30','TB','C');
            $this->SetXY($this->GetX()+109,$this->GetY()-8); 
            $this->MultiCell(13,4, 'SALDO '.chr(10).' 31 - 60','TB','C');
            $this->SetXY($this->GetX()+122,$this->GetY()-8); 
            $this->MultiCell(13,4, 'SALDO '.chr(10).' 61 - 90','TB','C');
            $this->SetXY($this->GetX()+135,$this->GetY()-8); 
            $this->MultiCell(13,4, 'SALDO '.chr(10).' 91 - 120','TB','C');
            $this->SetXY($this->GetX()+148,$this->GetY()-8); 
            $this->MultiCell(13,4, 'SALDO '.chr(10).' 121 o mas','TB','C');
            $this->SetXY($this->GetX()+161,$this->GetY()-8); 
            $this->Cell(20,8, 'SALDO TOTAL','TB',0,'C');
            $this->Cell(15,8, 'VENCE','TB',0,'C');
            $this->Ln();
        }
    }
    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica '.'            Usuario: '.$this->User),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }
    function reduce_text_length($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 45) {
            $text = substr($text, 0, 42);
            $text.="...";
            return $text;
        }
        return $text;
    }
    function print_totals_address(){
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell(70,$this->height, $this->sma->utf8Decode("Subtotal Sucursal Cliente ".$this->address) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_30_address) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_60_address) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_90_address) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_120_address) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_mas_address) ,'T',0,'R');
        $this->Cell(26,$this->height, $this->sma->formatMoney($this->total_address) ,'T',0,'R');
        $this->Cell(15,$this->height, '' ,'T',0,'R');
        $this->Ln();
        $this->total_30_address = $this->total_60_address = $this->total_90_address = $this->total_120_address = $this->total_mas_address = $this->total_address = 0;
    }
    function print_totals_customer(){
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell(70,$this->height, $this->sma->utf8Decode("Subtotal Cliente ".$this->customer) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_30_customer) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_60_customer) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_90_customer) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_120_customer) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_mas_customer) ,'T',0,'R');
        $this->Cell(26,$this->height, $this->sma->formatMoney($this->total_customer) ,'T',0,'R');
        $this->Cell(15,$this->height, '' ,'T',0,'R');
        $this->Ln();
        $this->total_30_customer = $this->total_60_customer = $this->total_90_customer = $this->total_120_customer = $this->total_mas_customer = $this->total_customer = 0;
    }
    function print_totals_city(){
        $this->SetFont('Arial','B',$this->fuente); 
        $this->Cell(70,$this->height, $this->sma->utf8Decode("Subtotal Ciudad ".$this->city) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_30_city) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_60_city) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_90_city) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_120_city) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_mas_city) ,'T',0,'R');
        $this->Cell(26,$this->height, $this->sma->formatMoney($this->total_city) ,'T',0,'R');
        $this->Cell(15,$this->height, '' ,'T',0,'R');
        $this->Ln();
        $this->total_30_city = $this->total_60_city = $this->total_90_city = $this->total_120_city = $this->total_mas_city = $this->total_city = 0;
    }
    function print_totals_state(){
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell(70,$this->height, $this->sma->utf8Decode("Subtotal Departamento.".$this->cstate) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_30_state) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_60_state) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_90_state) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_120_state) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_mas_state) ,'T',0,'R');
        $this->Cell(26,$this->height, $this->sma->formatMoney($this->total_state) ,'T',0,'R');
        $this->Cell(15,$this->height, '' ,'T',0,'R');
        $this->Ln();
        $this->total_30_state = $this->total_60_state = $this->total_90_state = $this->total_120_state = $this->total_mas_state = $this->total_state = 0;
    }
    function print_totals_seller(){
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell(70,$this->height, $this->sma->utf8Decode("Subtotal Vendedor ".$this->seller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_30_seller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_60_seller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_90_seller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_120_seller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_mas_seller) ,'T',0,'R');
        $this->Cell(26,$this->height, $this->sma->formatMoney($this->total_seller) ,'T',0,'R');
        $this->Cell(15,$this->height, '' ,'T',0,'R');
        $this->Ln();
        $this->total_30_seller = $this->total_60_seller = $this->total_90_seller = $this->total_120_seller = $this->total_mas_seller = $this->total_seller = 0;
    }
    function print_totals_biller(){
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell(70,$this->height, $this->sma->utf8Decode("Subtotal Sucursal ".$this->biller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_30_biller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_60_biller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_90_biller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_120_biller) ,'T',0,'R');
        $this->Cell(17,$this->height, $this->sma->formatMoney($this->total_mas_biller) ,'T',0,'R');
        $this->Cell(26,$this->height, $this->sma->formatMoney($this->total_biller) ,'T',0,'R');
        $this->Cell(15,$this->height, '' ,'T',0,'R');
        $this->Ln();
        $this->total_30_biller = $this->total_60_biller = $this->total_90_biller = $this->total_120_biller = $this->total_mas_biller = $this->total_biller = 0;
    }
}

$fuente = 6;
$adicional_fuente = 2;
$height = 5;
$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->fuente = $fuente;
$pdf->height = $height;
$pdf->fuente = $fuente;
$pdf->sma = $this->sma;
$pdf->AliasNbPages();
$pdf->User = $this->session->userdata('username');
$pdf->total_30 = $pdf->total_60 = $pdf->total_90 = $pdf->total_120 = $pdf->total_mas = $total = 0;
$pdf->total_30_biller = $pdf->total_60_biller = $pdf->total_90_biller = $pdf->total_120_biller = $pdf->total_mas_biller = $pdf->total_biller = 0;
$pdf->total_30_seller = $pdf->total_60_seller = $pdf->total_90_seller = $pdf->total_120_seller = $pdf->total_mas_seller = $pdf->total_seller = 0;
$pdf->total_30_state = $pdf->total_60_state = $pdf->total_90_state = $pdf->total_120_state = $pdf->total_mas_state = $pdf->total_state = 0;
$pdf->total_30_city = $pdf->total_60_city = $pdf->total_90_city = $pdf->total_120_city = $pdf->total_mas_city = $pdf->total_city = 0;
$pdf->total_30_address = $pdf->total_60_address = $pdf->total_90_address = $pdf->total_120_address = $pdf->total_mas_address = $pdf->total_address = 0;
$pdf->total_30_customer = $pdf->total_60_customer = $pdf->total_90_customer = $pdf->total_120_customer = $pdf->total_mas_customer = $pdf->total_customer = 0;
$pdf->biller = $pdf->seller = $pdf->customer = $pdf->cstate = $pdf->city = $pdf->address = null;
$pdf->group_data = $group_data;
if (!$group_data) {
    $pdf->AddPage();
}
foreach ($sales as $sale) 
{
    if ($group_data && (!$pdf->biller || ($pdf->biller && $pdf->biller != $sale['Sucursal']))) {  
        if ($pdf->total_address > 0 && $group_by_customer_address){
            $pdf->print_totals_address();
        }
        if ($pdf->total_customer > 0 && $group_by_customer){
            $pdf->print_totals_customer();
        }
        if ($pdf->total_city > 0 && $group_by_city){
            $pdf->print_totals_city();
        }
        if ($pdf->total_state > 0 && $group_by_city){ 
            $pdf->print_totals_state();
        }  
        if ($pdf->total_seller > 0 && $group_by_seller){
            $pdf->print_totals_seller();
        }
        if ($pdf->total_biller > 0){ 
            $pdf->print_totals_biller();
        }
        $pdf->biller = $sale['Sucursal']; 
        $pdf->seller = $pdf->customer = $pdf->cstate = $pdf->city = $pdf->address = null;
        $pdf->biller_name = $pdf->biller;
        $pdf->AddPage();
    }
    if ($group_data && ($group_by_seller && (!$pdf->seller || ($pdf->seller && $pdf->seller != $sale['nom_seller'])))) { 
        if ($pdf->total_address > 0 && $group_by_customer_address){
            $pdf->print_totals_address();
        }
        if ($pdf->total_customer > 0 && $group_by_customer){
            $pdf->print_totals_customer();
        }
        if ($pdf->total_city > 0 && $group_by_city){
            $pdf->print_totals_city();
        }
        if ($pdf->total_state > 0 && $group_by_city){ 
            $pdf->print_totals_state();
        }  
        if ($pdf->total_seller > 0 && $group_by_seller){
            $pdf->print_totals_seller();
        }
        $pdf->seller = $sale['nom_seller']; 
        $pdf->customer = $pdf->cstate = $pdf->city = $pdf->address = null;
        $pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
        $pdf->Cell(196, $height, $this->sma->utf8Decode('Vendedor : '.$pdf->seller),0,0,'L');
        $pdf->SetFont('Arial','',$fuente);
        $pdf->Ln();
    }
    if ($group_data && ($group_by_city && (!$pdf->cstate || ($pdf->cstate && $pdf->cstate != $sale['customer_state'])))) { 
        if ($pdf->total_address > 0 && $group_by_customer_address){
            $pdf->print_totals_address();
        }
        if ($pdf->total_customer > 0 && $group_by_customer){
            $pdf->print_totals_customer();
        }
        if ($pdf->total_city > 0 && $group_by_city){
            $pdf->print_totals_city();
        }
        if ($pdf->total_state > 0 && $group_by_city){ 
            $pdf->print_totals_state();
        }  
        $pdf->cstate = $sale['customer_state']; 
        $pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
        $pdf->Cell(196, $height, $this->sma->utf8Decode('Departamento : '.$pdf->cstate),0,0,'L');
        $pdf->SetFont('Arial','',$fuente);
        $pdf->Ln();
    }
    if ($group_data && ($group_by_city && (!$pdf->city || ($pdf->city && $pdf->city != $sale['customer_city'])))) { 
        if ($pdf->total_address > 0 && $group_by_customer_address){
            $pdf->print_totals_address();
        }
        if ($pdf->total_customer > 0 && $group_by_customer){
            $pdf->print_totals_customer();
        }
        if ($pdf->total_city > 0 && $group_by_city){
            $pdf->print_totals_city();
        }
        $pdf->city = $sale['customer_city'];
        $pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
        $pdf->Cell(196, $height, $this->sma->utf8Decode('Ciudad : '.$pdf->city),0,0,'L');
        $pdf->SetFont('Arial','',$fuente);
        $pdf->Ln();
    }
    if ($group_data && ($group_by_customer && (!$pdf->customer || ($pdf->customer && $pdf->customer != $sale['nom_companie'])))) { 
        if ($pdf->total_address > 0 && $group_by_customer_address){
            $pdf->print_totals_address();
        }
        if ($pdf->total_customer > 0 && $group_by_customer){
            $pdf->print_totals_customer();
        }
        $pdf->customer = $sale['nom_companie']; 
        $pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
        $pdf->Cell(196, $height, $this->sma->utf8Decode('Cliente : '.$pdf->customer),0,0,'L');
        $pdf->SetFont('Arial','',$fuente);
        $pdf->Ln();
    }
    if ($group_data && ($group_by_customer_address && (!$pdf->address || ($pdf->address && $pdf->address != $sale['customer_sucursal'])))) { 
        if ($pdf->total_address > 0 && $group_by_customer_address){
            $pdf->print_totals_address();
        }
        $pdf->address = $sale['customer_sucursal']; 
        $pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
        $pdf->Cell(196, $height, $this->sma->utf8Decode('Sucursal Cliente : '.$pdf->address),0,0,'L');
        $pdf->SetFont('Arial','',$fuente);
        $pdf->Ln();
    }
    
    $pdf->SetFont('Arial','',$fuente);
    $fecha = substr(!empty($sale['due_date']) ? $sale['due_date'] : $sale['Fecha_Creacion'],0,10);
    $fecha_now = isset($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d');
    $dias   = (strtotime($fecha_now)-strtotime($fecha))/86400;
    $dias   = $dias < 0 ? 0 : $dias; 
    $dias   = floor($dias);
    $vendedor_old = $sale['cod_seller']; 
    $nit_old = $sale['documento'];
    $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 = $saldo_mas = 0;
    $Saldo = $sale['Saldo'];
    if ($dias <=30) 
    {
        $saldo_30 = $Saldo;
        $pdf->total_30 += $sale['Saldo'];
    }
    else if ($dias <=60)
    {
        $saldo_60 = $Saldo;
        $pdf->total_60 += $sale['Saldo'];
    }
    else if ($dias <=90)
    {
        $saldo_90 = $Saldo;
        $pdf->total_90 += $sale['Saldo'];
    }
    elseif ($dias <=120) 
    { 
        $saldo_120 = $Saldo;
        $pdf->total_120 += $sale['Saldo'];
    }
    else
    {
        $saldo_mas = $Saldo;
        $pdf->total_mas += $sale['Saldo'];
    }

    $total += $sale['Saldo'];
    $pdf->total_30_address += $saldo_30;
    $pdf->total_30_customer += $saldo_30;
    $pdf->total_30_city += $saldo_30;
    $pdf->total_30_state += $saldo_30;
    $pdf->total_30_seller += $saldo_30;
    $pdf->total_30_biller += $saldo_30;
    $pdf->total_60_address += $saldo_60;
    $pdf->total_60_customer += $saldo_60;
    $pdf->total_60_city += $saldo_60;
    $pdf->total_60_state += $saldo_60;
    $pdf->total_60_seller += $saldo_60;
    $pdf->total_60_biller += $saldo_60;
    $pdf->total_90_address += $saldo_90;
    $pdf->total_90_customer += $saldo_90;
    $pdf->total_90_city += $saldo_90;
    $pdf->total_90_state += $saldo_90;
    $pdf->total_90_seller += $saldo_90;
    $pdf->total_90_biller += $saldo_90;
    $pdf->total_120_address += $saldo_120;
    $pdf->total_120_customer += $saldo_120;
    $pdf->total_120_city += $saldo_120;
    $pdf->total_120_state += $saldo_120;
    $pdf->total_120_seller += $saldo_120;
    $pdf->total_120_biller += $saldo_120;
    $pdf->total_mas_address += $saldo_mas;
    $pdf->total_mas_customer += $saldo_mas;
    $pdf->total_mas_city += $saldo_mas;
    $pdf->total_mas_state += $saldo_mas;
    $pdf->total_mas_seller += $saldo_mas;
    $pdf->total_mas_biller += $saldo_mas;
    $pdf->total_address += $sale['Saldo'];
    $pdf->total_customer += $sale['Saldo'];
    $pdf->total_city += $sale['Saldo'];
    $pdf->total_state += $sale['Saldo'];
    $pdf->total_seller += $sale['Saldo'];
    $pdf->total_biller += $sale['Saldo'];

    if ($group_data) {
         if ($dias >= 0) {
            $pdf->Cell(40,$height, $sale['Referencia'] ,0,0,'L');
            $pdf->Cell(20,$height, substr($sale['Fecha_Creacion'],0,10) ,0,0,'C');
            $pdf->Cell(10,$height, $dias ,0,0,'C');
            $pdf->Cell(17,$height, $this->sma->formatMoney($saldo_30) ,0,0,'R');
            $pdf->Cell(17,$height, $this->sma->formatMoney($saldo_60) ,0,0,'R');
            $pdf->Cell(17,$height, $this->sma->formatMoney($saldo_90) ,0,0,'R');
            $pdf->Cell(17,$height, $this->sma->formatMoney($saldo_120) ,0,0,'R');
            $pdf->Cell(17,$height, $this->sma->formatMoney($saldo_mas) ,0,0,'R');
            $pdf->Cell(26,$height, $this->sma->formatMoney($sale['Saldo']) ,0,0,'R');
            $pdf->Cell(15,$height, $sale['due_date'] ,0,0,'R');
            $pdf->Ln();
        }
    } else {
         if ($dias >= 0) {
            $pdf->Cell(15,$height, $sale['Referencia'],'',0,'L');
            $pdf->Cell(56,$height, $pdf->reduce_text_length($sale['nom_companie']),'',0,'L');
            $pdf->Cell(15,$height,  substr($sale['Fecha_Creacion'],0,10),'',0,'L');
            $pdf->Cell(10,$height, $dias,'',0,'L');
            $pdf->Cell(13,$height, $this->sma->formatMoney($saldo_30),'',0,'C');
            $pdf->Cell(13,$height, $this->sma->formatMoney($saldo_60),'',0,'C');
            $pdf->Cell(13,$height, $this->sma->formatMoney($saldo_90),'',0,'C');
            $pdf->Cell(13,$height, $this->sma->formatMoney($saldo_120),'',0,'C');
            $pdf->Cell(13,$height, $this->sma->formatMoney($saldo_mas),'',0,'C');
            $pdf->Cell(20,$height, $this->sma->formatMoney($sale['Saldo']),'',0,'C');
            $pdf->Cell(15,$height, $sale['due_date'],'',0,'C');
            $pdf->Ln();
        }
    }
}

$pdf->SetFont('Arial','B',$fuente);

if ($pdf->total_address > 0 && $group_by_customer_address){
    $pdf->print_totals_address();
}
if ($pdf->total_customer > 0 && $group_by_customer){
    $pdf->print_totals_customer();
}
if ($pdf->total_city > 0 && $group_by_city){
    $pdf->print_totals_city();
}
if ($pdf->total_state > 0 && $group_by_city){ 
    $pdf->print_totals_state();
}  
if ($pdf->total_seller > 0 && $group_by_seller){
    $pdf->print_totals_seller();
}
if ($pdf->total_biller > 0){ 
    $pdf->print_totals_biller();
}

if ($group_data) {
    $pdf->Cell(70,$height, $this->sma->utf8Decode("Total") ,'T',0,'R');
    $pdf->Cell(17,$height, $this->sma->formatMoney($pdf->total_30) ,'T',0,'R');
    $pdf->Cell(17,$height, $this->sma->formatMoney($pdf->total_60) ,'T',0,'R');
    $pdf->Cell(17,$height, $this->sma->formatMoney($pdf->total_90) ,'T',0,'R');
    $pdf->Cell(17,$height, $this->sma->formatMoney($pdf->total_120) ,'T',0,'R');
    $pdf->Cell(17,$height, $this->sma->formatMoney($pdf->total_mas) ,'T',0,'R');
    $pdf->Cell(26,$height, $this->sma->formatMoney($total) ,'T',0,'R');
    $pdf->Cell(15,$height, '' ,'T',0,'R');
} else {
    $pdf->Cell(96,$height, $this->sma->utf8Decode("Total") ,'T',0,'R');
    $pdf->Cell(13,$height, $this->sma->formatMoney($pdf->total_30) ,'T',0,'R');
    $pdf->Cell(13,$height, $this->sma->formatMoney($pdf->total_60) ,'T',0,'R');
    $pdf->Cell(13,$height, $this->sma->formatMoney($pdf->total_90) ,'T',0,'R');
    $pdf->Cell(13,$height, $this->sma->formatMoney($pdf->total_120) ,'T',0,'R');
    $pdf->Cell(13,$height, $this->sma->formatMoney($pdf->total_mas) ,'T',0,'R');
    $pdf->Cell(20,$height, $this->sma->formatMoney($total) ,'T',0,'R');
    $pdf->Cell(15,$height, '' ,'T',0,'R');
}
$pdf->Ln();
$pdf->Output('Informe de cartera por edades '.date('Y-m-d H_i_s').'.pdf', "I");