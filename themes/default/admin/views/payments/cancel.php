<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('cancel_payment'); ?></h2>
    </div>
</div>

<?php
    $bl[""] = "";
    $bldata = [];
    foreach ($billers as $biller) {
        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
        $bldata[$biller->id] = $biller;
    }
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php $attrib = array('id' => 'cancel_payment');
                    echo admin_form_open_multipart("payments/cancel/".$reference, $attrib); ?>
                    <input type="hidden" name="customer_id" value="<?= $encabezado->company_id ? $encabezado->company_id : $encabezado->customer_id?>">
                    <input type="hidden" name="cost_center_id" value="<?= $encabezado->cost_center_id ?>">
                    <input type="hidden" name="reference_no" value="<?= $encabezado->reference_no ?>">
                        <div class="row">
                            <div class="resAlert"></div>
                            <div class="col-sm-3">
                                <?= lang('date', 'date') ?>
                                <input type="text" name="date" id="date" class="form-control" value="<?= $encabezado->date; ?>">
                            </div>
                            <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= lang("biller", "biller"); ?>
                                        <select name="biller" class="form-control" id="biller" required="required">
                                            <?php foreach ($billers as $biller): ?>
                                              <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="col-sm-3" style="display: none;">
                                    <div class="form-group">
                                        <?= lang("biller", "biller"); ?>
                                        <select name="biller" class="form-control" id="biller">
                                            <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                $biller = $bldata[$this->session->userdata('biller_id')];
                                                ?>
                                                <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group col-sm-3">
                                <?= lang("reference_no", "document_type_id"); ?>
                                <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                            </div>

                            <div class="clearfix"></div>

                            <div style="font-size: 110%;">
                              <div class="col-sm-3">
                                <span><b><?= lang('reference_no') ?></b> :<br><?= $encabezado->reference_no ?></span>
                              </div>
                              <div class="col-sm-3">
                                <span><b><?= lang('customer') ?></b> :<br> <?= $encabezado->customer ?></span>
                              </div>
                              <div class="col-sm-3">
                                <span><b><?= lang('vat_no') ?></b> :<br> <?= $encabezado->vat_no ?></span>
                              </div>
                              <div class="col-sm-3">
                                <span><b><?= lang('date') ?></b> :<br> <?= $encabezado->date ?></span>
                              </div>
                              <div class="col-sm-3">
                                <span><b><?= lang('paid_by') ?></b> :<br> <?= $encabezado->paid_by ?></span>
                              </div>
                            </div>


                            <div class="clearfix"></div>

                            <div class="col-sm-12" style="padding-top: 3%">
                              <table class="table col-xs-12" style="width: 100%;">
                                <thead>
                                  <tr>
                                    <th><?= lang('affects_to') ?></th>
                                    <th><?= lang('value') ?></th>
                                    <th><?= lang('rete_fuente') ?></th>
                                    <th><?= lang('rete_iva') ?></th>
                                    <th><?= lang('rete_ica') ?></th>
                                    <th><?= lang('rete_bomberil') ?></th>
                                    <th><?= lang('rete_autoaviso') ?></th>
                                    <th><?= lang('rete_other') ?></th>
                                    <th><?= lang('total_discount') ?></th>
                                    <th><?= lang('amount_paid') ?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $total_amount = 0;
                                  $total_rete_fuente = 0;
                                  $total_rete_iva = 0;
                                  $total_rete_ica = 0;
                                  $total_rete_bomberil = 0;
                                  $total_rete_autoaviso = 0;
                                  $total_rete_other = 0;
                                  $total_discount = 0;
                                  $total_received = 0;
                                   ?>
                                  <?php if ($pmnts): ?>
                                    <?php
                                    $sales_discount = [];
                                    foreach ($pmnts as $pmnt) {
                                      if ($pmnt->type == 'discount') {
                                        if (isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id])) {
                                          $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] += $pmnt->amount;
                                        } else {
                                          $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] = $pmnt->amount;
                                        }
                                      } else {
                                        continue;
                                      }
                                    }
                                     ?>
                                    <?php foreach ($pmnts as $pmnt): ?>
                                      <?php if($pmnt->type == 'discount') { continue; } ?>
                                      <?php
                                          $sale_discount = isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] : 0;
                                          $total_discount += $sale_discount;
                                          $total_rete_fuente += $pmnt->rete_fuente_total;
                                          $total_rete_iva += $pmnt->rete_iva_total;
                                          $total_rete_ica += $pmnt->rete_ica_total;
                                          $total_rete_bomberil += $pmnt->rete_bomberil_total;
                                          $total_rete_autoaviso += $pmnt->rete_autoaviso_total;
                                          $total_rete_other += $pmnt->rete_other_total;
                                          $total_received += ($pmnt->reference_no != NULL ? $pmnt->amount : 0 ) + $sale_discount;
                                          $total_retentions = $pmnt->rete_fuente_total + $pmnt->rete_iva_total + $pmnt->rete_ica_total + $pmnt->rete_bomberil_total + $pmnt->rete_autoaviso_total + $pmnt->rete_other_total;
                                          $total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
                                       ?>
                                      <tr>
                                        <td><?= $pmnt->reference_no != NULL ? $pmnt->reference_no : $pmnt->note ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->reference_no != NULL ? $pmnt->amount + (isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] : 0) : 0) ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->rete_fuente_total) ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->rete_iva_total) ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->rete_ica_total) ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->rete_bomberil_total) ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->rete_autoaviso_total) ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->rete_other_total) ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney(isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] * -1 : 0) ?></td>
                                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->amount - $pmnt->rete_fuente_total - $pmnt->rete_iva_total - $pmnt->rete_ica_total - $pmnt->rete_bomberil_total - $pmnt->rete_autoaviso_total - $pmnt->rete_other_total) ?></td>
                                      </tr>
                                    <?php endforeach ?>
                                  <?php endif ?>
                                    <tr>
                                      <th><?= lang('total') ?></th>
                                      <td class="text-right"><?= $this->sma->formatMoney($total_received) ?></td>
                                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_fuente) ?></td>
                                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_iva) ?></td>
                                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_ica) ?></td>
                                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_bomberil) ?></td>
                                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_autoaviso) ?></td>
                                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_other) ?></td>
                                      <td class="text-right"><?= $this->sma->formatMoney(($total_discount * -1)) ?></td>
                                      <td class="text-right">
                                        <?= $this->sma->formatMoney($total_amount) ?>
                                        <input type="hidden" name="amount-paid" value="<?= $total_amount ?>">
                                      </td>
                                    </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        <?= form_submit('submit', lang("submit"), 'id="submit" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){
        $('#biller').trigger('change');
        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            startDate: "<?= date('Y-m-d', strtotime($encabezado->date)); ?>",
            endDate: max_input_date,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
    });

    $(document).on('change', '#biller', function(){
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/33/") ?>'+$('#biller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        });
    });

    $(document).on('change', '#date', function(){
        min_date = "<?= date('Y-m-d', strtotime($encabezado->date)); ?>";
        sel_date = moment($(this).val(),'DD/MM/YYYY HH:mm:ss a').format('YYYY-MM-DD');
        if (sel_date < min_date) {
            command: toastr.error('La fecha seleccionada para la anulación, no puede ser menor a la del recibo', 'Fecha Anulación incorrecta', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "6000",
                "extendedTimeOut": "1000",
            });
            $("#date").datetimepicker('update', new Date());
        }
    });
</script>