$(document).ready(function(){
	if (assproduction_order = localStorage.getItem('assproduction_order')) {
		nsProductionOrder(assproduction_order);
	} else {
		nsProductionOrder();
	}

	if (asssupplier = localStorage.getItem('asssupplier')) {
		$('#asssupplier').val(asssupplier).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        ptype: 4,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#asssupplier').trigger('change');
	} else {
		nsSupplier();
	}

    if (asscutting_order = localStorage.getItem('asscutting_order')) {
        $('#asscutting_order').val(asscutting_order).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"production_order/get_cutting_order/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "production_order/cutting_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#asscutting_order').trigger('change');
    } else {
        nsCuttingOrder();
    }

	if (assdate = localStorage.getItem('assdate')) {
		$('#assdate').val(assdate);
	}
	if (assest_date = localStorage.getItem('assest_date')) {
		$('#assest_date').val(assest_date);
	}
	if (assreference_no = localStorage.getItem('assreference_no')) {
		$('#assreference_no').val(assreference_no);
	}

    if (cuttings_id) {
        remove_local_storage();
        $.ajax({
            url : site.base_url+'production_order/get_cutting_order_items/0/0',
            data : {
                cuttings_id : cuttings_id.ids,
            },
            dataType : 'JSON'
        }).done(function(data){
            localStorage.setItem('assitems', JSON.stringify(data.por_rows));
            localStorage.setItem('asscomproducts', JSON.stringify(data.rows));
            localStorage.setItem('assproduction_order', data.production_order_id);
            nsProductionOrder(data.production_order_id);
            loadItems();
        });
    }
	
});

$(document).on('change', '#assproduction_order', function(){
    if (!$('#asscutting_order').val() && cuttings_id == false) {
        $.ajax({
            url : site.base_url+'production_order/get_cutting_order_items/0/'+$(this).val(),
            dataType : 'JSON'
        }).done(function(data){
            localStorage.setItem('assitems', JSON.stringify(data.por_rows));
            localStorage.setItem('asscomproducts', JSON.stringify(data.rows));

            setTimeout(function() {
                $("#assdate").datetimepicker('remove').datetimepicker({
                    format: site.dateFormats.js_ldate,
                    fontAwesome: true,
                    language: 'sma',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    enableOnReadonly : false,
                    startDate : data.por_date,
                    endDate : data.por_est_date
                });
                // $("#assdate").val(data.por_date);
                $("#assest_date").datetimepicker('remove').datetimepicker({
                    format: site.dateFormats.js_ldate,
                    fontAwesome: true,
                    language: 'sma',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    enableOnReadonly : false,
                    startDate : data.por_date,
                    endDate : data.por_est_date
                });
                $("#assest_date").val(data.por_est_date);
            }, 850);

            loadItems();
        });
    }
    localStorage.setItem('assproduction_order', $(this).val());
    $('#assproduction_order_reference_no').val($('#assproduction_order').select2('data').text);
});

$(document).on('change', '#asscutting_order', function(){
    $.ajax({
        url : site.base_url+'production_order/get_cutting_order_items/'+$(this).val(),
        dataType : 'JSON'
    }).done(function(data){
        localStorage.setItem('assitems', JSON.stringify(data.por_rows));
        localStorage.setItem('asscomproducts', JSON.stringify(data.rows));
        localStorage.setItem('assproduction_order', data.production_order_id);
        nsProductionOrder(data.production_order_id);
        loadItems();
    });
    localStorage.setItem('asscutting_order', $(this).val());
    $('#asscutting_order_reference_no').val($('#asscutting_order').select2('data').text);
});


$(document).on('change', '#asssupplier', function(){
	localStorage.setItem('asssupplier', $(this).val());
});
$(document).on('change', '#assest_date', function(){
	localStorage.setItem('assest_date', $(this).val());
});
$(document).on('change', '#assdate', function(){
	localStorage.setItem('assdate', $(this).val());
});
$(document).on('change', '#assreference_no', function(){
	localStorage.setItem('assreference_no', $(this).val());
});

function nsProductionOrder(por_id = false){

    if (por_id != false) {
        $('#assproduction_order').val(por_id).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"production_order/get_production_order/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {

                        setTimeout(function() {
                            $("#assdate").datetimepicker('remove').datetimepicker({
                                format: site.dateFormats.js_ldate,
                                fontAwesome: true,
                                language: 'sma',
                                weekStart: 1,
                                todayBtn: 1,
                                autoclose: 1,
                                todayHighlight: 1,
                                startView: 2,
                                forceParse: 0,
                                enableOnReadonly : false,
                                startDate : data[0].date,
                                endDate : data[0].est_date
                            });
                            // $("#assdate").val(data[0].date);
                            $("#assest_date").datetimepicker('remove').datetimepicker({
                                format: site.dateFormats.js_ldate,
                                fontAwesome: true,
                                language: 'sma',
                                weekStart: 1,
                                todayBtn: 1,
                                autoclose: 1,
                                todayHighlight: 1,
                                startView: 2,
                                forceParse: 0,
                                enableOnReadonly : false,
                                startDate : data[0].date,
                                endDate : data[0].est_date
                            });
                            $("#assest_date").val(data[0].est_date);
                        }, 850);

                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "production_order/production_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#assproduction_order').trigger('change');
    } else {
        $('#assproduction_order').select2({
            minimumInputLength: 1,
            width: '100%',
            ajax: {
                url: site.base_url + "production_order/production_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }
}

function nsCuttingOrder(){
    $('#asscutting_order').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "production_order/cutting_order_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function nsSupplier(){
	$('#asssupplier').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 5,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function loadItems(tabindex) {
    var decimals = 3;
    if (site.settings.rounding == 1) {
        decimals = 1;
    }

    if (localStorage.getItem('assitems')) {
        total_order_quantity = 0;
        total_cutted_quantity = 0;
        an = 1;
        Ttotal = 0;
        $("#assTable tbody").empty();
        assitems = JSON.parse(localStorage.getItem('assitems'));
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(assitems, function(o){return [parseInt(o.order)];}) : assitems;
        if (comproducts !== undefined) { delete comproducts; }
        var comproducts = [];
        $.each(sortedItems, function () {
            var item = this;
            var item_id = item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.product_id, item_qty = item.quantity, item_cutted_qty = item.cutted_quantity, item_code = item.product_code, item_name = item.product_name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '"><input name="product_name[]" type="hidden" value="' + item_name + '"><span class="sname" id="name_' + row_no + '">' + item_code +' - ' + item_name +'</span>'+
                        '</td>';
            tr_html += '<td>'+lang[item.type]+'</td>';
            tr_html += '<td>'+(item.variant != null ? item.variant : '')+'</td>';
            // tr_html += '<td>'+
            //                 '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatDecimal(item_cutted_qty, decimals) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" readonly>'+
            //             '</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatDecimal(item_cutted_qty, decimals) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" readonly>'+
                        '</td>';
            tr_html += '<td class="text-center"><i class="fa fa-times tip assdel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#assTable");
            total_order_quantity += parseFloat(item_qty);
            total_cutted_quantity += parseFloat(item_cutted_qty);
            an++;
        });
        var col = 4;
        var tfoot = '<tr id="tfoot" class="tfoot active">'+(site.settings.set_adjustment_cost ? '<th></th>' : '')+
                        '<th colspan="2">Total</th>'+
                        // '<th class="text-center">' + formatQuantity2(parseFloat(total_cutted_quantity)) + '</th>'+
                        '<th class="text-center">' + formatQuantity2(parseFloat(total_cutted_quantity)) + '</th>'+
                        '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                    '</tr>';
        $('#assTable tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        set_page_focus();
        loadCompositionItems(tabindex);
    }
}

function loadCompositionItems(focus_tabindex){

	comproducts = JSON.parse(localStorage.getItem('asscomproducts'));

    var decimals = 3;
    if (site.settings.rounding == 1) {
        decimals = 1;
    }
    $("#assCTable tbody").empty();
    count = 1;
    Ttotal = 0;
    ttorder = 0;
    ttfinished = 0;
    ttprocess = 0;
    ttassemble = 0;
    tabindex = 1;
    show_por_alert = false;
    $.each(comproducts, function (index, item) {
        if (item === undefined) { return; }
        var trclass = 'class="cproduct_'+item.id+'"';
        var newTr = $('<tr data-item-id="' + index + '"></tr>');
        tr_html = '<td '+trclass+'>'+
                        '<input name="cp_product_id[]" type="hidden" class="rid" value="' + item.product_id + '"><input name="cp_product_name[]" type="hidden" value="' + item.product_name + '"><span class="sname">' + item.product_code +' - ' + item.product_name +'</span>'+
                        '<input name="cp_cutting_detail_id[]" type="hidden" class="rid" value="' + (item.cutting_detail_id != "NULL" ? item.cutting_detail_id : null) + '">'+
                        '<input name="cp_production_order_pid[]" type="hidden" class="rid" value="' + item.production_order_pid + '">'+
                        '<input name="cp_production_order_option_id[]" type="hidden" class="rid" value="' + item.option_id + '">'+
                    '</td>';
        tr_html += '<td>'+
                            '<span>' + item.por_product_name +' - ' + item.por_product_code + (item.por_variant != null ? " - "+item.por_variant : "") + ' </span>'+
                        '</td>';
        tr_html += '<td '+trclass+'>'+lang[item.type]+'</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center order_quantity" name="order_quantity[]" type="text" value="' + formatDecimal((item.cutting_quantity - item.fault_quantity), decimals) + '" readonly>'+
                    '</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center finished_quantity" name="finished_quantity[]" type="text" value="' + formatDecimal((item.assemble_finished_quantity ? item.assemble_finished_quantity : 0), decimals) + '" readonly>'+
                    '</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center in_process_quantity" name="in_process_quantity[]" type="text" value="' + formatDecimal((item.assemble_in_process_quantity ? item.assemble_in_process_quantity : 0) - (item.assemble_finished_quantity ? item.assemble_finished_quantity : 0), decimals) + '" readonly>'+
                    '</td>';
        ass_input_qty = ((item.cutting_quantity - item.fault_quantity) - (item.assemble_in_process_quantity ? item.assemble_in_process_quantity : 0));
        if (ass_input_qty < 0) {
            ass_input_qty = 0;
            show_por_alert = true;
        }
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center to_assemble_quantity" name="to_assemble_quantity[]" type="text" value="' + 
                        formatDecimal(item.manual_qty !== undefined ? item.manual_qty : ass_input_qty, decimals) + '" max="'+ ass_input_qty +'" id="cqty_'+item.id+'" tabindex="'+tabindex+'">'+
                        '<label class="error" for="cqty_'+item.id+'" style="display:none;"></label>'+
                    '</td>';
        newTr.html(tr_html);
        newTr.appendTo("#assCTable");
        count += parseFloat(item.qty);
        ttorder += parseFloat(item.cutting_quantity - item.fault_quantity);
        ttfinished += parseFloat(item.assemble_finished_quantity ? item.assemble_finished_quantity : 0);
        ttprocess += parseFloat((item.assemble_in_process_quantity ? item.assemble_in_process_quantity : 0) - (item.assemble_finished_quantity ? item.assemble_finished_quantity : 0));
        ttassemble += parseFloat(item.manual_qty !== undefined ? item.manual_qty : (ass_input_qty));
        tabindex++;
    });


    var tfoot = '<tr id="tfoot" class="tfoot active">'+
                    '<th colspan="3">Total</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(ttorder)) + '</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(ttfinished)) + '</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(ttprocess)) + '</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(ttassemble)) + '</th>'+
                '</tr>';
    $('#assCTable tfoot').html(tfoot);
    if (show_por_alert) {
        command: toastr.warning('Existen ordenes de ensamble con productos en estado "en proceso" o "terminado" que tienen mayor cantidad que la orden de corte seleccionada, porfavor seleccione desde la orden de confección.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "8000",
                    "extendedTimeOut": "1000",
                });
    }

    if (focus_tabindex) {
        console.log('focus_tabindex '+focus_tabindex);
        $('[tabindex='+(focus_tabindex+1)+']').focus().select();
    } else {

        $('[tabindex='+1+']').focus().select();
    }

}

$('#reset').click(function (e) {
    bootbox.confirm(lang.r_u_sure, function (result) {
        if (result) {
            remove_local_storage();
            $('#modal-loading').show();
            location.reload();
        }
    });
});



$(document).on("change", '.to_assemble_quantity', function () {
    asscomproducts = JSON.parse(localStorage.getItem('asscomproducts'));
    var max = parseFloat($(this).prop('max'));
    var row = $(this).closest('tr');
    if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0 || parseFloat($(this).val()) > max) {
        $(this).val(max);
        command: toastr.error('El valor digitado está fuera del margen permitido.', '¡Atención!', {
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "4000",
            "extendedTimeOut": "1000",
        });
        return;
    }
    var new_qty = parseFloat($(this).val()),
    item_id = row.attr('data-item-id');
    asscomproducts[item_id].manual_qty = new_qty;
    localStorage.setItem('asscomproducts', JSON.stringify(asscomproducts));
    loadItems($(this).prop('tabindex'));
}).on('keydown', '.to_assemble_quantity', function(e){
    if (e.keyCode == 13) {
        $(this).trigger('change');
    }
});

function remove_local_storage(){
    if (localStorage.getItem('assitems')) {
        localStorage.removeItem('assitems');
    }
    if (localStorage.getItem('asscomproducts')) {
        localStorage.removeItem('asscomproducts');
    }
    if (localStorage.getItem('assdate')) {
        localStorage.removeItem('assdate');
    }
    if (localStorage.getItem('assest_date')) {
        localStorage.removeItem('assest_date');
    }
    if (localStorage.getItem('assproduction_order')) {
        localStorage.removeItem('assproduction_order');
    }
    if (localStorage.getItem('asscutting_order')) {
        localStorage.removeItem('asscutting_order');
    }
    if (localStorage.getItem('assreference_no')) {
        localStorage.removeItem('assreference_no');
    }
    if (localStorage.getItem('asssupplier')) {
        localStorage.removeItem('asssupplier');
    }
}