<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var is_edit = true;
    var count = 1, an = 1, product_variant = 0, shipping = 0,
        product_tax = 0, total = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>, toitems = {},
        audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3'),
        audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function () {
        <?php if ($transfer) { ?>
        localStorage.setItem('todate', '<?= date($dateFormats['php_ldate'], strtotime($transfer->date)) ?>');
        localStorage.setItem('from_warehouse', '<?= $transfer->from_warehouse_id ?>');
        localStorage.setItem('toref', '<?= $transfer->reference_no ?>');
        localStorage.setItem('to_warehouse', '<?= $transfer->to_warehouse_id ?>');
        localStorage.setItem('tostatus', '<?= $transfer->status ?>');
        localStorage.setItem('tonote', '<?= $this->sma->decode_html($transfer->note); ?>');
        localStorage.setItem('toshipping', '<?= $transfer->shipping ?>');
        localStorage.setItem('toitems', JSON.stringify(<?= $transfer_items; ?>));

        localStorage.setItem('tobiller_origin', '<?= $transfer->biller_id ?>');
        localStorage.setItem('tobiller_destination', '<?= $transfer->destination_biller_id ?>');
        <?php } ?>
        <?php if ($Owner || $Admin) { ?>
        $(document).on('change', '#todate', function (e) {
            localStorage.setItem('todate', $(this).val());
        });
        if (todate = localStorage.getItem('todate')) {
            $('#todate').val(todate);
        }
        <?php } ?>
        ItemnTotals();
        $("#add_item").autocomplete({
            //source: '<?= admin_url('transfers/suggestions'); ?>',
            source: function (request, response) {
                if (!$('#from_warehouse').val()) {
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    bootbox.alert('<?=lang('select_above');?>');
                    //response('');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('transfers/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#from_warehouse").val(),
                        tobiller_origin: "<?= $transfer->biller_id ?>",
                        tobiller_destination: "<?= $transfer->destination_biller_id ?>",
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    if ($('#from_warehouse').val()) {
                        bootbox.alert('<?= lang('no_match_found') ?>', function () {
                            $('#add_item').focus();
                        });
                    } else {
                        bootbox.alert('<?= lang('please_select_warehouse') ?>', function () {
                            $('#add_item').focus();
                        });
                    }
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_transfer_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });

        $(window).bind('beforeunload', function (e) {
            $.get('<?= admin_url('welcome/set_data/remove_tols/1'); ?>');
            if (count > 1) {
                var message = "You will loss data!";
                return message;
            }
        });
        $('#reset').click(function (e) {
            $(window).unbind('beforeunload');
        });
        $('#edit_transfer').click(function () {
            $(window).unbind('beforeunload');
            $('form.edit-to-form').submit();
        });
        var to_warehouse;
        $('#to_warehouse').on("select2-focus", function (e) {
            to_warehouse = $(this).val();
        }).on("select2-close", function (e) {
            if ($(this).val() == $('#from_warehouse').val()) {
                $(this).select2('val', to_warehouse);
                bootbox.alert('<?= lang('please_select_different_warehouse') ?>');
            }
        });
        var from_warehouse;
        $('#from_warehouse').on("select2-focus", function (e) {
            from_warehouse = $(this).val();
        }).on("select2-close", function (e) {
            if ($(this).val() == $('#to_warehouse').val()) {
                $(this).select2('val', from_warehouse);
                bootbox.alert('<?= lang('please_select_different_warehouse') ?>');
            }
        });
        /*var status = "
        <?=$transfer->status?>";
         $('#tostatus').change(function(){
         if(status == 'completed') {
         bootbox.alert('
        <?=lang('can_not_change_status_of_completed_transfer')?>');
         $('#tostatus').select2("val", tostatus);
         }
         });*/

    });
</script>

<div class="box">
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-to-form');
                echo admin_form_open_multipart("transfers/edit/" . $transfer->id, $attrib)
                ?>


                <div class="row">
                    <div class="col-lg-12">
                            <?php
                            $attrib = array('id' => 'add_tranfers_form', 'data-toggle' => 'validator', 'role' => 'form', 'target' => '_blank');
                            echo admin_form_open_multipart("transfers/add", $attrib)
                            ?>
                            <div class="row">
                                <div class="col-lg-12 resAlert"></div>
                                <?php if ($Owner || $Admin) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("date", "todate"); ?>
                                            <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="todate" required="required"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller_origin", "tobiller_origin"); ?>
                                            <select name="biller" class="form-control" id="tobiller_origin" required="required">
                                                <?php foreach ($billers as $biller): ?>
                                                  <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>"><?= $biller->company  ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4" style="display: none;">
                                        <div class="form-group">
                                            <?= lang("biller_origin", "tobiller_origin"); ?>
                                            <select name="biller" class="form-control" id="tobiller_origin">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>" selected="selected"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("biller_destination", "tobiller_destination"); ?>
                                        <select name="biller_destination" class="form-control" id="tobiller_destination" required="required">
                                            <?php if (count($billers) > 1): ?>
                                                <option value=""><?= lang('select') ?></option>
                                            <?php endif ?>
                                            <?php foreach ($billers as $biller): ?>
                                              <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>"><?= $biller->company ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("reference_no", "slref"); ?>
                                        <input type="text" name="reference_no" value="<?= $transfer->reference_no ?>" readonly class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("status", "tostatus"); ?>
                                        <?php
                                        $post = array('completed' => lang('completed'), 'pending' => lang('pending'), 'sent' => lang('sent'));
                                        echo form_dropdown('status', $post, (isset($_POST['status']) ? $_POST['status'] : ''), 'id="tostatus" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" style="width:100%;" ');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= lang('third', 'companies_id') ?>
                                    <?php
                                    $cOptions[''] = lang('select').lang('third');
                                    foreach ($companies as $row => $company) {
                                        $cOptions[$company->id] = $company->name;
                                    }

                                     ?>
                                    <?= form_dropdown('companies_id', $cOptions, '',' id="companies_id" class="form-control select" required style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("document", "document") ?>
                                        <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                            data-show-preview="false" class="form-control file">
                                    </div>
                                </div>
                                <?php if (isset($cost_centers)): ?>
                                    <div class="col-md-4 form-group">
                                        <?= lang('cost_center', 'cost_center_id') ?>
                                        <?php
                                        $ccopts[''] = lang('select');
                                        if ($cost_centers) {
                                            foreach ($cost_centers as $cost_center) {
                                                $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                            }
                                        }
                                         ?>
                                        <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <?= lang('destination_cost_center', 'destination_cost_center_id') ?>
                                        <?= form_dropdown('destination_cost_center_id', $ccopts, '', 'id="destination_cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="row">
                                <?php

                                    $wh[''] = lang('select');
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }

                                 ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("from_warehouse", "from_warehouse"); ?>

                                            <?php
                                            echo form_dropdown('from_warehouse', $wh, (isset($_POST['from_warehouse']) ? $_POST['from_warehouse'] : ''), 'id="from_warehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("from_warehouse") . '" required="required" style="width:100%;" ');
                                            ?>
                                        </div>
                                    </div>
                                <?php } else {
                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'from_warehouse',
                                        'id' => 'from_warehouse',
                                        'value' => $this->session->userdata('warehouse_id'),
                                    );
                                    echo form_input($warehouse_input);
                                } ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("to_warehouse", "to_warehouse"); ?>
                                        <?php
                                        echo form_dropdown('to_warehouse', $wh, (isset($_POST['to_warehouse']) ? $_POST['to_warehouse'] : ''), 'id="to_warehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("to_warehouse") . '" required="required" style="width:100%;" ');
                                        ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-12" id="sticker">
                                    <div class="well well-sm">
                                        <div class="form-group" style="margin-bottom:0;">
                                            <div class="input-group wide-tip">
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <i class="fa fa-2x fa-barcode addIcon"></i>
                                                </div>
                                                    <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_product_to_order") . '"'); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="control-group table-group">
                                        <label class="table-label"><?= lang("order_items"); ?></label>

                                        <div class="controls table-controls">
                                            <table id="toTable"
                                                class="table items  table-bordered table-condensed table-hover sortable_table">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <?= lang('product'); ?>
                                                    </th>
                                                    <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                                                        <th><?= lang("variant") ?></th>
                                                    <?php endif ?>
                                                    <th class="thead_variant"><?= lang("variant") ?></th>
                                                    <?php if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10): ?>
                                                        <th><?= lang('unit') ?></th>
                                                        <th><?= lang('product_unit_quantity') ?></th>
                                                        <th><?= lang('product_unit_cost') ?></th>
                                                    <?php endif ?>
                                                    <?php if ($this->Settings->product_serial): ?>
                                                        <th><?= lang("serial_no") ?></th>
                                                    <?php endif ?>
                                                    <?php
                                                    if ($Settings->product_expiry) {
                                                        echo '<th >' . $this->lang->line("expiry_date") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="thead_discount"><?= lang("net_unit_cost"); ?></th>
                                                    <?php
                                                    if ($Settings->product_discount) {
                                                        echo '<th class="thead_discount">' . $this->lang->line("discount") . '</th>';
                                                    }
                                                    ?>
                                                    <?php $restrict_permission = ($Admin || $Owner || $GP['products-cost'] == 1) ? '' : 'display:none;' ?>
                                                    <th style="width:160px; <?= $restrict_permission ?>"><?= lang("net_unit_cost"); ?></th>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        // echo '<th class="col-md-1">' . $this->lang->line("product_tax") . '</th>';
                                                        echo "<th style='".$restrict_permission."'>" . $this->lang->line("product_tax") . ' (<span class="currency">'. $default_currency->code.'</span>)  </th>';
                                                    }
                                                    if ($Settings->ipoconsumo) {
                                                        echo "<th style='".$restrict_permission."'>" . $this->lang->line("second_product_tax") . ' (<span class="currency">'. $default_currency->code.'</span>)  </th>';
                                                    }
                                                    ?>
                                                    <th style="width:160px; <?= $restrict_permission ?>"><?= lang('cost_x_tax') ?></th>
                                                    <th class="thead_shipping"><?= lang('purchase_shipping') ?></th>
                                                    <th style="width: 110px !important;"><?= lang("quantity"); ?></th>
                                                    <th style="<?= $restrict_permission ?>">
                                                        <?= lang("subtotal"); ?> (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th>
                                                    <th style="width: 30px !important; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                                </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot></tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="from-group">
                                        <?= lang("note", "tonote"); ?>
                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'id="tonote" class="form-control" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                    <div class="from-group"><?php echo form_submit('add_transfers', $this->lang->line("submit"), 'id="add_transfers" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                        <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                    <tr class="warning">
                                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                        <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                        <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                                        <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <?php echo form_close(); ?>
                        </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                        <div class="col-sm-8">
                            <div id="punits-div"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pprice" class="col-sm-4 control-label"><?= lang('cost') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pprice">
                        </div>
                    </div>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>
<?php if (!$Owner || !$Admin || $this->session->userdata('warehouse_id')) { ?>
<script>
    $(document).ready(function() {
        $("#to_warehouse option[value='<?= $this->session->userdata('warehouse_id'); ?>']").attr('disabled', 'disabled');
    });
</script>
<?php } ?>
