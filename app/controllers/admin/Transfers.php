<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transfers extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('transfers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('transfers_model');
        $this->load->admin_model('products_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->data['logo'] = true;
    }

    public function index()
    {
        $this->sma->checkPermissions();
        if ($this->input->get('new_transfers')) {
            $this->session->set_userdata('view_new_transfers', 1);
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('transfers')));
        $meta = array('page_title' => lang('transfers'), 'bc' => $bc);
        $this->page_construct('transfers/index', $meta, $this->data);
    }

    function getTransfers()
    {
        $this->sma->checkPermissions('index');
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date')." 00:00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date')." 23:59:00" : NULL;
        $warehouse_origin = $this->input->post('warehouse_origin_id') ? $this->input->post('warehouse_origin_id') : NULL;
        $warehouse_destination = $this->input->post('warehouse_destination_id') ? $this->input->post('warehouse_destination_id') : NULL;
        $status = $this->input->post('status') ? $this->input->post('status') : NULL;
        $accepted = $this->input->post('accepted') ? $this->input->post('accepted') : NULL;

        $detail_link = anchor('admin/transfers/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('transfer_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('admin/transfers/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_transfer'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/transfers/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_transfer'));
        $pdf_link = anchor('admin/transfers/pdf/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $print_barcode = anchor('admin/products/print_barcodes/?transfer=$1', '<i class="fa fa-print"></i> ' . lang('print_barcodes'));
        $delete_link = "<a href='#' class='tip po' title='<b>" . lang("delete_transfer") . "</b>' data-content=\"<p>"
                . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' id='a__$1' href='" . admin_url('transfers/delete/$1') . "'>"
                . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
                . lang('delete_transfer') . "</a>";
            $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>' . $detail_link . '</li>
                <li>' . $edit_link . '</li>
                <li>' . $pdf_link . '</li>
                <li>' . $email_link . '</li>
                <li>' . $print_barcode . '</li>
            </ul>
        </div></div>';

        $this->load->library('datatables');

        $this->datatables
            ->select("id, date, reference_no, from_warehouse_name as fname, from_warehouse_code as fcode, to_warehouse_name as tname,to_warehouse_code as tcode, total, total_tax, grand_total, status, CONCAT(id,'-',received), attachment")
            ->from('transfers')
            ->edit_column("fname", "$1 ($2)", "fname, fcode")
            ->edit_column("tname", "$1 ($2)", "tname, tcode");

        if (!$this->Owner && !$this->Admin) {
            if ($this->session->userdata('view_new_transfers')) {
                $this->session->unset_userdata('view_new_transfers');
                $this->datatables->where('to_warehouse_id', $this->session->userdata('warehouse_id'))->where('received', 0);
            } else {
                if ($this->session->userdata('warehouse_id')) {
                    $conditions = '';
                    $conditions .= "{$this->db->dbprefix('transfers')}.from_warehouse_id = " .$this->session->userdata('warehouse_id');
                    $conditions .= " OR {$this->db->dbprefix('transfers')}.to_warehouse_id = " .$this->session->userdata('warehouse_id');
                    $this->datatables->where("({$conditions})");
                }
                if ($this->session->userdata('biller_id')) {
                    $conditions = '';
                    $conditions .= "{$this->db->dbprefix('transfers')}.biller_id = " .$this->session->userdata('biller_id');
                    $conditions .= " OR {$this->db->dbprefix('transfers')}.destination_biller_id = " .$this->session->userdata('biller_id');
                    $this->datatables->where("({$conditions})");
                }
            }
                
            if (!$this->session->userdata('view_right')) {
                $this->datatables->where('created_by', $this->session->userdata('user_id'));
            }
        }

        if ($start_date) {
            $this->datatables->where('date >=', $start_date);
        }

        if ($end_date) {
            $this->datatables->where('date <=', $end_date);
        }

        if ($warehouse_origin) {
            $this->datatables->where('from_warehouse_id =', $warehouse_origin);
        }

        if ($warehouse_destination) {
            $this->datatables->where('to_warehouse_id =', $warehouse_destination);
        }

        if ($status) {
            $this->datatables->where('status =', $status);
        }

        if ($accepted) {
            if ($accepted == 2) {
                $accepted = 0;
            }
            $this->datatables->where('received =', $accepted);
        }

        $this->datatables->add_column("Actions", $action, "id")
            ->unset_column('fcode')
            ->unset_column('tcode');
        echo $this->datatables->generate();
    }

    function add($order_id = NULL)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('to_warehouse', lang("warehouse") . ' (' . lang("to") . ')', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('from_warehouse', lang("warehouse") . ' (' . lang("from") . ')', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('biller', lang("biller_origin"), 'required');
        $this->form_validation->set_rules('companies_id', lang("third"), 'required');
        $this->form_validation->set_rules('biller_destination', lang("biller_destination"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("reference_no"), 'required');

        if ($this->Settings->cost_center_selection == 1 && $this->Settings->modulary == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
            $this->form_validation->set_rules('destination_cost_center_id', lang("destination_cost_center"), 'required');
        }

        if ($this->form_validation->run()) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $to_warehouse = $this->input->post('to_warehouse');
            $from_warehouse = $this->input->post('from_warehouse');

            $biller = $this->input->post('biller');
            $biller_destination = $this->input->post('biller_destination');
            $document_type_id = $this->input->post('document_type_id');
            $companies_id = $this->input->post('companies_id');

            $note = $this->sma->clear_tags($this->input->post('note'));
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $status = $this->input->post('status');
            $from_warehouse_details = $this->site->getWarehouseByID($from_warehouse);
            $from_warehouse_code = $from_warehouse_details->code;
            $from_warehouse_name = $from_warehouse_details->name;
            $to_warehouse_details = $this->site->getWarehouseByID($to_warehouse);
            $to_warehouse_code = $to_warehouse_details->code;
            $to_warehouse_name = $to_warehouse_details->name;

            $order_id = $this->input->post('order_id');
            if ($order_id) {
                $order = $this->transfers_model->getOrderTransferId($order_id);
                $note.="Traslado originado desde orden de traslado N° ".$order->reference_no;
            }

            $total = 0;
            $consumption_purchase = 0;
            $product_tax = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $item_id = 0;
            $i = sizeof($_POST['product_code']);
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_net_cost = $this->sma->formatDecimal($_POST['net_cost'][$r]);
                $unit_cost = $this->sma->formatDecimal($_POST['unit_cost'][$r]);
                $real_unit_cost = $this->sma->formatDecimal($_POST['real_unit_cost'][$r]);
                $item_unit_quantity = str_replace(",", "", $_POST['quantity'][$r]);
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : null;
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null;
                $item_tax_2_rate = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                $item_unit_tax_2_val = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : null;
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : null;
                $serialModal_serial = isset($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : null;
                $product_unit_id_selected = isset($_POST['product_unit_id_selected'][$r]) ? $_POST['product_unit_id_selected'][$r] : null;
                $unit_product_tax_2_percentage = isset($_POST['unit_product_tax_2_percentage'][$r]) ? $_POST['unit_product_tax_2_percentage'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_base_unit = $_POST['product_purchase_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];
                if (isset($item_code) && isset($real_unit_cost) && isset($unit_cost) && isset($item_quantity)) {
                    $unit_cost = $this->sma->formatDecimal($unit_cost);
                    $pr_item_tax = $this->sma->formatDecimal($item_unit_tax_val * $item_unit_quantity);
                    $pr_item_tax_2 = $this->sma->formatDecimal($item_unit_tax_2_val * $item_unit_quantity);
                    $item_tax = $item_unit_tax_val;
                    $item_tax_2 = $item_unit_tax_2_val;
                    $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                    $tax_details_2 = $this->site->getTaxRateByID($item_tax_2_rate);
                    $tax_details_2_secondary = $this->site->getTaxRate2ByID($item_tax_2_rate);
                    $tax = $tax_2 = 0;
                    if ($item_tax_rate) {
                        $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
                        if ($tax_details->type == 2 && $tax_details->rate < 1 && $item_tax > 0) {
                            $tax = $item_tax;
                        }
                    }
                    if ($item_tax_2_rate && $tax_details_2) {
                        $tax_2 = $this->sma->formatDecimal($tax_details_2->rate, 0) . "%";
                        if ($tax_details_2->type == 2 && $tax_details_2->rate < 1 && $item_tax_2 > 0) {
                            $tax_2 = $item_tax_2;
                        }
                    }
                    $item_tax_2_percentage = $item_tax_2_rate;
                    if ($item_tax_2_rate && $tax_details_2_secondary) {
                        $item_tax_2_percentage =  $unit_product_tax_2_percentage;
                    }

                    $product_tax += $pr_item_tax + $pr_item_tax_2;
                    $consumption_purchase += $pr_item_tax_2;
                    $subtotal = (($item_net_cost * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $unit = $this->site->getUnitByID($item_unit > 0 ? $item_unit : $item_base_unit);
                    if ($item_quantity < 0.01) {
                        $this->session->set_flashdata('error', lang("invalid_product_quantity"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'option_id' => $item_option,
                        'net_unit_cost' => $item_net_cost,
                        'unit_cost' => $this->sma->formatDecimal($item_net_cost + $item_tax + $item_tax_2),
                        'quantity' => $item_quantity,
                        'product_unit_id' => ($item_unit > 0 ? $item_unit : $item_base_unit),
                        'product_unit_code' => isset($unit->code) ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'quantity_balance' => $item_quantity,
                        'quantity_received' => $item_quantity,
                        'warehouse_id' => $to_warehouse,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $item_tax_2_rate,
                        'tax_2' => $item_tax_2_percentage,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'real_unit_cost' => $real_unit_cost,
                        'date' => date('Y-m-d', strtotime($date)),
                        'serial_no' => $serialModal_serial ? $serialModal_serial : $item_serial,
                        'product_unit_id_selected' => $product_unit_id_selected,
                        'consumption_purchase' => $pr_item_tax_2,
                    );
                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity));
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $grand_total = $this->sma->formatDecimal(($total + $shipping + $product_tax), 4);
            $data = array(
                'date' => $this->site->movement_setted_datetime($date),
                'from_warehouse_id' => $from_warehouse,
                'from_warehouse_code' => $from_warehouse_code,
                'from_warehouse_name' => $from_warehouse_name,
                'to_warehouse_id' => $to_warehouse,
                'to_warehouse_code' => $to_warehouse_code,
                'to_warehouse_name' => $to_warehouse_name,
                'note' => $note,
                'total' => $total,
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('user_id'),
                'status' => $status,
                'shipping' => $shipping,
                'biller_id' => $biller,
                'destination_biller_id' => $biller_destination,
                'document_type_id' => $document_type_id,
                'companies_id' => $companies_id,
                'consumption_purchase' => $consumption_purchase,
                'total_tax' => $product_tax,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($this->Settings->cost_center_selection == 0) {
                $biller_cost_center = $this->site->getBillerCostCenter($biller);
                $destination_biller_cost_center = $this->site->getBillerCostCenter($biller_destination);
                if ($biller_cost_center && $destination_biller_cost_center) {
                    $data['cost_center_id'] = $biller_cost_center->id;
                    $data['destination_cost_center_id'] = $destination_biller_cost_center->id;
                } else {
                    $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                    redirect($_SERVER['HTTP_REFERER']);
                }
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
                $data['destination_cost_center_id'] = $this->input->post('destination_cost_center_id');
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            // $this->sma->print_arrays($data, $products);
            $res_id = $this->transfers_model->addTransfer($data, $products, $order_id);
        }

        if ($this->form_validation->run() == true && $res_id) {
            $this->session->set_userdata('remove_tols', 1);
            $this->session->set_flashdata('message', lang("transfer_added"));
            admin_redirect("transfers/view_pos/$res_id");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['name'] = array('name' => 'name',
                'id' => 'name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('name'),
            );
            $this->data['quantity'] = array('name' => 'quantity',
                'id' => 'quantity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('quantity'),
            );
            if ($order_id) {
                $order = $this->transfers_model->getOrderTransferId($order_id);
                if ($order->status == 'completed') {
                    $this->session->set_flashdata('error', lang('order_transfer_already_completed'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $items = $this->transfers_model->getOrderTransferItems($order_id);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                    } else {
                        unset($row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->quantity = 0;
                    $row->expiry = '';
                    $row->base_quantity = 0;
                    $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                    $row->product_unit_id_selected = $row->unit ? $row->unit : $item->product_unit_id;
                    $row->base_unit_cost = $row->cost ? $row->cost : $item->unit_cost;
                    $row->qty = 0;
                    $row->quantity_balance = 0;
                    $row->ordered_quantity = 0;
                    $row->quantity += 0;
                    $row->real_unit_cost = $row->cost;
                    $row->option = NULL;
                    $options = $this->transfers_model->getProductOptions($row->id, $order->warehouse_id, FALSE);
                    $pis = $this->site->getPurchasedItems($item->product_id, $order->warehouse_id);
                    if($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->quantity += 0;
                    $units = $this->site->getUnitsByBUID($row->base_unit);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $ri = $this->Settings->item_addition ? $row->id : $c;
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                        'row' => $row, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                    $c++;
                }
                $this->data['order_transfer_items'] = $pr;
                $this->data['order'] = $order;
            }
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['rnumber'] = ''; //$this->site->getReference('to');
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['companies'] = $this->site->getAllCompaniesWithState($this->Settings->companies_for_adjustments_transfers, null, true);
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('transfers'), 'page' => lang('transfers')), array('link' => '#', 'page' => lang('add_transfer')));
            $meta = array('page_title' => lang('add_transfer'), 'bc' => $bc);
            $this->page_construct('transfers/add', $meta, $this->data);
        }
    }

    function edit($id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $transfer = $this->transfers_model->getTransferByID($id);
        if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($transfer->created_by);
        }
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('to_warehouse', lang("warehouse") . ' (' . lang("to") . ')', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('from_warehouse', lang("warehouse") . ' (' . lang("from") . ')', 'required|is_natural_no_zero');

        if ($this->form_validation->run()) {

            $reference_no = $this->input->post('reference_no');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $to_warehouse = $this->input->post('to_warehouse');
            $from_warehouse = $this->input->post('from_warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $status = $this->input->post('status');
            $from_warehouse_details = $this->site->getWarehouseByID($from_warehouse);
            $from_warehouse_code = $from_warehouse_details->code;
            $from_warehouse_name = $from_warehouse_details->name;
            $to_warehouse_details = $this->site->getWarehouseByID($to_warehouse);
            $to_warehouse_code = $to_warehouse_details->code;
            $to_warehouse_name = $to_warehouse_details->name;

            $total = 0;
            $product_tax = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_code = $_POST['product_code'][$r];
                $item_net_cost = $this->sma->formatDecimal($_POST['net_cost'][$r]);
                $unit_cost = $this->sma->formatDecimal($_POST['unit_cost'][$r]);
                $real_unit_cost = $this->sma->formatDecimal($_POST['real_unit_cost'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $quantity_balance = $_POST['quantity_balance'][$r];
                $ordered_quantity = $_POST['ordered_quantity'][$r];
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : NULL;
                $item_expiry = isset($_POST['expiry'][$r]) ? $this->sma->fsd($_POST['expiry'][$r]) : NULL;
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'undefined' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : NULL;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($real_unit_cost) && isset($unit_cost) && isset($item_quantity)) {
                    $product_details = $this->transfers_model->getProductByCode($item_code);
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";
                    $item_net_cost = $unit_cost;

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {

                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_cost);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!empty($product_details) && $product_details->tax_method != 1) {
                            $item_net_cost = $unit_cost - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, false, $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = $this->sma->formatDecimal((($item_net_cost * $item_unit_quantity) + $pr_item_tax), 4);
                    $unit = $this->site->getUnitByID($item_unit);
                    // exit(var_dump($ordered_quantity));
                    $balance_qty =  ($status != 'completed') ? $item_quantity : ($item_quantity - ($ordered_quantity - $quantity_balance));
                    $product = array(
                        'product_id' => $product_details->id,
                        'product_code' => $item_code,
                        'product_name' => $product_details->name,
                        'option_id' => $item_option,
                        'net_unit_cost' => $item_net_cost,
                        'unit_cost' => $this->sma->formatDecimal(($item_net_cost + $item_tax), 4),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $item_unit,
                        'product_unit_code' => $unit->code,
                        'unit_quantity' => $item_unit_quantity,
                        'quantity_balance' => $balance_qty,
                        'warehouse_id' => $to_warehouse,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'expiry' => $item_expiry,
                        'real_unit_cost' => $real_unit_cost,
                        'date' => date('Y-m-d', strtotime($date)),
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $grand_total = $this->sma->formatDecimal(($total + $shipping + $product_tax), 4);
            $data = array('reference_no' => $reference_no,
                'date' => $date,
                'from_warehouse_id' => $from_warehouse,
                'from_warehouse_code' => $from_warehouse_code,
                'from_warehouse_name' => $from_warehouse_name,
                'to_warehouse_id' => $to_warehouse,
                'to_warehouse_code' => $to_warehouse_code,
                'to_warehouse_name' => $to_warehouse_name,
                'note' => $note,
                'total_tax' => $product_tax,
                'total' => $total,
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('user_id'),
                'status' => $status,
                'shipping' => $shipping
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }


        }

        if ($this->form_validation->run() == true && $this->transfers_model->updateTransfer($id, $data, $products)) {
            $this->session->set_userdata('remove_tols', 1);
            $this->session->set_flashdata('message', lang("transfer_updated"));
            admin_redirect("transfers");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['transfer'] = $this->transfers_model->getTransferByID($id);
            $transfer_items = $this->transfers_model->getAllTransferItems($id, $this->data['transfer']->status);
            krsort($transfer_items);
            $c = rand(100000, 9999999);
            foreach ($transfer_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                } else {
                    unset($row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                }
                $row->quantity = 0;
                $row->expiry = (($item->expiry && $item->expiry != '0000-00-00') ? $this->sma->hrsd($item->expiry) : '');
                $row->base_quantity = $item->quantity;
                $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                $row->product_unit_id_selected = $row->unit ? $row->unit : $item->product_unit_id;
                $row->base_unit_cost = $row->cost ? $row->cost : $item->unit_cost;
                $row->unit = $item->product_unit_id;
                $row->qty = $item->quantity;
                $row->quantity_balance = $item->quantity_balance;
                $row->ordered_quantity = $item->quantity;
                $row->quantity += $item->quantity_balance;
                $row->cost = $item->net_unit_cost;
                $row->unit_cost = $item->net_unit_cost+($item->item_tax/$item->quantity);
                $row->real_unit_cost = $item->real_unit_cost;
                $row->tax_rate = $item->tax_rate_id;
                $row->option = $item->option_id;
                $options = $this->transfers_model->getProductOptions($row->id, $this->data['transfer']->from_warehouse_id, FALSE);
                $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if($pis) {
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->quantity += $item->quantity;
                // if ($options) {
                //     $option_quantity = 0;
                //     foreach ($options as $option) {
                //         $pis = $this->site->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                //         if($pis){
                //             foreach ($pis as $pi) {
                //                 $option_quantity += $pi->quantity_balance;
                //             }
                //         }
                //         $option_quantity += $item->quantity;
                //         if($option->quantity > $option_quantity) {
                //             $option->quantity = $option_quantity;
                //         }
                //     }
                // }

                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $ri = $this->Settings->item_addition ? $row->id : $c;

                $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                $c++;
            }

            $this->data['transfer_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['companies'] = $this->site->getAllCompanies($this->Settings->companies_for_adjustments_transfers);

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('transfers'), 'page' => lang('transfers')), array('link' => '#', 'page' => lang('edit_transfer')));
            $meta = array('page_title' => lang('edit_transfer_quantity'), 'bc' => $bc);
            $this->page_construct('transfers/edit', $meta, $this->data);
        }
    }

    function transfer_by_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('to_warehouse', lang("warehouse") . ' (' . lang("to") . ')', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('from_warehouse', lang("warehouse") . ' (' . lang("from") . ')', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run()) {

            $reference_no = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('to');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $to_warehouse = $this->input->post('to_warehouse');
            $from_warehouse = $this->input->post('from_warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $status = $this->input->post('status');
            $from_warehouse_details = $this->site->getWarehouseByID($from_warehouse);
            $from_warehouse_code = $from_warehouse_details->code;
            $from_warehouse_name = $from_warehouse_details->name;
            $to_warehouse_details = $this->site->getWarehouseByID($to_warehouse);
            $to_warehouse_code = $to_warehouse_details->code;
            $to_warehouse_name = $to_warehouse_details->name;
            $biller = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');

            $total = 0;
            $product_tax = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            if (isset($_FILES["userfile"])) {

                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("transfers/transfer_bt_csv");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('product', 'unit_cost', 'quantity', 'variant', 'expiry');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $value = $this->site->cleanRowCsvImport($value);
                    if (!$final[] = array_combine($keys, $value)) { //VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key+1)));
                        admin_redirect("transfers/transfer_by_csv");
                        break;
                    }
                }

                $rw = 2;
                foreach ($final as $csv_pr) {

                    $item_code = $csv_pr['product'];
                    $unit_cost = $csv_pr['unit_cost'];
                    $item_quantity = $csv_pr['quantity'];
                    $variant = isset($csv_pr['variant']) ? $csv_pr['variant'] : NULL;
                    $item_expiry = isset($csv_pr['expiry']) ? $this->sma->fsd($csv_pr['expiry']) : NULL;

                    if (isset($item_code) && isset($unit_cost) && isset($item_quantity)) {
                        if (!($product_details = $this->transfers_model->getProductByCode($item_code))) {
                            $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $csv_pr['product'] . " ). " . lang("line_no") . " " . $rw);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        if ($variant) {
                            $item_option = $this->transfers_model->getProductVariantByName($variant, $product_details->id);
                            if (!$item_option) {
                                $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $csv_pr['product'] . " - " . $csv_pr['variant'] . " ). " . lang("line_no") . " " . $rw);
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $item_option = json_decode('{}');
                            $item_option->id = NULL;
                        }

                        if (!$this->Settings->overselling) {
                            $warehouse_quantity = $this->transfers_model->getWarehouseProduct($from_warehouse_details->id, $product_details->id, $item_option->id);
                            if ($warehouse_quantity->quantity < $item_quantity) {
                                $this->session->set_flashdata('error', lang("no_match_found") . " (" . lang('product_name') . " <strong>" . $product_details->name . "</strong> " . lang('product_code') . " <strong>" . $product_details->code . "</strong>) " . lang("line_no") . " " . $rw);
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }

                        $pr_item_tax = $item_tax = 0;
                        $tax = "";
                        $item_net_cost = $unit_cost;
                        if (isset($product_details->tax_rate) && $product_details->tax_rate != 0) {

                            $tax_details = $this->site->getTaxRateByID($product_details->tax_rate);
                            $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_cost);
                            $item_tax = $ctax['amount'];
                            $tax = $ctax['tax'];
                            if (!empty($product_details) && $product_details->tax_method != 1) {
                                $item_net_cost = $unit_cost - $item_tax;
                            }
                            $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_quantity), 4);
                            if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, false, $tax_details)) {
                                $total_cgst += $gst_data['cgst'];
                                $total_sgst += $gst_data['sgst'];
                                $total_igst += $gst_data['igst'];
                            }
                        }

                        $product_tax += $pr_item_tax;
                        $subtotal = $this->sma->formatDecimal((($item_net_cost * $item_quantity) + $pr_item_tax), 4);
                        $unit = $this->site->getUnitByID($product_details->unit);

                        $product = array(
                            'product_id' => $product_details->id,
                            'product_code' => $item_code,
                            'product_name' => $product_details->name,
                            'option_id' => $item_option->id,
                            'net_unit_cost' => $item_net_cost,
                            'unit_cost' => $this->sma->formatDecimal($unit_cost, 4),
                            'quantity' => $item_quantity,
                            'product_unit_id' => $unit ? $unit->id : NULL,
                            'product_unit_code' => $unit ? $unit->code : NULL,
                            'unit_quantity' => $item_quantity,
                            'quantity_balance' => $item_quantity,
                            'warehouse_id' => $to_warehouse,
                            'item_tax' => $pr_item_tax,
                            'tax_rate_id' => $product_details->tax_rate,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'expiry' => $item_expiry,
                            'real_unit_cost' => $unit_cost,
                            'date' => date('Y-m-d', strtotime($date)),
                        );

                        $products[] = ($product + $gst_data);
                        $total += $this->sma->formatDecimal(($item_net_cost * $item_quantity), 4);
                    }
                    $rw++;
                }
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_item"), 'required');
            } else {
                krsort($products);
            }
            $grand_total = $total + $shipping + $product_tax;
            $data = array('reference_no' => $reference_no,
                'date' => $date,
                'biller_id' => $biller,
                'document_type_id' => $document_type_id,
                'from_warehouse_id' => $from_warehouse,
                'from_warehouse_code' => $from_warehouse_code,
                'from_warehouse_name' => $from_warehouse_name,
                'to_warehouse_id' => $to_warehouse,
                'to_warehouse_code' => $to_warehouse_code,
                'to_warehouse_name' => $to_warehouse_name,
                'note' => $note,
                'total_tax' => $product_tax,
                'total' => $total,
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('user_id'),
                'status' => $status,
                'shipping' => $shipping
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);

        }

        if ($this->form_validation->run() == true && $this->transfers_model->addTransfer($data, $products)) {
            $this->session->set_userdata('remove_tols', 1);
            $this->session->set_flashdata('message', lang("transfer_added"));
            admin_redirect("transfers");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['name'] = array('name' => 'name',
                'id' => 'name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('name'),
            );
            $this->data['quantity'] = array('name' => 'quantity',
                'id' => 'quantity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('quantity'),
            );

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['rnumber'] = $this->site->getReference('to');
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('transfers'), 'page' => lang('transfers')), array('link' => '#', 'page' => lang('transfer_by_csv')));
            $meta = array('page_title' => lang('add_transfer_by_csv'), 'bc' => $bc);
            $this->page_construct('transfers/transfer_by_csv', $meta, $this->data);
        }
    }

    function view($transfer_id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);

        if ($this->input->get('id')) {
            $transfer_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $transfer = $this->transfers_model->getTransferByID($transfer_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($transfer->created_by, true);
        }
        $this->data['rows'] = $this->transfers_model->getAllTransferItems($transfer_id, $transfer->status);
        $this->data['from_warehouse'] = $this->site->getWarehouseByID($transfer->from_warehouse_id);
        $this->data['to_warehouse'] = $this->site->getWarehouseByID($transfer->to_warehouse_id);
        $this->data['from_biller'] = $this->site->getCompanyByID($transfer->biller_id);
        $this->data['to_biller'] = $this->site->getCompanyByID($transfer->destination_biller_id);
        $this->data['transfer'] = $transfer;
        $this->data['tid'] = $transfer_id;
        $this->data['created_by'] = $this->site->getUser($transfer->created_by);
        $this->load_view($this->theme . 'transfers/view', $this->data);
    }

    function view_pos($transfer_id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);

        if ($this->input->get('id')) {
            $transfer_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $transfer = $this->transfers_model->getTransferByID($transfer_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($transfer->created_by, true);
        }
        $this->data['rows'] = $this->transfers_model->getAllTransferItems($transfer_id, $transfer->status);
        $this->data['from_warehouse'] = $this->site->getWarehouseByID($transfer->from_warehouse_id);
        $this->data['to_warehouse'] = $this->site->getWarehouseByID($transfer->to_warehouse_id);
        $this->data['transfer'] = $transfer;
        $this->data['tid'] = $transfer_id;
        $this->data['created_by'] = $this->site->getUser($transfer->created_by);
        $this->data['biller'] = $this->site->getCompanyByID($transfer->biller_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($transfer->document_type_id);
        $this->load_view($this->theme . 'transfers/view_pos', $this->data);
    }

    function pdf($transfer_id = NULL, $view = NULL, $save_bufffer = NULL)
    {
        if ($this->input->get('id')) {
            $transfer_id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $transfer = $this->transfers_model->getTransferByID($transfer_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($transfer->created_by);
        }
        $this->data['rows'] = $this->transfers_model->getAllTransferItems($transfer_id, $transfer->status);
        $this->data['from_warehouse'] = $this->site->getWarehouseByID($transfer->from_warehouse_id);
        $this->data['to_warehouse'] = $this->site->getWarehouseByID($transfer->to_warehouse_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $transfer->biller_id);
        $this->data['transfer'] = $transfer;
        $this->data['invoice_header'] = $this->site->getInvoiceHeader($transfer->document_type_id, $transfer->reference_no);
        $this->data['document_type'] = $this->site->getDocumentTypeById($transfer->document_type_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($transfer->document_type_id, $transfer->reference_no);
        $this->data['tid'] = $transfer_id;
        $this->data['created_by'] = $this->site->getUser($transfer->created_by);
        $name = lang("transfer") . "_" . str_replace('/', '_', $transfer->reference_no) . ".pdf";
        $this->load_view($this->theme . 'transfers/pdf', $this->data);
        // if ($view) {
        //     $this->load_view($this->theme . 'transfers/pdf', $this->data);
        // } elseif ($save_bufffer) {
        //     return $this->sma->generate_pdf($html, $name, $save_bufffer);
        // } else {
        //     $this->sma->generate_pdf($html, $name);
        // }

            // $this->load_view($this->theme . 'transfers/pdf', $this->data);

    }

    public function combine_pdf($transfers_id)
    {
        $this->sma->checkPermissions('pdf');

        foreach ($transfers_id as $transfer_id) {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $transfer = $this->transfers_model->getTransferByID($transfer_id);
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($transfer->created_by);
            }
            $this->data['rows'] = $this->transfers_model->getAllTransferItems($transfer_id, $transfer->status);
            $this->data['from_warehouse'] = $this->site->getWarehouseByID($transfer->from_warehouse_id);
            $this->data['to_warehouse'] = $this->site->getWarehouseByID($transfer->to_warehouse_id);
            $this->data['transfer'] = $transfer;
            $this->data['tid'] = $transfer_id;
            $this->data['created_by'] = $this->site->getUser($transfer->created_by);

            $html[] = array(
                'content' => $this->load_view($this->theme . 'transfers/pdf', $this->data, TRUE),
                'footer' => '',
            );
        }

        $name = lang("transfers") . ".pdf";
        $this->sma->generate_pdf($html, $name);

    }

    function email($transfer_id = NULL)
    {
        $this->sma->checkPermissions(false, true);
        if ($this->input->get('id')) {
            $transfer_id = $this->input->get('id');
        }
        $transfer = $this->transfers_model->getTransferByID($transfer_id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        $this->form_validation->set_rules('note', lang("message"), 'trim');
        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($transfer->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = NULL;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = NULL;
            }

            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $transfer->reference_no,
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>'
            );
            $msg = $this->input->post('note');
            $message = $this->parser->parse_string($msg, $parse_data);
            $attachment = $this->pdf($transfer_id, NULL, 'S');

            try {
                if ($this->sma->send_email($to, $subject, $message, NULL, NULL, $attachment, $cc, $bcc)) {
                    delete_files($attachment);
                    $this->session->set_flashdata('message', lang("email_sent"));
                    admin_redirect("transfers");
                }
            } catch (Exception $e) {
                $this->session->set_flashdata('error', $e->getMessage());
                redirect($_SERVER["HTTP_REFERER"]);
            }

        } elseif ($this->input->post('send_email')) {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            if (file_exists('./themes/' . $this->Settings->theme . '/admin/views/email_templates/transfer.html')) {
                $transfer_temp = file_get_contents('themes/' . $this->Settings->theme . '/admin/views/email_templates/transfer.html');
            } else {
                $transfer_temp = file_get_contents('./themes/default/admin/views/email_templates/transfer.html');
            }
            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('transfer_order').' (' . $transfer->reference_no . ') '.lang('from').' ' . $transfer->from_warehouse_name),
            );
            $this->data['note'] = array('name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $transfer_temp),
            );
            $this->data['warehouse'] = $this->site->getWarehouseByID($transfer->to_warehouse_id);

            $this->data['id'] = $transfer_id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'transfers/email', $this->data);

        }
    }

    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->transfers_model->deleteTransfer($id)) {
            if($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("transfer_deleted")));
            }
            $this->session->set_flashdata('message', lang('transfer_deleted'));
            admin_redirect('welcome');
        }
    }

    function suggestions()
    {
        $this->sma->checkPermissions('index', TRUE);
        $term = $this->input->get('term', TRUE);
        $warehouse_id = $this->input->get('warehouse_id', TRUE);
        $tobiller_origin = $this->input->get('tobiller_origin', TRUE);
        $tobiller_destination = $this->input->get('tobiller_destination', TRUE);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];

        $rows = $this->transfers_model->getProductNames($sr, $warehouse_id, $tobiller_origin, $tobiller_destination);
        if ($rows) {
            $r = 0;
            foreach ($rows as $row) {
                $c = uniqid(mt_rand(), true);
                $option = FALSE;
                // $row->quantity = 0;
                $row->item_tax_method = $row->tax_method;
                $row->base_quantity = 1;
                $row->base_unit = $row->unit;
                $row->base_unit_cost = $row->cost;
                $row->unit = $row->purchase_unit ? $row->purchase_unit : $row->unit;
                $row->qty = 1;
                $row->discount = '0';
                $row->expiry = '';
                $row->quantity_balance = 0;
                $row->ordered_quantity = 0;
                $row->serial = '';
                $row->product_unit_id_selected = $row->purchase_unit ? $row->purchase_unit : $row->unit;
                $options = $this->transfers_model->getProductOptions($row->id, $warehouse_id, false);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->transfers_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->cost = 0;
                    $option_id = FALSE;
                }
                $row->option = $option_id;
                if (isset($row->variant_selected)) {
                    $row->variant_selected = $row->variant_selected;
                    $row->option = $row->variant_selected;
                }
                // $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                // if($pis){
                //     foreach ($pis as $pi) {
                //         $row->quantity += $pi->quantity_balance;
                //     }
                // }
                $wh_pr = $this->products_model->getProductDataByWarehouse(['product_id' => $row->id, 'warehouse_id' => $warehouse_id]);
                $row->wh_quantity = $wh_pr ? $wh_pr->quantity : 0;
                if ($pwlc = $this->transfers_model->get_product_warehouse_last_cost($row->id, $warehouse_id)) {
                    $row->cost = $pwlc->unit_cost;
                }
                if ($opt->cost != 0) {
                    $row->cost = $opt->cost;
                }
                $row->real_unit_cost = $row->cost;
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                if ($this->Settings->prioridad_precios_producto == 5 && $units[$row->unit]->operation_value > 0) {
                    $row->base_quantity = $units[$row->unit]->operation_value;
                    $row->qty = $units[$row->unit]->operation_value;
                }
                if (!$this->Settings->ipoconsumo) {
                    $row->consumption_purchase_tax = 0;
                }
                $pr[] = array('id' => sha1($c.$r), 'item_id' => ($row->id.($row->option)), 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function transfer_actions()
    {
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    foreach ($_POST['val'] as $id) {
                        $this->transfers_model->deleteTransfer($id);
                    }
                    $this->session->set_flashdata('message', lang("transfers_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'combine') {

                    $html = $this->combine_pdf($_POST['val']);

                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('transfers'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('from_warehouse'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('to_warehouse'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('grand_total'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('status'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('create_by'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $tansfer = $this->transfers_model->getTransferByID($id);
                        $user = $this->site->getUser($tansfer->created_by)->first_name .' '. $this->site->getUser($tansfer->created_by)->last_name;
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($tansfer->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $tansfer->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $tansfer->from_warehouse_name);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $tansfer->to_warehouse_name);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $tansfer->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $tansfer->status);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $user);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'tansfers_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } else if ($this->input->post('form_action') == 'post_transfer'){
                    foreach ($_POST['val'] as $id) {
                        $this->transfers_model->post_transfer($id);
                    }
                    $this->session->set_flashdata('message', lang("transfers_reaccounted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_transfer_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function update_status($id)
    {

        $this->form_validation->set_rules('status', lang("status"), 'required');

        if ($this->form_validation->run() == true) {
            $status = $this->input->post('status');
            $note = $this->sma->clear_tags($this->input->post('note'));
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'transfers');
        }

        if ($this->form_validation->run() == true && $this->transfers_model->updateStatus($id, $status, $note)) {
            $this->session->set_flashdata('message', lang('status_updated'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'transfers');
        } else {

            $this->data['inv'] = $this->transfers_model->getTransferByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'transfers/update_status', $this->data);

        }
    }

    public function product_variants_selection($product_id, $toitems_id, $warehouse_id, $search = NULL)
    {
        $options = $this->products_model->getProductOptions($product_id, $warehouse_id, $search);
        if (!empty($options)) {
            $html = "";
            $html .= '<ul class="ob">';
            foreach ($options as $option) {
                if (($option->quantity <= 0 && $this->Settings->overselling == 0)) {
                    continue;
                }
                $html .= '<li>
                            <button type="button" class="btn btn-primary btn-outline product_variant_select" data-pvid="' . $option->id . '">
                                <strong style="font-size:120%;" class="suspend_note">' . $option->name . '</strong><br>
                                <strong>' . $this->sma->formatQuantity($option->quantity) . '</strong>
                            </button>
                        </li>';
            }
            $html .= '</ul>';
        } else {
            $html = "<h3>" . lang('no_product_variants') . "</h3><p>&nbsp;</p>";
        }

        $data['html'] = $html;
        $data['product_id'] = $product_id;
        $data['toitems_id'] = $toitems_id;
        $data['pdata'] = $this->site->getProductByID($product_id);
        $this->load_view($this->theme . 'transfers/product_variants_selection', $data);
    }

    public function received($transfer_id = NULL){
        $this->form_validation->set_rules('accept_transfer', lang("accept_transfer"), 'required');
        if ($this->form_validation->run() == true && $this->transfers_model->update_received($transfer_id)) {
            $this->session->set_flashdata('message', lang('accept_transfer_updated'));
            redirect($_SERVER['HTTP_REFERER']);
        }
        else {
            $inv = $this->transfers_model->getTransferByID($transfer_id);
            $this->data['can_accept'] = false;
            if ($this->Owner || $this->Admin || 
                (!$this->session->userdata('warehouse_id') && !$this->session->userdata('biller_id')) || 
                (!$this->session->userdata('warehouse_id') && $this->session->userdata('biller_id') && $this->session->userdata('biller_id') == $inv->destination_biller_id) ||
                ($this->session->userdata('warehouse_id') && $this->session->userdata('warehouse_id') == $inv->to_warehouse_id)
            ) {
                $this->data['can_accept'] = true;
            }
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['inv'] = $inv;
            $this->data['transfer_id'] = $transfer_id;
            $this->load_view($this->theme . 'transfers/received', $this->data);
        }
    }

    function add_order()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('seller_id', lang("seller"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }
            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $seller_id = $this->input->post('seller_id');
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $note = $this->sma->clear_tags($this->input->post('note'));
            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $product_id = $_POST['product_id'][$r];
                $quantity = $_POST['quantity'][$r];
                if ($quantity < 1) {
                    $this->session->set_flashdata('error', lang("invalid_product_quantity"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $pcolor = $_POST['pcolor'][$r];
                $products[] = array(
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'color_id' => $pcolor,
                );
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }
            $data = array(
                'biller_id' => $biller_id,
                'warehouse_id' => $warehouse_id,
                'seller_id' => $seller_id,
                'date' => $this->site->movement_setted_datetime($date),
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'document_type_id' => $document_type_id,
                'status' => 'pending',
                'origin_biller_id' => ($this->session->userdata('biller_id') ? $this->session->userdata('biller_id') : $this->Settings->default_biller),
                'origin_warehouse_id' => ($this->session->userdata('warehouse_id') ? $this->session->userdata('warehouse_id') : $this->Settings->default_warehouse),
            );

        }
        if ($this->form_validation->run() == true && $this->transfers_model->addOrder($data, $products)) {
            // $this->sma->print_arrays($data, $products);
            $this->session->set_userdata('remove_otols', 1);
            $this->session->set_flashdata('message', lang("order_transfer_added"));
            admin_redirect('transfers/orders');
        } else {
            $this->data['companies'] = $this->site->getAllCompanies($this->Settings->companies_for_adjustments_transfers);
            $this->data['adjustment_items'] = FALSE;
            $this->data['warehouse_id'] = FALSE;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['sellers'] = $this->site->getAllCompanies($this->Settings->companies_for_adjustments_transfers, null, true);
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller', null, true);
            $this->data['origin_biller'] = $this->site->getAllCompaniesWithState('biller', ($this->session->userdata('biller_id') ? $this->session->userdata('biller_id') : $this->Settings->default_biller));
            $this->data['colors'] = $this->products_model->get_colors();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('transfers'), 'page' => lang('transfers')), array('link' => '#', 'page' => lang('add_transfer_order')));
            $meta = array('page_title' => lang('add_transfer_order'), 'bc' => $bc);
            $this->page_construct('transfers/add_order', $meta, $this->data);
        }
    }
    public function oto_suggestions()
    {
        $term = $this->input->get('term', true);
        $biller_id = $this->input->get('biller_id', true);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $rows = $this->products_model->getQASuggestions($sr, $biller_id);
        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                if ($this->Settings->product_variant_per_serial == 0) {
                    $options = $this->products_model->getProductOptions($row->id);
                } else {
                    $options = NULL;
                }
                if ($options) {
                    $opt = $option_id  ? $this->purchases_model->getProductOptionByID($option_id) : current($options);
                    if (!$option_id ) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->cost = 0;
                    $option_id = FALSE;
                }
                $row->avg_cost = $row->avg_cost;
                $row->cost = $row->cost;
                $row->option = $option_id;
                if (isset($row->variant_selected)) {
                    $row->variant_selected = $row->variant_selected;
                    $row->option = $row->variant_selected;
                }
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $row->serial = '';
                $c = sha1(uniqid(mt_rand(), true));
                $pr[] = array(
                    'id' => $c, 'item_id' => ($row->id.($row->option)), 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'options' => $options, 'tax_rate' => $tax_rate
                );
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function orders()
    {
        $this->sma->checkPermissions();
        if ($this->input->get('new_transfers')) {
            $this->session->set_userdata('view_new_transfers', 1);
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('transfers_orders')));
        $meta = array('page_title' => lang('transfers_orders'), 'bc' => $bc);
        $this->page_construct('transfers/orders', $meta, $this->data);
    }

    function getOrderTransfers()
    {
        $this->sma->checkPermissions('index');
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date')." 00:00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date')." 23:59:00" : NULL;
        $biller_id = $this->input->post('biller_id') ? $this->input->post('biller_id') : NULL;
        $warehouse_id = $this->input->post('warehouse_id') ? $this->input->post('warehouse_id') : NULL;
        $status = $this->input->post('status') ? $this->input->post('status') : NULL;

        $detail_link = anchor('admin/transfers/order_view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('transfer_details'), 'data-toggle="modal" data-target="#myModal"');

        $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>' . $detail_link . '</li>
            </ul>
        </div></div>';

        $this->load->library('datatables');

        $this->datatables
            ->select("order_transfers.id as id, 
                    date, 
                    destination_biller.name as dbiller_name,
                    third.name third_name,
                    reference_no,
                    origin_biller.name obiller_name,
                    warehouses.name,
                    order_transfers.status
                    ")
            ->join('warehouses', 'warehouses.id = order_transfers.warehouse_id')
            ->join('companies origin_biller', 'origin_biller.id = order_transfers.origin_biller_id')
            ->join('companies destination_biller', 'destination_biller.id = order_transfers.biller_id')
            ->join('companies third', 'third.id = order_transfers.seller_id')
            ->from('order_transfers');

        if ($start_date) {
            $this->datatables->where('date >=', $start_date);
        }

        if ($end_date) {
            $this->datatables->where('date <=', $end_date);
        }

        if ($biller_id) {
            $this->datatables->where('origin_biller_id =', $biller_id);
        }

        if ($warehouse_id) {
            $this->datatables->where('warehouse_id =', $warehouse_id);
        }

        if ($status) {
            $this->datatables->where('status =', $status);
        }

        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }



    function order_view($transfer_id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);

        if ($this->input->get('id')) {
            $transfer_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $transfer = $this->transfers_model->getOrderTransferId($transfer_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($transfer->created_by, true);
        }
        $this->data['rows'] = $this->transfers_model->getOrderTransferItems($transfer_id, $transfer->status);
        $this->data['warehouse'] = $this->site->getWarehouseByID($transfer->warehouse_id);
        $this->data['biller'] = $this->site->getCompanyByID($transfer->biller_id);
        $this->data['seller'] = $this->site->getCompanyByID($transfer->seller_id);
        $this->data['transfer'] = $transfer;
        $this->data['tid'] = $transfer_id;
        $this->data['created_by'] = $this->site->getUser($transfer->created_by);
        $this->load_view($this->theme . 'transfers/order_view', $this->data);
    }

    public function end_order($id)
    {
        if ($this->transfers_model->endOrder($id)) {
            $this->session->set_flashdata('message', lang("order_transfer_ended"));
            admin_redirect('transfers/orders');
        }
    }
}
