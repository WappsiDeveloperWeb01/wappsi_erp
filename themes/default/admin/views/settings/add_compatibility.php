<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('compatibilities_add_compatibility'); ?></h4>
        </div>
        <?= admin_form_open_multipart("system_settings/add_compatibility", ['data-toggle' => 'validator', 'role' => 'form']); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= lang('compatibilities_brand', 'marca'); ?>
                <select name="marca" id="marca" class="form-control">
                    <option value=""><?= lang("select"); ?></option>
                    <?php foreach ($brands as $brand) { ?>
                    <option value="<?=  $brand->id; ?>"><?=  $brand->name; ?></option>
                    <?php } ?>
                </select>
                <?= form_hidden("nombre_marca") ?>
            </div>

            <div class="form-group">
                <?= lang('compatibilities_name', 'nombre'); ?>
                <?= form_input('nombre', '', 'class="form-control" id="nombre" required="required"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('add_brand', lang('save'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change', '#marca', obtener_nombre_marca);
    });

    function obtener_nombre_marca()
    {
        var marca = $("#marca").val();
        var nombre_marca = $("#marca option:selected").text();

        if (marca != '') {
            $('input[name="nombre_marca"]').val(nombre_marca);
        } else {
            $('input[name="nombre_marca"]').val('');
        }
    }
</script>