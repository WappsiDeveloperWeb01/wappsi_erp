<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TypesMovement_model extends CI_Model {

	public function get_by_id($id)
	{
		$sufijo_contabilidad = $this->session->userdata('accounting_module');

		$this->db->select("*");
		$this->db->from("movement_type_con".$sufijo_contabilidad);
		$this->db->where("id", $id);

		$response = $this->db->get();
		return $response->row();
	}

	public function get_document_types()
	{
		$this->db->select("*");
		$this->db->from("documents_types");
		$this->db->WHERE("sales_prefix IS NOT NULL");
		$this->db->where_in("module", [11, 18, 12]);

		$response = $this->db->get();
		return $response->result();
	}

	public function get_existing_move_type($document_type_id, $ledger_id_array)
	{
		$sufijo_contabilidad = $this->session->userdata('accounting_module');

		$this->db->distinct();
		$this->db->select("ei.ledger_id");
		$this->db->from("sma_documents_types dt");
		$this->db->join("sma_entrytypes_con".$sufijo_contabilidad." et", "et.label = dt.sales_prefix", "inner");
		$this->db->join("sma_entries_con".$sufijo_contabilidad." e", "e.entrytype_id = et.id", "inner");
		$this->db->join("sma_entryitems_con".$sufijo_contabilidad." ei", "ei.entry_id = e.id", "inner");
		$this->db->where("dt.id", $document_type_id);
		$this->db->where_in("ei.ledger_id", $ledger_id_array);

		$response = $this->db->get();
		return $response->result();
	}

	public function insert($data)
	{
		$sufijo_contabilidad = $this->session->userdata('accounting_module');

		if ($this->db->insert("movement_type_con".$sufijo_contabilidad, $data)) {
			return TRUE;
		}

		return FALSE;
	}

	public function update($data, $id)
	{
		$sufijo_contabilidad = $this->session->userdata('accounting_module');

		$this->db->where("id", $id);
		if ($this->db->update("movement_type_con".$sufijo_contabilidad, $data)) {
			return TRUE;
		}

		return FALSE;
	}

	public function delete($id)
	{
		$sufijo_contabilidad = $this->session->userdata('accounting_module');

		$this->db->where("id", $id);
		if ($this->db->delete("movement_type_con".$sufijo_contabilidad)) {
			return TRUE;
		}

		return FALSE;
	}
}

/* End of file TypesMovement_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/TypesMovement_model.php */