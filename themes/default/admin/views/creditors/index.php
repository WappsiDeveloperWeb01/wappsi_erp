<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }
    $(document).ready(function() {
        oTable = $('#SupData').dataTable({
            "aaSorting": [
                [1, "asc"]
            ],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('creditors/getCreditors') ?>',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            'fnRowCallback': function(nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "creditor_details_link";
                return nRow;
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            }, null, null, null, null, null, null, null, {
                "bSortable": false
            }]
        });
    });
</script>
<?php if ($Owner || $Admin || $GP['bulk_actions']) {
    echo admin_form_open('suppliers/supplier_actions', 'id="action-form"');
} ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"><?= lang("actions") ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?= admin_url('creditors/add'); ?>" data-toggle="modal" data-target="#myModal" id="add">
                                            <i class="fa fa-plus-circle"></i> <?= lang("add_supplier"); ?>
                                        </a>
                                    </li>
                                    <?php if ($Owner || $Admin) : ?>
                                        <li>
                                            <a href="#" id="excel" data-action="export_excel">
                                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                            </a>
                                        </li>
                                    <?php endif ?>
                                    <?php if ($Owner) : ?>
                                        <li class="divider"></li>
                                        <!-- <li>
                                        <a href="#" class="bpo" title="<b><?= $this->lang->line("delete_suppliers") ?></b>"
                                            data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left">
                                            <i class="fa fa-trash-o"></i> <?= lang('delete_suppliers') ?>
                                        </a>
                                    </li> -->
                                    <?php endif ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="SupData" class="table table-hover">
                                    <thead>
                                        <tr class="primary">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkth" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("company"); ?></th>
                                            <th><?= lang("name"); ?></th>
                                            <th><?= lang("email_address"); ?></th>
                                            <th><?= lang("phone"); ?></th>
                                            <th><?= lang("city"); ?></th>
                                            <th><?= lang("country"); ?></th>
                                            <th><?= lang("vat_no"); ?></th>
                                            <th style="width:85px; text-align:center;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body -->

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>
<?php if ($action && $action == 'add') {
    echo '<script>$(document).ready(function(){$("#add").trigger("click");});</script>';
}
?>