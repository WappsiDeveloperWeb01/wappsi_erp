<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if ($modal) { ?>
    <style type="text/css">
        @media print {
            #wrapper {
                padding: 0px !important;
                margin: 0px !important;
            }
            #receiptData {
                padding: 0px !important;
                margin: 0px !important;
            }
            .modal-body{
                padding: 0px !important;
                margin: 0px !important;
            }
            .modal-content{
                padding: 0px !important;
                margin: 0px !important;
            }
            .modal-dialog{
                padding: 0px !important;
                margin: 0px !important;
            }
        }
    </style>

<div class="modal-dialog no-modal-header" role="document">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
<?php } else { ?>
    <!doctype html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>Comprobante de Pago: <?=$paymentNo;?></title>
            <base href="<?=base_url()?>"/>
            <meta http-equiv="cache-control" content="max-age=0"/>
            <meta http-equiv="cache-control" content="no-cache"/>
            <meta http-equiv="expires" content="0"/>
            <meta http-equiv="pragma" content="no-cache"/>
            <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
            <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
            <style type="text/css" media="all">
                body { color: #000; }
                #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
                .btn { border-radius: 0; margin-bottom: 5px; }
                .bootbox .modal-footer { border-top: 0; text-align: center; }
                h3 { margin: 5px 0; }
                .order_barcodes img { float: none !important; margin-top: 5px; }
                .text-center { text-align: center ; }
                table { width: 100% ; }
                @media print {
                    .no-print { display: none; }
                    #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                    .no-border { border: none !important; }
                    .border-bottom { border-bottom: 1px solid #ddd !important; }
                    table tfoot { display: table-row-group; }
                }
                .rectangular_logo{
                    max-width: 150px;
                    max-height: 150px;
                }
            </style>
        </head>
        <body>
<?php } ?>
    <div id="wrapper">
        <div id="receiptData">
            <div>
                <div style="text-align:center;">
                    <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>

                    <h4 style="text-transform:uppercase;"><?=$this->Settings->nombre_comercial;?></h4>
                    <?php
                    echo "NIT : ".$this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '');
                    if (isset($ciiu_code)) {
                        echo "</br> Códigos CIIU : ".$ciiu_code."</br>";
                    }
                    echo "<p>" . $biller->address .
                    "<br>" . lang("tel") . ": " . $biller->phone .
                    "<br>" . $biller->city . " " . $biller->postal_code . " " . $biller->state . " " . $biller->country;

                    echo "<br>";
                    echo '</p>';
                    ?>
                    <p style="text-align:center;"><?= $tipo_regimen ?></p>
                    <?php
                    if ($this->Settings->url_web != "") {
                        echo $this->Settings->url_web."<br>";
                    } ?>
                </div>
                <?php if ($invoiceExists) { ?>
                    <?php if ($Settings->invoice_view == 1 || $Settings->indian_gst) { ?>
                        <div class="col-sm-12 text-center">
                            <?php if ($inv->sale_status == 'returned'): ?>
                                <h4 style="font-weight:bold;">Devolución de venta</h4>
                            <?php else: ?>
                                <h4 style="font-weight:bold;"><?=  $document_type ? $document_type->nombre : lang('sale_invoice');?></h4>
                                <h4 style="font-weight:bold;"><?= lang("sale_no_ref") . ": " . $inv->reference_no . "<br>"; ?></h4>
                            <?php endif ?>
                        </div>
                    <?php } ?>
                    <?php
                        echo "" .lang("date") . ": " . $this->sma->hrld($inv->date) . "";
                        if ($inv->payment_term > 0 && $inv->payment_status != 'paid') {
                            $fecha_exp = strtotime("+".($inv->payment_term > 0 ? $inv->payment_term : 0)." day", strtotime($inv->date));
                            $fecha_exp = date('d/m/Y', $fecha_exp);
                            echo lang("expiration_date")." : ".$fecha_exp." <br>";
                        }
                        if (!empty($inv->return_sale_ref)) {
                            echo '<p>'.lang("return_ref").': '.$inv->return_sale_ref;
                            if ($inv->return_id) {
                                echo ' <a data-target="#myModal2" data-toggle="modal" href="'.admin_url('sales/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                            } else {
                                echo '</p>';
                            }
                        }
                        echo "<p>";
                            echo lang("customer") . ": " . ($customer->company && $customer->company != '-' ? $customer->company : $customer->name) . "<br>";
                            if ($customer->vat_no != "-") {
                                if ($customer->tipo_documento == 6) {
                                    echo  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion." <br>";
                                } else {
                                    echo  lang("vat_no") . ": " . $customer->vat_no." <br>";
                                }
                            }
                            echo lang("tel") . ": " . $inv->phone_negocio . "<br>";
                            echo lang("address") . ": " . $inv->direccion_negocio . "<br>";
                            if (!empty($inv->ciudad_negocio)) {
                                echo $inv->ciudad_negocio ." ".$inv->state_negocio." ".$inv->country_negocio ."<br>";
                            }
                            echo $customer->email ."<br>";
                        echo "</p>";
                    ?>
                    <?php if ($seller): ?>
                        <p><b><?= lang('seller') ?> :</b> <?= $seller->company != '-' ? $seller->company : $seller->name; ?></p>
                    <?php endif ?>
                    <?php if (!empty($inv->sale_origin_reference_no)): ?>
                        <p><b><?= lang('reference_origin') ?> :</b> <?= $inv->sale_origin_reference_no?></p>
                    <?php endif ?>
                    <?php
                        if ($inv->restobar_table_id) {
                            echo "<p><b>".lang("table_number") . ":</b> " . $inv->restobar_table_id . "</p>";
                        }
                    ?>
                <?php } else { ?>
                    <p>
                        <strong><?= lang('date') . ":</strong> {$paymentDate} {$paymentTime}" ?>
                        <br>
                        <strong><?= lang('customer') . ":</strong> {$customerName}" ?>
                        <br>
                        <strong><?= lang('vat_no') . ":</strong> {$customerVatno}" ?>
                    </p>
                <?php } ?>

                <?php if (isset($cost_center) && $cost_center): ?>
                    <div>
                        <label><?= lang('cost_center') ?></label>
                        <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                    </div>
                <?php endif ?>

                <?php if ($this->Settings->generate_automatic_invoice == YES) { ?>
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th style="text-align: center; width: 60%">Nombre</th>
                                <th style="text-align: center;">Precio</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($invoiceExists) { ?>
                                <?php
                                    $r = 1; $category = 0;
                                    $brand = 0;
                                    $tax_summary = array();
                                    $ccategories = [];
                                    $inv_total_before_promo = 0;
                                    foreach ($rows as $row) {
                                        if ($product_detail_promo == 1 && $row->price_before_promo > 0) {
                                            $inv_total_before_promo = ($row->price_before_promo - ($row->tax_method == 1 ? $row->net_unit_price : $row->unit_price)) * $row->quantity;
                                        }

                                        echo '<tr>
                                                <td class="no-border" style="padding-bottom: 0px !important;">
                                                    '.($row->product_name) .
                                                    ($row->variant ? ' (' . $row->variant. ($row->variant_code ? "-".$row->variant_code : "") . ')' : '') .
                                                    ($show_product_preferences == 1 && $row->preferences ? ' (' . $row->preferences . ')' : '') .
                                                    ($this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : '') .
                                                    ($row->serial_no != '' ? ' - '.$row->serial_no : '') .

                                                    ($product_detail_promo == 1 && $row->price_before_promo > 0 ? " (".lang('discount_by_promo')." -".$this->sma->formatDecimal((($row->price_before_promo - ($row->tax_method == 1 ? $row->net_unit_price : $row->unit_price)) / $row->price_before_promo) * 100, 2)." %)" : '' ).
                                                    ($row->under_cost_authorized == 1 ? ' (<i class="fa fa-unlock-alt"></i>)' : '').
                                                    '
                                                </td>
                                                <td><span class="pull-right">' . $this->sma->formatValue($value_decimals, $row->subtotal) . '</span></td>
                                            </tr>';
                                        if (!empty($row->second_name)) {
                                            echo '<tr>
                                                    <td colspan="2" class="no-border">'.$row->second_name.'</td>
                                                </tr>';
                                        }
                                        echo '<tr>
                                                <td class="no-border border-bottom" style="padding-top: 0px !important;">' . $this->sma->formatQuantity($row->quantity, $qty_decimals) . ' x '.$this->sma->formatValue($value_decimals, $row->unit_price).' '.($this->Settings->ipoconsumo && $row->tax_2 > 0 ? '+ '.sprintf(lang('ipoconsumo_additional_description_name'), $this->sma->formatMoney($row->tax_2)) : '').'
                                                </td>
                                                <td class="no-border border-bottom text-right" style="padding-top: 0px !important;">' . (!isset($document_type_invoice_format->view_item_tax) || (isset($document_type_invoice_format->view_item_tax) && $document_type_invoice_format->view_item_tax && $row->tax_code) ? '*'.$row->tax_code : '') . '
                                                </td>
                                            </tr>';

                                        $r++;
                                    }
                                    if (($document_type_invoice_format && $document_type_invoice_format->show_return_detail == 1) && $return_rows) {
                                        echo '<tr class="warning">
                                                <td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td>
                                            </tr>';
                                        foreach ($return_rows as $row) {
                                            echo '<tr>
                                                    <td colspan="2" class="no-border">#' . $r . ': &nbsp;&nbsp;' . $row->product_name, ($row->variant ? ' (' . $row->variant . ')' : '') . '<span class="pull-right">' . ($row->tax_code ? '*'.$row->tax_code : '') . '</span></td>
                                                    </tr>';
                                            echo '<tr>
                                                    <td class="no-border border-bottom">' . $this->sma->formatQuantity($row->quantity, $qty_decimals) . ' x '.$this->sma->formatValue($value_decimals, $row->unit_price).($row->item_tax != 0 ? ' - '.lang('tax').' <small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small> '.$this->sma->formatValue($value_decimals, $row->item_tax).' ('.lang('hsn_code').': '.$row->hsn_code.')' : '').' '.($this->Settings->ipoconsumo && $row->tax_2 > 0 ? '+ '.sprintf(lang('ipoconsumo_additional_description_name'), $this->sma->formatMoney($row->tax_2)) : '').'
                                                    </td>
                                                    <td class="no-border border-bottom text-right">' . $this->sma->formatValue($value_decimals, $row->subtotal) . '</td>
                                                </tr>';
                                            $r++;
                                        }
                                    }
                                ?>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <?php if ($invoiceExists) { ?>
                                <?php
                                    $rete_total = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total + ($inv->rete_bomberil_id && $inv->rete_bomberil_total > 0 ? $inv->rete_bomberil_total : 0) + ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0);
                                ?>
                                <?php if ($rete_total != 0 || $inv->order_discount != 0): ?>
                                    <tr>
                                        <th><?=lang("total_before_discount");?></th>
                                        <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax));?></th>
                                    </tr>
                                <?php endif ?>
                                <?php
                                if ($inv->order_tax != 0) {
                                    echo '<tr><th>' . lang("tax") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</th></tr>';
                                }
                                if ($rete_total != 0) {
                                    echo '<tr>
                                            <th>'.lang('retention').'</th>
                                            <th class="text-right"> -' . $this->sma->formatValue($value_decimals, $rete_total) . '</th>
                                        </tr>';
                                }
                                if ($inv->order_discount != 0) {
                                    echo '<tr><th>' . lang("order_discount") . '</th><th class="text-right"> -' . $this->sma->formatValue($value_decimals, $inv->order_discount) . '</th></tr>';
                                }
                                if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 1) {
                                    echo '<tr><th>' . lang("shipping") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $inv->shipping) . '</th></tr>';
                                }
                                if ($return_sale) {
                                    if ($return_sale->surcharge != 0) {
                                        echo '<tr><th>' . lang("order_discount") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $return_sale->surcharge) . '</th></tr>';
                                    }
                                }
                                if ($Settings->indian_gst) {
                                    if ($inv->cgst > 0) {
                                        $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                                        echo '<tr><td>' . lang('cgst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $cgst) : $cgst) . '</td></tr>';
                                    }
                                    if ($inv->sgst > 0) {
                                        $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                                        echo '<tr><td>' . lang('sgst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $sgst) : $sgst) . '</td></tr>';
                                    }
                                    if ($inv->igst > 0) {
                                        $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                                        echo '<tr><td>' . lang('igst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $igst) : $igst) . '</td></tr>';
                                    }
                                }
                                if ($inv->tip_amount != 0) {
                                    echo '<tr>
                                            <th>' . lang("tip_suggested") . '</th>
                                            <th class="text-right">' . $this->sma->formatValue($value_decimals, $inv->tip_amount) . '</th>
                                        </tr>';
                                }
                                if ($this->pos_settings->rounding || $inv->rounding != 0) {
                                    ?>
                                    <tr>
                                        <th><?=lang("rounding");?></th>
                                        <th class="text-right"><?= $this->sma->formatValue($value_decimals, $inv->rounding);?></th>
                                    </tr>
                                    <tr>
                                        <th><?=lang("total_to_pay");?></th>
                                        <th class="text-right" style="font-size: 130%;"><?=$this->sma->formatValue($value_decimals, $return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding)  - $rete_total );?></th>
                                    </tr>
                                    <?php
                                } else {
                                    ?>
                                    <tr>
                                        <th><?=lang("total_to_pay");?></th>
                                        <th class="text-right" style="font-size: 130%;"><?=$this->sma->formatValue($value_decimals, $return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total  - $rete_total );?></th>
                                    </tr>
                                    <?php
                                }
                                if ($inv->paid < ($inv->grand_total + $inv->rounding)) {
                                    ?>
                                    <tr>
                                        <th><?=lang("paid_amount");?></th>
                                        <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid);?></th>
                                    </tr>
                                    <tr>
                                        <th><?=lang("due_amount");?></th>
                                        <th class="text-right"><?=$this->sma->formatValue($value_decimals, ($return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding)) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid));?></th>
                                    </tr>
                                    <?php
                                } ?>
                            <?php } ?>
                        </tfoot>
                    </table>
                <?php } ?>
                <?php if ($invoiceExists) { ?>
                    <?php
                    if ($payments) {
                        echo '<table class="table table-condensed"><tbody>';
                        foreach ($payments as $payment) {
                            echo '<tr>';
                            if (($payment->paid_by == 'cash' || $payment->paid_by == 'deposit') && $payment->pos_paid) {
                                echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo '<td>' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->sma->formatValue($value_decimals, $payment->pos_balance) : 0) . '</td>';
                            } elseif (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe')) {
                                echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo '<td>' . lang("no") . ': ' . 'xxxx xxxx xxxx ' . substr($payment->cc_no, -4) . '</td>';
                                echo '<td>' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                            } elseif ($payment->paid_by == 'Cheque') {
                                echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo '<td>' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                            } elseif ($payment->paid_by == 'gift_card' && $payment->pos_paid) {
                                echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td>' . lang("no") . ': ' . $payment->cc_no . '</td>';
                                echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo '<td>' . lang("balance") . ': ' . $this->sma->formatValue($value_decimals, $this->sma->getCardBalance($payment->cc_no)) . '</td>';
                            } elseif ($payment->paid_by == 'other' && $payment->amount) {
                                echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                            } elseif($payment->paid_by == 'retencion') {
                                echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            } else {
                                echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                            }
                            echo '</tr>';
                        }
                        echo '</tbody></table>';
                    }
                    if ($return_payments) {
                        echo '<strong>'.lang('return_payments').'</strong><table class="table table-condensed"><tbody>';
                        foreach ($return_payments as $payment) {
                            $payment->amount = (0-$payment->amount);
                            echo '<tr>';
                            if (($payment->paid_by == 'cash' || $payment->paid_by == 'deposit') && $payment->pos_paid) {
                                echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo '<td>' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->sma->formatValue($value_decimals, $payment->pos_balance) : 0) . '</td>';
                            } elseif (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') && $payment->cc_no) {
                                echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo '<td>' . lang("no") . ': ' . 'xxxx xxxx xxxx ' . substr($payment->cc_no, -4) . '</td>';
                                echo '<td>' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                            } elseif ($payment->paid_by == 'Cheque' && $payment->cheque_no) {
                                echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo '<td>' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                            } elseif ($payment->paid_by == 'gift_card' && $payment->pos_paid) {
                                echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td>' . lang("no") . ': ' . $payment->cc_no . '</td>';
                                echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo '<td>' . lang("balance") . ': ' . $this->sma->formatValue($value_decimals, $this->sma->getCardBalance($payment->cc_no)) . '</td>';
                            } elseif ($payment->paid_by == 'other' && $payment->amount) {
                                echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                            } else {
                                echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                                echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                                echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                            }
                            echo '</tr>';
                        }
                        echo '</tbody></table>';
                    }
                    ?>
                <?php } ?>
                <?php if ($invoiceExists) { ?>
                    <?= $Settings->invoice_view > 0 && (!isset($document_type_invoice_format->view_item_tax) || (isset($document_type_invoice_format->view_item_tax) && $document_type_invoice_format->view_item_tax)) ? $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax), FALSE, FALSE, $tax_indicator, false, $value_decimals, $qty_decimals) : ''; ?>
                    <?php
                        if ($biller_data->product_order == "1" || $biller_data->product_order == "2")
                        {
                            echo $this->gscat->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax), FALSE, $biller_data->product_order);
                        }
                    ?>
                    <?= $customer->award_points != 0 && $Settings->each_spent > 0 ? '<p style="text-align:center;">'.lang('this_sale_points').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                    .'<br>'.
                    lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>' : ''; ?>
                    <?= $inv->note ? '<p><strong>' . lang('sale_note') . ':</strong>  ' . $this->sma->decode_html($inv->note) . '</p>' : ''; ?>
                    <?= $inv->staff_note ? '<p><strong>' . lang('staff_note') . ':</strong> ' . $this->sma->decode_html($inv->staff_note) . '</p>' : ''; ?>

                    <?php
                    if ($inv_total_before_promo > 0) {
                        // $tp = $this->sma->formatDecimal((($inv_total_before_promo - $inv->grand_total) / $inv_total_before_promo) * 100, 2)." %";
                        $tp = " $ ".$this->sma->formatMoney($inv_total_before_promo + $inv->order_discount);
                        echo "<p class='text-center'>".sprintf(lang('invoice_total_promo_discount'), $tp)."</p>";
                    }
                    ?>
                    <?php if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 0): ?>
                        <hr>
                        <h4><?= sprintf(lang('shipping_not_include_in_grand_total'), $this->sma->formatValue($value_decimals, $inv->shipping)) ?></h4>
                    <?php endif ?>
                    <?php if ($biller_data->concession_status == 1 && $biller_data->type_concession == 1): ?>
                        <?php
                            $cc_data = [];
                            foreach ($biller_categories_concession as $bcc) {
                                $cc_data[$bcc->category_id] = $bcc;
                            }
                        ?>
                        <?php foreach ($ccategories as $category_id => $category_total): ?>
                            <?php if (isset($cc_data[$category_id]) && $cc_data[$category_id]->concession_code): ?>
                                <?php
                                $total_factura_barcode = $this->sma->zero_left_consecutive(ceil($category_total), '000000000');
                                $concession_name = $cc_data[$category_id]->concession_name;
                                $concession_code = $cc_data[$category_id]->concession_code;
                                ?>
                                <div class="col-xs-12 text-center" style="padding-top: 10%">
                                    <span><?= lang('concession_name') ?> : <?= $concession_name ?></span><br>
                                    <span><?= lang('total') ?> : <?= $this->sma->formatMoney(ceil($category_total)) ?></span><br>
                                    <img src="<?= admin_url('misc/barcode/'.($concession_code.$total_factura_barcode).'/code128/40/0/0'); ?>" alt="<?= ($concession_code.$total_factura_barcode); ?>" class="bcimg" style="width: 70%;" />
                                    <br>
                                    <span><?= $concession_code.$total_factura_barcode ?></span>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>

                    <p style="text-align:center;"><?= $this->sma->decode_html($invoice_footer ? $invoice_footer : '') ?></p>

                    <?php if ($inv->resolucion): ?>
                        <p style="text-align:center;"><?= $inv->resolucion ?></p>
                    <?php endif ?>

                    <?php if ($inv->cufe): ?>
                        <p style="text-align:center; word-break: break-all;">CUFE : <?= $inv->cufe ?></p>
                        <div style="text-align:center;">
                            <img src="<?= base_url().'themes/default/admin/assets/images/qr_code/'. $inv->reference_no .'.png' ?>">
                        </div>
                        <br>
                    <?php endif ?>
                <?php } ?>

                <?php if (isset($inv)): ?>
                <p style="text-align:center;">Representación gráfica de la factura electrónica</p>
                <?php endif ?>

                <!-- Bloque Comprobante -->
                <h4 class="text-center">Comprobante de pago <br> <?= $paymentNo ?></h4>
                <br>
                <h4 class="text-center" style="border-bottom: 1px dotted #b2b2b2;">Detalle pago de cuotas</h4>
                <table class="table table-condensed" id="installmentTable">
                    <thead>
                        <tr>
                            <th>Crédito</th>
                            <th>Suc.</th>
                            <th class="text-center">Vr. Inicial</th>
                            <th># Cuota</th>
                            <th class="text-center">Capital</th>
                            <th class="text-center">Int.</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $totalAmount = $totalInterest = $totalCapital = $totalCurrentInterest = $totalDefaultInterest = $totalCurrentTaxInterest = $totalDefaultTaxInterest = $totalCommission = $totalVatCommission = $totalProInterest = $totalVatInterest = 0; ?>
                        <?php foreach ($paymentsFinancing as $payment) { ?>
                            <?php
                                $capital = $payment->capital_amount;
                                $currentInterest = ($payment->current_interest_amount + $payment->interest_tax);
                                $defaultInterest = ($payment->default_interest + $payment->default_interest_tax);

                                $interest = ($defaultInterest + $currentInterest);
                                $amount = ($capital + $interest);
                            ?>
                            <tr>
                                <td style="line-height: 1.1;">
                                    <?= $payment->credit_no ?>
                                    <br><small>Vence: <?= $payment->installment_due_date?></small>
                                    <br><small>Días mora: <?= $payment->default_days?></small>
                                </td>
                                <td class="text-center"><?= substr($biller->name, 0, 3) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($payment->installment_amount,0) ?></td>
                                <td class="text-center"><?= $payment->installment_no ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($capital, 0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($interest, 0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($amount, 0) ?></td>
                            </tr>
                            <?php
                                $totalAmount += $amount;
                                $totalCapital += $capital;
                                $totalInterest += $interest;
                                $totalCurrentTaxInterest += $payment->interest_tax;
                                $totalDefaultInterest += $payment->default_interest;
                                $totalCurrentInterest += $payment->current_interest_amount;
                                $totalDefaultTaxInterest += $payment->default_interest_tax;
                                $totalCommission += $payment->procredito_commission;
                                $totalVatCommission += $payment->vat_procredito_commission;
                                $totalProInterest += $payment->procredito_interest;
                                $totalVatInterest += $payment->vat_procredito_interest;
                            ?>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <?php if (($totalCommission) > 0) { ?>
                            <tr>
                                <th class="text-right" colspan="4">Total Commission Procrédito : </th>
                                <th class="text-right" colspan="3"><?= $this->sma->formatNumberWithoutDecimals($totalCommission + $totalVatCommission,0) ?></th>
                            </tr>
                        <?php } ?>
                        <?php if (($totalProInterest) > 0) { ?>
                            <tr>
                                <th class="text-right" colspan="4">Total Interés Procrédito : </th>
                                <th class="text-right" colspan="3"><?= $this->sma->formatNumberWithoutDecimals($totalProInterest + $totalVatInterest,0) ?></th>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th class="text-right" colspan="4">Total Pagado : </th>
                            <th class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalCapital,0) ?></th>
                            <th class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalInterest,0) ?></th>
                            <th class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalAmount,0) ?></th>
                        </tr>
                    </tfoot>
                </table>

                <table class="table table-condensed" id="insterestTable">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="text-center">Base</th>
                            <th class="text-center">IVA</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($totalCurrentInterest)) { ?>
                            <tr>
                                <td>Interés Corriente</td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalCurrentInterest,0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalCurrentTaxInterest,0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals(($totalCurrentInterest + $totalCurrentTaxInterest),0) ?></td>
                            </tr>
                        <?php } ?>
                        <?php if(!empty($totalDefaultInterest)) { ?>
                            <tr>
                                <td>Interés Mora</td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalDefaultInterest,0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalDefaultTaxInterest,0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals(($totalDefaultInterest + $totalDefaultTaxInterest),0) ?></td>
                            </tr>
                        <?php } ?>
                        <?php if(!empty($totalCommission)) { ?>
                            <tr>
                                <td>Comisión Procrédito</td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalCommission,0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalVatCommission,0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals(($totalCommission + $totalVatCommission),0) ?></td>
                            </tr>
                        <?php } ?>
                        <?php if(!empty($totalProInterest)) { ?>
                            <tr>
                                <td>Interés Procrédito</td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalProInterest,0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($totalVatInterest,0) ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals(($totalProInterest + $totalVatInterest),0) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <h4 class="text-center" style="border-bottom: 1px dotted #b2b2b2;">Formas de Pago</h4>
                <table class="table table-condensed" id="paymentsTable">
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th class="text-center">Valor Recibido</th>
                            <th class="text-center">Cambio</th>
                            <th class="text-center">Total Pagado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($paymentMethods as $method => $value) { ?>
                            <tr>
                                <td><?= $value["name"] ?></td>
                                <td class="text-right"><?= (isset($value["value_received"]) ? $this->sma->formatNumberWithoutDecimals($value["value_received"],0) : '') ?></td>
                                <td class="text-right"><?= (isset($value["change"]) ? $this->sma->formatNumberWithoutDecimals($value["change"],0) : '') ?></td>
                                <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($value["amount"],0) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                        </tr>
                    </tfoot>
                </table>

                <?php if (!empty($nextInstallments)) { ?>
                    <h4 class="text-center" style="border-bottom: 1px dotted #b2b2b2;">Saldos y próximos movimientos</h4>
                    <table class="table table-condensed" id="nextInstallmentTable">
                        <thead>
                            <tr>
                                <th>Crédito</th>
                                <th class="text-center">Vr. Inicial</th>
                                <th># Cuota</th>
                                <th class="text-center">Capital</th>
                                <th class="text-center">Int.</th>
                                <th class="text-center">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $totalAmount = $totalInterest = $totalCapital = $totalCurrentInterest = $totalDefaultInterest = $totalCurrentTaxInterest = $totalDefaultTaxInterest = 0; ?>
                            <?php $subTotalAmount = $subTotalCapital = $subTotalInterest = 0; ?>
                            <?php foreach ($nextInstallments as $installment) { ?>
                                <?php
                                    $capital = $installment->capitalAmount;
                                    $currentInterest = ($installment->currentInterestAmount + $installment->interestTax);
                                    $defaultInterest = ($installment->defaultInterest + (isset($installment->defaultInterestTax) ? $installment->defaultInterestTax : 0 ));
                                    $interest = ($currentInterest + $defaultInterest);
                                    $amount = ($capital + $interest);
                                ?>
                                <tr>
                                    <td style="line-height: 1.1;">
                                        <?= $installment->creditNo ?>
                                        <br><small>Vence: <?= $installment->installmentDueDate?></small>
                                        <br><small>Días mora: <?= $installment->daysExpired ?></small>
                                    </td>
                                    <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($installment->installmentAmount,0) ?></td>
                                    <td class="text-center"><?= $installment->installmentNo ?></td>
                                    <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($capital,0) ?></td>
                                    <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($interest,0) ?></td>
                                    <td class="text-right"><?= $this->sma->formatNumberWithoutDecimals($amount,0) ?></td>
                                </tr>
                                <?php
                                    $subTotalCapital += $capital;
                                    $subTotalInterest += $interest;
                                    $subTotalAmount += $amount;
                                ?>
                            <?php } ?>

                            <tr>
                                <th class="text-right" colspan="3">Saldo a pagar: </th>
                                <th class="text-right"><?= $this->sma->formatNumberWithoutDecimals($subTotalCapital,0) ?></th>
                                <th class="text-right"><?= $this->sma->formatNumberWithoutDecimals($subTotalInterest,0) ?></th>
                                <th class="text-right"><?= $this->sma->formatNumberWithoutDecimals($subTotalAmount,0) ?></th>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>

<!-- Fin Bloque Comprobante -->

                <p class="text-center">Software Wappsi POS desarrollado por Web Apps Innovation SAS NIT 901.090.070-9  www.wappsi.com</p>
                <p class="text-center"><?= lang('created_by')." : ".ucwords(mb_strtolower($created_by->first_name." ".$created_by->last_name)) ?></p>

                <?php if (isset($inv) && $inv->cufe): ?>
                <div style="text-align:center;">
                    <img src="<?= base_url().'assets/images/cadena_logo.jpeg' ?>" style="width: 50%;">
                </div>
                <?php endif ?>
            </div>
        </div>

        <div id="buttons" style="padding-top:10px;" class="no-print">
            <div class="alert alert-info loading_print_status" <?= $this->pos_settings->remote_printing != 4 ? 'style="display:none;"' : '' ?> >
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status') ?>
            </div>
            <div class="alert alert-success loading_print_status_success" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_success') ?>
            </div>
            <div class="alert alert-warning loading_print_status_warning" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_warning') ?>
            </div>
            <hr>

            <?php if(!$modal) : ?>
                <span class="col-xs-12">
                    <button class="btn btn-block btn-primary" onclick="print_format()"> Imprimir</button>
                </span>

                <span class="col-xs-12">
                    <button class="btn btn-block btn-danger" onclick="closeWindow()"><?= lang("close"); ?></a>
                </span>
            <?php endif ?>
        </div>
        <?php if ($modal) : ?>
            </div>
        </div>
    </div>
<?php else : ?>
        </body>
    </html>
<?php endif ?>

    </div>
    <?php if(!$modal) : ?>
        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
    <?php endif ?>

    <script type="text/javascript">
        $(document).ready(function () {
            print_format();
        });

        function closeWindow()
        {
            window.close();
        }

        function print_format()
        {
            if ("<?= $this->session->userdata('print_pos_finalized_invoice') ?>" == 0 || ("<?= isset($submit_target) && $submit_target == 2 ?>")) {
                return false;
            }
            if ("<?= $this->pos_settings->mobile_print_automatically_invoice ?>" == 0) {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    $('.btn_print_html').fadeOut();
                    return false;
                }
            }
            setTimeout(function() {
                window.print();
            }, 350);
        }
    </script>
