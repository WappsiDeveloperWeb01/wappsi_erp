<?php

use Mpdf\Utils\Arrays;

defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_contracts extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->enableElectronicPayroll) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }

        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->Payroll_settings = $this->site->get_payroll_setting();

        $this->lang->admin_load('payroll', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('Payroll_contracts_model');
        $this->load->admin_model("Employees_model");
        $this->load->admin_model("UserActivities_model");
        $this->load->admin_model('FutureItems_model');
        $this->load->admin_model('PayrollContractConcepts_model');
        $this->load->admin_model('Payroll_concepts_model');
        $this->load->admin_model('Payroll_management_model');
        $this->load->admin_model('PayrollFuturesItems_model');
	}

	public function index()
	{
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->data["contracts"] = $this->Payroll_contracts_model->get();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('payroll_contracts/index', ['page_title' => lang('payroll_contracts')], $this->data);
	}

	public function get_datatables()
	{
        $this->load->library('datatables');
        $this->datatables->select("payroll_contracts.id AS id,
        	payroll_contracts.internal_code,
        	LOWER(".$this->db->dbprefix("companies").".name) AS name,
        	payroll_types_contracts.description,
        	payroll_contracts.start_date,
        	payroll_contracts.end_date,
        	biller.company,
        	payroll_areas.name AS name_area,
        	payroll_professional_position.name AS name_professional_position,
        	payroll_contracts.contract_status,
            companies.id AS employee_id,
			payroll_contracts.settlement_date");
        $this->datatables->from("payroll_contracts");
        $this->datatables->join("companies", "companies.id = payroll_contracts.companies_id", "inner");
        $this->datatables->join("payroll_types_contracts", "payroll_types_contracts.id = payroll_contracts.contract_type", "left");
        $this->datatables->join("companies biller", "biller.id = payroll_contracts.biller_id", "left");
        $this->datatables->join("payroll_areas", "payroll_areas.id = payroll_contracts.area", "left");
        $this->datatables->join("payroll_professional_position", "payroll_professional_position.id = payroll_contracts.professional_position", "left");
        // $this->datatables->where("contract_status", ACTIVE);
        $this->datatables->add_column("Actions", '<div class="text-center">
                                        <div class="btn-group text-left">
                                            <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                                                <i class="fas fa-ellipsis-v fa-lg"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li class="edit_contract">
                                                    <a href="'. admin_url('payroll_contracts/edit/$1/contracts/$2') .'"><i class="fa fa-edit"></i>'. lang("edit") .'</a>
                                                </li>
                                                <li class="add_contract">
                                                    <a href="'. admin_url('payroll_contracts/add/$1/contracts/$2') .'"><i class="fa fa-plus-circle"></i>'. lang("contracts_add") .'</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>', "employee_id, id");
        echo $this->datatables->generate();
	}

    public function see($contract_id = NULL, $employee_id = NULL)
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $employee = $this->Employees_model->get_by_id($employee_id, $contract_id);
        $this->data["employee"] = $employee;
        $this->data["types_contracts"] = $this->Employees_model->get_types_contracts();
        $this->data["payment_means"] = $this->Payroll_contracts_model->get_payment_means();
        $this->data["employee_contacts"] = $this->Employees_model->get_employee_contacts($employee_id);
        $this->data["type_regime"] = $this->Employees_model->get_types_regime_by_id($employee->tipo_regimen);
        $this->data["document_type"] = $this->Employees_model->get_document_types_by_id($employee->tipo_documento);
        $this->data["concepts_contract"] = $this->Payroll_contracts_model->get_payroll_contract_concepts($employee_id, $contract_id);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->load_view($this->theme.'payroll_contracts/see', $this->data);
    }

	public function add($employee_id = NULL, $module = NULL, $contract_id = NULL)
	{
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->detect_contract_termination();

        if (empty($this->Payroll_settings)) {
        	$this->session->set_flashdata("error", lang("payroll_contracts_no_general_settings"));
        	admin_redirect("payroll_settings");
        }

        if (!empty($employee_id)) {
            if (!empty($this->Employees_model->getEmployeeWithContracts($employee_id)->ids)) {
                $this->session->set_flashdata("error", "No es posible para crear un nuevo contrato debido a que el empleado ya tiene uno activo");
                admin_redirect("payroll_contracts");
            }
        }

        $employees = $this->Employees_model->get_without_contracts($employee_id, $contract_id);

        if (!empty($employee_id)) {
            if (empty($employees)) {
                $this->session->set_flashdata("warning", "No existe empleado con contratos inactivos");
                admin_redirect("payroll_contracts");
            }
        }

        $this->data["module"] = $module;
        $this->data["employees"] = $employees;
        $this->data["employee_id"] = $employee_id;
        $this->data["afps"] = $this->Payroll_contracts_model->get_afps();
        $this->data["epss"] = $this->Payroll_contracts_model->get_epss();
        $this->data["areas"] = $this->Payroll_contracts_model->get_areas();
        $this->data["arls"] = $this->Payroll_contracts_model->get_arls();
        $this->data["cajas"] = $this->Payroll_contracts_model->get_cajas();
        $this->data["banks"] = $this->Payroll_contracts_model->get_banks();
        $this->data["billers"] = $this->Payroll_contracts_model->get_billers();
        $this->data["workdays"] = $this->Payroll_contracts_model->get_workdays();
        $this->data["cesantias"] = $this->Payroll_contracts_model->get_cesantias();
        $this->data["payment_means"] = $this->Payroll_contracts_model->get_payment_means();
        $this->data["types_contracts"] = $this->Payroll_contracts_model->get_types_contracts();
        $this->data["types_employees"] = $this->Payroll_contracts_model->get_types_employees();
        $this->data["arl_risk_classes"] = $this->Payroll_contracts_model->get_arl_risk_classes();
        $this->data["payment_frequencies"] = $this->Payroll_contracts_model->get_payment_frequency();
        $this->data["concepts"] = $this->Payroll_concepts_model->get_by_concept_type([SALARY_PAYMENTS, NON_SALARY_PAYMENTS]);

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct("payroll_contracts/add", ['page_title' => lang('contracts_add')], $this->data);
	}

    public function save()
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

    	$this->form_validation->set_rules("internal_code", lang("internal_code"), 'trim|required|is_unique[payroll_contracts.internal_code]');
    	$this->form_validation->set_rules("contract_type", lang("payroll_contracts_type_contract"), 'trim|required');
    	$this->form_validation->set_rules("start_date", lang("contracts_start_date"), 'trim|required');
    	if ($this->input->post("contract_type") != UNDEFINED_TERM) { $this->form_validation->set_rules("end_date", lang("contracts_end_date"), 'trim|required'); }
		$this->form_validation->set_rules("settlement_date", lang("contracts_settlement_date"), 'trim');
		$this->form_validation->set_rules("workday", lang("payroll_contracts_workday"), 'trim|required');
		if ($this->input->post("workday") == FOR_HOURS) { $this->form_validation->set_rules("daily_hours", lang("payroll_contracts_daily_hours"), 'trim|required'); }
		$this->form_validation->set_rules("biller_id", lang("payroll_contracts_biller"), 'trim|required');
		$this->form_validation->set_rules("area", lang("payroll_contracts_area"), 'trim|required');
		$this->form_validation->set_rules("professional_position", lang("payroll_contracts_professional_position"), 'trim|required');
		$this->form_validation->set_rules("retired_risk", lang("retired_risk"), 'trim|required');
		$this->form_validation->set_rules("employee_type", lang("payroll_contracts_type_employee"), 'trim|required');
		$this->form_validation->set_rules("arl_risk_classes", lang("arl_risk_classes"), "trim|required");

		$employee_type_data = $this->Payroll_contracts_model->get_types_employees_by_id($this->input->post("employee_type"));
		if ($employee_type_data->afp == YES) { $this->form_validation->set_rules("afp", lang("payroll_contracts_afp"), 'trim|required'); }
		if ($employee_type_data->eps == YES) { $this->form_validation->set_rules("eps", lang("payroll_contracts_eps"), 'trim|required'); }
		if ($employee_type_data->arl == YES) { $this->form_validation->set_rules("arl", lang("payroll_contracts_arl"), 'trim|required'); }
		if ($employee_type_data->caja == YES) { $this->form_validation->set_rules("caja", lang("payroll_contracts_caja"), 'trim|required'); }
		if ($employee_type_data->cesantias == YES) { $this->form_validation->set_rules("cesantia", lang("payroll_contracts_cesantia"), 'trim|required'); }

        if ($this->input->post("employee_type") == STUDENT_CONTRIBUTIONS_ONLY_LABOR_RISK) {
            $this->form_validation->set_rules("base_amount", lang("payroll_contracts_base_amount"), 'trim|required');
        } else {
            $this->form_validation->set_rules("base_amount", lang("payroll_contracts_base_amount"), 'trim|required|greater_than[0]');
        }
		if ($this->input->post("base_amount") <= ($this->Payroll_settings->minimum_salary_value * 2)) {
			$this->form_validation->set_rules("trans_allowance", lang("payroll_contracts_trans_allowance"), 'trim|required');
		}
		$this->form_validation->set_rules("integral_salary", lang("payroll_contracts_integral_salary"), 'trim');
		$this->form_validation->set_rules("withholding_method", lang("payroll_contracts_withholding_method"), 'trim|required');
		if ($this->input->post("withholding_method") == WITHHOLDING_PROCESS_2) { $this->form_validation->set_rules("withholding_percentage", lang("withholding_percentage"), 'trim|required'); }
		$this->form_validation->set_rules("payment_method", lang("payroll_contracts_payment_method"), 'trim|required');
		if ($this->input->post("payment_method") == WIRE_TRANSFER) {
			$this->form_validation->set_rules("bank", lang("bank"), 'trim|required');
			$this->form_validation->set_rules("account_type", lang("payroll_contracts_account_type"), 'trim|required');
		}

		if ($this->input->post('salary_bonus_check')) { $this->form_validation->set_rules('salary_bonus', lang("salary_bonus"), 'trim|required'); }
		if ($this->input->post('non_salary_bonus_check')) { $this->form_validation->set_rules('non_salary_bonus', lang("non_salary_bonus"), 'trim|required'); }

		if ($this->form_validation->run()) {
			$data = [
				"companies_id"=>$this->input->post("employee_id"),
				"internal_code"=>$this->input->post("internal_code"),
				"contract_type"=>$this->input->post("contract_type"),
				"employee_type"=>$this->input->post("employee_type"),
				"creation_date"=>date("y-m-d"),
				"start_date"=>$this->input->post("start_date"),
				"end_date"=>($this->input->post("end_date")) ? $this->input->post("end_date") : "0000-00-00",
				"settlement_date"=>($this->input->post("settlement_date") ? $this->input->post("settlement_date") : "0000-00-00"),
				"workday"=>$this->input->post("workday"),
				"biller_id"=>$this->input->post("biller_id"),
				"area"=>$this->input->post("area"),
				"professional_position"=>$this->input->post("professional_position"),
				"retired_risk"=>$this->input->post("retired_risk"),
				"contract_status"=>ACTIVE,
				"base_amount"=>$this->input->post("base_amount"),
				"integral_salary"=>$this->input->post("integral_salary"),
				"trans_allowance"=>$this->input->post("trans_allowance"),
				"payment_frequency"=>$this->Payroll_settings->payment_frequency,
				"withholding_method"=>HAND_CALCULATION,
				"payment_method"=>$this->input->post("payment_method"),
				"afp_id"=>$this->input->post("afp"),
				"eps_id"=>$this->input->post("eps"),
				"arl_id"=>$this->input->post("arl"),
				"arl_risk_classes" => $this->input->post("arl_risk_classes"),
				"caja_id"=>$this->input->post("caja"),
				"cesantia_id"=>$this->input->post("cesantia"),
				'created_by'=>$this->session->userdata('user_id')
			];
			if ($this->input->post("contract_type") != UNDEFINED_TERM) { $data["end_date"] = $this->input->post("end_date"); }
			if ($this->input->post("workday") == FOR_HOURS) { $data["daily_hours"] = $this->input->post("daily_hours"); }
			if ($this->input->post("withholding_method") == WITHHOLDING_PROCESS_2) { $data["withholding_percentage"] = $this->input->post("withholding_percentage"); }
			if ($this->input->post("payment_method") == WIRE_TRANSFER) {
				$data["bank"] = $this->input->post("bank");
				$data["account_type"] = $this->input->post("account_type");
				$data["account_no"] = $this->input->post("account_no");
			}

			$contract_id_saved = $this->Payroll_contracts_model->insert($data);
			if ($contract_id_saved != FALSE) {
				$concepts_contract = $this->get_concepts($contract_id_saved, $this->input->post("employee_id"));
                if (!empty($concepts_contract)) {
                    $this->PayrollContractConcepts_model->insert_batch($concepts_contract);
                }

				$this->session->set_flashdata("message", lang("payroll_contracts_saved"));
                admin_redirect("payroll_contracts");
			} else {
				$this->session->set_flashdata("post", (object) $this->input->post());
				$this->session->set_flashdata("error", lang("payroll_contracts_not_saved"));

        		admin_redirect("payroll_contracts/add/".$this->input->post("employee_id"));
			}
		} else {
			$this->session->set_flashdata("error", validation_errors());

			$this->session->set_flashdata("post", (object) $this->input->post());
            admin_redirect("payroll_contracts/add/".$this->input->post("employee_id"));
	    }
	}

    private function get_concepts($contract_id, $employee_id)
	{
		$data = [];
		if ($this->input->post('concept_check')) {
			foreach ($this->input->post('concept_check') as $key => $concept_check) {
				$data[] = [
					'contract_id' => $contract_id,
					'employee_id' => $employee_id,
					'concept_id' => $concept_check,
					'concept_type_id' => $this->input->post('concept_type')[$key],
					'amount' => $this->input->post('concept')[$key],
					'applied' => NOT
				];
			}
		}

		return $data;
	}

	public function edit($employee_id = NULL, $module = NULL, $contract_id = NULL)
	{
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        if (empty($this->Payroll_settings)) {
        	$this->session->set_flashdata("error", lang("payroll_contracts_no_general_settings"));
        	admin_redirect("payroll_settings");
        }

        $contract = $this->Payroll_contracts_model->get_by_employee_id($employee_id, $contract_id);

        $this->data["module"] = $module;
        $this->data["contract"] = $contract;
        $this->data["afps"] = $this->Payroll_contracts_model->get_afps();
        $this->data["epss"] = $this->Payroll_contracts_model->get_epss();
        $this->data["areas"] = $this->Payroll_contracts_model->get_areas();
        $this->data["arls"] = $this->Payroll_contracts_model->get_arls();
        $this->data["cajas"] = $this->Payroll_contracts_model->get_cajas();
        $this->data["banks"] = $this->Payroll_contracts_model->get_banks();
        $this->data["billers"] = $this->Payroll_contracts_model->get_billers();
        $this->data["workdays"] = $this->Payroll_contracts_model->get_workdays();
        $this->data["cesantias"] = $this->Payroll_contracts_model->get_cesantias();
        $this->data["payment_means"] = $this->Payroll_contracts_model->get_payment_means();
        $this->data["types_contracts"] = $this->Payroll_contracts_model->get_types_contracts();
        $this->data["types_employees"] = $this->Payroll_contracts_model->get_types_employees();
        $this->data["arl_risk_classes"] = $this->Payroll_contracts_model->get_arl_risk_classes();
        $this->data["employee"] = $this->Payroll_contracts_model->get_employee_by_id($employee_id);
        $this->data["payment_frequencies"] = $this->Payroll_contracts_model->get_payment_frequency();
        $this->data["concepts"] = $this->Payroll_concepts_model->get_by_concept_type([SALARY_PAYMENTS, NON_SALARY_PAYMENTS]);

        $existingContractPreparationPayroll = $this->Payroll_management_model->getExistingContractInPreparationPayroll($contract_id);

        $contract_concepts = $this->PayrollContractConcepts_model->get(["contract_id"=>$contract->id, "deleted"=>NOT]);
        $approved_payroll = $this->Payroll_management_model->get_by_status(APPROVED);

        $in_preparation_payroll = $this->Payroll_management_model->get_by_status(IN_PREPARATION);

		if (!empty($in_preparation_payroll)) {
			if ($in_preparation_payroll->number == SECOND_FORTNIGHT) {
				$month = $in_preparation_payroll->month;
                $payroll_id = $in_preparation_payroll->id;
				$payroll_items = $this->Payroll_management_model->get_payroll_items_by_month_employee_id($payroll_id, $employee_id, $month, FIRST_FORTNIGHT);
				$this->data["payroll_items"] = $payroll_items;
			}
		}

        $allow_concept_editing = TRUE;
        $ordered_contract_concepts = [];
        foreach ($contract_concepts as $contract_concept) {
        	$ordered_contract_concepts[$contract_concept->concept_id] = $contract_concept;
        }
        $this->data['contract_concepts'] = $ordered_contract_concepts;
        $this->data["existingContractPreparationPayroll"] = $existingContractPreparationPayroll;

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $pageTitle = lang('payroll_contracts_edit'). " <small>{$this->data["employee"]->name}</small>";
        $this->page_construct('payroll_contracts/edit', ['page_title' => $pageTitle], $this->data);
	}

    public function update()
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

    	$this->form_validation->set_rules("internal_code", lang("internal_code"), 'trim|required');
    	$this->form_validation->set_rules("contract_type", lang("payroll_contracts_type_contract"), 'trim|required');
    	$this->form_validation->set_rules("start_date", lang("contracts_start_date"), 'trim|required');
    	if ($this->input->post("contract_type") != UNDEFINED_TERM) { $this->form_validation->set_rules("end_date", lang("contracts_end_date"), 'trim|required'); }
		$this->form_validation->set_rules("settlement_date", lang("contracts_settlement_date"), 'trim');
		$this->form_validation->set_rules("workday", lang("payroll_contracts_workday"), 'trim|required');
		if ($this->input->post("workday") == FOR_HOURS) { $this->form_validation->set_rules("daily_hours", lang("payroll_contracts_daily_hours"), 'trim|required'); }
		$this->form_validation->set_rules("biller_id", lang("payroll_contracts_biller"), 'trim|required');
		$this->form_validation->set_rules("area", lang("payroll_contracts_area"), 'trim|required');
		$this->form_validation->set_rules("professional_position", lang("payroll_contracts_professional_position"), 'trim|required');
		$this->form_validation->set_rules("retired_risk", lang("retired_risk"), 'trim|greater_than_equal_to[0]');
		$this->form_validation->set_rules("employee_type", lang("payroll_contracts_type_employee"), 'trim|required');
		$this->form_validation->set_rules("arl_risk_classes", lang("arl_risk_classes"), "trim|required");

		$employee_type_data = $this->Payroll_contracts_model->get_types_employees_by_id($this->input->post("employee_type"));
		if ($employee_type_data->afp == YES) { $this->form_validation->set_rules("afp", lang("afp"), 'trim|required'); }
		if ($employee_type_data->eps == YES) { $this->form_validation->set_rules("eps", lang("eps"), 'trim|required'); }
		if ($employee_type_data->arl == YES) { $this->form_validation->set_rules("arl", lang("arl"), 'trim|required'); }
		if ($employee_type_data->caja == YES) { $this->form_validation->set_rules("caja", lang("caja"), 'trim|required'); }
		if ($employee_type_data->cesantias == YES) { $this->form_validation->set_rules("cesantia", lang("cesantia"), 'trim|required'); }

		$this->form_validation->set_rules("base_amount", lang("payroll_contracts_base_amount"), 'trim|required|greater_than[0]');
		if ($this->input->post("base_amount") <= ($this->Payroll_settings->minimum_salary_value * 2)) {
			$this->form_validation->set_rules("trans_allowance", lang("payroll_contracts_trans_allowance"), 'trim|required');
		}
		$this->form_validation->set_rules("integral_salary", lang("payroll_contracts_integral_salary"), 'trim');
		$this->form_validation->set_rules("withholding_method", lang("payroll_contracts_withholding_method"), 'trim');
		if ($this->input->post("withholding_method") == WITHHOLDING_PROCESS_2) { $this->form_validation->set_rules("withholding_percentage", lang("withholding_percentage"), 'trim|required'); }
		$this->form_validation->set_rules("payment_method", lang("payroll_contracts_payment_method"), 'trim|required');
		if ($this->input->post("payment_method") == WIRE_TRANSFER) {
			$this->form_validation->set_rules("bank", lang("bank"), 'trim|required');
			$this->form_validation->set_rules("account_type", lang("payroll_contracts_account_type"), 'trim|required');
		}

		if ($this->input->post('salary_bonus_check')) { $this->form_validation->set_rules('salary_bonus', lang("salary_bonus"), 'trim|required'); }
		if ($this->input->post('non_salary_bonus_check')) { $this->form_validation->set_rules('non_salary_bonus', lang("non_salary_bonus"), 'trim|required'); }

		$employee_id = $this->input->post("employee_id");
		$contract_id = $this->input->post('contract_id');

		if ($this->form_validation->run()) {
			$data = [
				"internal_code"=>$this->input->post("internal_code"),
				"contract_type"=>$this->input->post("contract_type"),
				"employee_type"=>$this->input->post("employee_type"),
				"start_date"=>$this->input->post("start_date"),
				"end_date"=>($this->input->post("end_date")) ? $this->input->post("end_date") : "0000-00-00",
				"settlement_date"=>($this->input->post("settlement_date")) ? $this->input->post("settlement_date") : "0000-00-00",
				"workday"=>$this->input->post("workday"),
				"biller_id"=>$this->input->post("biller_id"),
				"area"=>$this->input->post("area"),
				"professional_position"=>$this->input->post("professional_position"),
				"retired_risk"=>$this->input->post("retired_risk"),
				"base_amount"=>$this->input->post("base_amount"),
				"integral_salary"=>$this->input->post("integral_salary"),
				"trans_allowance"=>$this->input->post("trans_allowance"),
				"payment_frequency"=>$this->input->post('payment_frequency'),
				"withholding_method"=>HAND_CALCULATION,
				"payment_method"=>$this->input->post("payment_method"),
				"afp_id"=>$this->input->post("afp"),
				"eps_id"=>$this->input->post("eps"),
				"arl_id"=>$this->input->post("arl"),
				"arl_risk_classes"=>$this->input->post("arl_risk_classes"),
				"caja_id"=>$this->input->post("caja"),
				"cesantia_id"=>$this->input->post("cesantia")
			];

			if ($this->input->post("workday") == FOR_HOURS) { $data["daily_hours"] = $this->input->post("daily_hours"); }
			if ($this->input->post("withholding_method") == WITHHOLDING_PROCESS_2) { $data["withholding_percentage"] = $this->input->post("withholding_percentage"); }
			if ($this->input->post("payment_method") == WIRE_TRANSFER) {
				$data["bank"] = $this->input->post("bank");
				$data["account_type"] = $this->input->post("account_type");
				$data["account_no"] = $this->input->post("account_no");
			}
			if ($this->input->post('salary_bonus_check')) { $data['salary_bonus'] = $this->input->post('salary_bonus'); }
			if ($this->input->post('non_salary_bonus_check')) { $data['non_salary_bonus'] = $this->input->post('non_salary_bonus'); }

			$contract_data = $this->Payroll_contracts_model->get_by_id($contract_id);
			$contract_updated = $this->Payroll_contracts_model->update($data, $employee_id, $contract_id);

			if ($contract_updated == TRUE) {
                $this->detect_contract_termination();
                $this->updateContractConcepts($contract_id, $employee_id);

				$this->session->set_flashdata("message", lang("payroll_contracts_updated"));

                if ($this->input->post("module") == "contracts") {
                  admin_redirect("payroll_contracts");
                } else {
        		  admin_redirect("employees");
                }
			} else {
				$this->session->set_flashdata("post", (object) $this->input->post());
				$this->session->set_flashdata("error", lang("payroll_contracts_not_updated"));

        		admin_redirect("payroll_contracts/edit/". $employee_id);
			}
		} else {
			$this->session->set_flashdata("error", validation_errors());

			$this->session->set_flashdata("post", (object) $this->input->post());
            admin_redirect("payroll_contracts/edit/". $employee_id);
	    }
	}

    private function updateContractConcepts($contractId, $employeeId)
    {
        $this->validateChangesContractConcept($contractId, $employeeId);

        $this->clearContractConcepts($contractId, $employeeId);
    }

	private function validateChangesContractConcept($contractId, $employeeId)
	{
		$data = [];
        $conceptsChecked = $this->input->post('concept_check');
        $conceptsId      = $this->input->post('concept');

		if ($conceptsChecked) {
			foreach ($conceptsChecked as $key => $conceptIdCheck) {
                $contractConcept = $this->PayrollContractConcepts_model->find([
                    "contract_id" => $contractId,
                    "employee_id" => $employeeId,
                    "concept_id"  => $conceptIdCheck
                ]);

                if (!empty($contractConcept)) {
				    $newContractConcept = $contractConcept;

                    if ($contractConcept->amount != $conceptsId[$key]) {
                        $newContractConcept->amount = $conceptsId[$key];
                        $newContractConcept->applied = NOT;
                        $newContractConcept->deleted = NOT;

                        $contractConceptsUpdated = $this->PayrollContractConcepts_model->update($newContractConcept, $contractId, $employeeId, $conceptIdCheck);
                        if ($contractConceptsUpdated == true) {
                            $this->clearItemsFutures($contractId, $employeeId, $conceptIdCheck);
                        }

                        $data[] = $newContractConcept;
                    }
                } else {
                    $concept = [
                        'contract_id' => $contractId,
                        'employee_id' => $employeeId,
                        'concept_id' => $conceptIdCheck,
                        'concept_type_id' => $this->input->post('concept_type')[$key],
                        'amount' => $this->input->post('concept')[$key],
                        'applied' => NOT
                    ];
                    $this->PayrollContractConcepts_model->insert($concept);
                }
			}
		}

		return $data;
	}

    private function clearItemsFutures($contractId, $employeeId, $conceptIdCheck)
    {
        $this->PayrollFuturesItems_model->update(["applied"=>YES], [
            "contract_id" => $contractId,
            "employee_id" => $employeeId,
            "concept_id"  => $conceptIdCheck
        ]);
    }

    // private function clearContractConcepts($contractId, $employeeId, $contractConcepts = [])
    // {
    //     if (!empty($contractConcepts)) {
    //         foreach ($contractConcepts as $concept) {
    //             $this->PayrollContractConcepts_model->update(
    //                 ["applied" => NOT], $contractId, $employeeId, $concept->concept_id);
    //         }
    //     } else {
    //         $this->PayrollContractConcepts_model->update(
    //             ["deleted" => YES],
    //             $contractId, $employeeId);
    //     }
    // }

    private function clearContractConcepts($contractId, $employeeId)
    {
        $conceptsCheckeds = $this->input->post('concept_check');
        $contractConcepts = $this->PayrollContractConcepts_model->get(["contract_id" => $contractId, "employee_id" => $employeeId, "deleted"=>NOT]);

        if (!empty($conceptsCheckeds)) {
            if (!empty($contractConcepts)) {
                foreach ($contractConcepts as $concept) {
                    if (!in_array($concept->concept_id, $conceptsCheckeds)) {
                        $this->PayrollContractConcepts_model->update(
                            ["deleted" => YES, "applied" => YES],
                            $contractId, $employeeId, $concept->concept_id);

                        $this->PayrollFuturesItems_model->update(["applied"=>YES], [
                            "contract_id" => $contractId,
                            "employee_id" => $employeeId,
                            "concept_id"  => $concept->concept_id
                        ]);
                    }
                }
            }
        } else {
            $this->PayrollContractConcepts_model->update(
                ["deleted" => YES, "applied" => YES],
                $contractId, $employeeId);
        }
    }

    public function validate_existing_internal_code()
    {
        $internal_code = $this->input->get("internal_code");
        $employee_id = $this->input->get("employee_id");
        $existing_internal_code = $this->Payroll_contracts_model->get_internal_code($internal_code, $employee_id);
        if ($existing_internal_code === TRUE) {
            echo json_encode(TRUE);
        } else {
            echo json_encode(FALSE);
        }
    }

    public function get_professional_position()
    {
        $area_id = $this->input->get('area');
        $professional_position = $this->Payroll_contracts_model->get_professional_position_by_area_id($area_id);

        $dropdown_options_string = '<option value="">'.lang("select").'</option>';
        if ($professional_position !== FALSE) {
            foreach ($professional_position as $position) {
                $dropdown_options_string .= '<option value="'.$position->id.'">'.$position->name.'</option>';
            }
        }

        echo $dropdown_options_string;
    }

	private function set_user_log($contract_data, $updated_contract_data)
	{
        $diff = array_diff_assoc((array) $updated_contract_data, (array) $contract_data);

        if (!empty($diff)) {
            $description_changes_detected = '';
            $afps = $this->Payroll_contracts_model->get_afps();
            $epss = $this->Payroll_contracts_model->get_epss();
            $arls = $this->Payroll_contracts_model->get_arls();
            $areas = $this->Payroll_contracts_model->get_areas();
            $banks = $this->Payroll_contracts_model->get_banks();
            $cajas = $this->Payroll_contracts_model->get_cajas();
            $boolean_options = [NOT=>lang("no"), YES=>lang("yes")];
            $workdays = $this->Payroll_contracts_model->get_workdays();
            $cesantias = $this->Payroll_contracts_model->get_cesantias();
            $payment_methods = $this->Payroll_contracts_model->get_payment_means();
            $types_contracts = $this->Payroll_contracts_model->get_types_contracts();
            $types_employees = $this->Payroll_contracts_model->get_types_employees();
            $arl_risk_classes = $this->Payroll_contracts_model->get_arl_risk_classes();
            $professional_positions = $this->Payroll_contracts_model->get_professional_positions();
            $account_type = [1=>lang("payroll_contracts_savings_account"), 2=>lang("payroll_contracts_current_account"), 3=>lang("payroll_contracts_account_virtual_wallet")];
            $description = 'El usuario '. $this->session->userdata('first_name') .' '. $this->session->userdata('last_name') .' hizo lo siguiente cambios en la edición de Contrato: ';

            foreach ($diff as $field_name => $field_data) {
                $data_from = '';
                $data_to = '';

                if ($field_name == 'contract_type') {
                    foreach ($types_contracts as $type_contract) {
                        if ($type_contract->id == $contract_data->$field_name) {
                            $data_from = $type_contract->description;
                        }
                        if ($type_contract->id == $field_data) {
                            $data_to = $type_contract->description;
                        }
                    }
                } else if ($field_name == 'workday') {
                    foreach ($workdays as $workday) {
                        if ($workday->id == $contract_data->$field_name) {
                            $data_from = $workday->name;
                        }
                        if ($workday->id == $field_data) {
                            $data_to = $workday->name;
                        }
                    }
                } else if ($field_name == 'retired_risk' || $field_name == 'integral_salary' || $field_name == 'icbf_id' || $field_name == 'sena_id' || $field_name == 'trans_allowance') {
                    $data_from = $boolean_options[$contract_data->$field_name];
                    $data_to = $boolean_options[$field_data];
                } else if ($field_name == 'area') {
                    foreach ($areas as $area) {
                        if ($area->id == $contract_data->$field_name) {
                            $data_from = $area->name;
                        }
                        if ($area->id == $field_data) {
                            $data_to = $area->name;
                        }
                    }
                } else if ($field_name == 'professional_position') {
                    foreach ($professional_positions as $professional_position) {
                        if ($professional_position->id == $contract_data->$field_name) {
                            $data_from = $professional_position->name;
                        }
                        if ($professional_position->id == $field_data) {
                            $data_to = $professional_position->name;
                        }
                    }
                } else if ($field_name == 'employee_type') {
                    foreach ($types_employees as $type_employee) {
                        if ($type_employee->id == $contract_data->$field_name) {
                            $data_from = $type_employee->description;
                        }

                        if ($type_employee->id == $field_data) {
                            $data_to = $type_employee->description;
                        }
                    }
                } else if ($field_name == 'payment_method') {
                    foreach ($payment_methods as $payment_method) {
                        if ($payment_method->code == $contract_data->$field_name) {
                            $data_from = $payment_method->name;
                        }

                        if ($payment_method->code == $field_data) {
                            $data_to = $payment_method->name;
                        }
                    }
                } else if ($field_name == 'bank') {
                    foreach ($banks as $bank) {
                        if ($bank->id == $contract_data->$field_name) {
                            $data_from = $bank->name;
                        }

                        if ($bank->id == $field_data) {
                            $data_to = $bank->name;
                        }
                    }
                } else if ($field_name == 'account_type') {
                    $data_from = $account_type[$contract_data->$field_name];
                    $data_to = $account_type[$field_data];
                } else if ($field_name == 'afp_id') {
                    foreach ($afps as $afp) {
                        if ($afp->id == $contract_data->$field_name) {
                            $data_from = strtolower($afp->name);
                        }

                        if ($afp->id == $field_data) {
                            $data_to = strtolower($afp->name);
                        }
                    }
                } else if ($field_name == 'eps_id') {
                    foreach ($epss as $eps) {
                        if ($eps->id == $contract_data->$field_name) {
                            $data_from = strtolower($eps->name);
                        }

                        if ($eps->id == $field_data) {
                            $data_to = strtolower($eps->name);
                        }
                    }
                } else if ($field_name == 'cesantia_id') {
                    foreach ($cesantias as $cesantia) {
                        if ($cesantia->id == $contract_data->$field_name) {
                            $data_from = strtolower($cesantia->name);
                        }
                        if ($cesantia->id == $field_data) {
                            $data_to = strtolower($cesantia->name);
                        }
                    }
                } else if ($field_name == 'arl_id') {
                    foreach ($arls as $arl) {
                        if ($arl->id == $contract_data->$field_name) {
                            $data_from = strtolower($arl->name);
                        }
                        if ($arl->id == $field_data) {
                            $data_to = strtolower($arl->name);
                        }
                    }
                } else if ($field_name == 'arl_risk_classes') {
                    foreach ($arl_risk_classes as $arl_risk_class) {
                        if ($arl_risk_class->id == $contract_data->$field_name) {
                            $data_from = strtolower($arl_risk_class->name. ' - '. $arl_risk_class->description);
                        }
                        if ($arl_risk_class->id == $field_data) {
                            $data_to = strtolower($arl_risk_class->name. ' - '. $arl_risk_class->description);
                        }
                    }
                } else if ($field_name == 'caja_id') {
                    foreach ($cajas as $caja) {
                        if ($caja->id == $contract_data->$field_name) {
                            $data_from = strtolower($caja->name);
                        }
                        if ($caja->id == $field_data) {
                            $data_to = strtolower($caja->name);
                        }
                    }
                } else {
                    $data_from = $contract_data->$field_name;
                    $data_to = $field_data;
                }

                $description_changes_detected .= 'Campo: '. lang($field_name). ' de "'. $data_from .'" a "'. $data_to. '", ';
            }

            $log_user_data = [
                'date'=>date('Y-m-d H:m:i'),
                'type_id'=>EDITION,
                'user_id'=>$this->session->userdata('user_id'),
                'module_name'=>'payroll_contracts',
                'table_name'=>'payroll_contracts',
                'record_id'=> $contract_data->id,
                'description'=>trim($description . $description_changes_detected, ', ')
            ];
            $this->UserActivities_model->insert($log_user_data);
        }
	}

    public function detect_contract_termination()
    {
        $contracts = $this->Payroll_contracts_model->get();
        if (!empty($contracts)) {
            foreach ($contracts as $contract) {
                $end_date = $contract->end_date;
                $settlement_date =  $contract->settlement_date;
                if ($settlement_date != '0000-00-00') {
                    $this->Payroll_contracts_model->update(['contract_status'=>INACTIVE], $contract->companies_id, $contract->id);
                } else if ($end_date != '0000-00-00') {
                    if ($end_date < date('Y-m-d')) {
                        $this->Payroll_contracts_model->update(['contract_status'=>INACTIVE], $contract->companies_id, $contract->id);
                    }
                }
            }
        }
    }

    public function getPayrollContractsReport()
    {

        $this->load->library('excel');
        $this->load->helper('excel');

        $sheet = $this->excel->getActiveSheet();

        $fileName = "Reporte de Contratos";
        $sheet->setTitle($fileName);

        $this->getPayrollContractReportHeader($sheet);
        $this->getPayrollContractReportBody($sheet);

        create_excel($this->excel, $fileName);
    }

    private function getPayrollContractReportHeader($sheet)
    {
        $headerStyle = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
                ],
            ],
            'font' => [
                'bold' => true,
                'size' => 14,
                'color' => ['rgb' => '000000'],
            ],
        ];

        $secondHeaderStyle = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
            'font' => [
                "bold" => true,
                'color' => ['rgb' => '000000'],
            ],
        ];

        $sheet->mergeCells('A2:E2')->setCellValue("A2", "Reporte de Contratos de Nomína")->getStyle('A2')->getFont()->setBold(true);

        $sheet->mergeCells('A6:V6');
        $sheet->setCellValue("A6", "Contratos");
        $sheet->getStyle('A6:V6')->applyFromArray($headerStyle);

        $sheet->getRowDimension(7)->setRowHeight(48);
        $sheet->setCellValue("A7", "Empleado")->getColumnDimension("A")->setAutoSize(true);
        $sheet->setCellValue("B7", "Tipo de contrato")->getColumnDimension("B")->setAutoSize(true);
        $sheet->setCellValue("C7", "Código interno")->getColumnDimension("C")->setAutoSize(true);
        $sheet->setCellValue("D7", "Fecha de creación")->getColumnDimension("D")->setAutoSize(true);
        $sheet->setCellValue("E7", "Fecha Inicio")->getColumnDimension("E")->setAutoSize(true);
        $sheet->setCellValue("F7", "Fecha Fin")->getColumnDimension("F")->setAutoSize(true);
        $sheet->setCellValue("G7", "Fecha Terminación anticipada")->getColumnDimension("G")->setAutoSize(true);
        $sheet->setCellValue("H7", "Sucursal")->getColumnDimension("H")->setAutoSize(true);
        $sheet->setCellValue("I7", "Area")->getColumnDimension("I")->setAutoSize(true);
        $sheet->setCellValue("J7", "Cargo")->getColumnDimension("J")->setAutoSize(true);
        $sheet->setCellValue("K7", "Salario")->getColumnDimension("K")->setAutoSize(true);
        $sheet->setCellValue("L7", "Método de retención")->getColumnDimension("L")->setAutoSize(true);
        $sheet->setCellValue("M7", "Porcentaje de retención")->getColumnDimension("M")->setAutoSize(true);
        $sheet->setCellValue("N7", "Método de Pago")->getColumnDimension("N")->setAutoSize(true);
        $sheet->setCellValue("O7", "Tipo de Empleado")->getColumnDimension("O")->setAutoSize(true);
        $sheet->setCellValue("P7", "AFP")->getColumnDimension("P")->setAutoSize(true);
        $sheet->setCellValue("Q7", "EPS")->getColumnDimension("Q")->setAutoSize(true);
        $sheet->setCellValue("R7", "Cesantías")->getColumnDimension("R")->setAutoSize(true);
        $sheet->setCellValue("S7", "ARL")->getColumnDimension("S")->setAutoSize(true);
        $sheet->setCellValue("T7", "Clase de Riego")->getColumnDimension("T")->setAutoSize(true);
        $sheet->setCellValue("U7", "Caja de Compensación")->getColumnDimension("U")->setAutoSize(true);
        $sheet->setCellValue("V7", "Conceptos de contratos")->getColumnDimension("V")->setAutoSize(true);

        $sheet->getStyle('A7:V7')->applyFromArray($secondHeaderStyle);
        $sheet->getStyle('A7:V7')->getAlignment()->setWrapText(true);
    }

    private function getPayrollContractReportBody($sheet)
    {
        $payrollContract = new Payroll_contracts_model();
        $payrollContracts = $payrollContract->getPayrollContracts();

        if (!empty($payrollContracts)) {
            $row = 8;
            foreach ($payrollContracts as $contract) {
                $sheet->setCellValue("A{$row}", ucwords(mb_strtolower($contract->name)))->getColumnDimension("A")->setAutoSize(true);
                $sheet->setCellValue("B{$row}", $contract->contract_type)->getColumnDimension("B")->setAutoSize(true);
                $sheet->setCellValue("C{$row}", $contract->internal_code)->getColumnDimension("C")->setAutoSize(true);
                $sheet->setCellValue("D{$row}", $contract->creation_date)->getColumnDimension("D")->setAutoSize(true);
                $sheet->setCellValue("E{$row}", $contract->start_date)->getColumnDimension("E")->setAutoSize(true);
                $sheet->setCellValue("F{$row}", $contract->end_date)->getColumnDimension("F")->setAutoSize(true);
                $sheet->setCellValue("G{$row}", $contract->settlement_date)->getColumnDimension("G")->setAutoSize(true);
                $sheet->setCellValue("H{$row}", $contract->biller)->getColumnDimension("H")->setAutoSize(true);
                $sheet->setCellValue("I{$row}", $contract->area)->getColumnDimension("I")->setAutoSize(true);
                $sheet->setCellValue("J{$row}", $contract->professional_position)->getColumnDimension("J")->setAutoSize(true);
                $sheet->setCellValue("K{$row}", $contract->base_amount)->getColumnDimension("K")->setAutoSize(true);
                    $sheet->getStyle("K{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                $sheet->setCellValue("L{$row}", $contract->withholding_method)->getColumnDimension("L")->setAutoSize(true);
                $sheet->setCellValue("M{$row}", $contract->withholding_percentage)->getColumnDimension("M")->setAutoSize(true);
                $sheet->setCellValue("N{$row}", $contract->payment_method)->getColumnDimension("N")->setAutoSize(true);
                $sheet->setCellValue("O{$row}", $contract->employee_type)->getColumnDimension("O")->setAutoSize(true);
                $sheet->setCellValue("P{$row}", $contract->afp)->getColumnDimension("P")->setAutoSize(true);
                $sheet->setCellValue("Q{$row}", $contract->eps)->getColumnDimension("Q")->setAutoSize(true);
                $sheet->setCellValue("R{$row}", $contract->cesantia)->getColumnDimension("R")->setAutoSize(true);
                $sheet->setCellValue("S{$row}", $contract->arl)->getColumnDimension("S")->setAutoSize(true);
                $sheet->setCellValue("T{$row}", $contract->arl_risk_classes)->getColumnDimension("T")->setAutoSize(true);
                $sheet->setCellValue("U{$row}", $contract->caja)->getColumnDimension("U")->setAutoSize(true);
                $sheet->setCellValue("V{$row}", $contract->contract_concepts)->getColumnDimension("V")->setAutoSize(true);

                $row++;
            }
        } else {
            $this->session->set_flashdata('error', 'No hay datos disponibles');
            admin_redirect('payroll_contracts');
        }
    }
}

/* End of file Payroll_contracts.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/controllers/admin/Payroll_contracts.php */