<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['warranty']                           = 'Garantía';
$lang['add_warranty']                       = 'Agregar Garantía';
$lang['edit_warranty']                      = 'Editar Garantía';
$lang['search_product']                     = 'Buscar producto';
$lang['detail']                             = 'Detalle';
$lang['customer_data']                      = 'Datos del cliente';

$lang['warranty_reason']                    = 'Motivo de garantía';
$lang['warranty_details']                   = 'Detalle de garantía';
$lang['warranty_activity']                  = 'Actividad de la garantía';
$lang['product_status']                     = 'Estado del Producto';
$lang['customer_note']                      = 'Nota del cliente';
$lang['has_it_been_intervened']             = '¿Ha sido intervenido?';
$lang['contact_means']                      = 'Medio de contacto';
$lang['internal_note']                      = 'Nota interna';
$lang['whatsapp']                           = 'WhatsApp';
$lang['call']                               = 'Llamada';
$lang['customer_since']                     = 'Cliente desde';
$lang['customer_qualification']             = 'Calificación del cliente';
$lang['number_credits']                     = 'Número de créditos';
$lang['customer_attitude']                  = 'Actitud del cliente';
$lang['maximum_purchase']                   = 'Compra máxima';
$lang['add_activity']                       = 'Agregar actividad';
$lang['adjustments_document']               = 'Documento de ajustes';
$lang['transfer_document']                  = 'Documento de traslados';

$lang['required_input']                     = 'El campo %s es obligatorio';
$lang['note_addition_warranty']             = 'Solicitud de garantía del registro: %s del cliente: %d';
$lang['note_subtraction_warranty']          = 'Salida de producto de garantía del registro: %s del cliente: %d';
$lang['adjustment_documents_not_exist']     = 'Documentos de ajustes no existentes';
$lang['adjustment_documents_not_configured']= 'Documentos de ajustes no existentes';

$lang['warranty_saved']                     = 'Garantía guardada';
$lang['warranty_not_saved']                 = 'La Garantía no pudo ser guardada';
$lang['saved_warranty_activity']            = 'Actividad de garantía guardada';
$lang['unsaved_warranty_activity']          = 'La Actividad de garantía no pudo ser guardada';