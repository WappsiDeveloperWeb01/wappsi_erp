<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <!-- <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/view_adjustment_pos/'.$inv->id) ?>" target="_blank">
                <i class="fa fa-print"></i> POS
            </a> -->
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
            </div>
            <div class="text-center" style="margin-bottom:20px;">
                <h3><?= lang('making_order') ?> N° <?= $inv->reference_no; ?></h3>
            </div>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-5">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("estimated_date"); ?>: <?= $this->sma->hrld($inv->est_date); ?><br>
                        <?= lang("responsable"); ?>: <?= $inv->responsable; ?><br>
                    </p>
                    </div>
                    <div class="col-xs-7 text-right order_barcodes">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <label class="table-label"><?= lang("products"); ?> *</label>
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                                <tr>
                                    <th><?= lang("description"); ?> </th>
                                    <th><?= lang("variant"); ?>     </th>
                                    <th><?= lang("quantity"); ?>    </th>
                                    <th><?= lang("cutted"); ?>      </th>
                                    <th><?= lang("in_assemble"); ?> </th>
                                    <th><?= lang("assembled"); ?>   </th>
                                    <th><?= lang("in_packing"); ?>  </th>
                                    <th><?= lang("packed"); ?>      </th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $r = 1;
                                $tTotal = 0;
                                foreach ($rows as $row):
                                    if ($row->production_order_pfinished_item_id != NULL) {
                                        continue;
                                    }
                                ?>
                                    <tr>
                                        <td style="vertical-align:middle;">
                                            <?= $row->product_code.' - '.$row->product_name; ?>
                                        </td>
                                        <th><?= $row->variant; ?></th>
                                        <th class="text-right"><?= $this->sma->formatQuantity($row->quantity); $tTotal+= $row->quantity; ?></th>
                                        <th><?= $this->sma->formatQuantity($row->pfinished_cutting_quantity); ?></th>
                                        <th><?= $this->sma->formatQuantity($row->pfinished_assembling_quantity); ?></th>
                                        <th><?= $this->sma->formatQuantity($row->pfinished_assembling_finished_quantity); ?></th>
                                        <th><?= $this->sma->formatQuantity($row->pfinished_packing_quantity); ?></th>
                                        <th><?= $this->sma->formatQuantity($row->pfinished_packing_finished_quantity); ?></th>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" class="text-right"><?= lang('total') ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($tTotal); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="table-responsive">
                        <label class="table-label"><?= lang("components"); ?> *</label>
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                                <tr>
                                    <th><?= lang("description"); ?></th>
                                    <th><?= lang('product'); ?></th>
                                    <th><?= lang("quantity"); ?></th>
                                    <th><?= lang("cutted"); ?></th>
                                    <th><?= lang("in_assemble"); ?></th>
                                    <th><?= lang("assembled"); ?></th>
                                    <th><?= lang("in_packing"); ?></th>
                                    <th><?= lang("packed"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $r = 1;
                                $tTotal = 0;
                                foreach ($rows as $row):
                                    if ($row->production_order_pfinished_item_id == NULL) {
                                        continue;
                                    }
                                ?>
                                    <tr>
                                        <td style="vertical-align:middle;">
                                            <?= $row->product_code.' - '.$row->product_name; ?>
                                        </td>
                                        <td><?= $row->pfinished_name.' - '.$row->pfinished_code.($row->pfinished_variant ? ' - '.$row->pfinished_variant : ''); ?></td>
                                        <th class="text-right"><?= $this->sma->formatQuantity($row->quantity); $tTotal+= $row->quantity; ?></th>
                                        <td class="text-right"><?= $this->sma->formatQuantity($row->finished_quantity) ?></td>
                                        <td class="text-right"><?= $this->sma->formatQuantity($row->assembling_quantity - $row->assembling_finished_quantity) ?></td>
                                        <td class="text-right"><?= $this->sma->formatQuantity($row->assembling_finished_quantity) ?></td>
                                        <td class="text-right"><?= $this->sma->formatQuantity($row->packing_quantity - $row->packing_finished_quantity) ?></td>
                                        <td class="text-right"><?= $this->sma->formatQuantity($row->packing_finished_quantity) ?></td>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-right" colspan="2"><?= lang('total') ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($tTotal); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <div class="row">
                <div class="col-xs-12 text-left">
                    <?php if ($inv->note || $inv->note != "") { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang("note"); ?>:</p>
                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xs-12 text-center">
                    <p>
                        <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
