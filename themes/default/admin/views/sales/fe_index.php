<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    if ($this->session->userdata('detal_post_processing')) { 
        $this->session->unset_userdata('detal_post_processing'); 
        $this->session->unset_userdata('detal_post_processing_model'); 
    }
?>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="no-print notifications_container">
                <?php if (isset($pending_electronic_document_message)) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?= $pending_electronic_document_message; ?>
                    </div>
                <?php } ?>

                <?php if (isset($validationElectronicHist)) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?= $validationElectronicHist; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('sales/fe_index', ['id'=>'salesFilterForm']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('date_records_filter') ?></label>
                                            <?php $dateRecordsFilter = (isset($_POST["date_records_filter"]) ? $_POST["date_records_filter"] : ''); ?>
                                            <select class="form-control" name="date_records_filter" id="date_records_filter_dh">
                                                <?= $this->sma->get_filter_options($dateRecordsFilter); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('biller') ?></label>
                                            <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                            <?php $billerId = (empty($billerId)) ? $this->session->userdata('biller_id') : $billerId; ?>
                                            <select class="form-control" id="biller_id" name="biller_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('document_type') ?></label>
                                            <?php $documentTypeId = (isset($_POST["document_type"])) ? $_POST["document_type"] : ''; ?>
                                            <select class="form-control" id="document_type" name="document_type">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($documentsTypes as $documentType) : ?>
                                                    <option value="<?= $documentType->id ?>" <?= ($documentType->id == $documentTypeId) ? 'selected' : 'no' ?>><?= $documentType->sales_prefix." - ".$documentType->nombre ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('origin') ?></label>
                                            <?php $saleOrigin = (isset($_POST["sale_origin"])) ? $_POST["sale_origin"] : ''; ?>
                                            <select class="form-control" id="sale_origin" name="sale_origin">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($salesOrigins as $origin) : ?>
                                                    <option value="<?= $origin->id ?>" <?= ($origin->id == $saleOrigin) ? 'selected' : '' ?>><?= $origin->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="client"><?= $this->lang->line('customer') ?></label>
                                            <?php $client = (isset($_POST["client"])) ? $_POST["client"] : ''; ?>
                                            <input type="text" class="form-control input-tip" name="client" id="client" value="<?= $client ?>" placeholder="<?= lang('alls') ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label><?= $this->lang->line('seller') ?></label>
                                        <?php $sellerId = (isset($_POST["seller"])) ? $_POST["seller"] : ''; ?>
                                        <select class="form-control" name="seller" id="seller">
                                            <option value=""><?= lang('alls') ?></option>
                                            <?php foreach ($sellers as $seller): ?>
                                                <?php if (!$this->session->userdata('seller_id') || ($this->session->userdata('seller_id') && $seller->id == $this->session->userdata('seller_id'))): ?>
                                                    <option value="<?= $seller->id ?>" <?= ($seller->id == $sellerId) ? 'selected' : '' ?>><?= $seller->company ?></option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="warehouse_id"><?= $this->lang->line('warehouse') ?></label>
                                            <?php $warehouseId = (isset($_POST["warehouse_id"])) ? $_POST["warehouse_id"] : ''; ?>
                                            <select class="form-control" id="warehouse_id" name="warehouse_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($warehouses as $warehouse) : ?>
                                                    <option value="<?= $warehouse->id ?>" <?= ($warehouse->id == $warehouseId) ? 'selected' : '' ?>><?= $warehouse->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="payment_status"><?= lang('payment_status') ?></label>
                                            <?php $paymentStatus = (isset($_POST["payment_status"])) ? $_POST["payment_status"] : ''; ?>
                                            <select name="payment_status" id="payment_status" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($paymentsStatus as $status) : ?>
                                                    <option value="<?= $status->id ?>" <?= ($status->id == $paymentStatus) ? 'selected' : '' ?>><?= $status->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('originalPaymentMethod') ?></label>
                                            <?php $paymentMethod = (isset($_POST["payment_method"])) ? $_POST["payment_method"] : ''; ?>
                                            <select class="form-control" id="payment_method" name="payment_method">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($paymentsMethods as $method) : ?>
                                                    <option value="<?= $method->id ?>" <?= ($method->id == $paymentMethod) ? 'selected' : '' ?>><?= $method->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="fe_aceptado"><?= $this->lang->line('dianStatus') ?></label>
                                            <?php $feAceptado = (isset($_POST["fe_aceptado"])) ? $_POST["fe_aceptado"] : ''; ?>
                                            <select name="fe_aceptado" id="fe_aceptado" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($dianStates as $dianStatus) : ?>
                                                    <option value="<?= $dianStatus->id ?>" <?= ($dianStatus->id == $feAceptado) ? 'selected' : '' ?>><?= $dianStatus->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="fe_correo_enviado"><?= $this->lang->line('sended_email') ?></label>
                                            <?php $feCorreoEnviado = (isset($_POST["fe_correo_enviado"])) ? $_POST["fe_correo_enviado"] : ''; ?>
                                            <select name="fe_correo_enviado" id="fe_correo_enviado" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($electronicEmailStates as $emailStates) : ?>
                                                    <option value="<?= $emailStates->id ?>" <?= ($emailStates->id == $feCorreoEnviado) ? 'selected' : '' ?>><?= $emailStates->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php if ($this->Owner || $this->Admin): ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="user"><?= $this->lang->line('user') ?></label>
                                                <?php $userId = (isset($_POST["user"])) ? $_POST["user"] : ''; ?>
                                                <select class="form-control" name="user" id="user">
                                                    <option value=""><?= lang('alls') ?></option>
                                                    <?php foreach ($users as $user): ?>
                                                        <option value="<?= $user->id ?>" <?= ($user->id == $userId) ? 'selected' : '' ?>><?= $user->first_name." ".$user->last_name ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                </div>

                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('campaigns') ?></label>
                                            <?php $campaignId = (isset($_POST["campaign"])) ? $_POST["campaign"] : ''; ?>
                                            <select class="form-control" id="campaign" name="campaign">
                                                <option value=""><?= lang('allsf') ?></option>
                                                <?php foreach ($salesCampaigns as $campaign) : ?>
                                                    <option value="<?= $campaign->id ?>" <?= ($campaign->id == $campaignId) ? 'selected' : '' ?>><?= ucfirst(strtolower($campaign->name)) ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="date_controls_dh">
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for="start_date_dh"><?= $this->lang->line('start_date') ?></label>
                                                <?php $startDate = isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>
                                                <input class="form-control datetime" type="text" name="start_date" id="start_date_dh" value="<?= $startDate ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for="end_date_dh"><?= $this->lang->line('end_date') ?></label>
                                                <?php $endDate = isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>
                                                <input class="form-control datetime" type="text" name="end_date" id="end_date_dh" value="<?= $endDate ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
    <?php
        if ($Owner || $Admin || $GP['bulk_actions']) {
    	    echo admin_form_open('sales/sale_actions', 'id="action-form"');
    	}
    ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="SLData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("payment_term"); ?></th>
                                            <th><?= lang("reference_origin"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("affects_to"); ?></th>
                                            <th><?= lang("biller"); ?></th>
                                            <th><?= lang("customer_name"); ?></th>
                                            <th><?= lang("sale_status"); ?></th>
                                            <th><?= lang("grand_total"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("payment"); ?></th>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                            <th></th>
                                            <th><?= lang("acepted_DIAN"); ?></th>
                                            <th><?= lang("sended_email"); ?></th>
                                            <th><?= lang("received_ADQ"); ?></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="13" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
        <div style="display: none;">
            <input type="hidden" name="form_action" value="" id="form_action"/>
            <input type="hidden" name="electronic" value="" id="electronic"/>
            <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
        </div>
        <?=form_close()?>
    <?php } ?>
</div>

<div class="modal fade" id="error_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Lista de errores de facturación electrónica</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang("close"); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="myConfirmationModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myConfirmationModalLabel">Confirmación Envío de Factura de Contingencia</h4>
            </div>
            <div class="modal-body">
                <p>¿Está seguro de enviar la factura de contingencia?</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="confirm_send_document_id">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang("no"); ?></button>
                <button class="btn btn-primary" id="confirm_send_document_button"><?= lang("yes"); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmation_email_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="confirmation_email_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> <i class="fa fa-question-circle"></i> Confirmación de Correo Electrónico</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="confirmation_email">Correo electrónico</label>
                                <input class="form-control" type="email" name="confirmation_email" id="confirmation_email">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="send_electronic_invoice_email">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        removeSlls();

        loadDataTables();

        $(document).on('click', '#confirm_send_document', function() { $('#confirmation_modal').modal('hide'); });
        $(document).on('click', '.pending_errors', function () { openErrorModal($(this).data('sale_id')); });
        $(document).on('click', '#possible_solution_currency', function(){ possible_solution_currency($(this).data('sale_id')); });
        $(document).on('click', '.resend_electronic_document', function(e) { resend_electronic_document(e, $(this)); });
        $(document).on('click', '.change_date_electronic_document', function(e) { change_date_electronic_document(e, $(this)); });
        $(document).on('click', '#confirm_send_document_button', function() { confirm_send_document_button($('#confirm_send_document_id').val()); });
        $(document).on('click', '.email_confirmation', function () { confirmation_email($(this).data("sale_id")); });
        $(document).on('click', '#send_electronic_invoice_email', function () { send_electronic_invoice_email(); });
        $(document).on('click', '.change_document_status_cadena', function () { change_document_status_cadena($(this).data('sale_id')); });
        $(document).on('click', '.show_pending_files', function() { show_pending_files(); });
        $(document).on('change', '#biller_id', function() { loadDocumentsType($(this)); loadWarehouses($(this)); });
        $(document).on('change', '#date_records_filter_dh', function() { showHidefilterContent($(this)) });

        $('[data-toggle="tooltip"]').tooltip();

        $(document).on('click', '.sledit', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });

        $(document).on('click', '.slduplicate', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });


        $('#client').val('<?= $client ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#confirmation_email_modal').on('hidden.bs.modal', function (e) {
            $("#confirmation_email_modal #send_electronic_invoice_email").html('Enviar').attr('disabled', false);
        })
    });

    function removeSlls()
    {
        if (localStorage.getItem('remove_slls')) {
            if (localStorage.getItem('slitems')) { localStorage.removeItem('slitems'); }
            if (localStorage.getItem('sldiscount')) { localStorage.removeItem('sldiscount'); }
            if (localStorage.getItem('sltax2')) { localStorage.removeItem('sltax2'); }
            if (localStorage.getItem('slref')) { localStorage.removeItem('slref'); }
            if (localStorage.getItem('slshipping')) { localStorage.removeItem('slshipping'); }
            if (localStorage.getItem('slwarehouse')) { localStorage.removeItem('slwarehouse'); }
            if (localStorage.getItem('slnote')) { localStorage.removeItem('slnote'); }
            if (localStorage.getItem('slinnote')) { localStorage.removeItem('slinnote'); }
            if (localStorage.getItem('slcustomer')) { localStorage.removeItem('slcustomer'); }
            if (localStorage.getItem('slbiller')) {localStorage.removeItem('slbiller'); }
            if (localStorage.getItem('slseller')) { localStorage.removeItem('slseller'); }
            if (localStorage.getItem('slcurrency')) { localStorage.removeItem('slcurrency'); }
            if (localStorage.getItem('sldate')) { localStorage.removeItem('sldate'); }
            if (localStorage.getItem('slsale_status')) { localStorage.removeItem('slsale_status'); }
            if (localStorage.getItem('slpayment_status')) { localStorage.removeItem('slpayment_status'); }
            if (localStorage.getItem('paid_by')) { localStorage.removeItem('paid_by'); }
            if (localStorage.getItem('amount_1')) { localStorage.removeItem('amount_1'); }
            if (localStorage.getItem('paid_by_1')) { localStorage.removeItem('paid_by_1'); }
            if (localStorage.getItem('pcc_holder_1')) { localStorage.removeItem('pcc_holder_1'); }
            if (localStorage.getItem('pcc_type_1')) { localStorage.removeItem('pcc_type_1'); }
            if (localStorage.getItem('pcc_month_1')) { localStorage.removeItem('pcc_month_1'); }
            if (localStorage.getItem('pcc_year_1')) { localStorage.removeItem('pcc_year_1'); }
            if (localStorage.getItem('pcc_no_1')) { localStorage.removeItem('pcc_no_1'); }
            if (localStorage.getItem('cheque_no_1')) { localStorage.removeItem('cheque_no_1'); }
            if (localStorage.getItem('othercurrency')) { localStorage.removeItem('othercurrency'); }
            if (localStorage.getItem('othercurrencytrm')) { localStorage.removeItem('othercurrencytrm'); }
            if (localStorage.getItem('slpayment_term')) { localStorage.removeItem('slpayment_term'); }
            if (localStorage.getItem('slordersale')) { localStorage.removeItem('slordersale'); }
            if (localStorage.getItem('aiu_management')) { localStorage.removeItem('aiu_management'); }

            localStorage.removeItem('remove_slls')
        }

        <?php if ($this->session->userdata('remove_slls')) : ?>
            if (localStorage.getItem('slitems')) { localStorage.removeItem('slitems'); }
            if (localStorage.getItem('sldiscount')) { localStorage.removeItem('sldiscount'); }
            if (localStorage.getItem('sltax2')) { localStorage.removeItem('sltax2'); }
            if (localStorage.getItem('slref')) { localStorage.removeItem('slref'); }
            if (localStorage.getItem('slshipping')) { localStorage.removeItem('slshipping'); }
            if (localStorage.getItem('slwarehouse')) { localStorage.removeItem('slwarehouse'); }
            if (localStorage.getItem('slnote')) { localStorage.removeItem('slnote'); }
            if (localStorage.getItem('slinnote')) { localStorage.removeItem('slinnote'); }
            if (localStorage.getItem('slcustomer')) { localStorage.removeItem('slcustomer'); }
            if (localStorage.getItem('slbiller')) { localStorage.removeItem('slbiller'); }
            if (localStorage.getItem('slseller')) { localStorage.removeItem('slseller'); }
            if (localStorage.getItem('slcurrency')) { localStorage.removeItem('slcurrency'); }
            if (localStorage.getItem('sldate')) { localStorage.removeItem('sldate'); }
            if (localStorage.getItem('slsale_status')) { localStorage.removeItem('slsale_status'); }
            if (localStorage.getItem('slpayment_status')) { localStorage.removeItem('slpayment_status'); }
            if (localStorage.getItem('paid_by')) { localStorage.removeItem('paid_by'); }
            if (localStorage.getItem('amount_1')) { localStorage.removeItem('amount_1'); }
            if (localStorage.getItem('paid_by_1')) { localStorage.removeItem('paid_by_1'); }
            if (localStorage.getItem('pcc_holder_1')) { localStorage.removeItem('pcc_holder_1'); }
            if (localStorage.getItem('pcc_type_1')) { localStorage.removeItem('pcc_type_1'); }
            if (localStorage.getItem('pcc_month_1')) { localStorage.removeItem('pcc_month_1'); }
            if (localStorage.getItem('pcc_year_1')) { localStorage.removeItem('pcc_year_1'); }
            if (localStorage.getItem('pcc_no_1')) { localStorage.removeItem('pcc_no_1'); }
            if (localStorage.getItem('cheque_no_1')) { localStorage.removeItem('cheque_no_1'); }
            if (localStorage.getItem('othercurrency')) { localStorage.removeItem('othercurrency'); }
            if (localStorage.getItem('othercurrencytrm')) { localStorage.removeItem('othercurrencytrm'); }
            if (localStorage.getItem('slpayment_term')) { localStorage.removeItem('slpayment_term'); }
            if (localStorage.getItem('slordersale')) { localStorage.removeItem('slordersale'); }
            if (localStorage.getItem('aiu_management')) { localStorage.removeItem('aiu_management'); }
            if (localStorage.getItem('slretenciones')) { localStorage.removeItem('slretenciones'); }

            <?php $this->sma->unset_data('remove_slls'); ?>
        <?php endif ?>
    }

    function loadDataTables()
    {
        var _tabFilterFill = false;
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        oTable = $('#SLData').dataTable({
            aaSorting: [[1, "desc"]],
            aLengthMenu: nums,
            iDisplayLength:  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?=admin_url('sales/getSalesFE' . ($warehouse_id ? '/' . $warehouse_id : '') .'?v=1'. ($this->input->get('shop') ? '&shop='.$this->input->get('shop') : ''). ($this->input->get('attachment') ? '&attachment='.$this->input->get('attachment') : ''). ($this->input->get('delivery') ? '&delivery='.$this->input->get('delivery') : '')/*.'&dias_vencimiento='.(isset($dias_vencimiento) ? $dias_vencimiento : '')*/); ?>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                }, {
                    "name": "date_records_filter",
                    "value": $('#date_records_filter').val()
                }, {
                    "name": "biller_id",
                    "value": $('#biller_id').val()
                }, {
                    "name": "document_type",
                    "value": $('#document_type').val()
                }, {
                    "name": "sale_origin",
                    "value": $('#sale_origin').val()
                }, {
                    "name": "campaign",
                    "value": $('#campaign').val()
                }, {
                    "name": "seller",
                    "value": $('#seller').val()
                }, {
                    "name": "warehouse_id",
                    "value": $('#warehouse_id').val()
                }, {
                    "name": "payment_status",
                    "value": $('#payment_status').val()
                }, {
                    "name": "payment_method",
                    "value": $('#payment_method').val()
                }, {
                    "name" : "fe_aceptado",
                    "value" : $('#fe_aceptado').val()
                }, {
                    "name" : "fe_correo_enviado",
                    "value" : $('#fe_correo_enviado').val()
                }, {
                    "name" : "user",
                    "value" : $('#user').val()
                }, {
                    "name" : "client",
                    "value" : $('#client').val()
                }, {
                    "name": "start_date",
                    "value": "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
                }, {
                    "name": "end_date",
                    "value": "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>"
                }, {
                    "name": "optionFilter",
                    "value": option_filter
                });

                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];

                nRow.setAttribute('data-return-id', aData[13]);
                nRow.className = "invoice_link re"+aData[13];

                return nRow;
            },
            aoColumns: [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fld},
                null,
                null,
                null,
                null,
                null,
                null,
                {"mRender": row_status},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": pay_status},
                {"bSortable": false,"mRender": attachment},
                {"bVisible": false},
                {
                    mRender:  function ( data, type, row ) {
                        return aceptedDIAN(data, row[17], row[18], row[0]);
                    }
                },
                {"mRender": sended_email},
                {"bVisible": false},
                {"bVisible": false},
                {"bVisible": false},
                {"bVisible": false},
                {"bVisible": false},
                {
                    mRender: function (data, type, row) {
                        let acceptedDian = row[15]
                        let saleId = row[0]
                        let documentType = row[21]
                        let saleStatus = row[8]
                        let electronicDocument = row[20]
                        let dateInv = row[1]

                        return renderActionsButton(data, acceptedDian, saleId, documentType, saleStatus, electronicDocument, dateInv);
                    },
                    bSortable: false,
                    sWidth: '65px',
                    className: 'text-center'
                }
            ],
            fnDrawCallback: function (oSettings) {
                if (!_tabFilterFill) {
                    $('.additionalControlsContainer').html('<div class="wizard">' +
                        '<div class="steps clearfix">' +
                            '<ul role="tablist">' +
                                '<li role="tab" class="first current index_list" aria-disabled="false"  aria-selected="true" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="all" href="#">'+
                                        '<span class="all_span">0</span><br><?= lang('allsf') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" style="width:16.6%;">' +
                                    '<a class="wizard_index_step" data-optionfilter="returns" href="#" aria-controls="edit_biller_form-p-0">' +
                                        '<span class="returns_span">0</span><br><?= lang('returns') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="notApproved" href="#">' +
                                        '<span class="notApproved_span">0</span><br><?= lang('notApproved') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingPayment" href="#">'+
                                        '<span class="pendingPayment_span">0</span><br><?= lang('pendingPayment') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingDian" href="#">'+
                                        '<span class="pendingDian_span">0</span><br><?= lang('pendingDian') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingDispatch" href="#">'+
                                        '<span class="pendingDispatch_span">0</span><br><?= lang('pendingDispatch') ?>' +
                                    '</a>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>').addClass('col-without-padding');

                    _tabFilterFill = true;
                }

                $('.actionsButtonContainer').html(`<a href="<?= admin_url('sales/add') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>
                <div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="#" id="excel" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="combine" data-action="combine">
                                <i class="fa fa-file-pdf-o"></i> <?=lang('combine_to_pdf')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="post_sale" data-action="post_sale">
                                <i class="fa fa-file-pdf-o"></i> <?= lang('post_sale') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <?php if ($this->Owner) { ?>
                            <li>
                                <a href="<?= admin_url('sales/sincronizarDevolucionesVentas') ?>">
                                    <i class="fa fa-sync"></i> Sincronizar Devoluciones
                                </a>
                            </li>
                        <?php } ?>
                        <li>
                            <a href="#" id="mark_printed_sale" data-action="mark_printed_sale">
                                <i class="fa-solid fa-check-to-slot"></i> <?= lang('mark_printed_sale') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="mark_all_printed_sale" data-action="mark_all_printed_sale" data-electronic="1">
                                <i class="fa-solid fa-check-to-slot"></i> <?= lang('mark_all_printed_sale') ?>
                            </a>
                        </li>
                    </ul>
                </div>`);

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });

                loadDataTabFilters();
            }
        });
    }

    function possible_solution_currency(sale_id)
    {
        $.ajax({
            url: '<?= admin_url("sales/possible_solution_currency") ?>',
            type: 'GET',
            dataType: 'JSON',
            data: {
                'sale_id': sale_id
            },
        })
        .done(function(data) {
            if (data.status == true) {
                command: toastr.success(data.message, '<?= lang("toast_success_title") ?>', {onHidden: function() {
                        $('#error_modal').modal('hide');
                        window.location.href = '<?= admin_url("sales/resend_electronic_document/") ?>'+sale_id;
                    }
                });
            }else{
                command: toastr.error(data.message, '<?= lang("toast_error_title") ?>', {onHidden: function() { console.log('data.responseText') }});
            }
        })
        .fail(function(data) {
            command: toastr.error('<?= lang("ajax_error") ?>', '<?= lang("toast_error_title") ?>', {onHidden: function() { console.log(data.responseText) }});
        });
    }

    function change_document_status_cadena(sale_id)
    {
        window.location.href = site.base_url +'sales/change_status_accepted_DIAN/'+sale_id;

    }

    function resend_electronic_document(event, element)
    {
        event.preventDefault();

        swal({
            title: "Envío de Documento Electrónico.",
            text: "¿Está seguro de enviar el documento electrónico?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: 'Enviando documento electrónico...',
                text: 'Por favor, espere un momento.',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            var document_id = element.data('document_id');
            var route_resend_invoice =  element.find('a').attr('href');

            window.location.href = route_resend_invoice;
        });

        // $.ajax({
        //     url: '<?= admin_url("sales/validate_is_contingency_invoice"); ?>',
        //     type: 'POST',
        //     dataType: 'HTML',
        //     data: {
        //         '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
        //         'document_id': document_id
        //     },
        // })
        // .done(function(data) {
        //     if (data == '<?= YES ?>') {
        //         open_confirmation_modal_sending_contingency_invoice(route_resend_invoice);
        //     } else {
        //         window.location.href = route_resend_invoice;
        //     }
        // })
        // .fail(function(data) {
        //     console.log(data.responseText);
        // });
    }

    function change_date_electronic_document(event, element)
    {
        event.preventDefault();
        swal({
            title: "Cambio de fecha de Documento Electrónico.",
            text: "¿Está seguro de cambiar y reenviar el documento electrónico?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: true
        }, function () {
            const wrapper = document.getElementById('wrapper');
            wrapper.classList.add('wrapper-disabled');
            Swal.fire({
                title: 'Enviando documento electrónico...',
                html: '<div class="dots-container"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>Por favor, espere un momento.',
                icon: 'warning',
                showCancelButton: false,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: {
                    popup: 'swal2-popup'
                },
                backdrop: true
            });
            var document_id = element.data('document_id');
            var route_resend_invoice =  element.find('a').attr('href');
            window.location.href = route_resend_invoice;
        });
    }


    function open_confirmation_modal_sending_contingency_invoice(route_resend_invoice)
    {
        $('#confirmation_modal').modal('show');

        $('#confirmation_modal #confirm_send_document_id').val(route_resend_invoice);
    }

    function confirm_send_document_button(route_resend_invoice) {
        window.location.href = route_resend_invoice;
    }

    function confirmation_email(sale_id)
    {
        $.ajax({
            url: '<?= admin_url("sales/get_email_customer_by_sale_id"); ?>',
            type: 'POST',
            dataType: 'JSON',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'sale_id': sale_id
            },
        })
        .done(function(data) {
            if (data != '') {
                $('#confirmation_email_modal #confirmation_email').val(data);
                $("#confirmation_email_modal #send_electronic_invoice_email").val(sale_id);

                $('#confirmation_email_modal').modal({backdrop: 'static'});
            }
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function send_electronic_invoice_email()
    {
        sale_id = $("#confirmation_email_modal #send_electronic_invoice_email").val();
        email = $("#confirmation_email_modal #confirmation_email").val();

        if (!(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))) {
            Command: toastr.error('Por favor ingrese un dirección de correo electrónico válido', '<?= lang('toast_error_title') ?>', {onHidden: function() { $('#confirmation_email').focus(); }});
            return false;
        }

        $("#confirmation_email_modal #send_electronic_invoice_email").html('<i class="fa fa-spinner fa-pulse fa-fw"></i> enviando...').attr('disabled', true);

        window.location.href='<?= admin_url("sales/send_electronic_invoice_email"); ?>'+'/'+sale_id+'/'+email;
    }

    function calcularMinutos(start_date, end_date)
    {
        var fecha1 = new Date(start_date);
        var fecha2 = new Date(end_date);
        var diff = fecha2.getTime() - fecha1.getTime();
        var minutos = diff/(1000 * 60);
        return minutos;
    }

    // function setFilterText(){

    //     // var reference_text = $('#detal_reference_no option:selected').data('dtprefix');
    //     var biller_text = $('#biller option:selected').text();
    //     var customer_text = $('#customer').select2('data') !== null ? $('#customer').select2('data').text : '';
    //     var sale_status_text = $('#sale_status option:selected').text();
    //     var payment_status_text = $('#payment_status option:selected').text();
    //     var start_date_text = $('#start_date_dh').val();
    //     var end_date_text = $('#end_date_dh').val();
    //     var fe_aceptado = $('#fe_aceptado option:selected').text();
    //     var fe_correo_enviado = $('#fe_correo_enviado option:selected').text();
    //     var fe_recibido = $('#fe_recibido option:selected').text();
    //     var text = "Filtros configurados : ";
    //     coma = false;
    //     // if (detal_reference_no != '' && detal_reference_no !== undefined) {
    //     //     text+=" Tipo documento ("+reference_text+")";
    //     //     coma = true;
    //     // }
    //     if (biller != '' && biller !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Sucursal ("+biller_text+")";
    //         coma = true;
    //     }
    //     if (customer != '' && customer !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Cliente ("+customer_text+")";
    //         coma = true;
    //     }
    //     if (sale_status != '' && sale_status !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Estado de venta ("+sale_status_text+")";
    //         coma = true;
    //     }
    //     if (payment_status != '' && payment_status !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Estado de pago ("+payment_status_text+")";
    //         coma = true;
    //     }
    //     if (start_date != '' && start_date !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Fecha de inicio ("+start_date_text+")";
    //         coma = true;
    //     }
    //     if (end_date != '' && end_date !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Fecha final ("+end_date_text+")";
    //         coma = true;
    //     }
    //     if (fe_aceptado != '' && fe_aceptado !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Aceptado ("+fe_aceptado+")";
    //         coma = true;
    //     }
    //     if (fe_correo_enviado != '' && fe_correo_enviado !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Correo enviado ("+fe_correo_enviado+")";
    //         coma = true;
    //     }
    //     if (fe_recibido != '' && fe_recibido !== undefined) {
    //         text+= coma ? "," : "";
    //         text+=" Recibido ("+fe_recibido+")";
    //         coma = true;
    //     }
    //     console.log('txt '+text);
    //     $('.text_filter').html(text);

    // }

    function show_pending_files()
    {
        $('#fe_aceptado').select2('val', '1');

        $('#salesFilterSubmit').trigger('click');
    }

    function loadDocumentsType(element)
    {
        var billerId = element.val();
        var options = '<option value=""><?= $this->lang->line('allsf') ?></option>';

        $.ajax({
            type: "post",
            url: "<?= admin_url("sales/getDocumentTypes") ?>",
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'billerId'      : element.val(),
                'asynchronous'  : 1,
                'modules'       : [2, 4, 27]
            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(documentType => {
                        options += '<option value="'+documentType.id+'">'+documentType.sales_prefix+' - '+documentType.nombre+'</option>';
                    });
                }

                $('#document_type').html(options);
            }
        });
    }

    function loadWarehouses(element) {
        if (element.val()) {
            var defaultwh = element.children("option:selected").data('defaultwh');
            $('#warehouse_id').select2('val', defaultwh).trigger('change');
        } else {
            $('#warehouse_id option').each(function(index, option) {
                $(option).attr('disabled', false);
            });
            $('#warehouse_id').select2('val', '').trigger('change');
        }
    };

    function loadDataTabFilters() {
        $.ajax({
            type: 'post',
            url: '<?= admin_url('sales/getSalesFE'); ?>',
            data: {
                tabFilterData: 1,
                user: $('#user').val(),
                seller: $('#seller').val(),
                optionFilter: option_filter,
                client: $('#client').val(),
                campaign: $('#campaign').val(),
                biller_id: $('#biller_id').val(),
                sale_origin: $('#sale_origin').val(),
                fe_aceptado: $('#fe_aceptado').val(),
                warehouse_id: $('#warehouse_id').val(),
                document_type: $('#document_type').val(),
                payment_status: $('#payment_status').val(),
                payment_method: $('#payment_method').val(),
                fe_correo_enviado: $('#fe_correo_enviado').val(),
                date_records_filter: $('#date_records_filter').val(),
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                end_date: "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>",
                start_date: "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
            },
            dataType: 'json'
        }).done(function(data) {
            $('.all_span').text(data.all);
            $('.notApproved_span').text(data.notApproved);
            $('.pendingDian_span').text(data.pendingDian);
            $('.pendingDispatch_span').text(data.pendingDispatch);
            $('.pendingPayment_span').text(data.pendingPayment);
            $('.returns_span').text(data.returns);
        });
    }

    function showHidefilterContent(element)
    {
        var advancedFiltersContainer = '<?= $advancedFiltersContainer ? 1 : 0 ?>';

        var dateFilter = element.val();
        if (dateFilter != '') {
            if (dateFilter == 5) {
                $('#advancedFiltersContainer').fadeIn();
                $('.new-button-container .collapse-link').html('<i class="fa fa-lg fa-chevron-up"></i>');
            }
        }
    }

    function openErrorModal(documentId)
    {
        $.ajax({
            type: "post",
            url: "<?= admin_url('sales/getMessage') ?>",
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'documentId': documentId
            },
            dataType: "json",
            success: function (response) {
                swal({
                    title: response.title,
                    text: response.text,
                    type: 'info',
                    showCancelButton: true,
                    showConfirmButton: false,
                    cancelButtonText: "Ok"
                });
            }
        });
    }
</script>