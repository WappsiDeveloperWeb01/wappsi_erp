<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title" style="min-height: 0px; padding: 10px 20px 7px;">
                    <h3 style="margin-bottom: 5px;" id="variations_graph">Cola de impresión</h3>
                </div>
                <div class="ibox-content" style="padding: 1px 10px 7px;">
                	<div id="print_queue_container">
                        <table class="table table-condensed table-hover">
                            <thead>
                                <th class="text-center">Fecha</th>
                                <th>Referencia</th>
                                <th>Sucursal</th>
                                <th>cliente</th>
                                <th class="text-right">Total</th>
                                <th class="text-center">Estado</th>
                                <th class="text-center"><i class="fas fa-print"></i></th>
                            </thead>
                            <tbody>
                		        <?php if (!empty($unprinted_documents)): ?>
			                		<?php foreach ($unprinted_documents as $key => $document): ?>
                                        <?php if (($document->factura_electronica == 1 && !empty($document->cufe)) || ($document->factura_electronica == 0)) : ?>
                                            <tr>
                                                <td class="text-center"><?= $document->date; ?></td>
                                                <td><?= $document->reference_no; ?></td>
                                                <td><?= $document->biller; ?></td>
                                                <td><?= $document->customer; ?></td>
                                                <td class="text-right">
                                                    <?= number_format($document->grand_total, 2, ".", ""); ?>
                                                </td>
                                                <td class="text-center" id="status<?= $document->id ?>">
                                                    <?php if ($key == 0 && $document->printed == 0): ?>
                                                        <span class="label label-warning">Procesando...</span>
                                                    <?php else: ?>
                                                        <?php if ($document->printed == 0): ?>
                                                            <span class="label label-danger">En cola</span>
                                                        <?php else: ?>
                                                            <span class="label label-primary">Impreso</span>
                                                        <?php endif ?>
                                                    <?php endif ?>
                                                </td>
                                                <td class="text-center" id="toggle<?= $document->id ?>">
                                                    <?php if ($document->printed == 0): ?>
                                                        <i class="fas fa-toggle-off fa-2x text-default pointer printFormat" 
                                                        data-id="<?= $document->id ?>"
                                                        data-toggle-second="tooltip" data-placement="top" title="Marcar como impreso"></i>
                                                    <?php else: ?>
                                                        <i class="fas fa-toggle-on fa-2x text-success"></i>
                                                    <?php endif ?>
                                                </td>
                                            </tr>
                                        <?php endif ?>
                                    <?php endforeach ?>
                		        <?php endif ?>
                            </tbody>
                            <tfoot>
                                <th class="text-center">Fecha</th>
                                <th>Referencia</th>
                                <th>Sucursal</th>
                                <th>cliente</th>
                                <th class="text-right">Total</th>
                                <th class="text-center">Estado</th>
                                <th class="text-center"><i class="fas fa-print"></i></th>
                            </tfoot>
                        </table>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
        let documentId = '<?= $invoiceToPrint ?>'

        $('[data-toggle-second="tooltip"]').tooltip();
		
        printFormat(documentId)

        $(document).on('click', '.printFormat', function () {            
            markPrinted($(this).data('id'))
        })
	})

	function printFormat(document_id)
	{
        if (document_id != '') {
            $.ajax({
                url: '<?= admin_url("sales/printFormat"); ?>',
                type: 'POST',
                dataType: 'HTML',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'document_id': document_id
                }
            })
            .done(function(data) {
                imprSelec(data, document_id)
            })
            .fail(function(data) {
                console.log(data.responseText)
            });
        } else {
            setTimeout(function() {
                show_print_queue()
            }, 6500)
        }
	}

	function imprSelec(page, document_id) 
    {
	  	let ventimp = window.open('')
	  	ventimp.document.write(page)

  		setTimeout(function() {
  			ventimp.print()
  			ventimp.close()

            show_print_queue()            
  		}, 2000);
	}

	function show_print_queue()
	{
		$.ajax({
			url: '<?= admin_url("sales/get_unprinted_documents"); ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>'
			}
		})
		.done(function(response) {
			let tableBodyString = ''
            const documents = response.unprinted_documents
            const invoiceToPrint = response.invoiceToPrint

			$(documents).each(function (i, document) {
                if ((document.factura_electronica == 1 && document.cufe != '') || (document.factura_electronica == 0)) {
                    if (i == 0 && document.printed == 0) {
                        status_tag = '<span class="label label-warning">Procesando...</span>'
                        toggle = `<i class="fas fa-toggle-off fa-2x text-default pointer printFormat" 
                                    data-id="${document.id}"
                                    data-toggle-second="tooltip" data-placement="top" title="Marcar como impreso"></i>`
                    } else {
                        if (document.printed == 0) {
                            status_tag = '<span class="label label-danger">En cola</span>'
                            toggle = `<i class="fas fa-toggle-off fa-2x text-default pointer printFormat" 
                                    data-id="${document.id}"
                                    data-toggle-second="tooltip" data-placement="top" title="Marcar como impreso"></i>`
                        } else {
                            status_tag = '<span class="label label-primary">Impreso</span>'
                            toggle = `<i class="fas fa-toggle-on fa-2x text-success"></i>`
                        }
                    }

                    tableBodyString += `<tr>
                        <td class="text-center"> ${document.date} </td>
                        <td> ${document.reference_no} </td>
                        <td> ${document.biller} </td>
                        <td> ${document.customer} </td>
                        <td class="text-right"> ${formatDecimals(document.grand_total)} </td>
                        <td class="text-center" id="status${document.id}"> ${status_tag} </td>
                        <td class="text-center" id="toggle${document.id}"> ${toggle} </td>
                    </tr>`;
                }                
			});

			var table_string = `<table class="table table-condensed table-hover">
                <thead>
                    <th class="text-center">Fecha</th>
                    <th>Referencia</th>
                    <th>Sucursal</th>
                    <th>cliente</th>
                    <th class="text-right">Total</th>
                    <th class="text-center">Estado</th>
                    <th class="text-center">
                        <i class="fas fa-print"></i>
                    </th>
                </thead>
                <tbody>
                    ${tableBodyString}
                </tbody>
                <tfoot>
                    <th class="text-center">Fecha</th>
                    <th>Referencia</th>
                    <th>Sucursal</th>
                    <th>cliente</th>
                    <th class="text-right">Total</th>
                    <th class="text-center">Estado</th>
                    <th class="text-center">
                        <i class="fas fa-print"></i>
                    </th>
                </tfoot>
            </table>`;

			$('#print_queue_container').html(table_string)

            setTimeout(function() {
                printFormat(invoiceToPrint)
            }, 1000)
		})
		.fail(function(data) {
			console.log(data.responseText)
		});
	}
    
    function markPrinted(documentId)
    {
        swal({
            title: 'Marcando como Impreso',
            text: 'Por favor, espere un momento.',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });

        $.ajax({
            url: '<?= admin_url("sales/markPrintedAjax"); ?>',
            type: 'POST',
            dataType: 'HTML',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'documentId': documentId
            }
        })
        .done(function(data) {
            if (data == 'true') {
                $(`#status${documentId}`).html('<span class="label label-primary">Impreso</span>')
                $(`#toggle${documentId}`).html('<i class="fas fa-toggle-on fa-2x text-success"></i>')

                swal.close()
            }
        })
        .fail(function(data) {
            console.log(data.responseText)
        })
    }
</script>