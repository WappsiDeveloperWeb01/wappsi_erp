<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
    });
</script>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Actualizar información de facturación a clientes</h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12 ">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Escoger cliente para generar facturación
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <form id="invoicing_form" method="post">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("customer", "slcustomer"); ?>
                                                    <?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="slcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip"'); ?>
                                                    <em class="text-danger txt-error" style="display: none;"></em>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-primary" id="send" type="button">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->
<script language="javascript">
    $(document).ready(function () {
        $("#invoicing_form").validate({
            ignore: []
        });
        $('#slcustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('[data-toggle="tooltip"]').tooltip();
    });
    $(document).on('click', '#send', function(){
        if ($('#invoicing_form').valid()) {
            location.href = site.base_url+"wappsi_invoicing/download_customer_invoicing/"+$('#slcustomer').val();
        }
    });
    $(document).on('change', '#slcustomer', function(){
        if ($('#invoicing_form').valid()) {
            $.ajax({
                url : site.base_url+"wappsi_invoicing/validate_customer_database_valid/"+$('#slcustomer').val(),
                dataType : 'JSON',
            }).done(function(data){
                if (data.valid) {
                    command: toastr.success('El cliente seleccionado si es válido para actualizarle la facturación.', '¡Cliente válido!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#send').prop('disabled', false);
                } else {
                    command: toastr.error('El cliente seleccionado no es válido para actualizarle la facturación.', '¡Cliente no configurado!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#slcustomer').select2('val', '');
                    $('#send').prop('disabled', true);
                }
            });
        }
    });
</script>

