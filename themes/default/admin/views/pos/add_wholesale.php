<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<?php $this->session->unset_userdata('redirect_pos') ?>
<style type="text/css">
	.txt-error {
		color: #ed3439;
	}
</style>
<script type="text/javascript">if(parent.frames.length !== 0){top.location = '<?=admin_url('pos')?>';}</script>
	<script type="text/javascript">
		localStorage.removeItem('pospaid_by');
		var is_order = JSON.parse('<?= isset($order) ? "true" : "false" ?>');
		
		var is_quote = false;
	    <?php if (isset($quote_id)) { ?>
			is_quote = true;
	        if (localStorage.getItem('pos_wholesale_keep_prices_quote_id')) {
	            if (localStorage.getItem('pos_wholesale_keep_prices_quote_id') != "<?= $quote_id ?>") {
	                localStorage.removeItem('pos_wholesale_keep_prices');
	                localStorage.removeItem('pos_wholesale_keep_prices_quote_id');
	            }
	        } else {
	            localStorage.removeItem('pos_wholesale_keep_prices');
	            localStorage.removeItem('pos_wholesale_keep_prices_quote_id');
	        }
	    <?php } ?>
		count_suspended_bills = "<?= $count_suspended_bills ?>";
		var is_suspend_sale = false;
		<?php if ($suspend_sale) {?>
			is_suspend_sale = true;
		<?php } ?>
	</script>
<link rel="stylesheet" href="<?=$assets?>pos/css/posajax_wholesale.css" type="text/css"/>
<link rel="stylesheet" href="<?=$assets?>pos/css/print.css" type="text/css" media="print"/>
<?php $attrib = array('role' => 'form', 'id' => 'pos-sale-form', 'action' => admin_url('admin/pos'));
echo admin_form_open("pos", $attrib);?>
<input type="hidden" name="address_id" id="address_id">
<input type="hidden" name="except_category_taxes" id="except_category_taxes">
<input type="hidden" name="tax_exempt_customer" id="tax_exempt_customer">
<input type="hidden" name="type_pos" id="type_pos" value="2">
<div class="wrapper wrapper-content  animated fadeInRight no-printable">
	<div class="ibox float-e-margins border-bottom">
		<div class="ibox-content pos_area">
			<div class="row">
	            <div class="col-md-3 ">
					<div class="form-group">
		            	<?= lang('date', 'posdate') ?>
		            	<input type="datetime" name="date" id="posdate" class="form-control" value="<?= date('Y-m-d H:i:s') ?>" readonly>
		            </div>
				</div>
				<div class="col-md-3 ">
					<div class="form-group">
						<?= lang('biller', 'posbiller') ?>
						<?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
								<?php
								foreach ($billers as $biller) {
									$btest = ($biller->company && $biller->company != '-' ? $biller->company : $biller->name);
									$bl[$biller->id] = $btest;
									$posbillers[] = array('logo' => $biller->logo, 'company' => $btest);
									if ($biller->id == $pos_settings->default_biller) {
										$posbiller = array('logo' => $biller->logo, 'company' => $btest);
									}
								}
							if (!isset($posbiller)) {
									$posbiller = $posbillers[0];
							}
								?>
								<select name="posbiller" class="form-control" id="posbiller">
									<?php foreach ($billers as $biller): ?>
										<option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-documenttypedefault="<?= $biller->pos_document_type_default ?>"  <?= $biller->id == $this->Settings->default_biller ? 'selected="selected"' : '' ?>><?= $biller->company ?></option>
									<?php endforeach ?>
								</select>
						<?php } else {
							foreach ($billers as $biller) {
								$btest = ($biller->company && $biller->company != '-' ? $biller->company : $biller->name);
								$posbillers[] = array('logo' => $biller->logo, 'company' => $btest);
								if (isset($this->session->userdata('billers_id_assigned')[$biller->id])) {
									$posbiller = array('logo' => $biller->logo, 'company' => $btest);
									$posbiller_id = $biller->id;
									$posbillerdata[] = $biller;
								}
							}
							?>
								<select name="posbiller" class="form-control" id="posbiller" style="display: none;">
									<?php foreach ($posbillerdata as $biller): ?>
										<option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-documenttypedefault="<?= $biller->pos_document_type_default ?>" <?= $biller->id == $this->Settings->default_biller ? 'selected="selected"' : '' ?>><?= $biller->company ?></option>
									<?php endforeach ?>
								</select>
						<?php
						}
						?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
		              <?= lang('document_type', 'doc_type_id') ?>
		              <select name="document_type_id" class="form-control" id="doc_type_id" required="required"></select>
		              <span class="text-danger" id="doc_type_error" style="display: none;">* No se ha definido un tipo de documento, por favor, seleccione uno.</span>
		            </div>
	            </div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="pos-label-input"><?= lang('warehouse') ?></label>
													<?php

													$wh[''] = '';
																foreach ($warehouses as $warehouse) {
																	$wh[$warehouse->id] = $warehouse->name;
																}

													 ?>
													<?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) {
															?>
															<?php
																echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="poswarehouse" class="form-control pos-input-tip" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" ');
																?>
														<?php } else { ?>
															<select name="warehouse" id="poswarehouse" class="form-control">
																<option value="<?= $this->session->userdata('warehouse_id') ?>"><?= $wh[$this->session->userdata('warehouse_id')] ?></option>
															</select>
															<?php
														}
														?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="form-group">
						<?= lang('customer', 'poscustomer') ?>
						<div class="input-group">
							<?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="poscustomer" data-placeholder="'. $this->lang->line("select") .' '. $this->lang->line("customer") .'" required="required" class="form-control pos-input-tip" style="width:100%;"');
							?>
							<div class="input-group-addon no-print" style="padding: 2px 8px; border-left: 0;">
								<a href="#" id="set-customer-branch" class="external" data-toggle="modal" data-target="#cbModal">
									<i class="fa fa-location-arrow" id="addIcon" style="font-size: 1.2em;"></i>
								</a>
							</div>
							<div class="input-group-addon no-print" style="padding: 2px 8px; border-left: 0;">
								<a href="#" id="toogle-customer-read-attr" class="external">
									<i class="fas fa-pen-square" id="addIcon" style="font-size: 1.2em;"></i>
								</a>
							</div>
							<div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
								<a id="view-customer">

									<i class="fas fa-info-circle" id="addIcon" style="font-size: 1.3em;"></i>
								</a>
							</div>
							<?php if ($Owner || $Admin || $GP['customers-add']) { ?>
								<div class="input-group-addon no-print" style="padding: 2px 8px;">
									<a href="<?=admin_url('customers/add');?>" id="add-customer" class="external" data-toggle="modal" data-target="#myModal">
										<i class="fa fa-plus-square" id="addIcon" style="font-size: 1.5em;"></i>
									</a>
								</div>
							<?php } ?>
						</div>
						<div class="txt-error"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group" id="ui">
						<?= lang('add_item', 'add_item') ?>
						<?php if ($Owner || $Admin || $GP['products-add']) { ?>
							<div class="input-group">
							<?php } ?>
							<?php echo form_input('add_item', '', 'class="form-control pos-tip" id="add_item" data-placement="top" data-trigger="focus" placeholder="' . $this->lang->line("search_product_by_name_code") . '" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
							<?php if ($Owner || $Admin || $GP['products-add']) { ?>
								<!-- <div class="input-group-addon" style="padding: 2px 8px;">
									<a href="#" id="addManually">
										<i class="fa fa-plus-square" id="addIcon" style="font-size: 1.5em;"></i>
									</a>
								</div> -->
							<?php } ?>
							<div class="input-group-addon" style="padding: 2px 8px;">
								<a id="deleteProduct">
									<i class="fa fa-times" id="deleteIcon" style="font-size: 1.5em;"></i>
								</a>
							</div>
							<div class="input-group-addon" style="padding: 2px 8px;">
								<a id="searchProduct">
									<i class="fa fa-search" id="searchIcon" style="font-size: 1.5em;"></i>
								</a>
							</div>
							</div>
						<div style="clear:both;"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style="padding:10px 0 10px 0;">
					<table class="table items  table-bordered table-condensed table-hover sortable_table"
					id="posTable" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 5%; text-align: center;">
									<i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
								</th>
								<th width="40%"><?=lang("product");?> <span class="product_number"></span></th>
								<th width="15%"><?=lang("price");?></th>
								<th width="15%"><?=lang("qty");?></th>
								<th width="20%"><?=lang("subtotal");?></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="clearfix"></div>
				<!-- FILA 6 -->
				<!-- <div class="col-md-12 row"> -->
					<!-- IZQUIERDA -->
                   <div class="col-md-4 form-group">
	                   	<?php if ($this->Owner || $this->Admin || ($user_group_name != 'seller' && !$this->sma->keep_seller_from_user())): ?>
	                   		<div class="col-md-12">
	                            <?= lang("seller", "posseller"); ?>
	                            <?php
	                            echo form_dropdown('seller_id', array('0' => lang("select") . ' ' . lang("biller")), (isset($_POST['seller']) ? $_POST['seller'] : ''), 'id="posseller" data-placeholder="' . lang("select") . ' ' . lang("seller") . '" required="required" class="form-control input-tip" style="width:100%;"');
	                            ?>
	                   		</div>
		                <?php else: ?>
		                    <?php if ($this->sma->keep_seller_from_user()): ?>
		                        <input type="hidden" name="seller_id"  value="<?= $this->session->userdata('seller_id') ?>">
		                    <?php else: ?>
		                        <input type="hidden" name="seller_id"  value="<?= $this->session->userdata('company_id') ?>">
		                    <?php endif ?>
		                <?php endif ?>
                   		<div class="col-md-12">
                        	<?= lang('sale_note', 'sale_note') ?>
							<?=form_textarea('sale_note', '', 'id="sale_note" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('sale_note') . '" maxlength="250"');?>
                   		</div>
                    </div>
                    <!-- CENTRO -->
                    <div class="col-md-4"></div>
                   <!--  <div class="col-md-4 form-group">
                        	<?= lang('staff_note', 'staff_note') ?>
							<?=form_textarea('staffnote', '', 'id="staffnote" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('staff_note') . '" maxlength="250"');?>
                    </div> -->
					<!-- DERECHA -->
					<div class="col-md-4">
						<table id="totalTable" class="table" style="margin-top: 15px;">
							<tr>
								<th><?=lang('subtotal');?></th>
								<td class="text-right"><span id="subtotal">0.00</span></td>
							</tr>
							<tr>
								<th><?=lang('tax');?></th>
								<td class="text-right"><span id="ivaamount">0.00</span></td>
							</tr>
							<tr>
								<th><?=lang('total');?></th>
								<td class="text-right"><span id="total">0.00</span></td>
							</tr>
							<tr>
								<th class="selectable" id="pprete">Retenciones</th>
								<td class="text-right"><span id="rete">0.00</span></td>
							</tr>
							<tr>
								<th class="selectable" id="ppdiscount"><?=lang('discount');?></th>
								<td class="text-right"><span id="tds">0.00</span></td>
							</tr>
							<?php if ($this->Settings->default_tax_rate2 != 0): ?>
								<tr>
									<th class="selectable" id="pptax2"><?=lang('order_tax');?></th>
									<td class="text-right"><span id="ttax2">0.00</span></td>
								</tr>
							<?php endif ?>
							<?php if ($this->pos_settings->apply_suggested_tip > 0): ?>
								<!-- <tr>
									<th class="selectable" id="ptip"><?= lang('tip_amount') ?></th>
									<td class="text-right"><span id="tips">0.00</span></td>
								</tr> -->
							<?php endif ?>
							<?php if ($this->pos_settings->apply_suggested_home_delivery_amount > 0): ?>
								<tr>
									<th class="selectable" id="pshipping"><?= lang('shipping') ?></th>
									<td class="text-right"><span id="tship">0.00</span></td>
								</tr>
							<?php endif ?>
							<tr>
								<th><?=lang('total_payable');?></th>
								<td class="text-right"><span id="gtotal">0.00</span></td>
							</tr>
						</table>
					</div>
			</div>
			<div id="botbuttons" class="col-xs-12 text-center" style="padding: 0px !important; position: sticky; top: 100%">
				<input type="hidden" name="biller" id="biller" value="<?= ($Owner || $Admin || !$this->session->userdata('biller_id')) ? $pos_settings->default_biller : $this->session->userdata('biller_id')?>"/>
				<div class="menu-sobre-productos">
					<div class="btn-pos">
						<button type="button" class="btn btn-wappsi-cancelar btn-block" id="reset" style="height:67px; text-align:center; width: 100%; font-size:90%;">
							<i class="fas fa-times"></i><br>Cancelar
						</button>
					</div>
					<div class="btn-pos">
						<button type="button" class="btn btn-wappsi-pausar btn-block" id="suspend" style="height:67px; text-align:center; width: 100%; font-size:90%;">
							<i class="fas fa-pause"></i><br>Pausar
						</button>
					</div>
					<div class="btn-pos">
						<button type="button" class="btn btn-wappsi-pagar btn-block" id="payment" style="height:67px; text-align:center; width: 100%; font-size:90%;">
							<i class='fas fa-hand-holding-usd'></i><br> <?= $this->pos_settings->express_payment ? lang('pay_with_cash') : lang('pay') ?>
						</button>
					</div>
				</div>
			</div>
		</div>
			<div id="">
				<div class="c1">
					<div class="pos">
							<div id="leftdiv">
								<div id="print">
									<div id="left-middle">
										<div id="product-list">
										<div style="clear:both;"></div>
										</div>
									</div>
								<div style="clear:both;"></div>

								<div id="left-bottom">

								</div>
								<div class="clearfix"></div>
						<div style="clear:both; height:5px;"></div>
						<div id="num">
							<div id="icon"></div>
						</div>
						<span id="hidesuspend"></span>
						<input type="hidden" name="pos_note" value="" id="pos_note">
						<input type="hidden" name="staff_note" value="" id="staff_note">
        <div id="payment-con">
          <?php for ($i = 1; $i <= 5; $i++) {?>
            <input type="hidden" name="amount[]" id="amount_val_<?=$i?>" value=""/>
            <input type="hidden" name="balance_amount[]" id="balance_amount_<?=$i?>" value=""/>
                    <input type="hidden" name="due_payment[]" id="due_payment_<?=$i?>" class="due_payment_<?=$i?>" value=""/>
            <input type="hidden" name="paid_by[]" id="paid_by_val_<?=$i?>" value="cash"/>
            <input type="hidden" name="cc_no[]" id="cc_no_val_<?=$i?>" value=""/>
            <input type="hidden" name="paying_gift_card_no[]" id="paying_gift_card_no_val_<?=$i?>" value=""/>
            <input type="hidden" name="cc_holder[]" id="cc_holder_val_<?=$i?>" value=""/>
            <input type="hidden" name="cheque_no[]" id="cheque_no_val_<?=$i?>" value=""/>
            <input type="hidden" name="cc_month[]" id="cc_month_val_<?=$i?>" value=""/>
            <input type="hidden" name="cc_year[]" id="cc_year_val_<?=$i?>" value=""/>
            <input type="hidden" name="cc_type[]" id="cc_type_val_<?=$i?>" value=""/>
            <input type="hidden" name="cc_cvv2[]" id="cc_cvv2_val_<?=$i?>" value=""/>
            <input type="hidden" name="payment_note[]" id="payment_note_val_<?=$i?>" value=""/>
          <?php }
          ?>
        </div>
        <input name="order_tax" type="hidden" value="<?=$suspend_sale ? $suspend_sale->order_tax_id : ($old_sale ? $old_sale->order_tax_id : $Settings->default_tax_rate2);?>" id="postax2">
        <input name="discount" type="hidden" value="<?=$suspend_sale ? $suspend_sale->order_discount_id : ($old_sale ? $old_sale->order_discount_id : '');?>" id="posdiscount">
        <input name="shipping" type="hidden" value="<?=$suspend_sale ? $suspend_sale->shipping : ($old_sale ? $old_sale->shipping :  '0');?>" id="posshipping">
        <input type="hidden" name="rpaidby" id="rpaidby" value="cash" style="display: none;"/>
        <input type="hidden" name="gtotal_rete_amount" id="gtotal_rete_amount" value="0">
        <input type="hidden" name="total_items" id="total_items" value="0" style="display: none;"/>
        <input type="hidden" name="payment_term" id="payment_term">
        <input type="hidden" name="payment_document_type_id" id="payment_document_type_id">
        <input type="submit" id="submit_sale" value="Submit Sale" style="display: none;"/>
        <input type="hidden" name="mean_payment_code_fe" id="mean_payment_code_fe">
        <input type="hidden" name="cost_center_id" id="cost_center_id">
        <?php if (isset($quote_id)): ?>
        	<input type="hidden" name="quote_id" id="quote_id" value="<?= $quote_id ?>">
        <?php endif ?>
        <?php if (isset($sid) || isset($quote)): ?>
        	<input type="hidden" name="sale_origin_reference_no" id="sale_origin_reference_no" value="<?= isset($sid) && $suspend_sale ? $suspend_sale->suspend_note.'('.$suspend_sale->id.')' : $quote->reference_no ?>">
        <?php endif ?>
        <?php if (isset($order)): ?>
        	<input type="hidden" name="order" id="order" value="1">
        <?php endif ?>
      </div>
    </div>
			</div>
				<div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
									class="fa fa-2x">&times;</i></button>
									<h4 class="modal-title" id="rtModalLabel"><?=lang('edit_order_tax');?></h4>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-md-2">
											<label>Retención</label>
										</div>
										<div class="col-md-5">
											<label>Opción</label>
										</div>
										<div class="col-md-2">
											<label>Porcentaje</label>
										</div>
										<div class="col-md-3">
											<label>Valor</label>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<label>
												<input type="checkbox" class="skip" name="rete_fuente" id="rete_fuente"> Fuente
											</label>
										</div>
										<div class="col-md-5">
											<select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
												<option>Seleccione...</option>
											</select>
											<input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
											<input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
											<input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
										</div>
										<div class="col-md-2">
											<input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
										</div>
										<div class="col-md-3">
											<input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control" readonly>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<label>
												<input type="checkbox" class="skip" name="rete_iva" id="rete_iva"> Iva
											</label>
										</div>
										<div class="col-md-5">
											<select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
												<option>Seleccione...</option>
											</select>
											<input type="hidden" name="rete_iva_account" id="rete_iva_account">
											<input type="hidden" name="rete_iva_base" id="rete_iva_base">
											<input type="hidden" name="rete_iva_id" id="rete_iva_id">
										</div>
										<div class="col-md-2">
											<input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
										</div>
										<div class="col-md-3">
											<input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control" readonly>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<label>
												<input type="checkbox" class="skip" name="rete_ica" id="rete_ica"> Ica
											</label>
										</div>
										<div class="col-md-5">
											<select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
												<option>Seleccione...</option>
											</select>
											<input type="hidden" name="rete_ica_account" id="rete_ica_account">
											<input type="hidden" name="rete_ica_base" id="rete_ica_base">
											<input type="hidden" name="rete_ica_id" id="rete_ica_id">
										</div>
										<div class="col-md-2">
											<input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
										</div>
										<div class="col-md-3">
											<input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control" readonly>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<label>
												<input type="checkbox" class="skip" name="rete_otros" id="rete_otros"> Otros
											</label>
										</div>
										<div class="col-md-5">
											<select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
												<option>Seleccione...</option>
											</select>
											<input type="hidden" name="rete_otros_account" id="rete_otros_account">
											<input type="hidden" name="rete_otros_base" id="rete_otros_base">
											<input type="hidden" name="rete_otros_id" id="rete_otros_id">
										</div>
										<div class="col-md-2">
											<input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
										</div>
										<div class="col-md-3">
											<input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control" readonly>
										</div>
									</div>
									<div class="row">
										<div class="col-md-8 text-right">
											<label>Total : </label>
										</div>
										<div class="col-md-4 text-right">
											<label id="total_rete_amount"> 0.00 </label>
										</div>
										<input type="hidden" name="rete_applied" id="rete_applied" value="0">
									</div>
									<!-- <div class="form-group">
										<?=lang("order_tax", "order_tax_input");?>
										<?php
										$tr[""] = "";
										foreach ($tax_rates as $tax) {
											$tr[$tax->id] = $tax->name;
										}
										echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
										?>
									</div> -->
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" id="cancelOrderRete" data-dismiss="modal">Cancelar</button>
									<button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
								</div>
							</div>
						</div>
					</div>
			<?php echo form_close(); ?>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
</div>
<!-- FINAL CONTENT -->
<!-- <div class="rotate btn-cat-con">
<button type="button" id="open-brands" class="btn btn-info open-brands"><?= lang('brands'); ?></button>
<button type="button" id="open-subcategory" class="btn btn-warning open-subcategory"><?= lang('subcategories'); ?></button>
<button type="button" id="open-category" class="btn btn-primary open-category"><?= lang('categories'); ?></button>
</div> -->
<div class="modal fade in" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
    class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
    <h4 class="modal-title" id="payModalLabel"><?=lang('finalize_sale');?></h4>
  </div>
  <div class="modal-body" id="payment_content">
    <div class="col-md-12 resAlert"></div>
    <?php if (isset($cost_centers)): ?>
        <div class="col-md-6 form-group">
            <?php
            $ccopts[''] = lang('select').lang('cost_center');
            foreach ($cost_centers as $cost_center) {
                $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
            }
             ?>
             <?= form_dropdown('cost_center', $ccopts, '', 'id="cost_center" class="form-control input-tip" required="required" style="width:100%;" '); ?>
        </div>
    <?php endif ?>
    <div class="row">
    	<div class="col-sm-6">
            <select name="payment_reference_no" class="form-control" id="payment_reference_no" required="required"></select>
		</div>
		<hr class="col-sm-11">
      <div class="col-md-12">
						<div class="clearfir"></div>
						<div id="payments">
							<div class="well well-sm well_1">
								<div class="payment">
									<div class="row">
										<div class="col-sm-5 input_payment_term_paid_by">
											<div class="form-group">
												<?=lang("paying_by", "paid_by_1");?>
												<select name="paid_by[]" id="paid_by_1" class="form-control paid_by">
													<?= $this->sma->paid_opts(); ?>
													<?=$pos_settings->paypal_pro ? '<option value="ppp">' . lang("paypal_pro") . '</option>' : '';?>
													<?=$pos_settings->stripe ? '<option value="stripe">' . lang("stripe") . '</option>' : '';?>
													<?=$pos_settings->authorize ? '<option value="authorize">' . lang("authorize") . '</option>' : '';?>
												</select>
											</div>
										</div>
										<div class="col-sm-5 input_payment_term_amount">
											<div class="form-group">
												<?=lang("amount", "amount_1");?>
												<input name="amount[]" type="text" id="amount_1" class="pa form-control kb-pad1 amount only_number"/>
											</div>
										</div>
										<div class="col-sm-4 input_payment_term_val" style="display: none;">
											<div class="form-group">
												<label><?=lang("payment_term");?></label>
												<input type="text" class="pa form-control payment_term_val only_number">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-11">
											<div class="form-group gc_1" style="display: none;">
												<?=lang("gift_card_no", "gift_card_no_1");?>
												<input name="paying_gift_card_no[]" type="text" id="gift_card_no_1"
												class="pa form-control kb-pad gift_card_no"/>

												<div id="gc_details_1"></div>
											</div>
											<div class="pcc_1" style="display:none;">
												<div class="form-group">
													<input type="text" id="swipe_1" class="form-control swipe"
													placeholder="<?=lang('swipe')?>"/>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<input name="cc_no[]" type="text" id="pcc_no_1"
															class="form-control"
															placeholder="<?=lang('cc_no')?>"/>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">

															<input name="cc_holer[]" type="text" id="pcc_holder_1"
															class="form-control"
															placeholder="<?=lang('cc_holder')?>"/>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<select name="cc_type[]" id="pcc_type_1"
															class="form-control pcc_type"
															placeholder="<?=lang('card_type')?>">
															<option value="Visa"><?=lang("Visa");?></option>
															<option
															value="MasterCard"><?=lang("MasterCard");?></option>
															<option value="Amex"><?=lang("Amex");?></option>
															<option
															value="Discover"><?=lang("Discover");?></option>
														</select>
														<!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?=lang('card_type')?>" />-->
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<input name="cc_month[]" type="text" id="pcc_month_1"
														class="form-control"
														placeholder="<?=lang('month')?>"/>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">

														<input name="cc_year" type="text" id="pcc_year_1"
														class="form-control"
														placeholder="<?=lang('year')?>"/>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">

														<input name="cc_cvv2" type="text" id="pcc_cvv2_1"
														class="form-control"
														placeholder="<?=lang('cvv2')?>"/>
													</div>
												</div>
											</div>
										</div>
										<div class="pcheque_1" style="display:none;">
											<div class="form-group"><?=lang("cheque_no", "cheque_no_1");?>
												<input name="cheque_no[]" type="text" id="cheque_no_1"
												class="form-control cheque_no"/>
											</div>
										</div>
										<div class="form-group">
											<?=lang('payment_note', 'payment_note');?>
											<textarea name="payment_note[]" id="payment_note_1"
											class="pa form-control kb-text payment_note"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="multi-payment"></div>
					<button type="button" class="btn btn-primary col-md-12 addButton"><i
						class="fa fa-plus"></i> <?=lang('add_more_payments')?></button>
						<div style="clear:both; height:15px;"></div>
						<div class="font16">
							<table class="table table-bordered table-condensed" style="margin-bottom: 0;">
								<tbody>
									<tr>
										<td width="25%"><?=lang("total_items");?></td>
										<td width="25%" class="text-right"><span id="item_count">0.00</span></td>
										<td width="25%"><?=lang("total_payable");?></td>
										<td width="25%" class="text-right"><span id="twt">0.00</span></td>
									</tr>
									<tr>
										<td><?=lang("total_paying");?></td>
										<td class="text-right"><span id="total_paying">0.00</span></td>
										<td><?=lang("balance");?></td>
										<td class="text-right"><span id="balance">0.00</span></td>
									</tr>
								</tbody>
							</table>
							<div class="clearfix"></div>
						</div>
					</div>

							<div style="display: none;">
								<span style="font-size: 1.2em; font-weight: bold;"><?=lang('quick_cash');?></span>

								<div class="btn-group btn-group-vertical">
									<button type="button" class="btn btn-lg btn-info quick-cash" id="quick-payable">0.00
									</button>
									<?php
									foreach (lang('quick_cash_notes') as $cash_note_amount) {
										echo '<button type="button" class="btn btn-lg btn-warning quick-cash">' . $cash_note_amount . '</button>';
									}
									?>
									<button type="button" class="btn btn-lg btn-danger"
									id="clear-cash-notes"><?=lang('clear');?></button>
								</div>
							</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-block btn-lg btn-primary" id="submit-sale"><?=lang('submit');?></button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="cmModal" tabindex="-1" role="dialog" aria-labelledby="cmModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
					<i class="fa fa-2x">&times;</i></span>
					<span class="sr-only"><?=lang('close');?></span>
				</button>
				<h4 class="modal-title" id="cmModalLabel"></h4>
			</div>
			<div class="modal-body" id="pr_popover_content">
				<div class="form-group">
					<?= lang('comment', 'icomment'); ?>
					<?= form_textarea('comment', '', 'class="form-control" id="icomment" style="height:80px;"'); ?>
				</div>
				<div class="form-group">
					<?= lang('ordered', 'iordered'); ?>
					<?php
					$opts = array(0 => lang('no'), 1 => lang('yes'));
					?>
					<?= form_dropdown('ordered', $opts, '', 'class="form-control" id="iordered" style="width:100%;"'); ?>
				</div>
				<input type="hidden" id="irow_id" value=""/>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="editComment"><?=lang('submit')?></button>
			</div>
		</div>
	</div>
</div>
		<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
							class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
							<h4 class="modal-title" id="prModalLabel"></h4>
					</div>
					<div class="modal-body" id="pr_popover_content">
						<form class="form-horizontal" role="form">
		                    <div class="form-group">
		                        <label for="pname" class="col-sm-4 control-label"><?= lang('product_name') ?></label>
		                        <div class="col-sm-8">
		                            <?= form_input('pname', '', 'id="pname" class="form-control"') ?>
		                        </div>
		                    </div>
							<?php if ($Settings->tax1) {
								?>
								<div class="form-group">
									<label class="col-sm-4 control-label"><?=lang('product_tax')?></label>
									<div class="col-sm-8">
		                                <select name="ptax" id="ptax" class="form-control">
		                                    <?php foreach ($tax_rates as $tax): ?>
		                                        <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
		                                    <?php endforeach ?>
		                                </select>
									</div>
								</div>
		                        <div class="form-group row">
		                            <div class="col-sm-4">
		                                <label class="control-label"><?= lang('second_product_tax') ?></label>
		                            </div>
		                            <div class="col-sm-8">
		                                <input type="text" name="ptax2" id="ptax2" class="form-control" readonly>
		                            </div>
		                        </div>
							<?php } ?>
							<?php if ($Settings->product_serial) { ?>
								<div class="form-group">
									<label for="pserial" class="col-sm-4 control-label"><?=lang('serial_no')?></label>
									<div class="col-sm-8">
										<input type="text" class="form-control kb-text" id="pserial">
									</div>
								</div>
							<?php } ?>
							<div class="form-group">
								<label for="pquantity" class="col-sm-4 control-label"><?=lang('quantity')?></label>
								<div class="col-sm-8">
									<input type="text" class="form-control kb-pad" id="pquantity">
								</div>
							</div>
							<div class="form-group">
								<label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
								<div class="col-sm-8">
									<div id="punits-div"></div>
								</div>
							</div>
							<div class="form-group">
								<label for="poption" class="col-sm-4 control-label"><?=lang('product_option')?></label>
								<div class="col-sm-8">
									<div id="poptions-div"></div>
								</div>
							</div>
							<div class="form-group">
								<label for="ppreferences" class="col-sm-4 control-label"><?=lang('product_preferences')?></label>
								<div class="col-sm-8">
									<div id="ppreferences-div"></div>
								</div>
							</div>
							<?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
								<div class="form-group">
									<label for="pdiscount" class="col-sm-4 control-label"><?=lang('product_discount')?></label>
									<div class="col-sm-8">
										<input type="text" class="form-control kb-pad" id="pdiscount">
									</div>
								</div>
							<?php } ?>
							<div class="form-group">
								<label for="paddprice" class="col-sm-4 control-label"><?=lang('add_price_amount')?></label>

								<div class="col-sm-8">
									<input type="text" class="form-control kb-pad" id="paddprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
								</div>
							</div>
		                    <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "style='display:none;'" : "" ?>>
		                        <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>
		                        <div class="col-sm-8">
		                            <input type="text" class="form-control" id="pprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
		                        </div>
		                    </div>
		                    <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "" : "style='display:none;'" ?>>
		                        <label for="pproduct_unit_price" class="col-sm-4 control-label"><?= lang('sale_form_edit_product_unit_price') ?></label>
		                        <div class="col-sm-8">
		                            <input type="text" class="form-control" id="pproduct_unit_price" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
		                        </div>
		                    </div>
							<?php if ($Settings->ipoconsumo): ?>
		                        <div class="form-group">
		                            <label for="pprice_ipoconsumo" class="col-sm-4 control-label"><?= lang('unit_price_ipoconsumo') ?></label>
		                            <div class="col-sm-8">
		                                <input type="text" class="form-control" id="pprice_ipoconsumo"><br>
		                                <em><?= lang('unit_price_ipoconsumo_detail') ?></em>
		                            </div>
		                        </div>
		                    <?php endif ?>
							<table class="table table-bordered">
								<tr>
									<th style="width:25%;"><?=lang('net_unit_price');?></th>
									<th style="width:25%;"><span id="net_price"></span></th>
									<th style="width:25%;"><?=lang('product_taxes');?></th>
									<th style="width:25%;"><span id="pro_tax"></span></th>
								</tr>
							</table>
							<input type="hidden" id="punit_price" value=""/>
							<input type="hidden" id="old_tax" value=""/>
							<input type="hidden" id="old_qty" value=""/>
							<input type="hidden" id="old_price" value=""/>
							<input type="hidden" id="row_id" value=""/>
							<div class="panel panel-default">
		                        <div class="panel-heading"><?= lang('calculate_unit_price'); ?></div>
		                        <div class="panel-body">

		                            <div class="form-group">
		                                <label for="pcost" class="col-sm-4 control-label"><?= lang('subtotal_before_tax') ?></label>
		                                <div class="col-sm-8">
		                                    <div class="input-group">
		                                        <input type="text" class="form-control" id="psubtotal">
		                                        <div class="input-group-addon" style="padding: 2px 8px;">
		                                            <a id="calculate_unit_price" class="tip" title="<?= lang('calculate_unit_price'); ?>">
		                                                <i class="fa fa-calculator"></i>
		                                            </a>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="editItem"><?=lang('submit')?></button>
					</div>
				</div>
			</div>
		</div>
			<div class="modal fade in" id="gcModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="fa fa-2x">&times;</i></button>
								<h4 class="modal-title" id="myModalLabel"><?=lang('sell')." ".lang('gift_card');?></h4>
						</div>
						<div class="modal-body">
							<p><?=lang('enter_info');?></p>
							<div class="alert alert-danger gcerror-con" style="display: none;">
								<button data-dismiss="alert" class="close" type="button">×</button>
								<span id="gcerror"></span>
							</div>
							<div class="form-group">
								<label><?= lang('gift_card').lang('_no') ?></label> *
								<div class="input-group">
									<?php echo form_input('gccard_no', '', 'class="form-control" id="gccard_no"'); ?>
									<div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
										<a href="#" id="genNo"><i class="fa fa-cogs"></i></a>
									</div>
								</div>
							</div>
							<input type="hidden" name="gcname" value="<?=lang('gift_card')?>" id="gcname"/>
							<div class="form-group">
								<?=lang("value", "gcvalue");?> *
								<?php echo form_input('gcvalue', '', 'class="form-control" id="gcvalue"'); ?>
							</div>
							<div class="form-group">
								<?=lang("price", "gcprice");?> *
								<?php echo form_input('gcprice', '', 'class="form-control" id="gcprice"'); ?>
							</div>
							<div class="form-group">
								<?=lang("customer", "gccustomer");?>
								<?php echo form_input('gccustomer', '', 'class="form-control" id="gccustomer"'); ?>
							</div>
							<div class="form-group">
								<?=lang("expiry_date", "gcexpiry");?>
								<?php echo form_input('gcexpiry', $this->sma->hrsd(date("Y-m-d", strtotime("+2 year"))), 'class="form-control date" id="gcexpiry"'); ?>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" id="addGiftCard" class="btn btn-primary"><?=lang('sell_gift_card')?></button>
						</div>
					</div>
				</div>
			</div>
		<div class="modal fade in" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
							class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
							<h4 class="modal-title" id="mModalLabel"><?=lang('add_product_manually')?></h4>
					</div>
					<div class="modal-body" id="pr_popover_content">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label for="mcode" class="col-md-4 control-label"><?=lang('product_code')?> *</label>

								<div class="col-md-8">
									<input type="text" class="form-control kb-text" id="mcode">
								</div>
							</div>
							<div class="form-group">
								<label for="mname" class="col-md-4 control-label"><?=lang('product_name')?> *</label>

								<div class="col-md-8">
									<input type="text" class="form-control kb-text" id="mname">
								</div>
							</div>
							<?php if ($Settings->tax1) {
								?>
								<div class="form-group">
									<label for="mtax" class="col-md-4 control-label"><?=lang('product_tax')?> *</label>

									<div class="col-md-8">
										<?php
										$tr[""] = "";
										foreach ($tax_rates as $tax) {
											$tr[$tax->id] = $tax->name;
										}
										echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control pos-input-tip" style="width:100%;"');
										?>
									</div>
								</div>
							<?php }
							?>
							<div class="form-group">
								<label for="mquantity" class="col-md-4 control-label"><?=lang('quantity')?> *</label>

								<div class="col-md-8">
									<input type="text" class="form-control kb-pad" id="mquantity">
								</div>
							</div>
							<?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {?>
								<div class="form-group">
									<label for="mdiscount"
									class="col-md-4 control-label"><?=lang('product_discount')?></label>

									<div class="col-md-8">
										<input type="text" class="form-control kb-pad" id="mdiscount">
									</div>
								</div>
							<?php }
							?>
							<div class="form-group">
								<label for="mprice" class="col-md-4 control-label"><?=lang('unit_price')?> *</label>
								<div class="col-md-8">
									<input type="text" class="form-control kb-pad" id="mprice">
								</div>
							</div>
							<table class="table table-bordered">
								<tr>
									<th style="width:25%;"><?=lang('net_unit_price');?></th>
									<th style="width:25%;"><span id="mnet_price"></span></th>
									<th style="width:25%;"><?=lang('product_tax');?></th>
									<th style="width:25%;"><span id="mpro_tax"></span></th>
								</tr>
							</table>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="addItemManually"><?=lang('submit')?></button>
					</div>
				</div>
			</div>
		</div>
			<div class="modal fade in" id="sckModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
								<i class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span>
							</button>
							<button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
								<i class="fa fa-print"></i> <?= lang('print'); ?>
							</button>
							<h4 class="modal-title" id="mModalLabel"><?=lang('shortcut_keys')?></h4>
						</div>
						<div class="modal-body" id="pr_popover_content">
							<table class="table table-bordered table-condensed table-hover"
							style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th><?=lang('shortcut_keys')?></th>
									<th><?=lang('actions')?></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?=$pos_settings->focus_add_item?></td>
									<td><?=lang('focus_add_item')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->add_manual_product?></td>
									<td><?=lang('add_manual_product')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->customer_selection?></td>
									<td><?=lang('customer_selection')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->add_customer?></td>
									<td><?=lang('add_customer')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->toggle_category_slider?></td>
									<td><?=lang('toggle_category_slider')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->toggle_subcategory_slider?></td>
									<td><?=lang('toggle_subcategory_slider')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->cancel_sale?></td>
									<td><?=lang('cancel_sale')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->suspend_sale?></td>
									<td><?=lang('suspend_sale')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->print_items_list?></td>
									<td><?=lang('print_items_list')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->finalize_sale?></td>
									<td><?=lang('finalize_sale')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->today_sale?></td>
									<td><?=lang('today_sale')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->open_hold_bills?></td>
									<td><?=lang('open_hold_bills')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->close_register?></td>
									<td><?=lang('close_register')?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in" id="sPModal" tabindex="-1" role="dialog" aria-labelledby="sPModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h3><?= lang('add_item') ?></h3>
					</div>
					<div class="modal-body">
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th><?= lang('code') ?></th>
									<th><?= lang('description') ?></th>
									<th><?= lang('quantity') ?></th>
								</tr>
							</thead>
						</table>
						<table class="table" id="items_table" style="width: 100%;">
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in" id="sPUModal" tabindex="-1" role="dialog" aria-labelledby="sPUModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="product_name_spumodal"></h2>
						<h3><?= lang('unit_prices') ?></h3>
					</div>
					<div class="modal-body">
						<table class="table" id="unit_prices_table" style="width: 100%;">
							<thead>
								<tr>
									<th style="width: 5%;"></th>
									<th style="width: 35%;"><?= lang('unit') ?></th>
									<th style="width: 35%;"><?= lang('price') ?></th>
									<th style="width: 25%;"><?= lang('quantity') ?></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<div class="col-sm-3">
							<?= lang('unit_quantity', 'unit_quantity') ?>
							<input type="text" name="unit_quantity" id="unit_quantity" class="form-control">
						</div>
						<div class="col-sm-9">
							<button class="btn btn-success send_item_unit_select" type="button"><?= lang('submit') ?></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in" id="pricesChanged" tabindex="-1" role="dialog" aria-labelledby="pricesChangedLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h3><?= lang('product_prices_changed') ?></h3>
		            </div>
		            <div class="modal-body">
		                <table class="table">
		                    <thead>
		                        <tr>
		                            <th style="width: 35%;"><?= lang('product_name') ?></th>
		                            <th style="width: 20%;"><?= lang('previous_product_price') ?></th>
		                            <th style="width: 35%;"><?= lang('actual_product_price') ?></th>
		                        </tr>
		                    </thead>
		                    <tbody id="prices_changed_table">

		                    </tbody>
		                </table>
		            </div>
		            <div class="modal-footer">
		                <div class="col-sm-12">
		                    <button class="btn btn-danger" id="pos_wholesale_keep_prices" type="button"><?= lang('no_keep_prices') ?></button>
		                    <button class="btn btn-success" id="update_prices" type="button"><?= lang('yes_update_prices') ?></button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
			<div class="modal fade in" id="sckModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
								<i class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span>
							</button>
							<button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
								<i class="fa fa-print"></i> <?= lang('print'); ?>
							</button>
							<h4 class="modal-title" id="mModalLabel"><?=lang('shortcut_keys')?></h4>
						</div>
						<div class="modal-body" id="pr_popover_content">
							<table class="table table-bordered table-condensed table-hover"
							style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th><?=lang('shortcut_keys')?></th>
									<th><?=lang('actions')?></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?=$pos_settings->focus_add_item?></td>
									<td><?=lang('focus_add_item')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->add_manual_product?></td>
									<td><?=lang('add_manual_product')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->customer_selection?></td>
									<td><?=lang('customer_selection')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->add_customer?></td>
									<td><?=lang('add_customer')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->toggle_category_slider?></td>
									<td><?=lang('toggle_category_slider')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->toggle_subcategory_slider?></td>
									<td><?=lang('toggle_subcategory_slider')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->cancel_sale?></td>
									<td><?=lang('cancel_sale')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->suspend_sale?></td>
									<td><?=lang('suspend_sale')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->print_items_list?></td>
									<td><?=lang('print_items_list')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->finalize_sale?></td>
									<td><?=lang('finalize_sale')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->today_sale?></td>
									<td><?=lang('today_sale')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->open_hold_bills?></td>
									<td><?=lang('open_hold_bills')?></td>
								</tr>
								<tr>
									<td><?=$pos_settings->close_register?></td>
									<td><?=lang('close_register')?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in" id="dsModal" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<i class="fa fa-2x">&times;</i>
						</button>
						<h4 class="modal-title" id="dsModalLabel"><?=lang('edit_order_discount');?></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<?=lang("order_discount", "order_discount_input");?>
							<?php echo form_input('order_discount_input', '', 'class="form-control kb-pad" id="order_discount_input"'); ?>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="updateOrderDiscount" class="btn btn-primary"><?=lang('update')?></button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in" id="tipModal" tabindex="-1" role="dialog" aria-labelledby="tipModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<i class="fa fa-2x">&times;</i>
						</button>
						<h4 class="modal-title" id="tipModalLabel"><?=lang('tip_amount');?></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<?=lang("tip_amount", "tip_amount_input");?>
							<?php echo form_input('tip_amount_input', '', 'class="form-control kb-pad" id="tip_amount_input"'); ?>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="deleteTip" class="btn btn-danger"><i class="fa fa-times"></i></button>
						<button type="button" id="recalculateTip" class="btn btn-primary"><i class="fa fa-refresh"></i></button>
						<button type="button" id="updateTip" class="btn btn-primary"><?=lang('update')?></button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in" id="sModal" tabindex="-1" role="dialog" aria-labelledby="sModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<i class="fa fa-2x">&times;</i>
						</button>
						<h4 class="modal-title" id="sModalLabel"><?=lang('shipping');?></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<?=lang("shipping", "shipping_input");?>
							<?php echo form_input('shipping_input', '', 'class="form-control kb-pad" id="shipping_input"'); ?>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" id="updateShipping" class="btn btn-primary"><?=lang('update')?></button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in" id="txModal" tabindex="-1" role="dialog" aria-labelledby="txModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
							class="fa fa-2x">&times;</i></button>
							<h4 class="modal-title" id="txModalLabel"><?=lang('edit_order_tax');?></h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<?=lang("order_tax", "order_tax_input");?>
								<?php
								$tr[""] = "";
								foreach ($tax_rates as $tax) {
									$tr[$tax->id] = $tax->name;
								}
								echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
								?>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button type="button" id="updateOrderTax" class="btn btn-primary"><?=lang('update')?></button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade in" id="susModal" tabindex="-1" role="dialog" aria-labelledby="susModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="fa fa-2x">&times;</i></button>
								<h4 class="modal-title" id="susModalLabel"><?=lang('suspend_sale');?></h4>
							</div>
							<div class="modal-body">
								<p><?=lang('type_reference_note');?></p>
								<div class="form-group">
									<?=lang("reference_note", "reference_note");?>
									<?= form_input('reference_note', (!empty($reference_note) ? $reference_note : ''), 'class="form-control kb-text" id="reference_note"'); ?>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" id="suspend_sale" class="btn btn-primary"><?=lang('submit')?></button>
							</div>
						</div>
					</div>
				</div>
				<div id="order_tbl"><span id="order_span"></span>
					<table id="order-table" class="prT table" style="margin-bottom:0;" width="100%"></table>
				</div>
				<div id="bill_tbl"><span id="bill_span"></span>
					<table id="bill-table" width="100%" class="prT table" style="margin-bottom:0;"></table>
					<table id="bill-total-table" class="prT table" style="margin-bottom:0;" width="100%"></table>
					<span id="bill_footer"></span>
				</div>
				<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
				aria-hidden="true"></div>
				<div class="modal fade in" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
				aria-hidden="true"></div>
				<div class="modal fade in" id="cbModal" tabindex="-1" role="dialog" aria-labelledby="cbModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									<i class="fa fa-2x">&times;</i>
								</button>
								<h4 class="modal-title" id="cbModalLabel"><?=lang('customer_branch');?></h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
					            	<select name="customer_branch" id="poscustomer_branch" class="form-control">

					            	</select>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" id="updateCustomerBranch" class="btn btn-primary"><?=lang('update')?></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modal-loading" style="display: none;">
					<div class="blackbg"></div>
					<div class="loader"></div>
				</div>
				<?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code);?>
<script type="text/javascript">
var site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url('/'), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>, pos_settings = <?=json_encode($pos_settings);?>;
var lang = {
unexpected_value: '<?=lang('unexpected_value');?>',
select_above: '<?=lang('select_above');?>',
r_u_sure: '<?=lang('r_u_sure');?>',
bill: '<?=lang('bill');?>',
order: '<?=lang('order');?>',
total: '<?=lang('total');?>',
items: '<?=lang('items');?>',
discount: '<?=lang('discount');?>',
order_tax: '<?=lang('order_tax');?>',
grand_total: '<?=lang('grand_total');?>',
total_payable: '<?=lang('total_payable');?>',
rounding: '<?=lang('rounding');?>',
merchant_copy: '<?=lang('merchant_copy');?>'
};
</script>
<script type="text/javascript">
	var product_variant = 0, shipping = 0, p_page = 0, per_page = 0, tcp = "<?=$tcp?>", pro_limit = <?= $pos_settings->pro_limit; ?>, brand_id = 0, obrand_id = 0, cat_id = "<?=$pos_settings->default_section?>", ocat_id = "<?=$pos_settings->default_section?>", sub_cat_id = 0, osub_cat_id, count = 1, an = 1, DT = <?=$Settings->default_tax_rate?>, product_tax = 0, invoice_tax = 0, product_discount = 0, order_discount = 0, total_discount = 0, total = 0, total_paid = 0, grand_total = 0, KB = <?=$pos_settings->keyboard?>, tax_rates =<?php echo json_encode($tax_rates); ?>;
	var protect_delete = false,
		billers = <?= json_encode($posbillers); ?>,
		biller = <?= json_encode($posbiller); ?>;
	var username = '<?=$this->session->userdata('username');?>',
		order_data = '',
		bill_data = '';
	function widthFunctions(e) {
		var wh = $(window).height(),
		ww = $(window).width(),
		lth = $('#left-top').height(),
		lbh = $('#left-bottom').height();
		//Ajuste de alturas
		// if(ww > 800){
		//   $('.menuPOS').css("height", wh - $('#header').height());
		//   $('#content').css("height", wh - $('#header').height());
		// }
		// else{
		//   if(wh > 400){
		//     $('.menuPOS').css("height", wh - $('#header').height());
		//     $('#content').css("height", wh - $('#header').height());
		//   }else{
		//   }
		// }
		// if(wh <= 400){
		//   console.log("Movil acostado.");
		//   //$('#wrapper').css("margin-top",$('#header').height());
		//   $('#wrapper').css("height", wh);
		// }
		// $('#item-list').css("height", wh - 190);
		// $('#item-list').css("min-height", 515);
		// $('#left-middle').css("height", wh - lth - lbh - 102);
		// $('#left-middle').css("min-height", 278);
		// $('#product-list').css("height", wh - lth - lbh - 107);
		// $('#product-list').css("min-height", 278);
	}
	$(window).bind("resize", widthFunctions);
	$(document).ready(function () {
		cargar_codigo_forma_pago_fe();
		$('#view-customer').click(function(){
			$('#myModal').modal({remote: site.base_url + 'customers/view/' + $("input[name=customer]").val()});
			$('#myModal').modal('show');
		});
		$('textarea').keydown(function (e) {
			if (e.which == 13) {
				var s = $(this).val();
				$(this).val(s+'\n').focus();
				e.preventDefault();
				return false;
			}
		});
		<?php if ($sid) { ?>
			localStorage.setItem('positems', JSON.stringify(<?=$items;?>));
		<?php } ?>
		<?php if ($oid) { ?>
			localStorage.setItem('positems', JSON.stringify(<?=$items;?>));
		<?php } ?>
		<?php if ($this->session->userdata('remove_posls')) { ?>
			if (localStorage.getItem('positems')) {
		        localStorage.removeItem('positems');
		    }
		    if (localStorage.getItem('posdiscount')) {
		        localStorage.removeItem('posdiscount');
		    }
		    if (localStorage.getItem('postax2')) {
		        localStorage.removeItem('postax2');
		    }
		    if (localStorage.getItem('posshipping')) {
		        localStorage.removeItem('posshipping');
		    }
		    if (localStorage.getItem('posref')) {
		        localStorage.removeItem('posref');
		    }
		    if (localStorage.getItem('poswarehouse')) {
		        localStorage.removeItem('poswarehouse');
		    }
		    if (localStorage.getItem('posnote')) {
		        localStorage.removeItem('posnote');
		    }
		    if (localStorage.getItem('posinnote')) {
		        localStorage.removeItem('posinnote');
		    }
		    if (localStorage.getItem('poscustomer')) {
		        localStorage.removeItem('poscustomer');
		    }
		    if (localStorage.getItem('poscurrency')) {
		        localStorage.removeItem('poscurrency');
		    }
		    if (localStorage.getItem('posdate')) {
		        localStorage.removeItem('posdate');
		    }
		    if (localStorage.getItem('posstatus')) {
		        localStorage.removeItem('posstatus');
		    }
		    if (localStorage.getItem('posbiller')) {
		        localStorage.removeItem('posbiller');
		    }

		    if (localStorage.getItem('retenciones')) {
		        localStorage.removeItem('retenciones');
		    }
		    if (localStorage.getItem('restobar_mode_module')) {
		        localStorage.removeItem('restobar_mode_module');
		    }
		    if (localStorage.getItem('tip_amount')) {
		        localStorage.removeItem('tip_amount');
		    }
		    if (localStorage.getItem('delete_tip_amount')) {
		        localStorage.removeItem('delete_tip_amount');
		    }
			if (localStorage.getItem('customer_validate_min_base_retention')) {
			    localStorage.removeItem('customer_validate_min_base_retention');
			}
			location.reload();
		<?php $this->sma->unset_data('remove_posls');} ?>
		widthFunctions();
		<?php if ($suspend_sale) {?>
			localStorage.setItem('postax2', '<?=$suspend_sale->order_tax_id;?>');
			localStorage.setItem('posdiscount', '<?=$suspend_sale->order_discount_id;?>');
			localStorage.setItem('poswarehouse', '<?=$suspend_sale->warehouse_id;?>');
			localStorage.setItem('poscustomer', '<?=$suspend_sale->customer_id;?>');
			localStorage.setItem('posbiller', '<?=$suspend_sale->biller_id;?>');
			localStorage.setItem('posseller', '<?=$suspend_sale->seller_id;?>');
			localStorage.setItem('posshipping', '<?=$suspend_sale->shipping;?>');
		<?php }
		?>
		<?php if ($old_sale) {?>
			localStorage.setItem('postax2', '<?=$old_sale->order_tax_id;?>');
			localStorage.setItem('posdiscount', '<?=$old_sale->order_discount_id;?>');
			localStorage.setItem('poswarehouse', '<?=$old_sale->warehouse_id;?>');
			localStorage.setItem('poscustomer', '<?=$old_sale->customer_id;?>');
			localStorage.setItem('posbiller', '<?=$old_sale->biller_id;?>');
			localStorage.setItem('posshipping', '<?=$old_sale->shipping;?>');
		<?php }
		?>
		<?php if ($quote) {?>
			localStorage.setItem('postax2', '<?=$quote->order_tax_id;?>');
			localStorage.setItem('posdiscount', '<?=$quote->order_discount_id;?>');
			localStorage.setItem('poswarehouse', '<?=$quote->warehouse_id;?>');
			localStorage.setItem('poscustomer', '<?=$quote->customer_id;?>');
			localStorage.setItem('posbiller', '<?=$quote->biller_id;?>');
			localStorage.setItem('posshipping', '<?=$quote->shipping;?>');
			localStorage.setItem('poscustomer_branch', '<?=$quote->address_id;?>');
			localStorage.setItem('pospaid_by', '<?=isset($quote->payment_method) ? $quote->payment_method : false;?>');
			setTimeout(function() {
				$('#posbiller').trigger('change');
			}, 1200);
		<?php }
		?>
		if (!localStorage.getItem('postax2')) {
			localStorage.setItem('postax2', <?=$Settings->default_tax_rate2;?>);
		}
		$('.select').select2({minimumResultsForSearch: 7});
		$('#poscustomer').val(localStorage.getItem('poscustomer')).select2({
			minimumInputLength: 1,
			data: [],
			initSelection: function (element, callback) {
				$.ajax({
					type: "get", async: false,
					url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val()+"/1",
					dataType: "json",
					success: function (data) {
						callback(data[0]);
					}
				});
			},
			ajax: {
				url: site.base_url + "customers/suggestionsws",
				dataType: 'json',
				quietMillis: 15,
				data: function (term, page) {
					return {
						term: term,
						limit: 10
					};
				},
				results: function (data, page) {
					if (data.results != null) {
						return {results: data.results};
					} else {
						return {results: [{id: '', text: lang.no_match_found}]};
					}
				}
			}
		});
		get_customer_branches();
		if (KB) {
			display_keyboards();
			var result = false, sct = '';
			$('#poscustomer').on('select2-opening', function () {
				sct = '';
				$('.select2-input').addClass('kb-text');
				display_keyboards();
				$('.select2-input').bind('change.keyboard', function (e, keyboard, el) {
					if (el && el.value != '' && el.value.length > 0 && sct != el.value) {
						sct = el.value;
					}
					if(!el && sct.length > 0) {
						$('.select2-input').addClass('select2-active');
						setTimeout(function() {
							$.ajax({
								type: "get",
								async: false,
								url: "<?=admin_url('customers/suggestionsws')?>/?term=" + sct,
								dataType: "json",
								success: function (res) {
									if (res.results != null) {
										$('#poscustomer').select2({data: res}).select2('open');
										$('.select2-input').removeClass('select2-active');
									} else {
										// bootbox.alert('no_match_found');
										$('#poscustomer').select2('close');
										$('#test').click();
									}
								}
							});
						}, 500);
					}
				});
			});
			$('#poscustomer').on('select2-close', function () {
				$('.select2-input').removeClass('kb-text');
				$('#test').click();
				$('select, .select').select2('destroy');
				$('select, .select').select2({minimumResultsForSearch: 7});
			});
			$(document).bind('click', '#test', function () {
				var kb = $('#test').keyboard().getkeyboard();
				kb.close();
			});
		}
	$(document).on('change', '#posbiller', function(){
		$('#biller').val($(this).val());
		localStorage.setItem('posbiller', $(this).val());
		$.ajax({
		  url:'<?= admin_url("billers/getBillersDocumentTypes/1/") ?>'+$('#posbiller').val()+'<?= $this->session->userdata('pos_document_type_id') ? "/".$this->session->userdata('pos_document_type_id') : "" ?>',
		  type:'get',
		  dataType:'JSON'
		}).done(function(data){
		  response = data;
		  $('#doc_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
		  $('#doc_type_id').trigger('change');
		  <?php if ($this->session->userdata('document_type_id')): ?>
		  	$('#doc_type_id').select2('readonly', true);
		  <?php endif ?>
		  if (response.status == 0) {
		    $('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>");
		  } else {
		    $('.resAlert').css('display', 'none');
		  }
		});
		$.ajax({
	        url:'<?= admin_url("billers/getBillersDocumentTypes/19/") ?>'+$('#posbiller').val(),
	        type:'get',
	        dataType:'JSON'
	    }).done(function(data){
	        response = data;
	        $('#payment_reference_no').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
	        if (response.status == 0) {
		    $('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('payment_biller_without_documents_types') ?></div></div>");
	        }
	      $('#payment_reference_no').trigger('change');
	    });
		$.ajax({
		    url : site.base_url+"sales/getSellers",
		    type : "get",
		    data : {"biller_id" : $('#posbiller').val()}
		}).done(function(data){
		    if (data != false) {
		        $('#posseller').html(data).trigger('change');
		    }
		}).fail(function(data){
		    console.log(data);
		});
		<?php if ($sid || $oid) {?>
			var customer = localStorage.getItem('poscustomer');
			var warehouse = localStorage.getItem('poswarehouse');
			var seller = localStorage.getItem('posseller');
		<?php } else { ?>
			if (localStorage.getItem('poscustomer')) {
				var customer = localStorage.getItem('poscustomer');
			} else {
				var customer = $('#posbiller option:selected').data('customerdefault');
				<?php if ($customer): ?>
					if (customer == 0) {
						customer = <?= $customer->id;?>;
					}
				<?php endif ?>
				localStorage.setItem('poscustomer', customer);
			}
			var warehouse = $('#posbiller option:selected').data('warehousedefault');
			var seller = $('#posbiller option:selected').data('sellerdefault');
		<?php } ?>
		var pricegroup = $('#posbiller option:selected').data('pricegroupdefault');
		if (customer != '' && customer !== undefined) {
			$('#poscustomer').val(customer).select2({
					minimumInputLength: 1,
					data: [],
					initSelection: function (element, callback) {
						$.ajax({
							type: "get", async: false,
							url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val(),
							dataType: "json",
							success: function (data) {
								callback(data[0]);
							}
						});
					},
					ajax: {
						url: site.base_url + "customers/suggestions",
						dataType: 'json',
						quietMillis: 15,
						data: function (term, page) {
							return {
								term: term,
								limit: 10
							};
						},
						results: function (data, page) {
							if (data.results != null) {
								return {results: data.results};
							} else {
								return {results: [{id: '', text: lang.no_match_found}]};
							}
						}
					}
					});
	        $('#poscustomer').trigger('change');
		}
		if (warehouse != '') {
	        $('#poswarehouse option').each(function(index, option){
	            option = $(this);
	            option.prop('disabled', false);
	        });
	        $('#poswarehouse').select2('val', warehouse);
	        $('#poswarehouse option').each(function(index, option){
	            option = $(this);
	            if (option.val() != warehouse) {
	                option.prop('disabled', true);
	            }
	        });
		}
		// console.log('Seller '+seller);
		if (seller != '') {

		    setTimeout(function() {
		        $('#posseller').select2('val', seller).trigger('change');
		    }, 800);
		}
	});
	function cargar_codigo_forma_pago_fe() {
        var codigo_fe_forma_pago = $('#paid_by_1 option:selected').data('code_fe');
        $('#mean_payment_code_fe').val(codigo_fe_forma_pago);
    }
		<?php for ($i = 1; $i <= 5; $i++) {?>
			$('#paymentModal').on('change', '#amount_<?=$i?>', function (e) {
				$('#amount_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('blur', '#amount_<?=$i?>', function (e) {
				$('#amount_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('select2-close', '#paid_by_<?=$i?>', function (e) {
				$('#paid_by_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#pcc_no_<?=$i?>', function (e) {
				$('#cc_no_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#pcc_holder_<?=$i?>', function (e) {
				$('#cc_holder_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#gift_card_no_<?=$i?>', function (e) {
				$('#paying_gift_card_no_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#pcc_month_<?=$i?>', function (e) {
				$('#cc_month_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#pcc_year_<?=$i?>', function (e) {
				$('#cc_year_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#pcc_type_<?=$i?>', function (e) {
				$('#cc_type_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#pcc_cvv2_<?=$i?>', function (e) {
				$('#cc_cvv2_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#cheque_no_<?=$i?>', function (e) {
				$('#cheque_no_val_<?=$i?>').val($(this).val());
			});
			$('#paymentModal').on('change', '#payment_note_<?=$i?>', function (e) {
				$('#payment_note_val_<?=$i?>').val($(this).val());
			});
		<?php }
		?>
		//acá clic payment
		$('#payment').click(function () {
		set_customer_payment();
			$('#submit-sale').html('<i class="fa fa-spinner fa-spin"></i>  <?= lang('loading') ?>').attr('disabled', true);
			<?php if ($sid) {?>
				suspend = $('<span></span>');
				suspend.html('<input type="hidden" name="delete_id" value="<?php echo $sid; ?>" />');
				suspend.appendTo("#hidesuspend");
			<?php }
			?>
			//retenciones
			if ($('#rete_applied').val()) {
				total_rete_amount = $('#total_rete_amount').text();
				total_rete_amount = parseFloat(formatDecimal(total_rete_amount));
			} else {
				total_rete_amount = 0;
			}
			//retenciones
			var twt = formatDecimal((total + invoice_tax) - order_discount + shipping - total_rete_amount);
			if (count == 1) {
				bootbox.alert('<?=lang('x_total');?>');
				return false;
			}
			gtotal = formatDecimal(twt);
			<?php if ($pos_settings->rounding) {?>
				round_total = roundNumber(gtotal, <?=$pos_settings->rounding?>);
				var rounding = formatDecimal(0 - (gtotal - round_total));
				$('#twt').text(formatMoney(round_total) + ' (' + formatMoney(rounding) + ')');
				$('#quick-payable').text(round_total);
			<?php } else {?>
				$('#twt').text(formatMoney(gtotal));
				$('#quick-payable').text(gtotal);
			<?php }
			?>
			$('#item_count').text(formatDecimal(count - 1));
			$('#paymentModal').appendTo("body").modal('show');
			$('#amount_1').focus();
			//CUSTOMER PAYMENT TERM
			$.ajax({
				type: "get", async: false,
				url: site.base_url + "pos/payment_term_expire/" + $('#poscustomer').val() + "/1",
				success: function (data) {
					if (data == true) {doc_type_id
						$('.alertPaymentTerm').remove();
						mensaje = "El cliente seleccionado tiene facturas sin pagar y con el plazo de pago vencido.";
						$('.pos').append("<div class=\"alert alert-danger alertPaymentTerm\"><button type=\"button\" class=\"close fa-2x\" data-dismiss=\"alert\">&times;</button>"+mensaje+"</div>");
					}
				}
			});
			pospaid_by = localStorage.getItem('pospaid_by');
			if (!pospaid_by && localStorage.getItem('poscustomerdepositamount') && pos_settings.express_payment == 0 && deposit_payment_method.sale == true) {
				deposit_amount = parseFloat(localStorage.getItem('poscustomerdepositamount'));
				if (deposit_amount > 0) {
					bootbox.confirm({
		                message: "El cliente cuenta con "+formatMoney(deposit_amount)+" de saldo de anticipos, ¿Desea usar este medio de pago para esta venta?",
		                buttons: {
		                    confirm: {
		                        label: 'Si, usar anticipos',
		                        className: 'btn-success send-submit-sale btn-full'
		                    },
		                    cancel: {
		                        label: 'No, usar otro medio de pago',
		                        className: 'btn-danger btn-full'
		                    }
		                },
		                callback: function (result) {
		                    if (result) {
		                       if (gtotal > deposit_amount) {
		                       		$('#amount_1').val(deposit_amount).trigger('change');
		                       		$('.addButton').trigger('click');
		                       		$('#amount_2').val(gtotal - deposit_amount).trigger('change');
		                       } else {
		                        	$('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
		                       }
		                       $('#paid_by_1').select2('val', 'deposit').trigger('change');
		                    } else {
		                        $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
		                    }
		                    setTimeout(function() {
		                       	$('.amount').trigger('change');
	                       		calculateTotals();
		                    }, 800);
		                }
		            });
				}
			} else {
				if (pospaid_by) {
					$('#paid_by_1').select2('val', pospaid_by);
				}
			}
			//acá parámetro efectivo express
			if (pos_settings.express_payment == 1) {
	       		$('#amount_1').prop('readonly', true);
				setTimeout(function() {
					$('#submit-sale').text('<?=lang('submit');?>').attr('disabled', false);
	           		$('#amount_1').trigger('change');
	           		calculateTotals();
	            }, 850);
	            setTimeout(function() {
	           		$('#submit-sale').trigger('click');
	            }, 1200);
			}
		});
		$('#doc_type_id').on('change', function(){
		  if ($(this).val() != '') {
		    validar_resolucion_biller();
		    validate_key_log();
    		validate_fe_customer_data();
		  }
		});
		function validar_resolucion_biller(){
		  $('.alertResolucion').remove();
		  $('#submit-sale').css('display', '')
		  //VALIDAR RESOLUCIÓN
		  $.ajax({
		    type: "get", async: false,
		    url: site.base_url + "pos/validate_biller_resolution/"+$('#doc_type_id').val(),
		    success: function (data) {
		      if (data != false) {
		        $('.resAlert').css('display', '');
		        response = JSON.parse(data);
						mensaje = response.mensaje;
						$('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button>"+mensaje+"</div></div>");
						if (response.disable == true) {
							$('#submit-sale').attr('disabled', true).css('display', 'none');
						}
		      } else {
		          $('.resAlert').css('display', 'none');
		          $('#submit-sale').attr('disabled', false);
				  $('#doc_type_error').css('display', 'none');
		      }
		    }
		  });
		  //VALIDAR RESOLUCIÓN
		}
		$('#paymentModal').on('show.bs.modal', function(e) {
			if (pos_settings.express_payment == 0) {
				setTimeout(function() {
					$('#submit-sale').text('<?=lang('submit');?>').attr('disabled', false);
					calculateTotals();
				}, 3000);
			}
		});
		$('#paymentModal').on('shown.bs.modal', function(e) {
			// $('#amount_1').focus().val(0);
			$('#amount_1').focus();
			$('#quick-payable').click();
		});
		var pi = 'amount_1', pa = 2;
		$(document).on('click', '.quick-cash', function () {
			if ($('#quick-payable').find('span.badge').length) {
				$('#clear-cash-notes').click();
			}
			var $quick_cash = $(this);
			var amt = $quick_cash.contents().filter(function () {
				return this.nodeType == 3;
			}).text();
			var th = ',';
			var $pi = $('#' + pi);
			amt = formatDecimal(amt.split(th).join("")) * 1 + $pi.val() * 1;
			$pi.val(formatDecimal(amt)).focus();
			var note_count = $quick_cash.find('span');
			if (note_count.length == 0) {
				$quick_cash.append('<span class="badge">1</span>');
			} else {
				note_count.text(parseInt(note_count.text()) + 1);
			}
		});
		$(document).on('click', '#quick-payable', function () {
			$('#clear-cash-notes').click();
			$(this).append('<span class="badge">1</span>');
			$('#amount_1').val(formatDecimal(grand_total, 2));
		});
		$(document).on('click', '#clear-cash-notes', function () {
			$('.quick-cash').find('.badge').remove();
			$('#' + pi).val('0').focus();
		});
		$(document).on('change', '.gift_card_no', function () {
			var cn = $(this).val() ? $(this).val() : '';
			var payid = $(this).attr('id'),
			id = payid.substr(payid.length - 1);
			if (cn != '') {
				$.ajax({
					type: "get", async: false,
					url: site.base_url + "sales/validate_gift_card/" + cn,
					dataType: "json",
					success: function (data) {
						if (data === false) {
							$('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
							bootbox.alert('<?=lang('incorrect_gift_card')?>');
						} else if (data.customer_id !== null && data.customer_id !== $('#poscustomer').val()) {
							$('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
							bootbox.alert('<?=lang('gift_card_not_for_customer')?>');
						} else {
							$('#gc_details_' + id).html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
							$('#gift_card_no_' + id).parent('.form-group').removeClass('has-error');
							//calculateTotals();
							$('#amount_' + id).val(gtotal >= data.balance ? data.balance : gtotal).focus();
						}
					}
				});
			}
		});
		$(document).on('click', '.addButton', function () {
			if (pa <= 5) {
				$('#paid_by_1, #pcc_type_1').select2('destroy');
				var phtml = $('#payments').html(),
				update_html = phtml.replace(/_1/g, '_' + pa);
				pi = 'amount_' + pa;
				$('#multi-payment').append('<button type="button" class="close close-payment" style="margin: -10px 0px 0 0;"><i class="fa fa-2x">&times;</i></button>' + update_html);
				$('#paid_by_1, #pcc_type_1, #paid_by_' + pa + ', #pcc_type_' + pa).select2({minimumResultsForSearch: 7});
				read_card();
				pa++;
			} else {
				bootbox.alert('<?=lang('max_reached')?>');
				return false;
			}
			if (KB) { display_keyboards(); }
			$('#paymentModal').css('overflow-y', 'scroll');

			$('.deposit_message:last').remove()
		});
		$(document).on('click', '.close-payment', function () {
			$(this).next().remove();
			$(this).remove();
			pa--;
		});
		$(document).on('focus', '.amount', function () {
			pi = $(this).attr('id');
			calculateTotals();
			input = $(this);
			rows = input.parents('.row').find('select.paid_by');
			paid_by = $(rows[0]).val();
			pay_amount = input.val();
			customer = $('input[name="customer"]').val();
		}).on('blur', '.amount', function () {
			calculateTotals();
		});
		function calculateTotals() {
			var total_paying = 0;
			var ia = $(".amount");
			$.each(ia, function (i) {
				var this_amount = formatCNum($(this).val() ? $(this).val() : 0);
				total_paying += parseFloat(this_amount);
			});
			$('#total_paying').text(formatMoney(total_paying));
			<?php if ($pos_settings->rounding) {?>
				$('#balance').text(formatMoney(total_paying - round_total));
				$('#balance_' + pi).val(formatDecimal(total_paying - round_total));
				total_paid = total_paying;
				grand_total = round_total;
			<?php } else {?>
				$('#balance').text(formatMoney(total_paying - gtotal));
				$('#balance_' + pi).val(formatDecimal(total_paying - gtotal));
				total_paid = total_paying;
				grand_total = gtotal;
			<?php }
			?>
		}
		$("#add_item").autocomplete({
			source: function (request, response) {
				if (!$('#poscustomer').val()) {
					$('#add_item').val('').removeClass('ui-autocomplete-loading');
					bootbox.alert('<?=lang('select_above');?>');
					//response('');
					$('#add_item').focus();
					return false;
				}
				if (localStorage.getItem('balance_label')) {
					localStorage.removeItem('balance_label');
				}
				// Wappsi carne
				<?php if ($pos_settings->balance_settings == 2) { ?>
					console.log('Esta activado el manejo de carnes.');
					console.log("Buscando sugerencias balanza");
					console.log(request);
					var aux = request.term;
					aux = aux.substr(0, 2);
					console.log(aux);
					var peso = 0;
					if(aux == '<?= $pos_settings->balance_label_prefix ?>'){
						//9900050003208
						localStorage.setItem('balance_label', true);
						console.log('El producto es una carne.');
						aux = request.term;
						request.term = aux.substr(2,5);
						peso = aux.substr(7,5);
						console.log("PESO : "+peso);
						//Se van a revisar si los primeros 2 digitos del paso son 0 para determinar si es 0.peso_bascula
						var parteEntera = peso.substr(0,2);
			      var parteDecimal = peso.substr(2,3);
			      console.log("ENTERO : "+parteEntera);
			      console.log("DECIMAL : "+parteDecimal);
						// parteEntera = parseFloat(parteEntera);
						// if(parteEntera == 0){
						// 	peso = peso.substr(2,2);
						// 	peso = '0.'+peso;
						// }
			      		peso = parteEntera+"."+parteDecimal;
						peso = parseFloat(peso);
						console.log(request.term);
					}
				<?php } ?>
				// Termina Wappsi carne
				$.ajax({
					type: 'get',
					url: '<?=admin_url('sales/suggestions');?>',
					dataType: "json",
					data: {
						term: request.term,
						warehouse_id: $("#poswarehouse").val(),
						customer_id: $("#poscustomer").val(),
						biller_id: $("#posbiller").val(),
						address_id: $("#poscustomer_branch").val(),
						// Wappsi carne
						<?php if ($pos_settings->balance_settings == 2) { ?>
							peso: peso
						<?php } ?>
						// Termina Wappsi carne
					},
					success: function (data) {
						$(this).removeClass('ui-autocomplete-loading');
						response(data);
					}
				});
			},
			minLength: 1,
			autoFocus: false,
			delay: 250,
			response: function (event, ui) {
				if ($(this).val().length >= 16 && ui.content[0].id == 0) {
					command: toastr.error('<?=lang('no_match_found')?>', 'Mensaje de validación', { onHidden : function(){  } });
					setTimeout(function() {
						if (site.pos_settings.use_barcode_scanner == 1) {
							$("#add_item").val('');
						}
						$('#add_item').focus();
					}, 1200);
				}
				else if (ui.content.length == 1 && ui.content[0].id != 0) {
					ui.item = ui.content[0];
					$(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
					$(this).autocomplete('close');
				}
				else if (ui.content.length == 1 && ui.content[0].id == 0) {
					command: toastr.error('<?=lang('no_match_found')?>', 'Mensaje de validación', { onHidden : function(){  } });
					setTimeout(function() {
						if (site.pos_settings.use_barcode_scanner == 1) {
							$("#add_item").val('');
						}
						$('#add_item').focus();
					}, 1200);
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {
					if (ui.item.row.cnt_units_prices !== undefined && ui.item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && ui.item.row.promotion != 1) {
						var item_id = ui.item.item_id;
						$('.product_name_spumodal').text(ui.item.label);
				    	var warehouse_id = $('#poswarehouse').val();
				        $('#sPUModal').appendTo("body").modal('show');
				    	add_item_unit(item_id, warehouse_id);
						$(this).val('');
					} else {
						var row = add_invoice_item(ui.item);
						if (row)
						$(this).val('');
					}
				} else {
					bootbox.alert('<?=lang('no_match_found')?>');
				}
			}
		});

		<?php if ($pos_settings->tooltips) {echo '$(".pos-tip").tooltip();';}
		?>
		// $('#posTable').stickyTableHeaders({fixedOffset: $('#product-list')});
		// $('#posTable').stickyTableHeaders({scrollableArea: $('#product-list')});
		$('#product-list, #category-list, #subcategory-list, #brands-list').perfectScrollbar({suppressScrollX: true});
		$('select, .select').select2({minimumResultsForSearch: 7});

		$(document).on('click', '.product', function (e) {
			if (localStorage.getItem('balance_label')) {
				localStorage.removeItem('balance_label');
			}
			$('#modal-loading').show();
			code = $(this).data('code'),
			// code = $(this).val(),
			wh = $('#poswarehouse').val(),
			bl = $('#posbiller').val(),
			cu = $('#poscustomer').val();
			cub = $('#poscustomer_branch').val();
			// console.log("Código : "+code+", Warehouse : "+wh+", Customer : "+cu);
			$.ajax({
				type: "get",
				url: "<?=admin_url('pos/getProductDataByCode')?>",
				data: {code: code, warehouse_id: wh, customer_id: cu, biller_id : bl, address_id : cub},
				dataType: "json",
				success: function (data) {
					e.preventDefault();
					if (data !== null) {
						add_invoice_item(data);
						$('#modal-loading').hide();
						setTimeout(function() {
		        			$("#add_item").focus();
						}, 1200);
					} else {
						bootbox.alert('<?=lang('no_match_found')?>');
						$('#modal-loading').hide();
						setTimeout(function() {
		        			$("#add_item").focus();
						}, 1200);
					}
				}
			});
		});
		$(document).on('click', '.category', function () {
			if (cat_id != $(this).val()) {
				$('#open-category').click();
				$('#modal-loading').show();
				cat_id = $(this).val();
				$.ajax({
					type: "get",
					url: "<?=admin_url('pos/ajaxcategorydata');?>",
					data: {category_id: cat_id},
					dataType: "json",
					success: function (data) {
						$('#item-list').empty();
						var newPrs = $('<div></div>');
						newPrs.html(data.products);
						newPrs.appendTo("#item-list");
						$('#subcategory-list').empty();
						var newScs = $('<div></div>');
						newScs.html(data.subcategories);
						newScs.appendTo("#subcategory-list");
						tcp = data.tcp;
						nav_pointer();
					}
				}).done(function () {
					p_page = 'n';
					$('#category-' + cat_id).addClass('active');
					$('#category-' + ocat_id).removeClass('active');
					ocat_id = cat_id;
					$('#modal-loading').hide();
					nav_pointer();
				});
			}
		});
		$('#category-' + cat_id).addClass('active');
		$(document).on('click', '.brand', function () {
			if (brand_id != $(this).val()) {
				$('#open-brands').click();
				$('#modal-loading').show();
				brand_id = $(this).val();
				$.ajax({
					type: "get",
					url: "<?=admin_url('pos/ajaxbranddata');?>",
					data: {brand_id: brand_id},
					dataType: "json",
					success: function (data) {
						$('#item-list').empty();
						var newPrs = $('<div></div>');
						newPrs.html(data.products);
						newPrs.appendTo("#item-list");
						tcp = data.tcp;
						nav_pointer();
					}
				}).done(function () {
					p_page = 'n';
					$('#brand-' + brand_id).addClass('active');
					$('#brand-' + obrand_id).removeClass('active');
					obrand_id = brand_id;
					$('#category-' + cat_id).removeClass('active');
					$('#subcategory-' + sub_cat_id).removeClass('active');
					cat_id = 0; sub_cat_id = 0;
					$('#modal-loading').hide();
					nav_pointer();
				});
			}
		});
		$(document).on('click', '.subcategory', function () {
			if (sub_cat_id != $(this).val()) {
				$('#open-subcategory').click();
				$('#modal-loading').show();
				sub_cat_id = $(this).val();
				$.ajax({
					type: "get",
					url: "<?=admin_url('pos/ajaxproducts');?>",
					data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
					dataType: "html",
					success: function (data) {
						$('#item-list').empty();
						var newPrs = $('<div></div>');
						newPrs.html(data);
						newPrs.appendTo("#item-list");
					}
				}).done(function () {
					p_page = 'n';
					$('#subcategory-' + sub_cat_id).addClass('active');
					$('#subcategory-' + osub_cat_id).removeClass('active');
					$('#modal-loading').hide();
				});
			}
		});
		$('#last').click(function () {
			if (p_page == 'n') {
				p_page = 0
		}
		paginas = parseInt(tcp / pro_limit);
		p_page = paginas * pro_limit;
		if (tcp >= pro_limit && p_page < tcp) {
			$('#modal-loading').show();
			$.ajax({
				type: "get",
				url: "<?=admin_url('pos/ajaxproducts');?>",
				data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
				dataType: "html",
				success: function (data) {
					$('#item-list').empty();
					var newPrs = $('<div></div>');
					newPrs.html(data);
					newPrs.appendTo("#item-list");
					nav_pointer();
				}
			}).done(function () {
				$('#modal-loading').hide();
			});
		} else {
			p_page = p_page - pro_limit;
		}
		});
		$('#next').click(function () {
			if (p_page == 'n') {
				p_page = 0
			}
			p_page = p_page + pro_limit;
			if (tcp >= pro_limit && p_page < tcp) {
				$('#modal-loading').show();
				$.ajax({
					type: "get",
					url: "<?=admin_url('pos/ajaxproducts');?>",
					data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
					dataType: "html",
					success: function (data) {
						$('#item-list').empty();
						var newPrs = $('<div></div>');
						newPrs.html(data);
						newPrs.appendTo("#item-list");
						nav_pointer();
					}
				}).done(function () {
					$('#modal-loading').hide();
				});
			} else {
				p_page = p_page - pro_limit;
			}
		});

		$('#previous').click(function () {
			if (p_page == 'n') {
				p_page = 0;
		}
		if (p_page != 0) {
			$('#modal-loading').show();
			p_page = p_page - pro_limit;
			if (p_page == 0) {
				p_page = 'n'
			}
			$.ajax({
				type: "get",
				url: "<?=admin_url('pos/ajaxproducts');?>",
				data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
				dataType: "html",
				success: function (data) {
					$('#item-list').empty();
					var newPrs = $('<div></div>');
					newPrs.html(data);
					newPrs.appendTo("#item-list");
					nav_pointer();
				}

			}).done(function () {
				$('#modal-loading').hide();
			});
		}
		});

		$('#first').click(function () {
		if (p_page == 'n') {
			p_page = 0;
		}
		if (p_page != 0) {
			$('#modal-loading').show();
			// p_page = p_page - pro_limit;
			p_page = 0;
			if (p_page == 0) {
				p_page = 'n'
			}
			$.ajax({
				type: "get",
				url: "<?=admin_url('pos/ajaxproducts');?>",
				data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
				dataType: "html",
				success: function (data) {
					$('#item-list').empty();
					var newPrs = $('<div></div>');
					newPrs.html(data);
					newPrs.appendTo("#item-list");
					nav_pointer();
				}

			}).done(function () {
				$('#modal-loading').hide();
			});
		}
		});

		$(document).on('change', '.paid_by', function () {

			var p_val = $(this).val(),
			id = $(this).attr('id'),
			pa_no = id.substr(id.length - 1);
		    var due_payment = $(this).find('option:selected').data('duepayment');
			$('#rpaidby').val(p_val);
			if (p_val == 'cash' || p_val == 'other') {
				$('.pcheque_' + pa_no).hide();
				$('.pcc_' + pa_no).hide();
				$('.pcash_' + pa_no).show();
				$('#amount_' + pa_no).focus();
			} else if (p_val == 'CC' || p_val == 'stripe' || p_val == 'ppp' || p_val == 'authorize') {
				$('.pcheque_' + pa_no).hide();
				$('.pcash_' + pa_no).hide();
				$('.pcc_' + pa_no).show();
				$('#swipe_' + pa_no).focus();
			} else if (p_val == 'Cheque') {
				$('.pcc_' + pa_no).hide();
				$('.pcash_' + pa_no).hide();
				$('.pcheque_' + pa_no).show();
				$('#cheque_no_' + pa_no).focus();
			} else {
				$('.pcheque_' + pa_no).hide();
				$('.pcc_' + pa_no).hide();
				$('.pcash_' + pa_no).hide();
			}
			if (p_val == 'gift_card') {
				$('.gc_' + pa_no).show();
				$('.ngc_' + pa_no).hide();
				$('#gift_card_no_' + pa_no).focus();
			} else {
				$('.ngc_' + pa_no).show();
				$('.gc_' + pa_no).hide();
				$('#gc_details_' + pa_no).html('');
			}

			ipt_index = $('select.paid_by').index($(this));
			input_payment_term_paid_by = $('.input_payment_term_paid_by').eq(ipt_index);
			input_payment_term_amount = $('.input_payment_term_amount').eq(ipt_index);
			input_payment_term_val = $('.input_payment_term_val').eq(ipt_index);
			payment_term_input = $(input_payment_term_val).find('.payment_term_val');
			if (p_val == 'Credito' || due_payment == 1) {
				$(input_payment_term_paid_by).removeClass('col-sm-5');
				$(input_payment_term_amount).removeClass('col-sm-5');
				$(input_payment_term_paid_by).addClass('col-sm-4');
				$(input_payment_term_amount).addClass('col-sm-4');
				$(input_payment_term_val).fadeIn();
				$(payment_term_input).val(1);
				$('#payment_term').val(1);
				$('.addButton').prop('disabled', true);
				$('#amount_' + pa_no).trigger('change');
				$('.due_payment_' + pa_no).val(1);
				$('#payment_term').val(1);
				setTimeout(function() {
					$('.payment_term_val').focus();
				}, 850);
				calculateTotals();
			} else {
				$(input_payment_term_paid_by).removeClass('col-sm-4');
				$(input_payment_term_amount).removeClass('col-sm-4');
				$(input_payment_term_paid_by).addClass('col-sm-5');
				$(input_payment_term_amount).addClass('col-sm-5');
				$(input_payment_term_val).fadeOut();
				$(payment_term_input).val('');
				$('#payment_term').val('');
				$('.addButton').prop('disabled', false);
				$('.due_payment_' + pa_no).val(0);
				$('#payment_term').val(0);
			}

			payment = $(this).closest('.payment');
			paid_by = $(this).val();
			amount_to_pay = payment.find('.amount').val();
			customer = $('#poscustomer').val();
			payment.find('.deposit_message').remove();
			if (paid_by == "deposit") {
			    $.ajax({
			        url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
			    }).done(function(data) {
			        payment.find('.deposit_message').remove();
			        payment.append(data);
			    });
			}

		});

		$(document).on('change', '.payment_term_val', function () {
			$('#payment_term').val($(this).val());
		});

		$(document).on('click', '#submit-sale', function () {

			if ($('#doc_type_id').val() == '') {
				$('#doc_type_error').css('display', '');
				$('#submit-sale').prop('disabled', true);
				$('#doc_type_id').select2('focus');
				return false;
			} else {
				$('#doc_type_error').css('display', 'none');
			}

			if ((total_paid == 0 || formatDecimal(total_paid, 2) < formatDecimal(grand_total, 2)) && $('#paid_by_1').val() != 'Credito') {
				bootbox.prompt({
					title: "<?=lang('paid_l_t_payable');?>",
					placeholder: 'Días de plazo para el pago',
					inputType: 'text',
					callback: function (result) {
						console.log(result);
						if (result == null) {
							$('#payment_term').val(0);
						} else {
							if (result < 0) { result = 0 }
							$('#payment_term').val(result);
							$('#pos_note').val(localStorage.getItem('posnote'));
							$('#staff_note').val(localStorage.getItem('staffnote'));
							$('#submit-sale').text('<?=lang('loading');?>').attr('disabled', true);
							$('#pos-sale-form').submit();
						}
					}
				});

				return false;
			} else {
				$('#pos_note').val(localStorage.getItem('posnote'));
				$('#staff_note').val(localStorage.getItem('staffnote'));
				$(this).text('<?=lang('loading');?>').attr('disabled', true);
				$('#pos-sale-form').submit();
			}
		});

		$('#suspend').click(function () {
			if (count <= 1) {
				bootbox.alert('<?=lang('x_suspend');?>');
				return false;
			} else {
				$('#susModal').modal();
				setTimeout(function() {
					$('#reference_note').focus();
				}, 1000);
			}
		});
		$('#suspend_sale').click(function () {
			ref = $('#reference_note').val();
			if (!ref || ref == '') {
				bootbox.alert('<?=lang('type_reference_note');?>');
				return false;
			} else {
				suspend = $('<span></span>');
				<?php if ($sid) {?>
					suspend.html('<input type="hidden" name="delete_id" value="<?php echo $sid; ?>" /><input type="hidden" name="suspend" value="yes" /><input type="hidden" name="suspend_note" value="' + ref + '" />');
				<?php } else {?>
					suspend.html('<input type="hidden" name="suspend" value="yes" /><input type="hidden" name="suspend_note" value="' + ref + '" />');
				<?php }
				?>
				suspend.appendTo("#hidesuspend");
				$('#total_items').val(count - 1);
				$('#pos-sale-form').submit();

			}
		});

		$('#print_order').click(function () {
			if (count == 1) {
				bootbox.alert('<?=lang('x_total');?>');
				return false;
			}
			<?php if ($pos_settings->remote_printing != 1) { ?>
				printOrder();
			<?php } else { ?>
				Popup($('#order_tbl').html());
			<?php } ?>
		});
		$('#print_bill').click(function () {
			if (count == 1) {
				bootbox.alert('<?=lang('x_total');?>');
				return false;
			}
			<?php if ($pos_settings->remote_printing != 1) { ?>
				printBill();
			<?php } else { ?>
				Popup($('#bill_tbl').html());
			<?php } ?>
		});

		setTimeout(function() {
			// console.log('Entra');
			if ($('#posbiller option').length == 1) {
				$('#posbiller').select2('readonly', true);
			}

			if ($('#doc_type_id option').length == 1) {
				$('#doc_type_id').select2('readonly', true);
			}

			if ($('#poswarehouse option').length == 1) {
				$('#poswarehouse').select2('readonly', true);
			}
		}, 800);

		$('#updateCustomerBranch').click();
	});
	//FIN DOCUMENT READY

	$(function () {
		$(".alert").effect("shake");
		setTimeout(function () {
			$(".alert").hide('blind', {}, 500)
		}, 15000);
		<?php if ($pos_settings->display_time) {?>
			var now = new moment();
			$('#display_time').text(now.format((site.dateFormats.js_sdate).toUpperCase() + " HH:mm"));
			setInterval(function () {
				var now = new moment();
				$('#display_time').text(now.format((site.dateFormats.js_sdate).toUpperCase() + " HH:mm"));
			}, 1000);
		<?php }
		?>
	});
	<?php if ($pos_settings->remote_printing == 1) { ?>
	function Popup(data) {
	var mywindow = window.open('', 'sma_pos_print', 'height=500,width=300');
	mywindow.document.write('<html><head><title>Print</title>');
		mywindow.document.write('<link rel="stylesheet" href="<?=$assets?>styles/helpers/bootstrap.min.css" type="text/css" />');
		mywindow.document.write('</head><body >');
			mywindow.document.write(data);
			mywindow.document.write('</body></html>');
			mywindow.print();
			mywindow.close();
			return true;
		}
	<?php }
	?>

	$(document).on('ifClicked', '.select_auto_2', function(e){
		var index = $('.select_auto_2').index($(this));
		$('.add_item_unit').eq(index).trigger('click');
	});

	$(document).on('keyup', '.select_auto_2', function(e){
            if (e.keyCode == 13) {
                $(this).closest('.add_item_unit').trigger('click');
            }
        });

	// $('#sPUModal').on('shown.bs.modal', function () {
    	// $('#unit_prices_table_filter .input-xs').focus();
	// });

	$(document).on('click', '#searchProduct', function (e) {

        var oTable = $('#items_table').dataTable({
                  aaSorting: [[1, "asc"]],
                  aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                  iDisplayLength: <?= $Settings->rows_per_page ?>,
                  bProcessing: true, 'bServerSide': true,
                  sAjaxSource: site.base_url+"pos/getItemsWholeSale/"+$('#poswarehouse').val(),
	        	  ordering: false,
    			  "bDestroy": true,
                  fnServerData: function (sSource, aoData, fnCallback)
                  {
                    aoData.push({
                      name: "<?= $this->security->get_csrf_token_name() ?>",
                      value: "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback/*, error: function(data) {console.log(data.responseText)}*/ });
                  },
		            fnRowCallback: function (nRow, aData, iDisplayIndex) {
		                var oSettings = oTable.fnSettings();
		                //$("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
		                // nRow.id = aData[0];
		                nRow.setAttribute('data-itemname', aData[2]);
		                nRow.setAttribute('data-itemid', aData[4]);
		                nRow.className = "add_item clickable-row";
		                //if(aData[7] > aData[9]){ nRow.className = "product_link warning"; } else { nRow.className = "product_link"; }
		                return nRow;
		            },
                  aoColumns: [
                    {"mRender": radio, bSortable : false},
                    {bSortable : false},
                    {bSortable : false},
                    {bSortable : false},
                    {bVisible: false, bSortable : false},
                  ],
                  initComplete: function(settings, json)
                  {
                    $('input[type="radio", name="val[]"]:first').focus();
                  }
                }).dtFilter([
                  {column_number: 0, filter_default_label: "[<?=lang('code');?>]", filter_type: "text", mdata: []},
                  {column_number: 1, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", data: []},
                  {column_number: 2, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
                ], "footer");

		$('#items_table').css('width', '100%');
        $('#sPModal').appendTo("body").modal('show');
        $('#items_table thead').remove();

        return false;
    });

    $(document).on('click', '.add_item', function(e) {

    	var item_id = $(this).data('itemid');
    	var item_name = $(this).data('itemname');
    	var warehouse_id = $('#poswarehouse').val();
		$('.product_name_spumodal').text(item_name);
    	add_item_unit(item_id, warehouse_id);

        $('#sPModal').modal('hide');
		$('#unit_prices_table').css('width', '100%');
		setTimeout(function() {
        	$('#sPUModal').appendTo("body").modal('show');
		}, 600);

        e.stopPropagation();
    });

    function get_customer_branches(){
		if ($('#poscustomer').val()) {
			$.ajax({
				url:"<?= admin_url('customers/get_customer_addresses') ?>/"+$('#poscustomer').val(),
			}).done(function(data){
				$('#poscustomer_branch').html(data).select2();
				$('#poscustomer_branch').trigger('change');
				if ($('#poscustomer_branch').val()) {
					$('#address_id').val($('#poscustomer_branch').val());
				}
			});
		}
	}

	$('#poscustomer_branch').on('change', function(){
		$('#address_id').val($(this).val());
	});

	$('#set-customer-branch').on('click', function(){
    	get_customer_branches();
	});

	$('#updateCustomerBranch').on('click', function(){
		cb = $('#poscustomer_branch').val();
		if (!cb) {
			cb = localStorage.getItem('poscustomer_branch');
		}
		localStorage.setItem('poscustomer_branch', cb);
		$('#address_id').val(cb);
		$('#cbModal').modal('hide');
	});

	$(document).on('click', '#deleteProduct', function(){
		$('#add_item').val('');
	});

    function add_item_unit(item_id, warehouse_id){
    	// $('#sPModal').modal('hide');

    	var ooTable = $('#unit_prices_table').dataTable({
			// aaSorting: [[0, "asc"], [2, "desc"]],
			aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
			iDisplayLength: <?= $Settings->rows_per_page ?>,
			bProcessing: true, 'bServerSide': true,
			sAjaxSource: site.base_url+"pos/itemSelectUnit/"+item_id+"/"+warehouse_id+"/"+$('#poscustomer').val(),
			"bDestroy": true,
			fnServerData: function (sSource, aoData, fnCallback)
			{
			aoData.push({
			  name: "<?= $this->security->get_csrf_token_name() ?>",
			  value: "<?= $this->security->get_csrf_hash() ?>"
			});
			$.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback/*, error: function(data) {console.log(data.responseText)}*/ });
			},
			fnRowCallback: function (nRow, aData, iDisplayIndex) {
			    var oSettings = ooTable.fnSettings();
			    //$("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
			    // nRow.id = aData[0];
			    nRow.setAttribute('data-itemunitid', aData[4]);
			    nRow.setAttribute('data-productid', aData[5]);
			    nRow.setAttribute('data-productunitid', aData[6]);
			    nRow.className = "add_item_unit";
			    //if(aData[7] > aData[9]){ nRow.className = "product_link warning"; } else { nRow.className = "product_link"; }
			    return nRow;
			},
			aoColumns: [
			{bSortable: false, "mRender": radio_2},
			{bSortable: false },
			{bSortable: false, className : 'text-right'},
			{bSortable: false, className : 'text-right'},
			{bSortable: false, bVisible: false},
			{bSortable: false, bVisible: false},
			{bSortable: false, bVisible: false},
			],
			initComplete: function(settings, json)
			{
			console.log(json);
			// $('#sPUModal').modal('show');
			}
			}).dtFilter([
			{column_number: 0, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", mdata: []},
			{column_number: 1, filter_default_label: "[<?=lang('price');?>]", filter_type: "text", data: []},
			{column_number: 2, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
			], "footer");

			// $('#unit_prices_table thead').remove();
    }

    $(document).on('click', '.add_item_unit', function() {

    	var product_id = $(this).data('productid');
    	var unit_price_id = $(this).data('itemunitid');
    	var product_unit_id = $(this).data('productunitid');

    	var unit_data = {
    						'product_id' : product_id,
    						'unit_price_id' : unit_price_id,
    						'product_unit_id' : product_unit_id,
    					};

    	localStorage.setItem('unit_data', JSON.stringify(unit_data));
    	$(this).find('.select_auto_2').iCheck('check');
    	$('#unit_quantity').val(1).select();

    });

    $(document).on('keypress', 'input[name="radio_2"]', function(e){
    	if (e.keyCode == 13) {
    		// $(this).closest('.add_item_unit').trigger('click');
    		// $(this).closest('.add_item_unit').remove();
    	}
	});

    $(document).on('keyup', '#unit_quantity', function(e){
    	if (e.keyCode == 13) {
    		if (unit_data = JSON.parse(localStorage.getItem('unit_data'))) {
		    	localStorage.removeItem('unit_data');
	    		var warehouse_id = $('#poswarehouse').val();
	    		var unit_quantity = $(this).val();
		    	$.ajax({
		    		url:site.base_url+"sales/iusuggestions",
		    		type:"get",
		    		data:{
		    				'product_id' : unit_data.product_id,
		    				'unit_price_id' : unit_data.unit_price_id,
		    				'product_unit_id' : unit_data.product_unit_id,
		    				'warehouse_id' : warehouse_id,
		    				'unit_quantity' : unit_quantity,
		    				'biller_id' : $('#posbiller').val(),
		    				'customer_id' : $('#poscustomer').val(),
		    				'address_id' : $('#poscustomer_branch').val(),
		    			}
		    	}).done(function(data){
		    		add_invoice_item(data);
		    		$('#sPUModal').modal('hide');
		    	});
    		} else {
    			$('#unit_quantity').val(1).select();
    		}
    	}
    });

	$('#sPModal').on('shown.bs.modal', function () {
    	$('#items_table_filter .input-xs').focus();
	});

	$('#sPUModal').on('shown.bs.modal', function () {
    	$('#unit_prices_table_filter .input-xs').focus();
    	$('#unit_prices_table_length').remove();
    	$('#unit_prices_table_filter').remove();
    	$('#unit_prices_table_info').remove();
    	$('#unit_prices_table_paginate').remove();
    	$('#unit_quantity').val(1);
    	setTimeout(function() {
    		$('.select_auto_2:first').iCheck('check').focus();
    	}, 200);
	});

    $(document).on('hide.bs.modal', '#sPModal', function (e) {
	  e.stopPropagation() // stops modal from being shown
	});

    $(document).on('hide.bs.modal', '#sPUModal', function (e) {
	  e.stopPropagation() // stops modal from being shown
	});
</script>

<?php
if ( ! $pos_settings->remote_printing) {
?>
<script type="text/javascript">
	var order_printers = <?= json_encode($order_printers); ?>;
	function printOrder() {
		$.each(order_printers, function() {
			var socket_data = { 'printer': this,
			'logo': (biller && biller.logo ? biller.logo : ''),
			'text': order_data };
			$.get('<?= admin_url('pos/p/order'); ?>', {data: JSON.stringify(socket_data)});
		});
		return false;
	}

	function printBill() {
		var socket_data = {
			'printer': <?= json_encode($printer); ?>,
			'logo': (biller && biller.logo ? biller.logo : ''),
			'text': bill_data
		};
		$.get('<?= admin_url('pos/p'); ?>', {data: JSON.stringify(socket_data)});
		return false;
	}
</script>
<?php
} elseif ($pos_settings->remote_printing == 2) {
?>
<script src="<?= $assets ?>js/socket.io.min.js" type="text/javascript"></script>
<script type="text/javascript">
	socket = io.connect('http://localhost:6440', {'reconnection': false});

	function printBill() {
		if (socket.connected) {
			var socket_data = {'printer': <?= json_encode($printer); ?>, 'text': bill_data};
			socket.emit('print-now', socket_data);
			return false;
		} else {
			bootbox.alert('<?= lang('pos_print_error'); ?>');
			return false;
		}
	}

	var order_printers = <?= json_encode($order_printers); ?>;
	function printOrder() {
		if (socket.connected) {
			$.each(order_printers, function() {
				var socket_data = {'printer': this, 'text': order_data};
				socket.emit('print-now', socket_data);
			});
			return false;
		} else {
			bootbox.alert('<?= lang('pos_print_error'); ?>');
			return false;
		}
	}
</script>
<?php

} elseif ($pos_settings->remote_printing == 3) {

?>
<script type="text/javascript">
	try {
		socket = new WebSocket('ws://127.0.0.1:6441');
		socket.onopen = function () {
			console.log('Connected');
			return;
		};
		socket.onclose = function () {
			console.log('Not Connected');
			return;
		};
	} catch (e) {
		console.log(e);
	}

	var order_printers = <?= $pos_settings->local_printers ? "''" : json_encode($order_printers); ?>;
	function printOrder() {
		if (socket.readyState == 1) {

			if (order_printers == '') {

				var socket_data = { 'printer': false, 'order': true,
				'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
				'text': order_data };
				socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));

			} else {

				$.each(order_printers, function() {
					var socket_data = { 'printer': this,
					'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
					'text': order_data };
					socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));
				});

			}
			return false;
		} else {
			bootbox.alert('<?= lang('pos_print_error'); ?>');
			return false;
		}
	}

	function printBill() {
		if (socket.readyState == 1) {
			var socket_data = {
				'printer': <?= $pos_settings->local_printers ? "''" : json_encode($printer); ?>,
				'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
				'text': bill_data
			};
			socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));
			return false;
		} else {
			bootbox.alert('<?= lang('pos_print_error'); ?>');
			return false;
		}
	}



</script>
<?php
}
?>
<script type="text/javascript">
$('.sortable_table tbody').sortable({
	containerSelector: 'tr'
});

$('.pace-done').addClass('mini-navbar');

<?php if (!$sid && !$oid): ?>
	$(document).on('select2-open', '#posbiller', function(){
		console.log('ABRE');
	    localStorage.removeItem('poscustomer');
	});
<?php endif ?>
</script>
<?php
$s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
foreach (lang('select2_lang') as $s2_key => $s2_line) {
$s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
}
$s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
?>
<script type="text/javascript" charset="UTF-8"><?=$s2_file_date?></script>
<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
<?php
if (isset($print) && !empty($print)) {
/* include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'pos'.DIRECTORY_SEPARATOR.'remote_printing.php'; */
include 'remote_printing.php';
}
?>
