<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <script type="text/javascript">if (parent.frames.length !== 0) { top.location = '<?=admin_url()?>'; }</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link href="<?= $assets ?>styles/helpers/login.css" rel="stylesheet"/>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="<?= $assets ?>inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= $assets ?>inspinia/font-awesome-5.8.1/css/fontawesome.css" rel="stylesheet">
    <link href="<?= $assets ?>inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets ?>inspinia/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/select2.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery.validate.min.js"></script>
    <noscript><style type="text/css">#loading { display: none; }</style></noscript>

    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
    <link href="<?= $assets ?>inspinia/css/animate.css" rel="stylesheet">
    <link href="<?= $assets ?>inspinia/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="<?= $assets ?>inspinia/js/plugins/toastr/toastr.min.js"></script>
    <link href="<?= base_url() ?>vendor/icheck-1.x/skins/all.css?v=1.0.2" rel="stylesheet">
    <link href="<?= base_url() ?>vendor/select2-4.0.11/dist/css/select2.css" rel="stylesheet" />
    <script type="text/javascript" src="<?= $assets ?>inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <link href="<?= $assets ?>inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        h2{
        margin-top: 3% !important;
        margin-bottom: 8% !important;
        }

        .middle-box{
        -webkit-box-shadow: 0px 20px 24px 1px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 20px 24px 1px rgba(0,0,0,0.75);
        box-shadow: 0px 20px 24px 1px rgba(0,0,0,0.75);
        }

        .pace-done {
            background-color: #1c84c6 !important;
        }

        .middle-box {
            box-shadow: none !important;
        }

        .input-login {
            padding-left: 42px;
            padding-right: 10px !important;
        }

        svg{
            min-height: 100vh;
            width : 100%;
            background-position : center;
            background-size : cover;
            margin-bottom : -5px;
        }

        .sideRightLogin{
            max-height : 100vh;
        }

        .sideLeftLogin{
            max-height : 100vh;
        }

        #logo_empresa{
            max-width: 300px;
            max-height: 80px;
        }

        <?php if (!empty($this->Settings->entry_image_1)) { ?>
            .carousel-inner #entry_image_1 {
                background-image: url("<?= base_url("/assets/uploads/logos/".$this->Settings->entry_image_1) ?>")
            }
        <?php } ?>

        <?php if (!empty($this->Settings->entry_image_2)) { ?>
            .carousel-inner #entry_image_2 {
                background-image: url("<?= base_url("/assets/uploads/logos/".$this->Settings->entry_image_2) ?>")
            }
        <?php } ?>

        <?php if (!empty($this->Settings->entry_image_3)) { ?>
            .carousel-inner #entry_image_3 {
                background-image: url("<?= base_url("/assets/uploads/logos/".$this->Settings->entry_image_3) ?>")
            }
        <?php } ?>

    </style>
</head>

<?php if ($this->Settings->interconect_login == 1): ?>
    <script type="text/javascript">
      location.href="https://<?= $_SERVER['SERVER_NAME'] ?>/erp";
    </script>
<?php else: ?>
    <body class="login-page">
        <noscript>
            <div class="global-site-notice noscript">
                <div class="notice-inner">
                    <p>
                        <strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                        your browser to utilize the functionality of this website.
                    </p>
                </div>
            </div>
        </noscript>
        <div class="page-back">
        <div class="row loginContent">
            <div class="col-md-7 sideLeftLogin hidden-xs hidden-sm">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php if (!empty($this->Settings->entry_image_1)) { ?>
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <?php } ?>

                        <?php if (!empty($this->Settings->entry_image_2)) { ?>
                            <li data-target="#carousel-example-generic" data-slide-to="1" class="<?= (empty($this->Settings->entry_image_1)) ? 'active': '' ?>"></li>
                        <?php } ?>

                        <?php if (!empty($this->Settings->entry_image_3)) { ?>
                            <li data-target="#carousel-example-generic" data-slide-to="2" class="<?= (empty($this->Settings->entry_image_1) && empty($this->Settings->entry_image_2)) ? 'active': '' ?>"></li>
                        <?php } ?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php if (!empty($this->Settings->entry_image_1)) { ?>
                        <div class="item active">
                            <svg id="entry_image_1"></svg>
                            <div class="carousel-caption"></div>
                        </div>
                        <?php } ?>

                        <?php if (!empty($this->Settings->entry_image_2)) { ?>
                        <div class="item <?= (empty($this->Settings->entry_image_1)) ? 'active': '' ?>">
                            <svg id="entry_image_2"> </svg>
                            <div class="carousel-caption"></div>
                        </div>
                        <?php } ?>

                        <?php if (!empty($this->Settings->entry_image_3)) { ?>
                        <div class="item" <?= (empty($this->Settings->entry_image_1) && empty($this->Settings->entry_image_2)) ? 'active': '' ?>>
                            <svg id="entry_image_3"> </svg>
                            <div class="carousel-caption"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5 sideRightLogin">
                <div class="middle-box text-center animated fadeInDown"  id="login">
                    <div class="login_mobile" id="login_mobile">
                    <?php  if (!empty($this->Settings->logo)): ?>
                        <img src="<?= base_url('/assets/uploads/logos/'.$this->Settings->logo) ?>" id="logo_empresa">
                    <?php endif; ?>    
                    </div>
                    <div class="login-box-body">
                        <?php if ($Settings->mmode) { ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?= lang('site_offline') ?>
                            </div>
                            <?php
                        }
                        if ($error) {
                            ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $error; ?></ul>
                                <?php if ($this->session->userdata('link_log_out')): ?>
                                    <?= $this->session->userdata('link_log_out') ?>
                                    <?php
                                    $this->session->unset_userdata('link_log_out');
                                    ?>
                                <?php endif ?>
                            </div>
                            <?php
                        }
                        if ($message) { ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $message; ?></ul>
                            </div>
                        <?php } ?>

                        <h1 id="register_label"><?= lang('login_subheading'); ?></h1>   

                        <?= admin_form_open("auth/login", 'class="login" data-toggle="validator" style="margin-top: 4px;"'); ?>
                        <div class="form-group has-feedback">
                            <input id="input_name_login" type="text" value="<?= DEMO ? 'owner@tecdiary.com' : ''; ?>" required="required" class="form-control input-login" name="identity"
                                placeholder="<?= lang('username') ?>"/>
                            <span class="glyphicon glyphicon-envelope form-control-feedback icons_login"></span>
                        </div>
                        <div class="form-group has-feedback" style="margin-bottom: 0px;">
                            <input id="input_pass_login" type="password" value="<?= DEMO ? '12345678' : ''; ?>" required="required" class="form-control  input-login" name="password"
                                placeholder="<?= lang('pw') ?>" />
                            <span class="glyphicon glyphicon-lock form-control-feedback icons_login"></span>
                        </div>

                            <?php if ($Settings->captcha) { ?>
                                <div class="col-sm-12">
                                    <div class="textbox-wrap form-group">
                                        <div class="row">
                                            <div class="col-sm-6 div-captcha-left">
                                                <span class="captcha-image"><?= $image; ?></span>
                                            </div>
                                            <div class="col-sm-6 div-captcha-right">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <a href="<?= admin_url('auth/reload_captcha'); ?>" class="reload-captcha">
                                                            <i class="fa fa-refresh"></i>
                                                        </a>
                                                    </span>
                                                    <?= form_input($captcha); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        <div class="row">
                            <div class="col-xs-12 text-left">
                            <div class="form-group rememberContainer">
                                <label class="remember">
                                    <?= form_checkbox('remember', '1', FALSE, 'id="remember" class="skip"'); ?> &nbsp;
                                    <span for="remember" style="font-weight: 100;"><?= lang('remember_me') ?></span>
                                </label>
                                <label class="pull-right">
                                    <a href="#forgot_password" class="forgot_password_link"><?= lang('forgotten_password_recover') ?></a>
                                </label>
                            </div>
                            </div>
                            <div class="col-xs-12">
                                <button id="btnLogin" class="btn btn-primary pull-right btn-block btnLogin" type="submit"><?= lang('login') ?> &nbsp; <i class="fa fa-sign-in"></i></button>
                            </div>
                            <?= form_close(); ?>
                            <div class="col-xs-12">

                            </div>
                            <!-- <div class="col-xs-12">
                                <span style="font-size:80%;">Al iniciar sesión acepto los <a href="https://www.wappsi.com.co/terminos-y-condiciones" target="_blank" style="font-weight: 600;">Términos y condiciones Wappsi</a> y la <a href="https://www.wappsi.com.co/politica-uso-de-datos"  target="_blank" style="font-weight: 600;">Política de uso de datos personales</a></span>
                            </div> -->
                            <div class="col-xs-12" style="margin-top: 5px;">
                                <span style="font-weight: 600;"><?= lang('version') ?> <?= $this->Settings->version ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="forgot_password" style="display: none;">
                    <div class=" container">
                        <div class="login-form-div">
                            <div class="login-content">
                                <?php
                                if ($error) {
                                    ?>
                                    <div class="alert alert-danger">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <ul class="list-group"><?= $error; ?></ul>
                                    </div>
                                    <?php
                                }
                                if ($message) {
                                    ?>
                                    <div class="alert alert-success">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <ul class="list-group"><?= $message; ?></ul>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="div-title col-sm-12">
                                    <h3 class="text-primary"><?= lang('forgot_password') ?></h3>
                                </div>
                                <?= admin_form_open("auth/forgot_password", 'class="login" data-toggle="validator"'); ?>
                                <div class="col-sm-12">
                                    <p>
                                        <?= lang('type_email_to_reset'); ?>
                                    </p>
                                    <div class="textbox-wrap form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon "><i class="fa fa-envelope"></i></span>
                                            <input type="email" name="forgot_email" class="form-control "
                                            placeholder="<?= lang('email_address') ?>" required="required"/>
                                        </div>
                                    </div>
                                    <div class="form-action">
                                        <a class="btn btn-success pull-left login_link" href="#login">
                                            <i class="fa fa-chevron-left"></i> <?= lang('back') ?>
                                        </a>
                                        <button type="submit" class="btn btn-primary pull-right">
                                            <?= lang('submit') ?> &nbsp;&nbsp; <i class="fa fa-envelope"></i>
                                        </button>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>

        <script src="<?= $assets ?>js/jquery.cookie.js"></script>
        <script src="<?= $assets ?>js/login.js"></script>
        <script src="<?= $assets ?>inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="<?= $assets ?>inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?= $assets ?>inspinia/js/inspinia.js"></script>
        <script src="<?= $assets ?>inspinia/js/plugins/pace/pace.min.js"></script>
    </body>
<?php endif ?>
</html>

<script type="text/javascript">
    $(window).load(function () {
        $("#loading").fadeOut("slow");

        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '20%'
        });
    });
</script>