$(document).ready(function(){
    $("#redate").datetimepicker({
        format: site.dateFormats.js_ldate,
        fontAwesome: true,
        language: 'sma',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        enableOnReadonly : false,
        startDate : end_date
    }).datetimepicker('update', new Date());
    if (reref = localStorage.getItem('reref')) {
        $('#reref').val(reref);
    }
    if (rediscount = localStorage.getItem('rediscount')) {
        $('#rediscount').val(rediscount);
    }
    if (retax2 = localStorage.getItem('retax2')) {
        $('#retax2').val(retax2);
    }
    if (return_surcharge = localStorage.getItem('return_surcharge')) {
        $('#return_surcharge').val(return_surcharge);
    }
    if (localStorage.getItem('reitems')) {
        loadItems();
    }
    obtener_documents_types();
    recalcular_re_retenciones();
    cargar_codigo_forma_pago_fe();
    customer_retentions();
    localStorage.removeItem('rereturn_payment_setted');
    $(document).on('change', '#apply_return_order_discount', function(){
        loadItems();
    });
});
$(document).on('change', '.paid_by', function () {
    var p_val = $(this).val();
    var pbnum = $(this).data('pbnum');
    var due_payment = $(this).find('option:selected').data('duepayment');
    localStorage.setItem('paid_by', p_val);
    $('#rpaidby').val(p_val);
    if (p_val == 'cash' ||  p_val == 'other') {
        $('.pcheque_'+pbnum).hide();
        $('.pcc_'+pbnum).hide();
        $('.pcash_'+pbnum).show();
        $('#payment_note_'+pbnum).focus();
        $('.div_slpayment_term_'+pbnum).hide();
        $('#add_more_payments').prop('disabled', false);
    } else if (p_val == 'CC') {
        $('.pcheque_'+pbnum).hide();
        $('.pcash_'+pbnum).hide();
        $('.pcc_'+pbnum).show();
        $('#pcc_no_'+pbnum).focus();
        $('.div_slpayment_term_'+pbnum).hide();
        $('#add_more_payments').prop('disabled', false);
    } else if (p_val == 'Cheque') {
        $('.pcc_'+pbnum).hide();
        $('.pcash_'+pbnum).hide();
        $('.pcheque_'+pbnum).show();
        $('#cheque_no_'+pbnum).focus();
        $('.div_slpayment_term_'+pbnum).hide();
        $('#add_more_payments').prop('disabled', false);
    } else {
        $('.pcheque_'+pbnum).hide();
        $('.pcc_'+pbnum).hide();
        $('.pcash_'+pbnum).hide();
        $('.div_slpayment_term_'+pbnum).hide();
        $('#add_more_payments').prop('disabled', false);
    }
    if (p_val == 'gift_card') {
        if (!paid_by_giftcard) {
            $('.gc_'+pbnum).show();
            $('#gift_card_no_'+pbnum).val(generateCardNo()).prop('readonly', true);
        }
        $('.ngc_'+pbnum).hide();
    } else {
        $('.ngc_'+pbnum).show();
        $('.gc_'+pbnum).hide();
        $('#gc_details_'+pbnum).html('');
    }
    if (due_payment == 1 || p_val == 'Credito') {
        $('.due_payment_'+pbnum).val(1);
        $('.pcc_'+pbnum).hide();
        $('.pcash_'+pbnum).hide();
        $('.pcheque_'+pbnum).hide();
        $('.div_slpayment_term_'+pbnum).show();
        input_slpayment_term = $('.div_slpayment_term_'+pbnum).find('input');
        if ($('#customerpaymentterm').val()) {
            $(input_slpayment_term).val($('#customerpaymentterm').val()).trigger('change');
        }
        $('#add_more_payments').prop('disabled', true);
        var index = $($(this)).index('select.paid_by');
        num_due = 0;
        $.each($('select.paid_by'), function(index, select){
            if ($(select).val() == 'Credito') {
                num_due++;
            }
        });
        if (num_due > 1) {
            $(this).select2('val', 'cash').trigger('change');
            command: toastr.error('No puede escoger más de una forma de pago a crédito.', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
        }
    } else {
        $('.due_payment_'+pbnum).val(0);
    }
    setTimeout(function() {
        cargar_codigo_forma_pago_fe();
    }, 850);
});
/* ------------------------------
 * Sell Gift Card modal
 ------------------------------- */
$(document).on('click', '#sellGiftCard', function (e) {
    $('#gcvalue').val($('#amount_1').val());
    $('#gccard_no').val(generateCardNo());
    $('#gcModal').appendTo("body").modal('show');
    return false;
});
$(document).on('click', '#noCus', function (e) {
    e.preventDefault();
    $('#gccustomer').select2('val', '');
    return false;
});
$('#genNo').click(function () {
    var no = generateCardNo();
    $(this).parent().parent('.input-group').children('input').val(no);
    return false;
});
$(document).on('click', '#addGiftCard', function (e) {
    var mid = (new Date).getTime(),
        gccode = $('#gccard_no').val(),
        gcname = $('#gcname').val(),
        gcvalue = $('#gcvalue').val(),
        gccustomer = $('#gccustomer').val(),
        gcexpiry = $('#gcexpiry').val() ? $('#gcexpiry').val() : '',
        gcprice = parseFloat($('#gcprice').val());
    if (gccode == '' || gcvalue == '' || gcprice == '' || gcvalue == 0 || gcprice == 0) {
        $('#gcerror').text('Please fill the required fields');
        $('.gcerror-con').show();
        return false;
    }
    var gc_data = new Array();
    gc_data[0] = gccode;
    gc_data[1] = gcvalue;
    gc_data[2] = gccustomer;
    gc_data[3] = gcexpiry;
    if (typeof reitems === "undefined") {
        var reitems = {};
    }
    $.ajax({
        type: 'get',
        url: site.base_url + 'sales/sell_gift_card',
        dataType: "json",
        data: {gcdata: gc_data},
        success: function (data) {
            if (data.result === 'success') {
                $('#gift_card_no').val(gccode);
                $('#gc_details').text(lang.gift_card_added);
                $('#gcModal').modal('hide');
            } else {
                $('#gcerror').text(data.message);
                $('.gcerror-con').show();
            }
        }
    });
    return false;
});
var old_row_qty;
$(document).on("focus", '.rquantity', function () {
    old_row_qty = $(this).val();
}).on("change", '.rquantity', function () {
    var row = $(this).closest('tr');
        reitems = JSON.parse(localStorage.getItem('reitems'));
    var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
    if (!is_numeric(new_qty) || (new_qty > reitems[item_id].row.oqty)) {
        $(this).val(old_row_qty);
        bootbox.alert(lang.unexpected_value);
        return false;
    }
    if(new_qty > reitems[item_id].row.oqty) {
        bootbox.alert(lang.unexpected_value);
        $(this).val(old_row_qty);
        return false;
    }
    reitems[item_id].row.base_quantity = new_qty;
    if(reitems[item_id].row.unit != reitems[item_id].row.base_unit) {
        $.each(reitems[item_id].units, function(){
            if (this.id == reitems[item_id].row.unit) {
                reitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
            }
        });
    }
    reitems[item_id].row.qty = new_qty;
    localStorage.setItem('reitems', JSON.stringify(reitems));
    recalcular_re_retenciones();
    loadItems($(this).prop('tabindex'));
});
var old_surcharge;
$(document).on("focus", '#return_surcharge', function () {
    old_surcharge = $(this).val() ? $(this).val() : '0';
}).on("change", '#return_surcharge', function () {
    var new_surcharge = $(this).val() ? $(this).val() : '0';
    if (!is_valid_discount(new_surcharge)) {
        $(this).val(new_surcharge);
        bootbox.alert(lang.unexpected_value);
        return;
    }
    localStorage.setItem('return_surcharge', JSON.stringify(new_surcharge));
    loadItems();
});
$(document).on('click', '.redel', function () {
    var row = $(this).closest('tr');
    var item_id = row.attr('data-item-id');
    delete reitems[item_id];
    row.remove();
    if(reitems.hasOwnProperty(item_id)) { } else {
        localStorage.setItem('reitems', JSON.stringify(reitems));
        loadItems();
        return;
    }
});
$(document).on('change', '#paid_by_1', cargar_codigo_forma_pago_fe);
$("#reference_invoice").autocomplete({
    source: function (request, response) {
        $.ajax({
            type: 'get',
            url: site.base_url+'sales/re_sale_suggestions',
            dataType: "json",
            data: {
                term: request.term,
                biller_id: $("#rebiller").val(),
            },
            success: function (data) {
                $(this).removeClass('ui-autocomplete-loading');
                $('#add_return').prop('disabled', true);
                response(data);
            }
        });
    },
    minLength: 1,
    autoFocus: false,
    delay: 250,
    response: function (event, ui) {
        if (ui.content.length == 1) {
            ui.item = ui.content[0];
            $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
            $(this).autocomplete('close');
            $(this).removeClass('ui-autocomplete-loading');
        }
        else if (ui.content.length == 0) {
            bootbox.alert(lang.no_match_found, function () {
                $('#reference_invoice').focus();
            });
            $(this).removeClass('ui-autocomplete-loading');
            $(this).val('');
        }
    },
    select: function (event, ui) {
        event.preventDefault();
        if (ui.item.id !== 0) {
            $('#reference_invoice').prop('readonly', true);
            $('#affects').val(ui.item.label);
            $('#sale_id').val(ui.item.id);
            localStorage.setItem('reitems', JSON.stringify(ui.item.items));
            localStorage.setItem('rediscount', ui.item.sale.order_discount_id);
            localStorage.setItem('retax2', ui.item.sale.order_tax_id);
            localStorage.setItem('reinvoice_paid', ui.item.sale.paid);
            localStorage.setItem('repayment_status', ui.item.sale.payment_status);
            localStorage.setItem('reshipping', ui.item.sale.shipping);
            localStorage.setItem('resale_balance',  (ui.item.sale.grand_total - (ui.item.sale.paid - (ui.item.sale.rete_fuente_total + ui.item.sale.rete_iva_total + ui.item.sale.rete_ica_total + ui.item.sale.rete_other_total))));
            localStorage.setItem('return_surcharge', '0');
            localStorage.removeItem('re_retenciones');
                if (ui.item.sale.rete_fuente_id || ui.item.sale.rete_iva_id || ui.item.sale.rete_ica_id || ui.item.sale.rete_other_id) {
                    re_retenciones = {
                                'gtotal' : $('#gtotal').text(),
                                'id_rete_fuente' : ui.item.sale.rete_fuente_id,
                                'id_rete_iva' : ui.item.sale.rete_iva_id,
                                'id_rete_ica' : ui.item.sale.rete_ica_id,
                                'id_rete_otros' : ui.item.sale.rete_other_id,
                                'id_rete_bomberil' : ui.item.sale.rete_bomberil_id,
                                'id_rete_autoaviso' : ui.item.sale.rete_autoaviso_id,
                            };
                    localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));
                }
            $(this).val('');
            obtener_documents_types(ui.item.sale.pos, ui.item.sale.biller_id, ui.item.resolution_data.factura_electronica);
            loadItems();
            setTimeout(function() {
                recalcular_re_retenciones();
            }, 800);
        } else {
            bootbox.alert(lang.no_match_found);
        }
    }
});

function cargar_codigo_forma_pago_fe() {
    $('select.paid_by').each(function(index, select){
        index++;
        element_index = $('select.paid_by').index(select);
        element_codigo_fe_forma_pago = $('#paid_by_'+index+' option:selected').data('code_fe');
        $('.mean_payment_code_fe').eq(element_index).val(element_codigo_fe_forma_pago);
    });
}
//localStorage.clear();
//aca load
function loadItems(tabindex = null) {
    if (localStorage.getItem('reitems')) {
        // var trmrate = getTrmRate();
        var trmrate = 1;
        $('#currency').select2('readonly', true);
        subtotal = 0;
        total = 0;
        count = 1;
        ocount = 1;
        an = 1;
        product_tax = 0;
        tip_amount = localStorage.getItem('retip_amount') ? localStorage.getItem('retip_amount') : 0;
        product_tax_2 = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        order_discount_not_applied = 0;
        total_discount = 0;
        var lock_submit = false;
        $("#reTable tbody").empty();
        reitems = JSON.parse(localStorage.getItem('reitems'));
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(reitems, function(o) { return [parseInt(o.order)]; }) : reitems;
        reordersale = localStorage.getItem('reordersale');
        rquantity_readonly = 'readonly';
        $('#edit_sale').attr('disabled', false);
        $.each(sortedItems, function () {
            var item = this;
            // var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            var item_id =  item.item_id ;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id,
                item_type = item.row.type,
                combo_items = item.combo_items,
                item_oqty = item.row.oqty,
                item_qty = item.row.qty,
                item_aqty = item.row.quantity,
                product_unit_id_selected = item.row.product_unit_id_selected,
                item_tax_method = item.row.tax_method,
                item_ds = item.row.discount,
                item_discount = 0,
                item_option = item.row.option,
                item_code = item.row.code,
                item_serial = item.row.serial,
                item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;"),
                product_unit = item.row.unit,
                base_quantity = item.row.base_quantity,
                unit_price = item.row.real_unit_price,
                sale_item_id = item.row.sale_item_id,
                item_price = item.row.net_unit_price,
                pr_tax = item.tax_rate,
                pr_tax_val = item.row.item_tax,
                pr_tax_rate = pr_tax.rate,
                pr_tax_id = pr_tax.id,
                item_ds = item.row.discount,
                item_discount =  item.row.item_discount;
                if (pr_tax === false) {
                    var pr_tax_val = 0,
                        pr_tax_id = 1,
                        pr_tax_rate = 0;
                }
            pr_tax_2_val = item.row.consumption_sale_tax;
            pr_tax_2_rate = item.row.sale_tax_rate_2_percentage ? item.row.sale_tax_rate_2_percentage : pr_tax_2_val;
            pr_tax_2_rate_id = item.row.sale_tax_rate_2_id;

            if (product_id == site.settings.gift_card_product_id) {
                item_qty = item_oqty;
                base_quantity = item_oqty;
            }
            var sel_opt = '';
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.name+" - "+this.code;
                }
            });
            if (othercurrency = localStorage.getItem('othercurrency')) {
                rate = localStorage.getItem('othercurrencyrate');
                if (trm = localStorage.getItem('othercurrencytrm')) {
                    trmrate = rate / trm;
                }
            }
            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                        '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                        '<input name="sale_item_id[]" type="hidden" class="slrid" value="' + sale_item_id + '">'+
                        '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                        '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                        '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                        '<input name="product_seller_id[]" type="hidden" value="'+ (item.row.seller_id !== undefined && item.row.seller_id ? item.row.seller_id : "") +'">'+
                        '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                        '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name +(sel_opt != '' ? ' ('+sel_opt+')' : '')+'</span> '+
                      '</td>';
            if (site.settings.product_serial == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'">'+
                            '</td>';
            }
            if (site.settings.product_variant_per_serial == 1) {
                tr_html += '<td class="text-right">'+
                                '<span> '+(item_serial ? item_serial : '')+'</span>'+
                                '<input name="serial[]" type="hidden" value="'+item_serial+'">'+
                            '</td>';
            }
            // alert(" >> trmrate "+trmrate);
            tr_html += '<td class="text-right">'+
                            '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney((parseFloat(item_price) + parseFloat(item_discount))*trmrate) + '</span>'+
                        '</td>';
            if ((site.settings.product_discount == 1) || item_discount) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '">'+
                                '<input class="form-control input-sm" name="product_discount_val[]" type="hidden" value="' + (item_discount) + '">'+
                                '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' +get_percentage_from_amount(item_ds, (item_price + item_discount))+ '</span>'+
                            '</td>';
            }
            tr_html += '<td class="text-right">'+
                            '<input class="form-control input-sm text-right rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '">'+
                            '<input class="ruprice" name="unit_price[]" type="hidden" value="' + unit_price + '">'+
                            '<input class="realuprice" name="real_unit_price[]" type="hidden" value="' + item.row.real_unit_price + '">'+
                            '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(item_price * trmrate) + '</span>'+
                        '</td>';

            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + pr_tax_id + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax_' + row_no + '" value="' + pr_tax_val + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_rate[]" type="hidden" value="' + formatDecimal(pr_tax_rate) + '%">'+
                                '<input class="form-control input-sm text-right" name="product_tax_val[]" type="hidden" value="' + pr_tax_rate + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + formatDecimal(pr_tax_rate) + '%' +
                            '</td>';
            }
            if (site.settings.ipoconsumo == 3 || site.settings.ipoconsumo == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_2[]" type="hidden" id="product_tax_' + row_no + '_2" value="' + (pr_tax_2_rate_id) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_rate_2[]" type="hidden" value="' + (pr_tax_2_rate) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_' + row_no + '_2" value="' + (pr_tax_2_val) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_val_2[]" type="hidden" value="' + (pr_tax_2_rate) + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '_2">' + (pr_tax_2_rate ? '(' + pr_tax_2_rate + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_2_val * item_qty)) +
                            '</td>';
            }
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">'+
                                '<span class="text-right">' + formatMoney(formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val))) * trmrate)) + '</span>'+
                            '</td>';
            }
            tr_html += '<td>'+
                            '<input class="form-control text-center" type="text" value="' + item_oqty + '" readonly>'+
                        '</td>';
            if (item_oqty <= 0) {
                rquantity_readonly = 'readonly';
            } else {
                if (!reordersale && return_type != 2) {
                    rquantity_readonly = '';
                }
            }
            if (product_id == site.settings.gift_card_product_id) {
                rquantity_readonly = 'readonly';
            }
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + item_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+rquantity_readonly+'>'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + base_quantity + '">'+
                            '<input name="product_aqty[]" type="hidden" class="item_aqty" value="' + item_aqty + '">'+
                            (product_unit_id_selected ? '<input name="product_unit_id_selected[]" type="hidden" class="item_aqty" value="' + product_unit_id_selected + '">' : '')+
                        '</td>';
            tr_html += '<td class="text-right"><span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty))*trmrate) + '</span></td>';
            tr_html += '<td class="text-center"><i class="fa fa-times tip pointer redel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.appendTo("#reTable");
            subtotal += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
            total += formatDecimal(((formatDecimal(item_price) + formatDecimal(pr_tax_val) + formatDecimal(pr_tax_2_val)) * formatDecimal(item_qty)));
            count += parseFloat(item_qty);
            ocount += parseFloat(item_oqty);
            product_tax += parseFloat(pr_tax_val * item_qty);
            product_tax_2 += parseFloat(pr_tax_2_val * item_qty);
            product_discount += (item_discount * item_qty);
            an++;
            if (item_type == 'standard' && item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item_option && base_quantity > this.quantity) {
                        $('#row_' + row_no).addClass('danger');
                        if(site.settings.overselling != 1) {
                            $('#edit_sale').attr('disabled', true);
                            lock_submit = true;
                        }
                    }
                });
            } else if(item_type == 'standard' && base_quantity > item_aqty) {
                $('#row_' + row_no).addClass('danger');
                if(site.settings.overselling != 1) {
                    $('#edit_sale').attr('disabled', true);
                    lock_submit = true;
                }
            } else if (item_type == 'combo') {
                if(combo_items === false) {
                    $('#row_' + row_no).addClass('danger');
                    if(site.settings.overselling != 1) {
                        $('#edit_sale').attr('disabled', true);
                        lock_submit = true;
                    }
                } else {
                    $.each(combo_items, function() {
                       if(parseFloat(this.quantity) < (parseFloat(this.qty)*base_quantity) && this.type == 'standard') {
                           $('#row_' + row_no).addClass('danger');
                            if(site.settings.overselling != 1) {
                                $('#edit_sale').attr('disabled', true);
                                lock_submit = true;
                            }
                       }
                   });
                }
            }
        });
        var col = 2;
        if (site.settings.product_serial == 1) { col++; }
        if (site.settings.product_variant_per_serial == 1) { col++; }
        var tfoot = '<tr id="tfoot" class="tfoot active">'+
                        '<th colspan="'+col+'">Total ('+(an - 1)+')</th>';
        if ((site.settings.product_discount == 1) || product_discount) {
            tfoot += '<th class="text-right">'+formatMoney(trmrate * product_discount)+'</th>';
        }
        tfoot +='<th class="text-right" id="subtotalS">'+formatMoney(trmrate * subtotal)+'</th>';
        if (site.settings.tax1 == 1) {
            // console.log('PR TAX :'+product_tax);
            tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax)+'</th>';
        }
        if (site.settings.ipoconsumo == 3 || site.settings.ipoconsumo == 1) {
            tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax_2)+'</th>';
        }
        if (site.settings.tax1 == 1) {
            tfoot += '<th class="text-right"></th>';
        }
        tfoot +='<th class="text-center"></th>';
        tfoot +='<th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
        tfoot += '<th class="text-right" id="totalS">'+formatMoney(trmrate * total)+'</th>'+
                 '<th class="text-center">'+
                 '  <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>'+
                 '</th>'+
                '</tr>';
        $('#reTable tfoot').html(tfoot);
        // Order level discount calculations
        var distribute_discount = false;
        if (rediscount = localStorage.getItem('rediscount')) {
            var ds = rediscount;
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    // order_discount = formatDecimal((((total) * parseFloat(pds[0])) / 100));
                    order_discount = formatDecimal((((subtotal) * parseFloat(pds[0])) / 100));
                } else {
                    order_discount = formatDecimal(ds);
                }
            } else {
                distribute_discount = true;
                order_discount = formatDecimal(ds);
            }

            if (order_discount > 0 && return_type != 2) {
                if (count != 1 && parseFloat(ocount) == parseFloat(count)) {
                    $('.div_order_discount').fadeOut();
                    $('#apply_return_order_discount').select2('val', 1);
                    header_alert('warning', 'Al ser venta completa, el descuento si se aplicará a la devolución');
                } else {
                    $('.div_order_discount').fadeIn();
                }
            } else {
                $('.div_order_discount').fadeOut();
                header_alert('warning', 'Al ser venta completa, el descuento si se aplicará a la devolución');
            }
            //total_discount += parseFloat(order_discount);
        }
        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (retax2 = localStorage.getItem('retax2')) {
                $.each(tax_rates, function () {
                    if (this.id == retax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        } else if (this.type == 1) {
                            // invoice_tax = formatDecimal((((subtotal - order_discount) * this.rate) / 100));
                            invoice_tax = formatDecimal((((subtotal) * this.rate) / 100));
                        }
                    }
                });
            }
        }
        // re_retenciones
        if (re_retenciones = JSON.parse(localStorage.getItem('re_retenciones'))) {
            total_rete_amount = formatDecimal(re_retenciones.total_rete_amount);
            // console.log('Retención : '+total_rete_amount);
        } else {
            total_rete_amount = 0;
        }
        //re_retenciones
        if (localStorage.getItem('reshipping')) {
            shipping = formatDecimal(localStorage.getItem('reshipping'));
        } else {
            shipping = 0;
        }


        var gtotal = parseFloat(((parseFloat(total) + parseFloat(invoice_tax))));
        percentage_return =  (gtotal * 100) / (inv_grand_total + inv_order_discount - inv_tip_amount);
        if (order_discount > 0 && distribute_discount) {
            order_discount = order_discount * (percentage_return / 100);
        }
        if ($('#apply_return_order_discount').val() == 0) {
            order_discount_not_applied = order_discount;
            order_discount = 0;
        }
        $('#rediscount').val(order_discount);
        $('#rediscount_not_applied').val(order_discount_not_applied);
        total_discount = parseFloat(order_discount + product_discount);

        total_payment_discount = 0;
        $('input[name="discount_amount[]"]').each(function(index, input_discount){
            prorrated_discount = formatDecimal(formatDecimal($(input_discount).data('originalamount')) * (percentage_return/100));
            $(input_discount).val(prorrated_discount);
            total_payment_discount+=prorrated_discount;
        });


        // Totals calculations after item addition
        var gtotal = formatDecimal(parseFloat(((parseFloat(total) + parseFloat(invoice_tax)) - parseFloat(order_discount))), site.settings.decimals);
        // percentage_return =  (gtotal * 100) / (inv_grand_total - inv_tip_amount);
        localStorage.setItem('percentage_return', percentage_return);
        if (tip_amount > 0) {
            tip_amount = tip_amount * (percentage_return / 100);
            gtotal += tip_amount;
            $('#tip_amount').val(tip_amount);
        }
        $('#amount_1').data('maxval', gtotal).prop('readonly', false);
        repayment_status = localStorage.getItem('repayment_status');
        reinvoice_paid = localStorage.getItem('reinvoice_paid');
        resale_balance = localStorage.getItem('resale_balance');
        total_to_pay = 0;
        if (repayment_status == 'paid' || repayment_status == 'partial') {
            $('.amount').each(function(index, input){
                $(input).val(0);
            });
            total_rete_amount = 0;
            if (re_retenciones = JSON.parse(localStorage.getItem('re_retenciones'))) {
                total_rete_amount = re_retenciones.total_rete_amount;
            }
            if ((gtotal - formatDecimal(total_rete_amount) - amount_discounted) <= reinvoice_paid) {
                total_to_pay = gtotal - formatDecimal(total_rete_amount) - total_payment_discount;
                $('.amount_to_paid').val(formatMoney(gtotal - formatDecimal(total_rete_amount) - total_payment_discount));
                $('#amount_1').val(formatDecimal(gtotal - formatDecimal(total_rete_amount) - total_payment_discount));
            } else {
                total_to_pay = gtotal - resale_balance;
                $('.amount_to_paid').val(formatMoney(gtotal - resale_balance));
                $('#amount_1').val(formatDecimal(gtotal - resale_balance));
            }
            console.log($('#amount_1').val());
            $('#discounted').val(formatDecimal(total_payment_discount));
        }

        $('#total').text(formatMoney((total - (product_tax + product_tax_2)) * trmrate));
        $('#ipoconsumo_total').text(formatMoney(product_tax_2 * trmrate));
        $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
        $('#total_items').val((parseFloat(count) - 1));
        $('#tds').text(formatMoney(order_discount * trmrate));
        $('#ttax1').text(formatMoney(product_tax * trmrate));
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(invoice_tax * trmrate));
        }
        $('#tship').text(formatMoney(shipping * trmrate));
        $('#gtotal').text(formatMoney(gtotal * trmrate));
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            // $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            // $(window).scrollTop($(window).scrollTop() + 1);
        }
        if (count > 1) {
            $('#recustomer').select2("readonly", true);
            $('#recustomerbranch').select2("readonly", true);
            $('#rewarehouse').select2("readonly", true);
            $('#rediscount').prop("readonly", true);
            $('#reshipping').prop("readonly", true);
        }
        if (lock_submit) {
            localStorage.setItem('locked_for_items_quantity', 1);
        } else {
            if (localStorage.getItem('locked_for_items_quantity')) {
                localStorage.removeItem('locked_for_items_quantity');
            }
        }
        if (re_retenciones = JSON.parse(localStorage.getItem('re_retenciones'))) {
            re_retenciones.gtotal = $('#gtotal').text();
            localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));
        }
        // verify_locked_submit();
        set_page_focus(tabindex);
        verify_payment_register(gtotal-total_payment_discount-total_rc_rete_amount, total_to_pay);
    }
}


$(document).on('click', '#rete_fuente', function(){
    if ($(this).is(':checked')) {
         $.ajax({
            url: site.base_url+"sales/opcionesWithHolding/FUENTE"
        }).done(function(data){
            $('#rete_fuente_option').html(data);
            $('#rete_fuente_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#rete_fuente_option').select2('val', '').attr('disabled', true).select();
        $('#rete_fuente_tax').val('');
        $('#rete_fuente_valor').val('');
        setReteTotalAmount(rete_fuente_amount, '-');
    }
});
$(document).on('click', '#rete_iva', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"sales/opcionesWithHolding/IVA"
        }).done(function(data){
            $('#rete_iva_option').html(data);
            $('#rete_iva_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#rete_iva_option').select2('val', '').attr('disabled', true).select();
        $('#rete_iva_tax').val('');
        $('#rete_iva_valor').val('');
        setReteTotalAmount(rete_iva_amount, '-');
    }
});
$(document).on('click', '#rete_ica', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"sales/opcionesWithHolding/ICA"
        }).done(function(data){
            $('#rete_ica_option').html(data);
            $('#rete_ica_option').attr('disabled', false).select();
        }).fail(function(data){
            // console.log(data);
        });
    } else {
        $('#rete_ica_option').select2('val', '').attr('disabled', true).select();
        $('#rete_ica_tax').val('');
        $('#rete_ica_valor').val('');
        // reset_rete_bomberil();
        setReteTotalAmount(rete_ica_amount, '-');
    }
});
$(document).on('click', '#rete_otros', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"sales/opcionesWithHolding/OTRA"
        }).done(function(data){
            $('#rete_otros_option').html(data);
            $('#rete_otros_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#rete_otros_option').select2('val', '').attr('disabled', true).select();
        $('#rete_otros_tax').val('');
        $('#rete_otros_valor').val('');
        setReteTotalAmount(rete_otros_amount, '-');
    }
});
$(document).on('click', '#rete_bomberil', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"sales/opcionesWithHolding/BOMB"
        }).done(function(data){
            $('#rete_bomberil_option').html(data);
            $('#rete_bomberil_option').attr('disabled', false).select();
        }).fail(function(data){
            // console.log(data);
        });
    } else {
        $('#rete_bomberil_option').select2('val', '').attr('disabled', true).select();
        $('#rete_bomberil_tax').val('');
        $('#rete_bomberil_valor').val('');
        setReteTotalAmount(rete_bomberil_amount, '-');
    }
});
$(document).on('click', '#rete_autoaviso', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"sales/opcionesWithHolding/TABL"
        }).done(function(data){
            $('#rete_autoaviso_option').html(data);
            $('#rete_autoaviso_option').attr('disabled', false).select();
        }).fail(function(data){
            // console.log(data);
        });
    } else {
        $('#rete_autoaviso_option').select2('val', '').attr('disabled', true).select();
        $('#rete_autoaviso_tax').val('');
        $('#rete_autoaviso_valor').val('');
        setReteTotalAmount(rete_autoaviso_amount, '-');
    }
});
var rete_fuente_amount = 0;
var rete_ica_amount = 0;
var rete_iva_amount = 0;
var rete_otros_amount = 0;
$(document).on('change', '#rete_fuente_option', function(){
    rete_fuente_id = $(this).val();
    $('#rete_fuente_id').val(rete_fuente_id);
    trmraterete = 1;
    prevamnt = $('#rete_fuente_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_fuente_option option:selected').data('percentage');
    apply = $('#rete_fuente_option option:selected').data('apply');
    account = $('#rete_fuente_option option:selected').data('account');
    min_base = $('#rete_fuente_option option:selected').data('minbase');
    min_base = min_base * (parseFloat(localStorage.getItem('percentage_return')) / 100);
    $('#rete_fuente_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
    re_validate_min_retention = localStorage.getItem('re_validate_min_retention');
    // if (!re_validate_min_retention || (amount >= min_base || customer_validate_min_base_retention == 'false')) {
    if ((amount >= min_base || customer_validate_min_base_retention == 'false') && formatDecimal(rete_applied_total.rete_fuente_total) > 0) {
        $('#rete_fuente_base').val(amount);
        cAmount = amount * (percentage / 100);
        if (trmraterete == 1 && site.settings.rounding == 1) {
            cAmount = Math.round(cAmount);
        } else {
            cAmount = formatDecimal(cAmount);
        }
        $('#rete_fuente_tax').val(percentage);
        $('#rete_fuente_valor').val(cAmount);
        rete_fuente_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_fuente_amount, '+');
    }
});
$(document).on('change', '#rete_iva_option', function(){
    rete_iva_id = $(this).val();
    $('#rete_iva_id').val(rete_iva_id);
    trmraterete = 1;
    prevamnt = $('#rete_iva_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_iva_option option:selected').data('percentage');
    apply = $('#rete_iva_option option:selected').data('apply');
    account = $('#rete_iva_option option:selected').data('account');
    min_base = $('#rete_iva_option option:selected').data('minbase');
    min_base = min_base * (parseFloat(localStorage.getItem('percentage_return')) / 100);
    $('#rete_iva_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
    re_validate_min_retention = localStorage.getItem('re_validate_min_retention');
    // if (!re_validate_min_retention || (amount >= min_base || customer_validate_min_base_retention == 'false')) {
    if ((amount >= min_base || customer_validate_min_base_retention == 'false') && formatDecimal(rete_applied_total.rete_iva_total) > 0) {
        $('#rete_iva_base').val(amount);
        cAmount = amount * (percentage / 100);
        if (trmraterete == 1 && site.settings.rounding == 1) {
            cAmount = Math.round(cAmount);
        } else {
            cAmount = formatDecimal(cAmount);
        }
        $('#rete_iva_tax').val(percentage);
        $('#rete_iva_valor').val(cAmount);
        rete_iva_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_iva_amount, '+');
    }
});
$(document).on('change', '#rete_ica_option', function(){
    rete_ica_id = $(this).val();
    $('#rete_ica_id').val(rete_ica_id);
    trmraterete = 1;
    prevamnt = $('#rete_ica_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_ica_option option:selected').data('percentage');
    apply = $('#rete_ica_option option:selected').data('apply');
    account = $('#rete_ica_option option:selected').data('account');
    min_base = $('#rete_ica_option option:selected').data('minbase');
    min_base = min_base * (parseFloat(localStorage.getItem('percentage_return')) / 100);
    $('#rete_ica_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
    re_validate_min_retention = localStorage.getItem('re_validate_min_retention');
    // if (!re_validate_min_retention || (amount >= min_base || customer_validate_min_base_retention == 'false')) {
    if ((amount >= min_base || customer_validate_min_base_retention == 'false') && formatDecimal(rete_applied_total.rete_ica_total) > 0) {
        $('#rete_ica_base').val(amount);
        cAmount = amount * (percentage / 100);
        if (trmraterete == 1 && site.settings.rounding == 1) {
            cAmount = Math.round(cAmount);
        } else {
            cAmount = formatDecimal(cAmount);
        }
        $('#rete_ica_tax').val(percentage);
        $('#rete_ica_valor').val(cAmount);
        rete_ica_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_ica_amount, '+');
    }
});
$(document).on('change', '#rete_otros_option', function(){
    rete_otros_id = $(this).val();
    $('#rete_otros_id').val(rete_otros_id);
    trmraterete = 1;
    prevamnt = $('#rete_otros_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_otros_option option:selected').data('percentage');
    apply = $('#rete_otros_option option:selected').data('apply');
    account = $('#rete_otros_option option:selected').data('account');
    min_base = $('#rete_otros_option option:selected').data('minbase');
    min_base = min_base * (parseFloat(localStorage.getItem('percentage_return')) / 100);
    $('#rete_otros_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
    re_validate_min_retention = localStorage.getItem('re_validate_min_retention');
    // if (!re_validate_min_retention || (amount >= min_base || customer_validate_min_base_retention == 'false')) {
    if ((amount >= min_base || customer_validate_min_base_retention == 'false')  && formatDecimal(rete_applied_total.rete_other_total) > 0) {
        $('#rete_otros_base').val(amount);
        cAmount = amount * (percentage / 100);
        if (trmraterete == 1 && site.settings.rounding == 1) {
            cAmount = Math.round(cAmount);
        } else {
            cAmount = formatDecimal(cAmount);
        }
        $('#rete_otros_tax').val(percentage);
        $('#rete_otros_valor').val(cAmount);
        rete_otros_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_otros_amount, '+');
    }
});
$(document).on('change', '#rete_bomberil_option', function(){
    rete_bomberil_id = $(this).val();
    $('#rete_bomberil_id').val(rete_bomberil_id);
    trmraterete = 1;
    prevamnt = $('#rete_bomberil_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_bomberil_option option:selected').data('percentage');
    apply = $('#rete_bomberil_option option:selected').data('apply');
    account = $('#rete_bomberil_option option:selected').data('account');
    min_base = $('#rete_bomberil_option option:selected').data('minbase');
    min_base = min_base * (parseFloat(localStorage.getItem('percentage_return')) / 100);
    $('#rete_bomberil_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
    re_validate_min_retention = localStorage.getItem('re_validate_min_retention');
    // if (!re_validate_min_retention || (amount >= min_base || customer_validate_min_base_retention == 'false')) {
    if ((amount >= min_base || customer_validate_min_base_retention == 'false')) {
        $('#rete_bomberil_base').val(amount);
        cAmount = amount * (percentage / 100);
        if (trmraterete == 1 && site.settings.rounding == 1) {
            cAmount = Math.round(cAmount);
        } else {
            cAmount = formatDecimal(cAmount);
        }
        $('#rete_bomberil_tax').val(percentage);
        $('#rete_bomberil_valor').val(cAmount);
        rete_bomberil_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_bomberil_amount, '+');
    }
});
$(document).on('change', '#rete_autoaviso_option', function(){
    rete_autoaviso_id = $(this).val();
    $('#rete_autoaviso_id').val(rete_autoaviso_id);
    trmraterete = 1;
    prevamnt = $('#rete_autoaviso_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#rete_autoaviso_option option:selected').data('percentage');
    apply = $('#rete_autoaviso_option option:selected').data('apply');
    account = $('#rete_autoaviso_option option:selected').data('account');
    min_base = $('#rete_autoaviso_option option:selected').data('minbase');
    min_base = min_base * (parseFloat(localStorage.getItem('percentage_return')) / 100);
    $('#rete_autoaviso_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
    re_validate_min_retention = localStorage.getItem('re_validate_min_retention');
    // if (!re_validate_min_retention || (amount >= min_base || customer_validate_min_base_retention == 'false')) {
    if ((amount >= min_base || customer_validate_min_base_retention == 'false')) {
        $('#rete_autoaviso_base').val(amount);
        cAmount = amount * (percentage / 100);
        if (trmraterete == 1 && site.settings.rounding == 1) {
            cAmount = Math.round(cAmount);
        } else {
            cAmount = formatDecimal(cAmount);
        }
        $('#rete_autoaviso_tax').val(percentage);
        $('#rete_autoaviso_valor').val(cAmount);
        rete_autoaviso_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_autoaviso_amount, '+');
    }
});
function getReteAmount(apply){
    amount = 0;
    if (apply == "ST") {
        amount = $('#subtotalS').text();
    } else if (apply == "TX") {
        amount = $('#ivaamount').text();
    } else if (apply == "TO") {
        amount = $('#totalS').text();
    } else if (apply == "IC") {
        amount = $('#rete_ica_valor').val();
    }
    console.log("amount >> "+amount);
    console.log("apply >> "+apply);
    return amount;
}
function setReteTotalAmount(amount, action){
    // // console.log('Monto : '+amount+' acción : '+action);
    rtotal_rete =
                formatDecimal($('#rete_fuente_valor').val()) +
                formatDecimal($('#rete_iva_valor').val()) +
                formatDecimal($('#rete_ica_valor').val()) +
                formatDecimal($('#rete_otros_valor').val())+
                formatDecimal($('#rete_bomberil_valor').val())+
                formatDecimal($('#rete_autoaviso_valor').val())
                ;
    // $('#total_rete_amount_show').text(formatMoney(trmrate * rtotal_rete));
    $('#total_rete_amount').text(formatMoney(rtotal_rete));
}
$(document).on('click', '#updateOrderRete', function () {
    $('#ajaxCall').fadeIn();
    rete_prev = JSON.parse(localStorage.getItem('re_retenciones'));
    var re_retenciones = {
        'gtotal' : (rete_prev && rete_prev.gtotal > 0 ? rete_prev.gtotal : parseFloat(formatDecimal($('#gtotal').text()))),
        'total_rete_amount' : $('#total_rete_amount').text(),
        'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
        'id_rete_fuente' : $('#rete_fuente_option').val(),
        'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
        'id_rete_iva' : $('#rete_iva_option').val(),
        'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
        'id_rete_ica' : $('#rete_ica_option').val(),
        'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
        'id_rete_otros' : $('#rete_otros_option').val(),
        'bomberil_option' : $('#rete_bomberil_option option:selected').data('percentage'),
        'id_rete_bomberil' : $('#rete_bomberil_option').val(),
        'autoaviso_option' : $('#rete_autoaviso_option option:selected').data('percentage'),
        'id_rete_autoaviso' : $('#rete_autoaviso_option').val(),
        };
    re_retenciones.total_discounted = formatMoney(parseFloat(formatDecimal((rete_prev && rete_prev.gtotal > 0 ? rete_prev.gtotal : parseFloat(formatDecimal($('#gtotal').text()))))) - parseFloat(formatDecimal($('#total_rete_amount').text())));

    setTimeout(function() {
        $('#gtotal').text(re_retenciones.total_discounted);
        $('#ajaxCall').fadeOut();
    }, 1000);
    localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));
    if (parseFloat(formatDecimal(re_retenciones.total_rete_amount)) > 0) {
        $('#rete_applied').val(1);
        $('#gtotal_rete_amount').val(parseFloat(formatDecimal(re_retenciones.total_rete_amount)))
    } else {
        $('#rete_applied').val(0);
    }
    $('#rete').val(re_retenciones.total_rete_amount);
    $('#rtModal').modal('hide');
 });
$('#rtModal').on('hidden.bs.modal', function () {
    // loadItems();
});
$(document).on('click', '#cancelOrderRete', function(){
    localStorage.removeItem('re_retenciones');
    loadItems();
    $('#rete_fuente').prop('checked', true).trigger('click');
    $('#rete_iva').prop('checked', true).trigger('click');
    $('#rete_ica').prop('checked', true).trigger('click');
    $('#rete_otros').prop('checked', true).trigger('click');
    $('#updateOrderRete').trigger('click');
});
function recalcular_re_retenciones(){
    re_retenciones = JSON.parse(localStorage.getItem('re_retenciones'));
    $('#add_return').prop('disabled', true);
    if (re_retenciones != null) {
        if (re_retenciones.id_rete_fuente > 0) {
            if (!$('#rete_fuente').is(':checked')) {
                $('#rete_fuente').trigger('click');
            }
        }
        if (re_retenciones.id_rete_iva > 0) {
            if (!$('#rete_iva').is(':checked')) {
                $('#rete_iva').trigger('click');
            }
        }
        if (re_retenciones.id_rete_ica > 0) {
            if (!$('#rete_ica').is(':checked')) {
                $('#rete_ica').trigger('click');
            }
        }
        if (re_retenciones.id_rete_otros > 0) {
            if (!$('#rete_otros').is(':checked')) {
                $('#rete_otros').trigger('click');
            }
        }
        setTimeout(function() {
            $('#rete_bomberil').prop('disabled', false);
            $('#rete_autoaviso').prop('disabled', false);

            if (re_retenciones.id_rete_bomberil > 0) {
                if (!$('#rete_bomberil').is(':checked')) {
                    $('#rete_bomberil').trigger('click');
                }
            }
            if (re_retenciones.id_rete_autoaviso > 0) {
                if (!$('#rete_autoaviso').is(':checked')) {
                    $('#rete_autoaviso').trigger('click');
                }
            }
        }, 1000);
        setTimeout(function() {
            verify_percentage_for_retention();
            $.each($('#rete_fuente_option option'), function(index, value){
                if(re_retenciones.id_rete_fuente > 0 && value.value == re_retenciones.id_rete_fuente){
                    $('#rete_fuente_option').select2('val', value.value).trigger('change');
                }
            });
            $.each($('#rete_iva_option option'), function(index, value){
                if(re_retenciones.id_rete_iva > 0 && value.value == re_retenciones.id_rete_iva){
                    $('#rete_iva_option').select2('val', value.value).trigger('change');
                }
            });
            $.each($('#rete_ica_option option'), function(index, value){
                if(re_retenciones.id_rete_ica > 0 && value.value == re_retenciones.id_rete_ica){
                    $('#rete_ica_option').select2('val', value.value).trigger('change');
                }
            });
            $.each($('#rete_otros_option option'), function(index, value){
                if(re_retenciones.id_rete_otros > 0 && value.value == re_retenciones.id_rete_otros){
                    $('#rete_otros_option').select2('val', value.value).trigger('change');
                }
            });
            $.each($('#rete_bomberil_option option'), function(index, value){
                if(re_retenciones.id_rete_bomberil > 0 && value.value == re_retenciones.id_rete_bomberil){
                    $('#rete_bomberil_option').select2('val', value.value).trigger('change');
                }
            });
            $.each($('#rete_autoaviso_option option'), function(index, value){
                if(re_retenciones.id_rete_autoaviso > 0 && value.value == re_retenciones.id_rete_autoaviso){
                    $('#rete_autoaviso_option').select2('val', value.value).trigger('change');
                }
            });
            $('#updateOrderRete').trigger('click');
            $('#add_return').prop('disabled', false);
            loadItems();
        }, 2000);
    } else {
        $('#add_return').prop('disabled', false);
    }
}
function verify_payment_register(gtotal, total_to_pay){
    paid = parseFloat(localStorage.getItem('reinvoice_paid'));
    payment_status = localStorage.getItem('repayment_status');
    sale_balance = parseFloat(localStorage.getItem('resale_balance'));
    total_rete_amount = 0;
    if (rete = JSON.parse(localStorage.getItem('re_retenciones'))) {
        total_rete_amount = parseFloat(formatDecimal(rete.total_rete_amount));
    }
    if (payment_status == 'partial') {
        if (gtotal > sale_balance ) {
            $('#payments').fadeIn();
            // $('#amount_1').val(gtotal - sale_balance);
            // $('.amount_to_paid').val(formatMoney(gtotal - sale_balance));
            if (site.settings.default_return_payment_method == '0' && deposit_advertisement_showed == false) {
                deposit_advertisement_showed = true;
                setTimeout(function() {
                    bootbox.confirm({
                        message: '<p style="font-size=150%;">El valor de la devolución <b>('+formatMoney(gtotal)+')</b> supera el saldo de la factura afectada  <b>('+formatMoney(sale_balance)+')</b>, el valor restante <b>('+formatMoney(gtotal - sale_balance)+')</b>, ¿Cómo desea devolverlo?',
                        buttons: {
                            confirm: {
                                label: 'Registrar cómo anticipo',
                                className: 'btn-success send-submit-sale btn-full'
                            },
                            cancel: {
                                label: 'Formas de pago originales',
                                className: 'btn-danger btn-full'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $('#paid_by_1').select2('val', 'deposit').trigger('change');
                                localStorage.setItem('rereturn_payment_setted', 1);
                                set_return_payments(gtotal - sale_balance);
                            } else {
                                set_return_payments(gtotal - sale_balance);
                                $('#paid_by_1').trigger('change');
                            }
                        }
                    });
                }, 800);
            } else if (site.settings.default_return_payment_method == '1') {
                setTimeout(function() {
                    $('#paid_by_1').select2('val', site.settings.default_return_payment_method).trigger('change').select2('readonly', true);
                }, 800);
            } else {
            }
        } else {
            $('#payments').fadeOut();
            $('#amount_1').val(0);
            $('.amount_to_paid').val(formatMoney(0));
        }
    } else if (payment_status == 'pending' || payment_status == 'due') {
        $('#amount_1').val(0);
        $('.amount_to_paid').val(formatMoney(0));
    } else if (payment_status == 'paid') {
        $('#payments').fadeIn();
        if (site.settings.default_return_payment_method == '0') {
            if (deposit_payment_method.sale == true && deposit_advertisement_showed == false) {
                deposit_advertisement_showed = true;
                setTimeout(function() {
                    bootbox.confirm({
                        message: 'La factura no tiene saldo, el valor total de la devolución <b>('+formatMoney(gtotal)+')</b>, ¿Cómo desea devolverlo?',
                        buttons: {
                            confirm: {
                                label: 'Registrar cómo anticipo',
                                className: 'btn-success send-submit-sale btn-full'
                            },
                            cancel: {
                                label: 'Formas de pago originales',
                                className: 'btn-danger btn-full'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $('#paid_by_1').select2('val', 'deposit').trigger('change');
                                localStorage.setItem('rereturn_payment_setted', 1);
                            } else {
                                set_return_payments();
                                $('#paid_by_1').trigger('change');
                            }
                        }
                    });
                }, 800);
            } else if (deposit_payment_method.sale == false || (deposit_payment_method.sale == true && deposit_advertisement_showed == true)) {
                set_return_payments(gtotal);
            }
        } else {
            setTimeout(function() {
                $('#paid_by_1').select2('val', site.settings.default_return_payment_method).trigger('change').select2('readonly', true);
            }, 800);
        }
    }
    $('#discounted').val(formatDecimal(total_payment_discount));
}
function customer_retentions(){
    var customer_validate_min_base_retention = false;
    $.ajax({
        url : site.base_url+"customers/get_customer_by_id/"+customer_id,
        dataType : "JSON"
    }).done(function(data){
        if (data.customer_validate_min_base_retention == 0) {
            customer_validate_min_base_retention = true;
        }
        localStorage.setItem('customer_validate_min_base_retention', customer_validate_min_base_retention);
    });
}
function verify_percentage_for_retention(){
    var inv_total = parseFloat(inv_grand_total);
    localStorage.removeItem('re_validate_min_retention');
    return_total = parseFloat(formatDecimal($('#gtotal').text()));
    // console.log('return_total '+return_total);
    percentage = (return_total * 100) / inv_total;
    if (percentage > 80) {
        localStorage.setItem('re_validate_min_retention', true);
    }
}
$(document).on('change', 'select.paid_by', function(){
    pbnum = $(this).data('pbnum');
    paid_by = $(this).val();
    if (paid_by == 'deposit') {
        $('.deposit_div_'+pbnum).fadeIn();
        $.ajax({
            url:site.base_url+'billers/getBillersDocumentTypes/30/'+$('#rebiller').val(),
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('.deposit_document_type_id[data-pbnum="'+pbnum+'"]').html(response.options).select2();
            reinvoice_paid = parseFloat(localStorage.getItem('reinvoice_paid'));
            $('#amount_'+pbnum).prop('readonly', true);
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }
            $('.deposit_document_type_id[data-pbnum="'+pbnum+'"]').trigger('change');
        });
    } else {
        $('.deposit_div_'+pbnum).fadeOut();
        $('#amount_'+pbnum).prop('readonly', false);
    }
});
function reset_rete_bomberil(){
    $('#rete_bomberil').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_autoaviso').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_bomberil_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_autoaviso_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_bomberil_tax').val('');
    $('#rete_bomberil_valor').val('');
    $('#rete_autoaviso_tax').val('');
    $('#rete_autoaviso_valor').val('');
}

$(document).on('click', '#add_more_payments', function(){
    add_more_payment();
});

function add_more_payment(paid_by = null, amount = 0){
    var num = $('.pay_option').length;
    num++;
    $.ajax({
        url : site.base_url+'sales/get_paid_opts',
        data : { paid_by : paid_by, amount : amount  }
    }).done(function(paid_opts){
        html = '<div class="col-md-12 pay_option">'+
                   '<div class="well well-sm well_'+num+'">'+
                       '<div class="col-md-'+num+'2">'+
                           '<div class="row">'+
                                '<div class="col-sm-4 deposit_div_'+num+'" style="display:none;">'+
                                    lang.deposit_document_type_id+
                                    '<select name="deposit_document_type_id[]" class="form-control deposit_document_type_id" data-pbnum="'+num+'">'+
                                        '<option value="">seleccione</option>'+
                                    '</select>'+
                                '</div>'+
                               '<div class="col-sm-4">'+
                                   '<div class="form-group">'+
                                       lang.paying_by+
                                       '<select name="paid_by[]" id="paid_by_'+num+'" data-pbnum="'+num+'" class="form-control paid_by">'+
                                           paid_opts+
                                       '</select>'+
                                       '<input type="hidden" name="due_payment[]" class="due_payment_'+num+'">'+
                                   '</div>'+
                                   '<input type="hidden" name="mean_payment_code_fe[]" class="mean_payment_code_fe">'+
                               '</div>'+
                               '<div class="col-sm-4">'+
                                   '<div class="payment">'+
                                       '<div class="form-group ngc_'+num+'">'+
                                           lang.value+
                                           '<input type="text" id="trm_amount_'+num+'" data-num="'+num+'" class="pa form-control kb-pad trm_amount only_number">'+
                                           '<input name="amount-paid[]" type="text" id="amount_'+num+'"'+
                                               'class="pa form-control kb-pad amount only_number" value='+amount+'  data-num="'+num+'" />'+
                                       '</div>'+
                                       '<div class="form-group gc_'+num+'" style="display: none;">'+
                                           '<?= lang("gift_card_no", "gift_card_no"); ?>'+
                                           '<input name="gift_card_no[]" type="text" id="gift_card_no_'+num+'"'+
                                               'class="pa form-control gift_card_no kb-pad"/>'+
                                           '<div id="gc_details_1"></div>'+
                                       '</div>'+
                                   '</div>'+
                               '</div>'+
                               '<div class="col-sm-4 div_slpayment_term_'+num+'" style="display:none;">'+
                                    '<div class="form-group">'+
                                        lang.payment_term+
                                        '<input name="payment_term[]" type="text" id="payment_term_'+num+'"'+
                                               'class="pa form-control kb-pad sale_payment_term only_number"/>'+
                                    '</div>'+
                               '</div>'+
                               '<span class="fa fa-times delete_payment_opt" style="position: absolute;right: 1.6%;font-size: 130%;color: #ed5767;"></span>'+
                           '</div>'+
                           '<div class="clearfix"></div>'+
                           '<div class="pcc_'+num+'" style="display:none;">'+
                               '<div class="row">'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_no[]" type="text" id="pcc_no_'+num+'"'+
                                               'class="form-control" placeholder="'+lang.cc_no+'"/>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_holder[]" type="text" id="pcc_holder_'+num+'"'+
                                               'class="form-control"'+
                                               'placeholder="'+lang.cc_holder+'"/>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<select name="pcc_type[]" id="pcc_type_'+num+'"'+
                                                   'class="form-control pcc_type"'+
                                                   'placeholder="'+lang.card_type+'">'+
                                               '<option value="Visa">'+lang.Visa+'</option>'+
                                               '<option'+
                                                   'value="MasterCard">'+lang.MasterCard+'</option>'+
                                               '<option value="Amex">'+lang.Amex+'</option>'+
                                               '<option value="Discover">'+lang.Discover+'</option>'+
                                           '</select>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_month[]" type="text" id="pcc_month_'+num+'"'+
                                               'class="form-control" placeholder="'+lang.month+'"/>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_year[]" type="text" id="pcc_year_'+num+'"'+
                                               'class="form-control" placeholder="'+lang.year+'"/>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_ccv[]" type="text" id="pcc_cvv2_'+num+'"'+
                                               'class="form-control" placeholder="'+lang.cvv2+'"/>'+
                                       '</div>'+
                                   '</div>'+
                               '</div>'+
                           '</div>'+
                           '<div class="pcheque_'+num+'" style="display:none;">'+
                               '<div class="form-group">'+lang.cheque_no+
                                   '<input name="cheque_no[]" type="text" id="cheque_no_'+num+'"'+
                                       'class="form-control cheque_no"/>'+
                               '</div>'+
                           '</div>'+
                       '</div>'+
                       '<div class="clearfix"></div>'+
                   '</div>'+
               '</div>';
        setTimeout(function() {
            if (getTrmRate() != 1) {
                $('.trm_amount').fadeIn();
                $('.amount').fadeOut();
                $('#trm_amount_'+num).val(formatDecimal(amount * getTrmRate())).trigger('keyup');
            } else {
                $('.trm_amount').fadeOut();
                $('.amount').fadeIn();
            }
        }, 550);
        $('.payments_div').append(html);
        $('#paid_by_'+num).trigger('change');
        set_payment_method_billers();
    });
}

$(document).on('click', '.delete_payment_opt', function(){
    pay_opt = $(this).parents('.pay_option');
    pay_opt.remove();
});

function set_return_payments(set_amount = 0){
    retention = localStorage.getItem('re_retenciones') ? formatDecimal(JSON.parse(localStorage.getItem('re_retenciones')).total_rete_amount) : 0;
    set_amount -= retention;
    num_pm = 0;
    amount_setted = set_amount > 0 ? true : false ;
    if (amount_setted) {
        $('.delete_payment_opt').trigger('click');
    }
    $('#amount_1').prop('readonly', false);
    if (localStorage.getItem('rereturn_payment_setted')) {
        $('#amount_1').val(formatDecimal(set_amount)).prop('readonly', true);

        if (getTrmRate() != 1) {
            $('.trm_amount').fadeIn();
            $('.amount').fadeOut();
            $('#trm_amount_'+(1)).val(formatDecimal(set_amount * getTrmRate())).trigger('keyup');
        } else {
            $('.trm_amount').fadeOut();
            $('.amount').fadeIn();
        }
    } else {
        if (Object.keys(sale_payments).length > 0) {
            $.each(sale_payments, function(payment_method, amount){
                if (payment_method != 'due' && payment_method != 'retention' && payment_method != 'discount') {
                    if (set_amount > 0) {
                        if (set_amount > amount) {
                            value_amount_to_pay = amount;
                            set_amount -= amount;
                        } else {
                            value_amount_to_pay = set_amount;
                            set_amount = 0;
                        }
                    } else if (set_amount == 0) {
                        if (amount_setted == true) {
                            return false;
                        } else {
                            value_amount_to_pay = amount;
                        }
                    } else {
                        value_amount_to_pay = amount;
                    }
                    num_pm++;
                    if (num_pm > 1 && value_amount_to_pay > 0) {
                        add_more_payment(payment_method, value_amount_to_pay);
                    } else {
                        $('#paid_by_1').select2('val', payment_method);
                        $('#amount_1').val(formatDecimal(value_amount_to_pay));
                        setTimeout(function() {
                            $('#paid_by_1').trigger('change');
                        }, 1200);
                        if (getTrmRate() != 1) {
                            $('.trm_amount').fadeIn();
                            $('.amount').fadeOut();
                            $('#trm_amount_'+num_pm).val(formatDecimal(value_amount_to_pay * getTrmRate())).trigger('keyup');
                        } else {
                            $('.trm_amount').fadeOut();
                            $('.amount').fadeIn();
                        }
                    }
                }
            });
        }
    } 
}

function getTrmRate(){
    trmrate = 1;
    othercurrencytrm = $('#trm').val();
    othercurrencyrate = $('#currency option:selected').data('rate');
    if (othercurrencytrm > 0) {
        trmrate = othercurrencyrate / othercurrencytrm;
    }
    return trmrate;
}

$(document).on('keyup', '.trm_amount', function(){
    num = $(this).data('num');
    input_payment = $('input[name="amount-paid[]"][data-num="'+num+'"]');
    input_payment.val(formatDecimals($(this).val() * parseFloat($('#trm').val())));
});