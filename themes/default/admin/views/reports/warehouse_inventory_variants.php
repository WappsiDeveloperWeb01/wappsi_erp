<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
// $v=''; exit(var_dump($_POST));
if ($this->input->post('warehouse')) { $v.= "&warehouse=".$this->input->post('warehouse'); }
if ($this->input->post('product')) { $v.= "&product=".$this->input->post('product'); }
if ($this->input->post('variant')) { $v.= "&variant=".$this->input->post('variant'); }
if ($this->input->post('quantity_zero')) { $v .= "&quantity_zero=1"; $advancedFiltersContainer=true; }
if ($this->input->post('include_products_without_variants')) { $v .= "&include_products_without_variants=1"; $advancedFiltersContainer=true; }
if ($this->input->post('include_products_discontinued')) { $v .= "&include_products_discontinued=1"; $advancedFiltersContainer=true; }
// exit(var_dump($_POST));
?>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12">
			<?php echo admin_form_open("reports/warehouse_inventory_variants", 'id="form-filter"'); ?>
				<input type="hidden" name="filter_action" id="filter_action" value="1">
				<div class="ibox float-e-margins boder-bottom">
					<div class="ibox-title">
						<div class="row">
							<div class="col-sm-11">
								<div class="row">
									<div class="col-sm-12 col-md-4">
										<div class="form-group">
											<?= lang('warehouse','warehouse'); ?>
											<?php
												$bodegas[''] = lang("select_warehouse");
												foreach ($warehouses as $key => $value) {
													$bodegas[$value['ID']] = $value['nombre'];
												}
												echo form_dropdown('warehouse', $bodegas, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ''), 'class="form-control select text-center" id="warehouse" style="width:100%" ');
											?>
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="form-group">
											<?= lang('product','product'); ?>
											<div class="controls" id="product_data">
										    	<?php
                                    				echo form_input('product', (isset($_POST['product']) ? $_POST['product'] : ''), 'class="form-control text-center" id="product"  placeholder="' . lang("select_product_to_load") . '"');
                                    			?>
                                			</div>
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="form-group">
											<?= lang('variant', 'variant'); ?>
											<div class="controls" id="variant_data">
												<?php
  													echo form_input('variant', (isset($_POST['variant']) ? $_POST['variant'] : ''), 'class="form-control text-center" id="variant"  placeholder="' . lang("select_variant_to_load") . '"');
												?>
											</div>
										</div>
									</div>
								</div> <!-- row -->
							</div> <!-- col-sm-11 -->
							<div class="col-sm-1 col-without-padding text-center">
                            	<div class="new-button-container">
                                	<?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                	<button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                	<button class="btn btn-primary new-button" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                            	</div>
                        	</div>
						</div> <!-- row -->
					</div> <!-- ibox-title -->

					<div class="ibox-content" style = "display : <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
						<div class="row">
							<div class="col-sm-12">
								<h4>Filtros avanzados</h4>
                            	<hr>
							</div>
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-12 col-md-3">
										<div class="form-group">
											<?= lang('not_show_quantity_zero', 'not_show_quantity_zero'); ?>
											<div class="controls" id="quantity_zero">
												<?php
													echo form_checkbox('quantity_zero', 'quantity_zero', (isset($_POST['quantity_zero']) ? 'checked' : ''));
												?>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-md-3">
										<div class="form-group">
											<?= lang('include_products_without_variants', 'include_products_without_variants'); ?>
											<div class="controls" id="include_products_without_variants">
												<?php
													echo form_checkbox('include_products_without_variants', 'include_products_without_variants', (isset($_POST['include_products_without_variants']) ? 'checked' : ''));
												?>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-md-3">
										<div class="form-group">
											<?= lang('include_products_discontinued', 'include_products_discontinued'); ?>
											<div class="controls" id="include_products_discontinued">
												<?php
													echo form_checkbox('include_products_discontinued', 'include_products_discontinued', (isset($_POST['include_products_discontinued']) ? 'checked' : ''));
												?>
											</div>
										</div>
									</div>
								</div> <!-- row -->
								<div class="form-group">
									<div class="controls">
										<button type="submit" class="btn btn-primary"><?= lang('submit') ?></button>
									</div>
								</div>	
							</div> <!-- col-sm-12 -->
						</div> <!-- row -->
					</div> <!-- ibox-content -->
				</div> <!-- ibox- -->
			<?php echo form_close(); ?>
		</div> <!-- col-lg-12 -->
	</div> <!-- row -->

	<?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<div class="table-responsive">
									<table id="PrRData" class="table  table-bordered table-condensed table-hover dfTable reports-table" style="margin-bottom:5px;">
										<thead>
											<tr class="active">
												<th><?= lang('warehouse'); ?></th>
												<th><?= lang('code'); ?></th>
												<th><?= lang('product'); ?></th>
												<th><?= lang('status'); ?></th>
												<th><?= lang('variant_code'); ?></th>
												<th><?= lang('variant'); ?></th>
												<th><?= lang('product_qty'); ?></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="5" class="dataTables-empty"><?= lang('loading_data_from_server'); ?></td>
											</tr>
										</tbody>
										<tfoot class="dtFilter">
											<tr class="active">
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div> <!-- col-lg-12 -->
						</div> <!-- row -->
					</div> <!-- ibox-content -->
				</div> <!-- ibox -->
			</div> <!-- col-lg-12 -->
		</div>	<!-- row -->
	<?php endif ?>
</div> <!-- wrapper -->

<script type="text/javascript">
	$(document).ready(function(){

		// SELECT2 PRODUCTOS
		$('#product').select2("destroy").empty().attr('placeholder', "<?= lang('select_product_to_load') ?>").select2({
			allowClear : true,
			placeholder : "<?= lang('select_product_to_load') ?>",
			data : [{ id : '', text : '<?= lang('select_product_to_load') ?>' }]
		});
		getProducts();

		// SELECT2 VARIANTES
		$('#variant').select2("destroy").empty().attr('placeholder', "<?= lang('select_variant_to_load') ?>").css('width', '100%').select2({
			allowClear : true,
			placeholder : "<?= lang('select_variant_to_load') ?>",
			data : [{ id : '', text : '<?= lang('select_variant_to_load') ?>' }]
		});

		$('#warehouse').change(function(){
			var bodega = $(this).val();
			if (bodega) {
				$.ajax({
					type: 'GET',
					async: false,
					url: "<?= admin_url('reports/getWarehouseProducts') ?>/" +bodega,
					dataType: 'json',
				})
				.done(function(scdata) {
					if (scdata != null) {
						$("#product").select2("val", '').empty().attr("placeholder", "<?= lang('select_product_to_load') ?>").select2({allowClear: true,
                                placeholder: "<?= lang('select_product_to_load') ?>",
                                data: scdata
                        });
					}else {
                        $("#product").select2("val", '').empty().attr("placeholder", "<?= lang('no_product') ?>").select2({allowClear: true,
                               	placeholder: "<?= lang('no_product') ?>",
                                data: [{id: '', text: '<?= lang('no_product') ?>'}]
                        });
                    }
				})
				.fail(function() {
					bootbox.alert('<?= lang('ajax_error') ?>');
				})
				.always(function() {

				});
			}
		});

		$('#product').change(function(){
			var producto = $(this).val();
			if (producto) {
				$.ajax({
					type: 'GET',
					async: false,
					url: "<?= admin_url('reports/getVariants') ?>/" +producto,
					dataType: 'json',
				})
				.done(function(scdata) {
					if (scdata != null) {
						$("#variant").select2("val", '').empty().attr("placeholder", "<?= lang('select_variant_to_load') ?>").select2({allowClear: true,
                                placeholder: "<?= lang('select_variant_to_load') ?>",
                                data: scdata
                        });
					}else {
                        $("#variant").select2("val", "").empty().attr("placeholder", "<?= lang('no_variant') ?>").select2({allowClear: true,
                               	placeholder: "<?= lang('no_variant') ?>",
                                data: [{id: '', text: '<?= lang('no_variant') ?>'}]
                        });
                    }
				})
				.fail(function() {
					bootbox.alert('<?= lang('ajax_error') ?>');
				})
				.always(function() {

				});
			}else {
				$("#variant").select2("destroy").empty().attr("placeholder", "<?= lang('select_product_to_load') ?>").select2({allowClear: true,
                    placeholder: "<?= lang('select_product_to_load') ?>",
                    data: [{id: '', text: '<?= lang('select_product_to_load') ?>'}]
                });
			}
		});

		<?php if (isset($_POST['variant']) && !empty($_POST['variant'])) : ?>
        	$.ajax({
            	type: "get", async: false,
            	url: "<?= admin_url('reports/getVariants') ?>/" + <?= $_POST['product'] ?>,
            	dataType: "json",
            	success: function (scdata) {
                	if (scdata != null) {
                    	$("#variant").select2("destroy").empty().attr("placeholder", "<?= lang('select_variant_to_load') ?>").select2({allowClear: true,
                        	placeholder: "<?= lang('no_product') ?>",
                        	data: scdata
                    	});
                	}
            	}
        	});
    	<?php endif ?>

		$('#submit_filter').on('click', function(e){ console.log('im here')
			$('#filter_action').val(1);
        });

		//DATATABLES
		<?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
			var oTable = $('#PrRData').dataTable({
				"aaSorting": [0, "ASC"],
            	"aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            	"iDisplayLength": <?= $Settings->rows_per_page ?>,
            	'bProcessing': true, 'bServerSide': true,
            	'sAjaxSource': '<?= admin_url('reports/getWarehousesInventoryVariants/?'.$v) ?>',
				'dom' : '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            	'fnServerData': function (sSource, aoData, fnCallback) {
                	aoData.push({
                    	"name": "<?= $this->security->get_csrf_token_name() ?>",
                    	"value": "<?= $this->security->get_csrf_hash() ?>"
                	});
                	$.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            	},
            	'fnRowCallback': function (nRow, aData, iDisplayIndex) {
					$('.actionsButtonContainer').html('<div class="pull-right dropdown">'+
                    '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                    '<ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">'+
                        '<li>'+
                            '<a href="<?=admin_url('reports/getWarehousesInventoryVariants/xls/?'.$v)?>"  id="xls" data-action="labels"><i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?></a>'+
                        '</li>'+
                    '</ul>'+
                '</div>');
            	},
            	"aoColumns": [
            				{"bSortable" : false},
            				{"bSortable" : false},
                            {"bSortable" : true },
                            {"mRender": formatStatus, "bSortable" : true, "class" : "text-center" },
                            {"mRender": formatText, "bSortable" : true, "class" : "text-center" },
                            {"mRender": formatText, "class" : "text-center", "bSortable" : true},
                            {"mRender": formatQuantity, "class" : "text-center", "bSortable": true, "bSearchable": false}
                         ],
                buttons : [{extend:'excel', title:'Afiliados', className:'btnExportarExcel', exportOptions: {columns : [1,2,3,4,5]}}],
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                	var tt1 = 0;
                	for (var i = 0; i < aaData.length; i++) {
                    	tt1 += parseFloat(aaData[aiDisplay[i]][5]);
                	}
               		var nCells = nRow.getElementsByTagName('th');
                	nCells[5].innerHTML = '<div class="text-center">'+formatQuantity2(tt1)+'</div>';
            	}
			}).fnSetFilteringDelay().dtFilter([
            	{column_number: 0, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
            	{column_number: 1, filter_default_label: "[<?=lang('code');?>]", filter_type: "text", data: []},
            	{column_number: 2, filter_default_label: "[<?=lang('product');?>]", filter_type: "text", data: []},
            	{column_number: 3, filter_default_label: "[<?=lang('variant_code');?>]", filter_type: "text", data: []},
            	{column_number: 4, filter_default_label: "[<?=lang('variant');?>]", filter_type: "text", data: []},
            	{column_number: 5, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "number", data: []},
       	 	], "footer");
       	<?php endif ?>


	});

	function formatText(x){
		return (x != null) ? '<div class="text-center">'+x+'</div>' : '';
	}

	function formatStatus(x){ console.log(x)
		if (x == '1') {
			return '<div class="text-center text-default"><i style="cursor:pointer;" class="fa fa-times text-default" data-toggle="tooltip" data-placement="top" title="'+lang["no"]+'"> </i></div>';
		} else if(x == '0') {
			return '<div class="text-center text-info"><i style="cursor:pointer;" class="fa fa-check" data-toggle="tooltip" data-placement="top" title="'+lang["yes"]+'"></i></div>';
		}
	}

	function getProducts(){
        $.ajax({
            type: "get", async: false,
            url: "<?= admin_url('reports/getWarehouseProducts') ?>",
            dataType: "json",
            success: function (scdata) {
                if (scdata != null) {
                    $("#product").select2("destroy").empty().attr("placeholder", "<?= lang('select_product_to_load') ?>").select2({allowClear: true,
                        placeholder: "<?= lang('no_product') ?>",
                        data: scdata
                    });
                }
            }
        });
	}
</script>
