                <div class="clearfix"></div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"></div>
                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" data-backdrop="static" data-keyboard="false"></div>
                <div class="modal fade in" id="gc_modal" tabindex="-1" role="dialog" aria-labelledby="gc_modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <span class="modal-title product_name_spumodal" style="font-size: 150%;"><?=sprintf(lang('product_gift_card_modal_header'), lang('gift_card'));?></span>
                            </div>
                            <div class="modal-body">
                                <form class="form product_gift_card_form">
                                    <div class="form-group">
                                        <input type="hidden" name="product_id_gift_card" id="product_id_gift_card">
                                        <?= lang("card_no", "card_no"); ?>
                                        <div class="input-group">
                                            <?php echo form_input('card_no', '', 'class="form-control" id="card_no" required="required"'); ?>
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                <a href="#" id="genNo">
                                                    <i class="fa fa-cogs"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= lang("value", "gc_value"); ?>
                                        <?php echo form_input('gc_value', '', 'class="form-control only_number" id="gc_value" required'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?= lang("expiry_date", "gc_expiry"); ?>
                                        <input type="date" name="expiry" id="gc_expiry" class="form-control" value="<?= date("Y-m-d", strtotime("+1 year")) ?>" data-originaldate="<?= date("Y-m-d", strtotime("+1 year")) ?>" required>
                                    </div>
                                </form>

                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success send_product_gift_card_data" type="button"><?= lang('submit') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                </script>

                <div id="modal-loading" style="display: none;">
                    <div class="blackbg"></div>
                    <div class="loader"></div>
                </div>
                <div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
                </div>
                <div class="footer">
                    <script>
                        $( document ).ready(function() {
                            $('.modal').on('hidden.bs.modal', function (e) {
                                $.each($('.modal'), function(index, v){
                                    if ($(this).css('display') == "block") {
                                        $(this).modal('toggle');
                                    }
                                });
                            });

                            $('.mm_<?=$m?>').addClass('active');
                            $('#<?=$m?>_<?=$v?>').addClass('active');

                            // alert('#<?=$m?>_<?=$v?>');

                            var padre =  $('#<?=$m?>_<?=$v?>').parent();
                            //Preguntamos si el padre tiene la clase nav-third-level para saber si hay otro padre.
                            if(padre.hasClass( "nav-third-level" )){
                                padre =  $('.mm_<?=$m?>').parent();
                                padre = padre.parent();
                                padre.addClass('active');
                            }

                            $(window).on('click', function(){
                                label = $('.notification_label_wifi');
                                if (window.navigator.onLine == true) {
                                    if (label.hasClass('label-danger') == true) {
                                        label.removeClass('label-danger');
                                    }
                                    label.addClass('label-primary');
                                } else {
                                    if (label.hasClass('label-primary') == true) {
                                        label.removeClass('label-primary');
                                    }
                                    label.addClass('label-danger');
                                }
                            });

                            arreglar_tamaño_divs();
                        });

                        function arreglar_tamaño_divs(){
                            var heights = $(".same-height").map(function() {
                                return $(this).height();
                            }).get(),

                            maxHeight = Math.max.apply(null, heights);

                            $(".same-height").height(maxHeight);
                        }
                    </script>
                    <div class="no-printable">
                        <strong>Copyright</strong> Wappsi &copy;

                        <?= date('Y') . " " . $Settings->site_name; ?>

                        <?= "versión ".$Settings->version; ?> <?= strpos($Settings->version, 'ED') !== false && $this->Owner ? '<i class="fa fa-exclamation-circle input-tip" data-tip="ED En desarrollo, UA última actualización"></i>' : '' ?>
                        <?php if ($_SERVER["REMOTE_ADDR"] == '127.0.0.1') {} ?>
                    </div>
                </div>
            <!-- </div>  -->
        <!-- </div> -->

        <?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code); ?>
        <script type="text/javascript">
            var dt_lang = <?=$dt_lang?>,
                dp_lang = <?=$dp_lang?>,
                payment_methods_billers = JSON.parse('<?= $this->payment_methods_billers ?>'),
                site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url(), 'assets' => $assets, 'settings' => $Settings, 'pos_settings' => $this->pos_settings, 'dateFormats' => $dateFormats, 'local_ip' => LOCAL_IP, 'except_tax_rate_id' => $except_tax_rate_id, 'run_update' => $this->run_update, 'año_actual_movimientos' => $this->año_actual_movimientos, 'v_action' => $this->v, 'm_module'=> $this->m))?>;
            var deposit_payment_method = <?= json_encode($this->deposit_payment_method) ?>;
            var due_payment_method = <?= json_encode($this->due_payment_method) ?>;
            var is_owner = <?= $this->Owner ? $this->Owner : 0; ?>;
            var is_admin = <?= $this->Admin ? $this->Admin : 0; ?>;
            var fe_technology_provider = '<?= $this->Settings->fe_technology_provider; ?>',
                CADENA =  '<?= CADENA; ?>',
                FACTURATECH =  '<?= FACTURATECH; ?>',
                DELCOP = '<?= DELCOP; ?>',
                FACTURA_POS = '<?= FACTURA_POS; ?>',
                FACTURA_DETAL = '<?= FACTURA_DETAL; ?>',
                NOTA_CREDITO_POS = '<?= NOTA_CREDITO_POS; ?>',
                NOTA_CREDITO_DETAL = '<?= NOTA_CREDITO_DETAL; ?>',
                NOTA_DEBITO_POS = '<?= NOTA_DEBITO_POS; ?>',
                NOTA_DEBITO_DETAL = '<?= NOTA_DEBITO_DETAL; ?>';
            var lang = JSON.parse('<?= json_encode($this->sma->replace_comillas(get_all_lang()->language)) ?>');
            var user_permissions = JSON.parse('<?= ($this->Owner || $this->Admin ? "false" : str_replace("-", "_", json_encode($GP))) ?>');
            var billers_data = JSON.parse('<?= json_encode($this->billers_data) ?>');
        <?php
            $barcodes = $this->session->userdata('barcodes');
            $this->session->unset_userdata('barcodes');
        ?>
        var session_user_data = JSON.parse('<?= json_encode($this->session->userdata()) ?>');
        <?php $this->session->set_userdata('barcodes', $barcodes); ?>
        </script>
        <?php
        $s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
        foreach (lang('select2_lang') as $s2_key => $s2_line) {
            $s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
        }
        $s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
        ?>
        <!-- Scripts de Inspinia -->
        <!-- Mainly scripts -->
        <script src="<?= $assets ?>inspinia/js/plugins/metisMenu/jquery.metisMenu.js<?= $files_version ?>"></script>
        <script src="<?= $assets ?>inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js<?= $files_version ?>"></script>
        <!-- Custom and plugin javascript -->
        <script src="<?= $assets ?>inspinia/js/inspinia.js<?= $files_version ?>"></script>
        <script src="<?= $assets ?>inspinia/js/plugins/pace/pace.min.js<?= $files_version ?>"></script>
        <!-- Terminan los scripts de Inspinia -->
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= $assets ?>js/hc/dataTables/media/css/dataTables.bootstrap.min.css<?= $files_version ?>">
        <link rel="stylesheet" href="<?= $assets ?>js/hc/dataTables/extensions/Buttons/css/buttons.bootstrap.min.css<?= $files_version ?>">
        <!-- DataTables -->
        <script src="<?= $assets ?>js/hc/dataTables/media/js/jquery.dataTables.min.js<?= $files_version ?>"></script>
        <!-- <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js<?= $files_version ?>"></script> -->
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.dtFilter.min.js<?= $files_version ?>"></script>
        <script src="<?= $assets ?>js/hc/dataTables/media/js/dataTables.bootstrap.min.js<?= $files_version ?>"></script>
        <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js<?= $files_version ?>"></script>
        <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js<?= $files_version ?>"></script>
        <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js<?= $files_version ?>"></script>
        <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js<?= $files_version ?>"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js<?= $files_version ?>"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js<?= $files_version ?>"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js<?= $files_version ?>"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js<?= $files_version ?>"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js<?= $files_version ?>"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.calculator.min.js<?= $files_version ?>"></script>
        <script type="text/javascript" src="<?= $assets ?>js/core.js<?= $files_version ?>"></script>
        <script type="text/javascript" src="<?= $assets ?>js/perfect-scrollbar.min.js<?= $files_version ?>"></script>
        <script type="text/javascript" src="<?=$assets?>pos/js/plugins.min.js<?= $files_version ?>"></script>
        <script type="text/javascript" src="<?=$assets?>pos/js/parse-track-data.js<?= $files_version ?>"></script>

        <script src="<?= base_url() ?>vendor/jquery_colorpicker/jquery.minicolors.js"></script>
        <link rel="stylesheet" href="<?= base_url() ?>vendor/jquery_colorpicker/jquery.minicolors.css">

        <script type="text/javascript" src="<?= base_url() ?>vendor/inputmask/jquery.inputmask.js<?= $files_version ?>"></script>
        <?php if (!empty($jasny_js) && isset($jasny_js)) { echo $jasny_js; } ?>


        <?php if (isset($ISPOS) && $ISPOS == TRUE): ?>
        <script type="text/javascript" src="<?=$assets?>pos/js/pos.ajax.js<?= $files_version ?>"></script>
        <?php endif ?>
        <?php if (isset($ISPOSWS) && $ISPOSWS == TRUE): ?>
        <script type="text/javascript" src="<?=$assets?>pos/js/pos_wholesale.ajax.js<?= $files_version ?>"></script>
        <?php endif ?>
        <?= ($m == 'purchases' && ($v == 'add' || $v == 'edit' || $v == 'purchase_by_csv' || $v == 'add_support_document') || $v == 'edit_support_document') ? '<script type="text/javascript" src="' . $assets . 'js/purchases.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'purchases' && ($v == 'add_payment')) ? '<script type="text/javascript" src="' . $assets . 'js/payments.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'purchases' && ($v == 'add_import' || $v == 'edit_import')) ? '<script type="text/javascript" src="' . $assets . 'js/imports.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'transfers' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/transfers.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'sales' && ($v == 'add' || $v == 'add_2' || $v == 'edit' || $v == 'edit_2')) ? '<script type="text/javascript" src="' . $assets . 'js/sales.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'sales' && ($v == 'return_sale')) ? '<script type="text/javascript" src="' . $assets . 'js/return_sale.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'wms' && ($v == 'add_picking')) ? '<script type="text/javascript" src="' . $assets . 'js/wms_picking.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'wms' && ($v == 'add_packing')) ? '<script type="text/javascript" src="' . $assets . 'js/wms_packing.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'sales' && ($v == 'add_order' || $v == 'edit_order' )) ? '<script type="text/javascript" src="' . $assets . 'js/order.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'returns' && ($v == 'add' || $v == 'add_past_years_sale_return' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/returns.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'returns' && ($v == 'add_past_years_purchase_return')) ? '<script type="text/javascript" src="' . $assets . 'js/returns_purchase.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'quotes' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/quotes.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'quotes' && ($v == 'addqpurchase')) ? '<script type="text/javascript" src="' . $assets . 'js/quotes_purchases.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'quotes' && ($v == 'addqexpense')) ? '<script type="text/javascript" src="' . $assets . 'js/quotes_expenses.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'products' && ($v == 'add_adjustment' || $v == 'edit_adjustment')) ? '<script type="text/javascript" src="' . $assets . 'js/adjustments.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'products' && ($v == 'add_production_order' || $v == 'edit_production_order')) ? '<script type="text/javascript" src="' . $assets . 'js/production_order.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'products' && ($v == 'add_product_transformation')) ? '<script type="text/javascript" src="' . $assets . 'js/product_transformation.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'payments' && ($v == 'add')) ? '<script type="text/javascript" src="' . $assets . 'js/payments.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'payments' && ($v == 'padd' || $v == 'pedit')) ? '<script type="text/javascript" src="' . $assets . 'js/ppayments.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'payments' && ($v == 'exadd' || $v == 'payment_actions')) ? '<script type="text/javascript" src="' . $assets . 'js/expayments.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'production_order' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/production_making_order.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'production_order' && ($v == 'add_cutting_order')) ? '<script type="text/javascript" src="' . $assets . 'js/cutting_order.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'production_order' && ($v == 'add_assemble_order')) ? '<script type="text/javascript" src="' . $assets . 'js/assemble_order.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'production_order' && ($v == 'add_packing_order')) ? '<script type="text/javascript" src="' . $assets . 'js/packing_order.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'hotel' && ($v == 'add_guest_register' || $v == 'edit_guest_register')) ? '<script type="text/javascript" src="' . $assets . 'js/guest_registers.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'transfers' && ($v == 'add_order')) ? '<script type="text/javascript" src="' . $assets . 'js/order_transfers.js'.$files_version.'"></script>' : ''; ?>
        <?= ($m == 'wappsi_invoicing' && ($v == 'electronic_hits')) ? '<script type="text/javascript" src="' . $assets . 'js/electronic_hits.js'.$files_version.'"></script>' : ''; ?>
        <script type="text/javascript" charset="UTF-8">var oTable = '', r_u_sure = "<?=lang('r_u_sure')?>";
            <?=$s2_file_date?>
            $.extend(true, $.fn.dataTable.defaults, {"oLanguage":<?=$dt_lang?>});
            $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        </script>
        <?= (DEMO) ? '<script src="'.$assets.'js/ppp_ad.min.js<?= $files_version ?>"></script>' : ''; ?>
        <?php if ($this->session->userdata('clean_localstorage')): ?>
            <script type="text/javascript">
                // localStorage.clear();
                <?php $this->session->unset_userdata('clean_localstorage') ?>
            </script>
        <?php endif ?>
        <script type="text/javascript">
            $('#date_records_filter_h').on('change', function(){
                filter = $(this).val();
                fecha_inicial = "";
                fecha_final = "<?= date('Y-m-d') ?>";
                hide_date_controls = true;
                if (filter == 5) { //RANGO DE FECHAS
                    fecha_final = "";
                    hide_date_controls = false;
                } else if (filter == 1) { // HOY
                    fecha_inicial = "<?= date('Y-m-d') ?>";
                } else if (filter == 2) { // MES
                    fecha_inicial = "<?= date('Y-m-01') ?>";
                } else if (filter == 3) { // TRIMESTRE
                    fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                } else if (filter == 8) { // SEMESTRE
                    fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
                } else if (filter == 4) { // AÑO
                    fecha_inicial = "<?= date('Y-01-01') ?>";
                } else if (filter == 6) { // MES PASADO
                    fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 1 month")) ?>";
                    fecha_final = "<?= date("Y-m-d",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                } else if (filter == 9) { // TRIMESTRE PASADO
                    fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                    fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                } else if (filter == 10) { // SEMESTRE PASADO
                    fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                    fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                } else if (filter == 7) { // AÑO PASADO
                    fecha_inicial = "<?= date("Y-01-01",strtotime(date("Y-m-01")."- 1 year")) ?>";
                    fecha_final = "<?= date("Y-12-31",strtotime(date("Y-m-01")."- 1 year")) ?>";
                } else if (filter == 11) { // AÑO PASADO
                    fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 2 month")) ?>";
                } else if (filter == 0) {
                    fecha_inicial = "";
                    fecha_final = "";
                    hide_date_controls = true;
                }
                if (filter != 5) {
                    $('#start_date_h').val(fecha_inicial);
                    $('#end_date_h').val(fecha_final);
                }
                if (hide_date_controls) {
                    $('.date_controls_h').css('display', 'none');
                } else {
                    $('.date_controls_h').css('display', '');
                }
            });
            $('#date_records_filter_dh').on('change', function(){
                filter = $(this).val();
                fecha_inicial = "";
                <?php 
                $jsd = $this->dateFormats['js_sdate'];
                if ($jsd == 'mm-dd-yyyy' || $jsd == 'mm/dd/yyyy' || $jsd == 'mm.dd.yyyy') {
                    $date_format_pls = 'm/d/Y H:i';
                } else {
                    $date_format_pls = 'd/m/Y H:i';
                }
                 ?>
                 
                fecha_final = "<?= date($date_format_pls, strtotime(date('Y-m-d 23:59'))) ?>";

                hide_date_controls = true;
                if (filter == 5) { //RANGO DE FECHAS
                    fecha_final = "";
                    hide_date_controls = false;
                } else if (filter == 1) { // HOY
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date('Y-m-d 00:00'))) ?>";
                } else if (filter == 2) { // MES
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date('Y-m-01 00:00'))) ?>";
                } else if (filter == 3) { // TRIMESTRE
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date("Y-m-d 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")))) ?>";
                } else if (filter == 8) { // SEMESTRE
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date("Y-m-d 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")))) ?>";
                } else if (filter == 4) { // AÑO
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date('Y-01-01 00:00'))) ?>";
                } else if (filter == 6) { // MES PASADO
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date("Y-m-d 00:00",strtotime(date("Y-m-01")."- 1 month")))) ?>";
                    fecha_final = "<?= date($date_format_pls, strtotime(date("Y-m-d 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))))) ?>";
                } else if (filter == 9) { // TRIMESTRE PASADO
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date("Y-m-d 00:00",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))))) ?>";
                    fecha_final = "<?= date($date_format_pls, strtotime(date("Y-m-d 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))))))) ?>";
                } else if (filter == 10) { // SEMESTRE PASADO
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date("Y-m-d 00:00",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))))) ?>";
                    fecha_final = "<?= date($date_format_pls, strtotime(date("Y-m-d 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))))))) ?>";
                } else if (filter == 7) { // AÑO PASADO
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date("Y-01-01 00:00",strtotime(date("Y-m-01")."- 1 year")))) ?>";
                    fecha_final = "<?= date($date_format_pls, strtotime(date("Y-12-31 23:59",strtotime(date("Y-m-01")."- 1 year")))) ?>";
                } else if (filter == 11) { // AÑO PASADO
                    fecha_inicial = "<?= date($date_format_pls, strtotime(date("Y-m-d 00:00",strtotime(date("Y-m-01")."- 2 month")))) ?>";
                } else if (filter == 0) {
                    fecha_inicial = "";
                    fecha_final = "";
                    hide_date_controls = true;
                }
                if (filter != 5) {
                    $('#start_date_dh').val(fecha_inicial);
                    $('#end_date_dh').val(fecha_final);
                }
                if (hide_date_controls) {
                    $('.date_controls_dh').css('display', 'none');
                } else {
                    $('.date_controls_dh').css('display', '');
                }
            });
            <?php if (isset($_POST['start_date'])): ?>
                setTimeout(function() {
                    $("#start_date_dh").datetimepicker({
                        format: site.dateFormats.js_ldate,
                        fontAwesome: true,
                        language: 'sma',
                        weekStart: 1,
                        todayBtn: 1,
                        startDate: min_input_date,
                        endDate: max_input_date,
                        autoclose: 1,
                        todayHighlight: 1,
                        startView: 2,
                        forceParse: 0
                    }).datetimepicker('update', ("<?= $_POST['start_date'] ?>"));
                }, 850);
            <?php endif ?>
            <?php if (isset($_POST['end_date'])): ?>
                setTimeout(function() {
                    $("#end_date_dh").datetimepicker({
                        format: site.dateFormats.js_ldate,
                        fontAwesome: true,
                        language: 'sma',
                        weekStart: 1,
                        todayBtn: 1,
                        startDate: min_input_date,
                        endDate: max_input_date,
                        autoclose: 1,
                        todayHighlight: 1,
                        startView: 2,
                        forceParse: 0
                    }).datetimepicker('update', ("<?= $_POST['end_date'] ?>"));
                }, 850);
            <?php endif ?>
        </script>

        <?php if (!isset($this->modules_NOT_index[$this->m]) && isset($this->modules_index[$this->v])): ?>
            <?php if ($this->session->userdata('flash_message_2')): ?>
                <script type="text/javascript">
                    header_alert('success', '<?= strip_tags($this->session->userdata('flash_message_2')); ?>');
                </script>
                <?php $this->session->unset_userdata('flash_message_2') ?>
            <?php endif ?>
            <?php if ($this->session->userdata('flash_error_2')): ?>
                <script type="text/javascript">
                    header_alert('error', '<?= strip_tags($this->session->userdata('flash_error_2')); ?>');
                </script>
                <?php $this->session->unset_userdata('flash_error_2') ?>
            <?php endif ?>
            <?php if ($this->session->userdata('flash_warning_2')): ?>
                <script type="text/javascript">
                    header_alert('warning', '<?= strip_tags($this->session->userdata('flash_warning_2')); ?>');
                </script>
                <?php $this->session->unset_userdata('flash_warning_2') ?>
            <?php endif ?>
        <?php else: ?>
            <?php if ($this->session->userdata('flash_message_2')): ?>
                <?php $this->session->unset_userdata('flash_message_2') ?>
            <?php endif ?>
            <?php if ($this->session->userdata('flash_error_2')): ?>
                <?php $this->session->unset_userdata('flash_error_2') ?>
            <?php endif ?>
            <?php if ($this->session->userdata('flash_warning_2')): ?>
                <?php $this->session->unset_userdata('flash_warning_2') ?>
            <?php endif ?>
        <?php endif ?>
        <?php if ($message) { $message = str_replace("\n", "", $message); ?>
            <script type="text/javascript">
                header_alert('success', '<?= strip_tags($message); ?>');
            </script>
            <?php if (!isset($this->modules_NOT_index[$this->m]) && isset($this->modules_index[$this->v])): ?>
                <?php $this->session->set_userdata('flash_message_2', $message) ?>
            <?php endif ?>
            <?php $this->session->unset_userdata('flash_message') ?>
            <?php $this->session->unset_userdata('message') ?>
        <?php } ?>
        <?php if ($error) { $error = str_replace("\n", "", $error); ?>
            <script type="text/javascript">
                header_alert('error', '<?= strip_tags($error); ?>');
            </script>
            <?php if (!isset($this->modules_NOT_index[$this->m]) && isset($this->modules_index[$this->v])): ?>
                <?php $this->session->set_userdata('flash_error_2', $error) ?>
            <?php endif ?>
            <?php $this->session->unset_userdata('flash_error') ?>
            <?php $this->session->unset_userdata('error') ?>
        <?php } ?>
        <?php if ($warning) { $warning = str_replace("\n", "", $warning); ?>
            <script type="text/javascript">
                header_alert('warning', '<?= strip_tags($warning); ?>');
            </script>
            <?php if (!isset($this->modules_NOT_index[$this->m]) && isset($this->modules_index[$this->v])): ?>
                <?php $this->session->set_userdata('flash_warning_2', $warning) ?>
            <?php endif ?>
            <?php $this->session->unset_userdata('flash_warning') ?>
            <?php $this->session->unset_userdata('warning') ?>
        <?php } ?>
    </body>
</html>
<style type="text/css">
    /* Animación con keyframe llamada "latidos" */
    @keyframes latidos {
        from { transform: none; }
        30% { transform: scale(1.02); }
        to { transform: none; }
    }
    /* En la clase corazon vamos a llamar latidos en la animación  */
    .corazon {
        animation: latidos 1.4s infinite;
        transform-origin: center;
    }
</style>