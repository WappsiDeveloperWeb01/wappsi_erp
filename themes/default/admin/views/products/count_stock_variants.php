<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'stForm'); echo admin_form_open_multipart("products/count_stock_variants", $attrib); ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php if ($Owner || $Admin) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("date", "date"); ?>
                                                <?php echo form_input('date', 
                                                                    (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld(date('Y-m-d H:i:s'))), 
                                                                    'class="form-control input-tip" id="date" required="required" readonly="true" '); ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "biller"); ?>
                                                <?php
                                                $wh[''] = '';
                                                foreach ($billers as $biller) {
                                                    $wh[$biller->id] = $biller->name;
                                                }
                                                echo form_dropdown('biller', $wh, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="biller" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } else {
                                        $biller_input = array(
                                            'type' => 'hidden',
                                            'name' => 'biller',
                                            'id' => 'biller',
                                            'value' => $this->session->userdata('biller_id'),
                                            );

                                        echo form_input($biller_input);
                                    } ?>

                                    <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("warehouse", "warehouse"); ?>
                                                <?php
                                                    $wh = [];
                                                    $wh[''] = '';
                                                    foreach ($warehouses as $warehouse) {
                                                        $wh[$warehouse->id] = $warehouse->name;
                                                    }
                                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="warehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } else {
                                        $warehouse_input = array(
                                            'type' => 'hidden',
                                            'name' => 'warehouse',
                                            'id' => 'warehouse',
                                            'value' => $this->session->userdata('warehouse_id'),
                                            );

                                        echo form_input($warehouse_input);
                                    } ?>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("reference_no", "document_type_id"); ?>
                                            <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 form-group">
                                        <?= lang('third', 'company_id') ?>
                                        <?php
                                            $cOptions[''] = lang('select_third');
                                            foreach ($companies as $row => $company) {
                                                $cOptions[$company->id] = $company->name;
                                            }
                                        ?>
                                        <?= form_dropdown('company_id', $cOptions, '',' id="company_id" class="form-control select" required style="width:100%;"'); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("type", "type"); ?>
                                            <?php
                                                $opts = ['2' => lang('variant_code')];
                                                echo form_dropdown('type', $opts, '', 'id="type" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("type") . '" required="required" style="width:100%;" ');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="well well-small">
                                                <button
                                                    class="btn btn-primary pull-right" id='fichero' ><i
                                                    class="fa fa-download"></i> <?= lang("download_sample_file") ?>
                                                </button>
                                                <span 
                                                    class="text-warning"><?= lang("xls1"); ?></span><br/>
                                                    <?= lang("csv2"); ?> 
                                                        <span
                                                            class="text-info">(<?= lang("product_code") . '/' .lang('variant_code'). ', ' . lang("quantity") ?>)
                                                        </span> 
                                            </div>                                      
                                            <div class="form-group">
                                                <label for="xls_file"><?= lang("upload_file"); ?></label>
                                                <input type="file" data-browse-label="<?= lang('browse'); ?>" name="xls_file" class="form-control file" data-show-upload="false" data-show-preview="false" id="xls_file" required="required"/>
                                            </div>
                                            <div class="form-group">
                                                <?= lang("note", "qanote"); ?>
                                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qanote" style="margin-top: 10px; height: 100px;"'); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    
                                        <div class="col-md-12">
                                            <div class="fprom-group">
                                                <?= form_submit('count_stock', 
                                                            lang("submit"), 
                                                            'id="count_stock" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#biller').trigger('change');
        $('#type').on('change', function (){
            $(this).attr('readonly', true);
        });
        $('#reset').on('click', function(){
           window.location.reload();
        });
        $('#fichero').on('click', function(e){
            e.preventDefault();
            let type = $('#type').val();
            if (type == '') {
                $('#type').select2('open');
                header_alert('warning', 'Por favor, seleccione un tipo de conteo');
                return true;
            }
            v = 'xls='+type;
            window.location.href = "<?=admin_url('products/physical_count?')?>"+v;
            return false;
        })
    });

    $(document).on('change', '#biller', function (e) {
        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/38/") ?>'+$('#biller').val(),
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                localStorage.setItem('locked_for_biller_resolution', 1);
            }
            $('#document_type_id').trigger('change');
        });
    });
</script>