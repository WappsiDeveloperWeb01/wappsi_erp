<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Financing_model extends CI_Model
{
    private $tableName = "credit_financing";

    public function __construct()
    {
        parent::__construct();
    }

    public function getCustomerCreditBranch($data)
    {
        $this->db->select("b.id, b.name");
        $this->db->where($data);
        $this->db->join("companies b", "b.id = credit_financing.biller_id");
        $this->db->group_by("biller_id");
        $q = $this->db->get("credit_financing");
        return $q->result();
    }

    public function getCreditsCustomerSuggestions($term, $limit = 10)
    {
        $this->db->select("c.id, name AS text");
        $this->db->join('companies c', 'c.id = credit_financing.customer_id');
        $this->db->where("(name LIKE '%{$term}%' OR vat_no LIKE '%{$term}%') ");
        $this->db->limit($limit);
        $this->db->group_by('vat_no');
        $q = $this->db->get($this->tableName);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getCreditsByCustomerBranch($data)
    {
        $this->db->where($data);
        $q = $this->db->get($this->tableName);
        return $q->result();
    }

    public function getCreditInstallments($data)
    {
        $this->db->select("
            cfi.id AS installmentId,
            b.name AS billerName,
            credit_financing.credit_no AS creditNo,
            cfi.installment_no AS installmentNo,
            cfi.installment_due_date AS installmentDueDate,
            IF(DATEDIFF(CURDATE(), cfi.installment_due_date) > 0,
                DATEDIFF(CURDATE(), cfi.installment_due_date),
                0
            ) AS daysExpired,
            0 AS defaultInterest,
            cfi.current_interest_amount AS currentInterestAmount,
            cfi.interest_tax AS interestTax,
            cfi.capital_amount AS capitalAmount,
            cfi.installment_amount AS installmentAmount,
            credit_status AS creditStatus");
        $this->db->where($data);
        $this->db->where("cfi.status", 0);
        $this->db->join("credit_financing_installments cfi", "cfi.financing_credit_id = credit_financing.id");
        $this->db->join("companies b", "b.id = credit_financing.biller_id");
        $this->db->order_by("cfi.installment_due_date, daysExpired");
        $q = $this->db->get($this->tableName);
        return $q->result();
    }

    public function find($data)
    {
        $this->db->select("
            f.capital_amount,
            affiliate_consecutive,
            old_branch_code,
            document_code,
            vat_no,
            type_person,
            first_name,
            second_name,
            first_lastname,
            second_lastname,
            name,
            company,
            city_code,
            address,
            phone,
            email,
            credit_no,
            credit_date,
            credit_term_freq,
            p.capital_amount pCapitalAmount,
            p.current_interest_amount,
            p.interest_tax,
            SUM(p.capital_amount + p.current_interest_amount + p.interest_tax) paid,
            count(*) number_installments,
            (SELECT
                    COUNT(*) FROM
                    sma_credit_financing_installments
                WHERE
                    financing_credit_id = f.id
                        AND status = 1) number_installment_paid,
            (SELECT
                    COUNT(*)
                FROM
                    sma_credit_financing_installments
                WHERE
                    financing_credit_id = f.id
                        AND status = 0) number_overdue_installments
        ");
        $this->db->from("$this->tableName f");
        $this->db->join("companies c", "c.id = f.customer_id");
        $this->db->join("biller_data bd", "bd.biller_id = f.biller_id");
        $this->db->join("credit_financing_installments i", "i.financing_credit_id = f.id", "left");
        $this->db->join("credit_financing_installments_payment p", "p.installment_id = i.id", "left");
        $this->db->where($data);
        $this->db->where("f.customer_id > 0");
        $q = $this->db->get();
        return $q->row();
    }

    public function getInitialReportData()
    {
        $q = $this->db->query("SELECT
            *,
            (
                (((capital_amount * default_interest_percentage) / 100) / 30) * default_days
            ) AS default_interest_daily
        FROM
            (SELECT
                *,
                DATEDIFF(CURDATE(), installment_due_date) AS default_days,
                (SELECT
                    default_interest_percentage_credit_financing
                FROM
                    sma_settings
                ) AS default_interest_percentage
            FROM
                sma_credit_financing_installments
            WHERE
                (capital_amount + current_interest_amount + interest_tax) > 0
            ORDER BY installment_due_date DESC)AS temporal_table");

        // $q = $this->db->query("SELECT
        //     *,
        //     (((capital_amount * default_interest_percentage) / 100) / 30) * default_days AS default_interest_daily
        // FROM
        //     (SELECT
        //         *,
        //         DATEDIFF(CURDATE(), installment_due_date) AS default_days,
        //         (SELECT
        //             default_interest_percentage_credit_financing
        //         FROM
        //             sma_settings
        //         ) AS default_interest_percentage
        //     FROM
        //         sma_credit_financing_installments
        //     WHERE
        //         CURDATE() > installment_due_date
        //             AND status = 0 AND (capital_amount + current_interest_amount) > 0
        //     ORDER BY installment_due_date DESC)AS temporal_table");
        return $q->result();
    }

    public function create($data)
    {
        if ($this->db->insert($this->tableName, $data)) {
            return $this->db->insert_id();
        }

        return false;
    }

    public function update($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update($this->tableName, $data)) {
            return true;
        }
        return false;
    }
}
