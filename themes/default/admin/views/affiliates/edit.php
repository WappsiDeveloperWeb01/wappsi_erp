<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
      <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_affiliate'); ?></h4>
    </div>

    <?= admin_form_open_multipart("affiliates/edit/" . $affiliate->id, array('data-toggle' => 'validator', 'role' => 'form')); ?>
    <div class="modal-body">
      <p><?= lang('enter_info'); ?></p>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <?= lang("id_document_type", "id_document_type"); ?>
            <?php
              $document_type_options[''] = lang('select');
              foreach ($id_document_types as $document_type)
              {
                if ($document_type->id != 6)
                {
                  $document_type_options[$document_type->id] = lang($document_type->nombre);
                }
              }
            ?>
            <?= form_dropdown('document_type', $document_type_options, $affiliate->tipo_documento, 'id="document_type" class="form-control select" style="width:100%;" required="required"'); ?>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <?= lang("vat_no", "document_number"); ?>
            <?= form_input('document_number', $affiliate->vat_no, 'class="form-control" id="document_number" required="required"'); ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Primer Nombre</label><span class='input_required'> *</span>
            <input type="text" name="first_name" id="first_name" value="<?= $affiliate->first_name ?>" class="form-control required">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Segundo Nombre</label>
            <input type="text" name="second_name" id="second_name" value="<?= $affiliate->second_name ?>" class="form-control">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Primer Apellido</label><span class='input_required'> *</span>
            <input type="text" name="first_lastname" id="first_lastname" value="<?= $affiliate->first_lastname ?>" class="form-control required">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Segundo Apellido</label>
            <input type="text" name="second_lastname" id="second_lastname" value="<?= $affiliate->second_lastname ?>" class="form-control">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <?= lang("email_address", "email_address"); ?>
            <input type="email" name="email" class="form-control" required="required" value="<?= $affiliate->email ?>" id="email_address"/>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <?= lang("phone", "phone"); ?>
            <input type="tel" name="phone" class="form-control" required="required" value="<?= $affiliate->phone ?>" id="phone"/>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <?= lang("address", "address"); ?>
            <?php echo form_input('address', $affiliate->address, 'class="form-control" id="address" required="required"'); ?>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <?= lang("postal_code", "postal_code"); ?>
            <?php echo form_input('postal_code', $affiliate->postal_code, 'class="form-control postal_code" id="postal_code"'); ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
          <?= lang("country", "country"); ?>
            <select class="form-control select" name="country" id="country" required>
              <option value=""><?= lang('select'); ?></option>
              <?php foreach ($countries as $row => $country): ?>
                <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= ($country->NOMBRE == $affiliate->country) ? "selected='selected'" : "" ?> ><?= $country->NOMBRE ?></option>
              <?php endforeach ?>
            </select>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <?= lang("state", "state"); ?>
            <select class="form-control select" name="state" id="state" required>
              <option value=""><?= lang('select'); ?></option>
            </select>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
              <?= lang("city", "city"); ?>
            <select class="form-control select" name="city" id="city" required>
              <option value=""><?= lang('select') ?></option>
            </select>
            <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
          </div>
        </div>
        <?php 
          $billers_affiliate_default = [];
         ?>
        <div class="form-group col-md-6">
          <?= lang('biller', 'billers'); ?>
          <select class="form-control select" name="billers[]" id="billers" multiple="multiple" required="required">
            <?php foreach ($billers as $biller) : ?>
              <option value="<?= $biller->id ?>" <?= $affiliate->id == $biller->default_affiliate_id ? 'data-default="1"' : '' ?> ><?= $biller->company; ?></option>
              <?php 
              if ($affiliate->id == $biller->default_affiliate_id) {
                $billers_affiliate_default[$biller->id] = $biller->default_affiliate_id;
              }
               ?>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <input type="hidden" name="affiliates_default" value="<?= json_encode($billers_affiliate_default) ?>">
        <div class="row">
          <div class="form-group col-md-6">
            <?php $prev_zone = NULL; ?>
            <table class="table">
              <thead>
                <tr>
                  <th>Código</th>
                  <th>Sucursal</th>
                  <th>Dirección</th>
                </tr>
              </thead>
              <tbody>
                <?php if ($addresses): ?>
                  <?php foreach ($addresses as $address): ?>
                    <?php if ($address->selected == 1): ?>
                      <?php 
                      $actual_address = !empty($address->location) ? $address->location : 'No Registra';
                      if ($prev_zone == NULL || $actual_address != $prev_zone): 
                        ?>
                          <tr>
                            <th colspan="3">Zona : <?= $actual_address ?></th>
                          </tr>
                          <?php $prev_zone = $actual_address; ?>
                      <?php endif ?>
                      <tr>
                        <td><?= $address->code ?></td>
                        <td><?= $address->sucursal ?></td>
                        <td><?= $address->direccion ?></td>
                      </tr>
                    <?php endif ?>
                  <?php endforeach ?>
                <?php endif ?>
                  
              </tbody>
            </table>
                
            <!-- <?= lang('customer_branch', 'addresses'); ?>
            <select class="form-control select" name="addresses[]" id="addresses" multiple="multiple" style="height: auto; display: block;">
              <option vaue="">Seleccione...</option>
              <?php foreach ($addresses as $address) : ?>
                <option value="<?= $address->id ?>" <?= $address->selected == 1 ? 'selected="selected"' : '' ?>><?= "(".$address->vat_no_company.") ".$address->company_name." - ".$address->sucursal; ?></option>
              <?php endforeach ?>
            </select> -->
          </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php echo form_submit('edit_affiliate', lang('edit_affiliate'), 'class="btn btn-primary"'); ?>
    </div>
  </div>
  <?php echo form_close(); ?>
</div>
<script type="text/javascript">
  $(document).ready(function (e)
  {
    set_ubication();

    $('#tipo_documento').trigger('change');

    $('#country').on('change', function(){
      dpto = $('#country option:selected').data('code');

      $.ajax({
        url:"<?= admin_url() ?>customers/get_states/"+dpto,
      })
      .done(function(data) { $('#state').html(data); })
      .fail(function(data) { console.log(data.responseText); });
    });

    $('#state').on('change', function(){
      dpto = $('#state option:selected').data('code');

      $.ajax({
        url:"<?= admin_url() ?>customers/get_cities/"+dpto,
      })
      .done(function(data){ $('#city').html(data);})
      .fail(function(data){ console.log(data); });
    });

    $('#city').on('change', function(){
      code = $('#city option:selected').data('code');
      $('.postal_code').val(code);
      $('#city_code').val(code);
    });

    // Algoritmo para obtener los billers y cargarlos por defecto en el campo sucursal.
    var billers_array = [];
    <?php if ($billers_affiliate !== FALSE) { ?>
      <?php foreach ($billers_affiliate as $biller) { ?>
          billers_array.push('<?= $biller->biller_id; ?>');
      <?php } ?>

      $('#billers').select2().select2('val', billers_array);
    <?php } ?>

    $(document).on('change', '#billers', function(){
      $('#billers option').each(function(index, option){
          if ($(option).val() && $(option).data('default') == 1 && !$(option).is(':selected')) {
             command: toastr.warning('El vendedor está cómo predeterminado para la sucursal <b>'+$(option).text()+'</b>, dicha sucursal quedará sin vendedor predeterminado ', '¡Atención!', {
                      "showDuration": "500",
                      "hideDuration": "1000",
                      "timeOut": "4000",
                      "extendedTimeOut": "1000",
                  });
          }
      });
    });

  });

  function set_ubication(){
          country = $('#country option:selected').data('code');
          state = "<?= $affiliate->state ?>";
          set_city = "<?= $affiliate->city ?>";
          $.ajax({
            url:"<?= admin_url() ?>customers/get_states",
            method : "POST",
            data : {
              "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
              country : country,
              state : state
            }
          })
          .done(function(data) {
            $('#state').html(data);
            $('#state').select2();
            state = $('#state option:selected').data('code');

            $.ajax({
              url:"<?= admin_url() ?>customers/get_cities",
              method : "POST",
              data : {
                "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
                state : state,
                city : set_city
              }
            }).done(function(data){
              $('#city').html(data);
              $('#city').select2('val', set_city);

            })
            .fail(function(data){ console.log(data.responseText); });

          }).fail(function(data){ console.log(data.respnoseText); });
        }
</script>
<?= $modal_js ?>
