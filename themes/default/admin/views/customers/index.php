<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight no-print">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('customers/index', ['id'=>'customerFilterForm']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="type_person_filter"><?= $this->lang->line('type_person') ?></label>
                                            <?php $typePerson = (isset($_POST['type_person_filter']) ? $_POST['type_person_filter'] : '') ?>
                                            <select class="form-control" name="type_person_filter" id="type_person_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($typesPerson)) : ?>
                                                    <?php foreach ($typesPerson as $typeperson) : ?>
                                                        <option value="<?= $typeperson->id ?>" <?= ($typeperson->id == $typePerson) ? 'selected' : '' ?>><?= $typeperson->description ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="tipo_regimen_filter"><?= $this->lang->line('tipo_regimen') ?></label>
                                            <?php $typeRegime = (isset($_POST['tipo_regimen_filter']) ? $_POST['tipo_regimen_filter'] : '') ?>
                                            <select class="form-control" name="tipo_regimen_filter" id="tipo_regimen_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($typesRegime)) : ?>
                                                    <?php foreach ($typesRegime as $typeregime) : ?>
                                                        <option value="<?= $typeregime->id ?>" <?= ($typeregime->id == $typeRegime) ? 'selected' : '' ?> code="<?= $typeregime->codigo ?>"><?= $typeregime->description ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="customer_group_id_filter"><?= $this->lang->line('customer_groups') ?></label>
                                            <?php $customerGroupId = (isset($_POST['customer_group_id_filter']) ? $_POST['customer_group_id_filter'] : '') ?>
                                            <select class="form-control" name="customer_group_id_filter" id="customer_group_id_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($customerGroups)) : ?>
                                                    <?php foreach ($customerGroups as $customerGroup) : ?>
                                                        <option value="<?= $customerGroup->id ?>" <?= ($customerGroup->id == $customerGroupId) ? 'selected' : '' ?>><?= $customerGroup->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="price_group_id_filter"><?= $this->lang->line('price_group') ?></label>
                                            <?php $priceGroupId = (isset($_POST['price_group_id_filter']) ? $_POST['price_group_id_filter'] : '') ?>
                                            <select class="form-control" name="price_group_id_filter" id="price_group_id_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($priceGroups)) : ?>
                                                    <?php foreach ($priceGroups as $priceGroup) : ?>
                                                        <option value="<?= $priceGroup->id ?>" <?= ($priceGroup->id == $priceGroupId) ? 'selected' : '' ?>><?= $priceGroup->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="customer_payment_type_filter"><?= $this->lang->line('customer_payment_type') ?></label>
                                            <?php $customerPaymentTypeId = (isset($_POST['customer_payment_type_filter']) ? $_POST['customer_payment_type_filter'] : '') ?>
                                            <select class="form-control" name="customer_payment_type_filter" id="customer_payment_type_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($customerPaymentsTypes)) : ?>
                                                    <?php foreach ($customerPaymentsTypes as $customerPaymentType) : ?>
                                                        <option value="<?= $customerPaymentType->id ?>" <?= ($customerPaymentType->id == $customerPaymentTypeId) ? 'selected' : '' ?>><?= $customerPaymentType->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label for="customer_seller_id_assigned_filter"><?= $this->lang->line('seller') ?></label>
                                        <?php $customerSellerIdAssigned = (isset($_POST["customer_seller_id_assigned_filter"])) ? $_POST["customer_seller_id_assigned_filter"] : ''; ?>
                                        <select class="form-control" name="customer_seller_id_assigned_filter" id="customer_seller_id_assigned_filter">
                                            <?php if (!$this->sma->seller_restricted()): ?>
                                                <option value=""><?= lang('select') ?></option>
                                            <?php endif ?>
                                            <?php if ($sellers): ?>
                                                <?php foreach ($sellers as $seller): ?>
                                                    <?php if (!$this->sma->seller_restricted()||($this->sma->seller_restricted() && $seller->id == $this->sma->seller_restricted())): ?>
                                                        <option value="<?= $seller->id ?>" <?= ($this->sma->seller_restricted() && $seller->id == $this->sma->seller_restricted()) ? "selected='selected'" : "" ?>><?= $seller->company ?></option>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="customerFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="country_filter"><?= $this->lang->line("country") ?></label>
                                            <?php $countryId = (isset($_POST['country_filter'])) ? $_POST['country_filter']: '' ?>
                                            <select class="form-control select" name="country_filter" id="country_filter">
                                                <option value=""><?= $this->lang->line("alls") ?></option>
                                                <?php if (!empty($countries)) : ?>
                                                    <?php foreach ($countries as $country): ?>
                                                        <option value="<?= $country->NOMBRE ?>" code="<?= $country->CODIGO ?>" <?= ($country->NOMBRE == $countryId) ? 'selected' : '' ?>><?= $country->NOMBRE ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                            <input type="hidden" name="countryCode_filter" id="countryCode_filter" value="<?= $_POST['countryCode_filter'] ?? '' ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="state_filter"><?= $this->lang->line("state") ?></label>
                                            <?php $stateId = (isset($_POST['state_filter'])) ? $_POST['state_filter']: '' ?>
                                            <select class="form-control" name="state_filter" id="state_filter">
                                                <option value=""><?= $this->lang->line("alls") ?></option>
                                                <?php if (!empty($states)) : ?>
                                                    <?php foreach ($states as $state): ?>
                                                        <option value="<?= $state->DEPARTAMENTO ?>" data-code="<?= $state->CODDEPARTAMENTO ?>" <?= ($state->DEPARTAMENTO == $stateId) ? 'selected' : '' ?>><?= $state->DEPARTAMENTO ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                            <input type="hidden" name="stateCode_filter" id="stateCode_filter" value="<?= $_POST['stateCode_filter'] ?? '' ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="city_filter"><?= $this->lang->line("city"); ?></label>
                                            <?php $cityId = (isset($_POST['city_filter'])) ? $_POST['city_filter']: '' ?>
                                            <select class="form-control" name="city_filter" id="city_filter">
                                                <option value=""><?= $this->lang->line("allsf") ?></option>
                                                <?php if (!empty($cities)) : ?>
                                                    <?php foreach ($cities as $city): ?>
                                                        <option value="<?= $city->DESCRIPCION ?>" data-code="<?= $city->CODIGO ?>" <?= ($city->DESCRIPCION == $cityId) ? 'selected' : '' ?>><?= $city->DESCRIPCION ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                            <input type="hidden" name="cityCode_filter" id="cityCode_filter" value="<?= $_POST['cityCode_filter'] ?? '' ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="zone_filter"><?= $this->lang->line("zone"); ?></label>
                                            <?php $zoneId = (isset($_POST['zone_filter'])) ? $_POST['zone_filter']: '' ?>
                                            <select class="form-control select" name="zone_filter" id="zone_filter">
                                                <option value=""><?= $this->lang->line("allsf") ?></option>
                                                <?php if (!empty($zones)) : ?>
                                                    <?php foreach ($zones as $zone): ?>
                                                        <option value="<?= $zone->id ?>" data-code="<?= $zone->zone_code ?>" <?= ($zone->id == $zoneId) ? 'selected' : '' ?>><?= $zone->zone_name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                            <input type="hidden" name="zoneCode_filter" id="zoneCode_filter" value="<?= $_POST['zoneCode_filter'] ?? '' ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="subzone_filter"><?= $this->lang->line("subzone"); ?></label>
                                            <?php $subzoneId = (isset($_POST['subzone_filter'])) ? $_POST['subzone_filter']: '' ?>
                                            <select class="form-control select" name="subzone_filter" id="subzone_filter">
                                                <option value=""><?= $this->lang->line("allsf") ?></option>
                                                <?php if (!empty($subzones)) : ?>
                                                    <?php foreach ($subzones as $subzone): ?>
                                                        <option value="<?= $subzone->id ?>" data-code="<?= $subzone->subzone_code ?>" <?= ($subzone->id == $subzoneId) ? 'selected' : '' ?>><?= $subzone->subzone_name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                            <input type="hidden" name="subzoneCode_filter" id="subzoneCode_filter" value="<?= $_POST['subzoneCode_filter'] ?? '' ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="birth_month_filter"><?= $this->lang->line('birth_month') ?></label>
                                            <?php $monthId = (isset($_POST["birth_month_filter"])) ? $_POST["birth_month_filter"] : ''; ?>
                                            <select class="form-control" id="birth_month_filter" name="birth_month_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($months as $months) : ?>
                                                    <option value="<?= $months->id ?>" <?= ($months->id == $monthId) ? 'selected' : '' ?>><?= $months->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="customer_products_filter"><?= $this->lang->line('customer_products') ?></label>
                                            <?php $productCategory = (isset($_POST['customer_products_filter'])) ? $_POST['customer_products_filter'] : '' ?>
                                            <select class="form-control" name="customer_products_filter" id="customer_products_filter">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php if (!empty($customerProducts)) : ?>
                                                    <?php foreach ($customerProducts as $customerProducts) : ?>
                                                        <option value="<?= $customerProducts->product_id ?>" <?= ($customerProducts->product_id == $productCategory) ? 'selected' : '' ?>><?= $customerProducts->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label for="partnerId_filter"><?= $this->lang->line('partner') ?></label>
                                        <?php $partnerId = (isset($_POST["partnerId_filter"])) ? $_POST["partnerId_filter"] : ''; ?>
                                        <select class="form-control" name="partnerId_filter" id="partnerId_filter">
                                            <option value=""><?= lang('alls') ?></option>
                                            <?php if (!empty($partners)) : ?>
                                                <?php foreach ($partners as $partner): ?>
                                                    <option value="<?= $partner->id ?>" <?= ($partner->id == $partnerId) ? 'selected' : '' ?>><?= $partner->company ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="tax_exempt_customer__filter" style="margin-top: 25px">
                                                <?php $taxExemptCustomer = (isset($_POST["tax_exempt_customer_filter"])) ? $_POST["tax_exempt_customer_filter"] : ''; ?>
                                                <input type="checkbox" name="tax_exempt_customer_filter" id="tax_exempt_customer_filter" <?= (!empty($taxExemptCustomer) ? 'checked' : '') ?>> <?= $this->lang->line('tax_exempt_customer') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) {
    echo admin_form_open('customers/customer_actions', 'id="action-form"');
} ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-hover" id="CusData">
                                    <thead>
                                        <tr class="primary">
                                            <th>
                                                <input class="checkbox checkth" type="checkbox" name="check"    />
                                            </th>
                                            <th><?= lang("image") ?></th>
                                            <th><?= lang("company"); ?></th>
                                            <th><?= lang("name"); ?></th>
                                            <th><?= lang("email_address"); ?></th>
                                            <th><?= lang("phone"); ?></th>
                                            <th><?= lang("price_group"); ?></th>
                                            <th><?= lang("customer_group"); ?></th>
                                            <th><?= lang("vat_no"); ?></th>
                                            <th><?= lang("deposit"); ?></th>
                                            <th><?= lang("award_points"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin  || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script>
    $(document).ready(function() {
        var option_filter = false;
        _tabFilterFill = false;

        loadDataTables();

        $(document).on('change', '#country_filter', function(){ getStates(); });
        $(document).on('change', '#state_filter', function(){ getCities(); });
        $(document).on('change', '#city_filter', function() { getZones(); });
        $(document).on('change', '#zone_filter', function() { getSubzones() });
        $(document).on('change', '#subzone_filter', function() { getSubzoneCode() });

        $('[data-toggle="tooltip"]').tooltip();
    });

    function loadDataTables()
    {
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $(document).ready(function () {
            oTable = $('#CusData').dataTable({
                aaSorting: [[2, "asc"]],
                aLengthMenu: nums,
                iDisplayLength: $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
                bProcessing: true,
                bServerSide: true,
                sAjaxSource: '<?= admin_url('customers/getCustomers') ?>',
                dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
                fnServerData: function (sSource, aoData, fnCallback) {
                    aoData.push({"name": "<?= $this->security->get_csrf_token_name() ?>", "value": "<?= $this->security->get_csrf_hash() ?>"});
                    aoData.push({"name": "type_person_filter", "value": $('#type_person_filter').val() });
                    aoData.push({"name": "tipo_regimen_filter", "value": $('#tipo_regimen_filter').val() });
                    aoData.push({"name": "customer_group_id_filter", "value": $('#customer_group_id_filter').val() });
                    aoData.push({"name": "price_group_id_filter", "value": $('#price_group_id_filter').val() });
                    aoData.push({"name": "customer_payment_type_filter", "value": $('#customer_payment_type_filter').val() });
                    aoData.push({"name": "customer_seller_id_assigned_filter", "value": $('#customer_seller_id_assigned_filter').val() });
                    aoData.push({"name": "country_filter", "value": $('#country_filter').val() });
                    aoData.push({"name": "state_filter", "value": $('#state_filter').val() });
                    aoData.push({"name": "city_filter", "value": $('#city_filter').val() });
                    aoData.push({"name": "zone_filter", "value": $('#zone_filter').val() });
                    aoData.push({"name": "subzone_filter", "value": $('#subzone_filter').val() });
                    aoData.push({"name": "birth_month_filter", "value": $('#birth_month_filter').val() });
                    aoData.push({"name": "customer_products_filter", "value": $('#customer_products_filter').val() });
                    aoData.push({"name": "partnerId_filter", "value": $('#partnerId_filter').val() });
                    aoData.push({"name": "tax_exempt_customer_filter", "value": $('#tax_exempt_customer_filter').is(':checked') ? 1 : 0 });
                    aoData.push({"name": "option_filter", "value": option_filter });

                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex)
                {
                    nRow.id = aData[0];
                    nRow.className = "customer_details_link";
                    return nRow;
                },
                fnDrawCallback: function(settings)
                {
                    if (!_tabFilterFill) {
                        $('.additionalControlsContainer').html('<div class="wizard">' +
                            '<div class="steps clearfix">' +
                                '<ul role="tablist">'+
                                    '<li role="tab" class="first current index_list" aria-disabled="false" aria-selected="true" style="width:16.6%">'+
                                        '<a class="wizard_index_step" data-optionfilter="active"><span class="active_span">0</span><br>Act.</a>'+
                                    '</li>'+
                                    '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%">'+
                                        '<a class="wizard_index_step" data-optionfilter="all"><span class="all_span">0</span><br>Todos</a>'+
                                    '</li>'+
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%">'+
                                        '<a class="wizard_index_step" data-optionfilter="inactive"><span class="inactive_span">0</span><br>Inac.</a>'+
                                    '</li>'+
                                    '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%">'+
                                        '<a class="wizard_index_step" data-optionfilter="user"><span class="user_span"></span><br>Usuarios</a>'+
                                    '</li>'+
                                    '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%">'+
                                        '<a class="wizard_index_step" data-optionfilter="advances"><span class="advances_span"></span><br>Anticipos</a>'+
                                    '</li>'+
                                    '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%">'+
                                        '<a class="wizard_index_step" data-optionfilter="giftCard"><span class="giftCard_span"></span><br>Tarjeta regalo</a>'+
                                    '</li>'+
                                '</ul>'+
                            '</div>'+
                        '</div>').addClass('col-without-padding');

                        _tabFilterFill = true;
                    }

                    $('.actionsButtonContainer').html(`<a href="<?= admin_url('customers/add') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" id="add" data-toggle-second="tooltip" data-placement="top" title="Agregar">
                        <i class="fas fa-plus fa-lg"></i>
                    </a>
                    <div class="dropdown pull-right"">
                        <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="<?= admin_url('customers/import_csv'); ?>" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus-circle"></i> <?= lang("import_by_csv"); ?>
                                </a>
                            </li>
                            <?php if ($Owner || $Admin) { ?>
                            <li>
                                <a href="#" id="excel" data-action="export_excel">
                                    <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                </a>
                            </li>
                            <li>
                                <a href="#" id="excel" data-action="export_all_to_excel">
                                    <i class="fa fa-file-excel-o"></i> <?= lang('export_all_to_excel') ?>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if ($Owner) { ?>
                            <li class="divider"></li>
                            <li>
                                <a href="#" class="bpo" title="<b><?= $this->lang->line("deactivate_customers") ?></b>" 
                                    data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='deactivate'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" 
                                    data-html="true" data-placement="left">
                                    <i class="fa fa-trash-o"></i> <?= lang('deactivate_customers') ?>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>`);

                    $('[data-toggle-second="tooltip"]').tooltip();
                    $('input[type="checkbox"]').iCheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        radioClass: 'iradio_flat-blue',
                        increaseArea: '20%'
                    });

                    loadDataTabFilters();
                },
                aoColumns: [
                    {"bSortable": false, "mRender": checkbox },
                    {
                        className: 'text-center',
                        render: function(data, type, row) {
                            if (data !== null) {
                                return '<img class="img-circle" src="assets/uploads/avatars/'+ data +'" style="width: 48px; height: 48px;"/>';
                            } else {
                                return '<img class="img-circle" src="assets/uploads/avatars/defaultUser.jpg" style="width: 48px; height: 48px;"/>';
                            }
                        }
                    },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"mRender": currencyFormat},
                    null,
                    {"mRender": generalStatus},
                    {"bSortable": false,"bSearchable": false}
                ]
            });

            // $('#myModal').on('hidden.bs.modal', function () {
            //     customerTables.fnDraw( false );
            // });
        });
    }

    function loadDataTabFilters()
    {
        $.ajax({
            url : '<?= admin_url('customers/getCustomers') ?>',
            dataType : 'JSON',
            type : 'POST',
            data : {
                '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                'type_person_filter': $('#type_person_filter').val(),
                'tipo_regimen_filter': $('#tipo_regimen_filter').val(),
                'customer_group_id_filter': $('#customer_group_id_filter').val(),
                'price_group_id_filter': $('#price_group_id_filter').val(),
                'customer_payment_type_filter': $('#customer_payment_type_filter').val(),
                'customer_seller_id_assigned_filter': $('#customer_seller_id_assigned_filter').val(),
                'country_filter': $('#country_filter').val(),
                'state_filter': $('#state_filter').val(),
                'city_filter': $('#city_filter').val(),
                'zone_filter': $('#zone_filter').val(),
                'subzone_filter': $('#subzone_filter').val(),
                'birth_month_filter': $('#birth_month_filter').val(),
                'tax_exempt_customer_filter': $('#tax_exempt_customer_filter').is(':checked') ? 1 : 0,
                'customer_products_filter': $('#customer_products_filter').val(),
                'partnerId_filter': $('#partnerId_filter').val(),
                'option_filter': option_filter,
                'get_json' : true
            }
        }).done(function(data){
            $('.active_span').text(data.active);
            $('.all_span').text(data.all);
            $('.inactive_span').text(data.inactive);
            $('.user_span').text(data.user);
            $('.advances_span').text(data.advances);
            $('.giftCard_span').text(data.giftCard);
        });
    }

    function getStates()
    {
        var country = $('#country_filter option:selected').attr('code');
        $('#countryCode_filter').val(country);

        $.ajax({
            url: site.base_url+"customers/get_states/"+country,
        }).done(function(response) {
            $('#state_filter').html(response);
            $('#state_filter').trigger('change');
        }).fail(function(data) {
          console.log(data.responseText);
        });
    }

    function getCities()
    {
        var state = $('#state_filter option:selected').data('code');
        $('#stateCode_filter').val(state);

        $.ajax({
          url: site.base_url+"customers/get_cities/"+state,
        }).done(function(response) {
            $('#city_filter').html(response);
            $('#city_filter').trigger('change');
        }).fail(function(data) {
          console.log(data.responseText);
        });
    }

    function getZones()
    {
        var city = $('#city_filter option:selected').data('code');
        $('#cityCode_filter').val(city);

        $.ajax({
            url: site.base_url+'customers/get_zones/'+city
        }).done(function(data) {
            $('#zone_filter').html(data);
            $('#zone_filter').trigger('change');
        });
    }

    function getSubzones()
    {
        var code = $('#zone_filter option:selected').data('code');
        $('#zoneCode_filter').val(code);

        $.ajax({
            url: site.base_url +'customers/get_subzones/'+ code
        }).done(function(data){
            $('#subzone_filter').html(data);
            $('#subzone_filter').trigger('change');
        });
    }

    function getSubzoneCode()
    {
        var code = $('#subzone_filter option:selected').data('code');
        $('#subzoneCode_filter').val(code);
    }
</script>