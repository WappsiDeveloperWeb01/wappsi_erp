<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css" media="screen">
    #PRData td:nth-child(7) { text-align: right; }

    <?php if($Owner || $Admin || $this->session->userdata('show_cost')) { ?>
        #PRData td:nth-child(9) { text-align: right; }
    <?php } if($Owner || $Admin || $this->session->userdata('show_price')) { ?>
        #PRData td:nth-child(8) { text-align: right; }
    <?php } ?>
</style>

<script>
    var oTable = false;

    $(document).ready(function ()
    {
        $("#form_recosting").validate({
            ignore: []
        });
        $(document).on('click', '#clean_filters_button', function() { clean_filters(); })


        $(document).on('click', '#load_data', function(){
            load_data();
        });
    });

    function load_data(){
        if (oTable == false) {
            $('#table_Data').fadeIn();
            if ($(window).width() < 1000) {
                var nums = [[10, 25], [10, 25]];
            } else {
                var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
            }

            oTable = $('#PRData').dataTable({
                "aaSorting": [[2, "asc"], [3, "asc"]],
                "aLengthMenu": nums,
                "iDisplayLength": 100,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('products/getRecostingProducts'); ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({"name": "<?= $this->security->get_csrf_token_name() ?>", "value": "<?= $this->security->get_csrf_hash() ?>"});
                    aoData.push({"name": "category", "value": $('#category').val()});
                    aoData.push({"name": "product_id", "value": $('#report_product_id').val()});
                    aoData.push({"name": "hide_recosted_products", "value": ($('#hide_recosted_products').is(':checked') ? true : false)});

                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                    nRow.id = aData[0];
                    nRow.className = "product_link";
                    return nRow;
                },
                "aoColumns": [
                    {"bSortable": false, "mRender": checkbox},
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"mRender": discontinued_status},
                    {"bSortable": false},
                ]
            }).fnSetFilteringDelay().dtFilter([
            ], "footer");
        } else {
            oTable.fnUpdate();
        }
    }
</script>

<?= admin_form_open('products/recosting_selection', 'id="form_recosting"'); ?>
<div class="wrapper wrapper-content  animated fadeInRight no-print">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <?= lang('start_date', 'rc_start_date') ?>
                                    <input type="date" name="start_date" id="rc_start_date" class="form-control"  value="<?= date('Y-01-01') ?>" required>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <?= lang('end_date', 'rc_end_date') ?>
                                    <input type="date" name="end_date" id="rc_end_date" class="form-control" value="<?= date('Y-m-d') ?>" required>
                                </div>
                                <!-- <div class="col-sm-4 form-group">
                                    <?= lang('warehouse', 'rc_warehouse') ?>
                                    <?php
                                    $wh[""] = lang('select');
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, '', 'class="form-control" id="rc_warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"  required');
                                    ?>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <label>
                                        <input type="checkbox" name="show_recosting_documentation" class="form-control">
                                        <?= lang('show_recosting_documentation') ?>
                                    </label>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <button type="button" name="boton_name" id="submit_recosting" class="btn btn-primary"> <?= lang("send"); ?></button>
                                </div>

                                <?php if ($this->session->userdata('products_without_movements')): ?>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-sm-12">
                                    <label><?= lang('products_without_movements') ?></label>
                                    <textarea class="form-control skip" readonly><?= $this->session->userdata('products_without_movements') ?></textarea>
                                </div>
                                    <?php $this->session->unset_userdata('products_without_movements') ?>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("product", "suggest_product"); ?>
                                        <?php echo form_input('sproduct', (isset($_POST['sproduct']) ? $_POST['sproduct'] : ""), 'class="form-control" id="suggest_product"'); ?>
                                        <input type="hidden" name="product" value="<?= isset($_POST['product']) ? $_POST['product'] : "" ?>" id="report_product_id"/>
                                    </div>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <?= lang("category", "category") ?>
                                    <select name="category" class="form-control select" id="category"  style="width:100%">
                                        <option value=""><?= lang('select')." ".lang('category') ?></option>
                                        <?php
                                        foreach ($categories as $category) { ?>
                                            <option value="<?= $category->id ?>"><?= $category->name ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <br>
                                <div class="col-sm-4 form-group">
                                    <label>
                                        <input type="checkbox" name="hide_recosted_products" id="hide_recosted_products" checked>
                                        <?= lang('hide_recosted_products') ?>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <button class="btn btn-primary" type="button" id="load_data"><i class="fa fa-search"></i>  <?= lang('search') ?></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <hr>
                            <div class="table-responsive" id="table_Data" style="display:none;">
                                <table id="PRData" class="table table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr class="primary">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkth" type="checkbox" name="check"/>
                                            </th>
                                            <th><?= lang("code") ?></th>
                                            <th><?= lang("referece") ?></th>
                                            <th><?= lang("name") ?></th>
                                            <th><?= lang("type") ?></th>
                                            <th><?= lang("category") ?></th>
                                            <th><?= lang("quantity") ?></th>
                                            <th><?= lang("product_details"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th style="min-width:65px; text-align:center;"><?= lang("actions") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="width:65px; text-align:center;"><?= lang("actions") ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= form_close() ?>

<script type="text/javascript">
    $(document).on('click', '#submit_recosting', function(){
        if ($('#form_recosting').valid()) {
            $('#form_recosting').submit();
        }
    });
</script>