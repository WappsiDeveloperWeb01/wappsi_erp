<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php setlocale(LC_ALL, "es-ES"); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-9 col-lg-offset-1">
                            <div class="table-responsive">
                                <table id="items_table" class="table table-hover " style="width: 100%">
                                    <thead>
                                        <tr class="primary">
                                            <th></th>
                                            <th></th>
                                            <th><?= ($this->Payroll_settings->payment_frequency == BIWEEKLY) ? "Quincena" : "Mes"; ?></th>
                                            <th>Concepto</th>
                                            <th></th>
                                            <th>Dias/horas</th>
                                            <th></th>
                                            <th class="col-sm-2">Devengados</th>
                                            <th class="col-sm-2">Deducciones</th>
                                            <th></th>
                                            <th><?= lang('actions') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody class="pointer">
                                        <tr>
                                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="selectionPayroll">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seleccionar Quincena a Ajustar</h4>
            </div>
            <div class="modal-body">
            <?php
                $payrolls = explode(",", $grouped_payroll->ids);

                foreach ($payrolls as $key => $payroll) { ?>
                    <?php $name_button = ($key == 0) ? "Primera Quincena" : "Segunda Quincena"; ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <button class="btn btn-primary btn-block openAddNovetlyForm" value="<?= $payroll ?>" data-dismiss="modal"><?= $name_button ?></button>
                        </div>
                    </div>
                    <?= ($key == 0) ? "<br>" : ""; ?>
                <?php } ?>
                <input type="hidden" name="path" id="path" value="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default new-button" data-dismiss="modal" data-toggle="tooltip" data-placement="top" title="<?= lang('close') ?>"><i class="fas fa-close fa-lg"></i></button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        load_datatable();

        $(document).on('click', '#addNovetly a', function(event) { selectBiweekly(event, $(this));});
        $(document).on('click', '.openAddNovetlyForm', function(event) { addNovetly($(this).val()); });
        $(document).on('click', '.remove_novelty a', function(event) { remove_novelty(event, $(this)); });
        $(document).on('click', '.create_adjustment a', function(event) { create_adjustment(event, $(this)); });
    });

    function load_datatable()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#items_table').dataTable({
            "aaSorting": [[4, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('payroll_electronic/get_payroll_items_for_adjusment_datatables/'. $electronic_payroll->year .'/'. $electronic_payroll->month .'/'. $employee_id .'/'. $contract_id) ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            drawCallback: function(settings) {
                <?php if ($this->Owner || $this->Admin) { ?>
                    $('.actionsButtonContainer').html('<div class="dropdown pull-right">'+
                        '<button class="btn btn-primary btn-outline new-button" type="button" id="dropdownMenu" data-toggle="dropdown" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>">'+
                            '<i class="fas fa-ellipsis-v fa-lg"></i>'+
                        '</button>'+
                        '<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu">'+
                            '<li id="addNovetly"><a href="<?= admin_url('payroll_electronic/add_novetly') ?>"><i class="fa fa-plus"></i> <?= lang('add') . " novedad" ?></a></li>'+
                            '<li class="create_adjustment"><a href="<?= admin_url("payroll_electronic/save_adjustment/".$electronic_payroll->id."/".$electronic_payroll_employee_id .'/'. $contract_id) ?>"><i class="fa fa-check"></i> <?= lang('create_adjustmet') ?></a></li>'+
                        '</ul>'+
                    '</div>');
                <?php } else { ?>
                    <?php if ($GP['payroll_management-add_novelty']) { ?>
                        $('.actionsButtonContainer').html('<div class="dropdown pull-right">'+
                            '<button class="btn btn-primary btn-outline new-button" type="button" id="dropdownMenu" data-toggle="dropdown" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>">'+
                                '<i class="fas fa-ellipsis-v fa-lg"></i>'+
                            '</button>'+
                            '<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu">'+
                                '<li id="addNovetly"><a href="<?= admin_url('payroll_electronic/add_novetly') ?>"><i class="fa fa-plus"></i> <?= lang('add') . " novedad" ?></a></li>'+
                                '<li class="create_adjustment"><a href="<?= admin_url("payroll_electronic/save_adjustment/".$electronic_payroll->id."/".$electronic_payroll_employee_id .'/'. $contract_id) ?>"><i class="fa fa-check"></i> <?= lang('create_adjustmet') ?></a></li>'+
                            '</ul>'+
                        '</div>');
                    <?php } ?>
                <?php } ?>
            },
            "aoColumns": [
                { visible: false},
                { visible: false},
                {
                    className: "text-center",
                    render: function(data, b, row) {
                        <?php if ($this->Payroll_settings->payment_frequency == BIWEEKLY) { ?>
                            return data;
                        <?php } else { ?>
                            var month = row[1];
                            _months = { 1: "Enero", 2: "Febrero", 3: "Marzo", 4: "Abril", 5: "Mayo", 6: "Junio", 7: "Julio", 8: "Agosto", 9: "Septiembre", 10: "Octubre", 11: "Noviembre", 12: "Diciembre"};
                            return _months[month];
                        <?php } ?>
                    }
                },
                {
                    render: function(data, b, row) {
                        var description = row[4];
                        if (description == '' || description == null) {
                            return data;
                        } else {
                            return data +'<br><small>'+ description +'</small>';
                        }
                    }
                },
                { visible: false},
                {
                    className: 'text-center',
                    render: function(data, b, row) {
                        var time_unit = row[6];
                        var amount = (time_unit == 'horas') ? data : parseInt(data);

                        if (amount == 0) {
                            return '';
                        } else {
                            return parseInt(amount) +' '+ time_unit;
                        }
                    }
                },
                { visible: false},
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                { visible: false },
                { bSortable: false }

            ]
        });
    }

    function selectBiweekly(event, element)
    {
        event.preventDefault();

        $('#path').val(element.attr('href'));
        <?php if ($this->Payroll_settings->payment_frequency == BIWEEKLY) { ?>
            $('#selectionPayroll').modal('show');
        <?php } else { ?>
            <?php $payroll_id = $grouped_payroll->ids; ?>
            addNovetly(<?= $payroll_id ?>);
        <?php } ?>

    }

    function addNovetly(payroll_id)
    {
        $('#selectionPayroll').modal('hide');

        setTimeout(function f()
        {
            var path = $('#path').val();
            var employee_id = '<?= $employee_id ?>';
            var contract_id = '<?= $contract_id ?>';

            $('#myModal').modal({remote: path +'/'+ payroll_id +'/'+ employee_id +'/'+ contract_id });
            $('#myModal').modal('show');
        }, 800, payroll_id);
    }

    function create_adjustment(event, element)
    {
        event.preventDefault();
        swal({
            title: '¿Está seguro de guardar el Ajuste de Nómina?',
            text: 'Se realizará el envío del ajuste',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, Crear!",
            closeOnConfirm: false,
            closeOnClickOutside: false,
        }, function() {
            swal({
                title: 'Creando ajuste de Nómina',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });
            window.location.href = element.attr('href');
        });
    }

    function remove_novelty(event, element)
    {
        event.preventDefault();
        swal({
            title: '¿Está seguro de eliminar la novedad?',
            text: 'Se realizará el recálculo para los Parafiscales y las provisiones',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, Borrar!",
            closeOnConfirm: false,
            closeOnClickOutside: false,
        }, function() {
            swal({
                title: 'Eliminando novedad',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });
            window.location.href = element.attr('href');
        })
    }
</script>
