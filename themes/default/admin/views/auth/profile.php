<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
    $user->biller_id = json_decode($user->biller_id);
    $biller_id_assigneds = [];
    if ($user->biller_id) {
        if (is_array($user->biller_id) || is_object($user->biller_id)) {
            foreach ($user->biller_id as $key => $value) {
                $biller_id_assigneds[$value] = 1;
            }
        } else {
                $biller_id_assigneds[$user->biller_id] = 1;
        }
    }
 ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content row">
            <ul id="myTab" class="nav nav-tabs col-sm-12">
                <li class=""><a href="#edit" class="tab-grey"><?= lang('edit') ?></a></li>
                <li class=""><a href="#cpassword" class="tab-grey"><?= lang('change_password') ?></a></li>
                <li class=""><a href="#avatar" class="tab-grey"><?= lang('avatar') ?></a></li>
            </ul>
            <div class="tab-content col-sm-12 row">
                <div id="edit" class="tab-pane fade in">
                    <div class="box">
                        <div class="box-content col-sm-12 row">
                            <?= admin_form_open('auth/edit_user/' . $user->id, ['id' => 'edit_user']); ?>
                                <?= form_hidden('id', $id); ?>
                                <?= form_hidden($csrf); ?>

                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= lang('first_name', 'first_name'); ?>
                                                <div class="controls">
                                                    <?= form_input('first_name', $user->first_name, 'class="form-control" id="first_name" required="required"'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= lang('last_name', 'last_name'); ?>
                                                <div class="controls">
                                                    <?= form_input('last_name', $user->last_name, 'class="form-control" id="last_name" required="required"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (!$this->ion_auth->in_group('customer', $id) && !$this->ion_auth->in_group('supplier', $id)) { ?>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('company', 'company'); ?>
                                                    <div class="controls">
                                                        <?= form_input('company', $user->company, 'class="form-control" id="company" required="required"'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= form_hidden('company', $user->company); ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= lang('phone', 'phone'); ?>
                                                <div class="controls">
                                                    <input type="tel" name="phone" class="form-control" id="phone"
                                                    required="required" value="<?= $user->phone ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= lang('gender', 'gender'); ?>
                                                <div class="controls">  <?php
                                                    $ge[''] = array('male' => lang('male'), 'female' => lang('female'));
                                                    echo form_dropdown('gender', $ge, (isset($_POST['gender']) ? $_POST['gender'] : $user->gender), 'class="tip form-control" id="gender" required="required"');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if (($Owner || $Admin) && $id != $this->session->userdata('user_id')) { ?>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('award_points', 'award_points'); ?>
                                                    <?= form_input('award_points', set_value('award_points', $user->award_points), 'class="form-control tip" id="award_points"  required="required"'); ?>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= lang('user_pc_serial', 'user_pc_serial'); ?>
                                                <input type="text" name="user_pc_serial" class="form-control" id="user_pc_serial"
                                                value="<?= $user->user_pc_serial ?>"/>
                                            </div>
                                        </div>

                                        <?php if ($Owner && $id != $this->session->userdata('user_id')) { ?>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('username', 'username'); ?>
                                                    <input type="text" name="username" class="form-control"
                                                    id="username" value="<?= $user->username ?>"
                                                    required="required"/>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('email', 'email'); ?>
                                                    <input type="email" name="email" class="form-control" id="email"
                                                    value="<?= $user->email ?>" required="required"/>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('password', 'password'); ?>
                                                    <?= form_input($password); ?>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('confirm_password', 'password_confirm'); ?>
                                                    <?= form_input($password_confirm); ?>
                                                </div>
                                            </div>                                            
                                        <?php } ?>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button class="btn btn-primary submit_update" type="button"><?= lang('update') ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">
                                        <?php if ($Owner && $id != $this->session->userdata('user_id')) { ?>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang("language", "language"); ?>
                                                    <?php
                                                        $lang = [ 
                                                            'arabic'                    => 'Arabic',
                                                            'english'                   => 'English',
                                                            'german'                    => 'German',
                                                            'portuguese-brazilian'      => 'Portuguese (Brazil)',
                                                            'simplified-chinese'        => 'Simplified Chinese',
                                                            'spanish'                   => 'Spanish',
                                                            'thai'                      => 'Thai',
                                                            'traditional-chinese'       => 'Traditional Chinese',
                                                            'turkish'                   => 'Turkish',
                                                            'vietnamese'                => 'Vietnamese'
                                                        ]
                                                    ?>
                                                    <?= form_dropdown('language', $lang, $user->language ? $user->language : $Settings->language, 'class="form-control tip" id="language" required="required" style="width:100%;"') ?>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('status', 'status'); ?>
                                                    <?php
                                                    $opt = array(1 => lang('active'), 0 => lang('inactive'));
                                                    echo form_dropdown('status', $opt, (isset($_POST['status']) ? $_POST['status'] : $user->active), 'id="status" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>

                                            <?php if (!$this->ion_auth->in_group('customer', $id) && !$this->ion_auth->in_group('supplier', $id)) { ?>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?= lang("group", "group"); ?>
                                                        <select name="group" id="group" required="required" class="form-control select">
                                                            <?php foreach ($groups as $group): ?>
                                                            <option value="<?= $group['id'] ?>" <?= $user->group_id == $group['id'] ? 'selected="selected"' : '' ?> data-relationedcompanies="<?= $group['relationed_companies'] ?>" data-groupname="<?= $group['name'] ?>"><?= ucwords(mb_strtolower(($group['description']))) ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                        <input type="hidden" name="group_name" id="group_name">
                                                    </div>
                                                </div>

                                                <div class="companies_relation">
                                                    <div class="col-md-12">
                                                        <div class="from-group" style="width: 100%;">
                                                            <?= lang('seller_corresponds_to', 'company_id') ?>
                                                            <select name="company_id" id="company_id" class="form-control">
                                                            </select>
                                                            <em class="text-danger"><?= lang('input_required') ?></em>
                                                            <input type="hidden" name="relationedcompanies" id="relationedcompanies" value="0">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group" style="width: 100%; margin: 0 !important">
                                                            <?= lang('register_cash_movements_with_another_user', 'register_cash_movements_with_another_user') ?>
                                                            <select name="register_cash_movements_with_another_user" id="register_cash_movements_with_another_user" class="form-control">
                                                                <option value=""><?= lang('use_own_cash_register') ?></option>
                                                                <?php foreach ($users as $euser): ?>
                                                                <?php if ($user->id != $euser->id): ?>
                                                                <option value="<?= $euser->id ?>" <?= $user->register_cash_movements_with_another_user == $euser->id ? 'selected' : '' ?>><?= $euser->first_name." ".$euser->last_name." (".$euser->username.")" ?></option>
                                                                <?php endif ?>
                                                                <?php endforeach ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                    <div class="clearfix"></div>

                                                    <div class="col-md-12">
                                                        <div class="administrator">
                                                            <div class="form-group">
                                                                <?= lang("main_administrator", "main_administrator"); ?>
                                                                <select name="main_administrator" id="main_administrator" class="form-control">
                                                                    <option value="0"><?= lang('no') ?></option>
                                                                    <option value="1"><?= lang('yes') ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="no">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("biller", "biller"); ?>
                                                                <select name="biller[]" id="biller" class="form-control">
                                                                    <option value=""><?= lang('select') ?></option>
                                                                    <?php foreach ($billers as $biller): ?>
                                                                    <option value="<?= $biller->id ?>" <?= isset($biller_id_assigneds[$biller->id]) ? 'selected="selected"' : '' ?>><?= $biller->company ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                    <?= lang('document_type', 'sale_document_type_id') ?>
                                                                <select name="sale_document_type_id" class="form-control" id="sale_document_type_id"></select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                    <?= lang('fe_document_type', 'doc_type_id') ?>
                                                                <select name="doc_type_id" class="form-control" id="doc_type_id"></select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                    <?= lang('pos_document_type', 'pos_doc_type_id') ?>
                                                                <select name="pos_doc_type_id" class="form-control" id="pos_doc_type_id"></select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                    <?= lang('fe_pos_document_type', 'fe_pos_doc_type_id') ?>
                                                                <select name="fe_pos_doc_type_id" class="form-control" id="fe_pos_doc_type_id"></select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("warehouse", "warehouse"); ?>
                                                                <?php
                                                                $wh[''] = lang('select').' '.lang('warehouse');
                                                                foreach ($warehouses as $warehouse) {
                                                                $wh[$warehouse->id] = $warehouse->name;
                                                                }
                                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $user->warehouse_id), 'id="warehouse" class="form-control select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" style="width:100%;" ');
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group seller_div">
                                                                <?= lang("seller", "seller"); ?>
                                                                <?php
                                                                $sl[""] = lang('select').' '.lang('seller');
                                                                foreach ($sellers as $seller) {
                                                                $sl[$seller->id] = $seller->company != '-' ? $seller->company : $seller->name;
                                                                }
                                                                echo form_dropdown('seller', $sl, (isset($_POST['seller']) ? $_POST['seller'] : $user->seller_id), 'id="seller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("seller") . '" class="form-control select" style="width:100%;"');
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("view_right", "view_right"); ?>
                                                                <?php
                                                                $vropts = array(1 => lang('all_records'), 0 => lang('own_records'));
                                                                echo form_dropdown('view_right', $vropts, (isset($_POST['view_right']) ? $_POST['view_right'] : $user->view_right), 'id="view_right" class="form-control select" style="width:100%;"');
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("edit_right", "edit_right"); ?>
                                                                <?php
                                                                $opts = array(1 => lang('yes'), 0 => lang('no'));
                                                                echo form_dropdown('edit_right', $opts, (isset($_POST['edit_right']) ? $_POST['edit_right'] : $user->edit_right), 'id="edit_right" class="form-control select" style="width:100%;"');
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("allow_discount", "allow_discount"); ?>
                                                                <?= form_dropdown('allow_discount', $opts, (isset($_POST['allow_discount']) ? $_POST['allow_discount'] : $user->allow_discount), 'id="allow_discount" class="form-control select" style="width:100%;"'); ?>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12">
                                                            <label class="checkbox" for="cant_finish_pos_invoice">
                                                                <input type="checkbox" name="cant_finish_pos_invoice" value="1" id="cant_finish_pos_invoice" <?= $user->cant_finish_pos_invoice == 1 ? 'checked' : '' ?> />
                                                                <?= lang('cant_finish_pos_invoice') ?>
                                                            </label>
                                                        </div>                                                    
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <?= lang('user_dashboard', 'dashboard_id') ?>
                                                            <select name="dashboard_id" id="dashboard_id" class="form-control">
                                                                <option value=""><?= lang('select') ?></option>
                                                                <?php if ($dashboards): ?>
                                                                <?php foreach ($dashboards as $dashboard): ?>
                                                                <option value="<?= $dashboard->id ?>" <?= $user->dashboard_id == $dashboard->id ? 'selected="selected"' : '' ?>><?= $dashboard->name ?></option>
                                                                <?php endforeach ?>
                                                                <?php endif ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <?= lang('print_pos_finalized_invoice', 'print_pos_finalized_invoice') ?>
                                                            <select name="print_pos_finalized_invoice" id="print_pos_finalized_invoice" class="form-control">
                                                                <option value=""><?= lang('select') ?></option>
                                                                <option value="0" <?= $user->print_pos_finalized_invoice == 0 ? 'selected' : '' ?>><?= lang('no_print') ?></option>
                                                                <option value="1" <?= $user->print_pos_finalized_invoice == 1 ? 'selected' : '' ?>><?= lang('print') ?></option>
                                                                <option value="2" <?= $user->print_pos_finalized_invoice == 2 ? 'selected' : '' ?>><?= lang('select_before_finishing') ?></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
                <div id="cpassword" class="tab-pane fade">
                    <div class="box">
                        <!-- <div class="box-header col-sm-12">
                            <h2 class="blue"><i class="fa-fw fa fa-key nb"></i><?= lang('change_password'); ?></h2>
                        </div> -->
                        <div class="box-content col-sm-12 row">
                            <?= admin_form_open("auth/change_password", 'id="change-password-form"'); ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang('old_password', 'curr_password'); ?> <br/>
                                        <?= form_password('old_password', '', 'class="form-control" id="curr_password" required="required"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label
                                        for="new_password"><?= sprintf(lang('new_password'), $min_password_length); ?></label>
                                        <br/>
                                        <?= form_password('new_password', '', 'class="form-control" id="new_password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-regexp-message="'.lang('pasword_hint').'"'); ?>
                                        <span class="help-block"><?= lang('pasword_hint') ?></span>
                                    </div>
                                    <div class="form-group">
                                        <?= lang('confirm_password', 'new_password_confirm'); ?> <br/>
                                        <?= form_password('new_password_confirm', '', 'class="form-control" id="new_password_confirm" required="required" data-bv-identical="true" data-bv-identical-field="new_password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                    </div>
                                    <?= form_input($user_id); ?>
                                    <p><?= form_submit('change_password', lang('change_password'), 'class="btn btn-primary"'); ?></p>
                                </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
                <div id="avatar" class="tab-pane fade">
                    <div class="box">
                        <!-- <div class="box-header col-sm-12">
                            <h2 class="blue"><i class="fa-fw fa fa-file-picture-o nb"></i><?= lang('change_avatar'); ?></h2>
                        </div> -->
                        <div class="box-content col-sm-12 row">
                            <div class="row">
                                <?= admin_form_open_multipart("auth/update_avatar"); ?>
                                <div class="form-group col-sm-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 200px;">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                                            <img alt="User Image" src="<?= $user->avatar ? base_url() . 'assets/uploads/avatars/thumbs/' . $user->avatar : base_url() . 'assets/images/' . $user->gender . '.png' ?>" style="width: 100% !important;">
                                        </div>
                                        <div>
                                            <span class="btn btn-file btn-profile-img btn-profile-img-edit">
                                            <span class="fileinput-new">
                                                <i class="fa fa-pencil"></i>
                                            </span>
                                            <span class="fileinput-exists">
                                                <i class="fa fa-pencil"></i>
                                            </span>
                                            <input type="file" name="avatar" id="userimageupdate" /></span>
                                            <!-- <a href="#" class="btn btn-danger fileinput-exists btn-profile-img btn-profile-img-remove" data-dismiss="fileinput"><i class="fa fa-trash"></i></a> -->
                                            <button type="submit" name="update_avatar" class="btn btn-primary fileinput-exists btn-profile-img btn-profile-img-submit"><i class="fa fa-check"></i></button>
                                        </div>
                                    </div>
                                    <br>
                                    <label><?= lang('customer_profile_photo') ?></label>
                                </div>
                                <?= form_hidden('id', $id); ?>
                                <?= form_hidden($csrf); ?>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#change-password-form').bootstrapValidator({
        message: 'Please enter/select a value',
        submitButtons: 'input[type="submit"]'
    });
    $('#edit_user').validate({
        ignore: []
    });
});

$(document).on('click', '.submit_update', function() {
    if ($('#edit_user').valid()) {
        $('#edit_user').submit();
    }
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#previewImage').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#userimageupdate").change(function() {
    readURL(this);
});
</script>
<?php if ($Owner && $id != $this->session->userdata('user_id')) { ?>
    <?php
     ?>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var user = JSON.parse('<?=json_encode($user)?>');

        $('#group').change(function(event) {
            var group = $(this).val();
            var groupname = $('#group option:selected').data('groupname');
            if (group == 1 || group == 2 || groupname == 'preparation') {
                $('.no').slideUp();
            } else {
                $('.no').slideDown();
            }
            if (group == 2) {
                $('.administrator').slideDown();
            } else {
                $('.administrator').slideUp();
            }
        });

        $('#main_administrator').val(user.main_administrator).trigger('change');

        // $('#group').trigger('change');

        $('#biller').on('change', function() {
            getDocumentTypeToSalesAjax()
            getDocumentTypeToElectronicBillingAjax()
            getDocumentTypePosAjax()
            getDocumentTypePosToElectronicBillingAjax()
        })

        $('#group').val(user.group_id).trigger('change');
        $('#biller').trigger('change')
    });

    function getDocumentTypeToSalesAjax()
    {
        $.ajax({
            url     : site.base_url + 'billers/getBillersDocumentTypesAjax',
            type    : 'POST',
            dataType: 'JSON',
            data    : {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                billerId        : $('#biller').val(),
                isItElectronic  : 0,
                module          : 2,
                selected        : '<?= $user->sale_document_type_id ?>'
            },
        }).done(function(data) {
            response = data;
            $('#sale_document_type_id').html(response.options).select2()

            let docTypeId = '<?= $user->sale_document_type_id ?>'
            const options = $(response.options);
            $.each(options, function(index, option) {
                if ($(option).val() == docTypeId) {
                    $('#sale_document_type_id').select2('val', docTypeId)
                }
            })

            showAlerts(response)
        });
    }

    function getDocumentTypeToElectronicBillingAjax()
    {
        $.ajax({
            url     : site.base_url + 'billers/getBillersDocumentTypesAjax',
            type    : 'POST',
            dataType: 'JSON',
            data    : {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                billerId        : $('#biller').val(),
                isItElectronic  : 1,
                module          : 2,
                selected        : '<?= $user->document_type_id ?>'
            },
        }).done(function(data) {
            response = data;
            $('#doc_type_id').html(response.options).select2()

            let docTypeId = '<?= $user->document_type_id ?>'
            const options = $(response.options);
            $.each(options, function(index, option) {
                if ($(option).val() == docTypeId) {
                    $('#doc_type_id').select2('val', docTypeId)
                }
            })

            showAlerts(response)
        });
    }

    function getDocumentTypePosAjax()
    {
        $.ajax({
            url     : site.base_url + 'billers/getBillersDocumentTypesAjax',
            type    : 'POST',
            dataType: 'JSON',
            data    : {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                billerId        : $('#biller').val(),
                isItElectronic  : 0,
                module          : 1,
            },
        }).done(function(data) {
            response = data;
            $('#pos_doc_type_id').html(response.options).select2()

            let docTypeId = '<?= $user->pos_document_type_id ?>'
            const options = $(response.options);
            $.each(options, function(index, option) {
                if ($(option).val() == docTypeId) {
                    $('#pos_doc_type_id').select2('val', docTypeId)
                }
            })

            showAlerts(response)
        });
    }

    function getDocumentTypePosToElectronicBillingAjax()
    {
        $.ajax({
            url     : site.base_url + 'billers/getBillersDocumentTypesAjax',
            type    : 'POST',
            dataType: 'JSON',
            data    : {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                billerId        : $('#biller').val(),
                isItElectronic  : 1,
                module          : 1,
            },
        }).done(function(data) {
            response = data;

            $('#fe_pos_doc_type_id').html(response.options).select2();

            let docTypeId = '<?= $user->fe_pos_document_type_id ?>'
            const options = $(response.options);
            $.each(options, function(index, option) {
                if ($(option).val() == docTypeId) {
                    $('#fe_pos_doc_type_id').select2('val', docTypeId)
                }
            })

            showAlerts(response)
        });
    }

    function showAlerts(response)
    {
        if (response.not_parametrized != "") {
            command: toastr.warning('Los documentos <b> (' + response.not_parametrized + ') no están parametrizados </b> en contabilidad', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "6000",
                "extendedTimeOut": "1000",
            });
        }

        if (response.status == 0) {
            $('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>");
        } else {
            $('.resAlert').css('display', 'none');
        }
    }

    $(document).on('change', '#group', function() {
        var selected = $('#group option:selected');
        $('#group_name').val(selected.data('groupname'));
        if (selected.data('relationedcompanies')) {
            $.ajax({
                url: "<?= admin_url('auth/get_companies') ?>",
                type: "get",
                data: { group_name: (selected.data('relationedcompanies') == 1 ? selected.data('groupname') : 'seller'), selected: '<?= $user->company_id ?>' }
            }).done(function(data) {
                if (data != false) {
                    $('.companies_relation').css('display', '');
                    $('#company_id').html(data);
                    $('#company_id').select2({ width: '100%', required: true });
                    $('#relationedcompanies').val(1);
                } else {
                    $('.companies_relation').css('display', 'none');
                    $('#relationedcompanies').val(0);
                }
            });
            $('.seller_div').fadeOut();
            $('#seller').select2('val', '');
            $('#biller').prop('required', true);
            if (site.settings.electronic_billing == 1) {
                $('#doc_type_id').prop('required', true);
            } else {
                $('#doc_type_id').prop('required', false);
            }
        } else {
            $('.companies_relation').css('display', 'none');
            $('#company_id').select2('val', '');
            $('#company_id').empty().select2({ required: false });
            $('#relationedcompanies').val(0);
            $('.seller_div').fadeIn();
            $('#biller').prop('required', false);
            $('#doc_type_id').prop('required', false);
        }
    });
</script>
<?php } ?>