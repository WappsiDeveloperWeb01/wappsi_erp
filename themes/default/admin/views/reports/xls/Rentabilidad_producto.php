<?php
require_once APPPATH.'../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

$i = $j = 0 ;

$j++;
$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(30);

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1:F1")->getFont()->setBold(true);

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, 'Codigo')
			->setCellValue('B'.$j, 'Producto')
			->setCellValue('C'.$j, 'Total')
			->setCellValue('D'.$j, 'Costo')
			->setCellValue('E'.$j, 'Utilidad')
			->setCellValue('F'.$j, 'Margen');

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(16);

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultColumnDimension()
			->setWidth(16);

foreach ($profitabilitys as $profitability) 
{

	$j++;
	$objPHPExcel->setActiveSheetIndex(0)->getStyle('D'.$j.':F'.$j)->getAlignment()->setHorizontal(
		PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	);

    $total_uni = number_format($profitability['total'],2,',','.');
    $total = number_format(($profitability['costo']),2,',','.');
    $utilidad = number_format(($profitability['utilidad']),2,',','.');
    $margen = number_format(($profitability['margen']),4,',','.');
 
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit('A'.$j, $profitability['cod_product'],  PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValue('B'.$j, $profitability['name'])
				->setCellValue('C'.$j, $total_uni)
				->setCellValue('D'.$j, $total) 
				->setCellValue('E'.$j, $utilidad)
				->setCellValue('F'.$j, $margen) ;
}

for ($i='A'; $i <= 'Z'; $i++) {
	$objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
}

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Rentabilidad por Producto');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Informe de Rentabilidad por Producto.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
