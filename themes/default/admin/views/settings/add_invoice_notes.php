<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?></h3>
        </div>
        <?= admin_form_open("system_settings/save_invoice_notes", ["id"=>"add_invoice_notes_form"]); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= form_label(lang("invoice_notes_description") ." *"); ?>
                <?= form_input(["name"=>"description", "id"=>"description", "class"=>"form-control", "max-length"=>500, "required"=>TRUE]); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("invoice_notes_status") ." *"); ?>
                <?php $sopts = [""=>lang('select'), "1"=>"Activo", "0"=>"Inactivo"]; ?>
                <?= form_dropdown(["name"=>"status", "id"=>"status", "class"=>"form-control select2", "required"=>TRUE], $sopts); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("invoice_notes_module") ." *"); ?>
                <?php $mopts = [""=>lang('select'), "2"=>"Ventas", "14"=>"Recibo de caja"]; ?>
                <?= form_dropdown(["name"=>"module", "id"=>"module", "class"=>"form-control select2", "required"=>TRUE], $mopts); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_button("invoice_notes_add", lang("save"), 'class="btn btn-primary" id="invoice_notes_add"'); ?>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script>
    $(document).ready(function() {
        $("#add_invoice_notes_form").validate({ ignore: [] });

        $(document).on('click', '#invoice_notes_add', function() {
            if ($('#add_invoice_notes_form').valid()) {
                $('#add_invoice_notes_form').submit();
            }
        });
    });
</script>
