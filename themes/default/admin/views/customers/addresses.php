<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('customer_branches') . " " . $company->name; ?></h4>
        </div>

        <div class="modal-body">
            <!--<p><?= lang('list_results'); ?></p>-->

            <div class="table-responsive">
                <table id="CSUData" cellpadding="0" cellspacing="0" border="0"
                       class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr class="primary">
                        <th><?= lang("ID"); ?></th>
                        <th><?= lang("customer_branches"); ?></th>
                        <th style="width:85px;"><?= lang("actions"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($addresses)) {
                        foreach ($addresses as $address) {
                            echo '<tr>' .
                                    '<td>'.$address->id.'
                                    </td>
                                    <td>
                                        <b style="font-size:110%;">' . ucwords(mb_strtolower($address->sucursal)) . '</b>
                                        <br>' . ucwords(mb_strtolower($address->direccion)) . '
                                        ' . ($address->subzone_name ? "<br>Barrio : ".ucwords(mb_strtolower($address->subzone_name)) : "") . '
                                        ' . ($address->zone_name ? "<br>Zona : ".ucwords(mb_strtolower($address->zone_name)) : "") . '
                                        <br>' . ucwords(mb_strtolower($address->city.' '.$address->postal_code)) . '
                                        <br>' . ucwords(mb_strtolower($address->state.' '.$address->country)) . '
                                        <br>' . lang('phone').' '. $address->phone . '
                                        <br>' . lang('seller').' : '. ($address->seller_name ? ucwords(mb_strtolower($address->seller_name)) : lang('no_register')) . '
                                    </td>
                                    '.'<td class="text-center">

                                        <div class="text-center">
                                        <div class="btn-group text-left">
                                            <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Acciones
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li>
                                                    <a href="' . admin_url('customers/edit_address/' . $address->id) . '" class="tip" title="" data-toggle="modal" data-target="#myModal2"><i class="fa fa-edit"></i> ' . lang("edit_customer_branch") . '</a>
                                                 </li>
                                                '.
                                                ( $this->Settings->wappsi_invoice_issuer == 1 && $this->Owner ?
                                                "<li>
                                                    <a href='" . admin_url('wappsi_invoicing/download_customer_invoicing/'.$address->company_id.'/'.$address->id) . "'>
                                                        <i class=\"fa fa-sync\"></i>
                                                        ".lang("download_customer_invoicing")."
                                                    </a>
                                                </li>" : "").'
                                            </ul>
                                        </div>
                                    </div>

                                        
                                    </td>' .
                                '</tr>';
                        }
                    } else { ?>
                        <tr>
                            <td colspan="3" class="dataTables_empty"><?= lang('sEmptyTable') ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>


        </div>
        <div class="modal-footer">
            <a href="<?= admin_url('customers/add_address/'.$company->id); ?>" class="btn btn-primary pull-left" data-toggle='modal' data-target='#myModal2'><?= lang('add_customer_branch'); ?></a>
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
        </div>
    </div>
    <?= $modal_js ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tip').tooltip();
        });
    </script>

