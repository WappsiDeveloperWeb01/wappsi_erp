<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= sprintf(lang('add_gift_card'), lang('gift_card')); ?></h4>
        </div>
        <?php $attrib = array('id'=>'add_gc');
        echo admin_form_open("sales/add_gift_card", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group div_creation" style="display:none;">
                <?= lang("biller", "gcbiller"); ?>
                <?php
                $bl[""] = "";
                foreach ($billers as $biller) {
                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                }
                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="gcbiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" class="form_creation form-control input-tip select" style="width:100%;"');
                ?>
            </div>
            <div class="form-group div_creation" style="display:none;">
                <?= lang("document_type", "gc_document_type_id"); ?>
                <select name="document_type_id" class="form-control form_creation" id="gc_document_type_id">

                </select>
            </div>
            <div class="form-group">
                <label><?= lang("gift_card")." No"; ?> </label>
                <div class="input-group">
                    <?php echo form_input('card_no', '', 'class="form-control" id="card_no" required="required"'); ?>
                    <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                        <a href="#" id="genNo">
                            <i class="fa fa-cogs"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="customer-con">
                <div class="form-group">
                    <?= lang("customer", "customer"); ?>
                    <?php echo form_input('customer', '', 'class="form-control" id="customer" required'); ?>
                    <br>
                    <label>
                        <input type="checkbox" name="use_points" id="use_points" checked>
                        <?= lang('use_points') ?>
                    </label>
                </div>
                <div class="well well-sm" id="award-points-con" style="display:none;">
                    <div class="form-group" id="ca-points-con">
                        <?= lang("ca_points", "ca_points"); ?>
                        <?php echo form_input('ca_points', '', 'class="form-control only_number" id="ca_points"'); ?>
                        Valor punto <?= $this->sma->formatMoney($this->Settings->ca_point_value) ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= lang("value", "value"); ?>
                <?php echo form_input('value', '', 'class="form-control" id="value" readonly required min="1"'); ?>
            </div>
            <div class="form-group paid_by" style="display:none;">
                <?= lang("paid_by", "paid_by"); ?>
                <select name="paid_by" id="paid_by" class="form-control">
                    <?= $this->sma->paid_opts(NULL, false, true, true, true, false, true); ?>
                </select>
            </div>
            <div class="form-group ">
                <?= lang("expiry_date", "expiry"); ?>
                <input type="date" name="expiry" id="expiry" value="<?= date("Y-m-d", strtotime("+1 year")) ?>" class="form-control">
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" id="add_gift_card" class="btn btn-primary"><?= sprintf(lang('add_gift_card'), lang('gift_card')) ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#add_gc").validate({
              ignore: []
          });
        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $('#customer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        var customer_points = 0;
        $('#customer').on('select2-close', function () {
            var selected_customer = $(this).val();
            $.ajax({
                type: "get", async: false,
                url: site.base_url + "customers/get_award_points/" + selected_customer,
                dataType: 'json',
                success: function (data) {
                    if ($('#use_points').is(':checked')) {
                        $('#award-points-con').slideDown();
                    }
                    if (data != null) {
                        $('#award_points').html(data.ca_points);
                        customer_points = parseInt(data.ca_points);
                        $('#ca_points').val(data.ca_points).trigger('change');
                    }
                }
            });
        });
        $(document).on('change', '#ca_points', function () {
            var points_redeem = parseInt($(this).val());
            if (points_redeem <= customer_points) {
                $("[name='add_gift_card']").attr('disabled', false);
                if (formatDecimal(site.settings.ca_point_value) > 0) {
                    $('#value').val(formatDecimal(site.settings.ca_point_value) * points_redeem);
                } else {
                    $("[name='add_gift_card']").attr('disabled', true);
                    $('#value').val(0);
                }
            } else {
                $("[name='add_gift_card']").attr('disabled', true);
                $('#value').val(0);
            }
        });
        $(document).on('ifChecked', '#use_points', function (event) {
            $('#ca-points-con').slideDown();
            $('#award-points-con').slideDown();
            $('.paid_by').fadeOut().prop('required', false);
            $('#value').prop('readonly', true).val(0);
            $('#ca_points').val(0);
            $('.div_creation').fadeOut();
            $('.form_creation').prop('required', false);
        });
        $(document).on('ifUnchecked', '#use_points', function (event) {
            $('#ca-points-con').slideUp();
            $('#award-points-con').slideUp();
            $('.paid_by').fadeIn().prop('required', true);
            $('#value').prop('readonly', false).val(0);
            $('#ca_points').val(0);
            $('.div_creation').fadeIn();
            $('.form_creation').prop('required', true);
        });
        $('#staff_points').on('ifChecked', function (event) {
            $('#customer-con').slideUp('fast');
            $('#staff-con').slideDown();
        });
        $('#staff_points').on('ifUnchecked', function (event) {
            $('#staff-con').slideUp('fast');
            $('#customer-con').slideDown();
        });
        $('#user').change(function () {
            var selected_user = $(this).val();
            $.ajax({
                type: "get", async: false,
                url: site.base_url + "sales/get_award_points/" + selected_user,
                dataType: 'json',
                success: function (data) {
                    if (data != null) {
                        $('#staff_award_points').html(data.sa_points);
                        $('#sa_points').val(data.sa_points);
                        if (data.sa_points > 0) {
                            $('#sa-points-con').slideDown();
                        } else {
                            $('#sa-points-con').slideUp();
                        }
                    } else {
                        $('#sa-points-con').slideUp();
                    }
                }
            });
        });
        
        $('#add_gift_card').on('click', function(){
            if ($('#add_gc').valid()) {
                $('#add_gc').submit();
            }
        });
        $(document).on('change', '#gcbiller', function(){
                $.ajax({
                    url: site.base_url+'billers/getBillersDocumentTypes/61/'+$('#gcbiller').val(),
                    type:'get',
                    dataType:'JSON'
                }).done(function(data){
                    response = data;
                    $('#gc_document_type_id').html(response.options).select2();
                    if (response.not_parametrized != "") {
                        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                                    "showDuration": "500",
                                    "hideDuration": "1000",
                                    "timeOut": "6000",
                                    "extendedTimeOut": "1000",
                                });
                    }
                    if (response.status == 0) {
                      $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                    }
                    $('#gc_document_type_id').trigger('change');
                });
            });
        $('#gcbiller').trigger('change');
    });

</script>    