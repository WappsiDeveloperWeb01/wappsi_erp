<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";

if ($this->input->post('category')) {
    $v .= "&category=" . $this->input->post('category');
}
if ($this->input->post('biller')) {
    $v .= "&biller=" . $this->input->post('biller');
}
if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
if ($this->input->post('date_records_filter')) {
    $v .= "&date_records_filter=" . $this->input->post('date_records_filter');
}
if ($this->input->post('visualization_day')) {
    $v .= "&visualization_day=" . $this->input->post('visualization_day');
}
if ($this->input->post('visualization_month')) {
    $v .= "&visualization_month=" . $this->input->post('visualization_month');
}
if ($this->input->post('visualization_month_year')) {
    $v .= "&visualization_month_year=" . $this->input->post('visualization_month_year');
}
if ($this->input->post('exclude_returns')) {
    $v .= "&exclude_returns=" . $this->input->post('exclude_returns');
}


?>
<script>
    $(document).ready(function () {
        $.ajax({
            url : '<?= admin_url('reports/getCategoriesReport/?v=1'.$v) ?>'
        }).done(function(data){
            $('#PrRData tbody').html(data);
            oTable = $('#PrRData').dataTable({
                "aaSorting": [[0, "asc"]],
                "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                // 'bProcessing': true, 'bServerSide': true,
                // 'sAjaxSource': '<?= admin_url('reports/getCategoriesReport/?v=1'.$v) ?>',
                // 'fnServerData': function (sSource, aoData, fnCallback) {
                //     aoData.push({
                //         "name": "<?= $this->security->get_csrf_token_name() ?>",
                //         "value": "<?= $this->security->get_csrf_hash() ?>"
                //     });
                //     $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                // },
                "aoColumns": [
                                null,
                                null,
                                {"mRender": currencyFormat},
                                {"mRender": currencyFormat},
                                {"mRender": decimalFormat},
                                {"mRender": decimalFormat},
                                {"mRender": decimalFormat},
                                {"mRender": currencyFormat}
                            ],
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    var pQty = 0, sQty = 0, pAmt = 0, sAmt = 0, pl = 0, pl2 = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        // console.log(aaData);
                        pQty += aaData[aiDisplay[i]] ? parseFloat(aaData[aiDisplay[i]][2]) : 0;
                        sQty += aaData[aiDisplay[i]] ? parseFloat(aaData[aiDisplay[i]][3]) : 0;
                        pAmt += aaData[aiDisplay[i]] ? parseFloat(aaData[aiDisplay[i]][4]) : 0;
                        sAmt += aaData[aiDisplay[i]] ? parseFloat(aaData[aiDisplay[i]][5]) : 0;
                        pl += aaData[aiDisplay[i]] ? parseFloat(aaData[aiDisplay[i]][6]) : 0;
                        pl2 += aaData[aiDisplay[i]] ? parseFloat(aaData[aiDisplay[i]][7]) : 0;
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[2].innerHTML = decimalFormat(parseFloat(pQty));
                    nCells[3].innerHTML = decimalFormat(parseFloat(sQty));
                    nCells[4].innerHTML = currencyFormat(parseFloat(pAmt));
                    nCells[5].innerHTML = currencyFormat(parseFloat(sAmt));
                    nCells[6].innerHTML = decimalFormat(parseFloat(((sQty - pQty)/sQty)*100));
                    nCells[7].innerHTML = currencyFormat(parseFloat(pl2));
                }
            }).fnSetFilteringDelay().dtFilter([
                {column_number: 0, filter_default_label: "[<?=sprintf(lang('category_code'), lang('category'));?>]", filter_type: "text", data: []},
                {column_number: 1, filter_default_label: "[<?=lang('category_name');?>]", filter_type: "text", data: []},
            ], "footer");
            });
        
        
        <?php if ($this->session->userdata('biller_id')): ?>
            setTimeout(function() {
                $('#slbiller').select2('val', '<?= $this->session->userdata('biller_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
        <?php if ($this->session->userdata('warehouse_id')): ?>
            setTimeout(function() {
                $('#warehouse').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
    });

    $(document).ready(function () {
        $('#form').hide();
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });

    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo admin_form_open("reports/categories", 'id="form_filter"'); ?>
                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="exclude_returns" value="" class="form-control">
                                                <?= lang('exclude_returns') ?>
                                            </label>
                                        </div>
                                    </div>

                                    <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                    // exit(var_dump($this->session->userdata('biller_id')));
                                    ?>

                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "slbiller"); ?>
                                                <select name="biller" class="form-control" id="slbiller" required="required">
                                                    <option value=""><?= lang('select') ?></option>
                                                    <?php foreach ($billers as $biller) : ?>
                                                        <option value="<?= $biller->id ?>" <?= (isset($_POST['biller']) && $_POST['biller'] == $biller->id ? "selected" : "") ?>><?= $biller->company ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "slbiller"); ?>
                                                <select name="biller" class="form-control" id="slbiller">
                                                    <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                                        $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                        <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                            <?php
                                            $wh[""] = lang('select').' '.lang('warehouse');
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("category", "category") ?>
                                            <?php
                                            $cat[''] = lang('select');
                                            foreach ($categories as $category) {
                                                $cat[$category->id] = $category->name;
                                            }
                                            echo form_dropdown('category', $cat, (isset($_POST['category']) ? $_POST['category'] : ''), 'class="form-control select" id="category" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
                                            ?>
                                        </div>
                                    </div>


                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('date_records_filter', 'date_records_filter') ?>
                                        <select name="date_records_filter" id="date_records_filter_h" class="form-control">
                                            <option value="daily">Ver diario</option>
                                            <option value="monthly">Ver Mensual</option>
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>

                                    <div class="date_controls_h">
                                        <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                            <div class="col-sm-4 form-group">
                                                <?= lang('filter_year', 'filter_year_h') ?>
                                                <select name="filter_year" id="filter_year_h" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <?= lang('start_date', 'start_date') ?>
                                            <input type="date" name="start_date" id="start_date_h" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                        </div>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <?= lang('end_date', 'end_date') ?>
                                            <input type="date" name="end_date" id="end_date_h" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="div_visualization_day" style="display:none;">
                                        <div class="col-sm-4">
                                            <?= lang('visualization_day', 'visualization_day') ?>
                                            <input type="date" name="visualization_day" id="visualization_day" class="form-control" value="<?= isset($_POST['visualization_day']) ? $_POST['visualization_day'] : date('Y-m-d')  ?>" />
                                        </div>
                                    </div>
                                    <div class="div_visualization_month" style="display:none;">
                                        <div class="col-sm-2 form-group">
                                            <?= lang('visualization_month_year', 'visualization_month_year_h') ?>
                                            <select name="visualization_month_year" id="visualization_month_year_h" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= lang('visualization_month', 'visualization_month') ?>
                                            <select name="visualization_month" id="visualization_month" class="form-control"/>
                                                <option value="1"><?= lang('cal_january') ?></option>
                                                <option value="2"><?= lang('cal_february') ?></option>
                                                <option value="3"><?= lang('cal_march') ?></option>
                                                <option value="4"><?= lang('cal_april') ?></option>
                                                <option value="5"><?= lang('cal_may') ?></option>
                                                <option value="6"><?= lang('cal_june') ?></option>
                                                <option value="7"><?= lang('cal_july') ?></option>
                                                <option value="8"><?= lang('cal_august') ?></option>
                                                <option value="9"><?= lang('cal_october') ?></option>
                                                <option value="10"><?= lang('cal_september') ?></option>
                                                <option value="11"><?= lang('cal_november') ?></option>
                                                <option value="12"><?= lang('cal_december') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div
                                        class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <div class="row">

                        <?php if (isset($_POST['date_records_filter']) && $_POST['date_records_filter'] == 'daily'): ?>
                            <div class="col-sm-4">
                                <button class="btn btn-primary pull-left previous-day"> << Anterior</button>
                            </div>
                            <div class="col-sm-4 text-center">
                                <h3><?= $_POST['visualization_day'] ?></h3>
                            </div>
                            <div class="col-sm-4">
                                <button class="btn btn-primary pull-right next-day">Siguiente >> </button>
                            </div>
                            <hr class="col-sm-11">
                        <?php endif ?>
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <li class="dropdown">
                                        <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                            <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?>
                                        </a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                            <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="PrRData"
                                    class="table  table-bordered table-condensed table-hover dfTable reports-table"
                                    style="margin-bottom:5px;">
                                    <thead>
                                    <tr class="active">
                                        <th><?= lang("category"); ?></th>
                                        <th><?= lang("subcategory"); ?></th>
                                        <th><?= lang("purchased"); ?></th>
                                        <th><?= lang("sold"); ?></th>
                                        <th><?= lang("purchased_amount"); ?></th>
                                        <th><?= lang("sold_amount"); ?></th>
                                        <th><?= lang("profitability_margin"); ?></th>
                                        <th><?= lang("diff"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr class="active">
                                        <th></th>
                                        <th></th>
                                        <th><?= lang("purchased"); ?></th>
                                        <th><?= lang("sold"); ?></th>
                                        <th><?= lang("purchased_amount"); ?></th>
                                        <th><?= lang("sold_amount"); ?></th>
                                        <th><?= lang("profitability_margin"); ?></th>
                                        <th><?= lang("diff"); ?></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getCategoriesReport/pdf/?v=1'.$v)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getCategoriesReport/0/xls/?v=1'.$v)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
    });


    $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_h').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_h').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });

    $(document).on('change', '#date_records_filter_h', function(){
        $('.div_visualization_day').fadeOut();
        $('.div_visualization_month').fadeOut();
        if ($(this).val() == 'daily') {
            $('.div_visualization_day').fadeIn();
        } else if ($(this).val() == 'monthly') {
            $('.div_visualization_month').fadeIn();
        }
    });

    $(document).on('click', '.previous-day', function(){
        var myDate=new Date($('#visualization_day').val());
        // var dt = myDate.getDate() + '/' + ("0" + (myDate.getMonth() + 1)).slice(-2) + '/' + myDate.getFullYear();
        var dt = myDate.getFullYear()+"-"+("0" + (myDate.getMonth() + 1)).slice(-2)+"-"+(parseInt(myDate.getDate()) < 10 ? "0"+myDate.getDate() : myDate.getDate());
        console.log(dt);
        $('#visualization_day').val(dt);
        $('#loading').fadeIn();
        setTimeout(function() {
            $('#form_filter').submit();
        }, 850);
    });

    $(document).on('click', '.next-day', function(){
        var myDate=new Date($('#visualization_day').val());
        myDate.setDate(myDate.getDate()+2);
        var dt = myDate.getFullYear()+"-"+("0" + (myDate.getMonth() + 1)).slice(-2)+"-"+(parseInt(myDate.getDate()) < 10 ? "0"+myDate.getDate() : myDate.getDate());
        console.log(dt);
        $('#visualization_day').val(dt);
        $('#loading').fadeIn();
        setTimeout(function() {
            $('#form_filter').submit();
        }, 850);
    });


</script>