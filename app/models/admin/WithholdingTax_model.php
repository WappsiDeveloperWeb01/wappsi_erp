<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WithholdingTax_model extends CI_Model {

	public function get()
	{
		$this->db->select("*");
		$this->db->from("withholdings");

		$response = $this->db->get();
		return $response->result();
	}

	public function get_by_id($id)
	{
		$this->db->select("*");
		$this->db->from("withholdings");
		$this->db->where("id", $id);

		$response = $this->db->get();
		return $response->row();
	}

    public function get_by_data($data, $id)
    {
        $this->db->select("*");
		$this->db->from("withholdings");
		$this->db->where($data);
        if (!empty($id)) {
            $this->db->where("id != {$id}");
        }

		$response = $this->db->get();
		return $response->row();
    }

	public function movement_existing_tax_withholding($withholding_id, $withholding_type)
	{
		$this->db->distinct();

		if ($withholding_type == "FUENTE") {
			$this->db->select("rete_fuente_id");
			$this->db->where("rete_fuente_id", $withholding_id);
		} else if ($withholding_type == "IVA") {
			$this->db->select("rete_iva_id");
			$this->db->where("rete_iva_id", $withholding_id);
		} else {
			$this->db->select("rete_ica_id");
			$this->db->where("rete_ica_id", $withholding_id);
		}

		$this->db->from("sales");
		$response = $this->db->get();
		return $response->row();
	}

	public function insert($data)
	{
		if ($this->db->insert("withholdings", $data)) {
			return TRUE;
		}

		return FALSE;
	}

	public function update($data, $id)
	{
		$this->db->where("id", $id);
		if ($this->db->update("withholdings", $data)) {
			return TRUE;
		}

		return FALSE;
	}

	public function delete($id)
	{
		$this->db->where("id", $id);
		if ($this->db->delete("withholdings")) {
			return TRUE;
		}

		return FALSE;
	}
}

/* End of file WithholdingTax_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/WithholdingTax_model.php */