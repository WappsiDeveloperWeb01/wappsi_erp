<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Billers extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        $this->lang->admin_load('billers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('DocumentsTypes_model');
    }

    public function index($action = NULL)
    {

        if (!$this->Owner && !$this->Admin) {
            $this->sma->checkPermissions();
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('billers')));
        $meta = array('page_title' => lang('billers'), 'bc' => $bc);
        $this->page_construct('billers/index', $meta, $this->data);
    }

    public function getBillers()
    {
        if (!$this->Owner && !$this->Admin) {
            $this->sma->checkPermissions('index');
        }

        $delete = $updateCosting = "";

        $this->load->library('datatables');
        $this->datatables
            ->select("companies.id, name,
            IF({$this->db->dbprefix('biller_data')}.branch_type = 1, 'Fisica', 'Virtual') as branch_type,
            phone, email, city, country, status,
                if(status = 1, 'fas fa-user-check', 'fas fa-user-slash') as faicon,
                if(status = 1, '".lang('deactivate_biller')."', '".lang('activate_biller')."') as detailaction,
                if(status = 1, '" . admin_url('billers/deactivate/') . "', '" . admin_url('billers/activate/') . "') as linkaction,
                if({$this->db->dbprefix('biller_data')}.charge_shipping_cost = 1, '', 'display:none;') as shipping_cost_action
                ")

            ->from("companies")
            ->join('biller_data', 'biller_data.biller_id = companies.id')
            ->where('group_name', 'biller');

        if ($this->Owner) {
            $delete = "<li>
                <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure_biller') . "</p>
                    <a class='btn btn-danger po-delete' href='$4/$1'>" . lang('i_m_sure') . "</a>
                    <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"$2\"></i>$3
                </a>
            </li>";
            $updateCosting = "<li>
                <a style=\"$5\" class=\"tip\" title='" . $this->lang->line("update_ubication_shipping_cost") . "' href='" . admin_url('system_settings/update_ubication_shipping_cost/0/$1') . "' target=\"_blank\">
                    <i class=\"fa fa-truck\"></i>
                </a>
            </li>";
        }

        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                            <div class=\"btn-group text-left\">
                                                <button type=\"button\" class=\"btn btn-default new-button new-button-sm btn-xs dropdown-toggle\" data-toggle=\"dropdown\" data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                                                    <i class='fas fa-ellipsis-v fa-lg'></i>
                                                </button>
                                                <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                    <li>
                                                        <a class=\"tip\" title='' href='" . admin_url('billers/edit/$1') . "'>
                                                            <i class=\"fa fa-edit\"></i>" . $this->lang->line("edit_biller") . "
                                                        </a>
                                                    </li>
                                                    $delete
                                                    $updateCosting
                                                </ul>
                                            </div>
                                        </div>", "companies.id, faicon, detailaction, linkaction, shipping_cost_action");
        $this->datatables->unset_column('faicon');
        $this->datatables->unset_column('detailaction');
        $this->datatables->unset_column('linkaction');
        $this->datatables->unset_column('shipping_cost_action');
        echo $this->datatables->generate();
    }

    public function add()
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[companies.email]');

        if ($this->Settings->financing_module) {
            $this->form_validation->set_rules('affiliate_consecutive', lang("affiliate_consecutive"), 'trim|required');
            $this->form_validation->set_rules('old_branch_code', lang("old_branch_code"), 'trim|required');
        }

        if ($this->form_validation->run('companies/add') == true) {
            $nombreCompleto = $this->input->post('name');
            $data = array(
                'name' => $this->input->post('business_name'),
                'company' => $this->input->post('trade_name'),
                'email' => $this->input->post('email'),
                'group_id' => NULL,
                'group_name' => 'biller',
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'logo' => $this->input->post('logo'),
                'logo_square' => $this->input->post('square_logo'),
                "location" => $this->input->post("zone"),
                "subzone" => $this->input->post("subzone"),
                "city_code" => $this->input->post("city_code"),
                "type_person" => $this->input->post("type_person"),
                "tipo_regimen" => $this->input->post("type_vat_regime"),
                "tipo_documento" => $this->input->post("document_type"),
                "first_name" => $this->input->post("first_name"),
                "second_name" => $this->input->post("second_name"),
                "first_lastname" => $this->input->post("first_lastname"),
                "second_lastname" => $this->input->post("second_lastname"),
                "commercial_register" => $this->input->post("commercial_register"),
            );

            $data_aditional = array(
                'default_price_group' => $this->input->post('biller_default_price_group'),
                'default_warehouse_id' => $this->input->post('biller_default_warehouse_id'),
                'default_customer_id' => $this->input->post('biller_default_customer_id'),
                'default_seller_id' => $this->input->post('biller_default_seller_id'),
                'default_cost_center_id' => $this->input->post('cost_center'),
                'pin_code' => $this->input->post('pin_code'),
                'product_order' => $this->input->post('product_order'),
                'branch_type' => $this->input->post('branch_type'),
                'coverage_country' => $this->input->post('coverage_country'),
                'coverage_state' => $this->input->post('coverage_state'),
                'coverage_city' => $this->input->post('coverage_city'),
                'coverage_zone' => $this->input->post('coverage_zone'),
                'charge_shipping_cost' => $this->input->post('charge_shipping_cost'),
                'shipping_cost' => $this->input->post('shipping_cost'),
                'max_total_shipping_cost' => $this->input->post('max_total_shipping_cost'),
                'language' => $this->input->post('language'),
                'currency' => $this->input->post('currency'),
                'prioridad_precios_producto' => $this->input->post('prioridad_precios_producto'),
                'concession_status'   => $this->input->post('concession_status'),
                'default_pos_section'   => $this->input->post('default_pos_section'),
                "great_contributor" => $this->input->post("great_contributor"),
            );

            if ($this->Settings->financing_module) {
                $data_aditional["affiliate_consecutive"] = $this->input->post("affiliate_consecutive");
                $data_aditional["old_branch_code"] = $this->input->post("old_branch_code");
            }

            $biller_default_price_group = $this->input->post('biller_default_price_group') != '' ? $this->input->post('biller_default_price_group') : NULL ;
            $biller_default_warehouse_id = $this->input->post('biller_default_warehouse_id') != '' ? $this->input->post('biller_default_warehouse_id') : NULL ;
            $biller_default_customer_id = $this->input->post('biller_default_customer_id') != '' ? $this->input->post('biller_default_customer_id') : NULL ;
            $biller_default_seller_id = $this->input->post('biller_default_seller_id') != '' ? $this->input->post('biller_default_seller_id') : NULL ;

            $resoluciones = $this->input->post('document_types');

            $default_dts = [
                '1' => $this->input->post('document_type_pos_default'),
                '2' => $this->input->post('document_type_detal_default'),
                '5' => $this->input->post('document_type_purchases_default'),
                "default_electronic_document_for_financing" => $this->input->post("default_electronic_document_for_financing"),
            ];

            $data_categories_concession = [];
            if ($this->input->post('category_concession_code')) {
                $category_concession_code = $this->input->post('category_concession_code');
                $category_concession_name = $this->input->post('category_concession_name');
                foreach ($category_concession_code as $concession_category_id => $cc_code) {
                    $data_categories_concession[] = [
                                                        'category_id' => $concession_category_id,
                                                        'concession_code' => $cc_code,
                                                        'concession_name' => isset($category_concession_name[$concession_category_id]) ? $category_concession_name[$concession_category_id] : NULL,
                                                    ];
                }
            }

            // $this->sma->print_arrays($data);

        } elseif ($this->input->post('add_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('billers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->addCompany($data, $data_aditional, $resoluciones, $default_dts, $data_categories_concession)) {
            $this->session->set_flashdata('message', $this->lang->line("biller_added"));
            admin_redirect("billers");
        } else {
            $logos = $this->getLogoList();
            $this->data['logo_files'] = $logos['logo_files'];
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['customers'] = $this->companies_model->getAllCustomerCompanies();
            $this->data['sellers'] = $this->companies_model->getAllSellerCompanies();
            $this->data['settings'] = $this->site->get_setting();
            $this->data['cost_centers'] = $this->site->getCostCenters();
            $this->data['document_types'] = $this->companies_model->getAllDocumentTypes();
            $this->data['currencies'] = $this->settings_model->getAllCurrencies();
            $this->data['categories'] = $this->site->getAllCategories();

            $this->data["types_person"] = $this->site->get_types_person();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $this->data["types_obligations"] = $this->site->get_types_obligations();


            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('billers'), 'page' => lang('billers')), array('link' => '#', 'page' => lang('add_biller')));
            $meta = array('page_title' => lang('add_biller'), 'bc' => $bc);
            $this->page_construct('billers/add', $meta, $this->data);
        }
    }

    public function edit($id = NULL)
    {
        if (!$this->Owner && !$this->Admin) { $this->sma->checkPermissions(false, true); }

        if ($this->input->get('id')) { $id = $this->input->get('id'); }

        $company_details = $this->companies_model->getCompanyByID($id);
        $company_details_aditional = $this->companies_model->getBillerDataAditional($id);
        if ($this->input->post('email') != $company_details->email) {
            $this->form_validation->set_rules('code', lang("email_address"), 'is_unique[companies.email]');
        }
        if ($this->Settings->financing_module) {
            $this->form_validation->set_rules('affiliate_consecutive', lang("affiliate_consecutive"), 'trim|required');
            $this->form_validation->set_rules('old_branch_code', lang("old_branch_code"), 'trim|required');
        }

        if ($this->form_validation->run('companies/add') == true) {
            $biller_warehouses_related = $this->input->post('biller_warehouses_related');
            if ($biller_warehouses_related && is_array($biller_warehouses_related) && count($biller_warehouses_related) > 0) {
                $biller_warehouses_related = json_encode($biller_warehouses_related);
            }

            $nombreCompleto = $this->input->post('name');
            $data = array(
                'vat_no' => $this->input->post('vat_no'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'name' => $this->input->post('business_name'),
                'company' => $this->input->post('trade_name'),
                'email' => $this->input->post('email'),
                'group_id' => NULL,
                'group_name' => 'biller',
                'address' => $this->input->post('address'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'logo' => $this->input->post('logo'),
                'logo_square' => $this->input->post('square_logo'),
                "location" => $this->input->post("zone"),
                "subzone" => $this->input->post("subzone"),
                "city_code" => $this->input->post("city_code"),
                "type_person" => $this->input->post("type_person"),
                "tipo_regimen" => $this->input->post("type_vat_regime"),
                "tipo_documento" => $this->input->post("document_type"),
                "first_name" => $this->input->post("first_name"),
                "second_name" => $this->input->post("second_name"),
                "first_lastname" => $this->input->post("first_lastname"),
                "second_lastname" => $this->input->post("second_lastname"),
                "commercial_register" => $this->input->post("commercial_register"),
            );

            $data_aditional = array(
                'default_price_group' => $this->input->post('biller_default_price_group'),
                'default_warehouse_id' => $this->input->post('biller_default_warehouse_id'),
                'default_customer_id' => $this->input->post('biller_default_customer_id'),
                'default_seller_id' => $this->input->post('biller_default_seller_id'),
                'default_affiliate_id' => $this->input->post('biller_default_affiliate_id'),
                'default_cost_center_id' => $this->input->post('cost_center'),
                'pin_code' => $this->input->post('pin_code'),
                'product_order' => $this->input->post('product_order'),
                'branch_type' => $this->input->post('branch_type'),
                'coverage_country' => $this->input->post('coverage_country'),
                'coverage_state' => $this->input->post('coverage_state'),
                'coverage_city' => $this->input->post('coverage_city'),
                'coverage_zone' => $this->input->post('coverage_zone'),
                'charge_shipping_cost' => $this->input->post('charge_shipping_cost'),
                'shipping_cost' => $this->input->post('shipping_cost'),
                'max_total_shipping_cost' => $this->input->post('max_total_shipping_cost'),
                'language' => $this->input->post('language'),
                'currency' => $this->input->post('currency'),
                'prioridad_precios_producto' => $this->input->post('prioridad_precios_producto'),
                'concession_status'   => $this->input->post('concession_status'),
                'default_pos_section'   => $this->input->post('default_pos_section'),
                'preparation_adjustment_document_type_id'   => $this->input->post('document_type_preparation_adjustment'),
                'rete_autoica_percentage'   => $this->input->post('rete_autoica_percentage'),
                'rete_autoica_account'   => $this->input->post('rete_autoica_account'),
                'rete_autoica_account_counterpart'   => $this->input->post('rete_autoica_account_counterpart'),
                'rete_bomberil_percentage'   => $this->input->post('rete_bomberil_percentage'),
                'rete_bomberil_account'   => $this->input->post('rete_bomberil_account'),
                'rete_bomberil_account_counterpart'   => $this->input->post('rete_bomberil_account_counterpart'),
                'rete_autoaviso_percentage'   => $this->input->post('rete_autoaviso_percentage'),
                'rete_autoaviso_account'   => $this->input->post('rete_autoaviso_account'),
                'cash_payment_method_account'   => $this->input->post('cash_payment_method_account'),
                'pin_code_request'   => $this->input->post('pin_code_request'),
                'pin_code_method'   => $this->input->post('pin_code_method'),
                'min_sale_amount'   => $this->input->post('min_sale_amount'),
                'warehouses_related'   => $biller_warehouses_related,
                "great_contributor" => $this->input->post("great_contributor"),
                "type_concession" => $this->input->post("type_concession"),
                "rete_autoaviso_account_counterpart" => $this->input->post("rete_autoaviso_account_counterpart"),
                "default_credit_payment_method" => $this->input->post("default_credit_payment_method"),
            );

            if ($this->Settings->financing_module) {
                $data_aditional["affiliate_consecutive"] = $this->input->post("affiliate_consecutive");
                $data_aditional["old_branch_code"] = $this->input->post("old_branch_code");
            }

            $biller_default_price_group = $this->input->post('biller_default_price_group') != '' ? $this->input->post('biller_default_price_group') : NULL ;
            $biller_default_warehouse_id = $this->input->post('biller_default_warehouse_id') != '' ? $this->input->post('biller_default_warehouse_id') : NULL ;
            $biller_default_customer_id = $this->input->post('biller_default_customer_id') != '' ? $this->input->post('biller_default_customer_id') : NULL ;
            $biller_default_seller_id = $this->input->post('biller_default_seller_id') != '' ? $this->input->post('biller_default_seller_id') : NULL ;
            $resoluciones = $this->input->post('document_types');
            $default_dts = [
                            '1' => $this->input->post('document_type_pos_default'),
                            '2' => $this->input->post('document_type_detal_default'),
                            '5' => $this->input->post('document_type_purchases_default'),
                            '8' => $this->input->post('document_type_order_sales_default'),
                            '3' => $this->input->post('pos_document_type_default_returns'),
                            "default_electronic_document_for_financing" => $this->input->post("default_electronic_document_for_financing")
                            ];

            $data_categories_concession = [];
            if ($this->input->post('category_concession_code')) {
                $category_concession_code = $this->input->post('category_concession_code');
                $category_concession_name = $this->input->post('category_concession_name');
                foreach ($category_concession_code as $concession_category_id => $cc_code) {
                    $data_categories_concession[] = [
                                                        'category_id' => $concession_category_id,
                                                        'biller_id' => $id,
                                                        'concession_code' => $cc_code,
                                                        'concession_name' => isset($category_concession_name[$concession_category_id]) ? $category_concession_name[$concession_category_id] : NULL,
                                                    ];
                }
            }
            $schedule = NULL;
            if ($data_aditional['branch_type'] == 2) {
                $j = count($_POST['week_day']);
                for ($i=0; $i < $j ; $i++) {
                    $schedule[] = [
                        'week_day' => $_POST['week_day'][$i],
                        'start' => $_POST['start'][$i],
                        'end' => $_POST['end'][$i],
                    ];
                }
            }

            $this->site->delete_types_customer_obligations($id, TRANSMITTER);
            foreach ($this->input->post("types_obligations") as $type_obligation) {
                $data_types_customer_obligations[] = [
                    "customer_id" => $id,
                    "types_obligations_id" =>$type_obligation,
                    "relation"=>TRANSMITTER
                ];
            }
            $this->settings_model->insert_type_customer_obligations($data_types_customer_obligations);

        } elseif ($this->input->post('edit_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('billers');
        }

        if ($this->form_validation->run() == true) {
            $datosIniciales = $this->companies_model->user_activities_log($id, $data, $data_aditional, $resoluciones, $default_dts, $data_categories_concession);
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data, $data_aditional, $resoluciones, $default_dts, $data_categories_concession, $schedule)) {
            $this->companies_model->user_activities_log_register($id, $data, $data_aditional, $resoluciones, $default_dts, $data_categories_concession, $datosIniciales);
            $this->session->set_flashdata('message', $this->lang->line("biller_updated"));
            admin_redirect("billers");
        } else {
            $this->data['biller'] = $company_details;
            $this->data['biller_data'] = $company_details_aditional;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $logos = $this->getLogoList();
            $this->data['logo_files'] = $logos['logo_files'];
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            // $this->data['customers'] = $this->companies_model->getAllCustomerCompanies();
            $this->data['sellers'] = $this->site->getSellerByBiller($id);
            $this->data['affiliates'] = $this->site->getAffiliateByBiller($id);
            $this->data['document_types_selected'] = $this->companies_model->getBillerDocumentTypes($id);
            $this->data['document_types'] = $this->companies_model->getAllDocumentTypes();
            $this->data['cost_centers'] = $this->site->getAllCostCenters();

            $this->data["types_person"] = $this->site->get_types_person();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $this->data["types_obligations"] = $this->site->get_types_obligations();
            $this->data["types_customer_obligations"] = $this->site->getTypesCustomerObligations($id, TRANSMITTER);

            if ($schedule = $this->companies_model->get_biller_schedule($id)) {
                foreach ($schedule as $sh) {
                    $sh_arr[$sh->week_day]['start'] = $sh->start;
                    $sh_arr[$sh->week_day]['end'] = $sh->end;
                }
                $this->data['schedule'] = $sh_arr;
            }

            $this->data['biller_categories_concession'] = $this->companies_model->get_all_biller_categories_concessions($id);
            if ($this->Settings->modulary) {
                $this->data['ledgers'] = $this->site->getAllLedgers();
            }
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['currencies'] = $this->settings_model->getAllCurrencies();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('billers'), 'page' => lang('billers')), array('link' => '#', 'page' => lang('edit_biller')));
            $meta = array('page_title' => lang('edit_biller'), 'bc' => $bc);
            $this->page_construct('billers/edit', $meta, $this->data);
        }
    }

    public function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->deleteBiller($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("biller_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("biller_x_deleted_have_sales")));
        }
    }

    public function deactivate($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->deactivateBiller($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("biller_deactivated")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("biller_x_deactivate_is_biller_default")));
        }
    }

    public function activate($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->activateBiller($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("biller_activated")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => "Error"));
        }
    }

    public function suggestions($term = NULL, $limit = NULL)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getBillerSuggestions($term, $limit);
        $this->sma->send_json($rows);
    }

    public function getBiller($id = NULL)
    {
        $this->sma->checkPermissions('index');

        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->company)));
    }

    public function getLogoList()
    {
        $this->load->helper('directory');
        $dirname = "assets/uploads/logos";
        $ext = array("jpg", "png", "jpeg", "gif");
        $logo_files = array();
        // exit(var_dump($dirname));
        if ($handle = opendir($dirname)) {
            while (false !== ($file = readdir($handle)))
                for ($i = 0; $i < sizeof($ext); $i++)
                    if (stristr($file, "." . $ext[$i])) {
                        // $img_size = getimagesize(base_url().$dirname."/".$file);
                        // $img_width = $img_size[0];
                        // $img_height = $img_size[1];
                        // if ($img_width > $img_height) {
                        //     $rectangular_files[] = $file;
                        // } else if ($img_width == $img_height) {
                            $logo_files[] = $file;
                        // }
                    }
            closedir($handle);
        }
        sort($logo_files);
        return ['logo_files' => $logo_files];
    }

    public function biller_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteBiller($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('billers_x_deleted_have_sales'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("billers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('billers'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('city'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->site->getCompanyByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->company);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->city);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'billers_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_biller_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function getBillersDocumentTypes($module = NULL, $biller_id = NULL, $doc_type_id = NULL, $is_document_electronic = NULL)
    {
        $status = "0";
        $options = "";
        $not_parametrized = "";
        $module = !empty($module) ? $module : $this->input->post("module");
        $biller_id = !empty($biller_id) ? $biller_id : $this->input->post("biller_id");
        $doc_type_id = !empty($doc_type_id) ? $this->sma->get_var_null($doc_type_id) : $this->input->post("doc_type_id");
        $empty_word = $this->input->post("empty_word") ? $this->input->post("empty_word") : NULL;
        $arr_docs = $this->input->post("arr_docs") ? $this->input->post("arr_docs") : NULL;
        $is_document_electronic = (!is_null($is_document_electronic)) ? $is_document_electronic : (($this->input->post("is_document_electronic")) ? $this->input->post("is_document_electronic") : NULL);
        $campo_default = '';
        if ($module == 1) {
            if ($is_document_electronic == 1) {
                $campo_default = "pos_document_type_default_fe";
            }else{
                $campo_default = "pos_document_type_default";
            }
        }else{
            $campo_default = "detal_document_type_default";
        }
        $bdt_aditional = $this->companies_model->getBillerDataAditional($biller_id);
        $bdt = $this->companies_model->getBillerDocumentTypes($biller_id, $module, $is_document_electronic, $arr_docs);

        if ($bdt) {
            $status = '1';
            if (count($bdt) > 1) {
                $options.="<option value=''>".($empty_word ? lang('all_document_types') : lang('select'))."</option>";
            }
            $already_selected = false;
            foreach ($bdt as $document_type) {
                if (!$this->site->validate_doc_type_accounting_parametrization($document_type->id)) {
                    $not_parametrized .= $document_type->sales_prefix.", ";
                }
                $selected = "";
                if ($already_selected == false) {
                    if ($bdt_aditional != false && $bdt_aditional->{$campo_default} == $document_type->id) {
                        $selected = "selected='selected'";
                        $already_selected = true;
                    } else if (!is_null($doc_type_id) && $document_type->id == $doc_type_id) {
                        $selected = "selected='selected'";
                        $already_selected = true;
                    } else {
                        if ($this->Settings->electronic_billing == YES) {
                            if ($document_type->factura_electronica == YES) {
                                if ($document_type->module != 35) {
                                    $selected = "selected='selected'";
                                    $already_selected = true;
                                }
                            }
                        }
                    }
                }

                if ($module == POS) {
                    if ($this->pos_settings->activate_electronic_pos == YES /* || $this->pos_settings->mode_electronic_pos == 1 */) {
                        if ($this->pos_settings->mode_electronic_pos == 1) {
                            if ($document_type->factura_electronica == YES) {
                                if ($this->pos_settings->work_environment == TEST) {
                                    if ($document_type->fe_work_environment == TEST) {
                                        $options.="<option value='".$document_type->id."' ". $selected ."  data-fe='".$document_type->factura_electronica."' data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."'>".$document_type->sales_prefix ." - ".$document_type->nombre."</option>";
                                    }
                                } else {
                                    if ($document_type->fe_work_environment != TEST) {
                                        $options.="<option value='".$document_type->id."' ".$selected." data-fe='".$document_type->factura_electronica."' data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."'>".$document_type->sales_prefix ." - ".$document_type->nombre."</option>";
                                    }
                                }
                            }
                        } else {
                            if ($this->pos_settings->work_environment == TEST) {
                                if ($document_type->fe_work_environment == TEST) {
                                    $options.="<option value='".$document_type->id."' ". $selected ."  data-fe='".$document_type->factura_electronica."' data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."'>".$document_type->sales_prefix ." - ".$document_type->nombre."</option>";
                                }
                            } else {
                                if ($document_type->fe_work_environment != TEST) {
                                    $options.="<option value='".$document_type->id."' ".$selected." data-fe='".$document_type->factura_electronica."' data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."'>".$document_type->sales_prefix ." - ".$document_type->nombre."</option>";
                                }
                            }
                        }
                    } else {
                        if ($document_type->factura_electronica == NOT) {
                            $options.="<option value='".$document_type->id."'  ".$selected." data-fe='".$document_type->factura_electronica."' data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."'>".$document_type->sales_prefix." - ".$document_type->nombre."</option>";
                        }
                    }
                } else if ($module == FACTURA_DETAL) {
                    if ($this->Settings->electronic_billing == YES) {
                        if ($this->Settings->fe_work_environment == TEST) {
                            if ($document_type->fe_work_environment == TEST) {
                                $options.="<option value='".$document_type->id."' ". $selected ."  data-fe='".$document_type->factura_electronica."' data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."'>".$document_type->sales_prefix ." - ".$document_type->nombre."</option>";
                            }
                        } else {
                            if ($document_type->fe_work_environment != TEST) {
                                $options.="<option value='".$document_type->id."' ".$selected." data-fe='".$document_type->factura_electronica."'  data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."'>".$document_type->sales_prefix ." - ".$document_type->nombre."</option>";
                            }
                        }
                    } else {
                        $options.="<option value='".$document_type->id."'  ".$selected." data-fe='".$document_type->factura_electronica."' data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."'>".$document_type->sales_prefix." - ".$document_type->nombre."</option>";
                    }
                } else {
                    $options.="<option value='".$document_type->id."'  ".$selected." data-fe='".$document_type->factura_electronica."'  data-kl='".$document_type->key_log."' data-prefix='".$document_type->sales_prefix."' data-module='".$document_type->module."'>".$document_type->sales_prefix." - ".$document_type->nombre."</option>";
                }
            }
        } else {
            $options.='<option value="">'.($empty_word ? lang('all_document_types') : 'Sin resoluciones').'</option>';
        }

        echo json_encode(['status' => $status, 'options' => $options, 'not_parametrized' => trim($not_parametrized, ", ")]);
    }

    public function getBillersDocumentTypesAjax()
    {
        $options        = "<option value='' selected>". lang('select') ."</option>";
        $isItElectronic = $this->input->post("isItElectronic");
        $billerId       = $this->input->post("billerId");
        $module         = $this->input->post('module');
        $status         = false;
        $notParametrized= "";

        $data = [
            "bdt.biller_id" =>$billerId,
            "module"        =>$module
        ];

        if (!($this->Owner || $this->Admin)) {
            $data["u.id"] = $this->session->userdata("user_id");
        }

        if ($isItElectronic != null) {
            $data["factura_electronica"] = $isItElectronic;
        }

        if ($isItElectronic == 1) {
            $data["fe_work_environment"] = $this->Settings->fe_work_environment;
        }

        $documentTypes = $this->DocumentsTypes_model->getDocumentsTypeBiller($data);
        if (!empty($documentTypes)) {
            $status = true;

            foreach ($documentTypes as $documentType) {
                $options .= "<option value='{$documentType->id}'
                    data-fe='{$documentType->factura_electronica}'
                    data-kl='{$documentType->key_log}'
                    data-prefix='{$documentType->sales_prefix}'> {$documentType->sales_prefix} - {$documentType->nombre}</option>";

                if (!$this->site->validate_doc_type_accounting_parametrization($documentType->id)) {
                    $notParametrized .= $documentType->sales_prefix.", ";
                }
            }
        }

        echo json_encode(['status' => $status, 'options' => $options, 'not_parametrized' => trim($notParametrized, ", ")]);
    }

    public function getMultipleBillersDocumentTypes()
    {
        $options = "";
        $biller_id = $this->input->post("biller_id");
        $modules = json_decode($this->input->post("modules"));
        $bdt = $this->companies_model->getMultipleBillerDocumentTypes($biller_id, $modules);
        $not_parametrized = "";
        if ($bdt) {
            $status = '1';
            if (count($bdt) > 1) {
                $options.="<option value=''>".lang('select')."</option>";
            }
            foreach ($bdt as $document_type) {
                $selected = "";
                $options.="<option value='".$document_type->id."'>".$document_type->sales_prefix."</option>";
            }
        } else {
            $status = '0';
            $options.='<option value="">'.'Sin resoluciones'.'</option>';
        }
        $response = ['status' => $status, 'options' => $options, 'not_parametrized' => trim($not_parametrized, ", ")];
        echo json_encode($response);
    }

    public function view($id)
    {
        $this->sma->checkPermissions('index', true);
        $company_details = $this->companies_model->getCompanyByID($id);
        $company_details_aditional = $this->companies_model->getBillerDataAditional($id);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['biller'] = $company_details;
        $this->data['biller_aditional'] = $company_details_aditional;
        $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['customers'] = $this->companies_model->getAllCustomerCompanies();
        $this->data['sellers'] = $this->site->getSellerByBiller($id);
        $this->data['cost_centers'] = $this->site->getCostCenters($id);
        $this->load_view($this->theme.'billers/view', $this->data);
    }

    public function update_status($id)
    {
        $this->form_validation->set_rules('status', lang("status"), 'required');
        if ($this->form_validation->run() == true) {
            $status = $this->input->post('status');
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'billers');
        }
        if ($this->form_validation->run() == true) {

            if ($status == 1) {
                if ($this->companies_model->activateBiller($id)) {
                    $this->session->set_flashdata('message', lang('biller_activated'));
                }
            } else {
                if ($this->companies_model->deactivateBiller($id)) {
                    $this->session->set_flashdata('message', lang('biller_deactivated'));
                } else {
                    $this->session->set_flashdata('error', lang('biller_x_deactivate_is_biller_default'));
                }
            }
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'billers');
        } else {
            $this->data['biller'] = $this->companies_model->getCompanyByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'billers/update_status', $this->data);
        }
    }

    public function random_pin_code()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('random_pin_code')));
        $meta = array('page_title' => lang('random_pin_code'), 'bc' => $bc);
        $this->page_construct('billers/random_pin_code', $meta, $this->data);
    }

    public function update_random_pin_code()
    {
        $biller_id = $this->input->get('biller_id');
        $random_pin_code = $this->input->get('random_pin_code');
        $this->db->update('biller_data', ['random_pin_code' => $random_pin_code, 'random_pin_code_date' => date('Y-m-d H:i:s'), 'last_update' => date('Y-m-d H:i:s')], ['biller_id'=>$biller_id]);
        echo json_encode(['response' => 1]);
    }

    public function get_random_pin_code()
    {
        $this->site->validate_random_pin_code_date();
        $biller_id = $this->input->get('biller_id');
        $bdata = $this->db->get_where('biller_data', ['biller_id'=>$biller_id]);
        if ($bdata->num_rows() > 0) {
            $bdata = $bdata->row();
            echo json_encode(['pin_code'=>md5($bdata->pin_code_method == 2 ? $bdata->random_pin_code : $bdata->pin_code)]);
        } else{
            echo json_encode(['pin_code'=>false]);
        }
    }
}
