<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency_model extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }

    public function get_currency_by_code($code)
    {
    	$this->db->select("*");
    	$this->db->from("currencies");
    	$this->db->where("code", $code);
    	$response = $this->db->get();

    	return $response->row();
    }
}

/* End of file Currency_model.php */
/* Location: .//opt/lampp/htdocs/wappsi/app/models/admin/Currency_model.php */