<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        function tax_type(x) {
            return (x == 1) ? "<?=lang('percentage')?>" : "<?=lang('fixed')?>";
        }

        oTable = $('#wmsPicking').dataTable({
            "aaSorting": [[2, "asc"], [3, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('wms/getPickings') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex)
            {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "wms_picking_link";
                return nRow;
            },
            "aoColumns": [
                        {"mRender": checkbox},
                        {"mRender": fld},
                        null, 
                        null, 
                        null, 
                        null, 
                        null, 
                        {"mRender": por_status },
                        {"mRender": por_status },
                        {"bSortable": false}
                    ]
        });
    });

    <?php if ($this->session->userdata('remove_wmspicking')) {?>
        localStorage.removeItem('wmspicustomer');
        localStorage.removeItem('wmspibiller');
        localStorage.removeItem('wmspios_reference');
        localStorage.removeItem('wms_order_sales');
    <?php
        $this->sma->unset_data('remove_wmspicking');
    }
    ?>
</script>
<?= admin_form_open('system_settings/warehouse_actions', 'id="action-form"') ?>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $page_title ?></h2>
    </div>
</div>
<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?php echo admin_url('wms/add_picking'); ?>">
                                            <i class="fa fa-plus"></i> <?= lang('add_picking') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><?//= lang("list_results"); ?></p> -->
                            <div class="table-responsive">
                                <table id="wmsPicking" class="table table-bordered table-hover table-condensed reports-table">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkth" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("date"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("biller"); ?></th>
                                        <th><?= lang("customer"); ?></th>
                                        <th><?= lang("packing_warehouse"); ?></th>
                                        <th><?= lang("order_sale_origin"); ?></th>
                                        <th><?= lang("packed"); ?></th>
                                        <th><?= lang("invoiced"); ?></th>
                                        <th style="width:65px;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="4" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->
<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>

