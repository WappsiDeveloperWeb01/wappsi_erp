<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SupportDocument_model extends CI_Model
{
    const PER_OPERATION = 1;
    const WEEKLY_ACCUMULATED = 2;
    const SUPPORT_DOCUMENT = 35;
    const SUPPORTING_DOCUMENT_ADJUSTMENT_NOTE = 52;

    public function getResolutionData($id)
    {
        $this->db->select("*");
        $this->db->from("documents_types");
        if (!empty($id)) {
            $this->db->where("id", $id);
        }
        $response = $this->db->get();

        if (!empty($id)) {
            return $response->row();
        } else {
            return $response->result();
        }
    }

    public function buildRequestFile($documentId, $toShow = FALSE)
    {
        $technologyProvider = $this->Settings->fe_technology_provider;
        $typeElectronicDocument = $this->getTypeElectronicDocument($documentId);

        if (CADENA == CADENA) {
            
            if ($typeElectronicDocument == self::SUPPORT_DOCUMENT) {
                $this->SupportDocument_model->cadenaBuildXmlSupportDocument($documentId, $toShow);
            } else if ($typeElectronicDocument == self::SUPPORTING_DOCUMENT_ADJUSTMENT_NOTE) {
                $this->SupportDocument_model->cadenaBuildXmlSupportDocumentAdjusmentNote($documentId, $toShow);
            }
        } else if ($technologyProvider == BPM) {
            if ($typeElectronicDocument == self::SUPPORT_DOCUMENT) {
                $json = $this->bpmBuildJsonSupportDocument($documentId, $toShow);
            } else if ($typeElectronicDocument == self::SUPPORTING_DOCUMENT_ADJUSTMENT_NOTE) {
                // $json = $this->bpmBuildJsonSupportDocumentAdjusmentNote($documentId, $toShow);
            }
        }
    }

    public function sendElectronicDocument($documentId)
    {
        $typeElectronicDocument = $this->getTypeElectronicDocument($documentId);
        $feTechnologyProvider = $this->Settings->fe_technology_provider;

        $this->validationDocumentElectronic($documentId);

        if (CADENA == CADENA) {
            if ($typeElectronicDocument == self::SUPPORT_DOCUMENT) {
                $xmlFile = $this->cadenaBuildXmlSupportDocument($documentId);
            } else if ($typeElectronicDocument == self::SUPPORTING_DOCUMENT_ADJUSTMENT_NOTE) {
                $xmlFile = $this->cadenaBuildXmlSupportDocumentAdjusmentNote($documentId);
            }
        } elseif ($feTechnologyProvider == BPM) {
            if ($typeElectronicDocument == self::SUPPORT_DOCUMENT) {
                $xmlFile = $this->SupportDocument_model->bpmBuildJsonSupportDocument($documentId);
            } else if ($typeElectronicDocument == self::SUPPORTING_DOCUMENT_ADJUSTMENT_NOTE) {
                $xmlFile = $this->SupportDocument_model->bpmBuildJsonSupportDocumentAdjusmentNote($documentId);
            }
        }

        $response = $this->consumeWebService($xmlFile);

        $this->manageResponse($response, $documentId);
    }

    public function getTypeElectronicDocument($purchaseId)
    {
        $purchase = $this->site->getPurchaseByID($purchaseId);
        $resolution = $this->getResolutionData($purchase->document_type_id);

        return $resolution->module;
    }

    private function validationDocumentElectronic($purchaseId)
    {
        $hits = $this->site->getHits();
        $customer = $this->site->get_setting();
        $purchase = $this->site->getPurchaseByID($purchaseId);
        $payments = $this->site->getPurchasePayments($purchaseId);
        $purchaseItems = $this->site->getAllPurchaseItems($purchaseId);
        $supplier = $this->site->getCompanyByID($purchase->supplier_id);
        $invoicing = $this->site->getInvoicing(ELECTRONIC_DOCUMENTS);
        $customerTypeRegimen = $this->getTypeRegime($customer->tipo_regimen)->codigo;
        $customerObligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
        $supplierObligations = $this->site->getTypesCustomerObligations($purchase->supplier_id, ACQUIRER);
        $resolution = $this->getResolutionData($purchase->document_type_id);

        if ($this->Settings->electronic_hits_validation == YES) {
            if ($invoicing->amount <= 0) {
                $response = (object) [
                    "response"  => FALSE,
                    "message"   => $this->lang->line('empty_electronic_packages')
                ];
            }

            if ($hits->amount > $invoicing->amount) {
                $response = (object) [
                    "response"  => FALSE,
                    "message"   => $this->lang->line("past_max_attempts")
                ];
            }
        }

        if ($customer->tipo_documento != NIT) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. El tipo de documento del cliente no es permitido."
            ];
        }

        if (empty($customer->tipo_persona)) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. El Tipo de persona para el cliente no puede ser nulo."
            ];
        }

        if (empty($customerObligations)) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. Las responsabilidades para el cliente no se han asignado."
            ];
        }

        if (empty($customerTypeRegimen)) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. El código tipo de régimen del cliente no puede ser nulo."
            ];
        }

        if ($supplier->document_code != 31) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. El código del tipo de documento del proveedor no corresponde a un NIT."
            ];
        }

        if (empty($supplier->address)) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. Se debe agregar una dirrección para el proveedor."
            ];
        }

        if (empty($supplierObligations)) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. Las responsabilidades para el proveedor no se han asignado."
            ];
        }

        if (strpos($supplier->vat_no, ".") || strpos($supplier->vat_no, "-")) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. El número del documento del proveedor no puede llevar puntos (.) ni guiones (-)."
            ];
        }

        if ($supplier->taxResident == NOT && (empty($supplier->postal_code) || (strlen($supplier->postal_code) != 6))) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. Por favor validar el código postal del proveedor, debe ser un código válido."
            ];
        }

        if (empty($purchaseItems)) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. No existen artículos relacionados a la Documento."
            ];
        }

        if (!empty($payments)) {
            foreach ($payments as $payment) {
                if ($payment->paid_by != 'retencion') {
                    if (empty($payment->mean_payment_code_fe)) {
                        $response = (object) [
                            "status" => FALSE,
                            "message" => "Documento electrónico no enviado. El Documento no tiene asignado el código de medio de pago DIAN."
                        ];
                    }

                    if (empty($payment->amount)) {
                        $response = (object) [
                            "status" => FALSE,
                            "message" => "Documento electrónico no enviado. Por favor, verificar registro de los pagos para el documento."
                        ];
                    }
                }
            }
        }

        foreach ($purchaseItems as $item) {
            if (empty($item->code_fe)) {
                $response = (object) [
                    "status" => FALSE,
                    "message" => "Documento electrónico no enviado. El código de impuesto para el impuesto " . $item->name . " dado por la DIAN no se encuentra diligenciado."
                ];

                break;
            }
        }

        if ($purchase->acceptedDian == 2) {
            $response = (object) [
                "status" => FALSE,
                "message" => "Documento electrónico no enviado. El documento ya ha sido enviado."
            ];
        }

        if ($resolution->module == self::SUPPORT_DOCUMENT) {
            if ($purchase->status != 'received') {
                $response = (object) [
                    "status" => FALSE,
                    "message" => "Documento electrónico no enviado. El documento se encuentra en estado NO APROBADO."
                ];
            }
        }

        if (isset($response) && $response->status === FALSE) {
            $this->session->set_flashdata('error', $response->message);
            admin_redirect("purchases/supporting_document_index");
        }
    }

    private function getTypeRegime($id)
    {
        $this->db->select('codigo');
        $this->db->where('id', $id);
        $response = $this->db->get('types_vat_regime');

        return $response->row();
    }

    /********************************* CADENA *********************************/
        public function cadenaBuildXmlSupportDocument($documentId, $showXml = false)
        {
            $customer = $this->site->get_setting();
            $taxRates = $this->getTaxRateByCodeFe();
            $purchase = $this->site->getPurchaseByID($documentId);
            $payments = $this->site->getPurchasePayments($documentId);
            $biller = $this->site->getCompanyByID($purchase->biller_id);
            $purchaseItems = $this->site->getAllPurchaseItems($documentId);
            $supplier = $this->site->getCompanyByID($purchase->supplier_id);
            $resolution = $this->site->getDocumentTypeById($purchase->document_type_id);
            $customerObligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $supplierObligations = $this->site->getTypesCustomerObligations($purchase->supplier_id);
            $technologyProvider = $this->site->getTechnologyProviderConfiguration(CADENA, $this->Settings->fe_work_environment);
            $UUID = $this->generateCUDS($purchase, $purchaseItems, $supplier, $customer, $resolution, $technologyProvider, $showXml);

            if ($showXml) {
                header("content-type: application/xml;");
            }

            $xml = new DOMDocument("1.0");
            $xml_invoice = $xml->createElement("Invoice");
            $xml_invoice->setAttribute("xmlns", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
            $xml_invoice->setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
            $xml_invoice->setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
            $xml_invoice->setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
            $xml_invoice->setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
            $xml_invoice->setAttribute("xmlns:sts", "dian:gov:co:facturaelectronica:Structures-2-1");
            $xml_invoice->setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
            $xml_invoice->setAttribute("xmlns:xades141", "http://uri.etsi.org/01903/v1.4.1#");
            $xml_invoice->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            $xml_invoice->setAttribute("xsi:schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Invoice-2.1.xsd");

            /***************** Resolución de numeración *******************/
                $xml_ext_UBLExtensions = $xml->createElement("ext:UBLExtensions");
                $xml_ext_UBLExtension = $xml->createElement("ext:UBLExtension");
                $xml_ext_ExtensionContent = $xml->createElement("ext:ExtensionContent");
                $xml_sts_DianExtensions = $xml->createElement("sts:DianExtensions");
                $xml_sts_InvoiceControl = $xml->createElement("sts:InvoiceControl");
                $xml_sts_InvoiceControl->appendChild($xml->createElement("sts:InvoiceAuthorization", $resolution->num_resolucion));
                $xml_sts_AuthorizationPeriod = $xml->createElement("sts:AuthorizationPeriod");
                $xml_sts_AuthorizationPeriod->appendChild($xml->createElement("cbc:StartDate", $resolution->emision_resolucion));
                $xml_sts_AuthorizationPeriod->appendChild($xml->createElement("cbc:EndDate", $resolution->vencimiento_resolucion));
                $xml_sts_InvoiceControl->appendChild($xml_sts_AuthorizationPeriod);
                $xml_sts_AuthorizedInvoices = $xml->createElement("sts:AuthorizedInvoices");
                $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:Prefix', $resolution->sales_prefix));
                $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:From', $resolution->inicio_resolucion));
                $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:To', $resolution->fin_resolucion));
                $xml_sts_InvoiceControl->appendChild($xml_sts_AuthorizedInvoices);
                $xml_sts_DianExtensions->appendChild($xml_sts_InvoiceControl);
                $xml_ext_ExtensionContent->appendChild($xml_sts_DianExtensions);
                $xml_ext_UBLExtension->appendChild($xml_ext_ExtensionContent);
                $xml_ext_UBLExtensions->appendChild($xml_ext_UBLExtension);
                $xml_invoice->appendChild($xml_ext_UBLExtensions);
            /**************************************************************/

            /************************* Encabezado *************************/
                $custumazionId = ($supplier->taxResident == YES) ? 11 : 10;
                $xml_invoice->appendChild($xml->createElement("cbc:CustomizationID", $custumazionId));
                $xml_invoice->appendChild($xml->createElement("cbc:ProfileExecutionID", $resolution->fe_work_environment));
                $xml_invoice->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $purchase->reference_no)));
                $xml_cbc_UUID = $xml->createElement("cbc:UUID", $UUID);
                $xml_cbc_UUID->setAttribute("schemeID", $resolution->fe_work_environment);
                $xml_cbc_UUID->setAttribute("schemeName", "CUDS-SHA384");
                $xml_invoice->appendChild($xml_cbc_UUID);
                $xml_invoice->appendChild($xml->createElement("cbc:IssueDate", $this->formatDate($purchase->date)->date));
                $xml_invoice->appendChild($xml->createElement("cbc:IssueTime", $this->formatDate($purchase->date)->time));
                $xml_invoice->appendChild($xml->createElement("cbc:InvoiceTypeCode", "05"));
                $xml_invoice->appendChild($xml->createElement("cbc:DocumentCurrencyCode", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency)));
                $xml_invoice->appendChild($xml->createElement("cbc:LineCountNumeric", count($purchaseItems)));
            /**************************************************************/

            /*************************** Emisor ***************************/
                $xml_cac_AccountingSupplierParty = $xml->createElement("cac:AccountingSupplierParty");
                $xml_cac_AccountingSupplierParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $supplier->type_person));
                $xml_cac_Party = $xml->createElement("cac:Party");
                $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                $xml_cac_Address = $xml->createElement("cac:Address");
                $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($supplier->city_code, -5)));
                $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($supplier->city))));
                $xml_cac_Address->appendChild($xml->createElement("cbc:PostalZone", $supplier->postal_code));
                $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($supplier->state))));
                $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($supplier->CODDEPARTAMENTO, -2)));
                $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $supplier->address));
                $xml_cac_Address->appendChild($xml_cac_AddressLine);
                $xml_cac_Country = $xml->createElement("cac:Country");
                $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $supplier->codigo_iso));
                $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($supplier->country)));
                $xml_cbc_Name->setAttribute("languageID", "es");
                $xml_cac_Country->appendChild($xml_cbc_Name);
                $xml_cac_Address->appendChild($xml_cac_Country);
                $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);
                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($supplier->name)));
                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clearDocumentNumber($supplier->vat_no));
                $xml_cbc_CompanyID->setAttribute("schemeID", $supplier->digito_verificacion);
                $xml_cbc_CompanyID->setAttribute("schemeName", $supplier->document_code);
                $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                $typeSupplierObligations = '';
                if (!empty($supplierObligations)) {
                    foreach ($supplierObligations as $supplierObligation) {
                        $typeSupplierObligations .= $supplierObligation->types_obligations_id . ';';
                    }
                }
                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($typeSupplierObligations, ";"));
                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);
                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "ZZ"));
                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "No aplica"));
                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);
                $xml_cac_AccountingSupplierParty->appendChild($xml_cac_Party);
                $xml_invoice->appendChild($xml_cac_AccountingSupplierParty);
            /**************************************************************/

            /************************ Adquiriente. ************************/
                $xml_cac_AccountingCustomerParty = $xml->createElement("cac:AccountingCustomerParty");
                $xml_cac_AccountingCustomerParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $customer->tipo_persona));
                $xml_cac_Party = $xml->createElement("cac:Party");
                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer->razon_social)));
                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clearDocumentNumber($customer->numero_documento));
                $xml_cbc_CompanyID->setAttribute("schemeID", $customer->digito_verificacion);
                $xml_cbc_CompanyID->setAttribute("schemeName", $customer->tipo_documento);
                $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                $typeCustomerObligations = '';
                if (!empty($customerObligations)) {
                    foreach ($customerObligations as $customerObligation) {
                        $typeCustomerObligations .= $customerObligation->types_obligations_id . ';';
                    }
                }
                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($typeCustomerObligations, ";"));
                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);
                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);
                $xml_cac_AccountingCustomerParty->appendChild($xml_cac_Party);
                $xml_invoice->appendChild($xml_cac_AccountingCustomerParty);
            /**************************************************************/

            /************************ Medios pago *************************/
                $paymentAmount = 0;
                $paymentsCleanArray = [];

                if (!empty($payments)) {
                    foreach ($payments as $payment) {
                        $paymentAmount += $payment->amount;

                        if ($payment->paid_by != 'retencion') {
                            $paymentsCleanArray[] = $payment;
                        }
                    }
                }

                $method = ($purchase->grand_total == $paymentAmount) ? CASH : CREDIT;

                if (!empty($paymentsCleanArray)) {
                    foreach ($paymentsCleanArray as $payment) {
                        $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $payment->mean_payment_code_fe));
                        if ($method == CREDIT) {
                            $creationDate = date("Y-m-d", strtotime($purchase->date));
                            $paymentTerm = (empty($purchase->payment_term)) ? 0 : $purchase->payment_term;
                            $dueDate = date("Y-m-d", strtotime("+" . $paymentTerm . " day", strtotime($creationDate)));

                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $dueDate));
                        }
                        $xml_invoice->appendChild($xml_cac_PaymentMeans);
                    }
                } else {
                    $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                    $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                    $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", "ZZZ"));

                    if ($method == CREDIT) {
                        $creationDate = date("Y-m-d", strtotime($purchase->date));
                        $paymentTerm = (empty($purchase->payment_term)) ? 0 : $purchase->payment_term;
                        $dueDate = date("Y-m-d", strtotime("+" . $paymentTerm . " day", strtotime($creationDate)));

                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $dueDate));
                    }
                    $xml_invoice->appendChild($xml_cac_PaymentMeans);
                }
            /**************************************************************/

            /******************** Descuentos y cargos *********************/
                $chargeOrDiscount = 1;

                if ($purchase->order_discount > 0) {
                    $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $chargeOrDiscount++));
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "false"));
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReasonCode", "01"));
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReason", "Otro descuento"));

                    if (strpos($purchase->order_discount_id, "%") !== FALSE) {
                        $orderDiscountPercentage = str_replace("%", "", $purchase->order_discount_id);
                    } else {
                        $orderDiscountPercentage = ($purchase->order_discount * 100) / ($purchase->total + $purchase->total_tax);
                    }
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimalNoRound($orderDiscountPercentage, NUMBER_DECIMALS)));

                    $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($purchase->order_discount, NUMBER_DECIMALS));
                    $xml_cac_Amount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                    $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($purchase->total + $purchase->total_tax, NUMBER_DECIMALS));
                    $xml_cac_BaseAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);
                    $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                }

                if ($purchase->shipping > 0) {
                    $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $chargeOrDiscount++));
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                    $orderChargePercentage = ($purchase->shipping * 100) / $purchase->total;
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($orderChargePercentage, NUMBER_DECIMALS)));

                    $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($purchase->shipping, NUMBER_DECIMALS));
                    $xml_cac_Amount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                    $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($purchase->total, NUMBER_DECIMALS));
                    $xml_cac_BaseAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                    $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                }
            /**************************************************************/

            /********************** Importes totales. *********************/
                /************************ Impuestos. **********************/
                    $productTaxIva = $product_tax_ic = $product_tax_inc = $product_tax_bolsas = $subtotal = $totalTax = $shipping = $order_discount = $exempt_existing = $nominal_tax = $orderDiscount =  0;

                    foreach ($taxRates as $taxRate) {
                        if ($taxRate->code_fe == IVA) {
                            $index = (int) $taxRate->rate;
                            $taxValueIvaArray[$index] = 0;
                            $taxBaseIvaArray[$index] = 0;
                        } else if ($taxRate->code_fe == INC) {
                            $index = (int) $taxRate->rate;
                            $tax_value_inc_array[$index] = 0;
                            $tax_base_inc_array[$index] = 0;
                        } else if ($taxRate->code_fe == BOLSAS) {
                            $index = (int) $taxRate->rate;
                            $tax_base_bolsas_array[$index] = 0;
                            $tax_value_bolsas_array[$index] = 0;
                        }
                    }

                    foreach ($purchaseItems as $item) {
                        $netUnitCost = $this->sma->formatDecimal($item->net_unit_cost, NUMBER_DECIMALS);
                        $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);

                        $subtotal += $this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS);
                        $totalTax += $this->sma->formatDecimal(($this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                    }
                /***********************************************************/
            /**************************************************************/

            /************************ Retenciones. ************************/
                if ($purchase->rete_fuente_total > 0) {
                    $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                        $rete_fuente_total = ($subtotal * $purchase->rete_fuente_percentage) / 100;

                        $TaxAmount = $rete_fuente_total;
                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                            $xml_cbc_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                        $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                            $TaxableAmount = $subtotal;
                            $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                $xml_cbc_TaxableAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                            $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                $xml_cbc_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($purchase->rete_fuente_percentage, 5)));

                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "06"));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteRenta"));
                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                        $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                    $xml_invoice->appendChild($xml_cac_WithholdingTaxTotal);
                }
            /**************************************************************/

            /********************** Total impuestos ***********************/
                $xml_cac_LegalMonetaryTotal = $xml->createElement("cac:LegalMonetaryTotal");
                $lineExtensionAmount = $subtotal;
                $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($lineExtensionAmount, NUMBER_DECIMALS));
                $xml_cbc_LineExtensionAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_LineExtensionAmount);

                $taxExclusiveAmount = $subtotal;
                $xml_cbc_TaxExclusiveAmount = $xml->createElement("cbc:TaxExclusiveAmount", $this->sma->formatDecimal($taxExclusiveAmount, NUMBER_DECIMALS));
                $xml_cbc_TaxExclusiveAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxExclusiveAmount);

                $taxInclusiveAmount = ($this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($totalTax, NUMBER_DECIMALS));
                $xml_cbc_TaxInclusiveAmount = $xml->createElement("cbc:TaxInclusiveAmount", $this->sma->formatDecimal($taxInclusiveAmount, NUMBER_DECIMALS));
                $xml_cbc_TaxInclusiveAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxInclusiveAmount);

                if ($purchase->order_discount > 0) {
                    $orderDiscount = $purchase->order_discount;
                    $allowanceTotalAmount = $orderDiscount;

                    $xml_cbc_AllowanceTotalAmount = $xml->createElement("cbc:AllowanceTotalAmount", $this->sma->formatDecimal($allowanceTotalAmount, NUMBER_DECIMALS));
                    $xml_cbc_AllowanceTotalAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_AllowanceTotalAmount);
                }

                if ($purchase->shipping > 0) {
                    $shipping = $purchase->shipping;
                    $chargeTotalAmount = $shipping;

                    $xml_cbc_ChargeTotalAmount = $xml->createElement("cbc:ChargeTotalAmount", $this->sma->formatDecimal($chargeTotalAmount, NUMBER_DECIMALS));
                    $xml_cbc_ChargeTotalAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_ChargeTotalAmount);
                }

                $payableAmount = ($this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($totalTax, NUMBER_DECIMALS) + $shipping - $orderDiscount);
                $xml_cbc_PayableAmount = $xml->createElement("cbc:PayableAmount", $this->sma->formatDecimal($payableAmount, NUMBER_DECIMALS));
                $xml_cbc_PayableAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_PayableAmount);
                $xml_invoice->appendChild($xml_cac_LegalMonetaryTotal);
            /**************************************************************/

            /************************* Productos **************************/
                $consecutiveProductIdentifier = 1;
                foreach ($purchaseItems as $item) {
                    $netUnitCost = $this->sma->formatDecimal($item->net_unit_cost, NUMBER_DECIMALS);
                    $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);

                    $xml_cac_InvoiceLine = $xml->createElement("cac:InvoiceLine");
                    $xml_cac_InvoiceLine->appendChild($xml->createElement("cbc:ID", $consecutiveProductIdentifier));
                    $xml_cbc_InvoicedQuantity = $xml->createElement("cbc:InvoicedQuantity", $quantity);
                    $xml_cbc_InvoicedQuantity->setAttribute("unitCode", "NIU");
                    $xml_cac_InvoiceLine->appendChild($xml_cbc_InvoicedQuantity);

                    $lineExtensionAmount = ($netUnitCost * $quantity);
                    $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($lineExtensionAmount, NUMBER_DECIMALS));
                    $xml_cbc_LineExtensionAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_InvoiceLine->appendChild($xml_cbc_LineExtensionAmount);

                    $cacInvoicePeriod = $xml->createElement('cac:InvoicePeriod');
                    $cacInvoicePeriod->appendChild($xml->createElement('cbc:StartDate', $this->formatDate($purchase->date)->date));
                    if ($purchase->generationTransmissionMode == self::PER_OPERATION) {
                        $cacInvoicePeriod->appendChild($xml->createElement('cbc:DescriptionCode', self::PER_OPERATION));
                        $cacInvoicePeriod->appendChild($xml->createElement('cbc:Description', 'Por operación'));
                    } else {
                        $cacInvoicePeriod->appendChild($xml->createElement('cbc:DescriptionCode', self::WEEKLY_ACCUMULATED));
                        $cacInvoicePeriod->appendChild($xml->createElement('cbc:Description', 'Acumulado semanal'));
                    }
                    $xml_cac_InvoiceLine->appendChild($cacInvoicePeriod);

                    $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                        $TaxAmount = $this->sma->formatDecimal(($this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                            $xml_cbc_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                        $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                            $TaxableAmount = ($netUnitCost * $quantity);
                            $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                $xml_cbc_TaxableAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                            $TaxAmount = $this->sma->formatDecimal(($this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                            $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                $xml_cbc_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                if ($item->code_fe != BOLSAS) {
                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($item->rate, NUMBER_DECIMALS)));
                                }
                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->code_fe));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->code_fe_name));
                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                    $xml_cac_InvoiceLine->appendChild($xml_cac_TaxTotal);
                    $xml_cac_Item = $xml->createElement("cac:Item");
                    $xml_cac_Item->appendChild($xml->createElement("cbc:Description", htmlspecialchars($item->product_name)));

                    $xml_cac_StandardItemIdentification = $xml->createElement("cac:StandardItemIdentification");
                    $xml_cbc_ID = $xml->createElement("cbc:ID", $item->product_code);
                    $xml_cbc_ID->setAttribute("schemeID", "999");
                    $xml_cbc_ID->setAttribute("schemeName", "Estándar de adopción del contribuyente");
                    $xml_cac_StandardItemIdentification->appendChild($xml_cbc_ID);
                    $xml_cac_Item->appendChild($xml_cac_StandardItemIdentification);
                    $xml_cac_InvoiceLine->appendChild($xml_cac_Item);

                    $xml_cac_Price = $xml->createElement("cac:Price");
                    $priceAmount = $item->net_unit_cost;
                    $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $netUnitCost);
                    $xml_cbc_PriceAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_Price->appendChild($xml_cbc_PriceAmount);

                    $xml_cbc_BaseQuantity = $xml->createElement("cbc:BaseQuantity", $quantity);
                    $xml_cbc_BaseQuantity->setAttribute("unitCode", "NIU");
                    $xml_cac_Price->appendChild($xml_cbc_BaseQuantity);
                    $xml_cac_InvoiceLine->appendChild($xml_cac_Price);
                    $xml_invoice->appendChild($xml_cac_InvoiceLine);

                    $consecutiveProductIdentifier++;
                }
            /**************************************************************/

            /************************* DATA *******************************/
                $xml_DATA = $xml->createElement("DATA");
                    $xml_DATA->appendChild($xml->createElement("UBL21", "true"));
                    $xml_Partnership = $xml->createElement("Partnership");
                        $xml_Partnership->appendChild($xml->createElement("ID", "901090070"));
                        $xml_Partnership->appendChild($xml->createElement("TechKey"/*, $resolution->clave_tecnica*/));
                        if ($this->Settings->fe_work_environment == TEST) {
                            $xml_Partnership->appendChild($xml->createElement("SetTestID"/*, $resolution->fe_testid*/));
                        }
                    $xml_DATA->appendChild($xml_Partnership);
                $xml_invoice->appendChild($xml_DATA);
            /**************************************************************/

            $xml->appendChild($xml_invoice);

            if (!$showXml) {
                return base64_encode($xml->saveXML());
            }

            echo $xml->saveXML();
        }

        public function cadenaBuildXmlSupportDocumentAdjusmentNote($documentId, $showXml = false)
        {
            $customer = $this->site->get_setting();
            $taxRates = $this->getTaxRateByCodeFe();
            $purchase = $this->site->getPurchaseByID($documentId);
            $payments = $this->site->getPurchasePayments($documentId);
            $biller = $this->site->getCompanyByID($purchase->biller_id);
            $purchaseItems = $this->site->getAllPurchaseItems($documentId);
            $supplier = $this->site->getCompanyByID($purchase->supplier_id);
            $resolution = $this->site->getDocumentTypeById($purchase->document_type_id);
            $customerObligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $supplierObligations = $this->site->getTypesCustomerObligations($purchase->supplier_id);
            $referencePurchase = $this->site->getPurchaseByID($purchase->purchase_id);
            $technologyProvider = $this->site->getTechnologyProviderConfiguration(CADENA, $this->Settings->fe_work_environment);
            $UUID = $this->generateCUDS($purchase, $purchaseItems, $supplier, $customer, $resolution, $technologyProvider, $showXml);


            if ($showXml) {
                header("content-type: application/xml;");
            }

            $xml = new DOMDocument("1.0");
            $xml_creditNote = $xml->createElement("CreditNote");
            $xml_creditNote->setAttribute("xmlns", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2");
            $xml_creditNote->setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
            $xml_creditNote->setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
            $xml_creditNote->setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
            $xml_creditNote->setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
            $xml_creditNote->setAttribute("xmlns:sts", "dian:gov:co:facturaelectronica:Structures-2-1");
            $xml_creditNote->setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
            $xml_creditNote->setAttribute("xmlns:xades141", "http://uri.etsi.org/01903/v1.4.1#");
            $xml_creditNote->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            $xml_creditNote->setAttribute("xsi:schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-CreditNote-2.1.xsd");

            /************************* Encabezado *************************/
                $custumazionId = ($supplier->taxResident == YES) ? 11 : 10;
                $xml_creditNote->appendChild($xml->createElement("cbc:CustomizationID", $custumazionId));
                $xml_creditNote->appendChild($xml->createElement("cbc:ProfileExecutionID", $resolution->fe_work_environment));
                $xml_creditNote->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $purchase->reference_no)));
                $xml_cbc_UUID = $xml->createElement("cbc:UUID", $UUID);
                    $xml_cbc_UUID->setAttribute("schemeID", $resolution->fe_work_environment);
                    $xml_cbc_UUID->setAttribute("schemeName", "CUDS-SHA384");
                $xml_creditNote->appendChild($xml_cbc_UUID);
                $xml_creditNote->appendChild($xml->createElement("cbc:IssueDate", $this->formatDate($purchase->date)->date));
                $xml_creditNote->appendChild($xml->createElement("cbc:IssueTime", $this->formatDate($purchase->date)->time));
                $xml_creditNote->appendChild($xml->createElement("cbc:CreditNoteTypeCode", "95"));
                $xml_creditNote->appendChild($xml->createElement("cbc:DocumentCurrencyCode", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency)));
                $xml_creditNote->appendChild($xml->createElement("cbc:LineCountNumeric", count($purchaseItems)));
            /**************************************************************/

            /************************ Discrepancia *************************/
                if ($purchase->payment_status == 'partial') {
                    $cbcResponseCode = "1";
                    $cbcDescription = "Devolución parcial de los bienes y/o no aceptación parcial del servicio";
                } else {
                    $cbcResponseCode = "2";
                    $cbcDescription = "Anulación del documento soporte en adquisiciones efectuadas a sujetos no obligados a expedir factura de venta o documento equivalente";
                }

                $xml_cac_DiscrepancyResponse = $xml->createElement("cac:DiscrepancyResponse");
                    $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ResponseCode", $cbcResponseCode));
                    $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:Description", $cbcDescription));
                $xml_creditNote->appendChild($xml_cac_DiscrepancyResponse);
            /**************************************************************/

            /********************* Factura referencia *********************/
                $xml_cac_BillingReference = $xml->createElement("cac:BillingReference");
                    $xml_cac_InvoiceDocumentReference = $xml->createElement("cac:InvoiceDocumentReference");
                        $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $purchase->return_purchase_ref)));
                        $xml_cbc_UUID = $xml->createElement("cbc:UUID", $referencePurchase->uuid);
                            $xml_cbc_UUID->setAttribute("schemeName", "CUDS-SHA384");
                        $xml_cac_InvoiceDocumentReference->appendChild($xml_cbc_UUID);

                        $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:IssueDate", $this->formatDate($referencePurchase->date)->date));

                    $xml_cac_BillingReference->appendChild($xml_cac_InvoiceDocumentReference);
                $xml_creditNote->appendChild($xml_cac_BillingReference);
            /**************************************************************/

            /*************************** Emisor ***************************/
                $xml_cac_AccountingSupplierParty = $xml->createElement("cac:AccountingSupplierParty");
                $xml_cac_AccountingSupplierParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $supplier->type_person));
                $xml_cac_Party = $xml->createElement("cac:Party");
                $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                $xml_cac_Address = $xml->createElement("cac:Address");
                $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($supplier->city_code, -5)));
                $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($supplier->city))));
                $xml_cac_Address->appendChild($xml->createElement("cbc:PostalZone", $supplier->postal_code));
                $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($supplier->state))));
                $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($supplier->CODDEPARTAMENTO, -2)));
                $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $supplier->address));
                $xml_cac_Address->appendChild($xml_cac_AddressLine);
                $xml_cac_Country = $xml->createElement("cac:Country");
                $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $supplier->codigo_iso));
                $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($supplier->country)));
                $xml_cbc_Name->setAttribute("languageID", "es");
                $xml_cac_Country->appendChild($xml_cbc_Name);
                $xml_cac_Address->appendChild($xml_cac_Country);
                $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);
                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($supplier->name)));
                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clearDocumentNumber($supplier->vat_no));
                $xml_cbc_CompanyID->setAttribute("schemeID", $supplier->digito_verificacion);
                $xml_cbc_CompanyID->setAttribute("schemeName", $supplier->document_code);
                $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                $typeSupplierObligations = '';
                if (!empty($supplierObligations)) {
                    foreach ($supplierObligations as $supplierObligation) {
                        $typeSupplierObligations .= $supplierObligation->types_obligations_id . ';';
                    }
                }
                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($typeSupplierObligations, ";"));
                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);
                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);
                $xml_cac_AccountingSupplierParty->appendChild($xml_cac_Party);
                $xml_creditNote->appendChild($xml_cac_AccountingSupplierParty);
            /**************************************************************/

            /************************ Adquiriente. ************************/
                $xml_cac_AccountingCustomerParty = $xml->createElement("cac:AccountingCustomerParty");
                $xml_cac_AccountingCustomerParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $customer->tipo_persona));
                $xml_cac_Party = $xml->createElement("cac:Party");
                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer->razon_social)));
                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clearDocumentNumber($customer->numero_documento));
                $xml_cbc_CompanyID->setAttribute("schemeID", $customer->digito_verificacion);
                $xml_cbc_CompanyID->setAttribute("schemeName", $customer->tipo_documento);
                $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                $typeCustomerObligations = '';
                if (!empty($customerObligations)) {
                    foreach ($customerObligations as $customerObligation) {
                        $typeCustomerObligations .= $customerObligation->types_obligations_id . ';';
                    }
                }
                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($typeCustomerObligations, ";"));
                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);
                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);
                $xml_cac_AccountingCustomerParty->appendChild($xml_cac_Party);
                $xml_creditNote->appendChild($xml_cac_AccountingCustomerParty);
            /**************************************************************/

            /************************ Medios pago *************************/
                $paymentAmount = 0;
                $paymentsCleanArray = [];

                if (!empty($payments)) {
                    foreach ($payments as $payment) {
                        $paymentAmount += $payment->amount;

                        if ($payment->paid_by != 'retencion') {
                            $paymentsCleanArray[] = $payment;
                        }
                    }
                }

                $method = ($purchase->grand_total == $paymentAmount) ? CASH : CREDIT;

                if (!empty($paymentsCleanArray)) {
                    foreach ($paymentsCleanArray as $payment) {
                        $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $payment->mean_payment_code_fe));
                        if ($method == CREDIT) {
                            $creationDate = date("Y-m-d", strtotime($purchase->date));
                            $paymentTerm = (empty($purchase->payment_term)) ? 0 : $purchase->payment_term;
                            $dueDate = date("Y-m-d", strtotime("+" . $paymentTerm . " day", strtotime($creationDate)));

                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $dueDate));
                        }
                        $xml_creditNote->appendChild($xml_cac_PaymentMeans);
                    }
                } else {
                    $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                    $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                    $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", "ZZZ"));

                    if ($method == CREDIT) {
                        $creationDate = date("Y-m-d", strtotime($purchase->date));
                        $paymentTerm = (empty($purchase->payment_term)) ? 0 : $purchase->payment_term;
                        $dueDate = date("Y-m-d", strtotime("+" . $paymentTerm . " day", strtotime($creationDate)));

                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $dueDate));
                    }
                    $xml_creditNote->appendChild($xml_cac_PaymentMeans);
                }
            /**************************************************************/

            /******************** Descuentos y cargos *********************/
                $chargeOrDiscount = 1;

                if ($purchase->order_discount > 0) {
                    $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $chargeOrDiscount++));
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "false"));
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReasonCode", "01"));
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReason", "Otro descuento"));

                    if (strpos($purchase->order_discount_id, "%") !== FALSE) {
                        $orderDiscountPercentage = str_replace("%", "", $purchase->order_discount_id);
                    } else {
                        $orderDiscountPercentage = ($purchase->order_discount * 100) / ($purchase->total + $purchase->totalTax);
                    }
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimalNoRound($orderDiscountPercentage, NUMBER_DECIMALS)));

                    $amount = $purchase->order_discount;
                    $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($amount, NUMBER_DECIMALS));
                    $xml_cac_Amount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                    $baseAmount = $purchase->total + $purchase->total_tax;
                    $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($baseAmount, NUMBER_DECIMALS));
                    $xml_cac_BaseAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);
                    $xml_creditNote->appendChild($xml_cac_AllowanceCharge);
                }

                if ($purchase->shipping > 0) {
                    $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $chargeOrDiscount++));
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                    $orderChargePercentage = ($purchase->shipping * 100) / $purchase->total;
                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($orderChargePercentage, NUMBER_DECIMALS)));

                    $amount = $purchase->shipping;
                    $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($amount, NUMBER_DECIMALS));
                    $xml_cac_Amount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                    $baseAmount = $purchase->total;
                    $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($baseAmount, NUMBER_DECIMALS));
                    $xml_cac_BaseAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                    $xml_creditNote->appendChild($xml_cac_AllowanceCharge);
                }
            /**************************************************************/

            /********************** Importes totales. *********************/
                /************************ Impuestos. **********************/
                    $productTaxIva = $product_tax_ic = $product_tax_inc = $product_tax_bolsas = $subtotal = $totalTax = $shipping = $order_discount = $exempt_existing = $nominal_tax = 0;

                    foreach ($taxRates as $taxRate) {
                        if ($taxRate->code_fe == IVA) {
                            $index = (int) $taxRate->rate;
                            $taxValueIvaArray[$index] = 0;
                            $taxBaseIvaArray[$index] = 0;
                        }
                    }

                    foreach ($purchaseItems as $item) {
                        $netUnitCost = $this->sma->formatDecimal($item->net_unit_cost, NUMBER_DECIMALS);
                        $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);

                        $subtotal += $this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS);
                        $totalTax += $this->sma->formatDecimal(($this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                        if ($item->code_fe == IVA) {
                            if ($item->rate == 0) { $exempt_existing++; }

                            $productTaxIva += $this->sma->formatDecimal(($netUnitCost * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                            foreach ($taxRates as $taxRate) {
                                if ($item->rate == $taxRate->rate) {
                                    $index = (int) $taxRate->rate;
                                    $taxBaseIvaArray[$index] += $this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS);
                                    $taxValueIvaArray[$index] += $this->sma->formatDecimal(($netUnitCost * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                                    break;
                                }
                            }
                        }
                    }

                    if (count($purchaseItems) >= 1 && $exempt_existing > 0) {
                        $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($productTaxIva, 2));
                                $xml_cac_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                            foreach ($taxBaseIvaArray as $percentage => $taxBaseIva) {
                                if (!empty($taxBaseIva)) {
                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($taxBaseIva), 2));
                                            $xml_cac_TaxableAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                        $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($taxValueIvaArray[$percentage], 2));
                                            $xml_cac_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 2)));
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                }
                            }
                        $xml_creditNote->appendChild($xml_cac_TaxTotal);
                    } /*else if (count($items_sale) >= 1 && $exempt_existing > 0) {
                        $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                            $TaxAmount = ($sale->sale_currency != "COP") ? $product_tax_iva / $sale->sale_currency_trm : $product_tax_iva;
                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                $xml_cac_TaxAmount->setAttribute("currencyID", (is_null($sale->sale_currency) ? "COP" : $sale->sale_currency));
                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                            foreach ($taxValueIvaArray as $percentage => $tax_base_iva) {
                                if (!empty($tax_base_iva)) {
                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = ($sale->sale_currency != "COP") ? $tax_base_iva / $sale->sale_currency_trm : $tax_base_iva;
                                        $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                            $xml_cac_TaxableAmount->setAttribute("currencyID", (is_null($sale->sale_currency) ? "COP" : $sale->sale_currency));
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                        $TaxAmount = ($sale->sale_currency != "COP") ? $tax_value_iva_array[$percentage] / $sale->sale_currency_trm : $tax_value_iva_array[$percentage];
                                        $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                            $xml_cac_TaxAmount->setAttribute("currencyID", (is_null($sale->sale_currency) ? "COP" : $sale->sale_currency));
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 2)));
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                }
                            }

                        $xml_invoice->appendChild($xml_cac_TaxTotal);
                    }*/
                /***********************************************************/
            /**************************************************************/

            /************************ Retenciones. ************************/
                // if ($purchase->rete_fuente_total > 0) {
                //     $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                //         $rete_fuente_total = ($subtotal * $purchase->rete_fuente_percentage) / 100;

                //         $TaxAmount = ($purchase->purchase_currency != "COP") ? $rete_fuente_total / $purchase->purchase_currency_trm : $rete_fuente_total;
                //         $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                //             $xml_cbc_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                //         $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                //         $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                //             $TaxableAmount = $subtotal;
                //             $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                //                 $xml_cbc_TaxableAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                //             $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                //             $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                //                 $xml_cbc_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                //             $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                //             $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                //                 $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($purchase->rete_fuente_percentage, 5)));

                //                 $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                //                     $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "06"));
                //                     $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteRenta"));
                //                 $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                //             $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                //         $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                //     $xml_creditNote->appendChild($xml_cac_WithholdingTaxTotal);
                // }
            /**************************************************************/

            /********************** Total impuestos ***********************/
                $xml_cac_LegalMonetaryTotal = $xml->createElement("cac:LegalMonetaryTotal");
                $lineExtensionAmount = $subtotal;
                $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal(abs($lineExtensionAmount), NUMBER_DECIMALS));
                $xml_cbc_LineExtensionAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_LineExtensionAmount);

                $taxExclusiveAmount = $subtotal;
                $xml_cbc_TaxExclusiveAmount = $xml->createElement("cbc:TaxExclusiveAmount", $this->sma->formatDecimal(abs($taxExclusiveAmount), NUMBER_DECIMALS));
                $xml_cbc_TaxExclusiveAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxExclusiveAmount);

                $taxInclusiveAmount = ($this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($totalTax, NUMBER_DECIMALS));
                $xml_cbc_TaxInclusiveAmount = $xml->createElement("cbc:TaxInclusiveAmount", $this->sma->formatDecimal(abs($taxInclusiveAmount), NUMBER_DECIMALS));
                $xml_cbc_TaxInclusiveAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxInclusiveAmount);

                if ($purchase->order_discount > 0) {
                    $orderDiscount = $purchase->order_discount;
                    $allowanceTotalAmount = $orderDiscount;

                    $xml_cbc_AllowanceTotalAmount = $xml->createElement("cbc:AllowanceTotalAmount", $this->sma->formatDecimal($allowanceTotalAmount, NUMBER_DECIMALS));
                    $xml_cbc_AllowanceTotalAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_AllowanceTotalAmount);
                }

                if ($purchase->shipping > 0) {
                    $shipping = $purchase->shipping;
                    $chargeTotalAmount = $shipping;

                    $xml_cbc_ChargeTotalAmount = $xml->createElement("cbc:ChargeTotalAmount", $this->sma->formatDecimal($chargeTotalAmount, NUMBER_DECIMALS));
                    $xml_cbc_ChargeTotalAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                    $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_ChargeTotalAmount);
                }

                $payableAmount = ($this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($totalTax, NUMBER_DECIMALS) + $shipping - $order_discount);
                $xml_cbc_PayableAmount = $xml->createElement("cbc:PayableAmount", $this->sma->formatDecimal(abs($payableAmount), NUMBER_DECIMALS));
                $xml_cbc_PayableAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_PayableAmount);
                $xml_creditNote->appendChild($xml_cac_LegalMonetaryTotal);
            /**************************************************************/

            /************************* Productos **************************/
                $consecutiveProductIdentifier = 1;
                foreach ($purchaseItems as $item) {
                    $xml_cac_CreditNoteLine = $xml->createElement("cac:CreditNoteLine");
                        $xml_cac_CreditNoteLine->appendChild($xml->createElement("cbc:ID", $consecutiveProductIdentifier));
                        $xml_cbc_CreditedQuantity = $xml->createElement("cbc:CreditedQuantity", $this->sma->formatDecimal(abs($item->quantity), NUMBER_DECIMALS));
                            $xml_cbc_CreditedQuantity->setAttribute("unitCode", "NIU");
                        $xml_cac_CreditNoteLine->appendChild($xml_cbc_CreditedQuantity);

                        $lineExtensionAmount = ($this->sma->formatDecimal(abs($item->net_unit_cost), NUMBER_DECIMALS) * $this->sma->formatDecimal(abs($item->quantity), NUMBER_DECIMALS));
                            $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal(abs($lineExtensionAmount), NUMBER_DECIMALS));
                                $xml_cbc_LineExtensionAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                        $xml_cac_CreditNoteLine->appendChild($xml_cbc_LineExtensionAmount);

                        $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                            $TaxAmount = $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_cost * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                            $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                $xml_cbc_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                            $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                $TaxableAmount = ($this->sma->formatDecimal(abs($item->net_unit_cost), NUMBER_DECIMALS) * $this->sma->formatDecimal(abs($item->quantity), NUMBER_DECIMALS));
                                $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                    $xml_cbc_TaxableAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                $TaxAmount = $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_cost * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                                $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                    $xml_cbc_TaxAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($item->rate, NUMBER_DECIMALS)));
                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->code_fe));
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->code_fe_name));
                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                        $xml_cac_CreditNoteLine->appendChild($xml_cac_TaxTotal);

                        $xml_cac_Item = $xml->createElement("cac:Item");
                            $xml_cac_Item->appendChild($xml->createElement("cbc:Description", htmlspecialchars($item->product_name)));

                            $xml_cac_StandardItemIdentification = $xml->createElement("cac:StandardItemIdentification");
                                $xml_cbc_ID = $xml->createElement("cbc:ID", $item->product_code);
                                    $xml_cbc_ID->setAttribute("schemeID", "999");
                                    $xml_cbc_ID->setAttribute("schemeName", "Estándar de adopción del contribuyente");
                                $xml_cac_StandardItemIdentification->appendChild($xml_cbc_ID);
                            $xml_cac_Item->appendChild($xml_cac_StandardItemIdentification);
                        $xml_cac_CreditNoteLine->appendChild($xml_cac_Item);

                        $xml_cac_Price = $xml->createElement("cac:Price");
                            $priceAmount = $item->net_unit_cost;
                            $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal(abs($priceAmount), NUMBER_DECIMALS));
                                $xml_cbc_PriceAmount->setAttribute("currencyID", (is_null($purchase->purchase_currency) ? "COP" : $purchase->purchase_currency));
                            $xml_cac_Price->appendChild($xml_cbc_PriceAmount);
                            $xml_cbc_BaseQuantity = $xml->createElement("cbc:BaseQuantity", $this->sma->formatDecimal(abs($item->quantity), NUMBER_DECIMALS));
                                $xml_cbc_BaseQuantity->setAttribute("unitCode", "NIU");
                            $xml_cac_Price->appendChild($xml_cbc_BaseQuantity);
                            $xml_cac_CreditNoteLine->appendChild($xml_cac_Price);
                    $xml_creditNote->appendChild($xml_cac_CreditNoteLine);

                    $consecutiveProductIdentifier++;
                }
            /**************************************************************/

            /************************* DATA *******************************/
                // if ($this->Settings->fe_technology_provider == CADENA) {
                    $xml_DATA = $xml->createElement("DATA");
                    $xml_DATA->appendChild($xml->createElement("UBL21", "true"));

                    $xml_Partnership = $xml->createElement("Partnership");
                    $xml_Partnership->appendChild($xml->createElement("ID", "901090070"));
                    $xml_Partnership->appendChild($xml->createElement("TechKey"/*, $resolution->clave_tecnica*/));
                    if ($this->Settings->fe_work_environment == TEST) {
                        $xml_Partnership->appendChild($xml->createElement("SetTestID"/*, $resolution->fe_testid*/));
                    }
                    $xml_DATA->appendChild($xml_Partnership);
                    $xml_creditNote->appendChild($xml_DATA);
                // }
            /**************************************************************/

            $xml->appendChild($xml_creditNote);

            if (!$showXml) {
                return base64_encode($xml->saveXML());
            }

            echo $xml->saveXML();
        }

        private function generateCUDS($purchase, $purchaseItems, $supplier, $customer, $resolution, $technologyProvider, $decrypt = FALSE)
        {
            $pin = $technologyProvider->software_pin;
            $ivaValue = $subtotal = $totalTax = 0.00;
            $workEnvironment = $this->Settings->fe_work_environment;

            foreach ($purchaseItems as $item) {
                $rate = $item->rate;
                $codeFe = $item->code_fe;
                $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                $netUnitCost = $this->sma->formatDecimal($item->net_unit_cost, NUMBER_DECIMALS);

                $subtotal += $this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS);
                $totalTax += $this->sma->formatDecimal(($this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS) * $rate) / 100, NUMBER_DECIMALS);

                if ($codeFe == "01") {
                    $ivaValue += $this->sma->formatDecimal(($this->sma->formatDecimal($netUnitCost * $quantity, NUMBER_DECIMALS) * $rate) / 100, NUMBER_DECIMALS);
                }
            }

            $iva = $this->sma->formatDecimal(abs($ivaValue), NUMBER_DECIMALS);
            $subtotal = $this->sma->formatDecimal(abs($subtotal), NUMBER_DECIMALS);
            $total = ($this->sma->formatDecimal(abs($subtotal), NUMBER_DECIMALS) + $this->sma->formatDecimal(abs($totalTax), NUMBER_DECIMALS) + $this->sma->formatDecimal($purchase->shipping, NUMBER_DECIMALS) - $this->sma->formatDecimal($purchase->order_discount, NUMBER_DECIMALS));

            if ($decrypt == FALSE) {
                $CUDS = hash(
                    'sha384',
                    str_replace('-', '', $purchase->reference_no) .
                        $this->formatDate($purchase->date)->date .
                        $this->formatDate($purchase->date)->time .
                        $this->sma->formatDecimalNoRound($subtotal, NUMBER_DECIMALS) .
                        "01" .
                        $this->sma->formatDecimalNoRound($iva, NUMBER_DECIMALS) .
                        $this->sma->formatDecimalNoRound($total, NUMBER_DECIMALS) .
                        str_replace("-", "", $supplier->vat_no) .
                        str_replace('-', '', $customer->numero_documento) .
                        $pin .
                        $workEnvironment
                );
            } else {
                $CUDS =
                    str_replace('-', '', $purchase->reference_no) . " " .
                    $this->formatDate($purchase->date)->date . " " .
                    $this->formatDate($purchase->date)->time . " " .
                    $this->sma->formatDecimalNoRound($subtotal, NUMBER_DECIMALS) . " " .
                    "01" . " " .
                    $this->sma->formatDecimalNoRound($iva, NUMBER_DECIMALS) . " " .
                    $this->sma->formatDecimalNoRound($total, NUMBER_DECIMALS) . " " .
                    str_replace("-", "", $supplier->vat_no) . " " .
                    str_replace('-', '', $customer->numero_documento) . " " .
                    $pin . " " .
                    $workEnvironment;
            }

            return $CUDS;
        }

        public function consumeWebService($xml_file)
        {
            $technologyProviderConfiguration = $this->getTechnologyProviderConfiguration(CADENA, $this->Settings->fe_work_environment);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $technologyProviderConfiguration->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "\"$xml_file\"",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: text/plain",
                    "efacturaAuthorizationToken: $technologyProviderConfiguration->security_token"
                ),
            ));

            $response = curl_exec($curl);
            $error = curl_error($curl);

            curl_close($curl);

            log_message('debug', 'WS - documento soporte: '. $response);

            return $response;
        }

        private function manageResponse($response, $purchaseId)
        {
            $purchase = $this->site->getPurchaseByID($purchaseId);
            $response = json_decode($response);

            if (!isset($response->statusCode)) {
                $this->session->set_flashdata('error', $response->message);
                admin_redirect("purchases/supporting_document_index");
            }

            if ($response->statusCode == 200) {
                $data['acceptedDian'] = ACCEPTED;
                $data['statusCode'] = $response->statusCode;
                $data['trackId'] = $response->trackId;
                $data['uuid'] = $response->uuid;
                $data['statusMessage'] = $response->statusMessage;
                $data['statusDescription'] = $response->statusDescription;
                $data["errorMessage"] = implode('; ', $response->warnings);
                $data["document"] = $response->document;
                $data["qrCode"] = $this->createQRCode($purchase, $response->uuid);
                $this->updatePurchaseById($data, $purchaseId);
                $this->site->saveHits($purchaseId, SUPPORTING_DOCUMENT, ($response->statusMessage . ' ' . $response->statusDescription), SUCCESS, $purchase->date);

                $this->session->set_flashdata('message', $response->statusMessage . ' ' . $response->statusDescription);
                admin_redirect("purchases/supporting_document_index");
            }

            if ($response->statusCode == 400) {
                $data['acceptedDian'] = PENDING;
                $data['errorMessage'] = $response->errorMessage;
                $data['errorReason'] = $response->errorReason;
                $this->updatePurchaseById($data, $purchaseId);
                $this->site->saveHits($purchaseId, SUPPORTING_DOCUMENT, ($response->errorMessage . ' ' . $response->errorReason), ERROR, $purchase->date);

                $this->session->set_flashdata('error', $response->errorMessage . ' ' . $response->errorReason);
                admin_redirect("purchases/supporting_document_index");
            }

            if ($response->statusCode == 401) {
                $data['acceptedDian'] = PENDING;
                $data['statusCode'] = $response->statusCode;
                $data['errorMessage'] = $response->errorMessage;
                $data["errorReason"] = $response->errorReason;
                $this->updatePurchaseById($data, $purchaseId);
                $this->site->saveHits($purchaseId, SUPPORTING_DOCUMENT, ($response->errorMessage . ' ' . $response->errorReason), ERROR, $purchase->date);

                $this->session->set_flashdata('error', $response->errorMessage . ' ' . $response->errorReason);
                admin_redirect("purchases/supporting_document_index");
            }

            if ($response->statusCode == 409) {
                $data['acceptedDian'] = PENDING;
                $data['statusCode'] = $response->statusCode;
                $data['trackId'] = $response->trackId;
                $data['uuid'] = $response->uuid;
                $data['errorMessage'] = $response->errorMessage;
                $data['errorReason'] = implode('; ', $response->errorReason);
                $this->updatePurchaseById($data, $purchaseId);
                $this->site->saveHits($purchaseId, SUPPORTING_DOCUMENT, ($response->errorMessage . ' ' . $data['errorReason']), ERROR, $purchase->date);

                $this->session->set_flashdata('error', $response->errorMessage . ' ' . $data['errorReason']);
                admin_redirect("purchases/supporting_document_index");
            }

            if ($response->statusCode == 502) {
                $data['acceptedDian'] = PENDING;
                $data['statusCode'] = $response->statusCode;
                $data['errorMessage'] = $response->errorMessage;
                $data["errorReason"] = $response->errorReason;
                $this->updatePurchaseById($data, $purchaseId);
                $this->site->saveHits($purchaseId, SUPPORTING_DOCUMENT, ($response->errorMessage . ' ' . $response->errorReason), ERROR, $purchase->date);

                $this->session->set_flashdata('error', $response->errorMessage . ' ' . $response->errorReason);
                admin_redirect("purchases/supporting_document_index");
            }

            if ($response->statusCode == 503) {
                $data['acceptedDian'] = PENDING;
                $data['statusCode'] = $response->statusCode;
                $data['errorMessage'] = $response->errorMessage;
                $data["errorReason"] = $response->errorReason;
                $this->updatePurchaseById($data, $purchaseId);
                $this->site->saveHits($purchaseId, SUPPORTING_DOCUMENT, ($response->errorMessage . ' ' . $response->errorReason), ERROR, $purchase->date);

                $this->session->set_flashdata('error', $response->errorMessage . ' ' . $response->errorReason);
                admin_redirect("purchases/supporting_document_index");
            }

            if ($response->statusCode == 504) {
                $data['acceptedDian'] = PENDING;
                $data['statusCode'] = $response->statusCode;
                $data['errorMessage'] = $response->errorMessage;
                $data["errorReason"] = $response->errorReason;
                $this->updatePurchaseById($data, $purchaseId);
                $this->site->saveHits($purchaseId, SUPPORTING_DOCUMENT, ($response->errorMessage . ' ' . $response->errorReason), ERROR, $purchase->date);

                $this->session->set_flashdata('error', $response->errorMessage . ' ' . $response->errorReason);
                admin_redirect("purchases/supporting_document_index");
            }
        }

        public function cadenaWsCheckStatus($documentId)
        {
            $purchase = $this->site->getPurchaseByID($documentId);
            $supplier = $this->site->getCompanyByID($purchase->supplier_id);

            $curl = curl_init();
            $issuerNit = $supplier->vat_no;
            $reference = str_replace("-", "", $purchase->reference_no);
            $documentTypeCode = "05";
            $prefix = explode("-", $purchase->reference_no)[0];
            $receiptNit = $this->Settings->numero_documento;

            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/documentos?nit_emisor='.$issuerNit.'&id_documento='.$reference.'&codigo_tipo_documento='.$documentTypeCode./* '&prefijo='.$prefix. */'&nit_receptor='.$receiptNit,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => [
                    'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                    'Content-Type: text/plain',
                    'Partnership-Id: 901090070'
                ],
            ]);

            $response = curl_exec($curl);
            log_message('debug', 'WS - consulta documento soporte: '. $response);
            curl_close($curl);

            return json_decode($response);

        }

        public function manageResponseCheckStatus($response, $documentId)
        {
            if ($response->statusCode == 200) {
                $data = [
                    "statusCode"        => $response->statusCode,
                    "trackId"           => $response->trackId,
                    "uuid"              => $response->uuid,
                    "statusMessage"     => $response->statusMessage,
                    "statusDescription" => $response->statusDescription,
                    "document"          => $response->document,
                    "acceptedDian"      => ACCEPTED
                ];
                if ($this->updatePurchaseById($data, $documentId)) {
                    $this->session->set_flashdata('message', $response->statusMessage . ' ' . $response->statusDescription);
                    admin_redirect("purchases/supporting_document_index");
                } else {
                    $this->session->set_flashdata('error', "No fue posible actualizar los datos consultados para la compra");
                admin_redirect("purchases/supporting_document_index");
                }

            }

            if ($response->statusCode == 400 || $response->statusCode == 401 || $response->statusCode == 404) {
                $this->session->set_flashdata('error', $response->errorMessage . ' ' . $response->errorReason);
                admin_redirect("purchases/supporting_document_index");
            }
        }
    /**************************************************************************/

    /********************************** BPM ***********************************/
        public function bpmBuildJsonSupportDocument($documentId, $toShow)
        {
            $document = $this->site->getPurchaseByID($documentId);
            // $supplier = $this->site->getCompanyByID($document->supplier_id);

            $json =  [
                "TipoDocumento"     => $this->getDocumentTypeCode($document),
                "TipoConcepto"      => $this->getConceptType($document),
                "TipoOperacion"     => "10",
                "TipoNegociacion"   => $this->getPaymentMethod($document),
                "MedioPago"         => $this->getPaymentMean($document),
                "FechaExpedicion"   => $document->date,
                "FechaVencimiento"  => $this->getDueDate($document),
                "TotalImpuestos"    => floatval($this->getTotals($document, 'totalTax')),
                "TotalRetenciones"  => floatval($this->getTotalWithholdings($document)),
                "Retenciones"       => $this->getWithholdings($document),
                "InfoAdicional"     => $document->note,
                "Vendedor"          => $this->getAcquirer($document),
                "Detalle"           => $this->getDetails($document)
            ];

            $json = $this->getTrm($json, $document);
            $json = $this->getCharge($json, $document);
            $json = $this->getDiscount($json, $document);
            $json = $this->getAdvances($json, $document);
            // $json = $this->getResolution($json, $document);

            if (!empty($document->fe_generatedDocument)) {
                $json["DocumentoEditar"] = $document->fe_generatedDocument;
            }

            return json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        private function getDocumentTypeCode($document)
        {
            $typeElectronicDocument = $this->site->getTypeElectronicDocument($document->id, DOCUMENTO_SOPORTE);

            if ($typeElectronicDocument == DOCUMENT_SUPPORT) {
                $typeDocument = 7;
            }
            return $typeDocument;
        }

        private function getConceptType($document)
        {
            $conceptType = "";
            $typeElectronicDocument = $this->site->getTypeElectronicDocument($document->id, DOCUMENTO_SOPORTE);

            // if ($typeElectronicDocument == CREDIT_NOTE) {
            //     if (!empty($document->fe_debit_credit_note_concept_dian_code)) {
            //         $conceptType = $document->fe_debit_credit_note_concept_dian_code;
            //     } else {
            //         if (abs($referenceDocument->grand_total) == abs($document->grand_total)) {
            //             $conceptType = 2;
            //         } else {
            //             $conceptType = 1;
            //             if (!empty($document->fe_debit_credit_note_concept_dian_code)) {
            //                 $conceptType = $document->fe_debit_credit_note_concept_dian_code;
            //             }
            //         }
            //     }
            // } else if ($typeElectronicDocument == DEBIT_NOTE) {
            //     if (!empty($document->fe_debit_credit_note_concept_dian_code)) {
            //         $conceptType = $document->fe_debit_credit_note_concept_dian_code;
            //     }
            // }

            return $conceptType;
        }

        private function getPaymentMethod($document)
        {
            $paymentAmount = 0;
            $payments = $this->site->getPurchasePayments($document->id);

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    $paymentAmount += $payment->amount;
                }
            }

            return ($document->grand_total == $paymentAmount) ? CASH : CREDIT;
        }

        private function getPaymentMean($document)
        {
            $paymentsWithoutWithholdings = [];
            $payments = $this->site->getPurchasePayments($document->id);

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if ($payment->paid_by != 'retencion') {
                        $paymentsWithoutWithholdings[] = $payment;
                    }
                }
            }

            if (!empty($paymentsWithoutWithholdings)) {
                foreach ($paymentsWithoutWithholdings as $payment) {
                    return $payment->mean_payment_code_fe;
                }
            } else {
                return "ZZZ";
            }
        }

        private function getDueDate($document)
        {
            $hours = date("H:i:s", strtotime($document->date));
            $paymentMethod = $this->getPaymentMethod($document);

            return ($paymentMethod == CREDIT) ? $document->due_date ." ". $hours : $document->date;
        }

        private function getTotals($document, $name)
        {
            $subtotal = 0;
            $totalTax = 0;
            $items = $this->site->getAllSaleItems($document->id);

            foreach ($items as $item) {
                $subtotal += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                $totalTax += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
            }

            return $this->sma->formatDecimal(abs($$name), NUMBER_DECIMALS);
        }

        private function getTotalWithholdings($document)
        {
            $withHoldings = $document->rete_fuente_total + $document->rete_iva_total + $document->rete_ica_total;
            return $this->sma->formatDecimal($withHoldings, NUMBER_DECIMALS);
        }

        private function getWithholdings($document)
        {
            $withHoldings = [];

            if (!empty(floatval($document->rete_iva_total))) {
                $withHoldings[] = [
                    "Codigo"=> "05",
                    "Valor" => floatval($document->rete_iva_percentage),
                    "Total" => floatval($document->rete_iva_total)
                ];
            }

            if (!empty(floatval($document->rete_fuente_total))) {
                $withHoldings[] = [
                    "Codigo"=> "06",
                    "Valor" => floatval($document->rete_fuente_percentage),
                    "Total" => floatval($document->rete_fuente_total)
                ];
            }

            if (!empty(floatval($document->rete_ica_total))) {
                $withHoldings[] = [
                    "Codigo"=> "07",
                    "Valor" => floatval($document->rete_ica_percentage),
                    "Total" => floatval($document->rete_ica_total)
                ];
            }

            return $withHoldings;
        }

        private function getAcquirer($document)
        {
            $supplier = $this->site->getCompanyByID($document->supplier_id);
            $typeRegimen = $this->site->get_types_vat_regime($supplier->tipo_regimen);
            $typesObligations = $this->site->getTypesCustomerObligations($supplier->id, ACQUIRER);
            $obligations = $this->getObligations($typesObligations);

            $acquirer = [
                "TipoIdentificacion"=> $supplier->document_code,
                "Ciudad"            => substr($supplier->city_code, -5),
                "TipoOrganizacion"  => $supplier->tipo_regimen,
                "TipoRegimen"       => $typeRegimen->codigo,
                "ResponsabilidadesFiscales" => $obligations,
                "Identificacion"    => $supplier->vat_no,
                "Nombre"            => htmlspecialchars($supplier->name),
                "Apellido"          => "",
                "CorreoElectronico" => "",
                "Telefono"          => $supplier->phone,
                "Direccion"         => $supplier->address
            ];

            if ($document->purchase_currency == "COP") {
                $acquirer["Pais"] = $supplier->codigo_iso;
                $acquirer["NombreEstado"] = ucfirst(mb_strtolower($supplier->state));
                $acquirer["NombreCiudad"] = ucfirst(mb_strtolower($supplier->city));
            }

            return $acquirer;
        }

        private function getObligations($typesObligations)
        {
            $obligations = [];
            foreach ($typesObligations as $typesObligation) {
                $obligations[]['codigo'] = $typesObligation->types_obligations_id;
            }
            return $obligations;
        }

        private function getDetails($document)
        {
            $items = $this->site->getAllPurchaseItems($document->id);
            $details = [];
            foreach ($items as $item) {
                $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                $netUnitCost = $this->sma->formatDecimal($item->net_unit_cost, NUMBER_DECIMALS);
                $subtotal = $this->sma->formatDecimal($quantity * $netUnitCost, NUMBER_DECIMALS);
                $totalTax = $this->sma->formatDecimal(($subtotal * $item->rate) / 100, NUMBER_DECIMALS);

                $details[] = [
                    "Codigo"            => $item->product_code,
                    "Descripcion"       => htmlspecialchars(str_replace('"', '', $item->product_name)),
                    "Cantidad"          => floatval(abs($quantity)),
                    "Valor"             => floatval(abs($netUnitCost)),
                    "TotalImpuestos"    => floatval(abs($totalTax)),
                    "TotalRetenciones"  => 0,
                    "Total"             => floatval(abs($this->sma->formatDecimal(($subtotal + $totalTax), NUMBER_DECIMALS))),
                    "Impuestos"         => [
                        [
                            "Codigo"    => $item->code_fe,
                            "Valor"     => floatval($this->sma->formatDecimal($item->rate, NUMBER_DECIMALS)),
                            "Total"     => floatval(abs($totalTax))
                        ]
                    ]
                ];
            }
            return $details;
        }

        private function getTrm($json, $document)
        {
            if ($document->purchase_currency != "COP") {
                $json["TasaMoneda"] = $document->purchase_currency;
                $json["TasaValor"] = $document->purchase_currency_trm;
                $json["TasaFecha"] = $document->date;
            }

            return $json;
        }

        private function getCharge($json, $document)
        {
            $chargePercentage = 0;

            if ($document->shipping > 0) {
                $chargePercentage = ($document->shipping * 100) / ($this->getTotals($document, "subtotal") + $this->getTotals($document, "totalTax"));

                $json["RecargoGeneralPorcentaje"] = floatval($this->sma->formatDecimal($chargePercentage), NUMBER_DECIMALS);
                $json["RecargoGeneralMotivo"] = "Recargo genral factura";
            }

            return $json;
        }

        private function getDiscount($json, $document)
        {
            if ($document->order_discount > 0) {
                $json["DescuentoGeneralValor"] = floatval($this->sma->formatDecimal($document->order_discount_id, NUMBER_DECIMALS));
                $json["DescuentoGeneralMotivo"] = "Descuento general factura";
            }

            return $json;
        }

        private function getAdvances($json, $document)
        {
            $payments = $this->site->getPurchasePayments($document->id);

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if ($payment->paid_by == 'deposit') {
                        $json["Anticipos"][] = [
                            "FechaHora" => $payment->date,
                            "Valor" => $this->sma->formatDecimal(abs($payment->amount), NUMBER_DECIMALS),
                            "Comentario" => $payment->note,
                        ];
                    }
                }
            }

            return $json;
        }

        private function getResolution($json, $document)
        {
            $typeElectronicDocument = $this->site->getTypeElectronicDocument($document->id, DOCUMENTO_SOPORTE);

            if ($this->Settings->fe_work_environment == TEST) {
                $resolutions = $this->bpmWsGetResolutions($typeElectronicDocument);

                if ($resolutions->Data->Summary->Success = YES) {
                    $resolutionsData = $resolutions->Data->DataResult;
                    $this->amountResolutions = count($resolutionsData);

                    foreach ($resolutionsData as $resolutionData) {
                        if ($typeElectronicDocument == INVOICE) {
                            $documentoRelacionado = $resolutionData->Prefijo.($resolutionData->UltimoConsecutivoGenerado + 1);
                        } else if ($typeElectronicDocument == CREDIT_NOTE) {
                            $externalInvoiceReference = $document->external_invoice_reference;
                            if (!empty($externalInvoiceReference)) {
                                $documentoRelacionado = $externalInvoiceReference;
                            } else {
                                $referenceDocument = $this->site->getSaleByID($document->sale_id);
                                $documentoRelacionado = $referenceDocument->fe_generatedDocument;
                            }

                        } else if ($typeElectronicDocument == DEBIT_NOTE) {
                            $referenceDocument = $this->site->getSaleByID($document->reference_invoice_id);
                            $documentoRelacionado = $referenceDocument->fe_generatedDocument;
                        }

                        $documentoConsecutivo = ($resolutionData->UltimoConsecutivoGenerado + 1);
                    }
                }
            } else {
                if ($typeElectronicDocument == INVOICE) {
                    $documentoRelacionado = str_replace("-", "", $document->reference_no);
                } else if ($typeElectronicDocument == CREDIT_NOTE) {
                    $referenceDocument = $this->site->getSaleByID($document->sale_id);
                    $documentoRelacionado = $referenceDocument->fe_generatedDocument;
                } else if ($typeElectronicDocument == DEBIT_NOTE) {
                    $referenceDocument = $this->site->getSaleByID($document->reference_invoice_id);
                    $documentoRelacionado = $referenceDocument->fe_generatedDocument;
                }
                $documentoConsecutivo = explode("-", $document->reference_no)[1];

                if ($this->amountResolutions > 1) {
                    $resolutionWappsi = $this->site->getDocumentTypeById($document->document_type_id);
                    $json["Resolucion"]["Numero"]= $resolutionWappsi->num_resolucion;
                    $json["Resolucion"]["Prefijo"]= $resolutionWappsi->sales_prefix;
                }
            }

            $json["DocumentoRelacionado"] = $documentoRelacionado;
            $json["DocumentoConsecutivo"] = $documentoConsecutivo;

            return $json;
        }

        public function bpmWsGetResolutions($typeElectronicDocument)
        {
            $technologyProviderConfiguration = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

            if ($typeElectronicDocument == INVOICE) {
                $tipoDocumento = 'FA';
            } else if ($typeElectronicDocument == DEBIT_NOTE) {
                $tipoDocumento = 'ND';
            } else if ($typeElectronicDocument == CREDIT_NOTE) {
                $tipoDocumento = 'NC';
            } else if ($typeElectronicDocument == DOCUMENT_SUPPORT) {
                $tipoDocumento = 'NC';
            }

            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => $technologyProviderConfiguration->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "Controller": "Documentos",
                    "Action" : "GetNumeracion",
                    "ParamValues" : {
                        "TokenIdentificador": "'. $technologyProviderConfiguration->security_token .'",
                        "TipoDocumento": "'. $tipoDocumento .'"
                    }
                }',
                CURLOPT_HTTPHEADER => [ 'Content-Type: application/json' ],
            ]);

            $response = json_decode(curl_exec($curl));

            curl_close($curl);

            return (object) $response;
        }

        public function bpmWSSendDocument($json)
        {
            $technologyProvider = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => $technologyProvider->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
                    "Controller"    : "Facturas",
                    "Action"        : "SendDocument",
                    "ParamValues"   : {
                        "TokenIdentificador": "'.$technologyProvider->security_token.'",
                        "DocumentoObject"   : '.$json.',
                        "ObtenerXml"        : true
                    }
                }',
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            ]);

            $response = curl_exec($curl);
            curl_close($curl);

            log_message('debug', 'FE - BPM envío documento: '. $response);

            return $response;
        }

        public function manageSendResponse($response, $eDocument)
        {
            $document = $this->site->getSaleByID($eDocument->sale_id);

            $summary = $response->Data->Summary;

            if ($summary->Success == YES) {
                $dataResult = $response->Data->DataResult[0];
                if ($dataResult->DocumentoEstadoDianIdentificador == 7200002) {
                    $data = [
                        "acceptedDian"  => ACCEPTED,
                        "uuid"          => $dataResult->DocumentoCufe,
                        "qrCode"        => $dataResult->DocumentoQr,
                        "statusMessage"    => "Documento aceptado por la DIAN",
                        "statusDescription" => "La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada.",
                        "fe_generatedDocument"  => $dataResult->DocumentoGenerado
                    ];

                    if ($this->site->updateSale($data, $document->id)) {
                        $response = [
                            'response'  => TRUE,
                            'message'   => "Documento aceptado por la DIAN. La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada."
                        ];
                    } else {
                        $response = [
                            'response'  => TRUE,
                            'message'   => "Documento aceptado por la DIAN. La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada. Pero no pudo ser actualizado en Wappsi. Por favor consulta el estado del documento."
                        ];
                    }

                    $this->site->saveHits($document->id, ELECTRONIC_BILL, $data["fe_mensaje"], SUCCESS, $document->date);
                }
            } else {
                $this->site->updateSale(["fe_mensaje_soporte_tecnico" => $summary->Message], $document->id);
                $this->site->saveHits($document->id, ELECTRONIC_BILL, "", ERROR, $document->date);

                $response = [
                    'response' => FALSE,
                    'message' => str_replace("'", "", $summary->Message)
                ];
            }

            return (object) $response;
        }
    /**************************************************************************/

    private function formatDate($fullDate)
    {
        $date = explode(' ', $fullDate);
        $arrayDate = ['date' => $date[0]];
        if (isset($date[1])) {
            $arrayDate["time"] = $date[1] . "-05:00";
        }
        return (object) $arrayDate;
    }

    private function clearDocumentNumber($documentNumber)
    {
        $documentNumber = str_replace('.', '', $documentNumber);
        $documentNumber = explode('-', $documentNumber);
        return $documentNumber[0];
    }

    private function getTaxRateByCodeFe()
    {
        $this->db->select("code_fe, rate");
        $this->db->order_by("code_fe", "asc");
        $this->db->order_by("rate", "asc");
        $response = $this->db->get("tax_rates");

        return $response->result();
    }

    public function createQRCode($purchase, $uuId = NULL)
    {
        $supplier = $this->site->getCompanyByID($purchase->supplier_id);

        $purchaseItems = $this->site->getAllPurchaseItems($purchase->id);
        $purchase = $this->site->getPurchaseByID($purchase->id);
        $customer = $this->site->get_setting();
        $uuid = (!empty($purchase->uuid)) ? $purchase->uuid : $uuId;


        $subtotal = 0;
        foreach ($purchaseItems as $item) {
            $subtotal += $this->sma->formatDecimal($item->net_unit_cost * $item->quantity, NUMBER_DECIMALS);
        }

        $subtotal = $subtotal;

        $codeQR = "NumDS: " . str_replace('-', '', $purchase->reference_no);
        $codeQR .= " FecDS: " . $this->formatDate($purchase->date)->date;
        $codeQR .= " HorDS: " . $this->formatDate($purchase->date)->time;
        $codeQR .= " NumSNO: " . $this->clearDocumentNumber($supplier->vat_no);
        $codeQR .= " DocABS: " . $this->clearDocumentNumber($customer->numero_documento);
        $codeQR .= " ValDS: " . $subtotal;
        $codeQR .= " ValIva: " . 0;
        $codeQR .= " ValTolDS: " . $subtotal;
        $codeQR .= " CUDS: " . $uuid;
        $codeQR .= " QRCode: https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=". $uuid;

        return $codeQR;
    }

    public function getTechnologyProviderConfiguration($technologyProviderId, $workEnvironment)
    {
        $this->db->where("technology_provider_id", $technologyProviderId)
            ->where("work_enviroment", $workEnvironment);
        $response = $this->db->get("technology_provider_configuration");
        return $response->row();
    }

    public function updatePurchaseById($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("purchases", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function sendEmail($documentId)
    {
        $purchase = $this->site->getPurchaseByID($documentId);
        $biller = $this->site->getCompanyByID($purchase->biller_id);
        $supplier = $this->site->getCompanyByID($purchase->supplier_id);

        $template = file_get_contents($this->theme_path . 'purchases/templates/supportDocument.php');
        $data = [
            'supplierName'      => $purchase->supplier,
            'receiverName'      => $this->Settings->razon_social,
            'createdDate'  => $purchase->date,
            'logo'              => base_url("assets/uploads/logos/". $biller->logo),
            'reference' => $purchase->reference_no
        ];
        $body = $this->parser->parse_string($template, $data);
        $subject = 'Envio de documento de soporte';

        $emailSent = $this->sma->send_email($supplier->email, $subject, $body/*, null, null, $attachment*/);
        if ($emailSent) {
            $this->session->set_flashdata('message', $this->lang->line("email_sent"));
            admin_redirect("purchases/supporting_document_index");
        }

        $this->session->set_flashdata('error', $this->lang->line('email_not_sent'));
        admin_redirect("purchases/supporting_document_index");

    }
}

/* End of file SupportDocument_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/SupportDocument_model.php */