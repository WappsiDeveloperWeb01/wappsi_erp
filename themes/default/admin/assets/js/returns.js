$(document).ready(function (e) {
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }

// $('#add_return, #edit_return').attr('disabled', true);
$(document).on('change', '.rserial', function () {
    var item_id = $(this).closest('tr').attr('data-item-id');
    reitems[item_id].row.serial = $(this).val();
    localStorage.setItem('reitems', JSON.stringify(reitems));
});

// If there is any item in localStorage
if (localStorage.getItem('reitems')) {
    loadItems();
}

$('#reset').click(function (e) {
    bootbox.confirm(lang.r_u_sure, function (result) {
        if (result) {
            if (localStorage.getItem('reitems')) {
                localStorage.removeItem('reitems');
            }
            if (localStorage.getItem('rediscount')) {
                localStorage.removeItem('rediscount');
            }
            if (localStorage.getItem('retax2')) {
                localStorage.removeItem('retax2');
            }
            if (localStorage.getItem('reref')) {
                localStorage.removeItem('reref');
            }
            if (localStorage.getItem('rewarehouse')) {
                localStorage.removeItem('rewarehouse');
            }
            if (localStorage.getItem('renote')) {
                localStorage.removeItem('renote');
            }
            if (localStorage.getItem('reinnote')) {
                localStorage.removeItem('reinnote');
            }
            if (localStorage.getItem('recustomer')) {
                localStorage.removeItem('recustomer');
            }
            if (localStorage.getItem('redate')) {
                localStorage.removeItem('redate');
            }
            if (localStorage.getItem('rebiller')) {
                localStorage.removeItem('rebiller');
            }
            if (localStorage.getItem('resale')) {
                localStorage.removeItem('resale');
            }
            if (localStorage.getItem('reyear_database')) {
                localStorage.removeItem('reyear_database');
            }
            $('#modal-loading').show();
            location.reload();
        }
    });
});

$('#redate').change(function (e) {
    localStorage.setItem('redate', $(this).val());
});
if (redate = localStorage.getItem('redate')) {
    $('#redate').val(redate);
}

$('#reref').change(function (e) {
    localStorage.setItem('reref', $(this).val());
});
$('#reyear_database').change(function (e) {
    localStorage.setItem('reyear_database', $(this).val());
});
if (reref = localStorage.getItem('reref')) {
    $('#reref').val(reref);
}
if (reyear_database = localStorage.getItem('reyear_database')) {
    $('#reyear_database').select2('val', reyear_database);
}
if (resale = localStorage.getItem('resale')) {
    $('#resale').val(resale);
    setTimeout(function() {
        $('.search_sale').trigger('click');
    }, 850);
}

$('#rebiller').change(function (e) {
    localStorage.setItem('rebiller', $(this).val());
    if (odt_is_pos == 1) {
        document_type = 3;
    } else if (odt_is_pos == 0) {
        document_type = 4;
    }
    console.log(" >> odt_is_pos "+odt_is_pos);
    $.ajax({
      url:site.base_url+'billers/getBillersDocumentTypes/'+document_type+'/'+$('#rebiller').val(),
      type:'get',
      dataType:'JSON'
    }).done(function(data){
        response = data;
        $('#document_type_id').html(response.options).select2();
        if (response.status == 0) {
            $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            localStorage.setItem('locked_for_biller_resolution', 1);
            // verify_locked_submit();
        } else {
            if (localStorage.getItem('locked_for_biller_resolution')) {
                localStorage.removeItem('locked_for_biller_resolution');
            }
            // verify_locked_submit();
        }
        $('#document_type_id').trigger('change');
    });
    $.ajax({
        url:site.base_url+'billers/getBillersDocumentTypes/31/'+$('#rebiller').val(),
        type:'get',
        dataType:'JSON'
    }).done(function(data){
        response = data;
        $('#payment_reference_no').html(response.options).select2();
        $('#payment_reference_no').trigger('change');
    });
});
if (rebiller = localStorage.getItem('rebiller')) {
    $('#rebiller').val(rebiller);
}

$('#rewarehouse').change(function (e) {
    localStorage.setItem('rewarehouse', $(this).val());
});
if (rewarehouse = localStorage.getItem('rewarehouse')) {
    $('#rewarehouse').select2("val", rewarehouse);
}

$('#recustomer').change(function (e) {
    localStorage.setItem('recustomer', $(this).val());
});
if (recustomer = localStorage.getItem('recustomer')) {
    setCustomer(recustomer);
}


$('#retax2').change(function (e) {
    localStorage.setItem('retax2', $(this).val());
    $('#retax2').val($(this).val());
});
if (retax2 = localStorage.getItem('retax2')) {
    $('#retax2').select2("val", retax2);
}

$('#rediscount').change(function (e) {
    localStorage.setItem('rediscount', $(this).val());
});
if (rediscount = localStorage.getItem('rediscount')) {
    $('#rediscount').val(rediscount);
}

$('#renote').redactor('destroy');
$('#renote').redactor({
    buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
    formattingTags: ['p', 'pre', 'h3', 'h4'],
    minHeight: 100,
    changeCallback: function (e) {
        var v = this.get();
        localStorage.setItem('renote', v);
    }
});
if (renote = localStorage.getItem('renote')) {
    $('#renote').redactor('set', renote);
}

$('#reinnote').redactor('destroy');
$('#reinnote').redactor({
    buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
    formattingTags: ['p', 'pre', 'h3', 'h4'],
    minHeight: 100,
    changeCallback: function (e) {
        var v = this.get();
        localStorage.setItem('reinnote', v);
    }
});
if (reinnote = localStorage.getItem('reinnote')) {
    $('#reinnote').redactor('set', reinnote);
}

$('body').bind('keypress', function (e) {
    if ($(e.target).hasClass('redactor_editor')) {
        return true;
    }
    if (e.keyCode == 13) {
        e.preventDefault();
        return false;
    }
});

if (site.settings.tax2 != 0) {
    $('#retax2').change(function () {
        localStorage.setItem('retax2', $(this).val());
        loadItems();
        return;
    });
}

var old_rediscount;
$('#rediscount').focus(function () {
    old_rediscount = $(this).val();
}).change(function () {
    var new_discount = $(this).val() ? $(this).val() : '0';
    if (is_valid_discount(new_discount)) {
        localStorage.removeItem('rediscount');
        localStorage.setItem('rediscount', new_discount);
        loadItems();
        return;
    } else {
        $(this).val(old_rediscount);
        bootbox.alert(lang.unexpected_value);
        return;
    }

});

$(document).on('click', '.redel', function () {
    var row = $(this).closest('tr');
    var item_id = row.attr('data-item-id');
    delete reitems[item_id];
    row.remove();
    if(reitems.hasOwnProperty(item_id)) { } else {
        localStorage.setItem('reitems', JSON.stringify(reitems));
        loadItems();
        return;
    }
});

$(document).on('click', '.edit', function () {
    var row = $(this).closest('tr');
    var row_id = row.attr('id');
    item_id = row.attr('data-item-id');
    item = reitems[item_id];
    var qty = row.children().children('.rquantity').val(),
    product_option = row.children().children('.roption').val(),
    unit_price = formatDecimal(row.children().children('.ruprice').val()),
    discount = row.children().children('.rdiscount').val();
    if(item.options !== false) {
        $.each(item.options, function () {
            if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                unit_price = parseFloat(item.row.real_unit_price)+parseFloat(this.price);
            }
        });
    }
    var real_unit_price = item.row.real_unit_price;
    var net_price = unit_price;
    $('#prModalLabel').text(item.row.name + ' (' + item.row.code + ')');
    if (site.settings.tax1) {
        $('#ptax').select2('val', item.row.tax_rate);
        $('#old_tax').val(item.row.tax_rate);
        var item_discount = 0, ds = discount ? discount : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = formatDecimal(parseFloat(((unit_price) * parseFloat(pds[0])) / 100), 4);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        net_price -= item_discount;
        var pr_tax = item.row.tax_rate, pr_tax_val = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (reitems[item_id].row.tax_method == 0) {
                            pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))), 4);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            net_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / 100), 4);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }
    }
    if (site.settings.product_serial !== 0) {
        $('#pserial').val(row.children().children('.rserial').val());
    }
    var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
    if(item.options !== false) {
        var o = 1;
        opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
        $.each(item.options, function () {
            if(o == 1) {
                if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
            }
            $("<option />", {value: this.id, text: this.name}).appendTo(opt);
            o++;
        });
    } else {
        product_variant = 0;
    }

    uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
    if (item.units) {
        uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
        $.each(item.units, function () {
            if(this.id == item.row.unit) {
                $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
            } else {
                $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
            }
        });
    }

    $('#poptions-div').html(opt);
    $('#punits-div').html(uopt);
    $('select.select').select2({minimumResultsForSearch: 7});
    $('#pquantity').val(qty);
    $('#old_qty').val(qty);
    $('#pprice').val(unit_price);
    $('#punit_price').val(formatDecimal(parseFloat(unit_price)+parseFloat(pr_tax_val)));
    $('#poption').select2('val', item.row.option);
    $('#old_price').val(unit_price);
    $('#row_id').val(row_id);
    $('#item_id').val(item_id);
    $('#pserial').val(row.children().children('.rserial').val());
    $('#pdiscount').val(discount);
    $('#net_price').text(formatMoney(net_price));
    $('#pro_tax').text(formatMoney(pr_tax_val));
    $('#prModal').appendTo("body").modal('show');

});

$('#prModal').on('shown.bs.modal', function (e) {
    if($('#poption').select2('val') != '') {
        $('#poption').select2('val', product_variant);
        product_variant = 0;
    }
});

$(document).on('change', '#pprice, #ptax, #pdiscount', function () {
    var row = $('#' + $('#row_id').val());
    var item_id = row.attr('data-item-id');
    var unit_price = parseFloat($('#pprice').val());
    var item = reitems[item_id];
    var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
    if (ds.indexOf("%") !== -1) {
        var pds = ds.split("%");
        if (!isNaN(pds[0])) {
            item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
        } else {
            item_discount = parseFloat(ds);
        }
    } else {
        item_discount = parseFloat(ds);
    }
    unit_price -= item_discount;
    var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
    var pr_tax_val = 0, pr_tax_rate = 0;
    if (pr_tax !== null && pr_tax != 0) {
        $.each(tax_rates, function () {
            if(this.id == pr_tax){
                if (this.type == 1) {

                    if (item_tax_method == 0) {
                        pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)), 4);
                        pr_tax_rate = formatDecimal(this.rate) + '%';
                        unit_price -= pr_tax_val;
                    } else {
                        pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / 100), 4);
                        pr_tax_rate = formatDecimal(this.rate) + '%';
                    }

                } else if (this.type == 2) {

                    pr_tax_val = parseFloat(this.rate);
                    pr_tax_rate = this.rate;

                }
            }
        });
    }

    $('#net_price').text(formatMoney(unit_price));
    $('#pro_tax').text(formatMoney(pr_tax_val));
});

$(document).on('change', '#punit', function () {
    var row = $('#' + $('#row_id').val());
    var item_id = row.attr('data-item-id');
    var item = reitems[item_id];
    if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
        $(this).val(old_row_qty);
        bootbox.alert(lang.unexpected_value);
        return;
    }
    var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
    if(item.options !== false) {
        $.each(item.options, function () {
            if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                aprice = parseFloat(this.price);
            }
        });
    }
    if(item.units && unit != reitems[item_id].row.base_unit) {
        $.each(item.units, function(){
            if (this.id == unit) {
                base_quantity = unitToBaseQty($('#pquantity').val(), this);
                $('#pprice').val(formatDecimal(((parseFloat(item.row.base_unit_price+aprice))*unitToBaseQty(1, this)), 4)).change();
            }
        });
    } else {
        $('#pprice').val(formatDecimal(item.row.base_unit_price+aprice)).change();
    }
});

    /* -----------------------
     * Edit Row Method
     ----------------------- */
     $(document).on('click', '#editItem', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_rate = false;
        if (new_pr_tax) {
            $.each(tax_rates, function () {
                if (this.id == new_pr_tax) {
                    new_pr_tax_rate = this;
                }
            });
        }
        var price = parseFloat($('#pprice').val());
        if(item.options !== false) {
            var opt = $('#poption').val();
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    price = price-parseFloat(this.price);
                }
            });
        }
        if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
            if(!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > price) {
                bootbox.alert(lang.unexpected_value);
                return false;
            }
        }
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var unit = $('#punit').val();
        var base_quantity = parseFloat($('#pquantity').val());
        if(unit != reitems[item_id].row.base_unit) {
            $.each(reitems[item_id].units, function(){
                if (this.id == unit) {
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        reitems[item_id].row.fup = 1,
        reitems[item_id].row.qty = parseFloat($('#pquantity').val()),
        reitems[item_id].row.base_quantity = parseFloat(base_quantity),
        reitems[item_id].row.real_unit_price = price,
        reitems[item_id].row.unit = unit,
        reitems[item_id].row.tax_rate = new_pr_tax,
        reitems[item_id].tax_rate = new_pr_tax_rate,
        reitems[item_id].row.discount = $('#pdiscount').val() ? $('#pdiscount').val() : '',
        reitems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '',
        reitems[item_id].row.serial = $('#pserial').val();
        localStorage.setItem('reitems', JSON.stringify(reitems));
        $('#prModal').modal('hide');

        loadItems();
        return;
    });

    /* -----------------------
     * Product option change
     ----------------------- */
     $(document).on('change', '#poption', function () {
        var row = $('#' + $('#row_id').val()), opt = $(this).val();
        var item_id = row.attr('data-item-id');
        var item = reitems[item_id];
        var unit = $('#punit').val(), base_quantity = parseFloat($('#pquantity').val()), base_unit_price = item.row.base_unit_price;
        if(unit != reitems[item_id].row.base_unit) {
            $.each(reitems[item_id].units, function(){
                if (this.id == unit) {
                    base_unit_price = formatDecimal((parseFloat(item.row.base_unit_price)*(unitToBaseQty(1, this))), 4)
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        $('#pprice').val(parseFloat(base_unit_price)).trigger('change');
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    // $('#pprice').val(parseFloat(base_unit_price)+(parseFloat(this.price))).trigger('change');
                }
            });
        }
    });

    /* ------------------------------
     * Show manual item addition modal
     ------------------------------- */
     $(document).on('click', '#addManually', function (e) {
        if (count == 1) {
            reitems = {};
        }
        $('#mnet_price').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal').appendTo("body").modal('show');
        return false;
    });

     $(document).on('click', '#addItemManually', function (e) {
        var mid = (new Date).getTime(),
        mcode = $('#mcode').val(),
        mname = $('#mname').val(),
        mtax = parseInt($('#mtax').val()),
        mqty = parseFloat($('#mquantity').val()),
        mdiscount = $('#mdiscount').val() ? $('#mdiscount').val() : '0',
        unit_price = parseFloat($('#mprice').val()),
        mtax_rate = {};
        if (mcode && mname && mqty && unit_price) {
            $.each(tax_rates, function () {
                if (this.id == mtax) {
                    mtax_rate = this;
                }
            });

            reitems[mid] = {"id": mid, "item_id": mid, "label": mname + ' (' + mcode + ')', "row": {"id": mid, "code": mcode, "name": mname, "quantity": mqty, "base_quantity": mqty, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": mtax, "tax_method": 0, "qty": mqty, "type": "manual", "discount": mdiscount, "serial": "", "option":""}, "tax_rate": mtax_rate, 'units': false, "options":false};
            localStorage.setItem('reitems', JSON.stringify(reitems));
            loadItems();
        }
        $('#mModal').modal('hide');
        $('#mcode').val('');
        $('#mname').val('');
        $('#mtax').val('');
        $('#mquantity').val('');
        $('#mdiscount').val('');
        $('#mprice').val('');
        return false;
    });

     $(document).on('change', '#mprice, #mtax, #mdiscount', function () {
        var unit_price = parseFloat($('#mprice').val());
        var ds = $('#mdiscount').val() ? $('#mdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#mtax').val(), item_tax_method = 0;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))), 4);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / 100), 4);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        $('#mnet_price').text(formatMoney(unit_price));
        $('#mpro_tax').text(formatMoney(pr_tax_val));
    });

    /* --------------------------
     * Edit Row Quantity Method
     --------------------------- */
     var old_row_qty;
     $(document).on("focus", '.rquantity', function () {
        old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        reitems[item_id].row.base_quantity = new_qty;
        if(reitems[item_id].row.unit != reitems[item_id].row.base_unit) {
            $.each(reitems[item_id].units, function(){
                if (this.id == reitems[item_id].row.unit) {
                    reitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        reitems[item_id].row.qty = new_qty;
        localStorage.setItem('reitems', JSON.stringify(reitems));
        loadItems();
    });

    /* --------------------------
     * Edit Row Price Method
     -------------------------- */
     var old_price;
     $(document).on("focus", '.rprice', function () {
        old_price = $(this).val();
    }).on("change", '.rprice', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_price);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_price = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        reitems[item_id].row.price = new_price;
        localStorage.setItem('reitems', JSON.stringify(reitems));
        loadItems();
    });
});

//localStorage.clear();
//aca load
function loadItems() {
    $("#reTable tbody").empty();
    if (localStorage.getItem('reitems')) {
        total = 0;
        count = 1;
        an = 1;
        product_tax = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        total_discount = 0;

        $("#reTable tbody").empty();
        reitems = JSON.parse(localStorage.getItem('reitems'));
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(reitems, function(o) { return [parseInt(o.order)]; }) : reitems;
        $('#add_sale, #edit_sale').attr('disabled', false);
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_type = item.row.type, combo_items = item.combo_items, item_price = item.row.price, item_qty = item.row.qty, item_aqty = item.row.quantity, item_tax_method = item.row.tax_method, item_ds = item.row.discount, item_discount = 0, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var product_unit = item.row.unit, base_quantity = item.row.base_quantity;
            var unit_price = item.row.real_unit_price;
            if(item.units && item.row.fup != 1 && product_unit != item.row.base_unit) {
                $.each(item.units, function() {
                    if (this.id == product_unit) {
                        base_quantity = formatDecimal(unitToBaseQty(item.row.qty, this), 4);
                        unit_price = formatDecimal((parseFloat(item.row.base_unit_price)*(unitToBaseQty(1, this))), 4);
                    }
                });
            }
            // if(item.options !== false) {
            //     $.each(item.options, function () {
            //         if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
            //             item_price = unit_price+(parseFloat(this.price));
            //             unit_price = item_price;
            //         }
            //     });
            // }

            var ds = item_ds ? item_ds : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal((((unit_price) * parseFloat(pds[0])) / 100), 4);
                } else {
                    item_discount = formatDecimal(ds);
                }
            } else {
               item_discount = formatDecimal(ds);
           }
           product_discount += parseFloat(item_discount * item_qty);

           unit_price = formatDecimal(unit_price-item_discount);
           var pr_tax = item.tax_rate;
           var pr_tax_val = 0, pr_tax_rate = 0;
           if (site.settings.tax1 == 1) {
            if (pr_tax !== false && pr_tax != 0) {
                if (pr_tax.type == 1) {

                    if (item_tax_method == '0') {
                        pr_tax_val = formatDecimal((((unit_price) * parseFloat(pr_tax.rate)) / (100 + parseFloat(pr_tax.rate))), 4);
                        pr_tax_rate = formatDecimal(pr_tax.rate) + '%';
                    } else {
                        pr_tax_val = formatDecimal((((unit_price) * parseFloat(pr_tax.rate)) / 100), 4);
                        pr_tax_rate = formatDecimal(pr_tax.rate) + '%';
                    }

                } else if (pr_tax.type == 2) {

                    pr_tax_val = parseFloat(pr_tax.rate);
                    pr_tax_rate = pr_tax.rate;

                }
                product_tax += pr_tax_val * item_qty;
            }
        }
        item_price = item_tax_method == 0 ? formatDecimal(unit_price-pr_tax_val, 4) : formatDecimal(unit_price);
        unit_price = formatDecimal(unit_price+item_discount, 4);
        var sel_opt = '';
        $.each(item.options, function () {
            if(this.id == item_option) {
                sel_opt = this.name;
            }
        });
        var row_no = item.id;
        var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
        tr_html = '<td>'+
                        '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                        '<input name="sale_item_id[]" type="hidden" class="sale_item_id" value="' + (item.row.sale_item_id !== undefined ? item.row.sale_item_id : NULL) + '">'+
                        '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                        '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                        '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                        '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                        '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name +(sel_opt != '' ? ' ('+sel_opt+')' : '')+'</span> <i class="pull-right fa fa-edit tip pointer edit" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i>'+
                    '</td>';
        if (site.settings.product_serial == 1) {
            tr_html += '<td class="text-right"><input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'"></td>';
        }
        tr_html += '<td class="text-right"><input class="form-control input-sm text-right rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '"><input class="ruprice" name="unit_price[]" type="hidden" value="' + unit_price + '"><input class="realuprice" name="real_unit_price[]" type="hidden" value="' + item.row.real_unit_price + '"><span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(item_price) + '</span></td>';
        tr_html += '<td><input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + parseFloat(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();"><input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '"><input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + base_quantity + '"></td>';
        if ((site.settings.product_discount == 1 && allow_discount == 1) || item_discount) {
            tr_html += '<td class="text-right"><input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '"><span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + formatMoney(0 - (item_discount * item_qty)) + '</span></td>';
        }
        if (site.settings.tax1 == 1) {
            tr_html += '<td class="text-right"><input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + pr_tax.id + '"><span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (parseFloat(pr_tax_rate) != 0 ? '(' + formatDecimal(pr_tax_rate) + ')' : '') + ' ' + formatMoney(pr_tax_val * item_qty) + '</span></td>';
        }
        tr_html += '<td class="text-right"><span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty))) + '</span></td>';
        tr_html += '<td class="text-center"><i class="fa fa-times tip pointer redel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
        newTr.html(tr_html);
        newTr.prependTo("#reTable");
        total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty)), 4);
        setTimeout(function() {
            set_return_payments(total);
        }, 850);
        count += parseFloat(item_qty);
        an++;
    });

    var col = 2;
    if (site.settings.product_serial == 1) { col++; }
    var tfoot = '<tr id="tfoot" class="tfoot active">'+
                    '<th >Total</th>'+
                    '<th id="subtotalP">'+formatMoney(total - product_tax)+'</th>'+
                    '<th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
    if ((site.settings.product_discount == 1 && allow_discount == 1) || product_discount) {
        tfoot += '<th class="text-right">'+formatMoney(product_discount)+'</th>';
    }
    if (site.settings.tax1 == 1) {
        tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(product_tax)+'</th>';
    }
    tfoot += '<th class="text-right" id="totalP">'+formatMoney(total)+'</th>'+
             '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
            '</tr>';
    $('#reTable tfoot').html(tfoot);

    if (rediscount = localStorage.getItem('rediscount')) {
        var ds = rediscount;
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                order_discount = formatDecimal((((total) * parseFloat(pds[0])) / 100), 4);
            } else {
                order_discount = formatDecimal(ds);
            }
        } else {
            order_discount = formatDecimal(ds);
        }
    }

    if (site.settings.tax2 != 0) {
        if (retax2 = localStorage.getItem('retax2')) {
            $.each(tax_rates, function () {
                if (this.id == retax2) {
                    if (this.type == 2) {
                        invoice_tax = formatDecimal(this.rate);
                    } else if (this.type == 1) {
                        invoice_tax = formatDecimal((((total - order_discount) * this.rate) / 100), 4);
                    }
                }
            });
        }
    }

    total_discount = parseFloat(order_discount + product_discount);
    var gtotal = parseFloat((total + invoice_tax) - order_discount);
    $('#total').text(formatMoney(total));
    $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
    $('#total_items').val((parseFloat(count) - 1));
    $('#tds').text(formatMoney(order_discount));
    if (site.settings.tax2 != 0) {
        $('#ttax2').text(formatMoney(invoice_tax));
    }
    $('#gtotal').text(formatMoney(gtotal));
    if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
        $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
        $(window).scrollTop($(window).scrollTop() + 1);
    }
    set_page_focus();
    }
}

/* -----------------------------
 * Add Sale Order Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
 function add_return_item(item) {

    if (count == 1) {
        reitems = {};
    }
    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (reitems[item_id]) {

        var new_qty = parseFloat(reitems[item_id].row.qty) + 1;
        reitems[item_id].row.base_quantity = new_qty;
        if(reitems[item_id].row.unit != reitems[item_id].row.base_unit) {
            $.each(reitems[item_id].units, function(){
                if (this.id == reitems[item_id].row.unit) {
                    reitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        reitems[item_id].row.qty = new_qty;

    } else {
        reitems[item_id] = item;
    }
    reitems[item_id].order = new Date().getTime();
    localStorage.setItem('reitems', JSON.stringify(reitems));
    loadItems();
    return true;
}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}


//NUEVO SCRIPT AÑOS PASADOS

$(document).on('click', '.search_sale', function(){
    search_past_year_sale();
    localStorage.setItem('resale', $('#resale').val());
});
$(document).on('keyup', '#resale', function(e){
    if (e.keyCode == 13) {
        search_past_year_sale();
    }
    localStorage.setItem('resale', $('#resale').val());
});

function search_past_year_sale(){
    if ($('#reyear_database').val()) {
        $.ajax({
            url : site.base_url+"returns/past_year_sale_suggestion",
            data : {
                term : $('#resale').val(),
                database: $('#reyear_database').val(),
            },
        }).done(function(data){
            if (data.response == 1) {
                odt_is_pos = data.sale.pos;
                console.log(" >> odt_is_pos "+odt_is_pos);
                $('#rebiller').select2('val', data.sale.biller_id).select2('readonly', true);
                $('#sale_reference_no').val(data.sale.reference_no);
                $('#sale_pos').val(data.sale.pos);
                $('#rewarehouse').select2('val', data.sale.warehouse_id).select2('readonly', true);
                setTimeout(function() {
                    $('#rebiller').trigger('change');
                }, 1200);
                setCustomer(data.sale.customer_id);
                localStorage.setItem('reitems', JSON.stringify(data.sale_items));

                re_retenciones = {
                    'gtotal' : data.sale.gtotal,
                    'id_rete_fuente' : data.sale.rete_fuente_id,
                    'id_rete_iva' : data.sale.rete_iva_id,
                    'id_rete_ica' : data.sale.rete_ica_id,
                    'id_rete_otros' : data.sale.rete_other_id,
                    'id_rete_bomberil' : data.sale.rete_bomberil_id,
                    'id_rete_autoaviso' : data.sale.rete_autoaviso_id,
                };

                localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));
                recalcular_re_retenciones();

                setTimeout(function() {
                    loadItems();
                }, 1000);
                sale_payments = data.payments;
                if (Object.keys(data.payments).length > 0) {
                    $('#payments').fadeIn();
                    $('#payment_reference_no').prop('required', true);
                    // $('.deposit_document_type_id').prop('required', true);
                } else {
                    $('#payments').fadeOut();
                    $('#payment_reference_no').prop('required', false);
                    $('.deposit_document_type_id').prop('required', false);
                }

            } else {
                localStorage.removeItem('reitems');
                loadItems();
                command: toastr.error(data.msg, '¡Error!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        }).fail(function(data){
            command: toastr.error('Ocurrió un error durante la consulta, contáctese con soporte.', '¡Error!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        });
    } else {
        $('#reyear_database').select2('open');
        command: toastr.error('Debe escoger un año para la consulta de la factura.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
    }
}

function setCustomer(customer){
    $('#recustomer').val(customer).select2({
        minimumInputLength: 1,
        data: [],
        initSelection: function (element, callback) {
            $.ajax({
                type: "get", async: false,
                url: site.base_url+"customers/getCustomer/" + $(element).val(),
                dataType: "json",
                success: function (data) {
                    callback(data[0]);
                }
            });
        },
        ajax: {
            url: site.base_url + "customers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
    $('#recustomer').select2('readonly', true);
}

function set_return_payments(set_amount = 0){
    retention = localStorage.getItem('re_retenciones') ? formatDecimal(JSON.parse(localStorage.getItem('re_retenciones')).total_rete_amount) : 0;
    set_amount -= retention;
    num_pm = 0;
    amount_setted = set_amount > 0 ? true : false ;
    if (amount_setted) {
        $('.delete_payment_opt').trigger('click');
    }
    $('#amount_1').prop('readonly', false);
    if (localStorage.getItem('rereturn_payment_setted')) {
        $('#amount_1').val(formatDecimal(set_amount)).prop('readonly', true);
    } else {
        if (sale_payments) {
            $('.delete_payment_opt').trigger('click');
            $.each(sale_payments, function(payment_method, amount){
                if (payment_method != 'due' && payment_method != 'retention' && payment_method != 'discount') {
                    if (set_amount > 0) {
                        if (set_amount > amount) {
                            value_amount_to_pay = amount;
                            set_amount -= amount;
                        } else {
                            value_amount_to_pay = set_amount;
                            set_amount = 0;
                        }
                    } else if (set_amount == 0) {
                        if (amount_setted == true) {
                            return false;
                        } else {
                            value_amount_to_pay = amount;
                        }
                    } else {
                        value_amount_to_pay = amount;
                    }
                    num_pm++;
                    if (num_pm > 1 && value_amount_to_pay > 0) {
                        add_more_payment(payment_method, value_amount_to_pay);
                    } else {
                        $('#paid_by_1').select2('val', payment_method);
                        $('#amount_1').val(formatDecimal(value_amount_to_pay));
                        setTimeout(function() {
                            $('#paid_by_1').trigger('change');
                        }, 1200);
                    }
                }
            });
        }

    }
}

function add_more_payment(paid_by = null, amount = 0){
    var num = $('.pay_option').length;
    num++;
    $.ajax({
        url : site.base_url+'sales/get_paid_opts',
        data : { paid_by : paid_by, amount : amount  }
    }).done(function(paid_opts){
        if ($('.pay_option[data-num="'+num+'"]').length > 0) {
            $('.pay_option[data-num="'+num+'"]').remove();
        }
        html = '<div class="col-md-12 pay_option" data-num="'+num+'">'+
                   '<div class="well well-sm well_'+num+'">'+
                       '<div class="col-md-'+num+'2">'+
                           '<div class="row">'+
                                '<div class="col-sm-4 deposit_div_'+num+'" style="display:none;">'+
                                    lang.deposit_document_type_id+
                                    '<select name="deposit_document_type_id[]" class="form-control deposit_document_type_id" data-pbnum="'+num+'">'+
                                        '<option value="">seleccione</option>'+
                                    '</select>'+
                                '</div>'+
                               '<div class="col-sm-4">'+
                                   '<div class="form-group">'+
                                       lang.paying_by+
                                       '<select name="paid_by[]" id="paid_by_'+num+'" data-pbnum="'+num+'" class="form-control paid_by">'+
                                           paid_opts+
                                       '</select>'+
                                       '<input type="hidden" name="due_payment[]" class="due_payment_'+num+'">'+
                                   '</div>'+
                                   '<input type="hidden" name="mean_payment_code_fe[]" class="mean_payment_code_fe">'+
                               '</div>'+
                               '<div class="col-sm-4">'+
                                   '<div class="payment">'+
                                       '<div class="form-group ngc_'+num+'">'+
                                           lang.value+
                                           '<input name="amount-paid[]" type="text" id="amount_'+num+'"'+
                                               'class="pa form-control kb-pad amount only_number" value='+amount+' />'+
                                       '</div>'+
                                       '<div class="form-group gc_'+num+'" style="display: none;">'+
                                           '<?= lang("gift_card_no", "gift_card_no"); ?>'+
                                           '<input name="gift_card_no[]" type="text" id="gift_card_no_'+num+'"'+
                                               'class="pa form-control gift_card_no kb-pad"/>'+
                                           '<div id="gc_details_1"></div>'+
                                       '</div>'+
                                   '</div>'+
                               '</div>'+
                               '<div class="col-sm-4 div_slpayment_term_'+num+'" style="display:none;">'+
                                    '<div class="form-group">'+
                                        lang.payment_term+
                                        '<input name="payment_term[]" type="text" id="payment_term_'+num+'"'+
                                               'class="pa form-control kb-pad sale_payment_term only_number"/>'+
                                    '</div>'+
                               '</div>'+
                               '<span class="fa fa-times delete_payment_opt" style="position: absolute;right: 1.6%;font-size: 130%;color: #ed5767;"></span>'+
                           '</div>'+
                           '<div class="clearfix"></div>'+
                           '<div class="pcc_'+num+'" style="display:none;">'+
                               '<div class="row">'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_no[]" type="text" id="pcc_no_'+num+'"'+
                                               'class="form-control" placeholder="'+lang.cc_no+'"/>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_holder[]" type="text" id="pcc_holder_'+num+'"'+
                                               'class="form-control"'+
                                               'placeholder="'+lang.cc_holder+'"/>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<select name="pcc_type[]" id="pcc_type_'+num+'"'+
                                                   'class="form-control pcc_type"'+
                                                   'placeholder="'+lang.card_type+'">'+
                                               '<option value="Visa">'+lang.Visa+'</option>'+
                                               '<option'+
                                                   'value="MasterCard">'+lang.MasterCard+'</option>'+
                                               '<option value="Amex">'+lang.Amex+'</option>'+
                                               '<option value="Discover">'+lang.Discover+'</option>'+
                                           '</select>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_month[]" type="text" id="pcc_month_'+num+'"'+
                                               'class="form-control" placeholder="'+lang.month+'"/>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_year[]" type="text" id="pcc_year_'+num+'"'+
                                               'class="form-control" placeholder="'+lang.year+'"/>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="col-md-4">'+
                                       '<div class="form-group">'+
                                           '<input name="pcc_ccv[]" type="text" id="pcc_cvv2_'+num+'"'+
                                               'class="form-control" placeholder="'+lang.cvv2+'"/>'+
                                       '</div>'+
                                   '</div>'+
                               '</div>'+
                           '</div>'+
                           '<div class="pcheque_'+num+'" style="display:none;">'+
                               '<div class="form-group">'+lang.cheque_no+
                                   '<input name="cheque_no[]" type="text" id="cheque_no_'+num+'"'+
                                       'class="form-control cheque_no"/>'+
                               '</div>'+
                           '</div>'+
                       '</div>'+
                       '<div class="clearfix"></div>'+
                   '</div>'+
               '</div>';
        $('.payments_div').append(html);
        $('#paid_by_'+num).trigger('change');
        set_payment_method_billers();
    });
}

$(document).on('click', '.delete_payment_opt', function(){
    pay_opt = $(this).parents('.pay_option');
    pay_opt.remove();
});

$(document).on('change', 'select.paid_by', function(){
    pbnum = $(this).data('pbnum');
    paid_by = $(this).val();
    if (paid_by == 'deposit') {
        $('.deposit_div_'+pbnum).fadeIn();

        $.ajax({
            url:site.base_url+'billers/getBillersDocumentTypes/30/'+$('#rebiller').val(),
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('.deposit_document_type_id[data-pbnum="'+pbnum+'"]').html(response.options).select2();
            reinvoice_paid = parseFloat(localStorage.getItem('reinvoice_paid'));
            $('#amount_'+pbnum).prop('readonly', true);
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }
            $('.deposit_document_type_id[data-pbnum="'+pbnum+'"]').trigger('change');
            $('.deposit_document_type_id[data-pbnum="'+pbnum+'"]').prop('required', true);
        });
    } else {
        $('.deposit_div_'+pbnum).fadeOut();
        $('.deposit_document_type_id[data-pbnum="'+pbnum+'"]').prop('required', false);
        $('#amount_'+pbnum).prop('readonly', false);
    }
});


function getTrmRate(){
    trmrate = 1;

    if (othercurrency = localStorage.getItem('othercurrency')) {
        othercurrencytrm = localStorage.getItem('othercurrencytrm');
        othercurrencyrate = localStorage.getItem('othercurrencyrate');
        othercurrencycode = localStorage.getItem('othercurrencycode');

        trmrate = othercurrencyrate / othercurrencytrm;
    }

    return trmrate;
}

// RETENCIONES

    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_fuente_option').val('').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_iva_option').val('').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_ica_option').val('').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_otros_option').val('').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    $(document).on('click', '#rete_bomberil', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/BOMB"
            }).done(function(data){
                $('#rete_bomberil_option').html(data);
                $('#rete_bomberil_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_bomberil_option').val('').attr('disabled', true).select();
            $('#rete_bomberil_tax').val('');
            $('#rete_bomberil_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    $(document).on('click', '#rete_autoaviso', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/TABL"
            }).done(function(data){
                $('#rete_autoaviso_option').html(data);
                $('#rete_autoaviso_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_autoaviso_option').val('').attr('disabled', true).select();
            $('#rete_autoaviso_tax').val('');
            $('#rete_autoaviso_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option, #rete_iva_option, #rete_ica_option, #rete_otros_option, #rete_bomberil_option, #rete_autoaviso_option', function(){
        rete_sale_option_changed($(this).prop('id'), getTrmRate());
    });

    function getReteAmount(apply){
        amount = 0;
        if (apply == "ST") {
            amount = $('#subtotalP').text();
        } else if (apply == "TX") {
            amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = $('#totalP').text();
        } else if (apply == "IC") {
            amount = $('#rete_ica_valor').val();
        }
        if (othercurrency = localStorage.getItem('othercurrency')) {
            othercurrencytrm = localStorage.getItem('othercurrencytrm');
            if (othercurrencytrm > 0) {
                amount = amount * othercurrencytrm;
            }
        }
        return amount;
    }

    function setReteTotalAmount(){
        rtotal_rete =
                    ($('#rete_fuente_assumed').is(':checked') ? 0 : formatDecimal($('#rete_fuente_valor').val())) +
                    ($('#rete_iva_assumed').is(':checked') ? 0 : formatDecimal($('#rete_iva_valor').val())) +
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_ica_valor').val())) +
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_bomberil_valor').val())) +
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_autoaviso_valor').val())) +
                    ($('#rete_otros_assumed').is(':checked') ? 0 : formatDecimal($('#rete_otros_valor').val()));
        trmrate = getTrmRate();
        $('#total_rete_amount_show').text(formatMoney(trmrate * rtotal_rete));
        $('#total_rete_amount').text(formatMoney(rtotal_rete));
    }


    $(document).on('click', '#updateOrderRete', function () {
        // var ts = $('#order_tax_input').val();
        // $('#sltax2').val(ts);

        $('#ajaxCall').fadeIn();

        rete_prev = JSON.parse(localStorage.getItem('re_retenciones'));
        var re_retenciones = {
            'gtotal' : (rete_prev ? rete_prev.gtotal : $('#gtotal').text()),
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            'rete_fuente_assumed' : $('#rete_fuente_assumed').is(':checked'),
            'rete_iva_assumed' : $('#rete_iva_assumed').is(':checked'),
            'rete_ica_assumed' : $('#rete_ica_assumed').is(':checked'),
            'rete_otros_assumed' : $('#rete_otros_assumed').is(':checked'),
            'bomberil_option' : $('#rete_bomberil_option option:selected').data('percentage'),
            'id_rete_bomberil' : $('#rete_bomberil_option').val(),
            'autoaviso_option' : $('#rete_autoaviso_option option:selected').data('percentage'),
            'id_rete_autoaviso' : $('#rete_autoaviso_option').val(),
        };
        re_retenciones.total_discounted = formatMoney(parseFloat(formatDecimal(rete_prev ? rete_prev.gtotal : $('#gtotal').text())) - parseFloat(formatDecimal($('#total_rete_amount').text())));

        localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));

        setTimeout(function() {
            $('#gtotal').text(re_retenciones.total_discounted);
            $('#ajaxCall').fadeOut();
            loadItems();
        }, 1000);

        general_total_rete = formatDecimal($('#rete_fuente_valor').val()) + formatDecimal($('#rete_iva_valor').val()) + formatDecimal($('#rete_ica_valor').val()) + formatDecimal($('#rete_otros_valor').val());
        if (general_total_rete > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(re_retenciones.total_rete_amount)))
        } else {
            $('#rete_applied').val(0);
        }
        $('#retencion_show').val(re_retenciones.total_rete_amount);
        $('#rtModal').modal('hide');
    });

    $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('re_retenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
    });

    function recalcular_re_retenciones(){

        $('#add_return').prop('disabled', true);
        re_retenciones = JSON.parse(localStorage.getItem('re_retenciones'));
        if (re_retenciones != null) {
                if (re_retenciones.id_rete_fuente> 0) {
                    if (!$('#rete_fuente').is(':checked')) {
                        $('#rete_fuente').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_iva> 0) {
                    if (!$('#rete_iva').is(':checked')) {
                        $('#rete_iva').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_ica> 0) {
                    if (!$('#rete_ica').is(':checked')) {
                        $('#rete_ica').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_bomberil> 0) {
                    $('#rete_bomberil').prop('disabled', false);
                    if (!$('#rete_bomberil').is(':checked')) {
                        $('#rete_bomberil').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_autoaviso> 0) {
                    $('#rete_autoaviso').prop('disabled', false);
                    if (!$('#rete_autoaviso').is(':checked')) {
                        $('#rete_autoaviso').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_otros> 0) {
                    if (!$('#rete_otros').is(':checked')) {
                        $('#rete_otros').trigger('click');
                    }
                }
                if (re_retenciones.rete_fuente_assumed != false) {
                    if (!$('#rete_fuente_assumed').is(':checked')) {
                        $('#rete_fuente_assumed').trigger('click');
                    }
                }
                if (re_retenciones.rete_iva_assumed != false) {
                    if (!$('#rete_iva_assumed').is(':checked')) {
                        $('#rete_iva_assumed').trigger('click');
                    }
                }
                if (re_retenciones.rete_ica_assumed != false) {
                    if (!$('#rete_ica_assumed').is(':checked')) {
                        $('#rete_ica_assumed').trigger('click');
                    }
                }
                if (re_retenciones.rete_otros_assumed != false) {
                    if (!$('#rete_otros_assumed').is(':checked')) {
                        $('#rete_otros_assumed').trigger('click');
                    }
                }

                setTimeout(function() {
                    $.each($('#rete_fuente_option option'), function(index, value){
                        if(re_retenciones.id_rete_fuente != '' && $(value).val() == re_retenciones.id_rete_fuente){
                            $('#rete_fuente_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_iva_option option'), function(index, value){
                        if(re_retenciones.id_rete_iva != '' && $(value).val() == re_retenciones.id_rete_iva){
                            $('#rete_iva_option').select2('val', $(value).val()).trigger('change');
                        }
                    });

                    $.each($('#rete_ica_option option'), function(index, value){
                        if(re_retenciones.id_rete_ica != '' && $(value).val() == re_retenciones.id_rete_ica){
                            $('#rete_ica_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_otros_option option'), function(index, value){
                        if(re_retenciones.id_rete_otros != '' && $(value).val() == re_retenciones.id_rete_otros){
                            $('#rete_otros_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_bomberil_option option'), function(index, value){
                        if(re_retenciones.id_rete_bomberil != '' && $(value).val() == re_retenciones.id_rete_bomberil){
                            $('#rete_bomberil_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_autoaviso_option option'), function(index, value){
                        if(re_retenciones.id_rete_autoaviso != '' && $(value).val() == re_retenciones.id_rete_autoaviso){
                            $('#rete_autoaviso_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $('#rete_otros_option').select2().trigger('change');
                    $('#updateOrderRete').trigger('click');
                    $('#add_return').prop('disabled', false);
                }, 2000);
        } else {
            $('#add_return').prop('disabled', false);
        }
    }

    $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('re_retenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
    });