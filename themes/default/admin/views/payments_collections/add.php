<?php defined('BASEPATH') OR exit('No direct script access allowed');
    // var_dump($_SESSION);
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" id="close_modal" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="pc_myModalLabel"><?php echo sprintf(lang('payments_collections_add'), lang('payments_collections')); ?></h4>
        </div>
        <?php
        $attrib = array('id' => 'pc_add', 'target' => '_blank');
        echo admin_form_open_multipart("payments_collections/add", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <?php if ($Owner || $Admin) { ?>
                <div class="form-group">
                    <?= lang("date", "date"); ?>
                    <?= form_input('pc_date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="pc_date" required="required" disabled'); ?>
                </div>
            <?php } ?>
            <?php
            $bl[""] = "";
            $bldata = [];
            foreach ($billers as $biller) {
                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                $bldata[$biller->id] = $biller;
            }
            ?>

            <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                <div class="form-group">
                    <div class="form-group">
                        <?= lang("biller", "pc_biller"); ?>
                        <select name="pc_biller" class="form-control" id="pc_biller" required="required">
                            <?php foreach ($billers as $biller) : ?>
                                <option value="<?= $biller->id ?>"><?= $biller->company ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            <?php } else { ?>
                <div class="form-group">
                    <?= lang("biller", "pc_biller"); ?>
                    <select name="pc_biller" class="form-control" id="pc_biller">
                        <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                            $biller = $bldata[$this->session->userdata('biller_id')];
                        ?>
                            <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                        <?php endif ?>
                    </select>
                </div>
            <?php } ?>
            <div class="form-group">
                <?= lang("reference", "pc_document_type_id"); ?>
                <select name="pc_document_type_id" class="form-control" id="pc_document_type_id" required>

                </select>
            </div>
            <div class="form-group">
              <label class="control-label" for="customer_id"><?php echo lang('customer'); ?></label>
              <?= form_input('customer_id', '', 'id="customer_id" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip"'); ?>
            </div>
            <?php if (isset($cost_centers)): ?>
                <div class="form-group">
                    <?= lang('cost_center', 'pc_cost_center_id') ?>
                    <?php
                    $ccopts[''] = lang('select');
                    foreach ($cost_centers as $cost_center) {
                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                    }
                     ?>
                     <?= form_dropdown('pc_cost_center_id', $ccopts, '', 'id="pc_cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                </div>
            <?php endif ?>
            <?php if ($this->Settings->payment_collection_webservice == 1): ?>
                <div class="row">
                    <div class="col-sm-6">
                        <?= lang("kaiowa_payment_collection_reference", "kaiowa_payment_collection_reference"); ?>
                        <div class="input-group">
                          <input name="kaiowa_payment_collection_reference" type="text" id="kaiowa_payment_collection_reference" value="" class="pa form-control kb-pad"/>
                          <span class="input-group-addon" id="consultar_recaudo_kaiowa" style="cursor:pointer;"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <?= lang("kaiowa_payment_collection_amount", "kaiowa_payment_collection_amount"); ?>
                        <input name="kaiowa_payment_collection_amount" type="hidden" id="kaiowa_payment_collection_amount" value="" class="pa form-control kb-pad"/>
                        <input name="format_kaiowa_payment_collection_amount" type="text" id="format_kaiowa_payment_collection_amount" value="" class="pa form-control kb-pad" readonly/>
                    </div>
                </div>
            <?php endif ?>
            <hr class="col-sm-11">
            <div class="col-sm-9">
                <h3><?= lang('payment_methods') ?></h3>
                <em class="text-danger error_amount" style="display:none;"><?= lang('complete_amount_paid') ?></em>
            </div>
            <div class="col-sm-3 text-right">
                <button type="button" class="btn btn-primary add_payment"><i class="fa fa-plus"></i></button>
            </div>

            <div class="payments">
                <div class="div_payment_1">
                    <div class="row paid">
                        <div class="col-sm-6">
                            <?= lang("value", "pc_amount"); ?>
                            <input name="pc_amount[]" type="text" id="pc_amount_1" value="" class="pa form-control kb-pad pc_amount only_number"
                                   required="required"/>
                        </div>
                        <div class="col-sm-6">
                            <?= lang("paid_by", "paid_by"); ?>
                            <select name="paid_by[]" id="pc_paid_by_1" class="form-control">
                                <?= $this->sma->paid_opts('cash', false, true, false, true); ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= lang("note", "pc_note"); ?>
                <?php echo form_textarea('pc_note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="pc_note"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary form_submit" id="form_submit" disabled><?= lang('submit') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(document).off('click', '.form_submit');
        $(document).off('click', '.add_payment');
        $(document).off('click', '#consultar_recaudo_kaiowa');
        $(document).off('keyup', '.pc_amount');
        $(document).off('change', '#pc_biller');
        $(document).off('keyup', '#kaiowa_payment_collection_reference');
        $("#pc_add").validate({
            ignore: []
        });
        $(document).on('click', '.form_submit', function(){
            if ($('#pc_add').valid()) {
                $('#pc_add').submit();
                $('#form_submit').prop('disabled', true);
                setTimeout(function() {
                    location.href = site.base_url+'payments_collections/index';
                }, 500);
            }
        });
        $(document).on('click', '.add_payment', function(){
            add_payment();
        });
        $(document).on('click', '#consultar_recaudo_kaiowa', function(){
            if ($('#customer_id').val()) {
                if ($('#kaiowa_payment_collection_reference').val()) {
                    kaiowa_payment_collection_reference = $('#kaiowa_payment_collection_reference').val();
                    /* AQUÍ PROGRAMACIÓN WEBSERVICE */
                    checkAmountPaidKiowa();
                    /* AQUÍ PROGRAMACIÓN WEBSERVICE */
                } else {
                    header_alert('warning', 'Porfavor especifique el número de recaudo');
                    $('#kaiowa_payment_collection_reference').focus();
                }
            } else {
                header_alert('warning', 'Porfavor escoja el cliente primero');
                $('#customer_id').select2('open');
            }
        });
        $(document).on('keyup, change', '.pc_amount', function(){
            if (site.settings.payment_collection_webservice == 1) {
                var kaiowa_payment_collection_amount = parseFloat($('#kaiowa_payment_collection_amount').val());
                var inputs_amount = 0;
                $('.pc_amount').each(function(index, input){
                    inputs_amount+=parseFloat($(input).val());
                    if (inputs_amount == kaiowa_payment_collection_amount) {
                        $('.form_submit').prop('disabled', false);
                        $('.error_amount').fadeOut();
                    } else {
                        $('.form_submit').prop('disabled', true);
                        $('.error_amount').fadeIn();
                        $('.cap_amount').html(formatMoney(kaiowa_payment_collection_amount - inputs_amount));
                    }
                });
            } else {
                var inputs_amount = 0;
                $('.pc_amount').each(function(index, input){
                    inputs_amount+=parseFloat($(input).val());
                });
                if (inputs_amount > 0) {
                    $('.form_submit').prop('disabled', false);
                } else {
                    $('.form_submit').prop('disabled', true);
                }
            }
        });
        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $("#pc_date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
        $(document).on('change', '#pc_biller', function(){
            $.ajax({
                url: site.base_url+'billers/getBillersDocumentTypes/57/'+$('#pc_biller').val(),
                type:'get',
                dataType:'JSON'
            }).done(function(data){
                response = data;
                $('#pc_document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                                "showDuration": "500",
                                "hideDuration": "1000",
                                "timeOut": "6000",
                                "extendedTimeOut": "1000",
                            });
                }
                if (response.status == 0) {
                  $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                }
                $('#pc_document_type_id').trigger('change');
            });
        });
        $('#pc_biller').trigger('change');
        nsCustomer();

        function nsCustomer() {
            $('#customer_id').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        }
        var num_div_payment = 1;
        function add_payment(){
            $('#ajaxCall').fadeIn();
            $('#pc_paid_by_1').select2('destroy');
            $('.add_payment').prop('disabled', true);
            num_div_payment++;
            html_div = $('.div_payment_1').html();
            html_div = html_div.replace(/_1/g, '_'+num_div_payment);
            $(html_div).appendTo('.payments');
            setTimeout(function() {
                if (site.settings.payment_collection_webservice == 1) {
                    var kaiowa_payment_collection_amount = parseFloat($('#kaiowa_payment_collection_amount').val());
                    $('.pc_amount').each(function(index, input){
                        if ($(input).val() > 0) {
                            kaiowa_payment_collection_amount-=parseFloat($(input).val());
                        }
                    });
                    if (kaiowa_payment_collection_amount > 0) {
                        $('#pc_amount_'+num_div_payment).val(kaiowa_payment_collection_amount);
                        $('#pc_amount_'+num_div_payment).trigger('change');
                    }
                }
                $('#pc_paid_by_'+num_div_payment).select2().select2('val', '');
                $(document).on('change', '#pc_paid_by_'+num_div_payment, function(){
                    payment = $(this).closest('.payments');
                    payment.find('.deposit_message').remove();
                    validate_paids($("#customer_id").val());
                });
                $('#pc_paid_by_1').select2();
                $('#ajaxCall').fadeOut();
                $('.add_payment').prop('disabled', false);
            }, 850);
        }
        $(document).on('keyup', '#kaiowa_payment_collection_reference', function(e){
            if (e.keyCode == 13) {
                $('#consultar_recaudo_kaiowa').trigger('click');
            }
        });

        $(document).on('change', '#customer_id', function(){
            validate_paids($(this).val());
        });

        $(document).on('change', '#pc_paid_by_1', function(){
            payment = $(this).closest('.payments');
            payment.find('.deposit_message').remove();
            validate_paids($('#customer_id').val());
        });
    });

function checkAmountPaidKiowa()
{
    swal({
        title: 'Consultando Recibo en Kiowa...',
        text: 'Por favor, espere un momento.',
        type: 'info',
        showCancelButton: false,
        showConfirmButton: false,
        closeOnClickOutside: false,
    });

    $.ajax({
        type: 'post',
        url: site.base_url+'Payments_collections/checkAmountPaidKiowa',
        data: {
            '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
            'customerId' : $('#customer_id').val(),
            'kaiowaReference' : $('#kaiowa_payment_collection_reference').val(),
        },
        dataType: 'json',
        success: function (response) {
            if (response.status == true) {
                $('#kaiowa_payment_collection_amount').val(response.data);
                $('#pc_amount_1').val(response.data);
                $('.pc_amount').trigger('change');
                $('#format_kaiowa_payment_collection_amount').val(formatMoney(response.data));

            } else {
                header_alert('danger', response.message);
                $('#kaiowa_payment_collection_amount').val('')
            }
            swal.close()
        }
    });
}

function validate_paids(customer){
    var elementos = $(".payments").find(".paid"); // buscar todos los selects creados de pagos
    elementos.each(function() {
        payment = $(this).closest('.payments');
        let num_div = ($(this).children().eq(1).find(".form-control").attr("id"));
        num_div = num_div.replace("s2id_pc_paid_by_", "");
        paid_by = $('#pc_paid_by_'+num_div).val();
        amount_to_pay = parseFloat(formatDecimal($('#pc_amount_'+num_div).val()));
        customer = $('#customer_id').val();
        if (paid_by == "deposit") {
            $.ajax({
                url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
            }).done(function(data) {
                payment.find('.deposit_message').remove();

                const paymentsTables = $(data)
                paymentsTables.find('.panel-body').css('padding', 0)
                let str = paymentsTables.prop('outerHTML')

                payment.append(str);
                let busqueda = $(".deposit_message").find('*').filter(function() {
                    return $(this).text().trim() === 'No hay anticipos con saldos registrados';
                });
                if (busqueda.length > 0) {
                    $('#form_submit').prop('disabled', true);
                }
            });
        }else{
            $('#form_submit').prop('disabled', false);
        }
    });
}
</script>
