<div class="table-responsive">
		<table id="TableData" class="table table-stripped" >
			<thead>
				<tr>
					<td class="text-center"> <b>REFERENCIA VENTA</b></td>
					<td class="text-center"> <b>FECHA VENTA</b></td>
					<td class="text-center"> <b>DIAS EN MORA</b></td>
					<td class="text-center"> <b>VALOR VENTA</b></td>
					<td class="text-center"> <b>SALDO PAGADO</b></td>
					<td class="text-center"> <b>SALDO PENDIENTE</b></td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$nit_old = $sucursal_old = $vendedor_old =  'old' ; 
					$total = $val_pago = $Saldo = 0;
					$total_total = $total_val_pago = $total_Saldo = 0;		
					$total_total_Sucursal = $total_val_pago_Sucursal = $total_Saldo_Sucursal = 0;			
					$i = 0;
					$prin_ven = $prin_clien = false;
				?>
				<?php foreach ($sales as $sale): ?>
					<?php 
					$i++; 
					$prin_clien = false;
					?>

					<?php if ($sale['documento'] != $nit_old): ?>
						<?php if ($i > 1): ?>
							<tr > 
								<td ></td>
								<td></td>
								<td class="text-right" >SubTotal Cliente </td>
								<td class="text-right">$ <?php echo number_format($total,0,'','.');?></td>
								<td class="text-right">$ <?php echo number_format($val_pago,0,'','.');?></td>
								<td class="text-right">$ <?php echo number_format($Saldo,0,'','.');?></td>
							</tr>
							<?php $total = $val_pago = $Saldo = 0;	$prin_clien = true;	?>
						<?php endif; ?>
					<?php endif; ?>

					<?php if ($sale['cod_Sucursal'] != $sucursal_old): ?>
						<?php if ($i > 1): ?>
							<?php if ($prin_ven == false): ?>
								<?php if ($prin_clien == false): ?>
									<tr > 
										<td ></td>
										<td></td>
										<td class="text-right" >SubTotal Cliente </td>
										<td class="text-right">$ <?php echo number_format($total,0,'','.');?></td>
										<td class="text-right">$ <?php echo number_format($val_pago,0,'','.');?></td>
										<td class="text-right">$ <?php echo number_format($Saldo,0,'','.');?></td>
									</tr>
									<?php $total = $val_pago = $Saldo = 0;		?>
								<?php endif; ?>
								<tr class="active" > 
									<td ></td>
									<td></td>
									<td class="text-right">SubTotal Vendedor </td>
									<td class="text-right">$ <?php echo number_format($total_total,0,'','.');?></td>
									<td class="text-right">$ <?php echo number_format($total_val_pago,0,'','.');?></td>
									<td class="text-right">$ <?php echo number_format($total_Saldo,0,'','.');?></td>
								</tr>
								<?php $total_total = $total_val_pago = $total_Saldo = 0;	$prin_ven = true;	?>
							<?php endif; ?>						
							<tr class="active" > 
								<td ></td>
								<td></td>
								<td class="text-right">SubTotal Sucursal </td>
								<td class="text-right">$ <?php echo number_format($total_total_Sucursal,0,'','.');?></td>
								<td class="text-right">$ <?php echo number_format($total_val_pago_Sucursal,0,'','.');?></td>
								<td class="text-right">$ <?php echo number_format($total_Saldo_Sucursal,0,'','.');?></td>
							</tr>
							<?php $total_total_Sucursal = $total_val_pago_Sucursal = $total_Saldo_Sucursal = 0;			?>
						<?php endif; ?>
						<tr class="active">
							<td ><h5><b>Sucursal: <?php echo $sale['Sucursal']; ?></b></h5></td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ></td>
						</tr>
						<?php 
							$nit_old = $sucursal_old = $vendedor_old =  'old' ; 
							$total = $val_pago = $Saldo = 0;
							$total_total = $total_val_pago = $total_Saldo = 0;		
							$total_total_Sucursal = $total_val_pago_Sucursal = $total_Saldo_Sucursal = 0;			
							$i = 0;
							$prin_ven = $prin_clien = false;
						?>
					<?php endif; ?>

					<?php if ($sale['cod_seller'] != $vendedor_old): ?>
						<?php if ($i > 1): ?>
							<tr class="active" > 
								<td ></td>
								<td></td>
								<td class="text-right">SubTotal Vendedor </td>
								<td class="text-right">$ <?php echo number_format($total_total,0,'','.');?></td>
								<td class="text-right">$ <?php echo number_format($total_val_pago,0,'','.');?></td>
								<td class="text-right">$ <?php echo number_format($total_Saldo,0,'','.');?></td>
							</tr>
						<?php endif; ?>
						<tr class="active"> 
							<td ><b>Vendedor: </b><?php echo $sale['nom_seller']; ?></td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ></td>
						</tr>
						<?php if ($sale['documento'] == $nit_old): ?>
							<tr class="active">
								<td style="border-right: 1px solid transparent;">
									<b> Cliente:</b> <?php echo $sale['nom_companie']; ?> 
								</td>
								<td style="border-right: 1px solid transparent;">
									<b>NIT:</b> <?php echo $sale['documento']; ?>
								</td>
								<td style="border-right: 1px solid transparent;">
									
								</td>
								<td ></td>
								<td ></td>
								<td ></td>
							</tr>
							<?php $total_total = $total_val_pago = $total_Saldo = 0;  ?>
						<?php endif; ?>
					<?php endif; ?>

					<?php if ($sale['documento'] != $nit_old): ?>
						<?php if ($prin_clien == false): ?>
							<?php if ($sale['cod_seller'] == $vendedor_old): ?>
								<tr > 
									<td ></td>
									<td></td>
									<td class="text-right" >SubTotal Cliente </td>
									<td class="text-right">$ <?php echo number_format($total,0,'','.');?></td>
									<td class="text-right">$ <?php echo number_format($val_pago,0,'','.');?></td>
									<td class="text-right">$ <?php echo number_format($Saldo,0,'','.');?></td>
								</tr>
								<?php $total = $val_pago = $Saldo = 0;		?>
							<?php endif; ?>
						<?php endif; ?>
						<tr class="active">
							<td style="border-right: 1px solid transparent;">
								<b> Cliente:</b> <?php echo $sale['nom_companie']; ?> 
							</td>
							<td style="border-right: 1px solid transparent;">
								<b>NIT:</b> <?php echo $sale['documento']; ?>
							</td>
							<td style="border-right: 1px solid transparent;">
								
							</td>
							<td ></td>
							<td ></td>
							<td ></td>
						</tr>
						<?php $total_total = $total_val_pago = $total_Saldo = 0;  ?>
					<?php endif; ?>

					<?php $sucursal_old = $sale['cod_Sucursal']; ?>
					<?php $vendedor_old = $sale['cod_seller']; ?>
					<?php $nit_old = $sale['documento']; ?>
					<?php 
						$fecha = substr($sale['Fecha_Creacion'],0,10);
						$fecha_now = date('Y-m-d');
						$dias	= (strtotime($fecha)-strtotime($fecha_now))/86400;
						$dias 	= abs($dias); 
						$dias 	= floor($dias);
					?>
					<tr >
						<td scope="row"><?php echo $sale['Referencia']; ?></td>
						<td class="text-center"><?php echo substr($sale['Fecha_Creacion'],0,10); ?></td>
						<td class="text-center"><?php echo $dias; ?></td>
						<td class="text-right">$ <?php echo number_format($sale['total'],0,'','.'); ?></td>
						<td class="text-right">$ <?php echo number_format($sale['val_pago'],0,'','.'); ?></td>
						<td class="text-right">$ <?php echo number_format($sale['Saldo'],0,'','.'); ?></td>
					</tr>
					<?php 
						$total +=  $sale['total'];
						$val_pago += $sale['val_pago'];
						$Saldo += $sale['Saldo'];	
						$total_total_Sucursal += $sale['total'];
						$total_val_pago_Sucursal += $sale['val_pago'];
						$total_Saldo_Sucursal +=  $sale['Saldo'];	
						$total_total += $sale['total'];
						$total_val_pago += $sale['val_pago'];
						$total_Saldo +=  $sale['Saldo'];				
					?>
				<?php endforeach; ?>

				<tr > 
					<td ></td>
					<td ></td>
					<td class="text-right" >SubTotal Cliente </td>
					<td class="text-right">$ <?php echo number_format($total,0,'','.');?></td>
					<td class="text-right">$ <?php echo number_format($val_pago,0,'','.');?></td>
					<td class="text-right">$ <?php echo number_format($Saldo,0,'','.');?></td>
				</tr>
				<tr class="active" > 
					<td ></td>
					<td></td>
					<td class="text-right">SubTotal Vendedor </td>
					<td class="text-right">$ <?php echo number_format($total_total,0,'','.');?></td>
					<td class="text-right">$ <?php echo number_format($total_val_pago,0,'','.');?></td>
					<td class="text-right">$ <?php echo number_format($total_Saldo,0,'','.');?></td>
				</tr>
				<tr class="active" > 
					<td ></td>
					<td></td>
					<td class="text-right">SubTotal Sucursal </td>
					<td class="text-right">$ <?php echo number_format($total_total_Sucursal,0,'','.');?></td>
					<td class="text-right">$ <?php echo number_format($total_val_pago_Sucursal,0,'','.');?></td>
					<td class="text-right">$ <?php echo number_format($total_Saldo_Sucursal,0,'','.');?></td>
				</tr>
			</tbody>
		</table>
	</div>