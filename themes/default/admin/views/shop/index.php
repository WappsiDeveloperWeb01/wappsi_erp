<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php $attrib = array('class' => 'wizard-big wizard', 'id' => 'shop_settings_form');
                    echo admin_form_open_multipart("shop_settings", $attrib);
                    ?>
                    <h1><?= lang('settings') ?></h1>
                    <section>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('ecommerce_active', 'ecommerce_active'); ?>
                                <?php $popts = [0 => lang('no'), 1 => lang('yes')]; ?>
                                <?= form_dropdown('ecommerce_active', $popts, set_value('ecommerce_active', $shop_settings->ecommerce_active), 'class="form-control tip" id="ecommerce_active" required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('biller', 'biller'); ?>
                                <?php
                                $bl[''] = lang('select').' '.lang('biller');
                                if ($billers) {
                                    foreach ($billers as $biller) {
                                        $bl[$biller->biller_id] = $biller->company && $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                }
                                ?>
                                <?= form_dropdown('biller', $bl, set_value('biller', $shop_settings->biller), 'class="form-control tip" id="biller"  required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('products_page', 'products_page'); ?>
                                <?php $popts = [0 => lang('leave_gap'), 1 => lang('re_arrange')]; ?>
                                <?= form_dropdown('products_page', $popts, set_value('products_page', $shop_settings->products_page), 'class="form-control tip" id="products_page" required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('about_link', 'about_link'); ?>
                                <?php
                                $pgs[''] = lang('select').' '.lang('page');
                                if ($pages) {
                                     foreach ($pages as $page) {
                                        $pgs[$page->slug] = $page->title;
                                    }
                                }
                                ?>
                                <?= form_dropdown('about_link', $pgs, set_value('about_link', $shop_settings->about_link), 'class="form-control tip" id="about_link"  required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('terms_link', 'terms_link'); ?>
                                <?= form_dropdown('terms_link', $pgs, set_value('terms_link', $shop_settings->terms_link), 'class="form-control tip" id="terms_link"  required="required"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('privacy_link', 'privacy_link'); ?>
                                <?= form_dropdown('privacy_link', $pgs, set_value('privacy_link', $shop_settings->privacy_link), 'class="form-control tip" id="privacy_link"  required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('contact_link', 'contact_link'); ?>
                                <?= form_dropdown('contact_link', $pgs, set_value('contact_link', $shop_settings->contact_link), 'class="form-control tip" id="contact_link"  required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('payment_text', 'payment_text'); ?>
                                <?= form_input('payment_text', set_value('payment_text', $shop_settings->payment_text), 'class="form-control tip" id="payment_text" required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('follow_text', 'follow_text'); ?>
                                <?= form_input('follow_text', set_value('follow_text', $shop_settings->follow_text), 'class="form-control tip" id="follow_text" required="required"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('facebook', 'facebook'); ?>
                                <?= form_input('facebook', set_value('facebook', $shop_settings->facebook), 'class="form-control tip" id="facebook" required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('twitter', 'twitter'); ?>
                                <?= form_input('twitter', set_value('twitter', $shop_settings->twitter), 'class="form-control tip" id="twitter"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('google_plus', 'google_plus'); ?>
                                <?= form_input('google_plus', set_value('google_plus', $shop_settings->google_plus), 'class="form-control tip" id="google_plus"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('instagram', 'instagram'); ?>
                                <?= form_input('instagram', set_value('instagram', $shop_settings->instagram), 'class="form-control tip" id="instagram"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('cookie_message', 'cookie_message'); ?>
                                <?= form_input('cookie_message', set_value('cookie_message', $shop_settings->cookie_message), 'class="form-control tip" id="cookie_message"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('cookie_link', 'cookie_link'); ?>
                                <?= form_dropdown('cookie_link', $pgs, set_value('cookie_link', $shop_settings->cookie_link), 'class="form-control tip" id="cookie_link"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('hide0', 'hide0'); ?>
                                <?php $yn = [0 => lang('no'), 1 => lang('yes')]; ?>
                                <?= form_dropdown('hide0', $yn, set_value('hide0', $shop_settings->hide0), 'class="form-control tip" id="hide0" required="required"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('sale_destination', 'sale_destination'); ?>
                                <?php $yn = [1 => lang('sale'), 2 => lang('order_sale')]; ?>
                                <?= form_dropdown('sale_destination', $yn, set_value('sale_destination', $shop_settings->sale_destination), 'class="form-control tip" id="sale_destination" required="required"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?= form_label(lang('activate_available_from_quantity'), 'activate_available_from_quantity') ?>
                                    <?= form_input(['name'=>'activate_available_from_quantity', 'id'=>'activate_available_from_quantity', 'class'=>'form-control', 'type'=>'number', 'value'=>$shop_settings->activate_available_from_quantity, 'min'=>1, 'required'=>true]) ?>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <?= form_label(lang('update_promotions_to_store'), 'update_promotions_to_store') ?>
                                    <?php $yn = [NOT => lang('no'), YES => lang('yes')]; ?>
                                    <?= form_dropdown('update_promotions_to_store', $yn, set_value('sale_deupdate_promotions_to_storestination', $shop_settings->update_promotions_to_store), 'class="form-control tip" id="update_promotions_to_store" required="required"'); ?>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <?= form_label(lang("api_key"), "api_key_hash") ?>
                                    <?= form_password(["name"=>"api_key_hash", "id"=>"api_key_hash","class"=>"form-control"], $shop_settings->api_key_hash) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <?= lang('bank_details', 'bank_details'); ?> <?= lang('bank_details_tip'); ?>
                                <?= form_textarea('bank_details', $shop_settings->bank_details, 'class="form-control tip" id="bank_details"'); ?>
                            </div>
                        </div>
                    </section>
                    <h1><?= lang('options') ?></h1>
                    <section>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('show_product_quantity', 'show_product_quantity') ?>
                                <br>
                                <select name="show_product_quantity" id="show_product_quantity" class="form-control">
                                    <option value="0" <?= $shop_settings->show_product_quantity == 0 ? 'selected="selected"' : '' ?>><?= lang('no_show_product_quantity') ?></option>
                                    <option value="1" <?= $shop_settings->show_product_quantity == 1 ? 'selected="selected"' : '' ?>><?= lang('show_biller_warehouse_product_quantity') ?></option>
                                    <option value="2" <?= $shop_settings->show_product_quantity == 2 ? 'selected="selected"' : '' ?>><?= lang('show_all_warehouses_product_quantity') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_product_zero_price', 'show_product_zero_price') ?>
                                <select name="show_product_zero_price" id="show_product_zero_price" class="form-control">
                                    <option value="0" <?= $shop_settings->show_product_zero_price == 0 ? 'selected="selected"' : '' ?>><?= lang('hide') ?></option>
                                    <option value="1" <?= $shop_settings->show_product_zero_price == 1 ? 'selected="selected"' : '' ?>><?= lang('show') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_product_zero_quantity', 'show_product_zero_quantity') ?>
                                <select name="show_product_zero_quantity" id="show_product_zero_quantity" class="form-control">
                                    <option value="0" <?= $shop_settings->show_product_zero_quantity == 0 ? 'selected="selected"' : '' ?>><?= lang('hide') ?></option>
                                    <option value="1" <?= $shop_settings->show_product_zero_quantity == 1 ? 'selected="selected"' : '' ?>><?= lang('show') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_categories_without_products', 'show_categories_without_products') ?>
                                <select name="show_categories_without_products" id="show_categories_without_products" class="form-control">
                                    <option value="0" <?= $shop_settings->show_categories_without_products == 0 ? 'selected="selected"' : '' ?>><?= lang('hide') ?></option>
                                    <option value="1" <?= $shop_settings->show_categories_without_products == 1 ? 'selected="selected"' : '' ?>><?= lang('show') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('overselling', 'overselling') ?>
                                <select name="overselling" id="overselling" class="form-control">
                                    <option value="0" <?= $shop_settings->overselling == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->overselling == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('use_product_variants', 'use_product_variants') ?>
                                <select name="use_product_variants" id="use_product_variants" class="form-control">
                                    <option value="0" <?= $shop_settings->use_product_variants == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->use_product_variants == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('use_product_preferences', 'use_product_preferences') ?>
                                <select name="use_product_preferences" id="use_product_preferences" class="form-control">
                                    <option value="0" <?= $shop_settings->use_product_preferences == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->use_product_preferences == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_categories_buttons', 'show_categories_buttons') ?>
                                <select name="show_categories_buttons" id="show_categories_buttons" class="form-control">
                                    <option value="0" <?= $shop_settings->show_categories_buttons == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->show_categories_buttons == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="row">


                            <div class="form-group col-md-3">
                                <?= lang('show_featured_products', 'show_featured_products') ?>
                                <select name="show_featured_products" id="show_featured_products" class="form-control">
                                    <option value="0" <?= $shop_settings->show_featured_products == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->show_featured_products == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_most_selled_products', 'show_most_selled_products') ?>
                                <select name="show_most_selled_products" id="show_most_selled_products" class="form-control">
                                    <option value="0" <?= $shop_settings->show_most_selled_products == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->show_most_selled_products == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_other_products', 'show_other_products') ?>
                                <select name="show_other_products" id="show_other_products" class="form-control">
                                    <option value="0" <?= $shop_settings->show_other_products == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->show_other_products == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_promotion_products', 'show_promotion_products') ?>
                                <select name="show_promotion_products" id="show_promotion_products" class="form-control">
                                    <option value="0" <?= $shop_settings->show_promotion_products == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->show_promotion_products == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('show_brands_buttons', 'show_brands_buttons') ?>
                                <select name="show_brands_buttons" id="show_brands_buttons" class="form-control">
                                    <option value="0" <?= $shop_settings->show_brands_buttons == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->show_brands_buttons == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_new_products', 'show_new_products') ?>
                                <select name="show_new_products" id="show_new_products" class="form-control">
                                    <option value="0" <?= $shop_settings->show_new_products == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->show_new_products == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('show_favorite_products', 'show_favorite_products') ?>
                                <select name="show_favorite_products" id="show_favorite_products" class="form-control">
                                    <option value="0" <?= $shop_settings->show_favorite_products == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->show_favorite_products == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('banners_transition_seconds', 'banners_transition_seconds') ?>
                                <input type="text" name="banners_transition_seconds" id="banners_transition_seconds" class="form-control only_number" value="<?= $shop_settings->banners_transition_seconds ?>" min="5">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('item_frame_rounding', 'item_frame_rounding') ?>
                                <select name="item_frame_rounding" id="item_frame_rounding" class="form-control">
                                    <option value="0" <?= $shop_settings->item_frame_rounding == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->item_frame_rounding == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('item_margin', 'item_margin') ?>
                                <select name="item_margin" id="item_margin" class="form-control">
                                    <option value="0" <?= $shop_settings->item_margin == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $shop_settings->item_margin == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                        </div>
                    </section>
                    <h1><?= lang('personalization') ?></h1>
                    <section>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <em>Usar formato hexadecimal e iniciando con el símbolo #, ejemplo : #FFFFF00</em>
                                <br>
                                <em>Haga click sobre el texto para abrir el elemento de selección de color</em>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('primary_color', 'primary_color') ?>
                                <div class="input-group">
                                    <input type="text" name="primary_color" id="primary_color" value="<?= $shop_settings->primary_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon primary_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('secondary_color', 'secondary_color') ?>
                                <div class="input-group">
                                    <input type="text" name="secondary_color" id="secondary_color" value="<?= $shop_settings->secondary_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon secondary_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('navbar_color', 'navbar_color') ?>
                                <div class="input-group">
                                    <input type="text" name="navbar_color" id="navbar_color" value="<?= $shop_settings->navbar_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon navbar_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('navbar_text_color', 'navbar_text_color') ?>
                                <div class="input-group">
                                    <input type="text" name="navbar_text_color" id="navbar_text_color" value="<?= $shop_settings->navbar_text_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon navbar_text_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('bar_menu_color', 'bar_menu_color') ?>
                                <div class="input-group">
                                    <input type="text" name="bar_menu_color" id="bar_menu_color" value="<?= $shop_settings->bar_menu_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon bar_menu_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('bar_menu_text_color', 'bar_menu_text_color') ?>
                                <div class="input-group">
                                    <input type="text" name="bar_menu_text_color" id="bar_menu_text_color" value="<?= $shop_settings->bar_menu_text_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon bar_menu_text_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('header_color', 'header_color') ?>
                                <div class="input-group">
                                    <input type="text" name="header_color" id="header_color" value="<?= $shop_settings->header_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon header_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('background_page_color', 'background_page_color') ?>
                                <div class="input-group">
                                    <input type="text" name="background_page_color" id="background_page_color" value="<?= $shop_settings->background_page_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon background_page_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('font_text_color', 'font_text_color') ?>
                                <div class="input-group">
                                    <input type="text" name="font_text_color" id="font_text_color" value="<?= $shop_settings->font_text_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon font_text_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('footer_color', 'footer_color') ?>
                                <div class="input-group">
                                    <input type="text" name="footer_color" id="footer_color" value="<?= $shop_settings->footer_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon footer_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('footer_text_color', 'footer_text_color') ?>
                                <div class="input-group">
                                    <input type="text" name="footer_text_color" id="footer_text_color" value="<?= $shop_settings->footer_text_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon footer_text_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('header_message_color', 'header_message_color') ?>
                                <div class="input-group">
                                    <input type="text" name="header_message_color" id="header_message_color" value="<?= $shop_settings->header_message_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon header_message_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('promotion_text_color', 'promotion_text_color') ?>
                                <div class="input-group">
                                    <input type="text" name="promotion_text_color" id="promotion_text_color" value="<?= $shop_settings->promotion_text_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon promotion_text_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('favorite_color', 'favorite_color') ?>
                                <div class="input-group">
                                    <input type="text" name="favorite_color" id="favorite_color" value="<?= $shop_settings->favorite_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon favorite_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('icons_color', 'icons_color') ?>
                                <div class="input-group">
                                    <input type="text" name="icons_color" id="icons_color" value="<?= $shop_settings->icons_color ?>" class="form-control personalization_color">
                                    <span class="input-group-addon icons_color_addon" id="basic-addon3"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('font_family', 'font_family') ?>
                                <select name="font_family" id="font_family" class="form-control">
                                    <option value="Roboto" <?= $shop_settings->font_family == "Roboto" ? 'selected="selected"' : '' ?>>Roboto</option>
                                    <option value="Arial" <?= $shop_settings->font_family == "Arial" ? 'selected="selected"' : '' ?>>Arial</option>
                                    <option value="Times" <?= $shop_settings->font_family == "Times" ? 'selected="selected"' : '' ?>>Times</option>
                                    <option value="Verdana" <?= $shop_settings->font_family == "Verdana" ? 'selected="selected"' : '' ?>>Verdana</option>
                                    <option value="Poppins" <?= $shop_settings->font_family == "Poppins" ? 'selected="selected"' : '' ?>>Poppins</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <?= lang('product_view', 'product_view') ?>
                                <select name="product_view" id="product_view" class="form-control">
                                    <option value="1" <?= $shop_settings->product_view == "1" ? 'selected="selected"' : '' ?>>Horizontal</option>
                                    <option value="2" <?= $shop_settings->product_view == "2" ? 'selected="selected"' : '' ?>>Vertical</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang("shop_favicon", "shop_favicon") ?>
                                <input id="shop_favicon" type="file" data-browse-label="<?= lang('browse'); ?>" name="shop_favicon" data-show-upload="false"
                                       data-show-preview="false" accept="image/*" class="form-control file">
                            </div>
                        </div>
                    </section>
                    <h1><?= lang('social_networks') ?></h1>
                    <section>
                        <?php $arr_sn = json_decode($shop_settings->manage_social_networks); ?>
                        <?php foreach ($arr_sn as $arr_sn_key => $arr_sn_data): ?>
                            <div class="col-sm-12 row">
                                <div class="col-sm-2">
                                    <?= lang('name', 'sn_name_'.$arr_sn_key) ?>
                                    <input type="text" class="form-control" id="sn_name_<?= $arr_sn_key ?>" name="sn_name[<?= $arr_sn_key ?>]" value="<?= $arr_sn_data->name ?>">
                                </div>
                                <div class="col-sm-3">
                                    <?= lang('url', 'sn_url_'.$arr_sn_key) ?>
                                    <input type="text" class="form-control" id="sn_url_<?= $arr_sn_key ?>" name="sn_url[<?= $arr_sn_key ?>]" value="<?= $arr_sn_data->url ?>">
                                </div>
                                <div class="col-sm-2">
                                    <?= lang('color', 'sn_color_'.$arr_sn_key) ?>
                                    <input type="text" class="form-control personalization_color" id="sn_color_<?= $arr_sn_key ?>" name="sn_color[<?= $arr_sn_key ?>]" value="<?= $arr_sn_data->color ?>">
                                </div>
                                <div class="col-sm-2">
                                    <?= lang('status', 'sn_status_'.$arr_sn_key) ?>
                                    <br>
                                    <label>
                                        <input type="radio" class="form-control" id="sn_status_<?= $arr_sn_key ?>_1" name="sn_status[<?= $arr_sn_key ?>]" value="1" <?= ($arr_sn_data->status == 1 ? 'checked' : '') ?>>
                                        <?= lang('active') ?>
                                    </label>
                                    <label>
                                        <input type="radio" class="form-control" id="sn_status_<?= $arr_sn_key ?>_1" name="sn_status[<?= $arr_sn_key ?>]" value="0" <?= ($arr_sn_data->status == 0 ? 'checked' : '') ?>>
                                        <?= lang('inactive') ?>
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <?= lang('icon', 'sn_icon_'.$arr_sn_key) ?>
                                    <?php
                                      $biller_logos[''] = '';
                                      foreach ($logo_files as $key => $value) {
                                          $biller_logos[$value] = $value;
                                      }
                                      echo form_dropdown('sn_icon['.$arr_sn_key.']', $biller_logos, $arr_sn_data->icon, 'class="form-control not_select" id="sn_icon_'.$arr_sn_key.'" required="required" '); ?>
                                </div>
                            </div>
                            <hr class="col-sm-11">
                        <?php endforeach ?>
                    </section>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>

<!--
<div class="row">
    <div class="col-md-12">
        <?php if ( ! DEMO ) { ?>
        <a href="<?= admin_url('shop_settings/slugify'); ?>" class="btn btn-default pull-right"><?= lang('auto_slugify'); ?></a>
        <?php } ?>
    </div>
</div>
<div class="row">
    <?php if ( ! DEMO ) { ?>
        <div class="col-md-12">
            <div class="form-inline well well-sm">
                <div class="form-group col-sm-12">
                    <div class="row">
                        <label for="sitemap" class="col-sm-2 control-label" style="margin-top:8px;"><?= lang("sitemap"); ?></label>
                        <div class="col-sm-10">
                            <div class="input-group col-sm-12">
                                <input type="text" class="form-control" value="<?= base_url('sitemap.xml'); ?>" readonly>
                                <a href="<?= base_url('sitemap.xml'); ?>" target="_blank" class="input-group-addon btn btn-primary" id="basic-addon2"><?=lang('visit');?></a>
                                <a href="<?= admin_url('shop_settings/sitemap'); ?>" target="_blank" class="input-group-addon btn btn-warning" id="basic-addon2"><?=lang('update');?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="well well-sm" style="margin-bottom:0;">
                <p><?= lang('call_back_heading'); ?></p>
                <p class="text-info">
                    <code><?= site_url('social_auth/endpoint?hauth_done=XXXXXX'); ?></code><br>
                    <code><?= base_url('index.php/social_auth/endpoint?hauth_done=XXXXXX'); ?></code>
                </p>
                <p><?= lang('replace_xxxxxx_with_provider'); ?></p>
                <p><strong><?= lang('enable_config_file'); ?></strong></p>
                <p><code>app/config/hybridauthlib.php</code></p>
                <p><?= lang('documentation_at'); ?>: <a href="http://hybridauth.github.io/hybridauth/userguide.html" target="_blank">http://hybridauth.github.io/hybridauth/userguide.html</a></p>
            </div>
        </div>
    <?php } ?>
</div>
-->
</div>

<script type="text/javascript">

    $(document).ready(function(){
        setTimeout(function() {
            $('.personalization_color').minicolors();
        }, 1200);

        var form = $("#shop_settings_form");
        var runValid = 1;

        wizard = form.steps({
            headerTag: "h1",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
             labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: '<i class="fa-solid fa-check" data-toggle="tooltip" title="Guardar" data-placement="top"></i>',
                next: '<i class="fa-solid fa-angles-right" data-toggle="tooltip" title="Siguiente" data-placement="top"></i>',
                previous: '<i class="fa-solid fa-angles-left"  data-toggle="tooltip" title="Anterior" data-placement="top"></i>',
                loading: "Loading ..."
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                if (runValid == 1) {
                  form.validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                  return form.valid();
                } else {
                  return true;
                }
            },
            onStepChanged: function(event) {
              $('section:not(:hidden) select[required="required"]').addClass('validate');
              setTimeout(function() {
                $('select:not(:hidden)').select2();
              }, 300);
            },
            onFinishing: function (event, currentIndex)
            {
                if (runValid == 1) {
                  form.validate().settings.ignore = ":disabled";
                  return form.valid();
                } else {
                  return true;
                }
            },
            onFinished: function (event, currentIndex)
            {
              if (runValid == 1) {
                form.validate().settings.ignore = ":disabled";
                if (form.valid()) {
                  $(form).submit();
                }
              }
            }
        });
        $('section:not(:hidden) select[required="required"]').addClass('validate');
        $('select:not(:hidden)').select2();
        $('.personalization_color').trigger('change');


        $(".wizard-big").on('change', '.personalization_color', function(){
            color = $(this).val();
            addon = $(this).prop('id')+'_addon';
            $('.'+addon).css('background-color', ''+color);
        });
    });



</script>