<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php $post = !empty($this->session->flashdata('post')) ? (object) $this->session->flashdata('post') : null; ?>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-content">
                    <?= admin_form_open('documentsReception/save', ['id'=>'addDocumentForm']) ?>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= form_label($this->lang->line('document_number'), 'documentId'); ?>
                            <?php $documentId = !empty($post) ? $post->documentId: ''; ?>
                            <?= form_input(['name'=>'documentId', 'id'=>'documentId', 'class'=>'form-control', 'required'=>true],  $documentId); ?>
                        </div>

                        <div class="col-lg-3">
                            <?= form_label($this->lang->line('uuid'), 'uuid'); ?>
                            <?php $uuid = !empty($post) ? $post->uuid: ''; ?>
                            <?= form_input(['name'=>'uuid', 'id'=>'uuid', 'class'=>'form-control', 'required'=>true], $uuid); ?>
                        </div>

                        <div class="col-lg-3">
                            <?= form_label($this->lang->line('supplierNit'), 'supplierNit'); ?>
                            <?php $supplierNit = !empty($post) ? $post->supplierNit: ''; ?>
                            <?= form_input(['name'=>'supplierNit', 'id'=>'supplierNit', 'class'=>'form-control', 'required'=>true], $supplierNit); ?>
                        </div>

                        <div class="col-lg-3">
                            <?= form_label($this->lang->line('receiverNit'), 'receiverNit'); ?>
                            <?php $receiverNit = !empty($post) ? $post->receiverNit: ''; ?>
                            <?= form_input(['name'=>'receiverNit', 'id'=>'receiverNit', 'class'=>'form-control', 'required'=>true], $receiverNit); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3">
                            <?= form_label($this->lang->line('invoice_register_date'), 'documentDate'); ?>
                            <?php $documentDate = !empty($post) ? $post->documentDate: ''; ?>
                            <?= form_input(['name'=>'documentDate', 'id'=>'documentDate', 'type'=>'date', 'class'=>'form-control', 'required'=>true], $documentDate); ?>
                        </div>

                        <div class="col-lg-3">
                            <?= form_label($this->lang->line('quote_purchase'), 'orderReference'); ?>
                            <?php $orderReference = !empty($post) ? $post->orderReference: ''; ?>
                            <?= form_input(['name'=>'orderReference', 'id'=>'orderReference', 'class'=>'form-control'], $orderReference); ?>
                        </div>

                        <div class="col-lg-3">
                            <?= form_label($this->lang->line('email_address'), 'electronicMail'); ?>
                            <?php $electronicMail = !empty($post) ? $post->electronicMail: ''; ?>
                            <?= form_input(['name'=>'electronicMail', 'id'=>'electronicMail', 'type'=>'mail', 'class'=>'form-control', 'required'=>true], $electronicMail); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <?= form_button(['id'=>'saveDocument', 'class'=>'btn btn-primary', 'style'=>'margin-top: 15px'], $this->lang->line('save')) ?>
                        </div>
                    </div>

                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        form = $('#addDocumentForm');

        form.validate({
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem[0].tagName == "SELECT") {
                    $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                    $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                } else {
                    elem.addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem[0].tagName == "SELECT") {
                    $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                    $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                } else {
                    elem.removeClass(errorClass);
                }
            }
        }).settings.ignore = ":disabled,:hidden:not(.validate)";

        $(document).on('click', '#saveDocument', function() { saveDocument(); });
    });

    function saveDocument()
    {
        if(form.valid()){
            $(form).submit();
        }
    }

</script>