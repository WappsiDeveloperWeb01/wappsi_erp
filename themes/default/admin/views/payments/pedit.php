<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var ppayment_edit = true;
    var purchase_id = false;
    var biller_id = "<?= $enc->biller_id ?>";
    var supplier_id = "<?= $enc->supplier_id ?>";
    var ptype = '<?= $enc->purchase_type ?>';
    // ----
    var document_type_id = '<?= $enc->document_type_id ?>';
    var payment_date = '<?= $enc->payment_date ?>';
    var ppdate = '<?= $enc->date ?>';
    var pp_reference_no = '<?= $enc->reference_no ?>';
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('edit_multi_payment_purchases'); ?></h2>
    </div>
</div>
<style type="text/css">
    input[placeholder] { width: 100% !important; }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php $attrib = array('id' => 'edit_multi_payment_purchases', 'target' => '_blank');
                    echo admin_form_open_multipart("payments/padd", $attrib); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?= lang('date', 'date') ?>
                                <input type="text" name="date" id="date" class="form-control" value="<?= date('Y-m-d') ?>">
                            </div>
                            <div class="col-md-3 form-group">
                                <?= lang("biller", "rcbiller"); ?>
                                <?php
                                $bl = [];
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller;
                                }
                                ?>
                                <select name="biller" class="form-control" id="rcbiller" required="required">
                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                        <?php foreach ($billers as $biller): ?>
                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endforeach ?>
                                    <?php } else { ?>
                                        <?php $biller = $bl[$this->session->userdata('biller_id')]; ?>
                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <?= lang('purchase_type_payment', 'purchase_type') ?>
                                <select name="purchase_type" id="purchase_type" class="form-control">
                                    <option value="1"><?= lang('purchase_type_purchase') ?></option>
                                    <option value="2"><?= lang('purchase_type_expense') ?></option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <?php if ($this->Settings->manual_payment_reference == 1): ?>
                                    <div class="form-group">
                                        <?= lang("reference_no", "document_type_id"); ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="manual_reference" id="manual_reference" class="form-control" placeholder="<?= lang('type_manual_reference') ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="form-group">
                                        <?= lang("reference_no", "document_type_id"); ?>
                                        <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="form-group col-sm-3">
                                <?= lang('supplier', 'supplier') ?>
                                <?php
                                echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ""), 'id="supplier" data-placeholder="' . lang("select") . ' ' . lang("supplier") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                ?>
                            </div>
                            <div class="form-group col-sm-3">
                                <?= lang('payment_distribution', 'payment_distribution') ?>
                                <input type="text" name="payment_distribution" id="payment_distribution" class="form-control number_mask">
                            </div>

                            <div class="form-group col-sm-3">
                                <?= lang('supplier_portfolio', 'supplier_portfolio') ?>
                                <input type="text" name="supplier_portfolio" id="supplier_portfolio" class="form-control number_mask" readonly>
                            </div>
                          <?php if (isset($cost_centers)): ?>
                                <div class="form-group col-sm-3">
                                    <?= lang('cost_center', 'cost_center_id') ?>
                                    <?php
                                    $ccopts[''] = lang('select');
                                    if ($cost_centers) {
                                        foreach ($cost_centers as $cost_center) {
                                            $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                        }
                                    }
                                     ?>
                                     <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                </div>
                            <?php endif ?>

                            <div class="form-group col-sm-3" style="padding-top: 1.6%;">
                              <button class="btn btn-primary" id="debug" type="button"> <span class="fa fa-trash"></span> <?= lang('delete_and_refresh') ?></button>
                            </div>

                          <hr class="col-sm-11">

                          <div class="form-group col-sm-3">
                              <?= lang('payment_date', 'payment_date') ?><br>
                              <input type="text" name="payment_date" id="payment_date" class="form-control datetime">
                          </div>

                          <div class="form-group col-sm-3">
                              <?= lang('consecutive_payment', 'consecutive_payment') ?><br>
                              <input type="text" name="consecutive_payment" id="consecutive_payment" class="form-control">
                          </div>

                          <div class="form-group col-sm-3">
                            <?= lang("attachment", "attachment") ?>
                            <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false" class="form-control file">
                          </div>
                          
                          <table class="table col-sm-12" id="table_purchases">
                            <thead>
                              <tr>
                                <th style="width: 7.5% ;"><?= lang('reference_no') ?></th>
                                <th style="width: 7.5% ;"><?= lang('affects_to') ?></th>
                                <th style="width: 13% ;"><?= lang('date') ?></th>
                                <th style="width: 13% ;"><?= lang('due_date') ?></th>
                                <th style="width: 12.2% ;"><?= lang('total') ?></th>
                                <th style="width: 12.2% ;"><?= lang('paid') ?></th>
                                <th style="width: 12.2% ;"><?= lang('balance') ?></th>
                                <th style="width: 12.2% ;"><?= lang('amount_to_paid') ?></th>
                                <th style="width: 15.2% ;"><?= lang('retention') ?></th>
                                <th style="width: 2.5% ;"><input type="checkbox" class="check_all skip"></th>
                              </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                              <tr>
                                <th><?= lang('reference_no') ?></th>
                                <th style="width: 7.5% ;"><?= lang('affects_to') ?></th>
                                <th><?= lang('date') ?></th>
                                <th><?= lang('due_date') ?></th>
                                <th><?= lang('total') ?></th>
                                <th><?= lang('paid') ?></th>
                                <th><?= lang('balance') ?></th>
                                <th><?= lang('amount_to_paid') ?></th>
                                <th><?= lang('retention') ?></th>
                                <th></th>
                              </tr>
                            </tfoot>
                          </table>
                        <?php if (1==1): ?>
                        <div class="col-sm-12">
                            <label><?= lang('other_concepts') ?></label>
                            <table class="table" id="table_concepts" width="100%" style="table-layout: fixed !important;">
                                <?php
                                    $lopts[''] = lang('select');
                                    if (isset($ledgers)) {
                                        foreach ($ledgers as $ledger) {
                                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                                        }
                                    }

                                    $type = array(
                                                    '' => lang('select'), 
                                                    '+' => lang('addition'), 
                                                    '-' => lang('substraction')
                                                );
                                 ?>
                                <thead>
                                    <tr>
                                        <th style="width: 14.28%;max-width: 14.28%;"></th>
                                        <th style="width: 23.56%;max-width: 23.56%;">
                                            <?= lang('description') ?>
                                        </th>
                                        <th style="width: 14.28%;max-width: 14.28%;">
                                            <?= lang('amount') ?>
                                        </th>
                                        <th style="width: 14.28%;max-width: 14.28%;">
                                            <?= lang('base') ?>
                                        </th>
                                        <?php if ($this->Settings->modulary): ?>
                                            <th style="width: 14.28%;max-width: 14.28%;">
                                                <?= lang('ledger') ?>
                                            </th>
                                        <?php endif ?>
                                        <th style="width: 14.28%;max-width: 14.28%;">
                                            <?= lang('supplier') ?>
                                        </th>
                                        <th style="width: 5%;">
                                            <button class="btn btn-default btn-sm btn-outline add_concept" type="button"><span class="fa fa-plus"></span></button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                        <?php endif ?>
                        <div id="payments">
                            <div class="col-md-12">
                                <div class="well well-sm well_1">
                                    <?php if ($this->Settings->purchase_payment_affects_cash_register == 1): ?>
                                        <p><b><?= lang('note') ?> :</b> <?= lang('affects_register_note') ?>.</p>
                                        <?= form_hidden('payment_affects_register', '1'); ?>
                                    <?php elseif ($this->Settings->purchase_payment_affects_cash_register == 0): ?>
                                        <?= form_hidden('payment_affects_register', '0'); ?>
                                    <?php else: ?>
                                      <div class="col-sm-3" id="div_affects_register">
                                          <?= lang('payment_affects_register', 'payment_affects_register') ?><br>
                                          <label>
                                              <input type="radio" name="payment_affects_register" id="payment_affects_register_yes" value="1">
                                              <?= lang('affects_register') ?>
                                          </label>
                                          <label>
                                              <input type="radio" name="payment_affects_register" id="payment_affects_register_no" value="0">
                                              <?= lang('no_affects_register') ?>
                                          </label>
                                          <label for="payment_affects_register" generated="true" class="error">* Obligatorio</label>
                                      </div>
                                    <?php endif ?>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <?= lang("paying_by", "paid_by_1"); ?>
                                                    <select name="paid_by" id="paid_by_1" class="form-control paid_by">
                                                        <?= $this->sma->paid_opts(null, true, false, false, true); ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <?= lang('total_p_received', 'total_received') ?>
                                                <input type="text" name="total_received" id="total_received" class="form-control text-right" readonly>
                                            </div>
                                            <div class="col-sm-2">
                                                <?= lang('total_rete_amount', 'total_rete_amount_2') ?>
                                                <input type="text" name="total_rete_amount_2" id="total_rete_amount_2" class="form-control text-right" readonly>
                                            </div>
                                            <div class="col-sm-2">
                                                <?= lang('total_other_concepts', 'total_other_concepts') ?>
                                                <input type="text" name="total_other_concepts" id="total_other_concepts" class="form-control text-right" value="0" readonly>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="payment">
                                                    <div class="form-group ngc">
                                                        <?= lang("total_amount_to_ppaid", "amount_1"); ?>
                                                        <input name="amount-paid" type="text" id="amount_1"
                                                            class="pa form-control kb-pad amount_1 text-right
                                                            " readonly />
                                                    </div>
                                                    <div class="form-group gc" style="display: none;">
                                                        <?= lang("gift_card_no", "gift_card_no"); ?>
                                                        <input name="gift_card_no" type="text" id="gift_card_no"
                                                            class="pa form-control kb-pad"/>

                                                        <div id="gc_details"></div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="pcc_1" style="display:none;">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_no" type="text" id="pcc_no_1"
                                                            class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_holder" type="text" id="pcc_holder_1"
                                                            class="form-control"
                                                            placeholder="<?= lang('cc_holder') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select name="pcc_type" id="pcc_type_1"
                                                                class="form-control pcc_type"
                                                                placeholder="<?= lang('card_type') ?>">
                                                            <option value="Visa"><?= lang("Visa"); ?></option>
                                                            <option
                                                                value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                            <option value="Amex"><?= lang("Amex"); ?></option>
                                                            <option value="Discover"><?= lang("Discover"); ?></option>
                                                        </select>
                                                        <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_month" type="text" id="pcc_month_1"
                                                            class="form-control" placeholder="<?= lang('month') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <input name="pcc_year" type="text" id="pcc_year_1"
                                                            class="form-control" placeholder="<?= lang('year') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <input name="pcc_ccv" type="text" id="pcc_cvv2_1"
                                                            class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pcheque_1" style="display:none;">
                                            <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                <input name="cheque_no" type="text" id="cheque_no_1"
                                                    class="form-control cheque_no"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?= lang('payment_note', 'payment_note_1'); ?>
                                                <a href="<?= admin_url('payments/add_invoice_note') ?>" class="btn btn-primary" style="padding: 3px 8px; margin-left: 2px;" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span></a>
                                            <textarea name="payment_note" id="payment_note_1"
                                                    class="pa form-control kb-text payment_note"></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div id="deposit" style="display: none;">
                            <div class="col-md-12">
                                <?= lang('supplier_amount_distribution_balance_to_deposit', 'set_deposit') ?>
                                <label>
                                    <input type="radio" name="set_deposit" class="set_deposit_on" value="1">
                                    <?= lang('yes') ?>
                                </label>
                                <label>
                                    <input type="radio" name="set_deposit" class="set_deposit_off" value="0">
                                    <?= lang('no') ?>
                                </label>
                            </div>
                            <div class="col-md-12 div_deposit_data">
                                <div class="well well-sm well_1">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("deposit_document_type_id", "deposit_document_type_id"); ?>
                                                    <select name="deposit_document_type_id" class="form-control" id="deposit_document_type_id" required="required"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("deposit_amount", "deposit_amount"); ?>
                                                    <input type="text" name="deposit_amount_text" id="deposit_amount_text" class="form-control text-right" readonly>
                                                    <input type="hidden" name="deposit_amount" id="deposit_amount" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?= lang('payment_note', 'payment_note_1'); ?>
                                                <a href="<?= admin_url('payments/add_invoice_note') ?>" class="btn btn-primary" style="padding: 3px 8px; margin-left: 2px;" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span></a>
                                            <textarea name="deposit_note" id="deposit_note_1"
                                                    class="pa form-control kb-text deposit_note"></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?= form_hidden('add_payment', '1'); ?>
                            <span class="text-danger negative_alert" style="display: none;"><?= lang('total_received_negative') ?></span><br>
                            <button class="btn btn-primary" type="button" id="add_payment"><?= lang('submit') ?></button>
                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <!-- retencion -->
        <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                  class="fa fa-2x">&times;</i></button>
                  <h4 class="modal-title" id="rtModalLabel"><?=lang('edit_order_tax');?></h4>
                </div>
                <div class="modal-body">
                  <div class="row">

                    <div class="col-sm-12">
                        <table class="table">
                            <tr>
                                <th>
                                    <?= lang('subtotal') ?>
                                </th>
                                <td class="text-right txt-subtotal">
                                    <input type="text" id="rete_base_subtotal" class="form-control number_mask" step="0.01" readonly>
                                </td>
                                <th>
                                    <?= lang('tax') ?>
                                </th>
                                <td class="text-right txt-tax">
                                    <input type="text" id="rete_base_tax" class="form-control number_mask" step="0.01" readonly>
                                </td>
                                <th>
                                    <?= lang('total') ?>
                                </th>
                                <td class="text-right txt-total">
                                    <input type="text" id="rete_base_total" class="form-control number_mask" step="0.01" readonly>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-sm-2">
                      <label>Retención</label>
                    </div>
                    <div class="col-sm-2">
                      <label>Asumida</label>
                    </div>
                    <div class="col-sm-3">
                      <label>Opción</label>
                    </div>
                    <div class="col-sm-2">
                      <label>Porcentaje</label>
                    </div>
                    <div class="col-sm-3">
                      <label>Valor</label>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                      </label>
                    </div>
                    <div class="col-sm-1">
                      <label>
                        <input type="checkbox" name="rete_fuente_assumed" class="skip" id="rete_fuente_assumed">
                      </label>
                    </div>
                    <div class="col-sm-4">
                      <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                      <input type="hidden" name="rete_fuente_assumed_account" id="rete_fuente_assumed_account">
                      <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                      <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_fuente_valor_show" id="rete_fuente_valor_show" class="form-control text-right" readonly>
                      <input type="hidden" name="rete_fuente_valor" id="rete_fuente_valor">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                      </label>
                    </div>
                    <div class="col-sm-1">
                      <label>
                        <input type="checkbox" name="rete_iva_assumed" class="skip" id="rete_iva_assumed">
                      </label>
                    </div>
                    <div class="col-sm-4">
                      <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                      <input type="hidden" name="rete_iva_assumed_account" id="rete_iva_assumed_account">
                      <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_iva_valor_show" id="rete_iva_valor_show" class="form-control text-right" readonly>
                      <input type="hidden" name="rete_iva_valor" id="rete_iva_valor">
                      <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                      </label>
                    </div>
                    <div class="col-sm-1">
                      <label>
                        <input type="checkbox" name="rete_ica_assumed" class="skip" id="rete_ica_assumed">
                      </label>
                    </div>
                    <div class="col-sm-4">
                      <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                      <input type="hidden" name="rete_ica_assumed_account" id="rete_ica_assumed_account">
                      <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_ica_valor_show" id="rete_ica_valor_show" class="form-control text-right" readonly>
                      <input type="hidden" name="rete_ica_valor" id="rete_ica_valor">
                      <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                      </label>
                    </div>
                    <div class="col-sm-1">
                      <label>
                        <input type="checkbox" name="rete_otros_assumed" class="skip" id="rete_otros_assumed">
                      </label>
                    </div>
                    <div class="col-sm-4">
                      <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                      <input type="hidden" name="rete_otros_assumed_account" id="rete_otros_assumed_account">
                      <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_otros_valor_show" id="rete_otros_valor_show" class="form-control text-right" readonly>
                      <input type="hidden" name="rete_otros_valor" id="rete_otros_valor">
                      <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-3">
                      <label>
                        <input type="checkbox" name="rete_bomberil" class="skip" id="rete_bomberil" disabled="true"> <?= lang('rete_bomberil') ?>
                      </label>
                    </div>
                    <div class="col-sm-4">
                      <select class="form-control" name="rete_bomberil_option" id="rete_bomberil_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_bomberil_account" id="rete_bomberil_account">
                      <input type="hidden" name="rete_bomberil_assumed_account" id="rete_bomberil_assumed_account">
                      <input type="hidden" name="rete_bomberil_base" id="rete_bomberil_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_bomberil_tax" id="rete_bomberil_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_bomberil_valor_show" id="rete_bomberil_valor_show" class="form-control text-right" readonly>
                      <input type="hidden" name="rete_bomberil_valor" id="rete_bomberil_valor">
                      <input type="hidden" name="rete_bomberil_id" id="rete_bomberil_id">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-3">
                      <label>
                        <input type="checkbox" name="rete_autoaviso" class="skip" id="rete_autoaviso" disabled="true"> <?= lang('rete_autoaviso') ?>
                      </label>
                    </div>
                    <div class="col-sm-4">
                      <select class="form-control" name="rete_autoaviso_option" id="rete_autoaviso_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_autoaviso_account" id="rete_autoaviso_account">
                      <input type="hidden" name="rete_autoaviso_assumed_account" id="rete_autoaviso_assumed_account">
                      <input type="hidden" name="rete_autoaviso_base" id="rete_autoaviso_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_autoaviso_tax" id="rete_autoaviso_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_autoaviso_valor_show" id="rete_autoaviso_valor_show" class="form-control text-right" readonly>
                      <input type="hidden" name="rete_autoaviso_valor" id="rete_autoaviso_valor">
                      <input type="hidden" name="rete_autoaviso_id" id="rete_autoaviso_id">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 text-right">
                      <label>Total : </label>
                    </div>
                    <div class="col-md-4 text-right">
                      <label id="total_rete_amount"> 0.00 </label>
                    </div>
                    <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                  </div>
                  <div class="col-sm-12">
                        <table class="table">
                            <tr>
                                <th>
                                    <?= lang('suggested_subtotal') ?>
                                </th>
                                <td class="text-right">
                                    <span id='suggested_subtotal'></span>
                                </td>
                                <th>
                                    <?= lang('suggested_tax') ?>
                                </th>
                                <td class="text-right">
                                    <span id='suggested_tax'></span>
                                </td>
                                <th>
                                    <?= lang('suggested_total') ?>
                                </th>
                                <td class="text-right">
                                    <span id='suggested_total'></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-sm-12">
                  <span class="text-danger advise_invalid_retention" style="display: none;">El total de la retención es mayor al monto a pagar</span>
                  

                <hr class="col-sm-11">

                <div class="col-md-4">
                    <h3>Descuento aplica a :</h3>
                </div>

                <div class="col-md-4">
                    <h3>Porcentaje/Valor a descontar :</h3>
                </div>

                <div class="col-md-2">
                    <h3>Total descuento :</h3>
                </div>

                <div class="col-md-2">
                    <input type="text" class="form-control" id="total_discount" readonly>
                </div>

                <!-- descuento 1 -->

                <?php if ($collection_discounts): ?>
                    <?php 
                    for ($i=1; $i <= 3 ; $i++) { 
                         ?>
                        <div class="col-md-4">
                            <select class="discount_apply_to form-control" data-index="<?= $i ?>">
                                <option value=""><?= lang('select') ?></option>
                                <?php if ($collection_discounts): ?>
                                    <?php foreach ($collection_discounts as $discount): ?>
                                        <option value="<?= $discount->apply_to ?>" <?= $this->Settings->modulary == 1 ? 'data-ledgerid="'.$discount->purchase_ledger_id.'"' : '';  ?>><?= lang($discount->name) ?> </option>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <input type="text" class="discount_amount form-control" data-index="<?= $i ?>">
                        </div>

                        <div class="col-md-4">
                            <input type="text" class="discount_amount_calculated form-control" data-index="<?= $i ?>" readonly>
                        </div>
                    <?php }
                     ?>
                <?php endif ?>


                <!-- descuento 1 -->


                <hr class="col-sm-11">
                </div>
                <div class="modal-footer">
                  <button type="button" id="cancelOrderRete" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
                </div>
              </div>
            </div>
          </div>
        <!-- retencion -->
    </div>
</div>