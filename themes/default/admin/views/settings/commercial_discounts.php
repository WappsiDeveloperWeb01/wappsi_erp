<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row wrapper border-bottom white-bg page-heading no-print">
    <div class="col-lg-8">
        <h2><?= $page_title ?></h2>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight  no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="commercial_discounts_table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="active">
                                        	<th></th>
                                            <th><?= lang('commercial_discounts_apply_to') ?></th>
                                            <th><?= lang('commercial_discounts_sales_accounting_account') ?></th>
                                            <th><?= lang('commercial_discounts_purchase_accounting_account') ?></th>
                                            <th><?= lang('commercial_discounts_name') ?></th>
                                            <th><?= lang('actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                        	<th></th>
                                            <th><?= lang('commercial_discounts_apply_to') ?></th>
                                            <th><?= lang('commercial_discounts_sales_accounting_account') ?></th>
                                            <th><?= lang('commercial_discounts_purchase_accounting_account') ?></th>
                                            <th><?= lang('commercial_discounts_name') ?></th>
                                            <th><?= lang('actions'); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
		load_commercial_discounts_table();

		$(document).on('change', '#percentage', function() { validate_allowed_percentages() });
		$(document).on('change', '#type', function() { load_retention_rates() });
	});

	function load_commercial_discounts_table()
	{
	    if ($(window).width() < 1000) {
	        var nums = [[10, 25], [10, 25]];
	    } else {
	        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
	    }

        oTable = $('#commercial_discounts_table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": nums,
            "iDisplayLength":  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=admin_url('system_settings/get_commercial_discounts_datatables')?>',
            dom: 'lr<"containerBtn"><"inputFiltro"f>tip<"clear">',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                { 'bVisible':false },
                null,
                null,
                null,
                {
                    mRender:  function ( data, type, row ) {
                        if (data == 'discount_apply_to_payment_subtotal') {
                            return '<?= lang("discount_apply_to_payment_subtotal"); ?>';
                        } else if (data == 'discount_apply_to_subtotal') {
                            return '<?= lang("discount_apply_to_subtotal"); ?>';
                        } else if (data == 'discount_apply_to_payment_total') {
                            return '<?= lang("discount_apply_to_payment_total"); ?>';
                        } else if (data == 'discount_apply_to_total') {
                            return '<?= lang("discount_apply_to_total"); ?>';
                        } else if (data == 'discount_apply_to_tax') {
                            return '<?= lang("discount_apply_to_tax"); ?>';
                        } else if (data == 'discount_amount_manually') {
                            return '<?= lang("discount_amount_manually"); ?>';
                        }
                    },
                },
                null
            ],
            fnFooterCallback: function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var nCells = nRow.getElementsByTagName('th');
            }
        }).fnSetFilteringDelay().dtFilter([], "footer");
	}

	function validate_allowed_percentages()
    {
        var type = $('#type').val();
        var percentage = $('#percentage').val()

        if (type != 'ICA' && percentage != '') {
            if (type == 'IVA') {
                var rates_array = [15, 100];
            } else {
                var rates_array = [0.1, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 6.0, 7.0, 10.0, 11.0, 20.0];
            }

            if (rates_array.includes(parseFloat(percentage)) == false) {
                command: toastr.error('El dato ingresado no coincide con los porcentajes dados por la DIAN.', '¡Error validación', {onHidden: function() { $('#percentage').val(''); }});
                return false;
            }
        }
    }

    function load_retention_rates()
    {
        var options = '';
        var type = $('#type').val();

        if (type != 'ICA') {
            if (type == 'IVA') {
                $('#percentage').prop('placeholder', 'Permitidos: 15%, 100%');
            } else {
                $('#percentage').prop('placeholder', 'Permitidos: 0.1%, 0.5%, 1%, 1.5%, 2%, 2.5%, 3%, 3.5%, 4%, 6%, 7%, 10%, 11%, 20%');
            }
        } else {
            $('#percentage').prop('placeholder', '');
        }
    }
</script>