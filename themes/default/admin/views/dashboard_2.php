<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Header -->

<style type="text/css">
    .widget {
        min-height: 100px;
    }
    .btn_personalized{
        color : #1c84c6;
        background-color : #fff;
        border: 1px solid #1c84c6;
        border-radius : 5px;
    }
    .btn_personalized:hover{
        color : #fff;
        background-color : #1c84c6;
    }
</style>

<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight col-sm-12">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <div class="row">
                        <?php if (($this->Admin || $this->Owner) || ($this->GP['pos-index'] && $this->GP['pos-sales'])): ?>
                            <div class="col-md-2" style="padding-bottom: 2%">
                                <button onclick="window.open('<?= admin_url('pos') ?>', '_self');" class="btn_personalized"  style="width: 100%; height: 80px; color:'#1c84c6'">
                                    <h3 class="font-bold"> <i class="icon fas fa-store fa-lg"></i> &nbsp; <?= lang('add_sale') ?></h3>
                                </button>
                            </div>
                            <div class="col-md-2" style="padding-bottom: 2%">
                                <button onclick="window.open('<?= admin_url('pos/sales') ?>', '_self');" class="btn_personalized"  style="width: 100%; height: 80px;">
                                    <h3 class="font-bold"> <i class="icon fa fa-bars fa-lg"></i> &nbsp; <?= lang('list_sales') ?></h3>
                                </button>
                            </div>
                        <?php endif ?>
                        <?php if (($this->Admin || $this->Owner) || ($this->GP['customers-add'])): ?>
                            <div class="col-md-2" style="padding-bottom: 2%">
                                <button href="<?= admin_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal" class="btn_personalized"  style="width: 100%; height: 80px;">
                                    <h3 class="font-bold"><i class="icon fa fa-users fa-lg"></i> &nbsp;<?= lang('add_customer') ?></h3>
                                </button>
                            </div>
                        <?php endif ?>
                        <?php if (($this->Admin || $this->Owner) || ($this->GP['customers-index'])): ?>
                            <div class="col-md-2" style="padding-bottom: 2%">
                                <button onclick="window.open('<?= admin_url('customers') ?>', '_self');" class="btn_personalized"  style="width: 100%; height: 80px;">
                                    <h3 class="font-bold"><i class="icon fa fa-bars fa-lg"></i> &nbsp; <?= lang('list_customers') ?></h3>
                                </button>
                            </div>
                        <?php endif ?>
                        <?php if (($this->Admin || $this->Owner) || ($this->GP['products-index'])): ?>
                        <div class="col-md-2" style="padding-bottom: 2%">
                            <button onclick="window.open('<?= admin_url('products') ?>', '_self');" class="btn_personalized"  style="width: 100%; height: 80px;">
                                <h3 class="font-bold"><i class="icon fa fa-bars fa-lg"></i> &nbsp; <?= lang('list_products') ?></h3>
                            </button>
                        </div>
                        <?php endif ?>
                        <?php if (($this->Admin || $this->Owner) || ($this->GP['sales-orders'])): ?>
                        <div class="col-md-2" style="padding-bottom: 2%">
                            <button onclick="window.open('<?= admin_url('sales/orders') ?>', '_self');" class="btn_personalized"  style="width: 100%; height: 80px;">
                                <h3 class="font-bold"><i class="icon fa fa-solid fa-scroll fa-lg"></i> &nbsp; <?= lang('list_orders') ?></h3>
                            </button>
                        </div>
                        <?php endif ?>
                        <?php if (($this->Admin || $this->Owner) || ($this->GP['sales-add_order'])): ?>
                        <div class="col-md-2" style="padding-bottom: 2%">
                            <button onclick="window.open('<?= admin_url('sales/add_order') ?>', '_self');" class="btn_personalized"  style="width: 100%; height: 80px;">
                                <h3 class="font-bold"><i class="icon fa fa-solid fa-file-circle-plus fa-lg"></i> &nbsp; <?= lang('add_order') ?></h3>
                            </button>
                        </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->
<script language="javascript">
    $(document).ready(function () {


    });
</script>