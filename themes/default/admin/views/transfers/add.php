<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var is_edit = false;
    <?php if (isset($order)): ?>
        localStorage.setItem('toitems', '<?= json_encode($order_transfer_items) ?>');
        localStorage.setItem('tobiller_origin', '<?= $order->origin_biller_id ?>');
        localStorage.setItem('from_warehouse', '<?= $order->origin_warehouse_id ?>');
        localStorage.setItem('tobiller_destination', '<?= $order->biller_id ?>');
        localStorage.setItem('to_warehouse', '<?= $order->warehouse_id ?>');
        localStorage.setItem('companies_id', '<?= $order->seller_id ?>');

        $(document).ready(function(){
            setTimeout(function() {
                $('#tobiller_destination').select2('readonly', true);
                $('#to_warehouse').select2('readonly', true);
            }, 850);
        });
    <?php endif ?>
    <?php if ($this->session->userdata('remove_tols')) { ?>
    if (localStorage.getItem('toitems')) {
        localStorage.removeItem('toitems');
    }
    if (localStorage.getItem('toshipping')) {
        localStorage.removeItem('toshipping');
    }
    if (localStorage.getItem('companies_id')) {
        localStorage.removeItem('companies_id');
    }
    if (localStorage.getItem('toref')) {
        localStorage.removeItem('toref');
    }
    if (localStorage.getItem('to_warehouse')) {
        localStorage.removeItem('to_warehouse');
    }
    if (localStorage.getItem('tonote')) {
        localStorage.removeItem('tonote');
    }
    if (localStorage.getItem('from_warehouse')) {
        localStorage.removeItem('from_warehouse');
    }
    if (localStorage.getItem('todate')) {
        localStorage.removeItem('todate');
    }
    if (localStorage.getItem('tostatus')) {
        localStorage.removeItem('tostatus');
    }
    if (localStorage.getItem('tobiller_origin')) {
        localStorage.removeItem('tobiller_origin');
    }
    if (localStorage.getItem('tobiller_destination')) {
        localStorage.removeItem('tobiller_destination');
    }
    <?php $this->sma->unset_data('remove_tols');
} ?>
    var count = 1, an = 1, product_variant = 0, shipping = 0,
        product_tax = 0, total = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>, toitems = {},
        audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3'),
        audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function () {
        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('todate')) {
            $("#todate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#todate', function (e) {
            localStorage.setItem('todate', $(this).val());
        });
        if (todate = localStorage.getItem('todate')) {
            $('#todate').val(todate);
        }
        <?php } ?>
        ItemnTotals();
        $("#add_item").autocomplete({
            //source: '<?= admin_url('transfers/suggestions'); ?>',
            source: function (request, response) {
                if (!$('#tobiller_origin').val() || !$('#tobiller_destination').val() || !$('#from_warehouse').val() || !$('#to_warehouse').val() || !$('#companies_id').val()) {

                    if (!$('#tobiller_origin').val()) {
                      command: toastr.warning('Por favor escoja Sucursal origen .', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                    if (!$('#tobiller_destination').val()) {
                        command: toastr.warning('Porfavor escoja Sucursal destino.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                    if (!$('#from_warehouse').val()) {
                        command: toastr.warning('Porfavor escoja Bodega origen.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                    if (!$('#to_warehouse').val()) {
                        command: toastr.warning('Porfavor escoja Bodega destino.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                    if (!$('#companies_id').val()) {
                        command: toastr.warning('Porfavor escoja Tercero.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('transfers/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#from_warehouse").val(),
                        tobiller_origin: $("#tobiller_origin").val(),
                        tobiller_destination: $("#tobiller_destination").val(),
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    if ($('#from_warehouse').val()) {
                        bootbox.alert('<?= lang('no_match_found') ?>', function () {
                            $('#add_item').focus();
                        });
                    } else {
                        bootbox.alert('<?= lang('please_select_warehouse') ?>', function () {
                            $('#add_item').focus();
                        });
                    }
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_transfer_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });

        var to_warehouse;
        var validation_twh = false;
        $('#to_warehouse').on("select2-focus", function (e) {
            to_warehouse = $(this).val();
            validation_twh = true;
        }).on("change", function (e) {
            if ($('#to_warehouse').val() && $('#to_warehouse').val() == $('#from_warehouse').val()) {
                localStorage.removeItem('from_warehouse');
                $(this).select2('val', '');
                if (validation_twh) {
                    bootbox.alert('<?= lang('please_select_different_warehouse_destination') ?>');
                }
            }
        });
        var from_warehouse;
        $('#from_warehouse').on("select2-focus", function (e) {
            from_warehouse = $(this).val();
        }).on("change", function (e) {
            if ($('#from_warehouse').val() && $('#from_warehouse').val() == $('#to_warehouse').val()) {
                $('#from_warehouse').select2('val', from_warehouse);
                bootbox.alert('<?= lang('please_select_different_warehouse_origin') ?>');
            }
        });

    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            $attrib = array('id' => 'add_tranfers_form', 'data-toggle' => 'validator', 'role' => 'form', 'target' => '_blank');
                            echo admin_form_open_multipart("transfers/add", $attrib);
                            ?>
                            <?php if (isset($order)): ?>
                                <input type="hidden" name="order_id" value="<?= $order->id ?>">
                            <?php endif ?>
                            <div class="row">
                                <div class="col-lg-12 resAlert"></div>
                                <?php if ($Owner || $Admin) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("date", "todate"); ?>
                                            <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="todate" required="required"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller_origin", "tobiller_origin"); ?>
                                            <select name="biller" class="form-control" id="tobiller_origin" required="required">
                                                <?php foreach ($billers as $biller): ?>
                                                  <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>"><?= $biller->company  ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4" style="display: none;">
                                        <div class="form-group">
                                            <?= lang("biller_origin", "tobiller_origin"); ?>
                                            <select name="biller" class="form-control" id="tobiller_origin">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>" selected="selected"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("biller_destination", "tobiller_destination"); ?>
                                        <select name="biller_destination" class="form-control" id="tobiller_destination" required="required">
                                            <?php if (count($billers) > 1): ?>
                                                <option value=""><?= lang('select') ?></option>
                                            <?php endif ?>
                                            <?php foreach ($billers as $biller): ?>
                                              <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>"><?= $biller->company ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("reference_no", "slref"); ?>
                                        <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("status", "tostatus"); ?>
                                        <?php
                                        $post = array('completed' => lang('completed'), 'pending' => lang('pending'), 'sent' => lang('sent'));
                                        echo form_dropdown('status', $post, (isset($_POST['status']) ? $_POST['status'] : ''), 'id="tostatus" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" style="width:100%;" ');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= lang('third', 'companies_id') ?>
                                    <?php
                                    $cOptions[''] = lang('select').lang('third');
                                    foreach ($companies as $row => $company) {
                                        $cOptions[$company->id] = $company->name;
                                    }

                                     ?>
                                    <?= form_dropdown('companies_id', $cOptions, '',' id="companies_id" class="form-control select" required style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("document", "document") ?>
                                        <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                            data-show-preview="false" class="form-control file">
                                    </div>
                                </div>
                                <?php if (isset($cost_centers)): ?>
                                    <div class="col-md-4 form-group">
                                        <?= lang('cost_center', 'cost_center_id') ?>
                                        <?php
                                        $ccopts[''] = lang('select');
                                        if ($cost_centers) {
                                            foreach ($cost_centers as $cost_center) {
                                                $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                            }
                                        }
                                         ?>
                                        <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <?= lang('destination_cost_center', 'destination_cost_center_id') ?>
                                        <?= form_dropdown('destination_cost_center_id', $ccopts, '', 'id="destination_cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="row">
                                <?php

                                    $wh[''] = lang('select');
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }

                                 ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("from_warehouse", "from_warehouse"); ?>

                                            <?php
                                            echo form_dropdown('from_warehouse', $wh, (isset($_POST['from_warehouse']) ? $_POST['from_warehouse'] : ''), 'id="from_warehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("from_warehouse") . '" required="required" style="width:100%;" ');
                                            ?>
                                        </div>
                                    </div>
                                <?php } else {
                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'from_warehouse',
                                        'id' => 'from_warehouse',
                                        'value' => $this->session->userdata('warehouse_id'),
                                    );
                                    echo form_input($warehouse_input);
                                } ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("to_warehouse", "to_warehouse"); ?>
                                        <?php
                                        echo form_dropdown('to_warehouse', $wh, (isset($_POST['to_warehouse']) ? $_POST['to_warehouse'] : ''), 'id="to_warehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("to_warehouse") . '" required="required" style="width:100%;" ');
                                        ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-12" id="sticker">
                                    <div class="well well-sm">
                                        <div class="form-group" style="margin-bottom:0;">
                                            <div class="input-group wide-tip">
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <i class="fa fa-2x fa-barcode addIcon"></i>
                                                </div>
                                                    <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_product_to_order") . '"'); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="control-group table-group">
                                        <label class="table-label"><?= lang("order_items"); ?></label>

                                        <div class="controls table-controls">
                                            <table id="toTable"
                                                class="table items  table-bordered table-condensed table-hover sortable_table">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <?= lang('product'); ?>
                                                    </th>
                                                    <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                                                        <th><?= lang("variant") ?></th>
                                                    <?php endif ?>
                                                    <th class="thead_variant"><?= lang("variant") ?></th>
                                                    <?php if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10): ?>
                                                        <th><?= lang('unit') ?></th>
                                                        <th><?= lang('product_unit_quantity') ?></th>
                                                        <th><?= lang('product_unit_cost') ?></th>
                                                    <?php endif ?>
                                                    <?php if ($this->Settings->product_serial): ?>
                                                        <th><?= lang("serial_no") ?></th>
                                                    <?php endif ?>
                                                    <?php
                                                    if ($Settings->product_expiry) {
                                                        echo '<th >' . $this->lang->line("expiry_date") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="thead_discount"><?= lang("net_unit_cost"); ?></th>
                                                    <?php
                                                    if ($Settings->product_discount) {
                                                        echo '<th class="thead_discount">' . $this->lang->line("discount") . '</th>';
                                                    }
                                                    ?>
                                                    <?php $restrict_permission = ($Admin || $Owner || $GP['products-cost'] == 1) ? '' : 'display:none;' ?>
                                                    <th style="width:160px; <?= $restrict_permission ?>"><?= lang("net_unit_cost"); ?></th>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        // echo '<th class="col-md-1">' . $this->lang->line("product_tax") . '</th>';
                                                        echo "<th style='".$restrict_permission."'>" . $this->lang->line("product_tax") . ' (<span class="currency">'. $default_currency->code.'</span>)  </th>';
                                                    }
                                                    if ($Settings->ipoconsumo) {
                                                        echo "<th style='".$restrict_permission."'>" . $this->lang->line("second_product_tax") . ' (<span class="currency">'. $default_currency->code.'</span>)  </th>';
                                                    }
                                                    ?>
                                                    <th style="width:160px; <?= $restrict_permission ?>"><?= lang('cost_x_tax') ?></th>
                                                    <th class="thead_shipping"><?= lang('purchase_shipping') ?></th>
                                                    <th style="width: 110px !important;"><?= lang("quantity"); ?></th>
                                                    <th style="<?= $restrict_permission ?>">
                                                        <?= lang("subtotal"); ?> (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th>
                                                    <th style="width: 30px !important; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                                </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot></tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="from-group">
                                        <?= lang("note", "tonote"); ?>
                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'id="tonote" class="form-control" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                    <div class="from-group"><?php echo form_submit('add_transfers', $this->lang->line("submit"), 'id="add_transfers" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                        <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                    <tr class="warning">
                                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                        <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                        <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                                        <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.Body -->

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
                <h4 class="modal-title" id="prModalLabelCode"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if (!isset($quote) || (isset($quote) && $quote->quote_type == 1)): ?>
                        <div class="edit_purchase">
                            <div class="form-group">
                                <label for="punit" class="col-sm-4"><?= lang('purchase_product_unit') ?></label>
                                <div class="col-sm-8">
                                    <div id="punits-div"></div>
                                </div>
                            </div>
                            <?php if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10): ?>
                                <div class="form-group pedit_input_group">
                                    <label for="punit_quantity" class="col-sm-4"><?= lang('purchase_product_unit_quantity') ?><span class="um_name"></span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="punit_quantity" readonly>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="form-group">
                                <label for="pquantity" class="col-sm-4"><?= ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? lang('purchase_product_quantity') : lang('quantity') ?><span class="um_name"></span></label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="pquantity">
                                </div>
                            </div>
                            <div class="form-group variants_div">
                                <label for="poption" class="col-sm-4"><?= lang('product_option') ?></label>
                                <div class="col-sm-8">
                                    <div id="poptions-div"></div>
                                </div>
                            </div>
                            <?php if ($Settings->tax1) { ?>
                                <div class="form-group">
                                    <label class="col-sm-4"><?= lang('product_tax') ?></label>
                                    <div class="col-sm-8">
                                        <select name="ptax" id="ptax" class="form-control" readonly>
                                            <?php foreach ($tax_rates as $tax): ?>
                                                <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->Settings->ipoconsumo == 2 || $this->Settings->ipoconsumo == 1): ?>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label"><?= lang('second_product_tax') ?></label>
                                    </div>
                                    <div class="col-sm-8 ptax2_div">
                                        <input type="text" name="ptax2" id="ptax2" class="form-control" readonly>
                                    </div>
                                    <div class="col-sm-4 ptax2_percentage_div" style="display:none;">
                                        <input type="text" name="ptax2_percentage" id="ptax2_percentage" class="form-control" readonly>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if ($Settings->product_serial) { ?>
                                <div class="form-group">
                                    <label for="pserial" class="col-sm-4"><?= lang('serial_no') ?></label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="pserial">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($Settings->product_expiry) { ?>
                                <div class="form-group">
                                    <label for="pexpiry" class="col-sm-4"><?= lang('product_expiry') ?></label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control date" id="pexpiry">
                                    </div>
                                </div>
                            <?php } ?>
                            <!--  -->

                            <div class="form-group">
                                <label class="col-sm-4"></label>
                                <div class="col-sm-4">
                                    <label class=""><?= lang('exclusive') ?></label>
                                </div><div class="col-sm-4">
                                    <label class=""><?= lang('inclusive') ?></label>
                                </div>
                            </div>

                            <?php if ($this->Settings->precios_por_unidad_presentacion == 1 && ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10)): ?>
                                <div class="form-group">
                                    <label for="pcost_um" class="col-sm-4"><?= lang('unit_cost_um') ?> <span class="um_name"></span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pcost_um_net">
                                    </div><div class="col-sm-4">
                                        <input type="text" class="form-control" id="pcost_um">
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if ($this->Settings->precios_por_unidad_presentacion == 1 && ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10)): ?>
                                <div class="form-group">
                                    <label for="pcost_um_total" class="col-sm-4"><?= lang('total_cost_um') ?> <span class="um_name"></span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pcost_um_total_net">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pcost_um_total">
                                    </div>
                                </div>
                            <?php endif ?>

                            <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "style='display:none;'" : "" ?>>
                                <label for="pcost" class="col-sm-4"><?= lang('unit_cost') ?></label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="pcost_net">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="pcost">
                                </div>
                            </div>
                            
                            <!--  -->
                            <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "" : "style='display:none;'" ?>>
                                <label for="pproduct_unit_cost" class="col-sm-4"><?= lang('purchase_form_edit_product_unit_cost') ?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="pproduct_unit_cost">
                                </div>
                            </div>
                            <?php if ($Settings->ipoconsumo): ?>
                                <!-- <div class="form-group">
                                    <label for="pcost_ipoconsumo" class="col-sm-4"><?= lang('unit_cost_ipoconsumo') ?></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="pcost_ipoconsumo"><br>
                                        <em><?= lang('unit_cost_ipoconsumo_detail') ?></em>
                                    </div>
                                </div> -->
                            <?php endif ?>
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width:25%;"><?= lang('net_unit_cost'); ?></th>
                                    <th style="width:25%;"><span id="net_cost"></span></th>
                                    <th style="width:25%;"><?= lang('product_taxes'); ?></th>
                                    <th style="width:25%;"><span id="pro_tax"></span></th>
                                </tr>
                            </table>
                            <!-- <div class="panel panel-default">
                                <div class="panel-heading"><?= lang('calculate_unit_cost'); ?></div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label for="pcost" class="col-sm-4"><?= lang('subtotal_before_tax') ?></label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="psubtotal">
                                                <div class="input-group-addon" style="padding: 2px 8px;">
                                                    <a href="#" id="calculate_unit_price" class="tip" title="<?= lang('calculate_unit_cost'); ?>">
                                                        <i class="fa fa-calculator"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <?php if (!isset($quote)): ?>
                            <div class="edit_purchase_expense" style="display: none;">
                                <div class="form-group pname-div">
                                    <label for="pname" class="col-sm-4 control-label"><?= lang('name') ?></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="pname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pcost" class="col-sm-4 control-label"><?= lang('expense_cost') ?></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="pcost">
                                    </div>
                                </div>
                                <?php if ($Settings->tax1) { ?>
                                    <div class="form-group">
                                        <label class="col-sm-12"><?= lang('tax_1') ?></label>
                                        <div class="col-sm-6">
                                            <?php
                                            $tr[""] = lang('n/a');
                                            foreach ($tax_rates as $tax) {
                                                $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                            ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= form_input('ptax_value', '', 'id="ptax_value" class="form-control"'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12"><?= lang('tax_2') ?></label>
                                        <div class="col-sm-6">
                                            <?php
                                            $tr[""] = lang('n/a');
                                            foreach ($tax_rates as $tax) {
                                                $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('ptax_2', $tr, "", 'id="ptax_2" class="form-control pos-input-tip" style="width:100%;"');
                                            ?>
                                        </div>

                                        <div class="col-sm-6">
                                            <?= form_input('ptax_2_value', '', 'id="ptax_2_value" class="form-control"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <th style="width:16.66%;"><?= lang('expense_cost'); ?></th>
                                        <th style="width:16.66%;"><span id="net_cost"></span></th>
                                        <th style="width:16.66%;"><?= lang('expense_tax'); ?></th>
                                        <th style="width:16.66%;"><span id="pro_tax"></span></th>
                                        <th style="width:16.66%;"><?= lang('total'); ?></th>
                                        <th style="width:16.66%;"><span id="pro_total"></span></th>
                                    </tr>
                                </table>
                            </div>
                        <?php endif ?>
                        <?php if ($this->Settings->update_prices_from_purchases == 1 &&($this->Owner || $this->Admin || $this->GP['system_settings-update_products_group_prices'])): ?>
                            <div class="div_update_prices">
                                <div class="col-sm-12">
                                    <h3>Actualización de precios</h3>
                                </div>
                                <?php if ($this->Settings->prioridad_precios_producto == 1 || $this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) { ?>
                                    <?php foreach ($units as $pg): ?>
                                        <div class="row">
                                            <div class="col-sm-12 pgprice_div" style="display:none;" data-pgid="<?= $pg->id ?>">
                                                <label><?= $pg->name ?></label>
                                            </div>
                                            <div class="col-sm-4 pgprice_div" style="display:none;" data-pgid="<?= $pg->id ?>">
                                                <label>Actual</label>
                                                <input type="text" name="pgprice" data-pgid="<?= $pg->id ?>" class="form-control pgprice" readonly>
                                            </div>
                                            <div class="col-sm-4 pgprice_div" style="display:none;" data-pgid="<?= $pg->id ?>">
                                                <label>Margen</label>
                                                <input type="text" name="pgmargin" data-pgid="<?= $pg->id ?>" class="form-control pgmargin">
                                            </div>
                                            <div class="col-sm-4 pgprice_div" style="display:none;" data-pgid="<?= $pg->id ?>">
                                                <label>Nuevo</label>
                                                <input type="text" name="pprice_group" data-pgid="<?= $pg->id ?>" class="form-control pgnew_price">
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                <?php } else { ?>
                                    <?php foreach ($price_groups as $pg): ?>
                                        <div class="col-sm-6">
                                            <label><?= $pg->name ?></label>
                                            <input type="text" name="pprice_group" data-pgid="<?= $pg->id ?>" class="form-control pgprice">
                                        </div>
                                    <?php endforeach ?>
                                <?php } ?>
                            </div>

                            <hr class="col-sm-11">
                        <?php endif ?>
                    <?php elseif(isset($quote) && $quote->quote_type == 2): ?>
                        <div class="edit_purchase_expense">
                            <?php if ($Settings->tax1) { ?>
                                <div class="form-group">
                                    <label class="col-sm-12"><?= lang('tax_1') ?></label>
                                    <div class="col-sm-6">
                                        <?php
                                        $tr[""] = lang('n/a');
                                        foreach ($tax_rates as $tax) {
                                            $tr[$tax->id] = $tax->name;
                                        }
                                        echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= form_input('ptax_value', '', 'id="ptax_value" class="form-control"'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12"><?= lang('tax_2') ?></label>
                                    <div class="col-sm-6">
                                        <?php
                                        $tr[""] = lang('n/a');
                                        foreach ($tax_rates as $tax) {
                                            $tr[$tax->id] = $tax->name;
                                        }
                                        echo form_dropdown('ptax_2', $tr, "", 'id="ptax_2" class="form-control pos-input-tip" style="width:100%;"');
                                        ?>
                                    </div>

                                    <div class="col-sm-6">
                                        <?= form_input('ptax_2_value', '', 'id="ptax_2_value" class="form-control"'); ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width:16.66%;"><?= lang('expense_cost'); ?></th>
                                    <th style="width:16.66%;"><span id="net_cost"></span></th>
                                    <th style="width:16.66%;"><?= lang('expense_tax'); ?></th>
                                    <th style="width:16.66%;"><span id="pro_tax"></span></th>
                                    <th style="width:16.66%;"><?= lang('total'); ?></th>
                                    <th style="width:16.66%;"><span id="pro_total"></span></th>
                                </tr>
                            </table>
                        </div>
                    <?php endif ?>

                    <input type="hidden" id="punit_cost" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_cost" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                    <input type="hidden" id="pcost" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_serialModal"></h2>
                <h3><?= lang('ticket_data') ?></h3>
            </div>
            <div class="modal-body">
                <form id="serialModal_form">
                    <input type="hidden" id="serialModal_product_id">
                    <input type="hidden" id="serialModal_option_id">
                    <div class="col-sm-12 form-group">
                        <?= lang('serial_no', 'serial') ?>
                        <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= lang('meters', 'meters') ?>
                        <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!$Owner || !$Admin || $this->session->userdata('warehouse_id')) { ?>
<script>
    $(document).ready(function() {
        $("#to_warehouse option[value='<?= $this->session->userdata('warehouse_id'); ?>']").attr('disabled', 'disabled');
    });
</script>
<?php } ?>

<script type="text/javascript">

    $(document).on('change', '#tobiller_origin', function(){
        localStorage.setItem('tobiller_origin', $(this).val());
        default_warehouse = $('#tobiller_origin option:selected').data('warehousedefault');
        $('#from_warehouse').select2('val', default_warehouse);

        warehouses_related = billers_data[$('#tobiller_origin').val()].warehouses_related; 
        if (warehouses_related) {
            $('#from_warehouse option').each(function(index, option){
                $(option).prop('disabled', false);
                if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                    $(option).prop('disabled', true);
                }
            });
            setTimeout(function() {
                $('#from_warehouse').select2().trigger('change');
            }, 850);
        }

        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/12/") ?>'+$('#tobiller_origin').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        });
    });

</script>