<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_settings extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        $this->lang->admin_load('front_end', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('shop_admin_model');
        $this->load->shop_model('shop_model');
        $this->upload_path = 'assets/uploads/';
        $this->image_types = 'gif|jpg|jpeg|png';
        $this->allowed_file_size = '2028';
    }

    function index() {

        $this->sma->checkPermissions();
        $shop_settings = $this->shop_admin_model->getShopSettings();
        $this->form_validation->set_rules('biller', lang('biller'), 'trim|required');
        if ($this->form_validation->run() == true) {
            $this->load->library('upload');
            $photo = null;
            if ($_FILES['shop_favicon']['size'] > 0) {
                $config['upload_path'] = 'themes/default/shop/assets/images/';
                // exit($config['upload_path']);
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size'] = '1024';
                $config['max_width'] = 100;
                $config['max_height'] = 100;
                $config['overwrite'] = FALSE;
                $config['max_filename'] = 25;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('shop_favicon')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("shop_settings");
                }
                $photo = $this->upload->file_name;
            }
            $sn_arr = json_decode($shop_settings->manage_social_networks);

            $sn_name = $this->input->post('sn_name');
            $sn_url = $this->input->post('sn_url');
            $sn_color = $this->input->post('sn_color');
            $sn_status = $this->input->post('sn_status');
            $sn_icon = $this->input->post('sn_icon');

            foreach ($sn_name as $sn_name_key => $sn_name_value) {
                $sn_arr->{$sn_name_key}->name = $sn_name_value;
                $sn_arr->{$sn_name_key}->url = $sn_url[$sn_name_key];
                $sn_arr->{$sn_name_key}->color = $sn_color[$sn_name_key];
                $sn_arr->{$sn_name_key}->status = $sn_status[$sn_name_key];
                $sn_arr->{$sn_name_key}->icon = $sn_icon[$sn_name_key];
            }
            $new_sn_arr = json_encode($sn_arr);

            $data = array(
                'description' => DEMO ? 'Stock Manager Advance - SMA Shop - Demo Ecommerce Shop that would help you to sell your products from your site.' : $this->input->post('description'),
                'biller' => $this->input->post('biller'),
                'about_link' => $this->input->post('about_link'),
                'terms_link' => $this->input->post('terms_link'),
                'privacy_link' => $this->input->post('privacy_link'),
                'contact_link' => $this->input->post('contact_link'),
                'payment_text' => $this->input->post('payment_text'),
                'follow_text' => $this->input->post('follow_text'),
                'facebook' => $this->input->post('facebook'),
                'twitter' => $this->input->post('twitter'),
                'google_plus' => $this->input->post('google_plus'),
                'instagram' => $this->input->post('instagram'),
                'cookie_message' => DEMO ? 'We use cookies to improve your experience on our website. By browsing this website, you agree to our use of cookies.' : $this->input->post('cookie_message'),
                'cookie_link' => $this->input->post('cookie_link'),
                'shipping' => $this->input->post('shipping'),
                'bank_details' => $this->input->post('bank_details'),
                'products_page' => $this->input->post('products_page'),
                'hide0' => $this->input->post('hide0'),
                'show_product_quantity' => $this->input->post('show_product_quantity'),
                'show_product_zero_price' => $this->input->post('show_product_zero_price'),
                'show_product_zero_quantity' => $this->input->post('show_product_zero_quantity'),
                'show_categories_without_products' => $this->input->post('show_categories_without_products'),
                'overselling' => $this->input->post('overselling'),
                'use_product_variants' => $this->input->post('use_product_variants'),
                'use_product_preferences' => $this->input->post('use_product_preferences'),
                'primary_color' => $this->input->post('primary_color'),
                'secondary_color' => $this->input->post('secondary_color'),
                'bar_menu_color' => $this->input->post('bar_menu_color'),
                'font_text_color' => $this->input->post('font_text_color'),
                'background_page_color' => $this->input->post('background_page_color'),
                'header_color' => $this->input->post('header_color'),
                'footer_color' => $this->input->post('footer_color'),
                'font_family' => $this->input->post('font_family'),
                'header_message_color' => $this->input->post('header_message_color'),
                'product_view' => $this->input->post('product_view'),
                'navbar_color' => $this->input->post('navbar_color'),
                'navbar_text_color' => $this->input->post('navbar_text_color'),
                'bar_menu_text_color' => $this->input->post('bar_menu_text_color'),
                'footer_text_color' => $this->input->post('footer_text_color'),
                'promotion_text_color' => $this->input->post('promotion_text_color'),
                'favorite_color' => $this->input->post('favorite_color'),
                'sale_destination' => $this->input->post('sale_destination'),
                'show_categories_buttons' => $this->input->post('show_categories_buttons'),
                'show_featured_products' => $this->input->post('show_featured_products'),
                'show_most_selled_products' => $this->input->post('show_most_selled_products'),
                'show_other_products' => $this->input->post('show_other_products'),
                'show_promotion_products' => $this->input->post('show_promotion_products'),
                'show_brands_buttons' => $this->input->post('show_brands_buttons'),
                'show_new_products' => $this->input->post('show_new_products'),
                'show_favorite_products' => $this->input->post('show_favorite_products'),
                'icons_color' => $this->input->post('icons_color'),
                'banners_transition_seconds' => $this->input->post('banners_transition_seconds'),
                'item_frame_rounding' => $this->input->post('item_frame_rounding'),
                'item_margin' => $this->input->post('item_margin'),
                'manage_social_networks' => $new_sn_arr,
                'activate_available_from_quantity' => $this->input->post('activate_available_from_quantity'),
                'update_promotions_to_store' => $this->input->post('update_promotions_to_store'),
                'ecommerce_active' => $this->input->post('ecommerce_active'),
                'api_key_hash' => password_hash($this->input->post("api_key_hash"), PASSWORD_DEFAULT),
            );

            if ($photo) {
                $data['shop_favicon'] = $photo;
            }
        }

        if ($this->form_validation->run() == true && $this->shop_admin_model->updateShopSettings($data)) {
            $this->session->set_flashdata('message', lang('settings_updated'));
            admin_redirect("shop_settings");
        } else {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['billers'] = $this->shop_model->get_virtual_billers();
            $this->data['pages'] = $this->shop_admin_model->getAllPages();
            $this->data['shop_settings'] = $shop_settings;
            $logos = $this->getLogoList();
            $this->data['logo_files'] = $logos['logo_files'];
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('shop_settings')));
            $meta = array('page_title' => lang('shop_settings'), 'bc' => $bc);
            $this->page_construct('shop/index', $meta, $this->data);
        }
    }

    function slider() {

        $this->sma->checkPermissions();
        $this->form_validation->set_rules('caption1', lang('caption').' 1', 'trim|max_length[160]');
        $shop_settings = $this->shop_admin_model->getShopSettings();
        if ($this->form_validation->run() == true) {
            $deleted = $this->input->post('deleted');
            $link = $this->input->post('link[]');
            $caption = $this->input->post('caption[]');
            $slider_id = $this->input->post('slider_id[]');
            $this->load->library('upload');
            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            $file = $_FILES;
            $shop_settings_arr = ($shop_settings->slider != "[]" ? json_decode($shop_settings->slider) : (Object)  json_decode("{}"));
            foreach ($link as $link_id => $link_value) {
                if (isset($slider_id[$link_id])) {
                    if (isset($shop_settings_arr->{$link_id})) {
                        $shop_settings_arr->{$link_id}->link = $link_value;
                        $shop_settings_arr->{$link_id}->caption = $caption[$link_id];
                    }
                } else {
                    if ($file['image']['size'][$link_id] > 0) {
                            $_FILES['userfile']['name'] = $file['image']['name'][$link_id];
                            $_FILES['userfile']['type'] = $file['image']['type'][$link_id];
                            $_FILES['userfile']['tmp_name'] = $file['image']['tmp_name'][$link_id];
                            $_FILES['userfile']['error'] = $file['image']['error'][$link_id];
                            $_FILES['userfile']['size'] = $file['image']['size'][$link_id];
                        if (!$this->upload->do_upload()) {
                            $error = $this->upload->display_errors();
                            $this->session->set_flashdata('error', $error);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        if (!isset($shop_settings_arr->{$link_id})) {
                            $shop_settings_arr->{$link_id} = (Object)  json_decode("{}");
                        }
                        $shop_settings_arr->{$link_id}->image = $this->upload->file_name;
                        $shop_settings_arr->{$link_id}->link = $link[$link_id];
                        $shop_settings_arr->{$link_id}->caption = $caption[$link_id];
                    }
                }
            }
            if ($deleted) {
                foreach ($deleted as $key => $value) {
                    if (isset($shop_settings_arr->{$value}->image)) {
                        if (!unlink("assets/uploads/".$shop_settings_arr->{$value}->image)) {
                            // code...
                        }
                    }
                    unset($shop_settings_arr->{$value});
                }
            }
            $shop_settings_arr = (array) $shop_settings_arr;
            SORT($shop_settings_arr);
            $shop_settings_arr = (Object) $shop_settings_arr;
            // $this->sma->print_arrays($shop_settings_arr);


        }

        if ($this->form_validation->run() == true && $this->shop_admin_model->updateSlider($shop_settings_arr)) {
            $this->session->set_flashdata('message', lang('silder_updated'));
            admin_redirect("shop_settings/slider");
        } else {
            $this->data['slider_settings'] = json_decode($shop_settings->slider);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('shop_settings'), 'page' => lang('shop_settings')), array('link' => '#', 'page' => lang('slider_settings')));
            $meta = array('page_title' => lang('slider_settings'), 'bc' => $bc);
            $this->page_construct('shop/slider', $meta, $this->data);
        }
    }

    function pages() {

        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('shop_settings'), 'page' => lang('shop_settings')), array('link' => '#', 'page' => lang('pages')));
        $meta = array('page_title' => lang('pages'), 'bc' => $bc);
        $this->page_construct('shop/pages', $meta, $this->data);
    }

    function getPages() {

        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, slug, active, order_no, title")
            ->from("pages")
            ->add_column("Actions", "<div class=\"text-center\">
                <a href='" . admin_url('shop_settings/edit_page/$1') . "' class='tip btn btn-default new-button new-button-sm'  data-toggle-second='tooltip' data-placement='top' title='" . lang("edit_page") . "'>
                    <i class=\"fa fa-edit\"></i>
                </a>
                <a href='#' class='tip po btn btn-danger new-button new-button-sm' title='<b>" . lang("delete_page") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('shop_settings/delete_page/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o fa-lg\"></i>
                </a>
            </div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }

    function add_page() {

        $this->sma->checkPermissions();
        $this->form_validation->set_rules('name', lang('name'), 'required|max_length[15]');
        $this->form_validation->set_rules('title', lang('title'), 'required|max_length[60]');
        $this->form_validation->set_rules('description', lang('description'), 'required');
        $this->form_validation->set_rules('body', lang('body'), 'required');
        $this->form_validation->set_rules('order_no', lang('order_no'), 'required|is_natural_no_zero');
        $this->form_validation->set_rules('slug', lang('slug'), 'trim|required|is_unique[pages.slug]|alpha_dash');
        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'body' => $this->input->post('body', TRUE),
                'slug' => $this->input->post('slug'),
                'order_no' => $this->input->post('order_no'),
                'active' => $this->input->post('active') ? $this->input->post('active') : 0,
                'updated_at' => date('Y-m-d H:i:s'),
            );
        }

        if ($this->form_validation->run() == true && $this->shop_admin_model->addPage($data)) {

            $this->session->set_flashdata('message', lang('page_added'));
            admin_redirect("shop_settings/pages");

        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_page')));
            $meta = array('page_title' => lang('add_page'), 'bc' => $bc);
            $this->page_construct('shop/add_page', $meta, $this->data);

        }
    }

    function edit_page($id = NULL) {
        $this->sma->checkPermissions();
        $page = $this->shop_admin_model->getPageByID($id);
        $this->form_validation->set_rules('name', lang('name'), 'required|max_length[15]');
        $this->form_validation->set_rules('title', lang('title'), 'required|max_length[60]');
        $this->form_validation->set_rules('description', lang('description'), 'required');
        $this->form_validation->set_rules('body', lang('body'), 'required');
        $this->form_validation->set_rules('order_no', lang('order_no'), 'required|is_natural_no_zero');
        $this->form_validation->set_rules('slug', lang('slug'), 'trim|required|alpha_dash');
        if($page->slug != $this->input->post('slug')) {
            $this->form_validation->set_rules('slug', lang('slug'), 'is_unique[pages.slug]');
        }
        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'body' => $this->input->post('body', TRUE),
                'slug' => $this->input->post('slug'),
                'order_no' => $this->input->post('order_no'),
                'active' => $this->input->post('active') ? $this->input->post('active') : 0,
                'updated_at' => date('Y-m-d H:i:s'),
            );
        }

        if ($this->form_validation->run() == true && $this->shop_admin_model->updatePage($id, $data)) {

            $this->session->set_flashdata('message', lang('page_updated'));
            admin_redirect("shop_settings/pages");

        } else {

            $this->data['page'] = $page;
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_page')));
            $meta = array('page_title' => lang('edit_page'), 'bc' => $bc);
            $this->page_construct('shop/edit_page', $meta, $this->data);

        }
    }

    function delete_page($id = NULL) {
        $this->sma->checkPermissions();
        if ($this->shop_admin_model->deletePage($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("page_deleted")));
        }
    }

    function page_actions() {

        $this->sma->checkPermissions();
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->shop_admin_model->deletePage($id);
                    }
                    $this->session->set_flashdata('message', lang("pages_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function slugify() {

        $this->sma->checkPermissions();
        if ($products = $this->shop_admin_model->getAllProducts()) {
            $this->db->update('products', ['slug' => NULL]);
            foreach ($products as $product) {
                $slug = $this->sma->slug($product->name);
                $this->db->update('products', ['slug' => $slug], ['id' => $product->id]);
            }
            $this->session->set_flashdata('message', lang("slugs_updated"));
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'admin/shop_settings');
        }
        $this->session->set_flashdata('error', lang("no_product_found"));
        redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'admin/shop_settings');

    }

    function sitemap()
    {
        $this->sma->checkPermissions();
        $categories = $this->shop_admin_model->getAllCategories();
        $products = $this->shop_admin_model->getAllProducts();
        $brands = $this->shop_admin_model->getAllBrands();
        $pages = $this->shop_admin_model->getAllPages();
        $map = '<?xml version="1.0" encoding="UTF-8" ?>';

        $map .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $map .= '<url>';
        $map .= '<loc>'.site_url().'</loc> ';
        $map .= '<priority>1.0</priority>';
        $map .= '<changefreq>daily</changefreq>';
        // $map .= '<lastmod>'.date('Y-m-d').'</lastmod>';
        $map .= '</url>';

        if (!empty($categories)) {
            foreach($categories as $category) {
                $map .= '<url>';
                $map .= '<loc>'.site_url('category/'.$category->slug).'</loc> ';
                $map .= '<priority>0.8</priority>';
                $map .= '</url>';
                $subcategories = $this->shop_admin_model->getSubCategories($category->id);
                if (!empty($subcategories)) {
                    foreach($subcategories as $subcategory) {
                        $map .= '<url>';
                        $map .= '<loc>'.site_url('category/'.$category->slug.'/'.$subcategory->slug).'</loc> ';
                        $map .= '<priority>0.8</priority>';
                        $map .= '</url>';
                    }
                }
            }
        }

        if (!empty($brands)) {
            foreach($brands as $brand) {
                $map .= '<url>';
                $map .= '<loc>'.shop_url($brand->slug).'</loc> ';
                $map .= '<priority>0.8</priority>';
                $map .= '</url>';
            }
        }

        if (!empty($products)) {
            foreach($products as $products) {
                $map .= '<url>';
                $map .= '<loc>'.site_url('product/'.$products->slug).'</loc> ';
                $map .= '<priority>0.6</priority>';
                $map .= '</url>';
            }
        }

        if (!empty($pages)) {
            foreach($pages as $page) {
                $map .= '<url>';
                $map .= '<loc>'.site_url('page/'.$page->slug).'</loc> ';
                $map .= '<priority>0.8</priority>';
                $map .= '<changefreq>yearly</changefreq>';
                if ($page->updated_at) {
                    $map .= '<lastmod>'.date('Y-m-d', strtotime($page->updated_at)).'</lastmod>';
                }
                $map .= '</url>';
            }
        }

        $map .= '</urlset>';
        file_put_contents('sitemap.xml', $map);
        header('Location: '.base_url('sitemap.xml'));
        exit;
    }

    public function updates()
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->form_validation->set_rules('purchase_code', lang("purchase_code"), 'required');
        $this->form_validation->set_rules('envato_username', lang("envato_username"), 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->db->update('shop_settings', array('purchase_code' => $this->input->post('purchase_code', TRUE), 'envato_username' => $this->input->post('envato_username', TRUE)), array('shop_id' => 1));
            admin_redirect('shop_settings/updates');
        } else {
            $shop_settings = $this->shop_admin_model->getShopSettings();
            $fields = array('version' => $shop_settings->version, 'code' => $shop_settings->purchase_code, 'username' => $shop_settings->envato_username, 'site' => base_url());
            $this->load->helper('update');
            $protocol = is_https() ? 'https://' : 'http://';
            $updates = get_remote_contents($protocol . 'api.tecdiary.com/v1/update/', $fields);
            $this->data['shop_settings'] = $shop_settings;
            $this->data['updates'] = json_decode($updates);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('updates')));
            $meta = array('page_title' => lang('updates'), 'bc' => $bc);
            $this->page_construct('shop/updates', $meta, $this->data);
        }
    }

    public function install_update($file, $m_version, $version)
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->load->helper('update');
        save_remote_file($file . '.zip');
        $this->sma->unzip('./files/updates/' . $file . '.zip');
        if ($m_version) {
            $this->load->library('migration');
            if (!$this->migration->latest()) {
                $this->session->set_flashdata('error', $this->migration->error_string());
                admin_redirect("shop_settings/updates");
            }
        }
        $this->db->update('shop_settings', array('version' => $version), array('shop_id' => 1));
        unlink('./files/updates/' . $file . '.zip');
        $this->session->set_flashdata('success', lang('update_done'));
        admin_redirect("shop_settings/updates");
    }

    function sms_settings() {

        $this->sma->checkPermissions();
        // $this->form_validation->set_rules('auto_send', lang('auto_send'), 'trim|required');
        $this->form_validation->set_rules('gateway', lang('gateway'), 'trim|required');

        if ($this->input->post('gateway') == 'Custom') {
            $this->form_validation->set_rules('Custom_url', lang('url'), 'trim|required');
            $this->form_validation->set_rules('Custom_send_to_name', lang('send_to_name'), 'trim|required');
            $this->form_validation->set_rules('Custom_msg_name', lang('msg_name'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Clickatell') {
            $this->form_validation->set_rules('Clickatell_apiKey', lang('apiKey'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Gupshup') {
            $this->form_validation->set_rules('Gupshup_userid', lang('userid'), 'trim|required');
            $this->form_validation->set_rules('Gupshup_password', lang('password'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Itexmo') {
            $this->form_validation->set_rules('Itexmo_api_code', lang('api_code'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'MVaayoo') {
            $this->form_validation->set_rules('MVaayoo_user', lang('MVaayoo_user'), 'trim|required');
            $this->form_validation->set_rules('MVaayoo_senderID', lang('senderID'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'SmsAchariya') {
            $this->form_validation->set_rules('SmsAchariya_domain', lang('domain'), 'trim|required');
            $this->form_validation->set_rules('SmsAchariya_uid', lang('uid'), 'trim|required');
            $this->form_validation->set_rules('SmsAchariya_pin', lang('pin'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'SmsCountry') {
            $this->form_validation->set_rules('SmsCountry_user', lang('user'), 'trim|required');
            $this->form_validation->set_rules('SmsCountry_passwd', lang('passwd'), 'trim|required');
            $this->form_validation->set_rules('SmsCountry_sid', lang('sid'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'SmsLane') {
            $this->form_validation->set_rules('SmsLane_user', lang('user'), 'trim|required');
            $this->form_validation->set_rules('SmsLane_password', lang('password'), 'trim|required');
            $this->form_validation->set_rules('SmsLane_sid', lang('sid'), 'trim|required');
            $this->form_validation->set_rules('SmsLane_gwid', lang('gwid'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Nexmo') {
            $this->form_validation->set_rules('Nexmo_api_key', lang('api_key'), 'trim|required');
            $this->form_validation->set_rules('Nexmo_api_secret', lang('api_secret'), 'trim|required');
            $this->form_validation->set_rules('Nexmo_from', lang('from'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Twilio') {
            $this->form_validation->set_rules('Twilio_account_sid', lang('account_sid'), 'trim|required');
            $this->form_validation->set_rules('Twilio_auth_token', lang('auth_token'), 'trim|required');
            $this->form_validation->set_rules('Twilio_from', lang('from'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Mocker') {
            $this->form_validation->set_rules('Mocker_sender_id', lang('sender_id'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Infobip') {
            $this->form_validation->set_rules('Infobip_username', lang('username'), 'trim|required');
            $this->form_validation->set_rules('Infobip_password', lang('password'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Bulksms') {
            $this->form_validation->set_rules('Bulksms_eapi_url', lang('eapi_url'), 'trim|required');
            $this->form_validation->set_rules('Bulksms_username', lang('username'), 'trim|required');
            $this->form_validation->set_rules('Bulksms_password', lang('password'), 'trim|required');
        } elseif ($this->input->post('gateway') == 'Smsapi') {
            $this->form_validation->set_rules('Smsapi_access_token', lang('access_token'), 'trim|required');
            $this->form_validation->set_rules('Smsapi_from', lang('from'), 'trim|required');
        }

        if ($this->form_validation->run() == true) {

            $Custom = [
                'url' => $this->input->post('Custom_url'),
                'params' => [
                    'send_to_name' => $this->input->post('Custom_send_to_name'),
                    'msg_name' => $this->input->post('Custom_msg_name'),
                    'keys' => [
                        'param1' => $this->input->post('Custom_param1_key'),
                        'param2' => $this->input->post('Custom_param2_key'),
                        'param3' => $this->input->post('Custom_param3_key'),
                        'param4' => $this->input->post('Custom_param4_key'),
                        'param5' => $this->input->post('Custom_param5_key')
                    ],
                    'others' => [
                        $this->input->post('Custom_param1_key') => $this->input->post('Custom_param1_value'),
                        $this->input->post('Custom_param2_key') => $this->input->post('Custom_param2_value'),
                        $this->input->post('Custom_param3_key') => $this->input->post('Custom_param3_value'),
                        $this->input->post('Custom_param4_key') => $this->input->post('Custom_param4_value'),
                        $this->input->post('Custom_param5_key') => $this->input->post('Custom_param5_value')
                    ]
                ]
            ];

            $data = [
                'gateway' => DEMO ? 'Log' : $this->input->post('gateway'),
                'Custom' => $Custom,
                'Clickatell' => [
                    'apiKey'  => $this->input->post('Clickatell_apiKey')
                ],
                'Gupshup' => [
                    'userid'  => $this->input->post('Gupshup_userid'),
                    'password' => $this->input->post('Gupshup_password')
                ],
                'Itexmo' => ['api_code'  => $this->input->post('Itexmo_api_code')],
                'MVaayoo' => [
                    'user'  => $this->input->post('MVaayoo_user'),
                    'senderID'  => $this->input->post('MVaayoo_senderID')
                ],
                'SmsAchariya' => [
                    'domain'  => $this->input->post('SmsAchariya_domain'),
                    'uid'  => $this->input->post('SmsAchariya_uid'),
                    'pin' => $this->input->post('SmsAchariya_pin')
                ],
                'SmsCountry' => [
                    'user' => $this->input->post('SmsCountry_user'),
                    'passwd' => $this->input->post('SmsCountry_passwd'),
                    'sid' => $this->input->post('SmsCountry_sid')
                ],
                'SmsLane' => [
                    'user' => $this->input->post('SmsLane_user'),
                    'password' => $this->input->post('SmsLane_password'),
                    'sid' => $this->input->post('SmsLane_sid'),
                    'gwid' => $this->input->post('SmsLane_gwid')
                ],
                'Nexmo' => [
                    'api_key' => $this->input->post('Nexmo_api_key'),
                    'api_secret' => $this->input->post('Nexmo_api_secret'),
                    'from'  => $this->input->post('Nexmo_from')
                ],
                'Twilio' => [
                    'account_sid' => $this->input->post('Twilio_account_sid'),
                    'auth_token' => $this->input->post('Twilio_auth_token'),
                    'from'  => $this->input->post('Twilio_from')
                ],
                'Mocker' => ['sender_id'  => $this->input->post('Mocker_sender_id')],
                'Infobip' => [
                    'username'  => $this->input->post('Infobip_username'),
                    'password' => $this->input->post('Infobip_password')
                ],
                'Bulksms' => [
                    'eapi_url' => $this->input->post('Bulksms_eapi_url'),
                    'username'  => $this->input->post('Bulksms_username'),
                    'password' => $this->input->post('Bulksms_password')
                ],
                'Smsapi' => [
                    'access_token' => $this->input->post('Smsapi_access_token'),
                    'from' => $this->input->post('Smsapi_from')
                ]
            ];

            $sms_config = [
                'auto_send' => $this->input->post('auto_send'),
                'config' => json_encode($data)
            ];

        }

        if ($this->form_validation->run() == true && $this->shop_admin_model->updateSmsSettings($sms_config)) {

            $this->session->set_flashdata('message', lang('settings_updated'));
            admin_redirect("shop_settings/sms_settings");

        } else {

            $sms_settings = $this->site->getSmsSettings();
            $sms_settings->config = json_decode($sms_settings->config);
            $this->data['sms_settings'] = $sms_settings;
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sms_settings')));
            $meta = array('page_title' => lang('sms_settings'), 'bc' => $bc);
            $this->page_construct('shop/sms_settings', $meta, $this->data);
        }
    }

    function sms_log($date = null) {
        $this->sma->checkPermissions();
        if (!$date) { $date = date('Y-m-d'); }
        $file = APPPATH . 'logs/' . 'sms-'.$date.'.log';
        if (file_exists($file)) {
            $log = file_get_contents($file, FILE_USE_INCLUDE_PATH, null);
            $lines = explode("\n", $log);
            array_walk($lines, function(&$item) {
                if (strpos($item, 'SMS.ERROR') !== false) {
                    $item = "<span class='text-danger' style='white-space: normal;'>{$item}</span>";
                } else {
                    $item = "<span style='white-space: normal;'>{$item}</span>";
                }
            });
            $content = implode("\n\n", $lines);
        } else {
            $content = "<span class='text-danger'>".lang('log_x_exists')."</span>";
        }

        $this->data['log'] = $content;
        $this->data['date'] = $date;
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sms_log')));
        $meta = array('page_title' => lang('sms_log'), 'bc' => $bc);
        $this->page_construct('shop/log', $meta, $this->data);
    }

    function send_sms($date = null) {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('mobile', lang('mobile'), 'trim|required');
        $this->form_validation->set_rules('message', lang('message'), 'trim|required');
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        if ($this->form_validation->run() == true) {
            $this->load->library('sms');
            $res = $this->sms->send(substr($this->input->post('mobile'), 0, -1), $this->input->post('message'));
            if (isset($res['error']) && $res['error']) {
                $this->data['error'] = lang('sms_request_failed');
            } else {
                $this->data['message'] = lang('sms_request_sent');
            }
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('send_sms')));
        $meta = array('page_title' => lang('send_sms'), 'bc' => $bc);
        $this->page_construct('shop/send_sms', $meta, $this->data);

    }
    public function getLogoList()
        {
            $this->load->helper('directory');
            $dirname = "assets/images/social_networks_logos";
            $ext = array("jpg", "png", "jpeg", "gif");
            $logo_files = array();
            // exit(var_dump($dirname));
            if ($handle = opendir($dirname)) {
                while (false !== ($file = readdir($handle)))
                    for ($i = 0; $i < sizeof($ext); $i++)
                        if (stristr($file, "." . $ext[$i])) {
                            // $img_size = getimagesize(base_url().$dirname."/".$file);
                            // $img_width = $img_size[0];
                            // $img_height = $img_size[1];
                            // if ($img_width > $img_height) {
                            //     $rectangular_files[] = $file;
                            // } else if ($img_width == $img_height) {
                                $logo_files[] = $file;
                            // }
                        }
                closedir($handle);
            }
            sort($logo_files);
            return ['logo_files' => $logo_files];
        }
}
