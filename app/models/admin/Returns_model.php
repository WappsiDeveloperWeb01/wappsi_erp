<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Returns_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('sales_model');
    }

    public function getProductNames($term)
    {
        $limit = $this->Settings->max_num_results_display;
        $this->db->where("(name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('returns', ['id' => $id], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getReturnItems($return_id)
    {
        $this->db->select('return_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=return_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=return_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=return_items.tax_rate_id', 'left')
            ->where('return_id', $return_id)
            ->group_by('return_items.id')
            ->order_by('id', 'asc');

        $q = $this->db->get('return_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function addReturn($data = array(), $items = array())
    {
        if ($this->db->insert('returns', $data)) {
            $return_id = $this->db->insert_id();
            if ($this->site->getReference('re') == $data['reference_no']) {
                $this->site->updateReference('re');
            }

            foreach ($items as $item) {
                $item['return_id'] = $return_id;
                $this->db->insert('return_items', $item);
                if ($item['product_type'] == 'standard') {
                    $clause = ['product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item['option_id']];
                    $this->site->setPurchaseItem($clause, $item['quantity']);
                    $this->site->syncQuantity(null, null, null, $item['product_id']);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item['product_id']);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, ($combo_item->qty*$item['quantity']));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function updateReturn($id, $data = array(), $items = array())
    {
        $this->resetSaleActions($id);

        if ($this->db->update('returns', $data, array('id' => $id)) && $this->db->delete('return_items', array('return_id' => $id))) {
            // $return_id = $id;
            foreach ($items as $item) {
                // $item['return_id'] = $return_id;
                $this->db->insert('return_items', $item);
                if ($item['product_type'] == 'standard') {
                    $clause = ['product_id' => $item['product_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item['option_id']];
                    $this->site->setPurchaseItem($clause, $item['quantity']);
                    $this->site->syncQuantity(null, null, null, $item['product_id']);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item['product_id']);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, ($combo_item->qty*$item['quantity']));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
            return true;
        }

        return false;
    }

    public function resetSaleActions($id)
    {
        if ($items = $this->getReturnItems($id)) {
            foreach ($items as $item) {
                if ($item->product_type == 'standard') {
                    $clause = ['product_id' => $item->product_id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item->option_id];
                    $this->site->setPurchaseItem($clause, (0-$item->quantity));
                    $this->site->syncQuantity(null, null, null, $item->product_id);
                } elseif ($item->product_type == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item->product_id);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, (0-($combo_item->qty*$item->quantity)));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
        }
    }

    public function deleteReturn($id)
    {
        $this->resetSaleActions($id);
        if ($this->db->delete('return_items', array('return_id' => $id)) && $this->db->delete('returns', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getProductOptions($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_sale_reference($reference, $customer_id = NULL, $is_electronic_document = null, $module = null)
    {
        $this->db->select("sales.id, reference_no AS text, sales.id AS value");
        $this->db->from("sales");
        $this->db->join("documents_types", "documents_types.id = sales.document_type_id", "inner");
        if ($customer_id != NULL) { $this->db->where("customer_id", $customer_id); }
        $this->db->like("reference_no", $reference);
        // $this->db->where("return_sale_ref IS NULL");

        if ($module == NOTA_CREDITO_POS) {
            $this->db->where("module", FACTURA_POS);
        } else if ($module == NOTA_CREDITO_DETAL) {
            $this->db->where("module", FACTURA_DETAL);
        }

        $this->db->order_by("id", "DESC");
        $response = $this->db->get();

        return $response->result();
    }

    public function get_sale_reference_by_id($id, $customer_id = NULL, $is_electronic_document = null, $module = null)
    {
        $this->db->select("sales.id, reference_no AS text, sales.id AS value");
        $this->db->from("sales");
        $this->db->join("documents_types", "documents_types.id = sales.document_type_id", "inner");
        if ($customer_id != NULL) { $this->db->where("customer_id", $customer_id); }
        $this->db->where("sales.id", $id);
        // $this->db->where("return_sale_ref IS NULL");
        $this->db->where("module", $module);
        $this->db->order_by("id", "DESC");

        $response = $this->db->get();

        return $response->result();
    }

    public function get_sale($id)
    {
        $this->db->select("sales.*,
                            sales.id AS reference_invoice_id,
                            sales.reference_no AS invoice_reference,
                            sales.customer_id,
                            sales.customer AS customer_name,
                            sales.grand_total,
                            sales.biller_id AS branch_id,
                            sales.biller AS biller_branch_name,
                            DATE(date) AS date,
                            sales.pos AS module,
                            sales.rete_fuente_total AS total_withholdings_fuente,
                            sales.rete_fuente_percentage AS fuente_withholdings_percentage,
                            sales.rete_iva_total AS total_withholdings_iva,
                            sales.rete_iva_percentage AS iva_withholdings_percentage,
                            sales.rete_ica_total AS total_withholdings_ica,
                            sales.rete_ica_percentage AS ica_withholdings_percentage,
                            sales.rete_other_total AS total_withholdings_other,
                            sales.rete_other_percentage AS other_withholdings_percentage,
                            sales.seller_id AS seller_id,
                            companies.name AS branch_name,
                            addresses.id AS addresses_id,
                            addresses.sucursal AS customer_branch,
                            documents_types.factura_electronica AS is_electronic_invoice,
                            sales.rete_fuente_id,
                            sales.rete_iva_id,
                            sales.rete_ica_id,
                            sales.rete_other_id,
                            documents_types.module");
        $this->db->join("companies", "companies.id = sales.biller_id", "inner");
        $this->db->join("addresses", "addresses.id = sales.address_id", "inner");
        $this->db->join("documents_types", "documents_types.id = sales.document_type_id", "inner");
        $q = $this->db->get_where("sales", ["sales.id" => $id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_total_value_credit_notes_by_sale_id($sale_id)
    {
        $this->db->select("SUM(grand_total) AS total_return");
        $this->db->from("sales_returns");
        $this->db->join("sales", "sales.id = sales_returns.return_id", "inner");
        $this->db->where("sales_returns.sale_id", $sale_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_total_value_debit_notes_by_reference($reference)
    {
        $this->db->select("SUM(grand_total) AS total_debit");
        $this->db->from("sales");
        $this->db->where("sales.reference_no", $reference);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_credit_note_concepts()
    {
        $this->db->select("debit_credit_notes_concepts.id,
                        debit_credit_notes_concepts.dian_code,
                        debit_credit_notes_concepts.description,
                        debit_credit_notes_concepts.tax_amount_ledger_account,
                        debit_credit_notes_concepts.tax_base_ledger_account,
                        COALESCE(".$this->db->dbprefix("tax_rates").".rate, 0) AS concept_tax_rate,
                        tax_rates.id AS concept_tax_rate_id");
        $this->db->from("debit_credit_notes_concepts");
        $this->db->join("tax_rates", "tax_rates.id = debit_credit_notes_concepts.tax_id", "left");
        $this->db->where("debit_credit_notes_concepts.type", "NC");
        $this->db->where("debit_credit_notes_concepts.description IS NOT NULL");
        $this->db->where("debit_credit_notes_concepts.view", 1);
        $response = $this->db->get();

        return $response->result();
    }

    public function getCreditNoteConceptbyId($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("debit_credit_notes_concepts");
        return $q->row();
    }

    public function save_credit_note($data = [], $items = [], $payments =[], $externalInvoice = FALSE)
    {
        if (empty($data) || empty($items)) { return FALSE; }
        $document_type = $this->get_document_type($data["document_type_id"]);
        $data["reference_no"] = $document_type->prefix ."-". $document_type->consecutive;
        if ($this->db->insert("sales", $data)) {
            $sale_id = $this->db->insert_id();
            $data = $this->validateConsecutiveDocument($sale_id, $data, $document_type->consecutive);
            if ($externalInvoice == FALSE) {
                $this->db->query("UPDATE {$this->db->dbprefix('sales')}
                SET {$this->db->dbprefix('sales')}.return_sale_ref = CONCAT(IF({$this->db->dbprefix('sales')}.return_sale_ref IS NOT NULL, CONCAT({$this->db->dbprefix('sales')}.return_sale_ref, ', '), ''), '".$data['reference_no']."'), return_other_concepts = 1
                WHERE {$this->db->dbprefix('sales')}.id = ".$data['sale_id']);
                $this->db->insert('sales_returns', [
                    'sale_id'=> $data['sale_id'],
                    'sale_reference_no'=> $data['return_sale_ref'],
                    'return_id'=> $sale_id,
                    'return_reference_no'=> $data['reference_no']
                ]);
            }
            $items["sale_id"] = $sale_id;
            $total_payment = 0;
            $total_deposit = 0;
            $pay_reference = NULL;
            foreach ($payments as $payment) {
                $payment['sale_id'] = $sale_id;
                // $this->sma->print_arrays($payment);
                if (array_key_exists('deposit_document_type_id', $payment)) {
                    // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
                    $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $payment['deposit_document_type_id']);
                    if($referenceBiller){
                        $reference_deposit = $referenceBiller;
                    }else{
                        $reference_deposit = $this->site->getReference('rc');
                    }
                    $ref = explode("-", $reference_deposit);
                    $deposit_consecutive = $ref[1];
                    // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
                    $deposit_data = array(
                        'date' => date('Y-m-d H:i:s'),
                        'reference_no' => $reference_deposit,
                        'amount' => $payment['amount'] * -1,
                        'balance' => $payment['amount'] * -1,
                        // 'paid_by' => 'cash',
                        'paid_by' => $payment['paid_by'],
                        'note' => 'Anticipo generado automáticamente por devolución',
                        'company_id' => $data['customer_id'],
                        'created_by' => $this->session->userdata('user_id'),
                        'biller_id' => $data['biller_id'],
                        'document_type_id' => $payment['deposit_document_type_id'],
                        'origen_reference_no' => $data['reference_no'],
                        'origen_document_type_id' => $data['document_type_id'],
                        'origen' => '2',
                    );
                    if ($this->db->insert('deposits', $deposit_data)) {
                        $this->site->updateBillerConsecutive($payment['deposit_document_type_id'], $deposit_consecutive+1);
                        $this->site->wappsiContabilidadDeposito($deposit_data);
                        $customer = $this->site->getCompanyByID($data['customer_id']);
                        $this->db->update('companies', ['deposit_amount' => ($customer->deposit_amount + ($payment['amount'] * -1) )], ['id'=> $customer->id]);
                    }
                    unset($payment['deposit_document_type_id']);
                    $this->db->insert('payments', $payment);
                    $total_deposit+= $payment['amount'] * -1;
                } else {
                    if ($payment['paid_by'] != 'retencion' && isset($payment['document_type_id'])) {
                        // $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $payment['document_type_id']);
                        // if($referenceBiller){
                        //     $pay_reference = $referenceBiller;
                        // }
                        // $payment['reference_no'] = $pay_reference;
                        // $ref = explode("-", $pay_reference);
                        // $p_consecutive = $ref[1];
                    } else {
                        $pay_reference = $this->site->getReference('pay');
                        $payment['reference_no'] = $pay_reference;
                    }
                    if ($payment['paid_by'] != 'due' && $payment['paid_by'] != 'retencion') {
                        $total_payment += $payment['amount'];
                    }
                    $this->db->insert('payments', $payment);
                }
            }

            if ($externalInvoice == FALSE) {
                $sale = $this->site->getWappsiSaleById($data['sale_id']);
                if ($sale->payment_status == 'partial') {
                    $total_retenciones = (isset($data['rete_fuente_total']) ? $data['rete_fuente_total'] : 0) + (isset($data['rete_iva_total']) ? $data['rete_iva_total'] : 0) + (isset($data['rete_ica_total']) ? $data['rete_ica_total'] : 0) + (isset($data['rete_other_total']) ? $data['rete_other_total'] : 0);
                    $tabla = 'payments';
                    $payment_add = array();
                    $payment_add['date'] = $data['date'];
                    $payment_add['sale_id'] = $data['sale_id'];
                    $payment_add['amount'] = ($data['grand_total'] * -1) - $total_retenciones -  $total_deposit - ($total_payment * -1);
                    $payment_add['reference_no'] = $sale->return_sale_ref;
                    $payment_add['paid_by'] = "due";
                    $payment_add['type'] = "devolución";
                    $payment_add['note'] = "Saldo de devolución restante de crédito ".$data['reference_no'];
                    $payment_add['created_by'] = $this->session->userdata('user_id');
                    $this->db->insert($tabla,$payment_add);
                } else if ($data['payment_status'] == 'paid' && $sale->payment_status != 'paid') {
                    $total_retenciones = (isset($data['rete_fuente_total']) ? $data['rete_fuente_total'] : 0) + (isset($data['rete_iva_total']) ? $data['rete_iva_total'] : 0) + (isset($data['rete_ica_total']) ? $data['rete_ica_total'] : 0) + (isset($data['rete_other_total']) ? $data['rete_other_total'] : 0);
                    $tabla = 'payments';
                    $payment_add = array();
                    $payment_add['date'] = $data['date'];
                    $payment_add['sale_id'] = $data['sale_id'];
                    $payment_add['amount'] = ($data['grand_total'] * -1) - $total_deposit - $total_retenciones;
                    $payment_add['reference_no'] = $sale->return_sale_ref;
                    $payment_add['paid_by'] = "due";
                    $payment_add['type'] = "devolución";
                    $payment_add['note'] = "Saldo de devolución restante de crédito ".$data['reference_no'];
                    $payment_add['created_by'] = $this->session->userdata('user_id');
                    $this->db->insert($tabla,$payment_add);
                }
            }

            $this->site->syncSalePayments($sale_id);
            $biller_data = $this->site->getAllCompaniesWithState('biller', $data['biller_id']);
            if ($biller_data->default_customer_id != $data['customer_id']) {
                $this->sma->update_award_points($data);
            }
            if ($externalInvoice == FALSE) { $this->site->syncSalePayments($data['sale_id']); }
            $this->site->wappsiContabilidadVentasDevolucionOtherConcepts($sale_id, $data, $items, $payments);
            if ($this->db->insert("sale_items", $items)) {
                return $sale_id;
            }
            return FALSE;
        }
        return FALSE;
    }

    private function validateConsecutiveDocument($sale_id, $data, $consecutive)
    {
        $check_reference_data = [
            'table' => 'sales',
            'record_id' => $sale_id,
            'reference' => $data['reference_no'],
            'biller_id' => $data['biller_id'],
            'document_type_id' => $data['document_type_id']
        ];
        if ($new_reference = $this->site->check_reference($check_reference_data)) {
            $data['reference_no'] = $new_reference;
        }
        return $data;
    }

    public function get_document_type($id)
    {
        $this->db->select("sales_consecutive AS consecutive, sales_prefix AS prefix");
        $this->db->from("documents_types");
        $this->db->where("id", $id);
        $response = $this->db->get();

        return $response->row();
    }

    public function update_consecutive_document_type($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("documents_types", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function update_reference_invoice($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("sales", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function add_return_past_year($data = [], $items = [], $payments = [])
    {
        $cost = $this->site->costing($items, TRUE);
        if (isset($data['year_database'])) {
            $year_database = $data['year_database'];
            $DB3 = $this->site->get_past_year_connection($year_database);
            // unset($data['year_database']);
        }
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        $ref = explode("-", $referenceBiller);
        $data['reference_no'] = $referenceBiller;
        $consecutive = $ref[1];
        if ($this->db->insert('sales', $data)) {
            $return_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($items as $item) {
                if (isset($item['sale_item_id'])) {
                    $sale_item_id = $item['sale_item_id'];
                    $DB3->query("UPDATE {$DB3->dbprefix('sale_items')} SET {$DB3->dbprefix('sale_items')}.returned_quantity = COALESCE({$DB3->dbprefix('sale_items')}.returned_quantity, 0) + ".($item['quantity'] * -1)." WHERE {$DB3->dbprefix('sale_items')}.id = ".$sale_item_id);
                    unset($item['sale_item_id']);
                }
                $item['sale_id'] = $return_id;
                $this->db->insert('sale_items', $item);
                if ($item['product_type'] == 'standard') {
                    $clause = ['product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item['option_id']];
                    $this->site->setPurchaseItem($clause, $item['quantity']);
                    $this->site->syncQuantity(null, null, null, $item['product_id']);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item['product_id']);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, ($combo_item->qty*$item['quantity']));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
            foreach ($payments as $payment) {
                if ($payment['paid_by'] == 'retencion') {
                    $payment['sale_id'] = $return_id;
                    $this->db->insert('payments', $payment);
                } else {
                    $pmntreferenceBiller = $this->site->getReferenceBiller($data['biller_id'], $payment['document_type_id']);
                    $pmntref = explode("-", $pmntreferenceBiller);
                    $payment['reference_no'] = $pmntreferenceBiller;
                    $pmntconsecutive = $pmntref[1];
                    $payment['sale_id'] = $return_id;
                    if ($payment['paid_by'] == 'deposit') {
                        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $payment['deposit_document_type_id']);
                        if($referenceBiller){
                            $reference_deposit = $referenceBiller;
                        }
                        $ref = explode("-", $reference_deposit);
                        $deposit_consecutive = $ref[1];
                        // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
                        $deposit_data = array(
                            'date' => date('Y-m-d H:i:s'),
                            'reference_no' => $reference_deposit,
                            'amount' => $payment['amount'] * -1,
                            'balance' => $payment['amount'] * -1,
                            // 'paid_by' => 'cash',
                            'paid_by' => $payment['paid_by'],
                            'note' => 'Anticipo generado automáticamente por devolución',
                            'company_id' => $data['customer_id'],
                            'created_by' => $this->session->userdata('user_id'),
                            'biller_id' => $data['biller_id'],
                            'document_type_id' => $payment['deposit_document_type_id'],
                            'origen_reference_no' => $data['reference_no'],
                            'origen_document_type_id' => $data['document_type_id'],
                            'origen' => '2',
                        );
                        if (isset($data['cost_center_id'])) {
                            $deposit_data['cost_center_id'] = $data['cost_center_id'];
                        }
                        if ($this->db->insert('deposits', $deposit_data)) {
                            $this->site->updateBillerConsecutive($payment['deposit_document_type_id'], $deposit_consecutive+1);
                            $this->site->wappsiContabilidadDeposito($deposit_data);
                            $customer = $this->site->getCompanyByID($data['customer_id']);
                            $this->db->update('companies', ['deposit_amount' => ($customer->deposit_amount + ($payment['amount'] * -1) )], ['id'=> $customer->id]);
                        }
                        unset($payment['deposit_document_type_id']);
                        $this->db->insert('payments', $payment);
                    } else {
                        $this->db->insert('payments', $payment);
                        $this->site->updateBillerConsecutive($payment['document_type_id'], $pmntconsecutive+1);
                        if ($payment['paid_by'] == 'due') {
                            $rref = $this->db->get_where('sales', ['reference_no'=>"SI".$data['return_sale_ref']]);
                            if ($rref->num_rows() > 0) {
                                $rref = $rref->row();
                                $data_payment = array(
                                    'date' => $data['date'],
                                    'document_type_id' => $payment['document_type_id'],
                                    'reference_no' => $payment['reference_no'],
                                    'amount' => ($payment['amount'] * -1),
                                    'sale_id' => $rref->id,
                                    'paid_by' => 'due',
                                    'cheque_no' => NULL,
                                    'cc_no' => NULL,
                                    'cc_holder' => NULL,
                                    'cc_month' => NULL,
                                    'cc_year' => NULL,
                                    'cc_type' => NULL,
                                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                    'type' => 'returned',
                                );
                                $this->db->insert('payments', $data_payment);
                                $this->site->syncSalePayments($rref->id);
                            }
                        }
                    }
                }
            }
            $this->sales_model->recontabilizarVenta($return_id);
            return $return_id;
        }
        return false;
    }

    public function add_return_purchase_past_year($data = [], $items = [], $payments = [])
    {
        // $this->sma->print_arrays($data, $items);
        if (isset($data['year_database'])) {
            $year_database = $data['year_database'];
            $DB3 = $this->site->get_past_year_connection($year_database);
            // unset($data['year_database']);
        }
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        $ref = explode("-", $referenceBiller);
        $data['reference_no'] = $referenceBiller;
        $consecutive = $ref[1];
        if ($this->db->insert('purchases', $data)) {
            $return_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($items as $item) {
                if (isset($item['option_id']) && $item['option_id']) {
                    $exist_option = $this->db->get_where('product_variants', ['id'=>$item['option_id']]);
                    if ($exist_option->num_rows() > 0) {
                    } else {
                        $option = $this->site->get_past_year_option_by_id($item['option_id'], $year_database);
                        $insert_option = [
                            'id' => $option->id,
                            'product_id' => $option->product_id,
                            'name' => $option->name,
                            'cost' => $option->cost,
                            'price' => $option->price,
                            'quantity' => $option->quantity,
                            'suffix' => $option->suffix,
                            'color' => $option->color,
                            'last_update' => $option->last_update
                        ];
                        $this->db->insert('product_variants', $insert_option);
                    }
                }
                if (isset($item['purchase_item_id'])) {
                    $purchase_item_id = $item['purchase_item_id'];
                    $DB3->query("UPDATE {$DB3->dbprefix('purchase_items')} SET {$DB3->dbprefix('purchase_items')}.returned_quantity = COALESCE({$DB3->dbprefix('purchase_items')}.returned_quantity, 0) + ".($item['quantity'] * -1)." WHERE {$DB3->dbprefix('purchase_items')}.id = ".$purchase_item_id);
                    unset($item['purchase_item_id']);
                }
                $item['purchase_id'] = $return_id;
                $this->db->insert('purchase_items', $item);
                $clause = ['product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item['option_id']];
                $this->site->setPurchaseItem($clause, $item['quantity']);
                $this->site->syncQuantity(null, null, null, $item['product_id']);
            }
            foreach ($payments as $payment) {
                if ($payment['paid_by'] == 'retencion') {
                    $payment['purchase_id'] = $return_id;
                    $this->db->insert('payments', $payment);
                } else {
                    $pmntreferenceBiller = $this->site->getReferenceBiller($data['biller_id'], $payment['document_type_id']);
                    $pmntref = explode("-", $pmntreferenceBiller);
                    $payment['reference_no'] = $pmntreferenceBiller;
                    $pmntconsecutive = $pmntref[1];
                    $payment['purchase_id'] = $return_id;
                    if ($payment['paid_by'] == 'deposit') {
                        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $payment['deposit_document_type_id']);
                        if($referenceBiller){
                            $reference_deposit = $referenceBiller;
                        }
                        $ref = explode("-", $reference_deposit);
                        $deposit_consecutive = $ref[1];
                        // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
                        $deposit_data = array(
                            'date' => date('Y-m-d H:i:s'),
                            'reference_no' => $reference_deposit,
                            'amount' => $payment['amount'] * -1,
                            'balance' => $payment['amount'] * -1,
                            // 'paid_by' => 'cash',
                            'paid_by' => $payment['paid_by'],
                            'note' => 'Anticipo generado automáticamente por devolución',
                            'company_id' => $data['supplier_id'],
                            'created_by' => $this->session->userdata('user_id'),
                            'biller_id' => $data['biller_id'],
                            'document_type_id' => $payment['deposit_document_type_id'],
                            'origen_reference_no' => $data['reference_no'],
                            'origen_document_type_id' => $data['document_type_id'],
                            'origen' => '2',
                        );
                        if (isset($data['cost_center_id'])) {
                            $deposit_data['cost_center_id'] = $data['cost_center_id'];
                        }
                        if ($this->db->insert('deposits', $deposit_data)) {
                            $this->site->updateBillerConsecutive($payment['deposit_document_type_id'], $deposit_consecutive+1);
                            $this->site->wappsiContabilidadDeposito($deposit_data);
                            $customer = $this->site->getCompanyByID($data['supplier_id']);
                            $this->db->update('companies', ['deposit_amount' => ($customer->deposit_amount + ($payment['amount'] * -1) )], ['id'=> $customer->id]);
                        }
                        unset($payment['deposit_document_type_id']);
                        $this->db->insert('payments', $payment);
                    } else {
                        $this->db->insert('payments', $payment);
                        $this->site->updateBillerConsecutive($payment['document_type_id'], $pmntconsecutive+1);
                        if ($payment['paid_by'] == 'due') {
                            $rref = $this->db->get_where('purchases', ['reference_no'=>"SI".$data['return_purchase_ref']]);
                            if ($rref->num_rows() > 0) {
                                $rref = $rref->row();
                                $data_payment = array(
                                    'date' => $data['date'],
                                    'document_type_id' => $payment['document_type_id'],
                                    'reference_no' => $payment['reference_no'],
                                    'amount' => ($payment['amount'] * -1),
                                    'purchase_id' => $rref->id,
                                    'paid_by' => 'due',
                                    'cheque_no' => NULL,
                                    'cc_no' => NULL,
                                    'cc_holder' => NULL,
                                    'cc_month' => NULL,
                                    'cc_year' => NULL,
                                    'cc_type' => NULL,
                                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                    'type' => 'returned',
                                );
                                $this->db->insert('payments', $data_payment);
                                $this->site->syncpurchasePayments($rref->id);
                            }
                        }
                    }
                }
            }
            $this->purchases_model->recontabilizarCompra($return_id);
            return $return_id;
        }
        return false;
    }

    public function get_moduleById($id){
        $q = $this->db->get_where('documents_types', ['id' => $id], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
}
