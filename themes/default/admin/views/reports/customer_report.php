<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row wrapper border-bottom white-bg page-heading no-printable">
   <div class="col-lg-12">
      <h2>
         <?php if ($customer->type_person == NATURAL_PERSON) { ?>
            <?=lang('customer')." : ".$customer->name ?>
         <?php } else { ?>
            <?=lang('customer')." : ".$customer->name ?>
            <?=lang('social_reason')." : ".$customer->company ?>
         <?php } ?>
      </h2>
   </div>
</div>

<div class="clearfix"></div>
<div class="row">
   <div class="col-md-2">
      <div class="small-box padding1010 bblue">
         <h3 class="sales_amount_text"><?= isset($sales->total_amount) ? $this->sma->formatMoney($sales->total_amount) : '0.00' ?></h3>
         <p><?= lang('sales_amount') ?></p>
      </div>
   </div>
   <div class="col-md-2">
      <div class="small-box padding1010 bdarkGreen">
         <h3 class="sales_amount_text"><?= isset($sales->paid) ? $this->sma->formatMoney($sales->paid) : '0.00' ?></h3>
         <p><?= lang('total_paid') ?></p>
      </div>
   </div>
   <div class="col-md-2">
      <div class="small-box padding1010 borange">
         <h3 class="sales_amount_text"><?= (isset($sales->total_amount) || isset($sales->paid)) ? $this->sma->formatMoney($sales->total_amount - $sales->paid) : '0.00' ?></h3>
         <p><?= lang('due_amount') ?></p>
      </div>
   </div>
   <div class="col-md-2">
      <div class="small-box padding1010 blightBlue">
         <h3 class="total_count_text"><?= $total_sales ?></h3>
         <p><?= lang('total_sales') ?></p>
      </div>
   </div>
   <div class="col-md-2">
      <div class="small-box padding1010 bblue">
         <h3 class="total_quotes_count_text"><?= $total_quotes ?></h3>
         <p><?= lang('total_quotes') ?></p>
      </div>
   </div>
   <div class="col-md-2">
      <div class="small-box padding1010 bdarkGreen">
         <h3 class="total_returns_count_text"><?= $total_returns ?></h3>
         <p><?= lang('total_returns') ?></p>
      </div>
   </div>
</div>

<ul id="myTabCustomerReports" class="nav nav-tabs no-print">

   <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
      <li class='nav-item'><a class="nav-link tab-grey" data-toggle='tab' href="#sales_summary-con"><?= lang('sales_summary') ?></a></li>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
      <li class='nav-item'><a class="nav-link sales-con tab-grey" data-toggle='tab' href="#sales-con"><?= lang('sales_detail') ?></a></li>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['payments-index']): ?>
      <li class='nav-item'><a class="nav-link payments-con tab-grey" data-toggle='tab' href="#payments-con"><?= lang('payments') ?></a></li>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['quotes-index']): ?>
      <li class='nav-item'><a class="nav-link quotes-con tab-grey" data-toggle='tab' href="#quotes-con"><?= lang('quotes') ?></a></li>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['customers-list_deposits']): ?>
      <li class='nav-item'><a class="nav-link deposits-con tab-grey" data-toggle='tab' href="#deposits-con"><?= lang('deposits') ?></a></li>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
      <li class='nav-item'><a class="nav-link movements-con tab-grey" data-toggle='tab' href="#movements-con"><?= lang('customer_movements') ?></a></li>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
      <li class='nav-item'><a class="nav-link awardpoints-con tab-grey" data-toggle='tab' href="#awardpoints-con"><?= lang('award_points') ?></a></li>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-orders']): ?>
      <li class='nav-item'><a class="nav-link order_sales-con tab-grey" data-toggle='tab' href="#order_sales-con"><?= lang('order_sales') ?></a></li>
   <?php endif ?>
</ul>

<div class="tab-content">
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
      <!-- ventas resumidas -->
      <div id="sales_summary-con" class="tab-pane fade">
         <div class="box sales-table">
            <div class="box-header">
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" class="toggle_up_summ tip" title="<?= lang('hide_form') ?>">
                           <i class="icon fa fa-toggle-up"></i>
                        </a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="toggle_down_summ tip" title="<?= lang('show_form') ?>">
                           <i class="icon fa fa-toggle-down"></i>
                        </a>
                     </li>
                  </ul>
               </div>
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" id="xls_2" class="tip" title="<?= lang('download_xls') ?>">
                           <i class="icon fa fa-file-excel-o"></i>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="box-content">
               <div class="row">
                  <div class="col-lg-12">
                     <p class="introtext"><?= lang('customize_report'); ?></p>
                     <div id="summ_form">
                        <?php echo admin_form_open("reports/customer_report/" . $user_id); ?>
                           <div class="row">
                              <?php
                                 $bl[""] = "";
                                 $bldata = [];
                                 foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    $bldata[$biller->id] = $biller;
                                 }
                              ?>
                              <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                 <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                       <?= lang("biller", "sales_summary_biller"); ?>
                                       <select name="sales_summary_biller" class="form-control" id="sales_summary_biller">
                                          <option value=""><?= lang('all_billers') ?></option>
                                             <?php foreach ($billers as $biller) : ?>
                                                <option value="<?= $biller->id ?>" <?= (isset($_POST['sales_summary_biller']) && $_POST['sales_summary_biller'] == $biller->id ? "selected" : "") ?>><?= $biller->company ?></option>
                                             <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              <?php } else { ?>
                                 <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                       <?= lang("biller", "sales_summary_biller"); ?>
                                       <select name="sales_summary_biller" class="form-control" id="sales_summary_biller">
                                          <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                             $biller = $bldata[$this->session->userdata('biller_id')];
                                          ?>
                                             <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                          <?php endif ?>
                                       </select>
                                    </div>
                                 </div>
                              <?php } ?>
                              <div class="col-md-2 col-xs-12">
                                 <div class="form-group">
                                    <label class="control-label" for="sales_summary_address"><?= lang("customer_branch"); ?></label>
                                    <?php
                                       $adr[""] = lang('all_billers');
                                       foreach ($addresses as $address) {
                                          $adr[$address->id] = $address->sucursal;
                                       }
                                       echo form_dropdown('sales_summary_address', $adr, (isset($_POST['sales_summary_address']) ? $_POST['sales_summary_address'] : ""), 'class="form-control" id="sales_summary_address" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("address") . '"');
                                    ?>
                                 </div>
                              </div>
                              <div class="col-md-2 col-xs-12" >
                                 <?= lang('date_records_filter', 'sales_summary_date_records_filter') ?>
                                 <select name="sales_summary_date_records_filter" id="sales_summary_date_records_filter" class="form-control">
                                    <?= $this->sma->get_filter_options(); ?>
                                 </select>
                              </div>
                              <div class="sales_summary_date_controls">
                                 <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                       <?= lang("start_date", "sales_summarystart_date"); ?>
                                       <?php echo form_input('sales_summary_start_date', (isset($_POST['sales_summary_start_date']) ? $_POST['sales_summary_start_date'] : ""), 'class="form-control datetime" id="sales_summary_start_date" autocomplete="off"'); ?>
                                    </div>
                                 </div>
                                 <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                       <?= lang("end_date", "sales_summaryend_date"); ?>
                                       <?php echo form_input('sales_summary_end_date', (isset($_POST['sales_summary_end_date']) ? $_POST['sales_summary_end_date'] : ""), 'class="form-control datetime" id="sales_summary_end_date" autocomplete="off"'); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="controls"> 
                                 <?php echo form_submit('sales_summary_submit_sale_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_sales_summary_report"'); ?> 
                              </div>
                           </div>
                        <?php echo form_close(); ?>
                     </div>
                     <div class="clearfix"></div>
                     <div class="table-responsive">
                        <h4 class="text_filter_sumarySales"></h4>
                        <table id="SlRSData" class="table table-bordered table-hover table-condensed reports-table reports-table" style="width:100%;">
                           <thead>
                              <tr>
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("reference_no"); ?></th>
                                 <th><?= lang("affects_to"); ?></th>
                                 <th><?= lang('biller') ?></th>
                                 <th><?= lang("customer_branch"); ?></th>
                                 <th>Total</th>
                                 <th><?= lang("paid"); ?></th>
                                 <th><?= lang("balance"); ?></th>
                                 <th><?= lang("payment_status"); ?></th>
                                 <th><?= lang("status"); ?></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                              </tr>
                           </tbody>
                           <tfoot class="dtFilter">
                              <tr class="active">
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("reference_no"); ?></th>
                                 <th><?= lang("affects_to"); ?></th>
                                 <th><?= lang('biller') ?></th>
                                 <th><?= lang("customer_branch"); ?></th>
                                 <th>Total</th>
                                 <th><?= lang("paid"); ?></th>
                                 <th><?= lang("balance"); ?></th>
                                 <th><?= lang("payment_status"); ?></th>
                                 <th><?= lang("status"); ?></th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            $v2 = "&customer=" . $user_id;
            if ($this->input->post('sales_summary_submit_sale_report')) {
               if ($this->input->post('sales_summary_biller')) { $v2 .= "&biller=" . $this->input->post('sales_summary_biller'); }
               if ($this->input->post('sales_summary_address')) { $v2 .= "&address=" . $this->input->post('sales_summary_address'); }
            }
         ?>
         <script type="text/javascript">
            $(document).ready(function () {
               hide_date_controls = true; 
               $('#sales_summary_date_records_filter').on('change', function(){
                  filter = $(this).val();
                  if(filter == 5){
                     $('.sales_summary_date_controls').css('display', '');
                  }else{
                     $('.sales_summary_date_controls').css('display', 'none');
                  }
               });
               if (hide_date_controls) {
                  $('.sales_summary_date_controls').css('display', 'none');
               } else {
                  $('.sales_summary_date_controls').css('display', '');
               }
               $('#summ_form').hide();
               $('.toggle_down_summ').click(function () {
                  $("#summ_form").slideDown();
                  return false;
               });
               $('.toggle_up_summ').click(function () {
                  $("#summ_form").slideUp();
                  return false;
               });

               $('#sales_summary_date_records_filter').val("<?= isset($_POST['sales_summary_date_records_filter']) ? $_POST['sales_summary_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
               let $v2 = get_date_filter($('#sales_summary_date_records_filter').val(), 
                                          $('#sales_summary_start_date').val(),
                                          $('#sales_summary_end_date').val(),
                                          'text_filter_sumarySales', 
                                          $('#sales_summary_biller option:selected').text(),  
                                          $('#sales_summary_address option:selected').text(), );
               $v2 += '<?= $v2 ?>';
               $('#xls_2').click(function (event) {
                  event.preventDefault();
                  window.location.href = "<?=admin_url('reports/getSalesSummaryReport/0/xls/?v=1')?>"+$v2;
                  return false;
               }); //ventas resumidas
               <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
                  oTable = $('#SlRSData').dataTable({
                     "aaSorting": [[0, "desc"]],
                     "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                     "iDisplayLength": <?= $Settings->rows_per_page ?>,
                     'bProcessing': true, 'bServerSide': true,
                     'sAjaxSource': '<?= admin_url('reports/getSalesSummaryReport/?v=1') ?>'+$v2,
                     'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                           "name": "<?= $this->security->get_csrf_token_name() ?>",
                           "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                     },
                     'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                        nRow.id = aData[10];
                        nRow.className = (aData[5] > 0) ? "invoice_link2" : "invoice_link2 warning";
                        return nRow;
                     },
                     "aoColumns": [
                        {"mRender": fld},
                        null,
                        null,
                        null,
                        null,
                        {"mRender": currencyFormat},
                        {"mRender": currencyFormat},
                        {"mRender": currencyFormat},
                        {"mRender": pay_status},
                        {"mRender": row_status}
                     ],
                     "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        var gtotal = 0, paid = 0, balance = 0;
                        for (var i = 0; i < aaData.length; i++) {
                           gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                           paid += parseFloat(aaData[aiDisplay[i]][6]);
                           balance += parseFloat(aaData[aiDisplay[i]][7]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[5].innerHTML = currencyFormat(parseFloat(gtotal));
                        nCells[6].innerHTML = currencyFormat(parseFloat(paid));
                        nCells[7].innerHTML = currencyFormat(parseFloat(balance));
                     }
                  }).fnSetFilteringDelay().dtFilter([
                     {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                     {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                     {column_number: 3, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                     {column_number: 3, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                     {column_number: 6, filter_default_label: "[<?=lang('payment_status');?>]", filter_type: "text", data: []},
                  ], "footer");
               <?php endif ?>
                  
            });   
      
         </script>
      </div>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
      <!-- ventas detalladas -->
      <div id="sales-con" class="tab-pane fade in active">
         <div class="box sales-table">
            <div class="box-header no-print">
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                           <i class="icon fa fa-toggle-up"></i>
                        </a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                           <i class="icon fa fa-toggle-down"></i>
                        </a>
                     </li>
                  </ul>
               </div>
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                           <i class="icon fa fa-file-excel-o"></i>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="box-content">
               <div class="row">
                  <div class="col-lg-12">
                     <p class="introtext"><?= lang('customize_report'); ?></p>
                     <div id="form">
                        <?php echo admin_form_open("reports/customer_report/" . $user_id, 'id="form_filter" class="no-print"'); ?>
                           <div class="row">
                              <?php
                                 $bl[""] = "";
                                 $bldata = [];
                                 foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    $bldata[$biller->id] = $biller;
                                 }
                              ?>
                              <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                 <div class="col-md-2">
                                    <div class="form-group">
                                       <?= lang("biller", "biller"); ?>
                                       <select name="biller" class="form-control" id="biller">
                                          <option value=""><?= lang('all_billers') ?></option>
                                          <?php foreach ($billers as $biller) : ?>
                                             <option value="<?= $biller->id ?>" <?= (isset($_POST['biller']) && $_POST['biller'] == $biller->id ? "selected" : "") ?>><?= $biller->company ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              <?php } else { ?>
                                 <div class="col-md-2">
                                    <div class="form-group">
                                       <?= lang("biller", "biller"); ?>
                                       <select name="biller" class="form-control" id="biller">
                                          <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                             $biller = $bldata[$this->session->userdata('biller_id')];
                                          ?>
                                             <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                          <?php endif ?>
                                       </select>
                                    </div>
                                 </div>
                              <?php } ?>
                                 <div class="col-sm-2">
                                    <div class="form-group">
                                       <label class="control-label" for="address"><?= lang("customer_branch"); ?></label>
                                       <?php
                                          $adr[""] = lang('all_billers');
                                          foreach ($addresses as $address) {
                                             $adr[$address->id] = $address->sucursal;
                                          }
                                          echo form_dropdown('address', $adr, (isset($_POST['address']) ? $_POST['address'] : ""), 'class="form-control" id="address" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("address") . '"');
                                       ?>
                                    </div>
                                 </div>
                                 <div class="col-md-2 col-xs-12" >
                                    <?= lang('date_records_filter', 'sales_date_records_filter') ?>
                                    <select name="sales_date_records_filter" id="sales_date_records_filter" class="form-control">
                                       <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                 </div>
                                 <div class="sales_date_controls">
                                    <div class="col-sm-2">
                                       <div class="form-group">
                                          <?= lang("start_date", "start_date"); ?>
                                          <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="sales_start_date" autocomplete="off"'); ?>
                                       </div>
                                    </div>
                                    <div class="col-sm-2">
                                       <div class="form-group">
                                          <?= lang("end_date", "end_date"); ?>
                                          <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="sales_end_date" autocomplete="off"'); ?>
                                       </div>
                                    </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="controls"> <?php echo form_submit('submit_sale_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_sale_report"'); ?> </div>
                           </div>
                        <?php echo form_close(); ?>
                     </div>
                     <div class="clearfix"></div>
                     <div class="table-responsive">
                        <h4 class="text_filter_detailSales"></h4>
                        <table id="Sales_datail" class="table table-bordered table-hover table-condensed reports-table reports-table" style="width:100%;">
                           <thead>
                              <tr>
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("reference_no"); ?></th>
                                 <th><?= lang("affects_to"); ?></th>
                                 <th><?= lang("biller"); ?></th>
                                 <th><?= lang("customer_branch"); ?></th>
                                 <th><?= lang("product"); ?></th>
                                 <th><?= lang("net_unit_price"); ?></th>
                                 <th><?= lang("price_x_discount"); ?></th>
                                 <th><?= lang("tax"); ?></th>
                                 <th><?= lang("price_x_tax"); ?></th>
                                 <th><?= lang("quantity"); ?></th>
                                 <th><?= lang("ST"); ?></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td colspan="12" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                              </tr>
                           </tbody>
                           <tfoot class="dtFilter">
                              <tr class="active">
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("reference_no"); ?></th>
                                 <th><?= lang("affects_to"); ?></th>
                                 <th><?= lang("biller"); ?></th>
                                 <th><?= lang("customer_branch"); ?></th>
                                 <th><?= lang("product"); ?></th>
                                 <th><?= lang("net_unit_price"); ?></th>
                                 <th><?= lang("price_x_discount"); ?></th>
                                 <th><?= lang("tax"); ?></th>
                                 <th><?= lang("price_x_tax"); ?></th>
                                 <th><?= lang("quantity"); ?></th>
                                 <th><?= lang("ST"); ?></th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            $v = "&customer=" . $user_id;
            if ($this->input->post('submit_sale_report')) {
               if ($this->input->post('biller')) { $v .= "&biller=" . $this->input->post('biller'); }
               if ($this->input->post('address')) { $v .= "&address=" . $this->input->post('address'); }
            } 
         ?>
         <script type="text/javascript">
            $(document).ready(function () {
               $('#form').hide();
               $('.toggle_down').click(function () {
                  $("#form").slideDown();
                  return false;
               });
               $('.toggle_up').click(function () {
                  $("#form").slideUp();
                  return false;
               });

               <?php if (!isset($_POST['submit_sale_report'])) { ?>
                  $('#sales_date_records_filter').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
               <?php } else { ?>
                  $('#sales_date_records_filter').val("<?= isset($_POST['sales_date_records_filter']) ? $_POST['sales_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
               <?php } ?>
               <?php if (!isset($_POST['start_date'])): ?>
                  $('#sales_start_date').val(fecha_inicial);
                  $('#sales_end_date').val(fecha_final);
               <?php endif ?>
               $('#sales_date_records_filter').on('change', function(){
                  filter = $(this).val();
                  if (filter == 5) {
                     $('.sales_date_controls').css('display', '');
                  }else{
                     $('.sales_date_controls').css('display', 'none');
                  }
               })
               
               $('#sales_date_records_filter').val("<?= isset($_POST['sales_date_records_filter']) ? $_POST['sales_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
               let $v = get_date_filter(  $('#sales_date_records_filter').val(),
                                          $('#sales_start_date').val(),
                                          $('#sales_end_date').val(),
                                          'text_filter_detailSales',
                                          $('#biller option:selected').text(),  
                                          $('#address option:selected').text(),
                                       );
               $v += '<?= $v ?>';

               $('#xls').click(function (event) {
                  event.preventDefault();
                  window.location.href = "<?=admin_url('reports/getSalesReportCustomer/0/xls/?v=1')?>"+$v;
                  return false;
               }); //ventas detalladas
               <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
                  oTable = $('#Sales_datail').dataTable({
                     "aaSorting": [[0, "desc"]],
                     "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                     "iDisplayLength": <?= $Settings->rows_per_page ?>,
                     'bProcessing': true, 'bServerSide': true,
                     'sAjaxSource': '<?= admin_url('reports/getSalesReportCustomer/?v=1') ?>'+$v,
                     'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                           "name": "<?= $this->security->get_csrf_token_name() ?>",
                           "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                     },
                     'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                        nRow.id = aData[12];
                        nRow.className = (aData[10] > 0) ? "invoice_link2" : "invoice_link2 warning";
                        return nRow;
                     },
                     "aoColumns": [
                        {"mRender": fld},
                        null,
                        null,
                        null,
                        null,
                        {"bSearchable": false, "mRender": pqFormat},
                        {"mRender": pqFormat},
                        {"mRender": currencyFormat},
                        null,
                        {"mRender": currencyFormat},
                        {"mRender": currencyFormat},
                        {"mRender": currencyFormat}
                     ],
                     "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        
                     }
                  }).fnSetFilteringDelay().dtFilter([
                     {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                     {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                     {column_number: 2, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                     {column_number: 3, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                  ], "footer");
               <?php endif ?>
                  
            });
         </script>
      </div>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['payments-index']): ?>
      <!-- pagos -->
      <div id="payments-con" class="tab-pane fade">
         <div class="box payments-table">
            <div class="box-header">
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" class="paytoggle_up tip" title="<?= lang('hide_form') ?>">
                           <i class="icon fa fa-toggle-up"></i>
                        </a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="paytoggle_down tip" title="<?= lang('show_form') ?>">
                           <i class="icon fa fa-toggle-down"></i>
                        </a>
                     </li>
                  </ul>
               </div>
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" id="xls1" class="tip" title="<?= lang('download_xls') ?>">
                           <i class="icon fa fa-file-excel-o"></i>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="box-content">
               <div class="row">
                  <div class="col-lg-12">
                     <p class="introtext"><?= lang('customize_report'); ?></p>
                     <div id="payform">
                        <?php echo admin_form_open("reports/customer_report/" . $user_id."/#payments-con"); ?>
                           <div class="row">
                              <?php
                                 $bl[""] = "";
                                 $bldata = [];
                                 foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    $bldata[$biller->id] = $biller;
                                 }
                              ?>
                              <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                 <div class="col-md-2">
                                    <div class="form-group">
                                       <?= lang("biller", "payments_biller"); ?>
                                       <select name="payments_biller" class="form-control" id="payments_biller">
                                          <option value=""><?= lang('all_billers') ?></option>
                                          <?php foreach ($billers as $biller) : ?>
                                             <option value="<?= $biller->id ?>" <?= (isset($_POST['payments_biller']) && $_POST['payments_biller'] == $biller->id ? "selected" : "") ?>><?= $biller->company ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              <?php } else { ?>
                                 <div class="col-md-2">
                                    <div class="form-group">
                                    <?= lang("biller", "payments_biller"); ?>
                                       <select name="payments_biller" class="form-control" id="payments_biller">
                                          <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                             $biller = $bldata[$this->session->userdata('biller_id')];
                                          ?>
                                             <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                          <?php endif ?>
                                       </select>
                                    </div>
                                 </div>
                              <?php } ?>

                              <div class="col-sm-2">
                                 <div class="form-group">
                                    <label class="control-label" for="biller"><?= lang("customer_branch"); ?></label>
                                    <?php
                                       $adr[""] = lang('select').' '.lang('address');
                                       foreach ($addresses as $address) {
                                          $adr[$address->id] = $address->sucursal;
                                       }
                                       echo form_dropdown('pay_address', $adr, (isset($_POST['pay_address']) ? $_POST['pay_address'] : ""), 'class="form-control" id="pay_address" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("address") . '"');
                                    ?>
                                 </div>
                              </div>

                              <div class="col-sm-2">
                                 <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                    <?php
                                       $us[""] = lang('select').' '.lang('user');
                                       foreach ($users as $user) {
                                          $us[$user->id] = $user->first_name . " " . $user->last_name;
                                       }
                                       echo form_dropdown('pay_user', $us, (isset($_POST['pay_user']) ? $_POST['pay_user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                    ?>
                                 </div>
                              </div>
                              <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                 <?= lang('date_records_filter', 'pay_date_records_filter') ?>
                                 <select name="pay_date_records_filter" id="pay_date_records_filter" class="form-control">
                                    <?= $this->sma->get_filter_options(); ?>
                                 </select>
                              </div>
                              <div class="pay_date_controls">
                                 <div class="col-sm-2">
                                    <div class="form-group">
                                       <?= lang("start_date", "pay_start_date"); ?>
                                       <?php echo form_input('pay_start_date', (isset($_POST['pay_start_date']) ? $_POST['pay_start_date'] : ""), 'class="form-control datetime" id="pay_start_date" autocomplete="off"'); ?>
                                    </div>
                                 </div>
                                 <div class="col-sm-2">
                                    <div class="form-group">
                                       <?= lang("end_date", "pay_end_date"); ?>
                                       <?php echo form_input('pay_end_date', (isset($_POST['pay_end_date']) ? $_POST['pay_end_date'] : ""), 'class="form-control datetime" id="pay_end_date" autocomplete="off"'); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="controls"> <?php echo form_submit('submit_payment_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                           </div>
                        <?php echo form_close(); ?>
                     </div>
                     <div class="clearfix"></div>
                     <div class="table-responsive">
                        <h4 class="text_filter_payments"></h4>
                        <table id="PayRData" class="table table-bordered table-hover table-condensed reports-table reports-table" style="width:100%;">
                           <thead>
                              <tr>
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("payment_date"); ?></th>
                                 <th><?= lang("payment_ref"); ?></th>
                                 <th><?= lang("sale_ref"); ?></th>
                                 <th><?= lang("purchase_ref"); ?></th>
                                 <th><?= lang("paid_by"); ?></th>
                                 <th><?= lang("amount"); ?></th>
                                 <th><?= lang("type"); ?></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                              </tr>
                           </tbody>
                           <tfoot class="dtFilter">
                              <tr class="active">
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th><?= lang("amount"); ?></th>
                                 <th></th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
           </div>
         </div>
         <?php
            $p = "&customer=" . $user_id;
            if ($this->input->post('submit_payment_report')) {?>
               <script type="text/javascript">
                  setTimeout(function() {
                     $('.payments-con').trigger('click');
                  }, 1400);
               </script>
            <?php
               if ($this->input->post('pay_user')) { $p .= "&user=" . $this->input->post('pay_user'); }
               if ($this->input->post('pay_start_date')) { $p .= "&start_date=" . $this->input->post('pay_start_date'); }
               if ($this->input->post('pay_end_date')) { $p .= "&end_date=" . $this->input->post('pay_end_date'); }
               if ($this->input->post('pay_address')) { $p .= "&address=" . $this->input->post('pay_address'); }
               if ($this->input->post('payments_biller')) { $p .= "&biller_id=" . $this->input->post('payments_biller'); }
            }
         ?>
         <script>
            $(document).ready(function () {
               $('#payform').hide();
               $('.paytoggle_down').click(function () {
                   $("#payform").slideDown();
                   return false;
               });
               $('.paytoggle_up').click(function () {
                   $("#payform").slideUp();
                   return false;
               });
               var pb = <?= json_encode($pb); ?>;
               function paid_by(x) {
                  return (x != null) ? (pb[x] ? pb[x] : x) : x;
               }
               <?php if (!isset($_POST['pay_start_date'])) { ?>
                  $('#pay_date_records_filter').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
               <?php } else { ?>
                  $('#pay_date_records_filter').val("<?= isset($_POST['pay_date_records_filter']) ? $_POST['pay_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
               <?php } ?>
               <?php if (!isset($_POST['pay_start_date'])): ?>
                  $('#pay_start_date').val(fecha_inicial);
                  $('#pay_end_date').val(fecha_final);
               <?php endif ?>
               $('#pay_date_records_filter').on('change', function(){
                  let option = $(this).val()
                  if (option == 5) {
                     $('.pay_date_controls').css('display', '');
                  }else{
                     $('.pay_date_controls').css('display', 'none');
                  }
               })
               if ($('#pay_date_records_filter').val() == 5) {
                  $('.pay_date_controls').css('display', '');
               }

               $('#pay_date_records_filter').val("<?= isset($_POST['pay_date_records_filter']) ? $_POST['pay_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
               let $p = get_date_filter(  $('#pay_date_records_filter').val(),
                                          $('#pay_start_date').val(),
                                          $('#pay_end_date').val(),
                                          'text_filter_payments',
                                          $('#payments_biller option:selected').text(),  
                                          $('#pay_address option:selected').text(),
                                          $('#pay_user option:selected').text(),
                                       );
               $p += '<?= $p ?>';
               <?php if ($this->Admin || $this->Owner || $this->GP['payments-index']): ?>
                   oTable = $('#PayRData').dataTable({
                     "aaSorting": [[0, "desc"]],
                     "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                     "iDisplayLength": <?= $Settings->rows_per_page ?>,
                     'bProcessing': true, 'bServerSide': true,
                     'sAjaxSource': '<?= admin_url('reports/getPaymentsReport/?v=1&') ?>'+$p,
                     buttons : [{extend:'excel', title:'<?= lang('payments') ?>', className:'exportar_payments', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
                     dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
                     'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                           "name": "<?= $this->security->get_csrf_token_name() ?>",
                           "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                     },
                     "aoColumns": [
                        {"mRender": fld},
                        {"mRender": fld},
                        null,
                        null,
                        {"bVisible": false},
                        {"mRender": paid_by},
                        {"mRender": currencyFormat},
                        {"mRender": row_status}
                     ],
                     'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                        nRow.id = aData[8];
                        nRow.className = "payment_link";
                        if (aData[7] == 'returned') {
                           nRow.className = "payment_link danger";
                        }
                        return nRow;
                     },
                     "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        var total = 0;
                        for (var i = 0; i < aaData.length; i++) {
                           total += parseFloat(aaData[aiDisplay[i]][6]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[5].innerHTML = currencyFormat(parseFloat(total));
                     }
                  }).fnSetFilteringDelay().dtFilter([
                     {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                     {column_number: 1, filter_default_label: "[<?=lang('payment_ref');?>]", filter_type: "text", data: []},
                     {column_number: 2, filter_default_label: "[<?=lang('sale_ref');?>]", filter_type: "text", data: []},
                     {column_number: 4, filter_default_label: "[<?=lang('paid_by');?>]", filter_type: "text", data: []},
                     {column_number: 6, filter_default_label: "[<?=lang('type');?>]", filter_type: "text", data: []},
                  ], "footer");
               <?php endif ?>
                 
            });
         </script>
      </div>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['quotes-index']): ?>
      <!-- cotizaciones -->
      <div id="quotes-con" class="tab-pane fade">
         <div class="box">
            <div class="box-header">
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" id="xls2" class="tip" title="<?= lang('download_xls') ?>">
                           <i class="icon fa fa-file-excel-o"></i>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="box-content">
               <div class="row">
                  <div class="col-lg-12">
                     <p class="introtext"><?php echo lang('list_results'); ?></p>
                     <div class="table-responsive">
                        <table id="QuRData" class="table table-bordered table-hover table-condensed reports-table" style="width:100%;">
                           <thead>
                              <tr>
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("reference_no"); ?></th>
                                 <th><?= lang("biller"); ?></th>
                                 <th><?= lang("customer_name"); ?></th>
                                 <th><?= lang("product_qty"); ?></th>
                                 <th><?= lang("grand_total"); ?></th>
                                 <th><?= lang("status"); ?></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                              </tr>
                           </tbody>
                           <tfoot class="dtFilter">
                              <tr class="active">
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th><?= lang("product_qty"); ?></th>
                                 <th></th>
                                 <th></th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <script type="text/javascript">
            $(document).ready(function () {
               <?php if ($this->Admin || $this->Owner || $this->GP['quotes-index']): ?>
                  oTable = $('#QuRData').dataTable({
                     "aaSorting": [[0, "desc"]],
                     "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                     "iDisplayLength": <?= $Settings->rows_per_page ?>,
                     'bProcessing': true, 'bServerSide': true,
                     'sAjaxSource': '<?= admin_url('reports/getQuotesReport/?v=1&customer='.$user_id) ?>',
                        buttons : [{extend:'excel', title:'<?= lang('quotes') ?>', className:'exportar_quotes', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
                        dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
                     'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                           "name": "<?= $this->security->get_csrf_token_name() ?>",
                           "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                           'dataType': 'json',
                           'type': 'POST',
                           'url': sSource,
                           'data': aoData,
                           'success': fnCallback
                        });
                     },
                     "aoColumns": [{"mRender": fld}, null, null, null, {
                        "bSearchable": false,
                        "mRender": pqFormat
                     }, {"mRender": currencyFormat}, {"mRender": row_status}],
                  }).fnSetFilteringDelay().dtFilter([
                     {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                     {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                     {column_number: 2, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                     {column_number: 3, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                     {column_number: 5, filter_default_label: "[<?=lang('grand_total');?>]", filter_type: "text", data: []},
                     {column_number: 6, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
                  ], "footer");
               <?php endif ?>
                  
            });
         </script>
      </div>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['customers-list_deposits']): ?>
      <!-- deposits -->
      <div id="deposits-con" class="tab-pane fade">
         <div class="box">
            <div class="box-header">
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" id="xls3" class="tip" title="<?= lang('download_xls') ?>">
                           <i class="icon fa fa-file-excel-o"></i>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="box-content">
               <div class="row">
                  <div class="col-lg-12">
                     <p class="introtext"><?php echo lang('list_results'); ?></p>
                     <div class="table-responsive">
                        <table id="DepData" class="table table-bordered table-condensed table-hover reports-table" style="width:100%;">
                           <thead>
                              <tr>
                                 <th><?= lang("reference"); ?></th>
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("value"); ?></th>
                                 <th><?= lang("applied_valued"); ?></th>
                                 <th><?= lang("balance"); ?></th>
                                 <th><?= lang("payment_method"); ?></th>
                                 <th><?= lang("origin"); ?></th>
                                 <th><?= lang("origin_reference_no"); ?></th>
                                 <th><?= lang("created_by"); ?></th>
                                 <th><?= lang("customers"); ?></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                    <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                              </tr>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th><?= lang("reference"); ?></th>
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("value"); ?></th>
                                 <th><?= lang("applied_valued"); ?></th>
                                 <th><?= lang("balance"); ?></th>
                                 <th><?= lang("payment_method"); ?></th>
                                 <th><?= lang("origin"); ?></th>
                                 <th><?= lang("origin_reference_no"); ?></th>
                                 <th><?= lang("created_by"); ?></th>
                                 <th><?= lang("customers"); ?></th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <script type="text/javascript">
            $(document).ready(function () {

               <?php if ($this->Admin || $this->Owner || $this->GP['customers-list_deposits']): ?>
                  oTable = $('#DepData').dataTable({
                           "aaSorting": [[0, "desc"]],
                           "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                           "iDisplayLength": <?= $Settings->rows_per_page ?>,
                           buttons : [{extend:'excel', title:'<?= lang('deposits') ?>', className:'exportar_deposits', exportOptions: {columns : [0,1,2,3,4]}}],
                           dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
                           'bProcessing': true, 'bServerSide': true,
                           'sAjaxSource': '<?= admin_url('reports/get_depositsCustomer/'.$user_id) ?>',
                           'fnServerData': function (sSource, aoData, fnCallback) {
                              aoData.push({
                                 "name": "<?= $this->security->get_csrf_token_name() ?>",
                                 "value": "<?= $this->security->get_csrf_hash() ?>"
                              });
                              $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                           },"aoColumns": [
                                      null,
                                      {"mRender": fld},
                                      {"mRender": currencyFormat},
                                      {"mRender": currencyFormat},
                                      {"mRender": currencyFormat},
                                      null,
                                      null,
                                      null,
                                      null,
                                      null,
                                  ]
                  }).dtFilter([], "footer");
               <?php endif ?>
            });
         </script>
      </div>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
      <!-- movimientos -->
      <div id="movements-con" class="tab-pane fade">
         <div class="box">
            <div class="box-header">
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" id="xls4" class="tip" title="<?= lang('download_xls') ?>">
                           <i class="icon fa fa-file-excel-o"></i>
                        </a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="movementtoggle_up tip" title="<?= lang('hide_form') ?>">
                           <i class="icon fa fa-toggle-up"></i>
                        </a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="movementtoggle_down tip" title="<?= lang('show_form') ?>">
                           <i class="icon fa fa-toggle-down"></i>
                        </a>
                     </li>
                  </ul>
               </div>  
            </div>
            <div class="box-content">
               <div class="row">
                  <div class="col-lg-12">
                     <p class="introtext"><?php echo lang('list_results'); ?></p>
                     <div id="movementform">
                        <?php echo admin_form_open("reports/customer_report/" . $user_id."/#movements-con"); ?>
                           <div class="row">
                              <div class="col-sm-2">
                                 <div class="form-group">
                                    <label class="control-label" for="biller"><?= lang("customer_branch"); ?></label>
                                    <?php
                                       $adr[""] = lang('all_billers');
                                       foreach ($addresses as $address) {
                                          $adr[$address->id] = $address->sucursal;
                                       }
                                       echo form_dropdown('movements_address', $adr, (isset($_POST['movements_address']) ? $_POST['movements_address'] : ""), 'class="form-control" id="movements_address" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("address") . '"');
                                    ?>
                                 </div>
                              </div>
                              <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                 <?= lang('date_records_filter', 'movements_date_records_filter') ?>
                                 <select name="movements_date_records_filter" id="movements_date_records_filter" class="form-control">
                                    <?= $this->sma->get_filter_options(); ?>
                                 </select>
                              </div>
                              <div class="movements_date_controls">
                                 <div class="col-sm-2">
                                    <div class="form-group">
                                       <?= lang("start_date", "movements_start_date"); ?>
                                       <?php echo form_input('movements_start_date', (isset($_POST['movements_start_date']) ? $_POST['movements_start_date'] : ""), 'class="form-control datetime" id="movements_start_date" autocomplete="off"'); ?>
                                    </div>
                                 </div>
                                 <div class="col-sm-2">
                                    <div class="form-group">
                                       <?= lang("end_date", "movements_end_date"); ?>
                                       <?php echo form_input('movements_end_date', (isset($_POST['movements_end_date']) ? $_POST['movements_end_date'] : ""), 'class="form-control datetime" id="movements_end_date" autocomplete="off"'); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="controls"> <?php echo form_submit('submit_movements_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                           </div>
                        <?php echo form_close(); ?>
                     </div>
                     <div class="table-responsive">
                        <h4 class="text_filter_movements"></h4>
                        <table id="movementsData" class="table table-bordered table-hover reports-table">
                           <thead>
                              <tr class="primary">
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("reference_no"); ?></th>
                                 <th><?= lang("customer_branch"); ?></th>
                                 <th><?= lang("substraction"); ?></th>
                                 <th><?= lang("addition"); ?></th>
                                 <th><?= lang("balance"); ?></th>
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                           <tfoot>
                              <tr class="primary">
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("reference_no"); ?></th>
                                 <th><?= lang("customer_branch"); ?></th>
                                 <th><?= lang("substraction"); ?></th>
                                 <th><?= lang("addition"); ?></th>
                                 <th><?= lang("balance"); ?></th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
            <script type="text/javascript">
               $(document).ready(function () {
                  <?php if (!isset($_POST['movements_start_date'])): ?>
                     $('#movements_start_date').val(fecha_inicial);
                     $('#movements_end_date').val(fecha_final);
                  <?php endif ?>

                  <?php if (!isset($_POST['movements_start_date'])) { ?>
                     $('#movements_date_records_filter').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
                  <?php } else { ?>
                     $('#movements_date_records_filter').val("<?= isset($_POST['movements_date_records_filter']) ? $_POST['movements_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
                  <?php } ?>

                  $('#movementform').hide();
                  $('.movementtoggle_down').click(function () {
                     $("#movementform").slideDown();
                     return false;
                  });
                  $('.movementtoggle_up').click(function () {
                     $("#movementform").slideUp();
                     return false;
                  });

                  $('#movements_date_records_filter').on('change', function(){
                     let filter_m = $(this).val();
                     if (filter_m == 5) {
                        $('.movements_date_controls').css('display', '');
                     }else{
                        $('.movements_date_controls').css('display', 'none');
                     }
                  });
                  $('#movements_date_records_filter').trigger('change');

                  let $p = get_date_filter(  $('#movements_date_records_filter').val(),
                                          $('#movements_start_date').val(),
                                          $('#movements_end_date').val(),
                                          'text_filter_movements',
                                          null,  
                                          $('#movements_address option:selected').text(),
                                          null,
                                       );
                  // Expresión regular para encontrar el valor de start_date
                  var regex = /start_date=([^&]+)/;
                  // Ejecutar la expresión regular en la cadena
                  var match = $p.match(regex);
                  // Verificar si se encontró el valor de start_date
                  if (match && match.length > 1) {
                     var valor_start_date = match[1];
                  }else{
                     var valor_start_date = "";
                  }     
                    
                  // Expresión regular para encontrar el valor de start_date
                  var regex = /end_date=([^&]+)/;
                  // Ejecutar la expresión regular en la cadena
                  var match = $p.match(regex);
                  // Verificar si se encontró el valor de start_date
                  if (match && match.length > 1) {
                     var valor_end_date = match[1];
                  }

               <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
                  $.ajax({
                        url: '<?= admin_url('reports/get_movements/'.$user_id) ?>',
                        type : 'POST',
                        dataType : 'JSON',
                        data : {
                                    "<?= $this->security->get_csrf_token_name() ?>" :  "<?= $this->security->get_csrf_hash() ?>",
                                    "start_date" : valor_start_date,
                                    "end_date" : valor_end_date,
                                    "address" : '<?= $this->input->post("movements_address") ?>',
                                 }
                     }).done(function(data){

                     response = data;
                     // $('.kardex_balance').text('<?= lang('kardex_product_balance') ?>'+response.balance);
                     $('#movementsData tbody').html(response.html);
                     kardexTable = $('#movementsData').dataTable({
                           "aaSorting": [],
                           "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                           "iDisplayLength": <?= $this->Settings->rows_per_page ?>,
                           buttons : [{extend:'excel', title:'<?= lang('movements') ?>', className:'exportar_movements', exportOptions: {columns : [0,1,2,3,4,5]}}],
                           dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
                           'fnRowCallback': function (nRow, aData, iDisplayIndex) { },
                           "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) { }
                        }).fnSetFilteringDelay().dtFilter([

                        ], "footer");
                        $('#loading').fadeOut();
                     });
               <?php endif ?>
                     
               });
            </script>
         </div>
      </div>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
      <!-- award points -->
      <div id="awardpoints-con" class="tab-pane fade">
         <div class="box">
            <div class="box-header">
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" id="xls5" class="tip" title="<?= lang('download_xls') ?>">
                           <i class="icon fa fa-file-excel-o"></i>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="box-content">
               <div class="row">
                  <div class="col-lg-12">
                     <p class="introtext"><?php echo lang('list_results'); ?></p>
                     <div class="table-responsive">
                        <table id="awPData" class="table table-bordered table-hover table-condensed reports-table" style="width:100%;">
                           <thead>
                              <tr>
                                 <th><?= lang("customer"); ?></th>
                                 <th><?= lang("date"); ?></th>
                                 <th><?= lang("type"); ?></th>
                                 <th><?= lang("points"); ?></th>
                                 <th><?= lang("reference_no"); ?></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                              </tr>
                           </tbody>
                           <tfoot class="dtFilter">
                              <tr class="active">
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <script type="text/javascript">
            $(document).ready(function () {
               <?php if ($this->Admin || $this->Owner || $this->GP['sales-index']): ?>
                  oTable = $('#awPData').dataTable({
                     "aaSorting": [[1, "desc"]],
                     "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                     "iDisplayLength": <?= $Settings->rows_per_page ?>,
                     'bProcessing': true, 'bServerSide': true,
                     'sAjaxSource': '<?= admin_url('reports/get_customer_award_points_kardex/?v=1&customer='.$user_id) ?>',
                        buttons : [{extend:'excel', title:'<?= lang('award_points') ?>', className:'exportar_award_points', exportOptions: {columns : [0,1,2,3,4]}}],
                        dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
                     'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                           "name": "<?= $this->security->get_csrf_token_name() ?>",
                           "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                           'dataType': 'json',
                           'type': 'POST',
                           'url': sSource,
                           'data': aoData,
                           'success': fnCallback
                        });
                     },
                     "aoColumns": [
                        null,
                        {"mRender": fld},
                        {"mRender": apmv_type},
                        null,
                        null
                     ],
                  }).fnSetFilteringDelay().dtFilter([
                     {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                  ], "footer");
               <?php endif ?>

                  
            });
         </script>
      </div>
   <?php endif ?>
   <?php if ($this->Admin || $this->Owner || $this->GP['sales-orders']): ?>
      <!-- order sales  -->
      <div id="order_sales-con" class="tab-pane fade">
         <div class="box order_sales-table">
            <div class="box-header">
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" class="toggle_up_summ tip" title="<?= lang('hide_form') ?>">
                           <i class="icon fa fa-toggle-up"></i>
                        </a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="toggle_down_summ tip" title="<?= lang('show_form') ?>">
                           <i class="icon fa fa-toggle-down"></i>
                        </a>
                     </li>
                  </ul>
               </div>
               <div class="box-icon">
                  <ul class="btn-tasks">
                     <li class="dropdown">
                        <a href="#" id="xls7" class="tip" title="<?= lang('download_xls') ?>">
                           <i class="icon fa fa-file-excel-o"></i>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="box-content">
               <div class="row">
                  <div class="col-lg-12">
                     <p class="introtext"><?= lang('customize_report'); ?></p>
                     <div id="summ_form">
                        <?php echo admin_form_open("reports/customer_report/" . $user_id); ?>
                           <div class="row">
                              <?php
                                 $bl[""] = "";
                                 $bldata = [];
                                 foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    $bldata[$biller->id] = $biller;
                                 }
                              ?>
                              <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                 <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                       <?= lang("biller", "order_sales_summary_biller"); ?>
                                       <select name="order_sales_summary_biller" class="form-control" id="order_sales_summary_biller">
                                          <option value=""><?= lang('all_billers') ?></option>
                                             <?php foreach ($billers as $biller) : ?>
                                                <option value="<?= $biller->id ?>" <?= (isset($_POST['order_sales_summary_biller']) && $_POST['order_sales_summary_biller'] == $biller->id ? "selected" : "") ?>><?= $biller->company ?></option>
                                             <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              <?php } else { ?>
                                 <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                       <?= lang("biller", "order_sales_summary_biller"); ?>
                                       <select name="order_sales_summary_biller" class="form-control" id="order_sales_summary_biller">
                                          <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                             $biller = $bldata[$this->session->userdata('biller_id')];
                                          ?>
                                             <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                          <?php endif ?>
                                       </select>
                                    </div>
                                 </div>
                              <?php } ?>
                              <div class="col-md-2 col-xs-12" >
                                 <?= lang('date_records_filter', 'order_sales_summary_date_records_filter') ?>
                                 <select name="order_sales_summary_date_records_filter" id="order_sales_summary_date_records_filter" class="form-control">
                                    <?= $this->sma->get_filter_options(); ?>
                                 </select>
                              </div>
                              <div class="order_sales_summary_date_controls">
                                 <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                       <?= lang("start_date", "order_sales_summarystart_date"); ?>
                                       <?php echo form_input('order_sales_summary_start_date', (isset($_POST['order_sales_summary_start_date']) ? $_POST['order_sales_summary_start_date'] : ""), 'class="form-control datetime" id="order_sales_summary_start_date" autocomplete="off"'); ?>
                                    </div>
                                 </div>
                                 <div class="col-md-2 col-xs-12">
                                    <div class="form-group">
                                       <?= lang("end_date", "order_sales_summaryend_date"); ?>
                                       <?php echo form_input('order_sales_summary_end_date', (isset($_POST['order_sales_summary_end_date']) ? $_POST['order_sales_summary_end_date'] : ""), 'class="form-control datetime" id="order_sales_summary_end_date" autocomplete="off"'); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="controls"> 
                                 <?php echo form_submit('submit_order_sales_summary_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_order_sales_summary_report"'); ?> 
                              </div>
                           </div>
                        <?php echo form_close(); ?>
                     </div>
                     <div class="clearfix"></div>
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="table-responsive">
                              <h4 class="text_filter_sumarySales"></h4>
                              <table id="OsData" class="table table-hover" style="width:100%;" >
                                 <thead>
                                    <tr>
                                       <th>id</th>
                                       <th><?= lang("date"); ?></th>
                                       <th><?= lang("reference_no"); ?></th>
                                       <th><?= lang("destination_reference_no"); ?></th>
                                       <th><?= lang('biller') ?></th>
                                       <th><?= lang("customer"); ?></th>
                                       <th><?= lang("seller"); ?></th>
                                       <th class="text-center"><?= lang("status"); ?></th>
                                       <th class="text-center"><?= lang("grand_total"); ?></th>
                                       <th class="text-center"><?= lang("origin"); ?></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>
                                 </tbody>
                                 <tfoot class="dtFilter">
                                    <tr class="active">
                                       <th>id</th>
                                       <th><?= lang("date"); ?></th>
                                       <th><?= lang("reference_no"); ?></th>
                                       <th><?= lang("destination_reference_no"); ?></th>
                                       <th><?= lang('biller') ?></th>
                                       <th><?= lang("customer"); ?></th>
                                       <th><?= lang("seller"); ?></th>
                                       <th class="text-center"><?= lang("status"); ?></th>
                                       <th class="text-center"><?= lang("grand_total"); ?></th>
                                       <th class="text-center"><?= lang("origin"); ?></th>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </div>
                     </div>   
                  </div>
               </div>
            </div>
         </div>
         <?php
            $v2 = "&customer=" . $user_id;
            if ($this->input->post('submit_order_sales_summary_report')) {
               if ($this->input->post('order_sales_summary_biller')) { $v2 .= "&biller=" . $this->input->post('order_sales_summary_biller'); }
            }
         ?>
         <script type="text/javascript">
            $(document).ready(function () {
               hide_date_controls = true; 
               $('#order_sales_summary_date_records_filter').on('change', function(){
                  filter = $(this).val();
                  if(filter == 5){
                     $('.order_sales_summary_date_controls').css('display', '');
                  }else{
                     $('.order_sales_summary_date_controls').css('display', 'none');
                  }
               });
               if (hide_date_controls) {
                  $('.order_sales_summary_date_controls').css('display', 'none');
               } else {
                  $('.order_sales_summary_date_controls').css('display', '');
               }
               $('#summ_form').hide();
               $('.toggle_down_summ').click(function () {
                  $("#summ_form").slideDown();
                  return false;
               });
               $('.toggle_up_summ').click(function () {
                  $("#summ_form").slideUp();
                  return false;
               });

               $('#order_sales_summary_date_records_filter').val("<?= isset($_POST['order_sales_summary_date_records_filter']) ? $_POST['order_sales_summary_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
               let $v2 = get_date_filter($('#order_sales_summary_date_records_filter').val(), 
                                          $('#order_sales_summary_start_date').val(),
                                          $('#order_sales_summary_end_date').val(),
                                          'text_filter_sumaryOrderSales', 
                                          $('#order_sales_summary_biller option:selected').text(),  
                                          $('#order_sales_summary_address option:selected').text(), );
               $v2 += '<?= $v2 ?>';
               $('#xls7').click(function (event) {
                  event.preventDefault();
                  window.location.href = "<?=admin_url('reports/getOrderSalesSummaryReport/0/xls/?v=1')?>"+$v2;
                  return false;
               }); //ventas resumidas

               <?php if ($this->Admin || $this->Owner || $this->GP['sales-orders']): ?>
                  oTable = $('#OsData').dataTable({
                     "aaSorting": [[0, "desc"]],
                     "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                     "iDisplayLength": <?= $Settings->rows_per_page ?>,
                     'bProcessing': true, 'bServerSide': true,
                     'sAjaxSource': '<?= admin_url('reports/getOrderSalesSummaryReport/?v=1') ?>'+$v2,
                     'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                           "name": "<?= $this->security->get_csrf_token_name() ?>",
                           "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                     },
                     'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                        nRow.id = aData[0];
                        nRow.setAttribute('data-id', aData[0]);
                        nRow.className = "order_link re" + aData[0];
                        return nRow;
                     },
                     "aoColumns": [
                        {"bVisible": false},
                        {"mRender": fld},
                        null,
                        null,
                        null,
                        null,
                        null,
                        {"mRender": row_status},
                        {"mRender" : currencyFormat},
                        {"mRender" : order_origin},
                     ],
                     "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) { }
                  }).fnSetFilteringDelay().dtFilter([ ], "footer");
               <?php endif ?>
                  
            });   
         </script>
      </div>
   <?php endif ?>
</div>

<script type="text/javascript">
   $(document).ready(function () {
      <?php if(isset($_POST['sales_summary_submit_sale_report'])): ?>
         active_tabs('sales_summary-con');
      <?php elseif(isset($_POST['submit_sale_report'])): ?>
         active_tabs('sales-con');
      <?php elseif(isset($_POST['submit_payment_report'])): ?>
         active_tabs('payments-con');
      <?php elseif(isset($_POST['submit_movements_report'])): ?>
         active_tabs('movements-con');
      <?php elseif(isset($_POST['submit_order_sales_summary_report'])): ?>
         active_tabs('order_sales-con');   
      <?php else:?>
         <?php if ($this->Admin || $this->Owner): ?>
            active_tabs('sales_summary-con');
         <?php else: ?>
            <?php if ($this->GP['sales-index']): ?>
                 active_tabs('sales_summary-con');
            <?php elseif ($this->GP['payments-index']): ?>
                 active_tabs('payments-con');
            <?php elseif ($this->GP['quotes-index']): ?>
                 active_tabs('quotes-con');
            <?php elseif ($this->GP['customers-list_deposits']): ?>
                 active_tabs('deposits-con');
            <?php elseif ($this->GP['sales-orders']): ?>
                 active_tabs('order_sales-con');
            <?php endif ?>
         <?php endif ?>
      <?php endif; ?>

      $('#xls1').click(function (event) {
         event.preventDefault();
         $('.exportar_payments').click();
         return false;
      }); //Pagos
      $('#xls2').click(function (event) {
         event.preventDefault();
         $('.exportar_quotes').click();
         return false;
      }); //Cotizaciones
      $('#xls3').click(function (event) {
         event.preventDefault();
         $('.exportar_deposits').click();
         return false;
      }); //Depositos
      $('#xls4').click(function (event) {
         event.preventDefault();
         $('.exportar_movements').click();
         return false;
      }); //Movimientos
      $('#xls5').click(function (event) {
         event.preventDefault();
         $('.exportar_award_points').click();
         return false;
      }); //award points 
   });
    
   function get_date_filter(filter, start_date=null, end_date=null, tab, biller=null, address=null, user=null){
      let v = '';
      // console.log('pestaña', tab);
      // console.log('fecha inicial' + start_date);
      // console.log('fecha final' + end_date);
      // console.log('biller' + biller);
      // console.log('address' + address);
      // console.log('user' + user);
      // console.log('filter' + filter);
      fecha_inicial = "";
      fecha_final = "<?= date('d/m/Y 23:59') ?>";
      hide_date_controls = true;
      if (filter == 5) { //RANGO DE FECHAS
         fecha_final = "";
      if (filter == 5) { //RANGO DE FECHAS
         fecha_inicial = start_date;
         fecha_final = end_date;
         hide_date_controls = false;
      }
      } else if (filter == 1) { // HOY
         fecha_inicial = "<?= date('d/m/Y 00:00') ?>";
      } else if (filter == 2) { // MES
         fecha_inicial = "<?= date('01/m/Y 00:00') ?>";
      } else if (filter == 3) { // TRIMESTRE
         fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
      } else if (filter == 8) { // SEMESTRE
         fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
      } else if (filter == 4) { // AÑO
         fecha_inicial = "<?= date('01/01/Y 00:00') ?>";
      } else if (filter == 6) { // MES PASADO
         fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 1 month")) ?>";
         fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
      } else if (filter == 9) { // TRIMESTRE PASADO
         fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
         fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))))) ?>";
      } else if (filter == 10) { // SEMESTRE PASADO
         fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
         fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))))) ?>";
      } else if (filter == 7) { // AÑO PASADO
         fecha_inicial = "<?= date("01/01/Y 00:00",strtotime(date("Y-m-01")."- 1 year")) ?>";
         fecha_final = "<?= date("31/12/Y 23:59",strtotime(date("Y-m-01")."- 1 year")) ?>";
      } else if (filter == 11) { // AÑO PASADO
         fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 2 month")) ?>";
      } else if (filter == 0) {
         fecha_inicial = "";
         fecha_final = "";
         hide_date_controls = true;
      }
      <?php if (!isset($_POST['sales_summary_start_date'])): ?>
         $('#sales_summary_start_date').val(fecha_inicial);
         $('#sales_summary_end_date').val(fecha_final);
      <?php endif ?>
      v += '&start_date='+fecha_inicial;
      v += '&end_date='+fecha_final;
      setFilterText( tab, biller, address, fecha_inicial, fecha_final, user ) ;
      return v;
   }

   function setFilterText(label, biller_text=null, address=null, start_date_text=null, end_date_text=null, user=null){
      let text = "Filtros configurados : ";
      coma = false;
   
      if (biller_text) {
         text+= coma ? "," : "";
         text+=" Sucursal ("+biller_text+")";
         coma = true;
      }
 
      if (address) {
         text+= coma ? "," : "";
         text+=" Sucursal cliente ("+address+")";
         coma = true;
      }

      if (start_date_text) {
         text+= coma ? "," : "";
         text+=" Fecha de inicio ("+start_date_text+")";
         coma = true;
      }

      if (end_date_text) {
         text+= coma ? "," : "";
         text+=" Fecha final ("+end_date_text+")";
         coma = true;
      }

      if (user) {
         text+= coma ? "," : "";
         text+=" Fecha final ("+user+")";
         coma = true;
      }
      $('.'+label).html(text);
   }

   function active_tabs(tab){
      $('.nav-tabs a[href="#'+tab+'"]').tab('show');
   }
</script>