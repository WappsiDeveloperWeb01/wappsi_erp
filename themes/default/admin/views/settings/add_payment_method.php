<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_payment_method'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("system_settings/add_payment_method", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label class="control-label" for="code"><?php echo $this->lang->line("code"); ?></label>
                <?php echo form_input('code', '', 'class="form-control" id="code" required="required"'); ?>
            </div>
            <div class="form-group">
                <label class="control-label" for="name"><?php echo $this->lang->line("name"); ?></label>
                <?php echo form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="row">
                <div class="form-group col-sm-9">
                    <?= lang("icon", "icon"); ?>
                    <?php
                    $icons[''] = '';
                    foreach ($icon_files as $key => $value) {
                        $icons[$value] = $value;
                    }
                    echo form_dropdown('icon', $icons, 'default.png', 'class="form-control not_select" id="icon" required="required"'); ?>
                </div>
                <div id="icon-con" class="text-center col-sm-3" style="padding-top: 24px;">
                    <img class="img-responsive center-block" src="<?= base_url('assets/payment_methods_icons/default.png') ?>" alt="" style="max-width: 45px;">
                </div>
            </div>
            <div class="form-group">
                <?= lang('state_sale', 'state_sale') ?><br>
                <label>
                    <input type="checkbox" name="state_sale" id="state_sale" value="1">
                    <?= lang('active') ?>
                </label>
            </div>
            <div class="form-group">
                <?= lang('state_purchase', 'state_purchase') ?><br>
                <label>
                    <input type="checkbox" name="state_purchase" id="state_purchase" value="1">
                    <?= lang('active') ?>
                </label>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            <input type="checkbox" name="cash_payment" id="cash_payment">
                            <?= lang('cash_payment') ?>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            <input type="checkbox" name="due_payment" id="due_payment">
                            <?= lang('due_payment') ?>
                        </label>
                    </div>
                </div>
            </div>
            
            <?php if ($this->Settings->modulary): ?>
                <div class="form-group">
                    <?= lang('receipt_ledger', 'receipt_ledger_id') ?>
                    <?php
                        $lopts[''] = lang('select');

                        foreach ($ledgers as $ledger) {
                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                        }

                     ?>

                    <?= form_dropdown('receipt_ledger_id', $lopts, '', 'class="form-control" id="receipt_ledger_id" style="width:100%;"'); ?>

                </div>
                <div class="form-group">
                    <?= lang('payment_ledger', 'payment_ledger_id') ?>
                    <?php
                        $lopts[''] = lang('select');

                        foreach ($ledgers as $ledger) {
                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                        }

                     ?>

                    <?= form_dropdown('payment_ledger_id', $lopts, '', 'class="form-control" id="payment_ledger_id" style="width:100%;"'); ?>

                </div>
            <?php endif ?>

            <div class="form-group">
                <?= lang('mean_payment_code_fe', 'code_fe') ?>
                <select class="form-control" name="code_fe" id="code_fe" required>
                    <option value=""><?= lang('select'); ?></option>
                    <?php foreach ($payment_mean_codes as $payment_mean_code) { ?>
                        <option value="<?= $payment_mean_code->code; ?>"><?= $payment_mean_code->name; ?></option>
                    <?php } ?>
                </select>
            </div>

            <?php if ($this->Settings->payments_methods_retcom == 1): ?>
                <div class="form-group">
                    <?= lang('commision_value', 'commision_value') ?>
                    <input type="text" name="commision_value" id="commision_value" class="form-control">
                </div>
                <div class="form-group">
                    <?= lang('commision_ledger_id', 'commision_ledger_id') ?>
                    <?php
                        $lopts[''] = lang('select');
                        foreach ($ledgers as $ledger) {
                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                        }
                     ?>
                    <?= form_dropdown('commision_ledger_id', $lopts, '', 'class="form-control" id="commision_ledger_id" style="width:100%;"'); ?>
                </div>

                
                <div class="form-group">
                    <?= lang('retefuente_value', 'retefuente_value') ?>
                    <input type="text" name="retefuente_value" id="retefuente_value" class="form-control">
                </div>
                <div class="form-group">
                    <?= lang('retefuente_ledger_id', 'retefuente_ledger_id') ?>
                    <?php
                        $lopts[''] = lang('select');
                        foreach ($ledgers as $ledger) {
                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                        }
                     ?>
                    <?= form_dropdown('retefuente_ledger_id', $lopts, '', 'class="form-control" id="retefuente_ledger_id" style="width:100%;"'); ?>
                </div>

                
                <div class="form-group">
                    <?= lang('reteiva_value', 'reteiva_value') ?>
                    <input type="text" name="reteiva_value" id="reteiva_value" class="form-control">
                </div>
                <div class="form-group">
                    <?= lang('reteiva_ledger_id', 'reteiva_ledger_id') ?>
                    <?php
                        $lopts[''] = lang('select');
                        foreach ($ledgers as $ledger) {
                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                        }
                     ?>
                    <?= form_dropdown('reteiva_ledger_id', $lopts, '', 'class="form-control" id="reteiva_ledger_id" style="width:100%;"'); ?>
                </div>

                
                <div class="form-group">
                    <?= lang('reteica_value', 'reteica_value') ?>
                    <input type="text" name="reteica_value" id="reteica_value" class="form-control">
                </div>
                <div class="form-group">
                    <?= lang('reteica_ledger_id', 'reteica_ledger_id') ?>
                    <?php
                        $lopts[''] = lang('select');
                        foreach ($ledgers as $ledger) {
                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                        }
                     ?>
                    <?= form_dropdown('reteica_ledger_id', $lopts, '', 'class="form-control" id="reteica_ledger_id" style="width:100%;"'); ?>
                </div>

                <div class="form-group">
                    <?= lang('supplier', 'supplier_id') ?>
                    <?php
                        $lopts = [];
                        $lopts[''] = lang('select');
                        foreach ($suppliers as $supplier) {
                            $lopts[$supplier->id] = $supplier->company;
                        }
                     ?>
                    <?= form_dropdown('supplier_id', $lopts, '', 'class="form-control" id="supplier_id" style="width:100%;"'); ?>
                </div>
            <?php endif ?>

            <div class="form-group">
                <?= lang('biller', 'billers'); ?>
                <select class="form-control select" name="billers[]" id="billers" multiple="multiple">
                  <?php foreach ($billers as $biller) : ?>
                    <option value="<?= $biller->id ?>"><?= $biller->company; ?></option>
                  <?php endforeach ?>
                </select>
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_payment_method', lang('add_payment_method'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript">
    $(document).on('ifChecked', '#due_payment', function(){
        $('#cash_payment').iCheck('uncheck').iCheck('disable');
    });

    $(document).on('ifUnchecked', '#due_payment', function(){
        $('#cash_payment').iCheck('enable');
    });

    $(document).on('ifChecked', '#cash_payment', function(){
        $('#due_payment').iCheck('uncheck').iCheck('disable');
    });

    $(document).on('ifUnchecked', '#cash_payment', function(){
        $('#due_payment').iCheck('enable');
    });

    $(document).on('change', '#icon', function () {
        var icon = $(this).val();
        $('#icon-con').html('<img class="img-responsive center-block" src="<?=base_url('assets/payment_methods_icons')?>/' + icon + '" alt="" style="max-width: 45px;">');
    });
</script>
<?= $modal_js ?>
