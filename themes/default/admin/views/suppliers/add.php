<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_supplier'); ?></h4>
        </div>
        <?= admin_form_open_multipart("suppliers/add", ['id' => 'add-supplier-form']); ?>
        <input type="hidden" name="from_purchase" id="from_purchase" value="<?= $from_purchase ?>">
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Tipo de proveedor</label>
                    <?php
                    $tsupplier[1] = "Productos y Gastos";
                    $tsupplier[2] = "Productos";
                    $tsupplier[3] = "Gastos";
                    $tsupplier[4] = "Acreedores";
                    $tsupplier[5] = "Entidad Financiera";
                    $tsupplier[6] = "Entidad Promotora de Salud (EPS)";
                    $tsupplier[7] = "Fondo de Pensiones y Cesantías";
                    $tsupplier[8] = "Administradora de Riesgos Laborales (ARL)";
                    $tsupplier[9] = "Caja de compensación";
                    $tsupplier[10] = "Servicio Nacional de Aprendizaje (SENA)";
                    echo form_dropdown('supplier_type', $tsupplier, '', 'class="form-control select" id="supplier_type" style="width:100%;" required="required"');
                    ?>
                </div>
                <div class="form-group col-md-6">
                    <label>Tipo de persona</label>
                    <?php
                    $typesPersonOptions[""] = lang('select');
                    foreach ($typesPerson as $typePerson) {
                        $typesPersonOptions[$typePerson->id] = lang($typePerson->description);
                    }
                    ?>
                    <?= form_dropdown('type_person', $typesPersonOptions, '', 'class="form-control select" id="type_person" style="width:100%;" required="required"'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= form_label(lang("label_type_vat_regime"), "type_vat_regime"); ?>
                    <?php
                    $types_vat_regime_options[""] = lang('select');
                    foreach ($types_vat_regime as $type_vat_regime) {
                        $types_vat_regime_options[$type_vat_regime->id] = lang($type_vat_regime->description);
                    }
                    ?>
                    <?= form_dropdown(["name" => "tipo_regimen", "id" => "tipo_regimen", "class" => "form-control select", "required" => TRUE], $types_vat_regime_options); ?>
                </div>
                <div class="form-group col-md-6">
                    <?= lang("id_document_type", "id_document_type"); ?>
                    <select class="form-control" name="tipo_documento" id="tipo_documento" style="width: 100%;" required>
                        <option value=""><?= lang("select"); ?></option>
                        <?php foreach ($id_document_types as $idt) : ?>
                            <option value="<?= $idt->id; ?>" data-code="<?= $idt->codigo_doc; ?>"><?= lang($idt->nombre); ?></option>
                        <?php endforeach ?>
                    </select>
                    <?= form_hidden('document_code'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang("vat_no", "vat_no"); ?>
                    <?php echo form_input('vat_no', '', 'class="form-control" id="vat_no" required="required"'); ?>
                </div>
                <div class="form-group col-md-6 digito-verificacion" style="display: none;">
                    <?= lang("check_digit", "check_digit"); ?>
                    <?php echo form_input('digito_verificacion', '', 'class="form-control" id="digito_verificacion" readonly'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group person natural_person" style="display: none;">
                    <div class="col-md-6">
                        <label>Primer Nombre</label><span class='input_required'> *</span>
                        <input type="text" name="first_name" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Segundo Nombre</label>
                        <input type="text" name="second_name" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Primer Apellido</label><span class='input_required'> *</span>
                        <input type="text" name="first_lastname" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Segundo Apellido</label>
                        <input type="text" name="second_lastname" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6 person juridical_person">
                    <?= lang("name", "name"); ?>
                    <?php echo form_input('name', '', 'class="form-control tip required" id="name" data-bv-notempty="true"'); ?>
                </div>

                <div class="form-group col-md-6 company hide_pos_customer natural_person"  style="display: none;">
                    <?= lang("label_tradename", "company"); ?>
                    <?= form_input('company', '', 'class="form-control tip" id="company"'); ?>
                </div>

                <div class="form-group col-md-6">
                    <?= lang("email_address", "email_address"); ?>
                    <input type="email" name="email" class="form-control" required="required" id="email_address" />
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang("phone", "phone"); ?>
                    <input type="tel" name="phone" class="form-control" required="required" id="phone" />
                </div>
                <div class="form-group col-md-6">
                    <?= lang("postal_code", "postal_code"); ?>
                    <small><a href="https://visor.codigopostal.gov.co/472/visor/" target="_blank"> Buscar códigos postales</a></small>
                    <?php echo form_input('postal_code', '', 'class="form-control postal_code" id="postal_code"'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang("country", "country"); ?>
                    <select class="form-control select" name="country" id="country" required>
                        <option value="">Seleccione...</option>
                        <?php foreach ($countries as $row => $country) : ?>
                            <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= $country->NOMBRE == $this->Settings->pais ? 'selected="selected"' : '' ?>><?= $country->NOMBRE ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <?= lang("state", "state"); ?>
                    <select class="form-control select" name="state" id="state" required>
                        <option value="">Seleccione país</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang("city", "city"); ?>
                    <select class="form-control select" name="city" id="city" required>
                        <option value="">Seleccione departamento</option>
                    </select>
                    <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='" . $_POST['city_code'] . "'" : "" ?>>
                </div>
                <div class="form-group col-md-6">
                    <?= lang("address", "address"); ?>
                    <?php echo form_input('address', '', 'class="form-control" id="address" required="required"'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang('supplier_payment_type', 'supplier_payment_type') ?>
                    <?php
                    $cptypes = [
                        '1' => lang('payment_type_cash'),
                        '0' => lang('credit'),
                    ];
                    ?>
                    <?= form_dropdown('supplier_payment_type', $cptypes, '', 'class="form-control select" id="supplier_payment_type" style="width:100%;" required="required"'); ?>
                </div>
                <div class="form-group col-md-6 credit_customer" style="display: none;">
                    <?= lang('supplier_credit_limit', 'supplier_credit_limit') ?>
                    <input type="text" name="supplier_credit_limit" id="supplier_credit_limit" class="form-control only_number">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6 credit_customer" style="display: none;">
                    <?= lang('supplier_payment_term', 'supplier_payment_term') ?>
                    <input type="text" name="supplier_payment_term" id="supplier_payment_term" class="form-control only_number">
                </div>
                <div class="col-md-6" id="generateSupportingDocumentContainer">
                    <div class="form-group">
                        <label style="margin-top: 35px;">
                            <input type="checkbox" name="generateSupportingDocument" id="generateSupportingDocument"> <?= lang('Do_you_handle_supporting_documents?') ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= form_label(lang("label_types_obligations"), "types_obligations"); ?>
                    <?php
                    $typesObligationsOptions = [];
                    foreach ($typesObligations as $typesObligation) {
                        $typesObligationsOptions[$typesObligation->code] = $typesObligation->code . " - " . $typesObligation->description;
                    }
                    ?>
                    <?= form_dropdown(['name' => 'types_obligations[]', 'id' => 'types_obligations', 'class' => 'form-control select', 'multiple' => TRUE, 'style' => 'height: auto;', 'required' => TRUE], $typesObligationsOptions); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="taxResident" style="margin-top: 35px;">
                            <input type="checkbox" name="taxResident" id="taxResident"> <?= lang('tax_resident') ?>
                        </label>
                    </div>
                </div>
            </div>
            <?php if ($custom_fields) : ?>
                <div class="row">
                    <hr class="col-md-11">
                    <?php foreach ($custom_fields as $id => $arr) : ?>
                        <?php
                        $cf = $arr['data'];
                        $cf_values = isset($arr['values']) ? $arr['values'] : NULL;
                        ?>
                        <div class="col-md-6">
                            <input type="hidden" name="custom_field_code[]" value="<?= $cf->cf_code ?>">
                            <label><?= $cf->cf_name ?></label>
                            <?php if ($cf->cf_type == 'select' || $cf->cf_type == 'multiple') : ?>
                                <select name="custom_field_value[]" class="form-control" <?= $cf->cf_type == 'multiple' ? "multiple" : "" ?>>
                                    <?php if ($cf_values) : ?>
                                        <option value="">Seleccione...</option>
                                        <?php foreach ($cf_values as $cfv) : ?>
                                            <option value="<?= $cfv->cf_value ?>"><?= $cfv->cf_value ?></option>
                                        <?php endforeach ?>
                                    <?php else : ?>
                                        <option value="">Sin valores para el campo</option>
                                    <?php endif ?>
                                </select>
                            <?php endif ?>
                            <?php if ($cf->cf_type == 'date' || $cf->cf_type == 'text') : ?>
                                <input type="<?= $cf->cf_type ?>" name="custom_field_value[]" class="form-control">
                            <?php endif ?>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <?= lang('seller_phone', 'seller_phone') ?>
                    <input type="number" name="seller_phone" id="seller_phone" class="form-control only_number">
                </div>
                <div class="col-sm-6 form-group">
                    <?= lang('seller_email', 'seller_email') ?>
                    <input type="text" name="seller_email" id="seller_email" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <?= lang('seller_name', 'seller_name') ?>
                    <input type="text" name="seller_name" id="seller_name" class="form-control">
                </div>
                <div class="col-sm-6 form-group">
                    <?= lang('account_number', 'account_number') ?>
                    <input type="number" name="account_number" id="account_number" class="form-control only_number">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <?= lang('account_type', 'account_type') ?>
                    <input type="text" name="account_type" id="account_type" class="form-control">
                </div>
                <div class="col-sm-6 form-group">
                    <?= lang('bank_id', 'bank_id') ?>
                    <input type="text" name="bank_id" id="bank_id" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <?= lang('delivery_days', 'delivery_days') ?>
                    <input type="number" name="delivery_days" id="delivery_days" class="form-control only_number">
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="form-group col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><?= lang('retention') ?></th>
                                <th><?= lang('percentage') ?></th>
                                <th><?= lang('min_base') ?></th>
                                <th><?= lang('ledger') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($retentions) > 0) : ?>
                                <?php if (isset($retentions['FUENTE'])) : ?>
                                    <tr class="row_rete_fuente">
                                        <td>
                                            <select name="default_rete_fuente_id" id="default_rete_fuente_id" class="form-control">
                                                <option value=""><?= lang('no_rete_fuente_default') ?></option>
                                                <?php foreach ($retentions['FUENTE'] as $row) : ?>
                                                    <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : NULL ?>" data-minbase="<?= $row['min_base'] ?>"><?= $row['description'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_fuente_percentage form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_fuente_minbase form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_fuente_ledger form-control" readonly>
                                        </td>
                                    </tr>
                                <?php endif ?>
                                <?php if (isset($retentions['IVA'])) : ?>
                                    <tr class="row_rete_iva">
                                        <td>
                                            <select name="default_rete_iva_id" id="default_rete_iva_id" class="form-control">
                                                <option value=""><?= lang('no_rete_iva_default') ?></option>
                                                <?php foreach ($retentions['IVA'] as $row) : ?>
                                                    <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= $row['ledger_name'] ?>" data-minbase="<?= $row['min_base'] ?>"><?= $row['description'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_iva_percentage form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_iva_minbase form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_iva_ledger form-control" readonly>
                                        </td>
                                    </tr>
                                <?php endif ?>
                                <?php if (isset($retentions['ICA'])) : ?>
                                    <tr class="row_rete_ica">
                                        <td>
                                            <select name="default_rete_ica_id" id="default_rete_ica_id" class="form-control">
                                                <option value=""><?= lang('no_rete_ica_default') ?></option>
                                                <?php foreach ($retentions['ICA'] as $row) : ?>
                                                    <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= $row['ledger_name'] ?>" data-minbase="<?= $row['min_base'] ?>"><?= $row['description'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_ica_percentage form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_ica_minbase form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_ica_ledger form-control" readonly>
                                        </td>
                                    </tr>
                                <?php endif ?>
                                <?php if (isset($retentions['OTRA'])) : ?>
                                    <tr class="row_rete_other">
                                        <td>
                                            <select name="default_rete_other_id" id="default_rete_other_id" class="form-control">
                                                <option value=""><?= lang('no_rete_other_default') ?></option>
                                                <?php foreach ($retentions['OTRA'] as $row) : ?>
                                                    <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= $row['ledger_name'] ?>" data-minbase="<?= $row['min_base'] ?>"><?= $row['description'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_other_percentage form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_other_minbase form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_other_ledger form-control" readonly>
                                        </td>
                                    </tr>
                                <?php endif ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-success" id="submit_add_supplier" type="button"><?= lang('add_supplier') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(e) {
        $("#add-supplier-form").validate({
            ignore: []
        });
        $(document).on('click', '#submit_add_supplier', function() {
            form = $('#add-supplier-form');
            if (form.valid()) {
                form.submit();
            }
        });
        $('#tipo_documento').on('change', function() {
            if ($(this).val() == 6 && site.settings.get_companies_check_digit == 1) {
                $('.digito-verificacion').css('display', '');
                $('.digito-verificacion input').attr('required', true);
            } else {
                $('.digito-verificacion').css('display', 'none');
                $('.digito-verificacion input').attr('required', false);
            }

            $('input[name="document_code"]').val($('#tipo_documento option:selected').data('code'));
        });
        $('#type_person').on('change', function() {
            tp = $(this).val();
            st = $('#supplier_type').val();
            if (st >= 5 && st <= 10) {
                //revisar
            } else {
                $('input[name="phone"]').attr('required', true);
                if (tp == <?= NATURAL_PERSON ?>) {
                    $('input[name="first_name"]').attr('required', true);
                    $('input[name="first_lastname"]').attr('required', true);
                    $('.natural_person').css('display', '');
                    $.each($('.natural_person').find('.required'), function() {
                        $(this).attr('required', true);
                    });
                    $('.juridical_person').css('display', 'none');
                    $.each($('.juridical_person').find('.required'), function() {
                        $(this).attr('required', false);
                    });
                    $('#name').val('xx');
                } else if (tp == <?= LEGAL_PERSON ?>) {
                    $('input[name="first_name"]').removeAttr('required');
                    $('input[name="first_lastname"]').removeAttr('required');
                    $('.natural_person').css('display', 'none');
                    $.each($('.natural_person').find('.required'), function() {
                        $(this).attr('required', false);
                    });
                    $('.juridical_person').css('display', '');
                    $.each($('.juridical_person').find('.required'), function() {
                        $(this).attr('required', true);
                    });
                    $('#name').val('');
                }
            }
        });
        $('#state').on('change', function() {
            dpto = $('#state option:selected').data('code');
            $.ajax({
                url: "<?= admin_url() ?>customers/get_cities/" + dpto,
            }).done(function(data) {
                $('#city').html(data);
                <?php if (!empty($this->Settings->ciudad)) { ?>
                    $('#city').select2('val', '<?= $this->Settings->ciudad ?>');
                    $('#city').trigger('change');
                <?php } ?>
            }).fail(function(data) {
                console.log(data);
            });
        });
        $('#country').on('change', function() {
            dpto = $('#country option:selected').data('code');
            $.ajax({
                url: "<?= admin_url() ?>customers/get_states/" + dpto,
            }).done(function(data) {
                $('#state').html(data);
                <?php if (!empty($this->Settings->departamento)) { ?>
                    $('#state').select2('val', '<?= $this->Settings->departamento ?>');
                    $('#state').trigger('change');
                <?php } ?>
            }).fail(function(data) {
                console.log(data);
            });
        });
        $('#city').on('change', function() {
            code = $('#city option:selected').data('code');
            $('#city_code').val(code);
        });
        $('#vat_no').on('change', function() {
            nit = $(this).val();
            if (site.settings.get_companies_check_digit == 1) {
                dvf = calcularDigitoVerificacion(nit);
                $('#digito_verificacion').val(dvf);
            }
        });
        <?php if (!empty($this->Settings->pais)) { ?>
            $('#country').trigger('change');
        <?php } ?>

        $('#bank_id').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10,
                            ptype: 8,
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });

    });


    $('#vat_no').on('keyup', function() {
        var vat_no = $(this).val();
        $.ajax({
            url: '<?= admin_url("suppliers/validate_vat_no/") ?>' + vat_no
        }).done(function(data) {
            if (data == "true") {
                Command: toastr.error('Ya existe el proveedor', '¡Error!', {
                    onHidden: function() {}
                })
                $('#add_supplier').prop('disabled', true);
            }
            else {
                $('#add_supplier').prop('disabled', false);
            }
        });
    });
    $(document).on('change', '#default_rete_fuente_id', function() {
        var rete_fuente = $('#default_rete_fuente_id option:selected');
        var percentage = rete_fuente.data('percentage');
        var min_base = rete_fuente.data('minbase');
        var ledger = rete_fuente.data('account');
        $('.rete_fuente_percentage').val(percentage);
        $('.rete_fuente_minbase').val(min_base);
        $('.rete_fuente_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_iva_id', function() {
        var rete_iva = $('#default_rete_iva_id option:selected');
        var percentage = rete_iva.data('percentage');
        var min_base = rete_iva.data('minbase');
        var ledger = rete_iva.data('account');
        $('.rete_iva_percentage').val(percentage);
        $('.rete_iva_minbase').val(min_base);
        $('.rete_iva_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_ica_id', function() {
        var rete_ica = $('#default_rete_ica_id option:selected');
        var percentage = rete_ica.data('percentage');
        var min_base = rete_ica.data('minbase');
        var ledger = rete_ica.data('account');
        $('.rete_ica_percentage').val(percentage);
        $('.rete_ica_minbase').val(min_base);
        $('.rete_ica_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_other_id', function() {
        var rete_other = $('#default_rete_other_id option:selected');
        var percentage = rete_other.data('percentage');
        var min_base = rete_other.data('minbase');
        var ledger = rete_other.data('account');
        $('.rete_other_percentage').val(percentage);
        $('.rete_other_minbase').val(min_base);
        $('.rete_other_ledger').val(ledger);
    });
    $('#supplier_payment_type').on('change', function() {
        val = $(this).val();
        if (val == 0) {
            $('.credit_customer').css('display', '');
            $('#supplier_credit_limit').prop('required', true);
            $('#supplier_payment_term').prop('required', true);
        } else if (val == 1) {
            $('.credit_customer').css('display', 'none');
            $('#supplier_credit_limit').prop('required', false);
            $('#supplier_payment_term').prop('required', false);
        }
    });
    $('#email_address').on('change', function() {
        email_address = $(this).val();
        $.ajax({
            url: site.base_url + "suppliers/validate_email",
            type: "GET",
            data: {
                email: email_address
            }
        }).done(function(data) {
            data = JSON.parse(data);
            if (data) {
                Command: toastr.warning('Ya existe un tercero con el correo ingresado', '¡Advertencia!', {
                    onHidden: function() {}
                })
            }
        });
    });
    $('#supplier_type').on('change', function() {
        var st = $(this).val();
        if (st >= 5 && st <= 10) {
            $('input').prop('required', false);
            $('select').prop('required', false);

            $('#tipo_documento').prop('required', true);
            $('#type_person').prop('required', true);
            $('#vat_no').prop('required', true);
            $('#name').prop('required', true);
            $('#country').prop('required', true);
            $('#state').prop('required', true);
            $('#city').prop('required', true);


            $('#type_person').select2('val', 0).trigger('change');
            $('#tipo_documento').select2('val', 6).trigger('change');
            $('#tipo_regimen').select2('val', 2).trigger('change');

        } else {
            $('#type_person').trigger('change');
        }

        showHideGenerateSupportingDocumentContainer();
    });

    $(document).on('change', '#tipo_regimen', function() {
        showHideGenerateSupportingDocumentContainer();
    });

    function showHideGenerateSupportingDocumentContainer() {
        var supplierType = $('#supplier_type').val();
        var typeRegime = $('#tipo_regimen').val();

        if (supplierType <= 4) {
            if (typeRegime != 1) {
                $('#generateSupportingDocumentContainer').fadeOut();
                $('#generateSupportingDocument').iCheck('uncheck');
            } else {
                $('#generateSupportingDocumentContainer').fadeIn();
            }
        } else {
            $('#generateSupportingDocumentContainer').fadeOut();
            $('#generateSupportingDocument').iCheck('uncheck');
        }
    }
</script>
<?= $modal_js ?>