$(document).ready(function () {
    if (!localStorage.getItem('qucustomerspecialdiscount')) {
        customer_special_discount();
    }

    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }
    // Order level shipping and discoutn localStorage
    if (qudiscount = localStorage.getItem('qudiscount')) {
        $('#qudiscount').val(qudiscount);
    }
    $('#qutax2').change(function (e) {
        localStorage.setItem('qutax2', $(this).val());
        $('#qutax2').val($(this).val());
    });
    if (qutax2 = localStorage.getItem('qutax2')) {
        $('#qutax2').select2("val", qutax2);
    }
    $('#qustatus').change(function (e) {
        localStorage.setItem('qustatus', $(this).val());
    });
    if (qustatus = localStorage.getItem('qustatus')) {
        $('#qustatus').select2("val", qustatus);
    }
    var old_shipping;
    $('#qushipping').focus(function () {
        old_shipping = $(this).val();
    }).change(function () {
        if (!is_numeric($(this).val())) {
            $(this).val(old_shipping);
            bootbox.alert(lang.unexpected_value);
            return;
        } else {
            shipping = $(this).val() ? parseFloat($(this).val()) : '0';
        }
        localStorage.setItem('qushipping', shipping);
        var gtotal = ((total + invoice_tax) - order_discount) + shipping;
        $('#gtotal').text(formatMoney(gtotal));
        $('#tship').text(formatMoney(shipping));
    });
    if (qushipping = localStorage.getItem('qushipping')) {
        shipping = parseFloat(qushipping);
        $('#qushipping').val(shipping);
    } else {
        shipping = 0;
    }

    $('#qusupplier').change(function (e) {
        localStorage.setItem('qusupplier', $(this).val());
        $('#supplier_id').val($(this).val());
    });
    if (qusupplier = localStorage.getItem('qusupplier')) {
        $('#qusupplier').val(qusupplier).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    } else {
        nsSupplier();
    }

    // If there is any item in localStorage
    if (localStorage.getItem('quitems')) {
        loadItems();
    }

    // clear localStorage and reload
    $('#reset').click(function (e) {
            bootbox.confirm(lang.r_u_sure, function (result) {
                if (result) {
                    if (localStorage.getItem('quitems')) {
                        localStorage.removeItem('quitems');
                    }
                    if (localStorage.getItem('qudiscount')) {
                        localStorage.removeItem('qudiscount');
                    }
                    if (localStorage.getItem('qutax2')) {
                        localStorage.removeItem('qutax2');
                    }
                    if (localStorage.getItem('qushipping')) {
                        localStorage.removeItem('qushipping');
                    }
                    if (localStorage.getItem('quref')) {
                        localStorage.removeItem('quref');
                    }
                    if (localStorage.getItem('quwarehouse')) {
                        localStorage.removeItem('quwarehouse');
                    }
                    if (localStorage.getItem('qunote')) {
                        localStorage.removeItem('qunote');
                    }
                    if (localStorage.getItem('quinnote')) {
                        localStorage.removeItem('quinnote');
                    }
                    if (localStorage.getItem('qucustomer')) {
                        localStorage.removeItem('qucustomer');
                    }
                    if (localStorage.getItem('qucurrency')) {
                        localStorage.removeItem('qucurrency');
                    }
                    if (localStorage.getItem('qudate')) {
                        localStorage.removeItem('qudate');
                    }
                    if (localStorage.getItem('qustatus')) {
                        localStorage.removeItem('qustatus');
                    }
                    if (localStorage.getItem('qubiller')) {
                        localStorage.removeItem('qubiller');
                    }

                    $('#modal-loading').show();
                    location.reload();
                }
            });
    });

// save and load the fields in and/or from localStorage

    $('#quref').change(function (e) {
        localStorage.setItem('quref', $(this).val());
    });
    if (quref = localStorage.getItem('quref')) {
        $('#quref').val(quref);
    }
    $('#quwarehouse').change(function (e) {
        localStorage.setItem('quwarehouse', $(this).val());
    });
    if (quwarehouse = localStorage.getItem('quwarehouse')) {
        $('#quwarehouse').select2("val", quwarehouse);
    }

    $('#qunote').redactor('destroy');
    $('#qunote').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('qunote', v);
        }
    });
    if (qunote = localStorage.getItem('qunote')) {
        $('#qunote').redactor('set', qunote);
    }
    var $customer = $('#qucustomer');
    $customer.change(function (e) {
        localStorage.setItem('qucustomer', $(this).val());
    });
    if (qucustomer = localStorage.getItem('qucustomer')) {
        $customer.val(qucustomer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#qucustomer').trigger('change');
        if (qucustomerbranch = localStorage.getItem('qucustomerbranch')) {
            setTimeout(function() {
                $('#qucustomerbranch').select2("val", qucustomerbranch).trigger('change');
            }, 2000);
        }
    } else {
        nsCustomer();
    }

    $('#qucustomerbranch').change(function (e) {
        localStorage.setItem('qucustomerbranch', $(this).val());
    });

    $(document).on('change', '#document_type_id', function(){
        validate_key_log();
        loadItems();
    });

    // prevent default action upon enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    // Order tax calculation
    if (site.settings.tax2 != 0) {
        $('#qutax2').change(function () {
            localStorage.setItem('qutax2', $(this).val());
            loadItems();
            return;
        });
    }

    // Order discount calculation
    var old_qudiscount;
    $('#qudiscount').focus(function () {
        old_qudiscount = $(this).val();
    }).change(function () {
        var new_discount = $(this).val() ? $(this).val() : '0';
        if (is_valid_discount(new_discount)) {
            localStorage.removeItem('qudiscount');
            localStorage.setItem('qudiscount', new_discount);
            loadItems();
            return;
        } else {
            $(this).val(old_qudiscount);
            bootbox.alert(lang.unexpected_value);
            return;
        }

    });

    /* ----------------------
     * Delete Row Method
     * ---------------------- */
    $(document).on('click', '.qudel', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        delete quitems[item_id];
        row.remove();
        if(quitems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('quitems', JSON.stringify(quitems));
            loadItems();
            return;
        }
    });

    /* -----------------------
     * Edit Row Modal Hanlder
     ----------------------- */
    $(document).on('click', '.edit', function () {
        var trmrate = 1;
        var key_log = localStorage.getItem('slkeylog');
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        $('#row_id').val(row_id);
        // console.log('TR '+row_id);
        item_id = row.attr('data-item-id');
        item = quitems[item_id];        var qty = row.children().children('.rquantity').val(),
        product_option = row.children().children('.roption').val(),
        unit_price = formatDecimal(item.row.unit_price),
        discount = row.children().children('.rdiscount').val();
        if(item.options !== 'undefined' && item.options !== false) {
            $.each(item.options, function () {
                if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                    unit_price = parseFloat(item.row.real_unit_price)+parseFloat(this.price);
                }
            });
        }
        var real_unit_price = item.row.real_unit_price;
        var net_price = unit_price;
        $('#prModalLabel').text(item.row.name + ' (' + item.row.code + ')');
        $('#pname').val(item.row.name);
        if (site.settings.tax1) {
            // $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax2').val(item.row.consumption_sale_tax);

            if(site.settings.allow_change_sale_iva == 0){
                $('#ptax').select2('readonly', true);
            }

            $('#old_tax').val(item.row.tax_rate);
            var item_discount = 0, ds = discount ? discount : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal(parseFloat(((unit_price) * parseFloat(pds[0])) / 100));
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }
            net_price -= item_discount;
            var pr_tax = item.row.tax_rate, pr_tax_val = 0;
            if (pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            if (quitems[item_id].row.tax_method == 0) {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                net_price -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / 100));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }
                        } else if (this.type == 2) {
                            pr_tax_val = parseFloat(this.rate);
                            pr_tax_rate = this.rate;
                        }
                    }
                });
            }
        } else {
            $('#ptax').select2('val', 1).trigger('change');
        }
        if (site.settings.product_serial !== 0) {
            $('#pserial').val(row.children().children('.rserial').val());
        }
        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.options !== false) {
            var o = 1;
            opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
            $.each(item.options, function () {
                if(o == 1) {
                    if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                }
                $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                o++;
            });
        } else {
            $('.poptions_div').fadeOut();
            product_variant = 0;
        }


        uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        var pref = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.preferences !== null && item.preferences !== undefined && item.preferences !== false) {
            pref = $("<select id=\"ppreferences\" name=\"ppreferences\" class=\"form-control select\" multiple />");
            $.each(item.preferences, function () {
                $("<option />", {value: this.id, text: "("+this.category_name+") "+this.name, "data-pfcatid" : this.preference_category_id}).appendTo(pref);
            });
        } else {
            $('.ppreferences_div').fadeOut();
        }
        if (item.units) {
            // console.log(item.units);
            uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
            $.each(item.units, function () {
                if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                    if(this.product_unit_id == item.row.product_unit_id_selected) {
                        $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.product_unit_id, text: this.name}).appendTo(uopt);
                    }
                } else {
                    if(this.id == item.row.unit) {
                        $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                    }
                }
            });
            
            if (Object.keys(item.units).length == 1) {
                $('.punit_div').fadeOut();
            }
        }
        $('#poptions-div').html(opt);
        $('#ppreferences-div').html(pref);
        if (item.preferences_selected !== undefined && item.preferences_selected !== null) {
            $('#ppreferences').val(item.preferences_selected);
        }
        $('#punits-div').html(uopt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#pquantity').val(qty);
        $('#old_qty').val(qty);
        $('#pprice').val(trmrate != 1 ? formatDecimal(trmrate * unit_price, 6) : trmrate * unit_price);
        $('#pprice_ipoconsumo').val('');
        $('#punit_price').val(formatDecimal(parseFloat(unit_price)+parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax)));
        $('#poption').select2('val', item.row.option);
        $('#old_price').val(unit_price);
        $('#item_id').val(item_id);
        $('#pserial').val(row.children().children('.rserial').val());
        $('#pdiscount').val(getDiscountAssigned(discount, trmrate, 1));
        $('#net_price').text(trmrate != 1 ? formatDecimal(trmrate * net_price, 6) : formatMoney(trmrate * net_price));
        $('#pro_tax').text(formatMoney(trmrate * (parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax))));

        if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            units = item.units;
            unit_selected = units[item.row.product_unit_id_selected !== undefined && parseFloat(item.row.product_unit_id_selected) > 0 ? item.row.product_unit_id_selected : item.row.unit];
            unit_selected_quantity = qty;
            unit_selected_real_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty / unit_selected.operation_value;
                unit_selected_real_quantity = 1 * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty * unit_selected.operation_value;
                unit_selected_real_quantity = 1 / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            }
            $('#pquantity').val(unit_selected_quantity);
            $('#punit').select2('val', item.row.product_unit_id_selected).trigger('change');
            $('#pproduct_unit_price').val(formatDecimal((unit_price + parseFloat(item.row.consumption_sale_tax)) * unit_selected_real_quantity));
        }

        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
        $('#prModal').appendTo("body").modal('show');

    });

    $('#prModal').on('shown.bs.modal', function (e) {
        if($('#poption').select2('val') != '') {
            $('#poption').select2('val', product_variant);
            product_variant = 0;
        }
    });

    $(document).on('change', '#pprice, #pprice_ipoconsumo, #ptax, #pdiscount', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var unit_price = parseFloat($('#pprice').val());
        var pprice_ipoconsumo = parseFloat($('#pprice_ipoconsumo').val());
        var item = quitems[item_id];
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';

        if (pprice_ipoconsumo > 0 && parseFloat(pprice_ipoconsumo) > parseFloat(item.row.consumption_sale_tax)) {
            pprice_ipoconsumo = pprice_ipoconsumo - parseFloat(item.row.consumption_sale_tax);
            var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
            if (item.row.tax_method == 1 && pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            unit_price = pprice_ipoconsumo / ((parseFloat(this.rate) / 100) + 1);
                        } else if (this.type == 2) {
                            unit_price = pprice_ipoconsumo - parseFloat(this.rate);
                        }
                    }
                });
            } else if (item.row.tax_method == 0) {
                unit_price = pprice_ipoconsumo;
            }
        }

        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / 100));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }
        if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 1  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(unit_price));
        } else if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 0  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(pprice_ipoconsumo));
        } else if (parseFloat($('#pprice_ipoconsumo').val()) <= parseFloat(item.row.consumption_sale_tax)) {
            Command: toastr.error('Por favor ingrese un precio válido', 'Valores incorrectos', {onHidden : function(){}})
            $('#pprice_ipoconsumo').val(0);
        }
        $('#net_price').text(formatMoney(unit_price));
        setTimeout(function() {
            $('#pprice_ipoconsumo').val('');
        }, 500);
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax)));
    });

    $(document).on('change', '#punit', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = quitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10 && unit) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 1 && site.settings.precios_por_unidad_presentacion == 2) {
                valor = unit_selected.operation_value;
                if (unit_selected.operator == "*") {
                    valor =  parseFloat(unit_selected.valor_unitario) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  parseFloat(unit_selected.valor_unitario) * parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor);
                $('#pproduct_unit_price').val(unit_selected.valor_unitario);
            } else {
                $('#pprice').val(unit_selected.valor_unitario);
                $('#pproduct_unit_price').val(unit_selected.valor_unitario);
            }
            $('#pprice').change();
        } else if(site.settings.prioridad_precios_producto == 11){

        } else {
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                        aprice = parseFloat(this.price);
                    }
                });
            }
            if(item.units && unit != quitems[item_id].row.base_unit) {
                $.each(item.units, function(){
                    if (this.id == unit) {
                        base_quantity = unitToBaseQty($('#pquantity').val(), this);
                        $('#pprice').val(formatDecimal(((parseFloat(item.row.base_unit_price+aprice))*unitToBaseQty(1, this)))).change();
                    }
                });
            } else {
                $('#pprice').val(formatDecimal(item.row.base_unit_price+aprice)).change();
            } //¡¡¡¡ NO BORRAR !!!!
        }
        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
    });

    $(document).on('change', '#pproduct_unit_price', function(){
        p_unit_price = $(this).val();
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = quitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 1) {
                valor = unit_selected.operation_value;
                ipoconsumo_um = 0;
                if (unit_selected.operator == "*") {
                    valor =  parseFloat(p_unit_price) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  parseFloat(p_unit_price) * parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor);
            } else {
                $('#pprice').val(p_unit_price);
            }
            $('#pprice').change();
        }

    });

    /* -----------------------
     * Edit Row Method
     ----------------------- */
    $(document).on('click', '#editItem', function () {
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_rate = false;
        if (new_pr_tax) {
            $.each(tax_rates, function () {
                if (this.id == new_pr_tax) {
                    new_pr_tax_rate = this;
                }
            });
        }
        var price = parseFloat($('#pprice').val());
        if(item.options !== false) {
            var opt = $('#poption').val();
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    price = price-parseFloat(this.price);
                }
            });
        }
        if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
            if(!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > price) {
                bootbox.alert(lang.unexpected_value);
                return false;
            }
        }
        if (!is_numeric($('#pquantity').val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var unit = $('#punit').val();
        if (Object.keys(quitems[item_id].units).length != 1) { // <- Si tiene mas de una unidad de medida, envia a la funcion unitToBaseQty la seleccion del select punit, si solo tiene una continua el flujo normal
            var base_quantity = unitToBaseQty($('#pquantity').val(),quitems[item_id].units[unit]);
        }
        if(unit != quitems[item_id].row.base_unit) {
            $.each(quitems[item_id].units, function(){
                if (this.id == unit) {
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        if (base_quantity === undefined) { // <- base quantity si pasa hasta aca indefinido se envia con la unidad base 0
            const units = quitems[item_id].units;
            var firstUnitKey = Object.keys(units)[0];
            var base_quantity = unitToBaseQty($('#pquantity').val(),quitems[item_id].units[firstUnitKey]);
        }
       
        tpax2 = parseFloat($('#ptax2').val() ? $('#ptax2').val() : 0);
        quitems[item_id].row.fup = 1,
        quitems[item_id].row.qty = parseFloat($('#pquantity').val()),
        quitems[item_id].row.base_quantity = parseFloat(base_quantity),
        quitems[item_id].row.unit_price = (price-parseFloat(tpax2)) / trmrate,
        quitems[item_id].row.unit = unit,
        quitems[item_id].row.tax_rate = new_pr_tax,
        quitems[item_id].tax_rate = new_pr_tax_rate,
        quitems[item_id].row.discount = $('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '',
        quitems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '',
        quitems[item_id].row.serial = $('#pserial').val();
        quitems[item_id].row.name = $('#pname').val() ? $('#pname').val() : quitems[item_id].row.name;
        prev_product_unit_id = quitems[item_id].row.product_unit_id;
        quitems[item_id].row.product_unit_id = $('#punit').val();
        quitems[item_id].row.product_unit_id_selected = $('#punit').val();

        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) {
            var ditem = quitems[item_id];
            unit_selected = ditem.units[unit];
            qty = parseFloat($('#pquantity').val());
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            }
            quitems[item_id].row.qty = unit_selected_quantity;
            quitems[item_id].row.base_quantity = unit_selected_quantity;
        } else {
            quitems[item_id].row.unit_price_id = $('#punit').val();
        }
        if (site.settings.prioridad_precios_producto == 11 && prev_product_unit_id !== undefined && prev_product_unit_id != quitems[item_id].row.product_unit_id) {
            wappsiPreciosUnidad(quitems[item_id].row.id,item_id,$('#pquantity').val(), true);
        }
        var preferences_text = '';
        var preferences_selected = [];
        var parr = [];
        $('#ppreferences option:selected').each(function(index, option){
            if (preferences_selected[$(option).data('pfcatid')] === undefined) {
                preferences_selected[$(option).data('pfcatid')] = [];
            }
            preferences_selected[$(option).data('pfcatid')].push(parseInt($(option).val()));
            psplit = $(option).text().split(')');
            psplit_index = psplit[0].replace("(", "");
            if (parr[psplit_index] === undefined) {
                parr[psplit_index] = [];
            }
            parr[psplit_index].push(psplit[1]);
        });
        if ($('#ppreferences option:selected').length > 0) {
            phtml = "";
            for (let preference_category in parr) {
                phtml += "<b>"+preference_category+" : </b>";
                for (let index in parr[preference_category]) {
                    phtml += parr[preference_category][index]+", ";
                }
            }
            quitems[item_id].preferences_text = phtml;
            quitems[item_id].preferences_selected = preferences_selected;
        }

        localStorage.setItem('quitems', JSON.stringify(quitems));
        $('#prModal').modal('hide');
        loadItems();
        return;
    });

    /* -----------------------
     * Product option change
     ----------------------- */
     $(document).on('change', '#poption', function () {
        var row = $('#' + $('#row_id').val()), opt = $(this).val();
        var item_id = row.attr('data-item-id');
        var item = quitems[item_id];
        var unit = $('#punit').val(), base_quantity = parseFloat($('#pquantity').val()), base_unit_price = item.row.base_unit_price;
        if(unit != quitems[item_id].row.base_unit) {
            $.each(quitems[item_id].units, function(){
                if (this.id == unit) {
                    base_unit_price = formatDecimal((parseFloat(item.row.base_unit_price)*(unitToBaseQty(1, this))))
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    $('#pprice').val(parseFloat(base_unit_price)+(parseFloat(this.price))).trigger('change');
                }
            });
        }
    });

    /* ------------------------------
     * Show manual item addition modal
     ------------------------------- */
     $(document).on('click', '#addManually', function (e) {
        if (count == 1) {
            quitems = {};
            if ($('#quwarehouse').val() && $('#qucustomer').val()) {
                $('#qucustomer').select2("readonly", true);
                $('#quwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#mnet_price').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal').appendTo("body").modal('show');
        return false;
    });

     $(document).on('click', '#addItemManually', function (e) {
        var mid = (new Date).getTime(),
        mcode = $('#mcode').val(),
        mname = $('#mname').val(),
        mtax = parseInt($('#mtax').val()),
        mqty = parseFloat($('#mquantity').val()),
        mdiscount = $('#mdiscount').val() ? $('#mdiscount').val() : '0',
        unit_price = parseFloat($('#mprice').val()),
        mtax_rate = {};
        if (mcode && mname && mqty && unit_price) {
            $.each(tax_rates, function () {
                if (this.id == mtax) {
                    mtax_rate = this;
                }
            });

            quitems[mid] = {"id": mid, "item_id": mid, "label": mname + ' (' + mcode + ')', "row": {"id": mid, "code": mcode, "name": mname, "quantity": mqty, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": mtax, "tax_method": 0, "qty": mqty, "type": "manual", "discount": mdiscount, "serial": "", "option":""}, "tax_rate": mtax_rate, "options":false};
            localStorage.setItem('quitems', JSON.stringify(quitems));
            loadItems();
        }
        $('#mModal').modal('hide');
        $('#mcode').val('');
        $('#mname').val('');
        $('#mtax').val('');
        $('#mquantity').val('');
        $('#mdiscount').val('');
        $('#mprice').val('');
        return false;
    });

    $(document).on('change', '#mprice, #mtax, #mdiscount', function () {
        var unit_price = parseFloat($('#mprice').val());
        var ds = $('#mdiscount').val() ? $('#mdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#mtax').val(), item_tax_method = 0;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / 100);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        $('#mnet_price').text(formatMoney(unit_price));
        $('#mpro_tax').text(formatMoney(pr_tax_val));
    });

    /* --------------------------
     * Edit Row Quantity Method
     -------------------------- */
     var old_row_qty;
     $(document).on("focus", '.rquantity', function () {
        old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        quitems[item_id].row.base_quantity = new_qty;
        if(quitems[item_id].row.unit != quitems[item_id].row.base_unit) {
            $.each(quitems[item_id].units, function(){
                if (this.id == quitems[item_id].row.unit) {
                    quitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        quitems[item_id].row.qty = new_qty;
        wappsiPreciosUnidad(quitems[item_id].row.id,item_id,new_qty);
        localStorage.setItem('quitems', JSON.stringify(quitems));
        loadItems();
    }).on('keydown', '.rquantity', function(e){
        if (e.keyCode == 13) {
            localStorage.setItem('enter_pressed_for_quantity', true);
            $(this).trigger('change');
            setTimeout(function() {
                localStorage.removeItem('enter_pressed_for_quantity');
            }, 2000);
        }
    });

    /* --------------------------
     * Edit Row Price Method
     -------------------------- */
    var old_price;
    $(document).on("focus", '.rprice', function () {
        old_price = $(this).val();
    }).on("change", '.rprice', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_price);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_price = parseFloat($(this).val()),
                item_id = row.attr('data-item-id');
        quitems[item_id].row.price = new_price;
        localStorage.setItem('quitems', JSON.stringify(quitems));
        loadItems();
    });

    $(document).on("click", '#removeReadonly', function () {
       $('#qucustomer').select2('readonly', false);
       //$('#quwarehouse').select2('readonly', false);
       return false;
    });

    setTimeout(function() {
        $('#paid_by_1 option').each(function(index, option){
            if($(option).val() == 'Credito'){
                $(option).remove();
            }
        });
    }, 850);

});
/* -----------------------
 * Misc Actions
 ----------------------- */

// hellper function for customer if no localStorage value
function nsCustomer() {
    $('#qucustomer').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "customers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function nsSupplier() {
    $('#qusupplier').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

//localStorage.clear();
/* aca loaditems */
function loadItems() {

    if (localStorage.getItem('quitems')) {
        $('#qucustomer').select2('readonly', true);
        $('#quwarehouse').select2('readonly', true);
        $('#currency').select2('readonly', true);
        $('#trm').prop('readonly', true);
        total = 0;
        count = 1;
        an = 1;
        subtotal = 0;
        product_tax = 0;
        product_tax2 = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        total_discount = 0;
        var trmrate = 1;
        var key_log = localStorage.getItem('qukeylog');
        var qucustomerspecialdiscount = localStorage.getItem('qucustomerspecialdiscount');
        var tax_exempt_customer = false;

        $("#quTable tbody").empty();
        quitems = JSON.parse(localStorage.getItem('quitems'));
        sortedItems = _.sortBy(quitems, function(o) { return [parseInt(o.order)]; });
        if (site.settings.product_order == 2) {
            sortedItems.reverse();
        }
        $('#add_sale, #edit_sale').attr('disabled', false);
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_type = item.row.type, combo_items = item.combo_items, item_qty = formatDecimal(item.row.qty), item_aqty = item.row.quantity, item_tax_method = item.row.tax_method, item_ds = item.row.discount, item_discount = 0, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var product_unit = item.row.unit, base_quantity = item.row.base_quantity;
            var unit_price = item.row.unit_price;
            var item_price = item.row.price;
            var pr_tax_rate = 0;
            var product_unit = item.row.product_unit_id_selected, base_quantity = item.row.base_quantity;

            if (item.row.product_unit_id_selected !== undefined && item.units[item.row.product_unit_id_selected] !== undefined && site.settings.validate_um_min_factor_qty == 1) {
                if ($.inArray(site.settings.prioridad_precios_producto, [7, 10, 11])) {
                    unit_selected = item.units[item.row.product_unit_id_selected];
                    if (formatDecimal(item_qty) < formatDecimal(unit_selected.operation_value)) {
                        item_qty = formatDecimal(unit_selected.operation_value);
                        command: toastr.warning('El valor ingresado está por debajo del mínimo de la unidad escogida.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "50000",
                            "extendedTimeOut": "1000",
                        });
                    }
                }
            }

            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                        item_price = unit_price+(parseFloat(this.price));
                        unit_price = item_price;
                    }
                });
            }

            if (item.row.tax_exempt_customer) {
                tax_exempt_customer = true;
            }
            
            var ds = item_ds ? item_ds : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal((((item_price) * parseFloat(pds[0])) / 100));
                } else {
                    item_discount = formatDecimal(ds);
                }
            } else {
                 item_discount = formatDecimal(ds);
            }
            unit_price -= item_discount; //precio sin IVA - Descuento

            var pr_tax = item.tax_rate;
            var pr_tax_val = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_price, item_tax_method))) {
                pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
            }

            item_price = item_tax_method == 1 ? unit_price : unit_price - pr_tax_val;

            if (key_log == 1 && qucustomerspecialdiscount == 1) {
                item_price = item.row.real_unit_price;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, (item_tax_method == 1 ? 0 : item_tax_method)))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                item_price -= pr_tax_val;
                var ds = item_ds ? item_ds : '0';
                if (ds.indexOf("%") !== -1) {
                    var pds = ds.split("%");
                    if (!isNaN(pds[0])) {
                        item_discount = formatDecimal((((item_price) * parseFloat(pds[0])) / 100));
                    } else {
                        item_discount = formatDecimal(ds);
                    }
                } else {
                     item_discount = formatDecimal(ds);
                }
                item_price -= item_discount;

                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                
                if (item_tax_method == 0) {
                    item_price -= pr_tax_val;
                }
            } else if (key_log == 2) {
                item_price = item.row.real_unit_price;
                var ds = item_ds ? item_ds : '0';
                if (ds.indexOf("%") !== -1) {
                    var pds = ds.split("%");
                    if (!isNaN(pds[0])) {
                        item_discount = formatDecimal((((item_price) * parseFloat(pds[0])) / 100));
                    } else {
                        item_discount = formatDecimal(ds);
                    }
                } else {
                     item_discount = formatDecimal(ds);
                }
                item_price -= item_discount;
                if (item_tax_method == 1) {
                    var pr_tax_val = 0;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 1))) {
                        pr_tax_val = ptax[0];
                    }
                    item_price = parseFloat(formatDecimal(item_price)) + parseFloat(formatDecimal(pr_tax_val));
                }
                pr_tax_rate = 0;
                pr_tax_val = 0;
                pr_tax_2_rate = 0;
                product_tax = 0;
                product_tax_2 = 0;
                unit_price = item.row.real_unit_price;
            } else if (key_log == 3) {
                item_price = item.row.real_unit_price;
                var ds = item_ds ? item_ds : '0';
                if (ds.indexOf("%") !== -1) {
                    var pds = ds.split("%");
                    if (!isNaN(pds[0])) {
                        item_discount = formatDecimal((((item_price) * parseFloat(pds[0])) / 100));
                    } else {
                        item_discount = formatDecimal(ds);
                    }
                } else {
                     item_discount = formatDecimal(ds);
                }
                item_price -= item_discount;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                if (item_tax_method != 1) {
                    item_price -= pr_tax_val;
                }
                pr_tax_rate = 0;
                pr_tax_val = 0;
                pr_tax_2_rate = 0;
                product_tax = 0;
                product_tax_2 = 0;
                unit_price = item.row.real_unit_price;
            }
            pr_tax_2_val = item.row.consumption_sale_tax;
            pr_tax_2_rate = item.row.consumption_sale_tax;
            var sel_opt = '';
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.name;
                }
            });

            if (othercurrency = localStorage.getItem('othercurrency')) {
                rate = localStorage.getItem('othercurrencyrate');
                if (trm = localStorage.getItem('othercurrencytrm')) {
                    trmrate = rate / trm;
                }
            }

            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
           
            tr_html = '<td>'+
                        '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                        '<input name="ignore_hide_parameters[]" type="hidden" class="ignore_hide_parameters" value="' + item.row.ignore_hide_parameters + '">'+
                        '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                        '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                        '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                        '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                        '<input name="product_seller_id[]" type="hidden" value="'+ (item.row.seller_id !== undefined && item.row.seller_id ? item.row.seller_id : $('#quseller').val()) +'">'+
                        '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name +(sel_opt != '' ? ' ('+sel_opt+')' : '')+'</span> '+
                        '<i class="pull-right fa fa-edit tip pointer edit" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i>'+
                      '</td>';
            if (site.settings.product_serial == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'">'+
                            '</td>';
            }
            tr_html += '<td class="text-right">'+
                            '<span class="text-right sprice" id="sprice_' + row_no + '">' + (othercurrency ? formatDecimal(trmrate * item_price, 6) : formatMoney(trmrate * (item_price + item_discount))) + '</span>'+
                        '</td>';
            if ((site.settings.product_discount == 1 && allow_discount == 1) || item_discount) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '">'+
                                '<input class="form-control input-sm" name="product_discount_val[]" type="hidden" value="' + formatDecimal(item_discount * item_qty) + '">'+
                                // '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + formatMoney(trmrate * (0 - (item_discount * item_qty))) + '</span>'+
                                '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' +get_percentage_from_amount(item_ds, (item_price + item_discount))+ '</span>'+
                            '</td>';
            }
            tr_html += '<td class="text-right">'+
                            '<input class="form-control input-sm text-right rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '">'+
                            '<input class="ruprice" name="unit_price[]" type="hidden" value="' + (parseFloat(item.row.unit_price)) + '">'+
                            '<input class="realuprice" name="real_unit_price[]" type="hidden" value="' + (parseFloat(item.row.real_unit_price)) + '">'+
                            '<span class="text-right sprice" id="sprice_' + row_no + '">' + (othercurrency ? formatDecimal(trmrate * item_price, 6) : formatMoney(trmrate * item_price)) + '</span>'+
                        '</td>';

            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax.id : 1) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_rate[]" type="hidden" value="' + (key_log == '1' ? pr_tax_rate : 1) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax_val : 0) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_val[]" type="hidden" value="' + (key_log == '1' ? pr_tax_rate : 0) + '">'+
                                // '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (parseFloat(pr_tax_rate) != 0 ? '(' + formatDecimal(pr_tax_rate) + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_val * item_qty)) + '</span>'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (pr_tax_rate) +
                            '</td>';
            }
            if (site.settings.ipoconsumo == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_2[]" type="hidden" id="product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_rate : 1) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_2_rate[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_rate : 1) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_val : 0) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_val_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_rate : 0) + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '_2">' + formatMoney((pr_tax_2_rate * item_qty) * trmrate) +
                            '</td>';
            }
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">'+
                                '<span class="text-right">' + formatMoney(formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val))) * trmrate)) + '</span>'+
                            '</td>';
            }
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatDecimal(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + base_quantity + '">'+
                            '<input name="product_aqty[]" type="hidden" class="item_aqty" value="' + item_aqty + '">'+
                        '</td>';
            // tr_html += '<td class="text-right">'+
            //                 '<span class="text-right ssubtotal2" id="subtotal2_' + row_no + '">' + formatMoney(trmrate * ((parseFloat(item_price)) * parseFloat(item_qty))) + '</span>'+
            //             '</td>';
            
            tr_html += '<td class="text-right">'+
                            '<span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty))) + '</span>'+
                        '</td>';
            tr_html += '<td class="text-center"><i class="fa fa-times tip pointer qudel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            if (is_edit) {
                newTr.prependTo("#quTable");
            } else {
                newTr.appendTo("#quTable");
            }
            if (item.preferences_text !== null && item.preferences_text !== undefined) {
                var newTr2 = $('<tr class="tr_preferences" style="font-size:112%;"></tr>');
                tr2_html = '<td colspan="5">'+
                            '<input type="hidden" name="product_preferences_text[]" value="'+JSON.stringify(item.preferences_selected)+'">' +
                            item.preferences_text +
                          '</td>';
                newTr2.html(tr2_html);
                newTr2.appendTo("#quTable");
            }
            subtotal += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
            product_tax += formatDecimal(((parseFloat(pr_tax_val)) * parseFloat(item_qty)));
            product_tax2 += formatDecimal(((parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            product_discount += formatDecimal(((parseFloat(item_discount)) * parseFloat(item_qty)));
            total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            count += parseFloat(item_qty);
            an++;
            if (item_type == 'standard' && item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item_option && base_quantity > this.quantity) {
                        $('#row_' + row_no).addClass('danger');
                        if(site.settings.overselling != 1) { $('#add_sale, #edit_sale').attr('disabled', true); }
                    }
                });
            } else if(item_type == 'standard' && base_quantity > item_aqty) {
                $('#row_' + row_no).addClass('danger');
            } else if (item_type == 'combo') {
                if(combo_items === false) {
                    $('#row_' + row_no).addClass('danger');
                } else {
                    $.each(combo_items, function() {
                       if(parseFloat(this.quantity) < (parseFloat(this.qty)*base_quantity) && this.type == 'standard') {
                           $('#row_' + row_no).addClass('danger');
                       }
                   });
                }
            }

        });

        var col = 1;
        var tfoot = '<tr id="tfoot" class="tfoot active"><th colspan="'+col+'">Total</th><th class="text-right">' + formatMoney(subtotal - product_discount) + '</th>';
        if ((site.settings.product_discount == 1 && allow_discount == 1) || product_discount) {
            tfoot += '<th class="text-right">'+formatMoney(trmrate * product_discount)+'</th>';
        }
        tfoot += '<th class="text-right">'+formatMoney(trmrate * subtotal)+'</th>';
        if (site.settings.tax1 == 1) {
            tfoot += '<th class="text-right">'+formatMoney(trmrate * product_tax)+'</th>';
        }
        if (site.settings.ipoconsumo == 1) {
            tfoot += '<th class="text-right">'+formatMoney(trmrate * product_tax2)+'</th>';
        }
        tfoot += '<th class="text-right">'+formatMoney(trmrate * total)+'</th><th class="text-right">'+formatDecimal(count - 1)+'</th><th class="text-right">'+formatMoney(trmrate * total)+'</th><th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th></tr>';
        $('#quTable tfoot').html(tfoot);

        // Order level discount calculations
        if (qudiscount = localStorage.getItem('qudiscount')) {
            var ds = qudiscount;
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    order_discount = formatDecimal((((total) * parseFloat(pds[0])) / 100));
                } else {
                    order_discount = formatDecimal(ds);
                }
            } else {
                order_discount = formatDecimal(ds);
            }
        }

        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (qutax2 = localStorage.getItem('qutax2')) {
                $.each(tax_rates, function () {
                    if (this.id == qutax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        }
                        if (this.type == 1) {
                            invoice_tax = formatDecimal((((total - order_discount) * this.rate) / 100));
                        }
                    }
                });
            }
        }
        total_discount = parseFloat(order_discount + product_discount);
        // Totals calculations after item addition
        var gtotal = parseFloat(((total + invoice_tax) - order_discount) + shipping);
        $('#total').text(formatMoney(trmrate * total));
        $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
        $('#total_items').val((parseFloat(count) - 1));
        $('#tds').text(formatMoney(trmrate * order_discount));
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(trmrate * invoice_tax));
        }
        $('#tship').text(formatMoney(trmrate * shipping));
        $('#gtotal').text(formatMoney(trmrate * gtotal));
        lock_submit_by_min_sale_amount = false;
        if (gtotal > 0 && localStorage.getItem('qubiller') && billers_data[localStorage.getItem('qubiller')].min_sale_amount > 0) {
            $('.text_min_sale_amount').text(formatMoney(billers_data[localStorage.getItem('qubiller')].min_sale_amount));
            if (gtotal < billers_data[localStorage.getItem('qubiller')].min_sale_amount) {
                $('.error_min_sale_amount').fadeIn();
                $('#add_quote').prop('disabled', true);
                lock_submit_by_min_sale_amount = true;
            } else if (gtotal >= billers_data[localStorage.getItem('qubiller')].min_sale_amount) {
                $('.error_min_sale_amount').fadeOut();
                $('#add_quote').prop('disabled', false);
            }
        }
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        if (count > 1) {
            $('#qucustomer').select2("readonly", true);
            $('#qucustomerbranch').select2("readonly", true);
        }
        if (site.settings.product_order == 2) {
            an = 2;
        }
        if (tax_exempt_customer && site.settings.enable_customer_tax_exemption == 1) {
            $('#tax_exempt_customer').val(1);
        }
        set_page_focus(an);
    }
}

/* -----------------------------
 * Add Quotation Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
 function add_invoice_item(item) {

    if (count == 1) {
        quitems = {};
        if (   $('#quwarehouse').val()
                && $('#qucustomer').val()
                && $('#qucustomerbranch').val()
                && (($('#currency').val() == site.settings.default_currency && !$('#trm').val()) || ($('#currency').val() != site.settings.default_currency && $('#trm').val()))
            ) {
            $('#qucustomer').select2("readonly", true);
            $('#quwarehouse').select2("readonly", true);
        } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }
    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (quitems[item_id]) {
        var new_qty = parseFloat(quitems[item_id].row.qty) + 1;
        quitems[item_id].row.base_quantity = new_qty;
        if(quitems[item_id].row.unit != quitems[item_id].row.base_unit) {
            $.each(quitems[item_id].units, function(){
                if (this.id == quitems[item_id].row.unit) {
                    quitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        quitems[item_id].row.qty = new_qty;
    } else {
        quitems[item_id] = item;
    }
    quitems[item_id].order = new Date().getTime();
    localStorage.setItem('quitems', JSON.stringify(quitems));
    wappsiPreciosUnidad(quitems[item_id].row.id, item_id, quitems[item_id].row.qty);
    if (quitems[item_id].row.quantity <= 0) {
        command: toastr.warning('El producto no tiene cantidades en existencia', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
    }
    loadItems();
    if (site.pos_settings.show_variants_and_preferences == 1 && quitems[item_id].preferences) {
        setTimeout(function() {
            $('#myModal').modal({
                    remote: site.base_url + 'pos/product_preferences_selection/' + quitems[item_id].row.id + "/" + item_id+"/quitems", 
                    backdrop: 'static',
                    keyboard: false
                });
            $('#myModal').modal('show');
        }, 850);
    }
    return true;
}

$('#currency').on('change', function(){
    default_currency = site.settings.default_currency;
    actual_currency = $(this).val();
    actual_currency_rate = $('#currency option:selected').data('rate');
    if (default_currency != actual_currency) {
        $('.trm-control').css('display', '');
        $('#trm').prop('required', true);
        if (othercurrencytrm = localStorage.getItem('othercurrencytrm')) {
            $('#trm').val(othercurrencytrm);
        }
        localStorage.setItem('othercurrency', true);
        localStorage.setItem('othercurrencycode', actual_currency);
        localStorage.setItem('othercurrencyrate', actual_currency_rate);
        $('.currency').text(actual_currency);
    } else {
        $('.trm-control').css('display', 'none');
        $('#trm').prop('required', false);
        localStorage.removeItem('othercurrency');
        localStorage.removeItem('othercurrencycode');
        localStorage.removeItem('othercurrencyrate');
        localStorage.removeItem('othercurrencytrm');
        $('#trm').val('');
        $('.currency').text(default_currency);
    }
    loadItems();
});

$('#trm').on('change', function(){
    trm = $(this).val();

    if(trm != '' && trm != 0) {
        localStorage.setItem('othercurrencytrm', trm);
    } else {
        localStorage.removeItem('othercurrencytrm');
    }

    loadItems();

});

if (currencycode = localStorage.getItem('othercurrencycode')) {
    $('#currency').val(currencycode).trigger('change');
}

function wappsiPreciosUnidad(id,item_id,new_qty) {
    var biller_price_group = $('#qubiller option:selected').data('pricegroupdefault');
    var customer = $('#qucustomer').val();
    var biller = $('#qubiller').val();
    var address_id = $('#qucustomerbranch').val();
    var unit_price_id = null;
    if (site.settings.precios_por_unidad_presentacion == 2 || site.settings.prioridad_precios_producto == 11 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 7) {
        if (item_selected = quitems[item_id]) {
            unit_price_id = item_selected.row.product_unit_id_selected;
        }
    }
    var datos = {
                    id : id,
                    new_qty : new_qty,
                    price_group : biller_price_group,
                    customer : customer,
                    address_id : address_id,
                    biller : biller,
                    prioridad : site.settings.prioridad_precios_producto,
                    unit_price_id : unit_price_id
                };

    $.ajax({
        type: 'get',
        url: site.base_url+"sales/preciosUnidad",
        dataType: "json",
        data: datos,
        success: function (data) {
            $(this).removeClass('ui-autocomplete-loading');
            if (data !== null && data[0] !== null && data[0] != 0) {
                var valor = data[0];
                if (quitems[item_id]) {
                    quitems[item_id].row.real_unit_price = valor;
                    quitems[item_id].row.unit_price = valor;
                    if (site.settings.precios_por_unidad_presentacion == 2) {
                        quitems[item_id].row.unit_price_quantity = data[1];
                        if (data[1] !== undefined) {
                            quitems[item_id].row.real_unit_price = valor / data[1];
                            quitems[item_id].row.unit_price = valor / data[1];
                        }
                    }
                    if (data[2] !== undefined) {
                        quitems[item_id].row.discount = data[2];
                    }
                    if (data[3] !== undefined) {
                        quitems[item_id].row.tax_rate = data[3];
                        quitems[item_id].tax_rate = data[4];
                        quitems[item_id].row.tax_exempt_customer = true;
                    }
                }
            } else {
                quitems[item_id].row.real_unit_price = quitems[item_id].row.real_unit_price;
            }
            quitems[item_id].row.qty = new_qty;
            localStorage.setItem('quitems', JSON.stringify(quitems));
            $('#payment').prop('disabled', true);
            loadItems();
        }
    });

}

// Termina wappsiPreciosUnidad
function validate_key_log(){
    var documnet_type_id = $('#document_type_id').val();
    if (documnet_type_id != '') {
        $.ajax({
            type: "get", async: false,
            url: site.base_url+"sales/validate_key_log/" + $('#document_type_id').val(),
            dataType: "json",
            success: function (data) {
                localStorage.setItem('qukeylog', data);
                if (data != "1") {
                    $('#qubiller').select2('readonly', true);
                    $('#document_type_id').select2('readonly', true);
                    command: toastr.warning('Tipo de documento cambiado, refresque o reinicie si quiere volver a uno normal.', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                }
                loadItems();
            }
        });
    }
}

function customer_special_discount(){
    var customer_id = $('#qucustomer').val();
    $.ajax({
        url : site.base_url+"customers/get_customer_special_discount/"+customer_id,
        dataType : "JSON"
    }).done(function(data){
        localStorage.setItem('qucustomerspecialdiscount', data.special_discount);
        if (data.special_discount == 1) {
                loadItems();
                command: toastr.warning('Cliente con descuento especial asignado', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        }
    });
}

$(document).on('change', '#qu_payment_status', function(){
    if ($(this).val() == 1) {
        $('.div_paid_by').fadeIn();
        $('.div_payment_term').fadeOut();
    } else if ($(this).val() == 0) {
        $('.div_paid_by').fadeOut();
        $('.div_payment_term').fadeIn();
    }
});

function set_biller_warehouses_related(){
    warehouses_related = billers_data[$('#qubiller').val()].warehouses_related;
    if (warehouses_related) {
        $('#quwarehouse option').each(function(index, option){
            $(option).prop('disabled', false);
            if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                $(option).prop('disabled', true);
            }
        });
        setTimeout(function() {
            $('#quwarehouse').select2();
        }, 850);
    }
}