<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['affiliates'] = "Afiliados";
$lang['add_affiliate']	= "Agregar Afiliado";
$lang['edit_affiliate'] = "Editar Afiliado";
$lang['affiliate_list'] = "Lista Afiliados";
$lang['delete_affiliate'] 	= "Eliminar Afiliado";
$lang['enable_affiliate']  = "Habilitar Afiliado";
$lang['disable_affiliate'] = "Inhabilitar Afiliado";
$lang['delete_affiliates'] = "Eliminar Afiliados";
$lang['first_name_affiliate'] = "Primer nombre";
$lang['second_name_affiliate'] = "Segundo nombre";
$lang['first_lastname_affiliate'] = "Primer apellido";
$lang['second_lastname_affiliate'] = "Segundo apellido";

$lang['validation_message_required'] = "El campo %s es obligatorio.";
$lang['validation_message_valid_email'] = "El correo electrónico no es vállido.";

$lang['disabled_affiliate'] = "Afiliado deshabilitado.";
$lang['enabled_affiliate'] = "Afiliado habilitado.";
$lang['added_affiliate'] = "Afiliado agregado correctamente.";
$lang['affiliate_removed'] = "Afiliado eliminado correctamente.";
$lang['updated_affiliate'] = "Afiliado actualizado correctamente.";
$lang['enabled_not_affiliate'] = "Afiliado no ha sido habilitado.";
$lang['affiliates_removed'] = "Afiliados eliminados correctamente.";
$lang['disabled_not_affiliate'] = "Afiliado no ha sido deshabilitado.";
$lang['affiliate_not_removed'] = "No fue posible emilinar el Afiliado.";
$lang['existing_affiliate_sales'] = "No fue posible emilinar el Afiliado, debido a que existen ventas asociadas al mismo.";
$lang['no_affiliate_selected'] = "No se ha seleccionado ningún Afiliado.";
$lang['existing_affiliate_contracts'] = "No fue posible emilinar el Afiliado, debido a que existe un contrato laboral asociado al mismo";