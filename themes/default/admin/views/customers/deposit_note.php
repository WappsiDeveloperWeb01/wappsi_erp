<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    @media print {
        .title{
          display: initial;
        }
        table > tbody > tr > td {
          font-size: 80%;
        }
      }

      .modal-dialog span {
        font-size: 120%;
      }
</style>

<div class="modal-dialog no-modal-header modal-lg">
    <div class="modal-content">
        <div class="modal-body print" id="anticipo_imprimir">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <a class="btn btn-xs btn-default no-print pull-right" href="<?= admin_url('customers/deposit_note/').$deposit->id.'/1' ?>" target="blank"> <i class="fa fa-print"></i> <?= lang('print') ?></a>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-xs-6">
                            <img src="<?= base_url().'assets/uploads/logos/' . $biller->logo; ?>" alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                        </div>
                        <div class="col-xs-6">
                            <span><b><?= lang('proof_of_deposit') ?></b></span><br>
                            <span><?= lang('reference_no') ?> : <?= $deposit->reference_no ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12" style="padding-top: 23px;">
                            <span><b><?= lang('customer') ?></b> :<br> <?= $customer->company ? $customer->company : $customer->name ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            <span><b><?= lang('vat_no') ?></b> :<br> <?= $customer->vat_no ?></span>
                        </div>
                        <div class="col-xs-3">
                            <span><b><?= lang('date') ?></b> :<br> <?= $deposit->date ?></span>
                        </div>
                        <div class="col-xs-3">
                            <span><b><?= lang('paid_by') ?></b> :<br> <?= lang($deposit->paid_by) ?></span>
                        </div>
                        <div class="col-xs-3">
                            <span><b><?= lang('date') ?></b> :<br> <?= $deposit->date ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            <span><b><?= lang('value') ?></b> :<br> <?= $this->sma->formatMoney($deposit->amount) ?></span>
                        </div>
                        <div class="col-xs-3">
                            <span><b><?= lang('applied_valued') ?></b> :<br> <?= $this->sma->formatMoney($deposit->amount - $deposit->balance) ?></span>
                        </div>
                        <div class="col-xs-3">
                            <span><b><?= lang('balance') ?></b> :<br> <?= $this->sma->formatMoney($deposit->balance) ?></span>
                        </div>
                        <?php if (isset($cost_center) && $cost_center): ?>
                        <div class="col-xs-3">
                            <span><b><?= lang('cost_center') ?></b> :<br> <?= $cost_center->name ?> ( <?= $cost_center->code ?> )</span>
                        </div>
                        <?php endif ?>
                    </div>
                    <div class="clearfix"></div>

                    <?php 
                    if ($payments) {
                        $concepto = false;
                        foreach ($payments as $key_pmnt => $payment) {
                            if ($payment->sale_id == NULL) {
                                if (!$concepto || ($concepto && $concepto->reference_no != $payment->reference_no)) {
                                    if($concepto && $concepto->reference_no != $payment->reference_no){
                                        $concepto->sale_reference_no = 'Otros conceptos';
                                        $payments[] = $concepto;
                                        $concepto = false;
                                    }
                                    $concepto = $payment;
                                    unset($payments[$key_pmnt]);
                                } else if ($concepto && $concepto->reference_no == $payment->reference_no) {
                                    $concepto->amount += $payment->amount;
                                    unset($payments[$key_pmnt]);
                                }
                            }
                        }
                        if ($concepto) {
                            $concepto->sale_reference_no = 'Otros conceptos';
                            $payments[] = $concepto;
                            $concepto = false;
                        }
                    }
                     ?>

                    <?php if ($payments): ?>
                        <div class="row" style="padding-top: 4%">
                            <div class="col-md-12">
                                <h3><?= lang('payments_affects_to_deposit') ?></h3>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><?= lang('reference_no') ?></th>
                                            <th><?= lang('sale_reference_no') ?></th>
                                            <th><?= lang('date') ?></th>
                                            <th><?= lang('value') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($payments as $pmnt): ?>
                                            <tr>
                                                <td><?= $pmnt->reference_no ?></td>
                                                <td><?= $pmnt->sale_reference_no ?></td>
                                                <td><?= $pmnt->date ?></td>
                                                <td><?= $this->sma->formatMoney($pmnt->amount - $pmnt->rete_fuente_total - $pmnt->rete_iva_total - $pmnt->rete_ica_total - $pmnt->rete_other_total) ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th><?= lang('reference_no') ?></th>
                                            <th><?= lang('sale_reference_no') ?></th>
                                            <th><?= lang('date') ?></th>
                                            <th><?= lang('value') ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div class="row">
                <div class="col-sm-4 pull-left">
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p style="border-bottom: 1px solid #666;">&nbsp;</p>
                    <p><?= lang("stamp_sign"); ?></p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>