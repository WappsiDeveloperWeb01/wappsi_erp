<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Impresión</title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
        </style>
    </head>

    <body>
    <div id="wrapper">
        <div id="receiptData">
            <div>
                <div class="text-center">
                    <h4>Orden de preparación</h4>
                    <h2><?= $preparation_area_data->name ?></h2>
                    <?php if ($locator): ?>
                        <h2><?= lang('locator_details') ?> : <?= $locator; ?> </h2>
                    <?php endif ?>
                    <h4><?= date('Y-m-d H:i:s') ?></h4>
                </div>
                    <table class="table">
                        <tfoot>
                            <tr>
                                <th><?= lang('product_name') ?></th>
                                <th style="text-align: right;"><?= lang('quantity') ?></th>
                            </tr>
                        </tfoot>
                        <tbody id="pa_products_table">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>

    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
    <script type="text/javascript">
        site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url(), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var pa_items = JSON.parse(localStorage.getItem('pa_items'));
            var pa_items = pa_items[<?= $preparation_area_id ?>];
            $('#loading').fadeIn();
            var html = '';
            $.each(pa_items, function(index, product){
                html += '<tr>'+
                        '<td> '+product.product_name+" "+(product.preferences !== null ? product.preferences : '')+' </td>'+
                        '<td style="text-align: right;"> '+formatDecimal(product.quantity, 2)+' </td>'+
                        '</tr>';
            })
            $('#pa_products_table').html(html);
            window.print();
        });
        function formatDecimal(x, d) {
            if (!d) { d = parseFloat(site.settings.decimals); if (site.settings.rounding == 1) { d += 2; } } else { d = parseFloat(d)}
            return parseFloat(accounting.formatNumber(x, d, '', '.'));
        }
    </script>
</body>
</html>
