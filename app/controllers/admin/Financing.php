<?php

use FontLib\Table\Type\head;

defined('BASEPATH') or exit('No direct script access allowed');

class Financing extends MY_Controller
{
    const PENDING = 0;
    const PAID = 1;

    public $documentTypeElectronicBilling;
    public $amounttoPay;
    public $paidBy;
    public $amount;

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if ($this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('financing', $this->Settings->user_language);

        $this->load->admin_model('FinancingInstallmentsPayment_model');
        $this->load->admin_model('FinancingInstallment_model');
        $this->load->admin_model('CommissionPercentage_model');
        $this->load->admin_model('Electronic_billing_model');
        $this->load->admin_model('DocumentsTypes_model');
        $this->load->admin_model('PaymentMethods_model');
        $this->load->admin_model('UserActivities_model');
        $this->load->admin_model('Financing_model');
        $this->load->admin_model('Companies_model');
        $this->load->admin_model('TaxRates_model');
        $this->load->admin_model('Products_model');
        $this->load->admin_model('Deposits_model');
        $this->load->admin_model('Sales_model');

        $this->validateConfigurationData();
    }

    public function index()
    {
        $this->data["advancedFiltersContainer"] = false;
        $this->page_construct('financing/index', ['page_title' => lang('financing_list')], $this->data);
    }

    public function getFinancing()
    {
        $action = '<div class="btn-group text-left">
            <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                <i class="fas fa-ellipsis-v fa-lg"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li class="divider"></li>
                <li><label style="padding-left: 20px; padding-top: 10px">Otras acciones</label></li>
                <li class="link_edit_sale"></li>
                <li class="print_document"></li>
            </ul>
        </div>';

        $tabFilterData = $this->input->post("tabFilterData");
        $optionFilter = $this->input->post('optionFilter') != 'false' ? $this->input->post('optionFilter') : 'all';
        if (!empty($tabFilterData)) {
            $all      = $this->manageResponseRequest($action, false);
            $toNotify = $this->manageResponseRequest($action, "to_notify");
            $notified = $this->manageResponseRequest($action, 'notified');
            $toReport = $this->manageResponseRequest($action, 'to_report');
            $reported = $this->manageResponseRequest($action, 'reported');

            echo json_encode([
                "all"     => $all,
                "toNotify"=> $toNotify,
                "notified"=> $notified,
                "toReport"=> $toReport,
                "reported"=> $reported,
            ]);
        } else {
            echo $this->manageResponseRequest($action, $optionFilter);
        }
    }

    public function manageResponseRequest($action, $optionFilter)
    {
        $this->load->library('datatables');

        $tabFilterData = $this->input->post("tabFilterData");

        $this->datatables->select("{$this->db->dbprefix('credit_financing')}.id AS id,
            credit_no,
            b.name AS billerName,
            c.vat_no,
            c.name AS customerName,
            credit_date,
            ({$this->db->dbprefix('credit_financing')}.capital_amount + {$this->db->dbprefix('credit_financing')}.current_interest_amount + {$this->db->dbprefix('credit_financing')}.interest_tax) AS intialValue,
            IF(DATEDIFF(CURDATE(), credit_date) > 10,
                1,
                0
            ) AS diffDays,
            notified_credit,
            credit_status");
        $this->datatables->from('credit_financing');
        $this->datatables->join('companies b', "b.id = {$this->db->dbprefix('credit_financing')}.biller_id", 'inner');
        $this->datatables->join('companies c', "c.id = {$this->db->dbprefix('credit_financing')}.customer_id", 'inner');
        $this->datatables->where("({$this->db->dbprefix('credit_financing')}.capital_amount + {$this->db->dbprefix('credit_financing')}.current_interest_amount + {$this->db->dbprefix('credit_financing')}.interest_tax) > ", 0);

        if ($optionFilter == 'to_notify') {
            $this->datatables->where("DATEDIFF(CURDATE(), credit_date) > ", $this->Settings->days_for_notification);
            $this->datatables->where("notified_credit", 0);
            $this->datatables->where("credit_status", 1);
        } else if ($optionFilter == 'notified') {
            $this->datatables->where("notified_credit", 1);
            $this->datatables->where("credit_status", 1);
        } else if ($optionFilter == 'to_report') {
            $this->datatables->where("DATEDIFF(CURDATE(), date_notified) > ", $this->Settings->days_for_report);
            $this->datatables->where("credit_status", 1);
        } else if ($optionFilter == 'reported') {
            $this->datatables->where("credit_status", 2);
        }

        if (!empty($tabFilterData)) {
            return $this->datatables->get_result_num_rows();
        } else {
            return $this->datatables->generate();
        }
    }

    private function validateConfigurationData()
    {
        if ($this->Settings->financing_module == YES) {
            if (empty($this->Settings->concepts_for_current_interest) || empty($this->Settings->concepts_for_default_interest)) {
                $this->session->set_flashdata('error', lang('unfilled_configuration_data'));
                admin_redirect('system_settings');
            }
        } else {
            $this->session->set_flashdata('error', lang('inactive_financing_module'));
            admin_redirect('system_settings');
        }
    }

    public function creditCustomerSuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        $result = $this->Financing_model->getCreditsCustomerSuggestions($term, $limit);
        $this->sma->send_json($result);
    }

    public function paymentFinancingFee()
    {
        $this->sma->checkPermissions("make_collections");

        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['post'] = (!empty($this->session->flashdata('post'))) ? $this->session->flashdata('post') : null;
        $this->page_construct('financing/payment_financing_fee', ['page_title' => lang('payment_financing_fee')], $this->data);
    }

    public function getCustomerCreditBranchAjax()
    {
        $customerId = $this->input->get('customerId');
        $billers = $this->Financing_model->getCustomerCreditBranch(['customer_id' => $customerId]);
        echo json_encode($billers);
    }

    public function getCreditsByCustomerBranchAjax()
    {
        $customerId = $this->input->get('customerId');
        $billerId = $this->input->get('billerId');
        $credits = $this->Financing_model->getCreditsByCustomerBranch(['biller_id' => $billerId, 'customer_id' => $customerId]);
        echo json_encode($credits);
    }

    public function searchCreditsAjax()
    {
        $customer = $this->input->post("creditCustomer");

        $data = [
            'credit_financing.customer_id'  => $customer
        ];

        $customerbranch = $this->input->post("customerbranch");
        if (!empty($customerbranch)) {
            $data['biller_id'] = $customerbranch;
        }
        $documentTypeId = $this->input->post("documentTypeId");
        if (!empty($documentTypeId)) {
            $data['document_type_id'] = $documentTypeId;
        }
        $creditId = $this->input->post("credit");
        if (!empty($creditId)) {
            $data['credit_financing.id'] = $creditId;
        }

        $deposits = $this->getCustomerAdvances($customer);

        $credits  = $this->Financing_model->getCreditInstallments($data);
        $credits  = $this->subtractPayments($credits);
        $credits  = $this->calculatedaysExpiredIfPaymentsExisting($credits);
        $credits  = $this->calculateDefaultInterest($credits);

        $reportedCredits = $this->reportedCreditFilters($credits);

        $response = [
            "existingReportedCredits"   => $reportedCredits->existing,
            "showReportedCredits"       => $reportedCredits->show,
            "deposits"                  => $deposits,
            "credits"                   => $credits
        ];

        echo json_encode($response);
    }

    private function subtractPayments($credits)
    {
        if (!empty($credits)) {
            foreach ($credits as $credit) {
                $payments = $this->FinancingInstallmentsPayment_model->get(["installment_id" => $credit->installmentId]);

                if (!empty($payments)) {
                    foreach ($payments as $payment) {
                        $credit->currentInterestAmount -= $payment->current_interest_amount;
                        $credit->capitalAmount -= $payment->capital_amount;
                        $credit->interestTax -= $payment->interest_tax;

                        $credit->installmentAmount = ($credit->interestTax + $credit->currentInterestAmount + $credit->capitalAmount);
                    }
                }
            }
        }

        return $credits;
    }

    private function calculatedaysExpiredIfPaymentsExisting($credits)
    {
        if (!empty($credits)) {
            foreach ($credits as $credit) {
                $payments = $this->FinancingInstallmentsPayment_model->get(["installment_id" => $credit->installmentId]);
                if (!empty($payments)) {
                    $credit->daysExpired = 0;
                    $datePayments = new DateTime($payments[0]->paymentDate);
                    $dateInstallment = new DateTime($credit->installmentDueDate);

                    if ($datePayments > $dateInstallment) {
                        $today = new DateTime(date("Y-m-d"));
                        $diff = $datePayments->diff($today);
                        if ($diff->days > 0) {
                            $credit->daysExpired = $diff->days;
                        }
                    }
                }
            }
        }

        return $credits;
    }

    private function calculateDefaultInterest($credits)
    {
        if (!empty($credits)) {
            $product = $this->Products_model->getProductByID($this->Settings->concepts_for_default_interest);
            $taxRate = $this->TaxRates_model->find($product->tax_rate);

            foreach ($credits as $credit) {
                $credit->installmentAmountTotal = $credit->installmentAmount;

                if ($credit->daysExpired > 0) {
                    $defaultInterestMonthly = $credit->capitalAmount * ($this->Settings->default_interest_percentage_credit_financing / 100);
                    $defaultInterestDaily = $defaultInterestMonthly / 30;
                    $defaultInterest = $defaultInterestDaily * $credit->daysExpired;

                    $credit->defaultInterest = $this->sma->formatDecimal($defaultInterest, 4);
                    $credit->defaultInterestTax = $this->sma->formatDecimal(($defaultInterest * ($taxRate->rate / 100)), 4);

                    $credit->installmentAmountTotal += ($credit->defaultInterest + $credit->defaultInterestTax);
                }
            }
        }

        return $credits;
    }

    private function reportedCreditFilters($credits)
    {
        $existing = false;
        $show = true;

        if (!empty($credits)) {
            foreach ($credits as $credit) {
                if ($credit->creditStatus == 2) {
                    $existing = true;
                }
            }
        }

        if (!($this->Owner || $this->Admin)) {
            if ($this->GP["financing-make_collections_reported"] == 0) {
                $show = false;
            }
        }

        return (object) [
            "existing"   => $existing,
            "show"       => $show
        ];
    }

    private function getCustomerAdvances($customerId)
    {
        $total    = 0;
        $deposits = $this->Companies_model->getDepositsWithBalance($customerId);

        if (!empty($deposits)) {
            foreach ($deposits as $deposit) {
                $total += $deposit->balance;
            }
        }

        return $total;
    }

    public function add()
    {
        $paymentAgreement = false;

        $this->validateConfigurationDataToSales();

        $biller = $this->input->post('biller');
        $paidBy = $this->input->post('paid_by');
        $changes = $this->input->post("change");
        $capital = $this->input->post('capital');
        $notes = $this->input->post("paymentNote");
        $installmentIds = $this->input->post("installmentId");
        $totalsReceived = $this->input->post("totalReceived");
        $amountsPaymentMethods = $this->input->post('amounPaid');
        $documentTypeId = $this->input->post("document_type_id");
        $daysExpiredInstallments = $this->input->post("daysExpired");
        $installmentAmounts = $this->input->post("totalInstallmentAmount");
        $currentInterestTaxInstallments = $this->input->post("interestTax");
        $defaultInterestInstallments = $this->input->post("defaultInterest");
        $currentInterestInstallments = $this->input->post("currentInterestAmount");
        $defaultInterestTaxInstallments = $this->input->post("defaultInterestTax");
        $documentType = $this->DocumentsTypes_model->getReference($documentTypeId);

        if ($this->input->post("vat_currente_interest_pa")) {
            $vatCurrenteInterestPA = $this->sma->formatNumberMask($this->input->post("vat_currente_interest_pa"));
        }
        if ($this->input->post("vat_default_interest_pa")) {
            $vatDefaultInterestPA = $this->sma->formatNumberMask($this->input->post("vat_default_interest_pa"));
        }
        if ($this->input->post("currente_interest_pa")) {
            $currenteInterestPA = $this->sma->formatNumberMask($this->input->post("currente_interest_pa"));
        }
        if ($this->input->post("default_interest_pa")) {
            $defaultInterestPA = $this->sma->formatNumberMask($this->input->post("default_interest_pa"));
        }
        if ($this->input->post("capital_pa")) {
            $capitalPA = $this->sma->formatNumberMask($this->input->post("capital_pa"));
        }

        if ($this->input->post("vat_procredito_commission")) {
            $vatProcreditoCommission = $this->sma->formatNumberMask($this->input->post("vat_procredito_commission"));
        }
        if ($this->input->post("vat_procredito_interest")) {
            $vatProcreditoInterest = $this->sma->formatNumberMask($this->input->post("vat_procredito_interest"));
        }
        if ($this->input->post("procredito_commission")) {
            $procreditoCommission = $this->sma->formatNumberMask($this->input->post("procredito_commission"));
        }
        if ($this->input->post("procredito_interest")) {
            $procreditoInterest = $this->sma->formatNumberMask($this->input->post("procredito_interest"));
        }
        if ($this->input->post("late_fee_discount")) {
            $lateFeeDiscount = $this->sma->formatNumberMask($this->input->post("late_fee_discount"));
        }

        $this->validateConfigurationDataToSales();

        if (isset($capitalPA) && $capitalPA > 0) {
            $paymentAgreement = true;
            $numberOfInstallments = count($installmentIds);

            $defaultInterestPA      /= $numberOfInstallments;
            $vatDefaultInterestPA   /= $numberOfInstallments;
            $currenteInterestPA     /= $numberOfInstallments;
            $vatCurrenteInterestPA  /= $numberOfInstallments;

            foreach ($installmentIds as $id => $checked) {
                $defaultInterestInstallments[$id] = $defaultInterestPA;
                $defaultInterestTaxInstallments[$id] = $vatDefaultInterestPA;
                $currentInterestInstallments[$id] = $currenteInterestPA;
                $currentInterestTaxInstallments[$id] = $vatCurrenteInterestPA;

                $installmentAmounts[$id] = $capital[$id] + $defaultInterestPA + $vatDefaultInterestPA + $currenteInterestPA + $vatCurrenteInterestPA;
            }
        }

        if (isset($vatProcreditoCommission)) {
            if (($vatProcreditoCommission + $vatProcreditoInterest + $procreditoCommission + $procreditoInterest)) {
                $vatProcreditoCommission /= $numberOfInstallments;
                $vatProcreditoInterest /= $numberOfInstallments;
                $procreditoCommission /= $numberOfInstallments;
                $procreditoInterest /= $numberOfInstallments;

                foreach ($installmentIds as $id => $checked) {
                    $procreditoCommissions[$id] = $procreditoCommission;
                    $vatProcreditoCommissions[$id] = $vatProcreditoCommission;
                    $procreditoInterests[$id] = $procreditoInterest;
                    $vatProcreditoInterests[$id] = $vatProcreditoInterest;

                    $installmentAmounts[$id] += $procreditoCommission + $vatProcreditoCommission + $procreditoInterest + $vatProcreditoInterest;
                }
            }
        }

        foreach ($amountsPaymentMethods as $paymentMethodIndex => $amountMethodPayment) {
            foreach ($installmentIds as $id => $checked) {
                $paymentMethodAmount = $this->sma->formatNumberMask($amountsPaymentMethods[$paymentMethodIndex]); // se declara acá para tomar el nuevo saldo luego de restar el valor de las cuotas

                $data = [];
                if ($paymentMethodAmount > 0) { // saldo de la fomra de pago
                    $installmentAmount = $this->sma->formatNumberMask($installmentAmounts[$id]);

                    if ($installmentAmount > 0) {
                        if ($paymentMethodAmount >= $installmentAmount) {
                            $this->amount = $installmentAmount;
                            $amountsPaymentMethods[$paymentMethodIndex] = ($paymentMethodAmount - $installmentAmount); // se resta el valor de la cuota al monto del valor del medio de pago
                            $installmentAmounts[$id] = 0;
                        } else {
                            $this->amount = $paymentMethodAmount;
                            $amountsPaymentMethods[$paymentMethodIndex] = 0; // se resta totalmente el valor de la cuota del valor del medio de pago
                            $installmentAmounts[$id] = $installmentAmount - $paymentMethodAmount;
                        }

                        $daysExpired = $daysExpiredInstallments[$id];
                        $currentInterest = $currentInterestInstallments[$id];
                        $defaultInterest = $defaultInterestInstallments[$id];
                        $currentInterestTax = $currentInterestTaxInstallments[$id];
                        $defaultInterestTax = $defaultInterestTaxInstallments[$id];

                        if (isset($vatProcreditoCommission)) {
                            $procreditoCommission = $procreditoCommissions[$id];
                            $vatProcreditoCommission = $vatProcreditoCommissions[$id];
                            $procreditoInterest = $procreditoInterests[$id];
                            $vatProcreditoInterest = $vatProcreditoInterests[$id];
                        }

                        $data["installment_id"]             = $id;
                        $data["paid_by"]                    = $paidBy[$paymentMethodIndex];
                        $data["biller_id"]                  = $biller;
                        $data["document_type_id"]           = $documentTypeId;
                        $data["payment_no"]                 = $documentType->referenceNo;
                        $data["installment_paid_amount"]    = $this->sma->formatDecimal($this->amount, 4);

                        if (isset($vatProcreditoCommission)) {
                            $paymentProcreditoCommission = $this->calculatePaymentProcredito($procreditoCommission);
                            $procreditoCommissions[$id] = ((float) $procreditoCommissions[$id] - (float) $paymentProcreditoCommission);

                            $paymentVatProcreditoCommission = $this->calculatePaymentProcredito($vatProcreditoCommission);
                            $vatProcreditoCommissions[$id] = ((float) $vatProcreditoCommissions[$id] - (float) $paymentVatProcreditoCommission);

                            $paymentProcreditoInterest = $this->calculatePaymentProcredito($procreditoInterest);
                            $procreditoInterests[$id] = ((float) $procreditoInterests[$id] - (float) $paymentProcreditoInterest);

                            $paymentVatProcreditoInterest = $this->calculatePaymentProcredito($vatProcreditoInterest);
                            $vatProcreditoInterests[$id] = ((float) $vatProcreditoInterests[$id] - (float) $paymentVatProcreditoInterest);

                            $data["procredito_commission"]      = $paymentProcreditoCommission;
                            $data["vat_procredito_commission"]  = $paymentVatProcreditoCommission;
                            $data["procredito_interest"]        = $paymentProcreditoInterest;
                            $data["vat_procredito_interest"]    = $paymentVatProcreditoInterest;
                        }

                        $paymentDefaultInterest = $this->calculatePaymentInterest("default", $defaultInterest, $defaultInterestTax);
                        $defaultInterestInstallments[$id] = ((float) $defaultInterestInstallments[$id] - (float) $paymentDefaultInterest->interest);
                        $defaultInterestTaxInstallments[$id] = ((float) $defaultInterestTaxInstallments[$id] - (float) $paymentDefaultInterest->interestTax);

                        $paymentCurrentInterest = $this->calculatePaymentInterest("current", $currentInterest, $currentInterestTax);
                        $currentInterestInstallments[$id] = ((float) $currentInterestInstallments[$id] - (float) $paymentCurrentInterest->interest);
                        $currentInterestTaxInstallments[$id] = ((float) $currentInterestTaxInstallments[$id] - (float) $paymentCurrentInterest->interestTax);

                        $data["default_interest"]           = $paymentDefaultInterest->interest;
                        $data["default_interest_tax"]       = $paymentDefaultInterest->interestTax;
                        $data["current_interest_amount"]    = $paymentCurrentInterest->interest;
                        $data["interest_tax"]               = $paymentCurrentInterest->interestTax;
                        $data["payment_date"]               = date("Y-m-d H:i:s");
                        $data["default_days"]               = $daysExpired;
                        $data["capital_amount"]             = $this->sma->formatDecimal($this->amount, 4);
                        $data["default_interest_perc"]      = ($defaultInterest > 0) ? $this->Settings->default_interest_percentage_credit_financing : 0;
                        $data["created_by"]                 = $this->session->userdata('user_id');
                        if ($data["paid_by"] == 'cash') {
                            $data["value_received"]         = $this->sma->formatNumberMask($totalsReceived[$paymentMethodIndex]);
                            $data["change"]                 = $this->sma->formatNumberMask($changes[$paymentMethodIndex]);
                        } else {
                            $data["value_received"]         = 0;
                            $data["change"]                 = 0;
                        }

                        $note = !empty($lateFeeDiscount) ? "Valor de Ajuste de Valores: {$lateFeeDiscount}": "";
                        $data["note"]                       = "{$notes[$paymentMethodIndex]}  {$note}";
                    }
                }

                if (!empty($data)) {
                    $payments[] = $data;
                }
            }
        }

        $payments = $this->setPaymentsAffectedByDeposits($payments);

        if ($this->FinancingInstallmentsPayment_model->createBatch($payments)) {
            $this->DocumentsTypes_model->updateConsecutiveDocumentType(['sales_consecutive' => ($documentType->consecutive + 1)], $documentTypeId);

            $this->validateInstallmentStatus($payments, $paymentAgreement);
            $this->addSale($data["payment_no"]);

            $this->session->set_flashdata('installmentNo', $documentType->referenceNo);
            $this->session->set_flashdata('message', lang('Pago de cuota agregada correctamente'));

            admin_redirect('financing/paymentList');
        } else {
            $this->session->set_flashdata('error', lang('Pago de cuota no pudo ser agregada'));
            admin_redirect('financing/paymentList');
        }
    }

    private function validateConfigurationDataToSales()
    {
        if ($this->Settings->generate_automatic_invoice == YES) {
            $biller             = $this->site->getCompanyByID($this->input->post('biller'));
            $billerDataAditional= $this->Companies_model->getBillerDataAditional($biller->id);
            $documentType       = $this->DocumentsTypes_model->getDocumentType(["id"=>$billerDataAditional->default_electronic_document_for_financing]);
            if (empty($documentType)) {
                $this->session->set_flashdata('post', $this->input->post());
                $this->session->set_flashdata('error', lang('El Documento Electrónico para Financiación no ha sido configurado para la Sucursal seleccionada'));
                admin_redirect("Financing/paymentFinancingFee");
            }

            $this->documentTypeElectronicBilling = $documentType;
        }
    }

    private function setPaymentsAffectedByDeposits($payments)
    {
        $data = [];
        $customer = $this->site->getCompanyByID($this->input->post("creditCustomer"));

        if (!empty($payments)) {
            foreach ($payments as $payment) {
                $payment["affected_deposit_id"] = NULL;

                if ($payment["paid_by"] == "deposit") {
                    $amountTopay = $payment["capital_amount"];

                    $deposits = $this->Deposits_model->get(["company_id" => $customer->id, "balance >" => 0]);
                    foreach ($deposits as $deposit) {
                        if ($amountTopay == 0) {
                            break;
                        }
                        if ($deposit->balance < $amountTopay) {
                            $amountTopay -= $deposit->balance;
                            $updates[$deposit->id] = 0;
                            $affected_deposits[$deposit->id] = $deposit->balance;
                        } else if ($deposit->balance >= $amountTopay) {
                            $updates[$deposit->id] = $deposit->balance - $amountTopay;
                            $affected_deposits[$deposit->id] = $amountTopay;
                            $amountTopay -= $amountTopay;
                        }
                    }

                    if ($affected_deposits) {
                        foreach ($affected_deposits as $deposit_id => $amount_affected) {
                            $payment["affected_deposit_id"] = $deposit_id;

                            $this->Deposits_model->update(["balance" => $updates[$deposit_id]], $deposit_id);
                        }
                    }
                }

                $data[] = $payment;
            }
        }

        $this->site->sync_company_deposit_amount($customer->id);
        return $data;
    }

    private function calculatePaymentProcredito($concept)
    {
        if ($concept > 0) {
            if ($concept <= $this->amount) {
                $data = $concept;
            } else {
                $data = $this->amount;
            }

            $this->amount -= $concept;
        }

        $this->amount = ($this->amount < 0) ? 0 : $this->amount;

        return $this->sma->formatDecimal($data, 4);
    }

    private function calculatePaymentInterest($type, $interest, $interestTax)
    {
        $data = [
            "interest" => 0,
            "interestTax" => 0,
        ];

        if ($interest > 0 && $this->amount > 0) {
            $totalInterest = $interest + $interestTax;

            if ($totalInterest <= $this->amount) {
                $data["interest"] =  $this->sma->formatDecimal($interest, 4);
                $data["interestTax"] =  $this->sma->formatDecimal($interestTax, 4);
            } else {
                $concept = ($type == "current") ? $this->Settings->concepts_for_current_interest : $this->Settings->concepts_for_default_interest;
                $product = $this->Products_model->getProductByID($concept);

                $taxRate = $this->TaxRates_model->find($product->tax_rate);
                $paymentToInterest = $this->amount / (1 + ($taxRate->rate / 100));
                $paymentToInterestTax = $paymentToInterest * ($taxRate->rate / 100);

                $data["interest"] =  $this->sma->formatDecimal($paymentToInterest, 4);
                $data["interestTax"] =  $this->sma->formatDecimal($paymentToInterestTax, 4);
            }

            $this->amount -= $totalInterest;
        }

        $this->amount = ($this->amount < 0) ? 0 : $this->amount;

        return (object) $data;
    }

    private function addSale($paymentNo)
    {
        if ($this->Settings->generate_automatic_invoice == YES) {
            $payments   = $this->buildSalePayments($paymentNo);
            $items      = $this->buildSaleItems($paymentNo);
            $data       = $this->buildSale($items);

            if (!empty($items)) {
                if ($saleId = $this->Sales_model->addSale($data, $items, $payments)) {
                    $this->FinancingInstallmentsPayment_model->update(["sale_id" => $saleId], $paymentNo);
                    $this->createDocumentElectronic($saleId);
                }
            }
        }
    }

    private function buildSale($items)
    {
        $documentType   = $this->documentTypeElectronicBilling;
        $biller         = $this->site->getCompanyByID($this->input->post('biller'));
        $billerData     = $this->pos_model->get_biller_data_by_biller_id($biller->id);
        $customer       = $this->site->getCompanyByID($this->input->post("creditCustomer"));
        $address        = $this->site->getCustomerBranch($this->input->post("creditCustomer"));
        $resolutionText = ($documentType->save_resolution_in_sale == 1) ? $this->site->textoResolucion($documentType) : "";

        $subtotal = $productTax = 0;
        foreach ($items as $item) {
            $subtotal += $item["net_unit_price"];
            $productTax += $item["item_tax"];
        }

        $data = [
            'date'          => date("Y-m-d H:i:s"),
            'customer_id'   => $customer->id,
            'customer'      => $customer->name,
            'biller_id'     => $biller->id,
            'biller'        => $biller->name,
            'warehouse_id'  => $billerData->default_warehouse_id,
            'total'         => $subtotal,
            'product_tax'   => $productTax,
            'total_tax'     => $productTax,
            'grand_total'   => ($subtotal + $productTax),
            'total_items'   => count($items),
            'sale_status'   => "completed",
            'paid'          => 0,
            'address_id'    => $address,
            'created_by'    => $this->session->userdata('user_id'),
            'hash'          => hash('sha256', microtime() . mt_rand()),
            'resolucion'    => $resolutionText,
            'sale_currency' => $this->Settings->default_currency,
            'document_type_id' => $documentType->id,
            'payment_status'=> 'paid',
            'technology_provider' => $this->Settings->fe_technology_provider,
            'payment_term'  => 0,
            'pos'           => ($documentType->module == 1) ? 1 : 0,
        ];

        if ($this->Settings->cost_center_selection == 0) {
            $billerCostCenter = $this->site->getBillerCostCenter($biller->id);
            $data['cost_center_id'] = $billerCostCenter->id;
        } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
            $data['cost_center_id'] = $this->input->post('cost_center_id');
        }

        return $data;
    }

    private function buildSaleItems($paymentNo)
    {
        $data = [];
        $payments = $this->FinancingInstallmentsPayment_model->get(['payment_no' => $paymentNo]);
        $subtotalCurrent = $taxCurrent = $subtotalDefault = $taxDefault = $procredito_commission = $vat_procredito_commission =$procredito_interest = $vat_procredito_interest = 0;

        if (!empty($payments)) {
            foreach ($payments as $payment) {
                $subtotalCurrent += $payment->current_interest_amount;
                $taxCurrent += $payment->interest_tax;
                $subtotalDefault += $payment->default_interest;
                $taxDefault += $payment->default_interest_tax;
                $procredito_commission += $payment->procredito_commission;
                $vat_procredito_commission += $payment->vat_procredito_commission;
                $procredito_interest += $payment->procredito_interest;
                $vat_procredito_interest += $payment->vat_procredito_interest;
            }
        }

        $currentData = $this->buildSaleItem("current", $subtotalCurrent, $taxCurrent);
        if (!empty($currentData)) {
            $data[] = $currentData;
        }

        $defaultData = $this->buildSaleItem("default", $subtotalDefault, $taxDefault);
        if (!empty($defaultData)) {
            $data[] = $defaultData;
        }

        $commissionData = $this->buildSaleItem("commission", $procredito_commission, $vat_procredito_commission);
        if (!empty($commissionData)) {
            $data[] = $commissionData;
        }

        $interestData = $this->buildSaleItem("interest", $procredito_interest, $vat_procredito_interest);
        if (!empty($interestData)) {
            $data[] = $interestData;
        }

        return $data;
    }

    private function buildSaleItem($type, $baseAmount, $taxAmount)
    {
        $data = [];

        if (!empty($baseAmount)) {
            if ($type == "default") {
                $concept = $this->Settings->concepts_for_default_interest;
            } else if ($type == "current") {
                $concept = $this->Settings->concepts_for_current_interest;
            } else if ($type == "commission") {
                $concept = $this->Settings->concept_for_procredito_commission;
            } else {
                $concept = $this->Settings->concept_for_procredito_interest;
            }

            $product = $this->Products_model->getProductByID($concept);
            $taxRate = $this->TaxRates_model->find($product->tax_rate);

            $data = [
                "product_id"    => $product->id,
                "product_code"  => $product->code,
                "product_name"  => $product->name,
                "product_type"  => $product->type,
                "net_unit_price"=> $baseAmount,
                "unit_price"    => ($baseAmount + $taxAmount),
                "quantity"      => 1,
                "unit_quantity" => 1,
                "item_tax"      => $taxAmount,
                "tax_rate_id"   => $product->tax_rate,
                "tax"           => $taxRate->rate,
                "subtotal"      => ($baseAmount + $taxAmount),
                "real_unit_price"=> ($baseAmount + $taxAmount),
                "price_before_tax"=> $baseAmount
            ];
        }

        return $data;
    }

    private function buildSalePayments($paymentNo)
    {
        $data = [];
        $payments = $this->FinancingInstallmentsPayment_model->get(['payment_no' => $paymentNo], true);

        if (!empty($payments)) {
            $paymentMethods = [];
            $documentTypePayment = $this->getDocumentTypeToPayments($payments[0]->biller_id);

            foreach ($payments as $payment) {
                $paidBy = $payment->paid_by;
                if (!array_key_exists($paidBy, $paymentMethods)) {
                    $paymentMethods[$paidBy] = [
                        "amount"    => 0,
                        "note"      => $payment->note
                    ];
                }
            }

            foreach ($payments as $payment) {
                $paymentMethods[$payment->paid_by]["amount"] += ($payment->current_interest_amount + $payment->interest_tax + $payment->default_interest + $payment->default_interest_tax);
            }

            foreach ($paymentMethods as $paymentMethod => $paymentMethodArray) {
                if ($paymentMethodArray["amount"] > 0) {
                    $paymentMethodData  = $this->PaymentMethods_model->find(["code" => $paymentMethod]);

                    $data[] = [
                        'date'          => date("Y-m-d H:i:s"),
                        'amount'        => $paymentMethodArray["amount"],
                        'paid_by'       => $paymentMethod,
                        'created_by'    => $this->session->userdata('user_id'),
                        'note'          => $paymentMethodArray["note"],
                        'mean_payment_code_fe' => $paymentMethodData->code_fe,
                        'type'          => 'received',
                        'document_type_id' => $documentTypePayment->id,
                        'seller_id'     => $this->session->userdata('user_id'),
                    ];
                }
            }
        }

        return $data;
    }

    public function getDocumentTypeToPayments($billerId)
    {
        $moduleElectronicBilling = $this->documentTypeElectronicBilling->module;

        $module = ($moduleElectronicBilling == 1) ? 13 : 19;

        $documentType = $this->DocumentsTypes_model->getDocumentTypeBiller(["biller_id" => $billerId, "module" => $module]);
        return $documentType;
    }

    public function createDocumentElectronic($saleId)
    {
        $documentType = $this->documentTypeElectronicBilling;
        $resolution = $this->site->getDocumentTypeById($documentType->id);
        if ($resolution->factura_electronica == YES) {
            $sale = $this->site->getSaleByID($saleId);

            $document = [
                'sale_id' => $saleId,
                'biller_id' => $sale->biller_id,
                'customer_id' => $sale->customer_id,
                'reference' => $sale->reference_no,
                'attachment' => $sale->attachment
            ];
            $this->sendElectronicDocument($document);
        }
    }

    private function sendElectronicDocument($document)
    {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            return true;
        }

        $document           = (object) $document;
        $technologyProvider = $this->Settings->fe_technology_provider;
        $filename           = $this->site->getFilename($document->sale_id);
        $documentElectronicCreated = $this->Electronic_billing_model->send_document_electronic($document);

        if ($documentElectronicCreated->response == 0) {
            $this->session->set_flashdata('error', $documentElectronicCreated->message);
        } else {
            $email_delivery_response = '';

            if ($technologyProvider == CADENA || $technologyProvider == BPM) {
                $this->downloadFeXml($document->sale_id, TRUE);
                $this->downloadFePdf($document->sale_id, TRUE);

                $pathXML = "files/electronic_billing/{$filename}.xml";
                $pathPDF = "files/electronic_billing/{$filename}.pdf";
                $pathZIP = "files/electronic_billing/{$filename}.zip";

                if (file_exists($pathXML) && file_exists($pathPDF)) {
                    $zip = new ZipArchive();

                    $zip->open($pathZIP, ZipArchive::CREATE | ZipArchive::OVERWRITE);
                    $zip->addFile($pathXML, "{$filename}.xml");
                    $zip->addFile($pathPDF, "{$filename}.pdf");

                    $zip->close();

                    unlink($pathXML);
                    unlink($pathPDF);

                    if ($_SERVER['SERVER_NAME'] != 'localhost') {
                        $email_delivery_response = $this->Electronic_billing_model->receipt_delivery($document, $filename);
                    }
                }
            }

            $this->session->set_flashdata('message', 'Documento electrónico creado correctamente <br>' . $email_delivery_response);
        }
    }

    public function downloadFeXml($sale_id, $internal_download = FALSE)
    {
        $sale = $this->site->getSaleByID($sale_id);
        $technologyProvider = $this->Settings->fe_technology_provider;
        $filename = $this->site->getFilename($sale_id);

        if ($technologyProvider == CADENA) {
            if (!empty($sale->fe_xml)) {
                $xml_file = base64_decode($sale->fe_xml);

                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $path = 'files/electronic_billing';
                    if (!file_exists($path)) {
                        mkdir($path, 0777);
                    }

                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                } else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            } else {
                $prefix = strstr($sale->reference_no, '-', TRUE);
                $reference = str_replace('-', '', $sale->reference_no);
                $document_type_code = ($sale->total > 0) ? '01' : '92';
                $technology_provider_configuration = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/documentos?nit_emisor=' . $this->Settings->numero_documento . '&id_documento=' . $reference . '&codigo_tipo_documento=' . $document_type_code . '&prefijo=' . $prefix,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => [
                        'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                        'Content-Type: application/json',
                        'Partnership-Id: 901090070'
                    ],
                ));
                $response = curl_exec($curl);
                log_message('debug', 'CADENA FE-Consulta documento: '. $response);
                curl_close($curl);

                $response = json_decode($response);
                if (!empty($response)) {
                    if (isset($response->statusCode)) {
                        if ($response->statusCode == 200) {
                            $updated_sale = $this->site->updateSale([
                                'fe_aceptado' => 2,
                                'fe_mensaje' => 'Documento aceptado por la DIAN',
                                'fe_mensaje_soporte_tecnico' => 'La Factura electrónica ' . $reference . ', ha sido autorizada. Código es estado ' . $response->statusCode,
                                'fe_xml' => $response->document
                            ], $sale_id);

                            if ($updated_sale == TRUE) {
                                $xml_file = base64_decode($response->document);

                                if ($internal_download === TRUE) {
                                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                                    $xml->loadXML($xml_file);

                                    $path = 'files/electronic_billing';
                                    if (!file_exists($path)) {
                                        mkdir($path, 0777);
                                    }

                                    $xml->save('files/electronic_billing/' . $sale->reference_no . '.xml');
                                } else {
                                    header('Content-Type: application/xml;');
                                    header('Content-Disposition: attachment; filename="' . $sale->reference_no . '.xml"');
                                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                                    $xml->loadXML($xml_file);
                                    echo $xml->saveXML();
                                }
                            } else {
                                $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML.');
                                admin_redirect('sales/fe_index');
                            }
                        } else if ($response->statusCode == 400) {
                            $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML. El prefijo no coincide con el prefijo del id del documento.');
                            admin_redirect('sales/fe_index');
                        } else if ($response->statusCode == 404) {
                            $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML. Registro no encontrado con los parámetros de búsqueda enviados.');
                            admin_redirect('sales/fe_index');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Se ha perdido la conexión con el Web Services.');
                        admin_redirect('sales/fe_index');
                    }
                } else {
                    $this->session->set_flashdata('error', 'No es posible realizar conexión con Web Services.');
                    admin_redirect('sales/fe_index');
                }
            }

        } else if ($technologyProvider == DELCOP) {
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
            $response_token = $this->Electronic_billing_model->delcopGetAuthorizationoken();
            if ($response_token->success == FALSE) {
                $this->session->set_flashdata('error', "Los campos de Usuario o Contraseña DELCOP no son correctos. " . $response_token->error);
                admin_redirect('sales/fe_index');
            }

            if ($this->Settings->fe_work_environment == TEST) {
                $url = "https://www-prueba.titanio.com.co/PDE/public/api/PDE/descargar";
            } else {
                $url = "https://www.titanio.com.co/PDE/public/api/PDE/descargar";
            }

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => '{"token": "' . $response_token->token . '", "documentos": [{"transaccion_id": "' . $sale->fe_id_transaccion . '", "tipo_descarga": "1"}]}',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $response = json_decode($response);

            if ($response->error_id == 0) {
                $sale_data["fe_xml"] = $response->documentos[0]->data;
                $updated_sale = $this->site->updateSale($sale_data, $sale->id);

                $xml_file = base64_decode($response->documentos[0]->data);
                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $xml->save('files/electronic_billing/' . $sale->reference_no . '.xml');
                } else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $sale->reference_no . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            }
        } else if ($technologyProvider == BPM) {
            $xml_file = base64_decode($sale->fe_xml);

            if ($internal_download === TRUE) {
                $xml = new DOMDocument("1.0", "ISO-8859-15");
                $xml->loadXML($xml_file);

                $path = 'files/electronic_billing';
                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }

                $xml->save('files/electronic_billing/' . $filename . '.xml');
            } else {
                header('Content-Type: application/xml;');
                header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                $xml = new DOMDocument("1.0", "ISO-8859-15");

                $xml->loadXML($xml_file);
                echo $xml->saveXML();
            }
        }
    }

    public function downloadFePdf($sale_id, $internal_download = FALSE)
    {
        $technologyProvider = $this->Settings->fe_technology_provider;

        if ($technologyProvider == CADENA || $technologyProvider == BPM) {
            $this->saleView($sale_id, $internal_download);
        } else if ($technologyProvider == DELCOP) {
            $sale = $this->site->getSaleByID($sale_id);
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
            $response_token = $this->Electronic_billing_model->delcopGetAuthorizationoken();
            if ($response_token->success == FALSE) {
                $this->session->set_flashdata('error', "Los campos de Usuario o Contraseña DELCOP no son correctos. " . $response_token->error);
                admin_redirect('sales/fe_index');
            }

            if ($this->Settings->fe_work_environment == TEST) {
                $url = "https://www-prueba.titanio.com.co/PDE/public/api/PDE/descargar";
            } else {
                $url = "https://www.titanio.com.co/PDE/public/api/PDE/descargar";
            }

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => '{"token": "' . $response_token->token . '", "documentos": [{"transaccion_id": "' . $sale->fe_id_transaccion . '", "tipo_descarga": "2"}]}',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $response = json_decode($response);

            if ($response->error_id == 0) {
                $xml_file = base64_decode($response->documentos[0]->data);

                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $xml->save('files/electronic_billing/' . $sale->reference_no . '.xml');
                } else {
                    file_put_contents("files/electronic_billing/" . $sale->reference_no . ".pdf", $xml_file);

                    header("Content-type: application/pdf");
                    header("Content-Disposition: inline; filename=documento.pdf");
                    readfile("files/electronic_billing/" . $sale->reference_no . ".pdf");
                }
            }
        }
    }

    private function validateInstallmentStatus($payments, $paymentAgreement)
    {
        $installments = $quotes = $credits = $quoteInPayments = [];

        if ($paymentAgreement == true) {
            foreach ($payments as $payment) {
                $this->FinancingInstallment_model->update(["status" => self::PAID], $payment["installment_id"]);
            }
        } else {
            foreach ($payments as $payment) {
                if (!in_array($payment["installment_id"], $installments)) {
                    $quotes[] = $payment["installment_id"];
                }
            }

            $financings = $this->FinancingInstallment_model->getIn("id", $quotes);
            foreach ($financings as $financing) {
                if (!in_array($financing->financing_credit_id, $credits)) {
                    $credits[] = $financing->financing_credit_id;
                }
            }

            $quotes = $this->FinancingInstallment_model->getIn("financing_credit_id", $credits);
            foreach ($quotes as $quote) {
                if (!in_array($quote->id, $quoteInPayments)) {
                    $quoteInPayments[] = $quote->id;
                }
            }

            $payments = $this->FinancingInstallmentsPayment_model->getIn("installment_id", $quoteInPayments);
            foreach ($payments as $payment) {
                $paymentAmountTotal = $this->sma->formatDecimal($payment->capital_amount + $payment->current_interest_amount + $payment->interest_tax, 2);
                foreach ($quotes as $quote) {
                    $quoteAmountTotal = 0;
                    if ($quote->id == $payment->installment_id) {
                        $quoteAmountTotal = $this->sma->formatDecimal($quote->capital_amount + $quote->current_interest_amount + $quote->interest_tax, 2);

                        if ($paymentAmountTotal >= $quoteAmountTotal) {
                            $this->FinancingInstallment_model->update(["status" => self::PAID], $quote->id);
                        }
                    }
                }
            }
        }

    }

    public function paymentList()
    {
        $this->data["installmentNo"] = $this->session->flashdata('installmentNo');
        $this->data["noPrintFormat"] = $this->session->flashdata('noPrintFormat');

        $this->page_construct('financing/payment_list', ['page_title' => $this->lang->line('payment_list')], $this->data);
    }

    public function getPayments()
    {
        $this->load->library('datatables');

        $this->datatables->select("
            credit_financing_installments_payment.id,
            credit_financing_installments_payment.payment_no as payment_no,
            credit_financing_installments_payment.payment_date,
            b.name AS billerName,
            c.name AS customerName,
            c.vat_no,
            SUM({$this->db->dbprefix('credit_financing_installments_payment')}.capital_amount) AS capital_amount,
            SUM({$this->db->dbprefix('credit_financing_installments_payment')}.current_interest_amount + {$this->db->dbprefix('credit_financing_installments_payment')}.interest_tax) AS current_interest,
            SUM({$this->db->dbprefix('credit_financing_installments_payment')}.default_interest + {$this->db->dbprefix('credit_financing_installments_payment')}.default_interest_tax) AS default_interest,
            SUM({$this->db->dbprefix('credit_financing_installments_payment')}.procredito_commission + {$this->db->dbprefix('credit_financing_installments_payment')}.vat_procredito_commission) AS procredito_commission,
            SUM({$this->db->dbprefix('credit_financing_installments_payment')}.procredito_interest + {$this->db->dbprefix('credit_financing_installments_payment')}.vat_procredito_interest) AS procredito_interest,
            SUM(
                {$this->db->dbprefix('credit_financing_installments_payment')}.capital_amount +
                {$this->db->dbprefix('credit_financing_installments_payment')}.current_interest_amount +
                {$this->db->dbprefix('credit_financing_installments_payment')}.interest_tax +
                {$this->db->dbprefix('credit_financing_installments_payment')}.default_interest +
                {$this->db->dbprefix('credit_financing_installments_payment')}.default_interest_tax +
                {$this->db->dbprefix('credit_financing_installments_payment')}.procredito_commission +
                {$this->db->dbprefix('credit_financing_installments_payment')}.vat_procredito_commission +
                {$this->db->dbprefix('credit_financing_installments_payment')}.procredito_interest +
                {$this->db->dbprefix('credit_financing_installments_payment')}.vat_procredito_interest) AS totalto_pay
            ");
        $this->datatables->from("credit_financing_installments_payment");
        $this->datatables->join("companies b", "b.id = credit_financing_installments_payment.biller_id", "inner");
        $this->datatables->join("credit_financing_installments fi", "fi.id = credit_financing_installments_payment.installment_id", "inner");
        $this->datatables->join("companies c", "c.id = fi.customer_id", "inner");
        $this->datatables->group_by("payment_no");

        $this->datatables->add_column("Actions", '<div class="text-center">
                                        <div class="btn-group text-left">
                                            <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                                            <i class="fas fa-ellipsis-v fa-lg"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li>
                                                    <a href="'. admin_url('financing/printReceipt/$1') .'" target="_blank"><i class="fa fa-file-text-o"></i>'. lang("voucher") . '</a>
                                                </li>
                                                <li>
                                                    <a href="'. admin_url('financing/details/$1') .'" data-toggle="modal" data-target="#myModal"><i class="fa fa-file-text-o"></i>'. lang("detail") . '</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>', "payment_no");
        echo $this->datatables->generate();
    }

    public function printReceipt($paymentNo)
    {
        $payments       = $this->FinancingInstallmentsPayment_model->get(['payment_no' => $paymentNo]);

        $billerId       = $payments[0]->biller_id;
        $createdBy      = $payments[0]->created_by;
        $documentTypeId = $payments[0]->document_type_id;
        $paymentDate    = $payments[0]->paymentDate;
        $paymentTime    = $payments[0]->paymentTime;
        $customerName   = $payments[0]->customerName;
        $customerVatno  = $payments[0]->customerVatno;
        $customerId     = $payments[0]->customer_id;
        $saleId         = $payments[0]->sale_id;

        $createdBy      = $this->site->getUser($createdBy);
        $paymentMethods = $this->getPaymentMethods($paymentNo);
        $nextInstallments = $this->getNextInstallments($customerId);
        $biller         = $this->pos_model->getCompanyByID($billerId);
        $documentType   = $this->site->getDocumentTypeById($documentTypeId);
        $invoiceFormat  = $this->site->getInvoiceFormatById($documentType->quick_print_format_id);
        $tipo_regimen   = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);

        $this->validateInvoiceFormat($documentType);

        $this->data['invoiceExists']    = ($this->Settings->generate_automatic_invoice == 1 && $saleId != null) ? true : false;
        $this->data['tipo_regimen']     = lang($tipo_regimen->description);
        $this->data['nextInstallments'] = $nextInstallments;
        $this->data['paymentMethods']   = $paymentMethods;
        $this->data['customerVatno']    = $customerVatno;
        $this->data['customerName']     = $customerName;
        $this->data['paymentDate']      = $paymentDate;
        $this->data['paymentTime']      = $paymentTime;
        $this->data['created_by']       = $createdBy;
        $this->data['paymentNo']        = $paymentNo;
        $this->data['paymentsFinancing']= $payments;
        $this->data['biller']           = $biller;
        $this->data['modal']            = false;

        if ($this->data['invoiceExists']) {
            $inv            = $this->pos_model->getInvoiceByID($saleId);
            $biller         = $this->site->getAllCompaniesWithState('biller', $inv->biller_id, false);
            $seller         = $this->site->getSellerById(!empty($inv->seller_id) ? $inv->seller_id : $biller->default_seller_id);
            $invoiceFooter  = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
            $billerData     = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
            $documentType   = $this->site->getDocumentTypeById($inv->document_type_id);
            $rows           = $this->sales_model->getAllInvoiceItems($inv->id);
            $payments       = $this->sales_model->getPaymentsForSale($inv->id);
            $customer       = $this->site->getCompanyByID($inv->customer_id);

            $this->data['value_decimals']               = $this->Settings->qty_decimals;
            $this->data['qty_decimals']                 = $this->Settings->decimals;
            $this->data['invoice_footer']               = $invoiceFooter;
            $this->data['document_type']                = $documentType;
            $this->data['biller_data']                  = $billerData;
            $this->data['payments']                     = $payments;
            $this->data['customer']                     = $customer;
            $this->data['biller']                       = $biller;
            $this->data['seller']                       = $seller;
            $this->data['rows']                         = $rows;
            $this->data['inv']                          = $inv;
            $this->data['document_type_invoice_format'] = false;
            $this->data['show_product_preferences']     = false;
            $this->data['product_detail_promo']         = false;
            $this->data['return_payments']              = null;
            $this->data['return_sale']                  = null;
            $this->data['return_rows']                  = null;
            $this->data['tax_indicator']                = 0;
        }

        $this->load_view($this->theme.$invoiceFormat->format_url, $this->data);
    }

    private function getNextInstallments($customerId)
    {
        $installments= $this->FinancingInstallment_model->get(['credit_financing.customer_id' => $customerId]);

        $installments = $this->subtractPayments($installments);
        $installments = $this->calculatedaysExpiredIfPaymentsExisting($installments);
        $installments = $this->calculateDefaultInterest($installments);

        return $installments;
    }

    private function getPaymentMethods($paymentNo)
    {
        $payments       = $this->FinancingInstallmentsPayment_model->get(['payment_no' => $paymentNo], true);
        $paymentMethods = [];

        foreach ($payments as $payment) {
            if (!in_array($payment->paid_by, $paymentMethods)) {
                $paymentMethods[$payment->paid_by] = [];
                $paymentMethods[$payment->paid_by]['amount'] = 0;
            }
        }

        foreach ($payments as $payment) {
            if (!in_array($payment->paid_by, $paymentMethods)) {
                $paymentMethods[$payment->paid_by]['amount'] += $payment->installment_paid_amount;
                $paymentMethods[$payment->paid_by]['name'] = $payment->paidByName;

                if ($payment->paid_by == 'cash') {
                    $paymentMethods[$payment->paid_by]['change'] = $payment->change;
                    $paymentMethods[$payment->paid_by]['value_received'] = $payment->value_received;
                }
            }
        }

        return $paymentMethods;
    }

    private function validateInvoiceFormat($documentType)
    {
        if (empty($documentType->quick_print_format_id)) {
            $this->session->set_flashdata('error', lang('No se ha asignado el formato de impresión'));
            $this->session->set_flashdata('noPrintFormat', 1);

            admin_redirect("financing/paymentList");
        }
    }

    public function details($paymentNo = null)
    {
        $this->sma->checkPermissions(false, true);

        $this->data['payments']  = $this->FinancingInstallmentsPayment_model->get(['payment_no' => $paymentNo]);
        $this->data["paymentNo"] = $paymentNo;
        $this->load_view($this->theme . "financing/payments_details", $this->data);
    }

    public function paymentAgreement($installmentSelected = [])
    {
        $this->sma->checkPermissions(false, true);

        if (!empty($installmentSelected)) {
            $payment  = $this->FinancingInstallmentsPayment_model->get(['payment_no' => $paymentNo]);
            $this->sma->print_arrays($payment);
        }
    }

    public function calculateBaseAndVatAJAX()
    {
        $amount = $this->input->post("amount");
        $type   = $this->input->post("type");
        $base   = 0;

        if ($amount > 0) {
            if ($type == "default") {
                $concept = $this->Settings->concepts_for_default_interest;
            } else if ($type == "current") {
                $concept = $this->Settings->concepts_for_current_interest;
            } else if ($type == "commission") {
                $concept = $this->Settings->concept_for_procredito_commission;
            } else {
                $concept = $this->Settings->concept_for_procredito_interest;
            }

            $product = $this->Products_model->getProductByID($concept);
            $taxRate = $this->TaxRates_model->find($product->tax_rate);

            $base = $amount / (($taxRate->rate / 100) + 1);
            $vat  = $base * ($taxRate->rate / 100);
        }

        echo json_encode([
            "base" => $this->sma->formatDecimals($base, 2),
            "vat" => $this->sma->formatDecimals($vat, 2)
        ]);
    }

    public function calculateVATInterestAJAX()
    {
        $vatInterest = 0;
        $type = $this->input->post('type');
        $interest = $this->input->post('baseValue');

        if ($interest > 0) {
            $concept = ($type == "current") ? $this->Settings->concepts_for_current_interest : $this->Settings->concepts_for_default_interest;
            $product = $this->Products_model->getProductByID($concept);
            $taxRate = $this->TaxRates_model->find($product->tax_rate);

            $vatInterest = $interest * ($taxRate->rate / 100);
        }

        $data["interestTax"] =  $this->sma->formatDecimal($vatInterest, 2);

        echo json_encode($data);
    }

    public function installmentList()
    {
        $this->page_construct('financing/installment_list', ['page_title' => lang('credit_financing_language')], $this->data);
    }

    public function getInstallments()
    {
        $this->load->library('datatables');

        $permission = false;

        $this->datatables->select("
            credit_financing_installments.id AS id,
            credit_no,
            c.name,
            credit_financing_installments.installment_no,
            credit_financing_installments.installment_due_date,
            credit_financing_installments.installment_amount,
            credit_financing_installments.capital_amount,
            credit_financing_installments.current_interest_amount,
            credit_financing_installments.interest_tax,
            credit_financing_installments.status");
        $this->datatables->from("credit_financing_installments");
        $this->datatables->join("credit_financing f", "f.id = credit_financing_installments.financing_credit_id", "inner");
        $this->datatables->join("companies c", "c.id = credit_financing_installments.customer_id", "inner");

        if ($this->Admin || $this->Owner) {
            $permission = true;
        } else {
            if ($this->GP["financing-installment_edit"]) {
                $permission = true;
            }
        }

        if ($permission == true) {
            $this->datatables->add_column("Actions", '<div class="text-center">
                <div class="btn-group text-left">
                    <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                    <i class="fas fa-ellipsis-v fa-lg"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>
                            <a href="'. admin_url('financing/installmentEdit/$1') .'" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i>'. lang("installment_edit") . '</a>
                        </li>
                    </ul>
                </div>
            </div>', "id");
        }

        echo $this->datatables->generate();
    }

    public function installmentEdit($id)
    {
        $this->sma->checkPermissions(false, true);

        $installment = $this->FinancingInstallment_model->get(["cfi.id" => $id]);

        $this->data["installment"] = $installment[0];

        $this->load_view($this->theme . 'financing/installment_edit', $this->data);
    }

    public function installmentUpdate()
    {
        $id = $this->input->post("installmentId");
        $capitalAmount = $this->sma->formatNumberMask($this->input->post("capital_amount"));
        $currentInterest = $this->sma->formatNumberMask($this->input->post("current_interest"));
        $vatCurrentInterest = $this->sma->formatNumberMask($this->input->post("vat_current_interest"));
        $valueToApply = $this->sma->formatNumberMask($this->input->post("value_to_apply"));
        $installment = $this->FinancingInstallment_model->find(["id" => $id]);

        $data = [
            "installment_amount" => $valueToApply,
            "capital_amount" => $capitalAmount,
            "current_interest_amount" => $currentInterest,
            "interest_tax" => $vatCurrentInterest
        ];

        if ($this->FinancingInstallment_model->update($data, $id)) {
            $this->userActivities($installment);

            $this->session->set_flashdata('message', "Cuota modificada exitosamente");
            admin_redirect('financing/installmentList');
        } else {
            $this->session->set_flashdata('error', "No fue posible modificar la cuota");
            admin_redirect('financing/installmentList');
        }
    }

    private function userActivities($installment)
    {
        $capitalAmount = $this->sma->formatNumberMask($this->input->post("capital_amount"));
        $currentInterest = $this->sma->formatNumberMask($this->input->post("current_interest"));
        $vatCurrentInterest = $this->sma->formatNumberMask($this->input->post("vat_current_interest"));
        $valueToApply = $this->sma->formatNumberMask($this->input->post("value_to_apply"));

        $capitalChangeMade = $this->changesMade($installment->capital_amount, $capitalAmount, "Valor Capital de");
        $currentInterestChangeMade = $this->changesMade($installment->current_interest_amount, $currentInterest, "Interés Corriente de");
        $vatCurrentInterestChangeMade = $this->changesMade($installment->interest_tax, $vatCurrentInterest, "IVA Interés Corriente de");
        $valueToApplyChangeMade = $this->changesMade($installment->installment_amount, $valueToApply, "Valor Cuota de");

        if ($capitalChangeMade != "" || $currentInterestChangeMade != "" || $vatCurrentInterestChangeMade != "" || $valueToApplyChangeMade != "") {
            $description = "{$this->session->userdata("username")} editó los siguiente datos: {$capitalChangeMade} {$currentInterestChangeMade} {$vatCurrentInterestChangeMade} {$valueToApplyChangeMade}";

            $data = [
                "date" => date("Y-m-d H:i:s"),
                "type_id" => 1,
                "user_id" => $this->session->userdata('user_id'),
                "module_name" => "financing",
                "table_name" => "credit_financing_installments",
                "record_id" => $installment->id,
                "description" => $description,
            ];
            $this->UserActivities_model->insert($data);
        }
    }

    private function changesMade($data1, $data2, $string)
    {
        if ($this->sma->formatDecimal($data1) != $this->sma->formatDecimal($data2)) {
            return "{$string} {$data1} a {$data2}; ";
        }
    }

    public function calculateProcreditoCommissionAJAX()
    {
        $VATinterest           = 0;
        $commission            = 0;
        $capital               = $this->input->post("capital");
        $concept               = $this->Settings->concept_for_procredito_commission;
        $product               = $this->Products_model->getProductByID($concept);
        $commissionPercentages = $this->CommissionPercentage_model->get();

        if (!empty($commissionPercentages)) {
            $capital = $this->sma->formatDecimal($capital, 2);
            foreach ($commissionPercentages as $commissionPercentage) {
                $from = $this->sma->formatDecimal($commissionPercentage->from, 2);
                $to   = $this->sma->formatDecimal($commissionPercentage->to, 2);

                if ($from <= $capital && $capital <= $to) {
                    if ($commissionPercentage->type == 2) {
                        $commission = $commissionPercentage->percentage;
                        break;
                    } else {
                        $commission = $capital * ($commissionPercentage->percentage / 100);
                        break;
                    }
                } else if ($to == 0) {
                    $commission = $capital * ($commissionPercentage->percentage / 100);
                    break;
                }
            }

            if ($commission > 0) {
                $taxRate = $this->TaxRates_model->find($product->tax_rate);
                $VATinterest = $commission * ($taxRate->rate / 100);
            }
        }

        echo json_encode([
            "commission"  => $this->sma->formatDecimal($commission, 2),
            "VATinterest" => $this->sma->formatDecimal($VATinterest, 2)
        ]);
    }

    public function calculateProcreditoInterestAJAX()
    {
        $VATinterest        = 0;
        $procreditoInterest = $this->input->post("procreditoInterest");
        $concept            = $this->Settings->concept_for_procredito_interest;
        $product            = $this->Products_model->getProductByID($concept);
        $taxRate            = $this->TaxRates_model->find($product->tax_rate);

        $VATinterest = $procreditoInterest * ($taxRate->rate / 100);

        echo json_encode([
            "interest"  => $this->sma->formatDecimal($procreditoInterest, 2),
            "VATinterest" => $this->sma->formatDecimal($VATinterest, 2)
        ]);
    }

    public function calculateProcreditoVATCommission()
    {
        $VATinterest= 0;
        $commission = $this->input->post("commission");
        $concept    = $this->Settings->concept_for_procredito_commission;
        $product    = $this->Products_model->getProductByID($concept);

        if ($commission > 0) {
            $taxRate = $this->TaxRates_model->find($product->tax_rate);
            $VATinterest = $commission * ($taxRate->rate / 100);
        }

        echo json_encode([
            "VATinterest" => $this->sma->formatDecimal($VATinterest, 2)
        ]);
    }

    public function calculateProcreditoVATInterest()
    {
        $VATinterest= 0;
        $interest = $this->input->post("interest");
        $concept    = $this->Settings->concept_for_procredito_interest;
        $product    = $this->Products_model->getProductByID($concept);

        if ($interest > 0) {
            $taxRate = $this->TaxRates_model->find($product->tax_rate);
            $VATinterest = $interest * ($taxRate->rate / 100);
        }

        echo json_encode([
            "VATinterest" => $this->sma->formatDecimal($VATinterest, 2)
        ]);
    }

    public function notifyCredits()
    {
        $message = "";
        $unnotifiedCredits = [];
        $credits = $this->input->post("credits");

        foreach ($credits as $credit) {
            if ($this->Financing_model->update(["notified_credit" => YES, "date_notified" => date("Y-m-d H:i:s")], $credit) == false) {
                $unnotifiedCredits[] = $credits;
                $message += "{$credit}, ";
            }
        }

        if (!empty($message)) {
            $message = "Los siguientes Créditos no pudieron ser Notificados: ".trim($message, ', ');
        }

        echo json_encode([
            "message" => $message
        ]);
    }

    public function reportCredits()
    {
        $message = "";
        $unnotifiedCredits = [];
        $credits = $this->input->post("credits");

        foreach ($credits as $credit) {
            if ($this->Financing_model->update(["credit_status" => 2, "date_notified" => date("Y-m-d H:i:s")], $credit) == false) {
                $unnotifiedCredits[] = $credits;
                $message += "{$credit}, ";
            }
        }

        if (!empty($message)) {
            $message = "Los siguientes Créditos no pudieron ser Reportados: ".trim($message, ', ');
        }

        echo json_encode([
            "message" => $message
        ]);
    }

    public function initialReport()
    {
        $this->load->helper("file");

        $path = 'files/financing';
        if (!is_dir($path)) {
            mkdir($path, 0755);
        }

        $content = $this->getContent();
        $date = date("Ymd");
        $filename = "InformeProcredito_$date.txt";
        $filePath = "$path/$filename";

        write_file($filePath, $content);

        if (file_exists($filePath)) {
            // Enviar las cabeceras para la descarga del archivo
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($filePath) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));

            // Leer el archivo y enviar su contenido al navegador
            readfile($filePath);

            // Detener el resto de la ejecución del script
            exit;
        } else {
            echo "Error: No se pudo crear el archivo.";
        }
    }

    public function getContent()
    {
        $text = "";
        $header = [];

        $installments = $this->Financing_model->getInitialReportData();
        if (!empty($installments)) {
            // $this->sma->print_arrays($installments);
            foreach ($installments as $installment) {
                if (!in_array($installment->financing_credit_id, $header)) {
                    $header[] = $installment->financing_credit_id;

                    $financing = $this->Financing_model->find(["f.id" => $installment->financing_credit_id]);
                    if (!empty($financing)) {
                        $c4 = date('dmY');
                        $c5 = "1080005";
                        $c6 = !empty($financing->affiliate_consecutive) ? $financing->affiliate_consecutive : "";
                        $c7 = !empty($financing->old_branch_code) ? $financing->old_branch_code : "";
                        if ($this->Settings->tipo_documento == 13) {
                            $c8 = "1";
                        } else if ($this->Settings->tipo_documento == 31) {
                            $c8 = "2";
                        } else if ($this->Settings->tipo_documento == 21) {
                            $c8 = "3";
                        }
                        $c9 = $this->Settings->numero_documento;
                        if (!empty($financing->document_code)) {
                            if ($financing->document_code == 13) {
                                $c12 = "1";
                            } else if ($financing->document_code == 31) {
                                $c12 = "2";
                            } else if ($financing->document_code == 21) {
                                $c12 = "3";
                            }
                        } else {
                            $c12 = "";
                        }

                        $c13 = $financing->vat_no;
                        $c14 = ($financing->type_person == 2) ? $financing->first_name : $financing->company;
                        $c15 = !empty($financing->second_name) ? $financing->second_name : "";
                        $c16 = $financing->first_lastname;
                        $c17 = !empty($financing->second_lastname) ? $financing->second_lastname : "";
                        $c20 = substr($financing->city_code, 3, 2);
                        $c21 = substr($financing->city_code, 5, 3);
                        $c23 = !empty($financing->address) ? str_replace(".", "",$financing->address) : "";
                        $c25 = !empty($financing->phone) ? $financing->phone : "";
                        $c28 = $financing->email;
                        $c33 = $financing->credit_no;
                        $c34 = date("dmY", strtotime($financing->credit_date));
                        $c35 = ($financing->credit_term_freq == 2) ? "30" : "15";
                        $c39 = $financing->capital_amount + $financing->current_interest_amount + $financing->interest_tax;
                        $c41 = $financing->paid;
                        $c42 = $financing->capital_amount - $financing->paid;
                        $c43 = $financing->number_installments;
                        $c44 = $financing->number_installment_paid;
                        $c45 = $financing->number_overdue_installments;

                        $text .= "E|1||$c4|$c5|$c6|$c7|$c7|$c8|$c9|$c7|1|$c12|$c13|$c14|$c15|$c16|$c17|".
                        "|57|$c20|$c21|1|$c23|2|$c25||0|$c28|||6|1|$c33|$c34|$c35||||$c39|0|$c41|".
                        "$c42|$c43|$c44|$c45\r\n";
                    }
                }

                $column3 = $installment->id;
                $column4 = $installment->installment_no;
                $column5 = $installment->capital_amount + $installment->current_interest_amount + $installment->interest_tax;
                $column6 = ($installment->status == YES) ? $column5 : 0;
                $column8 = $installment->default_days;
                $column10 = date("dmY", strtotime($installment->installment_due_date));

                $text .= "D|6|$column3|$column4|$column5|$column6|2|$column8||$column10\r\n";
            }
        }

        return $text;
    }
}
