<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?= admin_form_open('system_settings/warehouse_actions', 'id="action-form"') ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?php echo admin_url('system_settings/add_warehouse'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus"></i> <?= lang('add_warehouse') ?>
                                        </a>
                                        </li>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#" id="delete" data-action="delete">
                                            <i class="fa fa-trash-o"></i> <?= lang('deactivate_warehouses') ?>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="CURData" class="table table-hover table-condensed reports-table">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkth" type="checkbox" name="check"/>
                                        </th>
                                        <th style="min-width:40px; width: 40px; text-align: center;"><?= lang("map"); ?></th>
                                        <th class="col-xs-1"><?= lang("code"); ?></th>
                                        <th class="col-xs-2"><?= lang("name"); ?></th>
                                        <th class="col-xs-2"><?= lang("type"); ?></th>
                                        <th class="col-xs-2"><?= lang("price_group"); ?></th>
                                        <th class="col-xs-2"><?= lang("phone"); ?></th>
                                        <th class="col-xs-2"><?= lang("email"); ?></th>
                                        <th class="col-xs-3"><?= lang("address"); ?></th>
                                        <th class="col-xs-3"><?= lang("status"); ?></th>
                                        <th style="width:65px;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>

<script>
    $(document).ready(function () {
        function tax_type(x) {
            return (x == 1) ? "<?=lang('percentage')?>" : "<?=lang('fixed')?>";
        }

        oTable = $('#CURData').dataTable({
            "aaSorting": [[2, "asc"], [3, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('system_settings/getWarehouses') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex)
            {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "warehouse_link";
                return nRow;
            },
            "aoColumns": [{"bSortable": false, "mRender": checkbox}, { "bSortable": false, "mRender": img_hl }, null, null, {"mRender": wh_type } , null, null, null, null, {"mRender": generalStatus }, {"bSortable": false}]
        });

        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

    });
</script>
