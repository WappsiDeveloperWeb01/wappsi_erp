<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="PData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th><?= lang("title"); ?></th>
                                            <th><?= lang("type"); ?></th>
                                            <th><?= lang("profile"); ?></th>
                                            <th><?= lang("path"); ?></th>
                                            <th><?= lang("ip_address"); ?></th>
                                            <th><?= lang("port"); ?></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#PData').dataTable({
            "aLengthMenu": [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, '<?= lang('all'); ?>']
            ],
            "aaSorting": [
                [0, "asc"]
            ],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('pos/get_printers') ?>',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            "aoColumns": [null, null, null, null, null, null, {
                "bSortable": false,
                "bSearchable": false
            }]
        }).fnSetFilteringDelay();
    });
</script>