<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <?= admin_form_open_multipart("system_settings/add_document_type/" . '', ['id' => 'add_document_type']); ?>
            <?= form_hidden('add', '1'); ?>
            <input type="hidden" name="electronic_billing" id="electronic_billing" value="<?= $this->Settings->electronic_billing; ?>">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?= lang('add_document_type'); ?></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="language"><?= lang("language"); ?></label>
                            <?php $langOpts = ['spanish' => lang('spanish'), 'english' => lang('english')]; ?>
                            <?= form_dropdown('language', $langOpts, $this->config->item('language'), 'class="form-control" id="language" style="width:100%;"'); ?>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang('document_type_module', 'module') ?>
                        <?php
                        $mopts = ['' => lang('select')];
                        foreach (lang('modules') as $module => $text) {
                            $mopts[$module] = $text;
                        }
                        ?>
                        <?= form_dropdown('module', $mopts, '', 'class="form-control" id="module" style="width:100%;" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang('document_type_nombre', 'nombre_doctype') ?>
                        <?= form_input('nombre_doctype', '', 'class="form-control" id="nombre_doctype" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang('document_type_sales_prefix', 'sales_prefix') ?>
                        <?= form_input('sales_prefix', '', 'class="form-control" id="sales_prefix" required="required"'); ?>
                        <em class="text-danger prefix_exists" style="display: none;"><?= lang('sales_prefix_already_exists') ?></em>
                    </div>
                </div>

                <div class="row">
                    <?php if($this->Owner): ?>
                        <div class="form-group col-md-4">
                            <?= lang('initial_resolution_wappsi','initial_resolution_wappsi') ?>
                            <?php echo form_input('inicio_resolucion_wappsi', '1', 'class="form-control is_required id="inicio_resolucion_wappsi" required="required"'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="form-group col-md-4">
                        <?= lang('document_type_sales_consecutive', 'sales_consecutive') ?>
                        <?= form_input('sales_consecutive', '', 'class="form-control only_number" id="sales_consecutive" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang('document_type_module_invoice_format', 'module_invoice_format_id') ?>
                        <?= form_dropdown('module_invoice_format_id', ['' => lang('select')], '', 'class="form-control" id="module_invoice_format_id" style="width:100%;"'); ?>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="quick_print_format_id"><?= lang("quick_print_format_id"); ?></label>
                            <?php $qpfopts = ['' => lang('select')]; ?>
                            <?= form_dropdown('quick_print_format_id', $qpfopts, '', 'class="form-control" id="quick_print_format_id" style="width:100%;"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-4 save_resolution_sale_section">
                        <label><?= lang('save_resolution_in_sale') ?></label><br>
                        <label>
                            <input type="radio" name="save_resolution_in_sale" id="save_resolution_in_sale_yes" value="1" checked>
                            <?= lang('yes') ?>
                        </label>
                        <label>
                            <input type="radio" name="save_resolution_in_sale" id="save_resolution_in_sale_no" value="0">
                            <?= lang('no') ?>
                        </label>
                    </div>

                    <div class="form-group col-md-4">
                        <?= form_label(lang('document_factura_electronica'), 'factura_electronica'); ?>
                        <?= form_dropdown('factura_electronica', [NOT => lang('no'), YES => lang('yes')], '', 'class="form-control is_required" id="factura_electronica" style="width:100%;" required="required"'); ?>
                    </div>

                    <div class="form-group col-md-4 mode_section">
                        <label for="mode"><?= lang('mode') ?> *</label>
                        <?= form_dropdown('mode',   [
                                                        '' => 'Seleccione',
                                                        '1' => 'FACTURA ELECTRÓNICA DE VENTA', 
                                                        '2' => 'DOCUMENTO EQUIVALENTE POS ELECTRÓNICO',
                                                    ], 
                                                    '', 'class="form-control" id="mode" 
                                                    style="width:100%;" '); ?>
                    </div>

                    <?php if ($this->Settings->electronic_billing == YES) { ?>
                        <?php if ($this->Settings->fe_technology_provider == DELCOP) { ?>
                            <div class="form-group col-md-4" id="transaction_id_container">
                                <?= form_label(lang("document_type_transaction_id"), "transaction_id"); ?>
                                <?= form_input(["class" => "form-control", "name" => "transaction_id", "id" => "transaction_id", "required" => "required"]); ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->Settings->electronic_billing == YES) { ?>
                        <div class="form-group col-md-4 sale_section" id="contingency_invoice_container">
                            <?= lang("contingency_invoice", "contingency_invoice"); ?>
                            <select class="form-control" name="contingency_invoice" id="contingency_invoice" required="required">
                                <option value="<?= NOT; ?>"><?= lang("no"); ?></option>
                                <option value="<?= YES; ?>"><?= lang("yes"); ?></option>
                            </select>
                        </div>
                    <?php } ?>

                    <div class="form-group col-md-4 sale_section support_section">
                        <?= lang('document_type_num_resolucion', 'num_resolucion') ?>
                        <div class="input-group">
                            <?= form_input('num_resolucion', '', 'class="form-control is_required is_support_required" id="num_resolucion" required="required"'); ?>
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-group-addon" id="consultResolution" type="button" data-toggle="tooltip" data-placement="top" title="Consultar datos de resolución"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="form-group col-md-4 sale_section support_section">
                        <?= lang('document_type_inicio_resolucion', 'inicio_resolucion') ?>
                        <?= form_input('inicio_resolucion', '', 'class="form-control only_number is_required is_support_required" id="inicio_resolucion" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-4 sale_section support_section">
                        <?= lang('document_type_fin_resolucion', 'fin_resolucion') ?>
                        <?= form_input('fin_resolucion', '', 'class="form-control only_number is_required is_support_required" id="fin_resolucion" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-4 sale_section support_section">
                        <?= lang('document_type_emision_resolucion', 'emision_resolucion') ?>
                        <input type="date" name="emision_resolucion" id="emision_resolucion" class="form-control is_required is_support_required" value="<?= '' ?>" max="2099-01-01" required='required'>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-4 sale_section support_section">
                        <?= lang('document_type_vencimiento_resolucion', 'vencimiento_resolucion') ?>
                        <input type="date" name="vencimiento_resolucion" id="vencimiento_resolucion" class="form-control is_required is_support_required" value="<?= '' ?>" max="2099-01-01" required='required'>
                    </div>
                    <div class="form-group col-md-4 sale_section">
                        <?= lang('document_type_palabra_resolucion', 'palabra_resolucion') ?>
                        <?php
                        $fopts = ['' => lang('select'), 1 => lang('Habilita'), 2 => lang('Autoriza')];
                        ?>
                        <?= form_dropdown('palabra_resolucion', $fopts, '', 'class="form-control is_required" id="palabra_resolucion" style="width:100%;" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-4 sale_section">
                        <?= lang('document_type_word_type_sale', 'word_type_sale') ?>
                        <?= form_input('word_type_sale', '', 'class="form-control" id="word_type_sale"'); ?>
                    </div>
                </div>

                <div class="row">
                    <?php if ($this->Settings->electronic_billing == YES) { ?>
                        <div class="form-group col-md-4 sale_section" id="contenedor_clave_tecnica">
                            <?= lang('document_type_technical_key', 'clave_tecnica'); ?>
                            <input type="text" name="clave_tecnica" id="clave_tecnica" class="form-control">
                        </div>

                        <div class="electronic_billing_container">
                            <div class="form-group col-md-4 sale_section">
                                <?php if ($this->Settings->fe_work_environment == PRODUCTION) { ?>
                                    <?= form_label(lang("document_type_work_enviroment"), "work_environment"); ?>
                                    <select class="form-control" name="work_environment" id="work_environment" <?= ($this->Settings->fe_work_environment == TEST) ? "disabled" : ""; ?>>
                                        <option value="<?= PRODUCTION; ?>"><?= lang("production"); ?></option>
                                        <option value="<?= TEST; ?>"><?= lang("test"); ?></option>
                                    </select>
                                <?php } ?>
                                <?php if ($this->Settings->fe_work_environment == TEST) { ?>
                                    <input type="hidden" name="work_environment" id="work_environment" value="<?= TEST; ?>">
                                <?php } ?>
                            </div>

                            <div id="testid_container" class="sale_section">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="testid"><?= lang("document_type_testid"); ?></label>
                                        <div style="display: flex; align-items: center;">
                                            <input class="form-control" type="text" name="testid" id="testid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 events_simba">
                                <label for=""><?= lang('sync_numbering') ?> </label>
                                <br>
                                <input
                                    type="checkbox" 
                                    name="getNumbertin_simba_status" 
                                    id="getNumbertin_simba_status" >
                            </div> 
                        </div>
                    <?php } ?>
                </div>

                <div class="row">
                    <div class="col-sm-12" id="highlightedTextContainer" style="display: none">
                        <div class="form-group">
                            <?= lang("highlighted_text", "highlighted_text") ?>
                            <input type="text" class="form-control" name="highlighted_text" id="highlighted_text">
                        </div>
                    </div>

                    <div class="form-group col-md-12 sale_section quote_section inv_footer_section">
                        <?= lang('document_type_invoice_footer', 'invoice_footer') ?>
                        <textarea name="invoice_footer" class="form-control" id="invoice_footer"><?= '' ?></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12 sale_section inv_footer_section">
                        <?= lang('document_type_invoice_header', 'invoice_header') ?>
                        <textarea name="invoice_header" class="form-control" id="invoice_header"><?= '' ?></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('add_consecutive_left_zeros', 'add_consecutive_left_zeros') ?>
                            <?= form_dropdown('add_consecutive_left_zeros', [NOT => lang('no'), NOT => lang('yes')], YES, 'class="form-control is_required" id="add_consecutive_left_zeros" style="width:100%;" required="required"'); ?>
                        </div>
                    </div>

                    <?php if ($this->Settings->electronic_billing == YES && $this->Settings->fe_technology_provider == BPM) : ?>
                        <?php if (isset($instances)) : ?>
                            <div class="col-md-4" id="instancesContainer">
                                <div class="form-group">
                                    <label for="instance"><?= $this->lang->line("instances") ?></label>
                                    <select class="form-control" name="instance" id="instance" required>
                                        <option value=""><?= $this->lang->line("select") ?></option>
                                        <?php foreach ($instances as $instance) : ?>
                                            <option value="<?= $instance->id ?>"><?= $instance->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        <?php endif  ?>
                    <?php endif  ?>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary new-button submit" type="button" data-toggle='tooltip' data-placement='top' title="<?= $this->lang->line('submit') ?>"><i class="fas fa-check"></i></button>
            </div>
        <?= form_close(); ?>
    </div>
</div>

<?= $modal_js ?>
<?= $documents_types_js ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $('#instancesContainer').fadeOut();

        $("#add_document_type").validate({
            ignore: []
        });

        $(document).on('change', '#module', function() {
            get_invoice_formats();
            if ($(this).val() == 8) {
                $('.inv_footer_section').css('display', 'block');
            }

            showHidehighlightedTextInput($(this).val())
        });
        $(document).on('change', '#factura_electronica', function() {
            show_hide_electronic_billing_fields();
            enabledDisabledConsultResolutionsButton($(this).val());
        });
        $(document).on('change', '#contingency_invoice', function() {
            show_hide_electronic_billing_fields();
        });
        $(document).on('change', '#work_environment', function() {
            show_hide_testid_input();
        });
        $(document).on('keyup', '#sales_prefix', function() {
            validate_sales_prefix();
        });
        $(document).on('click', '.submit', function() {
            save_document_type();
        });

        $('#module').trigger('change');
        $('#factura_electronica').trigger('change');
        $('#work_environment').trigger('change');

        $(document).on('click', '#consultResolution', function() { consultResolution(); });

        if (site.settings.fe_technology_provider == 3) {
            $('.events_simba').css('display', 'block');
        }else{
            $('.events_simba').css('display', 'none');
        }
    });

    function get_invoice_formats()
    {
        var allowedModules = ['1','2','3'];
        var module_selected = $('#module').val();
        if (module_selected != '') {
            $.ajax({
                dataType: 'json',
                url: '<?= admin_url('system_settings/get_invoice_formats') ?>/' + module_selected
            }).done(function(data) {
                $('#module_invoice_format_id').html(data.options);
                $('#module_invoice_format_id').select2('val', 'NULL');
                $('#quick_print_format_id').html(data.quick_print_options);
                $('#quick_print_format_id').select2('val', 'NULL');
            });
        }

        if (allowedModules.includes(module_selected)) {
            $('.sale_section').css('display', '');
            $('.save_resolution_sale_section').css('display', 'block');
            $('.is_required').each(function(index, input) {
                $(input).prop('required', true);
            });
        } else {
            $('.sale_section').css('display', 'none');
            $('.save_resolution_sale_section').css('display', 'none');
            $('.is_required').each(function(index, input) {
                $(input).prop('required', false);
            });
            $('.is_support_required').each(function(index, input) {
                $(input).prop('required', false);
            });
            if (module_selected == 7 ) { // <- here into if module selected is quote
                $('.quote_section').css('display', 'block');
            }
        }
        if (module_selected == 35) {
            $('.support_section').css('display', 'block');
            $('.is_support_required').each(function(index, input) {
                $(input).prop('required', true);
            });
        }
        $('#factura_electronica').trigger('change');
    }

    function save_document_type()
    {
        if ($('#add_document_type').valid()) {
            $('#add_document_type').submit();
        } else {
            set_same_height('col-md-4');
        }
    }

    function show_hide_electronic_billing_fields()
    {
        var module_selected = $('#module').val();
        var factura_electronica = $('#factura_electronica').val();
        var factura_contingencia = $('#contingency_invoice').val();
        if (factura_electronica == '<?= YES; ?>') {
            if (module_selected == 1 || module_selected == 2 || module_selected == 35) {
                $('#contingency_invoice_container').fadeIn();
                $('#contingency_invoice').attr('required', 'required');

                if (factura_contingencia == '<?= NOT; ?>') {
                    $('#contenedor_clave_tecnica').fadeIn();
                    $('#clave_tecnica').attr('required', 'required');

                    $('.electronic_billing_container').fadeIn();
                    $('#work_environment').attr('required', 'required');
                } else {
                    $('#contenedor_clave_tecnica').fadeOut();
                    $('#clave_tecnica').removeAttr('required');

                    $('.electronic_billing_container').fadeOut();
                    $('#work_environment').removeAttr('required');
                }
            } else {
                $('#contenedor_clave_tecnica').fadeOut();
                $('#clave_tecnica').removeAttr('required');

                $('.electronic_billing_container').fadeOut();
                $('#work_environment').removeAttr('required');
                $('#testid').removeAttr('required');
            }

            if (module_selected == 48 || module_selected == 49 || module_selected == 50 || module_selected == 51) {
                $('#transaction_id_container').fadeOut();
                $('#transaction_id').attr('required', false);
            } else {
                $('#transaction_id_container').fadeIn();
                $('#transaction_id').attr('required', 'required');
            }
            show_hide_testid_input();

            <?php if ($this->Settings->fe_technology_provider == BPM) { ?>
                $('#contenedor_clave_tecnica').fadeOut();
                $('#clave_tecnica').removeAttr('required');
            <?php } ?>
        } else {
            $('#contingency_invoice_container').fadeOut();
            $('#contingency_invoice').removeAttr('required');
            $('#transaction_id_container').fadeOut();
            $('#transaction_id').removeAttr('required');
            $('#contenedor_clave_tecnica').fadeOut();
            $('#clave_tecnica').removeAttr('required');
            $('#testid').removeAttr('required');
            $('#work_environment').removeAttr('required');
            $('.electronic_billing_container').fadeOut();
        }

        if (['1','2','3','4','26','27','35','52'].includes(module_selected) && module_selected && (factura_electronica == '<?= YES; ?>')) {
            $('#instancesContainer').fadeIn();
            $('#instance').attr('required', true);
        } else {
            $('#instancesContainer').fadeOut();
            $('#instance').removeAttr('required');
        }
        show_mode();
    }

    function show_hide_testid_input()
    {
        var work_environment = $('#work_environment').val();
        var module_selected = $('#module').val();
        if (work_environment == '<?= TEST; ?>') {
            <?php if ($this->Settings->fe_work_environment == TEST) { ?>
                $('#testid').attr('required', 'required');
                $('#testid_container').fadeIn();
            <?php } ?>
            if (module_selected == 1 || module_selected == 2) {
                $('#testid').attr('required', 'required');
                $('#testid_container').fadeIn();
            } else {
                $('#testid').removeAttr('required');
                $('#testid_container').fadeOut();
            }
        } else {
            $('#testid').removeAttr('required');
            $('#testid_container').fadeOut();
        }
    }

    function validate_sales_prefix()
    {
        prefix = $('#sales_prefix').val();
        if (prefix) {
            $.ajax({
                url: site.base_url + 'system_settings/validate_document_type_prefix/' + prefix,
                dataType: 'JSON'
            }).done(function(data) {
                if (data.response == true) {
                    $('.prefix_exists').fadeIn();
                    $('.submit').fadeOut();
                } else {
                    $('.prefix_exists').fadeOut();
                    $('.submit').fadeIn();
                }
            });
        }
    }

    $(document).on('ifChecked', '#save_resolution_in_sale_yes', function() {
        var save_resolution_in_sale = $(this).val();
        $('.sale_section').css('display', 'block');
        $('.is_required').each(function(index, input) {
            $(input).prop('required', true);
        });
    });

    $(document).on('ifChecked', '#save_resolution_in_sale_no', function() {
        var save_resolution_in_sale = $(this).val();
        $('.sale_section').css('display', 'none');
        $('.is_required').each(function(index, input) {
            $(input).prop('required', false);
        });
        $('.is_support_required').each(function(index, input) {
            $(input).prop('required', false);
        });
        $('.inv_footer_section').css('display', 'block');
    });

    function enabledDisabledConsultResolutionsButton(isElectronic)
    {
        if (isElectronic == 1) {
            $('#consultResolution').prop('disabled', false);
            // $('#security_token').attr('required', 'required');
        } else {
            $('#consultResolution').prop('disabled', true);
            // $('#security_token').removeAttr('required');
        }
    }

    function consultResolution()
    {
        let numberResolucion = $('#num_resolucion').val();
        let modules = $('#module').val();
        if (numberResolucion == '') {
            swal({
                title: "¡Error al consultar resolución!",
                text: "Por favor ingrese el número de la resolución.",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            });

            return false;
        }

        $.ajax({
            type: "post",
            url: site.base_url+'system_settings/getResolutionsWS',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'resolutionNumber': numberResolucion,
                'module': modules
            },
            dataType: "json",
            success: function (response) {
                if (Boolean(response) == true) {
                    $('#inicio_resolucion').val(response.fromNumber);
                    $('#sales_prefix').val(response.prefix);
                    $('#fin_resolucion').val(response.toNumber);
                    $('#emision_resolucion').val(response.validDateTimeFrom);
                    $('#vencimiento_resolucion').val(response.validDateTimeTo);
                    $('#clave_tecnica').val(response.technicalKey);
                }
            }
        });
    }
    
    $(document).on('ifChecked', '#getNumbertin_simba_status', function() {        
        $.ajax({
            url: '<?= admin_url("system_settings/getNumbering/"); ?>',
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
            },
        })
        .done(function(data) {
            if (data.response == false) {
                header_alert('warning', data.message)
                return false;
            }else{
                $("#getNumbertin_simba_status").attr('disabled', true);
                header_alert('success', data.message)
                return false;
            }
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    })

    function showHidehighlightedTextInput(module)
    {
        if (['1','2','3','4'].includes(module)) {
            $('#highlightedTextContainer').fadeIn();
        } else {
            $('#highlightedTextContainer').fadeOut();
            $('#highlighted_text').val('');
        }
    }
</script>
