<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">

</style>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <div class="col-xs-12 no-printable text-right">
                <button onclick="imprimir_factura();"> <span class="fa fa-print"></span> <?= lang("print") ?></button>
            </div>
            <?php if ($biller->logo) { ?>
                <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                     alt="<?= $Settings->site_name; ?>">
            <?php } ?>
            <div class="clearfix"></div>
            <div class="row padding10">
                <div class="col-xs-5">
                    <h2 class=""><?= $Settings->site_name; ?></h2>

                    <div class="clearfix"></div>
                </div>
            </div>
            <div style="clear: both;"></div>
            <p>&nbsp;</p>

            <div>
                <table class="table" id="contenido_factura" style="margin-bottom:0;">
                    <tbody>
                        <tr>
                            <td><strong><?= lang("date"); ?></strong></td>
                            <td><strong class="text-right"><?php echo $this->sma->hrld($payment_collection[0]->date); ?></strong></td>
                        </tr>
                        <tr>
                            <td><strong><?= lang("reference"); ?></strong></td>
                            <td><strong class="text-right"><?php echo $payment_collection[0]->reference_no; ?></strong></td>
                        </tr>
                        <tr>
                            <td><strong><?= lang("biller"); ?></strong></td>
                            <td><strong class="text-right"><?php echo $biller->name; ?></strong></td>
                        </tr>
                        <tr>
                            <td><strong><?= lang("customer"); ?></strong></td>
                            <td><strong class="text-right"><?php echo $payment_collection[0]->customer_name; ?></strong></td>
                        </tr>
                        <?php foreach ($payment_collection as $pc): ?>
                            <tr>
                                <td><strong><?= lang("paid_by"); ?></strong></td>
                                <td><strong class="text-right"><?php echo lang($pc->paid_by); ?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td><strong><?= lang("amount"); ?></strong></td>
                                <td><strong class="text-right"><?php echo $this->sma->formatMoney($pc->amount); ?></strong>
                                </td>
                            </tr>
                        <?php endforeach ?>
                            
                        <?php if (isset($cost_center) && $cost_center): ?>
                            <tr>
                                <td><strong><?= lang("cost_center"); ?></strong></td>
                                <td><strong class="text-right"><?= $cost_center->name ?> ( <?= $cost_center->code ?> )</strong>
                                </td>
                            </tr>
                        <?php endif ?>
                        <tr>
                            <td colspan="2"><?php echo $payment_collection[0]->note; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="clear: both;"></div>
            <div class="row">
                <div class="col-xs-4 pull-left">
                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p style="border-bottom: 1px solid #666;">&nbsp;</p>

                    <p><?= lang("stamp_sign"); ?></p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function imprimir_factura(){
        window.open('<?= admin_url("payments_collections/view/").$payment_collection[0]->reference_no ?>');
    }
</script>