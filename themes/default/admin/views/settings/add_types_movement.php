<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?></h3>
        </div>
        <?= admin_form_open("system_settings/save_types_movement", ["id"=>"add_types_movement_form"]); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= form_label(lang("types_movement_type")); ?>
                <?php $mopts = [""=>lang('select'), "adjustment"=>"Ajuste", "order_production"=>"Orden de producción", "transfer"=>"Traslado"]; ?>
                <?= form_dropdown(["name"=>"movement", "id"=>"movement", "class"=>"form-control select2", "required"=>TRUE], $mopts); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("types_movement_accounting_account_name", "ledger_id")); ?>
                <?php
                    $lopts[''] = lang('select');

                    foreach ($ledgers as $ledger) {
                        $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                    }
                ?>
                <?= form_dropdown(["name"=>"ledger_id", "id"=>"ledger_id", "class"=>"form-control select2", "required"=>TRUE], $lopts); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("types_movement_counterparty_account_name", "offsetting_account")); ?>
                <?php
                    $lopts[''] = lang('select');

                    foreach ($ledgers as $ledger) {
                        $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                    }
                ?>
                <?= form_dropdown(["name"=>"offsetting_account", "id"=>"offsetting_account", "class"=>"form-control select2", "required"=>TRUE], $lopts); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("types_movement_document_type_name", "document_type_id")); ?>
                <?php
                    $dtopts[''] = lang('select');

                    foreach ($document_types as $document_type) {
                        $dtopts[$document_type->id] = $document_type->sales_prefix ." - ". $document_type->nombre;
                    }
                ?>
                <?= form_dropdown(["name"=>"document_type_id", "id"=>"document_type_id", "class"=>"form-control select2", "required"=>TRUE], $dtopts); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?= form_button("type_movement_add", lang("save"), 'class="btn btn-primary" id="type_movement_add"'); ?>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script>
    $(document).ready(function() {
        $("#add_types_movement_form").validate({ ignore: [] });

        $(document).on('click', '#type_movement_add', function() {
            if ($('#add_types_movement_form').valid()) {
                $('#add_types_movement_form').submit();
            }
        });
    });
</script>
