<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i> </button>
            <h4 class="modal-title" id="myModalLabel">Agregar mesas</h4>
        </div>
        <div class="modal-body">
        	<?= form_open(admin_url("System_settings/save_multiple_table"), ["id"=>"save_multiple_table_form"]) ?>
        		<div class="row">
        			<?php foreach ($tables as $key => $table) { ?>
        				<?php if ($table->tipo == 'M') { $label = 'Mesas físicas'; } else if ($table->tipo == 'D') { $label = 'Domicilio'; } else { $label = 'Cliente recoje'; } ?>
        				<div class="col-sm-4">
        					<div class="input-group">
        						<label for=""><?= $label; ?></label>
        						<input class="form-control table_to_add" type="number" name="table_<?= $table->tipo; ?>">
        						<input type="hidden" name="current_number_<?= $table->tipo; ?>" id="current_number_<?= $table->tipo; ?>" value="<?= $table->numero; ?>">
        						<input type="hidden" name="table_area_<?= $table->tipo; ?>" id="table_area_<?= $table->tipo; ?>" value="<?= $table->area_id; ?>">
        						<p class="text-success">Número actual: <?= $table->numero; ?> mesas</p>
        					</div>
        				</div>
        			<?php } ?>
        		</div>
        	<?= form_close(); ?>
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cerrar</button>
        	<button type="button" class="btn btn-primary" id="save_multiple_table"><i class="fa fa-check"></i> Guardar</button>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
		$(document).on('click', '#save_multiple_table', function() { save_multiple_table(); });
	});

	function save_multiple_table()
	{
		if (validate_inputs()) {
			$("#save_multiple_table_form").submit();
		}
	}

	function validate_inputs()
	{
		var required = true;
		$("#save_multiple_table_form").find(':input[type=number]').each(function() {
         	if ($(this).val() != '') {
         		required = false;
         	}
        });

        if (required == true) {
        	Command: toastr.error('Por ingrese al menos la cantidad de mesas a crear en alguna de las areas.', '¡Error!');
        	return false;
        } else {
        	return true;
        }
	}
</script>