<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?= admin_form_open('system_settings/brand_actions', 'id="action-form"') ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="BrandTable" class="table table-hover reports-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input class="checkbox checkth" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("image"); ?></th>
                                            <th><?= lang("code"); ?></th>
                                            <th><?= lang("name"); ?></th>
                                            <th><?= lang("slug"); ?></th>
                                            <th><?= lang("synchronized"); ?></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7" class="dataTables_empty">
                                                <?= lang('loading_data_from_server') ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action" />
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>

<script>
    $(document).ready(function() {
        oTable = $('#BrandTable').dataTable({
            aaSorting: [
                [3, "asc"]
            ],
            aLengthMenu: [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('system_settings/getBrands') ?>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            aoColumns: [{
                bSortable: false,
                mRender: checkbox,
                sWidth: '30px',
                className: 'text-center',
            }, {
                bSortable: false,
                mRender: img_hl,
                sWidth: '40px',
                className: 'text-center',
            }, null, null, null,
            {
                sWidth: '65px',
                className: 'text-center',
                mRender: function(data, type, row) {
                    if (data == 1) {
                        return '<i class="fas fa-check-circle fa-2x text-success"></i>';
                    } else {
                        return '<i class="fas fa-ban fa-2x text-danger"></i>';
                    }
                }
            },
            {
                bSortable: false,
                sWidth: '65px',
                className: 'text-center'
            }],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html('<a href="<?= admin_url('system_settings/add_brand') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>'+
                '<div class="pull-right dropdown">'+
                    '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                    '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                        '<li>'+
                            '<a href="<?= admin_url('system_settings/import_brands'); ?>" data-toggle="modal" data-target="#myModal">'+
                                '<i class="fa fa-plus"></i> <?= lang('import_brands') ?>'+
                            '</a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="#" id="excel" data-action="export_excel">'+
                                '<i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>'+
                            '</a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="#" id="fix_slug" data-action="fix_slug">'+
                                '<i class="fa-solid fa-wand-magic-sparkles"></i> <?= lang('fix_slug') ?>'+
                            '</a>'+
                        '</li>'+
                        '<li>'+
                            '<a id="syncStore">'+
                                '<i class="fas fa-sync"></i> <?= lang('sync_store') ?>'+
                            '</a>'+
                        '</li>'+
                        '<li class="divider"></li>'+
                        '<li>'+
                            '<a href="#" id="delete" data-action="delete">'+
                                '<i class="fa fa-trash-o"></i> <?= sprintf(lang('delete_brands'), lang('brands')) ?>'+
                            '</a>'+
                        '</li>'+
                    '</ul>'+
                '</div>');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });

        $('#delete').click(function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#fix_slug').click(function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });


        $(document).on('click', '#syncStore', syncStore);
    });


    function syncStore()
    {
        brandIds = validateSelectedBrands();

        if (brandIds != false) {
            swal({
                title: '¡Sincronización en curso',
                text: "Por favor espere un momento mientras se realiza la sincronización",
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            $.ajax({
                type: "post",
                url: site.base_url + 'system_settings/syncBrandsStore',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'brandIds':  brandIds,
                },
                dataType: "json",
                success: function (response) {
                    let alert = '';

                    if (response.length !== 0) {
                        $.each(response, function (type, message) {
                            if (type == 'warning') {
                                header_alert(type, message, null, function () { swal.close(); window.location.reload(); });
                            }
                        });
                    } else {
                        header_alert('success', '¡Sincronización completada!', null, function () {  swal.close(); window.location.reload(); });
                    }
                }
            });
        }
    }

    function validateSelectedBrands() {
        let productIds = [];
        $('input[name="val[]"]').each(function() {
            if ($(this).is(":checked")) {
                productIds.push($(this).val());
            }
        });

        if (productIds.length === 0) {
            swal("Advertencia!", "No ha seleccionado ninguna Marca para sincronizar.", "warning");
            return false;
        }

        return productIds;
    }
</script>