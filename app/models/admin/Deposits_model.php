<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Deposits_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($data)
    {
        $this->db->order_by('date');
        $this->db->where($data);
        $q = $this->db->get("deposits");
        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }

    public function update($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("deposits", $data)) {
            return true;
        }
        return false;
    }
}
