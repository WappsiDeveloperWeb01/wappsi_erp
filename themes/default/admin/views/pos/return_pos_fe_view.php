<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if ($modal) { ?>

<style type="text/css">
    @media print {

      #wrapper {
        padding: 0px !important;
        margin: 0px !important;
      }
      #receiptData {
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-body{
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-content{
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-dialog{
        padding: 0px !important;
        margin: 0px !important;
      }

    }
</style>

<div class="modal-dialog no-modal-header" role="document"><div class="modal-content"><div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
    <?php
} else {
    ?><!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=$page_title . " " . lang("no") . " " . $inv->id;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
        </style>
    </head>

    <body>
        <?php
    } ?>
    <div id="wrapper">
        <div id="receiptData">

            <div class="no-print">
                <?php
                if ($message) {
                    ?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=is_array($message) ? print_r($message, true) : $message;?>
                    </div>
                    <?php
                } ?>

            </div>
            <div>
                <div class="text-center">
                    <?= !empty($biller->logo) ? '<img src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                    <h4 style="text-transform:uppercase;"><?=$this->Settings->nombre_comercial;?></h4>
                    <?php
                    echo "<p>" . $biller->address . " " . $biller->city . " " . $biller->postal_code . " " . $biller->state . " " . $biller->country .
                    "<br>" . lang("tel") . ": " . $biller->phone;

                    echo "<br>";

                    echo "NIT : ".$this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '');
                    if ($ciiu_code) {
                        echo "</br> Códigos CIIU : ".$ciiu_code."</br>";
                    }

                    echo "<br>";
                    if ($pos_settings->cf_title1 != "" && $pos_settings->cf_value1 != "") {
                        echo $pos_settings->cf_title1 . ": " . $pos_settings->cf_value1 . "<br>";
                    }
                    if ($pos_settings->cf_title2 != "" && $pos_settings->cf_value2 != "") {
                        echo $pos_settings->cf_title2 . ": " . $pos_settings->cf_value2 . "<br>";
                    }
                    echo '</p>';
                    ?>
                </div>
                <?php
                if ($Settings->invoice_view == 1 || $Settings->indian_gst) {
                    ?>
                    <div class="col-sm-12 text-center">
                        <?php if ($inv->sale_status == 'returned'): ?>
                            <h4 style="font-weight:bold;">Devolución de venta</h4>
                        <?php else: ?>
                            <h4 style="font-weight:bold;"><?=lang('sale_invoice');?></h4>
                        <?php endif ?>
                    </div>
                    <?php
                }
                echo "<p>" .lang("date") . ": " . $this->sma->hrld($inv->date) . "<br>";
                echo lang("sale_no_ref") . ": " . $inv->reference_no . "<br>";
                if (!empty($inv->return_sale_ref)) {
                    echo '<p>'.lang("return_ref").': '.$inv->return_sale_ref;
                    if ($inv->return_id) {
                        echo ' <a data-target="#myModal2" data-toggle="modal" href="'.admin_url('sales/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                    } else {
                        echo '</p>';
                    }
                }

                echo "<p>";
                echo lang("customer") . ": " . ($customer->company && $customer->company != '-' ? $customer->company : $customer->name) . "<br>";
                if ($pos_settings->customer_details) {
                    if ($customer->vat_no != "-" && $customer->vat_no != "") {

                        if ($customer->tipo_documento == 6) {
                            echo  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion." <br>";
                        } else {
                            echo  lang("vat_no") . ": " . $customer->vat_no." <br>";
                        }

                    }

                    if ($address) {
                        echo lang("tel") . ": " . $address->phone . "<br>";
                        echo lang("address") . ": " . $address->direccion . "<br>";
                        echo $address->city ." ".$address->state." ".$address->country ."<br>";
                    } else {
                        echo lang("tel") . ": " . $customer->phone . "<br>";
                        echo lang("address") . ": " . $customer->address . "<br>";
                        echo $customer->city ." ".$customer->state." ".$customer->country ."<br>";
                    }

                        echo $customer->email ."<br>";

                }
                echo "</p>";
                ?>
                <?php if ($seller): ?>
                    <p><b><?= lang('seller') ?> :</b> <?= $seller->company != '-' ? $seller->company : $seller->name; ?></p><br>
                <?php endif ?>

                <?php if (isset($cost_center) && $cost_center): ?>
                    <div>
                        <label><?= lang('cost_center') ?></label>
                        <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                    </div>
                <?php endif ?>

                <div style="clear:both;"></div>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Nombre</th>
                            <th style="text-align: center;">Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $r = 1; $category = 0;
                        // Variable para identificar las clasificación de marcas
                        $brand = 0;
                        $tax_summary = array();
                        // foreach ($rows as $row) {
                        //     if ($pos_settings->item_order == 1 && $category != $row->category_id) {
                        //         $category = $row->category_id;
                        //         echo '<tr>
                        //                 <td colspan="100%" class="no-border">
                        //                     <strong>'.$row->category_name.'</strong>
                        //                 </td>
                        //             </tr>';
                        //     }
                        //     echo '<tr>
                        //             <td colspan="2" class="no-border">#' . $row->barcode . ': &nbsp;&nbsp;' . product_name($row->product_name, ($printer ? $printer->char_per_line : null)) . ($row->variant && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? ' (' . $row->variant . ')' : '') . '<span class="pull-right">' . ($row->tax_code ? '*'.$row->tax_code : '') . '</span>
                        //             </td>
                        //         </tr>';
                        //     if (!empty($row->second_name)) {
                        //         echo '<tr>
                        //                 <td colspan="2" class="no-border">'.$row->second_name.'</td>
                        //             </tr>';
                        //     }
                        //     echo '<tr>
                        //             <td class="no-border border-bottom">' . $this->sma->formatQuantity($row->quantity) . ' x '.$this->sma->formatValue($value_decimals, $row->unit_price).($row->item_tax != 0 ? ' - '.lang('tax').' <small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small> '.$this->sma->formatValue($value_decimals, $row->item_tax).' ('.lang('hsn_code').': '.$row->hsn_code.')' : '').'
                        //             </td>
                        //             <td class="no-border border-bottom text-right">' . $this->sma->formatValue($value_decimals, $row->subtotal) . '
                        //             </td>
                        //         </tr>';

                        //     $r++;
                        // }
                        foreach ($rows as $row) {
                            if ($pos_settings->item_order == 1 && $category != $row->category_id) {
                                $category = $row->category_id;
                                echo '<tr>
                                        <td colspan="100%" class="no-border">
                                            <strong>'.$row->category_name.'</strong>
                                        </td>
                                    </tr>';
                            }

                            // Condición que clasifica los productos por marcas
                            if ($pos_settings->item_order == "2" && $brand != $row->brand_id) {
                                $brand = $row->brand_id;
                                echo '<tr>
                                        <td colspan="100%" class="no-border">
                                            <strong>'.$row->brand_name.'</strong>
                                        </td>
                                    </tr>';
                            }

                            echo '<tr>
                                    <td colspan="2" class="no-border">
                                        '.product_name($row->product_name, ($printer ? $printer->char_per_line : null)) .
                                        ($row->variant && (($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) || !$document_type_invoice_format) ? ' (' . $row->variant . ($row->variant_code ? ' - '.$row->variant_code : '') . ')' : '') .
                                        ($this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : '') .
                                        ($document_type_invoice_format && ($document_type_invoice_format->product_detail_discount) && $row->item_discount != 0 ? ", ".lang('product_discount_included')." (-".$row->discount.")" : "").
                                        ($row->serial_no != '' ? ' - '.$row->serial_no : '') .
                                        '<span class="pull-right">' . $this->sma->formatValue($value_decimals, $row->subtotal) . '</span>
                                    </td>
                                </tr>';
                            if (!empty($row->second_name)) {
                                echo '<tr>
                                        <td colspan="2" class="no-border">'.$row->second_name.'</td>
                                    </tr>';
                            }
                            echo '<tr>
                                    <td class="no-border border-bottom">' . $this->sma->formatQuantity($row->quantity, $qty_decimals) . ' x '.$this->sma->formatValue($value_decimals, $row->unit_price).'
                                    </td>
                                    <td class="no-border border-bottom text-right">' . ($row->tax_code ? '*'.$row->tax_code : '') . '
                                    </td>
                                </tr>';

                            $r++;
                        }
                        if (($document_type_invoice_format && $document_type_invoice_format->show_return_detail == 1) && $return_rows) {
                            echo '<tr class="warning">
                                    <td colspan="100%" class="no-border"><strong>'.lang('original_sale_items').'</strong></td>
                                  </tr>';
                            foreach ($return_rows as $row) {
                                if ($pos_settings->item_order == 1 && $category != $row->category_id) {
                                    $category = $row->category_id;
                                    echo '<tr>
                                            <td colspan="100%" class="no-border"><strong>'.$row->category_name.'</strong></td>
                                         </tr>';
                                }
                                echo '<tr>
                                        <td colspan="2" class="no-border">#' . $r . ': &nbsp;&nbsp;' . product_name($row->product_name, ($printer ? $printer->char_per_line : null)) . ($row->variant && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? ' (' . $row->variant . ')' : '') .
                                        ($document_type_invoice_format && ($document_type_invoice_format->product_detail_discount) && $row->item_discount != 0 ? ", ".lang('product_discount_included')." (-".$row->discount.")" : "").'<span class="pull-right">' . ($row->tax_code ? '*'.$row->tax_code : '') . '</span></td>
                                        </tr>';
                                echo '<tr>
                                        <td class="no-border border-bottom">' . $this->sma->formatQuantity($row->quantity, $qty_decimals) . ' x '.$this->sma->formatValue($value_decimals, $row->unit_price).($row->item_tax != 0 ? ' - '.lang('tax').' <small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small> '.$this->sma->formatValue($value_decimals, $row->item_tax).' ('.lang('hsn_code').': '.$row->hsn_code.')' : '').'
                                        </td>
                                        <td class="no-border border-bottom text-right">' . $this->sma->formatValue($value_decimals, $row->subtotal) . '</td>
                                    </tr>';
                                $r++;
                            }
                        }

                        ?>
                    </tbody>
                    <tfoot>
                        <?php
                            $rete_total = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total + ($inv->rete_bomberil_id && $inv->rete_bomberil_total > 0 ? $inv->rete_bomberil_total : 0) + ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0);
                        ?>
                        <tr>
                            <th><?=lang("sub_total");?></th>
                            <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax));?></th>
                        </tr>
                        <?php
                        if ($inv->order_tax != 0) {
                            echo '<tr><th>' . lang("tax") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</th></tr>';
                        }

                        if ($inv->rete_fuente_total != 0 || $inv->rete_iva_total != 0 || $inv->rete_ica_total != 0 || $inv->rete_other_total != 0) {
                            echo '<tr>
                                    <th>'.lang('retention').'</th>
                                    <th class="text-right"> -' . $this->sma->formatValue($value_decimals, $rete_total) . '</th>
                                  </tr>';
                        }

                        if ($inv->order_discount != 0) {
                            echo '<tr><th>' . lang("order_discount") . '</th><th class="text-right"> -' . $this->sma->formatValue($value_decimals, $inv->order_discount) . '</th></tr>';
                        }

                        if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 1) {
                            echo '<tr><th>' . lang("shipping") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $inv->shipping) . '</th></tr>';
                        }

                        if ($return_sale) {
                            if ($return_sale->surcharge != 0) {
                                echo '<tr><th>' . lang("order_discount") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $return_sale->surcharge) . '</th></tr>';
                            }
                        }
                        if ($inv->tip_amount != 0) {
                            echo '<tr>
                                    <th>' . lang("tip_suggested") . '</th>
                                    <th class="text-right">' . $this->sma->formatValue($value_decimals, $inv->tip_amount) . '</th>
                                  </tr>';
                        }
                        if ($pos_settings->rounding || $inv->rounding != 0) {
                            ?>
                            <tr>
                                <th><?=lang("rounding");?></th>
                                <th class="text-right"><?= $this->sma->formatValue($value_decimals, $inv->rounding);?></th>
                            </tr>
                            <tr>
                                <th><?=lang("grand_total");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding)  - $rete_total );?></th>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <th><?=lang("grand_total");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total  - $rete_total );?></th>
                            </tr>
                            <?php
                        }
                        if ($inv->paid < ($inv->grand_total + $inv->rounding)) {
                            ?>
                            <tr>
                                <th><?=lang("paid_amount");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid);?></th>
                            </tr>
                            <tr>
                                <th><?=lang("due_amount");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, ($return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding)) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid));?></th>
                            </tr>
                            <?php
                        } ?>
                    </tfoot>
                </table>
                <?php
                if ($payments) {
                    echo '<table class="table table-condensed"><tbody>';
                    foreach ($payments as $payment) {
                        echo '<tr>';
                        if (($payment->paid_by == 'cash' || $payment->paid_by == 'deposit') && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->sma->formatValue($value_decimals, $payment->pos_balance) : 0) . '</td>';
                        } elseif (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe')) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("no") . ': ' . 'xxxx xxxx xxxx ' . substr($payment->cc_no, -4) . '</td>';
                            echo '<td>' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                        } elseif ($payment->paid_by == 'Cheque') {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                        } elseif ($payment->paid_by == 'gift_card' && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td>' . lang("no") . ': ' . $payment->cc_no . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("balance") . ': ' . $this->sma->formatValue($value_decimals, $this->sma->getCardBalance($payment->cc_no)) . '</td>';
                        } elseif ($payment->paid_by == 'other' && $payment->amount) {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        } else {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        }
                        echo '</tr>';
                    }
                    echo '</tbody></table>';
                }
                if ($return_payments) {
                    echo '<strong>'.lang('return_payments').'</strong><table class="table table-condensed"><tbody>';
                    foreach ($return_payments as $payment) {
                        $payment->amount = (0-$payment->amount);
                        echo '<tr>';
                        if (($payment->paid_by == 'cash' || $payment->paid_by == 'deposit') && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->sma->formatValue($value_decimals, $payment->pos_balance) : 0) . '</td>';
                        } elseif (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') && $payment->cc_no) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("no") . ': ' . 'xxxx xxxx xxxx ' . substr($payment->cc_no, -4) . '</td>';
                            echo '<td>' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                        } elseif ($payment->paid_by == 'Cheque' && $payment->cheque_no) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                        } elseif ($payment->paid_by == 'gift_card' && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td>' . lang("no") . ': ' . $payment->cc_no . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("balance") . ': ' . $this->sma->formatValue($value_decimals, $this->sma->getCardBalance($payment->cc_no)) . '</td>';
                        } elseif ($payment->paid_by == 'other' && $payment->amount) {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        } else {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        }
                        echo '</tr>';
                    }
                    echo '</tbody></table>';
                }
                ?>

                <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax)) : ''; ?>

                <?php
                    if ($biller_data->product_order == "1" || $biller_data->product_order == "2")
                    {
                        echo $this->gscat->summary($invoice_products, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax), FALSE, $biller_data->product_order);
                    }
                ?>

                <?= $customer->award_points != 0 && $Settings->each_spent > 0 ? '<p class="text-center">'.lang('this_sale_points').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                .'<br>'.
                lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>' : ''; ?>
                <?= $inv->note ? '<p class="text-center">' . $this->sma->decode_html($inv->note) . '</p>' : ''; ?>
                <?= $inv->staff_note ? '<p class="no-print"><strong>' . lang('staff_note') . ':</strong> ' . $this->sma->decode_html($inv->staff_note) . '</p>' : ''; ?>

                <?php if ($inv->resolucion): ?>
                    <p class="text-center"><?= $inv->resolucion ?></p>
                <?php endif ?>

                <p class="text-center"><b><?= $tipo_regimen ?></b></p>

                <p class="text-center"><?= $this->sma->decode_html($invoice_footer ? $invoice_footer : '') ?></p>

                <?php if ($inv->resolucion): ?>
                    <p style="text-align:center;"><?= $inv->resolucion ?></p>
                <?php endif ?>

                <?php if ($inv->fe_aceptado == ACCEPTED && !empty($inv->cufe)): ?>
                    <p style="text-align:center; word-break: break-all;">CUDE : <?= $inv->cufe ?></p>
                    <?php if (!empty($affects)) { ?>
                        <p style="text-align:center; word-break: break-all;">CUFE : <?= $affects->cufe ?></p>
                    <?php } ?>
                    <div style="text-align:center;">
                        <img src="<?= base_url().'themes/default/admin/assets/images/qr_code/'.$inv->reference_no.'.png' ?>">
                    </div>
                    <br>
                    <p style="text-align:center;">Representación gráfica de la factura electrónica</p>
                <?php endif ?>

                <p class="text-center">Software Wappsi POS desarrollado por Web Apps Innovation SAS NIT 901.090.070-9  www.wappsi.com</p>

                <?php if ($inv->fe_aceptado == ACCEPTED): ?>
                    <div style="text-align:center;">
                        <img src="<?= base_url().$technologyProviderLogo ?>" style="width: 50%;">
                    </div>
                <?php endif ?>

                <?php if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 0): ?>
                    <hr>
                    <h4><?= sprintf(lang('shipping_not_include_in_grand_total'), $this->sma->formatValue($value_decimals, $inv->shipping)) ?></h4>
                <?php endif ?>
            </div>

        </div>

        <div id="buttons" style="padding-top:10px;" class="no-print">

            <div class="alert alert-info loading_print_status" <?= $pos_settings->remote_printing != 4 ? 'style="display:none;"' : '' ?> >
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status') ?>
            </div>

            <div class="alert alert-success loading_print_status_success" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_success') ?>
            </div>

            <div class="alert alert-warning loading_print_status_warning" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_warning') ?>
            </div>

            <hr>
            <?php
            if ($message) {
                ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?=is_array($message) ? print_r($message, true) : $message;?>
                </div>
                <?php
            } ?>
            <?php
            if ($modal) {
                ?>
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group">
                        <a href='#' class='po btn btn-primary' title='' data-placement="top" data-content="
                                    <p> <?= lang('where_to_duplicate') ?> </p>
                                    <a class='btn btn-primary' href='<?= admin_url('sales/add/?sale_id=').$inv->id ?>'> <?= lang('detal_sale') ?> </a>
                                    <a class='btn btn-primary' href='<?= admin_url('pos/index/?duplicate=').$inv->id ?>'> <?= lang('pos_sale') ?> </a>
                                    <button class='btn po-close'> <?= lang('cancel') ?> </button>" rel='popover'>
                                    <i class='fa fa-download'></i>
                            <?= lang('duplicate') ?>
                        </a>
                    </div>
                    <div class="btn-group" role="">
                        <?php
                        if ($pos->remote_printing == 1) {
                            echo '<button onclick="imprimir_factura_pos();" class="btn btn-block btn-primary">'.lang("print").'</button>';
                        } else {
                            echo '<button onclick="return printReceipt()" class="btn btn-block btn-primary">'.lang("print").'</button>';
                        }

                        ?>
                    </div>
                    <div class="btn-group" role="group">
                        <a class="btn btn-block btn-success" href="#" id="email"><?= lang("email"); ?></a>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close'); ?></button>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <span class="pull-right col-xs-12">
                    <?php
                    if ($pos->remote_printing == 1) {
                        echo '<button onclick="imprimir_factura_pos();" class="btn btn-block btn-primary">'.lang("print").'</button>';
                    } else {
                        echo '<button onclick="return printReceipt()" class="btn btn-block btn-primary">'.lang("print").'</button>';
                        echo '<button onclick="return openCashDrawer()" class="btn btn-block btn-default">'.lang("open_cash_drawer").'</button>';
                    }
                    ?>
                </span>
                <span class="pull-left col-xs-12"><a class="btn btn-block btn-success" href="#" id="email"><?= lang("email"); ?></a></span>
                <span class="col-xs-12">
                    <a class="btn btn-block btn-warning" href="<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>"><?= lang("back_to_pos"); ?></a>
                </span>
                <?php
            }
            //if ($pos->remote_printing == 1) {
                ?>
                <!-- <div style="clear:both;"></div>
                <div class="col-xs-12" style="background:#F5F5F5; padding:10px;">
                    <p style="font-weight:bold;">
                        Please don't forget to disble the header and footer in browser print settings.
                    </p>
                    <p style="text-transform: capitalize;">
                        <strong>FF:</strong> File &gt; Print Setup &gt; Margin &amp; Header/Footer Make all --blank--
                    </p>
                    <p style="text-transform: capitalize;">
                        <strong>chrome:</strong> Menu &gt; Print &gt; Disable Header/Footer in Option &amp; Set Margins to None
                    </p>
                </div> -->
                <?php
            //} ?>
            <!--<div style="clear:both;"></div>-->

        </div>
    </div>

    <?php
    if( ! $modal) {
        ?>
        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
        <?php
    }
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '#email', function() { open_email_confirmation_window(); return false; });

            <?php if($this->pos_settings->print_voucher_delivery == 1 && !$modal) { ?>
                imprimir_factura_pos();
                setTimeout(function() {
                    location.href = '<?= admin_url("pos/voucher_delivery/").$inv->id."/".$redirect_to_pos ?>';
                }, 500);
            <?php } else { ?>
                <?php if ($redirect_to_pos && $print_directly == 1) { ?>

                    window.scrollTo(0,document.body.scrollHeight);
                    setTimeout(function() {
                        $.ajax({
                            url:"<?= admin_url('pos/get_direct_printers_jobs/'.$direct_print_job_id.'/'.$printer->google_cloud_print_id) ?>",
                        }).done(function(data){
                            $('.loading_print_status').fadeOut();
                            window.scrollTo(0,document.body.scrollHeight);
                            if (data == 'DONE') {
                                $('.loading_print_status_success').fadeIn();
                            } else {
                                $('.loading_print_status_warning').fadeIn();
                            }
                        });
                    }, 8000);

                    setTimeout(function() {
                        location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                    }, 8500);
                <?php } else if ($redirect_to_pos) { ?>
                    imprimir_factura_pos();
                    setTimeout(function() {
                        location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                    }, 400);
                <?php } else { ?>
                    <?php
                    if ($print_directly == 1) { ?>

                        window.scrollTo(0,document.body.scrollHeight);
                        setTimeout(function() {
                            $.ajax({
                                url:"<?= admin_url('pos/get_direct_printers_jobs/'.$direct_print_job_id.'/'.$printer->google_cloud_print_id) ?>",
                            }).done(function(data){
                                $('.loading_print_status').fadeOut();
                                window.scrollTo(0,document.body.scrollHeight);
                                if (data == 'DONE') {
                                    $('.loading_print_status_success').fadeIn();
                                } else {
                                    $('.loading_print_status_warning').fadeIn();
                                }
                            });
                        }, 8000);
                    <?php } else if ($pos_settings->remote_printing == 1 && !$print_directly) { ?>
                        $(window).load(function () {
                            imprimir_factura_pos();
                            return false;
                        });
                        <?php
                    }
                    ?>
                <?php } ?>
            <?php } ?>
        });

        function open_email_confirmation_window()
        {
            bootbox.prompt({
                title: "<?= lang("email_address"); ?>",
                inputType: 'email',
                value: "<?= $customer->email; ?>",
                callback: function(email) {
                    if (email != null) {
                        generate_pos_invoice_pdf(email);
                    }
                }
            });
        }

        function generate_pos_invoice_pdf (email)
        {
            $.ajax({
                type: "post",
                url: "<?= admin_url('pos/generate_pos_invoice_pdf') ?>",
                dataType: "json",
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': "<?= $this->security->get_csrf_hash(); ?>",
                    'email': email,
                    'id': <?= $inv->id; ?>
                }
            })
            .done(function(data) {
                console.log(data);

                if (data.response = "1") {
                    send_copy_inovice(email);
                } else {
                    bootbox.alert({message: '<?= lang('ajax_request_failed'); ?>', size: 'small'});
                    return false;
                }
            })
            .fail(function(data) {
                console.log(data.responseText);
                bootbox.alert({message: '<?= lang('ajax_request_failed'); ?>', size: 'small'});
                return false;
            });
        }

        function send_copy_inovice(email)
        {
            $.ajax({
                type: "post",
                url: "<?= admin_url('pos/email_receipt') ?>",
                dataType: "json",
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': "<?= $this->security->get_csrf_hash(); ?>",
                    'email': email,
                    'id': <?= $inv->id; ?>
                }
            })
            .done(function(data) {
                bootbox.alert({message: data.msg, size: 'small'});
            })
            .fail(function() {
                bootbox.alert({message: '<?= lang('ajax_request_failed'); ?>', size: 'small'});
                return false;
            });
        }

        function imprimir_factura_pos()
        {
          var divToPrint=document.getElementById('receiptData');

          var newWin=window.open('','Print-Window');

          var header = $('head').html();

          newWin.document.open();

          newWin.document.write('<head>'+header+'</head><body onload="window.print(); setTimeout(function(){window.close();},1000);" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');

          newWin.document.close();

          // setTimeout(function(){newWin.close();},2500);
        }
    </script>

    <?php include 'remote_printing.php'; ?>
    <?php if($modal) { ?>
    </div>
</div>
</div>

<?php } else { ?>
</body>
</html>
<?php } ?>
