<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_social_security_parafiscal_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

	public function get_by_payroll_id(int $payroll_id)
	{
		$this->db->select("payroll_id,
						    employee_id,
						    description,
						    SUM(base_salary) AS base_salary,
						    percentage,
						    SUM(amount) AS amount");
		$this->db->where("payroll_id", $payroll_id);
		$this->db->group_by(["description", "percentage"]);
		$q = $this->db->get("payroll_social_security_parafiscal");
		return $q->result();
	}

	public function get_by_payroll_id_employee_id(int $payroll_id, int $employee_id)
	{
		$this->db->select("payroll_id,
						    employee_id,
						    description,
						    SUM(base_salary) AS base_salary,
						    percentage,
						    SUM(amount) AS amount");
		$this->db->where("payroll_id", $payroll_id);
		$this->db->where("employee_id", $employee_id);
		$this->db->group_by(["description", "percentage"]);
		$q = $this->db->get("payroll_social_security_parafiscal");
		return $q->result();
	}

	public function insert($data)
	{
		if ($this->db->insert_batch('payroll_social_security_parafiscal', $data) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function delete(int $payroll_id, int $employee_id)
	{
		$this->db->where("payroll_id", $payroll_id);
		$this->db->where("employee_id", $employee_id);
		if ($this->db->delete("payroll_social_security_parafiscal")) {
			return TRUE;
		}
		return FALSE;
	}

}

/* End of file Payroll_social_security_parafiscal_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/Payroll_social_security_parafiscal_model.php */