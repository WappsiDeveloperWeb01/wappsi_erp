<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open_multipart("purchases/save_return_other_concepts/", ['id'=>'return_other_concepts_form']); ?>
                                <input type="hidden" name="purchase_currency" id="purchase_currency" value="<?= $purchase_data->purchase_currency; ?>">
                                <input type="hidden" name="purchase_type" id="purchase_type" value="<?= $purchase_data->purchase_type; ?>">
                                <input type="hidden" name="warehouse_id" id="warehouse_id" value="<?= $purchase_data->warehouse_id; ?>">
                                <input type="hidden" name="order_discount_method" value="<?= $purchase_data->order_discount_method ?>">
                                <input type="hidden" name="supplier_name" id="supplier_name" value="<?= $purchase_data->supplier; ?>">
                                <input type="hidden" name="supplier_id" id="supplier_id" value="<?= $purchase_data->supplier_id; ?>">
                                <input type="hidden" name="grand_total" id="grand_total" value="<?= $purchase_data->grand_total; ?>">
                                <input type="hidden" name="purchase_id" id="purchase_id" value="<?= $purchase_data->id; ?>">
                                <input type="hidden" name="balance" id="balance" value="<?= $purchase_data->grand_total - $purchase_data->paid; ?>">
                                <input type="hidden" name="withholdings_applied" id="withholdings_applied" value="0">
                                <input type="hidden" name="shipping" id="shipping">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <?php if ($Owner || $Admin) : ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <?= lang("date", "redate"); ?>
                                                        <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="redate" required="required"'); ?>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) : ?>
                                                <div class="col-md-4 form-group">
                                                    <?= lang("biller", "biller_id"); ?>
                                                    <select name="biller_id" id="biller_id" class="form-control" required="required">
                                                        <?php foreach ($billers as $biller): ?>
                                                            <option value="<?= $biller->id ?>" <?= $purchase_data->biller_id == $biller->id ? 'selected="selected"' : '' ?>><?= $biller->company ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            <?php else : ?>
                                                <?php
                                                    $bldata = [];
                                                    foreach ($billers as $biller) {
                                                        $bldata[$biller->id] = $biller;
                                                    }
                                                ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <?= lang("biller", "biller_id"); ?>
                                                        <select name="biller_id" id="biller_id" class="form-control">
                                                            <?php if (isset($bldata[$this->session->userdata('biller_id')])): ?>
                                                                <?php $biller = $bldata[$this->session->userdata('biller_id')]; ?>
                                                                <option value="<?= $biller->id ?>" <?= $purchase_data->biller_id == $biller->id ? 'selected="selected"' : '' ?>><?= $biller->company ?></option>
                                                            <?php endif; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("reference_no", "reref"); ?>
                                                    <select name="document_type_id" id="document_type_id" class="form-control" required="required"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <?php if (isset($cost_centers)): ?>
                                                <div class="col-md-4 form-group">
                                                    <?= lang('cost_center', 'cost_center_id') ?>
                                                    <?php
                                                    $ccopts[''] = lang('select');
                                                    if ($cost_centers) {
                                                        foreach ($cost_centers as $cost_center) {
                                                            $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                                        }
                                                    }
                                                    ?>
                                                    <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                                </div>
                                            <?php endif ?>
                                            <div class="col-md-4">
                                                <?= form_label(lang("credit_note_other_concepts_label_concept"), 'concept'); ?>
                                                <div class="input-group">
                                                    <select class="form-control" name="concept" id="concept" style="max-width: 420px;">
                                                        <option value=""><?= lang("select"); ?></option>
                                                        <?php foreach ($credit_note_concepts as $credit_note_concept) { ?>
                                                            <option value="<?= $credit_note_concept->id; ?>" data-concept_tax_rate="<?= $credit_note_concept->concept_tax_rate; ?>" data-concept_dian_code="<?= $credit_note_concept->dian_code; ?>" data-concept_tax_rate_id="<?= $credit_note_concept->concept_tax_rate_id; ?>"><?= $credit_note_concept->description; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-success" type="button" name="add_concept" id="add_concept" data-toggle="tooltip" data-placement="top" title="Agregar Concepto"><i class="fa fa-plus"></i></button>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4 same-height">
                                                <label> Retención </label>
                                                <input type="text" name="retencion_show" id="retencion_show" class="form-control text-right" readonly>
                                                <input type="hidden" name="retencion" id="rete">
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= form_label(lang("purchase_reference"), 'reference_no'); ?>
                                                    <?= form_input("purchase_reference", (isset($_POST['purchase_reference']) ? $_POST['purchase_reference'] : $purchase_data->reference_no), 'class="form-control" id="purchase_reference" placeholder="'. lang("select") .'" required="required" readonly="read"'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-4 div_add_shipping" style="display: none;">
                                                <?= lang('add_shipping', 'add_shipping') ?>
                                                <select id="add_shipping" class="form-control">
                                                    <option value="0"><?= lang('no') ?></option>
                                                    <option value="1"><?= lang('yes') ?></option>
                                                </select>
                                                <em class="text-danger"><?= lang('add_shipping_detail') ?></em>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <?php if (isset($cost_centers)): ?>
                                                <div class="col-md-4 form-group">
                                                    <?= lang('cost_center', 'cost_center_id') ?>
                                                    <?php
                                                    $ccopts[''] = lang('select');
                                                    if ($cost_centers) {
                                                        foreach ($cost_centers as $cost_center) {
                                                            $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                                        }
                                                    }
                                                     ?>
                                                     <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                                </div>
                                            <?php endif ?>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="control-group table-group">
                                                    <label class="table-label"><?= lang("credit_note_other_concepts_label_concepts"); ?> *</label>
                                                    <div class="controls table-controls">
                                                        <table id="credit_note_table" class="table items  table-bordered table-condensed table-hover sortable_table">
                                                            <thead>
                                                                <tr>
                                                                    <th ><?= lang('credit_note_other_concepts_label_concept'); ?></th>
                                                                    <th width="10%"><?= lang("credit_note_other_concepts_label_value"); ?></th>
                                                                    <th width="10%"><?= lang("credit_note_other_concepts_label_tax"); ?></th>
                                                                    <th width="10%"><?= lang("credit_note_other_concepts_label_total"); ?></th>
                                                                    <th width="3%"><i class="fa fa-trash"></i></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                            <tfoot>
                                                                <tr class="warning">
                                                                    <th></th>
                                                                    <th width="10%"><?= lang('total') ?> <span class="totals_val pull-right" id="total_sum_concept_value">0.00</span></th>
                                                                    <th width="10%"><?= lang('grand_total') ?> <span class="totals_val pull-right" id="total_sum_concept_tax">0.00</span></th>
                                                                    <th width="10%"><?= lang('grand_total') ?> <span class="totals_val pull-right" id="total_sum">0.00</span></th>
                                                                    <th width="3%"></th>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php if ($purchase_data->payment_status == 'paid' || $purchase_data->payment_status == 'partial') { ?>
                                                    <div class="alert alert-success">
                                                        <p><?= lang('payment_status') ?> : <strong> <?= $purchase_data->payment_status ?> </strong> & <?= lang('paid_amount') ?> <strong> <?= $this->sma->formatMoney($purchase_data->paid) ?> </strong></p>
                                                        <p><?= lang('adjust_payments') ?></p>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="alert alert-warning">
                                                        <p><?= lang('payment_status') ?> : <strong> <?= $purchase_data->payment_status ?> </strong> & <?= lang('paid_amount') ?> <strong> <?= $this->sma->formatMoney($purchase_data->paid) ?> </strong></p>
                                                        <p><?= lang('adjust_payments') ?></p>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <?php if (($Owner || $Admin || $GP['purchases-payments']) && $allow_payment) { ?>
                                            <div id="payments" style="display: none;">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="well well-sm well_1">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                                                            <select name="payment_reference_no" class="form-control" id="payment_reference_no" required="required"></select>
                                                                        </div>

                                                                        <div class="form-group deposit_div" style="display: none;">
                                                                            <?= lang('deposit_document_type_id', 'deposit_document_type_id') ?>
                                                                            <select name="deposit_document_type_id" id="deposit_document_type_id" class="form-control"></select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="payment">
                                                                            <div class="form-group">
                                                                                <?= lang("amount", "amount_1"); ?>
                                                                                <input type="text" class="form-control amount_to_paid" readonly>
                                                                                <input type="hidden" name="amount-paid" id="amount_1"/>
                                                                                <input type="hidden" name="amount-discounted" id="discounted"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <?= lang("paying_by", "paid_by_1"); ?>
                                                                            <select name="paid_by" id="paid_by_1" class="form-control paid_by">
                                                                                <?= $this->sma->paid_opts(null, true); ?>
                                                                            </select>
                                                                            <input type="hidden" name="mean_payment_code_fe" id="mean_payment_code_fe">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="pcc_1" style="display:none;">
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <input name="pcc_no" type="text" id="pcc_no_1" class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <input name="pcc_holder" type="text" id="pcc_holder_1" class="form-control" placeholder="<?= lang('cc_holder') ?>"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <select name="pcc_type" id="pcc_type_1" class="form-control pcc_type" placeholder="<?= lang('card_type') ?>">
                                                                                    <option value="Visa"><?= lang("Visa"); ?></option>
                                                                                    <option value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                                    <option value="Amex"><?= lang("Amex"); ?></option>
                                                                                    <option value="Discover"><?= lang("Discover"); ?></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <input name="pcc_month" type="text" id="pcc_month_1" class="form-control" placeholder="<?= lang('month') ?>"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <input name="pcc_year" type="text" id="pcc_year_1" class="form-control" placeholder="<?= lang('year') ?>"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <input name="pcc_ccv" type="text" id="pcc_cvv2_1" class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="pcheque_1" style="display:none;">
                                                                    <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                                        <input name="cheque_no" type="text" id="cheque_no_1" class="form-control cheque_no"/>
                                                                    </div>
                                                                </div>
                                                                <div class="gc" style="display: none;">
                                                                    <div class="form-group">
                                                                        <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                        <div class="input-group">
                                                                            <input name="gift_card_no" type="text" id="gift_card_no" class="pa form-control kb-pad"/>
                                                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px; height:25px;">
                                                                                <a href="#" id="sellGiftCard" class="tip" title="<?= lang('sell_gift_card') ?>"><i class="fa fa-credit-card"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="gc_details"></div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <!-- <div class="row deposit_div" style="display: none;">
                                                                        <div class="col-md-4">
                                                                            <?= lang('deposit_document_type_id', 'deposit_document_type_id') ?>
                                                                            <select name="deposit_document_type_id" id="deposit_document_type_id" class="form-control">
                                                                            </select>
                                                                        </div>
                                                                    </div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                                        <input type="hidden" name="order_tax" value="" id="retax2" required="required"/>
                                        <input type="hidden" name="discount" value="" id="rediscount" required="required"/>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang("return_note", "renote"); ?>
                                                    <?= form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="renote" style="margin-top: 10px; height: 100px;"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?= form_button('save_return', $this->lang->line("submit"), 'id="save_return" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                                                <h4 class="modal-title" id="rtModalLabel"><?=lang('edit_order_tax');?></h4>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-2"><label>Retención</label></div>
                                                    <div class="col-sm-5"><label>Opción</label></div>
                                                    <div class="col-sm-2"><label>Porcentaje</label></div>
                                                    <div class="col-sm-3"><label>Valor</label></div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>
                                                            <input class="type_retention_check" type="checkbox" name="rete_fuente" class="skip" id="rete_fuente" data-type_retention="fuente"> Fuente
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select class="form-control retention_type_option" name="rete_fuente_option" id="rete_fuente_option"  data-type_retention="fuente" disabled='true'>
                                                            <option>Seleccione</option>
                                                        </select>
                                                        <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                                        <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                                        <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right total_value_withholding_type" readonly>
                                                        <!-- <input type="hidden" name="rete_fuente_valor" id="rete_fuente_valor"> -->
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>
                                                            <input class="type_retention_check"  type="checkbox" name="rete_iva" class="skip" id="rete_iva"  data-type_retention="iva"> Iva
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select class="form-control retention_type_option" name="rete_iva_option" id="rete_iva_option" data-type_retention="iva" disabled='true'>
                                                            <option>Seleccione...</option>
                                                        </select>
                                                        <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                                        <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                                        <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text " name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right total_value_withholding_type" readonly>
                                                        <!-- <input type="hidden" name="rete_iva_valor" id="rete_iva_valor"> -->
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>
                                                            <input class="type_retention_check"  type="checkbox" name="rete_ica" class="skip" id="rete_ica"  data-type_retention="ica"> Ica
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select class="form-control retention_type_option" name="rete_ica_option" id="rete_ica_option" data-type_retention="ica" disabled='true'>
                                                            <option>Seleccione...</option>
                                                        </select>
                                                        <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                                        <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                                        <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right total_value_withholding_type" readonly>
                                                        <!-- <input type="hidden" name="rete_ica_valor" id="rete_ica_valor"> -->
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>
                                                            <input class="type_retention_check"  type="checkbox" name="rete_otros" class="skip" id="rete_otros" data-type_retention="otros"> Otros
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select class="form-control retention_type_option" name="rete_otros_option" id="rete_otros_option" data-type_retention="otros" disabled='true'>
                                                            <option>Seleccione...</option>
                                                        </select>
                                                        <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                                        <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                                        <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right total_value_withholding_type" readonly>
                                                        <!-- <input type="hidden" name="rete_otros_valor" id="rete_otros_valor"> -->
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 text-right">
                                                        <label>Total : </label>
                                                    </div>
                                                    <div class="col-md-4 text-right">
                                                        <!-- <label id="total_rete_amount" style="display: none;"> 0.00 </label> -->
                                                        <label id="total_rete_amount"> 0.00 </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
                                                <button type="button" id="save_credit_note_withholdings" class="btn btn-primary"><?=lang('update')?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var count = 1, an = 1, DT = <?= $Settings->default_tax_rate ?>, po_edit = 1,product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, shipping = 0, surcharge = 0, potype = 1, deposit_advertisement_showed = false;
        _existing_concept = false;

        $('[data-toggle="tooltip"]').tooltip();

        localStorage.setItem('redate', '<?= $this->sma->hrld($purchase_data->date) ?>');
        localStorage.removeItem('reinvoice_paid');
        localStorage.removeItem('repayment_status');
        localStorage.removeItem('purchase_balance');
        localStorage.removeItem('total_retenciones');
        localStorage.removeItem('invoice_withholdings');
        <?php if (isset($purchase_data)) { ?>
            localStorage.setItem('reinvoice_paid', '<?= $purchase_data->paid ?>');
            localStorage.setItem('repayment_status', '<?= $purchase_data->payment_status ?>');
            localStorage.setItem('purchase_balance', '<?= ($purchase_data->grand_total - ($purchase_data->paid - ($purchase_data->rete_fuente_total + $purchase_data->rete_iva_total + $purchase_data->rete_ica_total + $purchase_data->rete_other_total))) ?>');
        <?php } ?>
        invoice_withholdings = {
            'gtotal' : $('#gtotal').text(),
            'id_rete_fuente' : '<?= $purchase_data->rete_fuente_id ?>',
            'id_rete_iva' : '<?= $purchase_data->rete_iva_id ?>',
            'id_rete_ica' : '<?= $purchase_data->rete_ica_id ?>',
            'id_rete_otros' : '<?= $purchase_data->rete_other_id ?>'
        };
        localStorage.setItem('invoice_withholdings', JSON.stringify(invoice_withholdings));
        localStorage.setItem('reorderdiscountmethod', '<?= $purchase_data->order_discount_method ?>');

        <?php if ($Owner || $Admin) { ?>
            $("#redate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());

        <?php } ?>
        $(document).on('change', '#redate', function (e) { set_return_date($(this).val()); });
        $(document).on('change', '#biller_id', function (e) { get_types_of_documents(); });
        $(document).on('click', '#retencion_show', function (e) { open_withholding_modal(); });
        $(document).on('click', '#add_concept', function() { add_concept(); });
        $(document).on('click', '.delete_row', function() { delete_concept_row($(this)); });
        $(document).on('change', '.concept_value', function() { calculate_taxes($(this)); });
        $(document).on('click', '#save_credit_note_withholdings', function() { save_credit_note_withholdings(); });
        $(document).on('click', '#save_return', function() { save_return(); });

        $(document).on('ifToggled', '.type_retention_check', function(){
            var type_retention = $(this).data('type_retention');

            if ($(this).is(':checked')) {
                 $.ajax({
                    url: site.base_url+"purchases/opcionesWithHolding/"+type_retention.toUpperCase()
                }).done(function(data){
                    $('#rete_'+type_retention+'_option').html(data);
                    $('#rete_'+type_retention+'_option').attr('disabled', false).select();
                }).fail(function(data){
                    console.log(data);
                });
            } else {
                $('#rete_'+type_retention+'_option').select2('val', '').attr('disabled', true).select();
                $('#rete_'+type_retention+'_tax').val('');
                $('#rete_'+type_retention+'_valor').val('');

                total_value_withholding_type();
            }
        });

        $(document).on('change', '.retention_type_option', function() {
            var type_retention = $(this).data('type_retention');

            var witholding_id = $(this).val();
            var apply = $('#rete_'+ type_retention +'_option option:selected').data('apply');
            var account = $('#rete_'+ type_retention +'_option option:selected').data('account');
            var percentage = $('#rete_'+ type_retention +'_option option:selected').data('percentage');

            $('#rete_'+type_retention+'_tax').val(percentage);
            $('#rete_'+type_retention+'_account').val(account);
            $('#rete_'+type_retention+'_id').val(witholding_id);

            amount = 0;
            if (apply == 'ST') {
                amount = $('#concept_value').val();
            } else if (apply == 'TX') {
                amount = $('#concept_tax').val();
            } else if (apply == 'TO') {
                amount = $('#total_value_concept').val();
            }

            total_withholding = (amount * percentage) / 100;
            $('#rete_'+type_retention+'_valor').val(formatDecimal(parseFloat(total_withholding)));
            $('#rete_'+type_retention+'_base').val(amount);

            total_value_withholding_type();
        });

        $(document).on('change', '#paid_by_1', function() { loadPaymentMeanCode($(this)); });

        get_biller_documentTypes();

        $('#biller_id').trigger('change');
        $('#paid_by_1').trigger('change');
    });

    function get_biller_documentTypes(){
        var biller_id = $('#biller_id').val();
        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/32/") ?>'+biller_id,
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;

            $('#payment_reference_no').html(response.options).select2();

            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
        });
    }

    function set_return_date(date)
    {
        localStorage.setItem('redate', date);
        $('#redate').val(date);
    }

    function get_types_of_documents()
    {
        var isElectronic = '<?= $isElectronic ?>';
        var module_id = ('<?= $purchase_data->purchase_type ?>' == 2) ? 22 : 6;

        if (isElectronic == 1) {
            module_id = 52;
        }

        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/") ?>'+module_id+"/"+$('#biller_id').val(),
            type:'GET',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();

            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }

            if (response.status == 0) {
                $('.repurchasert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }

            $('#document_type_id').trigger('change');
        });
    }

    function open_withholding_modal()
    {
        if ($('#concept').val() == '') {
            Command: toastr.error('No se ha seleccionado el concepto.', '¡Error!', {onHidden: function() { $('#concept').select2('focus'); }});
        } else if ($('#concept_value').val() == null || $('#concept_value').val() == '') {
            Command: toastr.error('No se ha ingresado valor al concepto seleccionado.', '¡Error!', {onHidden: function() { $('#concept_value').focus(); }});
        } else {
            $('#rtModal').modal('show');
        }
    }

    function add_concept()
    {
        var concept = $('#concept').val();
        var reference = $('#reference_no').val();
        var description = $('#concept option:selected').text();
        var tax_rate_id = $('#concept option:selected').data('concept_tax_rate_id');
        var tax_rate = $('#concept option:selected').data('concept_tax_rate');
        var concept_dian_code = $('#concept option:selected').data('concept_dian_code');

        if (validate_add_concept(reference, concept)) {
            var table_row = '<tr>'+
                                '<td>'+ description +'</td>'+
                                '<td>'+
                                    '<input class="form-control text-right concept_value" type="number" name="concept_value" id="concept_value">'+
                                    '<input type="hidden" name="concept_id" id="concept_id" value="'+ concept +'">'+
                                    '<input type="hidden" name="concept_name" id="concept_name" value="'+ description +'">'+
                                    '<input type="hidden" name="concept_tax_rate_id" id="concept_tax_rate_id" value="'+ tax_rate_id +'">'+
                                    '<input type="hidden" name="concept_tax_rate" id="concept_tax_rate" value="'+ tax_rate +'">'+
                                    '<input type="hidden" name="concept_dian_code" id="concept_dian_code" value="'+ concept_dian_code +'">'+
                                '</td>'+
                                '<td><input class="form-control text-right concept_tax" type="number" name="concept_tax" id="concept_tax" readonly></td>'+
                                '<td><input class="form-control text-right total_value_concept" type="number" name="total_value_concept" id="total_value_concept" readonly></td>'+
                                '<td class="text-center">'+
                                    '<i class="fa fa-trash fa-lg text-danger delete_row" data-toggle="tooltip" data-placement="top" title="Eliminar" style="cursor: pointer;"></i>'+
                                '</td>'+
                            '</tr>';

            $('#credit_note_table tbody').append(table_row);
            $('[data-toggle="tooltip"]').tooltip();
        }
    }

    function validate_add_concept(reference, concept)
    {
        if (reference == "") {
            Command: toastr.error('Debe seleccionar la factura que se quiere afectar', '<?= lang("toast_error_title"); ?>');
            return false;
        }

        if (concept == "") {
            Command: toastr.error('Debe seleccionar el concepto a agregar', '<?= lang("toast_error_title"); ?>');
            return false;
        }

        if (_existing_concept == true) {
            Command: toastr.error('El concepto ya se encuentra agregado', '<?= lang("toast_error_title"); ?>');
            return false;
        }

        _existing_concept = true;
        return true;
    }

    function delete_concept_row(row_control)
    {
        if (row_control === undefined) {
            $('#credit_note_table tbody tr').remove();
        } else {
            row_control.parents('tr').remove();
        }

        cancel_withholding_calculation();

        $('#total_sum_concept_value').text('0.00');
        $('#total_sum_concept_tax').text('0.00');
        $('#total_sum').text('0.00');
        $('#withholdings').val(0);

        _existing_concept = false;
    }

    function cancel_withholding_calculation()
    {
        $('.type_retention_check').iCheck('uncheck');

        total_value_withholding_type();
    }

    function total_value_withholding_type()
    {
        var total_sum_withholdings = 0;

        $('.total_value_withholding_type').each(function () {
            if ($(this).val() != '') {
                total_sum_withholdings += parseFloat($(this).val());
            }
        });

        if (total_sum_withholdings > 0) {
            $('#withholdings_applied').val(1);
        }

        $('#total_rete_amount').text(total_sum_withholdings);
        localStorage.setItem('total_retenciones', total_sum_withholdings);
        _retenciones = {
            'retention_id_fuente': $('#rete_fuente_option').val(),
            'retention_percentage_fuente': $('#rete_fuente_option option:selected').data('percentage'),
            'retention_total_fuente': $('#rete_fuente_valor').val(),
            'retention_base_fuente': $('#rete_fuente_base').val(),
            'retention_account_fuente': $('#rete_fuente_option option:selected').data('account'),

            'retention_id_iva': $('#rete_iva_option').val(),
            'retention_percentage_iva': $('#rete_iva_option option:selected').data('percentage'),
            'retention_total_iva': $('#rete_iva_valor').val(),
            'retention_base_iva': $('#rete_iva_base').val(),
            'retention_account_iva': $('#rete_iva_option option:selected').data('account'),

            'retention_id_ica': $('#rete_ica_option').val(),
            'retention_percentage_ica': $('#rete_ica_option option:selected').data('percentage'),
            'retention_total_ica': $('#rete_ica_valor').val(),
            'retention_base_ica': $('#rete_ica_base').val(),
            'retention_account_ica': $('#rete_ica_option option:selected').data('account'),

            'retention_id_other': $('#rete_otros_option').val(),
            'retention_percentage_other': $('#rete_otros_option option:selected').data('percentage'),
            'retention_total_other': $('#rete_otros_valor').val(),
            'retention_base_other': $('#rete_otros_base').val(),
            'retention_account_other': $('#rete_otros_option option:selected').data('account'),
        };

        $('#withholdings_data').val(JSON.stringify(_retenciones));
        total_sums();
    }

    function total_sums()
    {
        var total_sum_concept_value = total_sum_concept_tax = total_sum_withholdings = total_sum = 0;
        $('.concept_value').each(function() {
            total_sum_concept_value += parseFloat($(this).val());
        });
        $('#total_sum_concept_value').text(total_sum_concept_value);

        $('.concept_tax').each(function() {
            total_sum_concept_tax += parseFloat($(this).val());
        });
        $('#total_sum_concept_tax').text(total_sum_concept_tax);

        $('.total_value_concept').each(function() {
            total_sum += parseFloat($(this).val());
        });

        $('#total_sum').text(total_sum);
        <?php if (isset($purchase_data) && ($purchase_data->payment_status == 'paid' || $purchase_data->payment_status == 'partial')) { ?>
            total_rete_amount = 0;
            if (total_retenciones = JSON.parse(localStorage.getItem('total_retenciones'))) {
                total_rete_amount = parseFloat(formatDecimal(total_retenciones));
            }
            $('.amount_to_paid').val(formatMoney(total_sum - formatDecimal(total_rete_amount)));
            $('#amount_1').val(formatDecimal(total_sum - formatDecimal(total_rete_amount)));
        <?php } ?>

        verify_payment_register(total_sum);
    }

    function verify_payment_register(gtotal)
    {
        paid = parseFloat(localStorage.getItem('reinvoice_paid'));
        payment_status = localStorage.getItem('repayment_status');
        purchase_balance = parseFloat(localStorage.getItem('purchase_balance'));

        if (payment_status == 'partial') {

            if (gtotal > purchase_balance) {
                $('#payments').fadeIn();
                $('#amount_1').val(gtotal - purchase_balance);
                $('.amount_to_paid').val(formatMoney(gtotal - purchase_balance));

                //     if (deposit_payment_method.purchase == true && deposit_advertisement_showed == false) {
                //         deposit_advertisement_showed = true;
                //         setTimeout(function() {
                //             bootbox.confirm({
                //                 message: '<p style="font-size=150%;">El valor de la devolución <b>('+formatMoney(gtotal)+')</b> supera el saldo de la factura afectada  <b>('+formatMoney(purchase_balance)+')</b>, el valor restante <b>('+formatMoney(gtotal - purchase_balance)+')</b>, ¿Cómo se recibió?',
                //                 buttons: {
                //                     confirm: {
                //                         label: 'Registrar cómo anticipo',
                //                         className: 'btn-success send-submit-sale btn-full'
                //                     },
                //                     cancel: {
                //                         label: 'Recibido en efectivo',
                //                         className: 'btn-danger btn-full'
                //                     }
                //                 },
                //                 callback: function (result) {
                //                     if (result) {
                //                         $('#paid_by_1').select2('val', 'deposit').trigger('change');
                //                     } else {
                //                         $('#paid_by_1').select2('val', 'cash').trigger('change');
                //                     }
                //                 }
                //             });
                //         }, 800);
                //     }
            } else {
                $('#payments').fadeOut();
                $('#amount_1').val(0);
                $('.amount_to_paid').val(formatMoney(0));
            }
        } else if (payment_status == 'pending' || payment_status == 'due') {
            $('#amount_1').val(0);
            $('.amount_to_paid').val(formatMoney(0));
        } else if (payment_status == 'paid') {
            $('#payments').fadeIn();

            setTimeout(function() {
                bootbox.confirm({
                    message: 'La factura no tiene saldo, el valor total de la devolución <b>('+formatMoney(gtotal)+')</b>, ¿Cómo se recibió?',
                    buttons: {
                        confirm: {
                            label: 'Registrar cómo anticipo',
                            className: 'btn-success send-submit-sale btn-full'
                        },
                        cancel: {
                            label: 'Recibido en efectivo',
                            className: 'btn-danger btn-full'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $('#paid_by_1').select2('val', 'deposit').trigger('change');
                        } else {
                            $('#paid_by_1').select2('val', 'cash').trigger('change');
                        }
                    }
                });
            }, 800);
        }
    }

    function calculate_taxes(concept_value_input)
    {
        var tax_calculated_from_concept_note = calculate_tax_concept(concept_value_input);
        concept_value_input.parents('tr').find('#concept_tax').val(tax_calculated_from_concept_note);

        var total_calculated_from_concept_note = calculate_total_value_concept(concept_value_input);
        concept_value_input.parents('tr').find('#total_value_concept').val(total_calculated_from_concept_note);

        recalculate_withholdings();
        total_sums();
    }

    function calculate_tax_concept(concept_value_input)
    {
        var concept_tax_rate = $('#concept option:selected').data('concept_tax_rate');
        var concept_value = concept_value_input.val();
        var calculate_tax_note_concept = 0;

        if (concept_tax_rate > 0) {
            calculate_tax_note_concept = (concept_value * concept_tax_rate) / 100;
        }

        return formatDecimal(calculate_tax_note_concept);
    }

    function calculate_total_value_concept(concept_value_input)
    {
        var concept_value = concept_value_input.val();
        var concept_tax = concept_value_input.parents('tr').find('#concept_tax').val();

        var total_value_concept = parseFloat(concept_value) + parseFloat(concept_tax);

        return total_value_concept;
    }

    function recalculate_withholdings()
    {
        re_retenciones = JSON.parse(localStorage.getItem('invoice_withholdings'));

        if (re_retenciones != null) {
            if (re_retenciones.id_rete_fuente > 0 && re_retenciones.id_rete_fuente != null) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').iCheck('check');
                }
            }

            if (re_retenciones.id_rete_iva > 0 && re_retenciones.id_rete_iva != null) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').iCheck('check');
                }
            }

            if (re_retenciones.id_rete_ica > 0 && re_retenciones.id_rete_ica != null) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').iCheck('check');
                }
            }

            if (re_retenciones.id_rete_otros > 0 && re_retenciones.id_rete_otros != null) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').iCheck('check');
                }
            }

            setTimeout(function() {
                $.each($('#rete_fuente_option option'), function(index, value){
                    if(re_retenciones.id_rete_fuente != '' && value.value == re_retenciones.id_rete_fuente){
                        $('#rete_fuente_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_iva_option option'), function(index, value){
                    if(re_retenciones.id_rete_iva != '' && value.value == re_retenciones.id_rete_iva){
                        $('#rete_iva_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_ica_option option'), function(index, value){
                    if(re_retenciones.id_rete_ica != '' && value.value == re_retenciones.id_rete_ica){
                        $('#rete_ica_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_otros_option option'), function(index, value){
                    if(re_retenciones.id_rete_otros != '' && value.value == re_retenciones.id_rete_otros){
                        $('#rete_otros_option').select2('val', value.value).trigger('change');
                    }
                });

                save_credit_note_withholdings();

            }, 1000);
        }
    }

    function save_credit_note_withholdings()
    {
        var concept_value = $('#concept_value').val();
        var concept_tax = $('#concept_tax').val();
        var total_withholding = $('#total_rete_amount').text();

        _total_value_concept = (parseFloat(concept_value) + parseFloat(concept_tax)) - parseFloat(total_withholding);

        $('#retencion_show').val(parseFloat(total_withholding));
        $('#total_value_concept').val(formatDecimal(_total_value_concept, 4));

        $('#total_sum').text(formatDecimal(_total_value_concept, 4));

        $('#rtModal').modal('hide');
    }

    function save_return()
    {
        if (validate_form_inputs()) {
            $('#return_other_concepts_form').submit();
        }
    }

    function validate_form_inputs()
    {
        var total_sum = $('#total_sum').text();

        if (parseFloat(total_sum) == 0) {
            Command: toastr.error('No se puede crear La Nota Crédito. Por favor diligencie el formulario.', '<?= lang("toast_error_title"); ?>');
            return false;
        }

        // if (parseFloat(total_sum) > $('#balance').val()) {
        //     Command: toastr.error('El total de la Nota crédito es mayor al Saldo total de la factura afectada.', '<?= lang("toast_error_title"); ?>');
        //     return false;
        // }

        return true;
    }

    function loadPaymentMeanCode(control)
    {
        var codeFe = $('.paid_by option:selected').data('code_fe');
        $('#mean_payment_code_fe').val(codeFe);

        if (control.val() == 'deposit') {
            $('.deposit_div').fadeIn();
            $.ajax({
                url:'<?= admin_url("billers/getBillersDocumentTypes/15/") ?>'+$('#biller_id').val(),
                type:'get',
                dataType:'JSON'
            }).done(function(data){

                response = data;
                $('#deposit_document_type_id').html(response.options).select2();

                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                }
                $('#deposit_document_type_id').trigger('change');
            });
        } else {
            $('.deposit_div').fadeOut();
        }
    }
</script>