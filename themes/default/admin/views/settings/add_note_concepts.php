<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <?= admin_form_open("system_settings/add_note_concepts" . '', ['id' => 'add_note_concepts_form']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="myModalLabel"><?= lang('concept_notes_add'); ?></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <?= lang('concept_notes_type', 'type') ?>
                        <?php
                            $type_note_options = [
                                'NC' => lang('concept_notes_credit_note'),
                                'ND' => lang('concept_notes_debit_note'),
                                'NCC' => lang('concept_notes_credit_note_purchases')
                            ];
                        ?>
                        <?= form_dropdown('type', $type_note_options, '', 'class="form-control" id="type" style="width:100%;" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= lang('concept_notes_dian_code', 'dian_code') ?>
                        <select class="form-control" name="dian_code" id="dian_code" required="required">
                            <option value=""><?= lang("select"); ?></option>
                            <?php foreach ($dian_note_concepts as $dian_code) { ?>
                                <option value="<?= $dian_code->code; ?>"><?= $dian_code->name; ?></option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="dian_description" id="dian_description">
                    </div>
                    <div class="form-group col-md-12">
                        <?= lang('concept_notes_description', 'description') ?>
                        <?= form_input('description', '', 'class="form-control" id="description" required="required"'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <?= lang('concept_notes_tax', 'tax') ?>
                        <?php $taxes ?>
                        <?php
                            $tax_options = ['' => lang("select")];

                            foreach ($taxes as $tax) {
                                $tax_options[$tax->id] = $tax->name;
                            }
                        ?>
                        <?= form_dropdown('tax', $tax_options, '', 'class="form-control" id="tax" style="width:100%;"'); ?>
                    </div>
                    <?php if ($this->Settings->modulary == YES) { ?>
                        <div class="form-group col-md-6">
                            <?= lang('concept_notes_tax_amount_ledger_account', 'tax_amount_ledger_account') ?>
                            <?php
                                $tax_amount_ledger_account_option = [ '' => lang('select') ];
                                foreach ($tax_amount_ledger_account as $tax_account) {
                                    $tax_amount_ledger_account_option[$tax_account->id] = $tax_account->code ." - ". $tax_account->notes;
                                }
                            ?>
                            <?= form_dropdown('tax_amount_ledger_account', $tax_amount_ledger_account_option, '', 'class="form-control" id="tax_amount_ledger_account" style="width:100%;"'); ?>
                        </div>

                    <?php } else { ?>
                        <div class="form-group col-md-6">
                            <?= form_label(lang('concept_notes_tax_base_ledger_account'), 'tax_base_ledger_account'); ?>
                            <div class="checkbox" style="margin-top: 0px;">
                                <input type="checkbox" class="checkbox" name="view" id="view" value="1" checked="checked">
                                <label for="view" style="padding-left: 0px; font-weight: bolder;"><?= lang("concept_notes_active"); ?></label>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="row">
                    <?php if ($this->Settings->modulary == YES) { ?>
                        <div class="form-group col-md-6">
                            <?= form_label(lang('concept_notes_tax_base_ledger_account'), 'tax_base_ledger_account'); ?>
                            <?php
                                $tax_base_ledger_account_option = [ '' => lang('select') ];
                                foreach ($tax_amount_ledger_account as $tax_account) {
                                    $tax_base_ledger_account_option[$tax_account->id] = $tax_account->code ." - ". $tax_account->notes;
                                }
                            ?>
                            <?= form_dropdown('tax_base_ledger_account', $tax_base_ledger_account_option, '', 'class="form-control" id="tax_base_ledger_account" style="width:100%;"'); ?>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="view" style="margin-top: 25px;">
                                        <input type="checkbox" class="checkbox" name="view" id="view" value="1" checked="checked"> <strong><?= lang("concept_notes_active"); ?></strong>
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="applied_selfretention" style="padding-left: 0">
                                    <input type="checkbox" name="applied_selfretention" id="applied_selfretention"> <strong><?= $this->lang->line("Aplicar Autoretención") ?></strong>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary new-button" id="submit" type="submit" data-toggle="tooltip" data-placement="top" title="Agregar"><i class="fas fa-check fa-lg"></i></button>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('change', '#type', function() { get_dian_note_concept($(this).val()); });
        $(document).on('change', '#dian_code', function() { get_dian_description(); });
        $(document).on('click', '#submit', function() { save_note_concept(); });
        $(document).on('change', '#tax', function() { validate_account_field_obligation(); })

        $('select').select2();

        $('[data-toggle="tooltip"]').tooltip();

        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '20%'
        });
    });

    function get_dian_note_concept(type)
    {
        $.ajax({
            url: '<?= admin_url("system_settings/get_dian_note_concept") ?>',
            type: 'POST',
            dataType: 'HTML',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'type': type
            },
        })
        .done(function(data) {
            $('#dian_code').select2('val', '');
            $('#dian_code').html(data);
        })
        .fail(function(data) {
            Command: toastr.error('Se produjo en error. Por contacte con el administrador de la plataforma.', "Mensaje de error");
            console.log(data.responseText);
        });
    }

    function get_dian_description()
    {
        var dian_description = $('#dian_code option:selected').text();
        $('#dian_description').val(dian_description);

    }

    function validate_account_field_obligation()
    {
        var tax = $('#tax').val();
        var accouting_module = '<?= $this->Settings->modulary; ?>';

        if (tax != '' && accouting_module == 1) {
            $('#tax_amount_ledger_account').attr('required', true);
            $('#tax_base_ledger_account').attr('required', true);
        } else {
            $('#tax_amount_ledger_account').select2('val', '');
            $('#tax_amount_ledger_account').attr('required', false);

            $('#tax_base_ledger_account').select2('val', '');
            $('#tax_base_ledger_account').attr('required', false);
        }
    }

    function save_note_concept()
    {
        if (validate_inputs('add_note_concepts_form')) {
            $('#add_note_concepts_form').submit();
        }
    }

    function validate_inputs(form)
    {
        var validation_errors = true;

        $.each($('#'+form).serializeArray(), function(i, field) {
            if ($('#'+field.name).attr('required') == 'required') {
                if($('#'+field.name).val() == '') {
                    if ($('#'+field.name).parent('.form-group').find('#'+field.name+'-error').length == 0) {
                        $('#'+field.name).parent('.form-group').append('<label id="'+field.name+'-error" class="text-danger" for="dian_code">Este campo es obligatorio</label>');
                    }

                    validation_errors = false;
                } else {
                    $('#'+field.name+'-error').remove();
                }
            } else {
                if ($('#'+field.name).parent('.form-group').find('#'+field.name+'-error').length > 0) {
                    $('#'+field.name+'-error').remove();
                }
            }
        });

        return validation_errors;
    }
</script>