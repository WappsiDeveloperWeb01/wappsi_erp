<style type="text/css">
    .error {
        color :#f17272;
    }

    .blue input[type=checkbox]:checked+span::before,
    .blue input[type=radio]:checked+span::before {
        border-radius: 19px !important;
        height: 20px !important;
        width: 20px !important;
        padding-top: 2px;
    }

    input[type=checkbox]+span::before, input[type=radio]+span::before {
        height: 20px !important;
        width: 20px !important;
        border-radius: 19px !important;
        padding-top: 2px;
    }
    <?php if ($shop_settings->icons_color): ?>
        .dropdown-menu-login{
            color: <?= $shop_settings->icons_color ?>!important;
        }
        .shopping-cart{
            color: <?= $shop_settings->icons_color ?>!important;
        }
    <?php endif ?>
    <?php if ($shop_settings->primary_color): ?>
        .total_cart_items{
            background-color: <?= $shop_settings->primary_color ?>!important;
        }
        .blue,
        .blue .theme {
/*            background-color: <?= $shop_settings->primary_color ?>!important;*/
        }

        .blue .btn-theme {
            color: #fff;
            background-color: <?= $shop_settings->primary_color ?>;
            border-color: <?= $shop_settings->primary_color ?>
        }

        .blue .btn-theme.focus,
        .blue .btn-theme:focus {
            color: #fff;
            background-color: <?= $shop_settings->secondary_color ?>;
            border-color: <?= $shop_settings->secondary_color ?>
        }

        .blue .btn-theme.active,
        .blue .btn-theme:active,
        .blue .btn-theme:hover,
        .open>.blue .btn-theme.dropdown-toggle {
            color: #fff;
            background-color: <?= $shop_settings->secondary_color ?>;
            border-color: <?= $shop_settings->secondary_color ?>
        }

        .blue .btn-theme.active.focus,
        .blue .btn-theme.active:focus,
        .blue .btn-theme.active:hover,
        .blue .btn-theme:active.focus,
        .blue .btn-theme:active:focus,
        .blue .btn-theme:active:hover,
        .open>.blue .btn-theme.dropdown-toggle.focus,
        .open>.blue .btn-theme.dropdown-toggle:focus,
        .open>.blue .btn-theme.dropdown-toggle:hover {
            color: #fff;
            background-color: <?= $shop_settings->secondary_color ?>;
            border-color: <?= $shop_settings->secondary_color ?>
        }

        .blue .btn-theme.active,
        .blue .btn-theme:active,
        .open>.blue .btn-theme.dropdown-toggle {
            background-image: none
        }

        .blue .btn-theme.disabled.focus,
        .blue .btn-theme.disabled:focus,
        .blue .btn-theme.disabled:hover,
        .blue .btn-theme[disabled].focus,
        .blue .btn-theme[disabled]:focus,
        .blue .btn-theme[disabled]:hover,
        fieldset[disabled] .blue .btn-theme.focus,
        fieldset[disabled] .blue .btn-theme:focus,
        fieldset[disabled] .blue .btn-theme:hover {
            background-color: <?= $shop_settings->primary_color ?>;
            border-color: <?= $shop_settings->primary_color ?>
        }

        .blue .btn-theme .badge {
            color: <?= $shop_settings->primary_color ?>;
            background-color: #fff
        }

        <?php if (!$shop_settings->font_text_color): ?>
            .blue a {
                color: <?= $shop_settings->primary_color ?>
            }

            .blue a:active,
            .blue a:focus,
            .blue a:hover {
                color: <?= $shop_settings->primary_color ?>
            }
        <?php endif ?>

        .blue a.btn-danger,
        .blue a.btn-info,
        .blue a.btn-primary {
            color: #fff!important
        }

        .blue .form-control:focus {
            border-color: <?= $shop_settings->primary_color ?>
        }

        .blue input[type=checkbox]:checked+span::before,
        .blue input[type=radio]:checked+span::before {
            background-color: <?= $shop_settings->secondary_color ?> !important;
        }

        .blue input[type=checkbox]:checked+span::before,
        .blue input[type=radio]:checked+span::before {
            color: white !important;
        }

        input[type=checkbox]+span::before, input[type=radio]+span::before {
            border: 1px solid <?= $shop_settings->secondary_color ?> !important;
        }

        .blue .bg label {
            width: 100%;
            padding: 0
        }

        .blue .bg input[type=checkbox]+span,
        .blue .bg input[type=radio]+span {
            display: block;
            padding: 10px;
            width: 100%
        }

        .blue .bg input[type=checkbox]+span::before,
        .blue .bg input[type=checkbox]:checked+span::before,
        .blue .bg input[type=radio]+span::before,
        .blue .bg input[type=radio]:checked+span::before {
            display: none
        }

        .blue .bg input[type=checkbox]:checked+span,
        .blue .bg input[type=radio]:checked+span {
            color: #fff;
            background-color: <?= $shop_settings->primary_color ?>
        }

        .blue .title {
            border-bottom: 3px solid <?= $shop_settings->secondary_color ?>
        }

        .blue .title span {
            border-bottom: 3px solid <?= $shop_settings->secondary_color ?>
        }

        .blue .bootstrap-select .dropdown-toggle:focus,
        .blue .bootstrap-select.open .dropdown-toggle,
        .blue .bootstrap-select.open .dropdown-toggle:hover,
        .blue .img-thumbnail:hover {
            border-color: <?= $shop_settings->primary_color ?>
        }

        .blue .top-header ul.list-inline>li .dropdown-toggle,
        .blue .top-header ul.list-inline>li>a {
            color: #fff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, .2)
        }

        .blue .top-header ul.list-inline>li .dropdown-toggle:active,
        .blue .top-header ul.list-inline>li .dropdown-toggle:focus,
        .blue .top-header ul.list-inline>li .dropdown-toggle:hover,
        .blue .top-header ul.list-inline>li>a:active,
        .blue .top-header ul.list-inline>li>a:focus,
        .blue .top-header ul.list-inline>li>a:hover {
            background-color: <?= $shop_settings->secondary_color ?>;
            border-color: <?= $shop_settings->secondary_color ?>
        }

        .blue .top-header ul.list-inline>li .bootstrap-select.open>.dropdown-toggle,
        .blue .top-header ul.list-inline>li .dropdown.open>.dropdown-toggle {
            background-color: <?= $shop_settings->secondary_color ?>;
            color: #fff
        }

        .blue .top-header ul.list-inline>li:focus,
        .blue .top-header ul.list-inline>li:hover {
            background-color: <?= $shop_settings->secondary_color ?>
        }

        .blue .main-header .btn-search {
            background-color: <?= $shop_settings->primary_color ?>;
            border-color: transparent;
            color: #fff
        }

        .blue .main-header .btn-search:active,
        .blue .main-header .btn-search:focus {
            background-color: <?= $shop_settings->secondary_color ?>;
            border-color: transparent;
            color: #fff;
            outline: 0
        }

        .blue .main-header .cart-btn>button:hover {
            border-color: <?= $shop_settings->primary_color ?>
        }

        .blue .main-header .cart-btn.open>button {
            border-color: <?= $shop_settings->primary_color ?>!important
        }

        .blue .navbar .navbar-nav>.active>a,
        .blue .navbar .navbar-nav>.active>a:active,
        .blue .navbar .navbar-nav>.active>a:focus,
        .blue .navbar .navbar-nav>.active>a:hover,
        .blue .navbar .navbar-nav>.open>a,
        .blue .navbar .navbar-nav>.open>a:active,
        .blue .navbar .navbar-nav>.open>a:focus,
        .blue .navbar .navbar-nav>.open>a:hover,
        .blue .navbar .navbar-nav>li>a:active,
        .blue .navbar .navbar-nav>li>a:focus,
        .blue .navbar .navbar-nav>li>a:hover {
            background-color: #ffffff00;
            border-style: solid;
            border-width: 0.2px;
            border-color: <?= $shop_settings->secondary_color ?>;
            color: <?= $shop_settings->secondary_color ?> !important
        }

        .blue .navbar .btn-cart-xs:active,
        .blue .navbar .btn-cart-xs:focus,
        .blue .navbar .btn-cart-xs:hover,
        .blue .navbar .navbar-toggle:active,
        .blue .navbar .navbar-toggle:focus,
        .blue .navbar .navbar-toggle:hover {
            border-color: <?= $shop_settings->primary_color ?>
        }

        .blue .breadcrumb-container .breadcrumb>li>a {
            color: <?= $shop_settings->primary_color ?>
        }

        .blue .featured-products .product .stats-container .product_price {
            color: <?= $shop_settings->primary_color ?>!important
        }

        .blue .featured-products .product .btn:hover{
            background-color: <?= $shop_settings->primary_color ?>!important;
            border-color: <?= $shop_settings->primary_color ?>!important;
            color :white;
        }

        .blue .featured-products .product .image_overlay {
            background: <?= $shop_settings->primary_color ?>!important
        }

        .blue .pagination>.active>a,
        .blue .pagination>.active>a:focus,
        .blue .pagination>.active>a:hover,
        .blue .pagination>.active>span,
        .blue .pagination>.active>span:focus,
        .blue .pagination>.active>span:hover {
            background-color: <?= $shop_settings->primary_color ?>;
            border-color: <?= $shop_settings->primary_color ?>
        }

        .blue .footer a {
            color: <?= $shop_settings->primary_color ?>
        }

        .blue .footer a:hover {
            color: #349ff4
        }

        .blue .footer .follow-us li a:hover {
            background-color: <?= $shop_settings->primary_color ?>;
            color: #fff
        }

        .navbar .navbar-toggle {
            background-color: <?= $shop_settings->primary_color ?> !important;
        }

        .btn-primary {
            background-color: <?= $shop_settings->primary_color ?> !important;
            border-color: <?= $shop_settings->primary_color ?> !important;
        }

        .btn-primary.btn-outline{
            background-color: #fff !important;
            color: <?= $shop_settings->primary_color ?> !important;
            border-color: <?= $shop_settings->primary_color ?> !important;
        }

        .btn-primary.btn-outline:hover{
            background-color: <?= $shop_settings->secondary_color ?> !important;
            border-color: <?= $shop_settings->secondary_color ?> !important;
            color: white !important;
        }
        .category_link_square:hover{
/*            background-color: <?= $shop_settings->secondary_color ?> !important;*/
            border-color: <?= $shop_settings->secondary_color ?> !important;
            color: <?= $shop_settings->secondary_color ?> !important;
            font-weight: 600 !important;
        }
    <?php else: ?>
        .blue,
        .blue .theme {
            background-color: #2196f3!important
        }
        .promo_badge {
            background-color: #2196f3!important;
        }

        .blue .btn-theme {
            color: #fff;
            background-color: #2196f3;
            border-color: #0e8df2
        }

        .blue .btn-theme.focus,
        .blue .btn-theme:focus {
            color: #fff;
            background-color: #0c7cd5;
            border-color: #07477a
        }

        .blue .btn-theme.active,
        .blue .btn-theme:active,
        .blue .btn-theme:hover,
        .open>.blue .btn-theme.dropdown-toggle {
            color: #fff;
            background-color: #0c7cd5;
            border-color: #0a6bb8
        }

        .blue .btn-theme.active.focus,
        .blue .btn-theme.active:focus,
        .blue .btn-theme.active:hover,
        .blue .btn-theme:active.focus,
        .blue .btn-theme:active:focus,
        .blue .btn-theme:active:hover,
        .open>.blue .btn-theme.dropdown-toggle.focus,
        .open>.blue .btn-theme.dropdown-toggle:focus,
        .open>.blue .btn-theme.dropdown-toggle:hover {
            color: #fff;
            background-color: #0a68b4;
            border-color: #07477a
        }

        .blue .btn-theme.active,
        .blue .btn-theme:active,
        .open>.blue .btn-theme.dropdown-toggle {
            background-image: none
        }

        .blue .btn-theme.disabled.focus,
        .blue .btn-theme.disabled:focus,
        .blue .btn-theme.disabled:hover,
        .blue .btn-theme[disabled].focus,
        .blue .btn-theme[disabled]:focus,
        .blue .btn-theme[disabled]:hover,
        fieldset[disabled] .blue .btn-theme.focus,
        fieldset[disabled] .blue .btn-theme:focus,
        fieldset[disabled] .blue .btn-theme:hover {
            background-color: #2196f3;
            border-color: #0e8df2
        }

        .blue .btn-theme .badge {
            color: #2196f3;
            background-color: #fff
        }

        .blue a {
            color: #2196f3
        }

        .blue a:active,
        .blue a:focus,
        .blue a:hover {
            color: #0c82df
        }

        .blue a.btn-danger,
        .blue a.btn-info,
        .blue a.btn-primary {
            color: #fff!important
        }

        .blue .form-control:focus {
            border-color: #2196f3
        }

        .blue input[type=checkbox]:checked+span::before,
        .blue input[type=radio]:checked+span::before {
            color: #2196f3
        }

        .blue .bg label {
            width: 100%;
            padding: 0
        }

        .blue .bg input[type=checkbox]+span,
        .blue .bg input[type=radio]+span {
            display: block;
            padding: 10px;
            width: 100%
        }

        .blue .bg input[type=checkbox]+span::before,
        .blue .bg input[type=checkbox]:checked+span::before,
        .blue .bg input[type=radio]+span::before,
        .blue .bg input[type=radio]:checked+span::before {
            display: none
        }

        .blue .bg input[type=checkbox]:checked+span,
        .blue .bg input[type=radio]:checked+span {
            color: #fff;
            background-color: #2196f3
        }

        .blue .title {
            border-bottom: 3px solid #0e8df2
        }

        .blue .title span {
            border-bottom: 3px solid #0c82df
        }

        .blue .bootstrap-select .dropdown-toggle:focus,
        .blue .bootstrap-select.open .dropdown-toggle,
        .blue .bootstrap-select.open .dropdown-toggle:hover,
        .blue .img-thumbnail:hover {
            border-color: #2196f3
        }

        .blue .top-header {
            background-color: #2196f3
        }

        .blue .top-header ul.list-inline>li .dropdown-toggle,
        .blue .top-header ul.list-inline>li>a {
            color: #fff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, .2)
        }

        .blue .top-header ul.list-inline>li .dropdown-toggle:active,
        .blue .top-header ul.list-inline>li .dropdown-toggle:focus,
        .blue .top-header ul.list-inline>li .dropdown-toggle:hover,
        .blue .top-header ul.list-inline>li>a:active,
        .blue .top-header ul.list-inline>li>a:focus,
        .blue .top-header ul.list-inline>li>a:hover {
            background-color: #0e8df2;
            border-color: #0e8df2
        }

        .blue .top-header ul.list-inline>li .bootstrap-select.open>.dropdown-toggle,
        .blue .top-header ul.list-inline>li .dropdown.open>.dropdown-toggle {
            background-color: #0c82df;
            color: #fff
        }

        .blue .top-header ul.list-inline>li:focus,
        .blue .top-header ul.list-inline>li:hover {
            background-color: #0e8df2
        }

        .blue .main-header .btn-search {
            background-color: #2196f3;
            border-color: transparent;
            color: #fff
        }

        .blue .main-header .btn-search:active,
        .blue .main-header .btn-search:focus {
            background-color: #0c82df;
            border-color: transparent;
            color: #fff;
            outline: 0
        }

        .blue .main-header .cart-btn>button:hover {
            border-color: #2196f3
        }

        .blue .main-header .cart-btn.open>button {
            border-color: #2196f3!important
        }

        .blue .navbar .navbar-nav>.active>a,
        .blue .navbar .navbar-nav>.active>a:active,
        .blue .navbar .navbar-nav>.active>a:focus,
        .blue .navbar .navbar-nav>.active>a:hover,
        .blue .navbar .navbar-nav>.open>a,
        .blue .navbar .navbar-nav>.open>a:active,
        .blue .navbar .navbar-nav>.open>a:focus,
        .blue .navbar .navbar-nav>.open>a:hover,
        .blue .navbar .navbar-nav>li>a:active,
        .blue .navbar .navbar-nav>li>a:focus,
        .blue .navbar .navbar-nav>li>a:hover {
            background-color: #2196f3;
            color: #fff!important
        }

        .blue .navbar .btn-cart-xs:active,
        .blue .navbar .btn-cart-xs:focus,
        .blue .navbar .btn-cart-xs:hover,
        .blue .navbar .navbar-toggle:active,
        .blue .navbar .navbar-toggle:focus,
        .blue .navbar .navbar-toggle:hover {
            border-color: #2196f3
        }

        .blue .breadcrumb-container .breadcrumb>li>a {
            color: #2196f3
        }

        .blue .featured-products .product .btn:hover,
        .blue .featured-products .product .stats-container .product_price {
            color: #2196f3!important
        }

        .blue .featured-products .product .image_overlay {
            background: #2196f3!important
        }

        .blue .pagination>.active>a,
        .blue .pagination>.active>a:focus,
        .blue .pagination>.active>a:hover,
        .blue .pagination>.active>span,
        .blue .pagination>.active>span:focus,
        .blue .pagination>.active>span:hover {
            background-color: #2196f3;
            border-color: #2196f3
        }

        .blue .footer a {
            color: #2196f3
        }

        .blue .footer a:hover {
            color: #349ff4
        }

        .blue .footer .follow-us li a:hover {
            background-color: #2196f3;
            color: #fff
        }
    <?php endif ?>

    <?php if ($shop_settings->bar_menu_color): ?>
        .navbar {
            background-color: <?= $shop_settings->bar_menu_color ?> !important;
        }            
        .navbar .navbar-nav>li>a {
            color: #fff !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->font_text_color): ?>
        .blue a {
            color: <?= $shop_settings->font_text_color ?>
        }

        .blue a:active,
        .blue a:focus,
        .blue a:hover {
            color: <?= $shop_settings->font_text_color ?>
        }

        .blue {
            color: <?= $shop_settings->font_text_color ?> !important;
        }

        .blue .title {
            color : <?= $shop_settings->font_text_color ?>
        }

        .blue .title span {
            color : <?= $shop_settings->font_text_color ?>
        }

        .header_message {
            color: <?= $shop_settings->font_text_color ?> !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->font_family): ?>
        .swal2-modal, body {
            font-family: <?= $shop_settings->font_family ?> !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->background_page_color): ?>
        section.page-contents, section.page-contents > .container
        {
            background-color: <?= $shop_settings->background_page_color ?> !important;
        }
        .promo_badge {
            background-color: <?= $shop_settings->background_page_color ?> !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->header_color): ?>
        .main-header, header {
            background-color: <?= $shop_settings->header_color ?> !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->footer_color): ?>
        .footer {
            background-color: <?= $shop_settings->footer_color ?> !important;
        }

        .footer-bottom {
            background-color: <?= $shop_settings->footer_color ?> !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->header_message_color): ?>
        .header_message {
            background-color: <?= $shop_settings->header_message_color ?> !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->navbar_color): ?>
        .blue .top-header {
            background-color: <?= $shop_settings->navbar_color ?>
        }
    <?php endif ?>

    <?php if ($shop_settings->navbar_text_color): ?>
        .blue .top-header ul.list-inline>li .dropdown-toggle, .blue .top-header ul.list-inline>li>a {
            color: <?= $shop_settings->navbar_text_color ?> !important;
        }
    <?php endif ?>



    <?php if ($shop_settings->bar_menu_text_color): ?>
        .navbar .navbar-nav>li>a {
            color: <?= $shop_settings->bar_menu_text_color ?> !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->footer_text_color): ?>
        .footer .title-footer>span {
            color: <?= $shop_settings->footer_text_color ?> !important;
        }
        .footer {
            color: <?= $shop_settings->footer_text_color ?> !important;
        }
        .blue .footer a {
            color: <?= $shop_settings->footer_text_color ?> !important;
        }
        .back-to-top, .footer .footer-bottom {
            color: <?= $shop_settings->footer_text_color ?> !important;
        }
    <?php endif ?>

    <?php if ($shop_settings->promotion_text_color): ?>
        .promo_badge {
            background-color: <?= $shop_settings->promotion_text_color ?> !important;
        }
    <?php endif ?>

        .wishlist_inactive{
            color: #afafaf !important;
            border-color: #afafaf !important;
            border-width: 2px !important;
            background-color: #ffffff00 !important;
        }


    <?php if ($shop_settings->favorite_color): ?>
        .wishlist_active{
            color: white !important;
            border-color: <?= $shop_settings->favorite_color ?> !important;
            background-color: <?= $shop_settings->favorite_color ?> !important;
        }

        .wishlist_inactive:hover{
            color: white !important;
            border-color: <?= $shop_settings->favorite_color ?> !important;
            background-color: <?= $shop_settings->favorite_color ?> !important;
        }
        .wishlist_inactive:hover .fa-heart-o:before {
            content : "\f004" !important;
        }
    <?php else: ?>

    <?php endif ?>

    <?php if ($shop_settings->item_margin): ?>
        .product > .product-top > .product-image  {
            border-style: solid !important;
            border-width: 1px;
            border-radius: 11px;
            border-color: #cfcccc;
        }
        .category_link_square{
            border-style: solid;
            border-width: 1px;
        }
    <?php else: ?>
        .category_link_square{
            border-style: none !important;
        }
    <?php endif ?>


    <?php if ($shop_settings->item_frame_rounding): ?>
        .img_banner{
            border-radius: 21px;
        }
        .product > .product-top > .product-image > a > .img-responsive,
        .product > .product-top > .product-image > a > .img-responsive {
            border-radius: 11px;
        }
    <?php else: ?>
        .add-to-cart {
            border-radius: 0px !important;
        }
    <?php endif ?>

</style>