<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?= lang('preparation_order') ?></title>
        <base href="<?=base_url()?>"/>
        <style type="text/css">
            table {
                border-collapse: collapse;
                font-size: 80%; 
                width: 100%;
            }

            table > tbody > tr > td {
                border-style: solid;
                border-width: 0px 0px 1px 0px;
            }
            table > thead > tr > th {
                border-style: solid;
                border-width: 0px 0px 1px 0px;
            }

            .tr_preferences > td {
                padding: 0px 10px 0px 10px ! important;
                margin: : 0px! important;
                background-color: #f3f3f3;
                font-size: 85%;
            }
        </style>
    </head>

    <body style="margin: 0px; padding: 0px;">
        <div style="text-align: center;">
            <p style="font-size: 100%;"><b><?= lang('preparation_order') ?></b></p>
            <p style="font-size: 80%;"><b><?= $preparation_area ?></b></br>
               <b><?= lang('table_number')." : ".$table_number ?></b></br>
               <b><?= lang('seller')." : ".$seller->name ?></b></br>
               <b><?= lang('date')." : ".date('Y-m-d H:i:s') ?></b></p>
            <table class="table">
                <thead>
                    <tr>
                        <th style="text-align: center;"><?= lang('name') ?></th>
                        <th style="text-align: center;"><?= lang('quantity') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $product): ?>
                        <tr style="<?= isset($product['cancelled']) ? 'text-decoration: line-through;' : ''; ?>">
                            <td><?= $product['product_name'] ?></td>
                            <td style="text-align: right;"><?= $this->sma->formatQuantity($product['quantity']) ?></td>
                        </tr>    
                        <?php if ($product['preferences'] != NULL): ?>
                            <tr class="tr_preferences" style="<?= isset($product['cancelled']) ? 'text-decoration: line-through;' : ''; ?>">
                                <td colspan="2"><?= $product['preferences'] ?></td>
                            </tr>    
                        <?php endif ?>
                    <?php endforeach ?>                        
                </tbody>
            </table>
        </div>
    </body>
</html>