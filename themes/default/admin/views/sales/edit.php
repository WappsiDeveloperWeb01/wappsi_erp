<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    

    var count = 1, an = 1, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0, invoice_tax = 0, product_discount = 0, order_discount = 0, total_discount = 0, total = 0, allow_discount = <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? 1 : 0; ?>,
        tax_rates = <?php echo json_encode($tax_rates); ?>;
    var protect_delete = false;
    var protect_delete_pin_request = false;
    var order_sale = 0;
    var is_quote = false;
    var is_duplicate = false;
    var is_fe_pos_sale = false;
    var is_sale_edit = true;
    var sale_has_payments = false;
    var get_customer_payment = false;
    var sale_edit_id = "<?= $id ?>";
    if (localStorage.getItem('price_updated')) {
        localStorage.removeItem('price_updated');
    }
    <?php if ($id) { ?>
        if (localStorage.getItem('keep_prices_quote_id')) {
            if (localStorage.getItem('keep_prices_quote_id') != "<?= $id ?>") {
                localStorage.removeItem('keep_prices');
                localStorage.removeItem('keep_prices_quote_id');
            }
        } else {
            localStorage.removeItem('keep_prices');
            localStorage.removeItem('keep_prices_quote_id');
        }
    <?php } else { ?>
        localStorage.removeItem('keep_prices');
        localStorage.removeItem('keep_prices_quote_id');
    <?php } ?>
    localStorage.removeItem('aiu_management');
<?php 
    if ($inv->aiu_management) {
        $aiu_management = 
            [
                'aiu_admin_percentage' => $inv->aiu_admin_percentage,
                'aiu_imprev_percentage' => $inv->aiu_imprev_percentage,
                'aiu_util_percentage' => $inv->aiu_utilidad_percentage,
                'aiu_tax' => $inv->order_tax_aiu_id,
                'aiu_tax_base_apply_to' => $inv->order_tax_aiu_base_apply_to,
                'afecta_aiu' => $inv->aiu_amounts_affects_grand_total,
            ];
            ?>
            localStorage.setItem('aiu_management', '<?= json_encode($aiu_management) ?>');
            <?php
    }
 ?>
 console.log('AIU MANAGE');
 console.log(JSON.parse(localStorage.getItem('aiu_management')));
    $(document).ready(function () {

            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }
            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }
            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }
            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }
            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }
            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }
            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }
            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }
            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }
            if (localStorage.getItem('slseller')) {
                localStorage.removeItem('slseller');
            }
            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }
            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }
            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }
            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('payment_note_1')) {
                localStorage.removeItem('payment_note_1');
            }
            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }
            if (localStorage.getItem('slretenciones')) {
                localStorage.removeItem('slretenciones');
            }
            if (localStorage.getItem('othercurrency')) {
                localStorage.removeItem('othercurrency');
            }
            if (localStorage.getItem('othercurrencycode')) {
                localStorage.removeItem('othercurrencycode');
            }
            if (localStorage.getItem('othercurrencytrm')) {
                localStorage.removeItem('othercurrencytrm');
            }
            if (localStorage.getItem('slordersale')) {
                localStorage.removeItem('slordersale');
            }
            if (localStorage.getItem('customer_validate_min_base_retention')) {
                localStorage.removeItem('customer_validate_min_base_retention');
            }

        <?php if($id) { ?>

            $('#add_sale').css('display', 'none');

            command: toastr.warning('Por favor espere unos segundos...', 'Espere...');
            setTimeout(function() {
                $('#add_sale').css('display', '');
                command: toastr.success('Ya se completó la carga', 'Completo');
            }, 3000);

            localStorage.setItem('slcustomer', '<?= $inv->customer_id ?>');
            localStorage.setItem('slpayment_status', '<?= isset($inv->payment_status) ? $inv->payment_status : NULL ?>');
            <?php if (isset($inv->payment_method) && $inv->payment_method != 'Credito' && !empty($inv->payment_method) && $inv->payment_method != "''"): ?>
                localStorage.setItem('paid_by_1', '<?= $inv->payment_method ?>');
            <?php else: ?>
                payment_term_quote = formatDecimal(parseFloat('<?= isset($inv->payment_term) ? $inv->payment_term : null ?>'));
                if (payment_term_quote == 0) {
                    get_customer_payment = true;
                }
                localStorage.setItem('paid_by_1', 'Credito');
                $('#paid_by_1').select2('val', 'Credito').trigger('change');
                $('#sale_payment_term').val('<?= isset($inv->payment_term) ? $inv->payment_term : null  ?>');
                $('#slpayment_term_1').val('<?= isset($inv->payment_term) ? $inv->payment_term : null  ?>');
            <?php endif ?>
            localStorage.setItem('slsale_status', '<?= $inv->sale_status ?>');
            localStorage.setItem('slpayment_term', '<?= isset($inv->payment_term) ? $inv->payment_term : null  ?>');
            localStorage.setItem('slcustomerbranch', '<?= $inv->address_id ?>');
            localStorage.setItem('slbiller', '<?= $inv->biller_id ?>');
            localStorage.setItem('slseller', '<?= $inv->seller_id ?>');
            localStorage.setItem('slwarehouse', '<?= $inv->warehouse_id ?>');
            localStorage.setItem('slnote', '<?= isset($inv->note) ? str_replace(array("\r", "\n"), "", $this->sma->decode_html($inv->note)) : ''; ?>');
            localStorage.setItem('sldiscount', '<?= isset($inv->order_discount_id) ? $inv->order_discount_id : NULL ?>');
            localStorage.setItem('sltax2', '<?= isset($inv->order_tax_id) ? $inv->order_tax_id : NULL ?>');
            localStorage.setItem('slshipping', '<?= isset($inv->shipping) ? $inv->shipping : NULL ?>');
            localStorage.setItem('slitems', '<?= $inv_items ?>');
            localStorage.setItem('othercurrency', JSON.parse('<?= $inv->sale_currency ? "true" : "false" ?>'));
            localStorage.setItem('othercurrencycode', '<?= $inv->sale_currency ?>');
            localStorage.setItem('othercurrencytrm', '<?= $inv->sale_currency_trm ?>');

            <?php if (isset($inv->quote_currency) && $inv->quote_currency != $this->Settings->default_currency) { ?>
                localStorage.setItem('othercurrency', true);
                localStorage.setItem('othercurrencycode', '<?= $inv->quote_currency ?>');
                localStorage.setItem('othercurrencytrm', '<?= number_format($inv->quote_currency_trm, 0, '', '') ?>');
            <?php } ?>

            var slretenciones = {
                'gtotal' : '<?= $inv->grand_total ?>',
                'total_rete_amount' : '<?= $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total ?>',
                'fuente_option' : '<?= $inv->rete_fuente_percentage ?>',
                'id_rete_fuente' : '<?= $inv->rete_fuente_id ?>',
                'iva_option' : '<?= $inv->rete_iva_percentage ?>',
                'id_rete_iva' : '<?= $inv->rete_iva_id ?>',
                'ica_option' : '<?= $inv->rete_ica_percentage ?>',
                'id_rete_ica' : '<?= $inv->rete_ica_id ?>',
                'otros_option' : '<?= $inv->rete_other_percentage ?>',
                'id_rete_otros' : '<?= $inv->rete_other_id ?>',
            };
            slretenciones.total_discounted = '<?= $inv->grand_total - ($inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total) ?>';
            localStorage.setItem('slretenciones', JSON.stringify(slretenciones));
            console.log(slretenciones);

        <?php } ?>

        <?php if ($Owner || $Admin) { ?>
            if (!localStorage.getItem('sldate')) {
                $("#sldate").datetimepicker({
                    format: site.dateFormats.js_ldate,
                    fontAwesome: true,
                    language: 'sma',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0
                }).datetimepicker('update', new Date());
            }

            $(document).on('change', '#sldate', function (e) {
                localStorage.setItem('sldate', $(this).val());
            });

            if (sldate = localStorage.getItem('sldate')) {
                $('#sldate').val(sldate);
            }
        <?php } ?>

        $(document).on('change', '#slbiller', function (e) {

            localStorage.setItem('slbiller', $(this).val());

            if (localStorage.getItem('slcustomer')) {
                customer = localStorage.getItem('slcustomer');
            } else {
                customer = $('#slbiller option:selected').data('customerdefault');
            }
            warehouse = $('#slbiller option:selected').data('warehousedefault');
            seller = localStorage.getItem('slseller') ? localStorage.getItem('slseller') : $('#slbiller option:selected').data('sellerdefault');
            pricegroup = $('#slbiller option:selected').data('pricegroupdefault');

            if (customer != '' && customer !== undefined) {
                $('#slcustomer').val(customer).select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                      $.ajax({
                        type: "get", async: false,
                        url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                          callback(data[0]);
                        }
                      });
                    },
                    ajax: {
                      url: site.base_url + "customers/suggestions",
                      dataType: 'json',
                      quietMillis: 15,
                      data: function (term, page) {
                        return {
                          term: term,
                          limit: 10
                        };
                      },
                      results: function (data, page) {
                        if (data.results != null) {
                          return {results: data.results};
                        } else {
                          return {results: [{id: '', text: lang.no_match_found}]};
                        }
                      }
                    }
                });
                localStorage.setItem('slcustomer', customer);
                $('#slcustomer').trigger('change');
            }

            biller = $('#slbiller');

            $.ajax({
                url : site.base_url+"sales/getSellers",
                type : "get",
                data : {"biller_id" : biller.val()}
            }).done(function(data){
                if (data != false) {
                    $('#slseller').html(data);
                } else {
                    $('#slseller').select2('val', '');
                }
            }).fail(function(data){
                console.log(data);
            });

            $.ajax({
              url:'<?= admin_url("billers/getBillersDocumentTypes/2/") ?>'+$('#slbiller').val(),
              type:'get',
              dataType:'JSON'
            }).done(function(data){
              response = data;

              $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }

              if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                localStorage.setItem('locked_for_biller_resolution', 1);
                verify_locked_submit();
              } else {
                if (localStorage.getItem('locked_for_biller_resolution')) {
                    localStorage.removeItem('locked_for_biller_resolution');
                }
                verify_locked_submit();
              }
                $('#document_type_id').trigger('change');
            });

            $.ajax({
                url:'<?= admin_url("billers/getBillersDocumentTypes/13/") ?>'+$('#slbiller').val(),
                type:'get',
                dataType:'JSON'
            }).done(function(data){
                response = data;
                $('#payment_reference_no').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
                if (response.status == 0) {

                }
              $('#payment_reference_no').trigger('change');
            });

            setTimeout(function() {
                console.log('Bodega : '+warehouse);
                if (warehouse != '' && warehouse !== undefined) {
                    console.log('WAR '+warehouse);
                    $('#slwarehouse option').each(function(index, option){
                        option = $(this);
                        option.prop('disabled', false);
                    });

                    $('#slwarehouse').select2('val', warehouse);

                    $('#slwarehouse option').each(function(index, option){
                        option = $(this);
                        if (option.val() != warehouse) {
                            option.prop('disabled', true);
                        }
                    });
                }

                if (seller != '') {
                    $('#slseller option').each(function(index, option){
                        if ($(option).val() == seller) {
                            option.setAttribute('selected', true);
                        }
                    });

                    $('#slseller').select2();

                }
            }, 850);

        });

        $('#slcustomer').on('change', function(){
        });
        $('#slcustomerbranch').on('change', function(){
            setTimeout(function() {
                seller = $('#slcustomerbranch option:selected').data('sellerdefault');
                if (seller && is_fe_pos_sale == false) {
                    $('#slseller').select2('val', seller);
                }
            }, 1300);
        });
        if (slbiller = localStorage.getItem('slbiller')) {
            $('#slbiller').val(slbiller);
        }
        if (!localStorage.getItem('sltax2')) {
            localStorage.setItem('sltax2', <?=$Settings->default_tax_rate2;?>);
        }
        ItemnTotals();
        $('.bootbox').on('hidden.bs.modal', function (e) {
            $('#add_item').focus();
        });
        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#slcustomer').val() || !$('#slcustomerbranch').val() || !$('#document_type_id').val()) {

                    var msg = "";

                    if (!$('#slcustomer').val()) {
                        msg = "</br><?= lang('customer') ?>";
                    }

                    if (!$('#slcustomerbranch').val()) {
                        msg += "</br><?= lang('customer_branch') ?>";
                    }

                    if (!$('#document_type_id').val()) {
                        msg += "</br><?= lang('reference_no') ?>";
                    }

                    $('#add_item').val('').removeClass('ui-autocomplete-loading');


                    command: toastr.warning('<?=lang('select_above');?> : '+msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });

                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('sales/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#slwarehouse").val(),
                        customer_id: $("#slcustomer").val(),
                        biller_id: $("#slbiller").val(),
                        address_id: $("#slcustomerbranch").val(),
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    command: toastr.error('No se encontró ningún producto', 'Mensaje de validación', { onHidden : function(){   } });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    setTimeout(function() {
                        $(this).val('');
                        $('#add_item').focus();
                    }, 850);
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    command: toastr.error('No se encontró ningún producto', 'Mensaje de validación', { onHidden : function(){   } });
                    $(this).removeClass('ui-autocomplete-loading');
                    setTimeout(function() {
                        $(this).val('');
                        $('#add_item').focus();
                    }, 850);
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    if (ui.item.row.cnt_units_prices !== undefined && ui.item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && ui.item.row.promotion != 1) {
                        var item_id = ui.item.item_id;
                        $('.product_name_spumodal').text(ui.item.label);
                        var warehouse_id = $('#slwarehouse').val();
                        $('#sPUModal').appendTo("body").modal('show');
                        add_item_unit(item_id, warehouse_id);
                        $(this).val('');
                    } else {
                        var row = add_invoice_item(ui.item);
                        if (row)
                        $(this).val('');
                    }

                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $(document).on('change', '#gift_card_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "sales/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && data.customer_id !== $('#slcustomer').val()) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('gift_card_not_for_customer')?>');

                        } else {
                            $('#gc_details').html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
                            $('#gift_card_no').parent('.form-group').removeClass('has-error');
                        }
                    }
                });
            }
        });
    });
</script>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open_multipart("sales/edit/".$id, ['id' => 'edit_sale_form']); ?>
                            <input type="hidden" name="except_category_taxes" id="except_category_taxes">
                            <input type="hidden" name="tax_exempt_customer" id="tax_exempt_customer">
                            <input type="hidden" name="sale_payment_term" id="sale_payment_term">
                            <input type="hidden" name="sid" id="sid" value="<?= $id ?>">
                            <div class="row">
                                <div class="col-lg-12 resAlert"></div>
                                <?php if ($Owner || $Admin) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("date", "sldate"); ?>
                                            <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="sldate" required="required"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "slbiller"); ?>
                                            <select name="biller" class="form-control" id="slbiller" required="required">
                                                <?php foreach ($billers as $biller): ?>
                                                  <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4" style="display: none;">
                                        <div class="form-group">
                                            <?= lang("biller", "slbiller"); ?>
                                            <select name="biller" class="form-control" id="slbiller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("reference_no", "slref"); ?>
                                        <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php if ($this->Settings->aiu_management == 1): ?>
                                    <div class="col-md-4">
                                        <label>
                                            <?= lang('aiu_management') ?>
                                        </label>
                                        <br>
                                        <label>
                                            <input type="checkbox" name="aiu_management" id="aiu_management" value="1">
                                            <?= lang('enable') ?>
                                        </label>
                                        <button class="btn btn-primary btn_aiu" type="button" style="display:none;margin-left: 7%;"  onclick="$('#rtModal').modal('show');aplicar_aiu();"><i class="fa fa-pencil"></i>  <?= lang('set_aiu') ?></button>
                                    </div>
                                <?php endif ?>
                                <?php if (count($currencies) > 1): ?>
                                    <div class="col-md-4 row">
                                            <div class="col-md-6 form-group">
                                                <?= lang('currency', 'currency') ?>
                                                <select name="currency" class="form-control" id="currency">
                                                    <?php foreach ($currencies as $currency): ?>
                                                        <option value="<?= $currency->code ?>" data-rate="<?= $currency->rate ?>" <?= $currency->code == $this->Settings->default_currency ? "selected='selected'" : "" ?>><?= $currency->code ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6 form-group trm-control" style="display: none;">
                                                <?= lang('trm', 'trm') ?>
                                                <input type="number" name="trm" id="trm" class="form-control" required="true" step="0.01">
                                            </div>
                                    </div>
                                <?php else: ?>
                                    <input type="hidden" name="currency" value="<?= $this->Settings->default_currency ?>" id="currency">
                                <?php endif ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("warehouse", "slwarehouse"); ?>
                                            <?php
                                            $wh[''] = '';
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                            ?>
                                        </div>
                                    </div>
                                <?php } else {
                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'warehouse',
                                        'id' => 'slwarehouse',
                                        'value' => $this->session->userdata('warehouse_id'),
                                    );

                                    echo form_input($warehouse_input);
                                } ?>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("customer", "slcustomer"); ?>
                                        <div class="input-group">
                                            <?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="slcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="max-width:325px;"'); ?>
                                            <div class="input-group-addon no-print" style="padding: 2px 8px; border-left: 0;">
                                                <a href="#" id="toogle-customer-read-attr" class="external">
                                                    <i class="fa fa-pencil" id="addIcon" style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                            <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
                                                <a id="view-customer">
                                                    <i class="fa fa-eye" id="addIcon" style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                            <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <a href="<?= admin_url('customers/add'); ?>" id="add-customer"class="external" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-plus-circle" id="addIcon"  style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <em class="text-danger txt-error" style="display: none;"></em>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("customer_branch", "slcustomerbranch"); ?>
                                        <?php
                                        echo form_dropdown('address_id', array('0' => lang("select") . ' ' . lang("customer")), (isset($_POST['address_id']) ? $_POST['address_id'] : ''), 'id="slcustomerbranch" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>

                                <?php if ($this->Owner || $this->Admin || ($user_group_name != 'seller' && !$this->sma->keep_seller_from_user())): ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("seller", "slseller"); ?>
                                            <?php
                                            echo form_dropdown('seller_id', array('0' => lang("select") . ' ' . lang("biller")), (isset($_POST['seller']) ? $_POST['seller'] : ''), 'id="slseller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <?php if ($this->sma->keep_seller_from_user()): ?>
                                        <input type="hidden" name="seller_id"  value="<?= $this->session->userdata('seller_id') ?>">
                                    <?php else: ?>
                                        <input type="hidden" name="seller_id"  value="<?= $this->session->userdata('company_id') ?>">
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <div class="row">

                                <?php if (isset($cost_centers)): ?>
                                    <div class="col-md-4 form-group">
                                        <?= lang('cost_center', 'cost_center_id') ?>
                                        <?php
                                        $ccopts[''] = lang('select');
                                        if ($cost_centers) {
                                            foreach ($cost_centers as $cost_center) {
                                                $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                            }
                                        }
                                         ?>
                                         <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                <?php endif ?>


                            </div>
                            <div class="row">
                                <div class="col-md-12" id="sticker">
                                    <div class="form-group" style="margin-bottom:0;">

                                        <?php if ($Settings->allow_advanced_search == "1") : ?>
                                        <div class="input-group wide-tip">
                                        <?php endif ?>
                                            <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                            <?php if ($Settings->allow_advanced_search == "1") : ?>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <a href="#" data-toggle="modal" data-target="#modal_compatibilidad">
                                                        <i class="fa fa-search fa-lg" id="addIcon"></i>
                                                    </a>
                                                </div>
                                            <?php endif ?>
                                        <?php if ($Settings->allow_advanced_search == "1") : ?>
                                        </div>
                                        <?php endif ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="control-group table-group">
                                        <label class="table-label"><?= lang("order_items"); ?> *</label>

                                        <div class="controls table-controls">
                                            <table id="slTable" class="table items  table-bordered table-condensed table-hover sortable_table">
                                                <thead>
                                                <tr>
                                                    <th class="col-md-4"><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                                                    <?php
                                                    if ($Settings->product_serial || $Settings->product_variant_per_serial == 1) {
                                                        echo '<th class="col-md-2">' . lang("serial_no") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang('gross_net_unit_price') ?></th>
                                                    <?php
                                                    if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {
                                                        echo '<th class="col-md-1">' . lang("discount") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang("price_x_discount"); ?></th>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        echo '<th class="col-md-1">' . lang("product_tax") . '</th>';
                                                    }
                                                    if ($Settings->ipoconsumo == 3 || $Settings->ipoconsumo == 1) {
                                                        echo '<th class="col-md-1">' . lang("second_product_tax") . '</th>';
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        echo '<th class="col-md-1">' . lang("price_x_tax") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang("quantity"); ?></th>
                                                    <!-- <th>
                                                        <?= lang("subtotal"); ?>
                                                        (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th> -->
                                                    <th>
                                                        <?= lang("total"); ?>
                                                        (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th>
                                                    <th style="width: 30px !important; text-align: center;">
                                                        <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot></tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <?php if ($Settings->tax2) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("order_tax", "sltax2"); ?>
                                            <?php
                                            $tr[""] = "";
                                            foreach ($tax_rates as $tax) {
                                                $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('order_tax', $tr, (isset($_POST['order_tax']) ? $_POST['order_tax'] : $Settings->default_tax_rate2), 'id="sltax2" data-placeholder="' . lang("select") . ' ' . lang("order_tax") . '" class="form-control input-tip select" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("order_discount", "sldiscount"); ?>
                                            <?php echo form_input('order_discount', '', 'class="form-control input-tip" id="sldiscount"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("shipping", "slshipping"); ?>
                                        <?php echo form_input('shipping', '', 'class="form-control input-tip" id="slshipping"'); ?>

                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("document", "document") ?>
                                        <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                            data-show-preview="false" class="form-control file">
                                    </div>
                                </div>

                                <div class="col-sm-4" <?= isset($fe_pos_sale_id) && $fe_pos_sale_id ? 'style="display:none;"' : '' ?> >
                                    <div class="form-group">
                                        <?= lang("sale_status", "slsale_status"); ?>
                                        <?php $sst = array('completed' => lang('approved'), 'pending' => lang('not_approved'));
                                        echo form_dropdown('sale_status', $sst, '', 'class="form-control input-tip" required="required" id="slsale_status"'); ?>

                                    </div>
                                </div>

                                <div class="col-sm-4" <?= isset($fe_pos_sale_id) && $fe_pos_sale_id ? 'style="display:none;"' : '' ?>>
                                    <label onclick="$('#rtModal').modal('show');" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención</label>
                                    <input type="text" name="retencion" id="rete" class="form-control text-right" readonly>
                                </div>

                            </div>
                            <div class="row">
                                <div class="clearfix"></div>

                                <!-- <div id="payments" style="display: none;"> -->
                                <div id="payments">
                                    <hr>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                            <select name="payment_reference_no" class="form-control" id="payment_reference_no" required="required"></select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="payments_div">
                                        <div class="col-md-12 pay_option" data-num="1">
                                            <div class="well well-sm well_1">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <?= lang("paying_by", "paid_by_1"); ?>
                                                                <select name="paid_by[]" id="paid_by_1" data-pbnum="1" class="form-control paid_by">
                                                                    <?= $this->sma->paid_opts(); ?>
                                                                </select>
                                                                <input type="hidden" name="due_payment[]" class="due_payment_1">
                                                            </div>
                                                            <input type="hidden" name="mean_payment_code_fe[]" class="mean_payment_code_fe">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="payment">
                                                                <div class="form-group ngc_1">
                                                                    <?= lang("value", "amount_1"); ?>
                                                                    <input name="amount-paid[]" type="text" id="amount_1"
                                                                        class="pa form-control kb-pad amount only_number"/>
                                                                </div>
                                                                <div class="form-group gc_1" style="display: none;">
                                                                    <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                    <input name="gift_card_no[]" type="text" id="gift_card_no_1"
                                                                        class="pa form-control gift_card_no kb-pad"/>
                                                                    <div id="gc_details_1"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 div_slpayment_term_1" style="display:none;">
                                                            <div class="form-group">
                                                                <?= lang("payment_term", "slpayment_term_1"); ?>
                                                                <?php echo form_input('payment_term[]', '', 'class="form-control tip sale_payment_term" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="slpayment_term_1"'); ?>
                                                                <input type="hidden" name="customerpaymentterm" id="customerpaymentterm">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="pcc_1" style="display:none;">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_no[]" type="text" id="pcc_no_1"
                                                                        class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_holder[]" type="text" id="pcc_holder_1"
                                                                        class="form-control"
                                                                        placeholder="<?= lang('cc_holder') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <select name="pcc_type[]" id="pcc_type_1"
                                                                            class="form-control pcc_type"
                                                                            placeholder="<?= lang('card_type') ?>">
                                                                        <option value="Visa"><?= lang("Visa"); ?></option>
                                                                        <option
                                                                            value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                        <option value="Amex"><?= lang("Amex"); ?></option>
                                                                        <option value="Discover"><?= lang("Discover"); ?></option>
                                                                    </select>
                                                                    <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_month[]" type="text" id="pcc_month_1"
                                                                        class="form-control" placeholder="<?= lang('month') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <input name="pcc_year[]" type="text" id="pcc_year_1"
                                                                        class="form-control" placeholder="<?= lang('year') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <input name="pcc_ccv[]" type="text" id="pcc_cvv2_1"
                                                                        class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pcheque_1" style="display:none;">
                                                        <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                            <input name="cheque_no[]" type="text" id="cheque_no_1"
                                                                class="form-control cheque_no"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang('payment_note', 'payment_note_1'); ?>
                                                        <textarea name="payment_note[]" id="payment_note_1"
                                                                class="pa form-control kb-text payment_note"></textarea>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-bottom: 4%">
                                        <button type="button" class="btn btn-primary" id="add_more_payments"><?= lang('add_more_payments') ?></button>
                                    </div>
                                </div>

                                <input type="hidden" name="total_items" value="" id="total_items" required="required"/>

                                <div class="row" id="bt">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang("sale_note", "slnote"); ?>
                                                <a href="<?= admin_url('sales/add_invoice_note') ?>" class="btn btn-primary" style="padding: 3px 8px; margin-left: 2px;" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span></a>
                                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="slnote" style="margin-top: 10px; height: 100px;"'); ?>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang("staff_note", "slinnote"); ?>
                                                <?php echo form_textarea('staff_note', (isset($_POST['staff_note']) ? $_POST['staff_note'] : ""), 'class="form-control" id="slinnote" style="margin-top: 10px; height: 100px;"'); ?>

                                            </div>
                                        </div>


                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <em class="text-danger margin_alert"><?= lang('msg_error_invalid_margin') ?></em>
                                    <div class="form-group">

                                        <button type="button" class="btn btn-primary" id="add_sale" style="padding: 6px 15px; margin:15px 0;"><?= lang('submit') ?></button>
                                        <?php if($id) { ?>
                                            <button type="button" class="btn btn-danger" id="cancel"><?= lang('cancel') ?></div>
                                        <?php } ?>
                                </div>
                            </div>
                            <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                    <tr class="warning">
                                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                        <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                        <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                                        <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                                        <?php }?>
                                        <?php if ($Settings->tax2) { ?>
                                            <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                                        <?php } ?>
                                        <?php if ($Settings->ipoconsumo == 3 || $Settings->ipoconsumo == 1): ?>
                                            <td><?= lang('second_product_tax') ?> <span class="totals_val pull-right" id="ipoconsumo_total">0.00</span></td>
                                        <?php endif ?>
                                        <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                                        <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                    </tr>
                                </table>
                            </div>


<!-- retencion -->

 <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                                            <h4 class="modal-title" id="rtModalLabel"><?= lang('apply_order_retention'); ?></h4>
                                            <h4 class="modal-title" id="rtModalLabel2" style="display: none;"><?= lang('aiu_management'); ?></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="div_aiu" style="display: none;">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>¿AIU Afecta total?</label>
                                                        <br>
                                                        <label>
                                                            <input type="radio" name="afecta_aiu" class="afecta_aiu" value="1">
                                                            Si afecta
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="afecta_aiu" class="afecta_aiu" value="0" checked>
                                                            No afecta
                                                        </label>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label class="control-label"><?= lang('aiu_tax') ?></label>
                                                        <select name="aiu_tax" id="aiu_tax" class="form-control">
                                                            <?php foreach ($tax_rates as $tax) : ?>
                                                                <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label class="control-label"><?= lang('aiu_tax_base') ?></label>
                                                        <select name="aiu_tax_base_apply_to" id="aiu_tax_base_apply_to" class="form-control">
                                                            <option value="1">Subtotal</option>
                                                            <option value="2" class="option_aiu_base">Base AIU </option>
                                                            <option value="3" class="option_utilidad_base">Utilidad </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label class="control-label"><?= lang('aiu_tax_total') ?></label>
                                                        <input type="text" name="aiu_tax_total" id="aiu_tax_total" class="form-control" readonly>
                                                        <input type="hidden" name="aiu_tax_base" id="aiu_tax_base" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label>Base AIU</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>% Admon</label>
                                                        <input type="text" class="form-control only_number" name="aiu_admin_percentage" id="aiu_admin_percentage" value="<?= $this->sma->formatDecimals($this->Settings->aiu_perc_admin) ?>">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Administración</label>
                                                        <input type="text" class="form-control only_number" name="aiu_admin_total" id="aiu_admin_total" readonly>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>% Imprevistos</label>
                                                        <input type="text" class="form-control only_number" name="aiu_imprev_percentage" id="aiu_imprev_percentage" value="<?= $this->sma->formatDecimals($this->Settings->aiu_perc_imprev) ?>">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Imprevistos</label>
                                                        <input type="text" class="form-control only_number" name="aiu_imprev_total" id="aiu_imprev_total" readonly>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>% Utilidad</label>
                                                        <input type="text" class="form-control only_number" name="aiu_util_percentage" id="aiu_util_percentage" value="<?= $this->sma->formatDecimals($this->Settings->aiu_perc_utilidad) ?>">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Utilidad</label>
                                                        <input type="text" class="form-control only_number" name="aiu_util_total" id="aiu_util_total"  readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-8 text-right">
                                                        <b>Total AIU:</b>
                                                    </div>
                                                    <div class="col-sm-4 text-right">
                                                        <b class="total_aiu_calculated"> 0 </b>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>Retención</label>
                                                </div>
                                                <div class="retention_tr_div">
                                                    <div class="col-sm-5">
                                                        <label>Opción</label>
                                                    </div>
                                                </div>
                                                <div class="retention_aiu_div" style="display:none;">
                                                    <div class="col-sm-2">
                                                        <label>Base</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Opción</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Porcentaje</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Valor</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                                                    </label>
                                                </div>
                                                <div class="col-sm-2 rete_option_apply" style="display:none;">
                                                    <select id="rete_fuente_base_apply" class="rete_fuente_base_apply form_control" style="width:100%;">
                                                        <option value="1">Original</option>
                                                        <option value="2" class="option_aiu_base">Base AIU </option>
                                                        <option value="3" class="option_utilidad_base">Utilidad </option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5 rete_option">
                                                    <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                                    <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                                    <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                                                    </label>
                                                </div>
                                                <div class="col-sm-2 rete_option_apply" style="display:none;">
                                                    <select id="rete_iva_base_apply" class="rete_iva_base_apply form_control" style="width:100%;">
                                                        <option value="1">Original</option>
                                                        <option value="2" class="option_aiu_base">Base AIU </option>
                                                        <option value="3" class="option_utilidad_base">Utilidad </option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5 rete_option">
                                                    <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                                    <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                                    <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                                                    </label>
                                                </div>
                                                <div class="col-sm-2 rete_option_apply" style="display:none;">
                                                    <select id="rete_ica_base_apply" class="rete_ica_base_apply form_control" style="width:100%;">
                                                        <option value="1">Original</option>
                                                        <option value="2" class="option_aiu_base">Base AIU </option>
                                                        <option value="3" class="option_utilidad_base">Utilidad </option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5 rete_option">
                                                    <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                                    <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                                    <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-2 rete_option_apply" style="display:none;">
                                                    <select id="rete_other_base_apply" class="rete_other_base_apply form_control" style="width:100%;">
                                                        <option value="1">Original</option>
                                                        <option value="2" class="option_aiu_base">Base AIU </option>
                                                        <option value="3" class="option_utilidad_base">Utilidad </option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5 rete_option">
                                                    <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                                    <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                                    <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_bomberil" class="skip" id="rete_bomberil" disabled> <?= lang('rete_bomberil') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-5">
                                                    <select class="form-control" name="rete_bomberil_option" id="rete_bomberil_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_bomberil_account" id="rete_bomberil_account">
                                                    <input type="hidden" name="rete_bomberil_base" id="rete_bomberil_base">
                                                    <input type="hidden" name="rete_bomberil_id" id="rete_bomberil_id">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_bomberil_tax" id="rete_bomberil_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_bomberil_valor" id="rete_bomberil_valor" class="form-control text-right" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_autoaviso" class="skip" id="rete_autoaviso" disabled> <?= lang('rete_autoaviso') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-5">
                                                    <select class="form-control" name="rete_autoaviso_option" id="rete_autoaviso_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_autoaviso_account" id="rete_autoaviso_account">
                                                    <input type="hidden" name="rete_autoaviso_base" id="rete_autoaviso_base">
                                                    <input type="hidden" name="rete_autoaviso_id" id="rete_autoaviso_id">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_autoaviso_tax" id="rete_autoaviso_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_autoaviso_valor" id="rete_autoaviso_valor" class="form-control text-right" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 text-right aiu_rete_total">
                                                    <label>Total Retenciones : </label>
                                                </div>
                                                <div class="col-md-4 text-right">
                                                    <label id="total_rete_amount"> 0.00 </label>
                                                </div>
                                                <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                                            </div>


                                            <!-- <div class="form-group">
                                                <?= lang("order_tax", "order_tax_input"); ?>
                                                <?php
                                                $tr[""] = "";
                                                foreach ($tax_rates as $tax) {
                                                    $tr[$tax->id] = $tax->name;
                                                }
                                                echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                                                ?>
                                            </div> -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
                                            <button type="button" id="cancelOrderRete" class="btn btn-danger" data-dismiss="modal"><?= lang('reset') ?></button>
                                            <button type="button" id="updateOrderRete" class="btn btn-primary"><?= lang('update') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<!-- retencion -->

                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="pname" class="col-sm-4 control-label"><?= lang('product_name') ?></label>
                        <div class="col-sm-8">
                            <?= form_input('pname', '', 'id="pname" class="form-control"') ?>
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <select name="ptax" id="ptax" class="form-control">
                                    <?php foreach ($tax_rates as $tax): ?>
                                        <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="control-label"><?= lang('second_product_tax') ?></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" name="ptax2" id="ptax2" class="form-control" readonly>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pserial">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                        <div class="col-sm-8">
                            <div id="punits-div"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "style='display:none;'" : "" ?>>
                        <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                        </div>
                    </div>
                    <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "" : "style='display:none;'" ?>>
                        <label for="pproduct_unit_price" class="col-sm-4 control-label"><?= lang('sale_form_edit_product_unit_price') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pproduct_unit_price" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                        </div>
                    </div>
                    <?php if ($Settings->ipoconsumo == 3 || $Settings->ipoconsumo == 1): ?>
                        <div class="form-group">
                            <label for="pprice_ipoconsumo" class="col-sm-4 control-label"><?= lang('unit_price_ipoconsumo') ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pprice_ipoconsumo"><br>
                                <em><?= lang('unit_price_ipoconsumo_detail') ?></em>
                            </div>
                        </div>
                    <?php endif ?>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="net_price"></span></th>
                            <th style="width:25%;"><?= lang('product_taxes'); ?></th>
                            <th style="width:25%;"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_price" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                    <div class="panel panel-default">
                        <div class="panel-heading"><?= lang('calculate_unit_price'); ?></div>
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="pcost" class="col-sm-4 control-label"><?= lang('subtotal_before_tax') ?></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="psubtotal">
                                        <div class="input-group-addon" style="padding: 2px 8px;">
                                            <a href="#" id="calculate_unit_price" class="tip" title="<?= lang('calculate_unit_price'); ?>">
                                                <i class="fa fa-calculator"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>

                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="mdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?= lang('unit_price') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="mnet_price"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="gcModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="myModalLabel"><?= lang('sell_gift_card'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= lang('enter_info'); ?></p>

                <div class="alert alert-danger gcerror-con" style="display: none;">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span id="gcerror"></span>
                </div>
                <div class="form-group">
                    <?= lang("card_no", "gccard_no"); ?> *
                    <div class="input-group">
                        <?php echo form_input('gccard_no', '', 'class="form-control" id="gccard_no"'); ?>
                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;"><a href="#"
                                                                                                           id="genNo"><i
                                    class="fa fa-cogs"></i></a></div>
                    </div>
                </div>
                <input type="hidden" name="gcname" value="<?= lang('gift_card') ?>" id="gcname"/>

                <div class="form-group">
                    <?= lang("value", "gcvalue"); ?> *
                    <?php echo form_input('gcvalue', '', 'class="form-control" id="gcvalue"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("price", "gcprice"); ?> *
                    <?php echo form_input('gcprice', '', 'class="form-control" id="gcprice"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("customer", "gccustomer"); ?>
                    <?php echo form_input('gccustomer', '', 'class="form-control" id="gccustomer"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("expiry_date", "gcexpiry"); ?>
                    <?php echo form_input('gcexpiry', $this->sma->hrsd(date("Y-m-d", strtotime("+2 year"))), 'class="form-control date" id="gcexpiry"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="addGiftCard" class="btn btn-primary"><?= lang('sell_gift_card') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_compatibilidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
            <span class="sr-only"><?=lang('close');?></span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Busqueda avanzada</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <form id="formulario_compatibilidad_producto">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="producto">Código / Nombre</label>
                        <input class="form-control" type="text" name="producto" id="producto" placeholder="Código o nombre a buscar">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <input class="form-control" type="text" name="descripcion" id="descripcion">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="marca">Marca</label>
                        <select class="form-control" name="marca" id="marca">
                            <option value="">Seleccione...</option>
                            <?php foreach ($brands as $brand): ?>
                                <option value="<?= $brand->id ?>"><?= $brand->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-primary" type="button" id="buscar_productos" style="margin-top: 28px;"><i class="fa fa-search"></i> Buscar</button>
                </div>
            </form>
        </div>
        <hr>
        <div class="row">
            <table class="table table-condensed table-bordered table-hover" id="tabla_productos_compatibilidad">
                <thead>
                    <th>Código</th>
                    <th>Producto</th>
                    <th>Descripción</th>
                    <th>Marca</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

<div class="modal fade in" id="sPUModal" tabindex="-1" role="dialog" aria-labelledby="sPUModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_spumodal"></h2>
                <h3><?= lang('unit_prices') ?></h3>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 10%;"></th>
                            <th style="width: 35%;"><?= lang('unit') ?></th>
                            <th style="width: 35%;"><?= lang('price') ?></th>
                            <th style="width: 20%;"><?= lang('quantity') ?></th>
                        </tr>
                    </thead>
                </table>
                <table class="table" id="unit_prices_table" style="width: 100%;">
                    <tbody>

                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <div class="col-sm-3">
                    <?= lang('unit_quantity', 'unit_quantity') ?>
                    <input type="text" name="unit_quantity" id="unit_quantity" class="form-control">
                </div>
                <div class="col-sm-9">
                    <button class="btn btn-success send_item_unit_select" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pricesChanged" tabindex="-1" role="dialog" aria-labelledby="pricesChangedLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3><?= lang('product_prices_changed') ?></h3>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 35%;"><?= lang('product_name') ?></th>
                            <th style="width: 20%;"><?= lang('previous_product_price') ?></th>
                            <th style="width: 35%;"><?= lang('actual_product_price') ?></th>
                        </tr>
                    </thead>
                    <tbody id="prices_changed_table">

                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12">
                    <button class="btn btn-danger" id="keep_prices" type="button"><?= lang('no_keep_prices') ?></button>
                    <button class="btn btn-success" id="update_prices" type="button"><?= lang('yes_update_prices') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_serialModal"></h2>
                <h3><?= lang('ticket_data') ?></h3>
            </div>
            <div class="modal-body">
                <form id="serialModal_form">
                    <input type="hidden" id="serialModal_product_id">
                    <input type="hidden" id="serialModal_option_id">
                    <div class="col-sm-12 form-group">
                        <?= lang('serial_no', 'serial') ?>
                        <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= lang('meters', 'meters') ?>
                        <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {

        $(document).on('change', '#slpayment_status', function () {
            if ($(this).val() == 'pending' || $(this).val() == 'due') {
                $('.mean_payment_code_fe').val('');
            }
        });

        $('#gccustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#genNo').click(function () {
            var no = generateCardNo();
            $(this).parent().parent('.input-group').children('input').val(no);
            return false;
        });



        $('#slbiller').trigger('change');
        // setTimeout(function() {
        //     $('#slcustomer').trigger('change');
        // }, 1000);
    });

    $('#document_type_id').on('change', function() {
        if ($(this).val() != '') {
            validar_resolucion_biller();
            validate_assigned_technology_provider();
            validate_fe_customer_data();
        }
    });

    function validar_resolucion_biller()
    {
          $('.alertResolucion').remove();
          //VALIDAR RESOLUCIÓN
          $.ajax({
            type: "get", async: false,
            url: site.base_url + "sales/validate_biller_resolution/"+$('#slbiller').val()+"/"+$('#document_type_id').val(),
            success: function (data) {
              if (data != false) {

                response = JSON.parse(data);

                mensaje = response.mensaje;
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button>"+mensaje+"</div></div>").css('display', '');

                if (response.disable == true) {
                    localStorage.setItem('locked_for_biller_resolution', 1);
                    verify_locked_submit();
                } else {
                    if (localStorage.getItem('locked_for_biller_resolution')) {
                        localStorage.removeItem('locked_for_biller_resolution');
                    }
                    verify_locked_submit();
                }

              } else {
                    if (localStorage.getItem('locked_for_biller_resolution')) {
                        localStorage.removeItem('locked_for_biller_resolution');
                    }
                    verify_locked_submit();
                  $('.resAlert').css('display', 'none');
              }
            }
          });
          //VALIDAR RESOLUCIÓN
    }

    function validate_assigned_technology_provider()
    {
        $.ajax({
            url: '<?= admin_url('sales/validate_assigned_technology_provider') ?>',
            type: 'POST',
            dataType: 'JSON',
            data: {
                document_type_id: $('#document_type_id').val(),
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>'
            },
        })
        .done(function(response) {
            if (response == false) {
                Command: toastr.error('<?= lang("unassigned_technology_provider"); ?>', '<?= lang("error"); ?>');
                localStorage.setItem('locked_for_provider', 1);
                verify_locked_submit();
            } else {
                if (localStorage.getItem('locked_for_provider')) {
                    localStorage.removeItem('locked_for_provider');
                }
                verify_locked_submit();
            }
        })
        .fail(function(response) {
            console.log(response.responseText);
        });
    }
</script>

<script type="text/javascript">
        $(document).ready(function(){
            $(document).on('click', '#buscar_productos', buscar_productos);
            $(document).on('click', '.agregar_producto', function() { agregar_producto($(this)); });
        });

        function buscar_productos()
        {
            var producto = $('#producto').val();
            var descripcion = $('#descripcion').val();
            var marca = $('#marca').val();
            var warehouse = $('#slwarehouse').val();

            if (producto == "")
            {
                command: toastr.error('El campo es obligatorio', 'Mensaje de validación', { onHidden : function(){ $('#producto').focus(); } });
                return false;
            }

            $('#tabla_productos_compatibilidad').DataTable({
                aaSorting: [[1, "asc"]],
                aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                iDisplayLength: <?= $Settings->rows_per_page ?>,
                bDestroy: true,
                bProcessing: true,
                bServerSide: true,
                sAjaxSource: '<?= admin_url("pos/buscar_productos/") ?>',
                fnServerData: function (sSource, aoData, fnCallback)
                {
                    aoData.push({ name: "producto", value: producto });
                    aoData.push({ name: "descripcion", value: descripcion });
                    aoData.push({ name: "marca", value: marca });
                    aoData.push({ name: "warehouse", value: warehouse });
                    aoData.push({ name: "<?= $this->security->get_csrf_token_name() ?>", value: "<?= $this->security->get_csrf_hash() ?>" });

                    $.ajax({dataType: 'json', type: 'POST', 'url': sSource, 'data': aoData, success: fnCallback/*, error: function(data) {console.log(data.responseText)}*/ });
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    nRow.setAttribute('data-product_id', aData[6]);
                    nRow.className = "agregar_producto";
                    return nRow;
                },
                aoColumns: [
                    null,
                    null,
                    null,
                    null,
                    {"mRender" : formatQuantity},
                    {"mRender" : formatQuantity},
                ],
            });
        }

        function agregar_producto(elemento)
        {
            var product_id = elemento.data('product_id');
            var warehouse_id = $('#slwarehouse').val();
            var customer_id = $('#slcustomer').val();
            var biller_id = $('#slbiller').val();
            var address_id = $('#slcustomerbranch').val();

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '<?= admin_url("sales/iusuggestions") ?>',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'product_id': product_id,
                    'warehouse_id': warehouse_id,
                    'customer_id': customer_id,
                    'biller_id': biller_id,
                    'address_id': address_id,
                    'unit_quantity': 1
                }
            }).done(function(data) {
                add_invoice_item(data);
                $('#modal_compatibilidad').modal('hide');
            }).error(function (data) {
                console.log(data.responseText);
            })
        }

        function add_item_unit(item_id, warehouse_id){
            // $('#sPModal').modal('hide');

            var ooTable = $('#unit_prices_table').dataTable({
                aaSorting: [[1, "asc"]],
                aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                iDisplayLength: <?= $Settings->rows_per_page ?>,
                bProcessing: true, 'bServerSide': true,
                sAjaxSource: site.base_url+"pos/itemSelectUnit/"+item_id+"/"+warehouse_id+"/"+$('#slcustomer').val(),
                "bDestroy": true,
                fnServerData: function (sSource, aoData, fnCallback)
                {
                aoData.push({
                  name: "<?= $this->security->get_csrf_token_name() ?>",
                  value: "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback/*, error: function(data) {console.log(data.responseText)}*/ });
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var oSettings = ooTable.fnSettings();
                    //$("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
                    // nRow.id = aData[0];
                    nRow.setAttribute('data-itemunitid', aData[4]);
                    nRow.setAttribute('data-productid', aData[5]);
                    nRow.className = "add_item_unit";
                    //if(aData[7] > aData[9]){ nRow.className = "product_link warning"; } else { nRow.className = "product_link"; }
                    return nRow;
                },
                aoColumns: [
                {bSortable: false, "mRender": radio_2},
                {bSortable: false},
                {bSortable: false},
                {bSortable: false, className : 'text-right'},
                {bSortable: false,  bVisible: false},
                {bSortable: false,  bVisible: false},
                ],
                initComplete: function(settings, json)
                {
                console.log(json);
                $('#sPUModal').modal('show');
                }
                }).dtFilter([
                {column_number: 0, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", mdata: []},
                {column_number: 1, filter_default_label: "[<?=lang('price');?>]", filter_type: "text", data: []},
                {column_number: 2, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
                ], "footer");

                $('#unit_prices_table thead').remove();
        }

        $(document).on('click', '.add_item_unit', function() {

            var product_id = $(this).data('productid');
            var unit_price_id = $(this).data('itemunitid');

            var unit_data = {
                                'product_id' : product_id,
                                'unit_price_id' : unit_price_id,
                            };

            localStorage.setItem('unit_data', JSON.stringify(unit_data));

            $('#unit_quantity').val(1).select();

        });

        $(document).on('keyup', '#unit_quantity', function(e){
            if (e.keyCode == 13) {
                if (unit_data = JSON.parse(localStorage.getItem('unit_data'))) {
                    localStorage.removeItem('unit_data');
                    var warehouse_id = $('#slwarehouse').val();
                    var unit_quantity = $(this).val();
                    $.ajax({
                        url:site.base_url+"sales/iusuggestions",
                        type:"get",
                        data:{
                                'product_id' : unit_data.product_id,
                                'unit_price_id' : unit_data.unit_price_id,
                                'warehouse_id' : warehouse_id,
                                'unit_quantity' : unit_quantity,
                                'biller_id' : $('#slbiller').val(),
                                'address_id' : $('#slcustomerbranch').val(),
                                'customer_id' : $('#slcustomer').val()
                            }
                    }).done(function(data){
                        add_invoice_item(data);
                        $('#sPUModal').modal('hide');
                    });
                } else {
                    $('#unit_quantity').val(1).select();
                }
            }
        });

        $('#sPUModal').on('shown.bs.modal', function () {
            // $('#unit_prices_table_filter .input-xs').focus();
            $('#unit_prices_table_length').remove();
            $('#unit_prices_table_filter').remove();
            $('#unit_prices_table_info').remove();
            $('#unit_prices_table_paginate').remove();
            $('#unit_quantity').val(1);
            $('.select_auto_2:first').iCheck('check').focus();
        });

        $(document).on('hide.bs.modal', '#sPUModal', function (e) {
          e.stopPropagation() // stops modal from being shown
        });

        $(document).on('ifClicked', '.select_auto_2', function(e){
            var index = $('.select_auto_2').index($(this));
            $('.add_item_unit').eq(index).trigger('click');
        });

        $(document).on('keyup', '.select_auto_2', function(e){
            if (e.keyCode == 13) {
                $(this).closest('.add_item_unit').trigger('click');
            }
        });


        $(document).on('click', '.send_item_unit_select', function(){

            var itemunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('itemunitid');
            var productid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productid');

            var unit_data = {
                                'product_id' : productid,
                                'unit_price_id' : itemunitid,
                            };
            var warehouse_id = $('#slwarehouse').val();
            var unit_quantity = $('#unit_quantity').val();
            $.ajax({
                url:site.base_url+"sales/iusuggestions",
                type:"get",
                data:{
                        'product_id' : unit_data.product_id,
                        'unit_price_id' : unit_data.unit_price_id,
                        'warehouse_id' : warehouse_id,
                        'unit_quantity' : unit_quantity,
                        'biller_id' : $('#slbiller').val(),
                        'address_id' : $('#slcustomerbranch').val(),
                        'customer_id' : $('#slcustomer').val()
                    }
            }).done(function(data){
                add_invoice_item(data);
                $('#sPUModal').modal('hide');
            });

        });
</script>