<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">

    $(document).ready(function () {

        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('qadate')) {
            $("#qadate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#qadate', function (e) {
            localStorage.setItem('qadate', $(this).val());
        });
        if (qadate = localStorage.getItem('qadate')) {
            $('#qadate').val(qadate);
        }
        <?php } ?>

        $(document).on('change', '#qabiller', function (e) {
            $.ajax({
                url:'<?= admin_url("billers/getBillersDocumentTypes/11/") ?>'+$('#qabiller').val(),
                type:'get',
                dataType:'JSON'
            }).done(function(data){
                response = data;
                $('#document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                    });
                }
                if (response.status == 0) {
                    $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                    localStorage.setItem('locked_for_biller_resolution', 1);
                }
                $('#document_type_id').trigger('change');
            });
            localStorage.setItem('qabiller', $('#qabiller').val());
            $('#qawarehouse').select2('val', $('#qabiller option:selected').data('warehousedefault')).trigger('change');
        });
        if (qabiller = localStorage.getItem('qabiller')) {
            $('#qabiller').select2("val", qabiller).trigger('change');
        } else {
            $('#qabiller').trigger('change');
        }

        $('#type_id').on('change', function(){
            sectionCustomized($(this).val());
        }).trigger('change');

        $('#qawarehouse').on('change', () => selectedWarehouse($('#qawarehouse').val())).trigger('change');

        $('#fichero').on('click', function(event){
            if ($('#type_id').val() == '2') {
                _v = '';
                _v += 'warehouse='+$('#qawarehouse').val()+'&';
                if ($('#brand_id').val() != '') {
                    _v += 'brand='+$('#brand_id').val()+'&';
                }
                if ($('#category_id').val() != '') {
                    _v += 'category='+$('#category_id').val()+'&';
                }
                if ($('#products_with_negative_quantity').iCheck('update')[0].checked) {
                    _v += 'products_negatives=1';
                }
                $('#fichero').attr('href', site.url+'admin/products/get_templade_adjustements?'+_v); 
            }else{
                $('#fichero').attr('href', site.url+'assets/csv/sample_adjustment.xlsx'); 
            }
        });
    });

    function sectionCustomized(type){
        if (type == 1) {
            $('.sectionCustomized').attr('style', "display:none");
        }else{
            document.getElementById('qawarehouse').disabled=true;
            $('.sectionCustomized').attr('style', "display:block");
        }
    }

    function selectedWarehouse(warehouse){
        $('#qawarehouse_hidden').val(warehouse);
    }
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_adjustment_by_csv'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("products/add_adjustment_by_csv", $attrib);
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("date", "qadate"); ?>
                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="qadate" required="required"'); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                            $bl[""] = "";
                            $bldata = [];
                            foreach ($billers as $biller) {
                                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                $bldata[$biller->id] = $biller;
                            }
                        ?>
                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang("biller", "qabiller"); ?>
                                    <select name="biller" class="form-control" id="qabiller" required="required">
                                        <?php foreach ($billers as $biller): ?>
                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang("biller", "qabiller"); ?>
                                    <select name="biller" class="form-control" id="qabiller">
                                        <?php if (isset($bldata[$this->session->userdata('biller_id')])):

                                            $biller = $bldata[$this->session->userdata('biller_id')];

                                            ?>
                                            <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang("reference_no", "document_type_id"); ?>
                                <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                            </div>
                        </div>

                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("warehouse", "qawarehouse"); ?>
                                    <?php
                                    $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="qawarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;"');
                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'warehouse_hidden',
                                        'id' => 'qawarehouse_hidden',
                                        'value' => $this->session->userdata('warehouse_id'),
                                        );
                                    echo form_input($warehouse_input);
                                    ?>
                                </div>
                            </div>
                            <?php } else {
                                $warehouse_input = array(
                                    'type' => 'hidden',
                                    'name' => 'warehouse',
                                    'id' => 'qawarehouse',
                                    'value' => $this->session->userdata('warehouse_id'),
                                    );

                                echo form_input($warehouse_input);
                            } ?>


                        <div class="col-md-4 form-group">
                            <?= lang('third', 'companies_id') ?>
                            <?php 
                            $cOptions[''] = lang('select_third');
                            foreach ($companies as $row => $company) {
                                $cOptions[$company->id] = $company->name;
                            }

                             ?>
                            <?= form_dropdown('companies_id', $cOptions, '',' id="companies_id" class="form-control select" required style="width:100%;"'); ?>
                        </div>

                        <div class="col-md-4 form-group">
                            <label for="type"><?= lang('type') ?></label>
                            <?php 
                                $tOptions = [];
                                $tOptions['1'] = "Normal";
                                $tOptions['2'] = "Ajustar a 0";
                             ?>
                            <?= form_dropdown('type_id', $tOptions, '',' id="type_id" class="form-control select" required style="width:100%;"'); ?>
                        </div>
                        
                        <div class="col-md-4 form-group sectionCustomized">
                            <label for="category"><?= lang('category') ?></label>
                            <?php 
                                $caOptions[''] = lang('select');
                                foreach ($categories as $row => $category) {
                                    $caOptions[$category->id] = $category->name;
                                }
                             ?>
                            <?= form_dropdown('category_id', $caOptions, '',' id="category_id" class="form-control select" required style="width:100%;"'); ?>
                        </div>

                        <div class="col-md-4 form-group sectionCustomized">
                            <label for="brand"><?= lang('brand') ?></label>
                            <?php 
                                $bOptions[''] = lang('select');
                                foreach ($brands as $row => $brand) {
                                    $bOptions[$brand->id] = $brand->name;
                                }
                             ?>
                            <?= form_dropdown('brand_id', $bOptions, '',' id="brand_id" class="form-control select" required style="width:100%;"'); ?>
                        </div>


                        <div class="col-md-4 form-group sectionCustomized">
                            <?= lang('products_with_negative_quantity', 'products_with_negative_quantity'); ?>
							<div class="controls" id="products_with_negative_quantity">
								<?php
								    echo form_checkbox('products_with_negative_quantity', 'products_with_negative_quantity', (isset($_POST['products_with_negative_quantity']) ? 'checked' : ''));
								?>
							</div>
                        </div>

                        <div class="clearfix"></div>



                        <div class="col-md-12">

                            <div class="well well-small">
                                <a href="<?php echo base_url(); ?>assets/csv/sample_adjustment.xlsx"
                                 class="btn btn-primary pull-right" id='fichero' ><i
                                 class="fa fa-download"></i> <?= lang("download_sample_file") ?></a>
                                 <span class="text-warning"><?= lang("csv1"); ?></span><br/><?= lang("csv2"); ?> <span
                                 class="text-info">(<?= lang("product_code") . ', ' . lang("quantity") . ', ' . lang("variant"); ?>
                                 )</span> <?= lang("csv3"); ?>
                                <br><strong><?= lang('quantity_colum_tip'); ?></strong>
                             </div>

                             <div class="form-group">
                                <label for="xls_file"><?= lang("upload_file"); ?></label>
                                <input type="file" data-browse-label="<?= lang('browse'); ?>" name="xls_file" class="form-control file" data-show-upload="false" data-show-preview="false" id="xls_file" required="required"/>
                            </div>

                        </div>
                        <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= lang("note", "qanote"); ?>
                                    <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qanote" style="margin-top: 10px; height: 100px;"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        <div class="col-md-12">
                            <div
                                class="fprom-group"><?php echo form_submit('add_adjustment', lang("submit"), 'id="add_adjustment" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div>
    </div>
</div>
