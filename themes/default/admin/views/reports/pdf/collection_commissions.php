<?php

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

//$this->SetTextColor( 255, 0, 0); color rojo
//$this->SetTextColor( 0, 0, 0); color negro

// var_dump($POST);

class PDF extends FPDF
  {

    function Header()
    {

        $this->SetTitle($this->sma->utf8Decode('Informe de Liquidación por Recaudo de Cartera'));

        $this->SetFont('Arial','B', $this->fontsize_title+2);
        $this->cell(265, 5, $this->sma->utf8Decode('Informe de Liquidación por Recaudo de Cartera'), '', 1, 'C');
        $this->cell(92.24, 5, $this->sma->utf8Decode(''), '', 0, 'C');
        $this->SetFont('Arial','B', $this->fontsize_body);
        $this->cell(15.12, 5, $this->sma->utf8Decode('Desde : '), '', 0, 'C');
        $this->SetFont('Arial','', $this->fontsize_body);
        $this->cell(25.12, 5, $this->sma->utf8Decode($this->start_date ? $this->start_date : 'N/A'), '', 0, 'L');
        $this->SetFont('Arial','B', $this->fontsize_body);
        $this->cell(15.12, 5, $this->sma->utf8Decode('Hasta : '), '', 0, 'C');
        $this->SetFont('Arial','', $this->fontsize_body);
        $this->cell(25.12, 5, $this->sma->utf8Decode($this->end_date ? $this->end_date : 'N/A'), '', 0, 'L');
        $this->SetFont('Arial','B', $this->fontsize_body);
        $this->cell(25.12, 5, $this->sma->utf8Decode('Vendedor : '), '', 0, 'C');
        $this->SetFont('Arial','', $this->fontsize_body);
        $this->cell(67.12, 5, $this->sma->utf8Decode(isset($this->seller->company) ? $this->seller->company : $this->seller), '', 1, 'L');
        $this->cell(265, 5, $this->sma->utf8Decode(''), 'T', 0, 'C');

        // $this->SetFont('Arial','B', $this->fontsize_body);
        // $this->cell(50.5, 5, $this->sma->utf8Decode('Sucursal : '), '', 0, 'C');
        // $this->SetFont('Arial','', $this->fontsize_body);
        // $this->cell(50.5, 5, $this->sma->utf8Decode($this->biller ? $this->biller->company : 'Todas las sucursales'), '', 0, 'L');

        $this->ln(5);

        $this->SetFont('Arial','B', $this->fontsize_body);
        $this->cell(24.09, 5, $this->sma->utf8Decode('Número factura'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Valor sin Dcto'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Valor Dcto'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Valor IVA'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Valor Factura'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Valor Recaudo'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Notas Cr'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Otros Dctos'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Base liquidación'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('% Liquidación'), '', 0, 'C');
        $this->cell(24.09, 5, $this->sma->utf8Decode('Valor liquidado'), '', 1, 'C');

    }

    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina '.$this->PageNo()." de ".'{nb}'),'',1,'C');

    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

}


$pdf = new PDF('L', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);

$pdf->fontsize_title = 9;
$pdf->fontsize_body = 7;
$pdf->settings = $settings;
$pdf->biller = $biller;
$pdf->seller = $seller;
$pdf->start_date = $start_date;
$pdf->end_date = $end_date;

$pdf->Logo = base_url('assets/uploads/logos/'.$Settings->logo);
$pdf->print_logo = $print_logo;

$pdf->SetFont('Arial','', $pdf->fontsize_body);
// $pdf->Logo = base_url('assets/uploads/logos/'.$Settings->logo);
// $this->sma->print_arrays($sales);
$total_base = 0;
$total_comm = 0;
// $this->sma->print_arrays($sales);
$seller = 0;
$seller_liq = 0;
$customer = 0;
$customer_liq = 0;
foreach ($sales as $sale) {
    if ($seller == 0 || ($seller != $sale->seller_id)) {
        if ($seller == 0) {
            $pdf->seller = $sale->name_seller;
            $pdf->AddPage();
        }
        if ($customer_liq > 0) {
            $pdf->SetFont('Arial','B', $pdf->fontsize_body);
            $pdf->cell(225, 5, $this->sma->utf8Decode('Liquidado cliente'), 'B', 0, 'R');
            $pdf->cell(40, 5, $this->sma->utf8Decode($sma->formatMoney($customer_liq)), 'B', 1, 'R');
            $pdf->SetFont('Arial','', $pdf->fontsize_body);
            $customer_liq = 0;
            $customer = 0;
        }
        if ($seller_liq > 0) {
            $pdf->SetFont('Arial','B', $pdf->fontsize_body);
            $pdf->cell(225, 5, $this->sma->utf8Decode('Liquidado vendedor'), 'B', 0, 'R');
            $pdf->cell(40, 5, $this->sma->utf8Decode($sma->formatMoney($seller_liq)), 'B', 1, 'R');
            $pdf->SetFont('Arial','', $pdf->fontsize_body);
            $seller_liq = 0;
        }
        if ($seller != 0) {
            $pdf->seller = $sale->name_seller;
            $pdf->AddPage();
        }
        $seller = $sale->seller_id;
    }
    if ($customer == 0 || ($customer != $sale->customer_id)) {
        $customer = $sale->customer_id;
        if ($customer_liq > 0) {
            $pdf->SetFont('Arial','B', $pdf->fontsize_body);
            $pdf->cell(225, 5, $this->sma->utf8Decode('Liquidado cliente'), 'B', 0, 'R');
            $pdf->cell(40, 5, $this->sma->utf8Decode($sma->formatMoney($customer_liq)), 'B', 1, 'R');
            $pdf->SetFont('Arial','', $pdf->fontsize_body);
            $customer_liq = 0;
        }
        $pdf->SetFont('Arial','B', $pdf->fontsize_body);
        $pdf->cell(265, 5, $this->sma->utf8Decode($sale->customer), '', 1, 'L');
        $pdf->SetFont('Arial','', $pdf->fontsize_body);
    }
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sale->reference_no), '', 0, 'C');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->total + $sale->total_discount)), '', 0, 'C');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->total_discount)), '', 0, 'R');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->total_tax)), '', 0, 'R');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->grand_total)), '', 0, 'R');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->total_payment + $sale->return_sale_total)), '', 0, 'R');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->return_sale_total)), '', 0, 'R');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->total_other_discounts)), '', 0, 'R');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->comm_base)), '', 0, 'R');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatDecimal($sale->comm_perc)), '', 0, 'C');
    $pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($sale->comm_amount)), '', 1, 'R');
    // $pdf->cell(26.5, 5, $this->sma->utf8Decode($sma->formatMoney($sale->comm_base)), '', 0, 'R');
    $total_base += $sale->comm_base;
    $total_comm += $sale->comm_amount;
    $customer_liq += $sale->comm_amount;
    $seller_liq += $sale->comm_amount;
}

if ($customer_liq > 0) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(225, 5, $this->sma->utf8Decode('Liquidado cliente'), 'B', 0, 'R');
    $pdf->cell(40, 5, $this->sma->utf8Decode($sma->formatMoney($customer_liq)), 'B', 1, 'R');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
}

if ($seller_liq > 0) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(225, 5, $this->sma->utf8Decode('Liquidado vendedor'), 'B', 0, 'R');
    $pdf->cell(40, 5, $this->sma->utf8Decode($sma->formatMoney($seller_liq)), 'B', 1, 'R');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
}


$pdf->SetFont('Arial','B', $pdf->fontsize_body);
$pdf->cell(168.64, 5, $this->sma->utf8Decode(''), '', 0, 'C');
$pdf->cell(24.09, 5, $this->sma->utf8Decode('Totales'), '', 0, 'C');
$pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($total_base)), '', 0, 'R');
$pdf->cell(24.09, 5, $this->sma->utf8Decode('-'), '', 0, 'C');
$pdf->cell(24.09, 5, $this->sma->utf8Decode($sma->formatMoney($total_comm)), '', 1, 'R');


$descargar = false;

if ($descargar) {
  $pdf->Output("INFORME_COMISIONES.pdf", "D");
} else {
  $pdf->Output("INFORME_COMISIONES.pdf", "I");
}