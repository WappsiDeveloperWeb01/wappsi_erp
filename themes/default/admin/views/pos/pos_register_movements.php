<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";
    var posrmpayment_reference_no;
    var biller;
    var filter_user;
    var filter_movement_type;
    var filtered = 0;
    var filtered_ini_date;


    if (localStorage.getItem('posrm_filter_filtered_ini_date')) {
        filtered_ini_date = localStorage.getItem('posrm_filter_filtered_ini_date');
    }
    <?php if (isset($_POST['start_date'])) : ?>
        localStorage.setItem('posrm_filter_start_date', '<?= $_POST['start_date'] ?>');
        start_date = '<?= $_POST['start_date'] ?>';
    <?php else : ?>
        if (localStorage.getItem('posrm_filter_start_date')) {
            start_date = localStorage.getItem('posrm_filter_start_date');
        }
    <?php endif ?>

    <?php if (isset($_POST['end_date'])) : ?>
        localStorage.setItem('posrm_filter_end_date', '<?= $_POST['end_date'] ?>');
        end_date = '<?= $_POST['end_date'] ?>';
    <?php else : ?>
        if (localStorage.getItem('posrm_filter_end_date')) {
            end_date = localStorage.getItem('posrm_filter_end_date');
        }
    <?php endif ?>

    <?php if (isset($_POST['posrmpayment_reference_no'])) : ?>
        localStorage.setItem('posrm_filter_posrmpayment_reference_no', '<?= $_POST['posrmpayment_reference_no'] ?>');
        posrmpayment_reference_no = '<?= $_POST['posrmpayment_reference_no'] ?>';
    <?php else : ?>
        if (localStorage.getItem('posrm_filter_posrmpayment_reference_no')) {
            ppayment_reference_no = localStorage.getItem('posrm_filter_ppayment_reference_no');
        }
    <?php endif ?>

    <?php if (isset($_POST['biller'])) : ?>
        localStorage.setItem('posrm_filter_biller', '<?= $_POST['biller'] ?>');
        biller = '<?= $_POST['biller'] ?>';
    <?php else : ?>
        if (localStorage.getItem('posrm_filter_biller')) {
            biller = localStorage.getItem('posrm_filter_biller');
        }
    <?php endif ?>

    <?php if (isset($_POST['filtered'])) : ?>
        localStorage.setItem('posrm_filter_filtered', '<?= $_POST['filtered'] ?>');
        filtered = '<?= $_POST['filtered'] ?>';
        if (filtered_ini_date === undefined) {
            localStorage.setItem('posrm_filter_filtered_ini_date', '<?= date("Y-m-d H:i:s") ?>');
            filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        }
    <?php else : ?>
        if (localStorage.getItem('posrm_filter_filtered')) {
            filtered = localStorage.getItem('posrm_filter_filtered');
        }
    <?php endif ?>

    <?php if (isset($_POST['filter_user'])) : ?>
        localStorage.setItem('posrm_filter_filter_user', '<?= $_POST['filter_user'] ?>');
        filter_user = '<?= $_POST['filter_user'] ?>';
    <?php else : ?>
        if (localStorage.getItem('posrm_filter_filter_user')) {
            filter_user = localStorage.getItem('posrm_filter_filter_user');
        }
    <?php endif ?>

    <?php if (isset($_POST['filter_movement_type'])) : ?>
        localStorage.setItem('posrm_filter_filter_movement_type', '<?= $_POST['filter_movement_type'] ?>');
        filter_movement_type = '<?= $_POST['filter_movement_type'] ?>';
    <?php else : ?>
        if (localStorage.getItem('posrm_filter_filter_movement_type')) {
            filter_movement_type = localStorage.getItem('posrm_filter_filter_movement_type');
        }
    <?php endif ?>

    <?php if (isset($_POST['date_records_filter'])) : ?>
        localStorage.setItem('posrm_filter_date_records_filter', '<?= $_POST['date_records_filter'] ?>');
    <?php endif ?>


    $(document).ready(function() {
        oTable = $('#EXPData').dataTable({
            "aaSorting": [
                [1, "desc"]
            ],
            "aLengthMenu": [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('pos/get_register_movements'); ?>',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "start_date",
                    "value": start_date
                }, {
                    "name": "end_date",
                    "value": end_date
                }, {
                    "name": "posrmpayment_reference_no",
                    "value": posrmpayment_reference_no
                }, {
                    "name": "biller",
                    "value": biller
                }, {
                    "name": "user",
                    "value": filter_user
                }, {
                    "name": "movement_type",
                    "value": filter_movement_type
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            "aoColumns": [{
                    "bSortable": false,
                    "mRender": checkbox
                },
                {
                    "mRender": fld
                },
                null,
                {
                    "mRender": currencyFormat
                },
                null,
                null,
                null
            ],
            'fnRowCallback': function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "pos_register_movement";
                return nRow;
            },
            "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                var total = 0;
                for (var i = 0; i < aaData.length; i++) {
                    total += parseFloat(aaData[aiDisplay[i]][3]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[3].innerHTML = currencyFormat(total);
            }
        }).fnSetFilteringDelay().dtFilter([{
                column_number: 1,
                filter_default_label: "[<?= lang('date'); ?> (yyyy-mm-dd)]",
                filter_type: "text",
                data: []
            },
            {
                column_number: 2,
                filter_default_label: "[<?= lang('reference'); ?>]",
                filter_type: "text",
                data: []
            },
            {
                column_number: 4,
                filter_default_label: "[<?= lang('movement_type'); ?>]",
                filter_type: "text",
                data: []
            },
            {
                column_number: 5,
                filter_default_label: "[<?= lang('note'); ?>]",
                filter_type: "text",
                data: []
            },
            {
                column_number: 6,
                filter_default_label: "[<?= lang('created_by'); ?>]",
                filter_type: "text",
                data: []
            },
        ], "footer");

    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight no-print">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <?= admin_form_open('pos/pos_register_movements', ['id' => 'ppayments_filter']) ?>
                                <div class="col-sm-4">
                                    <?= lang('reference_no', 'posrmpayment_reference_no') ?>
                                    <select name="posrmpayment_reference_no" id="posrmpayment_reference_no" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($documents_types) : ?>
                                            <?php foreach ($documents_types as $dt) : ?>
                                                <option value="<?= $dt->id ?>" data-dtprefix="<?= $dt->sales_prefix ?>" <?= isset($_POST['posrmpayment_reference_no']) && $_POST['posrmpayment_reference_no'] == $dt->id ? 'selected="selected"' : '' ?>><?= $dt->nombre . " (" . $dt->sales_prefix . ")" ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                    <?php
                                    $biller_selected = '';
                                    $biller_readonly = false;
                                    if ($this->session->userdata('biller_id')) {
                                        $biller_selected = $this->session->userdata('biller_id');
                                        $biller_readonly = true;
                                    }

                                    $bl[""] = lang('select');
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                    ?>
                                </div>
                                <?php if ($this->Owner || $this->Admin) : ?>
                                    <div class="col-sm-4">
                                        <label><?= lang('user') ?></label>
                                        <select name="filter_user" id="filter_user" class="form-control">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php if ($users) : ?>
                                                <?php foreach ($users as $user) : ?>
                                                    <option value="<?= $user->id ?>"><?= $user->first_name . " " . $user->last_name ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                <?php endif ?>


                                <div class="form-group col-sm-4">
                                    <?= lang("movement_type", "filter_movement_type"); ?>
                                    <select name="filter_movement_type" class="form-control" id="filter_movement_type">
                                        <option value=""><?= lang('select') ?></option>
                                        <option value="1"><?= lang('movement_type_in') ?></option>
                                        <option value="2"><?= lang('movement_type_out') ?></option>
                                    </select>
                                </div>

                                <hr class="col-sm-11">

                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>

                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                        <div class="col-sm-4 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('start_date', 'start_date') ?>
                                        <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                    </div>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('end_date', 'end_date') ?>
                                        <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $this->sma->fld($_POST['end_date']) : $this->filtros_fecha_final ?>" class="form-control datetime">
                                    </div>
                                </div>

                                <div class="col-sm-12" style="margin-top: 2%;">
                                    <input type="hidden" name="filtered" value="1">
                                    <button type="submit" id="submit-purchases-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                    <button type="button" id="submit-purchases-filter-clean" class="btn btn-danger"><span class="fa fa-times"></span> <?= lang('reset') ?></button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($Owner) {
        echo admin_form_open('pos/register_movements_actions', 'id="action-form"');
    } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info');
                                                ?></p> -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <?php if ($this->Admin || $this->Owner || $GP['pos-pos_register_add_movement']): ?>
                                        <li>
                                            <a href="<?= admin_url('pos/pos_register_add_movement') ?>" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus-circle"></i> <?= lang('pos_register_add_movement') ?>
                                            </a>
                                        </li>
                                    <?php endif ?>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="excel" data-action="reaccount">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('post_register_movement') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><? //= lang('list_results');
                                                        ?></p> -->
                            <div class="table-responsive">
                                <span class="text_filter"></span>
                                <table id="EXPData" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th style="width: 5%; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th style="width: 12.84%;"><?= lang("date"); ?></th>
                                            <th style="width: 12.84%;"><?= lang("reference"); ?></th>
                                            <th style="width: 12.84%;"><?= lang("amount"); ?></th>
                                            <th style="width: 12.84%;"><?= lang("movement_type"); ?></th>
                                            <th style="width: 27.84%;"><?= lang("note"); ?></th>
                                            <th style="width: 15.84%;"><?= lang("created_by"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>


<script type="text/javascript">
    
$(document).ready(function(){
    setTimeout(function() {
        <?php if (!isset($_POST['date_records_filter'])): ?>
            $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            $('#ppayments_filter').submit();
        <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
            $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
        <?php endif ?>
    }, 150);
});

    $(document).ready(function() {

        if (filtered !== undefined) {
            setTimeout(function() {
                $('#start_date').val(start_date);
                $('#end_date').val(end_date);
                $('#posrmpayment_reference_no').select2('val', posrmpayment_reference_no);
                $('#biller').select2('val', biller);
                $('#filter_user').select2('val', filter_user);
                $('#filter_movement_type').select2('val', filter_movement_type);
                // $('.collapse-link').click();
            }, 900);
        }


        if (filtered_ini_date !== undefined) {
            minutos = calcularMinutos(filtered_ini_date, '<?= date("Y-m-d H:i:s") ?>');
            if (minutos >= 10) {
                localStorage.removeItem('posrm_filter_start_date');
                localStorage.removeItem('posrm_filter_end_date');
                localStorage.removeItem('posrm_filter_biller');
                localStorage.removeItem('posrm_filter_filter_user');
                localStorage.removeItem('posrm_filter_filter_movement_type');
                localStorage.removeItem('posrm_filter_posrmpayment_reference_no');
                localStorage.removeItem('posrm_filter_filtered');
                localStorage.removeItem('posrm_filter_filtered_ini_date');
                localStorage.removeItem('posrm_filter_date_records_filter');
                location.href = '<?= admin_url("pos/pos_register_movements") ?>';
            }
        }

        setTimeout(function() {
            setFilterText();
        }, 1500);


        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>

    });

    $(document).on('click', '#submit-purchases-filter-clean', function() {
        localStorage.removeItem('posrm_filter_start_date');
        localStorage.removeItem('posrm_filter_end_date');
        localStorage.removeItem('posrm_filter_biller');
        localStorage.removeItem('posrm_filter_filter_user');
        localStorage.removeItem('posrm_filter_filter_movement_type');
        localStorage.removeItem('posrm_filter_posrmpayment_reference_no');
        localStorage.removeItem('posrm_filter_filtered');
        localStorage.removeItem('posrm_filter_filtered_ini_date');
        localStorage.removeItem('posrm_filter_date_records_filter');
        location.href = '<?= admin_url("pos/pos_register_movements") ?>';
    });

    function calcularMinutos(start_date, end_date) {
        var fecha1 = new Date(start_date);
        var fecha2 = new Date(end_date);
        var diff = fecha2.getTime() - fecha1.getTime();
        var minutos = diff / (1000 * 60);
        return minutos;
    }

    function setFilterText() {

        var reference_text = $('#posrmpayment_reference_no option:selected').data('dtprefix');
        var filter_movement_type = $('#filter_movement_type option:selected').text();
        var biller_text = $('#biller option:selected').text();
        var start_date_text = $('#start_date').val();
        var end_date_text = $('#end_date').val();
        var text = "Filtros configurados : ";

        coma = false;

        if (posrmpayment_reference_no != '' && posrmpayment_reference_no !== undefined) {
            text += " Tipo documento (" + reference_text + ")";
            coma = true;
        }
        if (biller != '' && biller !== undefined) {
            text += coma ? "," : "";
            text += " Sucursal (" + biller_text + ")";
            coma = true;
        }
        if (start_date != '' && start_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha de inicio (" + start_date_text + ")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha final (" + end_date_text + ")";
            coma = true;
        }
        if (filter_movement_type != '' && filter_movement_type !== undefined) {
            text += coma ? "," : "";
            text += " Tipo de movimiento (" + filter_movement_type + ")";
            coma = true;
        }

        $('.text_filter').html(text);

    }

    $('#date_records_filter').on('change', function() {
        filter = $(this).val();
        fecha_inicial = "";
        fecha_final = "<?= date('d/m/Y 23:59') ?>";
        hide_date_controls = true;
        if (filter == 5) { //RANGO DE FECHAS
            fecha_final = "";
            hide_date_controls = false;
        } else if (filter == 1) { // HOY
            fecha_inicial = "<?= date('d/m/Y 00:00') ?>";
        } else if (filter == 2) { // MES
            fecha_inicial = "<?= date('01/m/Y 00:00') ?>";
        } else if (filter == 3) { // TRIMESTRE
            fecha_inicial = "<?= date("d/m/Y 00:00", strtotime(date("Y-m-01") . "- " . $this->meses_para_trimestre . " month")) ?>";
        } else if (filter == 4) { // AÑO
            fecha_inicial = "<?= date('01/01/Y 00:00') ?>";
        } else if (filter == 6) { // MES PASADO
            fecha_inicial = "<?= date("d/m/Y 00:00", strtotime(date("Y-m-01") . "- 1 month")) ?>";
            fecha_final = "<?= date("d/m/Y 23:59", strtotime($this->sma->get_last_day_of_month(date("Y-m-d", strtotime(date("Y-m-01") . "- 1 month"))))) ?>";
        } else if (filter == 7) { // AÑO PASADO
            fecha_inicial = "<?= date("01/01/Y 00:00", strtotime(date("Y-m-01") . "- 1 year")) ?>";
            fecha_final = "<?= date("31/12/Y 23:59", strtotime(date("Y-m-01") . "- 1 year")) ?>";
        } else if (filter == 0) {
            fecha_inicial = "";
            fecha_final = "";
            hide_date_controls = true;
        }
        $('#start_date').val(fecha_inicial);
        $('#end_date').val(fecha_final);
        if (hide_date_controls) {
            $('.date_controls').css('display', 'none');
        } else {
            $('.date_controls').css('display', '');
        }
    });
</script>