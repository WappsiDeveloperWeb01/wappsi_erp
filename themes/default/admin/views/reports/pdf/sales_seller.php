<?php
/*
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'vendor/fpdf/fpdf.php';
*/
require_once APPPATH.'../vendor/fpdf/fpdf.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {     
        $this->SetFont('Arial','B',12);
        $this->Image($this->Logo,18,8,30,0);
        $this->Cell(100);        
        $this->Cell(30,4,'INFORME DE VENTAS POR VENDEDOR',0,0,'C');
        $this->Ln();
        $this->Cell(37); 
        $this->SetFont('Arial','',6);
        $this->Cell(0,4,'Fecha : '.date('d-m-Y h:i A'),0,0,'C');
        $this->Ln(8);

        $this->SetFont('Arial','B',7.5);
        $this->Cell(20.19, 5, $this->sma->utf8Decode('Vendedor'),'B',0,'C');
        $this->Cell(20.19, 5, $this->sma->utf8Decode('Sucursal'),'B',0,'C');
        $this->Cell(36.19, 5, $this->sma->utf8Decode('Cliente'),'B',0,'C');
        $this->Cell(20.19, 5, $this->sma->utf8Decode('Fecha'),'B',0,'C');
        $this->Cell(15.19, 5, $this->sma->utf8Decode('Referencia'),'B',0,'C');
        $this->Cell(15.19, 5, $this->sma->utf8Decode('Referencia'),'B',0,'C');
        $this->Cell(20.19, 5, $this->sma->utf8Decode('Subtotal'),'B',0,'C');
        $this->Cell(20.19, 5, $this->sma->utf8Decode('Impuestos'),'B',0,'C');
        $this->Cell(20.19, 5, $this->sma->utf8Decode('Total'),'B',0,'C');
        $this->Cell(14.19, 5, $this->sma->utf8Decode('Estado'),'B',1,'C');

    }

    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica '.'            Usuario: '.$this->User),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }

}

$fuente = 6;
$adicional_fuente = 2;

$height = 5;

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);
//$pdf->Logo = base_url('assets/uploads/logos/'.$Settings->logo);
$pdf->Logo = 'https://static.wixstatic.com/media/a560e4_0525ed9cae7e4d42bd1ac3c8a7851bca~mv2.png';

$pdf->AliasNbPages();

$pdf->User = $this->session->userdata('username');

$pdf->AddPage();
$pdf->SetFont('Arial','',$fuente);

$sellers = [];
$subtotal_seller = 0;
$total_iva_seller = 0;
$total_seller = 0;
$subtotal = 0;
$total_iva = 0;
$total = 0;

foreach ($sales as $sale)
{

    if (!isset($sellers[$sale['cod_seller']])) {

        if ($total_seller != 0) {
            $pdf->SetFont('Arial','B',$fuente+2);
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode('Total vendedor'),'B',0,'R');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($subtotal_seller)),'B',0,'R');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_iva_seller)),'B',0,'R');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_seller)),'B',0,'R');
            $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',1,'C');
            $pdf->SetFont('Arial','',$fuente);
            $subtotal_seller = 0;
            $total_iva_seller = 0;
            $total_seller = 0;
        }

        $pdf->SetFont('Arial','B',$fuente+2);
        $sellers[$sale['cod_seller']] = 1;
        $pdf->Cell(44.88, 5, $this->sma->utf8Decode($sale['nom_seller']),'',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'',1,'C');
        $pdf->SetFont('Arial','',$fuente);

    }

    $referencia = ($sale['return_sale_ref']!='') ? $sale['return_sale_ref'] : $sale['reference_no'] ;
    $afec = ($sale['return_sale_ref']!='') ? $sale['reference_no'] : '' ;
    // exit(var_dump($tipo));
    if ($tipo == 'Detallado' || !$tipo) {
        $pdf->Cell(20.19, 5, $sale['nom_seller'],'',0,'C');
        $pdf->Cell(20.19, 5, $sale['Sucursal'],'',0,'C');
        $pdf->Cell(36.19, 5, $sale['customer_name'],'',0,'C');
        $pdf->Cell(20.19, 5, $sale['date'],'',0,'C');
        $pdf->Cell(15.19, 5, $referencia,'',0,'C');
        $pdf->Cell(15.19, 5, $afec,'',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->formatMoney($sale['total']),'',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->formatMoney($sale['total_tax']),'',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->formatMoney($sale['grand_total']),'',0,'R');
        $pdf->Cell(14.19, 5, lang($sale['payment_status']),'',1,'C');
    }

    $subtotal_seller += $sale['total'];
    $total_iva_seller += $sale['total_tax'];
    $total_seller += $sale['grand_total'];
    $subtotal += $sale['total'];
    $total_iva += $sale['total_tax'];
    $total += $sale['grand_total'];

    if ($sale === end($sales)) {
        $pdf->SetFont('Arial','B',$fuente+2);
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode('Total vendedor'),'B',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($subtotal_seller)),'B',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_iva_seller)),'B',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_seller)),'B',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',1,'C');
        $pdf->SetFont('Arial','',$fuente);
        $total_seller = 0;
    }


}
        $pdf->SetFont('Arial','B',$fuente+2);
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',0,'C');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode('Total'),'B',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($subtotal)),'B',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_iva)),'B',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode($this->sma->formatMoney($total)),'B',0,'R');
        $pdf->Cell(20.19, 5, $this->sma->utf8Decode(''),'B',1,'C');
        $pdf->SetFont('Arial','',$fuente);

$pdf->Output();

?>