<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->session->flashdata("post") ? $this->session->flashdata("post") : ""; ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?> <?= $employee_data->name; ?></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <table id="employee_concepts_table" class="table table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Concepto</th>
                                <th>Dias/horas</th>
                                <th class="col-sm-2">Devengados</th>
                                <th class="col-sm-2">Deducciones</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" class="dataTables_empty text-center"><?= lang('loading_data_from_server') ?></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2"></th>
                                <th id="total_employee_earned"></th>
                                <th id="total_employee_deductions"></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>

                    <div id="full_payment"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_button(["class"=>"btn btn-default", "data-dismiss"=>"modal"], lang("close")); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        _total_earned = 0;
        _total_deductions = 0;

        load_datatable();
    });

    function load_datatable() {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#employee_concepts_table').dataTable({
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url("payroll_electronic/get_employee_concepts_datatables/".$payroll_id."/".$employee_id) ?>',
            dom: 't',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnDrawCallback: function(settings) {
                $('#employee_concepts_table tbody tr').each(function() {
                    var data = $('#employee_concepts_table').DataTable().row(this).data();

                    let conceptId = data[4]
                    let paidToEmployee = data[6]
                    let provisionOptions = '<?= $this->Payroll_settings->provision_options ?>'
                    
                    if (provisionOptions != 3) {
                        if (['<?= LAYOFFS ?>'].includes(conceptId)) {
                            if (paidToEmployee == 0) {
                                $(this).hide();
                            }
                        }
                    }
                });
            },
            fnFooterCallback: function(row, aData) {
                var total_earned = total_deductions = 0;

                $.each(aData, function(index, data) {
                    let conceptId = data[4]
                    let paidToEmployee = data[6]
                    let provisionOptions = '<?= $this->Payroll_settings->provision_options ?>'

                    if (provisionOptions == 3) {
                        total_deductions += parseFloat((data[3] == '') ? 0 : data[3]);
                        total_earned += parseFloat((data[2] == '') ? 0 : data[2]);
                    } else {
                        if (['<?= LAYOFFS ?>'].includes(conceptId)) {
                            if (paidToEmployee == 1) {
                                total_deductions += parseFloat((data[3] == '') ? 0 : data[3]);
                                total_earned += parseFloat((data[2] == '') ? 0 : data[2]);
                            }
                        } else {
                            total_deductions += parseFloat((data[3] == '') ? 0 : data[3]);
                            total_earned += parseFloat((data[2] == '') ? 0 : data[2]);
                        }
                    }

                })

                $('#total_employee_earned').html(currencyFormat(total_earned))
                $('#total_employee_deductions').html(currencyFormat(total_deductions))

                _total_earned = total_earned;
                _total_deductions = total_deductions;
            },
            aoColumns: [
                null,
                {
                    className: 'text-center',
                    render: function name(data, b, row) {
                        time_unit = row[5];
                        amount = (time_unit == 'horas') ? data : parseInt(data);

                        if (amount == 0) {
                            return '';
                        } else {
                            return parseInt(amount) +' '+ time_unit;
                        }
                    }
                },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                { visible: false },
                { visible: false }
            ]
        });

        $('#employee_concepts_table').DataTable().on("draw", function(){
            calculate_total_payment();
        })

    }

    function calculate_total_payment() {
        var full_payment = (_total_earned - _total_deductions);
        $('#full_payment').html('<div class="alert alert-success text-right">Total a pagar: <label>'+ currencyFormat(full_payment) +' </label></div>');
    }
</script>
