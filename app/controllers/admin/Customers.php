<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller
{
    public $customerPaymentsTypes = [];
    public $months = [];

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('customers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('pos_model');
        $this->load->admin_model('sales_model');

        $this->customerPaymentsTypes =  json_decode(json_encode([
            ['id' =>'0', 'name' => lang('credit')],
            ['id' =>'1', 'name' => lang('payment_type_cash')]
        ]));

        $this->months =  json_decode(json_encode([
            ['id' => 1, 'name' => "Enero"],
            ['id' => 2, 'name' => "Febrero"],
            ['id' => 3, 'name' => "Marzo"],
            ['id' => 4, 'name' => "Abril"],
            ['id' => 5, 'name' => "Mayo"],
            ['id' => 6, 'name' => "Junio"],
            ['id' => 7, 'name' => "Julio"],
            ['id' => 8, 'name' => "Agosto"],
            ['id' => 9, 'name' => "Septiembre"],
            ['id' => 10, 'name' => "Octubre"],
            ['id' => 11, 'name' => "Noviembre"],
            ['id' => 12, 'name' => "Diciembre"]
        ]));
    }

    public function index($action = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['action'] = $action;
        $this->data['months'] = $this->months;
        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data["typesPerson"] = $this->site->get_types_person();
        $this->data["typesRegime"] = $this->site->get_types_vat_regime();
        $this->data['countries'] = $this->companies_model->getCountries();
        $this->data['customerProducts'] = $this->site->getCustomerProduct();
        $this->data["customerPaymentsTypes"] = $this->customerPaymentsTypes;
        $this->data['priceGroups'] = $this->companies_model->getAllPriceGroups();
        $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
        $this->data['partners'] = $this->site->getAllCompaniesWithState('partner');
        $this->data['customerGroups'] = $this->companies_model->getAllCustomerGroups();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->input->post('country_filter') || $this->input->post('month_filter') || $this->input->post('tax_exempt_customer_filter') == 'on'
            || $this->input->post('customer_products_filter') || $this->input->post('partnerId_filter')) {
            if ($this->input->post('country_filter')) {
                $this->data['states_filter'] = $this->companies_model->getStatesByCountry($this->input->post('countryCode_filter'));

                if ($this->input->post('state_filter')) {
                    $this->data['cities_filter'] = $this->companies_model->getCitiesByState($this->input->post('stateCode_filter'));

                    if ($this->input->post('city_filter')) {
                        $this->data['zones_filter'] = $this->companies_model->getZonesByCity($this->input->post('cityCode_filter'));

                        if ($this->input->post('zone_filter')) {
                            $this->data['subzones_filter'] = $this->companies_model->getSubZonesByZones($this->input->post('zoneCode_filter'));
                        }
                    }
                }
            }

            $this->data['advancedFiltersContainer'] = TRUE;
        }

        $this->page_construct('customers/index', ['page_title' => lang('customers')], $this->data);
    }

    public function getCustomers()
    {
        $this->sma->checkPermissions('index');

        $data = [
            'option_filter' => $this->input->post('option_filter') == 'false' ? FALSE : $this->input->post('option_filter'),
            'get_json' => $this->input->post('get_json') ? $this->input->post('get_json') : FALSE,
        ];

        if ($data['get_json']) {
            $data['option_filter'] = 'active';
            $active = $this->get_customers_result($data);
            $data['option_filter'] = 'all';
            $all = $this->get_customers_result($data);
            $data['option_filter'] = 'inactive';
            $inactive = $this->get_customers_result($data);
            $data['option_filter'] = 'user';
            $user = $this->get_customers_result($data);
            $data['option_filter'] = 'advances';
            $advances = $this->get_customers_result($data);
            $data['option_filter'] = 'giftCard';
            $giftCard = $this->get_customers_result($data);

            echo json_encode(['active'=>$active, 'all'=> $all, 'inactive'=>$inactive, 'user'=>$user, 'advances'=>$advances, 'giftCard'=>$giftCard]);
        } else {
            echo $this->get_customers_result($data);
        }
    }

    public function get_customers_result($data)
    {
        $option_filter = $data['option_filter'];
        $get_json = $data['get_json'];
        $typePerson = $this->input->post('type_person_filter') ?? '';
        $tipoRegimen = $this->input->post('tipo_regimen_filter') ?? '';
        $customerGroup = $this->input->post('customer_group_id_filter') ?? '';
        $priceGroup = $this->input->post('price_group_id_filter') ?? '';
        $customerPaymentType = $this->input->post('customer_payment_type_filter') ?? '';
        $customerSellerIdAssigned = $this->input->post('customer_seller_id_assigned_filter') ?? '';
        $country = $this->input->post('country_filter') ?? '';
        $state = $this->input->post('state_filter') ?? '';
        $city = $this->input->post('city_filter') ?? '';
        $zone = $this->input->post('zone_filter') ?? '';
        $subzone = $this->input->post('subzone_filter') ?? '';
        $birthMonth = $this->input->post('birth_month_filter') ?? '';
        $tax_exempt_customer = $this->input->post('tax_exempt_customer_filter') ?? '';
        $productCategory = $this->input->post('customer_products_filter') ?? '';
        $partnerId = $this->input->post('partnerId_filter') ?? '';

        $this->load->library('datatables');
        $this->datatables->select("{$this->db->dbprefix('companies')}.id AS companiesId,
            customer_profile_photo,
            {$this->db->dbprefix('companies')}.company,
            name,
            {$this->db->dbprefix('companies')}.email,
            {$this->db->dbprefix('companies')}.phone,
            price_group_name,
            customer_group_name,
            vat_no,
            deposit_amount,
            {$this->db->dbprefix('companies')}.award_points,
            {$this->db->dbprefix('companies')}.status,
            if({$this->db->dbprefix('companies')}.status = 1, 'fas fa-user-slash', 'fas fa-user-check') as faicon,
            if({$this->db->dbprefix('companies')}.status = 1, '".lang('deactivate_customer')."', '".lang('activate_customer')."') as detailaction,
            if({$this->db->dbprefix('companies')}.status = 1, '" . admin_url('customers/deactivate/') . "', '" . admin_url('customers/activate/') . "') as linkaction,");
        $this->datatables->where("group_name", "customer");
        $this->datatables->from("companies");

        if (!empty($typePerson)) {
            $this->datatables->where('type_person', $typePerson);
        }
        if (!empty($tipoRegimen)) {
            $this->datatables->where('tipo_regimen', $tipoRegimen);
        }
        if (!empty($customerGroup)) {
            $this->datatables->where('customer_group_id', $customerGroup);
        }
        if (!empty($priceGroup)) {
            $this->datatables->where('price_group_id', $priceGroup);
        }
        if (!empty($customerPaymentType)) {
            $this->datatables->where('customer_payment_type', $customerPaymentType);
        }
        if (!empty($customerSellerIdAssigned)) {
            $this->datatables->where('customer_seller_id_assigned', $customerSellerIdAssigned);
        }
        if (!empty($customerSellerIdAssigned)) {
            $this->datatables->where('customer_seller_id_assigned', $customerSellerIdAssigned);
        }
        if (!empty($country)) {
            $this->datatables->where('country', $country);
        }
        if (!empty($state)) {
            $this->datatables->where('state', $state);
        }
        if (!empty($city)) {
            $this->datatables->where('city', $city);
        }
        if (!empty($zone)) {
            $this->datatables->where('location', $zone);
        }
        if (!empty($subzone)) {
            $this->datatables->where('subzone', $subzone);
        }
        if (!empty($birthMonth)) {
            $this->datatables->where('birth_month', $birthMonth);
        }
        if (!empty($tax_exempt_customer)) {
            $this->datatables->where('Tax_exempt_customer', $tax_exempt_customer);
        }
        if (!empty($productCategory)) {
            $this->datatables->join('customer_product_category', 'customer_product_category.companies_id = companies.id', 'inner');
            $this->datatables->where('product_category_id', $productCategory);
        }
        if (!empty($partnerId)) {
            $this->datatables->where('id_partner', $partnerId);
        }

        if ($option_filter) {
            if ($option_filter == 'active') {
                $this->datatables->where('status', 1);
            } else if ($option_filter == 'inactive') {
                $this->datatables->where('status', 0);
            } else if ($option_filter == 'user') {
                $this->datatables->join("users", "users.company_id = companies.id", "inner");
            } else if ($option_filter == 'advances') {
                $this->datatables->where('deposit_amount >', 0);
            } else if ($option_filter == 'giftCard') {
                $this->datatables->select("sum(balance) AS giftCardsAmount");
                $this->datatables->join("gift_cards", "gift_cards.customer_id = companies.id", "inner");
                $this->datatables->group_by("balance");
                $this->datatables->having("giftCardsAmount > 0");
            }
        } else {
            $this->datatables->where('status', 1);
        }

        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                        <div class=\"btn-group text-left\">
                                            <button type=\"button\" class=\"btn btn-default new-button new-button-sm btn-xs dropdown-toggle\" data-toggle=\"dropdown\" data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                                            <i class='fas fa-ellipsis-v fa-lg'></i>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                ".
                                                ($this->Owner || $this->Admin || $this->GP['customers-list_deposits'] ? "<li>
                                                    <a href='" . admin_url('customers/deposits/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                       <i class=\"fa fa-money\"></i>
                                                            ".lang("list_deposits")."
                                                    </a>
                                                 </li>" : "")."
                                                <li>
                                                    <a href='" . admin_url('customers/add_deposit/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-plus\"></i>
                                                        ".lang("add_deposit")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('customers/addresses/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-location-arrow\"></i>
                                                        ".lang("customer_branches")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('customers/add_address/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-plus\"></i>
                                                        ".lang("add_customer_branch")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('customers/users/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-users\"></i>
                                                        ".lang("list_users")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('customers/add_user/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-user-plus\"></i>
                                                        ".lang("add_user")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-edit\"></i>
                                                        ".lang("edit_customer")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('reports/customer_report/$1') . "' target='_blank'>
                                                        <i class=\"fa fa-edit\"></i>
                                                        ".lang("customers_report")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('customers/sync_award_points/$1') . "'>
                                                        <i class=\"fa fa-sync\"></i>
                                                        ".lang("sync_award_points")."
                                                    </a>
                                                </li>".
                                                ($this->sma->actionPermissions('delete', 'customers') || $this->Admin || $this->Owner ?
                                                "<li>
                                                    <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='$4/$1'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"$2\"></i>
                                                        $3
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='".admin_url('customers/delete_customer/$1')."'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash\"></i>
                                                        ".lang('delete_customer')."
                                                    </a>
                                                </li>" : "")."".
                                                ( $this->Settings->wappsi_invoice_issuer == 1 && $this->Owner ?
                                                "<li>
                                                    <a href='" . admin_url('wappsi_invoicing/download_customer_invoicing/$1') . "'>
                                                        <i class=\"fa fa-sync\"></i>
                                                        ".lang("download_customer_invoicing")."
                                                    </a>
                                                </li>" : "")."
                                            </ul>
                                        </div>
                                    </div>", "companiesId, faicon, detailaction, linkaction");

        $this->datatables->unset_column('faicon');
        $this->datatables->unset_column('detailaction');
        $this->datatables->unset_column('linkaction');

        if ($get_json) {
            return $this->datatables->get_result_num_rows();
        } else {
            return $this->datatables->generate();
        }
    }

    public function getCustomersBirthdays($today = false)
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("id, vat_no, name, company, email, phone, price_group_name, customer_group_name, birth_day")
            ->from("companies")
            ->where('group_name', 'customer')
            ->where('birth_month', date('m'));
        if ($today) {
            $this->datatables->where('birth_day', date('d'));
        }
        echo $this->datatables->generate();
    }

    public function view($id = NULL)
    {
        $this->sma->checkPermissions('index', true);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['customer'] = $this->companies_model->getCompanyByID($id);
        $this->data['seller_assigned'] = $this->companies_model->getCompanyByID($this->data['customer']->customer_seller_id_assigned);
        $this->data["types_customer_obligations"] = $this->site->get_customer_obligations($id, ACQUIRER);
        $this->data['default_rete_fuente'] = $this->site->get_ledger_by_id($this->data['customer']->default_rete_fuente_id);
        $this->data['default_rete_iva'] = $this->site->get_ledger_by_id($this->data['customer']->default_rete_iva_id);
        $this->data['default_rete_ica'] = $this->site->get_ledger_by_id($this->data['customer']->default_rete_ica_id);
        $this->data['default_rete_other'] = $this->site->get_ledger_by_id($this->data['customer']->default_rete_other_id);
        $this->data["custom_fields"] = $this->site->get_custom_fields();
        $this->data['addresses'] = $this->companies_model->getCompanyAddresses($id);
        // exit(var_dump($this->data["custom_fields"]));
        $this->data['created_user'] = $this->site->getUserById($this->data['customer']->created_by);
        $this->load_view($this->theme.'customers/view', $this->data);
    }

    public function add()
    {
        $this->sma->checkPermissions(false, true);
        $only_pos = $this->input->post('customer_only_for_pos') ? $this->input->post('customer_only_for_pos') : NULL;
        $pos_call = $this->input->get('pos_call');
        if ($only_pos == NULL) {
            // $this->form_validation->set_rules('email', lang("email_address"), 'is_unique[companies.email]');
        }
        if ($this->form_validation->run('companies/add') == TRUE) {
            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_group'));
            $pg = $this->site->getPriceGroupByID($this->input->post('price_group'));
            $company = $this->input->post("company");
            $customer_type = $this->input->post("customer_type");
            $group_id = $this->companies_model->get_group_by_name($customer_type);
            $email = $this->input->post('email');
            if ($this->input->post('type_person') == NATURAL_PERSON) {
                $nombreCompleto = $this->input->post('first_name')." ".
                                  ($this->input->post('second_name') ? $this->input->post('second_name')." " : "").
                                  ($this->input->post('first_lastname') ? $this->input->post('first_lastname')." " : "").
                                  ($this->input->post('second_lastname') ? $this->input->post('second_lastname')." " : "");
            } else {
                $nombreCompleto = $this->input->post('name');
            }
            if ($only_pos) {
                $company = $nombreCompleto;
            }
            $data = array('name' => $this->input->post('name'),
                'email' => mb_strtolower($email),
                'group_id' => $group_id,
                'group_name' => $customer_type,
                'customer_group_id' => $this->input->post('customer_group'),
                'customer_group_name' => $cg->name,
                'price_group_id' => $this->input->post('price_group') ? $this->input->post('price_group') : NULL,
                'price_group_name' => $this->input->post('price_group') ? $pg->name : NULL,
                'company' => $company,
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $nombreCompleto,
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'cf1' => $this->sma->cf_arr_to_text($this->input->post('cf1')),
                'cf2' => $this->sma->cf_arr_to_text($this->input->post('cf2')),
                'cf3' => $this->sma->cf_arr_to_text($this->input->post('cf3')),
                'cf4' => $this->sma->cf_arr_to_text($this->input->post('cf4')),
                'cf5' => $this->sma->cf_arr_to_text($this->input->post('cf5')),
                'cf6' => $this->sma->cf_arr_to_text($this->input->post('cf6')),
                'type_person' => $this->input->post('type_person'),
                'tipo_regimen' => $this->input->post('type_vat_regime'),
                'tipo_documento' => $this->input->post('tipo_documento'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code'),
                "location" => $this->input->post("location"),
                "document_code" => $this->input->post("document_code"),
                "commercial_register" => $this->input->post("commercial_register"),
                "birth_month" => $this->input->post("birth_month"),
                "birth_day" => $this->input->post("birth_day"),
                // "customer_only_for_pos" => $this->input->post("customer_only_for_pos") ? 1 : 0,
                "customer_only_for_pos" => 0,
                "location" => $this->input->post("zone"),
                "subzone" => $this->input->post("subzone"),
                "customer_seller_id_assigned" => $this->input->post("customer_seller_id_assigned"),
                "customer_payment_type" => $this->input->post("customer_payment_type"),
                "customer_credit_limit" => $this->input->post("customer_credit_limit"),
                "customer_payment_term" => $this->input->post("customer_payment_term"),
                "customer_special_discount" => $this->input->post("customer_special_discount") ? 1 : 0,
                'fuente_retainer' => $this->input->post('fuente_retainer') ? 1 : 0,
                'iva_retainer' => $this->input->post('iva_retainer') ? 1 : 0,
                'ica_retainer' => $this->input->post('ica_retainer') ? 1 : 0,
                "default_rete_fuente_id" => $this->input->post("default_rete_fuente_id"),
                "default_rete_iva_id" => $this->input->post("default_rete_iva_id"),
                "default_rete_ica_id" => $this->input->post("default_rete_ica_id"),
                "default_rete_other_id" => $this->input->post("default_rete_other_id"),
                'customer_validate_min_base_retention' => $this->input->post('customer_validate_min_base_retention') ? 1 : 0,
                'tax_exempt_customer' => $this->input->post('tax_exempt_customer') ? 1 : 0,
                "note" => $this->input->post("note"),
                'status' => $this->input->post('status') ? 1 : 0,
                'registration_date' => date('Y-m-d H:i:s'),
                "latitude" => $this->input->post("latitude"),
                "longitude" => $this->input->post("longitude"),
                "customer_employer_id" => $this->input->post("employer"),
                "customer_employer_branch_id" => $this->input->post("employer_branch"),
                'award_points_no_management' => $this->input->post('award_points_no_management') ? 1 : 0,
                'created_by' => $this->session->userdata('user_id'),
            );
            if ($this->companies_model->validate_vat_no($data['vat_no'], 'customer')) {
                $this->session->set_flashdata('error', 'Ya existe un tercero con el mismo documento y mismo perfil.');
                admin_redirect('customers');
            }
            if ($_FILES['customer_profile_photo']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = 'assets/uploads/avatars';
                $config['allowed_types'] = 'gif|jpg|png';
                //$config['max_size'] = '500';
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('customer_profile_photo')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/add");
                }
                $photo = $this->upload->file_name;
                $data['customer_profile_photo'] = $photo;
                $config = NULL;
            }
            // $this->sma->print_arrays($data);
        } elseif ($this->input->post('add_customer')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }
        if ($this->form_validation->run() == true && $saved_customer_id = $this->companies_model->addCompany($data))
        {
          $type_customer_obligations = $this->input->post("types_obligations");
          $customer_obligations_array = [];
          if ($type_customer_obligations) {
              foreach ($type_customer_obligations as $customer_obligation)
              {
                $customer_obligations_array[] = [
                  "customer_id" => $saved_customer_id,
                  "types_obligations_id" => $customer_obligation,
                  "relation" => ACQUIRER
                ];
              }
            $this->companies_model->insert_type_customer_obligations($customer_obligations_array);
          }
            if ($this->Settings->export_to_csv_each_new_customer == 1) {
                $this->new_customer_export_csv($saved_customer_id);
            }
          $this->session->set_flashdata('message', lang("customer_added"));
          redirect($_SERVER["HTTP_REFERER"].'?new_customer_added='.$saved_customer_id);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['pos_call'] = $pos_call;
            $this->data['customer_js'] = $this->site->customers_js();
            $this->data["types_person"] = $this->site->get_types_person();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $this->data["types_obligations"] = $this->site->get_types_obligations();
            $this->data["custom_fields"] = $this->site->get_custom_fields();
            $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
            $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
            $rete_data = $this->sales_model->getWithHolding(NULL, 'S');
            $retentions = [];
            if ($rete_data) {
                foreach ($rete_data as $row) {
                    $retentions[$row['type']][] = $row;
                }
            }
            $this->data['retentions'] = $retentions;
            $this->load_view($this->theme . 'customers/add', $this->data);
        }
    }

    public function get_states($country = null, $cur_state = null, $response = null)
    {
        $country = $country ? $country : $this->input->post('country');
        $cur_state = $cur_state ? urldecode($cur_state) : $this->input->post('state');
        $states = $this->companies_model->getStatesByCountry($country);
        if ($response === null) {
            $options = '<option value="">'.$this->lang->line("alls").'</option>';
            if (!empty($states)) {
                foreach ($states as $row => $state) {
                    if ($cur_state !== null && $cur_state == $state->DEPARTAMENTO) {
                        $selected =' selected="selected"';
                    } else {
                        $selected = "";
                    }

                    $options.='<option value="'.$state->DEPARTAMENTO.'" data-code="'.$state->CODDEPARTAMENTO.'"'.$selected.'>'.$state->DEPARTAMENTO.'</option>';
                }
            }
            echo $options;
        } else {
            foreach ($states as $row => $state) {
                $options[$state->DEPARTAMENTO] = $state->DEPARTAMENTO;
            }
            return $options;
        }
    }

    public function get_cities($state = null, $cur_city = null, $response = null)
    {
        $state = $state ? $state : $this->input->post('state');
        $cur_city = $cur_city ? urldecode($cur_city) : $this->input->post('city');
        $cities = $this->companies_model->getCitiesByState($state);
        if ($response === null) {
            $options = '<option value="">'.$this->lang->line("allsf").'</option>';
            if (!empty($cities)) {
                foreach ($cities as $row => $city) {
                    if ($cur_city !== null && $cur_city == $city->DESCRIPCION) {
                        $selected =' selected="selected"';
                    } else {
                        $selected = "";
                    }

                    $options.='<option value="'.$city->DESCRIPCION.'" data-code="'.$city->CODIGO.'"'.$selected.'>'.$city->DESCRIPCION.'</option>';
                }
            }
            echo $options;
        } else {
            foreach ($cities as $row => $city) {
                $options[$city->DESCRIPCION] = $city->DESCRIPCION;
            }
            return $options;
        }
    }

    public function get_zones($city = null, $cur_zone = null, $response = null)
    {
        $city = $city ? $city : $this->input->post('city');
        $cur_zone = $cur_zone ? urldecode($cur_zone) : $this->input->post('zone');
        $zones = $this->companies_model->getZonesByCity($city);
        if ($response === null) {
            $options = '<option value="">'.$this->lang->line('allsf').'</option>';
            if (!empty($zones)) {
                foreach ($zones as $row => $zone) {
                    if ($cur_zone !== null && $cur_zone == $zone->id) {
                        $selected =' selected="selected"';
                    } else {
                        $selected = "";
                    }
                    $options.='<option value="'.$zone->id.'" data-code="'.$zone->zone_code.'"'.$selected.'>'.$zone->zone_name.'</option>';
                }
            }
            echo $options;
        } else {
            foreach ($zones as $row => $zone) {
                $options[$zone->zone_name] = $zone->zone_name;
            }
            return $options;
        }
    }

    public function get_subzones($zone = null, $cur_subzone = null, $response = null)
    {
        $subzones = $this->companies_model->getSubZonesByZones($zone);
        $zone = $zone ? $zone : $this->input->post('zone');
        $cur_subzone = $cur_subzone ? urldecode($cur_subzone) : $this->input->post('subzone');
        if ($response === null) {
            $options = '<option value="">'.$this->lang->line("alls").'</option>';
            if (!empty($subzones)) {
                foreach ($subzones as $row => $subzone) {
                    if ($cur_subzone !== null && $cur_subzone == $subzone->id) {
                        $selected =' selected="selected"';
                    } else {
                        $selected = "";
                    }
                    $options.='<option value="'.$subzone->id.'" data-code="'.$subzone->subzone_code.'"'.$selected.'>'.$subzone->subzone_name.'</option>';
                }
            }
            echo $options;
        } else {
            foreach ($subzones as $row => $subzone) {
                $options[$subzone->subzone_name] = $subzone->subzone_name;
            }
            return $options;
        }
    }

    public function edit($id = NULL)
    {
        $this->sma->checkPermissions(false, true);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $company_details = $this->companies_model->getCompanyByID($id);
        if ($this->input->post('vat_no') && $this->input->post('vat_no') != $company_details->vat_no) {
          // $this->form_validation->set_rules('vat_no', lang("vat_no"), 'is_unique[companies.vat_no]');
            if ($this->companies_model->validate_vat_no($this->input->post('vat_no'), 'customer')) {
                $this->session->set_flashdata('error', 'Ya existe un tercero con el mismo documento y mismo perfil.');
                admin_redirect('customers');
            }
        }

        $only_pos = $this->input->post('customer_only_for_pos') ? $this->input->post('customer_only_for_pos') : NULL;

        if ($only_pos == NULL && $this->input->post('email') && $this->input->post('email') != $company_details->email) {
          // $this->form_validation->set_rules('email', lang("email_address"), 'is_unique[companies.email]');
        }

        if ($this->form_validation->run('companies/add') == true) {

            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_group'));
            $pg = $this->site->getPriceGroupByID($this->input->post('price_group'));

            $email = $this->input->post('email');

            if ($this->input->post('type_person') == NATURAL_PERSON) {
                $nombreCompleto = $this->input->post('first_name')." ".
                                  ($this->input->post('second_name') ? $this->input->post('second_name')." " : "").
                                  ($this->input->post('first_lastname') ? $this->input->post('first_lastname')." " : "").
                                  ($this->input->post('second_lastname') ? $this->input->post('second_lastname')." " : "");
            } else {
                $nombreCompleto = $this->input->post('name');
            }

            $company = $this->input->post("company");

            if ($only_pos) {
                $company = $nombreCompleto;
            }

            $data = array('name' => $this->input->post('name'),
                'email' => mb_strtolower($email),
                'group_id' => '3',
                'group_name' => 'customer',
                'customer_group_id' => $this->input->post('customer_group'),
                'customer_group_name' => $cg->name,
                'price_group_id' => $this->input->post('price_group') ? $this->input->post('price_group') : NULL,
                'price_group_name' => $this->input->post('price_group') ? $pg->name : NULL,
                'company' => $company,
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $nombreCompleto,
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'cf1' => $this->sma->cf_arr_to_text($this->input->post('cf1')),
                'cf2' => $this->sma->cf_arr_to_text($this->input->post('cf2')),
                'cf3' => $this->sma->cf_arr_to_text($this->input->post('cf3')),
                'cf4' => $this->sma->cf_arr_to_text($this->input->post('cf4')),
                'cf5' => $this->sma->cf_arr_to_text($this->input->post('cf5')),
                'cf6' => $this->sma->cf_arr_to_text($this->input->post('cf6')),
                'type_person' => $this->input->post('type_person'),
                'tipo_regimen' => $this->input->post('type_vat_regime'),
                'tipo_documento' => $this->input->post('tipo_documento'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code'),
                "location" => $this->input->post("zone"),
                "subzone" => $this->input->post("subzone"),
                "document_code" => $this->input->post("document_code"),
                "commercial_register" => $this->input->post("commercial_register"),
                "birth_month" => $this->input->post("birth_month"),
                "birth_day" => $this->input->post("birth_day"),
                "latitude" => $this->input->post("latitude"),
                "longitude" => $this->input->post("longitude"),
                "customer_employer_id" => $this->input->post("employer"),
                "customer_employer_branch_id" => $this->input->post("employer_branch"),
                "customer_only_for_pos" => $this->input->post("customer_only_for_pos") ? 1 : 0,
                "customer_seller_id_assigned" => $this->input->post("customer_seller_id_assigned"),
                "customer_payment_type" => $this->input->post("customer_payment_type"),
                "customer_credit_limit" => $this->input->post("customer_credit_limit"),
                "customer_payment_term" => $this->input->post("customer_payment_term"),
                "customer_special_discount" => $this->input->post("customer_special_discount") ? 1 : 0,
                'fuente_retainer' => $this->input->post('fuente_retainer') ? 1 : 0,
                'iva_retainer' => $this->input->post('iva_retainer') ? 1 : 0,
                'ica_retainer' => $this->input->post('ica_retainer') ? 1 : 0,
                "default_rete_fuente_id" => $this->input->post("default_rete_fuente_id"),
                "default_rete_iva_id" => $this->input->post("default_rete_iva_id"),
                "default_rete_ica_id" => $this->input->post("default_rete_ica_id"),
                "default_rete_other_id" => $this->input->post("default_rete_other_id"),
                'customer_validate_min_base_retention' => $this->input->post('customer_validate_min_base_retention') ? 1 : 0,
                'tax_exempt_customer' => $this->input->post('tax_exempt_customer') ? 1 : 0,
                "note" => $this->input->post("note"),
                'status' => $this->input->post('status') ? 1 : 0,
                'award_points_no_management' => $this->input->post('award_points_no_management') ? 1 : 0,
            );

            if ($_FILES['customer_profile_photo']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = 'assets/uploads/avatars';
                $config['allowed_types'] = 'gif|jpg|png';
                //$config['max_size'] = '500';
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('customer_profile_photo')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/add");
                }
                $photo = $this->upload->file_name;
                $data['customer_profile_photo'] = $photo;
                $config = NULL;
            }
            $txt_fields_changed = '';
            foreach ($data as $field => $new_value) {
                if (!empty($new_value) && isset($company_details->{$field}) && $company_details->{$field} != $new_value) {
                    if ($txt_fields_changed == '') {
                        $txt_fields_changed = sprintf(lang('user_fields_changed'), $this->session->first_name." ".$this->session->last_name, lang($this->m), $id);
                    }

                    if ($field == 'customer_payment_type') {

                        if ($company_details->{$field} == 0) {
                            $company_details->{$field} = lang('credit');
                        } else if ($company_details->{$field} == 1) {
                            $company_details->{$field} = lang('payment_type_cash');
                        }
                        if ($new_value == 0) {
                            $new_value = lang('credit');
                        } else if ($new_value == 1) {
                            $new_value = lang('payment_type_cash');
                        }

                    } else if ($field == 'name' || $field == 'company' || $field == 'vat_no' || $field == 'digito_verificacion') {

                    } else {
                        if ($company_details->{$field} === 0) {
                            $company_details->{$field} = lang('no');
                        } else if ($company_details->{$field} === 1) {
                            $company_details->{$field} = lang('yes');
                        }
                        if ($new_value === 0) {
                            $new_value = lang('no');
                        } else if ($new_value === 1) {
                            $new_value = lang('yes');
                        }
                    }


                    $txt_fields_changed .= sprintf(lang('fields_changed'), lang($field), $company_details->{$field}, $new_value);
                }
            }
            // exit(var_dump($txt_fields_changed));
        } elseif ($this->input->post('edit_customer')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data)) {
            $this->companies_model->update_customer_main_address($id, $data);
            $this->site->delete_types_customer_obligations($id, ACQUIRER);
            $type_customer_obligations = $this->input->post("types_obligations");
            $customer_obligations_array = [];
            if ($type_customer_obligations) {
                foreach ($type_customer_obligations as $customer_obligation)
                {
                    $customer_obligations_array[] = [
                        "customer_id" => $id,
                        "types_obligations_id" => $customer_obligation,
                        "relation" => ACQUIRER
                    ];
                }
                $this->companies_model->insert_type_customer_obligations($customer_obligations_array);
            }
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 1,
                'user_id' => $this->session->userdata('user_id'),
                'table_name' => 'companies',
                'record_id' => $id,
                'module_name' => $this->m,
                'description' => $txt_fields_changed,
            ]);
            $this->session->set_flashdata('message', lang("customer_updated"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['customer'] = $company_details;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['customer_js'] = $this->site->customers_js();
            $this->data["types_person"] = $this->site->get_types_person();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $this->data["types_obligations"] = $this->site->get_types_obligations();
            $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
            $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data["custom_fields"] = $this->site->get_custom_fields();
            $this->data["types_customer_obligations"] = $this->site->getTypesCustomerObligations($id, ACQUIRER);
            $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
            $this->data['main_address'] = $this->companies_model->get_company_principal_address($id);
            $rete_data = $this->sales_model->getWithHolding(NULL, 'S');
            $retentions = [];
            if ($rete_data) {
                foreach ($rete_data as $row) {
                    $retentions[$row['type']][] = $row;
                }
            }
            $this->data['retentions'] = $retentions;

            $this->load_view($this->theme . 'customers/edit', $this->data);
        }
    }

    public function users($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['users'] = $this->companies_model->getCompanyUsers($company_id);
        $this->load_view($this->theme . 'customers/users', $this->data);
    }

    public function add_user($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('email', lang("email_address"), 'is_unique[users.email]');
        $this->form_validation->set_rules('password', lang('password'), 'required|min_length[8]|max_length[20]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', lang('confirm_password'), 'required');

        if ($this->form_validation->run('companies/add_user') == true) {
            $active = $this->input->post('status');
            $notify = $this->input->post('notify');
            list($username, $domain) = explode("@", $this->input->post('email'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'company_id' => $company->id,
                'company' => $company->company,
                'group_id' => 3
            );
            $this->load->library('ion_auth');
        } elseif ($this->input->post('add_user')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, $active, $notify)) {
            $this->session->set_flashdata('message', lang("user_added"));
            admin_redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load_view($this->theme . 'customers/add_user', $this->data);
        }
    }

    public function import_csv()
    {
        $this->sma->checkPermissions('add', true);
        $this->load->helper('security');
        $this->form_validation->set_rules('csv_file', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('warning', lang("disabled_in_demo"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if (isset($_FILES["csv_file"])) /* if($_FILES['userfile']['size'] > 0) */ {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/csv/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '2000';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('csv_file')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("customers");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen("assets/uploads/csv/" . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5001, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('company', 'type_person', 'name', 'email', 'phone', 'country', 'state',  'city', 'postal_code', 'address', 'tipo_documento', 'vat_no', 'digito_verificacion', 'customer_group_id',  'cf1', 'cf2', 'cf3', 'cf4', 'cf5', 'cf6');

                $final = array();
                foreach ($arrResult as $key => $value) {
                    $value = $this->site->cleanRowCsvImport($value);
                    if (!$final[] = array_combine($keys, $value)) { //VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key+1)));
                        admin_redirect("customers");
                        break;
                    }
                }
                $rw = 2;
                foreach ($final as $csv) {
                    if ($this->companies_model->getCompanyByEmail($csv['email'])) {
                        $this->session->set_flashdata('error', lang("check_customer_email") . " (" . $csv['email'] . "). " . lang("customer_already_exist") . " (" . lang("line_no") . " " . $rw . ")");
                        admin_redirect("customers");
                    }
                    $rw++;
                }
                foreach ($final as $record) {

                    $record['first_name'] = "";
                    $record['second_name'] = "";
                    $record['first_lastname'] = "";
                    $record['second_lastname'] = "";

                    if ($record['type_person'] == 1) {
                        $namePerson = explode(" ", $record['name']);
                        // exit(var_dump($namePerson));
                        if (count($namePerson) == 2) {
                          $record['first_name'] = $namePerson[0];
                          $record['first_lastname'] = $namePerson[1];
                        } else if (count($namePerson) == 3) {
                          $record['first_name'] = $namePerson[0];
                          $record['first_lastname'] = $namePerson[1];
                          $record['second_lastname'] = $namePerson[2];
                        } else if (count($namePerson) == 4) {
                          $record['first_name'] = $namePerson[0];
                          $record['second_name'] = $namePerson[1];
                          $record['first_lastname'] = $namePerson[2];
                          $record['second_lastname'] = $namePerson[3];
                        }
                    }

                    $record['group_id'] = $this->companies_model->getCompanyGroupIdByName('customer');
                    $record['group_name'] = 'customer';
                    $record['customer_group_id'] = 1;
                    $record['customer_group_name'] = 'General';

                    $record['country'] = mb_strtoupper($record['country']);
                    $record['state'] = mb_strtoupper($record['state']);
                    $record['city'] = mb_strtoupper($record['city']);
                    $record['created_by'] = $this->session->userdata('user_id');

                    foreach ($record as $row => $value) {
                        $record[$row] = trim($value, " ");
                    }

                    $data[] = $record;
                }
                // exit(var_dump($data));
                // $this->sma->print_arrays($data);
            }

        } elseif ($this->input->post('import')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            if ($this->companies_model->addCompanies($data)) {
                $this->session->set_flashdata('message', lang("customers_added"));
                admin_redirect('customers');
            }
        } else {
            $this->data['documentTypes'] = $this->companies_model->getIDDocumentTypes();
            $this->data['customerGroups'] = $this->companies_model->getAllCustomerGroups();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'customers/import', $this->data);
        }
    }

    public function deactivate($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($this->input->get('id') == 1) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("customer_x_deactivated")));
        }
        if ($this->companies_model->deactivate_customers($id, 0)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("customer_deactivated")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("error")));
        }
    }
    public function activate($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($this->input->get('id') == 1) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("customer_x_activated")));
        }
        if ($this->companies_model->deactivate_customers($id, 1)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("customer_activated")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("error")));
        }
    }

    public function delete_customer($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($this->input->get('id') == 1) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("customer_x_deleted")));
        }
        if ($this->companies_model->delete_customer($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("customer_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("customer_x_deleted_have_sales")));
        }
    }

    public function suggestions($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $result = $this->companies_model->getCustomerSuggestions($term, $limit);
        // $result = ['id' =>'', 'phone'=>'', 'text'=>$this->lang->line('alls'), 'value'=>$this->lang->line('alls')];
        if ($a) {
            $this->sma->send_json($result);
        }
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function suggestionsws($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $result = $this->companies_model->getCustomerSuggestionsWithVatNo($term, $limit);
        if ($a) {
            $this->sma->send_json($result);
        }
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function getCustomer($id = NULL, $withvatno = NULL)
    {
        $row = $this->companies_model->getCompanyByID($id);
        $name = $row->vat_no." - ".$row->company;
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $name, 'value' => $name)));
    }

    public function get_customer_details($id = NULL)
    {
        $this->sma->send_json($this->companies_model->getCompanyByID($id));
    }

    public function get_customer_branch_details($id = NULL)
    {
        $this->sma->send_json($this->companies_model->getCompanyAddressById($id));
    }

    public function get_award_points($id = NULL)
    {
        $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array('ca_points' => $row->award_points));
    }

    public function customer_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->input->post('form_action') == 'export_all_to_excel') {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('status'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('document_type'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('vat_no'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('digito_verificacion'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('type_person'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('tipo_regimen'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('customer_name')); //name
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('first_name'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('label_second_name'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('label_first_lastname'));
                $this->excel->getActiveSheet()->SetCellValue('K1', lang('label_second_lastname'));
                $this->excel->getActiveSheet()->SetCellValue('L1', lang('label_tradename')); //company
                $this->excel->getActiveSheet()->SetCellValue('M1', lang('address'));
                $this->excel->getActiveSheet()->SetCellValue('N1', lang('zone'));
                $this->excel->getActiveSheet()->SetCellValue('O1', lang('subzone'));
                $this->excel->getActiveSheet()->SetCellValue('P1', lang('city'));
                $this->excel->getActiveSheet()->SetCellValue('Q1', lang('state'));
                $this->excel->getActiveSheet()->SetCellValue('R1', lang('country'));
                $this->excel->getActiveSheet()->SetCellValue('S1', lang('phone'));
                $this->excel->getActiveSheet()->SetCellValue('T1', lang('email'));
                $this->excel->getActiveSheet()->SetCellValue('U1', lang('birth_month'));
                $this->excel->getActiveSheet()->SetCellValue('V1', lang('birth_day'));
                $this->excel->getActiveSheet()->SetCellValue('W1', lang('gender'));
                $this->excel->getActiveSheet()->SetCellValue('X1', lang('price_group'));
                $this->excel->getActiveSheet()->SetCellValue('Y1', lang('customer_group'));
                $this->excel->getActiveSheet()->SetCellValue('Z1', lang('customer_seller_id_assigned'));
                $this->excel->getActiveSheet()->SetCellValue('AA1', lang('partner'));
                $this->excel->getActiveSheet()->SetCellValue('AB1', lang('customer_payment_type'));
                $this->excel->getActiveSheet()->SetCellValue('AC1', lang('customer_credit_limit'));
                $this->excel->getActiveSheet()->SetCellValue('AD1', lang('customer_payment_term'));
                $this->excel->getActiveSheet()->SetCellValue('AE1', lang('award_points_no_management'));
                $this->excel->getActiveSheet()->SetCellValue('AF1', lang('award_points'));
                $this->excel->getActiveSheet()->SetCellValue('AG1', lang('deposit_amount'));
                $this->excel->getActiveSheet()->SetCellValue('AH1', lang('customer_only_for_pos'));
                $this->excel->getActiveSheet()->SetCellValue('AI1', lang('tax_exempt_customer'));
                $this->excel->getActiveSheet()->SetCellValue('AJ1', lang('customer_validate_min_base_retention'));
                $this->excel->getActiveSheet()->SetCellValue('AK1', lang('fuente_retainer'));
                $this->excel->getActiveSheet()->SetCellValue('AL1', lang('iva_retainer'));
                $this->excel->getActiveSheet()->SetCellValue('AM1', lang('ica_retainer'));
                $letter = "AN";
                $custom_fields = $this->site->get_custom_fields();
                if ($custom_fields) {
                    foreach ($custom_fields as $cf_id => $cf_data) {
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', $cf_data['data']->cf_name);
                        $letter++;
                    }
                }
                $row = 2;
                $customers = $this->companies_model->get_all_customers();
                foreach ($customers as $customer) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, ($customer->status == 1 ? lang('active') : lang('inactive')));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->tipo_documento);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->vat_no);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->digito_verificacion);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->tipo_persona);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->tipo_regimen);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->name);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->first_name);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->second_name);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->first_lastname);
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->second_lastname);
                    $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->company);
                    $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->address);
                    $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->location);
                    $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->subzone);
                    $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->city);
                    $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $customer->state);
                    $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer->country);
                    $this->excel->getActiveSheet()->SetCellValue('S' . $row, $customer->phone);
                    $this->excel->getActiveSheet()->SetCellValue('T' . $row, $customer->email);
                    $this->excel->getActiveSheet()->SetCellValue('U' . $row, isset($this->meses[$customer->birth_month]) ? $this->meses[$customer->birth_month] : ' ');
                    $this->excel->getActiveSheet()->SetCellValue('V' . $row, $customer->birth_day);
                    $this->excel->getActiveSheet()->SetCellValue('W' . $row, ($customer->gender == 1 ? 'Masculino' : 'Femenino'));
                    $this->excel->getActiveSheet()->SetCellValue('X' . $row, $customer->price_group_name);
                    $this->excel->getActiveSheet()->SetCellValue('Y' . $row, $customer->customer_group_name);
                    $this->excel->getActiveSheet()->SetCellValue('Z' . $row, $customer->seller);
                    $this->excel->getActiveSheet()->SetCellValue('AA' . $row, ' - ');
                    $this->excel->getActiveSheet()->SetCellValue('AB' . $row, ($customer->customer_payment_type == 1 ? 'Contado' : 'Crédito'));
                    $this->excel->getActiveSheet()->SetCellValue('AC' . $row, $customer->customer_credit_limit);
                    $this->excel->getActiveSheet()->SetCellValue('AD' . $row, $customer->customer_payment_term);
                    $this->excel->getActiveSheet()->SetCellValue('AE' . $row, ($customer->award_points_no_management == 1 ? lang('no_acumulate') : lang('yes_acumulate')));
                    $this->excel->getActiveSheet()->SetCellValue('AF' . $row, $customer->award_points);
                    $this->excel->getActiveSheet()->SetCellValue('AG' . $row, $customer->deposit_amount);
                    $this->excel->getActiveSheet()->SetCellValue('AH' . $row, ($customer->customer_only_for_pos == 1 ? lang('yes') : lang('no')));
                    $this->excel->getActiveSheet()->SetCellValue('AI' . $row, ($customer->tax_exempt_customer == 1 ? lang('yes') : lang('no')));
                    $this->excel->getActiveSheet()->SetCellValue('AJ' . $row, ($customer->customer_validate_min_base_retention == 1 ? lang('yes') : lang('no')));
                    $this->excel->getActiveSheet()->SetCellValue('AK' . $row, ($customer->default_rete_fuente_id != 0 ? lang('yes') : lang('no')));
                    $this->excel->getActiveSheet()->SetCellValue('AL' . $row, ($customer->default_rete_iva_id != 0 ? lang('yes') : lang('no')));
                    $this->excel->getActiveSheet()->SetCellValue('AM' . $row, ($customer->default_rete_ica_id != 0 ? lang('yes') : lang('no')));
                    $letter = "AM";
                    if ($custom_fields) {
                        foreach ($custom_fields as $cf_id => $cf_data) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.$row, $customer->{$cf_data['data']->cf_code});
                            $letter++;
                        }
                    }
                    $row++;
                }
                for ($col = 'A'; $col != 'AN'; $col++) {
                    $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                }
                $this->excel->getActiveSheet()->getStyle('O1:O'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'customers_' . date('Y_m_d_H_i_s');
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            } else {
                if (!empty($_POST['val'])) {
                    if ($this->input->post('form_action') == 'deactivate') {
                        $this->sma->checkPermissions('delete');
                        $error = false;
                        foreach ($_POST['val'] as $id) {
                            if (!$this->companies_model->deactivate_customers($id, 0)) {
                                $error = true;
                            }
                        }
                        if ($error) {
                            $this->session->set_flashdata('warning', lang('customers_x_deleted_have_sales'));
                        } else {
                            $this->session->set_flashdata('message', lang("customers_deactivated"));
                        }
                        redirect($_SERVER["HTTP_REFERER"]);
                    }

                    if ($this->input->post('form_action') == 'export_excel') {
                        $this->load->library('excel');
                        $this->excel->setActiveSheetIndex(0);
                        $this->excel->getActiveSheet()->setTitle(lang('customer'));
                        $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                        $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                        $this->excel->getActiveSheet()->SetCellValue('C1', lang('email'));
                        $this->excel->getActiveSheet()->SetCellValue('D1', lang('phone'));
                        $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));
                        $this->excel->getActiveSheet()->SetCellValue('F1', 'Zona/Barrio');
                        $this->excel->getActiveSheet()->SetCellValue('G1', lang('city'));
                        $this->excel->getActiveSheet()->SetCellValue('H1', lang('state'));
                        $this->excel->getActiveSheet()->SetCellValue('I1', lang('postal_code'));
                        $this->excel->getActiveSheet()->SetCellValue('J1', lang('country'));
                        $this->excel->getActiveSheet()->SetCellValue('K1', lang('vat_no'));
                        $this->excel->getActiveSheet()->SetCellValue('L1', lang('deposit_amount'));
                        $this->excel->getActiveSheet()->SetCellValue('M1', lang('seller'));
                        $this->excel->getActiveSheet()->SetCellValue('N1', 'Fecha primer compra');
                        $this->excel->getActiveSheet()->SetCellValue('O1', 'Fecha últ. compra');
                        $this->excel->getActiveSheet()->SetCellValue('P1', 'Valor últ. compra');
                        $this->excel->getActiveSheet()->SetCellValue('Q1', 'Total de compras');
                        $this->excel->getActiveSheet()->SetCellValue('R1', 'Grupo clientes');
                        $letter = "L";
                        $custom_fields = $this->site->get_custom_fields();
                        if ($custom_fields) {
                            foreach ($custom_fields as $cf_id => $cf_data) {
                                $this->excel->getActiveSheet()->SetCellValue($letter.'1', $cf_data['data']->cf_name);
                                $letter++;
                            }
                        }
                        $row = 2;
                        foreach ($_POST['val'] as $id) {
                            $customer = $this->companies_model->getCompanyByID($id);
                            $customer_add = $this->companies_model->get_customer_xls_additional_data($id);
                            $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->company);
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->name);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->email);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->phone);
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->address);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->zone_name." ".$customer->subzone_name);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->city);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->state);
                            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->postal_code);
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->country);
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->vat_no);
                            $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->deposit_amount);
                            $cseller = $this->site->getCompanyByID($customer->customer_seller_id_assigned);
                            $this->excel->getActiveSheet()->SetCellValue('M' . $row, ($cseller ? $cseller->name : ''));
                            $this->excel->getActiveSheet()->SetCellValue('N' . $row, ($customer_add ? date('Y-m-d', strtotime($customer_add->first_sale_date)) : ''));
                            $this->excel->getActiveSheet()->SetCellValue('O' . $row, ($customer_add ? date('Y-m-d', strtotime($customer_add->date)) : ''));
                            $this->excel->getActiveSheet()->SetCellValue('P' . $row, ($customer_add ? $customer_add->grand_total : ''));
                            $this->excel->getActiveSheet()->SetCellValue('Q' . $row, ($customer_add ? $customer_add->num_sales : ''));
                            $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer_group->name);
                            $letter = "Q";
                            if ($custom_fields) {
                                foreach ($custom_fields as $cf_id => $cf_data) {
                                    $this->excel->getActiveSheet()->SetCellValue($letter.$row, $customer->{$cf_data['data']->cf_code});
                                    $letter++;
                                }
                            }
                            $row++;
                        }
                        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                        $this->excel->getActiveSheet()->getStyle('O1:O'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $filename = 'customers_' . date('Y_m_d_H_i_s');
                        $this->load->helper('excel');
                        create_excel($this->excel, $filename);
                    }

                    if ($this->input->post('form_action') == 'sync_award_points') {
                        $error = false;
                        foreach ($_POST['val'] as $id) {
                            if (!$this->companies_model->sync_customer_award_points($id)) {
                                $error = true;
                            }
                        }
                        if ($error) {
                            $this->session->set_flashdata('warning', lang('synchronized_award_points'));
                        } else {
                            $this->session->set_flashdata('message', lang("synchronized_award_points"));
                        }
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $this->session->set_flashdata('error', lang("no_customer_selected"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function deposits($company_id = NULL)
    {
        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->load_view($this->theme . 'customers/deposits', $this->data);
    }

    public function get_deposits($company_id = NULL)
    {

        if ($this->input->post('start_date')) {
             $start_date = $this->input->post('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        $document_type_id = NULL;
        if ($this->input->post('cdeppayment_reference_no')) {
            $document_type_id = $this->input->post('cdeppayment_reference_no');
        }
        $payment_method = NULL;
        if ($this->input->post('cdeppayment_method')) {
            $payment_method = $this->input->post('cdeppayment_method');
        }
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $customer_id = $this->input->post('customer') ? $this->input->post('customer') : NULL;

        $contabilizar_venta = '<a class="reaccount_deposit_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> '.lang('post_deposit').' </a>';
        $view_deposit = "<a class=\"tip\" href='" . admin_url('customers/deposit_note/$1') . "' data-toggle='modal' data-target='#myModal2'><i class=\"fa fa-file-text-o\"></i>" . lang("deposit_note") . "</a>";
        $cancel_deposit = "<a class=\"tip\" href='" . admin_url('customers/cancel_deposit/$1') . "' data-toggle='modal' data-target='#myModal2'><i class=\"fa fa-trash\"></i>" . lang("cancel_deposit") . "</a>";


        $action = '<div class="text-center"><div class="btn-group text-left">
                        <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                        <i class="fas fa-ellipsis-v fa-lg"></i></button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>' . $contabilizar_venta . '</li>
                                <li>' . $view_deposit . '</li>
                                <li>' . $cancel_deposit . '</li>
                            </ul>
                        </div>
                    </div>';
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        deposits.id as id,
                        reference_no,
                        date,
                        amount,
                        (amount - balance) as applied_amount,
                        balance,
                        paid_by,
                        IF(origen = 1, 'Manual', 'Automático') as origen,
                        IF(origen_reference_no IS NOT NULL, origen_reference_no, 'Manual'),
                        CONCAT({$this->db->dbprefix('users')}.first_name,
                        ' ',
                        {$this->db->dbprefix('users')}.last_name) as created_by,
                        companies.name
                    ", false)
            ->from("deposits")
            ->join('users', 'users.id=deposits.created_by', 'left')
            ->join('companies', 'companies.id=deposits.company_id', 'left');

        if ($company_id != NULL) {
            $this->datatables->where($this->db->dbprefix('deposits').'.company_id', $company_id);
        }
        $this->datatables->where($this->db->dbprefix('companies').'.group_name', 'customer');

        /* FILTROS */

        if ($start_date) {
            $this->datatables->where('deposits.date >= ', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('deposits.date <= ', $end_date);
        }
        if ($document_type_id) {
            $this->datatables->where('deposits.document_type_id', $document_type_id);
        }
        if ($user_id) {
            $this->datatables->where('deposits.created_by', $user_id);
        }
        if ($payment_method) {
            $this->datatables->where('deposits.paid_by', $payment_method);
        }
        if ($biller_id) {
            $this->datatables->where('deposits.biller_id', $biller_id);
        }
        if ($customer_id) {
            $this->datatables->where('deposits.company_id', $customer_id);
        }
        /* FILTROS */
        $this->datatables->order_by('deposits.date desc');
        $this->datatables
            ->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function add_deposit($company_id = NULL)
    {
        $this->sma->checkPermissions();

        if (!$this->Owner && !$this->Admin) {
            if ($this->session->userdata('register_cash_movements_with_another_user')) {
                if (!$this->pos_model->registerData($this->session->userdata('register_cash_movements_with_another_user'))) {
                    $this->session->set_flashdata('error', lang('another_user_cash_not_open'));
                    admin_redirect('welcome');
                }
            } else {
                if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                    $this->session->set_flashdata('error', lang('register_not_open'));
                    admin_redirect('pos/open_register');
                }
            }
        }

        if (!$company_id) {
            $company_id = $this->input->post('customer');
        }

        $company = $this->companies_model->getCompanyByID($company_id);

        if ($this->Owner || $this->Admin) {
            $this->form_validation->set_rules('date', lang("date"), 'required');
        }
        $this->form_validation->set_rules('amount', lang("amount"), 'required|numeric');

        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }

        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $biller_id = $this->input->post('biller');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect('purchases/add');
            }
            $data = array(
                'date' => $date,
                'amount' => $this->input->post('amount'),
                'balance' => $this->input->post('amount'),
                'paid_by' => $this->input->post('paid_by'),
                'note' => $this->input->post('note'),
                'company_id' => $company->id,
                'created_by' => $this->session->userdata('user_id'),
                'biller_id' => $biller_id,
                'document_type_id' => $this->input->post('document_type_id'),
            );
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }
            $cdata = array(
                'deposit_amount' => ($company->deposit_amount+$this->input->post('amount'))
            );
        } elseif ($this->input->post('add_deposit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $id_deposit = $this->companies_model->addDeposit($data, $cdata)) {
            $this->session->set_flashdata('message', lang("deposit_added"));
            admin_redirect('customers/deposit_note/'.$id_deposit.'/1');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->load_view($this->theme . 'customers/add_deposit', $this->data);
        }
    }

    public function edit_deposit($id = NULL)
    {
        $this->sma->checkPermissions('deposits', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $deposit = $this->companies_model->getDepositByID($id);
        $company = $this->companies_model->getCompanyByID($deposit->company_id);

        if ($this->Owner || $this->Admin) {
            $this->form_validation->set_rules('date', lang("date"), 'required');
        }
        $this->form_validation->set_rules('amount', lang("amount"), 'required|numeric');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $deposit->date;
            }
            $data = array(
                'date' => $date,
                'amount' => $this->input->post('amount'),
                'paid_by' => $this->input->post('paid_by'),
                'note' => $this->input->post('note'),
                'company_id' => $deposit->company_id,
                'updated_by' => $this->session->userdata('user_id'),
                'updated_at' => $date = date('Y-m-d H:i:s'),
            );

            $cdata = array(
                'deposit_amount' => (($company->deposit_amount-$deposit->amount)+$this->input->post('amount'))
            );

        } elseif ($this->input->post('edit_deposit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateDeposit($id, $data, $cdata)) {
            $this->session->set_flashdata('message', lang("deposit_updated"));
            admin_redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->data['deposit'] = $deposit;
            $this->load_view($this->theme . 'customers/edit_deposit', $this->data);
        }
    }

    public function delete_deposit($id)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->companies_model->deleteDeposit($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("deposit_deleted")));
        }
    }

    public function deposit_note($id = null, $pdf = null)
    {
        $deposit = $this->companies_model->getDepositByID($id);
        $this->data['customer'] = $this->companies_model->getCompanyByID($deposit->company_id);
        $this->data['deposit'] = $deposit;
        $this->data['page_title'] = $this->lang->line("deposit_note");
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $deposit->biller_id);
        $this->data['payments'] = $this->site->get_payments_affects_deposit_sales($id);
        // exit(var_dump($this->data['payments']));
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $this->data['paid_by'] = lang($deposit->paid_by);
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($deposit->cost_center_id);
        }
        $this->data['modal'] = true;
        if ($pdf) {
            $this->data['modal'] = false;
            $this->data['document_type'] = $this->site->getDocumentTypeById($deposit->document_type_id);
            $document_type_invoice_format = $this->site->getInvoiceFormatById($this->data['document_type']->module_invoice_format_id);
            $url = 'customers/deposit_note_view';
            if ($document_type_invoice_format) {
                $url = $document_type_invoice_format->format_url;
            }
            $this->load_view($this->theme .$url, $this->data);
        } else {
            $this->load_view($this->theme . 'customers/deposit_note', $this->data);
        }
    }

    public function addresses($company_id = NULL)
    {
        $this->sma->checkPermissions('index', true);
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['addresses'] = $this->companies_model->getCompanyAddresses($company_id);
        $this->load_view($this->theme . 'customers/addresses', $this->data);
    }

    public function add_address($company_id = NULL)
    {
        $this->sma->checkPermissions('add', true);
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('direccion', lang("address"), 'required');
        $this->form_validation->set_rules('sucursal', lang("branch"), 'required');
        $this->form_validation->set_rules('city', lang("city"), 'required');
        $this->form_validation->set_rules('state', lang("state"), 'required');
        $this->form_validation->set_rules('country', lang("country"), 'required');
        $this->form_validation->set_rules('phone', lang("phone"), 'required');


        if ($this->form_validation->run() == true) {

            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_group'));
            $pg = $this->site->getPriceGroupByID($this->input->post('price_group'));
            $data = array(
                'code' => $this->input->post('code'),
                'direccion' => $this->input->post('direccion'),
                'sucursal' => $this->input->post('sucursal'),
                'city' => $this->input->post('city'),
                'postal_code' => $this->input->post('postal_code'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'location' => $this->input->post('zone'),
                'subzone' => $this->input->post('subzone'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'company_id' => $company->id,
                'customer_address_seller_id_assigned' => $this->input->post('customer_address_seller_id_assigned'),
                'city_code' => $this->input->post('city_code'),
                'price_group_id' => $this->input->post('price_group'),
                'price_group_name' => $this->input->post('price_group') ? $pg->name : NULL,
                'customer_group_id' => $this->input->post('customer_group'),
                'customer_group_name' => $this->input->post('customer_group') ? $cg->name : NULL,
                'seller_sale_comision' => $this->input->post('seller_sale_comision'),
                'seller_collection_comision' => $this->input->post('seller_collection_comision'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'vat_no' => $this->input->post('vat_no'),
            );

        } elseif ($this->input->post('add_address')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $saved_address_id = $this->companies_model->addAddress($data)) {
            if ($this->Settings->export_to_csv_each_new_customer == 1) {
                $this->new_customer_export_csv($company_id, false);
                $this->new_customer_address_export_csv($company_id, $saved_address_id);
            }
            $this->session->set_flashdata('message', lang("address_added"));
            admin_redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
            $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
            $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();
            $addresses = $this->companies_model->getCompanyAddresses($company_id);
            $addr_count = $addresses ? count($addresses) : 0;
            $this->data['code'] = $company->vat_no."-".($addr_count+1);
            $this->load_view($this->theme . 'customers/add_address', $this->data);
        }
    }

    public function edit_address($id = NULL)
    {
        $this->sma->checkPermissions('edit', true);

        $this->form_validation->set_rules('direccion', lang("address"), 'required');
        $this->form_validation->set_rules('sucursal', lang("branch"), 'required');
        $this->form_validation->set_rules('city', lang("city"), 'required');
        $this->form_validation->set_rules('state', lang("state"), 'required');
        $this->form_validation->set_rules('country', lang("country"), 'required');
        $this->form_validation->set_rules('phone', lang("phone"), 'required');

        if ($this->form_validation->run() == true) {

            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_group'));
            $pg = $this->site->getPriceGroupByID($this->input->post('price_group'));
            $data = array(
                'code' => $this->input->post('code'),
                'direccion' => $this->input->post('direccion'),
                'sucursal' => $this->input->post('sucursal'),
                'city' => $this->input->post('city'),
                'vat_no' => $this->input->post('vat_no'),
                'postal_code' => $this->input->post('postal_code'),
                'city_code' => $this->input->post('city_code'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'customer_address_seller_id_assigned' => $this->input->post('customer_address_seller_id_assigned'),
                'price_group_id' => $this->input->post('price_group'),
                'price_group_name' => $this->input->post('price_group') ? $pg->name : NULL,
                'customer_group_id' => $this->input->post('customer_group'),
                'customer_group_name' => $this->input->post('customer_group') ? $cg->name : NULL,
                'seller_sale_comision' => $this->input->post('seller_sale_comision'),
                'seller_collection_comision' => $this->input->post('seller_collection_comision'),
                'location' => $this->input->post('zone'),
                'subzone' => $this->input->post('subzone'),
                'updated_at' => date('Y-m-d H:i:s'),
                'last_update' => date('Y-m-d H:i:s'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
            );

        } elseif ($this->input->post('edit_address')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateAddress($id, $data)) {
            $this->session->set_flashdata('message', lang("address_updated"));
            admin_redirect("customers");
        } else {

            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['address'] = $this->companies_model->getAddressByID($id);
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
            $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
            $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();
            $company = $this->companies_model->getCompanyByID($this->data['address']->company_id);
            $this->data['company'] = $company;
            $this->load_view($this->theme . 'customers/edit_address', $this->data);
        }
    }

    public function delete_address($id)
    {
        $this->sma->checkPermissions('delete', TRUE);

        if ($this->companies_model->deleteAddress($id)) {
            $this->session->set_flashdata('message', lang("address_deleted"));
            admin_redirect("customers");
        }
    }

    public function list_deposits()
    {
        $this->sma->checkPermissions();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('customers')));
        $meta = array('page_title' => lang('list_deposits'), 'bc' => $bc);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([30]);
        $this->data['users'] = $this->site->get_all_users();

        $this->page_construct('customers/list_deposits', $meta, $this->data);
    }

    public function deposit_balance($customer_id, $amount = 0)
    {
        $customer = $this->site->getCompanyByID($customer_id);

        $balance = $customer ? $customer->deposit_amount : 0;

        if ($balance > $amount) {
            $type_msg = "primary";
        } else if ($balance < $amount) {
            $type_msg = "danger";
        } else {
            $type_msg = "default";
        }
        $amount_topay = $amount;
        $cdps = $this->companies_model->getDepositsWithBalance($customer_id, 20);

        $tabla = "<table class='table'>";
        $tabla .= "<tr>
                        <th class='text-center'> Número </th>
                        <th class='text-center'> Origen </th>
                        <th class='text-center'> Fecha </th>
                        <th class='text-center'> Valor </th>
                        <th class='text-center'> Aplicado </th>
                        <th class='text-center'> Balance </th>
                        <th class='text-center'> Valor a aplicar </th>
                   </tr>";
        if ($cdps != FALSE) {
            foreach ($cdps as $deposit) {

                if ($amount_topay == 0) {
                    break;
                }
                $amount_to_apply = 0;
                if ($deposit->balance < $amount_topay) {
                    $amount_topay -= $deposit->balance;
                    $amount_to_apply = $deposit->balance;
                } else if ($deposit->balance >= $amount_topay) {
                    $amount_to_apply = $amount_topay;
                    $amount_topay -= $amount_topay;
                }

                $tabla .= "<tr>
                                <td class='text-center'> ".$deposit->reference_no." </td>
                                <td class='text-center'> ".$deposit->origen_reference_no." </td>
                                <td class='text-center'> ".$deposit->date." </td>
                                <td class='text-center'> ".$this->sma->formatMoney($deposit->amount)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($deposit->amount - $deposit->balance)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($deposit->balance)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($amount_to_apply)." </td>
                           </tr>";
            }
        } else {
            $tabla .= "<tr>
                            <th colspan='6' class='text-center'> ".lang('customer_without_balance')." </th>
                       </tr>";
        }

        $tabla .= "<tr>
                        <th colspan='5' class='text-right'> <span class='fa fa-exclamation-circle text-".$type_msg."'></span> <b>".lang('customer_deposit_balance')."</b> </th>
                        <th class='text-right' id='balance' balance='".$balance."' data-balance='".$balance."'> ".$this->sma->formatMoney($balance)." </th>
                   </tr>";

        $tabla .= "</table>";

        $msg_deposit =  '<div class="panel panel-'.$type_msg.' deposit_message" style="border-radius:5px;">
          <div class="panel-body text-center" >
            </hr>
            '.$tabla.'
          </div>
        </div>';

        echo $msg_deposit;
    }

    public function alta_cliente($customer_id = NULL)
    {
        $datos_cliente = $this->companies_model->getCompanyByID($customer_id);

        $nombre = ($datos_cliente->type_person == 1) ? $datos_cliente->first_name . (! empty($datos_cliente->second_name) ? " ".$datos_cliente->second_name : "") : $datos_cliente->company;
        $apellido = ($datos_cliente->type_person == 1) ? $datos_cliente->first_lastname . (! empty($datos_cliente->second_lastname) ? " ".$datos_cliente->second_lastname : "") : "";

        $arreglo_datos_cliente = [
            "companyID"=>"1",
            "acountID"=>"1",
            "correo"=>"1",
            "password"=>"1",
            "DatosGenerales"=>[
                "Nombre"=>$nombre,
                "Apellidos"=>$apellido,
                "Email"=>$datos_cliente->email,
                "Telefono"=>$datos_cliente->phone,
            ],
            "DatosFacturacion"=>[
                "TipoPersona"=>$datos_cliente->type_person,
                "RazonSocial"=>$datos_cliente->name,
                "NIT"=>$datos_cliente->vat_no,
                "NombreSucursal"=>"1",
                "DigitoVerificador"=>"1",
                "Direccion"=>"1",
                "Barrio"=>"1",
                "Ciudad"=>"1",
                "Departamento"=>"1",
                "ComprobantesEmitirPorMes"=>"1",
            ]
        ];

        echo "<pre>";
        print_r($datos_cliente);
    }

    public function customers_birthdays($today = false)
    {
        $this->sma->checkPermissions('index');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['today'] = $today;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('customers_birthdays')));
        $meta = array('page_title' => lang('customers_birthdays'), 'bc' => $bc);
        $this->page_construct('customers/customers_birthdays', $meta, $this->data);
    }

    public function get_customer_addresses($customer_id)
    {

        $addresses = $this->companies_model->getCompanyAddresses($customer_id);
        // $option = '<option value="">'.lang('select').'</option>';
        $option = '';
        if ($addresses) {
            foreach ($addresses as $address) {
                $option .= '<option value="'.$address->id.'">'.$address->sucursal.' ('.$address->direccion.', '.$address->city.') '.'</option>';
            }
            echo $option;
        }
    }

    public function validate_vat_no($vat_no)
    {
        $validate = $this->companies_model->validate_vat_no($vat_no, 'customer');
        if ($validate) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function get_payment_type($customer_id)
    {
        $customer = $this->companies_model->getCompanyByID($customer_id);
        $day_delivery_time = $this->input->get('day_delivery_time');
        if ($day_delivery_time) {
            $q = $this->db->select('SUM(grand_total) AS total_grand_total')
            ->where(['customer_id'=>$customer_id, 'date >='=>($day_delivery_time ? $day_delivery_time : date('Y-m-d'))." 00:00:00", 'date <='=>($day_delivery_time ? $day_delivery_time : date('Y-m-d'))." 23:59:59"])
            ->get('order_sales');
            if ($q->num_rows() > 0) {
                $q = $q->row();
                $data['amount_ordered'] = $q->total_grand_total;
            } else{
                $data['amount_ordered'] = 0;
            }
        }
        $due_balance = 0;
        if ($this->Settings->control_customer_credit == 1) {
            $due_q = $this->db->select(' SUM(COALESCE(grand_total, 0) - COALESCE(paid, 0)) AS due_amount')
                    ->where('customer_id', $customer_id)->get('sales');
            if ($due_q->num_rows() > 0) {
                $due_q = $due_q->row();
                $due_balance = $due_q->due_amount;
            }
        }
            

        $due_term_expired = 0;
        if ($this->Settings->control_customer_credit == 1) {
            $due_q = $this->db
                    ->where('(DATEDIFF(NOW(), date)) > '.$customer->customer_payment_term)
                    ->where('((COALESCE(grand_total, 0) - COALESCE(paid, 0))) > 0')
                    ->where('customer_id', $customer_id)
                    ->get('sales');
            if ($due_q->num_rows() > 0) {
                $due_term_expired = 1;
            }
        }

        $data['payment_type'] = $customer->customer_payment_type;
        $data['payment_term'] = $customer->customer_payment_term;
        $data['term_expired'] = $due_term_expired;
        $data['credit_limit'] = $customer->customer_credit_limit - $due_balance;

        echo json_encode($data);
    }

    public function get_customer_special_discount($customer_id)
    {
        $customer = $this->companies_model->getCompanyByID($customer_id);
        $giftcard_balance = $this->companies_model->get_customer_giftcard_balance($customer_id);
        $portfolio = $this->companies_model->get_portfolio($customer->vat_no, 'customer_cxp');
        $data['special_discount'] = (String) $customer->customer_special_discount;
        $data['deposit_amount'] = $customer->deposit_amount;
        $data['cxp_portfolio'] = $portfolio;
        $data['giftcard_balance'] = $giftcard_balance;
        $data['tax_exempt_customer'] = $customer->tax_exempt_customer;
        if ($giftcard_balance > 0) {
            $data['gift_cards'] = $this->companies_model->get_customer_gift_cards_with_balance($customer_id);
        }
        echo json_encode($data);
    }

    public function get_customer_by_id($id, $biller_id = NULL)
    {
        $customer = $this->companies_model->getCompanyByID($id);
        $portfolio = $this->companies_model->get_portfolio($id, 'customer', $biller_id);
        $customer->apply_birthday_discount = false;
        if ($this->Settings->days_count_birthday_discount_application > 0 
            &&
            ($customer->birthday_year_applied == "" || $customer->birthday_year_applied < date('Y'))) {
            $birthday_year_applied = date('Y');
            $today = new DateTime();
            $birthdate = new DateTime(date('Y') . "-{$customer->birth_month}-{$customer->birth_day}");
            $applyDiscount = false;
            $discountDays = $this->Settings->days_count_birthday_discount;
            $customer->days_until_birthday = (int)$today->diff($birthdate)->days;
            if ($this->Settings->days_count_birthday_discount_application > 0) {
                switch ($this->Settings->days_count_birthday_discount_application) {
                    case 1:
                        $applyDiscount = ($customer->days_until_birthday < 0 && abs($customer->days_until_birthday) <= $discountDays);
                        break;
                    case 2:
                        $applyDiscount = ($customer->days_until_birthday > 0 && abs($customer->days_until_birthday) <= $discountDays);
                        break;
                    case 3:
                        $applyDiscount = (abs($customer->days_until_birthday) <= $discountDays);
                        break;
                    case 4:
                        $applyDiscount = ($customer->days_until_birthday == 0);
                        break;
                }
            }
            if (!$applyDiscount && $birthdate < $today) {
                $birthdate->modify('+1 year');
                $customer->days_until_birthday = (int)$today->diff($birthdate)->days;
                $birthday_year_applied = date('Y') + 1;
                if ($this->Settings->days_count_birthday_discount_application > 0) {
                    switch ($this->Settings->days_count_birthday_discount_application) {
                        case 1:
                            $applyDiscount = ($customer->days_until_birthday < 0 && abs($customer->days_until_birthday) <= $discountDays);
                            break;
                        case 2:
                            $applyDiscount = ($customer->days_until_birthday > 0 && abs($customer->days_until_birthday) <= $discountDays);
                            break;
                        case 3:
                            $applyDiscount = (abs($customer->days_until_birthday) <= $discountDays);
                            break;
                        case 4:
                            $applyDiscount = ($customer->days_until_birthday == 0);
                            break;
                    }
                }
            }
            $customer->apply_birthday_discount = $applyDiscount;
            $customer->birthday_year_applied = $birthday_year_applied;
            if ($applyDiscount) {
                // $this->db->update('companies', ['birthday_year_applied' => $birthday_year_applied], ['id'=>$id]);
            }
        }
        if ($customer) {
            $customer->portfolio = $portfolio->balance;
        }
        echo json_encode($customer);
    }

    public function new_customer_export_csv($id, $save_new_addres_too = true)
    {
        $root = dirname(dirname(dirname(__DIR__)))."/assets/wms/export/";
        $row = $this->companies_model->getCompanyByID($id);
        if($row){
            $delimiter = ";";
            $filename = $row->vat_no.".csv";
            $f = fopen($root.$filename, 'w');
            $lineData = array('ClienteCodigo','ClienteNombre','ClienteDireccion','ClienteCiudadId','ClienteTelefono');
            fputcsv($f, $lineData, $delimiter);
            $lineData = array($row->vat_no, $row->name, $row->address, $row->city_code, $row->phone);
            fputcsv($f, $lineData, $delimiter);
            $address = $this->companies_model->get_company_principal_address($row->id);
            if ($address && $save_new_addres_too) {
                $filename = $address->code.".csv";
                $f = fopen($root.$filename, 'w');
                $lineData = array('SucursalCodigo','SucursalNombre','ClienteCodigo','SucursalDireccion','SucursalCiudadId','SucursalTelefono');
                fputcsv($f, $lineData, $delimiter);
                $lineData = array($address->code, $address->sucursal, $row->vat_no, $address->direccion, $address->city_code, $address->phone);
                fputcsv($f, $lineData, $delimiter);
            }
        }
    }

    public function new_customer_address_export_csv($cid, $address_id)
    {
        $root = dirname(dirname(dirname(__DIR__)))."/assets/wms/export/";
        $delimiter = ";";
        $row = $this->companies_model->getCompanyByID($cid);
        $address = $this->companies_model->getAddressByID($address_id);
        if ($address && $row) {
            $filename = $address->code.".csv";
            $f = fopen($root.$filename, 'w');
            $lineData = array('SucursalCodigo','SucursalNombre','ClienteCodigo','SucursalDireccion','SucursalCiudadId','SucursalTelefono');
            fputcsv($f, $lineData, $delimiter);
            $lineData = array($address->code, $address->sucursal, $row->vat_no, $address->direccion, $address->city_code, $address->phone);
            fputcsv($f, $lineData, $delimiter);
        }
    }

    public function get_deposits_will_be_affected($customer_id, $amount = 0)
    {
        $customer = $this->site->getCompanyByID($customer_id);
        $balance = $customer->deposit_amount;
        $amount_topay = $amount;
        $cdps = $this->companies_model->getDepositsWithBalance($customer_id, 20);
        $num = 0;
        $error = 0;

        if ($balance < $amount) {
            $error = 1;
        }

        if ($cdps != FALSE) {
            foreach ($cdps as $deposit) {
                if ($amount_topay == 0) {
                    break;
                }
                $amount_to_apply = 0;
                if ($deposit->balance < $amount_topay) {
                    $amount_topay -= $deposit->balance;
                    $amount_to_apply = $deposit->balance;
                } else if ($deposit->balance >= $amount_topay) {
                    $amount_to_apply = $amount_topay;
                    $amount_topay -= $amount_topay;
                }
                $num++;
            }
        }
        echo json_encode(['num'=>$num,'error'=>$error]);
    }

    public function deposit_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');
        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'reaccount') {
                    // exit('AA');
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->recontabilizar_deposit($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('deposit_post_error'));
                    } else {
                        $this->session->set_flashdata('message', lang("deposit_posted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('deposits'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('total'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('amount_applied'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('balance'));

                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('type'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('origin'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('origin_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('created_by'));

                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('biller'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $qu = $this->companies_model->getDepositByID($id);
                        $customer = $this->site->getAllCompaniesWithState('customer', $qu->company_id);
                        $biller = $this->site->getAllCompaniesWithState('biller', $qu->biller_id);
                        $employee = $this->site->getUser($qu->created_by);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($qu->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $qu->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $qu->amount);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $qu->amount - $qu->balance);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $qu->balance);

                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($qu->paid_by));
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $qu->origen == 1 ? 'Manual' : 'Automática');
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $qu->origen_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $employee->first_name." ".$employee->last_name);


                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->company);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $biller->company);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $this->excel->getActiveSheet()->getStyle('C1:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $this->excel->getActiveSheet()->getStyle('D1:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $filename = 'deposits_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_deposit_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function contabilizarDeposito($deposit_id = null, $json = null)
    {
        $this->form_validation->set_rules('deposit_id', lang("deposit_id"), 'required');
        if ($this->form_validation->run() == true) {
            $deposit_id = $this->input->post('deposit_id');
            $msg = $this->companies_model->recontabilizar_deposit($deposit_id);
            $this->session->set_flashdata('message', $msg);
            admin_redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $deposits = $this->companies_model->getDepositByID($deposit_id, true);
            $exists = $this->site->getEntryTypeNumberExisting($deposits, false, false);
            $this->data['exists'] = $exists;
            $this->data['inv'] = $deposits;
            $this->load_view($this->theme . 'customers/recontabilizar_deposit', $this->data);
        }
    }

    public function sync_award_points($customer_id){
        $error = false;
        if (!$this->companies_model->sync_customer_award_points($customer_id)) {
            $error = true;
        }
        if ($error) {
            $this->session->set_flashdata('warning', lang('synchronized_award_points'));
        } else {
            $this->session->set_flashdata('message', lang("synchronized_award_points"));
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    public function suggestions_addresses($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $result = $this->companies_model->getCustomerSuggestionsAddresses($term, $limit);
        // $result = ['id' =>'', 'phone'=>'', 'text'=>$this->lang->line('alls'), 'value'=>$this->lang->line('alls')];
        if ($a) {
            $this->sma->send_json($result);
        }
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function cancel_deposit($id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin) {
            if ($this->session->userdata('register_cash_movements_with_another_user')) {
                if (!$this->pos_model->registerData($this->session->userdata('register_cash_movements_with_another_user'))) {
                    $this->session->set_flashdata('error', lang('another_user_cash_not_open'));
                    admin_redirect('welcome');
                }
            } else {
                if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                    $this->session->set_flashdata('error', lang('register_not_open'));
                    admin_redirect('pos/open_register');
                }
            }
        }
        if ($this->Owner || $this->Admin) {
            $this->form_validation->set_rules('date', lang("date"), 'required');
        }
        $this->form_validation->set_rules('amount', lang("amount"), 'required|numeric');
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if (!$id) {
            $id = $this->input->post('deposit_id');
        }
        $deposit = $this->companies_model->getDepositByID($id);
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $biller_id = $this->input->post('biller');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect('purchases/add');
            }
            $data = array(
                'date' => $date,
                'amount' => ($this->input->post('amount') * -1),
                'balance' => 0,
                'paid_by' => $this->input->post('paid_by'),
                'note' => $this->input->post('note'),
                'company_id' => $this->input->post('customer'),
                'created_by' => $this->session->userdata('user_id'),
                'biller_id' => $biller_id,
                'origen_reference_no' => $this->input->post('reference_no_origen'),
                'origen_document_type_id' => $this->input->post('origen_document_type_id'),
                'document_type_id' => $this->input->post('document_type_id'),
            );

            $company = $this->companies_model->getCompanyByID($this->input->post('customer'));
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }
            $cdata = array(
                'deposit_amount' => ($company->deposit_amount+($this->input->post('amount') * -1))
            );
            $ddata = array(
                'id' => $deposit->id,
                'amount' => ($deposit->balance+($this->input->post('amount') * -1))
            );
        } elseif ($this->input->post('add_deposit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $id_deposit = $this->companies_model->cancel_deposit($data, $cdata, 1, $ddata)) {
            $this->session->set_flashdata('message', lang("deposit_added"));
            admin_redirect('customers/deposit_note/'.$id_deposit.'/1');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['customer'] = $this->companies_model->getCompanyByID($deposit->company_id);
            $deposit = $this->companies_model->getDepositByID($id);
            $this->data['deposit'] = $deposit;
            $this->data['page_title'] = $this->lang->line("deposit_note");
            $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $deposit->biller_id);
            $this->data['payments'] = $this->site->get_payments_affects_deposit_sales($id);
            // exit(var_dump($this->data['payments']));
            $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
            $this->data['tipo_regimen'] = lang($tipo_regimen->description);
            $this->data['paid_by'] = lang($deposit->paid_by);
            if ($this->Settings->cost_center_selection != 2) {
                $this->data['cost_center'] = $this->site->getCostCenterByid($deposit->cost_center_id);
            }
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->load_view($this->theme . 'customers/cancel_deposit', $this->data);
        }
    }
}