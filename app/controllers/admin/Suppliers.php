<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliers extends MY_Controller
{
    public $typeSuppliers = [];
    public $typesWithholdings = [];
    public $supplierPaymentsTypes = [];

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->load->admin_model('pos_model');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model('products_model');

        $this->load->library('form_validation');
        $this->lang->admin_load('suppliers', $this->Settings->user_language);

        $this->typeSuppliers =  json_decode(json_encode([
            ['id' =>'1', 'name' => 'Productos y Gastos'],
            ['id' =>'2', 'name' => 'Productos'],
            ['id' =>'3', 'name' => 'Gastos'],
            ['id' =>'4', 'name' => 'Acreedores'],
            ['id' =>'5', 'name' => 'Entidad Financiera'],
            ['id' =>'6', 'name' => 'Entidad Promotora de Salud (EPS)'],
            ['id' =>'7', 'name' => 'Fondo de Pensiones y Cesantías'],
            ['id' =>'8', 'name' => 'Administradora de Riesgos Laborales (ARL)'],
            ['id' =>'9', 'name' => 'Caja de compensación'],
            ['id' =>'10', 'name' => 'Servicio Nacional de Aprendizaje (SENA)']
        ]));

        $this->supplierPaymentsTypes =  json_decode(json_encode([
            ['id' =>'0', 'name' => lang('credit')],
            ['id' =>'1', 'name' => lang('payment_type_cash')]
        ]));

        $this->typesWithholdings =  json_decode(json_encode([
            ['id' =>'1', 'name' => $this->lang->line('rete_fuente')],
            ['id' =>'2', 'name' => $this->lang->line('rete_iva')],
            ['id' =>'3', 'name' => $this->lang->line('rete_ica')]
        ]));
    }

    public function index($action = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['action'] = $action;
        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['typeSuppliers'] = $this->typeSuppliers;
        $this->data['typesWithholdings'] = $this->typesWithholdings;
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data["typesPerson"] = $this->site->get_types_person();
        $this->data["cities"] = $this->site->getCitiesFromSuppliers();
        $this->data["typesRegime"] = $this->site->get_types_vat_regime();
        $this->data['supplierPaymentsTypes'] = $this->supplierPaymentsTypes;

        if ($this->input->post('category_filter') || !empty($this->input->post('withHoldings_filter')) || !empty($this->input->post('taxResident_filter'))) {
            $this->data['subcategories_filter'] = $this->site->getSubCategories($this->input->post('category_filter'));

            if ($this->input->post('subcategories_filter')) {
                $this->data['subcategoriesSecondLevel_filter'] = $this->site->getSubCategories($this->input->post('subcategories_filter'));
            }

            $this->data['advancedFiltersContainer'] = TRUE;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('suppliers/index', ['page_title' => lang('suppliers')], $this->data);
    }

    public function getSuppliers()
    {
        $this->sma->checkPermissions('index');

        $optionFilter = $this->input->post('option_filter') == 'false' ? FALSE : $this->input->post('option_filter');
        $getJson = $this->input->post('get_json') ? $this->input->post('get_json') : FALSE;

        $data = [
            'optionFilter' => $optionFilter,
            'getJson' => $getJson,
        ];

        if ($getJson) {
            $data['optionFilter'] = 'active';
            $active = $this->getCustomersResult($data);
            $data['optionFilter'] = 'all';
            $all = $this->getCustomersResult($data);
            $data['optionFilter'] = 'inactive';
            $inactive = $this->getCustomersResult($data);
            $data['optionFilter'] = 'advances';
            $advances = $this->getCustomersResult($data);

            echo json_encode(['active'=>$active, 'all'=> $all, 'inactive'=>$inactive, 'advances'=>$advances]);
        } else {
            echo $this->getCustomersResult($data);
        }
    }

    public function getCustomersResult($data)
    {
        $optionFilter = $data['optionFilter'];
        $getJson = $data['getJson'];
        $supplierType = $this->input->post('supplier_type_filter');
        $typePerson = $this->input->post('type_person_filter');
        $tipoRegimen = $this->input->post('tipo_regimen_filter_filter');
        $city = $this->input->post('city_filter');
        $supplierPaymentType = $this->input->post('supplier_payment_type_filter');
        $generateSupportingDocument = $this->input->post('generateSupportingDocument');
        $category = $this->input->post('category_filter');
        $subcategory = $this->input->post('subcategory_filter');
        $subcategorySecondLevel = $this->input->post('subcategorySecondLevel_filter');
        $withHoldings = json_decode($this->input->post('withHoldings_filter'));
        $taxResident = $this->input->post('taxResident_filter');

        $this->load->library('datatables');

        $this->datatables->distinct("{$this->db->dbprefix('companies')}.id");
        $this->datatables->select("{$this->db->dbprefix('companies')}.id AS companiesId,
            customer_profile_photo,
            company,
            {$this->db->dbprefix('companies')}.name,
            email,
            phone,
            city,
            country,
            vat_no,
            if(status = 1, 'fas fa-user-slash', 'fas fa-user-check') as faicon,
            if(status = 1, '".lang('deactivate_supplier')."', '".lang('activate_supplier')."') as detailaction,
            if(status = 1, '" . admin_url('suppliers/deactivate') . "', '" . admin_url('suppliers/activate') . "') as linkaction,");
        $this->datatables->from("companies");
        $this->datatables->where('group_name', 'supplier');

        if (!empty($supplierType)) {
            $this->datatables->where('supplier_type', $supplierType);
        }
        if (!empty($typePerson)) {
            $this->datatables->where('type_person', $typePerson);
        }
        if (!empty($tipoRegimen)) {
            $this->datatables->where('tipo_regimen', $tipoRegimen);
        }
        if (!empty($city)) {
            $this->datatables->where('city', $city);
        }
        if (!empty($supplierPaymentType)) {
            $this->datatables->where('customer_payment_type', $supplierPaymentType);
        }
        if (!empty($generateSupportingDocument)) {
            $this->datatables->where('generateSupportingDocument', $generateSupportingDocument);
        }
        if (!empty($category)) {
            $this->datatables->join('products', '(sma_products.supplier1 = sma_companies.id OR sma_products.supplier2 = sma_companies.id OR
            sma_products.supplier3 = sma_companies.id OR
            sma_products.supplier4 = sma_companies.id OR
            sma_products.supplier5 = sma_companies.id)', 'inner');
            $this->datatables->where('category_id', $category);
        }
        if (!empty($subcategory)) {
            $this->datatables->where('subcategory', $subcategory);
        }
        if (!empty($$subcategorySecondLevel)) {
            $this->datatables->where('second_level_subcategory_id', $$subcategorySecondLevel);
        }
        if (!empty($withHoldings)) {
            if (in_array('1', $withHoldings)) {
                $this->datatables->where('default_rete_fuente_id', 1);
            }
            if (in_array('2', $withHoldings)) {
                $this->datatables->where('default_rete_iva_id', 1);
            }
            if (in_array('3', $withHoldings)) {
                $this->datatables->where('default_rete_ica_id', 1);
            }
        }
        if (!empty($taxResident)) {
            $this->datatables->where('taxResident', $taxResident);

        }

        if ($optionFilter) {
            if ($optionFilter == 'active') {
                $this->datatables->where('status', 1);
            } else if ($optionFilter == 'inactive') {
                $this->datatables->where('status', 0);
            } else if ($optionFilter == 'advances') {
                $this->datatables->where('deposit_amount >', 0);
            }
        } else {
            $this->datatables->where('status', 1);
        }

        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                        <div class=\"btn-group text-left\">
                                            <button type=\"button\" class=\"btn btn-default new-button new-button-sm btn-xs dropdown-toggle\" data-toggle=\"dropdown\"  data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                                                <i class='fas fa-ellipsis-v fa-lg'></i>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li>
                                                    <a class=\"tip\" title='' href='" . admin_url('products?supplier=$1') . "'><i class=\"fa fa-list\"></i>" . $this->lang->line("list_products") . "</a>
                                                </li>
                                                ".
                                                ($this->Owner || $this->Admin || $this->GP['suppliers-list_deposits'] ? "<li>
                                                    <a href='" . admin_url('suppliers/deposits/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                       <i class=\"fa fa-money\"></i>
                                                            ".lang("list_deposits")."
                                                    </a>
                                                 </li>" : "")."
                                                <li>
                                                    <a href='" . admin_url('suppliers/add_deposit/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-plus\"></i>
                                                        ".lang("add_deposit")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class=\"tip\" title='' href='" . admin_url('suppliers/users/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-users\"></i>" . $this->lang->line("list_users") . "</a>
                                                </li>
                                                <li>
                                                    <a class=\"tip\" title='' href='" . admin_url('suppliers/add_user/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-plus-circle\"></i>" . $this->lang->line("add_user") . "</a>
                                                </li>
                                                <li>
                                                    <a class=\"tip\" title='' href='" . admin_url('suppliers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i>" . $this->lang->line("edit_supplier") . "</a>
                                                </li>".
                                                ($this->sma->actionPermissions('delete', 'customers') || $this->Admin || $this->Owner ?
                                                "<li>
                                                    <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='$4/$1'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"$2\"></i>
                                                        $3
                                                    </a>
                                                </li>" : "")."
                                            </ul>
                                        </div>
                                    </div>", "companiesId, faicon, detailaction, linkaction");

        $this->datatables->unset_column('faicon');
        $this->datatables->unset_column('detailaction');
        $this->datatables->unset_column('linkaction');

        if ($getJson) {
            return $this->datatables->get_result_num_rows();
        } else {
            return $this->datatables->generate();
        }
    }

    public function view($id = NULL)
    {
        $this->sma->checkPermissions('index', true);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['supplier'] = $this->companies_model->getCompanyByID($id);
        $this->data["types_supplier_obligations"] = $this->site->get_customer_obligations($id, ACQUIRER);
        $this->data['default_rete_fuente'] = $this->site->get_ledger_by_id($this->data['supplier']->default_rete_fuente_id);
        $this->data['default_rete_iva'] = $this->site->get_ledger_by_id($this->data['supplier']->default_rete_iva_id);
        $this->data['default_rete_ica'] = $this->site->get_ledger_by_id($this->data['supplier']->default_rete_ica_id);
        $this->data['default_rete_other'] = $this->site->get_ledger_by_id($this->data['supplier']->default_rete_other_id);
        $this->data["custom_fields"] = $this->site->get_custom_fields();
        $this->data['created_user'] = $this->site->getUserById($this->data['supplier']->created_by);
        $this->load_view($this->theme.'suppliers/view',$this->data);
    }

    public function add($from_purchase = false)
    {
        $this->sma->checkPermissions(false, true);
        if ($this->form_validation->run('companies/add') == true) {
            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => '4',
                'group_name' => 'supplier',
                'company' => $this->input->post("company") && $this->input->post("company") != "" ? $this->input->post("company") : $this->getFullName(),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $this->getFullName(),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'type_person' => $this->input->post('type_person'),
                'tipo_regimen' => $this->input->post('tipo_regimen'),
                'tipo_documento' => $this->input->post('tipo_documento'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code'),
                "default_rete_fuente_id" => $this->input->post("default_rete_fuente_id"),
                "default_rete_iva_id" => $this->input->post("default_rete_iva_id"),
                "default_rete_ica_id" => $this->input->post("default_rete_ica_id"),
                "default_rete_other_id" => $this->input->post("default_rete_other_id"),
                'customer_validate_min_base_retention' => $this->input->post('customer_validate_min_base_retention') ? 1 : 0,
                "customer_payment_type" => $this->input->post("supplier_payment_type"),
                "customer_credit_limit" => $this->input->post("supplier_credit_limit"),
                "customer_payment_term" => $this->input->post("supplier_payment_term"),
                "supplier_type" => $this->input->post("supplier_type"),
                'registration_date' => date('Y-m-d H:i:s'),
                "generateSupportingDocument" => $this->input->post("generateSupportingDocument") ? 1 : 0,
                'document_code' => $this->input->post('document_code'),
                "taxResident" => $this->input->post("taxResident") ? 1 : 0,
                "seller_phone" => $this->input->post('seller_phone'),
                "seller_email" => $this->input->post('seller_email'),
                "seller_name" => $this->input->post('seller_name'),
                "account_number" => $this->input->post('account_number'),
                "account_type" => $this->input->post('account_type'),
                "bank_id" => $this->input->post('bank_id'),
                "delivery_days" => $this->input->post('delivery_days'),
                "created_by" => $this->session->userdata('user_id'),
            );

            if ($this->companies_model->validate_vat_no($data['vat_no'], 'supplier')) {
                $this->session->set_flashdata('error', 'Ya existe un tercero con el mismo documento y mismo perfil.');
                admin_redirect('suppliers');
            }

            if ($this->input->post('custom_field_value')) {
                $last_cf_code = '';
                foreach ($_POST['custom_field_value'] as $cf_id => $cf_value) {
                    if ($cf_value) {
                        if (!isset($_POST['custom_field_code'][$cf_id])) {
                            if (isset($data[$last_cf_code])) {
                                $data[$last_cf_code] .= ", ".$cf_value;
                            } else {
                                $data[$last_cf_code] = $cf_value;
                            }
                        } else {
                            $last_cf_code = $_POST['custom_field_code'][$cf_id];
                            $cf_code = $_POST['custom_field_code'][$cf_id];
                            $data[$cf_code] = $cf_value;
                        }
                    }
                }
            }

        } elseif ($this->input->post('add_supplier')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('suppliers');
        }

        if ($this->form_validation->run() == true && $sid = $this->companies_model->addCompany($data)) {
            $typeObligations = $this->input->post("types_obligations");
            $supplierObligationsArray = [];
            if ($typeObligations) {
                foreach ($typeObligations as $typeObligation) {
                  $supplierObligationsArray[] = [
                    "customer_id" => $sid,
                    "types_obligations_id" => $typeObligation,
                    "relation" => ACQUIRER
                  ];
                }
              $this->companies_model->insert_type_customer_obligations($supplierObligationsArray);
            }

            $this->session->set_flashdata('message', $this->lang->line("supplier_added"));
            $from_purchase = $this->input->post('from_purchase');
            if ($from_purchase) {
                admin_redirect('purchases/add?new_supplier_id='.$sid);
            } else {
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->data["typesPerson"] = $this->site->get_types_person();
            $this->data["typesObligations"] = $this->site->get_types_obligations();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $this->data["custom_fields"] = $this->site->get_custom_fields(3);
            $this->data["from_purchase"] = $from_purchase;
            $rete_data = $this->purchases_model->getWithHolding(NULL, 'P');
            $retentions = [];
            if ($rete_data) {
                foreach ($rete_data as $row) {
                    $retentions[$row['type']][] = $row;
                }
            }
            $this->data['retentions'] = $retentions;
            $this->load_view($this->theme . 'suppliers/add', $this->data);
        }
    }

    public function edit($id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $company_details = $this->companies_model->getCompanyByID($id);
        if ($this->input->post('email') != $company_details->email) {
            $this->form_validation->set_rules('code', lang("email_address"), 'is_unique[companies.email]');
        }

        if ($this->form_validation->run('companies/add') == true) {
            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => '4',
                'group_name' => 'supplier',
                'company' => $this->input->post("company") && $this->input->post("company") != "" ? $this->input->post("company") : $this->getFullName(),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $this->getFullName(),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'type_person' => $this->input->post('type_person'),
                'tipo_regimen' => $this->input->post('tipo_regimen'),
                'tipo_documento' => $this->input->post('tipo_documento'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code'),
                "default_rete_fuente_id" => $this->input->post("default_rete_fuente_id"),
                "default_rete_iva_id" => $this->input->post("default_rete_iva_id"),
                "default_rete_ica_id" => $this->input->post("default_rete_ica_id"),
                "default_rete_other_id" => $this->input->post("default_rete_other_id"),
                'customer_validate_min_base_retention' => $this->input->post('customer_validate_min_base_retention') ? 1 : 0,
                "customer_payment_type" => $this->input->post("supplier_payment_type"),
                "customer_credit_limit" => $this->input->post("supplier_credit_limit"),
                "customer_payment_term" => $this->input->post("supplier_payment_term"),
                "supplier_type" => $this->input->post("supplier_type"),
                "generateSupportingDocument" => $this->input->post("generateSupportingDocument") ? 1 : 0,
                'document_code' => $this->input->post('document_code'),
                "taxResident" => $this->input->post("taxResident") ? 1 : 0,
                "seller_phone" => $this->input->post('seller_phone'),
                "seller_email" => $this->input->post('seller_email'),
                "seller_name" => $this->input->post('seller_name'),
                "account_number" => $this->input->post('account_number'),
                "account_type" => $this->input->post('account_type'),
                "bank_id" => $this->input->post('bank_id'),
                "delivery_days" => $this->input->post('delivery_days'),
            );

            if ($this->input->post('custom_field_value')) {
                $last_cf_code = '';
                foreach ($_POST['custom_field_value'] as $cf_id => $cf_value) {
                    if ($cf_value) {
                        if (!isset($_POST['custom_field_code'][$cf_id])) {
                            if (isset($data[$last_cf_code])) {
                                $data[$last_cf_code] .= ", ".$cf_value;
                            } else {
                                $data[$last_cf_code] = $cf_value;
                            }
                        } else {
                            $last_cf_code = $_POST['custom_field_code'][$cf_id];
                            $cf_code = $_POST['custom_field_code'][$cf_id];
                            $data[$cf_code] = $cf_value;
                        }
                    }
                }
            }

        } elseif ($this->input->post('edit_supplier')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data)) {
            $this->site->delete_types_customer_obligations($id, ACQUIRER);
            $typeSupplierObligations = $this->input->post("types_obligations");
            $customerObligationsArray = [];
            if ($typeSupplierObligations) {
                foreach ($typeSupplierObligations as $customerObligation) {
                    $customerObligationsArray[] = [
                        "customer_id" => $id,
                        "types_obligations_id" => $customerObligation,
                        "relation" => ACQUIRER
                    ];
                }
                $this->companies_model->insert_type_customer_obligations($customerObligationsArray);
            }

            $this->session->set_flashdata('message', $this->lang->line("supplier_updated"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data["typesPerson"] = $this->site->get_types_person();
            $this->data["typesObligations"] = $this->site->get_types_obligations();
            $this->data["typesSupplierObligations"] = $this->site->getTypesCustomerObligations($id, ACQUIRER);
            $this->data['supplier'] = $company_details;
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $this->data["custom_fields"] = $this->site->get_custom_fields(3);
            $rete_data = $this->purchases_model->getWithHolding(NULL, 'P');
            $retentions = [];
            if ($rete_data) {
                foreach ($rete_data as $row) {
                    $retentions[$row['type']][] = $row;
                }
            }
            $this->data['retentions'] = $retentions;
            $this->load_view($this->theme . 'suppliers/edit', $this->data);
        }
    }

    private function getFullName()
    {
        $fullName = "";
        if ($this->input->post('type_person') == NATURAL_PERSON) {
            $fullName = $this->input->post('first_name');
            $fullName .= ($this->input->post('second_name')) ? " ".$this->input->post('second_name') : "";
            $fullName .= " ".$this->input->post('first_lastname');
            $fullName .= ($this->input->post('second_lastname')) ? " ".$this->input->post('second_lastname') : "";
        } else {
            $fullName = $this->input->post('name');
        }
        return $fullName;
    }

    public function users($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }


        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['users'] = $this->companies_model->getCompanyUsers($company_id);
        $this->load_view($this->theme . 'suppliers/users', $this->data);

    }

    public function add_user($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[users.email]');
        $this->form_validation->set_rules('password', $this->lang->line('password'), 'required|min_length[8]|max_length[20]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('confirm_password'), 'required');

        if ($this->form_validation->run('companies/add_user') == true) {
            $active = $this->input->post('status');
            $notify = $this->input->post('notify');
            list($username, $domain) = explode("@", $this->input->post('email'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'company_id' => $company->id,
                'company' => $company->company,
                'group_id' => 3
            );
            $this->load->library('ion_auth');
        } elseif ($this->input->post('add_user')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('suppliers');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, $active, $notify)) {
            $this->session->set_flashdata('message', $this->lang->line("user_added"));
            admin_redirect("suppliers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load_view($this->theme . 'suppliers/add_user', $this->data);
        }
    }

    public function import_csv()
    {
        $this->sma->checkPermissions('add', true);
        $this->load->helper('security');
        $this->form_validation->set_rules('csv_file', $this->lang->line("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('warning', $this->lang->line("disabled_in_demo"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if (isset($_FILES["csv_file"])) /* if($_FILES['userfile']['size'] > 0) */ {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/csv/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '2000';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('csv_file')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("suppliers");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen("assets/uploads/csv/" . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5001, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('company', 'type_person', 'name', 'email', 'phone', 'country', 'state',  'city', 'postal_code', 'address', 'tipo_documento', 'vat_no', 'digito_verificacion', 'customer_group_id',  'cf1', 'cf2', 'cf3', 'cf4', 'cf5', 'cf6');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $value = $this->site->cleanRowCsvImport($value);
                    if (!$final[] = array_combine($keys, $value)) { //VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key+1)));
                        admin_redirect("suppliers");
                        break;
                    }
                }
                $rw = 2;
                foreach ($final as $csv) {
                    if ($this->companies_model->getCompanyByEmail($csv['email'])) {
                        $this->session->set_flashdata('error', $this->lang->line("check_supplier_email") . " (" . $csv['email'] . "). " . $this->lang->line("supplier_already_exist") . " (" . $this->lang->line("line_no") . " " . $rw . ")");
                        admin_redirect("suppliers");
                    }
                    $rw++;
                }
                foreach ($final as $record) {
                    $record['first_name'] = "";
                    $record['second_name'] = "";
                    $record['first_lastname'] = "";
                    $record['second_lastname'] = "";

                    if ($record['type_person'] == 1) {
                        $namePerson = explode(" ", $record['name']);
                        // exit(var_dump($namePerson));
                        if (count($namePerson) == 2) {
                          $record['first_name'] = $namePerson[0];
                          $record['first_lastname'] = $namePerson[1];
                        } else if (count($namePerson) == 3) {
                          $record['first_name'] = $namePerson[0];
                          $record['first_lastname'] = $namePerson[1];
                          $record['second_lastname'] = $namePerson[2];
                        } else if (count($namePerson) == 4) {
                          $record['first_name'] = $namePerson[0];
                          $record['second_name'] = $namePerson[1];
                          $record['first_lastname'] = $namePerson[2];
                          $record['second_lastname'] = $namePerson[3];
                        }
                    }

                    $record['group_id'] = $this->companies_model->getCompanyGroupIdByName('supplier');
                    $record['group_name'] = 'supplier';

                    $record['country'] = mb_strtoupper($record['country']);
                    $record['state'] = mb_strtoupper($record['state']);
                    $record['city'] = mb_strtoupper($record['city']);
                    $record['created_by'] = $this->session->userdata('user_id');

                    $data[] = $record;
                }
                //$this->sma->print_arrays($data);
            }

        } elseif ($this->input->post('import')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            if ($this->companies_model->addCompanies($data)) {
                $this->session->set_flashdata('message', $this->lang->line("suppliers_added"));
                admin_redirect('suppliers');
            }
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'suppliers/import', $this->data);
        }
    }

    public function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->deleteSupplier($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("supplier_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("supplier_x_deleted_have_purchases")));
        }
    }

    public function suggestions($term = NULL, $limit = NULL, $ptype = 1)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $ptype = $this->input->get('ptype') ? $this->input->get('ptype') : 1;
        $support_document = $this->input->get('support_document') ? $this->input->get('support_document') : 0;
        $rows['results'] = $this->companies_model->getSupplierCreditorSuggestions($term, $limit, $ptype, $support_document);
        $this->sma->send_json($rows);
    }

    public function employee_suggestions($term = NULL, $limit = NULL, $ptype = 1)
    {
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getEmployees($term, $limit);
        $this->sma->send_json($rows);
    }

    public function getSupplier($id = NULL)
    {
        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->name."(".$row->company.")")));
    }

    public function supplier_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteSupplier($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('suppliers_x_deleted_have_purchases'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("suppliers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('city'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('postal_code'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('vat_no'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('scf1'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('scf2'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('scf3'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('scf4'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('scf5'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('scf6'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->site->getCompanyByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->company);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->address);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->city);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->state);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->postal_code);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->country);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->vat_no);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->cf1);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->cf2);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cf3);
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf4);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf5);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->cf6);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'suppliers_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_supplier_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function validate_vat_no($vat_no)
    {
        $validate = $this->companies_model->validate_vat_no($vat_no, 'supplier');
        if ($validate) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function get_supplier_by_id($id, $biller_id = NULL, $without_portfolio = false)
    {
        $biller_id = $biller_id === "NULL" ? false : $biller_id;
        $supplier = $this->companies_model->getCompanyByID($id);
        if ($without_portfolio == false) {
            $portfolio = $this->companies_model->get_portfolio($id, 'supplier', $biller_id);
            $supplier->portfolio = $portfolio->balance;
        }
        echo json_encode($supplier);
    }

    public function list_deposits()
    {
        $this->sma->checkPermissions();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('suppliers')));
        $meta = array('page_title' => lang('list_deposits_suppliers'), 'bc' => $bc);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([15]);
        $this->data['users'] = $this->site->get_all_users();

        $this->page_construct('suppliers/list_deposits', $meta, $this->data);
    }

    public function get_deposits($company_id = NULL)
    {

        if ($this->input->post('start_date')) {
             $start_date = $this->input->post('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        $document_type_id = NULL;
        if ($this->input->post('sdeppayment_reference_no')) {
            $document_type_id = $this->input->post('sdeppayment_reference_no');
        }
        $payment_method = NULL;
        if ($this->input->post('sdeppayment_method')) {
            $payment_method = $this->input->post('sdeppayment_method');
        }
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $supplier_id = $this->input->post('supplier') ? $this->input->post('supplier') : NULL;



        $contabilizar_venta = '<a class="reaccount_pr_deposit_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> '.lang('post_deposit').' </a>';
        $view_deposit = "<a class=\"tip\" href='" . admin_url('suppliers/deposit_note/$1') . "' data-toggle='modal' data-target='#myModal2'><i class=\"fa fa-file-text-o\"></i>" . lang("deposit_note") . "</a>";


        $action = '<div class="text-center"><div class="btn-group text-left">
                        <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                        <i class="fas fa-ellipsis-v fa-lg"></i></button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>' . $contabilizar_venta . '</li>
                                <li>' . $view_deposit . '</li>
                            </ul>
                        </div>
                    </div>';
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        deposits.id as id,
                        reference_no,
                        date,
                        amount,
                        (amount - balance) as applied_amount,
                        balance,
                        paid_by,
                        IF(origen = 1, 'Manual', 'Automático') as origen,
                        IF(origen_reference_no IS NOT NULL, origen_reference_no, 'Manual'),
                        CONCAT({$this->db->dbprefix('users')}.first_name,
                        ' ',
                        {$this->db->dbprefix('users')}.last_name) as created_by,
                        companies.name
                    ", false)
            ->from("deposits")
            ->join('users', 'users.id=deposits.created_by', 'left')
            ->join('companies', 'companies.id=deposits.company_id', 'left');

        if ($company_id != NULL) {
            $this->datatables->where($this->db->dbprefix('deposits').'.company_id', $company_id);
        }

        /* FILTROS */

        if ($start_date) {
            $this->datatables->where('deposits.date >= ', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('deposits.date <= ', $end_date);
        }
        if ($document_type_id) {
            $this->datatables->where('deposits.document_type_id', $document_type_id);
        }
        if ($user_id) {
            $this->datatables->where('deposits.created_by', $user_id);
        }
        if ($payment_method) {
            $this->datatables->where('deposits.paid_by', $payment_method);
        }
        if ($biller_id) {
            $this->datatables->where('deposits.biller_id', $biller_id);
        }
        if ($supplier_id) {
            $this->datatables->where('deposits.company_id', $supplier_id);
        }

        /* FILTROS */

        $this->datatables->where('('.$this->db->dbprefix('companies').'.group_name = "supplier" OR '.$this->db->dbprefix('companies').'.group_name = "creditor")');
        $this->datatables->order_by('deposits.date desc');
        $this->datatables
            ->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function add_deposit($company_id = NULL)
    {
        $this->sma->checkPermissions();

        if (!$this->Owner && !$this->Admin) {
            if ($this->session->userdata('register_cash_movements_with_another_user')) {
                if (!$this->pos_model->registerData($this->session->userdata('register_cash_movements_with_another_user'))) {
                    $this->session->set_flashdata('error', lang('another_user_cash_not_open'));
                    admin_redirect('welcome');
                }
            } else {
                if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                    $this->session->set_flashdata('error', lang('register_not_open'));
                    admin_redirect('pos/open_register');
                }
            }
        }

        if (!$company_id) {
            $company_id = $this->input->post('supplier');
        }

        $company = $this->companies_model->getCompanyByID($company_id);

        if ($this->Owner || $this->Admin) {
            $this->form_validation->set_rules('date', lang("date"), 'required');
        }
        $this->form_validation->set_rules('amount', lang("amount"), 'required|numeric');

        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }

        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $biller_id = $this->input->post('biller');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect('purchases/add');
            }
            $data = array(
                'date' => $date,
                'amount' => $this->input->post('amount'),
                'balance' => $this->input->post('amount'),
                'paid_by' => $this->input->post('paid_by'),
                'note' => $this->input->post('note'),
                'company_id' => $company->id,
                'created_by' => $this->session->userdata('user_id'),
                'biller_id' => $biller_id,
                'document_type_id' => $this->input->post('document_type_id'),
            );
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }
            $cdata = array(
                'deposit_amount' => ($company->deposit_amount+$this->input->post('amount'))
            );

            if ($this->Settings->cashier_close != 2) {
                if (isset($data) &&  ($this->input->post('paid_by') == "cash" && $this->input->post('amount') > 0) && !$this->pos_model->getRegisterState(($this->input->post('amount')))) {
                    $this->session->set_flashdata('error', "El dinero en caja no es suficiente para registrar el anticipo.");
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }

        } elseif ($this->input->post('add_deposit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('suppliers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->addDeposit($data, $cdata, 2)) {
            $this->site->updateReference('dp');
            $this->session->set_flashdata('message', lang("deposit_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->load_view($this->theme . 'suppliers/add_deposit', $this->data);
        }
    }

    public function deposit_note($id = null, $pdf = null)
    {
        $deposit = $this->companies_model->getDepositByID($id);
        $this->data['supplier'] = $this->companies_model->getCompanyByID($deposit->company_id);
        $this->data['deposit'] = $deposit;
        $this->data['page_title'] = $this->lang->line("deposit_note");
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $deposit->biller_id);
        $this->data['payments'] = $this->site->get_payments_affects_deposit_purchases($id);
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $this->data['paid_by'] = lang($deposit->paid_by);
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($deposit->cost_center_id);
        }
        if ($pdf) {
            $this->data['document_type'] = $this->site->getDocumentTypeById($deposit->document_type_id);
            $document_type_invoice_format = $this->site->getInvoiceFormatById($this->data['document_type']->module_invoice_format_id);
            $url = 'suppliers/deposit_note_view';
            if ($document_type_invoice_format) {
                $url = $document_type_invoice_format->format_url;
            }
            $this->load_view($this->theme .$url, $this->data);
        } else {
            $this->load_view($this->theme . 'suppliers/deposit_note', $this->data);
        }
    }

    public function deposits($company_id = NULL)
    {
        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->load_view($this->theme . 'suppliers/deposits', $this->data);
    }

    public function deposit_balance($supplier_id, $amount = 0)
    {

        $supplier = $this->site->getCompanyByID($supplier_id);
        $balance = $supplier->deposit_amount;
        if ($balance > $amount) {
            $type_msg = "primary";
        } else if ($balance < $amount) {
            $type_msg = "danger";
        } else {
            $type_msg = "default";
        }
        $amount_topay = $amount;
        $cdps = $this->companies_model->getDepositsWithBalance($supplier_id, 20);

        // exit(var_dump($cdps));

        $tabla = "<table class='table'>";
        $tabla .= "<tr>
                        <th class='text-center'> Número </th>
                        <th class='text-center'> Origen </th>
                        <th class='text-center'> Fecha </th>
                        <th class='text-center'> Valor </th>
                        <th class='text-center'> Aplicado </th>
                        <th class='text-center'> Balance </th>
                        <th class='text-center'> Valor a aplicar </th>
                   </tr>";
        if ($cdps != FALSE) {
            foreach ($cdps as $deposit) {

                if ($amount_topay == 0) {
                    break;
                }
                $amount_to_apply = 0;
                if ((Double) $deposit->balance < (Double) $amount_topay) {
                    $amount_topay -= $deposit->balance;
                    $amount_to_apply = $deposit->balance;
                } else if ($deposit->balance >= $amount_topay) {
                    $amount_to_apply = $amount_topay;
                    $amount_topay -= $amount_topay;
                }

                $tabla .= "<tr>
                                <td class='text-center'> ".$deposit->reference_no." </td>
                                <td class='text-center'> ".$deposit->origen_reference_no." </td>
                                <td class='text-center'> ".$deposit->date." </td>
                                <td class='text-center'> ".$this->sma->formatMoney($deposit->amount)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($deposit->amount - $deposit->balance)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($deposit->balance)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($amount_to_apply)." </td>
                           </tr>";
            }
        } else {
            $tabla .= "<tr>
                            <th colspan='6' class='text-center'> ".lang('supplier_without_balance')." </th>
                       </tr>";
        }

        $tabla .= "<tr>
                        <th colspan='5' class='text-right'> <span class='fa fa-exclamation-circle text-".$type_msg."'></span> <b>".lang('supplier_deposit_balance')."</b> </th>
                        <th class='text-right'> ".$this->sma->formatMoney($balance)." </th>
                   </tr>";

        $tabla .= "</table>";

        $msg_deposit =  '<div class="panel panel-'.$type_msg.' deposit_message" style="border-radius:5px;">
          <div class="panel-body text-center" >
            </hr>
            '.$tabla.'
          </div>
        </div>';

        echo $msg_deposit;
    }

    public function get_supplier_deposit_balance($supplier_id)
    {
        $supplier = $this->companies_model->getCompanyByID($supplier_id);
        $portfolio = $this->companies_model->get_portfolio($supplier->vat_no, 'supplier_cxc');
        $data['deposit_amount'] = $supplier->deposit_amount;
        $data['cxc_portfolio'] = $portfolio;
        echo json_encode($data);
    }

    public function get_payment_type($customer_id)
    {
        $customer = $this->companies_model->getCompanyByID($customer_id);

        $data['payment_type'] = $customer->customer_payment_type;
        $data['payment_term'] = $customer->customer_payment_term;
        $data['credit_limit'] = $customer->customer_credit_limit;

        echo json_encode($data);
    }

    public function deposit_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');
        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'reaccount') {
                    // exit('AA');
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->recontabilizar_deposit($id, 2)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('deposit_post_error'));
                    } else {
                        $this->session->set_flashdata('message', lang("deposit_posted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('deposits'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('total'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('amount_applied'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('balance'));

                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('type'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('origin'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('origin_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('created_by'));

                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('biller'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $qu = $this->companies_model->getDepositByID($id);
                        $customer = $this->site->getAllCompaniesWithState('supplier', $qu->company_id);
                        $biller = $this->site->getAllCompaniesWithState('biller', $qu->biller_id);
                        $employee = $this->site->getUser($qu->created_by);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($qu->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $qu->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $qu->amount);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $qu->amount - $qu->balance);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $qu->balance);

                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($qu->paid_by));
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $qu->origen == 1 ? 'Manual' : 'Automática');
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $qu->origen_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $employee->first_name." ".$employee->last_name);


                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->company);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $biller->company);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $this->excel->getActiveSheet()->getStyle('C1:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $this->excel->getActiveSheet()->getStyle('D1:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $filename = 'deposits_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_deposit_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function contabilizarDeposito($deposit_id = null, $json = null)
    {
        $this->form_validation->set_rules('deposit_id', lang("deposit_id"), 'required');
        if ($this->form_validation->run() == true) {
            $deposit_id = $this->input->post('deposit_id');
            $msg = $this->companies_model->recontabilizar_deposit($deposit_id, 2);
            $this->session->set_flashdata('message', $msg);
            admin_redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $deposit = $this->companies_model->getDepositByID($deposit_id, true);
            $exists = $this->site->getEntryTypeNumberExisting($deposit, false, false);
            $this->data['exists'] = $exists;
            $this->data['inv'] = $deposit;
            $this->load_view($this->theme . 'suppliers/recontabilizar_deposit', $this->data);
        }
    }

    public function validate_email(){
        $email = $this->input->get('email');
        $validate = $this->companies_model->validate_email($email);
        if ($validate) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function deactivate($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->input->get('id') == 1) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("supplier_x_deleted")));
        }

        if ($this->companies_model->deactivate_customers($id, 0)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("supplier_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("supplier_x_deleted_have_sales")));
        }
    }

    public function activate($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->input->get('id') == 1) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("supplier_x_activated")));
        }

        if ($this->companies_model->deleteCustomer($id, 1)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("supplier_activated")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("supplier_x_activated_have_sales")));
        }
    }
}
