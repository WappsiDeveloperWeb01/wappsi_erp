<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= $creditor->company && $creditor->company != '-' ? $creditor->company : $creditor->name; ?></h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table  table-bordered" style="margin-bottom:0;">
                    <tbody>
                    <tr>
                        <td><strong><?= lang("company"); ?></strong></td>
                        <td><?= $creditor->company; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("name"); ?></strong></td>
                        <td><?= $creditor->name; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("vat_no"); ?></strong></td>
                        <td><?= $creditor->vat_no; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("email"); ?></strong></td>
                        <td><?= $creditor->email; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("phone"); ?></strong></td>
                        <td><?= $creditor->phone; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("address"); ?></strong></td>
                        <td><?= $creditor->address; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("city"); ?></strong></td>
                        <td><?= $creditor->city; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("state"); ?></strong></td>
                        <td><?= $creditor->state; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("postal_code"); ?></strong></td>
                        <td><?= $creditor->postal_code; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("country"); ?></strong></td>
                        <td><?= $creditor->country; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("scf1"); ?></strong></td>
                        <td><?= $creditor->cf1; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("scf2"); ?></strong></td>
                        <td><?= $creditor->cf2; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("scf3"); ?></strong></td>
                        <td><?= $creditor->cf3; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("scf4"); ?></strong></td>
                        <td><?= $creditor->cf4; ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("scf5"); ?></strong></td>
                        <td><?= $creditor->cf5; ?></strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer no-print">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= lang('close'); ?></button>
                <?php if ($Owner || $Admin || $GP['reports-creditors']) { ?>
                    <a href="<?=admin_url('reports/supplier_report/'.$creditor->id);?>" target="_blank" class="btn btn-primary"><?= lang('suppliers_report'); ?></a>
                <?php } ?>
                <?php if ($Owner || $Admin || $GP['creditors-edit']) { ?>
                    <a href="<?=admin_url('creditors/edit/'.$creditor->id);?>" data-toggle="modal" data-target="#myModal2" class="btn btn-primary"><?= lang('edit_creditor'); ?></a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>