$(document).ready(function () {
    $("#purchase_form").validate({
          ignore: []
      });
    localStorage.removeItem('re_retenciones');
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }
    // Order level shipping and discoutn localStorage
    if (podiscount = localStorage.getItem('podiscount')) {
        $('#podiscount').val(podiscount);
    }
    $('#potax2').change(function (e) {
        localStorage.setItem('potax2', $(this).val());
    });
    if (potax2 = localStorage.getItem('potax2')) {
        $('#potax2').select2("val", potax2);
    }
    var old_shipping;
    $('#poshipping').focus(function () {
        old_shipping = $(this).val();
    }).change(function () {
        var posh = $(this).val() ? $(this).val() : 0;
        if (!is_numeric(posh)) {
            $(this).val(old_shipping);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        shipping = parseFloat(posh / trmrate);
        localStorage.setItem('poshipping', shipping);
        if ($('#prorate_shipping_cost').val() == 0) {
            $('#tship').text(formatMoney(shipping));
        } else {
            $('#tship').text(formatMoney(0));
        }
        loadItems();
    });
    if (poshipping = localStorage.getItem('poshipping')) {
        shipping = parseFloat(poshipping);
        $('#poshipping').val(shipping).trigger('change');
    }
    $('#popayment_term').change(function (e) {
        localStorage.setItem('popayment_term', $(this).val());
    });
    if (popayment_term = localStorage.getItem('popayment_term')) {
        $('#popayment_term').val(popayment_term);
    }
    if (poorderdiscountmethod = localStorage.getItem('poorderdiscountmethod')) {
        $('#order_discount_method').select2('val', poorderdiscountmethod).select2('readonly', true);
    }
    if (poorderdiscounttoproducts = localStorage.getItem('poorderdiscounttoproducts')) {
        $('#order_discount_to_products').select2('val', poorderdiscounttoproducts).select2('readonly', true);
    }
    if (prorate_shipping_cost = localStorage.getItem('prorate_shipping_cost')) {
        $('#prorate_shipping_cost').select2('val', prorate_shipping_cost).trigger('change');
    }
    if (po_form_import = JSON.parse(localStorage.getItem('po_form_import'))) {
        setTimeout(function() {
            $('#purchase_from_import').iCheck('check');
        }, 850);
        $('#postatus').select2('val', 'pending').select2('readonly', true).trigger('change');
    }
    if (potag = localStorage.getItem('potag')) {
        $('#potag').select2('val', potag);
    }
    if (pobiller = localStorage.getItem('pobiller')) {
        $('#pobiller').select2('val', pobiller).trigger('change');
    } else {
        setTimeout(function() {
            $('#pobiller').trigger('change');
        }, 850);
    }
    $('#purchase_type').on('change', function(){
        potype = $(this).val();
        localStorage.setItem('purchase_type', potype);
        $('#purchase_type').select2('readonly', true);
        if (potype == 1) {
            $('#paid_by_1 option').each(function(index, option) {
                if ($(option).val() == 'expense_causation') {
                    $(option).prop('disabled', true);
                }
            });
            $('.edit_purchase_expense').remove();
            $('.podiscount_div').fadeIn();
            $('.poshipping_div').fadeIn();
            $('#powarehouse').prop('required', true);
            $('.div_powarehouse').fadeIn();
        } else if ((potype == 2 || potype == 3)) {
            $('#paid_by_1 option').each(function(index, option) {
                if ($(option).val() == 'expense_causation') {
                    $(option).prop('disabled', false);
                }
            });
            $('.edit_purchase').remove();
            $('.edit_purchase_expense').css('display', '');
            $('.podiscount_div').fadeOut();
            $('.poshipping_div').fadeOut();
            $('#paid_by_1').select2('val', 'expense_causation').trigger('change');
            $('#powarehouse').prop('required', false);
            $('.div_powarehouse').fadeOut();
        }
        if (potype == 3) {
            $('#purchase_from_import').iCheck('check');
        }
    });
    if (purchase_type = localStorage.getItem('purchase_type')) {
        $('#purchase_type').select2('val', purchase_type).trigger('change');
        $('#purchase_type').select2('readonly', true);
        if (purchase_type == 1) {
            $('.edit_purchase_expense').remove();
            setTimeout(function() {
                $('#paid_by_1 option').each(function(index, option) {
                    if ($(option).val() == 'expense_causation') {
                        $(option).prop('disabled', true);
                    }
                    $('#paid_by_1').select2('val', 'Credito');
                });
            }, 850);
        } else {
            $('.edit_purchase').remove();
        }
    } else {
        setTimeout(function() {
            $('#paid_by_1 option').each(function(index, option) {
                if ($(option).val() == 'expense_causation') {
                    $(option).prop('disabled', true);
                }
                $('#paid_by_1').select2('val', 'Credito');
            });
        }, 850);
    }
    // If there is any item in localStorage
    if (localStorage.getItem('poitems')) {
        loadItems();
    }
    $('#popayment_status').change(function (e) {
        var ps = $(this).val();
        if (rete_prev = JSON.parse(localStorage.getItem('poretenciones'))) {
            total_rete_amount = formatDecimal(rete_prev.total_rete_amount);
            recalcular_poretenciones();
        } else {
            total_rete_amount = 0;
        }
        localStorage.setItem('popayment_status', ps);
        if (ps == 'partial' || ps == 'paid') {

            shipping = formatDecimal(parseFloat(localStorage.getItem('poshipping')));
            // if(ps == 'paid') {
                set_trm_input_payment(parseFloat(formatDecimal(gtotal)));
            // }
            $('#payments').slideDown();
            $('#pcc_no_1').focus();
        } else {
            $('#payments').slideUp();
        }
        if (localStorage.getItem('supplier_deposit_balance') && (ps == 'partial' || ps == 'paid')) {
            deposit_amount = parseFloat(localStorage.getItem('supplier_deposit_balance'));
            if (deposit_amount > 0 && deposit_payment_method.purchase == true && edit_paid_by != 'deposit') {
                bootbox.confirm({
                    message: "El proveedor cuenta con "+formatMoney(deposit_amount)+" de saldo de anticipos, ¿Desea usar este medio de pago para esta compra?",
                    buttons: {
                        confirm: {
                            label: 'Si, usar anticipos',
                            className: 'btn-success send-submit-sale btn-full'
                        },
                        cancel: {
                            label: 'No, usar otro medio de pago',
                            className: 'btn-danger btn-full'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            gtotal = parseFloat(formatDecimal($('#totalP').text()));
                            if (gtotal > deposit_amount) {
                                set_trm_input_payment(parseFloat(formatDecimal(deposit_amount)));
                            }
                           $('#paid_by_1').select2('val', 'deposit').trigger('change');
                        }
                    }
                });
            }
        }
    });

    popayment_status = 'paid';
    $('#popayment_status').select2("val", popayment_status).trigger('change');
    var ps = popayment_status;
    if (ps == 'partial' || ps == 'paid') {
        $('#payments').slideDown();
        $('#pcc_no_1').focus();
    } else {
        $('#payments').slideUp();
    }

    $(document).on('change', '.paid_by', function () {
        var p_val = $(this).val();
        var pbnum = $(this).data('pbnum');
        var due_payment = $(this).find('option:selected').data('duepayment');
        localStorage.setItem('paid_by', p_val);

        $('#mean_payment_code_fe').val($(this).find('option:selected').data('code_fe'));

        $('#rpaidby').val(p_val);
        if (rete_prev = JSON.parse(localStorage.getItem('poretenciones'))) {
            total_rete_amount = parseFloat(formatDecimal(rete_prev.total_rete_amount));
            recalcular_poretenciones();
        } else {
            total_rete_amount = 0;
        }
        ttotal = parseFloat(formatDecimal($('#total').text()));
        tds = parseFloat(formatDecimal($('#tds').text()));
        tship = parseFloat(formatDecimal($('#poshipping').val()));
        paid_by = $('#paid_by_1').val();
        if (paid_by != 'Credito' && paid_by != 'expense_causation') {
            $('#div_affects_register').css('display', '');
            // $('#amount_1').val(parseFloat(formatDecimal($('#gtotal').text())));
            set_trm_input_payment(parseFloat(formatDecimal(pogtotal)));
            // ACÁ REVISAR AMOUNT 1
        } else {
            $('#div_affects_register').css('display', 'none');
            $('#payment_affects_register').iCheck('uncheck');
        }
        if (p_val == 'cash' ||  p_val == 'other') {
            $('.pcheque_'+pbnum).hide();
            $('.pcc_'+pbnum).hide();
            $('.pcash_'+pbnum).show();
            $('#payment_note_'+pbnum).focus();
            $('.div_popayment_term_'+pbnum).hide();
            $('#add_more_payments').prop('disabled', false);
        } else if (p_val == 'CC') {
            $('.pcheque_'+pbnum).hide();
            $('.pcash_'+pbnum).hide();
            $('.pcc_'+pbnum).show();
            $('#pcc_no_'+pbnum).focus();
            $('.div_popayment_term_'+pbnum).hide();
            $('#add_more_payments').prop('disabled', false);
        } else if (p_val == 'Cheque') {
            $('.pcc_'+pbnum).hide();
            $('.pcash_'+pbnum).hide();
            $('.pcheque_'+pbnum).show();
            $('#cheque_no_'+pbnum).focus();
            $('.div_popayment_term_'+pbnum).hide();
            $('#add_more_payments').prop('disabled', false);
        } else {
            $('.pcheque_'+pbnum).hide();
            $('.pcc_'+pbnum).hide();
            $('.pcash_'+pbnum).hide();
            $('.div_popayment_term_'+pbnum).hide();
            $('#add_more_payments').prop('disabled', false);
        }
        if (p_val == 'gift_card') {
            $('.gc_'+pbnum).show();
            $('.ngc_'+pbnum).hide();
            $('#gift_card_no_'+pbnum).focus();
        } else {
            $('.ngc_'+pbnum).show();
            $('.gc_'+pbnum).hide();
            $('#gc_details_'+pbnum).html('');
        }
        if (p_val == 'expense_causation') {
            $('.div_popayment_term_'+pbnum).show();
        }
        if (due_payment == 1 || p_val == 'Credito') {
            $('.due_payment_'+pbnum).val(1);
            $('.pcc_'+pbnum).hide();
            $('.pcash_'+pbnum).hide();
            $('.pcheque_'+pbnum).hide();
            $('.div_popayment_term_'+pbnum).show();
            input_slpayment_term = $('.div_popayment_term_'+pbnum).find('input');
            if ($('#supplierpaymentterm').val()) {
                $(input_slpayment_term).val($('#supplierpaymentterm').val()).trigger('change');
            }
            $('#add_more_payments').prop('disabled', true);
            var index = $($(this)).index('select.paid_by');
            num_due = 0;
            $.each($('select.paid_by'), function(index, select){
                if ($(select).val() == 'Credito') {
                    num_due++;
                }
            });
            if (num_due > 1) {
                $(this).select2('val', 'cash').trigger('change');
                command: toastr.error('No puede escoger más de una forma de pago a crédito.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        } else {
            $('.due_payment_'+pbnum).val(0);
        }
        if (p_val == 'expense_causation') {
            $('.payment').fadeOut();
        } else {
            $('.payment').fadeIn();
        }
    });

    if (paid_by = localStorage.getItem('paid_by')) {
        var p_val = paid_by;
        $('.paid_by').select2("val", paid_by);
        setTimeout(function() {
            $('.paid_by').trigger('change');
        }, 850);
        $('#rpaidby').val(p_val);
        if (p_val == 'cash' ||  p_val == 'other') {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').show();
            $('#payment_note_1').focus();
        } else if (p_val == 'CC') {
            $('.pcheque_1').hide();
            $('.pcash_1').hide();
            $('.pcc_1').show();
            $('#pcc_no_1').focus();
        } else if (p_val == 'Cheque') {
            $('.pcc_1').hide();
            $('.pcash_1').hide();
            $('.pcheque_1').show();
            $('#cheque_no_1').focus();
        } else {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').hide();
        }
        if (p_val == 'gift_card') {
            $('.gc').show();
            $('.ngc').hide();
            $('#gift_card_no').focus();
        } else {
            $('.ngc').show();
            $('.gc').hide();
            $('#gc_details').html('');
        }


        if (p_val == 'cash') {
            $('#div_affects_register').css('display', '');
            // $('#payment_affects_registeriCheck('check');
        } else {
            $('#div_affects_register').css('display', 'none');
            // $('#payment_affects_register').iCheck('uncheck');
        }
    }
    // clear localStorage and reload
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('poitems')) {
                    localStorage.removeItem('poitems');
                }
                if (localStorage.getItem('podiscount')) {
                    localStorage.removeItem('podiscount');
                }
                if (localStorage.getItem('pobiller')) {
                    localStorage.removeItem('pobiller');
                }
                if (localStorage.getItem('potax2')) {
                    localStorage.removeItem('potax2');
                }
                if (localStorage.getItem('poshipping')) {
                    localStorage.removeItem('poshipping');
                }
                if (localStorage.getItem('poref')) {
                    localStorage.removeItem('poref');
                }
                if (localStorage.getItem('powarehouse')) {
                    localStorage.removeItem('powarehouse');
                }
                if (localStorage.getItem('ponote')) {
                    localStorage.removeItem('ponote');
                }
                if (localStorage.getItem('posupplier')) {
                    localStorage.removeItem('posupplier');
                }
                if (localStorage.getItem('pocurrency')) {
                    localStorage.removeItem('pocurrency');
                }
                if (localStorage.getItem('poextras')) {
                    localStorage.removeItem('poextras');
                }
                if (localStorage.getItem('podate')) {
                    localStorage.removeItem('podate');
                }
                if (localStorage.getItem('postatus')) {
                    localStorage.removeItem('postatus');
                }
                if (localStorage.getItem('popayment_term')) {
                    localStorage.removeItem('popayment_term');
                }
                if (localStorage.getItem('poretenciones')) {
                    localStorage.removeItem('poretenciones');
                }
                if (localStorage.getItem('poorderdiscountmethod')) {
                    localStorage.removeItem('poorderdiscountmethod');
                }
                if (localStorage.getItem('poorderdiscounttoproducts')) {
                    localStorage.removeItem('poorderdiscounttoproducts');
                }
                if (localStorage.getItem('othercurrency')) {
                    localStorage.removeItem('othercurrency');
                }
                if (localStorage.getItem('othercurrencycode')) {
                    localStorage.removeItem('othercurrencycode');
                }
                if (localStorage.getItem('othercurrencytrm')) {
                    localStorage.removeItem('othercurrencytrm');
                }
                if (localStorage.getItem('popayment_status')) {
                    localStorage.removeItem('popayment_status');
                }
                if (localStorage.getItem('purchase_type')) {
                    localStorage.removeItem('purchase_type');
                }
                if (localStorage.getItem('po_form_import')) {
                    localStorage.removeItem('po_form_import');
                }
                if (localStorage.getItem('potag')) {
                    localStorage.removeItem('potag');
                }
                if (localStorage.getItem('new_options_assigned')) {
                    localStorage.removeItem('new_options_assigned');
                }

                
                localStorage.removeItem('poreference_duplicated');

                $('#modal-loading').show();
                location.reload();
            }
        });
    });
    $('#add_purchase').on('click', function(){
        if (po_edit) {
            if ($('#purchase_date_changed').val() == 0) {
                bootbox.confirm({
                    message: "La fecha de la compra se actualizará con la actual del sistema. ¿Desea que se actualice o desea mantener la registrada?",
                    buttons: {
                        confirm: {
                            label: 'Si, actualizar',
                            className: 'btn-success send-submit-sale btn-full'
                        },
                        cancel: {
                            label: 'No, dejar la registrada',
                            className: 'btn-danger btn-full'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            send_purchase();
                        } else {
                            $('#purchase_date_changed').val(1);
                            setTimeout(function() {
                                send_purchase();
                            }, 850);

                        }
                    }
                });
            } else {
                send_purchase();
            }
        } else {
            if (site.settings.purchase_send_advertisement == 1) {
                $('#finishPurchase').modal('show');
            } else {
                send_purchase();
            }
        }
    });

    $('.padv_cancel').on('click', function(){
        $('#finishPurchase').modal('hide');
    });
    $('.padv_send_pending').on('click', function(){
        $('#postatus').select2('val', 'pending').trigger('change');
        setTimeout(function() {
            send_purchase();
        }, 1000);
    });
    $('.padv_send').on('click', function(){
        send_purchase();
    });

    function send_purchase(){
        if ($('#purchase_form').valid()) {
            if (localStorage.getItem('poretenciones')) {
                localStorage.removeItem('poretenciones');
            }
            var total_paid = 0;
            $.each($('.amount'), function(index, amount){
                total_paid += $(amount).val();
            });
            if ($('#postatus').val() == 'received' && total_paid < _gTotal && !$('#purchase_payment_term').val()) {
                bootbox.prompt({
                    title: lang.paid_l_t_payable,
                    placeholder: lang.days_to_pay,
                    inputType: 'text',
                    callback: function (result) {
                        if (result == null) {
                            $('#purchase_payment_term').val(0);
                        } else {
                            if (result < 0) { result = 0 }
                            $('#purchase_payment_term').val(result);
                            $('#loading').fadeIn();
                            if (localStorage.getItem('poretenciones')) {
                                localStorage.removeItem('poretenciones');
                            }
                            $('#purchase_form').submit();
                        }
                    }
                });
            } else {
                $('#loading').fadeIn();
                if (localStorage.getItem('poretenciones')) {
                    localStorage.removeItem('poretenciones');
                }
                $('#purchase_form').submit();
            }
        }

    }

    // save and load the fields in and/or from localStorage
    var $supplier = $('#posupplier'), $currency = $('#pocurrency');

    $('#poref').change(function (e) {
        localStorage.setItem('poref', $(this).val());
    });

    $('#poref').on('keyup', function(e){
        // this.value = this.value.replace(/[^0-9]/g,'');
        validate_supplier_ref();
    });

    if (poref = localStorage.getItem('poref')) {
        $('#poref').val(poref);
    }
    $('#powarehouse').change(function (e) {
        localStorage.setItem('powarehouse', $(this).val());
    });
    if (powarehouse = localStorage.getItem('powarehouse')) {
        $('#powarehouse').select2("val", powarehouse);
    }

        $('#ponote').redactor('destroy');
        $('#ponote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('ponote', v);
            }
        });
        if (ponote = localStorage.getItem('ponote')) {
            $('#ponote').redactor('set', ponote);
        }
        $('#posupplier').change(function (e) {
            localStorage.setItem('posupplier', $(this).val());
            $('#supplier_id').val($(this).val());
            validate_supplier_ref();
            supplier_deposit_balance();
            supplier_retentions();
            set_supplier_payment();
        });
        if (posupplier = localStorage.getItem('posupplier')) {
            $supplier.val(posupplier).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10,
                            ptype: potype,
                            support_document: support_document,
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
            if (!po_edit) {
                // $supplier.select2('readonly', true);
            }
            $('#posupplier').trigger('change');
            setTimeout(function() {
                validate_supplier_ref();
            }, 850);
        } else {
            nsSupplier();
            setTimeout(function() {
                validate_supplier_ref();
            }, 850);
        }
    if (localStorage.getItem('poextras')) {
        $('#extras').iCheck('check');
        $('#extras-con').show();
    }
    $('#extras').on('ifChecked', function () {
        localStorage.setItem('poextras', 1);
        $('#extras-con').slideDown();
    });
    $('#extras').on('ifUnchecked', function () {
        localStorage.removeItem("poextras");
        $('#extras-con').slideUp();
    });
    $(document).on('change', '.rexpiry', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        poitems[item_id].row.expiry = $(this).val();
        localStorage.setItem('poitems', JSON.stringify(poitems));
    });
    // prevent default action upon enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });
    // Order tax calcuation
    if (site.settings.tax2 != 0) {
        $('#potax2').change(function () {
            localStorage.setItem('potax2', $(this).val());
            loadItems();
            return;
        });
    }
    // Order discount calcuation
    var old_podiscount;
    $('#podiscount').focus(function () {
        old_podiscount = $(this).val();
    }).change(function () {
        var pod = $(this).val() ? $(this).val() : 0;
        if (is_valid_discount(pod)) {
            localStorage.removeItem('podiscount');
            localStorage.setItem('podiscount', pod);
            loadItems();
            return;
        } else {
            $(this).val(old_podiscount);
            bootbox.alert(lang.unexpected_value);
            return;
        }

    });
    /* ----------------------
     * Delete Row Method
     * ---------------------- */
     $(document).on('click', '.podel', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        delete poitems[item_id];
        row.remove();
        if(poitems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('poitems', JSON.stringify(poitems));
            loadItems();
            if ($('#prorate_shipping_cost').val() == 1 && localStorage.getItem('poshipping')) {
                setTimeout(function() {
                    loadItems();
                }, 1000);
            }
            return;
        }
    });
    /* -----------------------
     * Edit Row Modal Hanlder
     ----------------------- */
    $(document).on('click', '.edit', function () {
        $('.tax_changed_warning').fadeOut();
        $('#ptax').select2('readonly', false);
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        if (potype == 1) {
            if (site.settings.update_product_tax_from_purchase == 0) {
                $('#ptax').select2('readonly', true);
            }
            if (($('#order_discount_method').val() == 1 || $('#order_discount_method').val() == 4) && $('#podiscount').val()) {
                $('#pdiscount').prop('readonly', true);
                $('.error_order_discount').fadeIn();
            } else {
                $('#pdiscount').prop('readonly', false);
                $('.error_order_discount').fadeOut();
            }
            var row = $(this).closest('tr');
            var row_id = row.attr('id');
            item_id = row.attr('data-item-id');
            let poitems = JSON.parse(localStorage.getItem('poitems'));
            item = poitems[item_id];
            var qty = row.children().children('.rquantity').val(),
            product_option = row.children().children('.roption').val(),
            unit_cost = formatDecimal(item.row.real_unit_cost),
            discount = row.children().children('.rdiscount').val();
            $('#prModalLabel').text(item.row.name);
            $('#prModalLabelCode').text(item.row.code);
            var real_unit_cost = item.row.real_unit_cost;
            var net_cost = real_unit_cost;
            if (site.settings.tax1) {
                if (support_document == 1 && site.settings.product_default_exempt_tax_rate == 1) {
                    $('#ptax').select2('val', site.settings.product_default_exempt_tax_rate);
                    $('#ptax').prop('disabled', true);
                } else {
                    $('#ptax').select2('val', item.row.tax_rate);
                }
                $('#ptax2').val(item.row.consumption_purchase_tax);
                if (item.row.purchase_tax_rate_2_percentage) {
                    $('.ptax2_div').removeClass('col-sm-8').addClass('col-sm-4');
                    $('#ptax2_percentage').val(item.row.purchase_tax_rate_2_percentage);
                    $('.ptax2_percentage_div').fadeIn();
                } else {
                    $('.ptax2_div').removeClass('col-sm-4').addClass('col-sm-8');
                    $('#ptax2_percentage').val('');
                    $('.ptax2_percentage_div').fadeOut();
                }
                $('#old_tax').val(item.row.tax_rate);
                var pr_tax = item.row.tax_rate, pr_tax_val = 0;
                if (pr_tax !== null && pr_tax != 0) {
                    $.each(tax_rates, function () {
                        if(this.id == pr_tax){
                            if (this.type == 1) {

                                if (poitems[item_id].row.tax_method == 0) {
                                    pr_tax_val = formatDecimal((((real_unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                    pr_tax_rate = formatDecimal(this.rate) + '%';
                                    net_cost -= pr_tax_val;
                                } else {
                                    pr_tax_val = formatDecimal((((real_unit_cost) * parseFloat(this.rate)) / 100));
                                    pr_tax_rate = formatDecimal(this.rate) + '%';
                                }

                            } else if (this.type == 2) {

                                pr_tax_val = parseFloat(this.rate);
                                pr_tax_rate = this.rate;

                            }
                        }
                    });
                }
            }
            net_cost = formatDecimals(net_cost);
            if (site.settings.product_serial !== 0) {
                $('#pserial').val(row.children().children('.rserial').val());
            }
            var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
            if(item.options !== false) {
                var o = 1;
                opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
                $.each(item.options, function () {
                    if(o == 1) {
                        if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                    }
                    $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                    o++;
                });
            }

            uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
            $.each(item.units, function () {
                if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                    if(this.product_unit_id == item.row.product_unit_id_selected) {
                        $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.product_unit_id, text: this.name}).appendTo(uopt);
                    }
                } else {
                    if(this.id == item.row.unit) {
                        $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                    }
                }
            });
            if (item.options === false ) { $('.variants_div').fadeOut(); }
            $('#poptions-div').html(opt);
            $('#punits-div').html(uopt);
            $('select.select').select2({minimumResultsForSearch: 7});
            $('#pquantity').val(qty);
            $('#old_qty').val(qty);
            $('#pcost_net').val(trmrate * (net_cost));
            $('#pcost').val(trmrate * formatDecimal(unit_cost+parseFloat(item.row.consumption_purchase_tax)+(item.row.tax_method == 1 ? pr_tax_val : 0)));
            $('#pcost_ipoconsumo').val('');
            $('#punit_cost').val(formatDecimal(parseFloat(unit_cost)+parseFloat(pr_tax_val)+parseFloat(item.row.consumption_purchase_tax)));
            $('#poption').select2('val', item.row.option);
            $('#old_cost').val(unit_cost);
            $('#row_id').val(row_id);
            $('#item_id').val(item_id);
            $('#pexpiry').val(row.children().children('.rexpiry').val());
            $('#pdiscount').val(getDiscountAssigned(discount, trmrate, 1));
            $('#net_cost').text(formatMoney(trmrate * net_cost));
            $('#pro_tax').text(formatMoney(trmrate * (parseFloat(pr_tax_val) + parseFloat(item.row.consumption_purchase_tax))));
            $('#psubtotal').val('');
            $('#psupplier_part_no').val(item.row.supplier_part_no !== undefined ? item.row.supplier_part_no : "");
            $('#prModal').appendTo("body").modal('show');
            if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
                unit_selected_real_quantity = 1;
                units = item.units;
                unit_selected = units[item.row.product_unit_id_selected];
                unit_selected_quantity = qty;
                if (unit_selected.operator == '*') {
                    unit_selected_quantity = qty / unit_selected.operation_value;
                    unit_selected_real_quantity = 1 * unit_selected.operation_value;
                } else if (unit_selected.operator == '/') {
                    unit_selected_quantity = qty * unit_selected.operation_value;
                    unit_selected_real_quantity = 1 / unit_selected.operation_value;
                } else if (unit_selected.operator == '+') {
                    unit_selected_quantity = qty - unit_selected.operation_value;
                } else if (unit_selected.operator == '-') {
                    unit_selected_quantity = qty + unit_selected.operation_value;
                }
                $('#pquantity').val(unit_selected_quantity);
                $('#punit').select2('val', item.row.product_unit_id_selected).trigger('change');
                $('#pproduct_unit_cost').val(formatDecimal((parseFloat(unit_cost) + parseFloat(item.row.consumption_purchase_tax)) * unit_selected_real_quantity));
                $('#pproduct_unit_cost').trigger('change');
                $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
                $('#pcost_um_net').val(formatDecimal((net_cost) * unit_selected.operation_value));
                $('#pcost_um').val(formatDecimal(unit_cost * unit_selected.operation_value));
                $('#pcost_um_total_net').val(formatDecimal(unit_selected_quantity * (net_cost * unit_selected.operation_value)));
                $('#pcost_um_total').val(formatDecimal(unit_selected_quantity * (unit_cost * unit_selected.operation_value)));
                $('.um_name').html('"'+unit_selected.name+'"');

                $('#pcost_um_total_net').attr('readonly', true);
                $('#pcost_um_total').attr('readonly', true);
                $('#pcost').attr('readonly', true);
                $('#pcost_net').attr('readonly', true);
            }
            $('.purchase_form_edit_product_unit_name').text($('#punit option:selected').text());

            if (item.row.cnt_units_prices !== undefined && item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) && item.row.promotion != 1) {
                if (item.units !== undefined) {
                    sale_consumption_tax = 0;
                    if (site.settings.ipoconsumo) {
                        sale_consumption_tax = item.row.consumption_sale_tax;
                    }
                    $('.pgprice_div').fadeOut();
                    $.each(item.units, function(index, unit){
                        $('.pgprice_div[data-pgid="'+index+'"]').fadeIn();

                        if (site.settings.precios_por_unidad_presentacion == 2 && sale_consumption_tax > 0) {
                           if (isset(unit.operator) && unit.operator) {
                               if (unit.operator == "*") {
                                   sale_consumption_tax = sale_consumption_tax * unit.operation_value;
                               } else if (unit.operator == "/") {
                                   sale_consumption_tax = sale_consumption_tax / unit.operation_value;
                               }
                           }
                        }
                        $('.pgprice[data-pgid="'+index+'"]').val(formatDecimal(unit.valor_unitario_old) + formatDecimal(sale_consumption_tax));
                        if (unit.margin_update_price > 0) {
                            $('.pgmargin[data-pgid="'+index+'"]').val(formatDecimal(unit.margin_update_price));
                            if (site.settings.update_prices_by_margin_formula == 1) {
                                precio = unit_cost / (1 - (unit.margin_update_price / 100));
                            } else {
                                precio = unit_cost * (1 + (unit.margin_update_price / 100));
                            }
                            $('.pgnew_price[data-pgid="'+index+'"]').val(formatDecimal(precio));
                        } else {
                            $('.pgnew_price[data-pgid="'+index+'"]').val(formatDecimal(unit.valor_unitario_old) + formatDecimal(sale_consumption_tax));
                            margin = formatDecimals((((unit.valor_unitario_old - $('#pcost').val()) / unit.valor_unitario_old) * 100), 2);
                            $('.pgmargin[data-pgid="'+index+'"]').val(formatDecimal(margin));
                        }
                    });
                } else {
                    $('.div_update_prices').fadeOut();
                }
            } else {
                if (item.product_price_groups !== undefined && site.settings.update_prices_from_purchases == 1) {
                    sale_consumption_tax = 0;
                    if (site.settings.ipoconsumo) {
                        sale_consumption_tax = item.row.consumption_sale_tax;
                    }
                    $.each(item.product_price_groups, function(index, pg){
                        $('.pgprice[data-pgid="'+index+'"]').val(formatDecimal(pg.price) + formatDecimal(sale_consumption_tax));
                    });
                } else {
                    $('.div_update_prices').fadeOut();
                }
            }
        } else if ((potype == 2 || potype == 3)) {
            var row = $(this).closest('tr');
            var row_id = row.attr('id');
            item_id = row.attr('data-item-id');
            item = poitems[item_id];
            var qty = row.children().children('.rquantity').val(),
            product_option = row.children().children('.roption').val(),
            unit_cost = formatDecimal(row.children().children('.rucost').val()),
            discount = row.children().children('.rdiscount').val();
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item.row.option && this.cost != 0 && this.cost != '' && this.cost != null) {
                        unit_cost = parseFloat(item.row.real_unit_cost)+parseFloat(this.cost);
                    }
                });
            }
            var real_unit_cost = item.row.real_unit_cost;
            var net_cost = unit_cost;
            $('#prModalLabel').text(item.row.name + ' (' + item.row.code + ')');
            if (site.settings.tax1) {
                if (support_document == 1 && site.settings.product_default_exempt_tax_rate == 1) {
                    $('#ptax').select2('val', site.settings.product_default_exempt_tax_rate);
                    $('#ptax').prop('disabled', true);
                } else {
                    $('#ptax').select2('val', item.row.tax_rate);
                }

                $('#ptax_2').select2('val', item.row.tax_rate_2);
                if (item.tax_rate.type == 1) {
                    $('#ptax_value').prop('readonly', 'readonly');
                }
                if (item.tax_rate_2.type == 1) {
                    $('#ptax_2_value').prop('readonly', 'readonly');
                } //segundo iva
                if (item.tax_rate == false) {
                    $('#ptax').select2('val', '').select2('readonly', 'readonly');
                    $('#ptax_value').prop('readonly', 'readonly');
                }
                if (item.tax_rate_2 == false) {
                    $('#ptax_2').select2('val', '').select2('readonly', 'readonly');
                    $('#ptax_2_value').prop('readonly', 'readonly');
                }
                $('#old_tax').val(item.row.tax_rate);
                var item_discount = 0, ds = discount ? discount : '0';
                if (ds.indexOf("%") !== -1) {
                    var pds = ds.split("%");
                    if (!isNaN(pds[0])) {
                        item_discount = formatDecimal(parseFloat(((unit_cost) * parseFloat(pds[0])) / 100));
                    } else {
                        item_discount = formatDecimal(parseFloat(ds));
                    }
                } else {
                    item_discount = formatDecimal(parseFloat(ds));
                }
                net_cost -= item_discount;
                real_net_cost = net_cost;
                var pr_tax = item.row.tax_rate, pr_tax_val = 0;
                if (pr_tax !== null && pr_tax != 0) {
                    $.each(tax_rates, function () {
                        if(this.id == pr_tax){
                            if (this.type == 1) {
                                if (poitems[item_id].row.tax_method == 0) {
                                    pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                    pr_tax_rate = formatDecimal(this.rate) + '%';
                                    net_cost -= pr_tax_val;
                                } else {
                                    pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100));
                                    pr_tax_rate = formatDecimal(this.rate) + '%';
                                }
                                $('#ptax_value').prop('readonly', 'readonly');
                            } else if (this.type == 2) {
                                pr_tax_val = parseFloat(this.rate);
                                pr_tax_rate = this.rate;
                                $('#ptax_value').removeAttr('readonly');
                            }
                            $('#ptax_value').val(formatDecimal(trmrate * pr_tax_val));
                        }
                    });
                }
                var pr_tax_2 = item.row.tax_rate_2, pr_tax_2_val = 0;
                if (pr_tax_2 !== null && pr_tax_2 != 0) {
                    $.each(tax_rates, function () {
                        if(this.id == pr_tax_2){
                            if (this.type == 1) {
                                if (poitems[item_id].row.tax_method == 0) {
                                    pr_tax_2_val = formatDecimal((((real_net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                    pr_tax_2_rate = formatDecimal(this.rate) + '%';
                                    net_cost -= pr_tax_2_val;
                                } else {
                                    pr_tax_2_val = formatDecimal((((real_net_cost) * parseFloat(this.rate)) / 100));
                                    pr_tax_2_rate = formatDecimal(this.rate) + '%';
                                }
                                $('#ptax_2_value').prop('readonly', 'readonly');
                            } else if (this.type == 2) {
                                pr_tax_2_val = parseFloat(this.rate);
                                pr_tax_2_rate = this.rate;
                                $('#ptax_2_value').removeAttr('readonly');
                            }
                            $('#ptax_2_value').val(formatDecimal(trmrate * pr_tax_2_val));
                        }
                    });
                }
            }

            if (site.settings.product_serial !== 0) {
                $('#pserial').val(row.children().children('.rserial').val());
            }

            var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
            if(item.options !== false) {
                var o = 1;
                opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
                $.each(item.options, function () {
                    if(o == 1) {
                        if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                    }
                    $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                    o++;
                });
            } else {
                product_variant = 0;
            }

            uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
            if (item.units) {
                uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
                $.each(item.units, function () {
                    if(this.id == item.row.unit) {
                        $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                    }
                });
            }
            $('#poptions-div').html(opt);
            $('#punits-div').html(uopt);
            $('select.select').select2({minimumResultsForSearch: 7});
            $('#pquantity').val(qty);
            $('#old_qty').val(qty);
            $('#pcost').val(trmrate * unit_cost);
            $('#punit_cost').val(formatDecimal(parseFloat(unit_cost)+parseFloat(pr_tax_val)));
            $('#poption').select2('val', item.row.option);
            $('#old_cost').val(unit_cost);
            $('#row_id').val(row_id);
            $('#item_id').val(item_id);
            $('#pserial').val(row.children().children('.rserial').val());
            $('#pdiscount').val(discount);
            $('#net_cost').text(formatMoney(trmrate * (net_cost-item_discount)));
            $('#pro_tax').text(formatMoney(trmrate * (pr_tax_val + pr_tax_2_val)));
            $('#pro_total').text(formatMoney(trmrate * (net_cost-item_discount+pr_tax_val + pr_tax_2_val)));
            $('#prModal').appendTo("body").modal('show');
            $('#pname').val(item.row.name);
        }
    });

    $('#prModal').on('shown.bs.modal', function (e) {
        if($('#poption').select2('val') != '') {
            $('#poption').select2('val', product_variant);
            product_variant = 0;
        }
    });

    $(document).on('change', '.rserial', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        poitems[item_id].row.serial = $(this).val();
        localStorage.setItem('poitems', JSON.stringify(poitems));
    });

    $(document).on('change', '.rvariant', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        let poitems = JSON.parse(localStorage.getItem('poitems'));   
        poitems[item_id].row.option = $(this).val();
        localStorage.setItem('poitems', JSON.stringify(poitems));
        loadItems(true, true);
    });
    $(document).on('change', '.rpcost', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        cost = (formatDecimal($(this).val())) / getTrmRate();
        rpico = 0;
        if (poitems[item_id].row.purchase_tax_rate_2_percentage) {
            ipoconsumo_percentage_calculated =  calculate_second_tax(poitems[item_id].row.purchase_tax_rate_2_percentage, cost, poitems[item_id].row.tax_rate, poitems[item_id].row.tax_method, true);
            poitems[item_id].row.consumption_purchase_tax = ipoconsumo_percentage_calculated[1];
            rpico = ipoconsumo_percentage_calculated[1];
        }
        if (site.settings.tax_method == 1) {
            item_tax = calculateTax(poitems[item_id].tax_rate, cost, 0);
            item_tax_val = item_tax[0];
            cost = cost - item_tax_val;
        }
        poitems[item_id].row.real_unit_cost = cost - rpico;
        localStorage.setItem('poitems', JSON.stringify(poitems));
        reset_purchase_rete_base('costo');
        loadItems();
    });
    $(document).on('keydown', '.rpcost', function (e) {
        input = $(this);
        if (e.keyCode == 13) {
            input.trigger('change');
        }
    });
    $(document).on('change', '.rpnet_cost', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        var net_cost = unit_cost = parseFloat($(this).val()) / getTrmRate();
        rpico = 0;
        if (poitems[item_id].row.purchase_tax_rate_2_percentage) {
            var ipoconsumo_calculated = poitems[item_id].row.purchase_tax_rate_2_percentage ? calculate_second_tax(poitems[item_id].row.purchase_tax_rate_2_percentage, net_cost, 0, poitems[item_id].row.tax_method) :  [0, poitems[item_id].row.consumption_purchase_tax];
            rpico =  ipoconsumo_calculated[1];
            poitems[item_id].row.consumption_purchase_tax = ipoconsumo_calculated[1];
        }
        if (potype == 1 && site.settings.tax_method == 0) {
            var pr_tax = poitems[item_id].row.tax_rate, item_tax_method = poitems[item_id].row.tax_method;
            var pr_tax_val = 0;
            pr_tax_val = calculateTax(tax_rates[pr_tax], net_cost, 1);
            pr_tax_val = pr_tax_val[0];
            unit_cost = formatDecimal(formatDecimal(net_cost) + formatDecimal(pr_tax_val));
        }
        poitems[item_id].row.real_unit_cost = formatDecimal(unit_cost);
        localStorage.setItem('poitems', JSON.stringify(poitems));
        reset_purchase_rete_base('costo');
        loadItems();
    });
    $(document).on('keydown', '.rpnet_cost', function (e) {
        input = $(this);
        if (e.keyCode == 13) {
            input.trigger('change');
        }
    });
    $(document).on('change', '#pcost_um_net, #pcost_um_total_net', function(){
        if (potype == 1) {
            var row = $('#' + $('#row_id').val());
            var item_id = row.attr('data-item-id');
            var pcost = parseFloat($('#pcost').val());
            var pcost_um_net = parseFloat($('#pcost_um_net').val());
            var pcost_um_total_net = parseFloat($('#pcost_um_total_net').val());
            var item = poitems[item_id];
            var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
            if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) {
                punit_quantity = $('#punit_quantity').val();
                pquantity = $('#pquantity').val();
                unit_um_cost_unit_net = formatDecimal(pcost_um_net / punit_quantity); //costo UNIDAD por um unitario antes de IVA
                ptax_unit_um_cost_unit = calculateTax(tax_rates[pr_tax], unit_um_cost_unit_net, 1);
                unit_um_cost_unit = formatDecimal(unit_um_cost_unit_net) + formatDecimal(ptax_unit_um_cost_unit[0]);
                //costo Unidad por um unitario con iva
                unit_um_cost_total_unit_net = formatDecimal(pcost_um_total_net / (punit_quantity * pquantity)); //costo UNIDAD por um total
                ptax_unit_um_cost_total_unit = calculateTax(tax_rates[pr_tax], unit_um_cost_total_unit_net, 1);
                unit_um_cost_total_unit = formatDecimal(unit_um_cost_total_unit_net) + formatDecimal(ptax_unit_um_cost_total_unit[0]);
                //costo Unidad por um unitario con iva
                if (pcost != unit_um_cost_unit) {
                    $('#pcost').val(formatDecimals(unit_um_cost_unit)).trigger('change');
                    $('#pcost_um_total_net').val(formatDecimals((unit_um_cost_unit * punit_quantity) * pquantity));
                } else if (pcost != unit_um_cost_total_unit) {
                    $('#pcost').val(formatDecimals(unit_um_cost_total_unit)).trigger('change');
                    $('#pcost_um_net').val(formatDecimals(punit_quantity * unit_um_cost_total_unit));
                }
            }
        }
    });
    $(document).on('change', '#pcost_um, #pcost_um_total', function(){
        if (potype == 1) {
            var row = $('#' + $('#row_id').val());
            var item_id = row.attr('data-item-id');
            var pcost = parseFloat($('#pcost').val());
            var pcost_um = parseFloat($('#pcost_um').val());
            var pcost_um_total = parseFloat($('#pcost_um_total').val());
            var item = poitems[item_id];
            var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
            if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) {
                punit_quantity = $('#punit_quantity').val();
                pquantity = $('#pquantity').val();

                unit_um_cost_unit = formatDecimal(pcost_um / punit_quantity); //costo UNIDAD por um unitario
                ptax_unit_um_cost_unit = calculateTax(tax_rates[pr_tax], unit_um_cost_unit, item.row.tax_method);
                unit_um_cost_unit_net = formatDecimal(unit_um_cost_unit) - formatDecimal(ptax_unit_um_cost_unit[0]);


                unit_um_cost_total_unit = formatDecimal(pcost_um_total / (punit_quantity * pquantity)); //costo UNIDAD por um total
                ptax_unit_um_cost_total_unit = calculateTax(tax_rates[pr_tax], unit_um_cost_total_unit, item.row.tax_method);
                unit_um_cost_total_unit_net = formatDecimal(unit_um_cost_total_unit) - formatDecimal(ptax_unit_um_cost_total_unit[0]);
                //costo Unidad por um unitario con iva
                if (pcost != unit_um_cost_unit) {
                    $('#pcost').val(formatDecimals(unit_um_cost_unit)).trigger('change');
                    $('#pcost_net').val(formatDecimals(unit_um_cost_unit_net));
                    $('#pcost_um_total').val(formatDecimals((unit_um_cost_unit * punit_quantity) * pquantity));
                    $('#pcost_um_total_net').val(formatDecimals((unit_um_cost_unit_net * punit_quantity) * pquantity));
                    $('#pcost_um_net').val(formatDecimals(punit_quantity * unit_um_cost_unit_net));
                } else if (pcost != unit_um_cost_total_unit) {
                    $('#pcost').val(formatDecimals(unit_um_cost_total_unit)).trigger('change');
                    $('#pcost_net').val(formatDecimals(unit_um_cost_total_unit_net)).trigger('change');
                    $('#pcost_um').val(formatDecimals(punit_quantity * unit_um_cost_total_unit));
                    $('#pcost_um_net').val(formatDecimals(punit_quantity * unit_um_cost_total_unit_net));
                    $('#pcost_um_total_net').val(formatDecimals((unit_um_cost_total_unit_net * punit_quantity) * pquantity));
                }

            }

        }
    });
    $(document).on('change', '.um_rpnet_cost', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        var net_cost = unit_cost = parseFloat($(this).val()) / getTrmRate();
        units = poitems[item_id].units;
        unit_selected = units[poitems[item_id].row.product_unit_id_selected];
        net_cost = net_cost / unit_selected.operation_value;
        rpico = 0;
        if (poitems[item_id].row.purchase_tax_rate_2_percentage) {
            var ipoconsumo_calculated = poitems[item_id].row.purchase_tax_rate_2_percentage ? calculate_second_tax(poitems[item_id].row.purchase_tax_rate_2_percentage, net_cost, 0, poitems[item_id].row.tax_method) :  [0, poitems[item_id].row.consumption_purchase_tax];
            rpico =  ipoconsumo_calculated[1];
            poitems[item_id].row.consumption_purchase_tax = ipoconsumo_calculated[1];
        }
        if (potype == 1 && site.settings.tax_method == 0) {
            var pr_tax = poitems[item_id].row.tax_rate, item_tax_method = poitems[item_id].row.tax_method;
            var pr_tax_val = 0;
            pr_tax_val = calculateTax(tax_rates[pr_tax], net_cost, 1);
            pr_tax_val = pr_tax_val[0];
            unit_cost = formatDecimal(formatDecimal(net_cost) + formatDecimal(pr_tax_val));
        }
        poitems[item_id].row.real_unit_cost = formatDecimal(unit_cost);
        localStorage.setItem('poitems', JSON.stringify(poitems));
        reset_purchase_rete_base('costo');
        loadItems();
    });
    $(document).on('change', '.um_rpcost', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        cost = (formatDecimal($(this).val())) / getTrmRate();
        rpico = 0;
        units = poitems[item_id].units;
        unit_selected = units[poitems[item_id].row.product_unit_id_selected];
        cost = cost / unit_selected.operation_value;
        if (poitems[item_id].row.purchase_tax_rate_2_percentage) {
            ipoconsumo_percentage_calculated =  calculate_second_tax(poitems[item_id].row.purchase_tax_rate_2_percentage, cost, poitems[item_id].row.tax_rate, poitems[item_id].row.tax_method, true);
            poitems[item_id].row.consumption_purchase_tax = ipoconsumo_percentage_calculated[1];
            rpico = ipoconsumo_percentage_calculated[1];
        }
        if (site.settings.tax_method == 1) {
            item_tax = calculateTax(poitems[item_id].tax_rate, cost, 0);
            item_tax_val = item_tax[0];
            cost = cost - item_tax_val;
        }
        poitems[item_id].row.real_unit_cost = cost - rpico;
        localStorage.setItem('poitems', JSON.stringify(poitems));
        reset_purchase_rete_base('costo');
        loadItems();
    });
    $(document).on('change', '.pgmargin', function () {
        pgid = $(this).data('pgid');
        if (site.settings.update_prices_by_margin_formula == 1) {
            precio = $('#pcost').val() / (1 - ($(this).val() / 100));
        } else {
            precio = $('#pcost').val() * (1 + ($(this).val() / 100));
        }
        if (site.settings.disable_product_price_under_cost == 1 && formatDecimals(precio) < formatDecimals($('#pcost').val())) {
            Command: toastr.error('El margen resulta en un precio por debajo del costo', 'Margen incorrecto', {onHidden : function(){}});
            $('#editItem').attr('disabled', true);
        } else {
            $('#editItem').attr('disabled', false);
        }
        $('.pgnew_price[data-pgid="'+pgid+'"]').val(formatDecimal(precio));
    });
    $(document).on('change', '.pgnew_price', function () {
        pgid = $(this).data('pgid');
        margin = formatDecimals(((($(this).val() - $('#pcost').val()) / $(this).val()) * 100), 2);
        $('.pgmargin[data-pgid="'+pgid+'"]').val(formatDecimal(margin));


        if (site.settings.disable_product_price_under_cost == 1 && formatDecimals($(this).val()) < formatDecimals($('#pcost').val())) {
            Command: toastr.error('El nuevo precio está por debajo del costo', 'Precio incorrecto', {onHidden : function(){}});
            $('#editItem').attr('disabled', true);
        } else {
            $('#editItem').attr('disabled', false);
        }
    });
    $(document).on('change', '#pcost_net', function () {
        if (potype == 1) {
            var row = $('#' + $('#row_id').val());
            var item_id = row.attr('data-item-id');
            var net_cost = parseFloat($('#pcost_net').val());
            var item = poitems[item_id];
            var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
            var ipoconsumo_calculated = item.row.purchase_tax_rate_2_percentage ? calculate_second_tax(item.row.purchase_tax_rate_2_percentage, net_cost, 0, item.row.tax_method) :  [0, item.row.consumption_purchase_tax];
            var ipoconsumo =  ipoconsumo_calculated[1];
            var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
            var pr_tax_val = 0;
            pr_tax_val = calculateTax(tax_rates[pr_tax], net_cost, 1);
            pr_tax_val = pr_tax_val[0];
            unit_cost = formatDecimal(net_cost) + formatDecimal(pr_tax_val) + formatDecimal(ipoconsumo);
            $('#pcost').val(formatDecimal(unit_cost));
            $('#net_cost').text(formatMoney(net_cost));
            $('#ptax2').val(formatDecimal(ipoconsumo));
            setTimeout(function() {
                $('#pcost_ipoconsumo').val('');
            }, 500);
            $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(ipoconsumo)));
        }
    });
    $(document).on('change', '#pcost, #pcost_ipoconsumo, #ptax, #ptax_2, #ptax_value, #ptax_2_value, #pquantity', function () {
        if (potype == 1) {
            var row = $('#' + $('#row_id').val());
            var item_id = row.attr('data-item-id');
            var unit_cost = parseFloat($('#pcost').val());
            var pcost_ipoconsumo = parseFloat($('#pcost_ipoconsumo').val());
            var item = poitems[item_id];
            if (unit_cost != item.row.cost) {
                Command: toastr.warning('El costo ha sido cambiado', 'Costo cambió', {onHidden : function(){}})
            }
            if (item.row.purchase_tax_rate_2_percentage) {
                ipoconsumo_percentage_calculated =  calculate_second_tax(item.row.purchase_tax_rate_2_percentage, unit_cost, item.row.tax_rate, item.row.tax_method, true);
                $('#pcost_net').val(ipoconsumo_percentage_calculated[0]).trigger('change');
                return true;
            }
            var ipoconsumo = item.row.consumption_purchase_tax;
            var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
            unit_cost -= ipoconsumo;
            var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
            var pr_tax_val = 0, pr_tax_rate = 0;
            
            if (pr_tax !== null && pr_tax != 0) {
                selected_tax_rate = tax_rates[pr_tax];
                if (pr_tax != item.row.tax_rate && (selected_tax_rate.rate > 0)) {
                    unit_cost = unit_cost * (1 + (parseFloat(selected_tax_rate.rate) / 100));
                    $('#pcost').val(formatDecimals(unit_cost));
                    $('#ptax').select2('readonly', true);
                    $('.tax_changed_warning').fadeIn();
                    setTimeout(function() {
                        $('#pcost').select();
                    }, 600);
                }
                if (selected_tax_rate.type == 1) {
                    if (item_tax_method == 0) {
                        pr_tax_val = formatDecimal((((unit_cost) * parseFloat(selected_tax_rate.rate)) / (100 + parseFloat(selected_tax_rate.rate))));
                        pr_tax_rate = formatDecimal(selected_tax_rate.rate) + '%';
                        unit_cost -= pr_tax_val;
                    } else {
                        pr_tax_val = formatDecimal(unit_cost) - formatDecimal(unit_cost / (1 + (selected_tax_rate.rate / 100)));
                        pr_tax_rate = formatDecimal(selected_tax_rate.rate) + '%';
                        unit_cost -= pr_tax_val;
                    }
                } else if (selected_tax_rate.type == 2) {
                    pr_tax_val = parseFloat(selected_tax_rate.rate);
                    pr_tax_rate = selected_tax_rate.rate;
                }
            }
            if (pr_tax != item.row.tax_rate && !(selected_tax_rate.rate > 0)) {
                original_tax_rate = tax_rates[item.row.tax_rate];
                if (original_tax_rate.type == 1) {
                    if (item_tax_method == 0) {
                        pr_tax_val = formatDecimal((((unit_cost) * parseFloat(original_tax_rate.rate)) / (100 + parseFloat(original_tax_rate.rate))));
                        // pr_tax_rate = formatDecimal(original_tax_rate.rate) + '%';
                        unit_cost -= pr_tax_val;
                    } else {
                        
                        pr_tax_val = formatDecimal(unit_cost) - formatDecimal(unit_cost / (1 + (original_tax_rate.rate / 100)));
                        // pr_tax_rate = formatDecimal(original_tax_rate.rate) + '%';
                        unit_cost -= pr_tax_val;
                    }
                }
                $('#pcost').val(formatDecimals(unit_cost));
                $('#ptax').select2('readonly', true);
                $('.tax_changed_warning').fadeIn();
                setTimeout(function() {
                    $('#pcost').select();
                }, 600);
                pr_tax_val = 0;
            }
            if ($('#pcost_ipoconsumo').val() > 0 && item.row.tax_method == 1  && parseFloat($('#pcost_ipoconsumo').val()) > parseFloat(ipoconsumo)) {
                $('#pcost').val(formatDecimal(unit_cost));
            } else if ($('#pcost_ipoconsumo').val() > 0 && item.row.tax_method == 0  && parseFloat($('#pcost_ipoconsumo').val()) > parseFloat(ipoconsumo)) {
                $('#pcost').val(formatDecimal(pcost_ipoconsumo));
            } else if (parseFloat($('#pcost_ipoconsumo').val()) <= parseFloat(ipoconsumo)) {
                Command: toastr.error('Por favor ingrese un costo válido', 'Valores incorrectos', {onHidden : function(){}})
                $('#pcost_ipoconsumo').val(0);
            }
            $('#pcost_net').val(formatDecimal(unit_cost));
            $('#net_cost').text(formatMoney(unit_cost));
            $('#ptax2').val(formatDecimal(ipoconsumo));
            setTimeout(function() {
                $('#pcost_ipoconsumo').val('');
            }, 500);
            $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(ipoconsumo)));

            if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
                qty = $('#pquantity').val();
                units = item.units;
                unit_selected = units[$('#punit').val()];
                $('#pcost_um').val(formatDecimal((unit_cost+pr_tax_val) * unit_selected.operation_value));
                $('#pcost_um_total').val(formatDecimal(qty * ((unit_cost+pr_tax_val) * unit_selected.operation_value)));
            }

            if (item.row.cnt_units_prices !== undefined && item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) && item.row.promotion != 1) {
                if (item.units !== undefined) {
                    sale_consumption_tax = 0;
                    if (site.settings.ipoconsumo) {
                        sale_consumption_tax = item.row.consumption_sale_tax;
                    }
                    $('.pgprice_div').fadeOut();
                    $.each(item.units, function(index, unit){
                        $('.pgprice_div[data-pgid="'+index+'"]').fadeIn();

                        if (site.settings.precios_por_unidad_presentacion == 2 && sale_consumption_tax > 0) {
                           if (isset(unit.operator) && unit.operator) {
                               if (unit.operator == "*") {
                                   sale_consumption_tax = sale_consumption_tax * unit.operation_value;
                               } else if (unit.operator == "/") {
                                   sale_consumption_tax = sale_consumption_tax / unit.operation_value;
                               }
                           }
                        }
                        $('.pgmargin[data-pgid="'+index+'"]').val(formatDecimal(unit.margin_update_price));
                        if (site.settings.update_prices_by_margin_formula == 1) {
                            precio = (unit_cost+pr_tax_val) / (1 - (unit.margin_update_price / 100));
                        } else {
                            precio = (unit_cost+pr_tax_val) * (1 + (unit.margin_update_price / 100));
                        }
                        $('.pgnew_price[data-pgid="'+index+'"]').val(formatDecimal(precio));
                    });
                }
            }

        } else if ((potype == 2 || potype == 3)) {
            var row = $('#' + $('#row_id').val());
            var item_id = row.attr('data-item-id');
            var unit_cost = parseFloat($('#pcost').val());
            var item = poitems[item_id];
            // var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
            // if (ds.indexOf("%") !== -1) {
            //     var pds = ds.split("%");
            //     if (!isNaN(pds[0])) {
            //         item_discount = parseFloat(((unit_cost) * parseFloat(pds[0])) / 100);
            //     } else {
            //         item_discount = parseFloat(ds);
            //     }
            // } else {
            //     item_discount = parseFloat(ds);
            // }
            // unit_cost -= item_discount;
            real_unit_cost = unit_cost;
            var pr_tax = $('#ptax').val();
            var pr_tax_2 = $('#ptax_2').val();
            var item_tax_method = item.row.tax_method;
            var pr_tax_val = 0, pr_tax_rate = 0;
            var pr_tax_2_val = 0, pr_tax_2_rate = 0;
            if ((pr_tax !== null && pr_tax != 0) || (pr_tax_2 !== null && pr_tax_2 != 0)) {
                $.each(tax_rates, function () {
                    type_tax = this.type;
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            if (item_tax_method == 0) {
                                pr_tax_val = formatDecimal(((unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                unit_cost -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal((((unit_cost) * parseFloat(this.rate)) / 100));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }
                            $('#ptax_value').val(pr_tax_val).prop('readonly', 'readonly');
                        } else if (this.type == 2) {
                            $('#ptax_value').removeAttr('readonly');
                            rate_tax_c = $('#ptax_value').val();
                            pr_tax_val = parseFloat(rate_tax_c);
                            pr_tax_rate = rate_tax_c;
                        }
                    }
                    if(this.id == pr_tax_2){
                        if (this.type == 1) {
                            if (item_tax_method == 0) {
                                pr_tax_2_val = formatDecimal(((real_unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                                pr_tax_2_rate = formatDecimal(this.rate) + '%';
                                unit_cost -= pr_tax_2_val;
                            } else {
                                pr_tax_2_val = formatDecimal((((real_unit_cost) * parseFloat(this.rate)) / 100));
                                pr_tax_2_rate = formatDecimal(this.rate) + '%';
                            }
                            $('#ptax_2_value').val(pr_tax_2_val).prop('readonly', 'readonly');
                        } else if (this.type == 2) {
                            $('#ptax_2_value').removeAttr('readonly');
                            rate_tax_c = $('#ptax_2_value').val();
                            pr_tax_2_val = parseFloat(rate_tax_c);
                            pr_tax_2_rate = rate_tax_c;
                        }
                    }
                });
            }
            $('#net_cost').text(formatMoney(unit_cost));
            $('#pro_tax').text(formatMoney(pr_tax_val + pr_tax_2_val));
            $('#pro_total').text(formatMoney(unit_cost + pr_tax_val + pr_tax_2_val));
        }
    });
    $(document).on('change', '#punit', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = poitems[item_id];
        unit_cost = $('#pcost').val();
        unit = $('#punit').val();
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_purchase_tax));
        if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected = item.units[unit];
            var ipoconsumo_um = 0;
            if (unit_selected.operation_value > 1) {
                if (unit_selected.operator == "*") {
                    unit_cost =  parseFloat(unit_cost) * parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) * parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    unit_cost =  parseFloat(unit_cost) / parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) * parseFloat(unit_selected.operation_value);
                }
            }
            $('#pproduct_unit_cost').val(formatDecimal(unit_cost));
            $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
            $('.um_name').html(unit_selected.name);
            setTimeout(function() {
                $('#pproduct_unit_cost').trigger('change');
            }, 850);
        }
        $('.purchase_form_edit_product_unit_name').text($('#punit option:selected').text());
    });
    $(document).on('change', '#pproduct_unit_cost', function(){
        p_unit_cost = $(this).val();
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = poitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), acost = 0;
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_purchase_tax));
        if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 1) {
                valor = unit_selected.operation_value;
                if (unit_selected.operator == "*") {
                    valor =  parseFloat(p_unit_cost) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  parseFloat(p_unit_cost) * parseFloat(unit_selected.operation_value);
                }
                $('#pcost').val(valor).trigger('change');
            } else {
                $('#pcost').val(p_unit_cost).trigger('change');
            }
        }
    });
    $(document).on('click', '#calculate_unit_price', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = poitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        tax_rate = formatDecimal($('#ptax option:selected').data('taxrate')) / 100;
        tax_rate = tax_rate != 0 ? tax_rate + 1 : 1;
        var subtotal = parseFloat($('#psubtotal').val()),
        qty = parseFloat($('#pquantity').val());
        if (item.row.tax_method == 1) { tax_rate = 1; }
        $('#pcost').val(formatDecimal(((subtotal) * tax_rate))).change();
        return false;
    });
    /* -----------------------
     * Edit Row Method
     ----------------------- */
    $(document).on('click', '#editItem', function () {
        if (site.settings.purchases_products_supplier_code == 1) {
            if (!$('#psupplier_part_no').val()) {
                $('#psupplier_part_no').focus();
                header_alert('error', 'Debe diligenciar el código para el proveedor de la compra');
                return false;
            }
        }
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        if (potype == 1) {
            var row = $('#' + $('#row_id').val());
            var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_rate = {};
            if (new_pr_tax) {
                $.each(tax_rates, function () {
                    if (this.id == new_pr_tax) {
                        new_pr_tax_rate = this;
                    }
                });
            }
            if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0 || parseFloat($('#pcost').val()) < 0) {
                $(this).val(old_row_qty);
                bootbox.alert(lang.unexpected_value);
                return;
            }
            var unit = $('#punit').val();
            var base_quantity = parseFloat($('#pquantity').val());
            if(unit != poitems[item_id].row.base_unit) {
                $.each(poitems[item_id].units, function(){
                    if (this.id == unit) {
                        base_quantity = unitToBaseQty($('#pquantity').val(), this);
                    }
                });
            }
            tpax2 = parseFloat($('#ptax2').val() ? $('#ptax2').val() : 0);
            poitems[item_id].row.fup = 1,
            poitems[item_id].row.qty = parseFloat($('#pquantity').val()),
            poitems[item_id].row.consumption_purchase_tax = tpax2,
            poitems[item_id].row.supplier_part_no = $('#psupplier_part_no').val(),
            poitems[item_id].row.base_quantity = parseFloat(base_quantity),
            poitems[item_id].row.real_unit_cost = parseFloat(((poitems[item_id].row.tax_method == 1 ? $('#pcost_net').val() : $('#pcost').val())-parseFloat(tpax2)) / trmrate),
            poitems[item_id].row.tax_rate = new_pr_tax,
            poitems[item_id].tax_rate = new_pr_tax_rate,
            // poitems[item_id].row.discount = $('#order_discount_method').val() != 1 && $('#order_discount_method').val() != 4 ? ($('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '0') : 0,
            poitems[item_id].row.discount = ($('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : 0),
            poitems[item_id].row.expiry = $('#pexpiry').val() ? $('#pexpiry').val() : '';
            poitems[item_id].row.serial = $('#pserial').val();
            poitems[item_id].row.product_unit_id_selected = $('#punit').val();
            if (site.settings.product_variant_per_serial == 0 || poitems[item_id].row.attributes) {
                poitems[item_id].row.option = $('#poption[name="poption"]').val();
            }
            if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
                var ditem = poitems[item_id];
                unit_selected = ditem.units[unit];
                qty = parseFloat($('#pquantity').val());
                unit_selected_quantity = qty;
                if (unit_selected.operator == '*') {
                    unit_selected_quantity = qty * unit_selected.operation_value;
                } else if (unit_selected.operator == '/') {
                    unit_selected_quantity = qty / unit_selected.operation_value;
                } else if (unit_selected.operator == '+') {
                    unit_selected_quantity = qty + unit_selected.operation_value;
                } else if (unit_selected.operator == '-') {
                    unit_selected_quantity = qty - unit_selected.operation_value;
                }
                poitems[item_id].row.qty = unit_selected_quantity;
            } else {
                poitems[item_id].row.unit_price_id = $('#punit').val();
            }

            if (item.row.cnt_units_prices !== undefined && item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && item.row.promotion != 1) {
                if (poitems[item_id].units !== undefined) {
                    $.each($('.pgprice'), function(index, input_unit){
                        if (poitems[item_id].units[$(input_unit).data('pgid')]) {
                            poitems[item_id].units[$(input_unit).data('pgid')].margin_update_price = $('.pgmargin[data-pgid="'+$(input_unit).data('pgid')+'"]').val();
                            poitems[item_id].units[$(input_unit).data('pgid')].valor_unitario = $('.pgnew_price[data-pgid="'+$(input_unit).data('pgid')+'"]').val();
                        }
                    });
                } else {
                    $('.div_update_prices').fadeOut();
                }
            } else {
                if (poitems[item_id].product_price_groups !== undefined  && site.settings.update_prices_from_purchases == 1) {
                    $.each($('.pgprice'), function(index, input_unit){
                        if (poitems[item_id].product_price_groups[$(input_unit).data('pgid')]) {
                            poitems[item_id].product_price_groups[$(input_unit).data('pgid')].price = $(input_unit).val();
                        }
                    });
                } else {
                    $('.div_update_prices').fadeOut();
                }
            }
            // console.log(poitems);
            // return true;
            localStorage.setItem('poitems', JSON.stringify(poitems));
            $('#prModal').modal('hide');
            loadItems();
            if ($('#prorate_shipping_cost').val() == 1 && localStorage.getItem('poshipping')) {
                setTimeout(function() {
                    loadItems();
                }, 1000);
            }
            return;
        } else if ((potype == 2 || potype == 3)) {
            var row = $('#' + $('#row_id').val());
            var item_id = row.attr('data-item-id');
            var new_pr_tax = $('#ptax').val();
            var new_pr_tax_rate = false;
            var new_pr_tax_2 = (potype == 2 || potype == 3) ? $('#ptax_2').val() : 0;
            var new_pr_tax_2_rate = false;
            if (new_pr_tax) {
                $.each(tax_rates, function () {
                    if (this.id == new_pr_tax) {

                        if (this.rate < 1 && $('#ptax_value').val() > 0) {
                            this.rate = formatDecimal($('#ptax_value').val());
                        }

                        new_pr_tax_rate = this;
                    }

                    if (this.id == new_pr_tax_2) {

                        if (this.rate < 1 && $('#ptax_2_value').val() > 0) {
                            this.rate = formatDecimal($('#ptax_2_value').val());
                        }

                        new_pr_tax_2_rate = this;
                    }
                });
            }
            var cost = parseFloat($('#pcost').val());
            if(item.options !== false) {
                var opt = $('#poption').val();
                $.each(item.options, function () {
                    if(this.id == opt && this.cost != 0 && this.cost != '' && this.cost != null) {
                        cost = cost-parseFloat(this.cost);
                    }
                });
            }
            if (potype == 1) {
                if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
                    if(!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > cost) {
                        bootbox.alert(lang.unexpected_value);
                        return false;
                    }
                }
                if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
                    $(this).val(old_row_qty);
                    bootbox.alert(lang.unexpected_value);
                    return;
                }
            }
            // var unit = $('#punit').val();
            unit = 1;
            var base_quantity = parseFloat($('#pquantity').val());
            if(unit != poitems[item_id].row.base_unit) {
                $.each(poitems[item_id].units, function(){
                    if (this.id == unit) {
                        base_quantity = unitToBaseQty($('#pquantity').val(), this);
                    }
                });
            }
            poitems[item_id].row.fup = 1,
            // poitems[item_id].row.qty = 1,
            poitems[item_id].row.base_quantity = parseFloat(base_quantity),
            poitems[item_id].row.real_unit_cost = cost / trmrate,
            poitems[item_id].row.unit = 1,
            poitems[item_id].row.tax_rate = new_pr_tax,
            poitems[item_id].tax_rate = new_pr_tax_rate,
            poitems[item_id].row.discount = $('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '',
            poitems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '',
            poitems[item_id].row.serial = $('#pserial').val();
            poitems[item_id].row.name = $('#pname').val();
            poitems[item_id].row.tax_rate_2 = new_pr_tax_2;
            poitems[item_id].tax_rate_2 = new_pr_tax_2_rate;
            localStorage.setItem('poitems', JSON.stringify(poitems));
            $('#prModal').modal('hide');
            loadItems();
            return;
        }
    });
    /* ------------------------------
     * Show manual item addition modal
     ------------------------------- */
     $(document).on('click', '#addManually', function (e) {
        $('#mModal').appendTo("body").modal('show');
        return false;
    });
    /* --------------------------
     * Edit Row Quantity Method
     -------------------------- */
     var old_row_qty;
    $(document).on("focus", '.rquantity', function () {
        old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
        $('#add_purchase').prop('disabled', true);
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');


        var expresion=/^\d*(\.\d{1})?\d{0,1}$/;
        if (expresion.test(new_qty) !== true) {
            new_qty = formatQuantity2(new_qty);
            $(this).val(new_qty);
        }

        poitems[item_id].row.base_quantity = new_qty;
        if(poitems[item_id].row.unit != poitems[item_id].row.base_unit) {
            $.each(poitems[item_id].units, function(){
                if (this.id == poitems[item_id].row.unit) {
                    poitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        poitems[item_id].row.qty = new_qty;
        poitems[item_id].row.received = new_qty;
        if (po_edit) {
            poitems[item_id].row.quantity_balance = new_qty;
        }
        localStorage.setItem('poitems', JSON.stringify(poitems));
        loadItems();
        reset_purchase_rete_base('cantidades');
        if ($('#prorate_shipping_cost').val() == 1 && localStorage.getItem('poshipping')) {
            setTimeout(function() {
                loadItems();
            }, 1000);
        }
    }).on('keydown', '.rquantity', function(e){
        if (e.keyCode == 13) {
            $('#add_item').focus();
            loadItems();
            // $(this).trigger('change');
            e.stopPropagation();
        }
    });

    var old_received;
     $(document).on("focus", '.received', function () {
        old_received = $(this).val();
    }).on("change", '.received', function () {
        var row = $(this).closest('tr');
        new_received = $(this).val() ? $(this).val() : 0;
        if (!is_numeric(new_received)) {
            $(this).val(old_received);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_received = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        if (new_received > poitems[item_id].row.qty) {
            $(this).val(old_received);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        unit = formatDecimal(row.children().children('.runit').val()),
        $.each(poitems[item_id].units, function(){
            if (this.id == unit) {
                qty_received = formatDecimal(unitToBaseQty(new_received, this));
            }
        });
        poitems[item_id].row.unit_received = new_received;
        poitems[item_id].row.received = qty_received;
        localStorage.setItem('poitems', JSON.stringify(poitems));
        loadItems();
    });

    /* --------------------------
     * Edit Row Cost Method
     -------------------------- */
     var old_cost;
     $(document).on("focus", '.rcost', function () {
        old_cost = $(this).val();
    }).on("change", '.rcost', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_cost);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_cost = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        poitems[item_id].row.cost = new_cost;
        localStorage.setItem('poitems', JSON.stringify(poitems));
        loadItems();
    });
    $(document).on("click", '#removeReadonly', function () {
     $('#posupplier').select2('readonly', false);
     return false;
    });
    // if (po_edit) {
    //     $('#posupplier').select2("readonly", true);
    // }
    $(document).on('change', '#postatus', function(){
        localStorage.setItem('postatus', $(this).val());
        if ($(this).val() != 'received') {
            // if ($('#purchase_type').val() != 3) {
            //     $('#payments').fadeOut();
            // }
            $('#payment_affects_register').iCheck('disable');
            if (site.settings.purchase_send_advertisement == 1) {
                bootbox.alert('Al enviar la compra en estado no aprobado, no se afectará inventario, contabilidad ni costos promedio');
            }
        } 
        else {
            // $('#payments').fadeIn();
            // $('#paid_by_1').select2('val', ($('#purchase_type').val() == 1 || is_csv ? 'Credito' : 'expense_causation')).trigger('change');
            $('#payment_affects_register').iCheck('enable');
        }
    });
    if (postatus = localStorage.getItem('postatus')) {
        $('#postatus').select2('val', postatus).trigger('change');
    }

    if (po_edit == true && edit_paid_by != false) {
        setTimeout(function() {
            $('#paid_by_1').select2('val', edit_paid_by).trigger('change');
        }, 1200);
    }

    if (product_has_movements_after_date && site.settings.ignore_purchases_edit_validations == 0) {
        // $('#powarehouse').select2('readonly', true);
        $('#podate').prop('readonly', true);
        $('#podiscount').prop('readonly', true);
        $('#poshipping').prop('readonly', true);
    }
    $(document).on('ifChecked', '#purchase_from_import', function(){
        if (postatus != 'received' && !po_edit) {
            $('#postatus').select2('val', 'pending').select2('readonly', true).trigger('change');
            $('.tag_div').fadeIn();
            $('#potag').attr('required', true);
            header_alert('info', lang.purchase_from_import_warning);
            localStorage.setItem('po_form_import', true);
        }
           
    });
    $(document).on('ifUnchecked', '#purchase_from_import', function(){
       $('#postatus').select2('val', 'received').select2('readonly', false).trigger('change');
       localStorage.removeItem('po_form_import');
       $('.tag_div').fadeOut();
       $('#potag').attr('required', false);
    });
    $(document).on('change', '#potag', function(){
        localStorage.setItem('potag', $(this).val());
    });

     $('#potag').select2({
        formatResult : tag_option_color,
        formatSelection : tag_option_color
      });
     
     if (typeof invalid_csv_sale_products !== 'undefined') {
        setTimeout(function() {
            bootbox.alert(invalid_csv_sale_products);
            $(".bootbox").find(".modal-dialog").addClass("modal-lg");
        }, 1200);
    }
    if (po_edit) {
        setTimeout(function() {
            $('#payment_no_affects_register').iCheck('check');
            $('.paid_by').select2('val', suggested_paid_by).trigger('change');
            $('#amount_1').val(suggested_paid_amount).trigger('change');
        }, 1200);
    }
        
});

var deposit_advertisement_showed = false;
// var count = 1;
/* -----------------------
 * Misc Actions
 ----------------------- */

// hellper function for supplier if no localStorage value
function nsSupplier() {
    $('#posupplier').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: potype,
                    support_document: support_document,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
// aca load
        var pogtotal = 0;
function loadItems(set_focus = true, is_edit = false) {
    if (localStorage.getItem('poitems')) {
        if (!po_edit) {
            $('#currency').select2('readonly', true);
        }
        count = 1;
        general_item_count = 1;
        localStorage.removeItem('locked_by_tax_different_original');
        _gTotal = 0;
        var total = 0;
        var subtotal = 0;
        var subtotal_edit = 0;
        var totaltax = 0;
        var total_ipoconsumo = 0;
        var an = 1;
        var product_tax = 0;
        var product_tax_2 = 0;
        var invoice_tax = 0;
        var product_discount = 0;
        var order_discount = 0;
        var total_discount = 0;
        var item_order_discount = 0;
        var item_order_shipping = 0;
        var pooriginal_shipping = localStorage.getItem('pooriginal_shipping');
        var descuento_orden = $('#order_discount_method').val();
        var trmrate = 1;
        var posubtotal = 0;
        var poshipping_total = 0;
        var edit_date = true;
        var shipping = formatDecimal(parseFloat(localStorage.getItem('poshipping')));
        if (psbt = localStorage.getItem('posubtotal')) {
            posubtotal = psbt;
        }
        if (pgt = localStorage.getItem('gtotal_apportion')) {
            pogtotal = pgt;
        }
        descuento_a_productos = descuento_orden;
        if (descuento_a_productos == 1 || descuento_a_productos == 4) {
            if (podiscount = localStorage.getItem('podiscount')) {
                item_order_discount = podiscount;
            }
        }
        if ($('#prorate_shipping_cost').val() == 1) {
            if (poshipping = localStorage.getItem('poshipping')) {
                item_order_shipping = poshipping;
                var shipping = 0;
            }
        }
        $("#poTable tbody").empty();
        poitems = JSON.parse(localStorage.getItem('poitems'));
        sortedItems = _.sortBy(poitems, function(o){return [parseInt(o.order)];});
        if (site.settings.product_order == 2) {
            sortedItems.reverse();
        }
        var order_no = new Date().getTime();
        $.each(sortedItems, function (index) {
            var item = this;
            var item_id = (site.settings.item_addition == 1 ? item.item_id : item.id) + (item.row.option && item.row.option > 0 ? item.row.option : '');
            if (is_edit) update_index(item, item_id);  // <- Esta función se agrega debido a que cuando se editaba la linea del producto, se actualiza el item_id, este item_id es el que se asigna como data-item-id del tr de la tabla, esto genera conflictos al intentar editar nuevamente la linea pues el indice del poitems no se estaba actualizando pero el tr sí.
            item.order = item.order ? item.order :  new Date().getTime();
            var product_id = item.row.id, item_type = item.row.type, combo_items = item.combo_items, item_cost = item.row.cost, item_oqty = item.row.oqty, item_qty = item.row.qty, item_bqty = item.row.quantity_balance, item_expiry = item.row.expiry, item_tax_method = item.row.tax_method, item_ds = item.row.discount, item_discount = 0, item_shipping = 0, item_option = item.row.option, item_code = item.row.code, item_name = item.row.name !== undefined ? item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;") : item.row.product_name, item_serial = item.row.serial;
            var qty_received = (item.row.received >= 0) ? item.row.received : item.row.qty;
            var item_supplier_part_no = item.row.supplier_part_no ? item.row.supplier_part_no : '';
            if (item.row.new_entry == 1) { item_bqty = item_qty; item_oqty = item_qty; }
            var unit_cost = item.row.real_unit_cost;
            var product_unit = item.row.product_unit_id_selected, base_quantity = item.row.base_quantity;
            var tax_rate = item.row.tax_rate;
            var supplier = localStorage.getItem('posupplier'), belong = false;
            var item_ds_2 = 0;
            var units = [];
            var item_unit_selected =  item.row.product_unit_id_selected != null ?  item.row.product_unit_id_selected : item.row.unit;
            if ((potype == 2 || potype == 3) && item.tax_rate !== undefined && item.row.expense_tax_rate_id != item.tax_rate.id) {
                localStorage.setItem('locked_by_tax_different_original', 1);
                item.row.expense_diferent_tax_rate_id = true;
                $('.error_debug').text('El primer impuesto actual del gasto es diferente al configurado').css('display', '');
            } else {
                $('.error_debug').css('display', 'none');
            }
            if (po_edit && potype == 1) {
                edit_item = item.row.edit_item;
                if (!edit_item) {
                    edit_date = false;
                }
            } else {
                edit_item = true;
            }
            if (site.settings.ignore_purchases_edit_validations == 1) {
                edit_item = true;
            }
            if (supplier == item.row.supplier1) {
                belong = true;
            } else
            if (supplier == item.row.supplier2) {
                belong = true;
            } else
            if (supplier == item.row.supplier3) {
                belong = true;
            } else
            if (supplier == item.row.supplier4) {
                belong = true;
            } else
            if (supplier == item.row.supplier5) {
                belong = true;
            }
            var unit_qty_received = qty_received;
            if (item_order_discount != 0) {
                item_ds_2 = item_order_discount;
            }

            pr_t = {id : site.except_tax_rate_id, rate : 0};
            incorrect_tax_rate = false;
            if (item.tax_rate) {
                var pr_tax = item.tax_rate;
            } else {
                var pr_tax = pr_t;
                incorrect_tax_rate = true;
            }
            var pr_ptev_tax_val = pr_tax_val = pr_tax_rate = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_cost, item_tax_method))) {
                pr_ptev_tax_val = pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
            }
            var pr_tax_2 = item.tax_rate_2;
            var pr_ptev_tax_2_val = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax_2, unit_cost, item_tax_method))) {
                pr_ptev_tax_2_val = ptax[0];
            }
            var pr_tax_2_val = 0;
            if (edit_item) {
                var ds = item_ds ? item_ds : '0';
                var ds_2 = item_ds_2 ? item_ds_2 : '0';
                prorated_cost_discount = item.row.tax_method == 1 ?
                    parseFloat(unit_cost) :
                    (parseFloat(unit_cost) - (parseFloat(pr_ptev_tax_val) + parseFloat(pr_ptev_tax_2_val)));
                item_discount = pcalculateDiscount(ds, ds_2, prorated_cost_discount, posubtotal, item_qty);
                product_discount += parseFloat(item_discount * item_qty);
                if (descuento_a_productos == 1 || descuento_a_productos == 2) {
                    unit_cost = formatDecimal(unit_cost-item_discount);
                }
                if (item.row.purchase_tax_rate_2_percentage && support_document == 0) {
                    net_cost = (parseFloat(unit_cost) - (parseFloat(pr_ptev_tax_val) + parseFloat(pr_ptev_tax_2_val)));
                    ipoconsumo_percentage_calculated =  calculate_second_tax(item.row.purchase_tax_rate_2_percentage, net_cost);
                    pr_tax_2_val = ipoconsumo_percentage_calculated[1];
                }   
                if (descuento_a_productos == 4) {
                    unit_cost = formatDecimal(unit_cost-item_discount);
                }
            } else {
                item_discount = parseFloat(item_ds);
                product_discount += parseFloat(item_discount * item_qty);
                unit_cost = formatDecimal(unit_cost-item_discount);
            }

            if (item.row.shipping_unit_cost !== undefined && item.row.shipping_unit_cost > 0 && item_order_shipping == pooriginal_shipping) {
                item_shipping = formatDecimal(item.row.shipping_unit_cost);
            } else {
                prorated_cost_shipping = item.row.tax_method == 1 ?
                (parseFloat(unit_cost) + (parseFloat(pr_ptev_tax_val) + parseFloat(pr_ptev_tax_2_val))) :
                parseFloat(unit_cost);
                item_shipping = pcalculateShipping(item_order_shipping, prorated_cost_shipping, pogtotal, item_qty);
            }

            var pr_tax = item.tax_rate;
            var pr_tax_val = pr_tax_rate = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, ( unit_cost) - (item_tax_method == 1 ? 0 : pr_ptev_tax_val), 1))) {
                pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
                //suport document
                if (support_document == 1) {
                    // if (item_tax_method == 0) {
                    //     unit_cost = formatDecimal(unit_cost-((parseFloat(pr_ptev_tax_val)+parseFloat(pr_ptev_tax_2_val))));
                    // }
                    pr_tax_val = 0;
                    product_tax += 0;
                    pr_tax.id = site.settings.product_default_exempt_tax_rate;
                    tax_rate = site.settings.product_default_exempt_tax_rate;
                    pr_tax_rate = 0;
                }
                product_tax += pr_tax_val * item_qty;
                //suport document
            }
            var pr_tax_2 = item.tax_rate_2;
            var pr_tax_2_rate = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax_2, ( unit_cost) - (item_tax_method == 1 ? 0 : pr_ptev_tax_2_val), 1))) {
                pr_tax_2_val =  ptax[0];
                pr_tax_2_rate = ptax[1];
                product_tax_2 += pr_tax_2_val * item_qty;
            }
            item_cost = item_tax_method == 0 ? formatDecimal(unit_cost-((parseFloat(pr_ptev_tax_val)+parseFloat(pr_ptev_tax_2_val)))) : formatDecimal(unit_cost);
            item_cost = item_cost <= 0 ? 0 : item_cost;
            unit_cost = formatDecimal(unit_cost+item_discount);
            var sel_opt = '';
            var sel_opt_data;
            new_option_assigned = false;
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.code;
                    sel_opt_data = this;
                    new_option_assigned = this.new_option_assigned !== undefined && this.new_option_assigned == true;
                }
            });
            if (othercurrency = localStorage.getItem('othercurrency')) {
                rate = localStorage.getItem('othercurrencyrate');
                if (trm = localStorage.getItem('othercurrencytrm')) {
                    trmrate = rate / trm;
                }
            }
            /* profitability_margin */
            var cost = unit_cost;
            var prev_cost = item.row.prev_unit_cost;
            var price = item.row.prev_price;
            var tax_method = item_tax_method;
            var margin = 0;
            var pr_tax = tax_rates[tax_rate];

            if (cost > 0 && price > 0 && tax_rate > 0 && tax_method == 0) {
                margin_pr_tax_val = 0;
                if (ptax = calculateTax(pr_tax, price, tax_method)) {
                    margin_pr_tax_val = ptax[0];
                }
                var net_price = price - margin_pr_tax_val;
                if (ptax = calculateTax(pr_tax, cost, tax_method)) {
                    margin_pr_tax_val = ptax[0];
                }
                var net_cost = cost - margin_pr_tax_val;
            } else {
                var net_price = price;
                var net_cost = cost;
            }
            if (net_price > 0 && net_cost > 0) {
                margin = formatDecimals((((price - (cost + pr_tax_2_val)) / price) * 100), 2);
            }
            /* profitability_margin */
            ipoconsumo = false;
            if (!pr_tax_2 && support_document == 0) {
                pr_tax_2_val = pr_tax_2_val == 0 ? formatDecimal(item.row.consumption_purchase_tax ? item.row.consumption_purchase_tax : 0) : pr_tax_2_val;
                pr_tax_2_rate = item.row.purchase_tax_rate_2_percentage ? item.row.purchase_tax_rate_2_percentage : pr_tax_2_val;
                pr_tax_2_rate_id = item.row.purchase_tax_rate_2_id;
                product_tax_2 += pr_tax_2_val * item_qty;
                ipoconsumo = true;
            }
            var opt = $("<select id=\"poption\" name=\"variant\[\]\" class=\"form-control select rvariant\" />");
            if(item.options !== false && site.settings.product_variant_per_serial == 0) {
                $.each(item.options, function () {
                    if (item.row.option == this.id)
                        $("<option />", {value: this.id, text: this.name, selected: 'selected'}).appendTo(opt);
                    else
                        $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                });
            } else {
                $("<option />", {value: 0, text: 'n/a'}).appendTo(opt);
                opt = opt.hide();
            }
            var row_no = item.id + (item.row.option && item.row.option > 0 ? item.row.option : '');
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                        '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                        '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                        '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                        '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                        '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                        (site.settings.management_weight_in_variants == 1 ? 
                            '<input name="new_option_assigned[]" type="hidden" class="roption" value="' + (new_option_assigned ? 1 : 0) + '">'+
                            '<input name="new_option_assigned_code[]" type="hidden" class="roption" value="' + (new_option_assigned ? sel_opt_data.code : '') + '">'+
                            '<input name="new_option_assigned_option_id[]" type="hidden" class="roption" value="' + (new_option_assigned ? sel_opt_data.id : '') + '">'+
                            '<input name="new_option_assigned_name[]" type="hidden" class="roption" value="' + (new_option_assigned ? sel_opt_data.name : '') + '">'+
                            '<input name="new_option_assigned_consecutive[]" type="hidden" class="roption" value="' + (new_option_assigned ? sel_opt_data.pv_code_consecutive : '') + '">'+
                            '<input name="new_option_assigned_weight[]" type="hidden" class="roption" value="' + (new_option_assigned ? sel_opt_data.weight : '') + '">'
                        : '')+
                        '<input name="part_no[]" type="hidden" class="rpart_no" value="' + item_supplier_part_no + '">'+
                        '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name+' '+ (sel_opt ? ' ('+sel_opt+')' : '') +
                        (edit_item ? '<span class="label label-default">'+item_supplier_part_no+'</span></span> <i class="pull-right fa fa-edit tip edit" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i>' : '');


            if (item.row.cnt_units_prices !== undefined && item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) && item.row.promotion != 1) {
                if (item.units !== undefined) {
                    $.each(item.units, function(index, item_unit){
                        tr_html += '<input type="hidden" name="unit_update_price['+item.row.id+']['+index+']" value="'+item_unit.valor_unitario+'">';
                        tr_html += '<input type="hidden" name="unit_update_margin['+item.row.id+']['+index+']" value="'+item_unit.margin_update_price+'">';
                    });
                } else {
                    $('.div_update_prices').fadeOut();
                }

            } else {
                if (item.product_price_groups !== undefined && site.settings.update_prices_from_purchases == 1) {
                    $.each(item.product_price_groups, function(index, price_group){
                        tr_html += '<input type="hidden" name="pg_update_price['+item.row.id+']['+index+']" value="'+price_group.price+'">';
                    });
                } else {
                    $('.div_update_prices').fadeOut();
                }
            }

            tr_html += '</td>';
            if (site.settings.product_variant_per_serial == 1) {
                tr_html += '<td>'+
                                (item.row.serialModal_serial !== undefined ? item.row.serialModal_serial : '')+
                            '</td>';
            }
            tr_html += '<td class="thead_variant">'+(opt.get(0).outerHTML)+'</td>';

            unit_data_selected =  item.units[item_unit_selected] !== undefined ? item.units[item_unit_selected] : null;
            if (((site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10)  && potype == 1) && potype == 1) {
                unit_data_selected = item.units[item_unit_selected];
                tr_html += '<td>'+
                            '<input type="hidden" name="product_unit_id_selected[]" value="'+item_unit_selected+'">'+
                            '<span>'+unit_data_selected.code+'</span>'+
                          '</td>'; //nombre unidad

                item_unit_quantity = item_qty;
                item_unit_cost = item_cost;
                if (unit_data_selected.operator == '*') {
                    item_unit_quantity = item_qty / unit_data_selected.operation_value;
                    item_unit_cost = item_cost * unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '/'){
                    item_unit_quantity = item_qty * unit_data_selected.operation_value;
                    item_unit_cost = item_cost / unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '+'){
                    item_unit_quantity = item_qty - unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '-'){
                    item_unit_quantity = item_qty + unit_data_selected.operation_value;
                }

                tr_html += '<td>'+
                            '<span>'+formatQuantity(item_unit_quantity)+'</span>'+
                          '</td>'; //cantidad de la unidad
                tr_html += '<td class="thead_um_cost">'+
                            '<span><input name="" value="'+formatDecimal(item_unit_cost)+'" class="form-control text-right um_rpnet_cost"></span>'+
                          '</td>'; // costo de la unidad
            }
            if (site.settings.product_serial == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'">'+
                            '</td>';
            }
            if (site.settings.product_expiry == 1) {
                tr_html += '<td>'+
                              '<input class="form-control date rexpiry" name="expiry[]" type="text" value="' + item_expiry + '" data-id="' + row_no + '" data-item="' + item_id + '" id="expiry_' + row_no + '">'+
                            '</td>';
            }
            tr_html += '<td class="text-right thead_discount thead_unit_cost">'+
                            (item_discount > 0 ? '<input class="rpnet_cost form-control text-right" value="'+formatDecimal(trmrate * (parseFloat(item_cost + item_discount)))+'" '+(edit_item && (is_admin || is_owner || user_permissions.products_cost) ? '' : 'readonly')+' >' : formatMoney(trmrate * (item_cost + item_discount)))+
                        '</td>';
            if (site.settings.product_discount == 1) {
                tr_html += '<td class="text-right thead_discount">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + (item_ds != 0 ? item_ds : item_ds_2) + '">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount_val[]" type="hidden" id="discount_val_' + row_no + '" value="' + formatDecimal(0 - (item_discount * item_qty)) + '">'+
                                '<input class="form-control input-sm rdiscount" name="product_unit_discount_val[]" type="hidden" id="unit_discount_val_' + row_no + '" value="' + formatDecimal(item_discount) + '">'+
                                '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + formatMoney(trmrate * (0 - (item_discount ))) + '</span>'+
                           '</td>';
            }
            tr_html += '<td class="text-right thead_unit_cost">'+
                            '<input class="form-control input-sm text-right rcost" name="net_cost[]" type="hidden" id="cost_' + row_no + '" value="' + item_cost + '">'+
                            '<input class="rucost" name="unit_cost[]" type="hidden" value="' + unit_cost + '">'+
                            '<input class="realucost" name="real_unit_cost[]" type="hidden" value="' + item.row.real_unit_cost + '">'+
                            (item_discount == 0 ? '<input class="rpnet_cost form-control text-right" value="'+formatDecimal(trmrate * (parseFloat(item_cost)))+'" '+(edit_item && (is_admin || is_owner || user_permissions.products_cost) ? '' : 'readonly')+' >' : formatMoney(parseFloat(item_cost)))+
                       '</td>';
            if (po_edit) {
                tr_html += '<td class="rec_con">'+
                                '<input name="purchase_item_id[]" type="hidden" value="'+item.row.purchase_item_id+'">'+
                                '<input name="edit_item[]" type="hidden" value="'+edit_item+'">'+
                                '<input name="ordered_quantity[]" type="hidden" class="oqty" value="' + item_oqty + '">'+
                                '<input class="form-control text-center received" name="received[]" type="text" value="' + formatDecimal(unit_qty_received) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="received_' + row_no + '" onClick="this.select();" readonly>'+
                                '<input name="received_base_quantity[]" type="hidden" class="rrbase_quantity" value="' + qty_received + '">'+
                           '</td>';
            }
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + (pr_tax ? pr_tax.id : null) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax' + row_no + '" value="' + pr_tax_val + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (pr_tax_rate ? '(' + pr_tax_rate + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_val * (site.settings.precios_por_unidad_presentacion == 2 && unit_data_selected && unit_data_selected.operation_value !== undefined ? unit_data_selected.operation_value !== undefined : 1))) + '</span>';
                if (incorrect_tax_rate) {
                    tr_html += "  <span class='text-warning' style='font-weight:600;'>"+lang.incorrect_tax+' <i class="fa fa-exclamation-triangle"></i></span>';
                }
                tr_html += '</td>';
            }
            if (site.settings.ipoconsumo == 2 || site.settings.ipoconsumo == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax_2" name="product_tax_2[]" type="hidden" id="product_tax_2_' + row_no + '" value="' + (ipoconsumo ? pr_tax_2_rate_id : ( pr_tax_2 ?  pr_tax_2.id : null)) + '">'+
                                '<input class="form-control input-sm text-right" name="profitability_margin[]" type="hidden" id="profitability_margin_' + row_no + '" value="' + margin + '">'+
                                '<span class="text-right sproduct_tax_2" id="sproduct_tax_2_' + row_no + '">' + (pr_tax_2_rate ? '(' + pr_tax_2_rate + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_2_val)) + '</span>'+
                                '<input class="form-control input-sm text-right runit_product_tax_2" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_2_' + row_no + '" value="' + pr_tax_2_val + '">'+
                                '<input class="form-control input-sm text-right runit_product_tax_2_percentage" name="unit_product_tax_2_percentage[]" type="hidden" id="unit_product_tax_2_percentage_' + row_no + '" value="' + pr_tax_2_rate + '">'+
                            '</td>';
            }
            tr_html += '<td class="text-right thead_unit_cost">'+
                            '<input class="rpcost form-control text-right" value="'+formatDecimal(trmrate * (parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)))+'" '+(edit_item && (is_admin || is_owner || user_permissions.products_cost) ? '' : 'readonly')+'>'+
                        '</td>';
            if (((site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10)  && potype == 1) && potype == 1) {
                unit_data_selected = item.units[item_unit_selected];
                item_unit_quantity = item_qty;
                item_unit_cost = (parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val));
                if (unit_data_selected.operator == '*') {
                    item_unit_quantity = item_qty / unit_data_selected.operation_value;
                    item_unit_cost = item_unit_cost * unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '/'){
                    item_unit_quantity = item_qty * unit_data_selected.operation_value;
                    item_unit_cost = item_unit_cost / unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '+'){
                    item_unit_quantity = item_qty - unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '-'){
                    item_unit_quantity = item_qty + unit_data_selected.operation_value;
                }
                tr_html += '<td class="thead_um_cost">'+
                            '<span><input name="" value="'+formatDecimal(item_unit_cost)+'" class="form-control text-right um_rpcost"></span>'+
                          '</td>'; // costo de la unidad
            }
            tr_html += '<td class="text-center thead_shipping">'+
                            formatMoney(trmrate * (parseFloat(item_shipping) * parseFloat(item_qty)))+
                            '<input type="hidden" name="shipping_unit_cost[]" value="'+formatDecimal(item_shipping)+'">'+
                        '</td>';
            tr_html += '<td>'+
                            '<input name="quantity_balance[]" type="hidden" class="rbqty" value="' + item_bqty + '">'+
                            '<input class="form-control text-center rquantity" name="quantity[]" type="text" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" value="' + item_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+(!edit_item ? 'readonly' : '')+' >'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                            '<input name="product_purchase_unit[]" type="hidden" class="runit" value="' + item.row.base_unit + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + item_qty + '">'+
                            '<input name="serialModal_serial[]" type="hidden" value="' + (item.row.serialModal_serial ? item.row.serialModal_serial : '')+ '">'+
                       '</td>';
            tr_html += '<td class="text-right">'+
                            '<span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(trmrate * ((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val) + parseFloat(item_shipping)) * parseFloat(item_qty))) + '</span>'+
                        '</td>';
            tr_html += '<td class="text-center">'+
                            (edit_item && (is_admin || is_owner || user_permissions.purchases_edition_delete_item) ? '<i class="fa fa-times tip podel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i>' : '')+
                        '</td>';
            newTr.html(tr_html);
            poshipping_total += item_shipping * parseFloat(item_qty);
            // if (po_edit && site.settings.product_order == 2) {
                // newTr.prependTo("#poTable");
            // } else {
                newTr.appendTo("#poTable");
            // }
            subtotal += formatDecimal(((parseFloat(item_cost)) * parseFloat(item_qty)));
            subtotal_edit += (edit_item ? (formatDecimal(((parseFloat(item_cost)) * parseFloat(item_qty)))) : 0);
            totaltax += formatDecimal(((parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            total_ipoconsumo += formatDecimal(((parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            total += formatDecimal(((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            count += parseFloat(item_qty);
            general_item_count++;
            an++;
            if(!belong)
                $('#row_' + row_no).addClass('warning');
            if (site.settings.purchases_products_supplier_code == 1 && (item.row.supplier_part_no == undefined || item.row.supplier_part_no == "")) {
                header_alert('warning', 'El producto '+item.row.code+' - '+item.row.name+' no cuenta con código para el proveedor de esta compra, por favor diligencie uno');
                setTimeout(function() {
                    $('.edit[id="'+row_no+'"]').trigger('click');
                    $('#psupplier_part_no').focus();
                }, 1000);
                return false;
            }
        });
        var col = 1;
        if ((site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10)  && potype == 1) {
            col = 3;
        }
        if (site.settings.product_variant_per_serial == 1) {
            col++;
        }
        if (site.settings.product_expiry == 1) { col++; }
        if (site.settings.product_serial == 1) { col++; }
        var tfoot = '<tr id="tfoot" class="tfoot active">';
        tfoot += '<th colspan="'+col+'">Total ('+(an - 1)+')</th>';
        tfoot += '<th></th>';
        tfoot += '<th class="text-right thead_discount">'+formatMoney(trmrate * (product_discount + subtotal))+'</th>'; //aca total bruto
        if (site.settings.product_discount == 1) {
            tfoot += '<th class="text-right thead_discount">'+formatMoney(trmrate * product_discount)+'</th>';
        }
        tfoot += '<th class="text-right" id="subtotalP">'+formatMoney(trmrate * subtotal)+'</th>';
        if (psubtotal = localStorage.getItem('posubtotal')) {
            localStorage.removeItem('posubtotal');
        }
        if (po_edit) {
            localStorage.setItem('posubtotal', subtotal_edit+product_discount);
        } else {
            localStorage.setItem('posubtotal', subtotal+product_discount);
        }
        if (po_edit) {
            tfoot += '<th class="rec_con"></th>';
        }
        if (site.settings.tax1 == 1) {
            tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax)+'</th>';
        }
        if (site.settings.ipoconsumo == 2 || site.settings.ipoconsumo == 1) {
            tfoot += '<th class="text-right" id="ivaamount2">'+formatMoney(trmrate * product_tax_2)+'</th>';
        }
        tfoot += 
                '<th class="text-right">'+formatMoney(trmrate * (product_tax + product_tax_2 + subtotal))+'</th>'+
                '<th class="thead_shipping text-right">'+formatMoney(trmrate * poshipping_total)+'</th>'+
                '<th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>'+
                '<th class="text-right">'+formatMoney(trmrate * (total + poshipping_total))+' <span id="totalP" style="display:none;">'+formatDecimal(trmrate * (total + poshipping_total))+'</span></th>'+
                '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
            '</tr>';
        localStorage.setItem('gtotal', total);
        localStorage.setItem('gtotal_apportion', total - product_tax_2);
        $('#poTable tfoot').html(tfoot);

        if (product_discount > 0) {
            $('.thead_discount').fadeIn();
        } else {
            $('.thead_discount').fadeOut();
        }
        if (poshipping_total > 0) {
            $('.thead_shipping').fadeIn();
        } else {
            $('.thead_shipping').fadeOut();
        }
        if ((site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) && site.settings.precios_por_unidad_presentacion == 2) {
            $('.thead_unit_cost').fadeOut();
        } else {
            $('.thead_um_cost').fadeOut();
        }
        // Order level discount calculations
        if (descuento_a_productos == 2) {
            if (podiscount = localStorage.getItem('podiscount')) {
                var ds = podiscount;
                if (ds.indexOf("%") !== -1) {
                    var pds = ds.split("%");
                    if (!isNaN(pds[0])) {
                        order_discount = formatDecimal(((subtotal * parseFloat(pds[0])) / 100));
                    } else {
                        order_discount = formatDecimal(ds);
                    }
                } else {
                    order_discount = formatDecimal(ds);
                }
            }
        }
        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (potax2 = localStorage.getItem('potax2')) {
                $.each(tax_rates, function () {
                    if (this.id == potax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        }
                        if (this.type == 1) {
                            if (descuento_orden == 1 || descuento_orden == 4) { //Descuento afecta IVA
                                invoice_tax = formatDecimal((((subtotal - order_discount) * this.rate) / 100));
                            } else if (descuento_orden == 2) {//Descuento NO afecta iva
                                invoice_tax = formatDecimal((((subtotal + product_discount) * this.rate) / 100));
                            }
                        }
                    }
                });
            }
        }
        // poretenciones
        if (poretenciones = JSON.parse(localStorage.getItem('poretenciones'))) {
            total_rete_amount = formatDecimal(poretenciones.total_rete_amount);
        } else {
            total_rete_amount = 0;
        }
        //poretenciones
        total_discount = parseFloat(order_discount + product_discount);
        var gtotal = ((parseFloat(total) + parseFloat(invoice_tax)) - parseFloat(order_discount)) + parseFloat(shipping);
        _gTotal += parseFloat(formatDecimal(gtotal - total_rete_amount));
        pogtotal = gtotal;
        $('#amount_1').data('maxval', gtotal).prop('readonly', false);
        total_rete_amount = 0;
        if (poretenciones = JSON.parse(localStorage.getItem('poretenciones'))) {
            total_rete_amount = formatDecimal(parseFloat(poretenciones.total_rete_amount));
        }
        set_trm_input_payment(parseFloat(formatDecimal(gtotal - total_rete_amount)));
        $('#total').text(formatMoney(trmrate * total));
        $('#titems').text((an-1)+' ('+(formatQty(parseFloat(count) - 1))+')');
        $('#tds').text(formatMoney(trmrate * order_discount));
        if (site.settings.tax1) {
            $('#ttax1').text(formatMoney(trmrate * product_tax));
        }
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(trmrate * invoice_tax));
        }
        $('#gtotal').text(formatMoney(trmrate * (gtotal + poshipping_total)));

        $('#total_ipoconsumo').text(formatMoney(trmrate * total_ipoconsumo));
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        if (site.settings.ignore_purchases_edit_validations == 0 && (!edit_date || product_has_movements_after_date)) {
            $('#podate').prop('readonly', true);
        }
        // if (set_focus == true) {
        //     set_page_focus();
        // }
        if (site.settings.product_order == 2) {
            an = 2;
        }
        // if (set_focus == true) {
            set_page_focus(an);
        // }
        recalcular_poretenciones();
        $('.paid_by').trigger('change');
        setTimeout(function() {
            validate_submit_lock();
        }, 2000);
        if (gtotal > 0 && localStorage.getItem('supplier_deposit_balance') && deposit_advertisement_showed == false) {
            setTimeout(function() {
                deposit_advertisement_showed = true;
                deposit_amount = parseFloat(localStorage.getItem('supplier_deposit_balance'));
                if (deposit_amount > 0 && deposit_payment_method.purchase == true && edit_paid_by != 'deposit') {
                    bootbox.confirm({
                        message: "El proveedor cuenta con "+formatMoney(deposit_amount)+" de saldo de anticipos, ¿Desea usar este medio de pago para esta compra?",
                        buttons: {
                            confirm: {
                                label: 'Si, usar anticipos',
                                className: 'btn-success send-submit-sale btn-full'
                            },
                            cancel: {
                                label: 'No, usar otro medio de pago',
                                className: 'btn-danger btn-full'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                gtotal = parseFloat(formatDecimal($('#totalP').text()));
                                if (gtotal > deposit_amount) {
                                    set_trm_input_payment(parseFloat(formatDecimal(deposit_amount)));
                                }
                               $('#paid_by_1').select2('val', 'deposit').trigger('change');
                            }
                        }
                    });
                }
            }, 1500);
        }
    }
}

/* -----------------------------
 * Add Purchase Iten Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
 function add_purchase_item(item, modal = false, cont = false) {
    if (general_item_count == 1) {
        poitems = {};
        if ($('#posupplier').val() && (($('#currency').val() == site.settings.default_currency && !$('#trm').val()) || ($('#currency').val() != site.settings.default_currency && $('#trm').val()) ) && $('#purchase_type').val() != '') {
            if (!po_edit) {
                $('#posupplier').select2("readonly", true);
            }
            if (site.settings.descuento_orden == 3) {
                if ($('#order_discount_method').val()) {
                    $('#order_discount_method').select2("readonly", true);
                } else {
                    bootbox.alert(lang.select_above);
                    $('#order_discount_method').select2('open');
                    item = null;
                    return;
                }
            }
           $('#order_discount_to_products').select2("readonly", true);
        } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }
    localStorage.setItem('purchase_type', $('#purchase_type').val());
    $('#purchase_type').select2('readonly', true);
    if ($('#purchase_type').val() == 1) {
        $('.edit_purchase_expense').remove();
    } else {
        $('.edit_purchase').remove();
    }
    if (item == null)
        return;
    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    item_id = item_id + (item.row.option && item.row.option > 0 ? item.row.option : '');
    if (poitems[item_id]) {
        if (po_edit == false || (po_edit == true && poitems[item_id].row.edit_item)) {
            var new_qty = parseFloat(poitems[item_id].row.qty) + 1;
            poitems[item_id].row.base_quantity = new_qty;
            if(poitems[item_id].row.unit != poitems[item_id].row.base_unit) {
                $.each(poitems[item_id].units, function(){
                    if (this.id == poitems[item_id].row.unit) {
                        poitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                    }
                });
            }
            poitems[item_id].row.qty = new_qty;
        }
    } else {
        poitems[item_id] = item;
    }
    poitems[item_id].order = new Date().getTime();
    if (site.settings.product_variant_per_serial == 1 && item.row.attributes == 1 && modal === false) {
        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(poitems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal').modal('show');
        localStorage.setItem('modalSerial_item', JSON.stringify(poitems[item_id]));
        delete poitems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
        }, 850);
    } else if (site.settings.management_weight_in_variants == 1 && item.row.based_on_gram_value == 1 && modal === false && !item.row.option) {
        $('#product_variant_weight').select2('val', '');
        $('.product_name_optionWeightModal').text(poitems[item_id].label);
        $('#optionWeightModal_product_id').val(poitems[item_id].item_id);
        $('#optionWeightModal').modal('show');
        $('.div_option_weight_not_relationed').fadeOut();
        $('#product_variant_weight_code').attr('required', false).val('');
        $('#code_consecutive_setted').val(0);
        localStorage.setItem('optionWeight_item', JSON.stringify(poitems[item_id]));
        delete poitems[item_id];
        setTimeout(function() {
            $('#product_variant_weight').select2('open');
        }, 850);
    } else {
        localStorage.removeItem('modalSerial_item');
        localStorage.removeItem('optionWeight_item');
        if (site.settings.product_variant_per_serial == 1 && cont == false) {
            $('#serialModal').modal('hide');
            $('#add_item').val('').focus();
        }  else if (site.settings.management_weight_in_variants == 1 && cont == false) {
            $('#optionWeightModal').modal('hide');
            $('#add_item').val('').focus();
        } else {
            item = poitems[item_id];
            $.ajax({
                url : site.base_url + "products/get_random_id"
            }).done(function(data){
                item.id = data;
                localStorage.setItem('modalSerial_item', JSON.stringify(item));
                $('#serialModal_serial').val('');
                $('#serialModal_meters').val('');
            });

        }
        localStorage.setItem('poitems', JSON.stringify(poitems));
        loadItems();
        if ($('#prorate_shipping_cost').val() == 1 && localStorage.getItem('poshipping')) {
            setTimeout(function() {
                loadItems();
            }, 1000);
        }
        return true;
    }
}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}


$(document).on('change', '#order_discount_method', function(){
    if ($(this).val() != "") {
        localStorage.setItem('poorderdiscountmethod', $(this).val());
        loadItems();
    }
});

$(document).on('change', '#order_discount_to_products', function(){
    if ($(this).val() != "") {
        localStorage.setItem('poorderdiscounttoproducts', $(this).val());
        loadItems();
    }
});


// JS poretenciones

    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_fuente_option').val('').attr('disabled', true).select().trigger('change');
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor_show').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount();
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_iva_option').val('').attr('disabled', true).select().trigger('change');
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor_show').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount();
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_ica_option').val('').attr('disabled', true).select().trigger('change');
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor_show').val('');
            $('#rete_ica_valor').val('');
            reset_rete_bomberil();
            setReteTotalAmount();
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_otros_option').val('').attr('disabled', true).select().trigger('change');
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor_show').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount();
        }
    });

    $(document).on('click', '#rete_bomberil', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/BOMB"
            }).done(function(data){
                $('#rete_bomberil_option').html(data);
                $('#rete_bomberil_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_bomberil_option').val('').attr('disabled', true).select().trigger('change');
            $('#rete_bomberil_tax').val('');
            $('#rete_bomberil_valor_show').val('');
            $('#rete_bomberil_valor').val('');
            setReteTotalAmount();
        }
    });

    $(document).on('click', '#rete_autoaviso', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/TABL"
            }).done(function(data){
                $('#rete_autoaviso_option').html(data);
                $('#rete_autoaviso_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_autoaviso_option').val('').attr('disabled', true).select().trigger('change');
            $('#rete_autoaviso_tax').val('');
            $('#rete_autoaviso_valor_show').val('');
            $('#rete_autoaviso_valor').val('');
            setReteTotalAmount();
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option, #rete_iva_option, #rete_ica_option, #rete_otros_option, #rete_bomberil_option, #rete_autoaviso_option', function(){
        rete_purchase_option_changed($(this).prop('id'), getTrmRate());
    });
    $(document).on('change', '#input_rete_fuente_base, #input_rete_otros_base', function(){
        rete_purchase_option_changed($(this).prop('id') == 'input_rete_fuente_base' ? 'rete_fuente_option' : 'rete_otros_option', getTrmRate(), true);
    });
    function getReteAmount(apply){
        amount = 0;
        if (apply == "ST") {
            amount = $('#subtotalP').text();
        } else if (apply == "TX") {
            amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = $('#totalP').text();
        } else if (apply == "IC") {
            amount = $('#rete_ica_valor').val();
        }
        if (othercurrency = localStorage.getItem('othercurrency')) {
            othercurrencytrm = localStorage.getItem('othercurrencytrm');
            if (othercurrencytrm > 0) {
                amount = amount * othercurrencytrm;
            }
        }
        return amount;
    }

    function setReteTotalAmount(){
        // setTimeout(function() {
            rtotal_rete =
                        ($('#rete_fuente_assumed').is(':checked') ? 0 : formatDecimal($('#rete_fuente_valor').val())) +
                        ($('#rete_iva_assumed').is(':checked') ? 0 : formatDecimal($('#rete_iva_valor').val())) +
                        ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_ica_valor').val())) +
                        ($('#rete_otros_assumed').is(':checked') ? 0 : formatDecimal($('#rete_otros_valor').val()))+
                        ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_bomberil_valor').val()))+
                        ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_autoaviso_valor').val()));
            trmrate = getTrmRate();
            $('#total_rete_amount_show').text(formatMoney(trmrate * rtotal_rete));
            $('#total_rete_amount').text(formatMoney(rtotal_rete));
        // }, 850);
    }

    $(document).on('click', '#updateOrderRete', function () {
        $('#ajaxCall').fadeIn();
        rete_prev = JSON.parse(localStorage.getItem('poretenciones'));
        var poretenciones = {
            'gtotal' : parseFloat(formatDecimal($('#totalP').text())),
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            'bomberil_option' : $('#rete_bomberil_option option:selected').data('percentage'),
            'id_rete_bomberil' : $('#rete_bomberil_option').val(),
            'autoaviso_option' : $('#rete_autoaviso_option option:selected').data('percentage'),
            'id_rete_autoaviso' : $('#rete_autoaviso_option').val(),
            'rete_fuente_assumed' : $('#rete_fuente_assumed').is(':checked'),
            'rete_iva_assumed' : $('#rete_iva_assumed').is(':checked'),
            'rete_ica_assumed' : $('#rete_ica_assumed').is(':checked'),
            'rete_otros_assumed' : $('#rete_otros_assumed').is(':checked'),
        };
        
        if (rete_prev && rete_prev !== undefined && rete_prev.base_rete_fuente !== undefined && rete_prev.base_rete_fuente > 0) {
            poretenciones['base_rete_fuente'] = rete_prev.base_rete_fuente;
            poretenciones['base_rete_otros'] = rete_prev.base_rete_otros;
            poretenciones['calculated_base_rete_fuente'] = $('#input_rete_fuente_base').val();
            poretenciones['calculated_base_rete_otros'] = $('#input_rete_otros_base').val();
        }
        
        poretenciones.total_discounted = formatDecimal(parseFloat(formatDecimal($('#totalP').text())) - parseFloat(formatDecimal($('#total_rete_amount').text())));
        setTimeout(function() {
            shipping = formatDecimal($('#poshipping').val());
            $('#gtotal').text(formatMoney(parseFloat(formatDecimals(poretenciones.total_discounted)) + parseFloat(formatDecimals(shipping))));
            // $('#amount_1').val(formatDecimal(poretenciones.total_discounted));
            set_trm_input_payment(formatDecimal(poretenciones.total_discounted) / getTrmRate());
            $('#ajaxCall').fadeOut();
        }, 1000);
        localStorage.setItem('poretenciones', JSON.stringify(poretenciones));

        general_total_rete = formatDecimal($('#rete_fuente_valor').val()) + formatDecimal($('#rete_iva_valor').val()) + formatDecimal($('#rete_ica_valor').val()) + formatDecimal($('#rete_otros_valor').val());

        if (general_total_rete > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(poretenciones.total_rete_amount)))
        } else {
            $('#rete_applied').val(0);
        }
        if (is_csv == true && ($('#rete_fuente_option').val() || $('#rete_fuente_option').val() || $('#rete_ica_option').val() || $('#rete_otros_option').val())) {
            $('#rete_applied').val(1);
        }
        $('#retencion_show').val(poretenciones.total_rete_amount);
        $('#rtModal').modal('hide');
     });

     $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('poretenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
     });

function recalcular_poretenciones(){
    $('#add_purchase').prop('disabled', true);
    poretenciones = JSON.parse(localStorage.getItem('poretenciones'));
    if (poretenciones != null) {
            if (poretenciones.id_rete_fuente > 0) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').trigger('click');
                }
            }
            if (poretenciones.id_rete_iva > 0) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').trigger('click');
                }
            }
            if (poretenciones.id_rete_ica > 0) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').trigger('click');
                }
            }
            if (poretenciones.id_rete_otros > 0) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').trigger('click');
                }
            }
            if (poretenciones.id_rete_bomberil > 0) {
                if (!$('#rete_bomberil').is(':checked')) {
                    $('#rete_bomberil').prop('disabled', false).trigger('click');
                }
            }
            if (poretenciones.id_rete_autoaviso > 0) {
                if (!$('#rete_autoaviso').is(':checked')) {
                    $('#rete_autoaviso').prop('disabled', false).trigger('click');
                }
            }
            if (poretenciones.rete_fuente_assumed != false) {
                if (!$('#rete_fuente_assumed').is(':checked')) {
                    $('#rete_fuente_assumed').trigger('click');
                }
            }
            if (poretenciones.rete_iva_assumed != false) {
                if (!$('#rete_iva_assumed').is(':checked')) {
                    $('#rete_iva_assumed').trigger('click');
                }
            }
            if (poretenciones.rete_ica_assumed != false) {
                if (!$('#rete_ica_assumed').is(':checked')) {
                    $('#rete_ica_assumed').trigger('click');
                }
            }
            if (poretenciones.rete_otros_assumed != false) {
                if (!$('#rete_otros_assumed').is(':checked')) {
                    $('#rete_otros_assumed').trigger('click');
                }
            }

            setTimeout(function() {
                $.each($('#rete_fuente_option option'), function(index, value){
                    if(poretenciones.id_rete_fuente != '' && $(value).val() == poretenciones.id_rete_fuente){
                        $('#rete_fuente_option').select2('val', $(value).val()).trigger('change');
                    }
                });
                $.each($('#rete_iva_option option'), function(index, value){
                    if(poretenciones.id_rete_iva != '' && $(value).val() == poretenciones.id_rete_iva){
                        $('#rete_iva_option').select2('val', $(value).val()).trigger('change');
                    }
                });

                $.each($('#rete_ica_option option'), function(index, value){
                    if(poretenciones.id_rete_ica != '' && $(value).val() == poretenciones.id_rete_ica){
                        $('#rete_ica_option').select2('val', $(value).val()).trigger('change');
                    }
                });
                $.each($('#rete_otros_option option'), function(index, value){
                    if(poretenciones.id_rete_otros != '' && $(value).val() == poretenciones.id_rete_otros){
                        $('#rete_otros_option').select2('val', $(value).val()).trigger('change');
                    }
                });
                setTimeout(function() {
                    $.each($('#rete_autoaviso_option option'), function(index, value){
                        if(poretenciones.id_rete_autoaviso != '' && $(value).val() == poretenciones.id_rete_autoaviso){
                            $('#rete_autoaviso_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_bomberil_option option'), function(index, value){
                        if(poretenciones.id_rete_bomberil != '' && $(value).val() == poretenciones.id_rete_bomberil){
                            $('#rete_bomberil_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                }, 450);
                $('#rete_otros_option').select2().trigger('change');
                // console.log(poretenciones.calculated_base_rete_fuente);
                if (poretenciones.calculated_base_rete_fuente > 0) {
                    $('#input_rete_fuente_base').val(poretenciones.calculated_base_rete_fuente).trigger('change');
                }
                if (poretenciones.calculated_base_rete_otros > 0) {
                    $('#input_rete_otros_base').val(poretenciones.calculated_base_rete_otros).trigger('change');
                }
                $('#updateOrderRete').trigger('click');
                validate_submit_lock();
            }, 1650);
    } else {
        $('#rete_fuente').iCheck('check').trigger('click');
        $('#rete_otros').iCheck('check').trigger('click');
    }
}

if ($('#rete_applied').val()) {
  total_rete_amount = $('#total_rete_amount').text();
  total_rete_amount = parseFloat(formatDecimal(total_rete_amount));
} else {
  total_rete_amount = 0;
}
// JS poretenciones
$('#currency').on('change', function(){
    default_currency = site.settings.default_currency;
    actual_currency = $(this).val();
    actual_currency_rate = $('#currency option:selected').data('rate');
    if (default_currency != actual_currency) {
        $('.trm-control').css('display', '');
        $('#trm').prop('required', true);
        if (othercurrencytrm = localStorage.getItem('othercurrencytrm')) {
            $('#trm').val(othercurrencytrm);
        }
        localStorage.setItem('othercurrency', true);
        localStorage.setItem('othercurrencycode', actual_currency);
        localStorage.setItem('othercurrencyrate', actual_currency_rate);
        $('.currency').text(actual_currency);
    } else {
        $('.trm-control').css('display', 'none');
        $('#trm').prop('required', false);
        localStorage.removeItem('othercurrency');
        localStorage.removeItem('othercurrencycode');
        localStorage.removeItem('othercurrencyrate');
        localStorage.removeItem('othercurrencytrm');
        $('#trm').val('');
        $('.currency').text(default_currency);
    }
    loadItems();
});

$('#trm').on('change', function(){
    trm = $(this).val();
    if(trm != '' && trm != 0) {
        localStorage.setItem('othercurrencytrm', trm);
    } else {
        localStorage.removeItem('othercurrencytrm');
    }
    loadItems();
});

if (currencycode = localStorage.getItem('othercurrencycode')) {
    $('#currency').val(currencycode).trigger('change');
}

function getTrmRate(){
    trmrate = 1;
    if (othercurrency = localStorage.getItem('othercurrency')) {
        othercurrencytrm = localStorage.getItem('othercurrencytrm');
        othercurrencyrate = localStorage.getItem('othercurrencyrate');
        othercurrencycode = localStorage.getItem('othercurrencycode');
        trmrate = othercurrencyrate / othercurrencytrm;
    }
    return trmrate;
}

function validate_supplier_ref(){
    $('.referencia_duplicada').fadeOut();
    if ($('#poref').val() != "") {
        $.ajax({
            url:site.base_url+'purchases/validate_supplier_ref',
            type : 'get',
            data : {'supplier_id' : $('#posupplier').val(), 'reference_no' : $('#poref').val(), 'po_edit' : po_edit, 'id_purchase' : id_purchase, 'document_type_prefix' : $('#document_type_id option:selected').data('prefix')},
            dataType : 'JSON'
        }).done(function(data){
            localStorage.removeItem('locked_by_poreference_duplicated');
            if (data == true) {
                localStorage.setItem('locked_by_poreference_duplicated', 1);
                $('.referencia_duplicada').fadeIn();
                Command: toastr.error('Esta referencia ya está registrada para el proveedor escogido', 'Referencia duplicada', {onHidden : function(){}})
            }
            validate_submit_lock();
        });
    } 
}

function supplier_deposit_balance(){
    var supplier_id = $('#posupplier').val();
    $.ajax({
        url : site.base_url+"suppliers/get_supplier_deposit_balance/"+supplier_id,
        dataType : "JSON"
    }).done(function(data){
        localStorage.removeItem('supplier_deposit_balance');
        if (data.deposit_amount > 0) {
            localStorage.setItem('supplier_deposit_balance', parseFloat(data.deposit_amount));
        }
    });
}

$(document).on('change', '.paid_by', function(){
    payment = $(this).closest('.well');
    paid_by = $(this).val();
    amount_to_pay = payment.find('.amount').val();
    supplier = $('#posupplier').val();
    if (paid_by == "deposit" && edit_paid_by != 'deposit') {
        $.ajax({
            url:  site.base_url+"suppliers/deposit_balance/"+supplier+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    }
});

$(document).on('keyup', '.amount', function(){
    payment = $(this).closest('.well');
    paid_by = payment.find('select.paid_by');
    amount_to_pay = payment.find('.amount').val();
    supplier = $('#posupplier').val();
    if (paid_by.val() == "deposit" && edit_paid_by != 'deposit') {
        $.ajax({
            url:  site.base_url+"suppliers/deposit_balance/"+supplier+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    }
});

$(document).on('click', '#addManually1', function(){
    localStorage.setItem('add_product_from_purchase', 1);
    setTimeout(function() {
        location.href = site.base_url+'products/add/?from_purchase=1';
    }, 1000);
});

$(document).on('change', '#prorate_shipping_cost', function(){
    if ($(this).val()) {
        $('#poshipping').prop('readonly', false).trigger('change');
        localStorage.setItem('prorate_shipping_cost', $(this).val());
    } else {
        $('#poshipping').val('').trigger('change').prop('readonly', true);
        localStorage.removeItem('prorate_shipping_cost');
    }
    setTimeout(function() {
        loadItems();
    }, 800);
});

$(document).on('change', '#consecutive_supplier', function(){
    if ($(this).val()) {
        $.ajax({
            url : site.base_url+"purchases/validate_supplier_consecutive/"+$(this).val()+"/"+$('#posupplier').val()
        }).done(function(data){
            if (data) {
                command: toastr.error('El consecutivo indicado para el proveedor seleccionado ya existe.', '¡Consecutivo duplicado!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                $('#consecutive_supplier').val('').focus();
            }
        });
    }
});

$(document).on('change', '.purchase_payment_term', function(){
    $('#purchase_payment_term').val($(this).val());
});

function supplier_retentions(){
    if (po_edit == true) {
        return false;
    }
    var supplier_validate_min_base_retention = false;
    $.ajax({
        url : site.base_url+"suppliers/get_supplier_by_id/"+$('#posupplier').val()+"/NULL/1",
        dataType : "JSON"
    }).done(function(data){
        if (data.supplier_validate_min_base_retention == 0) {
            supplier_validate_min_base_retention = true;
        }
        localStorage.setItem('supplier_validate_min_base_retention', supplier_validate_min_base_retention);
        if((data.default_rete_fuente_id || data.default_iva_fuente_id || data.default_ica_fuente_id || data.default_other_fuente_id)) {
            var poretenciones = {
                    'gtotal' : parseFloat(formatDecimal($('#totalP').text())),
                    'id_rete_fuente' : data.default_rete_fuente_id,
                    'id_rete_iva' : data.default_rete_iva_id,
                    'id_rete_ica' : data.default_rete_ica_id,
                    'id_rete_otros' : data.default_rete_other_id,
                    'rete_fuente_assumed' : false,
                    'rete_iva_assumed' : false,
                    'rete_ica_assumed' : false,
                    'rete_otros_assumed' : false,
                };
            localStorage.setItem('poretenciones', JSON.stringify(poretenciones));
            setTimeout(function() {
                recalcular_poretenciones();
            }, 850);
        }
    });
}

function set_supplier_payment(){

    if (is_quote == true) {
        return false;
    }
    if (po_edit == true && edit_paid_by != false && purchase_has_multi_payments == false) {
        $('#paid_by_1').val(edit_paid_by).trigger('change');
        set_trm_input_payment(edit_paid_amount);
        return false;
    }
    var supplier_id = $('#posupplier').val();
    $('#popayment_status').select2('val', 'pending').trigger('change');
    $('#popayment_term').val('');
    if ($('#purchase_type').val() == 1) {
        if ($('#payment_method').length > 0) {
            $('#paid_by_1').val($('#payment_method').val()).trigger('change');
            return;
        }
        $.ajax({
            url : site.base_url+"suppliers/get_payment_type/"+supplier_id,
            dataType : "JSON"
        }).done(function(data){
            setTimeout(function() {
                if (data.payment_type == 0) {
                    $('#popayment_status').select2('val', 'pending').trigger('change');
                    $('#popayment_term_1').val(data.payment_term > 0 ? data.payment_term : 1);
                    $('#purchase_payment_term').val(data.payment_term > 0 ? data.payment_term : 1);
                    $('#supplierpaymentterm').val(data.payment_term > 0 ? data.payment_term : 1);
                    $('#paid_by_1').val($('#purchase_type').val() == 1 || is_csv ? 'Credito' : 'expense_causation').trigger('change');
                } else if (data.payment_type == 1) {
                    $('#popayment_status').select2('val', 'partial').trigger('change');
                    $('#paid_by_1').val('cash').trigger('change');
                }
            }, 850);
        }); 
    } else {
        $('#paid_by_1').val('expense_causation').trigger('change');
    }
        
}

///SERIAL MODAL

$(document).on('click', '.send_serial_modal', function(){
    validar_modal_serial();
});

$(document).on('click', '.continue_serial_modal', function(){
    validar_modal_serial(true);
});

function validar_modal_serial(cont = false){
    if ($('#serialModal_form').valid()) {
        serial = $('#serialModal_serial').val();
        meters = $('#serialModal_meters').val();
        item_id = $('#serialModal_product_id').val();
        item = JSON.parse(localStorage.getItem('modalSerial_item'));
        item.row.serialModal_serial = serial;
        item.row.qty = meters;
        item.row.type = 'addition';
        add_purchase_item(item, true, cont);
    }
}

$(document).on('keyup', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal, e);
});

$(document).on('change', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal);
});

$(document).on('keyup', '#serialModal_meters', function(e){
    if (e.keyCode == 13) {
        $('.send_serial_modal').trigger('click');
    }
});

function validate_serial(serialModal, e = null){
    if ( e == null || e != null && e.keyCode == 13) {
        serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
        $.ajax({
            url : site.base_url + 'products/get_product_serial_variant/'+serial_item.row.id+'/'+serialModal
        }).done(function(data){
            if (data == '1') {
                command: toastr.warning('El serial ingresado ya existe para este producto.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
                $('#serialModal_serial').val('').focus();
            } else {
                $('#serialModal_meters').focus();
            }
        });
    }
}

$(document).on('click', '#random_num', function(e){
    $('#serialModal_serial').select();
});

$(document).on('click', '.send_option_weight', function(){
    validar_option_weight();
});

var new_options_assigned = localStorage.getItem('new_options_assigned') ? JSON.parse(localStorage.getItem('new_options_assigned')) : {};
function validar_option_weight(cont = false){
    if ($('#optionWeightModal_form').valid()) {
        option_weight = $('#product_variant_weight').val();
        item_id = $('#optionWeightModal_product_id').val();
        item = JSON.parse(localStorage.getItem('optionWeight_item'));
        option_weight_selected_name = $('#product_variant_weight option:selected').text();
        option_weight_exists = false;
        $.each(item.options, function(index, option){
            if (option_weight_selected_name == option.name) {
                option_weight_exists = true;
                option_weight = option.id;
            }
        });
        item.row.option = option_weight;
        if (!option_weight_exists) {
            new_option = {
                id : option_weight,
                code : $('#product_variant_weight_code').val(),
                name : $('#product_variant_weight option:selected').text(),
                product_id : item_id,
                pv_code_consecutive : $('#product_variant_weight_consecutive').val(),
                weight : $('#product_variant_weight option:selected').data('weight'),
                new_option_assigned : true
            };
            item.options.push(new_option);
        }
        add_purchase_item(item, true, cont);
        if (new_options_assigned[item_id+"_"+option_weight] === undefined) {
            new_options_assigned[item_id+"_"+option_weight] = 1;
        }
        setTimeout(function() {
            localStorage.setItem('new_options_assigned', JSON.stringify(new_options_assigned));
        }, 1000);
    }
}

$(document).on('change', '#product_variant_weight', function(){
    option_weight = $('#product_variant_weight').val();
    item = JSON.parse(localStorage.getItem('optionWeight_item'));
    item_id = $('#optionWeightModal_product_id').val();
    option_weight_selected_name = $('#product_variant_weight option:selected').text();
    option_weight_exists = false;
    $.each(item.options, function(index, option){
        if (option_weight_selected_name == option.name) {
            option_weight_exists = true;
            option_weight = option.id;
        }
    });
    item.row.option = option_weight;
    if (new_options_assigned[item_id+"_"+option_weight] !== undefined) {
        header_alert('warning', lang.product_variant+' nuevo ya asignado en esta compra porfavor revise ('+option_weight_selected_name+')');
        $('#product_variant_weight').select2('val', '').select2('open');
    } else {
        if (!option_weight_exists) {
            $('.div_option_weight_not_relationed').fadeIn();
            $('#product_variant_weight_code').attr('required', true);
        } else {
            $('.div_option_weight_not_relationed').fadeOut();
            $('#product_variant_weight_code').attr('required', false);
        }
    }
});

$(document).on('click', '#rete_fuente_assumed, #rete_iva_assumed, #rete_ica_assumed, #rete_otros_assumed', function(e){
    if ($(this).is(':checked')) {
        localStorage.setItem('assumed_click', 1);
    } else {
        localStorage.setItem('assumed_click', 2);
    }
    rete = $(this).prop('id').replace('_assumed', '');
    $('#'+rete+'_option').trigger('change');
});

function reset_rete_bomberil(){
    $('#rete_bomberil').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_autoaviso').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_bomberil_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_autoaviso_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_bomberil_tax').val('');
    $('#rete_bomberil_valor').val('');
    $('#rete_autoaviso_tax').val('');
    $('#rete_autoaviso_valor').val('');
}

function validate_submit_lock(){
    setTimeout(function() {
        if (
            localStorage.getItem('locked_by_poreference_duplicated') ||
            localStorage.getItem('locked_by_tax_different_original')
            ) {
            $('#add_purchase').prop('disabled', true);
        } else {
            $('#add_purchase').prop('disabled', false);
        }
    }, 850);
}

function set_biller_warehouses_related(){
    warehouses_related = billers_data[$('#pobiller').val()].warehouses_related;
    if (warehouses_related) {
        $('#powarehouse option').each(function(index, option){
            $(option).prop('disabled', false);
            if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                $(option).prop('disabled', true);
            }
        });
        setTimeout(function() {
            $('#powarehouse').select2();
        }, 850);
    }
}

$(document).on('change', '#upload_xls', function(){
    input = $(this);
    if (input.is(':checked')) {
        $('.xls_file_div').fadeIn();
        $('#postatus').select2('val', 'pending').select2('readonly', true).trigger('change');
        header_alert('warning', lang.purchase_uploading_xls);
        header_alert('info', lang.purchase_uploading_xls_verify_products);
    } else {
        $('.xls_file_div').fadeOut();
        $('#postatus').select2('val', 'completed').select2('readonly', false).trigger('change');
    }
});

function set_trm_input_payment(amount = null){
    const stackTrace = new Error().stack;
    if (stackTrace) {
        const lineaDeLlamada = stackTrace.split('\n')[2];
        // console.log(" >>> "+lineaDeLlamada);
        // console.log(" >>> "+amount);
    }
    if (localStorage.getItem('othercurrencytrm')) {
        $('.trm_amount').fadeIn();
        $('.amount').fadeOut();
        $('#amount_1').val(amount);
        $('#trm_amount_1').val(formatDecimal(amount * getTrmRate()));
    } else {
        // console.log(" >> amount "+amount);
        $('.trm_amount').fadeOut();
        $('.amount').fadeIn();
        $('#amount_1').val(amount);
    }
}

$(document).on('keyup', '.trm_amount', function(){
    num = $(this).data('num');
    input_payment = $('input[name="amount-paid[]"][data-num="'+num+'"]');
    othercurrencytrm = parseFloat(localStorage.getItem('othercurrencytrm'));
    input_payment.val(formatDecimal($(this).val() * othercurrencytrm));
});

const update_index = (item, item_id) => {
    let poitems = JSON.parse(localStorage.getItem('poitems'));
    for (const key in poitems) {
        if (poitems.hasOwnProperty(key)) {
            if (poitems[key].id === item.id ) { // <- Este id es unico desde el backend se envia un numero encriptado en sha1
                poitemsAux = { ...poitems[key] }; 
                delete poitems[key];
                poitems[item_id] = poitemsAux;
            } 
        }
    }
    localStorage.setItem('poitems', JSON.stringify(poitems));
}
