<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Companies_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('settings_model');
    }

    public function getAllCompanies(){
        $q = $this->db->get('companies');
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllBillerCompanies()
    {
        $this->db->order_by('company');
        $q = $this->db->get_where('companies', array('group_name' => 'biller'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'customer'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSupplierCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSellerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'seller'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerGroups()
    {
        $q = $this->db->get('customer_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyUsers($company_id)
    {
        $q = $this->db->get_where('users', array('company_id' => $company_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyByID($id)
    {
        $q = $this->db->select('
                                companies.*,
                                documentypes.nombre as nombre_tipo_documento,
                                types_vat_regime.description as tvat_regime,
                                zones.zone_name as zone_name,
                                subzones.subzone_name as subzone_name,
                                customer_employer.company as employer_company,
                                customer_employer_address.sucursal as employer_branch,
                            ')
                 ->join('documentypes', 'documentypes.id = companies.tipo_documento', 'left')
                 ->join('types_vat_regime', 'types_vat_regime.id = companies.tipo_regimen', 'left')
                 ->join('zones', 'zones.id = companies.location', 'left')
                 ->join('subzones', 'subzones.id = companies.subzone', 'left')
                 ->join('companies as customer_employer', 'customer_employer.id = companies.customer_employer_id', 'left')
                 ->join('addresses as customer_employer_address', 'customer_employer_address.id = companies.customer_employer_branch_id', 'left')
                 ->where('companies.id', $id)
                 ->get('companies');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCompanyAddressById($id){
        $q = $this->db->select('*')
                ->where('id', $id)
                ->get('addresses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getBillerDataAditional($biller_id)
    {
        $q = $this->db->get_where('biller_data', array('biller_id' => $biller_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCompanyByEmail($email)
    {
        $q = $this->db->get_where('companies', array('email' => $email), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_billers()
    {
        $this->db->select("id, company AS name_biller");
        $this->db->from("companies");
        $this->db->where("group_name", "biller");
        $response = $this->db->get();

        if ($response->num_rows() > 0)
        {
            return $response->result();
        }

        return FALSE;
    }

    public function get_billers_seller_by_seller_id($seller_id)
    {
        $this->db->select('biller_id');
        $this->db->from('biller_seller');
        $this->db->where('companies_id', $seller_id);
        $response = $this->db->get();

        if ($response->num_rows() > 0)
        {
            return $response->result();
        }

        return FALSE;
    }

    public function get_seller($id)
    {
        $this->db->select('*');
        $this->db->from('companies');
        $this->db->where('id', $id);
        $response = $this->db->get();

        if ($response->num_rows() > 0)
        {
            return $response->row();
        }

        return FALSE;
    }

    public function get_existing_seller_by_document_number($document_number, $seller_id = NULL)
    {
        $this->db->select("vat_no");
        $this->db->from("companies");
        $this->db->where("vat_no", $document_number)->where('group_name', 'seller');
        if (! is_null($seller_id)) { $this->db->where('id !=', $seller_id); }
        $response = $this->db->get();

        if ($response->num_rows() > 0)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function get_existing_seller_by_email($email, $seller_id = NULL)
    {
        $this->db->select("email");
        $this->db->from("companies");
        $this->db->where("email", $email)->where('group_name', 'seller');
        if (!is_null($seller_id)) { $this->db->where('id !=', $seller_id); }
        $response = $this->db->get();

        if ($response->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function existing_sales_by_seller_id($seller_id)
    {
        $this->db->select('id');
        $this->db->from('sales');
        $this->db->where('seller_id', $seller_id);
        $response = $this->db->get();

        if ($response->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }


    public function existing_contracts_by_seller_id($seller_id)
    {
        $this->db->select('id');
        $this->db->from('payroll_contracts');
        $this->db->where('companies_id', $seller_id);
        $response = $this->db->get();

        if ($response->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function addCompany($data = array(), $data_aditional = array(), $resoluciones = array(), $default_dts = array(), $data_categories_concession = array())
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        $DB2 = false;
        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
            $DB2 = $this->settings_model->connect_new_year_database();
            if (!$this->settings_model->check_same_id_insert($DB2, 'companies')) {
                $this->session->set_flashdata('error', 'El tercero no se pudo crear, inconsistencia de datos entre años');
            }
        }
        $latitude = isset($data['latitude']) ? $data['latitude'] : NULL;
        $longitude = isset($data['longitude']) ? $data['longitude'] : NULL;
        if (isset($data['longitude'])) {
            unset($data['latitude']);
            unset($data['longitude']);
        }
        if ($this->db->insert('companies', $data)) {
            $cid = $this->db->insert_id();
            if ($DB2) {
                $DB2->insert('companies', $data);
            }
            if ($data['group_name'] == 'customer') {
                $datos = array(
                    'company_id' => $cid,
                    'code' => $data['vat_no']."-01",
                    'direccion' => $data['address'],
                    'sucursal' => $data['name'],
                    'city' => $data['city'],
                    'postal_code' => $data['postal_code'],
                    'state' => $data['state'],
                    'country' => $data['country'],
                    'phone' => $data['phone'],
                    'email' => $data['email'],
                    'city_code' => $data['city_code'],
                    'customer_address_seller_id_assigned' => isset($data['customer_seller_id_assigned']) ? $data['customer_seller_id_assigned'] : NULL,
                    'customer_group_id' => $data['customer_group_id'],
                    'customer_group_name' => $data['customer_group_name'],
                    'price_group_id' => $data['price_group_id'],
                    'price_group_name' => $data['price_group_name'],
                    'location' => $data['location'],
                    'subzone' => $data['subzone'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                );
                $data['datos'] = date('Y-m-d H:i:s');
                $this->db->insert('addresses', $datos);
                if ($DB2) {
                    $DB2->insert('addresses', $datos);
                }

            }
            if ($resoluciones && count($resoluciones) > 0) {
                foreach ($resoluciones as $row => $id) {
                    $data = array(
                                'biller_id' => $cid,
                                'document_type_id' => $id,
                                );
                    $this->db->insert('biller_documents_types', $data);
                    if ($DB2) {
                        $DB2->insert('biller_documents_types', $data);
                    }
                }
            }
            if (count($data_aditional) > 0) {
                if (isset($default_dts[1])) {
                    $data_aditional['pos_document_type_default'] = $default_dts[1];
                }
                if (isset($default_dts[2])) {
                    $data_aditional['detal_document_type_default'] = $default_dts[2];
                }
                if (isset($default_dts[5])) {
                    $data_aditional['purchases_document_type_default'] = $default_dts[5];
                }
                $data_aditional['biller_id'] = $cid;
                $data_aditional['last_update'] = date('Y-m-d H:i:s');
                $this->db->insert('biller_data', $data_aditional);
                if ($DB2) {
                    $DB2->insert('biller_data', $data_aditional);
                }
            }
            if ($data_categories_concession) {
                foreach ($data_categories_concession as $ccrow) {
                    $ccrow['biller_id'] = $cid;
                    $this->db->insert('biller_categories_concessions', $ccrow);
                    if ($DB2) {
                        $DB2->insert('biller_categories_concessions', $ccrow);
                    }
                }
            }

            return $cid;
        }
        return false;
    }

    public function insert_seller($data = [], $addresses = [])
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->insert('companies', $data))
        {
            $sid = $this->db->insert_id();
            $this->db->update('addresses', ['customer_address_seller_id_assigned' => NULL], ['customer_address_seller_id_assigned' => $sid]);
            if (count($addresses) > 0) {
                foreach ($addresses as $key => $address_id) {
                    $this->db->update('addresses', ['customer_address_seller_id_assigned' => $sid], ['id' => $address_id]);
                }
            }

            return $sid;
        }

        return false;
    }

    public function insert_billers_seller($data)
    {
        if ($this->db->insert_batch('biller_seller', $data))
        {
            return TRUE;
        }

        return FALSE;
    }

    public function existsCompany($vat_no, $group_id){
        $q = $this->db->where(array('group_id' => $group_id, 'vat_no' => $vat_no))->get('companies');
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateCompany($id, $data = array(), $data_aditional = array(), $resoluciones = array(), $default_dts = array(), $data_categories_concession = array(), $schedule = array())
    {
        $DB2 = false;
        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
            $DB2 = $this->settings_model->connect_new_year_database();
        }
        $company = $this->getCompanyByID($id);

        $latitude = isset($data['latitude']) ? $data['latitude'] : NULL;
        $longitude = isset($data['longitude']) ? $data['longitude'] : NULL;
        if (isset($data['longitude'])) {
            unset($data['latitude']);
            unset($data['longitude']);
        }

        if (isset($data['customer_seller_id_assigned'])) {
            $address_data = false;
            if ($company->address != $data['address']) {
                $address_data['direccion'] = $data['address'];
            }
            if ($company->name != $data['name']) {
                $address_data['sucursal'] = $data['name'];
            }
            if ($company->city != $data['city']) {
                $address_data['city'] = $data['city'];
            }
            if ($company->postal_code != $data['postal_code']) {
                $address_data['postal_code'] = $data['postal_code'];
            }
            if ($company->state != $data['state']) {
                $address_data['state'] = $data['state'];
            }
            if ($company->country != $data['country']) {
                $address_data['country'] = $data['country'];
            }
            if ($company->phone != $data['phone']) {
                $address_data['phone'] = $data['phone'];
            }
            if ($company->email != $data['email']) {
                $address_data['email'] = $data['email'];
            }
            if ($company->postal_code != $data['postal_code']) {
                $address_data['city_code'] = $data['postal_code'];
            }
            if ($company->customer_seller_id_assigned != $data['customer_seller_id_assigned']) {
                $address_data['customer_address_seller_id_assigned'] = $data['customer_seller_id_assigned'];
            }
            if ($company->customer_group_id != $data['customer_group_id']) {
                $address_data['customer_group_id'] = $data['customer_group_id'];
            }
            if ($company->price_group_id != $data['price_group_id']) {
                $address_data['price_group_id'] = $data['price_group_id'];
            }
            if ($company->price_group_name != $data['price_group_name']) {
                $address_data['price_group_name'] = $data['price_group_name'];
            }
            if ($company->location != $data['location']) {
                $address_data['location'] = $data['location'];
            }
            if ($company->subzone != $data['subzone']) {
                $address_data['subzone'] = $data['subzone'];
            }
            if ($latitude) {
                $address_data['latitude'] = $latitude;
            }
            if ($longitude) {
                $address_data['longitude'] = $longitude;
            }
            if ($main_address && $address_data) {
                $address_data['last_update'] = date('Y-m-d H:i:s');
                $main_address = $this->get_company_principal_address($id);
                $this->db->update('addresses', $address_data, ['id' => $main_address->id]);
            }
        }
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->update('companies', $data, ['id'=>$id])) {
            if ($DB2) {
                $DB2->update('companies', $data, ['id'=>$id]);
            }
            if (count($resoluciones) > 0) {
                $this->db->delete('biller_documents_types', array('biller_id' => $id));
                foreach ($resoluciones as $rid) {
                    if (!empty($rid)) {
                        $data = array(
                                'biller_id' => $id,
                                'document_type_id' => $rid,
                                );
                        $this->db->insert('biller_documents_types', $data);
                    }
                }
            }

            if (!empty($data_aditional)) {
                $q = $this->db->get_where('biller_data', array('biller_id' => $id));

                if (isset($default_dts[1])) {
                    $data_aditional['pos_document_type_default'] = $default_dts[1];
                }

                if (isset($default_dts[2])) {
                    $data_aditional['detal_document_type_default'] = $default_dts[2];
                }

                if (isset($default_dts[3])) {
                    $data_aditional['pos_document_type_default_returns'] = $default_dts[3];
                }

                if (isset($default_dts[5])) {
                    $data_aditional['purchases_document_type_default'] = $default_dts[5];
                }

                if (isset($default_dts[8])) {
                    $data_aditional['order_sales_document_type_default'] = $default_dts[8];
                }

                if (isset($default_dts['default_electronic_document_for_financing'])) {
                    $data_aditional['default_electronic_document_for_financing'] = $default_dts["default_electronic_document_for_financing"];
                }


                $data_aditional['biller_id'] = $id;
                $data_aditional['last_update'] = date('Y-m-d H:i:s');

                if ($q->num_rows() > 0) {
                    $this->db->update('biller_data', $data_aditional, array('biller_id' => $id));
                    if ($DB2) {
                        $DB2->update('biller_data', $data_aditional, array('biller_id' => $id));
                    }
                } else {
                    $this->db->insert('biller_data', $data_aditional);
                }
            }

            if ($data_categories_concession) {
                // exit(var_dump($data_categories_concession));
                foreach ($data_categories_concession as $ccrow) {
                    $q = $this->db->get_where('biller_categories_concessions', ['biller_id' => $ccrow['biller_id'], 'category_id' => $ccrow['category_id']]);
                    if ($q->num_rows() > 0) {
                        $this->db->update('biller_categories_concessions', ['concession_code' => $ccrow['concession_code'], 'concession_name' => $ccrow['concession_name']], ['biller_id'=>$ccrow['biller_id'], 'category_id'=>$ccrow['category_id']]);
                    } else {
                        $this->db->insert('biller_categories_concessions', $ccrow);
                    }
                }
            }

            if ($schedule) {
                foreach ($schedule as $sh) {
                    $q = $this->db->get_where('schedule', ['biller_id'=>$id, 'week_day'=>$sh['week_day']]);
                    if ($q->num_rows() > 0) {
                        $q = $q->row();
                        $this->db->update('schedule', $sh, ['id'=>$q->id]);
                    } else {
                        $sh['biller_id'] = $id;
                        $this->db->insert('schedule', $sh);

                    }
                }
            }

            return true;
        }
        return false;
    }

    public function addCompanies($data = array())
    {
        if ($this->db->insert_batch('companies', $data)) {
            return true;
        }
        return false;
    }

    public function deactivate_customers($id, $status)
    {
        if ($this->db->update('companies', ['status' => $status, 'last_update'=>date('Y-m-d H:i:s')], ['id' => $id])) {
            return true;
        }
        return FALSE;
    }
    public function delete_customer($id)
    {
        $sales = $this->db->get_where('sales', ['customer_id'=>$id]);
        $deposits = $this->db->get_where('deposits', ['company_id'=>$id]);
        $customer = $this->db->get_where('companies', ['id'=>$id])->row();
        if ($sales->num_rows() > 0 || $deposits->num_rows() > 0) {
            return false;
        }
        if ($this->db->delete('companies', ['id' => $id])) {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 2,
                'table_name' => 'customers',
                'record_id' => $id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => 'Eliminó el cliente '.($customer->company ? $customer->company : $customer->name).' N° doc '.$customer->vat_no,
            ]);
            return true;
        }
        return false;
    }

    public function update_seller($data, $id, $addresses = [])
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id);
        if ($this->db->update('companies', $data))
        {
            $this->db->update('addresses', ['customer_address_seller_id_assigned' => NULL], ['customer_address_seller_id_assigned' => $id]);
            if ($addresses && count($addresses) > 0) {
                foreach ($addresses as $key => $address_id) {
                    $this->db->update('addresses', ['customer_address_seller_id_assigned' => $id], ['id' => $address_id]);
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function disable_seller($id)
    {
        $this->db->where('id', $id);
        if ($this->db->update('companies', ['status'=>2]))
        {
            return TRUE;
        }

        return FALSE;
    }

    public function enable_seller($id)
    {
        $this->db->where('id', $id);
        if ($this->db->update('companies', ['status'=>1]))
        {
            return TRUE;
        }

        return FALSE;
    }

    public function delete_seller($id)
    {
        if ($this->db->delete('companies', ['id'=>$id]))
        {
            return TRUE;
        }

        return FALSE;
    }

    public function delete_billers_seller_by_seller_id($seller_id)
    {
        if ($this->db->delete('biller_seller', ['companies_id'=>$seller_id]))
        {
            return TRUE;
        }

        return FALSE;
    }

    public function deleteSupplier($id)
    {
        if ($this->getSupplierPurchases($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'supplier')) && $this->db->delete('users', array('company_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteBiller($id)
    {
        if ($this->getBillerSales($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'biller'))) {
            return true;
        }
        return FALSE;
    }

    public function deactivateBiller($id)
    {

        if ($this->Settings->default_biller == $id) {
           return false;
        }

        if ($this->db->update('companies', array('status' => 0), array('id' => $id))) {
            $this->db->update('users', ['biller_id' => NULL], ['biller_id' => $id]);
            return true;
        }

        return false;
    }

    public function activateBiller($id)
    {

        if ($this->db->update('companies', array('status' => 1), array('id' => $id))) {
            return true;
        }

        return false;
    }

    public function getBillerSuggestions($term, $limit = 10)
    {
        $this->db->select("id, company as text");
        $this->db->where(" (id LIKE '%" . $term . "%'
        OR name LIKE '%" . $term . "%'
        OR company LIKE '%" . $term . "%'
        OR vat_no LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'biller'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSuggestions($term, $limit = 10)
    {
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as value, phone", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%'
        OR name LIKE '%" . $term . "%'
        OR company LIKE '%" . $term . "%'
        OR email LIKE '%" . $term . "%'
        OR phone LIKE '%" . $term . "%'
        OR vat_no LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'customer', 'status' => 1), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSuggestionsWithVatNo($term, $limit = 10)
    {
        $this->db->select("id, CONCAT(vat_no, ' - ', name) as text, CONCAT(vat_no, ' - ', name) as value, phone", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%'
        OR name LIKE '%" . $term . "%'
        OR company LIKE '%" . $term . "%'
        OR email LIKE '%" . $term . "%'
        OR phone LIKE '%" . $term . "%'
        OR vat_no LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSupplierSuggestions($term, $limit = 10)
    {
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%'
        OR name LIKE '%" . $term . "%'
        OR company LIKE '%" . $term . "%'
        OR email LIKE '%" . $term . "%'
        OR phone LIKE '%" . $term . "%'
        OR vat_no LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getSupplierCreditorSuggestions($term, $limit = 10, $ptype = 1, $support_document = 0)
    {
        $st_sql = '';
        /*
            1. compras
            2. gastos
            3. or confecion
            4. or corte
            5. or ensamble
            6. or empaque
            7. comprobante egreso
        */
        if ($ptype == 1) {
            $st_sql = '(supplier_type = 1 OR supplier_type = 2)';
        } else if ($ptype == 2 || $ptype == 3) {
            $st_sql = '(supplier_type = 1 OR supplier_type = 3)';
        } else if ($ptype == 5 || $ptype == 6) {
            $st_sql = 'supplier_type = 3';
        } else if ($ptype == 7) {
            $st_sql = '(supplier_type = 1 OR supplier_type = 2 OR supplier_type = 3)';
        } else if ($ptype == 8) {
            $st_sql = 'supplier_type = 5';
        } else {
            $st_sql = 'supplier_type = '.$ptype;
        }
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%'
                                OR name LIKE '%" . $term . "%'
                                OR company LIKE '%" . $term . "%'
                                OR email LIKE '%" . $term . "%'
                                OR phone LIKE '%" . $term . "%'
                                OR vat_no LIKE '%" . $term . "%')
        ");
        if ($support_document == 1) {
            $this->db->where('generateSupportingDocument', 1);
            $this->db->where('tipo_regimen', 1);
            $this->db->where('supplier_type <= ', 4);
        } else {
            if ($ptype != 'all') {
                $this->db->where($st_sql);
            }
            if ($support_document != 'all') {
                $this->db->where('generateSupportingDocument', 0);
            }
        }
        $this->db->where('status', 1);
        $q = $this->db
                    ->where('(group_name = "supplier" or group_name = "creditor" )')
                    ->limit($limit)
                    ->get('companies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getEmployees($term, $limit = 10)
    {

        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%'
                                OR name LIKE '%" . $term . "%'
                                OR company LIKE '%" . $term . "%'
                                OR email LIKE '%" . $term . "%'
                                OR phone LIKE '%" . $term . "%'
                                OR vat_no LIKE '%" . $term . "%')
        ");
        $q = $this->db
                    ->where('(group_name = "employee")')
                    ->limit($limit)
                    ->get('companies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSales($id)
    {
        $this->db->where('customer_id', $id)->from('sales');
        return $this->db->count_all_results();
    }

    public function getBillerSales($id)
    {
        $this->db->where('biller_id', $id)->from('sales');
        return $this->db->count_all_results();
    }

    public function getSupplierPurchases($id)
    {
        $this->db->where('supplier_id', $id)->from('purchases');
        return $this->db->count_all_results();
    }

    public function addDeposit($data, $cdata, $dtype = 1)
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        if ($this->db->insert('deposits', $data)) {
            $id_deposit = $this->db->insert_id();
            $this->db->update('companies', $cdata, array('id' => $data['company_id']));
            $this->site->wappsiContabilidadDeposito($data, $dtype);
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            return $id_deposit;
        }
        return false;
    }

    public function cancel_deposit($data, $cdata, $dtype = 1, $ddata = null)
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        if ($this->db->insert('deposits', $data)) {
            $id_deposit = $this->db->insert_id();
            $this->db->update('companies', $cdata, array('id' => $data['company_id']));
            $this->db->update('deposits', array('balance' => $ddata['amount']), array('id' => $ddata['id']));
            $this->site->wappsiContabilidadDeposito($data, $dtype);
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            return $id_deposit;
        }
        return false;
    }

    public function updateDeposit($id, $data, $cdata)
    {
        if ($this->db->update('deposits', $data, array('id' => $id)) &&
            $this->db->update('companies', $cdata, array('id' => $data['company_id']))) {
            return true;
        }
        return false;
    }

    public function getDepositByID($id, $arr = false)
    {
        $q = $this->db->get_where('deposits', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            if ($arr) {
                return $q->row_array();
            }
            return $q->row();
        }
        return FALSE;
    }

    public function deleteDeposit($id)
    {
        $deposit = $this->getDepositByID($id);
        $company = $this->getCompanyByID($deposit->company_id);
        $cdata = array(
                'deposit_amount' => ($company->deposit_amount-$deposit->amount),
                'last_update' => date('Y-m-d H:i:s')
            );
        if ($this->db->update('companies', $cdata, array('id' => $deposit->company_id)) &&
            $this->db->delete('deposits', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getAllPriceGroups()
    {
        $q = $this->db->get('price_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getIDDocumentTypes(){
        $q = $this->db->get('documentypes');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyAddresses($company_id)
    {
        $q = $this->db->select('addresses.*, companies.company as seller_name, zones.zone_name as zone_name, subzones.subzone_name as subzone_name')
                ->join('companies', 'companies.id = addresses.customer_address_seller_id_assigned', 'left')
                 ->join('zones', 'zones.id = addresses.location', 'left')
                 ->join('subzones', 'subzones.id = addresses.subzone', 'left')
                ->where('company_id', $company_id)
                ->get('addresses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addAddress($data)
    {
        if ($this->db->insert('addresses', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function updateAddress($id, $data)
    {
        if ($this->db->update('addresses', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deleteAddress($id)
    {
        if ($this->db->delete('addresses', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getAddressByID($id)
    {
        $q = $this->db->get_where('addresses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCountries(){
        $q = $this->db->get('countries');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getStatesByCountry($country){
        $q = $this->db->where('PAIS', $country)->get('states');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCitiesByState($state){
        $q = $this->db->where('CODDEPARTAMENTO', $state)->get('cities');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCity($data) {
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get('cities');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getZonesByCity($city){
        $q = $this->db->where('codigo_ciudad', $city)->get('zones');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubZonesByZones($zone){
        $q = $this->db->where('zone_code', $zone)->get('subzones');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyGroupIdByName($group_name){
        $q = $this->db->where('name', $group_name)->get('groups');

        if ($q->num_rows() > 0) {
            if ($group = $q->row()) {
                return $group->id;
            }
        }
        return FALSE;
    }

    public function getDepositsWithBalance($customer_id, $limit = 4){
        $q = $this->db->where(array('company_id' => $customer_id, 'balance >' => 0))->order_by('date asc')->get('deposits', $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllDocumentTypes(){

        $q = $this->db->order_by('module asc')->get('documents_types');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function get_all_biller_categories_concessions($biller_id){

        $q = $this->db->get_where('biller_categories_concessions', ['biller_id'=>$biller_id, 'concession_code != ' => '']);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->category_id] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getBillerDocumentTypes($id, $module = NULL, $is_document_electronic = NULL, $arr_docs = []){
        if ($id != 'all') {
            $condiciones['biller_documents_types.biller_id'] = $id;
        }
        $not_electronic = [1, 5, 13, 17, 19, 20, 23, 24, 28, 31, 32];
        if ($module) {
            if (!in_array($module, $not_electronic)) {
                if ($this->Settings->electronic_billing == NOT) {
                    $condiciones["documents_types.factura_electronica"] = NOT;
                }
            }
            $condiciones['documents_types.module'] = $module;
        }
        if ($is_document_electronic !== NULL) {
            $condiciones['documents_types.factura_electronica'] = $is_document_electronic;
        }
        $this->db->select('documents_types.*')
                 ->join('documents_types', 'documents_types.id = biller_documents_types.document_type_id', 'inner')
                 ->where($condiciones)
                 ->order_by("documents_types.sales_prefix ASC, documents_types.factura_electronica ASC");
        if ($id == 'all') {
            $this->db->group_by('documents_types.id');
        }
        $q = $this->db->get('biller_documents_types');
        if ($module) {
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $biller_doc_type) {
                    if ($arr_docs != NULL && count($arr_docs) > 0 && !in_array($biller_doc_type->id, $arr_docs)) {
                        continue;
                    }
                    $data[] = $biller_doc_type;
                }
                return $data;
            }
        } else {
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $biller_doc_type) {
                    $data[$biller_doc_type->id] = 1;
                }
                return $data;
            }
        }

        return FALSE;
    }

    public function getMultipleBillerDocumentTypes($id, $modules){

        $this->db->select('documents_types.*')
                 ->join('documents_types', 'documents_types.id = biller_documents_types.document_type_id', 'inner')
                 ->where_in('documents_types.module', $modules)
                 ->where('biller_documents_types.biller_id', $id)
                 ->order_by("documents_types.sales_prefix ASC, documents_types.factura_electronica ASC");
        $q = $this->db->get('biller_documents_types');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $biller_doc_type) {
                $data[] = $biller_doc_type;
            }
            return $data;
        }
        return FALSE;
    }

    public function insert_type_customer_obligations($data) {
        if ($this->db->insert_batch("types_customer_obligations", $data) > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function update_customer_main_address($customer_id, $data){

        $main_address_data = [
                                'direccion' => $data['address'],
                                'country'   => $data['country'],
                                'state'     => $data['state'],
                                'city'      => $data['city'],
                                'city_code' => $data['city_code'],
                                'phone'     => $data['phone'],
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];

        $main_address = $this->db->where('company_id', $customer_id)
                                ->order_by('id', 'asc')
                                ->get('addresses', 1);

        if ($main_address->num_rows() > 0) {

            $main_address = $main_address->row();
            $this->db->update('addresses', $main_address_data, ['company_id' => $customer_id, 'id' => $main_address->id]);

        } else {

            $this->db->insert('addresses', $main_address_data);

        }
    }

    public function get_company_principal_address($id){
        $q = $this->db->order_by('id', 'ASC')->limit(1)->get_where('addresses', ['company_id' => $id]);
        if ($q->num_rows() > 0) {
            $q = $q->row();
            return $q;
        }
        return false;
    }

    public function validate_vat_no($vat_no, $group_name){
        $q = $this->db->get_where('companies', ['group_name' => $group_name, 'vat_no' => $vat_no]);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function validate_email($email){
        $q = $this->db->get_where('companies', ['email' => $email]);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function get_portfolio($company_id, $type, $biller_id = NULL){

        if ($type == 'customer') {

            $q = $this->db->select('SUM(grand_total - paid) as balance')
                            ->where('(grand_total - paid) > 0')
                            ->where('sale_status != "returned"')
                            ->where('biller_id', $biller_id)
                            ->where('customer_id', $company_id)
                            ->get('sales');

            if ($q->num_rows() > 0) {
                return $q->row();
            }

        } else if ($type == 'supplier') {

            $q = $this->db->select('SUM(grand_total - paid) as balance')
                            ->where('(grand_total - paid) > 0')
                            ->where('status != "returned"')
                            ->where('biller_id', $biller_id)
                            ->where('supplier_id', $company_id)
                            ->get('purchases');

            if ($q->num_rows() > 0) {
                return $q->row();
            }

        } else if ($type == 'supplier_cxc') {

            $q = $this->db->select('SUM('.$this->db->dbprefix('purchases').'.grand_total - '.$this->db->dbprefix('purchases').'.paid) as balance')
                            ->join('purchases', 'purchases.supplier_id = companies.id', 'INNER')
                            ->where('('.$this->db->dbprefix('purchases').'.grand_total - '.$this->db->dbprefix('purchases').'.paid) > 0')
                            ->where('purchases.status != "returned"')
                            ->where('companies.id', $company_id)
                            ->where('companies.group_name', 'supplier')
                            ->get('companies');

            if ($q->num_rows() > 0) {
                $result = $q->row();
                return $result->balance;
            }

        } else if ($type == 'customer_cxp') {

            $q = $this->db->select('SUM('.$this->db->dbprefix('sales').'.grand_total - '.$this->db->dbprefix('sales').'.paid) as balance')
                            ->join('sales', 'sales.customer_id = companies.id', 'INNER')
                            ->where('('.$this->db->dbprefix('sales').'.grand_total - '.$this->db->dbprefix('sales').'.paid) > 0')
                            ->where('sales.sale_status != "returned"')
                            ->where('companies.id', $company_id)
                            ->where('companies.group_name', 'customer')
                            ->get('companies');

            if ($q->num_rows() > 0) {
                $result = $q->row();
                return $result->balance;
            }

        }

        return false;

    }


    public function recontabilizar_deposit($id, $dtype = 1){
        $q = $this->db->get_where('deposits', ['id' => $id]);
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
            if(!$this->site->getEntryTypeNumberExisting($data, true, false)){
                $msg = "El anticipo (".$data['reference_no'].") no estaba contabilizado y se contabilizó. </br>";
            } else {
                $msg = "El anticipo (".$data['reference_no'].") ya estaba contabilizado y se reprocesó la contabilización. </br>";
            }
            $this->site->wappsiContabilidadDeposito($data, $dtype);
        } else {
            $msg = "Error. </br>";
        }
        return $msg;
    }


    public function user_activities_log($id, $data = array(), $data_aditional = array(), $resoluciones = array(), $default_dts = array(), $data_categories_concession = array())
    {
        $resolucionesIniciales = [];
        $defaultDocumentsIniciales = [];
        $categoriasConcesionesIniciales = [];

        $modulary = $this->Settings->modulary;
        if ($this->site->wappsiContabilidadVerificacion(date('Y-m-d'))) {
            $accounting_year_suffix = "_con".$this->session->userdata('accounting_module');
        } else {
            return false;
        }

        foreach ($data as $key => $value) {
            $this->db->select($this->db->dbprefix('companies'). '.' .$key. ' AS "' .$key. '"' );
        }
        $this->db->from($this->db->dbprefix('companies'));
        $this->db->where($this->db->dbprefix('companies').'.id', $id);
        $q1 = $this->db->get();
        $initialData1 = $q1->row_array();

        foreach ($data_aditional as $key => $value) {
            if ($key == 'default_price_group') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('price_groups')} WHERE id = {$this->db->dbprefix('biller_data')}.default_price_group ) AS 'default_group_price' ");
            }if ($key == 'default_warehouse_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('warehouses')} WHERE id = {$this->db->dbprefix('biller_data')}.default_warehouse_id ) AS 'default_warehouse' ");
            }if ($key == 'default_customer_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('companies')} WHERE id = {$this->db->dbprefix('biller_data')}.default_customer_id ) AS 'default_customer' ");
            }if ($key == 'default_seller_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('companies')} WHERE id = {$this->db->dbprefix('biller_data')}.default_seller_id ) AS 'default_seller' ");
            }if ($modulary == 1) {
                if ($key == 'default_cost_center_id') {

                $this->db->select("(SELECT name FROM {$this->db->dbprefix('cost_centers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.default_cost_center_id ) AS '" .$key. "'");
            }
            }if ($key == 'product_order') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.product_order = 0 THEN '" .lang('default'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.product_order = 1 THEN '" .lang('category'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.product_order = 2 THEN '" .lang('brand'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.product_order END AS '" .$key. "' ");
            }if ($key == 'branch_type') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.branch_type = 1 THEN '" .lang('physical'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.branch_type = 2 THEN '" .lang('virtual'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.branch_type END AS '" .$key. "' ");
            }if ($key == 'charge_shipping_cost') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.charge_shipping_cost = 0 THEN '" .lang('without_cost'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.charge_shipping_cost = 1 THEN '" .lang('according_to_location'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.charge_shipping_cost = 2 THEN '" .lang('according_to_volume_and_weight'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.charge_shipping_cost = 3 THEN '" .lang('fixed_cost'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.charge_shipping_cost END AS '" .$key. "' ");
            }if ($key == 'language') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.language = '', '" .lang('setted_in_settings'). "', {$this->db->dbprefix('biller_data')}.language ) AS '" .$key. "'");
            }if ($key == 'currency') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.currency = '', '" .lang('setted_in_settings'). "', {$this->db->dbprefix('biller_data')}.currency ) AS '" .$key. "' ");
            }if ($key == 'prioridad_precios_producto') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.prioridad_precios_producto = '', '" .lang('setted_in_settings'). "', {$this->db->dbprefix('biller_data')}.prioridad_precios_producto ) AS '".$key."' ");
            }if ($key == 'concession_status') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.concession_status = 0, '" .lang('inactive'). "', '" .lang('active'). "' ) AS '" .$key. "' ");
            }if ($key == 'default_pos_section') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.default_pos_section = 0, '" .lang('select'). "',  IF ({$this->db->dbprefix('biller_data')}.default_pos_section = 999, '" .lang('favorites'). "', IF ({$this->db->dbprefix('biller_data')}.default_pos_section = 998, '" .lang('promotions'). "',  (SELECT NAME FROM {$this->db->dbprefix('categories')} WHERE  id = '" .$value. "' )))) AS '" .$key.  "'  ");
            }if ($modulary == 1) {
                if ($key == 'rete_autoica_account') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_autoica_account ) AS '" .$key. "'");
                }if ($key == 'rete_autoica_account_counterpart') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_autoica_account_counterpart ) AS '" .$key. "'");
                }if ($key == 'rete_bomberil_account') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_bomberil_account ) AS '" .$key. "'");
                }if ($key == 'rete_bomberil_account_counterpart') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_bomberil_account_counterpart ) AS '" .$key. "'");
                }if ($key == 'rete_autoaviso_account') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_autoaviso_account ) AS '" .$key. "'");
                }if ($key == 'rete_autoaviso_account_counterpart') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_autoaviso_account_counterpart ) AS '" .$key. "'");
                }if ($key == 'cash_payment_method_account') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.cash_payment_method_account ) AS '" .$key. "'");
                }
            }
            if ($key == 'pin_code_request') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.pin_code_request = 1 THEN '" .lang('control_profitability_margin'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.pin_code_request = 2 THEN '" .lang('control_sale_actions'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.pin_code_request = 3 THEN '" .lang('control_both'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.pin_code_request END AS '" .$key. "' ");
            }if ($key == 'pin_code_method') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.pin_code_method = 1 THEN '" .lang('manual_pin_code'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.pin_code_method = 2 THEN '" .lang('random_pin_code'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.pin_code_method END AS '" .$key. "' ");
            }else if ($key !== 'default_price_group' && $key !== 'default_warehouse_id' && $key !== 'default_customer_id' && $key !== 'default_seller_id' && $key !== 'default_cost_center_id' && $key !== 'product_order' && $key !== 'branch_type' && $key !== 'charge_shipping_cost' && $key !== 'language' && $key !== 'currency' && $key !== 'prioridad_precios_producto' && $key !== 'concession_status' && $key !== 'default_pos_section' && $key !== 'preparation_adjustment_document_type_id' && $key !== 'rete_autoica_account' && $key !== 'rete_autoica_account_counterpart' && $key !== 'rete_bomberil_account' && $key !== 'rete_bomberil_account_counterpart' && $key !== 'rete_autoaviso_account' && $key !== 'rete_autoaviso_account_counterpart' && $key !== 'cash_payment_method_account' && $key !== 'pin_code_request' &&   $key !== 'city_code') {
                $this->db->select($this->db->dbprefix('biller_data').'.'.$key. ' AS "' .$key. '"' );
            }
        }
        $this->db->from($this->db->dbprefix('biller_data'));
        $this->db->where($this->db->dbprefix('biller_data').'.biller_id', $id);
        $q2= $this->db->get();
        $initialData2 = $q2->row_array();
        $initialData = array_merge($initialData1, $initialData2);

        // MANEJO PREFIJOS
        $this->db->select($this->db->dbprefix('documents_types').'.sales_prefix');
        $this->db->select($this->db->dbprefix('documents_types').'.module');
        $this->db->from($this->db->dbprefix('biller_documents_types'));
        $this->db->join($this->db->dbprefix('documents_types'), $this->db->dbprefix('documents_types').'.id = ' .$this->db->dbprefix('biller_documents_types'). '.document_type_id', 'inner');
        $this->db->where($this->db->dbprefix('biller_documents_types').'.biller_id', $id);
        $this->db->order_by($this->db->dbprefix('documents_types').'.module', 'ASC');
        $q3 = $this->db->get();
        if ($q3->num_rows() > 0) {
            foreach ($q3->result_array() as $key => $value) {
                $resolucionesIniciales[$value['sales_prefix']] = $value['module'];
            }
        }
        $initialData = array_merge($initialData, $resolucionesIniciales);

        // MANEJO PREFIJOS DEFAULT
        $this->db->select("(SELECT sales_prefix FROM {$this->db->dbprefix('documents_types')} WHERE id = {$this->db->dbprefix('biller_data')}.pos_document_type_default ) AS 'pos_document_type_default' ");
        $this->db->select("(SELECT sales_prefix FROM {$this->db->dbprefix('documents_types')} WHERE id = {$this->db->dbprefix('biller_data')}.detal_document_type_default ) AS 'detal_document_type_default' ");
        $this->db->select("(SELECT sales_prefix FROM {$this->db->dbprefix('documents_types')} WHERE id = {$this->db->dbprefix('biller_data')}.purchases_document_type_default ) AS 'purchases_document_type_default' ");
        $this->db->select("(SELECT sales_prefix FROM {$this->db->dbprefix('documents_types')} WHERE id = {$this->db->dbprefix('biller_data')}.preparation_adjustment_document_type_id ) AS 'preparation_adjustment_document_type_id' ");
        $this->db->from($this->db->dbprefix('biller_data'));
        $this->db->where($this->db->dbprefix('biller_data').'.biller_id', $id);
        $q4 = $this->db->get();
        $defaultDocumentsIniciales = $q4->row_array();
        $initialData = array_merge($initialData, $defaultDocumentsIniciales);

        // MANEJO DE CONSECIONES
        $this->db->select($this->db->dbprefix('categories').'.name');
        $this->db->select($this->db->dbprefix('biller_categories_concessions').'.concession_code');
        $this->db->select($this->db->dbprefix('biller_categories_concessions').'.concession_name');
        $this->db->select($this->db->dbprefix('biller_categories_concessions').'.id');
        $this->db->from($this->db->dbprefix('biller_categories_concessions'));
        $this->db->join($this->db->dbprefix('categories'), $this->db->dbprefix('categories').'.id = '.$this->db->dbprefix('biller_categories_concessions').'.category_id', 'inner');
        $this->db->where($this->db->dbprefix('biller_categories_concessions').'.biller_id', $id);
        $this->db->order_by($this->db->dbprefix('biller_categories_concessions').'.category_id', 'ASC');
        $q5 = $this->db->get();
        $categoriasConcesionesIniciales = $q5->result_array();
        $initialData = array_merge($initialData, $categoriasConcesionesIniciales);

        return $initialData;
    }

    public function user_activities_log_register($id, $data = array(), $data_aditional = array(), $resoluciones = array(), $default_dts = array(), $data_categories_concession = array(), $datosIniciales = array())
    {
        $resolucionesFinales = [];
        $defaultDocumentsFinales = [];
        $categoriasConcesionesFinales = [];
        $diferenciasResoluciones = [];
        $changesResoluciones = '';
        $changesPrefijosDefault = '';
        $changesConcessiones = '';
        $changes = '';

        $modulary = $this->Settings->modulary;
        if ($this->site->wappsiContabilidadVerificacion(date('Y-m-d'))) {
            $accounting_year_suffix = "_con".$this->session->userdata('accounting_module');
        }

        $this->db->select($this->db->dbprefix('companies').'.name');
        $this->db->from($this->db->dbprefix('companies'));
        $this->db->where($this->db->dbprefix('companies').'.id', $id);
        $qs = $this->db->get();
        $dataCompanies = $qs->row_array();
        $sucursal = $dataCompanies['name'];

        foreach ($data as $key => $value) {
            $this->db->select($this->db->dbprefix('companies'). '.' .$key. ' AS "' .$key. '"' );
        }
        $this->db->from($this->db->dbprefix('companies'));
        $this->db->where($this->db->dbprefix('companies').'.id', $id);
        $q1 = $this->db->get();
        $finalData1 = $q1->row_array();

        foreach ($data_aditional as $key => $value) {
            if ($key == 'default_price_group') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('price_groups')} WHERE id = {$this->db->dbprefix('biller_data')}.default_price_group ) AS 'default_group_price' ");
            }if ($key == 'default_warehouse_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('warehouses')} WHERE id = {$this->db->dbprefix('biller_data')}.default_warehouse_id ) AS 'default_warehouse' ");
            }if ($key == 'default_customer_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('companies')} WHERE id = {$this->db->dbprefix('biller_data')}.default_customer_id ) AS 'default_customer' ");
            }if ($key == 'default_seller_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('companies')} WHERE id = {$this->db->dbprefix('biller_data')}.default_seller_id ) AS 'default_seller' ");
            }if ($modulary == 1) {
                if ($key == 'default_cost_center_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('cost_centers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.default_cost_center_id ) AS '" .$key. "'");
            }
            }if ($key == 'product_order') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.product_order = 0 THEN '" .lang('default'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.product_order = 1 THEN '" .lang('category'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.product_order = 2 THEN '" .lang('brand'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.product_order END AS '" .$key. "' ");
            }if ($key == 'branch_type') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.branch_type = 1 THEN '" .lang('physical'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.branch_type = 2 THEN '" .lang('virtual'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.branch_type END AS '" .$key. "' ");
            }if ($key == 'charge_shipping_cost') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.charge_shipping_cost = 0 THEN '" .lang('without_cost'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.charge_shipping_cost = 1 THEN '" .lang('according_to_location'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.charge_shipping_cost = 2 THEN '" .lang('according_to_volume_and_weight'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.charge_shipping_cost = 3 THEN '" .lang('fixed_cost'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.charge_shipping_cost END AS '" .$key. "' ");
            }if ($key == 'language') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.language = '', '" .lang('setted_in_settings'). "', {$this->db->dbprefix('biller_data')}.language ) AS '" .$key. "'");
            }if ($key == 'currency') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.currency = '', '" .lang('setted_in_settings'). "', {$this->db->dbprefix('biller_data')}.currency ) AS '" .$key. "' ");
            }if ($key == 'prioridad_precios_producto') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.prioridad_precios_producto = '', '" .lang('setted_in_settings'). "', {$this->db->dbprefix('biller_data')}.prioridad_precios_producto ) AS '".$key."' ");
            }if ($key == 'concession_status') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.concession_status = 0, '" .lang('inactive'). "', '" .lang('active'). "' ) AS '" .$key. "' ");
            }if ($key == 'default_pos_section') {
                $this->db->select("IF ({$this->db->dbprefix('biller_data')}.default_pos_section = 0, '" .lang('select'). "',  IF ({$this->db->dbprefix('biller_data')}.default_pos_section = 999, '" .lang('favorites'). "', IF ({$this->db->dbprefix('biller_data')}.default_pos_section = 998, '" .lang('promotions'). "',  (SELECT NAME FROM {$this->db->dbprefix('categories')} WHERE  id = '" .$value. "' )))) AS '" .$key.  "'  ");
            }if ($modulary == 1) {
                if ($key == 'rete_autoica_account') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_autoica_account ) AS '" .$key. "'");
                }if ($key == 'rete_autoica_account_counterpart') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_autoica_account_counterpart ) AS '" .$key. "'");
                }if ($key == 'rete_bomberil_account') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_bomberil_account ) AS '" .$key. "'");
                }if ($key == 'rete_bomberil_account_counterpart') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_bomberil_account_counterpart ) AS '" .$key. "'");
                }if ($key == 'rete_autoaviso_account') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_autoaviso_account ) AS '" .$key. "'");
                }if ($key == 'rete_autoaviso_account_counterpart') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.rete_autoaviso_account_counterpart ) AS '" .$key. "'");
                }if ($key == 'cash_payment_method_account') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('ledgers')}".$accounting_year_suffix." WHERE id = {$this->db->dbprefix('biller_data')}.cash_payment_method_account ) AS '" .$key. "'");
                }
            }if ($key == 'pin_code_request') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.pin_code_request = 1 THEN '" .lang('control_profitability_margin'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.pin_code_request = 2 THEN '" .lang('control_sale_actions'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.pin_code_request = 3 THEN '" .lang('control_both'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.pin_code_request END AS '" .$key. "' ");
            }if ($key == 'pin_code_method') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('biller_data')}.pin_code_method = 1 THEN '" .lang('manual_pin_code'). "'
                                        WHEN {$this->db->dbprefix('biller_data')}.pin_code_method = 2 THEN '" .lang('random_pin_code'). "'
                                        ELSE {$this->db->dbprefix('biller_data')}.pin_code_method END AS '" .$key. "' ");
            }else if ($key !== 'default_price_group' && $key !== 'default_warehouse_id' && $key !== 'default_customer_id' && $key !== 'default_seller_id' && $key !== 'default_cost_center_id' && $key !== 'product_order' && $key !== 'branch_type' && $key !== 'charge_shipping_cost' && $key !== 'language' && $key !== 'currency' && $key !== 'prioridad_precios_producto' && $key !== 'concession_status' && $key !== 'default_pos_section' && $key !== 'preparation_adjustment_document_type_id' && $key !== 'rete_autoica_account' && $key !== 'rete_autoica_account_counterpart' && $key !== 'rete_bomberil_account' && $key !== 'rete_bomberil_account_counterpart' && $key !== 'rete_autoaviso_account' && $key !== 'rete_autoaviso_account_counterpart' && $key !== 'cash_payment_method_account' && $key !== 'pin_code_request' && $key !== 'city_code' ) {
                $this->db->select($this->db->dbprefix('biller_data').'.'.$key. ' AS "' .$key. '"' );
            }
        }
        $this->db->from($this->db->dbprefix('biller_data'));
        $this->db->where($this->db->dbprefix('biller_data').'.biller_id', $id);
        $q2= $this->db->get();
        $finalData2 = $q2->row_array();
        $finalData = array_merge($finalData1, $finalData2);

        $size1 = count($finalData);
        if (is_bool($datosIniciales) === false && !is_null($datosIniciales) ) {
            $datosIniciales1 = array_slice($datosIniciales, 0, $size1);
            $diferencias = array_diff($finalData, $datosIniciales1);
            if (!empty($diferencias)) {
                foreach ($diferencias as $key => $value) {
                    $valueInicial = trim($datosIniciales1[$key]);
                    $valueFinal = trim($finalData[$key]);
                    $changes .=  lang($key)  . ' '. lang('from') . ': ' . (($datosIniciales1[$key] == "" || is_null($datosIniciales1[$key])) ? "Vacío" : $valueInicial ). '  ' .lang('to') . ': '  . (($finalData[$key] == "" || is_null($finalData[$key])) ? "Vacío" : $valueFinal ) .', ';
                }
            }

            // MANEJO PREFIJOS
            $this->db->select($this->db->dbprefix('documents_types').'.sales_prefix');
            $this->db->select($this->db->dbprefix('documents_types').'.module');
            $this->db->from($this->db->dbprefix('biller_documents_types'));
            $this->db->join($this->db->dbprefix('documents_types'), $this->db->dbprefix('documents_types').'.id = ' .$this->db->dbprefix('biller_documents_types'). '.document_type_id', 'inner');
            $this->db->where($this->db->dbprefix('biller_documents_types').'.biller_id', $id);
            $this->db->order_by($this->db->dbprefix('documents_types').'.module', 'ASC');
            $q3 = $this->db->get();
            if ($q3->num_rows() > 0) {
                foreach ($q3->result_array() as $key => $value) {
                    $resolucionesFinales[$value['sales_prefix']] = $value['module'];
                }
            }

            $datosIniciales = array_slice($datosIniciales, $size1);
            $n = 0;
            $bandera = 0;
            foreach ( $datosIniciales as $key => $value ) {
                if ( $key !== 'pos_document_type_default' && $bandera == 0 ) { $n++; }
                else { $bandera = 1; }
            }
            $datosInicialesresoluciones = array_slice($datosIniciales, 0, $n);
            foreach ($datosInicialesresoluciones as $keyI => $valueI) {
                if (!array_key_exists($keyI, $resolucionesFinales)) {
                    $changesResoluciones .=  ' Elimino resolución ' .lang($keyI) . ' ' .lang('to'). ': ' .lang('modules')[$valueI].', ';
                }
            }
            foreach ($resolucionesFinales as $keyF => $valueF) {
                if (!array_key_exists($keyF, $datosInicialesresoluciones)) {
                    $changesResoluciones .=  ' Inserto resolución ' .lang($keyF) . ' ' .lang('to'). ': ' .lang('modules')[$valueF].', ';
                }
            }
            $changes = $changes.$changesResoluciones;

            // MANEJO PREFIJOS DEFAULT
            $this->db->select("(SELECT sales_prefix FROM {$this->db->dbprefix('documents_types')} WHERE id = {$this->db->dbprefix('biller_data')}.pos_document_type_default ) AS 'pos_document_type_default' ");
            $this->db->select("(SELECT sales_prefix FROM {$this->db->dbprefix('documents_types')} WHERE id = {$this->db->dbprefix('biller_data')}.detal_document_type_default ) AS 'detal_document_type_default' ");
            $this->db->select("(SELECT sales_prefix FROM {$this->db->dbprefix('documents_types')} WHERE id = {$this->db->dbprefix('biller_data')}.purchases_document_type_default ) AS 'purchases_document_type_default' ");
            $this->db->select("(SELECT sales_prefix FROM {$this->db->dbprefix('documents_types')} WHERE id = {$this->db->dbprefix('biller_data')}.preparation_adjustment_document_type_id ) AS 'preparation_adjustment_document_type_id' ");
            $this->db->from($this->db->dbprefix('biller_data'));
            $this->db->where($this->db->dbprefix('biller_data').'.biller_id', $id);
            $q4 = $this->db->get();
            $defaultDocumentsFinales = $q4->row_array();
            $datosIniciales = array_slice($datosIniciales, $n);
            $size2 = count($defaultDocumentsFinales);
            $datosIniciales2 = array_slice($datosIniciales, 0, $size2);
            $diferencias = array_diff($defaultDocumentsFinales, $datosIniciales2);
            if (!empty($diferencias)) {
                foreach ($diferencias as $key => $value) {
                    $changesPrefijosDefault .=  lang($key)  . ' '. lang('from') . ': ' . (($datosIniciales2[$key] == "" || is_null($datosIniciales2[$key])) ? "vacío" : $datosIniciales2[$key]) . '  ' .lang('to') . ': '  . (($defaultDocumentsFinales[$key] == "" || is_null($defaultDocumentsFinales[$key])) ? "vacío" : $defaultDocumentsFinales[$key] ) .', ';
                }
            }
            $changes = $changes.$changesPrefijosDefault;

            // MANEJO DE CONSECIONES
            $this->db->select($this->db->dbprefix('categories').'.name');
            $this->db->select($this->db->dbprefix('biller_categories_concessions').'.concession_code');
            $this->db->select($this->db->dbprefix('biller_categories_concessions').'.concession_name');
            $this->db->select($this->db->dbprefix('biller_categories_concessions').'.id');
            $this->db->from($this->db->dbprefix('biller_categories_concessions'));
            $this->db->join($this->db->dbprefix('categories'), $this->db->dbprefix('categories').'.id = '.$this->db->dbprefix('biller_categories_concessions').'.category_id', 'inner');
            $this->db->where($this->db->dbprefix('biller_categories_concessions').'.biller_id', $id);
            $this->db->order_by($this->db->dbprefix('biller_categories_concessions').'.category_id', 'ASC');
            $q5 = $this->db->get();
            $categoriasConcesionesFinales = $q5->result_array();
            $datosIniciales = array_slice($datosIniciales, $size2);
            foreach ($datosIniciales as $keyI => $valueI) {
                foreach ($categoriasConcesionesFinales as $keyF => $valueF) {
                    if ($valueI['id'] == $valueF['id']) {
                        if ( ($valueI['concession_code'] !== $valueF['concession_code']) && ($valueI['concession_name'] !== $valueF['concession_name']) ) {
                            $changesConcessiones.= 'Actualizo concesión ' .$valueI['name']. ' ' .lang('code'). ' ' .lang('from'). ': ' .( ($valueI['concession_code'] == '' || is_null($valueI['concession_code'])) ? "vacío" : $valueI['concession_code'] ). ' ' .lang('to'). ': ' .( ($valueF['concession_code'] == '' || is_null($valueF['concession_code'])) ? "vacío" : $valueF['concession_code'] ). ' ' .lang('name'). ' ' .lang('from'). ': ' .( ($valueI['concession_name'] == '' || is_null($valueI['concession_name'])) ? "vacío" : $valueI['concession_name'] ). ' ' .lang('to'). ': ' .( ($valueF['concession_name'] == '' || is_null($valueF['concession_name'])) ? "vacío" : $valueF['concession_name'] ). ', ';
                        }if ( ($valueI['concession_code'] !== $valueF['concession_code']) && ($valueI['concession_name'] == $valueF['concession_name']) ) {
                            $changesConcessiones.= 'Actualizo concesión ' .$valueI['name']. ' ' .lang('code'). ' ' .lang('from'). ': ' .( ($valueI['concession_code'] == '' || is_null($valueI['concession_code'])) ? "vacío" : $valueI['concession_code'] ). ' ' .lang('to'). ': ' .( ($valueF['concession_code'] == '' || is_null($valueF['concession_code'])) ? "vacío" : $valueF['concession_code'] ). ', ';
                        }if ( ($valueI['concession_code'] == $valueF['concession_code']) && ($valueI['concession_name'] !== $valueF['concession_name']) ) {
                            $changesConcessiones.= 'Actualizo concesión ' .$valueI['name']. ' ' .lang('code'). ' ' .lang('from'). ': ' .( ($valueI['concession_name'] == '' || is_null($valueI['concession_name'])) ? "vacío" : $valueI['concession_name'] ). ' ' .lang('to'). ': ' .( ($valueF['concession_name'] == '' || is_null($valueF['concession_name'])) ? "vacío" : $valueF['concession_name'] ). ', ';
                        }
                    }
                }
            }
            $changes = $changes.$changesConcessiones;
            $changes = trim($changes,  ", ");
            if ($changes !== '' ) {
                $txt_fields_changed = sprintf(lang('user_fields_changed_settings'), $this->session->first_name." ".$this->session->last_name, $sucursal. " : " . $changes );
                $this->db->insert('user_activities', [
                                    'date' => date('Y-m-d H:i:s'),
                                    'type_id' => 1,
                                    'user_id' => $this->session->userdata('user_id'),
                                    'module_name' => 'billers',
                                    'table_name' => 'billers',
                                    'record_id' => 1,
                                    'description' => $txt_fields_changed,
                ]);
            }
        }
    }
    public function get_group_by_name($name){
        $q = $this->db->get_where('groups', ['name'=>$name]);
        if ($q->num_rows() > 0) {
            $q = $q->row();
            return $q->id;
        }
    }

    public function get_customer_xls_additional_data($customer_id){
        $q = $this->db->select('sales.grand_total, sales.date, tbl.num_sales, tbl2.date AS first_sale_date')
                 ->join('(SELECT COUNT(id) num_sales, MAX(date) last_date FROM sma_sales WHERE customer_id = '.$customer_id.' AND grand_total > 0 GROUP BY customer_id) as tbl', 'tbl.last_date = sales.date')
                 ->join('(SELECT date, customer_id FROM sma_sales WHERE customer_id = '.$customer_id.' ORDER BY id ASC LIMIT 1) tbl2', 'sales.customer_id = tbl2.customer_id')
                 ->where('sales.customer_id', $customer_id)->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_biller_schedule($biller_id){
        $q = $this->db->get_where('schedule', ['biller_id'=>$biller_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_customer_giftcard_balance($customer_id){
        $q = $this->db->select('SUM(COALESCE(balance, 0)) as balance')
                      ->where('customer_id', $customer_id)
                      ->get('gift_cards');
        if ($q->num_rows() > 0) {
            return $q->row()->balance;
        }
        return false;
    }

    public function sync_customer_award_points($customer_id){

        $actual_date = '"'.date('Y-m-d H:i:s').'"';
        $sql_1 = "INSERT INTO sma_award_points (
            `id`,
            `date`,
            `type`,
            `created_by`,
            `customer_id`,
            `biller_id`,
            `sale_id`,
            `gift_card_id`,
            `movement_value`,
            `movement_points`
        ) (SELECT
                    NULL,
                    S.date,
                    if(S.grand_total < 0, 2, 1) as type,
                    S.created_by,
                    S.customer_id,
                    S.biller_id,
                    S.id,
                    NULL,
                    S.grand_total as movement_value,
                    SUM(ROUND((S.grand_total / ST.each_spent) * ST.ca_point)) movement_points
                FROM
                    sma_sales S
                        INNER JOIN
                    sma_settings ST ON ST.setting_id = 1
                        INNER JOIN
                    sma_companies C ON C.id = S.customer_id
                        LEFT JOIN
                    sma_award_points AP ON AP.sale_id = S.id
                WHERE S.date >= ST.sync_award_points_start_date AND C.award_points_no_management = 0
                AND S.customer_id = {$customer_id} AND AP.id IS NULL
                GROUP BY S.id);";
        $sql_2 = "UPDATE sma_award_points AP
                    INNER JOIN (SELECT
                    AP.id,
                    S.grand_total AS movement_value,
                    AP.movement_points,
                    SUM(ROUND((S.grand_total / ST.each_spent) * ST.ca_point)) calculated_movement_points
                FROM
                    sma_sales S
                        INNER JOIN
                    sma_settings ST ON ST.setting_id = 1
                        INNER JOIN
                    sma_companies C ON C.id = S.customer_id
                        INNER JOIN
                    sma_award_points AP ON AP.sale_id = S.id
                WHERE S.date >= ST.sync_award_points_start_date AND C.award_points_no_management = 0
                AND S.customer_id = {$customer_id}
                GROUP BY S.id
                HAVING AP.movement_points != calculated_movement_points) tbl ON tbl.id = AP.id
                SET
                AP.movement_points = tbl.calculated_movement_points,
                AP.movement_value = tbl.movement_value,
                AP.last_update = {$actual_date}
                WHERE AP.movement_points != tbl.calculated_movement_points;";
        $sql_3 = "UPDATE sma_award_points AP
                    INNER JOIN (SELECT
                    AP.id,
                    GC.value,
                    GC.ca_points_redeemed,
                    AP.movement_value,
                    AP.movement_points,
                    (GC.ca_points_redeemed * -1) AS correct_points
                    FROM sma_gift_cards GC
                    LEFT JOIN sma_award_points AP ON AP.gift_card_id = GC.id
                WHERE GC.ca_points_redeemed > 0 AND ((GC.ca_points_redeemed * -1) != AP.movement_points)
                AND GC.customer_id = {$customer_id}) tbl ON tbl.id = AP.id
                SET
                    AP.movement_points = tbl.correct_points,
                    AP.last_update = {$actual_date}
                WHERE AP.movement_points != tbl.correct_points;";
        $sql_4 = "INSERT INTO sma_award_points (
                    `id`,
                    `date`,
                    `type`,
                    `created_by`,
                    `customer_id`,
                    `biller_id`,
                    `sale_id`,
                    `gift_card_id`,
                    `movement_value`,
                    `movement_points`
                ) (SELECT
                    NULL,
                    GC.date,
                    2,
                    GC.created_by,
                    GC.customer_id,
                    NULL,
                    NULL,
                    GC.id,
                    GC.value as movement_value,
                    (GC.ca_points_redeemed * -1) as movement_points
                    FROM sma_gift_cards GC
                    LEFT JOIN sma_award_points AP ON AP.gift_card_id = GC.id
                WHERE GC.ca_points_redeemed > 0 AND AP.id IS NULL
                AND GC.customer_id = {$customer_id});";

        $sql_5 = "UPDATE sma_companies C
                    INNER JOIN (SELECT C.id, C.name, SUM(AP.movement_points) as correct_award_points, C.award_points FROM sma_companies C
                    INNER JOIN sma_award_points AP ON AP.customer_id = C.id
                    WHERE C.award_points_no_management = 0 AND C.id = {$customer_id}
                GROUP BY C.id
                HAVING C.award_points != correct_award_points) tbl ON tbl.id = C.id
                SET C.award_points = tbl.correct_award_points
                WHERE C.award_points != tbl.correct_award_points;";

        if ($this->db->query($sql_1)) {
            if ($this->db->query($sql_2)) {
                if ($this->db->query($sql_3)) {
                    if ($this->db->query($sql_4)) {
                        if ($this->db->query($sql_5)) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public function get_customer_gift_cards_with_balance($customer_id){
        $q = $this->db->where(['customer_id'=>$customer_id, 'balance >' => 0])->order_by('date asc')->get('gift_cards');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function get_all_customers(){
        $this->db->select('
                        companies.*,
                        documentypes.nombre as tipo_documento,
                        types_person.description as tipo_persona,
                        types_vat_regime.description as tipo_regimen,
                        seller.name as seller
                    ')
                ->join('documentypes', 'documentypes.id = companies.tipo_documento', 'left')
                ->join('types_person', 'types_person.id = companies.type_person', 'left')
                ->join('types_vat_regime', 'types_vat_regime.id = companies.tipo_regimen', 'left')
                ->join('companies seller', 'seller.id = companies.customer_seller_id_assigned', 'left')
                ;
        $q = $this->db->where('companies.group_name', 'customer')->get('companies');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function getBillerData($data)
    {
        $this->db->where($data);
        $q = $this->db->get('biller_data');
        return $q->row();
    }

    public function insertCustomer($data)
    {
        if ($this->db->insert('companies', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function insertAddress($data)
    {
        if ($this->db->insert('addresses', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function getBillerDocumentType($data)
    {
        $this->db->select('b.id, document_type_id, name, bd.default_warehouse_id, bd.default_seller_id');
        $this->db->where($data);
        $this->db->join('documents_types dt', 'dt.id = biller_documents_types.document_type_id');
        $this->db->join('companies b', 'b.id = biller_documents_types.biller_id');
        $this->db->join('biller_data bd', 'bd.biller_id = b.id');
        $q = $this->db->get('biller_documents_types');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getCustomerSuggestionsAddresses($term, $limit = 10)
    {
        $q = $this->db->select("{$this->db->dbprefix('addresses')}.id,
            CONCAT(
                    {$this->db->dbprefix('companies')}.name,
                    ' - (',
                    {$this->db->dbprefix('addresses')}.sucursal,
                    ' Dir: ',{$this->db->dbprefix('addresses')}.direccion,
                    ')'
                ) as text, CONCAT({$this->db->dbprefix('companies')}.name, ' - (', {$this->db->dbprefix('addresses')}.sucursal, ')') as value, {$this->db->dbprefix('addresses')}.phone")
                ->join('companies', 'companies.id = addresses.company_id')
                ->where("(
                            {$this->db->dbprefix('companies')}.id LIKE '%" . $term . "%'
                            OR {$this->db->dbprefix('companies')}.name LIKE '%" . $term . "%'
                            OR {$this->db->dbprefix('companies')}.company LIKE '%" . $term . "%'
                            OR {$this->db->dbprefix('companies')}.email LIKE '%" . $term . "%'
                            OR {$this->db->dbprefix('companies')}.phone LIKE '%" . $term . "%'
                            OR {$this->db->dbprefix('companies')}.vat_no LIKE '%" . $term . "%'
                            OR {$this->db->dbprefix('addresses')}.id LIKE '%" . $term . "%'
                            OR {$this->db->dbprefix('addresses')}.direccion LIKE '%" . $term . "%'
                            OR {$this->db->dbprefix('addresses')}.sucursal LIKE '%" . $term . "%'
                        )")->limit($limit)
                ->get('addresses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getDocumentTypeDevolutionDefaultPos($biller_id)
    {
        $q = $this->db->select("{$this->db->dbprefix('biller_data')}.pos_document_type_default_returns")
                      ->where(" {$this->db->dbprefix('biller_data')}.biller_id = $biller_id AND
                                {$this->db->dbprefix('biller_data')}.pos_document_type_default_returns IN
                                (SELECT {$this->db->dbprefix('documents_types')}.id
                                    FROM {$this->db->dbprefix('documents_types')}
                                    WHERE {$this->db->dbprefix('documents_types')}.module = 3
                                        AND {$this->db->dbprefix('documents_types')}.factura_electronica = 0
                                ) ")
                        ->get($this->db->dbprefix('biller_data'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function validate_existing_affiliate($fields, $affiliate_id = NULL)
    {
        
        if (!is_null($affiliate_id)) { $this->db->where('id !=', $affiliate_id); }
        $response = $this->db->where($fields)
                 ->where('group_name', 'affiliate')
                 ->get("companies");
        if ($response->num_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function existing_sales_by_affiliate_id($affiliate_id)
    {
        $this->db->select('id');
        $this->db->from('sales');
        $this->db->where('affiliate_id', $affiliate_id);
        $response = $this->db->get();

        if ($response->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }
}