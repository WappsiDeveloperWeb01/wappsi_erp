<?php 

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
  function Header()
  {
    $this->SetTitle($this->sma->utf8Decode('Traslado'));
    $this->RoundedRect(13, 7, 117, 29, 3, '1234', '');
    $this->setXY(10,7);
    $this->Image(( isset($this->biller->logo) ? base_url() . 'assets/uploads/logos/' . $this->biller->logo : '' ) ,18,17,29);

    $cx = 58;
    $cy = 10;
    $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
    $this->setXY($cx, $cy);
    $this->Cell(72, 5 , $this->sma->utf8Decode(mb_strtoupper($this->biller->company != '-' ? $this->biller->company : $this->biller->name)),'',1,'C');

    $cy+=5;
    $this->setXY($cx, $cy);
    $this->SetFont('Arial','',$this->fuente);
    $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->tipo_documento == 6 ? "-".$this->biller->digito_verificacion : '')),'',1,'C');

    $cy+=3;
    $this->setXY($cx, $cy);
    $this->MultiCell(72,3, $this->sma->utf8Decode($this->biller->address.", ".ucfirst(mb_strtolower($this->biller->city))),0,'C');
    $cy = $this->getY();
      
    $this->setXY($cx, $cy);
    $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'C');

    $cy+=3;
    $this->setXY($cx, $cy);
    $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'C');
    
    $cy+=3;
    $this->setXY($cx, $cy);
    $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'C');

    $cy+=3;
    $this->setXY($cx, $cy);
    $this->Cell(72, 3 , $this->sma->utf8Decode('Fecha : '.$this->factura->date),'',1,'C');

    //derecha
    $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
    $this->RoundedRect(132, 7, 77, 29, 3, '1234', '');
    $cx = 132;
    $cy = 11.5;
    $this->setXY($cx, $cy);
    $this->Cell(77, 10 , $this->sma->utf8Decode('TRASLADO'),'',1,'C');
    $cy +=10;
    $this->setXY($cx, $cy);
    $this->Cell(77, 10 , $this->sma->utf8Decode('N°. '.$this->factura->reference_no),'',1,'C');

    //izquierda
    $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
    $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
    $this->RoundedRect(13, 38, 100, 5, 3, '1', 'DF');
    $this->RoundedRect(13, 43, 100, 25, 3, '4', '');
    
    $cx = 13;
    $cy = 38;
    $this->setXY($cx, $cy);
    $this->Cell(108, 5 , $this->sma->utf8Decode('BODEGA ORIGEN'),'B',1,'C');

    $cx += 3;
    $cy += 8;
    $this->setXY($cx, $cy);
    $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->from_warehouse->name)),'',1,'L');
    
    $cy += 5;
    $this->setXY($cx, $cy);
    $this->SetFont('Arial','',$this->fuente);
    $this->Cell(115, 3 , $this->sma->utf8Decode("Código : ". $this->from_warehouse->code),'',1,'L');

    $cy += 3;
    $this->setXY($cx, $cy);
    $this->Cell(115, 3 , $this->sma->utf8Decode("Dirección : ".$this->sma->clear_tags($this->from_warehouse->address)),'',1,'L');

    $cy += 3;
    $this->setXY($cx, $cy);
    $this->Cell(115, 3 , $this->sma->utf8Decode("Teléfonos : ".$this->from_warehouse->phone),'',1,'L');
      
    $cy += 3;
    $this->setXY($cx, $cy);
    $this->Cell(115, 3 , $this->sma->utf8Decode("Correo : ".$this->from_warehouse->email),'',1,'L');
    
    //derecha
    $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
    $altura = 5;
    $adicional_altura = 2.3;
    $this->setFillColor($this->color_R,$this->color_G,$this->color_B);

    $this->RoundedRect(113, 38, 96, 5, 3, '2', 'DF');
    $this->RoundedRect(113, 43, 96, 25, 3, '3', '');

    $cx = 131;
    $cy = 38;
    $this->setXY($cx, $cy);
    $this->Cell(60, $altura , $this->sma->utf8Decode('BODEGA DESTINO'),'0',1,'C');

    $cx -= 16;
    $cy += $altura+2;
    $this->setXY($cx, $cy);
    $this->Cell(39, 5 , $this->sma->utf8Decode(mb_strtoupper($this->to_warehouse->name)),'',1,'L');

    $cy += 5;
    $this->setXY($cx, $cy);
    $this->SetFont('Arial','',$this->fuente);
    $this->Cell(115, 3 , $this->sma->utf8Decode("Código : ". $this->to_warehouse->code),'',1,'L');

    $cy += 3;
    $this->setXY($cx, $cy);
    $this->Cell(115, 3 , $this->sma->utf8Decode("Dirección : ".$this->sma->clear_tags($this->to_warehouse->address)),'',1,'L');

    $cy += 3;
    $this->setXY($cx, $cy);
    $this->Cell(115, 3 , $this->sma->utf8Decode("Teléfonos : ".$this->to_warehouse->phone),'',1,'L');
      
    $cy += 3;
    $this->setXY($cx, $cy);
    $this->Cell(115, 3 , $this->sma->utf8Decode("Correo : ".$this->to_warehouse->email),'',1,'L');

    $cy += 13;
    $cx = 13;
    $this->setXY($cx, $cy);
    $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
    $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
    $this->Cell(25,5, $this->sma->utf8Decode('Ref.'),'TBLR',0,'C',1);
    $this->Cell(90,5, $this->sma->utf8Decode('Descripción'),'TBLR',0,'C',1);
    $this->Cell(17,5, $this->sma->utf8Decode('Cant.'),'TBLR',0,'C',1);
    $this->Cell(32.2,5, $this->sma->utf8Decode('Vr. unit.'),'TBLR',0,'C',1);
    $this->Cell(32.2,5, $this->sma->utf8Decode('Vr. total'),'TBLR',1,'C',1);
  }

  function Footer()
  {
    // Print centered page number
    $this->RoundedRect(13, 248, 196, 17.5, 3, '1234', '');
    $this->SetXY(7, -30);
    $this->Cell(196, 5 , $this->sma->utf8Decode(''),'',1,'C');
    $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->invoice_footer)), 0, 'L');
    $this->SetXY(7, -14);
    $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica'),'',1,'C');
    $this->SetXY(195, -14);
    $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
  }

  function reduceTextToDescription1($text){
      $text="Nota : ".$text;
      if (strlen($text) > 390) {
          $text = substr($text, 0, 385);
          $text.="...";
          return $text;
      }
      return strip_tags($this->sma->decode_html($text));
  }

  function reduceTextToDescription2($text){
      if (strlen($text) > 380) {
          $text = substr($text, 0, 375);
          $text.="...";
          return $text;
      }
      return strip_tags($this->sma->decode_html($text));
  }

  function crop_text($text, $length = 50){
      if (strlen($text) > $length) {
          $text = substr($text, 0, ($length-5));
          $text.="...";
          return strip_tags($this->sma->decode_html($text));
      }
      return strip_tags($this->sma->decode_html($text));
  }

  function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
  {
      $k = $this->k;
      $hp = $this->h;
      if($style=='F')
          $op='f';
      elseif($style=='FD' || $style=='DF')
          $op='B';
      else
          $op='S';
      $MyArc = 4/3 * (sqrt(2) - 1);
      $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

      $xc = $x+$w-$r;
      $yc = $y+$r;
      $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
      if (strpos($corners, '2')===false)
          $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
      else
          $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

      $xc = $x+$w-$r;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
      if (strpos($corners, '3')===false)
          $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
      else
          $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

      $xc = $x+$r;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
      if (strpos($corners, '4')===false)
          $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
      else
          $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

      $xc = $x+$r ;
      $yc = $y+$r;
      $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
      if (strpos($corners, '1')===false)
      {
          $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
          $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
      }
      else
          $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
      $this->_out($op);
  }

  function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
  {
      $h = $this->h;
      $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
          $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
  }
}

$fuente = 8;
$adicional_fuente = 2;
$color_R = 200;
$color_G = 200;
$color_B = 200;

$pdf = new PDF('P', 'mm', array(216, 279));

/*seteo de variables*/

$pdf->biller = $biller;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->factura = $transfer;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->from_warehouse = $from_warehouse;
$pdf->to_warehouse = $to_warehouse;
$pdf->invoice_footer = $invoice_footer;
$pdf->sma = $this->sma;
$number_convert = new number_convert();
$pdf->setFillColor($color_R, $color_G, $color_B);

$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);

$pdf->fontsize_body = 6.5;
$pdf->fontsize_title = 8;
$pdf->AddPage();

$maximo_footer = 235;

$pdf->SetFont('Arial','',$fuente);
$total_tax_items = 0;
foreach ($rows as $item) {
  $total_tax_items += ($item->item_tax + $item->item_tax_2);
    if ($pdf->getY() > $maximo_footer) {
        $pdf->AddPage();
    }
    $columns = [];
        $inicio_fila_X = $pdf->getX()+6;
        $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_x'] = $pdf->getX()+6;
    $pdf->Cell(31,5, $this->sma->utf8Decode($item->product_code),'0',0,'R');
    $columns[0]['fin_x'] = $pdf->getX();
    $columns[1]['inicio_x'] = $pdf->getX();
        $cX = $pdf->getX();
        $cY = $pdf->getY();
        $inicio_altura_fila = $cY;
    $pdf->MultiCell(90,5, $this->sma->utf8Decode($item->product_name),'','');
    $columns[1]['fin_x'] = $cX+90;
        $fin_altura_fila = $pdf->getY();
        $pdf->setXY($cX+90, $cY);
    $columns[2]['inicio_x'] = $pdf->getX();
    $pdf->Cell(17,5, $this->sma->utf8Decode($this->sma->formatDecimal($item->quantity)),'',0,'C');
    $columns[2]['fin_x'] = $pdf->getX();
    $columns[3]['inicio_x'] = $pdf->getX();
    $pdf->Cell(32.2,5, $this->sma->utf8Decode($this->sma->formatMoney($item->net_unit_cost)),'',0,'R');
    $columns[3]['fin_x'] = $pdf->getX();
    $columns[4]['inicio_x'] = $pdf->getX();
    $pdf->Cell(32.2,5, $this->sma->utf8Decode($this->sma->formatMoney($item->net_unit_cost * $item->quantity)),'',0,'R');
    $columns[4]['fin_x'] = $pdf->getX();
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = $fin_altura_fila - $inicio_altura_fila;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    foreach ($columns as $key => $data) {
        $ancho_column = $data['fin_x'] - $data['inicio_x'];
        if ($altura_fila < 5) {
            $altura_fila = 5;
        }
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
    }
    $pdf->ln($altura_fila);
}

$pdf->SetFont('Arial','',$fuente);
if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX()+6;
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->setX($current_x);
$pdf->Cell(110, 6.125, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->setX(20);
$pdf->Multicell(110, 3, $this->sma->utf8Decode($number_convert->convertir($transfer->grand_total)),'','L');
$pdf->setX(19);

$current_x = $pdf->getX()+6;
$current_y = $pdf->getY();

$pdf->setXY($current_x, $current_y);
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Base'),'',1,'C');
$pdf->setX($current_x);
$pdf->Cell(28.9,5, $this->sma->utf8Decode($this->sma->formatMoney(($transfer->total + $transfer->total_tax))),'',1,'C');

$pdf->setXY($current_x + (28.9 * 1), $current_y);
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Impuesto'),'',1,'C');
$pdf->setX($current_x + (28.9 * 1));
$pdf->Cell(28.9,5, $this->sma->utf8Decode($this->sma->formatMoney($total_tax_items)),'',1,'C');

$pdf->setXY($current_x + (28.9 * 2), $current_y);
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');
$pdf->setX($current_x + 28.9 * 2);
$pdf->Cell(28.9,5, $this->sma->utf8Decode($this->sma->formatMoney($transfer->grand_total)),'',1,'C');

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;


//derecha

$pdf->setXY($cX_items_finished+124, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Sub Total'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatMoney(($transfer->total + $transfer->total_tax))), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_tax_items)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Total'), 'TBLR', 1, 'R', true);
$pdf->SetFont('Arial','',$fuente);
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatMoney($transfer->grand_total)), 'TBR', 1, 'R');


//FIRMAS

//izquierda
$pdf->ln(10);

$current_x = $pdf->getX()+6;
$current_y = $pdf->getY();
$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX()+6;
$current_y = $pdf->getY()+11;
$pdf->setX($current_x);
$pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($transfer->note)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Representante Legal'), '', 1, 'L');

//derecha
$pdf->setXY($current_x+115.6, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();
$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

$descargar = false;

if ($descargar) {
  $pdf->Output("BALANCE_GENERAL.pdf", "D");
} else {
  $pdf->Output("BALANCE_GENERAL.pdf", "I");
}