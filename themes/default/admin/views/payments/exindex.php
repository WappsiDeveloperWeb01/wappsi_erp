<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }
    $(document).ready(function() {
        oTable = $('#SLData').dataTable({
            "aaSorting": [
                [1, "desc"]
            ],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('payments/getPayments/3') ?>',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            'fnRowCallback': function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "multipayment_link";
                return nRow;
            },
            "aoColumns": [{
                    "mRender": checkbox
                },
                null,
                null,
                null,
                {
                    "bVisible": false
                },
                null,
                null,
                null,
                {
                    "mRender": currencyFormat,
                    "bSearchable": false
                },
                {
                    "mRender": currencyFormat,
                    "bSearchable": false
                },
                {
                    "mRender": pay_status
                },
                {
                    "bSortable": false,
                    "mRender": attachment
                },
            ]
        });

    });

    <?php if ($this->session->userdata('remove_multipayments')) { ?>
        if (localStorage.getItem('customer')) {
            localStorage.removeItem('customer');
        }
        if (localStorage.getItem('sales')) {
            localStorage.removeItem('sales');
        }
        if (localStorage.getItem('supplier')) {
            localStorage.removeItem('supplier');
        }
        if (localStorage.getItem('purchases')) {
            localStorage.removeItem('purchases');
        }
        if (localStorage.getItem('payment_distribution')) {
            localStorage.removeItem('payment_distribution');
        }
        if (localStorage.getItem('payment_date')) {
            localStorage.removeItem('payment_date');
        }
        if (localStorage.getItem('consecutive_payment')) {
            localStorage.removeItem('consecutive_payment');
        }
    <?php $this->sma->unset_data('remove_multipayments');
    }
    ?>
</script>

<div class="wrapper wrapper-content  animated fadeInRight no-print">

    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('payments/payment_actions', 'id="action-form"');
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?= admin_url('payments/exadd') ?>">
                                            <i class="fa fa-plus-circle"></i> <?= lang('add_multi_payment_expenses') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a id="post_sale" data-action="post_multi_payments">
                                            <i class="fa fa-plus-circle"></i> <?= lang('post_multi_payments') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="SLData" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("payment_date"); ?></th>
                                            <th><?= lang("affects_to"); ?></th>
                                            <th><?= lang("consecutive_payment"); ?></th>
                                            <th><?= lang("supplier_name"); ?></th>
                                            <th><?= lang("paid_by"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("applied"); ?></th>
                                            <th><?= lang("payment_status"); ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="4" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php }
?>