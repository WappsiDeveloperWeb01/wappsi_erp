<?php defined('BASEPATH') or exit('No direct script access allowed');

class Quotes extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('quotations', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('quotes_model');
        $this->load->admin_model('sales_model');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model("currency_model");
        $this->digital_upload_path = 'files/';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->data['logo'] = true;

    }

    public function index($warehouse_id = null)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('quotes')));
        $meta = array('page_title' => lang('quotes'), 'bc' => $bc);
        $this->page_construct('quotes/index', $meta, $this->data);

    }

    public function p_index($warehouse_id = null)
    {
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }
        
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('quotes_purchases/index', ['page_title' => lang('quotes_purchase_expense')], $this->data);

    }

    public function getQuotes($warehouse_id = null)
    {
        $this->sma->checkPermissions('index');

        $index_type = $this->input->post('index_type');

        if ((!$this->Owner && !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        if ($index_type == "customers") {
            $detail_link = anchor('admin/quotes/view/$1', '<i class="fa fa-print"></i> ' . lang('print'), 'target="_blank"');
            $email_link = anchor('admin/quotes/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_quote'), 'data-toggle="modal" data-target="#myModal"');
            $edit_link = anchor('admin/quotes/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_quote'));
            $convert_link = '
                                <a href="#" class="po open" title= "" data-placement="top" data-content=\'
                                    <p>'.lang("where_to_convert").'  <i class="fa fa-times po-close"></i> </p>
                                    <ul>
                                        <li><a class="po-item" href="'.admin_url("sales/add/$1").'"> '.lang("detal_sale").' </a></li>
                                        <li><a class="po-item" href="'.admin_url("pos/index/$1/1/1").'">'.lang("pos_sale").'</a></li>
                                        <li><a class="po-item" href="'.admin_url("pos/add_wholesale/$1/1").'">'.lang("pos_wholesale").'</a></li>
                                    </ul>
                                    \' rel="popover">
                                    <i class="fa fa-heart"></i> ' . lang('create_sale').'
                                </a>
                            ';
            $pdf_link = anchor('admin/quotes/view/$1/0/1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
            $delete_link = "<a href='#' class='po' title='<b>" . $this->lang->line("delete_quote") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('quotes/delete/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('delete_quote') . "</a>";

        } else if ($index_type == "suppliers") {

            $detail_link = anchor('admin/quotes/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('quote_purchase_expense_details'));
            $email_link = anchor('admin/quotes/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_quote_purchase_expense'), 'data-toggle="modal" data-target="#myModal"');
            $edit_link = anchor('admin/quotes/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_quote_purchase_expense'));
            $pc_link = anchor('admin/purchases/add/$1', '<i class="fa fa-star"></i> ' . lang('create_purchase'));
            $pdf_link = anchor('admin/quotes/view/$1/0/1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
            $delete_link = "<a href='#' class='po' title='<b>" . $this->lang->line("delete_quote_purchase_expense") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('quotes/delete/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('delete_quote_purchase_expense') . "</a>";

        }

        $action = '<div class="text-center"><div class="btn-group text-left">
            <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
            <i class="fas fa-ellipsis-v fa-lg"></i></button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>' . $detail_link . '</li>'.
                            '<li>' . $edit_link . '</li>';

        if ($index_type == "customers") {
            $action .= '<li>' . $convert_link . '</li>';
        } else if ($index_type == "suppliers") {
            $action .= '<li>' . $pc_link . '</li>';
        }


        $action .= '<li>' . $pdf_link . '</li>
                            <li>' . $email_link . '</li>
                        </ul>
                    </div></div>';
        //$action = '<div class="text-center">' . $detail_link . ' ' . $edit_link . ' ' . $email_link . ' ' . $delete_link . '</div>';

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("id, date, reference_no, destination_reference_no, biller, customer, supplier, grand_total, status, quote_type, attachment")
                ->from('quotes')
                ->where('warehouse_id', $warehouse_id);

                if ($index_type) {
                    if ($index_type == "customers") {
                        $this->datatables->where('customer_id >', 0);
                    } else if ($index_type == "suppliers") {
                        $this->datatables->where('supplier_id >', 0);
                    }
                }

        } else {
            $this->datatables
                ->select("id, date, reference_no, destination_reference_no, biller, customer, supplier, grand_total, status, quote_type, attachment")
                ->from('quotes');

                if ($index_type) {
                    if ($index_type == "customers") {
                        $this->datatables->where('customer_id >', 0);
                    } else if ($index_type == "suppliers") {
                        $this->datatables->where('supplier_id >', 0);
                    }
                }
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function modal_view($quote_id = null)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $quote_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->quotes_model->getQuoteByID($quote_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['rows'] = $this->quotes_model->getAllQuoteItems($quote_id);
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;

        $currency = $this->site->getCurrencyByCode($inv->quote_currency);
        $trmrate = 1;
        if (!empty($inv->quote_currency) && $inv->quote_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->quote_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;

        // exit(var_dump($trmrate));

        $this->load_view($this->theme . 'quotes/modal_view', $this->data);

    }

    public function view($quote_id = null, $for_email = false, $download = false)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('id')) {
            $quote_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->quotes_model->getQuoteByID($quote_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['document_type'] = $this->site->getDocumentTypeById($inv->document_type_id);
        $document_type_invoice_format = $this->site->getInvoiceFormatById($this->data['document_type']->module_invoice_format_id);
        $this->data['rows'] = $this->quotes_model->getAllQuoteItems($quote_id, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
        $this->data['customer'] = $this->site->getCompanyByID($inv->supplier_id ? $inv->supplier_id : $inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['sma'] = $this->sma;
        $this->data['biller_logo'] = 2;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $this->data['seller'] = $this->site->getSellerById($inv->seller_id);
        $this->data['sma'] = $this->sma;
        $this->data['biller_logo'] = 2;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['value_decimals'] = $this->Settings->qty_decimals;


        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) { $taxes_details[$tax->id] = $tax->name; }
        $this->data['taxes_details'] = $taxes_details;
        $this->data['show_code'] = 1;
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['invoice_header'] = $this->site->getInvoiceHeader($inv->document_type_id, $inv->reference_no);
        $trmrate = 1;
        if (!empty($inv->sale_currency) && $inv->sale_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->sale_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('quotes'), 'page' => lang('quotes')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_quote_details'), 'bc' => $bc);
        $url_format = "quotes/view";
        $prueba = false;
        if ($inv->supplier_id) {
            $url_format = "quotes/pview";
        }
        $this->data['product_detail_font_size'] = 0;


        $this->data['view_tax'] = 1;
        $this->data['tax_inc'] = 1;
        $this->data['document_type_invoice_format'] = false;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['value_decimals'] = $this->Settings->qty_decimals;

        if ($document_type_invoice_format && $prueba == false) {
            $url_format = $document_type_invoice_format->format_url;
            $view_tax = $document_type_invoice_format->view_item_tax ? true : false;
            $tax_inc = $document_type_invoice_format->tax_inc ? true : false;
            $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
            $this->data['document_type_invoice_format'] = $document_type_invoice_format;
            $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
            $this->data['show_code'] = $document_type_invoice_format->product_show_code;
            $this->data['show_document_type_header'] = $document_type_invoice_format->show_document_type_header;
            $this->data['product_detail_font_size'] = $document_type_invoice_format->product_detail_font_size > 0 ? $document_type_invoice_format->product_detail_font_size : 0;
        }
        $this->data['biller_logo'] = 2;
        $this->data['tax_indicator'] = 0;
        $this->data['product_detail_promo'] = 1;
        $this->data['show_code'] = 1;
        $this->data['show_award_points'] = 1;
        $this->data['show_product_preferences'] = 1;
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
        $this->data['trmrate'] = $trmrate;
        $this->data['ciiu_code'] = $this->site->get_ciiu_code_by_id($this->Settings->ciiu_code);
        $this->data['signature_root'] = is_file("assets/uploads/signatures/".$this->Settings->digital_signature) ? base_url().'assets/uploads/signatures/'.$this->Settings->digital_signature : false;
        $this->data['for_email'] = false;
        $this->data['for_download'] = false;

        $currencies = $this->site->getAllCurrencies();
        $currencies_names = [];
        if ($currencies) {
            foreach ($currencies as $currency) {
                $currencies_names[$currency->code] = $currency->name;
            }
        }
        $this->data['currencies_names'] = $currencies_names;
        
        // exit(var_dump($url_format));
        if ($for_email || $download) {
            if ($for_email) {
                $this->data['for_email'] = true;
            }
            if ($download) {
                $this->data['for_download'] = true;
            }
            if (strpos($url_format, 'pos_')) {
                $html = $this->load_view($this->theme.$url_format, $this->data, true);
                $this->sma->generate_pos_inv_pdf($html, $inv->reference_no, ($download ? 'D' : 'S'));
            } else {
                $this->load_view($this->theme.$url_format, $this->data);
            }
        } else {
            $this->load_view($this->theme.$url_format, $this->data);
        }
    }

    public function pdf($quote_id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

       if ($this->input->get('id')) {
            $quote_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->quotes_model->getQuoteByID($quote_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['rows'] = $this->quotes_model->getAllQuoteItems($quote_id);
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;

        $currency = $this->site->getCurrencyByCode($inv->quote_currency);
        $trmrate = 1;
        if (!empty($inv->quote_currency) && $inv->quote_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->quote_currency_trm;
        }
        $name = $this->lang->line("quote") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
        $html = $this->load_view($this->theme . 'quotes/pdf', $this->data, true);
        if (! $this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }
        if ($view) {
            $this->load_view($this->theme . 'quotes/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    public function combine_pdf($quotes_id)
    {
        $this->sma->checkPermissions('pdf');

        foreach ($quotes_id as $quote_id) {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $inv = $this->quotes_model->getQuoteByID($quote_id);
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $this->data['rows'] = $this->quotes_model->getAllQuoteItems($quote_id);
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
            $this->data['user'] = $this->site->getUser($inv->created_by);
            $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
            $this->data['inv'] = $inv;

            $html[] = array(
                'content' => $this->load_view($this->theme . 'quotes/pdf', $this->data, true),
                'footer' => '',
            );
        }

        $name = lang("quotes") . ".pdf";
        $this->sma->generate_pdf($html, $name);

    }

    public function add()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line("no_zero_required"));
        $this->form_validation->set_rules('customer', $this->lang->line("customer"), 'required');

        if ($currency = $this->input->post('currency')) {
            if ($currency != $this->Settings->default_currency) {
                $this->form_validation->set_rules('trm', $this->lang->line("trm"), 'required');
            }
        }

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $tax_exempt_customer = $this->input->post('tax_exempt_customer') ? true : false;
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $supplier_id = $this->input->post('supplier');
            $status = $this->input->post('status');
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = $customer_details->name != '-'  ? $customer_details->name : $customer_details->company;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            if ($supplier_id) {
                $supplier_details = $this->site->getCompanyByID($supplier_id);
                $supplier = $supplier_details->company != '-' ? $supplier_details->company : $supplier_details->name;
            } else {
                $supplier = NULL;
            }
            $note = $this->sma->clear_tags($this->input->post('note'));

            $actual_currency = $this->input->post('currency');
            $actual_currency_trm = $this->input->post('trm');

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $ignore_hide_parameters = $_POST['ignore_hide_parameters'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $item_net_price = $_POST['net_price'][$r]; /**/
                $real_unit_price = $_POST['real_unit_price'][$r];
                $unit_price = $_POST['unit_price'][$r];
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';

                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : 1;
                $product_tax_rate = isset($_POST['product_tax_rate'][$r]) ? $_POST['product_tax_rate'][$r] : 1; /**/
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null; /**/
                $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                $product_tax_rate_2 = isset($_POST['product_tax_2_rate'][$r]) ? $_POST['product_tax_2_rate'][$r] : null; /**/
                $item_unit_tax_val_2 = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : null; /**/

                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];
                $item_aquantity = $_POST['product_aqty'][$r];
                $pr_item_discount = $_POST['product_discount_val'][$r]; /**/
                $product_seller_id = $_POST['product_seller_id'][$r]; /**/
                $item_preferences = isset($_POST['product_preferences_text'][$r]) ? $this->sma->preferences_selection($_POST['product_preferences_text'][$r]) : null;

                // if ($item_quantity > $item_aquantity && $this->Settings->overselling == 0 && $item_type != 'service') {
                //     $this->session->set_flashdata('error', lang("sale_cannot_be_completed_for_item_quantity"));
                //     redirect($_SERVER["HTTP_REFERER"]);
                // }
                // exit(var_dump(($item_code && $real_unit_price && $item_quantity && $item_net_price > 0)));
                // exit($item_code." - ".$real_unit_price." - ".$item_quantity." - ".$item_net_price);
                if (($item_code && $item_quantity && $item_net_price > 0) || $ignore_hide_parameters == 1) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;

                    if ($item_type == 'digital') {
                        $digital = TRUE;
                    }

                    $pr_item_tax = $this->sma->formatDecimal(($item_unit_tax_val * $item_unit_quantity));
                    $pr_item_tax_2 = $this->sma->formatDecimal(($item_unit_tax_val_2 * $item_unit_quantity));
                    $unit = $this->site->getUnitByID($item_unit);
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $product_discount += $pr_item_discount;
                    $product_tax += $pr_item_tax + $pr_item_tax_2;

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,

                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $product_tax_rate,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $product_tax_rate_2,
                        'tax_2' => $product_tax_rate_2,

                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'price_before_tax' => $item_net_price + ($pr_item_discount / $item_quantity),
                        'preferences' => $item_preferences,
                        'seller_id' => $product_seller_id,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity));
                } else {
                    $this->session->set_flashdata('error', lang('invalid_products_data'));
                    admin_redirect('quotes/add');
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);

            $current_currency = $this->currency_model->get_currency_by_code($actual_currency);
            $default_currency = $this->currency_model->get_currency_by_code($this->Settings->default_currency);

            if ($actual_currency != $this->Settings->default_currency && $quote_id == null) {
                $note.=sprintf(lang('diferent_currency_trm'),
                    $actual_currency,
                    $current_currency->name,
                    $this->sma->formatMoney($actual_currency_trm),
                    $this->Settings->default_currency,
                    $default_currency->name,
                    $this->sma->formatMoney($grand_total)
                );
            }

            if ($tax_exempt_customer) {
                $note.=lang('tax_exempt_customer_text');
            }

            $payment_status = $this->input->post('payment_status');

            $data = array('date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'supplier_id' => $supplier_id,
                'supplier' => $supplier,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'status' => $status,
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'quote_currency' => $actual_currency,
                'quote_currency_trm' =>$actual_currency_trm,
                'address_id' => $this->input->post('address_id'),
                'seller_id' => $this->input->post('seller_id'),
                'document_type_id' => $this->input->post('document_type_id'),
                'payment_status' => $payment_status,
            );

            $biller_data = $this->site->getAllCompaniesWithState('biller', $biller_id);
            if ($biller_data->min_sale_amount > 0 && $grand_total < $biller_data->min_sale_amount) {
                $this->session->set_flashdata('error', sprintf(lang('min_sale_amount_error'), lang('quote'), $this->sma->formatMoney($biller_data->min_sale_amount)));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($payment_status == 1) {
                $data['payment_method'] = $this->input->post('paid_by');
            } else {
                $data['payment_term'] = $this->input->post('payment_term');
            }

            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);
        }

        if ($this->form_validation->run() == true && $this->quotes_model->addQuote($data, $products)) {
            $this->session->set_userdata('remove_quls', 1);
            $this->session->set_flashdata('message', $this->lang->line("quote_added"));
            admin_redirect('quotes');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            //$this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) ? $this->site->getAllWarehouses(1) : null;
            $this->data['qunumber'] = ''; //$this->site->getReference('qu');
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;
            $this->data['reference'] = $this->Settings->quote_prefix."-";
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('quotes'), 'page' => lang('quotes')), array('link' => '#', 'page' => lang('add_quote')));
            $meta = array('page_title' => lang('add_quote'), 'bc' => $bc);
            $this->page_construct('quotes/add', $meta, $this->data);
        }
    }

    public function edit($id = null)
    {
        $this->sma->checkPermissions();
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->quotes_model->getQuoteByID($id);
        if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        if ($inv->status != 'pending') {
            $this->session->set_flashdata('error', 'No se puede editar');
            admin_redirect('quotes');
        }
        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line("no_zero_required"));
        $this->form_validation->set_rules('reference_no', $this->lang->line("reference_no"), 'required');

        if (!empty($inv->customer_id)) {
            $this->form_validation->set_rules('customer', $this->lang->line("customer"), 'required');
        }
        // if (!empty($inv->supplier_id)) {
        //     $this->form_validation->set_rules('suppplier_id', $this->lang->line("suppplier"), 'required');
        // }
        //$this->form_validation->set_rules('note', $this->lang->line("note"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $supplier_id = $this->input->post('supplier');
            $status = $this->input->post('status');
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = $customer_details->name != '-'  ? $customer_details->name : $customer_details->company;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            if ($supplier_id) {
                $supplier_details = $this->site->getCompanyByID($supplier_id);
                $supplier = $supplier_details->company != '-' ? $supplier_details->company : $supplier_details->name;
            } else {
                $supplier = NULL;
            }
            $note = $this->sma->clear_tags($this->input->post('note'));
            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];
                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->quotes_model->getProductByCode($item_code) : null;
                    // $unit_price = $real_unit_price;
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";
                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }
                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);
                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $item_unit,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'real_unit_price' => $real_unit_price,
                    );
                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }
            $order_discount = $this->site->calculateDiscount($this->input->post('discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);
            $data = array('date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'supplier_id' => $supplier_id,
                'supplier' => $supplier,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $shipping,
                'grand_total' => $grand_total,
                'status' => $status,
                'updated_by' => $this->session->userdata('user_id'),
                'updated_at' => date('Y-m-d H:i:s'),
                'requisition' => $this->input->post("requisition"),
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }
            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            
        }
        if ($this->form_validation->run() == true && $this->quotes_model->updateQuote($id, $data, $products)) {
            $this->session->set_userdata('remove_quls', 1);
            $this->session->set_flashdata('message', $this->lang->line("quote_added"));
            
            if (!empty($inv->customer_id)) {
                admin_redirect('quotes');
            } else if (!empty($inv->supplier_id)) {
                admin_redirect('quotes/p_index');
            }
            
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['inv'] = $this->quotes_model->getQuoteByID($id);
            $inv_items = $this->quotes_model->getAllQuoteItems($id);
            // krsort($inv_items);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                if ($this->Settings->product_order == 1) {
                    if (!isset($max_order_id)) {
                        $max_order_id = $item->id;
                    } else {
                        $max_order_id--;
                    }
                }
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                } else {
                    unset($row->details, $row->product_details, $row->cost, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price);
                }
                $row->quantity = 0;
                $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id = $item->product_id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->base_quantity = $item->quantity;
                $row->seller_id = $item->seller_id;
                $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                $row->base_unit_price = $row->price ? $row->price : $item->unit_price;
                $row->unit = $item->product_unit_id;
                $row->product_unit_id_selected = $item->product_unit_id;
                $row->qty = $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                $row->real_unit_price = $item->real_unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->option = $item->option_id;
                $options = $this->quotes_model->getProductOptions($row->id, $item->warehouse_id);
                if ($options) {
                    $option_quantity = 0;
                    foreach ($options as $option) {
                        $pis = $this->site->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                        if ($pis) {
                            foreach ($pis as $pi) {
                                $option_quantity += $pi->quantity_balance;
                            }
                        }
                        if ($option->quantity > $option_quantity) {
                            $option->quantity = $option_quantity;
                        }
                    }
                }
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->quotes_model->getProductComboItems($row->id, $item->warehouse_id);
                    foreach ($combo_items as $combo_item) {
                        $combo_item->quantity = $combo_item->qty * $item->quantity;
                    }
                }
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $ri = $this->Settings->item_addition ? $row->id : $c;

                $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'order' => $item->id);
                if (isset($max_order_id)) {
                    $pr[$ri]['order'] = $max_order_id;
                }
                $c++;
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            //$this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) ? $this->site->getAllWarehouses(1) : null;
            $this->data['qunumber'] = ''; //$this->site->getReference('qu');
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;
            $this->data['reference'] = $this->Settings->quote_prefix."-";

            // $this->sma->print_arrays($inv);
            
            $this->page_construct('quotes/edit', ['page_title' => lang('edit_quote')], $this->data);
        }
    }

    public function delete($id = null)
    {
        $this->sma->checkPermissions(NULL, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }


        $qu = $this->quotes_model->getQuoteByID($id);

        if ($qu->status == "completed") {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("access_denied")));
        }

        if ($this->quotes_model->deleteQuote($id)) {
            if ($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("quote_deleted")));
            }
            $this->session->set_flashdata('message', lang('quote_deleted'));
            admin_redirect('welcome');
        }
    }

    public function suggestions()
    {
        $term = $this->input->get('term', true);
        $warehouse_id = $this->input->get('warehouse_id', true);
        $customer_id = $this->input->get('customer_id', true);
        $biller_id = $this->input->get('biller_id', true);
        $address_id = $this->input->get('address_id', true);
        $module = $this->input->get('module', true);
        // Wappsi carne
        $this->pos_settings = $this->pos_model->getSetting();
        if ($this->pos_settings->balance_settings == 2) {
            $peso = $this->input->get('peso', true);
        }
        // Termina Wappsi carne
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $warehouse = $this->site->getWarehouseByID($warehouse_id);
        $customer = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $rows = $this->quotes_model->getProductNames($sr, $warehouse_id, $biller_id);
        $biller = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : false;
        if ($rows) {
            $r = 0;
            $pr = [];
            foreach ($rows as $row) {
                if ($module == 'pos' && $row->hide_pos == 1) {
                    continue;
                }
                if ($module == 'detal' && $row->hide_detal == 1) {
                    continue;
                }
                $label_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $unit_price_id = null);
                if ($row->tax_method == 0 && $this->Settings->ipoconsumo) {
                    $consumption_sale_tax = $row->consumption_sale_tax;
                    $label_price['new_price'] = $label_price['new_price'] + $consumption_sale_tax;
                }
                $label_price = $label_price ? "(".$this->sma->formatMoney($label_price['new_price']).")" : "";
                $c = mt_rand();
                unset($row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $option = false;
                $row->quantity = $this->sma->formatQuantity($row->quantity);
                $row->item_tax_method = $row->tax_method;
                $row->qty = 1;
                // Wappsi carne
                if ($this->pos_settings->balance_settings == 2) {
                    if($peso > 0){
                        $row->qty = $peso;
                    }
                    $row->peso = $peso;
                }
                // Termina Wappsi carne
                $row->discount = '0';
                $row->serial = '';
                $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = FALSE;
                }
                $row->option = $option_id;
                // $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                // if ($pis) {
                //  $row->quantity = 0;
                //  foreach ($pis as $pi) {
                //      $row->quantity += $pi->quantity_balance;
                //  }
                // }
                if ($this->Settings->product_preferences_management == 1) {
                    $preferences = $this->sales_model->getProductPreferences($row->id);
                } else {
                    $preferences = false;
                }
                $data_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $row->sale_unit);
                $row->price = $data_price['new_price'];
                $row->discount = $data_price['new_discount']."%";
                if (!$this->sma->isPromo($row)) {
                    $row->promotion = 0;
                }
                $row->base_quantity = 1;
                // Wappsi carne
                if ($this->pos_settings->balance_settings == 2) {
                    if($peso > 0){
                        $row->base_quantity = $peso;
                    }
                }
                // Termina Wappsi carne
                $row->profitability_margin = $row->profitability_margin;
                $row->base_unit = $row->unit;
                $row->base_unit_price = $row->price;
                $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->product_unit_id_selected = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                    foreach ($combo_items as $combo_item) {
                        $ci_unit = $this->site->getUnitByID($combo_item->unit);
                        if ($ci_unit->operation_value != 1) {
                            if ($ci_unit->operator == "*") {
                                $combo_item->qty = ($combo_item->qty * $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "/") {
                                $combo_item->qty = ($combo_item->qty / $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "+") {
                                $combo_item->qty = ($combo_item->qty + $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "-") {
                                $combo_item->qty = ($combo_item->qty - $ci_unit->operation_value);
                            }
                        }
                    }
                }
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $cnt_units_prices =  $this->site->get_all_product_units_prices($row->id) ? count($this->site->get_all_product_units_prices($row->id))+1 : 1;
                $row->cnt_units_prices = $cnt_units_prices;
                if ($row->tax_method == 1) {
                    $tax = $this->sma->calculateTax($row->tax_rate, $row->price, 0);
                    $row->real_unit_price = $row->price + $tax;
                    $row->unit_price = $row->price + $tax;
                } else {
                    $row->real_unit_price = $row->price;
                    $row->unit_price = $row->price;
                }
                if ($tax_rate) {
                    $row->price_before_tax = $row->real_unit_price / (($tax_rate->rate / 100)+1);
                } else {
                    $row->price_before_tax = $row->real_unit_price;
                }

                if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                    continue;
                }
                $pr_name = $row->name . " (" . $row->code . ")";
                if ($this->Settings->show_brand_in_product_search) {
                    $pr_name.=" - ".$row->brand_name;
                }
                $ri = $c;

                $category = $this->site->getCategoryById($row->category_id);
                $subcategory = $this->site->getCategoryById($row->subcategory_id);
                $row->except_category_taxes = false;
                if ($this->sma->validate_except_category_taxes($category, $subcategory)) {
                    $row->tax_rate = $this->Settings->category_tax_exception_tax_rate_id;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $row->except_category_taxes = true;
                }
                $hybrid_prices = false;
                if ($this->Settings->prioridad_precios_producto == 11) { //híbrido
                    $hybrid_prices = $this->site->get_all_product_hybrid_prices($row->id);
                    $hybrid_prices = $hybrid_prices[$row->id];
                }
                if (!$this->Settings->ipoconsumo) {
                    $row->consumption_sale_tax = 0;
                }

                $label_quantity = $this->Settings->product_search_show_quantity == 1 ? ' (Cant : '.$row->quantity.')': '';

                $pr[$ri] = array('id' => $ri, 'item_id' => $row->id, 'label' => ucfirst(mb_strtolower($pr_name.$label_price.$label_quantity)), 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'options' => $options, 'preferences' => $preferences, 'category' => $category, 'subcategory' => $subcategory, 'hybrid_prices' => $hybrid_prices);
                $r++;
            }
            if (count($pr) > 0) {
                $products_order = [];
                foreach ($pr as $product) {
                    $products_order[] = $product['label'];
                }
                array_multisort($products_order, SORT_ASC, $pr);
                $this->sma->send_json($pr);
            } else {
                $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
            }
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function iusuggestions()
    {
        $product_id = $this->input->get_post('product_id');
        $unit_price_id = $this->input->get_post('unit_price_id');
        $product_unit_id = $this->input->get_post('product_unit_id');
        $warehouse_id = $this->input->get_post('warehouse_id');
        $unit_quantity = $this->input->get_post('unit_quantity');

        $customer_id = $this->input->get_post('customer_id', true);
        $biller_id = $this->input->get_post('biller_id', true);
        $address_id = $this->input->get_post('address_id', true);

        $unit_price = $this->db->get_where('unit_prices', array('id' => $unit_price_id, 'id_product' => $product_id));
        $unit_price = $unit_price->row();

        $warehouse = $this->site->getWarehouseByID($warehouse_id);
        $rows = $this->quotes_model->getProductNamesIU($product_id, $warehouse_id, $unit_price_id);

        $customer = $this->site->getCompanyByID($customer_id);
        // exit(var_dump($customer_id));
        // $this->sma->print_arrays($customer);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $biller = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : false;
        $option_id = $this->input->get_post('option_id');

        if ($rows) {
            $r = 0;
            $pr = [];
            foreach ($rows as $row) {

                if ($row->hide_pos == 1) {
                    continue;
                }

                $c = mt_rand();
                unset($row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $option = false;
                // $row->quantity = 0;
                $row->item_tax_method = $row->tax_method;
                $row->qty = (($unit_price && isset($unit_price->cantidad) && $unit_price->cantidad > 0 ? $unit_price->cantidad : 1) * $unit_quantity);

                $row->discount = '0';
                $row->serial = '';
                $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = FALSE;
                }
                $row->option = $option_id;
                $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                if ($this->Settings->product_preferences_management == 1) {
                    $preferences = $this->sales_model->getProductPreferences($row->id);
                } else {
                    $preferences = false;
                }
                $data_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $row->sale_unit);
                $row->price = $data_price['new_price'];
                $row->discount = $data_price['new_discount']."%";

                if (!$this->sma->isPromo($row)) {
                    $row->promotion = 0;
                }

                $row->real_unit_price = $row->price;
                $row->base_quantity = ((isset($unit_price->cantidad) && $unit_price->cantidad > 0 ? $unit_price->cantidad : 1) * $unit_quantity);

                $row->base_unit = $row->unit;
                $row->base_unit_price = $row->price;
                $row->unit_price_id = $unit_price_id;
                $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->product_unit_id = (isset($unit_price->unit_id) ?(Int) $unit_price->unit_id : $row->unit);
                $row->product_unit_id_selected = (isset($unit_price->unit_id) ? (Int) $unit_price->unit_id : $row->unit);
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                }
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $tax_rate_2 = false;
                $category = $this->site->getCategoryById($row->category_id);
                $subcategory = $this->site->getCategoryById($row->subcategory_id);
                $row->except_category_taxes = false;
                if ($this->sma->validate_except_category_taxes($category, $subcategory)) {
                    $row->tax_rate = $this->Settings->category_tax_exception_tax_rate_id;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $row->except_category_taxes = true;
                }

                if (!$this->Settings->ipoconsumo) {
                    $row->consumption_sale_tax = 0;
                }

                $pr = array('id' => sha1($c.$r), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'tax_2_rate' => $tax_rate_2, 'units' => $units, 'preferences' => $preferences,'options' => $options, 'category' => $category, 'subcategory' => $subcategory);
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function getSupplierCost($supplier_id, $product)
    {
        switch ($supplier_id) {
            case $product->supplier1:
                $cost =  $product->supplier1price > 0 ? $product->supplier1price : $product->cost;
                break;
            case $product->supplier2:
                $cost =  $product->supplier2price > 0 ? $product->supplier2price : $product->cost;
                break;
            case $product->supplier3:
                $cost =  $product->supplier3price > 0 ? $product->supplier3price : $product->cost;
                break;
            case $product->supplier4:
                $cost =  $product->supplier4price > 0 ? $product->supplier4price : $product->cost;
                break;
            case $product->supplier5:
                $cost =  $product->supplier5price > 0 ? $product->supplier5price : $product->cost;
                break;
            default:
                $cost = $product->cost;
        }
        return $cost;
    }

    public function psuggestions()
    {
        $term = $this->input->get('term', true);
        $supplier_id = $this->input->get('supplier_id', true);
        $biller_id = $this->input->get('biller_id', true);
        $type_quote_purchase = $this->input->get('type_quote_purchase', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        if ($type_quote_purchase == 1) {
            $analyzed = $this->sma->analyze_term($term);
            $sr = $analyzed['term'];
            $option_id = $analyzed['option_id'];

            $rows = $this->purchases_model->getProductNames($sr, $biller_id, $supplier_id);
            if ($rows) {
                $r = 0;
                foreach ($rows as $row) {
                    $c = uniqid(mt_rand(), true);
                    $option = false;
                    $row->item_tax_method = $row->tax_method;
                    $options = $this->purchases_model->getProductOptions($row->id);
                    if ($options) {
                        $opt = $option_id && $r == 0 ? $this->purchases_model->getProductOptionByID($option_id) : current($options);
                        if (!$option_id || $r > 0) {
                            $option_id = $opt->id;
                        }
                    } else {
                        $opt = json_decode('{}');
                        $opt->cost = 0;
                        $option_id = FALSE;
                    }
                    $row->option = $option_id;
                    $row->supplier_part_no = '';
                    if ($row->supplier1 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier1_part_no;
                    } elseif ($row->supplier2 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier2_part_no;
                    } elseif ($row->supplier3 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier3_part_no;
                    } elseif ($row->supplier4 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier4_part_no;
                    } elseif ($row->supplier5 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier5_part_no;
                    }
                    if ($opt->cost != 0) {
                        $row->cost = $opt->cost;
                    }
                    $row->serial = '';
                    $row->cost = $supplier_id ? $this->getSupplierCost($supplier_id, $row) : $row->cost;
                    $row->real_unit_cost = $row->cost;
                    $row->prev_unit_cost = $row->cost;
                    $row->prev_price = $row->price;
                    $row->base_quantity = 1;
                    $row->base_unit = $row->unit;
                    $row->base_unit_cost = $row->cost;
                    $row->unit = $row->purchase_unit ? $row->purchase_unit : $row->unit;
                    $row->new_entry = 1;
                    $row->expiry = '';
                    $row->qty = 1;
                    $row->quantity_balance = '';
                    $row->discount = '0';
                    $row->edit_item = true;
                    unset($row->details, $row->product_details, $row->price, $row->file, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    $cnt_units_prices =  $this->site->get_all_product_units_prices($row->id) ? count($this->site->get_all_product_units_prices($row->id))+1 : 1;
                    $row->cnt_units_prices = $cnt_units_prices;
                    $units = $this->site->get_product_units($row->id);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $tax_rate_2 = $this->site->getTaxRateByID(1);

                    $pr[] = array('id' => sha1($c.$r), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                        'row' => $row, 'tax_rate' => $tax_rate, 'tax_rate_2' => $tax_rate_2, 'units' => $units, 'options' => $options);
                    $r++;
                }
                $this->sma->send_json($pr);
            } else {
                $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
            }
        } else {

            $rows = $this->purchases_model->getExpensesCategoriesNames($term);

            if ($rows) {
                foreach ($rows as $row) {
                    $c = uniqid(mt_rand(), true);
                    $option = false;
                    $options = FALSE;
                    $row->tax_method = "1";
                    $opt = json_decode('{}');
                    $opt->cost = 0;
                    $option_id = FALSE;
                    $row->option = $option_id;
                    $row->supplier_part_no = '';
                    $row->real_unit_cost = $row->cost;
                    $row->base_quantity = 1;
                    $row->base_unit = 1;
                    $row->base_unit_cost = $row->cost;
                    $row->unit = 1;
                    $row->new_entry = 1;
                    $row->expiry = '';
                    $row->qty = 1;
                    $row->type = 'manual';
                    $row->quantity_balance = '';
                    $row->discount = '0';
                    unset($row->details, $row->product_details, $row->price, $row->file, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);

                    $units = $this->site->get_product_units($row->id);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $tax_rate_2 = $this->site->getTaxRateByID($row->tax_rate_2);

                    $pr[] = array('id' => sha1($c), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                        'row' => $row, 'tax_rate' => $tax_rate, 'tax_rate_2' => $tax_rate_2, 'units' => $units, 'options' => $options);
                }
            } else {
                $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
            }
            $this->sma->send_json($pr);
        }

    }

    public function quote_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {

                        $qu = $this->quotes_model->getQuoteByID($id);

                        if ($qu->status == "completed") {
                            $this->session->set_flashdata('error', $this->lang->line("access_denied"));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }

                        $this->quotes_model->deleteQuote($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("quotes_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'combine') {

                    $html = $this->combine_pdf($_POST['val']);

                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('quotes'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('total'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $qu = $this->quotes_model->getQuoteByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($qu->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $qu->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $qu->biller);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $qu->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $qu->total);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $qu->status);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'quotations_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } elseif ($this->input->post('form_action') == 'export_excel_detail') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('quotes'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('status'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('product') .' '. lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('product') .' '. lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('net_unit_price'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('unit_price'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('quantity'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('total'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $qu = $this->quotes_model->getQuoteByID($id);
                        $quItems = $this->quotes_model->getAllQuoteItems($id);
                        foreach ($quItems as $keyqI => $valueqI) {
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($qu->date));
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $qu->reference_no);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $qu->biller);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $qu->customer);
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $qu->status);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $valueqI->product_code);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $valueqI->product_name);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $valueqI->net_unit_price);
                            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $valueqI->unit_price);
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $valueqI->quantity);
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $valueqI->subtotal);
                            $row++;
                        } 
                    }
                    for ($i='A'; $i <= 'Z'; $i++) {
                        $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                    }
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'quotations_detail_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_quote_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function update_status($id)
    {

        $this->form_validation->set_rules('status', lang("status"), 'required');

        if ($this->form_validation->run() == true) {
            $status = $this->input->post('status');
            $note = $this->sma->clear_tags($this->input->post('note'));
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'sales');
        }

        if ($this->form_validation->run() == true && $this->quotes_model->updateStatus($id, $status, $note)) {
            $this->session->set_flashdata('message', lang('status_updated'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'sales');
        } else {

            $this->data['inv'] = $this->quotes_model->getQuoteByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'quotes/update_status', $this->data);

        }
    }


    public function addqpurchase()
    {
        $this->sma->checkPermissions('add', NULL, 'quotes');

        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line("no_zero_required"));
        $this->form_validation->set_rules('supplier', $this->lang->line("supplier"), 'required');

        if ($currency = $this->input->post('currency')) {
            if ($currency != $this->Settings->default_currency) {
                $this->form_validation->set_rules('trm', $this->lang->line("trm"), 'required');
            }
        }

        if ($this->form_validation->run() == true) {

            $reference = $this->site->getReference('qup');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $supplier_id = $this->input->post('supplier');
            $status = $this->input->post('status');
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;

            $customer_id = 0;
            $customer = '';

            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            if ($supplier_id) {
                $supplier_details = $this->site->getCompanyByID($supplier_id);
                $supplier = $supplier_details->name != '-' ? $supplier_details->name : $supplier_details->company;
            } else {
                $supplier = NULL;
            }
            $note = $this->sma->clear_tags($this->input->post('note'));

            $actual_currency = $this->input->post('currency');
            $actual_currency_trm = $this->input->post('trm');

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;

            //Retenciones
            if ($this->input->post('rete_applied')) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_fuente_id = $this->input->post('rete_fuente_option');
                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_iva_id = $this->input->post('rete_iva_option');
                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_ica_id = $this->input->post('rete_ica_option');
                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_other_id = $this->input->post('rete_otros_option');
                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;
                $rete_applied = TRUE;
            } else {
                $rete_applied = FALSE;
                $total_retenciones = 0;
            }
            //Retenciones


            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : null;
                $real_unit_cost = $this->sma->formatDecimal($_POST['real_unit_cost'][$r]);
                $unit_cost = $this->sma->formatDecimal($_POST['unit_cost'][$r]);
                $item_net_cost = $this->sma->formatDecimal($_POST['net_cost'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_tax_val = isset($_POST['product_tax_val'][$r]) ? $_POST['product_tax_val'][$r] : null;
                $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                $item_tax_val_2 = isset($_POST['product_tax_val_2'][$r]) ? $_POST['product_tax_val_2'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];
                $item_tax_method = isset($_POST['product_tax_method'][$r]) ? $_POST['product_tax_method'][$r] : null;
                $serialModal_serial = isset($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : null;
                if (isset($item_code) && isset($real_unit_cost) && isset($unit_cost) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->quotes_model->getProductByCode($item_code) : null;
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_cost);
                    $unit_cost = $this->sma->formatDecimal($unit_cost - $pr_discount);
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $pr_item_tax_2 = $item_tax = $item_tax_2 = 0;
                    $tax = "";
                    $tax_2 = "";
                    $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                    $tax_details_2 = $this->site->getTaxRateByID($item_tax_rate_2);
                    $pr_item_tax = $this->sma->formatDecimal(($item_tax_val * $item_unit_quantity), 4);
                    $pr_item_tax_2 += $this->sma->formatDecimal(($item_tax_val_2 * $item_unit_quantity), 4);
                    $product_tax += $pr_item_tax + $pr_item_tax_2;
                    $subtotal = (($item_net_cost * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $unit = $this->site->getUnitByID($item_unit);
                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_cost,
                        'unit_price' => $this->sma->formatDecimal($item_net_cost + $item_tax_val + $item_tax_val_2),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $item_unit,
                        'product_unit_code' => $unit->code,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax_details ? $tax_details->rate : $item_tax_rate,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $item_tax_rate_2,
                        'tax_2' => $tax_details_2 ? $tax_details_2->rate: $item_tax_rate_2,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'real_unit_price' => $real_unit_cost,
                        'serial_no' => $serialModal_serial,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('discount'), ($total));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);

            $current_currency = $this->currency_model->get_currency_by_code($actual_currency);
            $default_currency = $this->currency_model->get_currency_by_code($this->Settings->default_currency);

            if ($actual_currency != $this->Settings->default_currency && $quote_id == null) {
                $note.=sprintf(lang('diferent_currency_trm'),
                    $actual_currency,
                    $current_currency->name,
                    $this->sma->formatMoney($actual_currency_trm),
                    $this->Settings->default_currency,
                    $default_currency->name,
                    $this->sma->formatMoney($grand_total)
                );
            }


            $payment_status = $this->input->post('payment_status');

            $data = array('date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'supplier_id' => $supplier_id,
                'supplier' => $supplier,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'status' => $status,
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'quote_currency' => $actual_currency,
                'quote_currency_trm' =>$actual_currency_trm,
                'quote_type' => $this->input->post('type_quote_purchase'),
                'payment_status' =>$payment_status,
                'document_type_id' => $this->input->post('document_type_id'),
                'requisition' => $this->input->post("requisition"),
            );

            if ($payment_status == 1) {
                $data['payment_method'] = $this->input->post('paid_by');
            } else {
                $data['payment_term'] = $this->input->post('payment_term');
            }

            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($rete_applied) {
                 $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                 $data['rete_fuente_total'] = $rete_fuente_total;
                 $data['rete_fuente_account'] = $rete_fuente_account;
                 $data['rete_fuente_base'] = $rete_fuente_base;
                 $data['rete_fuente_id'] = $rete_fuente_id;
                 $data['rete_iva_percentage'] = $rete_iva_percentage;
                 $data['rete_iva_total'] = $rete_iva_total;
                 $data['rete_iva_account'] = $rete_iva_account;
                 $data['rete_iva_base'] = $rete_iva_base;
                 $data['rete_iva_id'] = $rete_iva_id;
                 $data['rete_ica_percentage'] = $rete_ica_percentage;
                 $data['rete_ica_total'] = $rete_ica_total;
                 $data['rete_ica_account'] = $rete_ica_account;
                 $data['rete_ica_base'] = $rete_ica_base;
                 $data['rete_ica_id'] = $rete_ica_id;
                 $data['rete_other_percentage'] = $rete_other_percentage;
                 $data['rete_other_total'] = $rete_other_total;
                 $data['rete_other_account'] = $rete_other_account;
                 $data['rete_other_base'] = $rete_other_base;
                 $data['rete_other_id'] = $rete_other_id;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);
        }

        if ($this->form_validation->run() == true && $this->quotes_model->addQuote($data, $products)) {
            $this->session->set_userdata('remove_quls', 1);
            $this->session->set_flashdata('message', $this->lang->line("quote_added"));
            admin_redirect('quotes/p_index');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['billers'] = ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')) ? $this->site->getAllCompanies('biller') : null;
            //$this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) ? $this->site->getAllWarehouses(1) : null;
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('quotes'), 'page' => lang('quotes')), array('link' => '#', 'page' => lang('add_quote_purchase')));
            $meta = array('page_title' => lang('add_quote_purchase'), 'bc' => $bc);
            $this->page_construct('quotes_purchases/add', $meta, $this->data);
        }
    }

    public function addqexpense()
    {
        $this->sma->checkPermissions();
        $this->data['billers'] = ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')) ? $this->site->getAllCompanies('biller') : null;
        //$this->data['currencies'] = $this->site->getAllCurrencies();
        $this->data['tax_rates'] = $this->site->getAllTaxRates();
        $this->data['warehouses'] = ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) ? $this->site->getAllWarehouses(1) : null;
        $this->data['qunumber'] = ''; //$this->site->getReference('qu');
        $this->data['currencies'] = $this->site->getAllCurrencies();
        $this->data['reference'] = $this->Settings->quote_purchase_prefix."-";
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('quotes'), 'page' => lang('quotes')), array('link' => '#', 'page' => lang('add_quote_expense')));
        $meta = array('page_title' => lang('add_quote_expense'), 'bc' => $bc);
        $this->page_construct('quotes_purchases/add_expense', $meta, $this->data);

    }

    public function create_from_quote($quote_id = null, $action = null)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $quote_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->quotes_model->getQuoteByID($quote_id);

        $this->data['rows'] = $this->quotes_model->getAllQuoteItems($quote_id);
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;

        if ($action == 'to_purchase' && $this->data['supplier']) {
            admin_redirect('admin/sales/add/'.$quote_id);
        } else if ($action == 'to_sale' && $this->data['customer']) {
            admin_redirect('admin/sales/add/'.$quote_id);
        } else {
            $this->session->set_flashdata('error', sprintf(lang('cant_create_to'), lang($action)));
            admin_redirect('quotes');
        }

    }

    public function get_item_price($row, $customer, $customer_group, $biller){

        if ($this->Settings->prioridad_precios_producto == 1 || $this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7) {

            if ($this->sma->isPromo($row)) {
                $row->price = $row->promo_price;
            } else if ($product_price_base = $this->site->getPriceGroupBase($row->id)) {
                $row->price = $product_price_base->price;
                if ($this->Settings->prioridad_precios_producto == 5) {
                    $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                }
            } else {
                $row->price = 0;
            }

        } else if ($this->Settings->prioridad_precios_producto == 2) {

            if ($biller && $biller->default_price_group) {
                if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $biller->default_price_group)) {
                    $row->price = $pr_group_price->price;
                } else {
                    $row->price = 0;
                }
                $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
            } else {
                $row->price = 0;
            }

        } else if ($this->Settings->prioridad_precios_producto == 3) {

            if ($customer && $customer->price_group_id) {
                if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $customer->price_group_id)) {
                    $row->price = $pr_group_price->price;
                } else {
                    $row->price = 0;
                }
                $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
            } else {
                $row->price = 0;
            }

        } else if ($this->Settings->prioridad_precios_producto == 4 || $this->Settings->prioridad_precios_producto == 6) {

            if ($this->sma->isPromo($row)) {

                $row->price = $row->promo_price;

            } else {

                if ($this->Settings->prioridad_precios_producto == 4) { //sucursal | cliente

                    if ($biller && $biller->default_price_group) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $biller->default_price_group)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    } elseif ($customer && $customer->price_group_id) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $customer->price_group_id)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    }

                } else if ($this->Settings->prioridad_precios_producto == 6) { // cliente | sucursal

                    if ($customer && $customer->price_group_id) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $customer->price_group_id)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    } elseif ($biller && $biller->default_price_group) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $biller->default_price_group)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    }

                }

            }
            $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
        }

        return $row->price;

    }

    public function email($quote_id = null)
    {
        $this->sma->checkPermissions(false, true);
        if ($this->input->get('id')) {
            $quote_id = $this->input->get('id');
        }
        $inv = $this->quotes_model->getQuoteByID($quote_id);
        $quote_type = $inv->supplier_id ? 2 : 1;
        $this->form_validation->set_rules('to', $this->lang->line("to") . " " . $this->lang->line("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', $this->lang->line("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', $this->lang->line("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', $this->lang->line("bcc"), 'trim|valid_emails');
        $this->form_validation->set_rules('note', $this->lang->line("message"), 'trim');
        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $customer = $this->site->getCompanyByID($inv->supplier_id ? $inv->supplier_id : $inv->customer_id);
            $this->send_quote_email($quote_type, $quote_id, $to, $subject);

        } elseif ($this->input->post('send_email')) {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', ($quote_type == 1 ? lang('quote') : lang('quote_purchase')).' (' . $inv->reference_no . ')'),
            );
            $this->data['customer'] = $this->site->getCompanyByID($inv->supplier_id ? $inv->supplier_id : $inv->customer_id);
            $this->data['id'] = $quote_id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'quotes/email', $this->data);

        }
    }

    public function send_quote_email($qtype, $id, $to, $subject){

        $this->view($id, true);
        $inv = $this->quotes_model->getQuoteByID($id);
        $attachment =  FCPATH.'files/'.$inv->reference_no.'.pdf';
        $this->load->library('parser');
        $customer = $this->site->getCompanyByID($inv->supplier_id ? $inv->supplier_id : $inv->customer_id);
        $biller = $this->site->getCompanyByID($inv->biller_id);

        if ($qtype == 1) {
            $template = file_get_contents('./themes/default/admin/views/email_templates/quote.html');
        } else {
            $template = file_get_contents('./themes/default/admin/views/email_templates/quote_purchase.html');
        }

        $parse_data = array(
            'reference_number' => $inv->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->company,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $biller->logo . '" alt="' . ($biller->company != '-' ? $biller->company : $biller->name) . '"/>',
        );
        $email_content = $this->parser->parse_string($template, $parse_data);
        $parse_data = array(
            'email_title' => $subject,
            'email_content' => $email_content,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
        );
        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/email_template_background.php');
        $message = $this->parser->parse_string($msg, $parse_data);

        $redirect = $qtype == 1 ? 'quotes/index' : 'quotes/p_index';

        try {
            if ($this->sma->send_email($to, $subject, $message, null, null, $attachment)) {
                unlink($attachment);
                $this->session->set_flashdata('message', lang("email_sent"));
                admin_redirect($redirect);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            admin_redirect($redirect);
        }

    }

}
