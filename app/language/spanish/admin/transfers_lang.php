<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */

$lang['add_transfer']                                   = "Agregar traslado";
$lang['edit_transfer']                                  = "Editar traslado";
$lang['delete_transfer']                                = "Eliminar traslado";
$lang['delete_transfers']                               = "Eliminar traslados";
$lang['transfer_added']                                 = "El traslado ha sido agregado con éxito";
$lang['transfer_updated']                               = "El traslado se ha actualizado correctamente";
$lang['transfer_deleted']                               = "El traslado se ha eliminado correctamente";
$lang['transfers_deleted']                              = "Los traslados se han eliminado correctamente";
$lang['import_by_csv']                                  = "Agregar traslados en archivo CSV";
$lang['ref_no']                                         = "Número";
$lang['transfer_details']                               = "Detalles del traslado";
$lang['email_transfer']                                 = "Correo electrónico del traslado";
$lang['transfer_quantity']                              = "Cantidad del traslado";
$lang['please_select_warehouse']                        = "Por favor, seleccione una bodega";
$lang['please_select_different_warehouse_origin']              = "Por favor, seleccione una bodega diferente para origen";
$lang['please_select_different_warehouse_destination']              = "Por favor, seleccione una bodega diferente para destino";
$lang['to_warehouse']                                   = "Para la bodega";
$lang['from_warehouse']                                 = "Desde la bodega ";
$lang['edit_transfer_quantity']                         = "Editar cantidad de traslado";
$lang['can_not_change_status_of_completed_transfer']    = "No se puede cambiar el estado de traslados completados";
$lang['transfer_by_csv']                                = "Agregar Traslado en CSV";
$lang['no_transfer_selected']                           = "No se ha seleccionado traslado. Por favor seleccione al menos una traslado.";
$lang['stamp_sign']                                     = "Firma";
$lang['received_by']                                    = "Recibido por";
$lang['users']                                          = "Usuarios";

$lang['transferring']                                   = "Transfiriendo";
$lang['first_2_are_required_other_optional']            = "<strong>Las primeras 2 columnas son obligatorias las demas opcionales.</strong>";
$lang['pr_not_found']                                   = "Producto no encontrado ";
$lang['line_no']                                        = "Número de línea";
$lang['transfer']										= "Remisión";
$lang['stamp_sign_delivered']							= "Firma entregado";
$lang['stamp_sign_received']							= "Firma recibido";

$lang['observation']									= "Observación";