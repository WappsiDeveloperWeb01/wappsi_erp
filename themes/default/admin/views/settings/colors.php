<?php defined('BASEPATH') or exit('No direct script access allowed');  ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="colorsTable" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkth" type="checkbox" name="check" />
                                            </th>
                                            <th><?= $this->lang->line("name"); ?></th>
                                            <th><?= $this->lang->line("status") ?></th>
                                            <th><?= lang("synchronized"); ?></th>
                                            <th><?= $this->lang->line("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="4" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('[data-toggle-second="tooltip"]').tooltip();

        oTable = $('#colorsTable').dataTable({
            aaSorting: [
                [1, "asc"]
            ],
            aLengthMenu: [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('system_settings/getColors') ?>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    name: "<?= $this->security->get_csrf_token_name() ?>",
                    value: "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: sSource,
                    data: aoData,
                    success: fnCallback
                });
            },
            aoColumns: [{
                    bSortable: false,
                    mRender: checkbox
                },
                null,
                {
                    mRender: function (data) {
                        if (data == 1) {
                            return '<li class="fas fa-check-circle fa-2x text-success"></li>';
                        } else {
                            return '<li class="fas fa-ban fa-2x text-danger"></li>';
                        }
                    },
                    className: 'text-center',
                    sWidth: '65px'
                },
                {
                    sWidth: '65px',
                    className: 'text-center',
                    mRender: function(data, type, row) {
                        if (data == 1) {
                            return '<i class="fas fa-check-circle fa-2x text-success"></i>';
                        } else {
                            return '<i class="fas fa-ban fa-2x text-danger"></i>';
                        }
                    }
                },
                {
                    bSortable: false,
                    sWidth: '65px',
                    className: 'text-center'
                }
            ],
            fnDrawCallback: function (oSettings) {
                <?php if ($this->Admin || $this->Owner || $this->GP['system_settings-addColor']): ?>
                    $('.actionsButtonContainer').html('<a href="<?= admin_url('system_settings/addColor') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>'+
                        '<div class="pull-right dropdown">'+
                        '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                        '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                            '<li>'+
                                '<a id="syncStore">'+
                                    '<i class="fas fa-sync"></i> <?= lang('sync_store') ?>'+
                                '</a>'+
                            '</li>'+
                            '<li>'+
                                '<a href="<?= admin_url('system_settings/getColors/xls') ?>">'+
                                    '<i class="fa fa-file-excel-o"></i> <?= lang('download_excel') ?>'+
                                '</a>'+
                            '</li>'+
                        '</ul>'+
                    '</div>');
                <?php endif ?>

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });

        $(document).on('click', '#syncStore', syncStore);
    });

    function syncStore()
    {
        let ids = validateSelectedIds();

        if (ids != false) {
            swal({
                title: '¡Sincronización en curso',
                text: "Por favor espere un momento mientras se realiza la sincronización",
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            $.ajax({
                type: "post",
                url: site.base_url + 'system_settings/syncColorsStore',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'ids':  ids,
                },
                dataType: "json",
                success: function (response) {
                    let alert = '';

                    if (response.length !== 0) {
                        $.each(response, function (type, message) {
                            if (type == 'warning') {
                                header_alert(type, message, null, function () { swal.close(); window.location.reload(); });
                            }
                        });
                    } else {
                        header_alert('success', '¡Sincronización completada!', null, function () {  swal.close(); window.location.reload(); });
                    }
                }
            });
        }
    }

    function validateSelectedIds() {
        let ids = [];
        $('input[name="val[]"]').each(function() {
            if ($(this).is(":checked")) {
                ids.push($(this).val());
            }
        });

        if (ids.length === 0) {
            swal("Advertencia!", "No ha seleccionado ningun Color para sincronizar.", "warning");
            return false;
        }

        return ids;
    }
</script>