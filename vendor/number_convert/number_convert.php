<?php
/**
 * Clase que implementa un convertidor de números
 * a letras.
 *
 * Soporte para PHP >= 5.4
 * Para soportar PHP 5.3, declare los arreglos
 * con la función array.
 *
 * @author AxiaCore S.A.S
 *
 */
class number_convert
{
    private static $UNIDADES_ES = [
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
    ];
    private static $DECENAS_ES = [
        'VENTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
    ];
    private static $CENTENAS_ES = [
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
    ];
    
    private static $UNIDADES_EN = [
        '',
        'ONE ',
        'TWO ',
        'THREE ',
        'FOUR ',
        'FIVE ',
        'SIX ',
        'SEVEN ',
        'EIGHT ',
        'NINE ',
        'TEN ',
        'ELEVEN ',
        'TWELVE ',
        'THIRTEEN ',
        'FOURTEEN ',
        'FIFTEEN ',
        'SIXTEEN ',
        'SEVENTEEN ',
        'EIGHTEEN ',
        'NINETEEN ',
        'TWENTY '
    ];
    private static $DECENAS_EN = [
        'TWENTY',
        'THIRTY ',
        'FORTY ',
        'FIFTY ',
        'SIXTY ',
        'SEVENTY ',
        'EIGHTY ',
        'NINETY ',
        'HUNDRED '
    ];
    private static $CENTENAS_EN = [
        'ONE HUNDRED ',
        'TWO HUNDRED ',
        'THREE HUNDRED ',
        'FOUR HUNDRED ',
        'FIVE HUNDRED ',
        'SIX HUNDRED ',
        'SEVEN HUNDRED ',
        'EIGHT HUNDRED ',
        'NINE HUNDRED '
    ];

    public static function convertir($number, $moneda = '', $centimos = 'centavos', $forzarCentimos = false, $idioma = 'ES')
    {
        $number = self::formatDecimal($number);
        // exit(var_dump($number));
        $converted = '';
        $decimales = '';
        $centimos = $centimos != NULL ? $centimos : ($idioma == 'ES' ? 'centavos' : 'cents');
        if (($number < 0) || ($number > 999999999)) {
            return $idioma == 'ES' ? 'No es posible convertir el número a letras' : 'Cannot convert the number to words';
        }
        $div_decimales = explode('.', $number);
        if (count($div_decimales) > 1) {
            $number = $div_decimales[0];
            $decNumberStr = (string) $div_decimales[1];
            if (strlen($decNumberStr) == 1) {
                $decNumberStr = $decNumberStr * 10;
            }
            if (strlen($decNumberStr) > 0) {
                $decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
                $decCientos = substr($decNumberStrFill, 6);
                $decimales = self::convertGroup($decCientos, $idioma);
            }
        } else if (count($div_decimales) == 1 && $forzarCentimos) {
            $decimales = $idioma == 'ES' ? 'CERO ' : 'ZERO ';
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= $idioma == 'ES' ? 'UN MILLON ' : 'ONE MILLION ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%s%s ', self::convertGroup($millones, $idioma), $idioma == 'ES' ? 'MILLONES' : 'MILLIONS');
            }
        }
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= $idioma == 'ES' ? 'MIL ' : 'ONE THOUSAND ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%s%s ', self::convertGroup($miles, $idioma), $idioma == 'ES' ? 'MIL' : 'THOUSAND');
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= $idioma == 'ES' ? 'UN ' : 'ONE ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', self::convertGroup($cientos, $idioma));
            }
        }
        if (empty($decimales)) {
            $valor_convertido = $converted . strtoupper($moneda);
        } else {
            $valor_convertido = $converted . strtoupper($moneda) . ($idioma == 'ES' ? ' CON ' : ' AND ') . $decimales . '' . strtoupper($centimos);
        }
        return $valor_convertido;
    }

    private static function convertGroup($n, $idioma)
    {
        $output = '';
        if ($idioma == 'ES') {
            $UNIDADES = self::$UNIDADES_ES;
            $DECENAS = self::$DECENAS_ES;
            $CENTENAS = self::$CENTENAS_ES;
        } else {
            $UNIDADES = self::$UNIDADES_EN;
            $DECENAS = self::$DECENAS_EN;
            $CENTENAS = self::$CENTENAS_EN;
        }

        if ($n == '100') {
            $output = $idioma == 'ES' ? 'CIEN ' : 'ONE HUNDRED ';
        } else if ($n[0] !== '0') {
            $output = $CENTENAS[$n[0] - 1];
        }
        $k = intval(substr($n, 1));
        if ($k <= 20) {
            $output .= $UNIDADES[$k];
        } else {
            if (($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%s'.($idioma == 'ES' ? ' Y ' : '').'%s', $DECENAS[intval($n[1]) - 2], $UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', $DECENAS[intval($n[1]) - 2], $UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }

    public static function formatDecimal($number) //para cálculos
    {
        $decimals = 2;
        if (!is_numeric($number)) {
            return null;
        }
        return number_format($number, $decimals, '.', '');
    }
}
?>
