<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="NTTable" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo $this->lang->line("notification"); ?></th>
                                            <th style="width: 140px;"><?php echo $this->lang->line("submitted_at"); ?></th>
                                            <th style="width: 140px;"><?php echo $this->lang->line("from"); ?></th>
                                            <th style="width: 140px;"><?php echo $this->lang->line("till"); ?></th>
                                            <th><?php echo $this->lang->line("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        oTable = $('#NTTable').dataTable({
            aaSorting: [
                [1, "asc"],
                [2, "asc"]
            ],
            aLengthMenu: [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('notifications/getNotifications') ?>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            aoColumns: [null, {
                "mRender": fld
            }, {
                "mRender": fld
            }, {
                "mRender": fld
            }, {
                bSortable: false,
                className: 'text-center',
                sWidth: "65px"
            }],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html(`<a href="<?= admin_url('notifications/add') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>`);

                $('[data-toggle-second="tooltip"]').tooltip();
            }
        });
    });
</script>