<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h2 id="myModalLabel"><?= ucwords(mb_strtolower($product->name)); ?></h2>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <div class="modal-body">
            <div class="row" id="print_this">
                <!-- <div class="col-xs-12" class="a">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-2x">&times;</i>
                    </button>
                    <h2 id="myModalLabel"><?= ucwords(mb_strtolower($product->name)); ?></h2>
                </div> -->
                <div class="row">
                    <div class="col-xs-5">
                        <img id="pr-image" src="<?= $this->sma->get_img_url($product->image) ?>" alt="<?= $product->name ?>" class="img-responsive img-thumbnail" />
                        <div id="multiimages" class="padding10">
                            <?php if (!empty($images)) { ?>
                                <a class="img-thumbnail change_img" href="<?= $this->sma->get_img_url($product->image) ?>" style="margin-right:5px;">
                                    <img class="img-responsive" src="<?= $this->sma->get_img_url($product->image) ?>" alt="<?= $product->image ?>" style="width:50px; height:50px;"/>
                                </a>
                                <?php foreach ($images as $ph) { ?>
                                    <div class="gallery-image">
                                        <a class="img-thumbnail change_img" href="<?= $this->sma->get_img_url($ph->photo) ?>" style="margin-right:5px;">
                                            <img class="img-responsive" src="<?= $this->sma->get_img_url($ph->photo) ?>" alt="<?= $ph->photo ?>" style="width:50px; height:50px;" />
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-xs-7">
                        <div class="table-responsive">
                            <table class="table table-borderless  dfTable table-right-left">
                                <tbody>
                                    <tr>
                                        <td><?= lang("type"); ?></td>
                                        <td><?= lang($product->type); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("name"); ?></td>
                                        <td><?= $product->name; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("product_list_code"); ?></td>
                                        <td><?= $product->code; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("reference"); ?></td>
                                        <td><?= $product->reference; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("brand"); ?></td>
                                        <td><?= $brand ? $brand->name : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("category"); ?></td>
                                        <td><?= $category->name; ?></td>
                                    </tr>
                                    <?php if ($product->subcategory_id) { ?>
                                        <tr>
                                            <td><?= lang("subcategory"); ?></td>
                                            <td><?= $subcategory->name; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($product->second_level_subcategory_id) { ?>
                                        <tr>
                                            <td><?= lang("second_level_subcategory_id"); ?></td>
                                            <td><?= $second_level_subcategory->name; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td><?= lang("unit"); ?></td>
                                        <td><?= $unit ? $unit->name . ' (' . $unit->code . ')' : ''; ?></td>
                                    </tr>
                                    <?php 
                                        $cost_tax = $this->sma->calculate_tax($product->tax_rate, $product->cost, $product->tax_method);
                                        $visual_cost = $product->tax_method == 0 ? ($product->cost - $cost_tax) : ($product->cost + $cost_tax);
                                        $visual_cost = $this->sma->formatMoney($this->sma->formatDecimals($visual_cost));
                                        
                                        $avg_cost_tax = $this->sma->calculate_tax($product->tax_rate, $product->avg_cost, 0);
                                        $visual_avg_cost = $product->tax_method == 0 ? ($product->avg_cost - $avg_cost_tax) : ($product->avg_cost);
                                        $avg_net_cost = ($product->avg_cost - $avg_cost_tax);
                                        $visual_avg_cost = $this->sma->formatMoney($this->sma->formatDecimals(($visual_avg_cost)));
                                        
                                        
                                        $price_tax = $this->sma->calculate_tax($product->tax_rate, $product->price, $product->tax_method);
                                        $visual_price = $product->tax_method == 0 ? ($product->price - $price_tax) : ($product->price + $price_tax);
                                        $visual_price = $this->sma->formatMoney($this->sma->formatDecimals($visual_price));
                                     ?>
                                    <?php
                                            
                                        if ($this->Admin || $this->Owner || $this->GP['products-cost']) {
                                            echo '<tr>
                                                    <td>' . lang("cost") . '</td>
                                                    <td>' . $this->sma->formatMoney($product->cost + $product->consumption_purchase_tax) .($product->tax_method == 0 ? '<i class="fa fa-exclamation-circle tip" data-toggle="tooltip" data-placement="top" title="'.lang('value_include_taxes').'"></i>' : '') . ' ('.$visual_cost.') </td>
                                                </tr>';
                                            echo '<tr>
                                                    <td>' . lang("avg_cost") . '</td>
                                                    <td>' . $this->sma->formatMoney(($product->tax_method == 0 ? $product->avg_cost : $avg_net_cost)) . ($product->tax_method == 0 ? '<i class="fa fa-exclamation-circle tip" data-toggle="tooltip" data-placement="top" title="'.lang('value_include_taxes').'"></i>' : '').' ('.$visual_avg_cost.') </td>
                                                </tr>';
                                        }
                                        if ($this->Admin || $this->Owner || $this->GP['products-price']) {
                                            echo '<tr>
                                                    <td>' . lang("price") . '</td>
                                                    <td>' . $this->sma->formatMoney($product->price + $product->consumption_sale_tax) . ($this->Settings->ipoconsumo && $product->tax_method == 0 && $product->consumption_sale_tax > 0 ? '<i class="fa fa-exclamation-circle tip" data-toggle="tooltip" data-placement="top" title="'.lang('value_include_taxes').'"></i>' : '').'</td>
                                                </tr>';
                                            if ($product->promotion) {
                                                echo '<tr><td>' . lang("promotion") . '</td><td>' . $this->sma->formatMoney($product->promo_price + ($this->Settings->ipoconsumo ? $product->consumption_sale_tax : 0)) . ' ('.$this->sma->hrsd($product->start_date).' - '.$this->sma->hrsd($product->end_date).')</td></tr>';
                                            }
                                        }
                                    ?>

                                    <?php if ($this->Settings->ipoconsumo) : ?>
                                        <tr>
                                            <td><?= lang("second_product_tax").lang('purchases'); ?></td>
                                            <td><?= ($tax_rate_2 ? 
                                                        $tax_rate_2->description." ".($product->purchase_tax_rate_2_percentage ? $product->purchase_tax_rate_2_percentage : "") 
                                                        : "")
                                                        ; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("second_product_tax").lang('sales'); ?></td>
                                            <td><?= ($tax_rate_2 ? 
                                                        $tax_rate_2->description." ".($product->sale_tax_rate_2_percentage ? $product->sale_tax_rate_2_percentage : "") 
                                                        : "").
                                                ($product->sale_tax_rate_2_milliliters ? "</br> Gr/Ml ".$this->sma->formatQuantity($product->sale_tax_rate_2_milliliters)." " : "").
                                                ($product->sale_tax_rate_2_degrees ? "</br> Grados ".$this->sma->formatQuantity($product->sale_tax_rate_2_degrees)." " : "").
                                                ($product->sale_tax_rate_2_nominal ? "</br> Tarifa ".$this->sma->formatMoney($product->sale_tax_rate_2_nominal)." " : "")
                                                        ; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("second_product_tax") . " " . lang('sales'); ?></td>
                                            <td><?= $product->consumption_sale_tax; ?></td>
                                        </tr>

                                        <tr>
                                            <td><?= lang("second_product_tax") . " " . lang('purchases'); ?></td>
                                            <td><?= $product->consumption_purchase_tax; ?></td>
                                        </tr>
                                    <?php endif ?>

                                    <?php if ($product->tax_rate) { ?>
                                        <tr>
                                            <td><?= lang("tax_rate"); ?></td>
                                            <td><?= $tax_rate->name; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("tax_method"); ?></td>
                                            <td><?= $product->tax_method == 0 ? lang('inclusive') : lang('exclusive'); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($product->alert_quantity != 0) { ?>
                                        <tr>
                                            <td><?= lang("alert_quantity"); ?></td>
                                            <td><?= $this->sma->formatQuantity($product->alert_quantity); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($preferences) {
                                        $cat_name = NULL;
                                    ?>
                                        <tr>
                                            <td><?= lang("product_preferences"); ?></td>
                                            <td><?php foreach ($preferences as $preference) {
                                                    if ($cat_name == NULL || $cat_name != $preference->prf_cat_name) {
                                                        echo '<p>' . $preference->prf_cat_name . '</p> ';
                                                    }
                                                    echo '<span class="label label-success">' . $preference->name . '</span> ';
                                                    $cat_name = $preference->prf_cat_name;
                                                } ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($this->Admin || $this->Owner || $this->GP['products-cost']) : 
                                        $profitability_margin = $this->sma->formatDecimals((($product->price - $product->cost) / ($product->price > 0 ? $product->price : 1))*100) . '%';   
                                    ?>
                                        <?php if($profitability_margin >= 0): ?>
                                            <tr>
                                                <td><?= lang("profitability_margin"); ?></td>
                                                <td><?= $profitability_margin ?> </td>
                                            </tr>
                                        <?php endif; ?>    
                                    <?php endif ?>
                                    <?php if ($custom_fields) : ?>
                                        <?php foreach ($custom_fields as $cf_arr) : ?>
                                            <?php $cf = $cf_arr['data']; ?>
                                            <?php if ($product->{$cf->cf_code}) : ?>
                                                <tr>
                                                    <td><?= $cf->cf_name ?></td>
                                                    <td><?= $product->{$cf->cf_code} ?></td>
                                                </tr>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- warehouses quantitys -->
                    <div class="col-xs-5">
                        <?php if ((!$Supplier || !$Customer) && !empty($warehouses) && ($product->type == 'standard' || $product->type == 'raw' || $product->type == 'subproduct' || $product->type == 'pfinished')) { ?>
                            <h3 class="bold"><?= lang('warehouse_quantity') ?></h3>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed dfTable two-columns">
                                    <thead>
                                        <tr>
                                            <th><?= lang('warehouse_name') ?></th>
                                            <th><?= lang('quantity') . ' <br> (' . lang('product_location') . ')'; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($warehouses as $warehouse) {

                                            if (!$this->Owner && !$this->Admin && $this->Settings->users_can_view_warehouse_quantitys == 0 && $this->session->userdata('warehouse_id')) {
                                                if ($this->session->userdata('warehouse_id') != $warehouse->id) {
                                                    continue;
                                                }
                                            }
                                            $decimales = 3;

                                            echo '<tr>
                                                    <td>
                                                        ' . $warehouse->name . ' (' . $warehouse->code . ')
                                                    </td>
                                                    <td class="text-center">
                                                        <strong>' . $this->sma->formatQuantity($warehouse->quantity) . '</strong> <br>' . ($warehouse->warehouse_location ? ' (' . $warehouse->warehouse_location . ')' : '') . '
                                                    </td>
                                                </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                    <!-- combo and options -->
                    <div class="col-xs-7">
                        <?php if ($product->type == 'combo' || $product->type == 'subproduct' || $product->type == 'pfinished') { ?>
                            <?php
                            $decimales = null;
                            if ($product->type != 'combo') {
                                $decimales = 3;
                            }
                            ?>
                            <h3 class="bold"><?= lang('combo_items') ?></h3>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed dfTable three-columns">
                                    <thead>
                                        <tr>
                                            <th><?= lang('product_name') ?></th>
                                            <th><?= lang('quantity') ?></th>
                                            <th><?= lang('cost') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $total_combo_cost = 0;
                                        foreach ($combo_items as $combo_item) {
                                            echo '<tr>
                                                    <td>' . $combo_item->name . ' (' . $combo_item->code . ') </td>
                                                    <td class="text-right">' . $this->sma->formatQuantity($combo_item->qty) . ' ' . $combo_item->unit_code . '</td>
                                                    <td class="text-right">' . $this->sma->formatMoney($combo_item->price * ($combo_item->qty * ($combo_item->operation_value > 0 ? $combo_item->operation_value : 1))) . '</td>
                                                  </tr>';
                                            $total_combo_cost += ($combo_item->price * ($combo_item->qty * ($combo_item->operation_value > 0 ? $combo_item->operation_value : 1)));
                                        } ?>
                                    </tbody>
                                    <tfoot>
                                        <th colspan="2"><?= lang('total_cost') ?></th>
                                        <th class="text-right"><?= $this->sma->formatMoney($total_combo_cost) ?></th>
                                    </tfoot>
                                </table>
                            </div>
                        <?php } ?>
                        <?php if (!empty($options)) { ?>
                            <h3 class="bold"><?= lang('product_variants_quantity'); ?></h3>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed dfTable">
                                    <thead>
                                        <tr>
                                            <th><?= lang('warehouse_name') ?></th>
                                            <th><?= lang('product_variant'); ?></th>
                                            <th><?= lang('quantity') . ' (' . lang('rack') . ')'; ?></th>
                                            <?php if ($Owner || $Admin) {
                                                echo '<th>' . lang('price_addition') . '</th>';
                                            } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($options as $option) {
                                            if ($option->wh_qty != 0) {
                                                echo '<tr><td>' . $option->wh_name . '</td><td>' . $option->name . ' (' . $option->code . ') ' . '</td><td class="text-center">' . $this->sma->formatQuantity($option->wh_qty) . '</td>';
                                                if ($Owner || $Admin && (!$Customer || $this->session->userdata('show_cost'))) {
                                                    echo '<td class="text-right">' . $this->sma->formatMoney($option->price) . '</td>';
                                                }
                                                echo '</tr>';
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <?php if ($variants) { ?>
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins border-bottom">
                                <div class="ibox-title">
                                    <h5><?= lang("product_variants"); ?></h5>
                                    <div class="ibox-tools">
                                        <button class="btn btn-primary show_collapse"><?= lang('view_more') ?> <i class="show_collapse fa fa-chevron-down"></i></button>
                                    </div>
                                </div>
                                <div class="ibox-content ibox_collapse hidden" style="display: none;">
                                    <?php if ($variants) { ?>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <?php foreach ($variants as $variant) {
                                                    echo '<span class="btn btn-sm btn-outline btn-success" style="margin-bottom:2px !important;">' . $variant->name . ' (' . $variant->code . ') </span> ';
                                                } ?>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        Sin variante asignada
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <?php if ($this->Settings->prioridad_precios_producto == 11) : ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><?= lang('unit') ?></th>
                                        <?php foreach ($price_groups as $key => $pg) : ?>
                                            <th><?= $pg->name ?></th>
                                        <?php endforeach ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($units as $unit) : ?>
                                        <?php
                                        if (!isset($hybrid_prices[$product->id][$unit->id])) {
                                            continue;
                                        }
                                        ?>
                                        <tr>
                                            <td><?= $unit->name ?></td>
                                            <?php foreach ($price_groups as $key => $pg) : ?>
                                                <td>
                                                    <?= (isset($hybrid_prices[$product->id][$unit->id][$pg->id]) ? $hybrid_prices[$product->id][$unit->id][$pg->id] : 0) + ($this->Settings->ipoconsumo && $product->tax_method == 0 ? $product->consumption_sale_tax : 0) ?>
                                                </td>
                                            <?php endforeach ?>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <?php if ($product_price_groups) : ?>
                                <h3><?= lang('price_groups') ?></h3>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><?= lang('price_group_name') ?></th>
                                            <th><?= lang('price_group_price') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($product_price_groups as $price_group) : ?>
                                            <tr>
                                                <th><?= $price_group->name ?></th>
                                                <td class="text-right"><?= $this->sma->formatMoney($price_group->price + ($this->Settings->ipoconsumo && $product->tax_method == 0 ? $product->consumption_sale_tax : 0)) ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            <?php else : ?>
                                <h3><?= lang('product_without_price_group_relation') ?></h3>
                            <?php endif ?>
                        <?php endif ?>
                    </div>
                    <?php if (isset($product_unit_prices) && $product_unit_prices) : ?>
                        <div class="col-xs-6">
                            <h3><?= lang('unit_prices') ?></h3>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><?= lang('product_unit_name') ?></th>
                                        <th><?= lang('factor') ?></th>
                                        <th><?= lang('active') ?></th>
                                        <th><?= lang('product_unit_price') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $main_product_unit_price['valor_unitario'] = $product->price;
                                    $main_product_unit_price['operation_value'] = 1;
                                    $main_product_unit_price['name'] = $unit ? $unit->name : '';
                                    array_unshift($product_unit_prices, (object) $main_product_unit_price);
                                    foreach ($product_unit_prices as $key => $unit_price) {
                                        $ordenar_units[$key] = $unit_price->operation_value;
                                    }

                                    ?>
                                    <?php foreach ($product_unit_prices as $unit_price) : ?>
                                        <tr>
                                            <th><?= $unit_price->name ?></th>
                                            <?php

                                            $status = '';
                                            if (isset($unit_price->status)) {
                                                if ($unit_price->status == 1) {
                                                    $status = lang('active');
                                                } else {
                                                    $status = lang('inactive');
                                                }
                                            }
                                            ?>
                                            <th><?= isset($unit_price->operator) ? $unit_price->operator . " " . $this->sma->formatQuantity($unit_price->operation_value) : " * 1" ?></th>
                                            <th><?= $status ?></th>
                                            <td class="text-right"><?= $this->sma->formatMoney($unit_price->valor_unitario + ($this->Settings->ipoconsumo && $product->tax_method == 0 ? $product->consumption_sale_tax : 0)) ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endif ?>
                </div>
                <?php if (count($billers_selected)) : ?>
                    <div class="col-xs-12">
                        <h3><?= lang('product_exclusive_billers') ?></h3>
                        <p>
                            <?php foreach ($billers_selected as $bskey => $bs) : ?>
                                <?= ($bskey > 0 ? ", " : "") . $bs->company ?>
                            <?php endforeach ?>
                        </p>
                    </div>
                <?php endif ?>
                <div class="col-xs-12">

                    <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
                    <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>
                </div>
            </div>
            <?php if (!$Supplier || !$Customer) { ?>
                <div class="row no-print">
                    <div class="col-xs-3">
                        <a href="<?= admin_url('products/print_barcodes/' . $product->id) ?>" class="tip btn btn-primary" title="<?= lang('print_barcode_label') ?>" style="width:100%;">
                            <i class="fa fa-print"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('print_barcode_label') ?></span>
                        </a>
                    </div>
                    <div class="col-xs-3">
                        <a href="<?= admin_url('products/pdf/' . $product->id) ?>" class="tip btn btn-primary" title="<?= lang('pdf') ?>" style="width:100%;">
                            <i class="fa fa-download"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                        </a>
                    </div>
                    <div class="col-xs-3">
                        <a href="<?= admin_url('products/edit/' . $product->id) ?>" class="tip btn btn-primary tip" title="<?= lang('edit_product') ?>" style="width:100%;">
                            <i class="fa fa-edit"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                        </a>
                    </div>
                    <div class="col-xs-3">
                        <a class="tip btn btn-primary tip" onclick="printDiv('print_this');" style="width:100%;">
                            <i class="fa fa-print"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('print') ?></span>
                        </a>
                    </div>
                    <!-- <div class="col-sm-4">
                        <a href="#" class="tip btn btn-danger bpo" title="<b><?= lang("delete_product") ?></b>"
                            data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('products/delete/' . $product->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                            data-html="true" data-placement="top">
                            <i class="fa fa-trash-o"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
                        </a>
                    </div> -->
                </div>
                <div class="row">
                    <hr class="col-sm-11">
                    <div class="col-md-3">
                        <?= lang('registration_date') ?> <br>
                        <label><?= $product->registration_date ?></label>
                    </div>
                    <div class="col-md-3">
                        <?= lang('created_by') ?> <br>
                        <label><?= $user_creation ? ($user_creation->first_name . " " . $user_creation->last_name) : " - " ?></label>
                    </div>
                    <div class="col-md-3">
                        <?= lang('last_update') ?> <br>
                        <label><?= $product->last_update ?></label>
                    </div>
                    <div class="col-md-3">
                        <?= lang('edited_by') ?> <br>
                        <label><?= $user_edition ? ($user_edition->first_name . " " . $user_edition->last_name) : " - " ?></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <?= lang('id') . " " . lang('product') ?> <br>
                        <label><?= $product->id ?></label>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.tip').tooltip();
                    });
                </script>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#myModal').ready(function() {
        $('.change_img').click(function(event) {
            event.preventDefault();
            var img_src = $(this).attr('href');
            $('#pr-image').attr('src', img_src);
            return false;
        });
        $('.show_collapse').on('click', function() {
            if ($('.ibox_collapse').hasClass('hidden')) {
                $('.ibox_collapse').slideDown();
                $('.ibox_collapse').removeClass('hidden');
            } else {
                $('.ibox_collapse').slideUp();
                $('.ibox_collapse').addClass('hidden');
            }
        });
    });

    function printDiv(nombreDiv) {
        var divToPrint = document.getElementById(nombreDiv);
        var newWin = window.open('', 'Print-Window');
        var header = $('head').html();
        newWin.document.open();
        newWin.document.write('<html><head>' + header + '</head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function() {
            newWin.close();
        }, 10);
    }
</script>