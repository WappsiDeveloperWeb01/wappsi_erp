<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?= lang('payment_collection') ?></title>
        <base href="<?=base_url()?>"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            .text-center { text-align: center ; }
            table { width: 100% ; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
        </style>
    </head>

    <body id="closeRegisterPrint">
        <div id="wrapper">
            <div style="text-align: center;">
                <div id="receiptData" style="text-align:center;"> 
                    <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="company_logo">' : ''; ?>
                    <h3 class=""><?= ucwords(mb_strtolower($Settings->site_name)); ?></h2>
                    <h4 class=""><?= lang('payment_collection') ?></h4>
                </div>
                <table class="table table-condensed" id="contenido_factura" style="margin-bottom:0;">
                    <tbody>
                        <tr>
                            <td class="text-left"><strong><?= lang("date"); ?>: </strong></td>
                            <td class="text-left"><?php echo $this->sma->hrld($payment_collection[0]->date); ?> </td>
                        </tr>
                        <tr>
                            <td class="text-left" ><strong><?= lang("reference"); ?>: </strong></td>
                            <td class="text-left"><?php echo $payment_collection[0]->reference_no; ?> </td>
                        </tr>
                        <tr>
                            <td class="text-left"><strong><?= lang("biller"); ?>: </strong></td>
                            <td class="text-left"><?php echo $biller->name; ?> </td>
                        </tr>
                        <tr>
                            <td class="text-left"><strong><?= lang("customer"); ?>: </strong></td>
                            <td class="text-left"><?php echo $payment_collection[0]->customer_name; ?></td>
                        </tr>
                        <tr>
                            <td class="text-left"><strong><?= lang("document_type_abreviate"); ?>: </strong></td>
                            <td class="text-left"><?php echo $payment_collection[0]->vat_no; ?></td>
                        </tr>
                        <?php foreach ($payment_collection as $pc): ?>
                            <tr>
                                <td class="text-left"><strong><?= lang("paid_by"); ?>: </strong></td>
                                <td class="text-left"><?php echo lang($pc->paid_by); ?></td>
                            </tr>
                            <tr>
                                <td class="text-left"><strong><?= lang("amount"); ?>: </strong></td>
                                <td class="text-left"><?php echo $this->sma->formatMoney($pc->amount); ?></td>
                            </tr>
                        <?php endforeach ?>
                            
                        <?php if (isset($cost_center) && $cost_center): ?>
                            <tr>
                                <td class="text-left"><strong><?= lang("cost_center"); ?>: </strong></td>
                                <td class="text-left"><?= $cost_center->name ?> ( <?= $cost_center->code ?> )</td>
                            </tr>
                        <?php endif ?>
                        <br>
                        <?php if(isset($payment_collection[0]->note) && $payment_collection[0]->note != ''): ?>
                            <tr>
                                <td colspan="2" class="text-center"><?php echo $payment_collection[0]->note; ?></td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <div class="col-xs-12">
                    <p><?= lang("created_by"); ?>: <?= $user->first_name.' '.$user->last_name; ?> </p>
                </div>
                <div class="modal-footer no-print">
                    <div>
                    <button  type="button" class="btn btn-block btn-primary " onclick="imprimirTirillaTraslado();">
                        <i class="fa fa-print"></i> <?= lang('print'); ?>
                    </button>
                    </div>
                    <button style="with:100%" type="button" class="btn btn-block btn btn-warning" onclick="window.location.href = '<?= admin_url().'payments_collections/index' ?>';">
                        <i class="fa fa-arrow-circle-left"></i> <?= lang('go_back'); ?>
                    </button>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', '.po', function (e) {
            e.preventDefault();
            $('.po').popover({
                html: true,
                placement: 'left',
                trigger: 'manual'
            }).popover('show').not(this).popover('hide');
            return false;
        });
        $(document).on('click', '.po-close', function () {
            $('.po').popover('hide');
            return false;
        });
        $(document).on('click', '.po-delete', function (e) {
            var row = $(this).closest('tr');
            e.preventDefault();
            $('.po').popover('hide');
            var link = $(this).attr('href');
            $.ajax({
                type: "get", url: link,
                success: function (data) {
                    row.remove();
                    addAlert(data, 'success');
                },
                error: function (data) {
                    addAlert('Failed', 'danger');
                }
            });
            return false;
        });
        setTimeout(function() {
            imprimirTirillaTraslado();
        }, 500);
    });

    function imprimirTirillaTraslado()
    {
      $('.without_close').css('display', '');
      var divToPrint=document.getElementById('closeRegisterPrint');
      var newWin=window.open('','Print-Window');
      var header = $('head').html();
      newWin.document.open();
      newWin.document.write('<head>'+header+'</head><body onload="window.print()" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');
      newWin.document.close();
      setTimeout(function(){
            newWin.close();
        },500);
    }
</script>