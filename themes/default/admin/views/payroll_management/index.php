<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $payroll_status = isset($payroll) && !empty($payroll) ? $payroll->status : NULL; ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="contracts_table" class="table table-hover " style="width: 100%">
                                    <thead>
                                        <tr class="primary">
                                            <th></th>
                                            <th><?= lang("code"); ?></th>
                                            <th><?= lang("creation_date"); ?></th>
                                            <th><?= lang("start_date"); ?></th>
                                            <th><?= lang("end_date"); ?></th>
                                            <th><?= lang("payroll_management_number_employees"); ?></th>
                                            <th><?= lang("payroll_management_earnings"); ?></th>
                                            <th><?= lang("payroll_management_deductions"); ?></th>
                                            <th><?= lang("payroll_management_total_payment"); ?></th>
                                            <th><?= lang("payroll_management_total_provisions"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody class="pointer">
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        load_datatable();
        existing_employee_salary_change();
        existing_configuration_data_changes();

        $(document).on('click', '.see_payroll td:not(:last-child)', function() {
            window.location.href = site.base_url + 'payroll_management/see/' + $(this).parent('.see_payroll').attr('id');
        });

        $(document).on('click', '.option_approve_payroll', function() { confirm_approval_current_payroll($(this).data('payroll_id')); });
        $(document).on('click', '.approve_payroll', function() {
            window.location.href = site.base_url + 'payroll_management/approve/'+ $('#payroll_id').val();
        });
        $(document).on('click', '.option_delete_payroll', function() {
            confirm_deletion_payroll($(this).data('payroll_id'));
        });
        $(document).on('click', '.optionToPayPayroll a', function(event) { confirmToPayPayroll(event, $(this)); });
    });

    function load_datatable()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#contracts_table').dataTable({
            "aaSorting": [[4, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('payroll_management/get_datatables') ?>',
            dom: '<"col-sm-7 containerBtn"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "see_payroll";
                return nRow;
            },
            drawCallback: function(settings) {
                <?php if ($this->Owner || $this->Admin) { ?>
                    <?php if ($payroll_status == APPROVED || $payroll_status == PAID || $payroll_status == NULL) { ?>
                        $('.actionsButtonContainer').html(`<a href="<?= admin_url('payroll_management/add') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>`);
                    <?php } ?>
                <?php } else { ?>
                <?php if ($GP['payroll_management-add'] == YES) { ?>
                    <?php if ($payroll_status == APPROVED || $payroll_status == PAID || $payroll_status == NULL) { ?>
                        $('.actionsButtonContainer').html(`<a href="<?= admin_url('payroll_management/add') ?>" class="btn btn-primary new-button" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>`);
                    <?php } ?>
                  <?php } ?>
                <?php } ?>

                $('[data-toggle-second="tooltip"]').tooltip();
            },
            "aoColumns": [
                { visible: false},
                { className: 'text-center' },
                { className: 'text-center' },
                null,
                { className: 'text-center' },
                { className: 'text-right' },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        return (data == 1) ? '<span class="label label-info"><?= lang("payroll_management_in_preparation") ?></span>' : (data == 2) ? '<span class="label label-success"><?= lang("payroll_management_approved") ?></span>' : '<span class="label label-primary"><?= lang("payroll_management_paid") ?></span>';
                    }
                },
                {
                    bSortable: false,
                    render: function(data, type, row) {
                        var actions = $(data);

                        <?php if ($this->Owner || $this->Admin) { ?>
                            if (row[10] == '<?= IN_PREPARATION ?>') {
                                actions.find('.dropdown-menu').find('.optionToPayPayroll a').remove();
                            }
                            if (row[10] == '<?= APPROVED ?>') {
                                actions.find('.dropdown-menu').find('.option_approve_payroll a').remove();
                                actions.find('.dropdown-menu').find('.option_update_payroll a').remove();
                                actions.find('.dropdown-menu').find('.option_delete_payroll a').remove();
                            }
                            if (row[10] == '<?= PAID ?>') {
                                actions.find('.dropdown-menu').find('.option_update_payroll a').remove();
                                actions.find('.dropdown-menu').find('.option_approve_payroll a').remove();
                                actions.find('.dropdown-menu').find('.optionToPayPayroll a').remove();
                                actions.find('.dropdown-menu').find('.option_delete_payroll a').remove();
                            }
                        <?php } else { ?>
                            if (row[10] != '<?= IN_PREPARATION ?>' || '<?= empty($GP['payroll_management-update']) ?>' == true) {
                                actions.find('.dropdown-menu').find('.option_update_payroll a').remove();
                                actions.find('.dropdown-menu').find('.option_delete_payroll a').remove();
                            }
                            if (row[10] == '<?= APPROVED ?>' || '<?= empty($GP['payroll_management-approve']) ?>' == true) {
                                actions.find('.dropdown-menu').find('.option_approve_payroll a').remove();
                            }
                        <?php } ?>

                        var data = actions.prop('outerHTML');
                        return data;
                    }
                }
            ]
        });
    }

    function existing_employee_salary_change()
    {
        <?php if ($this->session->userdata('employee_salary_change') == TRUE && !empty($in_preparation_payroll)) { ?>
            swal({
                title: 'Mensaje de Advertencia',
                text: 'El salario base de un empleado fue modificado. Es necesario Actualizar para reflejar el cambio en la nómina',
                type: 'warning',
                confirmButtonText: "¡Entendido!",
                closeOnConfirm: true
            });
        <?php } ?>
    }

    function confirm_approval_current_payroll(payroll_id)
    {
        swal({
            title: "¿Está seguro de aprobar la nómina?",
            text: "Después de aprobar no se podrá modificar ningún dato",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Aprobar!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                <?php if (!empty($payroll)) { ?>
                    <?php if ($this->Payroll_settings->payment_frequency == BIWEEKLY) { ?>
                        <?php if ($payroll->number == 2) { ?>
                            second_confirm_approval_current_payroll(payroll_id);
                        <?php } else { ?>
                            approve_current_payroll(payroll_id);
                        <?php } ?>
                    <?php } else { ?>
                        approve_current_payroll(payroll_id);
                    <?php } ?>
                <?php } ?>
            }
        });
    }

    function second_confirm_approval_current_payroll(payroll_id)
    {
        swal({
            title: "¡Advertencia!",
            text: "Este proceso consolidará los datos de nómina electrónica y no podrá revertirse. ¿Aún desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Continuar!",
            closeOnConfirm: false
        }, function () {
            approve_current_payroll(payroll_id);
        });
    }

    function approve_current_payroll(payroll_id)
    {
        swal({
            title: 'Aprobando nómina',
            text: 'Por favor, espere un momento',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });
        window.location.href = site.base_url + 'payroll_management/approve/'+ payroll_id;
    }

    function confirm_deletion_payroll(payroll_id)
    {
        swal({
            title: "¿Seguro de eliminar la Nómina?",
            text: "Se perderá todo el proceso realizado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, eliminarlo!",
            closeOnConfirm: false
        }, function () {
            delete_current_payroll(payroll_id);
        });
    }

    function delete_current_payroll(payroll_id)
    {
        swal({
            title: 'Eliminando nómina',
            text: 'Por favor, espere un momento',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });
        window.location.href = site.base_url + 'payroll_management/delete/'+ payroll_id;
    }

    function existing_configuration_data_changes()
    {
        <?php if ($this->session->userdata('existing_configuration_data_changes') == TRUE && !empty($in_preparation_payroll)) { ?>
            swal({
                title: 'Mensaje de Advertencia',
                text: 'Los porcentajes para el cálculo de los parafiscales han sido cambiados. Es necesario Actualizar para reflejar el cambio en la nómina',
                type: 'warning',
                confirmButtonText: "¡Entendido!",
                closeOnConfirm: true
            });
        <?php } ?>

        <?php if ($this->session->userdata('employee_settlement_date_change') == TRUE && !empty($in_preparation_payroll)) { ?>
            swal({
                title: 'Mensaje de Advertencia',
                text: 'La fecha final de liquidación de un empleado ha sido cambiado. Es necesario Actualizar para reflejar el cambio en la nómina',
                type: 'warning',
                confirmButtonText: "¡Entendido!",
                closeOnConfirm: true
            });
        <?php } ?>
    }

    function confirmToPayPayroll(event, element)
    {
        event.preventDefault();
        swal({
            title: "¿Seguro de pagar La nómina?",
            text: "Este proceso cambiará el estado de la nómina",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Pagar!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                swal({
                    title: 'Pagando la Nómina',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false,
                });
                window.location.href = element.attr('href');
            }
        });
    }
</script>