<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    p {
        word-break: break-all;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= $supplier->company && $supplier->company != '-' ? $supplier->company : $supplier->name; ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("name"); ?></h4>
                            <p><?= $supplier->name; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("id_document_type"); ?></h4>
                            <p><?= $supplier->nombre_tipo_documento; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("vat_no"); ?></h4>
                            <p><?= $supplier->vat_no.($supplier->digito_verificacion > 0 ? "-".$supplier->digito_verificacion : ""); ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("birth_month"); ?></h4>
                            <p><?= isset(lang('months')[$supplier->birth_month]) ? lang('months')[$supplier->birth_month]." - ".$supplier->birth_day : lang('undefined'); ?></p>
                        </div>
                    </div>
                    <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
                    <div class="row">
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("email"); ?></h4>
                            <p><?= $supplier->email; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("phone"); ?></h4>
                            <p><?= $supplier->phone; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("address"); ?></h4>
                            <p><?= $supplier->address; ?></p>
                        </div>
                    </div>
                    <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <h4><?= lang("label_types_obligations"); ?></h4>
                            <p>
                                <?php if ($types_supplier_obligations): ?>
                                    <?php foreach ($types_supplier_obligations as $tco): ?>
                                        <?= $tco->types_obligations_id." - ".$tco->description.($tco !== end($types_supplier_obligations) ? ", " : "") ?>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <?= lang('undefined') ?>
                                <?php endif ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("country"); ?></h4>
                    <p><?= $supplier->country; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("state"); ?></h4>
                    <p><?= $supplier->state; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("city"); ?></h4>
                    <p><?= $supplier->city; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("postal_code"); ?></h4>
                    <p><?= $supplier->postal_code; ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("zone"); ?></h4>
                    <p><?= $supplier->zone_name ? $supplier->zone_name : lang('undefined'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("subzone"); ?></h4>
                    <p><?= $supplier->subzone_name ? $supplier->subzone_name : lang('undefined'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("label_tradename"); ?></h4>
                    <p><?= $supplier->company ? $supplier->company : lang('undefined'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("label_commercial_register"); ?></h4>
                    <p><?= $supplier->commercial_register ? $supplier->commercial_register : lang('undefined'); ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("label_type_vat_regime"); ?></h4>
                    <p><?=lang($supplier->tvat_regime); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("price_group"); ?></h4>
                    <p><?= $supplier->price_group_name; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("deposit"); ?></h4>
                    <p><?= $this->sma->formatMoney($supplier->deposit_amount); ?></p>
                </div>
                <?php if ($created_user): ?>
                    <div class="col-sm-3 col-xs-3">
                    <h4><?= lang('created_by') ?></h4>
                        <p><?= $created_user->first_name. ' ' . $created_user->last_name ?></p>
                    </div>
                <?php endif; ?>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-4 col-xs-4">
                    <h4><?= lang("supplier_payment_type"); ?></h4>
                    <?php $cptypes = [
                                '' => lang('select'),
                                '0' => lang('credit'),
                                '1' => lang('payment_type_cash'),
                                ]; ?>
                    <p><?= $cptypes[$supplier->customer_payment_type]; ?></p>
                </div>
                <?php if ($supplier->customer_payment_type == 0): ?>
                    <div class="col-sm-4 col-xs-4">
                        <h4><?= lang("supplier_credit_limit"); ?></h4>
                        <p><?= $this->sma->formatMoney($supplier->customer_credit_limit) ?></p>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <h4><?= lang("supplier_payment_term"); ?></h4>
                        <p><?= $supplier->customer_payment_term ?></p>
                    </div>
                <?php endif ?>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <label><?= lang('default_rete_fuente') ?></label>
                    <p><?= $default_rete_fuente ? $default_rete_fuente->code." - ".$default_rete_fuente->name : lang('undefined') ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <label><?= lang('default_rete_iva') ?></label>
                    <p><?= $default_rete_iva ? $default_rete_iva->code." - ".$default_rete_iva->name : lang('undefined') ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <label><?= lang('default_rete_ica') ?></label>
                    <p><?= $default_rete_ica ? $default_rete_ica->code." - ".$default_rete_ica->name : lang('undefined') ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <label><?= lang('default_rete_other') ?></label>
                    <p><?= $default_rete_other ? $default_rete_other->code." - ".$default_rete_other->name : lang('undefined') ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-3">
                    <label><?= lang('note') ?></label>
                    <p><?= $supplier->note ?></p>
                </div>
            </div>
            
            <?php if ($custom_fields): ?>
                <hr>
                <div class="row">
                <?php foreach ($custom_fields as $cf_arr): ?>
                    <?php $cf = $cf_arr['data']; ?>
                    <?php if ($supplier->{$cf->cf_code}): ?>
                        <div class="col-sm-3 col-xs-3">
                            <label><?= $cf->cf_name ?></label>
                            <p><?= $supplier->{$cf->cf_code} ?></p>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
                </div>
            <?php endif ?>

            <div class="modal-footer no-print">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= lang('close'); ?></button>
                <?php if ($Owner || $Admin || $GP['reports-suppliers']) { ?>
                    <a href="<?=admin_url('reports/supplier_report/'.$supplier->id);?>" target="_blank" class="btn btn-primary"><i class="fa fa-chart-line"></i> <?= lang('suppliers_report'); ?></a>
                <?php } ?>
                <?php if ($Owner || $Admin || $GP['suppliers-edit']) { ?>
                    <a href="<?=admin_url('suppliers/edit/'.$supplier->id);?>" data-toggle="modal" data-target="#myModal2" class="btn btn-primary"><i class="fa fa-edit"></i> <?= lang('edit_supplier'); ?></a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>