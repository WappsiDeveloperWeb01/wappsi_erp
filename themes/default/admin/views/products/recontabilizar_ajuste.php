<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
@media print {
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 3px !important;
    }
}
</style>

<div class="modal-dialog modal-md no-modal-header">
    <div class="modal-content">
        <div class="modal-header">
            <h3>Recontabilizar ajuste <?= $inv['reference_no'] ?></h3>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("products/contabilizarAjuste", $attrib);
         ?>
            <div class="modal-body">
                <?php if (!$exists): ?>
                    <p>Usted va a contabilizar el ajuste <?= $inv['reference_no'] ?>, ¿Está seguro de esta acción?</p>
                <?php else: ?>
                    <p><b>Atención :</b> El ajuste <?= $inv['reference_no'] ?> ya está contabilizado, ¿desea reprocesar la contabilización?</p>
                <?php endif ?>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="adjustment_id" value="<?= $inv['id'] ?>">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Aceptar</button>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });
</script>
