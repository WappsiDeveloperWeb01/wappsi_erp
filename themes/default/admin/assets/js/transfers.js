$(document).ready(function () {
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }
    // Order level shipping and discoutn localStorage
    $('#tostatus').change(function (e) {
        localStorage.setItem('tostatus', $(this).val());
    });
    if (tostatus = localStorage.getItem('tostatus')) {
        $('#tostatus').select2("val", tostatus);
        if(tostatus == 'completed') {
            $('#tostatus').select2("readonly", true);
        }
    }
    var old_shipping;
    $('#toshipping').focus(function () {
        old_shipping = $(this).val();
    }).change(function () {
        if (!is_numeric($(this).val())) {
            $(this).val(old_shipping);
            bootbox.alert(lang.unexpected_value);
            return;
        } else {
            shipping = $(this).val() ? parseFloat($(this).val()) : '0';
        }
        localStorage.setItem('toshipping', shipping);
        var gtotal = total  + shipping;
        $('#gtotal').text(formatMoney(gtotal));
        $('#tship').text(formatMoney(shipping));
    });
    if (toshipping = localStorage.getItem('toshipping')) {
        shipping = parseFloat(toshipping);
        $('#toshipping').val(shipping);
    }
    //localStorage.clear();
    // If there is any item in localStorage
    if (localStorage.getItem('toitems')) {
        loadItems();
    }

    // clear localStorage and reload
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('toitems')) {
                    localStorage.removeItem('toitems');
                }
                if (localStorage.getItem('toshipping')) {
                    localStorage.removeItem('toshipping');
                }
                if (localStorage.getItem('toref')) {
                    localStorage.removeItem('toref');
                }
                if (localStorage.getItem('to_warehouse')) {
                    localStorage.removeItem('to_warehouse');
                }
                if (localStorage.getItem('tonote')) {
                    localStorage.removeItem('tonote');
                }
                if (localStorage.getItem('from_warehouse')) {
                    localStorage.removeItem('from_warehouse');
                }
                if (localStorage.getItem('todate')) {
                    localStorage.removeItem('todate');
                }
                if (localStorage.getItem('tostatus')) {
                    localStorage.removeItem('tostatus');
                }
                if (localStorage.getItem('tobiller_origin')) {
                    localStorage.removeItem('tobiller_origin');
                }
                if (localStorage.getItem('tobiller_destination')) {
                    localStorage.removeItem('tobiller_destination');
                }

                 $('#modal-loading').show();
                 location.reload();
             }
        });
    });

// save and load the fields in and/or from localStorage

    $('#toref').change(function (e) {
        localStorage.setItem('toref', $(this).val());
    });
    if (toref = localStorage.getItem('toref')) {
        $('#toref').val(toref);
    }
    $('#to_warehouse').change(function (e) {
        localStorage.setItem('to_warehouse', $(this).val());
    });
    if (to_warehouse = localStorage.getItem('to_warehouse')) {
        $('#to_warehouse').select2("val", to_warehouse);
    }
    $('#from_warehouse').change(function (e) {
        localStorage.setItem('from_warehouse', $(this).val());
    });
    if (from_warehouse = localStorage.getItem('from_warehouse')) {
        $('#from_warehouse').select2("val", from_warehouse);
        if (count > 1) {
            // $('#from_warehouse').select2("readonly", true);
        }
    }
    $('#companies_id').change(function(){
        localStorage.setItem('companies_id', $(this).val());
    });
    if (companies_id = localStorage.getItem('companies_id')) {
        $('#companies_id').select2("val", companies_id);
    }
    //$(document).on('change', '#tonote', function (e) {
        $('#tonote').redactor('destroy');
        $('#tonote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('tonote', v);
            }
        });
        if (tonote = localStorage.getItem('tonote')) {
            $('#tonote').redactor('set', tonote);
        }

        $(document).on('change', '.rexpiry', function () {
            var item_id = $(this).closest('tr').attr('data-item-id');
            toitems[item_id].row.expiry = $(this).val();
            localStorage.setItem('toitems', JSON.stringify(toitems));
        });


    // prevent default action upon enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });


    /* ----------------------
     * Delete Row Method
     * ---------------------- */

    $(document).on('click', '.todel', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        delete toitems[item_id];
        row.remove();
        if(toitems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('toitems', JSON.stringify(toitems));
            loadItems();
            return;
        }
    });

    /* --------------------------
     * Edit Row Quantity Method
     -------------------------- */
     var old_row_qty;
     $(document).on("focus", '.rquantity', function () {
        old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        toitems[item_id].row.base_quantity = new_qty;
        if(toitems[item_id].row.unit != toitems[item_id].row.base_unit) {
            $.each(toitems[item_id].units, function(){
                if (this.id == toitems[item_id].row.unit) {
                    toitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        toitems[item_id].row.qty = new_qty;
        localStorage.setItem('toitems', JSON.stringify(toitems));
        loadItems();
    }).on('keydown', '.rquantity', function(e){
        if (e.keyCode == 13) {
            localStorage.setItem('enter_pressed_for_quantity', true);
            $(this).trigger('change');
            setTimeout(function() {
                localStorage.removeItem('enter_pressed_for_quantity');
            }, 2000);
        }
    });

    /* --------------------------
     * Edit Row Cost Method
     -------------------------- */
     var old_cost;
     $(document).on("focus", '.rcost', function () {
        old_cost = $(this).val();
    }).on("change", '.rcost', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_cost);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_cost = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        toitems[item_id].row.cost = new_cost;
        localStorage.setItem('toitems', JSON.stringify(toitems));
        loadItems();
    });

    $(document).on("click", '#removeReadonly', function () {
        // $('#from_warehouse').select2('readonly', false);
        return false;
    });


    $(document).on('change', '#tobiller_destination', function(){
        tobiller_origin = $('#tobiller_origin').val();
        tobiller_destination = $('#tobiller_destination').val();
        localStorage.setItem('tobiller_destination', $(this).val());
        default_warehouse = $('#tobiller_destination option:selected').data('warehousedefault');
        setTimeout(function() {
            $('#to_warehouse').select2('val', default_warehouse).trigger('change');
            warehouses_related = billers_data[$('#tobiller_destination').val()]?.warehouses_related;   
            if (warehouses_related) {
               $('#to_warehouse option').each(function(index, option){
                    $(option).prop('disabled', false);
                    if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                        $(option).prop('disabled', true);
                    }
                });
                $('#to_warehouse').select2(); 
            } 
        }, 850);
        // } else {
            // $('#tobiller_destination').select2('val', '').trigger('change');
            // $('#to_warehouse').select2('val', '').trigger('change');
        if (tobiller_origin == tobiller_destination) {
            command: toastr.warning('Las sucursales de origen y destino son las mismas', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
        }
    });

    if (tobiller_origin = localStorage.getItem('tobiller_origin')) {
        $('#tobiller_origin').select2('val', tobiller_origin).trigger('change');
    } else {
        $('#tobiller_origin').trigger('change');
    }

    if (tobiller_destination = localStorage.getItem('tobiller_destination')) {
        $('#tobiller_destination').select2('val', tobiller_destination).trigger('change');
    } else {
        $('#tobiller_destination').trigger('change');
    }
    if (is_edit && (tostatus == 'sent' || tostatus == 'completed')) {
        $('#tobiller_origin').select2('readonly', true);
        $('#tobiller_destination').select2('readonly', true);
        $('#from_warehouse').select2('readonly', true);
        $('#to_warehouse').select2('readonly', true);
    } else if (is_edit) {
        $('#tobiller_origin').select2('readonly', true);
    }
});

/* -----------------------
 * Edit Row Modal Hanlder
 ----------------------- */
$(document).on('click', '.edit', function () {
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        if (site.settings.update_product_tax_from_purchase == 0) {
            $('#ptax').select2('readonly', true);
        }
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        item_id = row.attr('data-item-id');
        item = toitems[item_id];
        var qty = row.children().children('.rquantity').val(),
        product_option = row.children().children('.roption').val(),
        unit_cost = formatDecimal(item.row.real_unit_cost),
        discount = row.children().children('.rdiscount').val();
        $('#prModalLabel').text(item.row.name);
        $('#prModalLabelCode').text(item.row.code);
        var real_unit_cost = item.row.real_unit_cost;
        var net_cost = real_unit_cost;
        if (site.settings.tax1) {
            $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax2').val(item.row.consumption_purchase_tax);
            if (item.row.purchase_tax_rate_2_percentage) {
                $('.ptax2_div').removeClass('col-sm-8').addClass('col-sm-4');
                $('#ptax2_percentage').val(item.row.purchase_tax_rate_2_percentage);
                $('.ptax2_percentage_div').fadeIn();
            } else {
                $('.ptax2_div').removeClass('col-sm-4').addClass('col-sm-8');
                $('#ptax2_percentage').val('');
                $('.ptax2_percentage_div').fadeOut();
            }
            $('#old_tax').val(item.row.tax_rate);
            var pr_tax = item.row.tax_rate, pr_tax_val = 0;
            if (pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {

                            if (toitems[item_id].row.tax_method == 0) {
                                pr_tax_val = formatDecimal((((real_unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                net_cost -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal((((real_unit_cost) * parseFloat(this.rate)) / 100));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }

                        } else if (this.type == 2) {

                            pr_tax_val = parseFloat(this.rate);
                            pr_tax_rate = this.rate;

                        }
                    }
                });
            }
        }
        net_cost = formatDecimals(net_cost);
        if (site.settings.product_serial !== 0) {
            $('#pserial').val(row.children().children('.rserial').val());
        }
        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.options !== false) {
            var o = 1;
            opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
            $.each(item.options, function () {
                if(o == 1) {
                    if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                }
                $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                o++;
            });
        }

        uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
        $.each(item.units, function () {
            if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                if(this.product_unit_id == item.row.product_unit_id_selected) {
                    $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                } else {
                    $("<option />", {value: this.product_unit_id, text: this.name}).appendTo(uopt);
                }
            } else {
                if(this.id == item.row.unit) {
                    $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                } else {
                    $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                }
            }
        });
        if (item.options === false ) { $('.variants_div').fadeOut(); }
        $('#poptions-div').html(opt);
        $('#punits-div').html(uopt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#pquantity').val(qty);
        $('#old_qty').val(qty);
        $('#pcost_net').val((net_cost));
        $('#pcost').val((unit_cost+parseFloat(item.row.consumption_purchase_tax)));
        $('#pcost_ipoconsumo').val('');
        $('#punit_cost').val(formatDecimal(parseFloat(unit_cost)+parseFloat(pr_tax_val)+parseFloat(item.row.consumption_purchase_tax)));
        $('#poption').select2('val', item.row.option);
        $('#old_cost').val(unit_cost);
        $('#row_id').val(row_id);
        $('#item_id').val(item_id);
        $('#pexpiry').val(row.children().children('.rexpiry').val());
        $('#pdiscount').val(getDiscountAssigned(discount, trmrate, 1));
        $('#net_cost').text(formatMoney(net_cost));
        $('#pro_tax').text(formatMoney((parseFloat(pr_tax_val) + parseFloat(item.row.consumption_purchase_tax))));
        $('#psubtotal').val('');
        $('#prModal').appendTo("body").modal('show');
        if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected_real_quantity = 1;
            units = item.units;
            unit_selected = units[item.row.product_unit_id_selected];
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty / unit_selected.operation_value;
                unit_selected_real_quantity = 1 * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty * unit_selected.operation_value;
                unit_selected_real_quantity = 1 / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            }
            $('#pquantity').val(unit_selected_quantity);
            $('#punit').select2('val', item.row.product_unit_id_selected).trigger('change');
            $('#pproduct_unit_cost').val((parseFloat(unit_cost) + parseFloat(item.row.consumption_purchase_tax)) * unit_selected_real_quantity);
            $('#pproduct_unit_cost').trigger('change');
            $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
            $('#pcost_um_net').val(formatDecimal((net_cost) * unit_selected.operation_value));
            $('#pcost_um').val(formatDecimal(unit_cost * unit_selected.operation_value));
            $('#pcost_um_total_net').val(formatDecimal(unit_selected_quantity * (net_cost * unit_selected.operation_value)));
            $('#pcost_um_total').val(formatDecimal(unit_selected_quantity * (unit_cost * unit_selected.operation_value)));
            $('.um_name').html('"'+unit_selected.name+'"');

            $('#pcost_um_total_net').attr('readonly', true);
            $('#pcost_um_total').attr('readonly', true);
            $('#pcost').attr('readonly', true);
            $('#pcost_net').attr('readonly', true);
        }
        $('.purchase_form_edit_product_unit_name').text($('#punit option:selected').text());

        if (item.row.cnt_units_prices !== undefined && item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) && item.row.promotion != 1) {
            if (item.units !== undefined) {
                sale_consumption_tax = 0;
                if (site.settings.ipoconsumo) {
                    sale_consumption_tax = item.row.consumption_sale_tax;
                }
                $('.pgprice_div').fadeOut();
                $.each(item.units, function(index, unit){
                    $('.pgprice_div[data-pgid="'+index+'"]').fadeIn();

                    if (site.settings.precios_por_unidad_presentacion == 2 && sale_consumption_tax > 0) {
                       if (isset(unit.operator) && unit.operator) {
                           if (unit.operator == "*") {
                               sale_consumption_tax = sale_consumption_tax * unit.operation_value;
                           } else if (unit.operator == "/") {
                               sale_consumption_tax = sale_consumption_tax / unit.operation_value;
                           }
                       }
                    }
                    $('.pgprice[data-pgid="'+index+'"]').val(formatDecimal(unit.valor_unitario_old) + formatDecimal(sale_consumption_tax));
                    if (unit.margin_update_price > 0) {
                        $('.pgmargin[data-pgid="'+index+'"]').val(formatDecimal(unit.margin_update_price));
                        if (site.settings.update_prices_by_margin_formula == 1) {
                            precio = unit_cost / (1 - (unit.margin_update_price / 100));
                        } else {
                            precio = unit_cost * (1 + (unit.margin_update_price / 100));
                        }
                        $('.pgnew_price[data-pgid="'+index+'"]').val(formatDecimal(precio));
                    } else {
                        $('.pgnew_price[data-pgid="'+index+'"]').val(formatDecimal(unit.valor_unitario_old) + formatDecimal(sale_consumption_tax));
                        margin = formatDecimals((((unit.valor_unitario_old - $('#pcost').val()) / unit.valor_unitario_old) * 100), 2);
                        $('.pgmargin[data-pgid="'+index+'"]').val(formatDecimal(margin));
                    }
                });
            } else {
                $('.div_update_prices').fadeOut();
            }
        } else {
            if (item.product_price_groups !== undefined && site.settings.update_prices_from_purchases == 1) {
                sale_consumption_tax = 0;
                if (site.settings.ipoconsumo) {
                    sale_consumption_tax = item.row.consumption_sale_tax;
                }
                $.each(item.product_price_groups, function(index, pg){
                    $('.pgprice[data-pgid="'+index+'"]').val(formatDecimal(pg.price) + formatDecimal(sale_consumption_tax));
                });
            } else {
                $('.div_update_prices').fadeOut();
            }
        }

    });

$('#prModal').on('shown.bs.modal', function (e) {
    if($('#poption').select2('val') != '') {
        $('#poption').select2('val', product_variant);
        product_variant = 0;
    }
});
$(document).on('change', '.rserial', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        toitems[item_id].row.serial = $(this).val();
        localStorage.setItem('toitems', JSON.stringify(toitems));
    });

    $(document).on('change', '.rvariant', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        toitems[item_id].row.option = $(this).val();
        localStorage.setItem('toitems', JSON.stringify(toitems));
        loadItems();
    });
    $(document).on('change', '.rpcost', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        cost = formatDecimal($(this).val());
        if (site.settings.tax_method == 1) {
            item_tax = calculateTax(toitems[item_id].tax_rate, cost, 0);
            item_tax_val = item_tax[0];
            cost = cost - item_tax_val;
        }
        toitems[item_id].row.real_unit_cost = cost;
        localStorage.setItem('toitems', JSON.stringify(toitems));
        loadItems();
    });
    $(document).on('keydown', '.rpcost', function (e) {
        input = $(this);
        if (e.keyCode == 13) {
            input.trigger('change');
        }
    });
    $(document).on('change', '.rpnet_cost', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        var net_cost = unit_cost = parseFloat($(this).val());
        if (potype == 1 && site.settings.tax_method == 0) {
            var item = toitems[item_id];
            var ipoconsumo_calculated = item.row.purchase_tax_rate_2_percentage ? calculate_second_tax(item.row.purchase_tax_rate_2_percentage, net_cost, 0, item.row.tax_method) :  [0, item.row.consumption_purchase_tax];
            var ipoconsumo =  ipoconsumo_calculated[1];
            var pr_tax = item.row.tax_rate, item_tax_method = item.row.tax_method;
            var pr_tax_val = 0;
            pr_tax_val = calculateTax(tax_rates[pr_tax], net_cost, 1);
            pr_tax_val = pr_tax_val[0];
            unit_cost = formatDecimal(net_cost) + formatDecimal(pr_tax_val) + formatDecimal(ipoconsumo);
        }
        toitems[item_id].row.real_unit_cost = formatDecimal(unit_cost);
        localStorage.setItem('toitems', JSON.stringify(toitems));
        loadItems();
    });
    $(document).on('keydown', '.rpnet_cost', function (e) {
        input = $(this);
        if (e.keyCode == 13) {
            input.trigger('change');
        }
    });

$(document).on('change', '#punit', function () {
    var row = $('#' + $('#row_id').val());
    var item_id = row.attr('data-item-id');
    var item = toitems[item_id];
    if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
        $(this).val(old_row_qty);
        bootbox.alert(lang.unexpected_value);
        return;
    }
    var unit = $('#punit').val();
    if(unit != toitems[item_id].row.base_unit) {
        $.each(item.units, function() {
            if (this.id == unit) {
                $('#pprice').val(formatDecimal((parseFloat(item.row.base_unit_cost)*(unitToBaseQty(1, this))), 4)).change();
            }
        });
    } else {
        $('#pprice').val(formatDecimal(item.row.base_unit_cost)).change();
    }
});
$(document).on('change', '#pcost_um_net, #pcost_um_total_net', function(){
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var pcost = parseFloat($('#pcost').val());
        var pcost_um_net = parseFloat($('#pcost_um_net').val());
        var pcost_um_total_net = parseFloat($('#pcost_um_total_net').val());
        var item = toitems[item_id];
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) {
            punit_quantity = $('#punit_quantity').val();
            pquantity = $('#pquantity').val();
            unit_um_cost_unit_net = formatDecimal(pcost_um_net / punit_quantity); //costo UNIDAD por um unitario antes de IVA
            ptax_unit_um_cost_unit = calculateTax(tax_rates[pr_tax], unit_um_cost_unit_net, 1);
            unit_um_cost_unit = formatDecimal(unit_um_cost_unit_net) + formatDecimal(ptax_unit_um_cost_unit[0]);
            //costo Unidad por um unitario con iva
            unit_um_cost_total_unit_net = formatDecimal(pcost_um_total_net / (punit_quantity * pquantity)); //costo UNIDAD por um total
            ptax_unit_um_cost_total_unit = calculateTax(tax_rates[pr_tax], unit_um_cost_total_unit_net, 1);
            unit_um_cost_total_unit = formatDecimal(unit_um_cost_total_unit_net) + formatDecimal(ptax_unit_um_cost_total_unit[0]);
            //costo Unidad por um unitario con iva
            if (pcost != unit_um_cost_unit) {
                $('#pcost').val(formatDecimals(unit_um_cost_unit)).trigger('change');
                $('#pcost_um_total_net').val(formatDecimals((unit_um_cost_unit * punit_quantity) * pquantity));
            } else if (pcost != unit_um_cost_total_unit) {
                $('#pcost').val(formatDecimals(unit_um_cost_total_unit)).trigger('change');
                $('#pcost_um_net').val(formatDecimals(punit_quantity * unit_um_cost_total_unit));
            }
        }
    });
    $(document).on('change', '#pcost_um, #pcost_um_total', function(){
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var pcost = parseFloat($('#pcost').val());
        var pcost_um = parseFloat($('#pcost_um').val());
        var pcost_um_total = parseFloat($('#pcost_um_total').val());
        var item = toitems[item_id];
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) {
            punit_quantity = $('#punit_quantity').val();
            pquantity = $('#pquantity').val();

            unit_um_cost_unit = formatDecimal(pcost_um / punit_quantity); //costo UNIDAD por um unitario
            ptax_unit_um_cost_unit = calculateTax(tax_rates[pr_tax], unit_um_cost_unit, item.row.tax_method);
            unit_um_cost_unit_net = formatDecimal(unit_um_cost_unit) - formatDecimal(ptax_unit_um_cost_unit[0]);


            unit_um_cost_total_unit = formatDecimal(pcost_um_total / (punit_quantity * pquantity)); //costo UNIDAD por um total
            ptax_unit_um_cost_total_unit = calculateTax(tax_rates[pr_tax], unit_um_cost_total_unit, item.row.tax_method);
            unit_um_cost_total_unit_net = formatDecimal(unit_um_cost_total_unit) - formatDecimal(ptax_unit_um_cost_total_unit[0]);
            //costo Unidad por um unitario con iva
            if (pcost != unit_um_cost_unit) {
                $('#pcost').val(formatDecimals(unit_um_cost_unit)).trigger('change');
                $('#pcost_net').val(formatDecimals(unit_um_cost_unit_net));
                $('#pcost_um_total').val(formatDecimals((unit_um_cost_unit * punit_quantity) * pquantity));
                $('#pcost_um_total_net').val(formatDecimals((unit_um_cost_unit_net * punit_quantity) * pquantity));
                $('#pcost_um_net').val(formatDecimals(punit_quantity * unit_um_cost_unit_net));
            } else if (pcost != unit_um_cost_total_unit) {
                $('#pcost').val(formatDecimals(unit_um_cost_total_unit)).trigger('change');
                $('#pcost_net').val(formatDecimals(unit_um_cost_total_unit_net)).trigger('change');
                $('#pcost_um').val(formatDecimals(punit_quantity * unit_um_cost_total_unit));
                $('#pcost_um_net').val(formatDecimals(punit_quantity * unit_um_cost_total_unit_net));
                $('#pcost_um_total_net').val(formatDecimals((unit_um_cost_total_unit_net * punit_quantity) * pquantity));
            }

        }
    });
    
    $(document).on('change', '#pcost_net', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var net_cost = parseFloat($('#pcost_net').val());
        var item = toitems[item_id];
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
        var ipoconsumo_calculated = item.row.purchase_tax_rate_2_percentage ? calculate_second_tax(item.row.purchase_tax_rate_2_percentage, net_cost, 0, item.row.tax_method) :  [0, item.row.consumption_purchase_tax];
        var ipoconsumo =  ipoconsumo_calculated[1];
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0;
        pr_tax_val = calculateTax(tax_rates[pr_tax], net_cost, 1);
        pr_tax_val = pr_tax_val[0];
        unit_cost = formatDecimal(net_cost) + formatDecimal(pr_tax_val) + formatDecimal(ipoconsumo);
        $('#pcost').val(formatDecimals(unit_cost));
        $('#net_cost').text(formatMoney(net_cost));
        $('#ptax2').val(formatDecimals(ipoconsumo));
        setTimeout(function() {
            $('#pcost_ipoconsumo').val('');
        }, 500);
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(ipoconsumo)));
    });

    $(document).on('change', '#pcost, #pcost_ipoconsumo, #ptax, #ptax_2, #ptax_value, #ptax_2_value, #pquantity', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var unit_cost = parseFloat($('#pcost').val());
        var pcost_ipoconsumo = parseFloat($('#pcost_ipoconsumo').val());
        var item = toitems[item_id];
        if (unit_cost != item.row.cost) {
            Command: toastr.warning('El costo ha sido cambiado', 'Costo cambió', {onHidden : function(){}})
        }
        if (item.row.purchase_tax_rate_2_percentage) {
            ipoconsumo_percentage_calculated =  calculate_second_tax(item.row.purchase_tax_rate_2_percentage, unit_cost, item.row.tax_rate, item.row.tax_method, true);
            $('#pcost_net').val(ipoconsumo_percentage_calculated[0]).trigger('change');
            return true;
        }
        var ipoconsumo = item.row.consumption_purchase_tax;
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
        unit_cost -= ipoconsumo;
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal((((unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_cost -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((unit_cost) * parseFloat(this.rate)) / 100));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }
        
        if ($('#pcost_ipoconsumo').val() > 0 && item.row.tax_method == 1  && parseFloat($('#pcost_ipoconsumo').val()) > parseFloat(ipoconsumo)) {
            $('#pcost').val(formatDecimal(unit_cost));
        } else if ($('#pcost_ipoconsumo').val() > 0 && item.row.tax_method == 0  && parseFloat($('#pcost_ipoconsumo').val()) > parseFloat(ipoconsumo)) {
            $('#pcost').val(formatDecimal(pcost_ipoconsumo));
        } else if (parseFloat($('#pcost_ipoconsumo').val()) <= parseFloat(ipoconsumo)) {
            Command: toastr.error('Por favor ingrese un costo válido', 'Valores incorrectos', {onHidden : function(){}})
            $('#pcost_ipoconsumo').val(0);
        }
        $('#pcost_net').val(formatDecimals(unit_cost));
        $('#net_cost').text(formatMoney(unit_cost));
        $('#ptax2').val(formatDecimals(ipoconsumo));
        setTimeout(function() {
            $('#pcost_ipoconsumo').val('');
        }, 500);
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(ipoconsumo)));

        if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            qty = $('#pquantity').val();
            units = item.units;
            unit_selected = units[$('#punit').val()];
            $('#pcost_um').val(formatDecimal((unit_cost+pr_tax_val) * unit_selected.operation_value));
            $('#pcost_um_total').val(formatDecimal(qty * ((unit_cost+pr_tax_val) * unit_selected.operation_value)));
        }

        if (item.row.cnt_units_prices !== undefined && item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 10) && item.row.promotion != 1) {
            if (item.units !== undefined) {
                sale_consumption_tax = 0;
                if (site.settings.ipoconsumo) {
                    sale_consumption_tax = item.row.consumption_sale_tax;
                }
                $('.pgprice_div').fadeOut();
                $.each(item.units, function(index, unit){
                    $('.pgprice_div[data-pgid="'+index+'"]').fadeIn();

                    if (site.settings.precios_por_unidad_presentacion == 2 && sale_consumption_tax > 0) {
                       if (isset(unit.operator) && unit.operator) {
                           if (unit.operator == "*") {
                               sale_consumption_tax = sale_consumption_tax * unit.operation_value;
                           } else if (unit.operator == "/") {
                               sale_consumption_tax = sale_consumption_tax / unit.operation_value;
                           }
                       }
                    }
                    $('.pgmargin[data-pgid="'+index+'"]').val(formatDecimal(unit.margin_update_price));
                    if (site.settings.update_prices_by_margin_formula == 1) {
                        precio = (unit_cost+pr_tax_val) / (1 - (unit.margin_update_price / 100));
                    } else {
                        precio = (unit_cost+pr_tax_val) * (1 + (unit.margin_update_price / 100));
                    }
                    $('.pgnew_price[data-pgid="'+index+'"]').val(formatDecimal(precio));
                });
            }
        }
    });
    $(document).on('change', '#punit', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = toitems[item_id];
        unit_cost = $('#pcost').val();
        unit = $('#punit').val();
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_purchase_tax));
        if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected = item.units[unit];
            var ipoconsumo_um = 0;
            if (unit_selected.operation_value > 1) {
                if (unit_selected.operator == "*") {
                    unit_cost =  parseFloat(unit_cost) * parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) * parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    unit_cost =  parseFloat(unit_cost) / parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) * parseFloat(unit_selected.operation_value);
                }
            }
            $('#pproduct_unit_cost').val(parseFloat(unit_cost) + parseFloat(ipoconsumo_um));
            $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
            $('.um_name').html(unit_selected.name);
            setTimeout(function() {
                $('#pproduct_unit_cost').trigger('change')
            }, 850);
        }
        $('.purchase_form_edit_product_unit_name').text($('#punit option:selected').text());
    });
    $(document).on('change', '#pproduct_unit_cost', function(){
        p_unit_cost = $(this).val();
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = toitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), acost = 0;
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_purchase_tax));
        if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 1) {
                valor = unit_selected.operation_value;
                if (unit_selected.operator == "*") {
                    valor =  parseFloat(p_unit_cost) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  parseFloat(p_unit_cost) * parseFloat(unit_selected.operation_value);
                }
                $('#pcost').val(valor).trigger('change');
            } else {
                $('#pcost').val(p_unit_cost).trigger('change');
            }
        }
    });
    $(document).on('click', '#calculate_unit_price', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = toitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        tax_rate = formatDecimal($('#ptax option:selected').data('taxrate')) / 100;
        tax_rate = tax_rate != 0 ? tax_rate + 1 : 1;
        var subtotal = parseFloat($('#psubtotal').val()),
        qty = parseFloat($('#pquantity').val());
        if (item.row.tax_method == 1) { tax_rate = 1; }
        $('#pcost').val(formatDecimal(((subtotal) * tax_rate))).change();
        return false;
    });


/* -----------------------
 * Edit Row Method
 ----------------------- */
$(document).on('click', '#editItem', function () {

    var trmrate = 1;
    if (othercurrency = localStorage.getItem('othercurrency')) {
        rate = localStorage.getItem('othercurrencyrate');
        if (trm = localStorage.getItem('othercurrencytrm')) {
            trmrate = rate / trm;
        }
    }
    var row = $('#' + $('#row_id').val());
    var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_rate = {};
    if (new_pr_tax) {
        $.each(tax_rates, function () {
            if (this.id == new_pr_tax) {
                new_pr_tax_rate = this;
            }
        });
    }
    if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0 || parseFloat($('#pcost').val()) < 0) {
        $(this).val(old_row_qty);
        bootbox.alert(lang.unexpected_value);
        return;
    }
    var unit = $('#punit').val();
    var base_quantity = parseFloat($('#pquantity').val());
    if(unit != toitems[item_id].row.base_unit) {
        $.each(toitems[item_id].units, function(){
            if (this.id == unit) {
                base_quantity = unitToBaseQty($('#pquantity').val(), this);
            }
        });
    }
    tpax2 = parseFloat($('#ptax2').val() ? $('#ptax2').val() : 0);
    toitems[item_id].row.fup = 1,
    toitems[item_id].row.qty = parseFloat($('#pquantity').val()),
    toitems[item_id].row.consumption_purchase_tax = tpax2,
    toitems[item_id].row.base_quantity = parseFloat(base_quantity),
    toitems[item_id].row.real_unit_cost = parseFloat(($('#pcost').val()-parseFloat(tpax2)) / trmrate),
    toitems[item_id].row.tax_rate = new_pr_tax,
    toitems[item_id].tax_rate = new_pr_tax_rate,
    toitems[item_id].row.discount = $('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '0',
    toitems[item_id].row.expiry = $('#pexpiry').val() ? $('#pexpiry').val() : '';
    toitems[item_id].row.serial = $('#pserial').val();
    toitems[item_id].row.product_unit_id_selected = $('#punit').val();
    if (site.settings.product_variant_per_serial == 0) {
        toitems[item_id].row.option = $('#poption').val();
    }
    if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
        var ditem = toitems[item_id];
        unit_selected = ditem.units[unit];
        qty = parseFloat($('#pquantity').val());
        unit_selected_quantity = qty;
        if (unit_selected.operator == '*') {
            unit_selected_quantity = qty * unit_selected.operation_value;
        } else if (unit_selected.operator == '/') {
            unit_selected_quantity = qty / unit_selected.operation_value;
        } else if (unit_selected.operator == '+') {
            unit_selected_quantity = qty + unit_selected.operation_value;
        } else if (unit_selected.operator == '-') {
            unit_selected_quantity = qty - unit_selected.operation_value;
        }
        toitems[item_id].row.qty = unit_selected_quantity;
    } else {
        toitems[item_id].row.unit_price_id = $('#punit').val();
    }
    if (item.row.cnt_units_prices !== undefined && item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && item.row.promotion != 1) {
        if (toitems[item_id].units !== undefined) {
            $.each($('.pgprice'), function(index, input_unit){
                if (toitems[item_id].units[$(input_unit).data('pgid')]) {
                    toitems[item_id].units[$(input_unit).data('pgid')].margin_update_price = $('.pgmargin[data-pgid="'+$(input_unit).data('pgid')+'"]').val();
                    toitems[item_id].units[$(input_unit).data('pgid')].valor_unitario = $('.pgnew_price[data-pgid="'+$(input_unit).data('pgid')+'"]').val();
                }
            });
        } else {
            $('.div_update_prices').fadeOut();
        }
    } else {
        if (toitems[item_id].product_price_groups !== undefined  && site.settings.update_prices_from_purchases == 1) {
            $.each($('.pgprice'), function(index, input_unit){
                if (toitems[item_id].product_price_groups[$(input_unit).data('pgid')]) {
                    toitems[item_id].product_price_groups[$(input_unit).data('pgid')].price = $(input_unit).val();
                }
            });
        } else {
            $('.div_update_prices').fadeOut();
        }
    }
    localStorage.setItem('toitems', JSON.stringify(toitems));
    $('#prModal').modal('hide');
    loadItems();
    if ($('#prorate_shipping_cost').val() == 1 && localStorage.getItem('poshipping')) {
        setTimeout(function() {
            loadItems();
        }, 1000);
    }
    return;
});

/* -----------------------
 * Misc Actions
 ----------------------- */
//aca load
function loadItems(set_focus = true) {
    if (localStorage.getItem('toitems')) {
        count = 1;
        arrCount = [];
        general_item_count = 1;
        localStorage.removeItem('locked_by_tax_different_original');
        var total = 0;
        var subtotal = 0;
        var subtotal_edit = 0;
        var totaltax = 0;
        var total_ipoconsumo = 0;
        var an = 1;
        var product_tax = 0;
        var product_tax_2 = 0;
        var invoice_tax = 0;
        var product_discount = 0;
        var order_discount = 0;
        var total_discount = 0;
        var posubtotal = 0;
        var pogtotal = 0;
        var poshipping_total = 0;
        var edit_date = true;
        var lock_by_item_quantity = false;
        $("#toTable tbody").empty();
        toitems = JSON.parse(localStorage.getItem('toitems'));
        sortedItems = _.sortBy(toitems, function(o){return [parseInt(o.order)];});
        if (site.settings.product_order == 2) {
            sortedItems.reverse();
        }
        var order_no = new Date().getTime();
        _restrict_permission_cost = (is_admin || is_owner || user_permissions.products_cost == 1) ? '' : 'style=" display:none;"';
        $.each(sortedItems, function (index) {
            var item = this; 
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order :  new Date().getTime();
            var product_id = item.row.id, item_type = item.row.type, combo_items = item.combo_items, item_cost = item.row.cost, item_oqty = item.row.oqty, item_qty = item.row.qty, item_bqty = item.row.quantity_balance, item_expiry = item.row.expiry, item_tax_method = item.row.tax_method, item_ds = item.row.discount, item_discount = 0, item_shipping = 0, item_option = item.row.option, item_code = item.row.code, item_name = item.row.name !== undefined ? item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;") : item.row.product_name, item_serial = item.row.serial;
            var qty_received = (item.row.received >= 0) ? item.row.received : item.row.qty;
            var item_supplier_part_no = item.row.supplier_part_no ? item.row.supplier_part_no : '';
            var unit_cost = item.row.real_unit_cost;
            var product_unit = item.row.product_unit_id_selected, base_quantity = item.row.base_quantity;
            var tax_rate = item.row.tax_rate;
            var supplier = localStorage.getItem('posupplier'), belong = false;
            var item_ds_2 = 0;
            var units = [];
            var item_unit_selected =  item.row.product_unit_id_selected != null ?  item.row.product_unit_id_selected : item.row.unit;
            edit_item = true;
            var unit_qty_received = qty_received;
            pr_t = {id : site.except_tax_rate_id, rate : 0};
            incorrect_tax_rate = false;
            if (item.tax_rate) {
                var pr_tax = item.tax_rate;
            } else {
                var pr_tax = pr_t;
                incorrect_tax_rate = true;
            }
            var pr_ptev_tax_val = pr_tax_val = pr_tax_rate = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_cost, item_tax_method))) {
                pr_ptev_tax_val = pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
            }
            var pr_tax_2 = item.tax_rate_2;
            var pr_ptev_tax_2_val = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax_2, unit_cost, item_tax_method))) {
                pr_ptev_tax_2_val = ptax[0];
            }
            var pr_tax_2_val = 0;
            var ds = item_ds ? item_ds : '0';
            var ds_2 = item_ds_2 ? item_ds_2 : '0';
            prorated_cost_discount = item.row.tax_method == 1 ?
                parseFloat(unit_cost) :
                (parseFloat(unit_cost) - (parseFloat(pr_ptev_tax_val) + parseFloat(pr_ptev_tax_2_val)));
            item_discount = pcalculateDiscount(ds, ds_2, prorated_cost_discount, posubtotal, item_qty);
            product_discount += parseFloat(item_discount * item_qty);
            unit_cost = formatDecimal(unit_cost-item_discount);
            if (item.row.purchase_tax_rate_2_percentage) {
                net_cost = (parseFloat(unit_cost) - (parseFloat(pr_ptev_tax_val) + parseFloat(pr_ptev_tax_2_val)));
                ipoconsumo_percentage_calculated =  calculate_second_tax(item.row.purchase_tax_rate_2_percentage, net_cost);
                pr_tax_2_val = ipoconsumo_percentage_calculated[1];
            }
            var pr_tax = item.tax_rate;
            var pr_tax_val = pr_tax_rate = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_cost - (item_tax_method == 1 ? 0 : pr_ptev_tax_val), 1))) {
                pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
                product_tax += pr_tax_val * item_qty;
            }
            var pr_tax_2 = item.tax_rate_2;
            var pr_tax_2_rate = 0;
            item_cost = item_tax_method == 0 ? formatDecimal(unit_cost-((parseFloat(pr_ptev_tax_val)+parseFloat(pr_ptev_tax_2_val)))) : formatDecimal(unit_cost);
            item_cost = item_cost <= 0 ? 0 : item_cost;
            unit_cost = formatDecimal(unit_cost+item_discount);
            var sel_opt = '';
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.code;
                }
            });
            var cost = unit_cost;
            var tax_method = item_tax_method;
            var margin = 0;
            var pr_tax = tax_rates[tax_rate];
            ipoconsumo = false;
            pr_tax_2_val = pr_tax_2_val == 0 ? formatDecimal(item.row.consumption_purchase_tax ? item.row.consumption_purchase_tax : 0) : pr_tax_2_val;
            pr_tax_2_rate = item.row.purchase_tax_rate_2_percentage ? item.row.purchase_tax_rate_2_percentage : pr_tax_2_val;
            pr_tax_2_rate_id = item.row.purchase_tax_rate_2_id;
            product_tax_2 += pr_tax_2_val * item_qty;
            ipoconsumo = true;
            var opt = $("<select id=\"poption\" name=\"variant\[\]\" class=\"form-control select rvariant\" />");
            if(item.options !== false && site.settings.product_variant_per_serial == 0) {
                $.each(item.options, function () {
                    if (item.row.option == this.id)
                        $("<option />", {value: this.id, text: this.name, selected: 'selected'}).appendTo(opt);
                    else
                        $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                });
            } else {
                $("<option />", {value: 0, text: 'n/a'}).appendTo(opt);
                opt = opt.hide();
            }
            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                        '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                        '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                        '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                        '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                        '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                        '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name+' '+ (sel_opt ? ' ('+sel_opt+')' : '') +
                        (edit_item ? '</span> <i class="pull-right fa fa-edit tip edit" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i>' : '')+
                        (item.row.op_pending_quantity !== undefined && item.row.op_pending_quantity > 0 ? ' <i class="fa-solid fa-info-circle"  data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+
                                "En stock : "+formatDecimal((parseFloat(item.row.op_pending_quantity)+parseFloat(item.row.quantity)))+" | "+
                                "Pendientes orden de pedido : "+formatDecimal(item.row.op_pending_quantity)+" | "+
                                "Disponibles : "+formatDecimal(item.row.quantity)+
                            '"'+'"></i>': "");
            tr_html += '</td>';
            if (site.settings.product_variant_per_serial == 1) {
                tr_html += '<td>'+
                                (item.row.serialModal_serial !== undefined ? item.row.serialModal_serial : '')+
                            '</td>';
            }
            tr_html += '<td class="thead_variant">'+(opt.get(0).outerHTML)+'</td>';
            if (((site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10)  ) ) {
                unit_data_selected = item.units[item_unit_selected];
                tr_html += '<td>'+
                            '<input type="hidden" name="product_unit_id_selected[]" value="'+item_unit_selected+'">'+
                            '<span>'+unit_data_selected.code+'</span>'+
                          '</td>'; //nombre unidad

                item_unit_quantity = item_qty;
                item_unit_cost = item_cost;
                if (unit_data_selected.operator == '*') {
                    item_unit_quantity = item_qty / unit_data_selected.operation_value;
                    item_unit_cost = item_cost * unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '/'){
                    item_unit_quantity = item_qty * unit_data_selected.operation_value;
                    item_unit_cost = item_cost / unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '+'){
                    item_unit_quantity = item_qty - unit_data_selected.operation_value;
                } else if (unit_data_selected.operator == '-'){
                    item_unit_quantity = item_qty + unit_data_selected.operation_value;
                }

                tr_html += '<td>'+
                            '<span>'+formatQuantity(item_unit_quantity)+'</span>'+
                          '</td>'; //cantidad de la unidad
                tr_html += '<td>'+
                            '<span>'+formatMoney(item_unit_cost)+'</span>'+
                          '</td>'; // costo de la unidad
            }
            if (site.settings.product_serial == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'">'+
                            '</td>';
            }
            if (site.settings.product_expiry == 1) {
                tr_html += '<td>'+
                              '<input class="form-control date rexpiry" name="expiry[]" type="text" value="' + item_expiry + '" data-id="' + row_no + '" data-item="' + item_id + '" id="expiry_' + row_no + '">'+
                            '</td>';
            }
            tr_html += '<td class="text-right thead_discount">'+
                            '<span>'+formatMoney((item_cost + item_discount))+'</span>'+
                        '</td>';
            if (site.settings.product_discount == 1) {
                tr_html += '<td class="text-right thead_discount">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + (item_ds != 0 ? item_ds : item_ds_2) + '">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount_val[]" type="hidden" id="discount_val_' + row_no + '" value="' + formatDecimal(0 - (item_discount * item_qty)) + '">'+
                                '<input class="form-control input-sm rdiscount" name="product_unit_discount_val[]" type="hidden" id="unit_discount_val_' + row_no + '" value="' + formatDecimal(item_discount) + '">'+
                                '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + formatMoney((0 - (item_discount * item_qty))) + '</span>'+
                           '</td>';
            }
            tr_html += '<td class="text-right"'+ _restrict_permission_cost +'>'+
                            '<input class="form-control input-sm text-right rcost" name="net_cost[]" type="hidden" id="cost_' + row_no + '" value="' + item_cost + '">'+
                            '<input class="rucost" name="unit_cost[]" type="hidden" value="' + unit_cost + '">'+
                            '<input class="realucost" name="real_unit_cost[]" type="hidden" value="' + item.row.real_unit_cost + '">'+
                            '<input class="rpnet_cost form-control text-right" value="'+formatDecimals((parseFloat(item_cost)))+'">'+
                       '</td>';
            if (is_edit) {
                tr_html += '<td class="rec_con">'+
                                '<input name="purchase_item_id[]" type="hidden" value="'+item.row.purchase_item_id+'">'+
                                '<input name="edit_item[]" type="hidden" value="'+edit_item+'">'+
                                '<input name="ordered_quantity[]" type="hidden" class="oqty" value="' + (item_oqty ? item_oqty : 0) + '">'+
                                '<input class="form-control text-center received" name="received[]" type="text" value="' + formatDecimal(unit_qty_received) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="received_' + row_no + '" onClick="this.select();" readonly>'+
                                '<input name="received_base_quantity[]" type="hidden" class="rrbase_quantity" value="' + qty_received + '">'+
                           '</td>';
            }
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right"'+ _restrict_permission_cost +'>'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + (pr_tax ? pr_tax.id : null) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax' + row_no + '" value="' + pr_tax_val + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (pr_tax_rate ? '(' + pr_tax_rate + ')' : '') + ' ' + formatMoney((pr_tax_val)) + '</span>';
                if (incorrect_tax_rate) {
                    tr_html += "  <span class='text-warning' style='font-weight:600;'>"+lang.incorrect_tax+' <i class="fa fa-exclamation-triangle"></i></span>';
                }
                tr_html += '</td>';
            }
            if (site.settings.ipoconsumo == 2 || site.settings.ipoconsumo == 1) {
                tr_html += '<td class="text-right"'+ _restrict_permission_cost +' >'+
                                '<input class="form-control input-sm text-right rproduct_tax_2" name="product_tax_2[]" type="hidden" id="product_tax_2_' + row_no + '" value="' + (ipoconsumo ? pr_tax_2_rate_id : pr_tax_2.id) + '">'+
                                '<input class="form-control input-sm text-right" name="profitability_margin[]" type="hidden" id="profitability_margin_' + row_no + '" value="' + margin + '">'+
                                '<span class="text-right sproduct_tax_2" id="sproduct_tax_2_' + row_no + '">' + (pr_tax_2_rate ? '(' + pr_tax_2_rate + ')' : '') + ' ' + formatMoney((pr_tax_2_val * item_qty)) + '</span>'+
                                '<input class="form-control input-sm text-right runit_product_tax_2" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_2_' + row_no + '" value="' + pr_tax_2_val + '">'+
                                '<input class="form-control input-sm text-right runit_product_tax_2_percentage" name="unit_product_tax_2_percentage[]" type="hidden" id="unit_product_tax_2_percentage_' + row_no + '" value="' + pr_tax_2_rate + '">'+
                            '</td>';
            }
            tr_html += '<td class="text-right"' + _restrict_permission_cost + '>'+
                            '<input class="rpcost form-control text-right" value="'+formatDecimals((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)))+'">'+
                        '</td>';
            tr_html += '<td class="text-center thead_shipping">'+
                            formatMoney(0)+
                            '<input type="hidden" name="shipping_unit_cost[]" value="'+0+'">'+
                        '</td>';
            tr_html += '<td>'+
                            '<input name="quantity_balance[]" type="hidden" class="rbqty" value="' + item_bqty + '">'+
                            '<input class="form-control text-center rquantity" name="quantity[]" type="text" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" value="' + item_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+(!edit_item ? 'readonly' : '')+' >'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                            '<input name="product_purchase_unit[]" type="hidden" class="runit" value="' + item.row.base_unit + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + item_qty + '">'+
                            '<input name="serialModal_serial[]" type="hidden" value="' + (item.row.serialModal_serial ? item.row.serialModal_serial : '')+ '">'+
                       '</td>';
            tr_html += '<td class="text-right" ' +_restrict_permission_cost+ '>'+
                            '<span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty))) + '</span>'+
                        '</td>';
            tr_html += '<td class="text-center">'+
                            (edit_item ? '<i class="fa fa-times tip todel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i>' : '')+
                        '</td>';
            newTr.html(tr_html);
            poshipping_total = 0;
            if (is_edit && site.settings.product_order == 2) {
                newTr.prependTo("#toTable");
            } else {
                newTr.appendTo("#toTable");
            }
            subtotal += formatDecimal(((parseFloat(item_cost)) * parseFloat(item_qty)));
            subtotal_edit += (edit_item ? (formatDecimal(((parseFloat(item_cost)) * parseFloat(item_qty)))) : 0);
            totaltax += formatDecimal(((parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            total_ipoconsumo += formatDecimal(((parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            total += formatDecimal(((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            count += parseFloat(item_qty);
            arrCount[index] = parseFloat(item_qty);
            if (site.settings.overselling == 0 && (parseFloat(item_qty) > parseFloat(item.row.quantity))) {
                $('#row_' + row_no).addClass('danger');
                lock_by_item_quantity = true;
            }

            general_item_count++;
            an++;
        });
        var col = 1;
        if ((site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10)  ) {
            col = 4;
        }
        if (site.settings.product_variant_per_serial == 1) {
            col++;
        }
        if (site.settings.product_expiry == 1) { col++; }
        if (site.settings.product_serial == 1) { col++; }
        var tfoot = '<tr id="tfoot" class="tfoot active">';
        tfoot += '<th colspan="'+col+'">Total ('+(an - 1)+')</th>';
        tfoot += '<th></th>';
        tfoot += '<th class="text-right thead_discount">'+formatMoney((product_discount + subtotal))+'</th>'; //aca total bruto
        if (site.settings.product_discount == 1) {
            tfoot += '<th class="text-right thead_discount">'+formatMoney(product_discount)+'</th>';
        }
        tfoot += '<th class="text-right" id="subtotalP" '+_restrict_permission_cost+'>'+formatMoney(subtotal)+'</th>';
        if (psubtotal = localStorage.getItem('posubtotal')) {
            localStorage.removeItem('posubtotal');
        }
        if (is_edit) {
            localStorage.setItem('posubtotal', subtotal_edit+product_discount);
        } else {
            localStorage.setItem('posubtotal', subtotal+product_discount);
        }
        if (is_edit) {
            tfoot += '<th class="rec_con"></th>';
        }
        if (site.settings.tax1 == 1) {
            tfoot += '<th class="text-right" id="ivaamount" ' + _restrict_permission_cost + '>'+formatMoney(product_tax)+'</th>';
        }
        if (site.settings.ipoconsumo == 2 || site.settings.ipoconsumo == 1) {
            tfoot += '<th class="text-right" id="ivaamount" ' + _restrict_permission_cost + '>'+formatMoney(product_tax_2)+'</th>';
        }
        let sumCount = arrCount.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
        tfoot += '<th class="text-right" ' +_restrict_permission_cost+ '>'+formatMoney((product_tax + product_tax_2 + subtotal))+'</th>'+
                '<th class="thead_shipping">'+formatMoney(poshipping_total)+'</th>'+
                '<th class="text-center">' + formatQty(parseFloat(sumCount)) + '</th>'+
                '<th class="text-right" id="totalP" ' +_restrict_permission_cost+ '>'+formatMoney((total + poshipping_total))+'</th>'+
                '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
            '</tr>';
        $('#toTable tfoot').html(tfoot);
        if (product_discount > 0) {
            $('.thead_discount').fadeIn();
        } else {
            $('.thead_discount').fadeOut();
        }
        if (poshipping_total > 0) {
            $('.thead_shipping').fadeIn();
        } else {
            $('.thead_shipping').fadeOut();
        }
        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (potax2 = localStorage.getItem('potax2')) {
                $.each(tax_rates, function () {
                    if (this.id == potax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        }
                        if (this.type == 1) {
                            if (descuento_orden == 1) { //Descuento afecta IVA
                                invoice_tax = formatDecimal((((subtotal - order_discount) * this.rate) / 100));
                            } else if (descuento_orden == 2) {//Descuento NO afecta iva
                                invoice_tax = formatDecimal((((subtotal + product_discount) * this.rate) / 100));
                            }
                        }
                    }
                });
            }
        }
        // poretenciones
        if (poretenciones = JSON.parse(localStorage.getItem('poretenciones'))) {
            total_rete_amount = formatDecimal(poretenciones.total_rete_amount);
        } else {
            total_rete_amount = 0;
        }
        //poretenciones
        total_discount = parseFloat(order_discount + product_discount);
        var gtotal = ((parseFloat(total) + parseFloat(invoice_tax)) - parseFloat(order_discount)) + parseFloat(shipping);
        $('#amount_1').data('maxval', gtotal).prop('readonly', false);
        total_rete_amount = 0;
        if (poretenciones = JSON.parse(localStorage.getItem('poretenciones'))) {
            total_rete_amount = formatDecimal(parseFloat(poretenciones.total_rete_amount));
        }
        $('#amount_1').val(formatDecimal((gtotal - total_rete_amount), 2));
        $('#total').text(formatMoney(total));
        $('#titems').text((an-1)+' ('+(formatQty(parseFloat(count) - 1))+')');
        $('#tds').text(formatMoney(order_discount));
        if (site.settings.tax1) {
            $('#ttax1').text(formatMoney(product_tax));
        }
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(invoice_tax));
        }
        $('#gtotal').text(formatMoney(gtotal + poshipping_total));

        $('#total_ipoconsumo').text(formatMoney(total_ipoconsumo));
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        if (site.settings.ignore_purchases_edit_validations == 0 && (!edit_date)) {
            $('#podate').prop('readonly', true);
        }
        // if (set_focus == true) {
        //     set_page_focus();
        // }
        if (site.settings.product_order == 2) {
            an = 2;
        }
        // if (set_focus == true) {
            set_page_focus(an);
        // }
        $('.paid_by').trigger('change');
        setTimeout(function() {
        }, 2000);
        $('[data-toggle="tooltip"]').tooltip();
        if (lock_by_item_quantity) {
            $('#add_transfers').prop('disabled', true);
        } else {
            $('#add_transfers').prop('disabled', false);
        }
        if (gtotal > 0 && localStorage.getItem('supplier_deposit_balance') && deposit_advertisement_showed == false) {
            setTimeout(function() {
                deposit_advertisement_showed = true;
                deposit_amount = parseFloat(localStorage.getItem('supplier_deposit_balance'));
                if (deposit_amount > 0 && deposit_payment_method.purchase == true && edit_paid_by != 'deposit') {
                    bootbox.confirm({
                        message: "El proveedor cuenta con "+formatMoney(deposit_amount)+" de saldo de anticipos, ¿Desea usar este medio de pago para esta compra?",
                        buttons: {
                            confirm: {
                                label: 'Si, usar anticipos',
                                className: 'btn-success send-submit-sale btn-full'
                            },
                            cancel: {
                                label: 'No, usar otro medio de pago',
                                className: 'btn-danger btn-full'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                gtotal = parseFloat(formatDecimal($('#totalP').text()));
                                if (gtotal > deposit_amount) {
                                    $('#amount_1').val(deposit_amount);
                                }
                               $('#paid_by_1').select2('val', 'deposit').trigger('change');
                            }
                        }
                    });
                }
            }, 1500);
        }
    }
}

/* -----------------------------
 * Add Purchase Iten Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
 function add_transfer_item(item, modal = false, cont = false) {

    if (count == 1) {
        toitems = {};
        if ($('#from_warehouse').val()) {
            // $('#from_warehouse').select2("readonly", true);
        } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }
    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (toitems[item_id]) {

        var new_qty = parseFloat(toitems[item_id].row.qty) + 1;
        toitems[item_id].row.base_quantity = new_qty;
        if(toitems[item_id].row.unit != toitems[item_id].row.base_unit) {
            $.each(toitems[item_id].units, function(){
                if (this.id == toitems[item_id].row.unit) {
                    toitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        toitems[item_id].row.qty = new_qty;

    } else {
        toitems[item_id] = item;
    }
    toitems[item_id].order = new Date().getTime();

    if (site.settings.product_variant_per_serial == 1 && modal === false && item.row.attributes == 1) {
        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(toitems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal_option_id').val('');
        $('#serialModal').modal('show');
        localStorage.setItem('modalSerial_item', JSON.stringify(toitems[item_id]));
        delete toitems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
        }, 850);
    } else {
        if (site.settings.product_variant_per_serial == 1) {
            localStorage.removeItem('modalSerial_item');
            if (site.settings.product_variant_per_serial == 1 && cont == false) {
                $('#serialModal').modal('hide');
                $('#add_item').val('').focus();
            } else {
                item = toitems[item_id];
                $.ajax({
                    url : site.base_url + "products/get_random_id"
                }).done(function(data){
                    item.id = data;
                    console.log(item);
                    localStorage.setItem('modalSerial_item', JSON.stringify(item));
                    $('#serialModal_serial').val('');
                    $('#serialModal_meters').val('');
                });
            }
        }
        localStorage.setItem('toitems', JSON.stringify(toitems));
        loadItems();
        
        setTimeout(function() {
            if (site.pos_settings.show_variants_and_preferences == 1) {
                if (toitems[item_id].row.variant_selected === undefined) {
                    $('#row_'+item_id).find('.sname').trigger('click');
                }
            }
        }, 1500);
        return true;
    }
}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}

///SERIAL MODAL


$(document).on('click', '.send_serial_modal', function(){
    validar_modal_serial();
});

$(document).on('click', '.continue_serial_modal', function(){
    validar_modal_serial(true);
});


function validar_modal_serial(cont = false){
    if ($('#serialModal_form').valid()) {
        serial = $('#serialModal_serial').val();
        meters = $('#serialModal_meters').val();
        item_id = $('#serialModal_product_id').val();
        option_id = $('#serialModal_option_id').val();
        item = JSON.parse(localStorage.getItem('modalSerial_item'));
        item.row.serialModal_serial = serial;
        item.row.qty = meters;
        item.row.quantity = meters;
        item.row.base_quantity = meters;
        item.row.delivered_qty = 0;
        item.row.pending_qty = meters;
        item.row.bill_qty = 0;
        item.row.option = option_id;
        item.row.wh_quantity = formatDecimal($('#serialModal_meters').prop('max'));
        add_transfer_item(item, true, cont);
    }
}

$(document).on('keyup', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal, e);
});

$(document).on('change', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal);
});

$(document).on('keyup', '#serialModal_meters', function(e){
    if (e.keyCode == 13) {
        $('.send_serial_modal').trigger('click');
    }
});

function validate_serial(serialModal, e = null){
    if ( e == null || e != null && e.keyCode == 13) {
        serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
        from_warehouse = $('#from_warehouse').val();
        $.ajax({
            url : site.base_url + 'products/get_product_serial_variant_quantity/'+serial_item.row.id+'/'+serialModal+'/'+from_warehouse,
            dataType : 'JSON',
        }).done(function(data){
            if (data.id  !== undefined) {
                $('#serialModal_meters').val(data.quantity);
                $('#serialModal_meters').prop('max', data.quantity);
                $('#serialModal_option_id').val(data.option_id);
                $('#serialModal_meters').focus();
            } else {
                $('#serialModal_serial').val('');
                $('#serialModal_option_id').val('');
                 command: toastr.warning('El serial indicado no está registrado para este producto', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        });
    }
}

$(document).on('click', '#random_num', function(e){
    $('#serialModal_serial').select();
});

function check_submit_locked(){

    if (localStorage.getItem('submit_locked_by_quantity')) {
        $('#add_transfer').prop('disabled', true);
    } else {
        $('#add_transfer').prop('disabled', false);
    }

}

$(document).on('click', '.sname', function(e) {
    var row = $(this).closest('tr');
    var toitems_id = $(row).data('item-id');
    var itemid = row.find('.rid').val();

    if (site.settings.product_clic_action == 1) {
        $('#myModal').modal({remote: site.base_url + 'products/modal_view/' + itemid});
        $('#myModal').modal('show');
    } else if (site.settings.product_clic_action == 2) {
        // if (toitems[toitems_id].options && toitems[toitems_id].row.variant_selected === undefined) {
        if (toitems[toitems_id].options) {
            $('#myModal').modal({remote: site.base_url + 'transfers/product_variants_selection/' + itemid + "/" + toitems_id + "/"+$('#from_warehouse').val()});
            $('#myModal').modal('show');
        }
    } else if (site.settings.product_clic_action == 3) {
        row.find('.edit').trigger('click');
    }

});

$(document).on('click', '#add_transfers', function(){
    let submit = true;
    if (!$('#add_tranfers_form').valid()) {
        submit = false;
    }
    if (submit) {
        $('#add_tranfers_form').submit();
        $('#add_transfers').prop('disabled', true);
        $('#reset').prop('disabled', true);
        // remove_localstorage_data();
        setTimeout(function() {
            location.href = site.base_url+'transfers/index';
        }, 800);
    }
})

function remove_localstorage_data(){
    if (localStorage.getItem('toitems')) {
        localStorage.removeItem('toitems');
    }
    if (localStorage.getItem('toshipping')) {
        localStorage.removeItem('toshipping');
    }
    if (localStorage.getItem('toref')) {
        localStorage.removeItem('toref');
    }
    if (localStorage.getItem('to_warehouse')) {
        localStorage.removeItem('to_warehouse');
    }
    if (localStorage.getItem('tonote')) {
        localStorage.removeItem('tonote');
    }
    if (localStorage.getItem('from_warehouse')) {
        localStorage.removeItem('from_warehouse');
    }
    if (localStorage.getItem('todate')) {
        localStorage.removeItem('todate');
    }
    if (localStorage.getItem('tostatus')) {
        localStorage.removeItem('tostatus');
    }
    if (localStorage.getItem('tobiller_origin')) {
        localStorage.removeItem('tobiller_origin');
    }
    if (localStorage.getItem('tobiller_destination')) {
        localStorage.removeItem('tobiller_destination');
    }
    if (localStorage.getItem('companies_id')) {
        localStorage.removeItem('companies_id');
    }
    
}
