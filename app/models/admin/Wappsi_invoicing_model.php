<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Wappsi_invoicing_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function get_invoice_by_id($id){
        $q = $this->db->get_where('wappsi_invoicing', ['id'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
    public function get_invoice_by_reference_no($id){
        $q = $this->db->select('wi2.*')
                 ->join('wappsi_invoicing wi2', 'wi2.reference_no = wappsi_invoicing.reference_no')
                 ->where('wappsi_invoicing.id', $id)->get('wappsi_invoicing');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function get_electronic_hits_summary(){
        $sales = $this->db->where('tipo_registro', 1)->get('electronic_hits')->num_rows();
        $support_document = $this->db->where('tipo_registro', 3)->get('electronic_hits')->num_rows();
        $reception = $this->db->where('tipo_registro', 4)->get('electronic_hits')->num_rows();
        $payroll_hits = $this->db->where('tipo_registro', 2)->get('electronic_hits')->num_rows();
        $wappsi_invoicing = $this->db->where('tipo_mov', 4)->get('wappsi_invoicing');
        $available_electronic_hits = 0;
        if ($wappsi_invoicing->num_rows() > 0) {
            foreach (($wappsi_invoicing->result()) as $wi) {
                $available_electronic_hits += $wi->cantidad_unidad;
            }
        }
        return [
                'sales'=> $sales,
                'support_document'=> $support_document,
                'reception'=> $reception,
                'payroll_hits'=> $payroll_hits,
                'available_electronic_hits'=> $available_electronic_hits
            ];
    }

    public function sync_wappsi_invoicing($customer_id, $address_id = NULL, $sale_id = NULL){
        $customer = $this->site->getCompanyByID($customer_id);
        if ($address_id) {
            $address = $this->site->getAddressByID($address_id);
        }
        if ($sale_id && ($customer->customer_enabled_for_wappsi_invoicing == 0 || ($address_id && $address->customer_enabled_for_wappsi_invoicing == 0))) {
            return false;
        }
        $DBGLOBAL = $this->site->get_erp_db();
        $msg_type = 'error';
        $msg_text = '';
        if ($DBGLOBAL) {
            $db_exists = $DBGLOBAL->not_like('wappsi_customers_databases.database', 'prueba')->not_like('wappsi_customers_databases.database', 'demo')->where(['vat_no' => $customer->vat_no, 'wappsi_customer_address_id'=>$address_id])->get('wappsi_customers_databases');
            if ($db_exists->num_rows() > 0) {
                $db_customer = $db_exists->row();
                $wcdDB_q_new_config['hostname'] = $this->sma->wappsi_decrypt($db_customer->hostname, 'wappsi');
                $wcdDB_q_new_config['username'] = $this->sma->wappsi_decrypt($db_customer->username, 'wappsi');
                $wcdDB_q_new_config['password'] = $this->sma->wappsi_decrypt($db_customer->password, 'wappsi');
                $wcdDB_q_new_config['database'] = $db_customer->database;
                $wcdDB_q_new_config['dbdriver'] = $this->db->dbdriver;
                $wcdDB_q_new_config['dbprefix'] = 'sma_';
                $wcdDB_q_new_config['db_debug'] = $this->db->db_debug;
                $wcdDB_q_new_config['cache_on'] = $this->db->cache_on;
                $wcdDB_q_new_config['cachedir'] = $this->db->cachedir;
                $wcdDB_q_new_config['port']     = $this->db->port;
                $wcdDB_q_new_config['char_set'] = $this->db->char_set;
                $wcdDB_q_new_config['dbcollat'] = $this->db->dbcollat;
                $wcdDB_q_new_config['pconnect'] = $this->db->pconnect;
                if ($this->site->check_database($wcdDB_q_new_config)) {
                    $wcdDB = $this->load->database($wcdDB_q_new_config, TRUE);
                    $this->db->select('
                            products.wappsi_invoicing_type AS tipo_mov,
                            sale_items.product_name AS descripcion,
                            sale_items.quantity AS cantidad_facturada,
                            products.wappsi_invoicing_quantity AS cantidad_unidad,
                            sale_items.net_unit_price AS valor_neto,
                            CAST(COALESCE(('.$this->db->dbprefix('sale_items').'.item_tax / '.$this->db->dbprefix('sale_items').'.quantity), 0) AS DECIMAL(25,4)) AS valor_iva,
                            sale_items.unit_price AS valor_total,
                            products.wappsi_invoicing_frecuency AS frecuencia,
                            sales.reference_no AS reference_no,
                            sales.due_date AS vencimiento,
                            IF('.$this->db->dbprefix('sales').'.payment_status = "pending" OR '.$this->db->dbprefix('sales').'.payment_status = "due",
                                IF('.$this->db->dbprefix('sales').'.due_date < "'.date('Y-m-d H:i:s').'",
                                "3", 
                                "0")
                              ,
                            "1") AS estado_pago,
                            GROUP_CONCAT('.$this->db->dbprefix('payments').'.reference_no) AS pago_reference_no ,
                            MIN('.$this->db->dbprefix('payments').'.date) AS fecha_pago ,
                            sales.date AS fecha,
                            sales.id as sale_id
                        ')
                    ->join('sale_items', 'sale_items.sale_id = sales.id', 'inner')
                    ->join('products', 'products.id = sale_items.product_id', 'inner')
                    ->join('payments', 'payments.sale_id = sales.id', 'left')
                    ->where('sales.wappsi_invoicing_synchronized', 0)
                    ->where('sales.customer_id', $customer_id)
                    ->group_by('sale_items.id');
                    if ($sale_id) {
                        $this->db->where('sales.id', $sale_id);
                    }
                    if ($address_id) {
                        $this->db->where('sales.address_id', $address_id);
                    }
                    $products = $this->db->get('sales');
                    if ($products->num_rows() > 0) {
                        foreach (($products->result_array()) as $row) {
                            $this->db->update('sales', ['wappsi_invoicing_synchronized' => 1], ['id'=>$row['sale_id']]);
                            unset($row['sale_id']);
                            $wcdDB->insert('wappsi_invoicing',$row);
                        }
                        if (!$sale_id) {
                            $this->db->update('companies', ['customer_enabled_for_wappsi_invoicing' => 1], ['id'=>$customer_id]);
                        }
                        if ($address_id) {
                            $this->db->update('addresses', ['customer_enabled_for_wappsi_invoicing' => 1], ['id'=>$address_id]);
                        }
                        $msg_type = 'message';
                        $msg_text = 'Datos de facturación wappsi insertados con éxito al cliente seleccionado.';
                    } else {
                        $msg_text = 'No existe facturación WAPPSI pendiente para insertar en el cliente seleccionado.';
                    }
                } else {
                    $msg_text = 'No se pudo conectar a la Base de datos del cliente con los datos obtenidos de la tabla de bases de datos global.';
                }
            } else {
                $msg_text = 'El cliente seleccionado no es válido para enviar la información de facturación Wappsi por que no tiene datos de conexión en BD Global.';
            }
        } else {
            $msg_text = 'No se pudo conectar a la Base de datos GLOBAL.';
        }
        return ['msg_type'=>$msg_type, 'msg_text'=>$msg_text];
    }

    public function get_documents_types(){
        $q = $this->db->order_by('module', 'ASC')->get_where('documents_types', ['factura_electronica' => 1]);
        if ($q->num_rows() > 0) {
            $q->result_array();
            foreach (($q->result()) as $row) {
                $doc = new stdClass();
                $doc->id = $row->id;
                $doc->name = $row->nombre;
                $doc->module = $row->module;
                $doc->inicio_resolucion = $row->inicio_resolucion;
                $doc->fin_resolucion = $row->fin_resolucion;
                $doc->inicio_resolucion_wappsi = $row->inicio_resolucion_wappsi;
                $doc->sales_prefix = $row->sales_prefix;
                $doc->sales_consecutive = $row->sales_consecutive;
                $docs[] = $doc;
            }
            return $docs;
        }
        return false;
    }

    public function get_documentsTypesByModule($modulos)
    {
        $q = $this->db->order_by('module', 'ASC')
                        ->where('factura_electronica', '1')
                        ->where('sales_consecutive >', ' 1') 
                        ->where_in('module', $modulos)
                        ->get('documents_types');
        if ($q->num_rows() > 0) {
            $q->result_array();
            foreach (($q->result()) as $row) {
                $doc = new stdClass();
                $doc->id = $row->id;
                $doc->module = $row->module;
                $doc->sales_prefix = $row->sales_prefix;
                $doc->sales_consecutive = $row->sales_consecutive;
                $docs[] = $doc;
            }
            return $docs;
        }
        return false;
    }

    public function get_firts_register($table, $document_type_id, $year, $month)
    {
        if ($year == 'all') { 
            $years = $this->get_years();
            unset($years['all']);
            $keys = array_keys($years);
            $year = reset($keys);  // Primera clave
        }

        $this->db->from($table);
        $this->db->where('document_type_id', $document_type_id);
        if ($month && $month > 0) {
            $start_date = "$year-$month-01";
            $this->db->where('date >=', $start_date);
        }else{
            $start_date = "$year-01-01";
            $this->db->where('date >=', $start_date);
        }
        $this->db->order_by('date', 'ASC');
        $this->db->limit(1);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->row();    
        }
        return false;
    }

    public function get_last_register($table, $document_type_id, $year, $month)
    {
        if ($year == 'all') { 
            $years = $this->get_years();
            unset($years['all']);
            $keys = array_keys($years);
            $year = end($keys);   // Primera clave
        }

        $this->db->from($table);
        $this->db->where('document_type_id', $document_type_id);
        if ($month && $month > 0) {
            $last_date =  date("Y-m-t", strtotime("$year-$month-31"));
            $this->db->where('date <=', $last_date);
        }else{
            $last_date = "$year-12-31";
            $this->db->where('date <=', $last_date);
        }
        $this->db->order_by('date', 'DESC');
        $this->db->limit(1);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->row();    
        }
        return false;
    }

    public function get_count_document($table, $document_type_id, $year, $month){
        $date = 'date';
        if ($table == 'documents_reception_events') $date = 'createdAt';
        if ($table == 'payroll_electronic_employee' ) $date = 'creation_date';

        if ($year == 'all') { 
            $years = $this->get_years();
            unset($years['all']);
            $keys = array_keys($years);
            $year = end($keys);   // Primera clave
        }else {
            if ($month && $month != 'all') { // <- Sí, month es true, entonces el year debe ser diferente de all
                $first_date = date("Y-m-01", strtotime("$year-$month-01"));
                $last_date = date("Y-m-t 23:59:59", mktime(0, 0, 0, $month, 1, $year));
                $this->db->where("$date >=", $first_date);
                $this->db->where("$date <=", $last_date);
            }else{
                $first_date = "$year-01-01";
                $last_date = "$year-12-31 23:59:59";
                $this->db->where("$date >=", $first_date);
                $this->db->where("$date <=", $last_date);
            }
        }
        $this->db->from($table);
        $this->db->where('document_type_id', $document_type_id);
        $count = $this->db->count_all_results();
        if ($count > 0) {
            return $count;
        }
        return false;
    }

    public function get_years (){
        $initialDate = $this->Settings->system_start_date;
        $initialYear = date('Y', strtotime($initialDate));
        $actualDate = new DateTime();
        $actualYear = $actualDate->format('Y');
        $dataYear['all'] = 'Todos';
        for ($year = $initialYear; $year <= $actualYear; $year++) {
            $dataYear[$year] = $year;
        }
        return $dataYear;
    }

    public function get_months(){
        return [
            "all"     => lang('alls'),
            "1"     => lang('January'),
            "2"     => lang('February'),
            "3"     => lang('March'),
            "4"     => lang('April'),
            "5"     => lang('May'),
            "6"     => lang('June'),
            "7"     => lang('July'),
            "8"     => lang('August'),
            "9"     => lang('September'),
            "10"     => lang('October'),
            "11"     => lang('November'),
            "12"     => lang('December'),
        ];
    }

    public function get_avg_month($year, $month, $consumed){
        $count = 1;
        $divisor = ($month == 'all' || !$month) ? 12 : 1;
        if ($year == 'all') { 
            $years = $this->get_years();
            unset($years['all']);
            $count = count($years);
        }
        $factor = $count * $divisor;
        return $consumed / $factor;
    }

    public function get_total_acquired(){
        $this->db->select_sum('cantidad_unidad');
        $this->db->where('tipo_mov', 4);
        $q = $this->db->get('wappsi_invoicing');
        if ($q->num_rows() > 0) {
            return $q->row();    
        }
        return false;
    }
}