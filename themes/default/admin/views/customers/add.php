<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_customer'); ?></h4>
      </div>
      <?= admin_form_open_multipart("customers/add", ['id' => 'add-customer-form']); ?>
        <?= form_hidden('document_code'); ?>
        <div class="modal-body">
          <p><?= lang('enter_info'); ?></p>
          <div class="row">
            <div class="col-md-3 form-group">
              <label>
                <input type="checkbox" name="customer_only_for_pos" id="customer_only_for_pos"> <?= lang('just_basic_data') ?>
              </label>
            </div>
            <div class="col-sm-3 form-group hide_pos_customer">
              <label>
                <input type="checkbox" name="status" id="status" checked> <?= lang('customer')." ".lang('active') ?>
              </label>
            </div>
            <div class="col-md-3 form-group hide_pos_customer">
              <label>
                <input type="checkbox" name="award_points_no_management" id="award_points_no_management"> <?= lang('award_points_no_management') ?>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 form-group hide_pos_customer">
              <?= lang('customer_type', 'customer_type') ?>
              <select name="customer_type" id="customer_type" class="form-control">
                <option value="customer"><?= lang('customer') ?></option>
                <option value="lead"><?= lang('lead') ?></option>
              </select>
            </div>
            <?php if ($this->Settings->enable_customer_tax_exemption == 1): ?>
              <div class="col-md-6 form-group hide_pos_customer">
                <label>
                  <input type="checkbox" name="tax_exempt_customer" id="tax_exempt_customer"> <?= lang('tax_exempt_customer') ?>
                </label>
              </div>
            <?php endif ?>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
              <?= form_label(lang('label_type_person'), 'type_person'); ?>
              <select name="type_person" id="type_person" class="form-control" required="required">
                <option value=""><?= lang('select') ?></option>
                <?php foreach ($types_person as $type_person): ?>
                  <option value="<?= $type_person->id ?>"><?= lang($type_person->description) ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group col-md-6 hide_pos_customer">
              <?= form_label(lang("label_type_vat_regime"), "type_vat_regime"); ?>
              <?php
                $types_vat_regime_options[""] = lang('select');
                foreach ($types_vat_regime as $type_vat_regime)
                {
                  $types_vat_regime_options[$type_vat_regime->id] = lang($type_vat_regime->description);
                }
              ?>
              <?= form_dropdown(["name"=>"type_vat_regime", "id"=>"type_vat_regime", "class"=>"form-control select", "required"=>TRUE], $types_vat_regime_options); ?>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
              <?= lang("id_document_type", "id_document_type"); ?>
              <select class="form-control select" name="tipo_documento" id="tipo_documento" style="width: 100%;" required="required">
                <option value=""><?= lang("select"); ?></option>
                <?php foreach ($id_document_types as $idt) : ?>
                  <option value="<?= $idt->id; ?>" data-code="<?= $idt->codigo_doc; ?>"><?= lang($idt->nombre); ?></option>
                <?php endforeach ?>
              </select>
            </div>

            <div class="form-group col-md-6">
                <?= lang("vat_no", "vat_no"); ?>
                <?php echo form_input('vat_no', '', 'class="form-control" id="vat_no" required="required"'); ?>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6 digito-verificacion hide_pos_customer" style="display: none;">
                <?= lang("check_digit", "check_digit"); ?>
                <?php echo form_input('digito_verificacion', '', 'class="form-control" id="digito_verificacion" readonly'); ?>
            </div>

            <div class="form-group col-md-6 person juridical_person  hide_pos_customer">
                <?= lang("label_business_name", "name"); ?>
                <?php echo form_input('name', '', 'class="form-control tip" id="name" required="required"'); ?>
            </div>
          </div>
            <div class="form-group person natural_person" style="display: none;">
              <div class="row">
                <div class="col-md-6" style="margin-bottom:17px;" >
                  <label><?= lang('label_first_name') ?></label><span class='input_required'> *</span>
                  <input type="text" name="first_name" class="form-control">
                </div>
                <div class="col-md-6 hide_pos_customer" style="margin-bottom:17px;" >
                  <label><?= lang('label_second_name') ?></label>
                  <input type="text" name="second_name" class="form-control">
                </div>
                <div class="col-md-6" style="margin-bottom:17px;" >
                  <label><?= lang('label_first_lastname') ?></label><span class='input_required'> *</span>
                  <input type="text" name="first_lastname" class="form-control">
                </div>
                <div class="col-md-6 hide_pos_customer" style="margin-bottom:17px;" >
                  <label><?= lang('label_second_lastname') ?></label>
                  <input type="text" name="second_lastname" class="form-control">
                </div>
              </div>
            </div>
          <div class="row">
            <div class="form-group col-md-6 company">
                <?= lang("label_tradename", "company"); ?>
                <?= form_input('company', '', 'class="form-control tip" id="company" required="required"'); ?>
            </div>
            <div class="col-md-6 form-group juridical_person hide_pos_customer">
              <?= form_label(lang('label_commercial_register'), 'commercial_register'); ?>
              <?= form_input(['name'=>'commercial_register', 'id'=>'commercial_register', 'class'=>'form-control']); ?>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
                <?= lang("email_address", "email_address"); ?>
                <input type="email" name="email" class="form-control" id="email_address" required="required">
            </div>
            <div class="form-group col-md-6">
                <?= lang("phone", "phone"); ?>
                <input type="tel" name="phone" class="form-control" required="required" id="phone"/>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
              <?= lang("country", "country"); ?>
              <select class="form-control select" name="country" id="country" required>
                <option value=""><?= lang("select") ?></option>
                <?php foreach ($countries as $row => $country): ?>
                  <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= $country->NOMBRE == $this->Settings->customer_default_country ? 'selected="selected"' : '' ?>><?= $country->NOMBRE ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group col-md-6">
              <?= lang("state", "state"); ?>
              <select class="form-control select" name="state" id="state" required>
                <option value=""><?= lang("select") ?></option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
              <?= lang("city", "city"); ?>
              <select class="form-control select" name="city" id="city" required>
                <option value=""><?= lang("select") ?></option>
              </select>
              <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
            </div>
            <div class="form-group col-md-6">
              <?= lang("address", "address"); ?>
              <input class="form-control" type="text" name="address" id="address" required="required">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6 hide_pos_customer">
              <?= lang("zone", "zone"); ?>
              <select class="form-control select" name="zone" id="zone">
                <option value=""><?= lang("select") ?></option>
              </select>
              <input type="hidden" name="zone_code" id="zone_code" <?= (isset($_POST['zone_code'])) ? "value='".$_POST['zone_code']."'" : "" ?>>
            </div>
            <div class="form-group col-md-6 hide_pos_customer">
              <?= lang("subzone", "subzone"); ?>
              <select class="form-control select" name="subzone" id="subzone">
                <option value=""><?= lang("select") ?></option>
              </select>
              <input type="hidden" name="subzone_code" id="subzone_code" <?= (isset($_POST['subzone_code'])) ? "value='".$_POST['subzone_code']."'" : "" ?>>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6 hide_pos_customer">
              <?= lang("postal_code", "postal_code"); ?>
              <small><a href="https://visor.codigopostal.gov.co/472/visor/" target="_blank"> Buscar códigos postales</a></small>
              <?php echo form_input('postal_code', '', 'class="form-control postal_code" id="postal_code"'); ?>
            </div>
            <div class="form-group col-md-6 row">
              <div class="col-md-6">
                <?= lang('birth_month', 'birth_month') ?>
                <?php
                $bmopts[''] = lang('select');
                for ($i=1; $i <=12 ; $i++) {
                  $bmopts[$i] = lang('months')[$i];
                }

                 ?>
                 <?= form_dropdown('birth_month', $bmopts, '', 'class="form-control" id="birth_month"') ?>
              </div>
              <div class="col-md-6">
                <?= lang('birth_day', 'birth_day') ?>
                <?php

                $bdopts[''] = lang('select');

                 ?>
                <?= form_dropdown('birth_day', $bdopts, '', 'class="form-control" id="birth_day"') ?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 form-group hide_pos_customer">
              <?= form_label(lang("label_types_obligations"), "types_obligations"); ?>
                <?php
                    $types_obligations_options = [];
                    foreach ($types_obligations as $types_obligation)
                    {
                        $types_obligations_options[$types_obligation->code] = $types_obligation->code ." - ". $types_obligation->description;
                    }
                ?>
                <?= form_dropdown(['name'=>'types_obligations[]', 'id'=>'types_obligations', 'class'=>'form-control select', 'multiple'=>TRUE, 'style'=>'height: auto;', 'required'=>TRUE], $types_obligations_options); ?>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6 hide_pos_customer">
              <label class="control-label" for="price_group"><?php echo $this->lang->line("price_group"); ?></label>
              <?php
                $pgs[''] = lang('select').' '.lang('price_group');
                foreach ($price_groups as $price_group)
                {
                  $pgs[$price_group->id] = $price_group->name;
                }
              ?>
              <?= form_dropdown('price_group', $pgs, $Settings->price_group, 'class="form-control select" id="price_group" style="width:100%;"'); ?>
            </div>
            <div class="form-group col-md-6 hide_pos_customer">
                <label class="control-label" for="customer_group"><?php echo $this->lang->line("customer_group"); ?>  <input type="checkbox" name="customer_special_discount" id="customer_special_discount"> <?= lang('customer_special_discount', 'customer_special_discount') ?></label>
                <?php
                  foreach ($customer_groups as $customer_group)
                  {
                    $cgs[$customer_group->id] = $customer_group->name;
                  }
                ?>
                <?= form_dropdown('customer_group', $cgs, $Settings->customer_group, 'class="form-control select" id="customer_group" style="width:100%;" required="required"'); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 form-group hide_pos_customer">
              <?= lang('customer_seller_id_assigned', 'customer_seller_id_assigned') ?>
              <?php
                $sellers_opt[''] = lang('select');
                foreach ($sellers as $seller) {
                    $sellers_opt[$seller->id] = $seller->name;
                }
               ?>
              <?=  form_dropdown('customer_seller_id_assigned', $sellers_opt, '', 'class="form-control select" id="customer_seller_id_assigned"'); ?>
            </div>
            <div class="form-group col-md-6 hide_pos_customer">
                <?= lang('customer_payment_type', 'customer_payment_type') ?>
                <?php
                  $cptypes = [
                    '1' => lang('payment_type_cash'),
                    '0' => lang('credit'),
                  ];
                ?>
                <?= form_dropdown('customer_payment_type', $cptypes, '', 'class="form-control select" id="customer_payment_type" style="width:100%;" required="required"'); ?>
            </div>
          </div>
          <div class="row hide_pos_customer">
            <div class="form-group col-md-6 credit_customer" style="display: none;">
                <?= lang('customer_credit_limit', 'customer_credit_limit') ?>
                <input type="text" name="customer_credit_limit" id="customer_credit_limit" class="form-control only_number">
            </div>
            <div class="form-group col-md-6 credit_customer" style="display: none;">
                <?= lang('customer_payment_term', 'customer_payment_term') ?>
                <input type="text" name="customer_payment_term" id="customer_payment_term" class="form-control only_number">
            </div>
          </div>
          <div class="row hide_pos_customer">
            <div class="form-group col-md-6">
                <?= lang('latitude', 'latitude') ?>
                <input type="text" name="latitude" id="latitude" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <?= lang('longitude', 'longitude') ?>
                <input type="text" name="longitude" id="longitude" class="form-control">
            </div>
          </div>
          <div class="row hide_pos_customer">
                <div class="form-group col-sm-6">
                    <?= lang('customer_employer', 'employer') ?>
                    <?php
                    echo form_input('employer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="customer" data-placeholder="' . lang("select") . ' ' . lang("employer") . '" class="form-control input-tip" style="width:100%;"');
                    ?>
                </div>
                <div class="form-group col-sm-6">
                      <?= lang("customer_employer_branch", "employer_branch"); ?>
                      <?php
                      echo form_dropdown('employer_branch', array('0' => lang("select") . ' ' . lang("customer_employer")), (isset($_POST['employer_branch']) ? $_POST['employer_branch'] : ''), 'id="address_id" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                      ?>
                  </div>
              </div>

          <?php if ($custom_fields): ?>
            <div class="row hide_pos_customer">
              <hr class="col-md-11">
              <?php foreach ($custom_fields as $id => $arr): ?>
                <?php
                $cf = $arr['data'];
                $cf_values = isset($arr['values']) ? $arr['values'] : NULL;
                ?>
                <div class="col-md-6">
                  <label><?= $cf->cf_name ?></label>
                  <?php if ($cf->cf_type == 'select' || $cf->cf_type == 'multiple'): ?>
                    <select name="<?= $cf->cf_code ?>[]" class="form-control" <?= $cf->cf_type == 'multiple' ? "multiple" : "" ?>>
                      <?php if ($cf_values): ?>
                        <option value="">Seleccione...</option>
                        <?php foreach ($cf_values as $cfv): ?>
                          <option value="<?= $cfv->cf_value ?>"><?= $cfv->cf_value ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                        <option value="">Sin valores para el campo</option>
                      <?php endif ?>
                    </select>
                  <?php endif ?>
                  <?php if ($cf->cf_type == 'date' || $cf->cf_type == 'text'): ?>
                    <input type="<?= $cf->cf_type ?>" name="<?= $cf->cf_code ?>" class="form-control">
                  <?php endif ?>
                </div>
              <?php endforeach ?>
            </div>
          <?php endif ?>

          <div class="row hide_pos_customer">
              <hr class="col-md-11">
              <div class="form-group col-md-6">
                  <label>
                      <input type="checkbox" name="customer_validate_min_base_retention" id="customer_validate_min_base_retention">
                      <?= lang('customer_validate_min_base_retention') ?>
                  </label>
              </div>
          </div>
          <div class="row hide_pos_customer">
              <div class="form-group col-md-12">
                  <table class="table">
                    <thead>
                      <tr>
                        <th><?= lang('retention') ?></th>
                        <th><?= lang('percentage') ?></th>
                        <th><?= lang('min_base') ?></th>
                        <th><?= lang('ledger') ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (count($retentions) > 0): ?>
                        <?php if (isset($retentions['FUENTE'])): ?>
                          <tr class="row_rete_fuente">
                            <td>
                              <select name="default_rete_fuente_id" id="default_rete_fuente_id" class="form-control">
                                <option value=""><?= lang('no_rete_fuente_default') ?></option>
                                <?php foreach ($retentions['FUENTE'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>"><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['IVA'])): ?>
                          <tr class="row_rete_iva">
                            <td>
                              <select name="default_rete_iva_id" id="default_rete_iva_id" class="form-control">
                                <option value=""><?= lang('no_rete_iva_default') ?></option>
                                <?php foreach ($retentions['IVA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>"><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['ICA'])): ?>
                          <tr class="row_rete_ica">
                            <td>
                              <select name="default_rete_ica_id" id="default_rete_ica_id" class="form-control">
                                <option value=""><?= lang('no_rete_ica_default') ?></option>
                                <?php foreach ($retentions['ICA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>"><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['OTRA'])): ?>
                          <tr class="row_rete_other">
                            <td>
                              <select name="default_rete_other_id" id="default_rete_other_id" class="form-control">
                                <option value=""><?= lang('no_rete_other_default') ?></option>
                                <?php foreach ($retentions['OTRA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>"><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_other_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_other_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_other_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                      <?php endif ?>
                    </tbody>
                  </table>
              </div>
          </div>
          <div class="row hide_pos_customer">

              <div class="form-group col-sm-3 text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 200px;">
                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                    <img alt="User Image" style="width: 100% !important;">
                  </div>
                  <div>
                    <span class="btn btn-success btn-file btn-profile-img btn-profile-img-edit">
                      <span class="fileinput-new">
                        <i class="fa fa-pencil"></i>
                      </span>
                    <span class="fileinput-exists">
                      <i class="fa fa-pencil"></i>
                    </span>
                    <input type="file" name="customer_profile_photo" id="userimageupdate" /></span>
                   <!--  <a href="#" class="btn btn-danger fileinput-exists btn-profile-img btn-profile-img-remove" data-dismiss="fileinput"><i class="fa fa-trash"></i></a>
                    <button type="submit" name="uploadimage" class="btn btn-primary fileinput-exists btn-profile-img btn-profile-img-submit"><i class="fa fa-check"></i></button> -->
                    <span>Tamaño <?= $this->Settings->iwidth ?> x <?= $this->Settings->iheight ?></span>
                  </div>
                </div>
                <label><?= lang('customer_profile_photo') ?></label>
              </div>

          </div>
          <div class="row hide_pos_customer">
              <div class="form-group col-sm-12">
                <?= lang('note', 'note') ?>
                <textarea name="note" id="note" class="form-control">
                </textarea>
              </div>
          </div>
        </div>
        <div class="modal-footer">
            <p class="col-sm-6 pull-left text-left show_pos_customer add_more_data" style="display:none;cursor:pointer;"><i class="fa fa-exclamation-circle"></i> Añadir más datos</p>
            <input type="hidden" name="add_customer" value="1">
            <button class="btn btn-primary" id="submit_add_customer" type="button"><?= lang('add_customer') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
  $(document).ready(function (e) {
      $("#add-customer-form").validate({
          ignore: []
      });
      $(document).on('click', '#submit_add_customer', function(){
        form = $('#add-customer-form');
        if (form.valid()) {
          form.submit();
        }
      });
      $('select.select').select2({minimumResultsForSearch: 7});
      $(document).on('change', '#tipo_documento', function(){ show_hide_input_verification_digit($(this).val()); });
      $(document).on('change', '#type_person', function() { show_hide_inputs_type_person($(this).val()); });
      $('#state').on('change', function(){
        dpto = $('#state option:selected').data('code');
        $.ajax({
          url:"<?= admin_url() ?>customers/get_cities/"+dpto,
        }).done(function(data){
          $('#city').html(data);
          <?php if (!empty($this->Settings->customer_default_city)) { ?>
            $('#city').select2('val', '<?= $this->Settings->customer_default_city ?>');
            $('#city').trigger('change');
          <?php } ?>
        }).fail(function(data){
          console.log(data.responseText);
        });
      });

      $('#country').on('change', function(){
        dpto = $('#country option:selected').data('code');

        $.ajax({
          url:"<?= admin_url() ?>customers/get_states/"+dpto,
        }).done(function(data){
          $('#state').html(data);
          <?php if (!empty($this->Settings->customer_default_state)) { ?>
            $('#state').select2('val', '<?= $this->Settings->customer_default_state ?>');
            $('#state').trigger('change');
          <?php } ?>
        }).fail(function(data){
          console.log(data.responseText);
        });
      });

      $('#city').on('change', function(){
        code_country = $('#country option:selected').data('code');
        code = $('#city option:selected').data('code');
        postal_code = '';
        if (code) {
          postal_code = code.toString().replace(code_country, '');
        }

        $('.postal_code').val(postal_code);
        $('#city_code').val(code);

        $.ajax({
          url:"<?= admin_url().'customers/get_zones/' ?>"+code
        }).done(function(data){
          $('#zone').html(data);
        });

      });
      $('#zone').on('change', function(){
        code = $('#zone option:selected').data('code');
        $('.postal_code').val(code);
        $.ajax({
          url:"<?= admin_url().'customers/get_subzones/' ?>"+code
        }).done(function(data){
          $('#subzone').html(data);
        });
      });
      $('#subzone').on('change', function(){
        code = $('#subzone option:selected').data('code');
        $('.postal_code').val(code);
      });
      $('#vat_no').on('change', function(){
          nit = $(this).val();
          if (site.settings.get_companies_check_digit == 1) {
            dvf = calcularDigitoVerificacion(nit);
            $('#digito_verificacion').val(dvf);
          }
      });
      <?php if (!empty($this->Settings->customer_default_country)) { ?>
        $('#country').trigger('change');
      <?php } ?>
      setTimeout(function() {
        $('#customer_payment_type').select2('val', '<?= $this->Settings->customer_default_payment_type ?>').trigger('change');
      }, 850);
      <?php if ($this->Settings->customer_default_payment_type == 0): ?>
          $('#customer_payment_term').val('<?= $this->Settings->customer_default_payment_term ?>');
          $('#customer_credit_limit').val('<?= $this->Settings->customer_default_credit_limit ?>');
      <?php endif ?>
    });

    function show_hide_input_verification_digit(document_type_id)
    {
      if (document_type_id == 6 && site.settings.get_companies_check_digit == 1)
      {
        $('.digito-verificacion').css('display', '');
        $('.digito-verificacion input').attr('required', true);
      }
      else
      {
        $('.digito-verificacion').css('display', 'none');
        $('.digito-verificacion input').attr('required', false);
      }

      $('input[name="document_code"]').val($('#tipo_documento option:selected').data('code'));
    }

    function show_hide_inputs_type_person($type_person_id)
    {
      if ($type_person_id == '<?= NATURAL_PERSON; ?>')
      {
        $('input[name="first_name"]').attr('required', true);
        $('input[name="first_lastname"]').attr('required', true);
        $('.natural_person').css('display', 'block');
        $('input[name="name"]').val('XXX');
        $('.juridical_person').css('display', 'none');
        $('#name').val('-');
        // Iteración para remover los atributos "disabled" de todas las opciones de tipos de documento.
        $('#tipo_documento option').each(function()
        {
          $(this).removeAttr('disabled');
        });
        $('#tipo_documento').select2('val', '');
        $('#tipo_documento').trigger('change');
        $('input[name="document_code"]').val('');
        // Iteración para remover los atributos "disabled" de todas las opciones de tipo de régimen.
        // $('#type_vat_regime option').each(function(){
        //   $(this).removeAttr('disabled');
        // });
        $('#type_vat_regime').select2('val', '');

        $('#company').removeAttr('required');
      }
      else if ($type_person_id == '<?= LEGAL_PERSON; ?>')
      {
        // Se elimina los atributos requeridos.
        $('input[name="name"]').val('');
        $('input[name="commercial_register"]').val('');
        $('input[name="first_name"]').removeAttr('required');
        $('input[name="first_lastname"]').removeAttr('required');
        $('.natural_person').css('display', 'none');

        // Se agrega atributo required
        $('#name').val('');
        $('input[name="name"]').attr('required');
        $('.juridical_person').css('display', 'block');
        $('#name').val('');

        // Iteración para seleccionar el Nit extrictamente y desabilitar las demás opciones.
        $('#tipo_documento option').each(function() {
          if ($(this).data('code') == 31) {
            document_type_id = $(this).val();
            document_code = $(this).data('code');
          }
        });
        $('#tipo_documento').select2('val', document_type_id);
        $('#tipo_documento').trigger('change');
        $('input[name="document_code"]').val(document_code);
        $('#type_vat_regime').select2('val', <?= COMMON; ?>);
      }

      $("#add-customer-form").validate({
          ignore: []
      });
    }

    $(document).on('change', '#birth_month', function(){
      var month = $(this).val();
      var days_of_the_month = new Date("<?= date('Y') ?>", month, 0).getDate();
      var options_html = "";
      for (var i = 1; i <= days_of_the_month; i++) {
          options_html += "<option value='"+i+"'>"+i+"</option>";
      }
      $('#birth_day').html(options_html);
    });
    $(document).on('ifChecked', '#customer_only_for_pos', function(){
      set_only_pos_required();
    });
    $(document).on('ifUnchecked', '#customer_only_for_pos', function(){
      unset_only_pos_required();
    });
    $(document).on('change', '#customer_type', function(){
      if ($(this).val() == 'lead') {
        set_only_pos_required();
      } else {
        unset_only_pos_required();
      }
    });
    <?php if ($pos_call): ?>
      $('#customer_only_for_pos').iCheck('check').trigger('change');
    <?php endif ?>
    function set_only_pos_required(){
      $('#types_obligations').select2('val', 'O-99').attr('required', false);
      $('.hide_pos_customer').css('display', 'none');
      $('.hide_pos_customer').addClass('d-none');
      $('.show_pos_customer').css('display', '');
      // $('#email_address').removeAttr('required');
      $('#address').removeAttr('required');
      $('#phone').removeAttr('required');
      $('#type_person').val(2).trigger('change');
      $('#type_vat_regime').val(1).trigger('change');
      $('#tipo_documento').val(3).trigger('change');
      setTimeout(function() {
        $('#customer_payment_type').val(1).trigger('change');
      }, 1100);
      $('#vat_no').focus();
      $('#address').val('CL 00 00 00');
    }
    function unset_only_pos_required(){
      $('#types_obligations').select2('val', '').attr('required', true);
      $('.hide_pos_customer').css('display', '');
      $('.hide_pos_customer').removeClass('d-none');
      $('.show_pos_customer').css('display', 'none');
      // $('#email_address').prop('required', 'required');
      $('#address').prop('required', 'required');
      $('#phone').prop('required', 'required');
      $('#customer_payment_type').val(2).trigger('change');
      $('#address').val('');
    }

    $(document).on('keyup', '#vat_no', function(){
        var vat_no = $(this).val();
        $.ajax({
          url:'<?= admin_url("customers/validate_vat_no/") ?>'+vat_no
        }).done(function(data){
          if (data == "true") {
            Command: toastr.error('Ya existe el cliente', '¡Error!', {onHidden : function(){}})
            header_alert('error', 'Ya existe el cliente con documento '+$('#vat_no').val());
            $('#submit_add_customer').prop('disabled', true);
          } else {
            $('#submit_add_customer').prop('disabled', false);
          }
        });
    });
    $(document).on('change', '#customer_payment_type', function(){
      val = $(this).val();
      console.log(" >> val "+val);
      if (val == 0) {
        $('.credit_customer').css('display', '');
        $('#customer_credit_limit').prop('required', true);
        $('#customer_payment_term').prop('required', true).prop('min', 1);
      } else if (val == 1) {
        $('.credit_customer').css('display', 'none');
        $('#customer_credit_limit').prop('required', false);
        $('#customer_payment_term').prop('required', false).prop('min', 0);
      }
    });
    $(document).on('change', '#default_rete_fuente_id', function(){
      var rete_fuente = $('#default_rete_fuente_id option:selected');
      var percentage = rete_fuente.data('percentage');
      var min_base = rete_fuente.data('minbase');
      var ledger = rete_fuente.data('account');
      $('.rete_fuente_percentage').val(percentage);
      $('.rete_fuente_minbase').val(min_base);
      $('.rete_fuente_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_iva_id', function(){
      var rete_iva = $('#default_rete_iva_id option:selected');
      var percentage = rete_iva.data('percentage');
      var min_base = rete_iva.data('minbase');
      var ledger = rete_iva.data('account');
      $('.rete_iva_percentage').val(percentage);
      $('.rete_iva_minbase').val(min_base);
      $('.rete_iva_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_ica_id', function(){
      var rete_ica = $('#default_rete_ica_id option:selected');
      var percentage = rete_ica.data('percentage');
      var min_base = rete_ica.data('minbase');
      var ledger = rete_ica.data('account');
      $('.rete_ica_percentage').val(percentage);
      $('.rete_ica_minbase').val(min_base);
      $('.rete_ica_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_other_id', function(){
      var rete_other = $('#default_rete_other_id option:selected');
      var percentage = rete_other.data('percentage');
      var min_base = rete_other.data('minbase');
      var ledger = rete_other.data('account');
      $('.rete_other_percentage').val(percentage);
      $('.rete_other_minbase').val(min_base);
      $('.rete_other_ledger').val(ledger);
    });
    $(document).on('ifChecked', '#fuente_retainer', function(){
      $('.row_rete_fuente').fadeOut();
      $('#default_rete_fuente_id').select2('val', '').trigger('change');
    });
    $(document).on('ifUnchecked', '#fuente_retainer', function(){
      $('.row_rete_fuente').fadeIn();
    });
    $(document).on('ifChecked', '#iva_retainer', function(){
      $('.row_rete_iva').fadeOut();
      $('#default_rete_iva_id').select2('val', '').trigger('change');
    });
    $(document).on('ifUnchecked', '#iva_retainer', function(){
      $('.row_rete_iva').fadeIn();
    });
    $(document).on('ifChecked', '#ica_retainer', function(){
      $('.row_rete_ica').fadeOut();
      $('#default_rete_ica_id').select2('val', '').trigger('change');
    });
    $(document).on('ifUnchecked', '#ica_retainer', function(){
      $('.row_rete_ica').fadeIn();
    });
    $(document).on('keydown', '#longitude', function(){
      val = $(this).val();
      if (val > 999) {
        Command: toastr.error('Valor ingresado incorrecto', '¡Error!', {onHidden : function(){}})
        $(this).val('');
      }
    });
    $(document).on('keydown', '#latitude', function(){
      val = $(this).val();
      if (val > 999) {
        Command: toastr.error('Valor ingresado incorrecto', '¡Error!', {onHidden : function(){}})
        $(this).val('');
      }
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#previewImage').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#userimageupdate").change(function(){
      readURL(this);
    });

    $(document).on('change', '#name', function(){
      type_person = $('#type_person').val();
      name = $('#name').val();
      if (type_person == 1) {
        $('#company').val(name);
      }
    });

      $(document).on('click', '.add_more_data',function(){
        $('#customer_only_for_pos').iCheck('uncheck').trigger('change');
      });
    nscustomer();
    function nscustomer() {
        $('#customer').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
              $.ajax({
                type: "get", async: false,
                url: site.base_url+"customers/getcustomer/" + $(element).val(),
                dataType: "json",
                success: function (data) {
                  callback(data[0]);
                }
              });
            },
            ajax: {
              url: site.base_url+"customers/suggestions",
              dataType: 'json',
              quietMillis: 15,
              data: function (term, page) {
                return {
                  term: term,
                  limit: 10
                };
              },
              results: function (data, page) {
                if (data.results != null) {
                  return {results: data.results};
                } else {
                  return {results: [{id: '', text: lang.no_match_found}]};
                }
              }
            }
        });
    }
$(document).on('change', '#customer', function(){
  $.ajax({
        url: site.base_url+"sales/getCustomerAddresses",
        type: "get",
        data: {"customer_id" : $(this).val()}
    }).done(function(data){
        if (data != false) {
            $('#address_id').html(data);
            if (address_id = localStorage.getItem('address_id')) {
                $('#address_id option').each(function(index, option){
                    if ($(option).val() == address_id) {
                        $(option).prop('selected', 'selected');
                    }
                });
            }
            $('#address_id').select2();
        } else {
            $('#address_id').select2('val', '');
        }
    }); //FIN AJAX SUCURSALES
  });
</script>

<?= $modal_js ?>
<?= $customer_js ?>