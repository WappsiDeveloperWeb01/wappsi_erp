<?php

/***********

ORDER DE REMISIÓN SIN DATOS DE EMPRESA, NO SE DISCRIMINA EL IVA (4babys)

************/
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
  {
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Orden de remisión' ));
        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 14.5, 3, '1234', '');
        $cx = 132;
        $cy = 7;
        $this->setXY($cx, $cy);
        $this->Cell(77, 7.25 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Orden de remisión'),'',1,'C');
        $cy +=7.25;
        $this->setXY($cx, $cy);
        $this->Cell(77, 7.25 , $this->sma->utf8Decode('N°. '.$this->factura->reference_no),'',1,'C');

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 24, 118, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 29, 118, 30, 3, '4', '');
        $cx = 13;
        $cy = 24;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('INFORMACIÓN DEL CLIENTE'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->setXY($cx, $cy);
        // $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->customer->name != '-' ? $this->customer->name : $this->customer->company)),'',1,'L');
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 7;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->phone_negocio),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->email),'',1,'L');

        //derecha


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(131, 24, 78, 5, 3, '2', 'DF');
        $this->RoundedRect(131, 29, 78, 30, 3, '3', '');
        $cx = 131;
        $cy = 24;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA'),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('TOTAL'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, ($this->factura->grand_total  - $this->factura->rete_fuente_total - $this->factura->rete_iva_total - $this->factura->rete_ica_total - $this->factura->rete_other_total - ($this->factura->rete_bomberil_id && $this->factura->rete_bomberil_total > 0 ? $this->factura->rete_bomberil_total : 0) - ($this->factura->rete_autoaviso_id && $this->factura->rete_autoaviso_total > 0 ? $this->factura->rete_autoaviso_total : 0)) * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('VENCIMIENTO'),'LBR',1,'C',1);
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FORMA DE PAGO'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".($this->factura->payment_term > 0 ? $this->factura->payment_term : 0)." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sale_payment_method),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode('VENDEDOR'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');

        $this->ln();


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->Cell(17,5, $this->sma->utf8Decode('Código'),'TBLR',0,'C',1);
        $this->Cell(84,5, $this->sma->utf8Decode('Descripción de producto'),'TBLR',0,'C',1);
        $this->Cell(17,5, $this->sma->utf8Decode('Cant.'),'TBLR',0,'C',1);
        // if ($this->tax_inc) {
        //     $unit_price_size = 27.2;
        //     $this->Cell(12,5, $this->sma->utf8Decode(lang('iva')),'TBLR',0,'C',1);
        // } else {
        // }
        $this->Cell(22.6,5, $this->sma->utf8Decode('Vr. unit.'),'TBLR',0,'C',1);
        $this->Cell(10.6,5, $this->sma->utf8Decode('Dcto'),'TBLR',0,'C',1);
        $this->Cell(22.6,5, $this->sma->utf8Decode('Vr. con Dcto'),'TBLR',0,'C',1);
        $this->Cell(22.6,5, $this->sma->utf8Decode('Vr. total'),'TBLR',1,'C',1);

    }

    function Footer()
    {
        // Print centered page number
        // $this->RoundedRect(13, 248, 196, 17.5, 3, '1234', '');
        $this->SetXY(13, -30);
        // $this->Cell(196, 5 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
        // if (isset($this->cost_center) && $this->cost_center) {
        //     $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2( "Centro de costo : ".$this->cost_center->name." (".$this->cost_center->code.")".$this->description2)), 0, 'L');
        // } else {
        //     $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
        // }
        $this->SetXY(7, -14);
        // $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');

    }

    function reduceTextToDescription1($text){
        $text="Nota : ".$text;
        if (strlen($text) > 390) {
            $text = substr($text, 0, 385);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        if (strlen($text) > 380) {
            $text = substr($text, 0, 375);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

}


$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);


$number_convert = new number_convert();

$fuente = 8;
$adicional_fuente = 2;

$color_R = 200;
$color_G = 200;
$color_B = 200;

$pdf->setFillColor($color_R,$color_G,$color_B);

$pdf->biller = $biller;

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->sale_payment_method = $sale_payment_method;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->value_decimals = $value_decimals;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}
$pdf->tipo_regimen = $tipo_regimen;

$pdf->AddPage();

$maximo_footer = 265;

$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);

$taxes = [];
$total_bruto = 0;
$references = [];
    // exit(var_dump($rows));
    foreach ($rows as $item) {

        $total_bruto += (($item->net_unit_price + ($item->item_discount / $item->quantity)) * $item->quantity) * $trmrate;

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
        }

        if (isset($references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id])) {
            // $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['unit_price_1'] += ($item->net_unit_price + ($item->item_discount / $item->quantity)) * $trmrate;
            // $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['unit_price_2'] += $item->net_unit_price * $trmrate;
            // $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['unit_price_3'] += $item->unit_price * $trmrate;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_subtotal'] += ($references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['unit_price_1'] * $item->quantity) * $trmrate;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_quantity'] += $item->quantity;
            // $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_individual_discount'] += $item->item_discount / $item->quantity;
            // $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_individual_tax'] += $item->item_tax / $item->quantity;
        } else {
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_code'] = $document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->product_code;
            $pr_name = $pdf->sma->reduce_text_length(
                                                ($item->under_cost_authorized == 1 ? '    ' : '').
                                                $item->product_name.
                                                (!is_null($item->variant) && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? "( ".$item->variant." )" : '').
                                                ($pdf->Settings->show_brand_in_product_search ? " - ".$item->brand_name : '').
                                                (!is_null($item->serial_no) ? " - ".$item->serial_no : '').
                    strip_tags($this->sma->decode_html(($show_product_preferences == 1 && $item->preferences ? ' (' . $this->sma->print_preference_selection($item->preferences) . ')' : '')))
                                            , 60);
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_name'] = $pr_name;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_discount'] = $item->discount;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_individual_discount'] = $item->item_discount / $item->quantity;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_tax'] = $item->tax != '' ? $item->tax : 0;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_individual_tax'] = $item->item_tax / $item->quantity;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['unit_price_1'] = ($item->net_unit_price + ($item->item_discount / $item->quantity)) * $trmrate;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['unit_price_2'] = $item->net_unit_price * $trmrate;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['unit_price_3'] = $item->unit_price * $trmrate;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_subtotal'] = ($references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['unit_price_2'] * $item->quantity) * $trmrate;
            $references[$document_type_invoice_format && $document_type_invoice_format->group_by_product_reference == 1 ? $item->reference : $item->id]['pr_quantity'] = $item->quantity;
        }
    }

    // $this->sma->print_arrays($references);

    $cnt = 1;
    foreach ($references as $reference => $data) {
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }

        $pdf->Cell(17,5, $this->sma->utf8Decode($data['pr_code']),'BLR',0,'C');
        if ($item->under_cost_authorized == 1) {
            $unlock_icon_X = $pdf->getX();
            $unlock_icon_Y = $pdf->getY();
            $pdf->Image(base_url().'assets/images/unlock-icon.png',$unlock_icon_X+1, $unlock_icon_Y+0.6,2.6);
        }
        $pdf->Cell(84,5, $this->sma->utf8Decode($data['pr_name']),'BR',0,'L');
        $pdf->Cell(17,5, $this->sma->utf8Decode($this->sma->formatQuantity($data['pr_quantity'], $qty_decimals)),'BR',0,'C');
        $pdf->Cell(22.6,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_1'] * $trmrate)),'BR',0,'R');
        $pdf->Cell(10.6,5, $this->sma->utf8Decode($data['pr_discount']),'BR',0,'R');
        $pdf->Cell(22.6,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_2'] * $trmrate)),'BR',0,'R');
        $pdf->Cell(22.6,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['pr_subtotal'] * $trmrate)),'BR',1,'R');
        $cnt++;
    }

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 115.6, 14, 3, '1234', '');

$pdf->ln(1);

$pdf->SetFont('Arial','B',$fuente-1.2);
$pdf->Cell(1,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->Cell(115.6,5, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(14);
$pdf->SetFont('Arial','',$fuente-1.2);
// $pdf->Cell(115.6,6.125, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total, $currencies_names[$inv->sale_currency])),'',1,'L');
$pdf->MultiCell(115.6, 3, $this->sma->utf8Decode($number_convert->convertir((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate), (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency))), 0, 'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->setX(13);

$pdf->setXY($current_x, $current_y+8);

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

//derecha

$pdf->setXY($cX_items_finished+118, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();
// exit(var_dump($inv->total_discount));
// $this->sma->print_arrays($inv);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Sub total antes de Dcto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, (($inv->total + $inv->total_tax + $inv->product_discount) * $trmrate))), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Descuento'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total_discount * $trmrate)), 'TBR', 1, 'R');

if ($inv->shipping > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode('  Serv. Transporte'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('TOTAL A PAGAR'), 'TBLR', 1, 'R', 1);
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate)), 'TBR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente);

$pdf->Ln(5);

//FIRMAS

//izquierda
$pdf->ln(2);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;

$pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

//derecha
$pdf->setXY($current_x+78.4, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Entregó'), '', 1, 'L');

// $pdf->Image(( isset($this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : '' ) ,$current_x+35,$current_y+1,35);

// $descargar = false;
if ($inv->sale_status == 'pending') {
    $pdf->setXY(90, 90);
    $pdf->SetFont('Arial','B',50);
    $pdf->SetTextColor(125,125,125);
    $pdf->RotatedImage(base_url().'assets/images/not_approved.png',1,240,293,16,45);
    $pdf->SetFont('Arial','',$fuente);
    $pdf->SetTextColor(0,0,0);
}

if ($for_email === true) {
    $pdf->Output(FCPATH."files/".$inv->reference_no.".pdf", "F");
} else {
    if ($download) {
      $pdf->Output($inv->reference_no.".pdf", "D");
    } else {
      $pdf->Output($inv->reference_no.".pdf", "I");
    }    
}

