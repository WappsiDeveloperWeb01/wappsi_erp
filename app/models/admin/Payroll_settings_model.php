<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_settings_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function get()
    {
    	$q = $this->db->get("payroll_settings");
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    }


	public function get_payment_frequency()
	{
		$this->db->where("status", YES);
		$q = $this->db->get("payroll_period");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_arl()
	{
		$this->db->where("supplier_type", ARL);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_ccf()
	{
		$this->db->where("supplier_type", COMPENSATION_BOX);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_bank()
	{
		$this->db->where("supplier_type", FINANCIAL_ENTITY);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_ss_operators()
	{
		$q = $this->db->get("payroll_ss_operators");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_frequency_options($payment_frecuency)
	{
		if ($payment_frecuency == WEEKLY) {
    		$options = [
    			WEEKLY => 'Cada semana',
    			MONTHLY => 'En la última semana'
    		];
        } else if ($payment_frecuency == BIWEEKLY) {
        	$options = [
    			BIWEEKLY => 'Cada quincena',
    			MONTHLY => 'En la última quincena'
    		];
        } else if ($payment_frecuency == MONTHLY) {
        	$options = [MONTHLY => 'Mensual'];
        }

        return $options;
	}

	public function insert_payroll_settings($data)
	{
		if ($this->db->insert("payroll_settings", $data)){
			return TRUE;
		}
		return FALSE;
	}

	public function update_payroll_settings($data, $id)
	{
		$this->db->where("id", $id);
		if ($this->db->update("payroll_settings", $data)){
			return TRUE;
		}
		return FALSE;
	}
}

/* End of file Payroll_settings_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/payroll_settings_model.php */