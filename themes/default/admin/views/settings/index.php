<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    $wm = array('0' => lang('no'), '1' => lang('yes'));
    $ps = array('0' => lang("disable"), '1' => lang("enable"));
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?= admin_form_open_multipart("system_settings", ['id' => 'form_settings']); ?>
            <?= form_hidden('code_iso', ''); ?>
                <input type="password" name="" style="display: none;">

                <div class="ibox">
                    <div class="ibox-title">
                        <h5><?= lang('seccion_title_company_data') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">

                        <div class="col-sm-12">
                            <div class="title-action">
                                <a href="<?= admin_url('system_settings/skrill') ?>" class="btn btn-primary pull-right"><span><?= lang('skrill'); ?></span></a>
                                <a href="<?= admin_url('system_settings/paypal') ?>" class="btn btn-primary pull-right"><span><?= lang('paypal'); ?></span></a>
                            </div>
                        </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_type_person'), 'type_person'); ?>
                                        <?php
                                            $types_person_options[""] = lang('select');
                                            foreach ($types_person as $type_person) {
                                                $types_person_options[$type_person->id] = lang($type_person->description);
                                            }
                                        ?>
                                        <?= form_dropdown(['name'=>'type_person', 'id'=>'type_person', 'class'=>'form-control select', 'required'=>TRUE], $types_person_options, $Settings->tipo_persona); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang("label_type_vat_regime"), "type_vat_regime"); ?>
                                        <?php
                                            $types_vat_regime_options[""] = lang('select');
                                            foreach ($types_vat_regime as $type_vat_regime)
                                            {
                                                $types_vat_regime_options[$type_vat_regime->id] = lang($type_vat_regime->description);
                                            }
                                            $tvr_attr = ["name"=>"type_vat_regime", "id"=>"type_vat_regime", "class"=>"form-control select"];
                                            if ($this->Settings->electronic_billing == 1) {
                                                $tvr_attr["required"] = TRUE;
                                            }
                                        ?>
                                        <?= form_dropdown($tvr_attr, $types_vat_regime_options, $Settings->tipo_regimen); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_document_type'), 'document_type'); ?>
                                        <?php
                                            $document_type_options[""] = lang('select');
                                            foreach ($document_types as $document_type)
                                            {
                                                $document_type_options[$document_type->codigo_doc] = lang($document_type->nombre);
                                            }
                                        ?>
                                        <?= form_dropdown(['name'=>'document_type', 'id'=>'document_type', 'class'=>'form-control select', 'required'=>TRUE], $document_type_options, $Settings->tipo_documento); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_document_number'), 'document_number'); ?>
                                        <?= form_input(['name'=>'document_number', 'id'=>'document_number', 'class'=>'form-control', 'required'=>TRUE], $Settings->numero_documento); ?>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-3 col-sm-6 form-group" id="contenedor_digito_verificacion">
                                        <?= form_label(lang('check_digit'), 'digito_verificacion'); ?>
                                        <?= form_input(['type'=>'number', 'name'=>'digito_verificacion', 'id'=>'digito_verificacion', 'class'=>'form-control', 'readonly' => true], (isset($this->Settings->digito_verificacion) ? $this->Settings->digito_verificacion : "")); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group legal_person_field" style="display: <?= ($Settings->tipo_persona == LEGAL_PERSON) ? 'block' : 'none'; ?>;">
                                        <?= form_label(lang('label_business_name'), 'business_name'); ?>
                                        <?= form_input(['name'=>'business_name', 'id'=>'business_name', 'class'=>'form-control', 'required'=>TRUE], $Settings->razon_social); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($Settings->tipo_persona == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                        <?= form_label(lang('label_first_name'), 'first_name'); ?>
                                        <?= form_input(['name'=>'first_name', 'id'=>'first_name', 'class'=>'form-control natural_person_field_required', 'required'=>TRUE], $Settings->primer_nombre); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($Settings->tipo_persona == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                        <?= form_label(lang('label_second_name'), 'second_name'); ?>
                                        <?= form_input(['name'=>'second_name', 'id'=>'second_name', 'class'=>'form-control natural_person_field '], $Settings->segundo_nombre); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($Settings->tipo_persona == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                        <?= form_label(lang('label_first_lastname'), 'first_lastname'); ?>
                                        <?= form_input(['name'=>'first_lastname', 'id'=>'first_lastname', 'class'=>'form-control natural_person_field natural_person_field_required', 'required'=>TRUE], $Settings->primer_apellido); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($Settings->tipo_persona == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                        <?= form_label(lang('label_second_lastname'), 'second_lastname'); ?>
                                        <?= form_input(['name'=>'second_lastname', 'id'=>'second_lastname', 'class'=>'form-control natural_person_field'], $Settings->segundo_apellido); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($Settings->tipo_persona == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                        <?= form_label(lang('label_trade_name'), 'trade_name'); ?>
                                        <?= form_input(['name'=>'trade_name', 'id'=>'trade_name', 'class'=>'form-control natural_person_field_required', 'required'=>TRUE], $Settings->nombre_comercial); ?>
                                    </div>

                                    <div class="col-md-3 col-sm-6 form-group legal_person_field">
                                        <?= form_label(lang('label_commercial_register'), 'commercial_register'); ?>


                                        <?php $required = ($this->Settings->fe_technology_provider == FACTURATECH) ? "required" : "" ?>
                                        <?= form_input(['name'=>'commercial_register', 'id'=>'commercial_register', 'class'=>'form-control'], $Settings->matricula_mercantil, $required); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_country'), 'country'); ?>
                                        <select name="country" id="country" class="form-control select" required="required">
                                            <option value=""><?= lang('select'); ?></option>
                                            <?php foreach ($countries as $country) { ?>
                                                <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" data-code_iso="<?= $country->codigo_iso ?>" <?= (isset($Settings) && $country->NOMBRE == $Settings->pais) ? 'selected' : '' ?>>   <?= $country->NOMBRE; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_state'), 'state'); ?>
                                        <?php $state_options = [''=>lang('select')]; ?>
                                        <?= form_dropdown(['name'=>'state', 'id'=>'state', 'class'=>'form-control select', 'required'=>TRUE], $state_options); ?>
                                    </div>
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_city'), 'city'); ?>
                                        <?php $city_options = [''=>lang('select')]; ?>
                                        <?= form_dropdown(['name'=>'city', 'id'=>'city', 'class'=>'form-control select', 'required'=>TRUE], $city_options); ?>
                                        <input type="hidden" name="city_code" id="city_code">
                                    </div>
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_postal_code'), 'postal_code'); ?>
                                        <?= form_input(['name'=>'postal_code', 'id'=>'postal_code', 'class'=>'form-control', 'required'=>TRUE], $Settings->postal_code); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_address'). ' <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Eje: Cl 9 # 40c - 13 (Ap., of.)" style="cursor: pointer"></i>' , 'address'); ?>
                                        <?= form_input(['name'=>'address', 'id'=>'address', 'class'=>'form-control', 'required'=>TRUE], $Settings->direccion); ?>
                                    </div>
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('label_locality'), 'locality'); ?>
                                        <?= form_input(['name'=>'locality', 'id'=>'locality', 'class'=>'form-control', 'required'=>TRUE], $Settings->localidad); ?>
                                    </div>
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('phone'), 'phone'); ?>
                                        <?= form_input(['name'=>'phone', 'id'=>'phone', 'class'=>'form-control', 'required'=>TRUE], $Settings->phone); ?>
                                    </div>
                                    <div class="col-md-3 col-sm-6 form-group">
                                        <?= form_label(lang('url_web'), 'url_web'); ?>
                                        <?php $city_options = [''=>lang('select')]; ?>
                                        <?= form_input(['name'=>'url_web', 'id'=>'url_web', 'class'=>'form-control'], $Settings->url_web); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 form-group">
                                        <?= form_label(lang("label_types_obligations"), "types_obligations"); ?>
                                        <?php
                                            $types_obligations_options = [''=>lang('select')];
                                            foreach ($types_obligations as $types_obligation)
                                            {
                                                $types_obligations_options[$types_obligation->code] = $types_obligation->code ." - ". $types_obligation->description;
                                            }
                                        ?>
                                        <?= form_dropdown(['name'=>'types_obligations[]', 'id'=>'types_obligations', 'class'=>'form-control', 'multiple'=>TRUE, 'style'=>'height: auto;', 'required'=>TRUE], $types_obligations_options); ?>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <?= lang('great_contributor', 'great_contributor'); ?>
                                        <?php $prioridadOpts = [
                                                                 '1' => lang('yes'),
                                                                 '0' => lang('no'),
                                                                ]; ?>
                                        <?= form_dropdown('great_contributor', $prioridadOpts, $Settings->great_contributor, 'class="form-control tip" id="great_contributor"'); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label><?= lang('auto_retainer') ?></label><br>
                                        <label>
                                            <input type="checkbox" name="fuente_retainer" <?= $Settings->fuente_retainer ? 'checked="checked"' : '' ?>>
                                            <?= lang('fuente') ?>
                                        </label>
                                        <label>
                                            <input type="checkbox" name="iva_retainer" <?= $Settings->iva_retainer ? 'checked="checked"' : '' ?>>
                                            <?= lang('iva') ?>
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ica_retainer" <?= $Settings->ica_retainer ? 'checked="checked"' : '' ?>>
                                            <?= lang('ica') ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <?= lang('export_to_csv_each_new_customer', 'export_to_csv_each_new_customer'); ?>
                                        <?php $prioridadOpts = [
                                                                 ''  => lang('select'),
                                                                 '1' => lang('yes'),
                                                                 '0' => lang('no'),
                                                                ]; ?>
                                        <?= form_dropdown('export_to_csv_each_new_customer', $prioridadOpts, $Settings->export_to_csv_each_new_customer, 'class="form-control tip" id="export_to_csv_each_new_customer"'); ?>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <?= lang('rete_other_language', 'rete_other_language') ?>
                                        <input type="text" name="rete_other_language" class="form-control" placeholder="<?= lang('rete_other_language_ph') ?>" value="<?= $this->Settings->rete_other_language ?>">
                                    </div>
                                    <div class="col-md-3 form-group row">
                                        <?= lang('customer_branch_language', 'customer_branch_language') ?>
                                        <div class="col-md-6">
                                            <input type="text" name="customer_branch_language" class="form-control" value="<?= $this->Settings->customer_branch_language ?>" placeholder="<?= lang('customer_branch_language_ph') ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="customer_branches_language" class="form-control" value="<?= $this->Settings->customer_branches_language ?>" placeholder="<?= lang('customer_branches_language_ph') ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <?= lang('tip_language', 'tip_language') ?>
                                        <input type="text" name="tip_language" class="form-control" value="<?= $this->Settings->tip_language ?>" placeholder="<?= lang('tip_language_ph') ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <?= lang('shipping_language', 'shipping_language') ?>
                                        <input type="text" name="shipping_language" class="form-control" value="<?= $this->Settings->shipping_language ?>" placeholder="<?= lang('shipping_language_ph') ?>">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <?= lang('restobar_table_language', 'restobar_table_language') ?>
                                        <input type="text" name="restobar_table_language" class="form-control" value="<?= $this->Settings->restobar_table_language ?>" placeholder="<?= lang('restobar_table_language_ph') ?>">
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("ciiu_code", "ciiu_code"); ?>
                                            <div class="input-group">
                                                <?= form_input('ciiu_code', (isset($_POST['ciiu_code']) ? $_POST['ciiu_code'] : ""), 'id="ciiu_code" data-placeholder="' . lang("select") . ' ' . lang("ciiu_code") . '" class="form-control input-tip" aria-describedby="basic-addon2" '); ?>
                                                <div class="input-group-addon btn btn-danger">
                                                    <span class="fa fa-trash clean_ciiu_code" id="basic-addon2" style="cursor: pointer;" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5><?= lang('site_config') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <?php if ($this->Owner): ?>
                                <div class="col-md-4 col-sm-4">
                                    <?= lang("modulary", "modulary"); ?>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="checkbox">
                                                    <input type="checkbox" name="modulary" class="modulary" <?= $Settings->modulary == 1 ? "checked='checked'" : "" ?> value="1">
                                                    Modular
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-8 div_accounting_year" <?= $Settings->modulary == 1 && $Settings->years_database_management == 1 ? "" : "style='display:none;'" ?>>
                                            <select name="accounting_year_suffix" class="form-control" id="accounting_year_suffix">
                                                <?php if (count($contabilidad_años) > 1): ?>
                                                    <option value="">Selección de año</option>
                                                <?php endif ?>
                                                <?php if (count($contabilidad_años) > 0): ?>
                                                    <?php foreach ($contabilidad_años as $key => $value): ?>
                                                        <option value="<?= $value ?>" <?= $this->Settings->accounting_year_suffix == $value ? 'selected' : '' ?>><?= $value ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div> -->
                                    </div>

                                </div>
                            <?php endif ?>
                            <div class="col-md-4 col-sm-4">
                                    <?= lang("site_name", "site_name"); ?>
                                    <?= form_input('site_name', $Settings->site_name, 'class="form-control tip" id="site_name"  required="required"'); ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang("digital_signature", "digital_signature") ?> <span>1024 x 400 px</span>
                                <input id="digital_signature" type="file" data-browse-label="<?= lang('browse'); ?>" name="digital_signature" data-show-upload="false"
                                       data-show-preview="false" accept="image/*" class="form-control file">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?= lang("language", "language"); ?>
                                <?php
                                $lang = array(
                                    'arabic'                    => 'Arabic',
                                    'english'                   => 'English',
                                    'german'                    => 'German',
                                    'portuguese-brazilian'      => 'Portuguese (Brazil)',
                                    'simplified-chinese'        => 'Simplified Chinese',
                                    'spanish'                   => 'Spanish',
                                    'thai'                      => 'Thai',
                                    'traditional-chinese'       => 'Traditional Chinese',
                                    'turkish'                   => 'Turkish',
                                    'vietnamese'                => 'Vietnamese',
                                );
                                echo form_dropdown('language', $lang, $Settings->language, 'class="form-control tip" id="language" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="currency"><?= lang("default_currency"); ?></label>

                                <div class="controls"> <?php
                                    foreach ($currencies as $currency) {
                                        $cu[$currency->code] = $currency->name;
                                    }
                                    echo form_dropdown('currency', $cu, $Settings->default_currency, 'class="form-control tip" id="currency" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang("accounting_method", "accounting_method"); ?>
                                <?php
                                // $am = array(0 => 'LIFO (Last In First Out)', 1 => 'FIFO (First In First Out)', 2 => 'AVCO (Average Cost Method)');
                                $am = array(1 => lang('FIFO'), 2 => lang('AVCO'));
                                echo form_dropdown('accounting_method', $am, $Settings->accounting_method, 'class="form-control tip" id="accounting_method" required="required" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="email"><?= lang("default_email"); ?></label>
                                <?= form_input('email', $Settings->default_email, 'class="form-control tip" required="required" id="email"'); ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="customer_group"><?= lang("default_customer_group"); ?></label>
                                <?php
                                    foreach ($customer_groups as $customer_group) {
                                        $pgs[$customer_group->id] = $customer_group->name;
                                    }
                                    echo form_dropdown('customer_group', $pgs, $Settings->customer_group, 'class="form-control tip" id="customer_group" style="width:100%;" required="required"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="price_group"><?= lang("default_price_group"); ?></label>
                                <?php
                                    foreach ($price_groups as $price_group) {
                                        $cgs[$price_group->id] = $price_group->name;
                                    }
                                    echo form_dropdown('price_group', $cgs, $Settings->price_group, 'class="form-control tip" id="price_group" style="width:100%;" required="required"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?= lang('maintenance_mode', 'mmode'); ?>
                                <div class="controls">  <?php
                                    echo form_dropdown('mmode', $wm, (isset($_POST['mmode']) ? $_POST['mmode'] : $Settings->mmode), 'class="tip form-control" required="required" id="mmode" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="theme"><?= lang("theme"); ?></label>
                                <div class="controls">
                                    <?php
                                    $themes = array(
                                        'default' => 'Default'
                                    );
                                    echo form_dropdown('theme', $themes, $Settings->theme, 'id="theme" class="form-control tip" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="rtl"><?= lang("rtl_support"); ?></label>
                                <div class="controls">
                                    <?php
                                    echo form_dropdown('rtl', $ps, $Settings->rtl, 'id="rtl" class="form-control tip" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="captcha"><?= lang("login_captcha"); ?></label>
                                <div class="controls">
                                    <?php
                                    echo form_dropdown('captcha', $ps, $Settings->captcha, 'id="captcha" class="form-control tip" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="disable_editing"><?= lang("disable_editing"); ?></label>
                                <?= form_input('disable_editing', $Settings->disable_editing, 'class="form-control tip" id="disable_editing" required="required"'); ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="rows_per_page"><?= lang("rows_per_page"); ?></label>
                                <?php
                                $rppopts = array('10' => '10', '25' => '25', '50' => '50',  '100' => '100', '-1' => lang('all').' ('.lang('not_recommended').')');
                                echo form_dropdown('rows_per_page', $rppopts, $Settings->rows_per_page, 'id="rows_per_page" class="form-control tip" style="width:100%;" required="required"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="dateformat"><?= lang("dateformat"); ?></label>
                                <div class="controls">
                                    <?php
                                    foreach ($date_formats as $date_format) {
                                        $dt[$date_format->id] = $date_format->js;
                                    }
                                    echo form_dropdown('dateformat', $dt, $Settings->dateformat, 'id="dateformat" class="form-control tip" style="width:100%;" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label" for="timezone"><?= lang("timezone"); ?></label>
                                <?php
                                $timezone_identifiers = DateTimeZone::listIdentifiers();
                                foreach ($timezone_identifiers as $tzi) {
                                    $tz[$tzi] = $tzi;
                                }
                                ?>
                                <?= form_dropdown('timezone', $tz, TIMEZONE, 'class="form-control tip" id="timezone" required="required"'); ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label"
                                for="restrict_calendar"><?= lang("calendar"); ?></label>
                                <div class="controls">
                                    <?php
                                    $opt_cal = array(1 => lang('private'), 0 => lang('shared'));
                                    echo form_dropdown('restrict_calendar', $opt_cal, $Settings->restrict_calendar, 'class="form-control tip" required="required" id="restrict_calendar" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <label class="control-label"
                                    for="warehouse"><?= lang("default_warehouse"); ?></label>
                                <div class="controls"> <?php
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name . ' (' . $warehouse->code . ')';
                                    }
                                    echo form_dropdown('warehouse', $wh, $Settings->default_warehouse, 'class="form-control tip" id="warehouse" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang("default_biller", "biller"); ?>
                                <?php
                                $bl[""] = "";
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang('prioridad_precios_producto', 'prioridad_precios_producto'); ?>
                                <?php $prioridadOpts = [
                                                         '1' => lang('price_group_base_with_promotions'),
                                                         '2' => lang('biller_price_group'),
                                                         '3' => lang('customer_price_group'),
                                                         '8' => lang('customer_address_price_group'),
                                                         '4' => lang('biller_price_group_with_discounts'),
                                                         '6' => lang('customer_price_group_with_discounts'),
                                                         '9' => lang('customer_address_price_group_with_discounts'),
                                                         '5' => lang('price_per_unit'),
                                                         '7' => lang('price_per_unit_manual'),
                                                         '10' => lang('price_per_unit_price_group'),
                                                         '11' => lang('price_per_unit_hybrid'),
                                                        ]; ?>
                                <?= form_dropdown('prioridad_precios_producto', $prioridadOpts, $Settings->prioridad_precios_producto, 'class="form-control tip" id="prioridad_precios_producto" required="required"'); ?>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('pdf_lib', 'pdf_lib'); ?>
                                    <?php $pdflibs = ['mpdf' => 'mPDF', 'dompdf' => 'Dompdf']; ?>
                                    <?= form_dropdown('pdf_lib', $pdflibs, $Settings->pdf_lib, 'class="form-control tip" id="pdf_lib" required="required"'); ?>
                                </div>
                            </div>
                            <?php if (SHOP) { ?>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('apis_feature', 'apis'); ?>
                                    <?= form_dropdown('apis', $ps, $Settings->apis, 'class="form-control tip" id="apis" required="required"'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-4 col-sm-4 ppu_div" style="display: none;">
                                <?= lang('precios_por_unidad_presentacion', 'precios_por_unidad_presentacion'); ?>
                                <?php $prioridadOpts = [
                                                         ''  => lang('select'),
                                                         '1' => lang('price_per_unit_unit'),
                                                         '2' => lang('price_per_unit_presentation'),
                                                        ]; ?>
                                <?= form_dropdown('precios_por_unidad_presentacion', $prioridadOpts, ($Settings->precios_por_unidad_presentacion == 1 || $Settings->precios_por_unidad_presentacion == 2 ? $Settings->precios_por_unidad_presentacion : 5), 'class="form-control tip" id="precios_por_unidad_presentacion"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 ppu_div" style="display: none;">
                                <?= lang('validate_um_min_factor_qty', 'validate_um_min_factor_qty'); ?>
                                <?php $prioridadOpts = [
                                                         ''  => lang('select'),
                                                         '1' => lang('yes'),
                                                         '0' => lang('no'),
                                                        ]; ?>
                                <?= form_dropdown('validate_um_min_factor_qty', $prioridadOpts, $Settings->validate_um_min_factor_qty, 'class="form-control tip" id="validate_um_min_factor_qty"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('get_companies_check_digit', 'get_companies_check_digit'); ?>
                                    <?= form_dropdown('get_companies_check_digit', $ps, $Settings->get_companies_check_digit, 'class="form-control tip" id="get_companies_check_digit" required="required"'); ?>
                                </div>
                            </div>
                            <?php if ($Settings->modulary): ?>
                                <div class="div_modulary">
                                    <div class="col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <?= lang('tax_rate_traslate', 'tax_rate_traslate'); ?>
                                            <?= form_dropdown('tax_rate_traslate', $ps, $Settings->tax_rate_traslate, 'class="form-control tip" id="tax_rate_traslate" required="required"'); ?>
                                        </div>
                                    </div>
                                    <?php
                                        $lopts[''] = lang('select');
                                        if ($ledgers) {
                                            foreach ($ledgers as $ledger) {
                                                $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                                            }
                                        }
                                     ?>
                                    <div class="col-md-12 div_tax_rate_translate" style="display: none;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <?= lang('set_ledgers') ?>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table">
                                                    <tr>
                                                        <th style="width: 30%"></th>
                                                        <th style="width: 35%" class="text-center"><?= lang('purchases') ?></th>
                                                        <th style="width: 35%" class="text-center"><?= lang('sales') ?></th>
                                                    </tr>
                                                    <?php if ($tax_rate_traslate): ?>
                                                        <?php foreach ($tax_rate_traslate as $trt): ?>
                                                            <tr>
                                                                <?= form_hidden('trt_id[]', $trt->id); ?>
                                                                <th><?= lang($trt->type.'_ledger_id') ?></th>
                                                                <td><?= form_dropdown($trt->type.'_payment_ledger_id', $lopts, $trt->payment_ledger_id, 'class="form-control" id="'.$trt->type.'_payment_ledger_id" style="width:100%;" required="required"'); ?></td>
                                                                <td><?= form_dropdown($trt->type.'_receipt_ledger_id', $lopts, $trt->receipt_ledger_id, 'class="form-control" id="'.$trt->type.'_receipt_ledger_id" style="width:100%;" required="required"'); ?></td>
                                                            </tr>
                                                        <?php endforeach ?>
                                                    <?php else: ?>
                                                        <tr>
                                                            <th><?= lang('tax_rate_traslate_ledger_id') ?></th>
                                                            <td><?= form_dropdown('tax_rate_traslate_payment_ledger_id', $lopts, '', 'class="form-control" id="tax_rate_traslate_payment_ledger_id" style="width:100%;" required="required"'); ?></td>
                                                            <td><?= form_dropdown('tax_rate_traslate_receipt_ledger_id', $lopts, '', 'class="form-control" id="tax_rate_traslate_receipt_ledger_id" style="width:100%;" required="required"'); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><?= lang('trm_difference_ledger_id') ?></th>
                                                            <td><?= form_dropdown('trm_difference_payment_ledger_id', $lopts, '', 'class="form-control" id="trm_difference_payment_ledger_id" style="width:100%;" required="required"'); ?></td>
                                                            <td><?= form_dropdown('trm_difference_receipt_ledger_id', $lopts, '', 'class="form-control" id="trm_difference_receipt_ledger_id" style="width:100%;" required="required"'); ?></td>
                                                        </tr>
                                                    <?php endif ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <?= lang('cost_center_selection', 'cost_center_selection') ?>
                                        <?php

                                        $ccsopts = array(
                                                        '0' => lang('default_biller_cost_center'),
                                                        '1' => lang('select_cost_center'),
                                                        '2' => lang('disable'),
                                                        );

                                         ?>
                                        <?= form_dropdown('cost_center_selection', $ccsopts, $Settings->cost_center_selection, 'class="form-control tip" id="cost_center_selection" required="required"'); ?>
                                    </div>
                                    <div class="col-md-4 col-sm-4 default_cost_center" style="<?= $Settings->cost_center_selection != 2 ? "" : "display: none ;"; ?>">
                                        <?= lang('default_cost_center', 'default_cost_center') ?>
                                        <?php
                                        $ccopts[''] = lang('select');
                                        if ($cost_centers) {
                                            foreach ($cost_centers as $cost_center) {
                                                $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                            }
                                        }
                                         ?>
                                         <?= form_dropdown('default_cost_center', $ccopts, $Settings->default_cost_center, 'id="default_cost_center" class="form-control input-tip select" style="width:100%;" '); ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('customer_default_country'), 'customer_default_country'); ?>
                                <select name="customer_default_country" id="customer_default_country" class="form-control select">
                                    <option value=""><?= lang('select'); ?></option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= (isset($Settings) && $country->NOMBRE == $Settings->customer_default_country) ? 'selected' : '' ?>>   <?= $country->NOMBRE; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('customer_default_state'), 'customer_default_state'); ?>
                                <?php $state_options = [''=>lang('select')]; ?>
                                <?= form_dropdown(['name'=>'customer_default_state', 'id'=>'customer_default_state', 'class'=>'form-control select'], $state_options); ?>
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('customer_default_city'), 'customer_default_city'); ?>
                                <?php $city_options = [''=>lang('select')]; ?>
                                <?= form_dropdown(['name'=>'customer_default_city', 'id'=>'customer_default_city', 'class'=>'form-control select'], $city_options); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('default_records_filter'), 'default_records_filter'); ?>
                                <select name="default_records_filter" id="default_records_filter" class="form-control select" required="true">
                                    <option value="">Seleccione...</option>
                                    <?= $this->sma->get_filter_options(); ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <?= lang('max_num_results_display', 'max_num_results_display') ?>
                                <input type="text" name="max_num_results_display" id="max_num_results_display" class="form-control only_number" value="<?= $this->Settings->max_num_results_display ?>">
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('second_tax_management'), 'ipoconsumo'); ?><i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('second_tax_management_tooltip') ?>"></i>
                                <?php $ipoconsumo_opts = [
                                                            '0'=>lang('no'),
                                                            '2'=>lang('purchases'),
                                                            '3'=>lang('sales'),
                                                            '1'=>lang('purchases_and_sales'),
                                                            // '5'=>lang('records_date_range'),
                                                        ]; ?>
                                <?= form_dropdown(['name'=>'ipoconsumo', 'id'=>'ipoconsumo', 'class'=>'form-control select', 'required'=>TRUE], $ipoconsumo_opts, $Settings->ipoconsumo); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('manual_payment_reference'), 'manual_payment_reference'); ?>
                                <?php $manual_payment_reference_opts = [
                                                            '0'=>lang('no'),
                                                            '1'=>lang('yes'),
                                                            // '5'=>lang('records_date_range'),
                                                        ]; ?>
                                <?= form_dropdown(['name'=>'manual_payment_reference', 'id'=>'manual_payment_reference', 'class'=>'form-control select', 'required'=>TRUE], $manual_payment_reference_opts, $Settings->manual_payment_reference); ?>
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('self_withholding_percentage'), 'self_withholding_percentage'); ?>
                                <?php $self_withholding_percentage_opts = [
                                                            $this->sma->formatDecimal(0) => $this->sma->formatDecimal(0,2),
                                                            $this->sma->formatDecimal(0.4) => $this->sma->formatDecimal(0.4,2),
                                                            $this->sma->formatDecimal(0.55) => $this->sma->formatDecimal(0.55,2),
                                                            $this->sma->formatDecimal(0.8) => $this->sma->formatDecimal(0.8,2),
                                                            $this->sma->formatDecimal(1.1) => $this->sma->formatDecimal(1.1,2),
                                                            $this->sma->formatDecimal(1.5) => $this->sma->formatDecimal(1.5,2),
                                                            $this->sma->formatDecimal(1.6) => $this->sma->formatDecimal(1.6,2),
                                                        ]; ?>
                                <?= form_dropdown(['name'=>'self_withholding_percentage', 'id'=>'self_withholding_percentage', 'class'=>'form-control select', 'required'=>TRUE], $self_withholding_percentage_opts, $Settings->self_withholding_percentage); ?>
                            </div>
                        </div>
                            <?php if ($this->Owner): ?>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= form_label(lang('automatic_update'), 'automatic_update'); ?>
                                    <?php $automatic_update_opts = [
                                                                '0'=>lang('no'),
                                                                '1'=>lang('yes'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'automatic_update', 'id'=>'automatic_update', 'class'=>'form-control select', 'required'=>TRUE], $automatic_update_opts, $Settings->automatic_update); ?>
                                </div>
                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= form_label(lang('annual_closure'), 'annual_closure'); ?>
                                    <?php $annual_closure_opts = [
                                                                '0'=>lang('no'),
                                                                '1'=>lang('yes'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'annual_closure', 'id'=>'annual_closure', 'class'=>'form-control select', 'required'=>TRUE], $annual_closure_opts, $Settings->annual_closure); ?>
                                </div>
                                <div class="col-md-4">
                                    <?= lang('system_start_date', 'system_start_date') ?>
                                    <input type="date" name="system_start_date" id="system_start_date" class="form-control" value="<?= $this->Settings->system_start_date ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= form_label(lang('block_movements_from_another_year'), 'block_movements_from_another_year'); ?>
                                    <?php $block_movements_from_another_year_opts = [
                                                                '0'=>lang('no'),
                                                                '1'=>lang('yes'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'block_movements_from_another_year', 'id'=>'block_movements_from_another_year', 'class'=>'form-control select', 'required'=>TRUE], $block_movements_from_another_year_opts, $Settings->block_movements_from_another_year); ?>
                                </div>
                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= form_label(lang('barcode_reader_exact_search'), 'barcode_reader_exact_search'); ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('barcode_reader_exact_search_warning') ?>"></i>
                                    <?php $barcode_reader_exact_search_opts = [
                                                                '0'=>lang('no'),
                                                                '1'=>lang('yes'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'barcode_reader_exact_search', 'id'=>'barcode_reader_exact_search', 'class'=>'form-control select', 'required'=>TRUE], $barcode_reader_exact_search_opts, $Settings->barcode_reader_exact_search); ?>
                                </div>
                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= form_label(lang('big_data_limit_reports'), 'big_data_limit_reports'); ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('big_data_limit_reports_warning') ?>"></i>
                                    <?php $big_data_limit_reports_opts = [
                                                                '0'=>lang('no'),
                                                                '1'=>lang('yes'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'big_data_limit_reports', 'id'=>'big_data_limit_reports', 'class'=>'form-control select', 'required'=>TRUE], $big_data_limit_reports_opts, $Settings->big_data_limit_reports); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= form_label(lang('years_database_management'), 'years_database_management'); ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('years_database_management_warning') ?>"></i>
                                    <?php $years_database_management_opts = [
                                                                '1'=>lang('years_in_same_database'),
                                                                '2'=>lang('databases_per_years'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'years_database_management', 'id'=>'years_database_management', 'class'=>'form-control select', 'required'=>TRUE], $years_database_management_opts, $Settings->years_database_management); ?>
                                </div>
                                <div class="col-md-4">
                                    <?= lang('email_new_order_sale_notification', 'email_new_order_sale_notification') ?>
                                    <input type="email" name="email_new_order_sale_notification" id="email_new_order_sale_notification" class="form-control" value="<?= $this->Settings->email_new_order_sale_notification ?>">
                                </div>
                                <div class="col-md-4">
                                    <?= lang('delivery_day_max_orders', 'delivery_day_max_orders') ?>
                                    <input type="number" name="delivery_day_max_orders" id="delivery_day_max_orders" class="form-control only_number" value="<?= $this->Settings->delivery_day_max_orders ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?= lang('hotel_module_management', 'hotel_module_management') ?>
                                    <?php $hotel_module_management_opts = [
                                                                '0'=>lang('inactive'),
                                                                '1'=>lang('active'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'hotel_module_management', 'id'=>'hotel_module_management', 'class'=>'form-control select', 'required'=>TRUE], $hotel_module_management_opts, $Settings->hotel_module_management); ?>
                                </div>
                                <div class="col-md-4">
                                    <?= lang('wappsi_customer_address_id', 'wappsi_customer_address_id') ?>
                                    <input type="number" name="wappsi_customer_address_id" id="wappsi_customer_address_id" class="form-control only_number" value="<?= $this->Settings->wappsi_customer_address_id ?>">
                                </div>
                            </div>
                            <?php endif ?>

                        <div class="row">
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= lang('gift_card_text', 'gift_card_text') ?>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="gift_card_language" class="form-control" placeholder="<?= lang('gift_card_language') ?>" value="<?= $this->Settings->gift_card_language ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="gift_cards_language" class="form-control" placeholder="<?= lang('gift_cards_language') ?>" value="<?= $this->Settings->gift_cards_language ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('pos_register_hide_closing_detail'), 'pos_register_hide_closing_detail'); ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('pos_register_hide_closing_detail_warning') ?>"></i>
                                <?php $pos_register_hide_closing_detail_opts = [
                                                            '0'=>lang('hide_detail_show_final_cash'),
                                                            '1'=>lang('hide_detail_hide_final_cash'),
                                                            '2'=>lang('show_detail_show_final_cash'),
                                                            '3'=>lang('show_detail_hide_final_cash'),
                                                            // '5'=>lang('records_date_range'),
                                                        ]; ?>
                                <?= form_dropdown(['name'=>'pos_register_hide_closing_detail', 'id'=>'pos_register_hide_closing_detail', 'class'=>'form-control select', 'required'=>TRUE], $pos_register_hide_closing_detail_opts, $Settings->pos_register_hide_closing_detail); ?>
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('order_sale_notification'), 'order_sale_notification'); ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="<?= lang('change_sound_to_reproduce') ?>" style="cursor: pointer"></i>
                                <div class="row">
                                    <div class="col-sm-8">
                                         <?php
                                          $system_audios[0] = lang('no_audio');
                                          foreach ($audio_files as $key => $value) {
                                              $system_audios[$value] = $value;
                                          }
                                          echo form_dropdown('order_sale_notification', $system_audios, $Settings->order_sale_notification, 'class="form-control" id="order_sale_notification"'); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>
                                            <input type="checkbox" name="repeat_order_sale_notification" class="form-control" <?= $Settings->repeat_order_sale_notification == 1 ? 'checked' : '' ?>>
                                            <?= lang('repeat') ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="<?= lang('repeat_order_sale_notification_tooltip') ?>" style="cursor: pointer"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('pos_order_preparation_notification'), 'pos_order_preparation_notification'); ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="<?= lang('change_sound_to_reproduce') ?>" style="cursor: pointer"></i>

                                <div class="row">
                                    <div class="col-sm-8">
                                        <?php
                                        $system_audios[0] = lang('no_audio');
                                        foreach ($audio_files as $key => $value) {
                                          $system_audios[$value] = $value;
                                        }
                                        echo form_dropdown('pos_order_preparation_notification', $system_audios, $Settings->pos_order_preparation_notification, 'class="form-control" id="pos_order_preparation_notification"'); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>
                                            <input type="checkbox" name="repeat_pos_order_preparation_notification" class="form-control" <?= $Settings->repeat_pos_order_preparation_notification == 1 ? 'checked' : '' ?>>
                                            <?= lang('repeat') ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="<?= lang('repeat_pos_order_preparation_notification_tooltip') ?>" style="cursor: pointer"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <?= lang('days_to_new_product', 'days_to_new_product') ?>
                                <input type="text" name="days_to_new_product" id="days_to_new_product" class="form-control only_number" value="<?= $Settings->days_to_new_product ?>">
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= form_label(lang('cost_to_profit_and_validation'), 'cost_to_profit_and_validation'); ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('cost_to_profit_and_validation_warning') ?>"></i>
                                <?php $cost_to_profit_and_validation_opts = [
                                                            '1'=>lang('last_cost'),
                                                            '2'=>lang('avg_cost'),
                                                            // '5'=>lang('records_date_range'),
                                                        ]; ?>
                                <?= form_dropdown(['name'=>'cost_to_profit_and_validation', 'id'=>'cost_to_profit_and_validation', 'class'=>'form-control select', 'required'=>TRUE], $cost_to_profit_and_validation_opts, $Settings->cost_to_profit_and_validation); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <?= form_label(lang('variant_code_search'), 'variant_code_search'); ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('variant_code_search_warning') ?>"></i>
                                <?php $variant_code_search_opts = [
                                                            '1'=>lang('yes'),
                                                            '0'=>lang('no'),
                                                            // '5'=>lang('records_date_range'),
                                                        ]; ?>
                                <?= form_dropdown(['name'=>'variant_code_search', 'id'=>'variant_code_search', 'class'=>'form-control select', 'required'=>TRUE], $variant_code_search_opts, $Settings->variant_code_search); ?>
                            </div>

                            <?php if ($this->Owner) { ?>
                                <div class="col-md-4">
                                    <?= form_label($this->lang->line("data_synchronization_to_store"), "data_synchronization_to_store") ?>
                                    <?= form_dropdown(["name"=>"data_synchronization_to_store", "id"=>"data_synchronization_to_store", "class"=>"form-control select", "required"=>TRUE], ["1"=>$this->lang->line("active"), "0"=>$this->lang->line("inactive")], $Settings->data_synchronization_to_store) ?>
                                </div>
                            <?php } ?>
                            <?php if ($this->Owner): ?>
                                <div class="col-md-4">
                                    <?= form_label(lang('show_pos_movements_close_register'), 'show_pos_movements_close_register'); ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('show_pos_movements_close_register_warning') ?>"></i>
                                    <?php $show_pos_movements_close_register_opts = [
                                                                '1'=>lang('yes'),
                                                                '0'=>lang('no'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'show_pos_movements_close_register', 'id'=>'show_pos_movements_close_register', 'class'=>'form-control select', 'required'=>TRUE], $show_pos_movements_close_register_opts, $Settings->show_pos_movements_close_register); ?>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= lang('brand_language', 'brand_language') ?>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="brand_language" class="form-control" placeholder="<?= lang('brand_language') ?>" value="<?= $this->Settings->brand_language ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="brands_language" class="form-control" placeholder="<?= lang('brands_language') ?>" value="<?= $this->Settings->brands_language ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= lang('category_language', 'category_language') ?>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="category_language" class="form-control" placeholder="<?= lang('category_language') ?>" value="<?= $this->Settings->category_language ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="categories_language" class="form-control" placeholder="<?= lang('categories_language') ?>" value="<?= $this->Settings->categories_language ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= lang('subcategory_language', 'subcategory_language') ?>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="subcategory_language" class="form-control" placeholder="<?= lang('subcategory_language') ?>" value="<?= $this->Settings->subcategory_language ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="subcategories_language" class="form-control" placeholder="<?= lang('subcategories_language') ?>" value="<?= $this->Settings->subcategories_language ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= lang('subsubcategory_language', 'subsubcategory_language') ?>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="subsubcategory_language" class="form-control" placeholder="<?= lang('subsubcategory_language') ?>" value="<?= $this->Settings->subsubcategory_language ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="subsubcategories_language" class="form-control" placeholder="<?= lang('subsubcategories_language') ?>" value="<?= $this->Settings->subsubcategories_language ?>">
                                    </div>
                                </div>
                            </div>
                            <?php if ($this->Owner): ?>
                                <div class="col-md-4">
                                    <?= form_label(lang('imports_module'), 'imports_module'); ?>
                                    <?php $imports_module_opts = [
                                                                '1'=>lang('yes'),
                                                                '0'=>lang('no'),
                                                                // '5'=>lang('records_date_range'),
                                                            ]; ?>
                                    <?= form_dropdown(['name'=>'imports_module', 'id'=>'imports_module', 'class'=>'form-control select', 'required'=>TRUE], $imports_module_opts, $Settings->imports_module); ?>
                                </div>
                            <?php endif ?>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= lang('payments_collections_language', 'payments_collections_language') ?>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="payment_collection_language" class="form-control" placeholder="<?= lang('payment_collection_language') ?>" value="<?= $this->Settings->payment_collection_language ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="payments_collections_language" class="form-control" placeholder="<?= lang('payments_collections_language') ?>" value="<?= $this->Settings->payments_collections_language ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <?= form_label(sprintf(lang('payment_collection_webservice'), lang('payment_collection')), 'payment_collection_webservice'); ?>
                                <?php $payment_collection_webservice_opts = [
                                                            '1'=>lang('yes'),
                                                            '0'=>lang('no'),
                                                            // '5'=>lang('records_date_range'),
                                                        ]; ?>
                                <?= form_dropdown(['name'=>'payment_collection_webservice', 'id'=>'payment_collection_webservice', 'class'=>'form-control select', 'required'=>TRUE], $payment_collection_webservice_opts, $Settings->payment_collection_webservice); ?>
                            </div>
                            <div class="col-md-4 col-sm-12 form-group">
                                <?= form_label(lang('force_default_client_in_branch'), 'force_default_client_in_branch'); ?>
                                <?php $force_default_client_in_branch_opts = [
                                                                '1'=>lang('yes'),
                                                                '0'=>lang('no'),
                                                            ];
                                ?>
                                <?= form_dropdown(['name'=>'force_default_client_in_branch', 'id'=>'force_default_client_in_branch', 'class'=>'form-control select', 'required'=>TRUE], $force_default_client_in_branch_opts, $Settings->force_default_client_in_branch); ?>
                            </div>
                            <div class="col-md-4 col-sm-12 form-group">
                                <?= form_label(lang('allow_returns_date_before_current_date'), 'allow_returns_date_before_current_date'); ?>
                                <?php $allow_returns_date_before_current_date_opts = [
                                                                '1'=>lang('yes'),
                                                                '0'=>lang('no'),
                                                            ];
                                ?>
                                <?= form_dropdown(['name'=>'allow_returns_date_before_current_date', 'id'=>'allow_returns_date_before_current_date', 'class'=>'form-control select', 'required'=>TRUE], $allow_returns_date_before_current_date_opts, $Settings->allow_returns_date_before_current_date); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <?= form_label(lang('Tipo de tercero para ajustes y traslados'), 'companies_for_adjustments_transfers') ?>
                                <?php
                                    if (isset($groups) && !empty($groups)) {
                                        $gropts = [];
                                        foreach ($groups as $group) {
                                            if (in_array($group->name, ['seller', 'supplier', 'employee', 'customer'])) {
                                                $gropts[$group->name] = lang($group->name);
                                            }
                                        }
                                    }
                                    $companies_for_adjustments_transfers = isset($Settings->companies_for_adjustments_transfers) && !empty($Settings->companies_for_adjustments_transfers) ? $Settings->companies_for_adjustments_transfers : '';
                                ?>
                                <?= form_dropdown(['name'=>'companies_for_adjustments_transfers', 'id'=>'companies_for_adjustments_transfers', 'class'=>'form-control'], $gropts, $companies_for_adjustments_transfers) ?>
                            </div>
                            <div class="col-md-4 col-sm-12 form-group">
                                <?= form_label(lang('include_pending_order_quantity_stock'), 'include_pending_order_quantity_stock'); ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="<?= lang('include_pending_order_quantity_stock_tooltip') ?>" style="cursor: pointer"></i>
                                <?php $include_pending_order_quantity_stock_opts = [
                                                                '1'=>lang('yes'),
                                                                '0'=>lang('no'),
                                                            ];
                                ?>
                                <?= form_dropdown(['name'=>'include_pending_order_quantity_stock', 'id'=>'include_pending_order_quantity_stock', 'class'=>'form-control select', 'required'=>TRUE], $include_pending_order_quantity_stock_opts, $Settings->include_pending_order_quantity_stock); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5><?= lang('products') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang("product_tax", "tax_rate"); ?>
                                    <?php
                                    $tr['1'] = lang("select");
                                    foreach ($tax_rates as $rate) {
                                        $tr[$rate->id] = $rate->name;
                                    }
                                    echo form_dropdown('tax_rate', $tr, $Settings->default_tax_rate, 'class="form-control tip" id="tax_rate" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="racks"><?= lang("racks"); ?></label>

                                    <div class="controls">
                                        <?php
                                        echo form_dropdown('racks', $ps, $Settings->racks, 'id="racks" class="form-control tip" required="required" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="attributes"><?= lang("attributes"); ?></label>

                                    <div class="controls">
                                        <?php
                                        echo form_dropdown('attributes', $ps, $Settings->attributes, 'id="attributes" class="form-control tip"  required="required" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"
                                        for="product_expiry"><?= lang("product_expiry"); ?></label>

                                    <div class="controls">
                                        <?php
                                        echo form_dropdown('product_expiry', $ps, $Settings->product_expiry, 'id="product_expiry" class="form-control tip" required="required" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"
                                        for="remove_expired"><?= lang("remove_expired"); ?></label>

                                    <div class="controls">
                                        <?php
                                        $re_opts = array(0 => lang('no').', '.lang('i_ll_remove'), 1 => lang('yes').', '.lang('remove_automatically'));
                                        echo form_dropdown('remove_expired', $re_opts, $Settings->remove_expired, 'id="remove_expired" class="form-control tip" required="required" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="image_size"><?= lang("image_size"); ?> (Width :
                                        Height) *</label>

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <?= form_input('iwidth', $Settings->iwidth, 'class="form-control tip" id="iwidth" placeholder="image width" required="required"'); ?>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= form_input('iheight', $Settings->iheight, 'class="form-control tip" id="iheight" placeholder="image height" required="required"'); ?></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="thumbnail_size"><?= lang("thumbnail_size"); ?>
                                        (Width : Height) *</label>

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <?= form_input('twidth', $Settings->twidth, 'class="form-control tip" id="twidth" placeholder="thumbnail width" required="required"'); ?>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= form_input('theight', $Settings->theight, 'class="form-control tip" id="theight" placeholder="thumbnail height" required="required"'); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('watermark', 'watermark'); ?>
                                    <?php
                                        echo form_dropdown('watermark', $wm, (isset($_POST['watermark']) ? $_POST['watermark'] : $Settings->watermark), 'class="tip form-control" required="required" id="watermark" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('display_all_products', 'display_all_products'); ?>
                                    <?php
                                        $dopts = array(0 => lang('hide_with_0_qty'), 1 => lang('show_with_0_qty'));
                                        echo form_dropdown('display_all_products', $dopts, (isset($_POST['display_all_products']) ? $_POST['display_all_products'] : $Settings->display_all_products), 'class="tip form-control" required="required" id="display_all_products" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('barcode_separator', 'barcode_separator'); ?>
                                    <?php
                                        $bcopts = array('-' => lang('dash'), '.' => lang('dot'), '~' => lang('tilde'), '_' => lang('underscore'));
                                        echo form_dropdown('barcode_separator', $bcopts, (isset($_POST['barcode_separator']) ? $_POST['barcode_separator'] : $Settings->barcode_separator), 'class="tip form-control" required="required" id="barcode_separator" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('barcode_renderer', 'barcode_renderer'); ?>
                                    <?php
                                        $bcropts = array(1 => lang('image'), 0 => lang('svg'));
                                        echo form_dropdown('barcode_renderer', $bcropts, (isset($_POST['barcode_renderer']) ? $_POST['barcode_renderer'] : $Settings->barcode_img), 'class="tip form-control" required="required" id="barcode_renderer" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('update_cost_with_purchase', 'update_cost'); ?>
                                    <?= form_dropdown('update_cost', $wm, $Settings->update_cost, 'class="form-control" id="update_cost" required="required"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?php
                                    // $tax_method_options = array('1' => lang('exclusive'), '0' => lang('inclusive'), '2' => lang('select_each_product'));
                                    $tax_method_options = array('1' => lang('exclusive'), '0' => lang('inclusive'));
                                     ?>
                                    <?= lang('tax_method', 'tax_method'); ?>
                                    <?= form_dropdown('tax_method', $tax_method_options, $Settings->tax_method, 'class="form-control" id="tax_method" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?php
                                    $sacost = array('1' => lang('yes'), '0' => lang('no'));
                                     ?>
                                    <?= lang('set_adjustment_cost', 'set_adjustment_cost'); ?>
                                    <?= form_dropdown('set_adjustment_cost', $sacost, $Settings->set_adjustment_cost, 'class="form-control" id="set_adjustment_cost" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?php
                                    $sacost = array('1' => lang('yes'), '0' => lang('no'));
                                     ?>
                                    <?= lang('show_brand_in_product_search', 'show_brand_in_product_search'); ?>
                                    <?= form_dropdown('show_brand_in_product_search', $sacost, $Settings->show_brand_in_product_search, 'class="form-control" id="show_brand_in_product_search" required="required"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('users_can_view_price_groups', 'users_can_view_price_groups') ?>
                                    <select name="users_can_view_price_groups" id="users_can_view_price_groups" class="form-control">
                                        <option value="0" <?= $this->Settings->users_can_view_price_groups == 0 ? 'selected="selected"' : '' ?>><?= lang('users_can_view_price_groups_assigned') ?></option>
                                        <option value="1" <?= $this->Settings->users_can_view_price_groups == 1 ? 'selected="selected"' : '' ?>><?= lang('users_can_view_price_groups_all') ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('users_can_view_warehouse_quantitys', 'users_can_view_warehouse_quantitys') ?>
                                    <select name="users_can_view_warehouse_quantitys" id="users_can_view_warehouse_quantitys" class="form-control">
                                        <option value="0" <?= $this->Settings->users_can_view_warehouse_quantitys == 0 ? 'selected="selected"' : '' ?>><?= lang('users_can_view_warehouse_quantitys_assigned') ?></option>
                                        <option value="1" <?= $this->Settings->users_can_view_warehouse_quantitys == 1 ? 'selected="selected"' : '' ?>><?= lang('users_can_view_warehouse_quantitys_all') ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('product_order', 'product_order') ?>
                                    <select name="product_order" id="product_order" class="form-control">
                                        <option value="1" <?= $this->Settings->product_order == 1 ? 'selected="selected"' : '' ?>><?= lang('product_order_descending') ?></option>
                                        <option value="2" <?= $this->Settings->product_order == 2 ? 'selected="selected"' : '' ?>><?= lang('product_order_ascending') ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 form-group">
                                <?= lang('product_variants_text', 'product_variants_text') ?>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="product_variant_language" class="form-control" placeholder="<?= lang('product_variant_language') ?>" value="<?= $this->Settings->product_variant_language ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="product_variants_language" class="form-control" placeholder="<?= lang('product_variants_language') ?>" value="<?= $this->Settings->product_variants_language ?>">
                                    </div>
                                </div>
                            </div>
                            <?php if ($this->Owner): ?>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?= lang('product_variant_per_serial', 'product_variant_per_serial'); ?>
                                        <?= form_dropdown('product_variant_per_serial', $wm, $Settings->product_variant_per_serial, 'class="form-control" id="product_variant_per_serial" required="required"'); ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?= lang('product_search_show_quantity', 'product_search_show_quantity'); ?>
                                        <?= form_dropdown('product_search_show_quantity', $wm, $Settings->product_search_show_quantity, 'class="form-control" id="product_search_show_quantity" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?= lang('production_order_one_reference_limit', 'production_order_one_reference_limit'); ?>
                                        <?= form_dropdown('production_order_one_reference_limit', $wm, $Settings->production_order_one_reference_limit, 'class="form-control" id="production_order_one_reference_limit" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?= lang('set_product_variant_suffix', 'set_product_variant_suffix'); ?>
                                        <?= form_dropdown('set_product_variant_suffix', $wm, $Settings->set_product_variant_suffix, 'class="form-control" id="set_product_variant_suffix" required="required"'); ?>
                                    </div>
                                </div>
                            <!-- Terminan los campos de Productos -->
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?php
                                    $sacost = array('1' => lang('yes'), '0' => lang('no'));
                                     ?>
                                    <?= lang('wms_picking_filter_biller', 'wms_picking_filter_biller'); ?>
                                    <?= form_dropdown('wms_picking_filter_biller', $sacost, $Settings->wms_picking_filter_biller, 'class="form-control" id="wms_picking_filter_biller" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?php
                                    $sacost = array('1' => lang('yes'), '0' => lang('no'));
                                     ?>
                                    <?= lang('enable_customer_tax_exemption', 'enable_customer_tax_exemption'); ?>
                                    <?= form_dropdown('enable_customer_tax_exemption', $sacost, $Settings->enable_customer_tax_exemption, 'class="form-control" id="enable_customer_tax_exemption" required="required"'); ?>
                                </div>
                            </div>
                            <?php if ($this->Owner): ?>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?php
                                        $sacost = array('1' => lang('yes'), '0' => lang('no'));
                                         ?>
                                        <?= lang('disable_product_price_under_cost', 'disable_product_price_under_cost'); ?>
                                        <?= form_dropdown('disable_product_price_under_cost', $sacost, $Settings->disable_product_price_under_cost, 'class="form-control" id="disable_product_price_under_cost" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?php
                                        $sacost = array('1' => lang('yes'), '0' => lang('no'));
                                         ?>
                                        <?= lang('product_transformation_validate_quantity', 'product_transformation_validate_quantity'); ?>
                                        <?= form_dropdown('product_transformation_validate_quantity', $sacost, $Settings->product_transformation_validate_quantity, 'class="form-control" id="product_transformation_validate_quantity" required="required"'); ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('lock_cost_field_in_product', 'lock_cost_field_in_product'); ?>
                                <?= form_dropdown('lock_cost_field_in_product', $ppopts, $Settings->lock_cost_field_in_product, 'id="lock_cost_field_in_product" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('product_crud_validate_cost', 'product_crud_validate_cost'); ?>
                                <?= form_dropdown('product_crud_validate_cost', $ppopts, $Settings->product_crud_validate_cost, 'id="product_crud_validate_cost" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('product_preferences_management', 'product_preferences_management'); ?>
                                <?= form_dropdown('product_preferences_management', $ppopts, $Settings->product_preferences_management, 'id="product_preferences_management" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= lang("gift_card_product", "suggest_product"); ?>
                                    <?php echo form_input('sproduct', ($gift_card_product ? $gift_card_product->name." (".$gift_card_product->code.")" : ""), 'class="form-control" id="suggest_product" data-type="service"'); ?>
                                    <input type="hidden" name="gift_card_product_id" value="<?= ($gift_card_product ? $gift_card_product->id : "") ?>" id="report_product_id"/>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('transformation_product_reference_restriction', 'transformation_product_reference_restriction'); ?>
                                <?= form_dropdown('transformation_product_reference_restriction', $ppopts, $Settings->transformation_product_reference_restriction, 'id="transformation_product_reference_restriction" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array(  '0' => lang('no'),
                                                        '2' => lang('yes_same_server'),
                                                        '1' => lang('yes_diferent_server')
                                                    ); ?>
                                <?= lang('images_from_tpro', 'images_from_tpro'); ?>
                                <?= form_dropdown('images_from_tpro', $ppopts, $Settings->images_from_tpro, 'id="images_from_tpro" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array(
                                                    '' => lang('select').' '.lang('style'),
                                                    'zebra_32x25' => lang('zebra_32x25'),
                                                    'zebra_32x25_standard' => lang('zebra_32x25_standard'),
                                                    'zebra_50x25' => lang('zebra_50x25'),
                                                    'zebra_50x25_standard' => lang('zebra_50x25_standard'),
                                                    'zebra_100x50_standard' => lang('zebra_100x50_standard'),
                                                    'zebra_7_5x1_standard' => lang('zebra_7_5x1_standard'),
                                                    'ticket_8x4' => lang('ticket_8x4'),
                                                    'zebra_32x25_moda_variants' => lang('zebra_32x25_moda_variants'),
                                                    'zebra_7_2x1_standard' => lang('zebra_7_2x1_standard'),
                                                    'zebra_32x15_standard' => lang('zebra_32x15_standard'),
                                                ); ?>
                                <?= lang('default_zebra_print', 'default_zebra_print'); ?>
                                <?= form_dropdown('default_zebra_print', $ppopts, $Settings->default_zebra_print, 'id="default_zebra_print" class="form-control tip" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('automatic_loading_of_existence_in_transformation', 'automatic_loading_of_existence_in_transformation'); ?>
                                <?= form_dropdown('automatic_loading_of_existence_in_transformation', $ppopts, $Settings->automatic_loading_of_existence_in_transformation, 'id="automatic_loading_of_existence_in_transformation" class="form-control tip" required="required" style="width:100%;"'); ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('allow_zero_price_products', 'allow_zero_price_products'); ?>
                                <?= form_dropdown('allow_zero_price_products', $ppopts, $Settings->allow_zero_price_products, 'id="allow_zero_price_products" class="form-control tip" required="required" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <!-- <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('management_weight_in_variants', 'management_weight_in_variants'); ?>
                                <?= form_dropdown('management_weight_in_variants', $ppopts, $Settings->management_weight_in_variants, 'id="management_weight_in_variants" class="form-control tip" required="required" style="width:100%;"'); ?>
                            </div> -->
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('handle_jewerly_products', 'handle_jewerly_products'); ?>
                                <?= form_dropdown('handle_jewerly_products', $ppopts, $Settings->handle_jewerly_products, 'id="handle_jewerly_products" class="form-control tip" required="required" style="width:100%;"'); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title"> <h5><?= lang('purchases') ?></h5> </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang("invoice_tax", "purchase_tax_rate"); ?>
                                    <?php $tr['0'] = lang("disable");
                                    foreach ($tax_rates as $rate) {
                                        $tr[$rate->id] = $rate->name;
                                    }
                                    echo form_dropdown('purchase_tax_rate', $tr, $Settings->purchase_tax_rate, 'id="purchase_tax_rate" class="form-control tip" required="required" style="width:100%;"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <?php $dorpts = array('3' => lang('descuento_orden_decidir'),
                                                    '1' => lang('descuento_orden_afecta_iva'),
                                                    '2' => lang('descuento_orden_no_afecta_iva'),
                                                    '4' => lang('descuento_orden_afecta_primer_iva'),
                                                ); ?>
                                <?= lang('descuento_orden', 'descuento_orden'); ?>
                                <?= form_dropdown('descuento_orden', $dorpts, $Settings->descuento_orden, 'id="descuento_orden" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('purchase_payments_never_affects_register'),
                                                    '1' => lang('purchase_payments_always_affects_register'),
                                                    '2' => lang('always_ask_if_purchase_payments_affects_register')); ?>
                                <?= lang('purchase_payment_affects_cash_register', 'purchase_payment_affects_cash_register'); ?>
                                <?= form_dropdown('purchase_payment_affects_cash_register', $ppopts, $Settings->purchase_payment_affects_cash_register, 'id="purchase_payment_affects_cash_register" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('management_consecutive_suppliers', 'management_consecutive_suppliers'); ?>
                                <?= form_dropdown('management_consecutive_suppliers', $ppopts, $Settings->management_consecutive_suppliers, 'id="management_consecutive_suppliers" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang('prorate_shipping_cost', 'prorate_shipping_cost') ?>
                                <select name="prorate_shipping_cost" id="prorate_shipping_cost" class="form-control">
                                    <option value="0" <?= $this->Settings->prorate_shipping_cost == 0 ? "selected='selected'" : "" ?>><?= lang('never_prorate') ?></option>
                                    <option value="1" <?= $this->Settings->prorate_shipping_cost == 1 ? "selected='selected'" : "" ?>><?= lang('always_prorate') ?></option>
                                    <option value="2" <?= $this->Settings->prorate_shipping_cost == 2 ? "selected='selected'" : "" ?>><?= lang('descuento_orden_decidir') ?></option>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang("product_default_exempt_tax_rate", "product_default_exempt_tax_rate"); ?>
                                    <?php
                                    $pdtr[''] = lang("select");
                                    foreach ($tax_rates as $rate) {
                                        if ($rate->rate > 0) {
                                            continue;
                                        }
                                        $pdtr[$rate->id] = $rate->name;
                                    }
                                    echo form_dropdown('product_default_exempt_tax_rate', $pdtr, $Settings->product_default_exempt_tax_rate, 'class="form-control tip" id="product_default_exempt_tax_rate" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php if ($this->Owner): ?>
                                <div class="col-md-4 col-sm-4">
                                    <?php $ppopts = array('0' => lang('no'),
                                                        '1' => lang('yes')); ?>
                                    <?= lang('purchase_send_advertisement', 'purchase_send_advertisement'); ?>
                                    <?= form_dropdown('purchase_send_advertisement', $ppopts, $Settings->purchase_send_advertisement, 'id="purchase_send_advertisement" class="form-control tip" required="required" style="width:100%;"');
                                    ?>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <?php $ppopts = array('0' => lang('no'),
                                                        '1' => lang('yes')); ?>
                                    <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="<?= lang('ignore_purchases_edit_validations_tooltip') ?>" style="cursor: pointer"></i>  <?= lang('ignore_purchases_edit_validations', 'ignore_purchases_edit_validations'); ?>
                                    <?= form_dropdown('ignore_purchases_edit_validations', $ppopts, $Settings->ignore_purchases_edit_validations, 'id="ignore_purchases_edit_validations" class="form-control tip" required="required" style="width:100%;"');
                                    ?>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <?php $ppopts = array('0' => lang('just_date'),
                                                        '1' => lang('datetime')); ?>
                                    <?= lang('purchase_datetime_management', 'purchase_datetime_management'); ?>
                                    <?= form_dropdown('purchase_datetime_management', $ppopts, $Settings->purchase_datetime_management, 'id="purchase_datetime_management" class="form-control tip" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('update_prices_from_purchases', 'update_prices_from_purchases'); ?>
                                <?= form_dropdown('update_prices_from_purchases', $ppopts, $Settings->update_prices_from_purchases, 'id="update_prices_from_purchases" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('1' => 'costo / (1 - (margen / 100))',
                                                    '2' => 'costo * (1 + (margen / 100))'); ?>
                                <?= lang('update_prices_by_margin_formula', 'update_prices_by_margin_formula'); ?>
                                <?= form_dropdown('update_prices_by_margin_formula', $ppopts, $Settings->update_prices_by_margin_formula, 'id="update_prices_by_margin_formula" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('update_product_tax_from_purchase', 'update_product_tax_from_purchase'); ?>
                                <?= form_dropdown('update_product_tax_from_purchase', $ppopts, $Settings->update_product_tax_from_purchase, 'id="update_product_tax_from_purchase" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('purchases_products_supplier_code', 'purchases_products_supplier_code'); ?>
                                <?= form_dropdown('purchases_products_supplier_code', $ppopts, $Settings->purchases_products_supplier_code, 'id="purchases_products_supplier_code" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('withholding_validate_total_base', 'withholding_validate_total_base'); ?>
                                <?= form_dropdown('withholding_validate_total_base', $ppopts, $Settings->withholding_validate_total_base, 'id="withholding_validate_total_base" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $ppopts = array('0' => lang('no'),
                                                    '1' => lang('yes')); ?>
                                <?= lang('manage_purchases_evaluation', 'manage_purchases_evaluation'); ?>
                                <?= form_dropdown('manage_purchases_evaluation', $ppopts, $Settings->manage_purchases_evaluation, 'id="manage_purchases_evaluation" class="form-control tip" required="required" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title"> <h5><?= lang('sales') ?></h5> </div>
                    <div class="ibox-content">
                      <div class="row">
                        <?php if ($this->Owner): ?>
                            <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <label class="control-label" for="overselling"><?= lang("over_selling"); ?>  <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="<?= lang('overselling_warning_tooltip') ?>" style="cursor: pointer"></i></label>

                                  <div class="controls">
                                      <?php
                                      $opt = array(1 => lang('yes'), 0 => lang('no'));
                                      $readonly = "";
                                      $sett_overselling = $Settings->overselling;
                                      // if ($Settings->modulary == 1 ) {
                                      //   $sett_overselling = 0;
                                      // }
                                      echo form_dropdown('restrict_sale', $opt, $sett_overselling, 'class="form-control tip" id="overselling" required="required" style="width:100%;"');
                                      ?>
                                  </div>
                              </div>
                            </div>
                        <?php endif ?>
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <label class="control-label"
                                      for="reference_format"><?= lang("reference_format"); ?></label>

                                  <div class="controls">
                                      <?php
                                      $ref = array(1 => lang('prefix_year_no'), 2 => lang('prefix_month_year_no'), 3 => lang('sequence_number'), 4 => lang('random_number'));
                                      echo form_dropdown('reference_format', $ref, $Settings->reference_format, 'class="form-control tip" required="required" id="reference_format" style="width:100%;"');
                                      ?>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <?= lang("invoice_tax", "tax_rate2"); ?>
                                  <?php $tr['0'] = lang("disable");
                                  foreach ($tax_rates as $rate) {
                                      $tr[$rate->id] = $rate->name;
                                  }
                                  echo form_dropdown('tax_rate2', $tr, $Settings->default_tax_rate2, 'id="tax_rate2" class="form-control tip" required="required" style="width:100%;"'); ?>
                              </div>
                          </div>
                    </div>
                    <div class="row">
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <label class="control-label"
                                      for="product_discount"><?= lang("product_level_discount"); ?></label>

                                  <div class="controls">
                                      <?php
                                      echo form_dropdown('product_discount', $ps, $Settings->product_discount, 'id="product_discount" class="form-control tip" required="required" style="width:100%;"');
                                      ?>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <label class="control-label"
                                      for="product_serial"><?= lang("product_serial"); ?></label>

                                  <div class="controls">
                                      <?php
                                      echo form_dropdown('product_serial', $ps, $Settings->product_serial, 'id="product_serial" class="form-control tip" required="required" style="width:100%;"');
                                      ?>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <label class="control-label"
                                      for="detect_barcode"><?= lang("auto_detect_barcode"); ?></label>

                                  <div class="controls">
                                      <?php
                                      echo form_dropdown('detect_barcode', $ps, $Settings->auto_detect_barcode, 'id="detect_barcode" class="form-control tip" required="required" style="width:100%;"');
                                      ?>
                                  </div>
                              </div>
                          </div>
                    </div>
                    <div class="row">
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <label class="control-label" for="bc_fix"><?= lang("bc_fix"); ?></label>


                                  <?= form_input('bc_fix', $Settings->bc_fix, 'class="form-control tip" required="required" id="bc_fix"'); ?>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <label class="control-label"
                                      for="item_addition"><?= lang("item_addition"); ?></label>

                                  <div class="controls">
                                      <?php
                                      $ia = array(0 => lang('add_new_item'), 1 => lang('increase_quantity_if_item_exist'));
                                      echo form_dropdown('item_addition', $ia, $Settings->item_addition, 'id="item_addition" class="form-control tip" required="required" style="width:100%;"');
                                      ?>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <?= lang("set_focus", "set_focus"); ?>
                                  <?php
                                  $sfopts = array(0 => lang('add_item_input'), 1 => lang('last_order_item'));
                                  echo form_dropdown('set_focus', $sfopts, (isset($_POST['set_focus']) ? $_POST['set_focus'] : $Settings->set_focus), 'id="set_focus" data-placeholder="' . lang("select") . ' ' . lang("set_focus") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                  ?>
                              </div>
                          </div>
                    </div>
                    <div class="row">
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <label class="control-label" for="invoice_view"><?= lang("invoice_view"); ?></label>

                                  <div class="controls">
                                      <?php
                                      $opt_inv = array(1 => lang('tax_invoice'), 0 => lang('standard')/*, 2 => lang('indian_gst')*/);
                                      echo form_dropdown('invoice_view', $opt_inv, $Settings->invoice_view, 'class="form-control tip" required="required" id="invoice_view" style="width:100%;"');
                                      ?>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-4">
                              <div class="form-group">
                                  <?= lang('cashier_close_register', 'cashier_close') ?>

                                  <div class="controls">
                                      <?php
                                      $opt = array(1 => lang('yes'), 0 => lang('no'), 2 => lang('cash_register_not_used'));
                                      echo form_dropdown('cashier_close', $opt, $Settings->cashier_close, 'class="form-control tip" id="cashier_close" required="required" style="width:100%;"');
                                      ?>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-4 form-group">
                              <?= lang('allow_change_sale_iva', 'allow_change_sale_iva') ?>
                              <?php
                              $alch['0'] = 'No';
                              $alch['1'] = 'Si';
                                ?>
                                <?= form_dropdown('allow_change_sale_iva', $alch, $Settings->allow_change_sale_iva, 'class="form-control" id="allow_change_sale_iva" required="required"'); ?>
                          </div>
                          <div class="col-md-4 col-sm-4">
                              <?= lang('resolucion_porc_aviso', 'resolucion_porc_aviso') ?>
                              <?php
                                $opts_remaining_notification_percentage = [
                                    50 => '50%',
                                    40 => '40%',
                                    30 => '30%',
                                    20 => '20%',
                                    10 => '10%',
                                    5 => '5%',
                                    1 => '1%',
                                ];
                              ?>
                              <?= form_dropdown('resolucion_porc_aviso', $opts_remaining_notification_percentage, $Settings->resolucion_porc_aviso, 'class="form-control" id="resolucion_porc_aviso" required="required"'); ?>
                              <!-- <input type="number" name="resolucion_porc_aviso" class="form-control" value="<?= $Settings->resolucion_porc_aviso ?>" step="0.01"> -->
                          </div>
                    </div>
                    <div class="row">

                          <div class="col-md-4 col-sm-4">
                              <?= lang('resolucion_dias_aviso', 'resolucion_dias_aviso') ?>
                              <input type="number" name="resolucion_dias_aviso" class="form-control" value="<?= $Settings->resolucion_dias_aviso ?>">
                          </div>

                          <div class="col-md-4 col-sm-4 form-group">
                              <?= lang("advanced_search", "busqueda_avanzada"); ?>
                              <select class="form-control" name="busqueda_avanzada" id="busqueda_avanzada" required="required">
                                  <option value=""><?= lang("select"); ?></option>
                                  <option value="1" <?= (isset($Settings->allow_advanced_search) && $Settings->allow_advanced_search == "1") ? "selected" : ""; ?>><?= lang("yes"); ?></option>
                                  <option value="0" <?= (isset($Settings->allow_advanced_search) && $Settings->allow_advanced_search == "0") ? "selected" : ""; ?>><?= lang("no"); ?></option>
                              </select>
                          </div>

                          <div class="col-md-4 col-sm-4 form-group">
                              <?= lang('hide_products_in_zero_price', 'hide_products_in_zero_price') ?>
                              <select name="hide_products_in_zero_price" id="hide_products_in_zero_price" class="form-control" required="required">
                                  <option value="0" <?= $this->Settings->hide_products_in_zero_price == '0' ? 'selected="selected"' : '' ?> ><?= lang('show') ?></option>
                                  <option value="1" <?= $this->Settings->hide_products_in_zero_price == '1' ? 'selected="selected"' : '' ?> ><?= lang('hide') ?></option>
                              </select>
                          </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 form-group">
                          <?= lang('show_alert_sale_expired', 'show_alert_sale_expired') ?>
                          <select name="show_alert_sale_expired" id="show_alert_sale_expired" class="form-control">
                              <option value="1" <?= $this->Settings->show_alert_sale_expired == '1' ? 'selected="selected"' : '' ?> ><?= lang('show') ?></option>
                              <option value="0" <?= $this->Settings->show_alert_sale_expired == '0' ? 'selected="selected"' : '' ?> ><?= lang('dont_show') ?></option>
                          </select>
                        </div>
                        <div class="col-md-4 col-sm-4 form-group">
                          <?= lang('alert_sale_expired', 'alert_sale_expired') ?>
                          <input type="number" name="alert_sale_expired" class="form-control" value="<?= $Settings->alert_sale_expired ?>" required>
                        </div>
                        <div class="col-md-4 col-sm-4 form-group">
                          <?= lang('commision_payment_method', 'commision_payment_method') ?>
                          <select name="commision_payment_method" id="commision_payment_method" class="form-control">
                              <option value="1" <?= $this->Settings->commision_payment_method == '1' ? 'selected="selected"' : '' ?> ><?= lang('commision_payment_method_sales_paid') ?></option>
                              <option value="2" <?= $this->Settings->commision_payment_method == '2' ? 'selected="selected"' : '' ?> disabled><?= lang('commision_payment_method_sales_partial') ?></option>
                          </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 form-group">
                            <?= lang('default_expense_id_to_pay_commisions', 'default_expense_id_to_pay_commisions') ?>
                            <?php
                                $catopt[''] = lang('select');

                                if ($expense_categories) {
                                    foreach ($expense_categories as $excat) {
                                       $catopt[$excat->id] = $excat->name;
                                    }
                                }
                                echo form_dropdown('default_expense_id_to_pay_commisions', $catopt, $this->Settings->default_expense_id_to_pay_commisions, 'class="form-control" id="default_expense_id_to_pay_commisions"');
                             ?>
                        </div>
                        <div class="col-md-4 col-sm-4 form-group">
                            <?= lang('validate_minimun_sales_price', 'validate_minimun_sales_price') ?>
                            <?php
                                $pmv_opt = [
                                                '' => lang('no_validate'),
                                                '1' => lang('category'),
                                                '2' => lang('subcategory'),
                                                '3' => lang('minimun_list'),
                                            ];
                                echo form_dropdown('profitability_margin_validation', $pmv_opt, $this->Settings->profitability_margin_validation, 'class="form-control" id="profitability_margin_validation"');
                             ?>
                        </div>
                        <div class="col-md-4 col-sm-4 form-group div_margin_wc" style="display:none;">
                            <?= lang('which_cost_margin_validation', 'which_cost_margin_validation') ?>
                            <?php
                                $pmv_opt = [
                                                '1' => lang('product_cost'),
                                                '2' => lang('avg_cost'),
                                            ];
                                echo form_dropdown('which_cost_margin_validation', $pmv_opt, $this->Settings->which_cost_margin_validation, 'class="form-control" id="which_cost_margin_validation"');
                             ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang("keep_seller_from_user", "keep_seller_from_user"); ?>
                                <select class="form-control" name="keep_seller_from_user" id="keep_seller_from_user">
                                    <option value="1" <?= $this->Settings->keep_seller_from_user == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                    <option value="0" <?= $this->Settings->keep_seller_from_user == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 form-group">
                            <?= lang('invoice_values_greater_than_zero', 'invoice_values_greater_than_zero') ?>
                            <select class="form-control" name="invoice_values_greater_than_zero" id="invoice_values_greater_than_zero">
                                <option value="1" <?= $this->Settings->invoice_values_greater_than_zero == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                <option value="0" <?= $this->Settings->invoice_values_greater_than_zero == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <?= lang('except_category_taxes', 'except_category_taxes') ?>
                            <select class="form-control" name="except_category_taxes" id="except_category_taxes">
                                <option value="1" <?= $this->Settings->except_category_taxes == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                <option value="0" <?= $this->Settings->except_category_taxes == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group except_category" style="display: none;">
                            <?= lang('category_tax_exception_start_date', 'category_tax_exception_start_date') ?>
                            <input type="date" name="category_tax_exception_start_date" id="category_tax_exception_start_date" class="form-control" value="<?= $this->Settings->category_tax_exception_start_date ?>">
                        </div>
                        <div class="col-md-4 form-group except_category" style="display: none;">
                            <?= lang('category_tax_exception_end_date', 'category_tax_exception_end_date') ?>
                            <input type="date" name="category_tax_exception_end_date" id="category_tax_exception_end_date" class="form-control" value="<?= $this->Settings->category_tax_exception_end_date ?>">
                        </div>
                        <div class="col-md-4 form-group except_category" style="display: none;">
                            <?= lang('category_tax_exception_tax_rate_id', 'category_tax_exception_tax_rate_id') ?>
                            <?php
                            $tr = [];
                            $tr[''] = lang('select');
                            foreach ($tax_rates as $rate) {
                            $tr[$rate->id] = $rate->name;
                            }
                            echo form_dropdown('category_tax_exception_tax_rate_id', $tr, $Settings->category_tax_exception_tax_rate_id, 'id="category_tax_exception_tax_rate_id" class="form-control tip" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <?= lang('customer_territorial_decree_tax_rate_id', 'customer_territorial_decree_tax_rate_id') ?>
                            <?php
                            $tr = [];
                            $tr[''] = lang('select');
                            foreach ($tax_rates as $rate) {
                            $tr[$rate->id] = $rate->name;
                            }
                            echo form_dropdown('customer_territorial_decree_tax_rate_id', $tr, $Settings->customer_territorial_decree_tax_rate_id, 'id="customer_territorial_decree_tax_rate_id" class="form-control tip" style="width:100%;"');
                            ?>
                        </div>
                        <div class="col-md-4 form-group">
                            <?= lang('default_return_payment_method', 'default_return_payment_method') ?>
                            <select class="form-control" name="default_return_payment_method" id="default_return_payment_method">
                                <option value="cash" <?= $this->Settings->default_return_payment_method == 'cash' ? 'selected="selected"' : '' ?>><?= lang('cash') ?></option>
                                <option value="deposit" <?= $this->Settings->default_return_payment_method == 'deposit' ? 'selected="selected"' : '' ?>><?= lang('deposit') ?></option>
                                <option value="0" <?= $this->Settings->default_return_payment_method == '0' ? 'selected="selected"' : '' ?>><?= lang('select_during_sale_registration') ?></option>
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <?= lang('item_discount_apply_to', 'item_discount_apply_to') ?>
                            <select class="form-control" name="item_discount_apply_to" id="item_discount_apply_to">
                                <option value="1" <?= $this->Settings->item_discount_apply_to == 1 ? 'selected="selected"' : '' ?>><?= lang('base') ?></option>
                                <option value="2" <?= $this->Settings->item_discount_apply_to == 2 ? 'selected="selected"' : '' ?>><?= lang('total_price') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <?= lang('suspended_sales_to_retail', 'suspended_sales_to_retail') ?>
                            <select class="form-control" name="suspended_sales_to_retail" id="suspended_sales_to_retail">
                                <option value="1" <?= $this->Settings->suspended_sales_to_retail == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                <option value="2" <?= $this->Settings->suspended_sales_to_retail == 2 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <?= lang('payments_methods_retcom', 'payments_methods_retcom') ?>
                            <select class="form-control" name="payments_methods_retcom" id="payments_methods_retcom">
                                <option value="1" <?= $this->Settings->payments_methods_retcom == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                <option value="0" <?= $this->Settings->payments_methods_retcom == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                            </select>
                        </div>

                        <?php if ($this->Owner) : ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= form_label(lang("period_days_allowed_before_current_date"), "days_before_current_date"); ?>
                                    <?= form_input(["name"=>"days_before_current_date", "id"=>"days_before_current_date", "type"=>"number", "class"=>"form-control", "required"=>TRUE, "min"=>0, "max"=>5], $this->Settings->days_before_current_date); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="row">
                        <?php if ($this->Owner) : ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= form_label(lang("period_days_allowed_after_current_date"), "days_after_current_date"); ?>
                                    <?= form_input(["name"=>"days_after_current_date", "id"=>"days_after_current_date", "type"=>"number", "class"=>"form-control", "required"=>TRUE, "min"=>0, "max"=>10], $this->Settings->days_after_current_date); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-4 form-group">
                            <?= lang('alert_zero_cost_sale', 'alert_zero_cost_sale') ?>
                            <select class="form-control" name="alert_zero_cost_sale" id="alert_zero_cost_sale">
                                <option value="1" <?= $this->Settings->alert_zero_cost_sale == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                <option value="0" <?= $this->Settings->alert_zero_cost_sale == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <?= lang('max_minutes_random_pin_code', 'max_minutes_random_pin_code') ?>
                            <input type="text" name="max_minutes_random_pin_code" class="form-control only_number" value="<?= $this->Settings->max_minutes_random_pin_code ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <?= lang('customer_default_payment_type', 'customer_default_payment_type') ?>
                            <?php
                              $cptypes = [
                                '1' => lang('payment_type_cash'),
                                '0' => lang('credit'),
                              ];
                            ?>
                            <?= form_dropdown('customer_default_payment_type', $cptypes, $this->Settings->customer_default_payment_type, 'class="form-control select" id="customer_default_payment_type" style="width:100%;" required="required"'); ?>
                        </div>
                        <div class="form-group col-md-4 credit_customer" style="display: none;">
                            <?= lang('customer_default_credit_limit', 'customer_default_credit_limit') ?>
                            <input type="text" name="customer_default_credit_limit" id="customer_default_credit_limit" class="form-control only_number" value="<?= $this->Settings->customer_default_credit_limit ?>">
                        </div>
                        <div class="form-group col-md-4 credit_customer" style="display: none;">
                            <?= lang('customer_default_payment_term', 'customer_default_payment_term') ?>
                            <input type="text" name="customer_default_payment_term" id="customer_default_payment_term" class="form-control only_number" value="<?= $this->Settings->customer_default_payment_term ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <?= lang('management_order_sale_delivery_day', 'management_order_sale_delivery_day') ?>
                            <?php
                              $cptypes = [
                                '1' => lang('yes'),
                                '0' => lang('no'),
                              ];
                            ?>
                            <?= form_dropdown('management_order_sale_delivery_day', $cptypes, $this->Settings->management_order_sale_delivery_day, 'class="form-control select" id="management_order_sale_delivery_day" style="width:100%;" required="required"'); ?>
                        </div>
                        <div class="form-group col-md-4">
                            <?= lang('management_order_sale_delivery_time', 'management_order_sale_delivery_time') ?>
                            <?php
                              $cptypes = [
                                '1' => lang('yes'),
                                '0' => lang('no'),
                              ];
                            ?>
                            <?= form_dropdown('management_order_sale_delivery_time', $cptypes, $this->Settings->management_order_sale_delivery_time, 'class="form-control select" id="management_order_sale_delivery_time" style="width:100%;" required="required"'); ?>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <?= lang('product_clic_action', 'product_clic_action') ?>
                            <?php
                                $product_clic_action = array(
                                                                '1' => lang('view_product_details'),
                                                                '2' => lang('view_product_variants_and_preferences'),
                                                                '3' => lang('view_edit_product_form'),
                                                            );
                             ?>
                            <?= form_dropdown('product_clic_action', $product_clic_action, $this->Settings->product_clic_action, 'required="required" id="product_clic_action" class="form-control"') ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <?= lang('order_sales_conversion', 'order_sales_conversion') ?>
                            <?php
                                $order_sales_conversion = array(
                                                                '1' => lang('select'),
                                                                '2' => lang('pos'),
                                                                '3' => lang('sale'),
                                                            );
                             ?>
                            <?= form_dropdown('order_sales_conversion', $order_sales_conversion, $this->Settings->order_sales_conversion, 'required="required" id="order_sales_conversion" class="form-control"') ?>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <?= lang('product_seller_management', 'product_seller_management') ?>
                            <?php
                                $product_seller_management = array(
                                                                '1' => lang('yes'),
                                                                '0' => lang('no'),
                                                            );
                             ?>
                            <?= form_dropdown('product_seller_management', $product_seller_management, $this->Settings->product_seller_management, 'required="required" id="product_seller_management" class="form-control"') ?>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="detailed_budget_by_category" style="margin-top: 20px;">
                                    <?php $checkedDbc = ($Settings->detailed_budget_by_category == 1) ? 'checked' : '' ?>
                                    <input type="checkbox" name="detailed_budget_by_category" id="detailed_budget_by_category" <?= $checkedDbc ?>> &nbsp; <?= $this->lang->line('¿Presupuesto detallado por Categoría?') ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="detailed_budget_by_seller" style="margin-top: 20px;">
                                    <?php $checkedDbc = ($Settings->detailed_budget_by_seller == 1) ? 'checked' : '' ?>
                                    <input type="checkbox" name="detailed_budget_by_seller" id="detailed_budget_by_seller" <?= $checkedDbc ?>> &nbsp; <?= $this->lang->line('¿Presupuesto detallado por Vendedor?') ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <?= lang('aiu_management', 'aiu_management') ?>
                            <?php
                                $aiu_management = array(
                                                                '1' => lang('yes'),
                                                                '0' => lang('no'),
                                                            );
                             ?>
                            <?= form_dropdown('aiu_management', $aiu_management, $this->Settings->aiu_management, 'required="required" id="aiu_management" class="form-control"') ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 row div_aiu_perc" style="display:none;">
                            <div class="col-md-4">
                                <?= lang('aiu_perc_admin', 'aiu_perc_admin') ?>
                                <input type="text" name="aiu_perc_admin" id="aiu_perc_admin" class="form-control only_number" value="<?= $this->Settings->aiu_perc_admin ?>">
                            </div>
                            <div class="col-md-4">
                                <?= lang('aiu_perc_imprev', 'aiu_perc_imprev') ?>
                                <input type="text" name="aiu_perc_imprev" id="aiu_perc_imprev" class="form-control only_number" value="<?= $this->Settings->aiu_perc_imprev ?>">
                            </div>
                            <div class="col-md-4">
                                <?= lang('aiu_perc_utilidad', 'aiu_perc_utilidad') ?>
                                <input type="text" name="aiu_perc_utilidad" id="aiu_perc_utilidad" class="form-control only_number" value="<?= $this->Settings->aiu_perc_utilidad ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang("loaded_suspended_sale_validate_prices"), "loaded_suspended_sale_validate_prices") ?>
                                <?php $aopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                <?= form_dropdown(["name"=>"loaded_suspended_sale_validate_prices", "id"=>"loaded_suspended_sale_validate_prices", "class"=>"form-control"], $aopts, $this->Settings->loaded_suspended_sale_validate_prices); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang('Ignorar parámetro "sobreventa" en ordenes de pedido')) ?>
                                <?php $eoopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                <?= form_dropdown(['name'=>'exclude_overselling_on_sale_orders', 'id'=>'exclude_overselling_on_sale_orders', 'class'=>'form-control'], $eoopts, $this->Settings->exclude_overselling_on_sale_orders) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang('days_count_birthday_discount_application')) ?>
                                <?php $eoopts = [
                                    0 => lang('inactive'),
                                    1 => lang('before_birthday'),
                                    2 => lang('after_birthday'),
                                    3 => lang('before_and_after_birthday'),
                                    4 => lang('only_on_birthday'),
                                ]; ?>
                                <?= form_dropdown(['name'=>'days_count_birthday_discount_application', 'id'=>'days_count_birthday_discount_application', 'class'=>'form-control'], $eoopts, $this->Settings->days_count_birthday_discount_application) ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4 hbd_div" <?=  in_array($this->Settings->days_count_birthday_discount_application, [0, 4]) ? "style='display:none;'" : "" ?>>
                            <?= lang('days_count_birthday_discount', 'days_count_birthday_discount') ?>
                            <input type="text" name="days_count_birthday_discount" id="days_count_birthday_discount" class="form-control only_number" value="<?= in_array($this->Settings->days_count_birthday_discount_application, [0, 4]) ? 0 : $this->Settings->days_count_birthday_discount ?>">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang('highlight_products_without_quantities_in_edit_sales')) ?>
                                <?php $eoopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                <?= form_dropdown(['name'=>'highlight_products_without_quantities_in_edit_sales', 'id'=>'highlight_products_without_quantities_in_edit_sales', 'class'=>'form-control'], $eoopts, $this->Settings->highlight_products_without_quantities_in_edit_sales) ?>
                            </div>
                        </div>
                            
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang('exclusive_discount_group_customers')) ?>
                                <?php $eoopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                <?= form_dropdown(['name'=>'exclusive_discount_group_customers', 'id'=>'exclusive_discount_group_customers', 'class'=>'form-control'], $eoopts, $this->Settings->exclusive_discount_group_customers) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang('control_customer_credit')) ?>
                                <?php $eoopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                <?= form_dropdown(['name'=>'control_customer_credit', 'id'=>'control_customer_credit', 'class'=>'form-control'], $eoopts, $this->Settings->control_customer_credit) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang('affiliate_management')) ?>
                                <?php $eoopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                <?= form_dropdown(['name'=>'affiliate_management', 'id'=>'affiliate_management', 'class'=>'form-control'], $eoopts, $this->Settings->affiliate_management) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang('recurring_sale_management')) ?>
                                <?php $eoopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                <?= form_dropdown(['name'=>'recurring_sale_management', 'id'=>'recurring_sale_management', 'class'=>'form-control'], $eoopts, $this->Settings->recurring_sale_management) ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang('osdt_recurring_sales')) ?>
                                <?php 
                                $osdtopts[] = lang('select');
                                if ($osdt_documents_types) {
                                    foreach ($osdt_documents_types as $osdt_row) {
                                        $osdtopts[$osdt_row->id] = $osdt_row->sales_prefix." - ".$osdt_row->nombre;
                                    }
                                }

                                ?>
                                <?= form_dropdown(['name'=>'osdt_recurring_sales', 'id'=>'osdt_recurring_sales', 'class'=>'form-control'], $osdtopts, $this->Settings->osdt_recurring_sales) ?>
                            </div>
                        </div>


                    </div>
                    <?php if ($this->Owner) : ?>
                        <hr class="col-md-11">
                        <div class="row">
                            <div class="col-md-12">
                                <h2><?= lang("electronic_billing"); ?></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label for=""><?= lang('electronic_billing') ?></label>
                                    <select class="form-control" name="electronic_billing" id="electronic_billing">
                                        <option value="<?= NOT; ?>" <?= ($this->Settings->electronic_billing == NOT) ? "selected" : ""; ?>><?= lang("no"); ?></option>
                                        <option value="<?= YES; ?>" <?= ($this->Settings->electronic_billing == YES) ? "selected" : ""; ?>><?= lang("yes"); ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="electronig_billing_container">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label><?= lang('technology_provider') ?></label>
                                        <div style="display: flex; align-items: center;">
                                            <select name="technology_provider" id="technology_provider" class="form-control">
                                                <?php if ($technology_providers): ?>
                                                    <option value=""><?= lang('select') ?></option>
                                                    <?php foreach ($technology_providers as $tec_provider): ?>
                                                        <option value="<?= $tec_provider->id ?>" <?= $this->Settings->fe_technology_provider == $tec_provider->id ? 'selected="selected"' : '' ?>><?= $tec_provider->name ?></option>
                                                    <?php endforeach ?>
                                                <?php else: ?>
                                                    <option value=""><?= lang('no_data_available') ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?= lang("work_environment", "work_environment"); ?>
                                        <select class="form-control" name="work_environment" id="work_environment">
                                            <option value="<?= PRODUCTION; ?>" <?= ($Settings->fe_work_environment == PRODUCTION) ? "selected" : ""; ?>><?= lang("production"); ?></option>
                                            <option value="<?= TEST; ?>" <?= ($Settings->fe_work_environment == TEST) ? "selected" : ""; ?>><?= lang("test"); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row electronig_billing_container">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('webservices_url', 'webservices_url'); ?>
                                    <?= form_input(['name'=>'webservices_url', 'id'=>'webservices_url', 'class'=>'form-control', "disabled"=>TRUE]) ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('send_electronic_invoice_to', 'send_electronic_invoice_to'); ?>
                                    <select class="form-control" name="send_electronic_invoice_to" id="send_electronic_invoice_to">
                                        <option value=""><?= lang('select'); ?></option>
                                        <option value="1" <?= $this->Settings->send_electronic_invoice_to == 1 ? 'selected="selected"' : '' ?>><?= lang('main_email') ?></option>
                                        <option value="2" <?= $this->Settings->send_electronic_invoice_to == 2 ? 'selected="selected"' : '' ?>><?= lang('customer_branch_email') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <label style="position: relative; top: 28px;">
                                    <input type="checkbox" id="copy_mail_to_sender" name="copy_mail_to_sender" <?= $this->Settings->copy_mail_to_sender == YES ? 'checked' : '' ?>>
                                    <?= lang('copy_mail_to_sender') ?>
                                </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 electronig_billing_container">
                                <div class="form-group">
                                    <?= form_label(lang("add_individual_attachments"), "add_individual_attachments") ?>
                                    <?php $aopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                    <?= form_dropdown(["name"=>"add_individual_attachments", "id"=>"add_individual_attachments", "class"=>"form-control"], $aopts, $this->Settings->add_individual_attachments); ?>
                                </div>
                            </div>
                            <div class="col-md-1 events_simba">
                                <label for=""><?= lang('registration_of_the_agreement') ?> </label>
                                <br>
                                <input <?= $this->Settings->reg_conv_simba_status == 1 ? 'disabled' : '' ?> 
                                    type="checkbox" 
                                    name="reg_conv_simba_status" 
                                    id="reg_conv_simba_status"
                                    <?= ($Settings->reg_conv_simba_status == YES) ? 'checked' : '' ?>>
                            </div>
                            <div class="col-md-1 events_simba">
                                <label for=""><?= lang('qualification') ?> </label>
                                <br><br>
                                <input <?= $this->Settings->qualification_simba_status == 1 ? 'disabled' : '' ?> 
                                    type="checkbox" 
                                    name="qualification_simba_status" 
                                    id="qualification_simba_status" 
                                    <?= ($Settings->qualification_simba_status == YES) ? 'checked' : '' ?>>
                            </div> 
                            <div class="col-md-2 events_simba">
                                <label for=""><?= lang('qualification_pofe') ?> </label>
                                <br>
                                <input <?= $this->Settings->quali_simba_status_pofe == 1 ? 'disabled' : '' ?> 
                                    type="checkbox" 
                                    name="quali_simba_status_pofe" 
                                    id="quali_simba_status_pofe" 
                                    <?= ($Settings->quali_simba_status_pofe == YES) ? 'checked' : '' ?>>
                            </div> 
                            <div class="col-md-4 events_simba" >
                                <?= lang('document_type_testid', 'document_type_testid'); ?>
                                <?= form_input(['name'=>'test_id', 'id'=>'test_id', 'class'=>'form-control', 'value'=>$this->Settings->test_id]) ?>
                            </div>
                        </div>

                        <div class="row" id="userCredentialContainer">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('electronic_billing_user', 'fe_user'); ?>
                                    <?= form_input(['name'=>'fe_user', 'id'=>'fe_user', 'class'=>'form-control', 'value'=>$this->Settings->fe_user]) ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('electronic_billing_password', 'fe_password'); ?>
                                    <?= form_password(['name'=>'fe_password', 'id'=>'fe_password', 'class'=>'form-control', 'value'=>$this->Settings->fe_password]) ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('wappsi_print_format', 'wappsi_print_format'); ?>
                                    <?php $pfopts = [ NOT=>lang("no"), YES=>lang("yes") ]; ?>
                                    <?= form_dropdown(["name"=>"wappsi_print_format", "id"=>"wappsi_print_format", "class"=>"form-control"], $pfopts, $this->Settings->wappsi_print_format); ?>
                                </div>
                            </div>
                        </div>

                        <hr class="col-md-11">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><?= lang("electronic_processes"); ?></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="checkbox" name="documents_reception" id="documents_reception" <?= ($Settings->documents_reception == YES) ? 'checked' : '' ?>> &nbsp;
                                    <?= lang('documents_reception', 'documents_reception'); ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="checkbox" name="electronic_payroll" id="electronic_payroll" <?= ($Settings->electronic_payroll == YES) ? 'checked' : '' ?>> &nbsp;
                                    <?= form_label($this->lang->line('payroll_electronic'), 'electronic_payroll') ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <input type="checkbox" name="supportingDocument" id="supportingDocument" <?= ($Settings->supportingDocument == YES) ? 'checked' : '' ?>> &nbsp;
                                <?= form_label($this->lang->line('supporting_document'), 'supportingDocument'); ?>
                            </div>

                            <div class="col-sm-3">
                                <input type="checkbox" name="electronic_hits_validation" id="electronic_hits_validation" <?= ($Settings->electronic_hits_validation == YES) ? 'checked' : '' ?>> &nbsp;
                                <?= form_label($this->lang->line('electronic_hits_validation'), 'electronic_hits_validation'); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    </div>
                </div>

                <!-- <div class="ibox">
                    <div class="ibox-title"> <h5><?= lang('prefix') ?></h5> </div>
                    <div class="ibox-content">
                        <div class="row">

                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"
                                        for="return_prefix"><?= lang("return_prefix"); ?></label>

                                    <?= form_input('return_prefix', $Settings->return_prefix, 'class="form-control tip prefixes" id="return_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="payment_prefix"><?= lang("payment_prefix"); ?></label>
                                    <?= form_input('payment_prefix', $Settings->payment_prefix, 'class="form-control tip prefixes" id="payment_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="quote_prefix"><?= lang("quote_prefix"); ?></label>

                                    <?= form_input('quote_prefix', $Settings->quote_prefix, 'class="form-control tip prefixes" id="quote_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="order_sale_prefix"><?= lang("order_sale_prefix"); ?></label>
                                    <?= form_input('order_sale_prefix', $Settings->order_sale_prefix, 'class="form-control tip prefixes" id="order_sale_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('sma_payment_prefix', 'sma_payment_prefix'); ?>
                                    <?= form_input('sma_payment_prefix', $Settings->sma_payment_prefix, 'class="form-control tip prefixes" id="sma_payment_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('deposit_prefix', 'deposit_prefix'); ?>
                                    <?= form_input('deposit_prefix', $Settings->deposit_prefix, 'class="form-control tip prefixes" id="deposit_prefix"'); ?>
                                </div>
                            </div>

                            <hr class="col-md-11">

                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"
                                        for="purchase_prefix"><?= lang("purchase_prefix"); ?></label>

                                    <?= form_input('purchase_prefix', $Settings->purchase_prefix, 'class="form-control tip prefixes" id="purchase_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"
                                        for="returnp_prefix"><?= lang("returnp_prefix"); ?></label>

                                    <?= form_input('returnp_prefix', $Settings->returnp_prefix, 'class="form-control tip prefixes" id="returnp_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="ppayment_prefix"><?= lang("ppayment_prefix"); ?></label>
                                    <?= form_input('ppayment_prefix', $Settings->ppayment_prefix, 'class="form-control tip prefixes" id="ppayment_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="quote_purchase_prefix"><?= lang("quote_purchase_prefix"); ?></label>
                                    <?= form_input('quote_purchase_prefix', $Settings->quote_purchase_prefix, 'class="form-control tip prefixes" id="quote_purchase_prefix"'); ?>
                                </div>
                            </div>

                            <hr class="col-md-11">


                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"
                                        for="delivery_prefix"><?= lang("delivery_prefix"); ?></label>

                                    <?= form_input('delivery_prefix', $Settings->delivery_prefix, 'class="form-control tip prefixes" id="delivery_prefix"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"
                                        for="transfer_prefix"><?= lang("transfer_prefix"); ?></label>
                                    <?= form_input('transfer_prefix', $Settings->transfer_prefix, 'class="form-control tip prefixes" id="transfer_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('expense_prefix', 'expense_prefix'); ?>
                                    <?= form_input('expense_prefix', $Settings->expense_prefix, 'class="form-control tip prefixes" id="expense_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('qa_prefix', 'qa_prefix'); ?>
                                    <?= form_input('qa_prefix', $Settings->qa_prefix, 'class="form-control tip prefixes" id="qa_prefix"'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="ibox">
                    <div class="ibox-title"> <h5><?= lang('money_number_format') ?></h5> </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="decimals"><?= lang("decimals"); ?></label>

                                    <div class="controls"> <?php
                                        $decimals = array(2 => '2', 3 => '3', 4 => '4');
                                        echo form_dropdown('decimals', $decimals, $Settings->decimals, 'class="form-control tip" id="decimals"  style="width:100%;" required="required"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="qty_decimals"><?= lang("qty_decimals"); ?></label>

                                    <div class="controls"> <?php
                                        $qty_decimals = array(0 => '0', 2 => '2', 3 => '3', 4 => '4');
                                        echo form_dropdown('qty_decimals', $qty_decimals, $Settings->qty_decimals, 'class="form-control tip" id="qty_decimals"  style="width:100%;" required="required"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('sac', 'sac'); ?>
                                    <?= form_dropdown('sac', $ps, set_value('sac', $Settings->sac), 'class="form-control tip" id="sac"  required="required"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="nsac">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="decimals_sep"><?= lang("decimals_sep"); ?></label>

                                        <div class="controls"> <?php
                                            $dec_point = array('.' => lang('dot'), ',' => lang('comma'));
                                            echo form_dropdown('decimals_sep', $dec_point, $Settings->decimals_sep, 'class="form-control tip" id="decimals_sep"  style="width:100%;" required="required"');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="thousands_sep"><?= lang("thousands_sep"); ?></label>
                                        <div class="controls"> <?php
                                            $thousands_sep = array('.' => lang('dot'), ',' => lang('comma'), '0' => lang('space'));
                                            echo form_dropdown('thousands_sep', $thousands_sep, $Settings->thousands_sep, 'class="form-control tip" id="thousands_sep"  style="width:100%;" required="required"');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('display_currency_symbol', 'display_symbol'); ?>
                                    <?php $opts = array(0 => lang('disable'), 1 => lang('before'), 2 => lang('after')); ?>
                                    <?= form_dropdown('display_symbol', $opts, $Settings->display_symbol, 'class="form-control" id="display_symbol" style="width:100%;" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('currency_symbol', 'symbol'); ?>
                                    <?= form_input('symbol', $Settings->symbol, 'class="form-control" id="symbol" style="width:100%;"'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if ($this->Owner): ?>
                    <div class="ibox">
                        <div class="ibox-title"> <h5><?= lang('email') ?></h5> </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="protocol"><?= lang("email_protocol"); ?></label>

                                        <div class="controls"> <?php
                                            $popt = array('mail' => 'PHP Mail Function', 'sendmail' => 'Send Mail', 'smtp' => 'SMTP');
                                            echo form_dropdown('protocol', $popt, $Settings->protocol, 'class="form-control tip" id="protocol"  style="width:100%;" required="required"');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row" id="sendmail_config" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label" for="mailpath"><?= lang("mailpath"); ?></label>

                                                <?= form_input('mailpath', $Settings->mailpath, 'class="form-control tip" id="mailpath"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row" id="smtp_config" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"
                                                    for="smtp_host"><?= lang("smtp_host"); ?></label>

                                                <?= form_input('smtp_host', $Settings->smtp_host, 'class="form-control tip" id="smtp_host"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"
                                                    for="smtp_user"><?= lang("smtp_user"); ?></label>

                                                <?= form_input('smtp_user', $Settings->smtp_user, 'class="form-control tip" id="smtp_user"'); ?> </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"
                                                    for="smtp_pass"><?= lang("smtp_pass"); ?></label>

                                                <?= form_password('smtp_pass', md5($Settings->smtp_pass), 'class="form-control tip" id="smtp_pass" autocomplete="off"'); ?> </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"
                                                    for="smtp_port"><?= lang("smtp_port"); ?></label>

                                                <?= form_input('smtp_port', $Settings->smtp_port, 'class="form-control tip" id="smtp_port"'); ?> </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"
                                                    for="smtp_crypto"><?= lang("smtp_crypto"); ?></label>

                                                <div class="controls"> <?php
                                                    $crypto_opt = array('' => lang('none'), 'tls' => 'TLS', 'ssl' => 'SSL');
                                                    echo form_dropdown('smtp_crypto', $crypto_opt, $Settings->smtp_crypto, 'class="form-control tip" id="smtp_crypto"');
                                                    ?> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5><?= $this->lang->line("credit_financing_language") ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="">
                                        <input type="checkbox" name="financing_module" id="financing_module">
                                        <label for=""><?= lang("Modulo de financiación") ?></label>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div id="financingModuleContainer" style="display: none">
                            <div class="row ">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= form_label(lang("payment_method_for_credit_financing"), "payment_method_for_credit_financing") ?>
                                        <?php
                                            $pmcfOpts = ['' => lang('select')];
                                            if (!empty($paymentMethodForCreditFinancing)) {
                                                foreach ($paymentMethodForCreditFinancing as $paymentMethod) {
                                                    $pmcfOpts[$paymentMethod->code] = $paymentMethod->name;
                                                }
                                            }
                                        ?>
                                        <?= form_dropdown(["name"=>"payment_method_for_credit_financing", "id"=>"payment_method_for_credit_financing", "class"=>"form-control"], $pmcfOpts, $this->Settings->payment_method_for_credit_financing) ?>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= form_label(lang("enable_for"), "enable_for") ?>
                                        <?php
                                            $pmcfOpts = [
                                                0 => lang('all_billing'),
                                                1 => lang('sale_invoice'),
                                                2 => lang('pos_invoice'),
                                            ];
                                        ?>
                                        <?= form_dropdown(["name"=>"enable_for", "id"=>"enable_for", "class"=>"form-control"], $pmcfOpts, $this->Settings->enable_for) ?>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= form_label(lang("current_interest_percentage_credit_financing"), "current_interest_percentage_credit_financing") ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"current_interest_percentage_credit_financing","id"=>"current_interest_percentage_credit_financing","type"=>"number","class"=>"form-control"], $this->Settings->current_interest_percentage_credit_financing) ?>
                                            <span class="input-group-addon" id="basic-addon2"><i class="fas fa-percentage"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= form_label(lang("default_interest_percentage_credit_financing"), "default_interest_percentage_credit_financing") ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"default_interest_percentage_credit_financing","id"=>"default_interest_percentage_credit_financing","type"=>"number","class"=>"form-control"], $this->Settings->default_interest_percentage_credit_financing) ?>
                                            <span class="input-group-addon" id="basic-addon2"><i class="fas fa-percentage"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for=""><?= lang("days_for_notification") ?></label>
                                        <?php $daysForNotification = !empty($this->Settings->days_for_notification) ? $this->Settings->days_for_notification : '' ?>
                                        <input type="number" class="form-control" name="days_for_notification" id="days_for_notification" value="<?= $daysForNotification ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for=""><?= lang("days_for_report") ?></label>
                                        <?php $daysForReport = !empty($this->Settings->days_for_report) ? $this->Settings->days_for_report : '' ?>
                                        <input type="number" class="form-control" name="days_for_report" id="days_for_report" value="<?= $daysForReport ?>"/>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= form_label(lang("insurance_percentage_credit_financing"), "insurance_percentage_credit_financing") ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"insurance_percentage_credit_financing","id"=>"insurance_percentage_credit_financing","type"=>"number","class"=>"form-control"], $this->Settings->insurance_percentage_credit_financing) ?>
                                            <span class="input-group-addon" id="basic-addon2"><i class="fas fa-percentage"></i></span>
                                        </div>
                                    </div>
                                </div> -->
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= form_label(lang("quota_calculation_method_credit_financing"), "quota_calculation_method_credit_financing") ?>
                                        <?php $qcmcfOpts = ['' => lang('select'), 1 => lang('fixed_fee'), 2 => lang('variable_fee')]; ?>
                                        <?= form_dropdown(["name"=>"quota_calculation_method_credit_financing", "id"=>"quota_calculation_method_credit_financing", "class"=>"form-control"], $qcmcfOpts, $this->Settings->quota_calculation_method_credit_financing) ?>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="credit_financing_language"><?= lang("module_name_customization_credit_financing") ?></label>
                                        <?php $creditFinancingLanguage = !empty($this->Settings->credit_financing_language) ? $this->Settings->credit_financing_language : '' ?>
                                        <input class="form-control" type="text"  name="credit_financing_language" id="credit_financing_language" value="<?= $creditFinancingLanguage ?>">
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for=""><?= lang("concepts_for_current_interest") ?></label>
                                        <input type="text" class="form-control" name="concepts_for_current_interest" id="concepts_for_current_interest"/>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for=""><?= lang("concepts_for_default_interest") ?></label>
                                        <input type="text" class="form-control" name="concepts_for_default_interest" id="concepts_for_default_interest"/>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for=""><?= lang("concept_for_procredito_commission") ?></label>
                                        <input type="text" class="form-control" name="concept_for_procredito_commission" id="concept_for_procredito_commission"/>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for=""><?= lang("concept_for_procredito_interest") ?></label>
                                        <input type="text" class="form-control" name="concept_for_procredito_interest" id="concept_for_procredito_interest"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="" style="margin-top: 15px">
                                        <input type="checkbox" name="generate_automatic_invoice" id="generate_automatic_invoice" <?= !empty($this->Settings->generate_automatic_invoice) ? 'checked' : '' ?>>
                                        <label for="generate_automatic_invoice"><?= lang('generate_automatic_invoice') ?></label>
                                    </label>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5><?= $this->lang->line("colletionsConfiguration") ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("aIdMarca"), "a_id_marca") ?>
                                    <?= form_input(["name"=>"a_id_marca", "id"=>"a_id_marca", "class"=>"form-control"], $this->Settings->a_id_marca) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("aSchemacia"), "a_schemacia") ?>
                                    <?= form_input(["name"=>"a_schemacia", "id"=>"a_schemacia", "class"=>"form-control"], $this->Settings->a_schemacia) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("cidToken"), "cid_token") ?>
                                    <?= form_input(["name"=>"cid_token", "id"=>"cid_token", "class"=>"form-control"], $this->Settings->cid_token) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("urlComercio"), "url_comercio") ?>
                                    <?= form_input(["name"=>"url_comercio", "id"=>"url_comercio", "class"=>"form-control"], $this->Settings->url_comercio) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("aLogin"), "a_login") ?>
                                    <?= form_input(["name"=>"a_login", "id"=>"a_login", "class"=>"form-control"], $this->Settings->a_login) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("aPassword"), "a_password") ?>
                                    <?= form_input(["name"=>"a_password", "id"=>"a_password", "class"=>"form-control"], $this->Settings->a_password) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("cSucursal"), "c_sucursal") ?>
                                    <?= form_input(["name"=>"c_sucursal", "id"=>"c_sucursal", "class"=>"form-control"], $this->Settings->c_sucursal) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5><?= $this->lang->line("warranty") ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("block_warranty_by_warranty_days"), "block_warranty_by_warranty_days") ?>
                                    <?= form_dropdown(["name"=>"block_warranty_by_warranty_days", "id"=>"block_warranty_by_warranty_days", "class"=>"form-control"], [YES => $this->lang->line("yes"), NOT=>$this->lang->line('no')], $this->Settings->block_warranty_by_warranty_days) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("warranty_warehouse"), "warranty_warehouse") ?>
                                    <?php
                                        $wwOptions = [];
                                        if (!empty($warehouses)) {
                                            $wwOptions[] = $this->lang->line("select");
                                            foreach ($warehouses as $warehouse) {
                                                if ($warehouse->id != $this->Settings->warranty_supplier_warehouse) {
                                                    $wwOptions[$warehouse->id] = $warehouse->name;
                                                }
                                            }
                                        }
                                    ?>
                                    <?= form_dropdown(["name"=>"warranty_warehouse", "id"=>"warranty_warehouse", "class"=>"form-control"], $wwOptions, $this->Settings->warranty_warehouse) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label($this->lang->line("warranty_supplier_warehouse"), "warranty_supplier_warehouse") ?>
                                    <?php
                                        $wswOptions = [];
                                        if (!empty($warehouses)) {
                                            $wswOptions[] = $this->lang->line("select");
                                            foreach ($warehouses as $warehouse) {
                                                if ($warehouse->id != $this->Settings->warranty_warehouse) {
                                                    $wswOptions[$warehouse->id] = $warehouse->name;
                                                }
                                            }
                                        }
                                    ?>
                                    <?= form_dropdown(["name"=>"warranty_supplier_warehouse", "id"=>"warranty_supplier_warehouse", "class"=>"form-control"], $wswOptions, $this->Settings->warranty_supplier_warehouse) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label(lang("adjustments_document"), "adjustments_document") ?>
                                    <?php
                                        $ajgOptions = [];
                                        if (!empty($adjustmentsDocument)) {
                                            $ajgOptions[] = lang("select");
                                            foreach ($adjustmentsDocument as $adjustment) {
                                                $ajgOptions[$adjustment->id] = "$adjustment->sales_prefix - $adjustment->nombre";
                                            }
                                        }
                                    ?>
                                    <?= form_dropdown(["name"=>"adjustments_document", "id"=>"adjustments_document", "class"=>"form-control"], $ajgOptions, $this->Settings->adjustments_document) ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= form_label(lang("transfer_document"), "transfer_document") ?>
                                    <?php
                                        $trgOptions = [];
                                        if (!empty($transferDocument)) {
                                            $trgOptions[] = lang("select");
                                            foreach ($transferDocument as $transfer) {
                                                $trgOptions[$transfer->id] = "$adjustment->sales_prefix - $transfer->nombre";
                                            }
                                        }
                                    ?>
                                    <?= form_dropdown(["name"=>"transfer_document", "id"=>"transfer_document", "class"=>"form-control"], $trgOptions, $this->Settings->transfer_document) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title"> <h5><?= lang('award_points') ?></h5> </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><?= lang("customer_award_points"); ?></label>

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <?= lang('award_points_each_spent', 'each_spent'); ?>
                                            <?= form_input('each_spent', $this->sma->formatDecimal($Settings->each_spent), 'class="form-control each_spent"'.($Settings->each_spent > 0 ? 'readonly="true"' : '')); ?>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-5">
                                            <?= lang('award_points_ca_point', 'ca_point'); ?><br>
                                            <?= form_input('ca_point', $Settings->ca_point, 'class="form-control ca_point"'.($Settings->each_spent > 0 ? 'readonly="true"' : '')); ?>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <?= lang('award_points_ca_point_value', 'ca_point_value'); ?><br>
                                            <?= form_input('ca_point_value', $this->sma->formatDecimal($Settings->ca_point_value), 'class="form-control ca_point_value"'.($Settings->ca_point_value > 0 ? 'readonly="true"' : '')); ?>
                                            <em class="info_ca_percentage"></em>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if ($this->Owner): ?>
                    <div class="ibox">
                        <div class="ibox-title"> <h5><?= lang('dropbox_setup') ?></h5> </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <?= lang('dropbox_key', 'dropbox_key') ?>
                                    <input class="form-control" type="text" name="dropbox_key" id="dropbox_key" value="<?= $this->Settings->dropbox_key ?>">
                                </div>
                                <div class="col-sm-3">
                                    <?= lang('dropbox_secret', 'dropbox_secret') ?>
                                    <input class="form-control" type="password" name="dropbox_secret" id="dropbox_secret" value="<?= md5($this->Settings->dropbox_secret) ?>">
                                </div>
                                <div class="col-sm-3">
                                    <?= lang('dropbox_token', 'dropbox_token') ?>
                                    <input class="form-control" type="password" name="dropbox_token" id="dropbox_token" value="<?= md5($this->Settings->dropbox_token) ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($this->Owner): ?>
                    <div class="ibox">
                        <div class="ibox-title"> <h5><?= lang('cronjob_setup') ?></h5> </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4><?= lang('summary_of_sales') ?></h4>
                                </div>
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" name="detail_due_sales" <?= $this->Settings->detail_due_sales == 1 ? 'checked' : '' ?>>
                                        <?= lang('detail_due_sales') ?>
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" name="detail_partial_sales" <?= $this->Settings->detail_partial_sales == 1 ? 'checked' : '' ?>>
                                        <?= lang('detail_partial_sales') ?>
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" name="detail_paid_sales" <?= $this->Settings->detail_paid_sales == 1 ? 'checked' : '' ?>>
                                        <?= lang('detail_paid_sales') ?>
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" name="detail_sales_per_biller" <?= $this->Settings->detail_sales_per_biller == 1 ? 'checked' : '' ?>>
                                        <?= lang('detail_sales_per_biller') ?>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <h4><?= lang('summary_of_products') ?></h4>
                                </div>
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" name="detail_products_expirated" <?= $this->Settings->detail_products_expirated == 1 ? 'checked' : '' ?>>
                                        <?= lang('detail_products_expirated') ?>
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" name="detail_products_promo_expirated" <?= $this->Settings->detail_products_promo_expirated == 1 ? 'checked' : '' ?>>
                                        <?= lang('detail_products_promo_expirated') ?>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <h4><?= lang('summary_pos_registers') ?></h4>
                                </div>
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" name="detail_pos_registers" <?= $this->Settings->detail_pos_registers == 1 ? 'checked' : '' ?>>
                                        <?= lang('detail_pos_registers') ?>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <h4><?= lang('summary_zeta_report') ?></h4>
                                </div>
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" name="detail_zeta_report" <?= $this->Settings->detail_zeta_report == 1 ? 'checked' : '' ?>>
                                        <?= lang('detail_zeta_report') ?>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <h4><?= lang('cron_job_options') ?></h4>
                                </div>
                                <div class="col-sm-4">
                                    <label>
                                        <input type="checkbox" name="cron_job_db_backup" <?= $this->Settings->cron_job_db_backup == 1 ? 'checked' : '' ?>>
                                        <?= lang('cron_job_db_backup') ?>
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label>
                                        <input type="checkbox" name="cron_job_images_backup" <?= $this->Settings->cron_job_images_backup == 1 ? 'checked' : '' ?>>
                                        <?= lang('cron_job_images_backup') ?>
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label>
                                        <input type="checkbox" name="cron_job_ebilling_backup" <?= $this->Settings->cron_job_ebilling_backup == 1 ? 'checked' : '' ?>>
                                        <?= lang('cron_job_ebilling_backup') ?>
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label>
                                        <input type="checkbox" name="cron_job_send_mail" <?= $this->Settings->cron_job_send_mail == 1 ? 'checked' : '' ?>>
                                        <?= lang('cron_job_send_mail') ?>
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label>
                                        <?= lang('cron_job_db_backup_partitions') ?>
                                    </label>
                                    <input type="number" name="cron_job_db_backup_partitions" class="form-control" value="<?= $this->Settings->cron_job_db_backup_partitions ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <div class="ibox">
                    <div class="ibox-content">
                        <div class="alert alert-info" role="alert"><p>
                            <?php if (!DEMO) { ?>
                                <a class="btn btn-primary btn-xs pull-right" target="_blank" href="<?= admin_url('cron/run'); ?>">Run cron job now</a>
                            <?php } ?>
                            <p><strong>Cron Job</strong> (run at 1:00 AM daily):</p> <pre>0 1 * * * wget -qO- <?= admin_url('cron/run'); ?> &gt;/dev/null 2&gt;&amp;1</pre> OR <pre>0 1 * * * <?= (defined('PHP_BINDIR') ? PHP_BINDIR.DIRECTORY_SEPARATOR :'').'php '.FCPATH.SELF.' admin/cron run'; ?> >/dev/null 2>&1</pre> For CLI: <code>schedule path/to/php path/to/index.php controller method</code> </p>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-primary send_settings" type="button"><?= lang('update_settings') ?></button>
                            </div>
                        </div>
                    </div>
                </div>

            <?= form_close(); ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // $('#document_number').on('change', function(){ calcularDigitoVerificacion(); });
        $(document).on('change', '#electronic_billing', function() { show_hide_electronic_billing_container(); });
        $(document).on('change', '#technology_provider', function() { get_webservices_url(); });
        $(document).on('change', '#work_environment', function() { get_webservices_url(); });

        $('#detailed_budget_by_seller').on('ifChanged', function (e) {
            if ($(this).is(':checked')) {
                $('#detailed_budget_by_category').iCheck('enable');
            } else {
                $('#detailed_budget_by_category').iCheck('uncheck');
                $('#detailed_budget_by_category').iCheck('disable');
            }
        });

          <?php if(isset($message)) { echo 'localStorage.clear();'; } ?>
          var timezones = <?= json_encode(DateTimeZone::listIdentifiers(DateTimeZone::ALL)); ?>;
          $('#timezone').autocomplete({
              source: timezones
          });
          if ($('#protocol').val() == 'smtp') {
              $('#smtp_config').slideDown();
          } else if ($('#protocol').val() == 'sendmail') {
              $('#sendmail_config').slideDown();
          }
          $('#protocol').change(function () {
              if ($(this).val() == 'smtp') {
                  $('#sendmail_config').slideUp();
                  $('#smtp_config').slideDown();
              } else if ($(this).val() == 'sendmail') {
                  $('#smtp_config').slideUp();
                  $('#sendmail_config').slideDown();
              } else {
                  $('#smtp_config').slideUp();
                  $('#sendmail_config').slideUp();
              }
          });
          $('#overselling').change(function () {
              if ($(this).val() == 1) {
                  if ($('#accounting_method').select2("val") != 2) {
                      bootbox.alert('<?=lang('overselling_will_only_work_with_AVCO_accounting_method_only')?>');
                      $('#accounting_method').select2("val", '2');
                  }
              }
          });
          $('#accounting_method').change(function () {
              var oam = <?=$Settings->accounting_method?>, nam = $(this).val();
              if (oam != nam) {
                  bootbox.alert('<?=lang('accounting_method_change_alert')?>');
              }
          });
          $('#accounting_method').change(function () {
              if ($(this).val() != 2) {
                  if ($('#overselling').select2("val") == 1) {
                      bootbox.alert('<?=lang('overselling_will_only_work_with_AVCO_accounting_method_only')?>');
                      $('#overselling').select2("val", 0);
                  }
              }
          });
          $('#item_addition').change(function () {
              if ($(this).val() == 1) {
                  bootbox.alert('<?=lang('product_variants_feature_x')?>');
              }
          });
          var sac = $('#sac').val()
          if(sac == 1) {
              $('.nsac').slideUp();
          } else {
              $('.nsac').slideDown();
          }
          $('#sac').change(function () {
              if ($(this).val() == 1) {
                  $('.nsac').slideUp();
              } else {
                  $('.nsac').slideDown();
              }
          });

          $('#invoice_view').change(function(e) {
              if ($(this).val() == 2) {
                  $('#states').show();
              } else {
                  $('#states').hide();
              }
          });
          if ($('#invoice_view').val() == 2) {
              $('#states').show();
          } else {
              $('#states').hide();
          }


          $('.prefixes').on('input', function (e) {
              if (!/^[ a-záéíóúüñ]*$/i.test(this.value)) {
                  this.value = this.value.replace(/[^ a-záéíóúüñ]+/ig,"");
              }
          });

          $('[data-toggle="tooltip"]').tooltip();

          $(document).on('change', '#document_type', mostrarOcultarDigitoVerificacion);
          $(document).on('change', '#type_person', show_fields_type_person);
          $(document).on('change', '#country', load_states);
          $(document).on('change', '#state', load_cities);

          $(document).on('change', '#customer_default_country', load_customer_states);
          $(document).on('change', '#customer_default_state', load_customer_cities);
          $(document).on('change', '#technology_provider', function() { show_hide_user_password_electronic_billing(); });

          $('#type_person').trigger('change');
          $('#country').trigger('change');
          $('#customer_default_country').trigger('change');

          <?php if (!empty($types_customer_obligations)) { ?>
              var types_customer_obligations_array = [];
              <?php foreach ($types_customer_obligations as $type_customer_obligation) { ?>
                  types_customer_obligations_array.push('<?= $type_customer_obligation->types_obligations_id; ?>');
              <?php } ?>

              $('#types_obligations').select2({}).select2('val', types_customer_obligations_array);
          <?php } ?>
            mostrarOcultarDigitoVerificacion();

            $('#technology_provider').trigger('change');
            $('#prioridad_precios_producto').trigger('change');

            $('#form_settings').validate({
                ignore: []
            });

            $('.send_settings').on('click', function(){
                if ($('#form_settings').valid()) {
                    $('#form_settings').submit();
                }
            });

        $('#electronic_billing').trigger('change');
        $('#except_category_taxes').trigger('change');

        $(document).on('change', '#city', function(){ get_postal_code(); });
        <?php if (!$this->Owner) { ?>
            setTimeout(function() {
                if ($('#prioridad_precios_producto').val()) {
                    $('#prioridad_precios_producto').select2('readonly', true);
                }
            }, 800);
        <?php } ?>


        <?php if (!empty($this->Settings->ciiu_code)): ?>
            $('#ciiu_code').val('<?= $this->Settings->ciiu_code ?>').select2({
                minimumInputLength: 1,
                multiple: true,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"system_settings/get_ciiu_code/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "system_settings/ciiu_suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        <?php else: ?>
            $('#ciiu_code').select2({
                minimumInputLength: 1,
                multiple: true,
                ajax: {
                    url: site.base_url + "system_settings/ciiu_suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        <?php endif ?>
        $('#profitability_margin_validation').trigger('change');
        $('#customer_default_payment_type').trigger('change');
        $('#aiu_management').trigger('change');
        calculate_ca_percentage();


        verificationFinancingModule()

        loadCurrentInterestAutocomplete()
        loadCurrentDefaultAutocomplete()
        loadProcreditoCommissionAutocomplete()
        loadProcreditoInterestAutocomplete()
    });

    function get_webservices_url()
    {
        if ($('#electronic_billing').val() == 1) {
            var technology_provider = $('#technology_provider').val();
            var work_environment = $('#work_environment').val();
            if (technology_provider == '') {
                Command: toastr.error('Debe seleccionar un proveedor tecnológico.', 'Validación', {onHidden: function() { $("#webservices_url").val(''); }});
                return false;
            }
            $.ajax({
                url: '<?= admin_url("system_settings/get_webservices_url/"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'technology_provider': technology_provider,
                    'work_environment' : work_environment
                },
            })
            .done(function(data) {
                if (data.response == false) {
                    Command: toastr.warning(data.message, 'Validación', {onHidden: function() { $("#webservices_url").val(''); }});
                    return false;
                }
                $("#webservices_url").val(data.data.url);
            })
            .fail(function(data) {
                console.log(data.responseText);
            });
        }
    }


    function load_states()
    {
        country = $('#country option:selected').data('code');
        code_iso = $('#country option:selected').data('code_iso');

        $('input[name="code_iso"]').val(code_iso);

        $.ajax({
            url: '<?= admin_url("system_settings/load_states_by_country_id/"); ?>'+ country
        })
        .done(function(data)
        {
            $('#state').html(data);

            <?php if(! empty($Settings->departamento)) { ?>
                $('#state').select2('val', '<?= $Settings->departamento; ?>');
                $('#state').trigger('change');
            <?php } ?>
        })
        .fail(function(data) { console.log(data.responseText); });
    }

    function load_cities()
    {
        state = $('#state option:selected').data('code');

        $.ajax({
            url: '<?= admin_url("system_settings/load_cities_by_state_id/"); ?>'+ state
        })
        .done(function(data)
        {
            $('#city').html(data);
            <?php if(! empty($Settings->ciudad)) { ?>
                $('#city').select2('val', '<?= $Settings->ciudad; ?>');

                $('#city').trigger('change');
            <?php } ?>
        })
        .fail(function(data) { console.log(data.responseText); });
    }

    function load_customer_states()
    {
        country = $('#customer_default_country option:selected').data('code');
        code_iso = $('#customer_default_country option:selected').data('code_iso');

        $.ajax({
            url: '<?= admin_url("system_settings/load_states_by_country_id/"); ?>'+ country
        })
        .done(function(data)
        {
            $('#customer_default_state').html(data);

            <?php if(! empty($Settings->departamento)) { ?>
                $('#customer_default_state').select2('val', '<?= $Settings->customer_default_state; ?>');
                $('#customer_default_state').trigger('change');
            <?php } ?>
        })
        .fail(function(data) { console.log(data.responseText); });
    }

    function load_customer_cities()
    {
        state = $('#customer_default_state option:selected').data('code');

        $.ajax({
            url: '<?= admin_url("system_settings/load_cities_by_state_id/"); ?>'+ state
        })
        .done(function(data)
        {
            $('#customer_default_city').html(data);
            <?php if(! empty($Settings->ciudad)) { ?>
                $('#customer_default_city').select2('val', '<?= $Settings->customer_default_city; ?>');
            <?php } ?>
        })
        .fail(function(data) { console.log(data.responseText); });
    }

    function show_fields_type_person()
    {
        var type_person = $('#type_person').val();

        if (type_person == '<?= LEGAL_PERSON; ?>') {
            $('.legal_person_field').show();
            $('.natural_person_field').hide();
            $('.natural_person_field_required').prop('required', false);
            $('#document_type').select2('val', 31);

            $('#type_vat_regime').select2('val', <?= COMMON; ?>);

            $('#business_name').attr('required', 'required');

            $('#first_name').removeAttr('required');
            $('#first_lastname').removeAttr('required');
        } else if (type_person == '<?= NATURAL_PERSON; ?>') {
            $('.legal_person_field').hide();
            $('.natural_person_field').show();
            $('.natural_person_field_required').prop('required', true);

            $('#document_type option').each(function() {
                $(this).removeAttr('disabled');
            });

            $('#document_type').select2('val', '');
            $('#type_vat_regime').select2('val', '');

            $('#first_name').attr('required', 'required');
            $('#first_lastname').attr('required', 'required');

            $('#business_name').removeAttr('required');
        } else {
            $('.legal_person_field').hide();
            $('.natural_person_field').hide();
            $('.natural_person_field_required').prop('required', true);
        }
    }

    function show_hide_electronic_billing_container() {
        var electronic_billing = $('#electronic_billing').val();

        if (electronic_billing == "1") {
            $('.electronig_billing_container').fadeIn();

            $('#technology_provider').val('<?= $this->Settings->fe_technology_provider; ?>');
            $('#technology_provider').attr('required', 'required');
            $('#send_electronic_invoice_to').attr('required', true);
            $('#type_vat_regime').attr('required', true);
        } else {
            $('.electronig_billing_container').fadeOut();

            $('#technology_provider').val('');
            $('#technology_provider').removeAttr('required');
            $('#send_electronic_invoice_to').attr('required', false);
            $('#type_vat_regime').attr('required', false);
        }
        show_hide_user_password_electronic_billing();
    }

    $('#tax_rate_traslate').on('change', function(){
        val = $(this).val();
        taxRateTraslate(val);
    });
    taxRateTraslate('<?= $this->Settings->tax_rate_traslate ?>');

    function taxRateTraslate(val)
    {
        if (val == 1) {
            $('.div_tax_rate_translate').css('display', '');

                $('#tax_rate_traslate_payment_ledger_id').prop('required', true);
                $('#tax_rate_traslate_receipt_ledger_id').prop('required', true);
                $('#trm_difference_payment_ledger_id').prop('required', true);
                $('#trm_difference_receipt_ledger_id').prop('required', true);
        } else if (val == 0) {
            $('.div_tax_rate_translate').css('display', 'none');

                $('#tax_rate_traslate_payment_ledger_id').prop('required', false);
                $('#tax_rate_traslate_receipt_ledger_id').prop('required', false);
                $('#trm_difference_payment_ledger_id').prop('required', false);
                $('#trm_difference_receipt_ledger_id').prop('required', false);
        }
    }

    $(document).on('change', '#cost_center_selection', function(){

        var val = $(this).val();

        if (val != 2) {
            $('.default_cost_center').css('display', '');
            $('#default_cost_center').prop('required', true);
        } else {
            $('.default_cost_center').css('display', 'none');
            $('#default_cost_center').prop('required', false);
        }
    });

    function mostrarOcultarDigitoVerificacion()
    {
        var tipo_documento = $('#document_type').val();
        if (tipo_documento == 31) {
            $('#contenedor_digito_verificacion').fadeIn();
            $('#digito_verificacion').attr("required", true);
            $('#document_number').trigger('change');
        } else {
            $('#contenedor_digito_verificacion').fadeOut();
            $('#digito_verificacion').removeAttr("required");
        }
    }

    $(document).on('ifChecked', '.modulary', function(){
        // $('.div_modulary').css('display', '');
        // $('#overselling').select2('val', 0).select2('readonly', true);
        // Command: toastr.warning('Al activar el modular con contabilidad, no se pueden manejar sobreventas, por lo que dicho parámetro se desactivará', '¡Atención!', {onHidden: function() {  }});
        $('.div_accounting_year').fadeIn();
        $('#accounting_year_suffix').prop('required', true);
    });

    $(document).on('ifUnchecked', '.modulary', function(){
        // $('.div_modulary').css('display', 'none');
        // $('#overselling').select2('val', "<?= $Settings->overselling ?>").select2('readonly', false);
        $('.div_accounting_year').fadeOut();
        $('#accounting_year_suffix').prop('required', false);
    });

    $(document).on('change', '#tax_method', function(){

        tax_method_id = $(this).val();

        $.ajax({
            url: "<?= admin_url('system_settings/validate_products_tax_methods') ?>",
            type: "get" ,
            data : {tax_method : tax_method_id} ,
            dataType : 'json' ,
        }).done(function(data){
            if (data.different_defined_method) {
                // bootbox.alert('Productos con diferente método : '+data.product_counting);

                bootbox.confirm({
                    message: "<h3> <?= lang('products_with_different_tax_method') ?>  </h3>",
                    size: "large",
                    buttons: {
                        confirm: {
                            label: '<?= lang("submit") ?>',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: '<?= lang("cancel") ?>',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (!result) {
                            $('#tax_method').select2('val', 2);
                        }
                    }
                });
            }
        });

    });

    $(document).on('change', '#tax_rate_traslate', function(){

        modulary = $('.modulary').prop('checked');

        trt = $(this).val();
        select = $(this);

        if (trt == 1 && modulary) {
            $.ajax({
                url : "<?= admin_url('system_settings/get_account_parameter_method') ?>"
            }).done(function(data){
                if (data == 2) {
                    bootbox.alert("<?= lang('cannot_activate_tax_traslate') ?>", function(){
                        select.val(0).trigger('change');
                    });
                }
            });
        }

    });

    $(document).on('change', '#prioridad_precios_producto', function(){
        var val = $(this).val();

        if (val == 5 || val == 7 || val == 10 || val == 11) {
            $('.ppu_div').css('display', '');
            $('#precios_por_unidad_presentacion').prop('required', true);
        } else {
            $('.ppu_div').css('display', 'none');
            $('#precios_por_unidad_presentacion').prop('required', false);
        }

    });

    function show_hide_user_password_electronic_billing()
    {
        var technology_provider = $('#technology_provider').val();

        if (technology_provider == 1 || technology_provider == 2) {
            $('#fe_user').attr('required', 'required');
            $('#fe_password').attr('required', 'required');

            $('#userCredentialContainer').fadeIn();
        } else {
            $('#userCredentialContainer').fadeOut();

            $('#fe_user').removeAttr('required');
            $('#fe_password').removeAttr('required');
        }
    }

    function get_postal_code() {
        code_country = $('#country option:selected').data('code');
        code = $('#city option:selected').data('code');

        if (code === undefined) {
            postal_code = '';
            code = '';
        } else {
            postal_code = code.toString().replace(code_country, '');
        }

        $('#postal_code').val(postal_code);
        $('#city_code').val(code);
    }

    $(document).on('change', '#except_category_taxes', function(){
        val = $(this).val();

        if (val == 1) {
            $('.except_category').fadeIn();
            $('.except_category').find('.form-control').prop('required', true);
        } else {
            $('.except_category').fadeOut();
            $('.except_category').find('.form-control').prop('required', false);
        }

    });
    $(document).on('change', '#overselling', function(){
        val = $(this).val();

        if (val == 1) {
                Command: toastr.warning('<?= lang("overselling_warning_toast") ?>', '¡Atención!', {onHidden: function() { $("#webservices_url").val(''); }});
        }

    });

    $(document).on('click', '.clean_ciiu_code', function(){
        $('#ciiu_code').select2('val', '');
    });

    $(document).on('change', '#disable_product_price_under_cost', function(){
        if ($(this).val() == 1) {
            $('#profitability_margin_validation').select2('val', '').trigger('change');
        }
    });
    $(document).on('change', '#profitability_margin_validation', function(){
        if ($(this).val() != 0) {
            $('#disable_product_price_under_cost').select2('val', 0);
            $('.div_margin_wc').fadeIn();
        } else if ($(this).val() == 0) {
            $('.div_margin_wc').fadeOut();
        }
    });

    $(document).on('change', '#big_data_limit_reports', function(){
        if ($(this).val() == 1 && $('#default_records_filter').val() == 0) {
            $('#default_records_filter option').each(function(index, option){
                if ($(option).val() == 0) {
                    $(option).prop('disabled', true);
                }
            });
            $('#default_records_filter').select2('val', '');
        } else if ($(this).val() == 0) {
            $('#default_records_filter option').prop('disabled', false);
        }
    });

    $(document).on('change', '#customer_default_payment_type', function(){
      val = $(this).val();
      if (val == 0) {
        $('.credit_customer').css('display', '');
        $('#customer_default_credit_limit').prop('required', true);
        $('#customer_default_payment_term').prop('required', true);
      } else if (val == 1) {
        $('.credit_customer').css('display', 'none');
        $('#customer_default_credit_limit').prop('required', false);
        $('#customer_default_payment_term').prop('required', false);
      }
    });

    $(document).on('change', '.each_spent, .ca_point, .ca_point_value', function(){
        calculate_ca_percentage();
    });

    function calculate_ca_percentage(){
        each_spent = $('.each_spent').val();
        ca_point = $('.ca_point').val();
        ca_point_value = $('.ca_point_value').val();
        percentage = (ca_point_value * 100) / each_spent;
        $('.info_ca_percentage').html('El valor de un punto al redimir equivale al <b>'+formatDecimal(percentage)+'%</b> del valor para obtenerlo');
    }

    var sys_order_sale_notification;
    $(document).on('change', '#order_sale_notification', function(){
        if (sys_order_sale_notification) {
            sys_order_sale_notification.pause();
            sys_order_sale_notification.currentTime = 0;
        }
        if (sys_pos_order_preparation_notification) {
            sys_pos_order_preparation_notification.pause();
            sys_pos_order_preparation_notification.currentTime = 0;
        }
        sys_order_sale_notification = new Audio('<?= $assets.'/sounds/' ?>'+$(this).val());
        sys_order_sale_notification.play();
    });
    var sys_pos_order_preparation_notification;
    $(document).on('change', '#pos_order_preparation_notification', function(){
        if (sys_order_sale_notification) {
            sys_order_sale_notification.pause();
            sys_order_sale_notification.currentTime = 0;
        }
        if (sys_pos_order_preparation_notification) {
            sys_pos_order_preparation_notification.pause();
            sys_pos_order_preparation_notification.currentTime = 0;
        }
        sys_pos_order_preparation_notification = new Audio('<?= $assets.'/sounds/' ?>'+$(this).val());
        sys_pos_order_preparation_notification.play();
    });
    $(document).on('change', '#aiu_management', function(){
        if ($(this).val() == 1) {
            $('.div_aiu_perc').fadeIn();
        } else {
            $('.div_aiu_perc').fadeOut();
        }
    });


    $(document).on('change', '#days_count_birthday_discount_application', function(){
        arr = ["0","4"];
        if (arr.includes($(this).val())) {
            $('.hbd_div').fadeOut();
            $('#days_count_birthday_discount').val(0);
        } else {
            $('.hbd_div').fadeIn();
            $('#days_count_birthday_discount').val(site.settings.days_count_birthday_discount);
        }
    });

    function verificationFinancingModule()
    {
        $('#financing_module').on('ifChecked', function(e) {
            $('#financingModuleContainer').css({'display': 'block'});
            makeFinancingFieldsRequired()
        })

        $('#financing_module').on('ifUnchecked', function(e) {
            $('#financingModuleContainer').css({'display': 'none'});
            makeFinancingFieldsRequired(false)
        })

        let financingModule = '<?= $this->Settings->financing_module ?>'
        if (financingModule == 1) {
            $('#financing_module').iCheck('check');
        }
    }

    function makeFinancingFieldsRequired(required = true)
    {
        $('#payment_method_for_credit_financing').prop('required', required);
        $('#current_interest_percentage_credit_financing').prop('required', required);
        $('#default_interest_percentage_credit_financing').prop('required', required);
        $('#insurance_percentage_credit_financing').prop('required', required);
        $('#quota_calculation_method_credit_financing').prop('required', required);
        $('#concepts_for_current_interest').prop('required', required);
        $('#concepts_for_default_interest').prop('required', required);
    }

    // function loadGenerateAutomaticInvoice()
    // {
    //     $('#generate_automatic_invoice').on('ifChecked', function(e) {
    //         loadConceptsInterestComponent()
    //     });
    //     $('#generate_automatic_invoice').on('ifUnchecked', function(e) {
    //         loadConceptsInterestComponent(false)
    //     });

    //     let generateAutomaticInvoice = '<?= $this->Settings->generate_automatic_invoice ?>'
    //     if (generateAutomaticInvoice == 1) {
    //         $('#generate_automatic_invoice').iCheck('check');
    //     }
    // }

    function loadCurrentInterestAutocomplete()
    {
        let conceptsForCurrentInterest = '<?= $this->Settings->concepts_for_current_interest ?>'
        $('#concepts_for_current_interest').val(conceptsForCurrentInterest).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url + 'products/servicesSuggestions/',
                    dataType: "json",
                    data: {
                        term: $(element).val()
                    },
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "products/servicesSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function loadCurrentDefaultAutocomplete()
    {
        let conceptsForDefaultInterest = '<?= $this->Settings->concepts_for_default_interest ?>'
        $('#concepts_for_default_interest').val(conceptsForDefaultInterest).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url + 'products/servicesSuggestions/',
                    dataType: "json",
                    data: {
                        term: $(element).val()
                    },
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "products/servicesSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function loadProcreditoCommissionAutocomplete()
    {
        let conceptProcreditoCommission = '<?= $this->Settings->concept_for_procredito_commission ?>'
        $('#concept_for_procredito_commission').val(conceptProcreditoCommission).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url + 'products/servicesSuggestions/',
                    dataType: "json",
                    data: {
                        term: $(element).val()
                    },
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "products/servicesSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function loadProcreditoInterestAutocomplete()
    {
        let conceptProcreditoInterest = '<?= $this->Settings->concept_for_procredito_interest ?>'
        $('#concept_for_procredito_interest').val(conceptProcreditoInterest).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url + 'products/servicesSuggestions/',
                    dataType: "json",
                    data: {
                        term: $(element).val()
                    },
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "products/servicesSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }
    $('#document_number').on('change', function(){
          nit = $(this).val();
          if (site.settings.get_companies_check_digit == 1) {
            dvf = calcularDigitoVerificacion(nit);
            $('#digito_verificacion').val(dvf);
          }
      });

    $(document).on('change', '#technology_provider', function(){
        const provider = $('#technology_provider').val();
        if (provider == 3) {
            $('.events_simba').css('display', 'block');
        }else{
            $('.events_simba').css('display', 'none');
        }
    });

    $('#reg_conv_simba_status').on('ifChecked', function(event) {
        $.ajax({
            url: '<?= admin_url("system_settings/syncSimba/"); ?>',
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'fe_provider' : $('#technology_provider').val(),
                'fe_environment' : $('#work_environment').val(),
            },
        })
        .done(function(data) {
            if (data.response == false) {
                header_alert('warning', data.message)
                return false;
            }else{
                $("#reg_conv_simba_status").attr('disabled', true);
                header_alert('success', data.message)
                return false;
            }
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    })

    $('#qualification_simba_status').on('ifChecked', function(event) {
        $.ajax({
            url: '<?= admin_url("system_settings/HabilitarFE/"); ?>',
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'setTestId' : $('#test_id').val(),
            },
        })
        .done(function(data) {
            if (data.response == false) {
                header_alert('warning', data.message)
                return false;
            }else{
                $("#qualification_simba_status").attr('disabled', true);
                header_alert('success', data.message)
                return false;
            }
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    });

    $('#quali_simba_status_pofe').on('ifChecked', function(event) {
        $.ajax({
            url: '<?= admin_url("system_settings/HabilitarPoFE/"); ?>',
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'setTestId' : $('#test_id').val(),
            },
        })
        .done(function(data) {
            if (data.response == false) {
                header_alert('warning', data.message)
                return false;
            }else{
                $("#quali_simba_status_pofe").attr('disabled', true);
                header_alert('success', data.message)
                return false;
            }
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    });

    $(document).on('change', '#recurring_sale_management', function(){
        if ($(this).val() == 1) {
            $('#osdt_recurring_sales').select2('open');
            $('#osdt_recurring_sales').prop('required', true);
        } else {
            $('#osdt_recurring_sales').prop('required', false);
        }
    });

</script>
