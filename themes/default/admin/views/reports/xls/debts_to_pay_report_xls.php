<?php
require_once APPPATH.'../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

$gran_total_venta = 0;
$gran_total_pago = 0;
$gran_total_saldo = 0;
$gran_total_30 = 0;
$gran_total_60 = 0;
$gran_total_90 = 0;
$gran_total_120 = 0;
$gran_total_mas = 0;
$supplier =  '' ; 
$total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;
$i = $j = 0 ;
$saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';


$j++;
$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(30);

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1:I1")->getFont()->setBold(true);

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, 'REFERENCIA COMPRA')
			->setCellValue('B'.$j, 'FECHA')
			->setCellValue('C'.$j, 'VALOR COMPRA')
			->setCellValue('D'.$j, 'SALDO 0 - 30')
			->setCellValue('E'.$j, 'SALDO 31 - 60')
			->setCellValue('F'.$j, 'SALDO 61 - 90')
			->setCellValue('G'.$j, 'SALDO 91 - 120')
			->setCellValue('H'.$j, 'SALDO 121 o mas')
			->setCellValue('I'.$j, 'SALDO TOTAL') 
			->setCellValue('J'.$j, 'FECHA VENCIMIENTO') 
			->setCellValue('K'.$j, 'DÍAS MORA') ;

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(16);

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultColumnDimension()
			->setWidth(16);

foreach ($purchases as $purchase) 
{
    $validador = true;
    $i++;
	$saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';

	if ($purchase['cod_companie'] != $supplier)
    {       
        if ($i > 1)
        {
			$j++;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':B'.$j)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':H'.$j)->getAlignment()->setHorizontal(
				PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
			);
			$objPHPExcel->setActiveSheetIndex(0)
											->setCellValue('A'.$j, 'SubTotal Proveedor')
											->setCellValue('D'.$j, number_format($total_30,0,'',''))
											->setCellValue('E'.$j, number_format($total_60,0,'',''))
											->setCellValue('F'.$j, number_format($total_90,0,'',''))
											->setCellValue('G'.$j, number_format($total_120,0,'',''))
											->setCellValue('H'.$j, number_format($total_mas,0,'',''))
											->setCellValue('I'.$j, number_format($total,0,'','')) ;	
            $validador = false;
			$gran_total_30 += $total_30;
			$gran_total_60 += $total_60;
			$gran_total_90 += $total_90;
			$gran_total_120 += $total_120;
			$gran_total_mas += $total_mas;
			$gran_total_saldo += $total;
        }

		$j++;
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':I'.$j.''); 
		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$j, 'Proveedor: '.$purchase['supplier']) ;

        $supplier = $purchase['cod_companie']; 
        $total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;  
		$j++;
    }

    $fecha = $select_due_days_date == 1 ? substr($purchase['Fecha'],0,10) : substr($purchase['due_date'],0,10);
    $fecha_now = date('Y-m-d');
    $dias	= (strtotime($fecha)-strtotime($fecha_now))/86400;

    if ($dias < 0) {
        $dias   = abs($dias); 
        $dias   = floor($dias);
    } else {
        $dias  = 0;
    }

    $Saldo = number_format($purchase['Saldo'],0,'','');

    if ($dias <=30) 
    {
        $saldo_30 = $Saldo;
        $total_30 += $purchase['Saldo'];
    }
    else if ($dias <=60)
    {
        $saldo_60 = $Saldo;
        $total_60 += $purchase['Saldo'];
    }
    else if ($dias <=90)
    {
        $saldo_90 = $Saldo;
        $total_90 += $purchase['Saldo'];
    }
    elseif ($dias <=120) 
    { 
        $saldo_120 = $Saldo;
        $total_120 += $purchase['Saldo'];
    }
    else
    {
        $saldo_mas = $Saldo;
        $total_mas += $purchase['Saldo'];
    }
    $total += $purchase['Saldo'];

	$objPHPExcel->setActiveSheetIndex(0)->getStyle('C'.$j.':I'.$j)->getAlignment()->setHorizontal(
		PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	);
	if (!$view_summary) {
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$j, $purchase['Referencia'])
					->setCellValue('B'.$j, $purchase['Fecha'])
					->setCellValue('C'.$j, $purchase['total'])
					->setCellValue('D'.$j, $saldo_30)
					->setCellValue('E'.$j, $saldo_60)
					->setCellValue('F'.$j, $saldo_90)
					->setCellValue('G'.$j, $saldo_120)
					->setCellValue('H'.$j, $saldo_mas) 
					->setCellValue('I'.$j, $Saldo) 
					->setCellValue('J'.$j, $purchase['due_date']) 
					->setCellValue('K'.$j, $dias) 
					;

		$j++;
	}
}

$j++;
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':B'.$j)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':H'.$j)->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$j, 'SubTotal Proveedor')
					->setCellValue('D'.$j, number_format($total_30,0,'',''))
					->setCellValue('E'.$j, number_format($total_60,0,'',''))
					->setCellValue('F'.$j, number_format($total_90,0,'',''))
					->setCellValue('G'.$j, number_format($total_120,0,'',''))
					->setCellValue('H'.$j, number_format($total_mas,0,'',''))
					->setCellValue('I'.$j, number_format($total,0,'','')) ;	

$gran_total_30 += $total_30;
$gran_total_60 += $total_60;
$gran_total_90 += $total_90;
$gran_total_120 += $total_120;
$gran_total_mas += $total_mas;	
$gran_total_saldo += $total;
$j++;
$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$j, 'Total informe')
					->setCellValue('D'.$j, number_format($gran_total_30,0,'',''))
					->setCellValue('E'.$j, number_format($gran_total_60,0,'',''))
					->setCellValue('F'.$j, number_format($gran_total_90,0,'',''))
					->setCellValue('G'.$j, number_format($gran_total_120,0,'',''))
					->setCellValue('H'.$j, number_format($gran_total_mas,0,'',''))
					->setCellValue('I'.$j, number_format($gran_total_saldo,0,'','')) ;		


$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('D1:D'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('E1:E'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('F1:F'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('H1:H'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('I1:I'.$j)->getNumberFormat()->setFormatCode('#,##0.00');



// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('CUENTAS POR PAGAR POR EDADES');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="CUENTAS POR PAGAR POR EDADES.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
