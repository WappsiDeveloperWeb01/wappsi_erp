<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
<?= admin_form_open_multipart("products/update_csv_fix_products_cost", "id='csv_recosting_form'") ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5>Formulario</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <?= lang('start_date', 'rc_start_date') ?>
                                    <input type="date" name="start_date" id="rc_start_date" class="form-control"  value="<?= date('Y-01-01') ?>" required>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <?= lang('end_date', 'rc_end_date') ?>
                                    <input type="date" value="<?= date('Y-m-d') ?>" name="end_date" id="rc_end_date" class="form-control" required>
                                </div>
                                <div class="col-md-4">
                                    <label>Documentos a afectar</label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple" style="width: 100%;">
                                      <?php if ($document_types): ?>
                                        <?php foreach ($document_types as $dt): ?>
                                          <?php if ($dt->module == 11): ?>
                                          <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" selected='selected'> <?= $dt->sales_prefix ?> </option>
                                          <?php endif ?>
                                        <?php endforeach ?>
                                      <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <div class="clearfix"></div>
                                    <div class="well well-sm">
                                        <a href="<?php echo $this->config->base_url(); ?>assets/csv/sample_fix_products_cost.csv"
                                        class="btn btn-primary pull-right"><i class="fa fa-download"></i> <?= lang('download_sample_file') ?></a>
                                        <span class="text-warning"><?php echo $this->lang->line("csv1"); ?></span><br>
                                        <?php echo $this->lang->line("csv2"); ?> <span class="text-info">(<?= lang("product_code") . ', ' . lang("cost"); ?> )</span><br>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <?= lang("csv_file", "csv_file") ?>
                                    <input id="csv_file" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" required="required"
                                            data-show-upload="false" data-show-preview="false" class="form-control file">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="prioritize_last_cost" checked>
                                            Priorizar costo de última compra antes o dentro del rango de fechas (Si no hay, toma el del archivo CSV)
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <button type="button" name="boton_name" id="submit_recosting" class="btn btn-primary"> <?= lang("send"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= form_close() ?>

<script type="text/javascript">
    $(document).on('click', '#submit_recosting', function(){
        if ($('#csv_recosting_form').valid()) {
            $('#csv_recosting_form').submit();
        }
    });
</script>