<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row wrapper border-bottom white-bg page-heading no-print">
    <div class="col-lg-8">
        <h2><?= $page_title ?></h2>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight  no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="types_movement_table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr class="active">
                                    	<th></th>
                                        <th><?= lang('types_movement_type') ?></th>
                                        <th><?= lang('types_movement_accounting_account_name') ?></th>
                                        <th><?= lang('types_movement_counterparty_account_name') ?></th>
                                        <th><?= lang('types_movement_document_type_name') ?></th>
                                        <th><?= lang('actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                        	<th></th>
                                            <th><?= lang('types_movement_type') ?></th>
	                                        <th><?= lang('types_movement_accounting_account_name') ?></th>
	                                        <th><?= lang('types_movement_counterparty_account_name') ?></th>
	                                        <th><?= lang('types_movement_document_type_name') ?></th>
                                            <th><?= lang('actions'); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
		load_types_movement_table();
	});

	function load_types_movement_table()
	{
	    if ($(window).width() < 1000) {
	        var nums = [[10, 25], [10, 25]];
	    } else {
	        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
	    }

        oTable = $('#types_movement_table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": nums,
            "iDisplayLength":  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=admin_url('system_settings/get_types_movement_datatables')?>',
            dom: 'lr<"containerBtn"><"inputFiltro"f>tip<"clear">',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {},
            drawCallback: function(settings) {
		        $('.containerBtn').html('<div class="dropdown pull-right">'+
		                            		'<button class="btn btn-outline btn-success" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true">'+
		                              			'<?= lang('actions') ?> <span class="caret"></span>'+
		                        			'</button>'+
			                            '<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu2">'+
			                              	'<li>'+
				                                '<a href="<?= admin_url('system_settings/add_types_movement') ?>" data-toggle="modal" data-target="#myModal" class="tip" title="" data-original-title="<?= lang('types_movement_add') ?>">'+
				                                    '<i class="fa fa-plus-circle"></i> <?= lang('types_movement_add') ?>'+
				                                '</a>'+
			                                '</li>'+
			                            '</ul>'+
			                          '</div>');
		    },
            aoColumns: [
                { 'bVisible':false },
                {
                    mRender:  function ( data, type, row ) {
                        if (data == "adjustment") {
                            return 'Ajuste';
                        } else {
                            return 'Orden de producción';
                        }
                    },
                },
                null,
                null,
                null,
                null
            ],
            fnFooterCallback: function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var nCells = nRow.getElementsByTagName('th');
            }
        }).fnSetFilteringDelay().dtFilter([], "footer");
	}
</script>