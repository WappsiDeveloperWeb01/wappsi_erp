<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
@media print {
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 3px !important;
    }
}
</style>

<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Añadir notas a la venta</h3>
                </div>
                <div class="col-xs-6">
                    <label>Escoja una nota predefinida</label>
                    <?php
                    if ($invoce_notes != false) {
                        $iopts[''] = lang('select');
                        foreach ($invoce_notes as $in) {
                            $iopts[$in->id] = $in->description;
                        }
                    } else {
                        $iopts[''] = lang('no_data');
                    }

                     ?>
                     <div class="input-group">
                        <?= form_dropdown('invoice_notes', $iopts, '', 'id="invoice_notes" class="form-control pos-input-tip" style="width:100%;"'); ?>
                        <span class="input-group-addon"><button class="btn btn-primary" style="padding: 3px 8px; font-size: 70%;" id="add_invoice_note"><span class="fa fa-plus"></span></button></span>
                     </div>
                </div>
                <div class="col-xs-12">
                    <?= form_textarea('invoice_note', '', 'class="form-control" id="invoice_note"'); ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="add_invoice_note_sale">Añadir</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();

        $('#invoice_note').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
            }
        });

        note = $('#payment_note_1').redactor('get');

        if (note != '') {
            $('#invoice_note').redactor('set', note);
        }

    });

        $('#add_invoice_note').on('click', function(){
                select = $('#invoice_notes');
                p_note = $('#invoice_note').redactor('get');
                note = $('#invoice_notes option:selected').text();

                if (select.val() != '') {
                        $('#invoice_note').redactor('set', p_note+note);
                } else {
                    select.focus();
                }

        });


        $('#add_invoice_note_sale').on('click', function(){
                note = $('#invoice_note').redactor('get');
                $('#payment_note_1').redactor('set', note);
                $('#myModal').modal('hide');
        });

</script>
