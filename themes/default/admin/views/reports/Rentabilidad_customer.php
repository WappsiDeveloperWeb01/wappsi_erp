<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$v = "";
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<?php
	$fech_ini = date('Y-m-d');
	$fech_fin = date("Y-m-d",strtotime($fech_ini."+ 1 days"));
	foreach ($profitabilitys as $profitability)
	{
		if($fech_ini>$profitability['Fecha'])
		{
			$fech_ini = $profitability['Fecha'];
		}
	}
	$fech_ini = substr($fech_ini,0,10);

	$fech_ini = (!empty($datos)) ? $datos['inicial'] : $fech_ini ;
	$fech_fin = (!empty($datos)) ? $datos['Final'] : $fech_fin ;

?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
					<div class="row">
                        <div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3><?php echo lang('filter_date');?> <span id="textini"><?php echo $fech_ini; ?></span> a <span id="textfin"><?php echo $fech_fin; ?></span> </h3>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<?php echo form_open('admin/reports/search_rentabilidad_customer' , 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-4">
												<?= lang('view_customer_total', 'Cliente') ?>
												<select class="form-control " name="Cliente" id="Cliente" data-target="Sucursal">
													<option value="all" >  <?php echo lang('select').' '.lang('view_customer_total'); ?> </option>
													<?php foreach ($customers as $customer): ?>
														<option value="<?php echo $customer['customer_id']; ?>" <?php echo ((!empty($datos)) AND ($datos['Cliente']==$customer['customer_id'])) ? 'selected' : ''; ?> ><?php echo $customer['name']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
											<?php
	                                    $bl[""] = "";
	                                    $bldata = [];
	                                    foreach ($billers as $biller) {
	                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
	                                        $bldata[$biller->id] = $biller;
	                                    }
	                                    // exit(var_dump($this->session->userdata('biller_id')));
	                                    ?>

	                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <?= lang("biller", "slbiller"); ?>
	                                                <select name="biller" class="form-control" id="slbiller" required="required">
	                                                    <option value=""><?= lang('select') ?></option>
	                                                    <?php foreach ($billers as $biller) : ?>
	                                                        <option value="<?= $biller->id ?>" <?php echo ((!empty($datos)) AND ($datos['biller']==$biller->id)) ? 'selected' : ''; ?> ><?= $biller->company ?></option>
	                                                    <?php endforeach ?>
	                                                </select>
	                                            </div>
	                                        </div>
	                                    <?php } else { ?>
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <?= lang("biller", "slbiller"); ?>
	                                                <select name="biller" class="form-control" id="slbiller">
	                                                    <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
	                                                        $biller = $bldata[$this->session->userdata('biller_id')];
	                                                    ?>
	                                                        <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
	                                                    <?php endif ?>
	                                                </select>
	                                            </div>
	                                        </div>
	                                    <?php } ?>
											<div class="clearfix"></div>
		                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
		                                        <?= lang('date_records_filter', 'date_records_filter_dh') ?>
		                                        <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
		                                            <?= $this->sma->get_filter_options(); ?>
		                                        </select>
		                                    </div>
											<div class="date_controls_dh">
												<?php if ($this->Settings->big_data_limit_reports == 1): ?>
		                                            <div class="col-sm-4 form-group">
		                                                <?= lang('filter_year', 'filter_year_dh') ?>
		                                                <select name="filter_year" id="filter_year_dh" class="form-control" required>
		                                                    <?php foreach ($this->filter_year_options as $key => $value): ?>
		                                                        <option value="<?= $key ?>"><?= $key ?></option>
		                                                    <?php endforeach ?>
		                                                </select>
		                                            </div>
		                                        <?php endif ?>
		                                        <div class="col-sm-4">
		                                            <div class="form-group">
		                                                <?= lang("start_date", "start_date_dh"); ?>
		                                                <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh"'); ?>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="form-group">
		                                                <?= lang("end_date", "end_date_dh"); ?>
		                                                <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh"'); ?>
		                                            </div>
		                                        </div>
		                                    </div>
										</div>
										<br>
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-3">
												<button type="submit" class="btn btn-success" class="Buscar" id="Buscar" ><?php echo lang('submit');?></button>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6">
											</div>
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="pull-right dropdown">
													<button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
													<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
														<li>
															<a class="ExportPdf" id="ExportPdf"> PDF </a>
														</li>
														<li>
															<a class="ExportXls" id="ExportXls"> Excel </a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
							<div class="col-lg-12">
								<div class="table-responsive">
									<table id="TableData" class="table table-stripped" >
										<thead>
											<tr>
												<th class="text-center" style="width: 24%;"> <b><?php echo lang('view_customer_total'); ?></b></th>
												<th class="text-right" style="width: 11%;"> <b><?php echo lang('total'); ?></b></th>
												<th class="text-right" style="width: 11%;"> <b><?php echo lang('cost'); ?></b></th>
												<th class="text-right" style="width: 11%;"> <b><?php echo lang('profit'); ?></b></th>
												<th class="text-right" style="width: 11%;"> <b><?php echo lang('margin'); ?></b></th>
											</tr>
										</thead>
										<tbody >
											<?php foreach ($profitabilitys as $profitability): ?>
												<tr >
													<td ><?php echo $profitability['customer']; ?></td>
													<td class="text-right"><?php echo $this->sma->formatMoney($profitability['total']); ?></td>
													<td class="text-right"><?php echo $this->sma->formatMoney($profitability['costo']); ?></td>
													<td class="text-right"><?php echo $this->sma->formatMoney($profitability['Utilidad']); ?></td>
													<td class="text-right"><?php echo $this->sma->formatDecimal($profitability['margen']); ?> % </td>
												</tr>
											<?php endforeach; ?>
										</tbody>
										<tfoot>
											<tr>
												<th> <b><?php echo lang('view_customer_total'); ?></b></th>
												<th> <b><?php echo lang('total'); ?></b></th>
												<th> <b><?php echo lang('cost'); ?></b></th>
												<th> <b><?php echo lang('profit'); ?></b></th>
												<th> <b><?php echo lang('margin'); ?></b></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<br>

<script >


	function currencyFormat(x) {
		return x ;
	}

	$(document).ready(function() {
		$('#Cliente').select2();

		var table = $('#TableData').dataTable({
			responsive: true,
    		pagination: true,
			pageLength: 100,
			oLanguage: <?php echo $dt_lang; ?>
		});

		$("#ExportPdf").click(function() {
			var Cliente = $("#Cliente option:selected").val() ? $("#Cliente option:selected").val() : 'null';
			var inicial = $('#start_date').val() ? $('#start_date').val() : 'null';
			var Final = $('#end_date').val() ? $('#end_date').val() : 'null';
			var biller = $('#biller').val() ? $('#biller').val() : 'null';
			window.open(site.base_url+'reports/profitabilitys_customer_export_pdf/'+Cliente+'/'+inicial+'/'+Final+'/'+biller);
		});
		$("#ExportXls").click(function() {
			var Cliente = $("#Cliente option:selected").val() ? $("#Cliente option:selected").val() : 'null';
			var inicial = $('#start_date').val() ? $('#start_date').val() : 'null';
			var Final = $('#end_date').val() ? $('#end_date').val() : 'null';
			var biller = $('#biller').val() ? $('#biller').val() : 'null';
			window.open(site.base_url+'reports/profitabilitys_customer_export_xls/'+Cliente+'/'+inicial+'/'+Final+'/'+biller);
		});
	} );

    $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });
$('#Buscar').on('click', function(e){
    $('#filter_action').val(1);
    $('#form_filter').submit();
});

</script>


</body>
</html>