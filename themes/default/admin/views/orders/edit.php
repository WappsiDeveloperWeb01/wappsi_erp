<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">

    localStorage.removeItem('oslcustomerspecialdiscount');
    <?php if ($this->session->userdata('remove_posls')) { ?>
        if (localStorage.getItem('positems')) {
            localStorage.removeItem('positems');
        }
        if (localStorage.getItem('posdiscount')) {
            localStorage.removeItem('posdiscount');
        }
        if (localStorage.getItem('postax2')) {
            localStorage.removeItem('postax2');
        }
        if (localStorage.getItem('posshipping')) {
            localStorage.removeItem('posshipping');
        }
        if (localStorage.getItem('posref')) {
            localStorage.removeItem('posref');
        }
        if (localStorage.getItem('poswarehouse')) {
            localStorage.removeItem('poswarehouse');
        }
        if (localStorage.getItem('posnote')) {
            localStorage.removeItem('posnote');
        }
        if (localStorage.getItem('posinnote')) {
            localStorage.removeItem('posinnote');
        }
        if (localStorage.getItem('poscustomer')) {
            localStorage.removeItem('poscustomer');
        }
        if (localStorage.getItem('poscurrency')) {
            localStorage.removeItem('poscurrency');
        }
        if (localStorage.getItem('posdate')) {
            localStorage.removeItem('posdate');
        }
        if (localStorage.getItem('posstatus')) {
            localStorage.removeItem('posstatus');
        }
        if (localStorage.getItem('posbiller')) {
            localStorage.removeItem('posbiller');
        }
        if (localStorage.getItem('posseller')) {
            localStorage.removeItem('posseller');
        }
        if (localStorage.getItem('posretenciones')) {
            localStorage.removeItem('posretenciones');
        }
        if (localStorage.getItem('restobar_mode_module')) {
            localStorage.removeItem('restobar_mode_module');
        }
        if (localStorage.getItem('tip_amount')) {
            localStorage.removeItem('tip_amount');
        }
        if (localStorage.getItem('delete_tip_amount')) {
            localStorage.removeItem('delete_tip_amount');
        }
        if (localStorage.getItem('poskeylog')) {
            localStorage.removeItem('poskeylog');
        }
        if (localStorage.getItem('poscustomerspecialdiscount')) {
            localStorage.removeItem('poscustomerspecialdiscount');
        }
        if (localStorage.getItem('pos_keep_prices')) {
            localStorage.removeItem('pos_keep_prices');
        }
        if (localStorage.getItem('pos_keep_prices_quote_id')) {
            localStorage.removeItem('pos_keep_prices_quote_id');
        }
        if (localStorage.getItem('restobar_mode_module')) {
            localStorage.removeItem('restobar_mode_module');
        }
    <?php $this->sma->unset_data('remove_posls');} ?>

    var count = 1, an = 1, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, allow_discount = <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? 1 : 0; ?>,
        tax_rates = <?php echo json_encode($tax_rates); ?>;
        converting = JSON.parse("<?= $converting; ?>");
        delivery_time_id = "<?= $inv->delivery_time_id ?>";
        inv_id = "<?= $inv->id ?>";
    //var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
    //var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function () {
        <?php if ($inv) { ?>

        $('#add_sale').css('display', 'none');
            command: toastr.warning('Por favor espere unos segundos...', 'Espere...', { onHidden : function(){
            //code...
        } });
        setTimeout(function() {
            $('#add_sale').css('display', '');
                command: toastr.success('Ya se completó la carga', 'Completo', { onHidden : function(){
                //code...
            } });
        }, 3000);
        localStorage.setItem('osldate', '<?= $this->sma->hrld($inv->date) ?>');
        localStorage.setItem('oslcustomer', '<?= $inv->customer_id ?>');
        localStorage.setItem('oslcustomerbranch', '<?= $inv->address_id ?>');
        localStorage.setItem('oslseller', '<?= $inv->seller_id ?>');
        localStorage.setItem('oslbiller', '<?= $inv->biller_id ?>');
        localStorage.setItem('oslref', '<?= $inv->reference_no ?>');
        localStorage.setItem('oslwarehouse', '<?= $inv->warehouse_id ?>');
        localStorage.setItem('oslsale_status', '<?= $inv->sale_status ?>');
        localStorage.setItem('paid_by', '<?= $inv->payment_method ?>');
        localStorage.setItem('oslpayment_status', '<?= $inv->payment_status ?>');
        localStorage.setItem('oslpayment_term', '<?= $inv->payment_term ?>');
        localStorage.setItem('oslnote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($inv->note)); ?>');
        localStorage.setItem('oslinnote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($inv->staff_note)); ?>');
        localStorage.setItem('osldiscount', '<?= $inv->order_discount_id ?>');
        localStorage.setItem('osltax2', '<?= $inv->order_tax_id ?>');
        localStorage.setItem('oslshipping', '<?= $inv->shipping ?>');
        localStorage.setItem('document_type_id', '<?= $inv->document_type_id ?>');
        localStorage.setItem('oslitems', JSON.stringify(<?= $inv_items; ?>));
        <?php } ?>
        <?php if ($Owner || $Admin) { ?>
        $(document).on('change', '#osldate', function (e) {
            localStorage.setItem('osldate', $(this).val());
        });
        if (osldate = localStorage.getItem('osldate')) {
            $('#osldate').val(osldate);
        }
        <?php } ?>
        $(document).on('change', '#oslbiller', function (e) {
            localStorage.setItem('oslbiller', $(this).val());
        });
        if (oslbiller = localStorage.getItem('oslbiller')) {
            $('#oslbiller').val(oslbiller);
        }
        ItemnTotals();

       $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#oslcustomer').val() || !$('#oslcustomerbranch').val() || !$('#document_type_id').val()) {
                    var msg = "";
                    if (!$('#oslcustomer').val()) {
                        msg = "</br><?= lang('customer') ?>";
                    }
                    if (!$('#oslcustomerbranch').val()) {
                        msg += "</br><?= lang('customer_branch') ?>";
                    }
                    if (!$('#document_type_id').val()) {
                        msg += "</br><?= lang('reference_no') ?>";
                    }
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    command: toastr.warning('<?=lang('select_above');?> : '+msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('sales/order_suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        biller_id: $("#oslbiller").val(),
                        warehouse_id: $("#oslwarehouse").val(),
                        address_id: $("#oslcustomerbranch").val(),
                        customer_id: $("#oslcustomer").val()
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {

                    if (ui.item.row.cnt_units_prices !== undefined && ui.item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && ui.item.row.promotion != 1) {
                        var item_id = ui.item.item_id;
                        var warehouse_id = $('#oslwarehouse').val();
                        $('.product_name_spumodal').text(ui.item.label);
                        $('#sPUModal').appendTo("body").modal('show');
                        add_item_unit(item_id, warehouse_id);
                        $(this).val('');
                    } else {
                        var row = add_invoice_item(ui.item);
                        if (row)
                        $(this).val('');
                    }
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $('#reset').click(function (e) {
            localStorage.setItem('remove_oslls', true);
        });
        // $('#edit_sale').click(function () {
        //     $(window).unbind('beforeunload');
        //     $('form.edit-so-form').submit();
        // });


        $('#oslbiller').trigger('change');

        setTimeout(function() {
            if (site.settings.order_sales_conversion == 2) {
                $('#redirect_order_to').select2('val', 'pos').select2('readonly', true);
            } else if (site.settings.order_sales_conversion == 3) {
                $('#redirect_order_to').select2('val', 'detal').select2('readonly', true);
            }
        }, 850);
    });
</script>


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open_multipart("sales/edit_order/" . $inv->id, ['id' => 'form_edit_order_sale']) ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php if ($Owner || $Admin) { ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("date", "osldate"); ?>
                                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($inv->date)), 'class="form-control input-tip datetime" id="osldate" required="required"'); ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php
                                            $bl[""] = "";
                                            $bldata = [];
                                            foreach ($billers as $biller) {
                                                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                $bldata[$biller->id] = $biller;
                                            }
                                        ?>
                                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("biller", "oslbiller"); ?>
                                                    <select name="biller" class="form-control" id="oslbiller" required="required">
                                                        <?php foreach ($billers as $biller): ?>
                                                        <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("biller", "oslbiller"); ?>
                                                    <select name="biller" class="form-control" id="oslbiller">
                                                        <?php if (isset($bldata[$this->session->userdata('biller_id')])):

                                                            $biller = $bldata[$this->session->userdata('biller_id')];

                                                            ?>
                                                            <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" selected><?= $biller->company ?></option>
                                                        <?php endif ?>
                                                    </select>
                                                </div>
                                            </div>

                                        <?php } ?>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("reference_no", "oslref"); ?>
                                                <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                            </div>
                                        </div>
                                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("warehouse", "oslwarehouse"); ?>
                                                    <?php
                                                    $wh[''] = '';
                                                    foreach ($warehouses as $warehouse) {
                                                        $wh[$warehouse->id] = $warehouse->name;
                                                    }
                                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="oslwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                                    ?>
                                                </div>
                                            </div>
                                        <?php } else {
                                            $warehouse_input = array(
                                                'type' => 'hidden',
                                                'name' => 'warehouse',
                                                'id' => 'oslwarehouse',
                                                'value' => $this->session->userdata('warehouse_id'),
                                            );

                                            echo form_input($warehouse_input);
                                        } ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("customer", "oslcustomer"); ?>
                                                <div class="input-group">
                                                    <?php
                                                    echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="oslcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                                    ?>
                                                    <div class="input-group-addon no-print" style="padding: 2px 8px; border-left: 0;">
                                                        <a href="#" id="toogle-customer-read-attr" class="external">
                                                            <i class="fa fa-pencil" id="addIcon" style="font-size: 1.2em;"></i>
                                                        </a>
                                                    </div>
                                                    <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
                                                        <a id="view-customer">
                                                            <i class="fa fa-eye" id="addIcon" style="font-size: 1.2em;"></i>
                                                        </a>
                                                    </div>
                                                    <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                        <a href="<?= admin_url('customers/add'); ?>" id="add-customer"class="external" data-toggle="modal" data-target="#myModal">
                                                            <i class="fa fa-plus-circle" id="addIcon"  style="font-size: 1.2em;"></i>
                                                        </a>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("customer_branch", "oslcustomerbranch"); ?>
                                                <?php
                                                echo form_dropdown('address_id', array('0' => lang("select") . ' ' . lang("customer")), (isset($_POST['address_id']) ? $_POST['address_id'] : ''), 'id="oslcustomerbranch" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                ?>
                                            </div>
                                        </div>


                                    <?php if ($this->Owner || $this->Admin || ($user_group_name != 'seller' && !$this->sma->keep_seller_from_user())): ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("seller", "oslseller"); ?>
                                                    <?php
                                                    echo form_dropdown('seller_id', array('0' => lang("select") . ' ' . lang("biller")), (isset($_POST['seller']) ? $_POST['seller'] : ''), 'id="oslseller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        <?php else: ?>
                                            <?php if ($this->sma->keep_seller_from_user()): ?>
                                                <input type="hidden" name="seller_id"  value="<?= $this->session->userdata('seller_id') ?>">
                                            <?php else: ?>
                                                <input type="hidden" name="seller_id"  value="<?= $this->session->userdata('company_id') ?>">
                                            <?php endif ?>
                                        <?php endif ?>

                                        <?php if ($this->Settings->management_order_sale_delivery_day == 1): ?>
                                            <div class="col-md-4 form-group">
                                                <?= lang('day_delivery_time', 'day_delivery_time') ?>
                                                <input type="date" name="day_delivery_time" id="day_delivery_time" class="form-control" min="<?= $inv->delivery_day ? $inv->delivery_day : date('Y-m-d') ?>" value="<?= $inv->delivery_day ? $inv->delivery_day : date('Y-m-d') ?>">
                                            </div>
                                            <?php if ($this->Settings->management_order_sale_delivery_time == 1): ?>
                                                <div class="col-md-4 form-group">
                                                    <?= lang('delivery_time', 'delivery_time') ?>
                                                    <select name="delivery_time" id="delivery_time" class="form-control"required>
                                                        <option value=""><?= lang('select').lang('day_delivery_time') ?></option>
                                                    </select>
                                                </div>
                                            <?php endif ?>
                                        <?php endif ?>
                                        
                                        <div class="col-md-12" id="sticker">
                                            <div class="well well-sm">
                                                <div class="form-group" style="margin-bottom:0;">
                                                    <div class="input-group wide-tip">
                                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                                        <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                            <a href="#" id="addManually">
                                                                <i class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i>
                                                            </a>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="control-group table-group">
                                                <label class="table-label"><?= lang("order_items"); ?> *</label>

                                                <div class="controls table-controls">
                                                    <table id="oslTable" class="table items  table-bordered table-condensed table-hover sortable_table">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 26.38%"><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                                                            <?php
                                                            if ($Settings->product_serial || $Settings->product_variant_per_serial == 1) {
                                                                echo '<th style="width: 07.69%">' . lang("serial_no") . '</th>';
                                                            }
                                                            ?>
                                                            <th style="width: 07.69%"><?= lang('gross_net_unit_price') ?></th>
                                                            <?php
                                                            if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {
                                                                echo '<th style="width: 01.69%">' . lang("discount") . '</th>';
                                                            }
                                                            ?>
                                                            <th style="width: 07.69%"><?= lang("price_x_discount"); ?></th>
                                                            <?php
                                                            if ($Settings->tax1) {
                                                                echo '<th style="width: 01.69%">' . lang("product_tax") . '</th>';
                                                                if ($Settings->ipoconsumo == 1) {
                                                                    echo '<th style="width: 01.69%">' . lang("second_product_tax") . '</th>';
                                                                }
                                                            }
                                                            ?>
                                                            <th style="width: 07.69%"><?= lang("price_x_tax"); ?></th>
                                                            <th style="width: 07.69%"><?= lang("quantity"); ?></th>
                                                            <th style="width: 07.69%"><?= lang("quantity_dispatch"); ?></th>
                                                            <th style="width: 07.69%"><?= lang("quantity_dispatch_pending"); ?></th>
                                                            <th style="width: 07.69%"><?= lang("quantity_dispatch_to_bill"); ?></th>
                                                            <th style="width: 07.69%">
                                                                <?= lang("total"); ?>
                                                                (<span class="currency"><?= $default_currency->code ?></span>)
                                                            </th>
                                                            <th style="width: 1%">
                                                                <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                        <tfoot></tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if ($Settings->tax2) { ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("order_tax", "osltax2"); ?>
                                                    <?php
                                                    $tr[""] = "";
                                                    foreach ($tax_rates as $tax) {
                                                        $tr[$tax->id] = $tax->name;
                                                    }
                                                    echo form_dropdown('order_tax', $tr, (isset($_POST['order_tax']) ? $_POST['order_tax'] : $Settings->default_tax_rate2), 'id="osltax2" data-placeholder="' . lang("select") . ' ' . lang("order_tax") . '" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if (($Owner || $Admin || $this->session->userdata('allow_discount')) || $inv->order_discount_id) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("order_discount", "osldiscount"); ?>
                                                <?php echo form_input('order_discount', '', 'class="form-control input-tip" id="osldiscount" '.(($Owner || $Admin || $this->session->userdata('allow_discount')) ? '' : 'readonly="true"')); ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="col-sm-4 div_paid_by">
                                            <div class="form-group">
                                                <?= lang("paying_by", "paid_by_1"); ?>
                                                <select name="paid_by" id="paid_by_1" data-pbnum="1" class="form-control paid_by">
                                                    <?= $this->sma->paid_opts(); ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 div_payment_term" style="display: none;">
                                            <div class="form-group">
                                                <?= lang("order_payment_term", "osl_payment_term"); ?>
                                                <?php echo form_input('payment_term', 1, 'class="form-control input-tip" id="osl_payment_term"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("shipping", "oslshipping"); ?>
                                                <?php echo form_input('shipping', '', 'class="form-control input-tip" id="oslshipping"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("document", "document") ?>
                                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                                    data-show-preview="false" class="form-control file">
                                            </div>
                                        </div>

                                        <!-- <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("sale_status", "oslsale_status"); ?>
                                                <?php $sst = array('pending' => lang('pending'), 'completed' => lang('completed'));
                                                echo form_dropdown('sale_status', $sst, '', 'class="form-control input-tip" required="required" id="oslsale_status"');
                                                ?>

                                            </div>
                                        </div> -->
                                        <!-- <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("payment_term", "oslpayment_term"); ?>
                                                <?php echo form_input('payment_term', '', 'class="form-control tip" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="oslpayment_term"'); ?>

                                            </div>
                                        </div> -->
                                        <?= form_hidden('payment_status', $inv->payment_status); ?>
                                        <div class="clearfix"></div>

                                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>

                                        <div class="row" id="bt">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang("sale_note", "oslnote"); ?><a href="<?= admin_url('sales/add_invoice_order_note') ?>" class="btn btn-primary" style="padding: 3px 8px; margin-left: 2px;" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span></a>
                                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="oslnote" style="margin-top: 10px; height: 100px;"'); ?>

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang("staff_note", "oslinnote"); ?>
                                                        <?php echo form_textarea('staff_note', (isset($_POST['staff_note']) ? $_POST['staff_note'] : ""), 'class="form-control" id="oslinnote" style="margin-top: 10px; height: 100px;"'); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <?= lang("redirect_order_to", "redirect_order_to"); ?>
                                            <select class="form-control" name="redirect_order_to" id="redirect_order_to">
                                                <option value="">Seleccione</option>
                                                <option value="detal">Venta DETAL</option>
                                                <option value="pos">Venta POS</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12" style="padding-bottom: 1.5%;">
                                            <div class="fprom-group">
                                                <button type="button" class="btn btn-primary" id="edit_sale"><?= lang('submit') ?></button>
                                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                        <tr class="warning">
                                            <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                            <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                            <?php if (($Owner || $Admin || $this->session->userdata('allow_discount')) || $inv->total_discount) { ?>
                                            <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                                            <?php } ?>
                                            <?php if ($Settings->tax2) { ?>
                                                <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                                            <?php } ?>
                                            <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                                            <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                        </tr>
                                    </table>
                                </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body row" id="pr_popover_content">
                <!-- <form class="form-horizontal" role="form"> -->
                <div class="form-group pedit_input_group">
                    <label for="pname" class="col-sm-4 control-label"><?= lang('product_name') ?></label>
                    <div class="col-sm-8">
                        <?= form_input('pname', '', 'id="pname" class="form-control"') ?>
                    </div>
                </div>
                </br>
                <?php if ($Settings->tax1) { ?>
                    <div class="form-group pedit_input_group">
                        <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                        <div class="col-sm-8">
                            <select name="ptax" id="ptax" class="form-control">
                                <?php foreach ($tax_rates as $tax) : ?>
                                    <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    </br>
                    <?php if ($this->Settings->ipoconsumo == 3 || $this->Settings->ipoconsumo == 1): ?>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="control-label"><?= lang('second_product_tax') ?></label>
                            </div>
                            <div class="col-sm-8 ptax2_div">
                                <input type="text" name="ptax2" id="ptax2" class="form-control" readonly>
                            </div>
                            <div class="col-sm-4 ptax2_percentage_div" style="display:none;">
                                <input type="text" name="ptax2_percentage" id="ptax2_percentage" class="form-control" readonly>
                            </div>
                        </div>
                        </br>
                    <?php endif ?>
                <?php } ?>
                <?php if ($Settings->product_serial) { ?>
                    <div class="form-group pedit_input_group">
                        <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pserial">
                        </div>
                    </div>
                    </br>
                <?php } ?>
                <div class="form-group pedit_input_group punit_div">
                    <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                    <div class="col-sm-8">
                        <div id="punits-div"></div>
                    </div>
                </br>
                </div>
                <?php if ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10 || $this->Settings->prioridad_precios_producto == 5): ?>
                    <div class="form-group pedit_input_group">
                        <label for="punit_quantity" class="col-sm-4 control-label"><?= lang('product_unit_quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="punit_quantity" readonly>
                        </div>
                    </div>
                </br>
                <?php endif ?>
                <div class="form-group pedit_input_group">
                    <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="pquantity">
                    </div>
                </div>
                </br>
                <div class="form-group pedit_input_group poptions_div">
                    <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                    <div class="col-sm-8">
                        <div id="poptions-div"></div>
                    </div>
                </br>
                </div>
                <div class="form-group pedit_input_group ppreferences_div">
                    <label for="ppreferences" class="col-sm-4 control-label"><?= lang('product_preferences') ?></label>
                    <div class="col-sm-8">
                        <div id="ppreferences-div"></div>
                    </div>
                </br>
                </div>
                <div class="col-sm-12"></div>
                <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                    <div class="form-group pedit_input_group">
                        <label for="pdiscount" class="col-sm-4 control-label"><?= lang('total_product_discount') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-pad" id="ptdiscount">
                        </div>
                    </div>
                    <div class="col-sm-12"></div>
                    <div class="form-group pedit_input_group">
                        <label for="pdiscount" class="col-sm-4 control-label"><?= lang('product_discount') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pdiscount">
                        </div>
                    </div>
                    <div class="col-sm-12"></div>
                <?php } ?>
                <div class="form-group pedit_input_group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10 || $this->Settings->prioridad_precios_producto == 5) ? "style='display:none;'" : "" ?>>
                    <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="pprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                    </div>
                </div>
                </br>
                <div class="form-group div_pprice_authorization_code" style="display:none;">
                    <label for="pprice_authorization_code" class="col-sm-4 control-label"><?= lang('authorization_code') ?></label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="password" class="form-control" id="pprice_authorization_code" placeholder="<?= lang('authorization_code_placeholder') ?>">
                            <span class="input-group-addon" id="check_pprice_autoriz" style="cursor:pointer;"><i class="fas fa-lock-open"></i></span>
                        </div>
                        <input type="hidden" name="under_cost_authorized" id="under_cost_authorized" value="0">
                    </div>
                </div>
                </br>
                <div class="form-group pedit_input_group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10 || $this->Settings->prioridad_precios_producto == 5) ? "" : "style='display:none;'" ?>>
                    <label for="pproduct_unit_price" class="col-sm-4 control-label"><?= lang('sale_form_edit_product_unit_price') ?></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="pproduct_unit_price" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                    </div>
                </div>
                </br>
                <?php if ($Settings->ipoconsumo) : ?>
                    <!-- <div class="form-group pedit_input_group">
                        <label for="pprice_ipoconsumo" class="col-sm-4 control-label"><?= lang('unit_price_ipoconsumo') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pprice_ipoconsumo"></br>
                            <em><?= lang('unit_price_ipoconsumo_detail') ?></em>
                        </div>
                    </div>
                    </br> -->
                <?php endif ?>
                <table class="table table-bordered">
                    <tr>
                        <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                        <th style="width:25%;"><span id="net_price"></span></th>
                        <th style="width:25%;"><?= lang('product_taxes'); ?></th>
                        <th style="width:25%;"><span id="pro_tax"></span></th>
                    </tr>
                </table>
                </br>
                <input type="hidden" id="punit_price" value="" />
                <input type="hidden" id="old_tax" value="" />
                <input type="hidden" id="old_qty" value="" />
                <input type="hidden" id="old_price" value="" />
                <input type="hidden" id="row_id" value="" />
                <div class="panel panel-default">
                    <div class="panel-heading"><?= lang('calculate_unit_price'); ?></div>
                    <div class="panel-body">

                        <div class="form-group pedit_input_group">
                            <label for="pcost" class="col-sm-4 control-label"><?= lang('subtotal_before_tax') ?></label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="psubtotal">
                                    <div class="input-group-addon" style="padding: 2px 8px;">
                                        <a href="#" id="calculate_unit_price" class="tip" title="<?= lang('calculate_unit_price'); ?>">
                                            <i class="fa fa-calculator"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group pedit_input_group">
                    <label for="paddprice" class="col-sm-4 control-label"><?= lang('add_price_amount') ?></label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control kb-pad" id="paddprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                    </div>
                </div>
                <!-- </form> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
                <em class="text-danger error_under_cost" style="display:none;">El precio digitado está por debajo del costo del producto</em>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>

                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="mserial" class="col-sm-4 control-label"><?= lang('product_serial') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mserial">
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="mdiscount" class="col-sm-4 control-label">
                                <?= lang('product_discount') ?>
                            </label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mdiscount" <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? '' : 'readonly="true"'; ?>>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?= lang('unit_price') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="mnet_price"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="sPUModal" tabindex="-1" role="dialog" aria-labelledby="sPUModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_spumodal"></h2>
                <h3><?= lang('unit_prices') ?></h3>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 10%;"></th>
                            <th style="width: 35%;"><?= lang('unit') ?></th>
                            <th style="width: 35%;"><?= lang('price') ?></th>
                            <th style="width: 20%;"><?= lang('quantity') ?></th>
                        </tr>
                    </thead>
                </table>
                <table class="table" id="unit_prices_table" style="width: 100%;">
                    <tbody>

                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <div class="col-sm-3">
                    <?= lang('unit_quantity', 'unit_quantity') ?>
                    <input type="text" name="unit_quantity" id="unit_quantity" class="form-control">
                </div>
                <div class="col-sm-9">
                    <button class="btn btn-success send_item_unit_select" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_serialModal"></h2>
                <h3><?= lang('ticket_data') ?></h3>
            </div>
            <div class="modal-body">
                <form id="serialModal_form">
                    <input type="hidden" id="serialModal_product_id">
                    <input type="hidden" id="serialModal_option_id">
                    <div class="col-sm-12 form-group">
                        <?= lang('serial_no', 'serial') ?>
                        <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= lang('meters', 'meters') ?>
                        <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('click', '#edit_sale', function(){
        localStorage.removeItem('oslref');
    });

    $('#oslbiller').on('change', function() {
        biller = $('#oslbiller');

        $.ajax({
            url : site.base_url+"sales/getSellers",
            type : "get",
            data : {"biller_id" : biller.val()}
        }).done(function(data){
            if (data != false) {
                $('#oslseller').html(data).trigger('change');
            } else {
                $('#oslseller').select2('val', '').trigger('change');
            }
        }).fail(function(data){
            // console.log(data);
        });

        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/8/") ?>'+$('#oslbiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data) {
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                $('#add_sale').attr('disabled', true).css('display', 'none');
            } else {
                $('.resAlert').css('display', 'none');
                $('#add_sale').attr('disabled', false).css('display', '');
            }
            if (DT = localStorage.getItem('document_type_id')) {
                $('#document_type_id').select2('val', DT);
            }
            $('#document_type_id').trigger('change');
        });

    });

    $('#oslcustomer').on('change', function(){
        customer = $(this);

        $.ajax({
            url: site.base_url+"sales/getCustomerAddresses",
            type: "get",
            data: {"customer_id" : customer.val()}
        }).done(function(data){
            if (data != false) {
                $('#oslcustomerbranch').html(data).trigger('change');
            } else {
                $('#oslcustomerbranch').select2('val', '').trigger('change');
            }
        });
    });

    $('#oslcustomerbranch').on('change', function(){
        customer_special_discount();
        setTimeout(function() {
            lseller = "<?= $inv->seller_id ?>";
            seller = $('#oslcustomerbranch option:selected').data('sellerdefault');
            if (lseller) {
                $('#oslseller').select2('val', lseller);
            } else if (seller) {
                $('#oslseller').select2('val', seller);
            }
        }, 2000);
    });

    function add_item_unit(item_id, warehouse_id){
            // $('#sPModal').modal('hide');

            var ooTable = $('#unit_prices_table').dataTable({
                aaSorting: [[1, "asc"]],
                aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                iDisplayLength: <?= $Settings->rows_per_page ?>,
                bProcessing: true, 'bServerSide': true,
                sAjaxSource: site.base_url+"pos/itemSelectUnit/"+item_id+"/"+warehouse_id+"/"+$('#oslcustomer').val(),
                "bDestroy": true,
                fnServerData: function (sSource, aoData, fnCallback)
                {
                aoData.push({
                  name: "<?= $this->security->get_csrf_token_name() ?>",
                  value: "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback/*, error: function(data) {console.log(data.responseText)}*/ });
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var oSettings = ooTable.fnSettings();
                    //$("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
                    // nRow.id = aData[0];
                    nRow.setAttribute('data-itemunitid', aData[4]);
                    nRow.setAttribute('data-productid', aData[5]);
                    nRow.setAttribute('data-productunitid', aData[6]);
                    nRow.className = "add_item_unit";
                    //if(aData[7] > aData[9]){ nRow.className = "product_link warning"; } else { nRow.className = "product_link"; }
                    return nRow;
                },
                aoColumns: [
                {bSortable: false, "mRender": radio_2},
                {bSortable: false},
                {bSortable: false},
                {bSortable: false, className : "text-right"},
                {bSortable: false,  bVisible: false},
                {bSortable: false,  bVisible: false},
                ],
                initComplete: function(settings, json)
                {
                console.log(json);
                $('#sPUModal').modal('show');
                }
                }).dtFilter([
                {column_number: 0, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", mdata: []},
                {column_number: 1, filter_default_label: "[<?=lang('price');?>]", filter_type: "text", data: []},
                {column_number: 2, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
                ], "footer");

                $('#unit_prices_table thead').remove();
        }

        $(document).on('click', '.add_item_unit', function() {

            var product_id = $(this).data('productid');
            var unit_price_id = $(this).data('itemunitid');

            var unit_data = {
                                'product_id' : product_id,
                                'unit_price_id' : unit_price_id,
                            };

            localStorage.setItem('unit_data', JSON.stringify(unit_data));

            $('#unit_quantity').val(1).select();

        });

        $(document).on('keyup', '#unit_quantity', function(e) {
        if (e.keyCode == 13) {
            if (unit_data = JSON.parse(localStorage.getItem('unit_data'))) {
                localStorage.removeItem('unit_data');
                var unit_quantity = $(this).val();
                var itemunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('itemunitid');
                var productid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productid');
                var productunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productunitid');
                var warehouse_id = $('#oslwarehouse').val();
                var unit_quantity = $('#unit_quantity').val();
                unit_data = {
                            'product_id' : productid,
                            'unit_price_id' : itemunitid,
                            'product_unit_id': productunitid,
                        };
                $.ajax({
                    url: site.base_url + "sales/iusuggestions",
                    type: "get",
                    data: {
                        'product_id' : unit_data.product_id,
                        'unit_price_id' : unit_data.unit_price_id,
                        'product_unit_id' : unit_data.product_unit_id,
                        'warehouse_id' : warehouse_id,
                        'unit_quantity' : unit_quantity,
                        'biller_id': $('#oslbiller').val(),
                        'address_id': $('#oslcustomerbranch').val(),
                        'customer_id': $('#oslcustomer').val()
                    }
                }).done(function(data) {
                    add_invoice_item(data);
                    $('#sPUModal').modal('hide');
                });
            } else {
                $('#unit_quantity').val(1).select();
            }
        }
    });

        $('#sPUModal').on('shown.bs.modal', function () {
            // $('#unit_prices_table_filter .input-xs').focus();
            $('#unit_prices_table_length').remove();
            $('#unit_prices_table_filter').remove();
            $('#unit_prices_table_info').remove();
            $('#unit_prices_table_paginate').remove();
            $('#unit_quantity').val(1);
            $('.select_auto_2:first').iCheck('check').focus();
        });

        $(document).on('hide.bs.modal', '#sPUModal', function (e) {
          e.stopPropagation() // stops modal from being shown
        });

        $(document).on('ifClicked', '.select_auto_2', function(e){
            var index = $('.select_auto_2').index($(this));
            $('.add_item_unit').eq(index).trigger('click');
        });

        $(document).on('keyup', '.select_auto_2', function(e){
            if (e.keyCode == 13) {
                $(this).closest('.add_item_unit').trigger('click');
            }
        });


        $(document).on('click', '.send_item_unit_select', function() {

        var itemunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('itemunitid');
        var productid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productid');
        var productunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productunitid');

        var unit_data = {
            'product_id': productid,
            'unit_price_id': itemunitid,
            'product_unit_id': productunitid,
        };
        var warehouse_id = $('#oslwarehouse').val();
        var unit_quantity = $('#unit_quantity').val();
        $.ajax({
            url: site.base_url + "sales/iusuggestions",
            type: "get",
            data: {
                'product_id': unit_data.product_id,
                'unit_price_id': unit_data.unit_price_id,
                'product_unit_id' : unit_data.product_unit_id,
                'warehouse_id': warehouse_id,
                'unit_quantity': unit_quantity,
                'biller_id': $('#oslbiller').val(),
                'address_id': $('#oslcustomerbranch').val(),
                'customer_id': $('#oslcustomer').val()
            }
        }).done(function(data) {
            add_invoice_item(data);
            $('#sPUModal').modal('hide');
        });

    });
</script>
