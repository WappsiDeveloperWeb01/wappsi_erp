<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<script>
  $(document).ready(function () {
    oTable = $('#tabla_vendedores').dataTable({
      aaSorting: [[1, "asc"]],
      aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
      iDisplayLength: <?= $Settings->rows_per_page ?>,
      bProcessing: true, 'bServerSide': true,
      sAjaxSource: '<?= admin_url('seller/get_sellers') ?>',
      fnServerData: function (sSource, aoData, fnCallback)
      {
        aoData.push({
          name: "<?= $this->security->get_csrf_token_name() ?>",
          value: "<?= $this->security->get_csrf_hash() ?>"
        });
        $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback/*, error: function(data) {console.log(data.responseText)}*/ });
      },
      aoColumns: [
        { bSortable: false, mRender: checkbox },
        null,
        null,
        null,
        null,
        null,
        null,
        { bSortable: false}
      ],
      initComplete: function(settings, json)
      {
        console.log(json);
      }
    }).dtFilter([
      {column_number: 1, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", mdata: []},
      {column_number: 2, filter_default_label: "[<?=lang('vat_no');?>]", filter_type: "text", data: []},
      {column_number: 3, filter_default_label: "[<?=lang('phone');?>]", filter_type: "text", data: []},
      {column_number: 4, filter_default_label: "[<?=lang('email_address');?>]", filter_type: "text", data: []},
      {column_number: 5, filter_default_label: "[<?=lang('city');?>]", filter_type: "text", data: []},
      {column_number: 6, filter_default_label: "[<?=lang('country');?>]", filter_type: "text", data: []},
    ], "footer");
  });
</script>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { echo admin_form_open('seller/seller_actions', 'id="action-form"'); } ?>
<div class="wrapper wrapper-content  animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-content">
          <div class="row">
            <div class="col-lg-12">
              <div class="pull-right dropdown">
                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right">
                  <?= lang('actions'); ?> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                <?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
                  <li><a href="<?= admin_url('seller/add'); ?>" data-toggle="modal" data-target="#myModal" id="add"><i class="fa fa-plus-circle"></i> <?= lang("add_seller"); ?></a></li>
                <?php } ?>
                  <li><a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel'); ?></a></li>
                  <li class="divider"></li>
                <?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
                  <!-- <li>
                    <a href="#" class="bpo" title="<b><?= $this->lang->line("delete_sellers") ?></b>" data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left"><i class="fa fa-trash-o"></i> <?= lang('delete_sellers') ?>
                    </a>
                  </li> -->
                <?php } ?>
                </ul>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
              <div class="table-responsive">
                <table id="tabla_vendedores" class="table table-hover">
                  <thead>
                    <tr class="primary">
                      <th style="min-width:30px; width: 30px; text-align: center;">
                        <input class="checkbox checkth" type="checkbox" name="check"/>
                      </th>
                      <th><?= lang("name"); ?></th>
                      <th><?= lang("vat_no"); ?></th>
                      <th><?= lang("phone"); ?></th>
                      <th><?= lang("email_address"); ?></th>
                      <th><?= lang("city"); ?></th>
                      <th><?= lang("country"); ?></th>
                      <th style="width:85px;"><?= lang("actions"); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                    </tr>
                  </tbody>
                  <tfoot class="dtFilter">
                    <tr class="active">
                      <th style="min-width:30px; width: 30px; text-align: center;">
                        <input class="checkbox checkft" type="checkbox" name="check"/>
                      </th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th style="width:85px;" class="text-center"><?= lang("actions"); ?></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /Body -->

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
  <div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
  </div>
  <?= form_close() ?>
<?php } ?>
<?php if (isset($action) && $action == 'add') {
    echo '<script>$(document).ready(function(){$("#add").trigger("click");});</script>';
}
?>