<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Products_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('purchases_model');
        $this->load->admin_model('settings_model');
    }

    public function getAllProducts()
    {
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubCategoryProducts($subcategory_id)
    {
        $q = $this->db->get_where('products', array('subcategory_id' => $subcategory_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptions($pid, $warehouse_id = NULL, $search = null, $pedit = false)
    {
        if ($warehouse_id) {
            if ($pedit) {
                $this->db->select('product_variants.*, '.$this->db->dbprefix('product_variants').'.name,
                    ('.$this->db->dbprefix('warehouses_products_variants').'.quantity'.($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') as quantity
                    '.($this->Settings->include_pending_order_quantity_stock == 1 ? ", COALESCE(OP.pending_quantity, 0) as op_pending_quantity" : ", 0 as op_pending_quantity").'');
            } else {
                $this->db->select('product_variants.*,
                    '.($warehouse_id ? ''.$this->db->dbprefix('warehouses_products_variants').'.quantity' : ''.$this->db->dbprefix('product_variants').'.quantity').',
                    CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "")) as name,
                    ('.$this->db->dbprefix('warehouses_products_variants').'.quantity'.($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') as quantity
                    '.($this->Settings->include_pending_order_quantity_stock == 1 ? ", COALESCE(OP.pending_quantity, 0) as op_pending_quantity" : ", 0 as op_pending_quantity").'');
            }
        } else {
            if ($pedit) {
                $this->db->select('product_variants.*, '.$this->db->dbprefix('product_variants').'.name');
            } else {
                $this->db->select('product_variants.*,
                    '.($warehouse_id ? 'warehouses_products_variants.quantity' : 'product_variants.quantity').',
                    CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "")) as name');
            }
        }
            
        if ($search) {
            $this->db->like($this->db->dbprefix('product_variants').'.name', $search);
        }
        if ($warehouse_id) {
            $this->db->join('warehouses_products_variants', 'warehouses_products_variants.option_id = product_variants.id AND warehouses_products_variants.warehouse_id = '.$warehouse_id, 'left');
        }

        if ($warehouse_id && $this->Settings->include_pending_order_quantity_stock == 1) {
            $pending_order_sql = "(SELECT 
                                    OSI.product_id,
                                    OSI.option_id,
                                    SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
                                FROM sma_order_sale_items OSI
                                    INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
                                WHERE OS.sale_status != 'cancelled'
                                ".($warehouse_id ? ' AND OSI.warehouse_id = ' . $warehouse_id : '')."
                                GROUP BY OSI.product_id, OSI.option_id) OP";
            $this->db->join($pending_order_sql, 'OP.product_id=product_variants.product_id AND OP.option_id = product_variants.id', 'left');
        }
        // if ($this->Settings->overselling == 0 && $pedit == false) {
        //     if ($warehouse_id) {
        //         $this->db->where('warehouses_products_variants.quantity >', 0);
        //     } else {
        //         $this->db->where('product_variants.quantity >', 0);
        //     }
        // }
        $this->db->where('product_variants.product_id', $pid);
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptionsForPrinting($pid, $warehouse_id = null)
    {
        $this->db->select('
                            product_variants.*,
                            CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "")) as name
                            '.($warehouse_id ? ", ".$this->db->dbprefix('warehouses_products_variants').'.quantity AS wh_quantity' : '').'
                        ');
        if ($warehouse_id) {
            $this->db->join('warehouses_products_variants', 'warehouses_products_variants.option_id = product_variants.id AND warehouses_products_variants.warehouse_id = '.$warehouse_id);
        }
        $this->db->where('product_variants.product_id', $pid);
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouseProductOptions($pid, $wh_id = null)
    {

        $this->db->select('product_variants.*, CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "")) as name')
                ->join('warehouses_products_variants', 'warehouses_products_variants.option_id = product_variants.id AND warehouses_products_variants.warehouse_id = '.$wh_id);
        $this->db->where('product_variants.product_id', $pid);
        $this->db->where('warehouses_products_variants.quantity >', 0);
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductPreferences($pid, $search = null)
    {
        $this->db->select('product_preferences.*, preferences_categories.name as prf_cat_name, preferences_categories.selection_limit, preferences_categories.required')
                ->join('preferences_categories', 'preferences_categories.id = product_preferences.preference_category_id', 'join')
                ->where('product_id', $pid);
        if ($search) {
            $this->db->like($this->db->dbprefix('product_preferences').'.name', $search);
        }
        $q = $this->db->order_by('preferences_categories.name ASC')->get('product_preferences');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptionsWithWH($pid)
    {
        $this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
            ->where('warehouses.status', '1')
            ->group_by(array('' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'))
            ->order_by('product_variants.id');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $pid, 'warehouses_products_variants.quantity !=' => NULL));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductComboItems($pid)
    {
        $this->db->select($this->db->dbprefix('products') . '.id as id,
                          ' . $this->db->dbprefix('products') . '.code as code,
                          ' . $this->db->dbprefix('combo_items') . '.quantity as qty,
                          ' . $this->db->dbprefix('products') . '.name as name,
                          ' . $this->db->dbprefix('products') . '.cost as price,
                          ' . $this->db->dbprefix('products') . '.type as type,
                          ' . $this->db->dbprefix('combo_items') . '.unit as unit,
                          '.$this->db->dbprefix('units').'.code as unit_code,
                          '.$this->db->dbprefix('units').'.operation_value as operation_value,
                          parent_unit.id as parent_unit_id
                          ')
                    ->join('products', 'products.code=combo_items.item_code', 'left')
                    ->join('units', 'combo_items.unit=units.id', 'left')
                    ->join('units parent_unit', 'units.base_unit=parent_unit.id', 'left')
                    ->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $units_data = $this->site->getUnitsByBUID($row->parent_unit_id ? $row->parent_unit_id : $row->unit);
                $units = [];
                foreach ($units_data as $unitdata) {
                    $units[$unitdata->id] = $unitdata;
                }
                $row->units = $units;
                $row->parent_id = $pid;
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductWithCategory($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('categories') . '.name as category')
        ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function has_purchase($product_id, $warehouse_id = NULL)
    {
        if($warehouse_id) { $this->db->where('warehouse_id', $warehouse_id); }
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductDetails($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.code, ' . $this->db->dbprefix('products') . '.name, ' . $this->db->dbprefix('categories') . '.code as category_code, cost, price, quantity, alert_quantity')
            ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductDetail($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*,
            ' . $this->db->dbprefix('tax_rates') . '.name as tax_rate_name,
            '.$this->db->dbprefix('tax_rates') . '.code as tax_rate_code,
            '.$this->db->dbprefix("tax_rates").'.rate,
            '.$this->db->dbprefix("units").'.code AS unitCode,
            c.name as category_code,
            sc.name as subcategory_code', FALSE)
            ->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
            ->join('categories c', 'c.id=products.category_id', 'left')
            ->join('categories sc', 'sc.id=products.subcategory_id', 'left')
            ->join('units', 'products.sale_unit=units.id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSubCategories($parent_id) {
        $this->db->select('id as id, CONCAT(UCASE(LEFT(name, 1)), LCASE(SUBSTRING(name, 2))) as text')
        ->where('parent_id', $parent_id)->where('(subcategory_id IS NULL or subcategory_id = 0)')->order_by('name');
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubCategoriesSecondLevel($parent_id) {
        $this->db->select('id as id, CONCAT(UCASE(LEFT(name, 1)), LCASE(SUBSTRING(name, 2))) as text')
        ->where('subcategory_id', $parent_id)->order_by('name');
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getMultipleSubCategories($parent_id) {
        $sql_or = "";
        foreach ($parent_id as $key => $pid) {
            if ($sql_or != "") {
                $sql_or.=" OR ";
            }
            $sql_or .= "parent_id = ".$pid;
        }
        $this->db->select('id as id, name as text')
        ->where($sql_or)->order_by('name');
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductByCategoryID($id)
    {

        $q = $this->db->get_where('products', array('category_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return true;
        }
        return FALSE;
    }

    public function getAllWarehousesWithPQ($product_id)
    {
        $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.min_quantity,' . $this->db->dbprefix('warehouses_products') . '.max_quantity, ' . $this->db->dbprefix('warehouses_products') . '.warehouse_location')
            ->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
            ->where('warehouses_products.product_id', $product_id)
            ->where('warehouses.status', '1')
            ->group_by('warehouses.id');
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductPhotos($id)
    {
        $q = $this->db->get_where("product_photos", array('product_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addProduct($arr_data)
    {
        $data = $arr_data['data'];
        $items = $arr_data['items'];
        $warehouse_qty = $arr_data['warehouse_qty'];
        $product_attributes = $arr_data['product_attributes'];
        $photos = $arr_data['photos'];
        $product_preferences = $arr_data['product_preferences'];
        $wh_locations = $arr_data['wh_locations'];
        $price_groups_data = $arr_data['price_groups_data'];
        $multiple_units = $arr_data['multiple_units'];
        $hybrid_prices = $arr_data['hybrid_prices'];
        $munits_prg = $arr_data['munits_prg'];
        $attr_suffix = $arr_data['attr_suffix'];
        $billers = $arr_data['billers'];
        $data['last_update'] = date('Y-m-d H:i:s');
        $DB2 = false;
        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
            $DB2 = $this->settings_model->connect_new_year_database();
            if (!$this->settings_model->check_same_id_insert($DB2, 'products')) {
                $this->session->set_flashdata('error', 'El producto no se pudo crear, inconsistencia de datos entre años');
            }
        }
        $data['created_by'] = $this->session->userdata('user_id');

        $update_pcode_consecutive = false;
        if (isset($data['product_code_consecutive']) && $data['product_code_consecutive'] == 1) {
            $product_code_consecutive = $data['product_code_consecutive'];
            $ccode = $this->site->getCodeConsecutive('product');
            $data['code'] = $ccode;
            $update_pcode_consecutive = true;
        } else if ($data['product_code_consecutive'] == NULL) {
            unset($data['product_code_consecutive']);
        }

        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();
            if ($DB2) {
                $data['id'] = $product_id;
                $DB2->insert('products', $data);
            }

            if ($update_pcode_consecutive) {
                $this->site->updateCodeConsecutive('product', ($data['code']+1));
            }
            $items_base[] = array(
                            'product_id' => $product_id,
                            'price' => $data['price'],
                        );
            $pg_unit = [];
            if ($price_groups_data) {
                foreach ($price_groups_data as $pg) {
                    if (($this->Settings->allow_zero_price_products == 1 && $pg['price'] >= 0) || ($this->Settings->allow_zero_price_products == 0 && $pg['price'] > 0)) {
                        $consumption_sale_tax = $data['consumption_sale_tax'];
                        if ($this->Settings->prioridad_precios_producto == 10) {
                            if (isset($munits_prg[$pg['id']])) {
                                $unit = $this->getUnitById($munits_prg[$pg['id']]);
                                if ($unit->operator && $this->Settings->precios_por_unidad_presentacion == 2) {
                                    if ($unit->operator == "*") {
                                        $consumption_sale_tax = $consumption_sale_tax * $unit->operation_value;
                                    } else if ($unit->operator == "/") {
                                        $consumption_sale_tax = $consumption_sale_tax / $unit->operation_value;
                                    }
                                }
                            }
                            $pg_unit[$pg['id']] = $pg['price'] - $consumption_sale_tax;
                        }
                        $this->db->insert('product_prices', ['product_id' => $product_id, 'price_group_id' => $pg['id'], 'price' => ($pg['price'] - $consumption_sale_tax)]);
                        if ($DB2) {
                            $DB2->insert('product_prices', ['product_id' => $product_id, 'price_group_id' => $pg['id'], 'price' => ($pg['price'] - $consumption_sale_tax)]);
                        }
                    }
                }
            }
            if ($hybrid_prices) {
                foreach ($hybrid_prices as $unit_id => $prices) {
                    foreach ($prices as $price_group_id => $price) {
                        if ($price > 0) {
                            $this->site->update_product_hybrid_price($product_id, $price_group_id, $unit_id, $price);
                        }
                    }
                }
            } else {
                if ($multiple_units) {
                    foreach ($multiple_units as $munit) {
                        $unit = $this->getUnitById($munit['unit_id']);
                        if ($this->Settings->prioridad_precios_producto == 10) {
                            if (isset($pg_unit[$unit->price_group_id])) {
                                $munit['price'] = $pg_unit[$unit->price_group_id];
                            }
                        } else {
                            $consumption_sale_tax = $data['consumption_sale_tax'];
                            if ($unit->operator && $this->Settings->precios_por_unidad_presentacion == 2) {
                                if ($unit->operator == "*") {
                                    $consumption_sale_tax = $consumption_sale_tax * $unit->operation_value;
                                } else if ($unit->operator == "/") {
                                    $consumption_sale_tax = $consumption_sale_tax / $unit->operation_value;
                                }
                            }
                            $munit['price'] = $munit['price'] - $consumption_sale_tax;
                        }
                        $this->db->insert('unit_prices', ['id_product' => $product_id, 'unit_id' => $munit['unit_id'], 'valor_unitario' => ($munit['price']), 'cantidad' => $munit['cantidad']]);
                        if ($DB2) {
                            $DB2->insert('unit_prices', ['id_product' => $product_id, 'unit_id' => $munit['unit_id'], 'valor_unitario' => ($munit['price']), 'cantidad' => $munit['cantidad']]);
                        }
                    }
                } else {
                    if ($this->Settings->prioridad_precios_producto == 10 && count($pg_unit) > 0) {
                        foreach ($pg_unit as $pg_id => $price) {
                            $units = $this->site->get_units_by_price_group($pg_id, $product_id);
                            if ($units) {
                                foreach ($units as $unit) {

                                    $this->db->insert('unit_prices', ['id_product' => $product_id, 'unit_id' => $unit->id, 'valor_unitario' => ($price), 'cantidad' => $unit->operation_value]);

                                    if ($DB2) {
                                        $DB2->insert('unit_prices', ['id_product' => $product_id, 'unit_id' => $unit->id, 'valor_unitario' => ($price), 'cantidad' => $unit->operation_value]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->updateProductsPriceGroupBase($items_base);
            $this->addProductCostingByMonth($product_id);
            if ($items) {
                foreach ($items as $item) {
                    $item['product_id'] = $product_id;
                    $this->db->insert('combo_items', $item);
                    if ($DB2) {
                        $DB2->insert('combo_items', $item);
                    }
                }
            }
            $warehouses = $this->site->getAllWarehouses();
            foreach ($warehouses as $warehouse) {
                $wh_location = NULL;
                if (isset($wh_locations[$warehouse->id])) {
                    $wh_location = $wh_locations[$warehouse->id];
                }
                $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0, 'warehouse_location' => $wh_location, 'last_update' => date('Y-m-d H:i:s')));
                if ($DB2) {
                    $DB2->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0, 'warehouse_location' => $wh_location, 'last_update' => date('Y-m-d H:i:s')));
                }
            }
            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);
            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    if (isset($wh_qty['quantity']) && ! empty($wh_qty['quantity'])) {
                        $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack'], 'avg_cost' => $data['cost'], 'last_update' => date('Y-m-d H:i:s')));
                        if ($DB2) {
                            $DB2->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack'], 'avg_cost' => $data['cost'], 'last_update' => date('Y-m-d H:i:s')));
                        }
                        if (!$product_attributes) {
                            $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                            $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                            $unit_cost = $data['cost'];
                            if ($tax_rate) {
                                if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                    if ($data['tax_method'] == '0') {
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                        $net_item_cost = $data['cost'] - $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    } else {
                                        $net_item_cost = $data['cost'];
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                        $unit_cost = $data['cost'] + $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    }
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $item_tax = $tax_rate->rate;
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = 0;
                            }
                            $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);
                            $item = array(
                                'product_id' => $product_id,
                                'product_code' => $data['code'],
                                'product_name' => $data['name'],
                                'net_unit_cost' => $net_item_cost,
                                'unit_cost' => $unit_cost,
                                'real_unit_cost' => $unit_cost,
                                'quantity' => $wh_qty['quantity'],
                                'quantity_balance' => $wh_qty['quantity'],
                                'quantity_received' => $wh_qty['quantity'],
                                'item_tax' => $item_tax,
                                'tax_rate_id' => $tax_rate_id,
                                'tax' => $tax,
                                'subtotal' => $subtotal,
                                'warehouse_id' => $wh_qty['warehouse_id'],
                                'date' => date('Y-m-d'),
                                'status' => 'received',
                            );
                            $this->db->insert('purchase_items', $item);
                            if ($DB2) {
                                $DB2->insert('purchase_items', $item);
                            }
                            $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                        }
                    }
                }
            }
            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {
                    //variant code consecutive
                    $update_pr_attr_consecutive = false;
                    if (isset($pr_attr['attr_code_consecutive']) && $pr_attr['attr_code_consecutive'] == 1) {
                        $pr_attr['pv_code_consecutive'] = $attr_code_consecutive = $pr_attr['attr_code_consecutive'];
                        unset($pr_attr['attr_code_consecutive']);
                        $pr_attr['code'] = $this->site->getCodeConsecutive('variant');
                        $update_pr_attr_consecutive = true;
                    }
                    unset($pr_attr['attr_code_consecutive']);
                    //variant code consecutive
                    $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);
                    $pr_attr['product_id'] = $product_id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    if (isset($pr_attr['attr_index']) && isset($attr_suffix[$pr_attr['attr_index']])) {
                        $pr_attr['suffix'] = $attr_suffix[$pr_attr['attr_index']];
                        unset($pr_attr['attr_index']);
                    }
                    if ($pr_attr_details) {
                        $option_id = $pr_attr_details->id;
                    } else {
                        $pr_attr['last_update'] = date('Y-m-d H:i:s');
                        $new_id = NULL;
                        if ($DB2) {
                            if (!$this->settings_model->check_same_id_insert($DB2, 'product_variants')) {
                                $new_id = $this->settings_model->get_new_year_new_id_for_insertion($DB2, 'product_variants');
                            }
                        }
                        $pr_attr['id'] = $new_id;
                        $this->db->insert('product_variants', $pr_attr);
                        $option_id = $this->db->insert_id();
                        if ($DB2) {
                            $DB2->insert('product_variants', $pr_attr);
                        }
                    }

                    //variant code consecutive
                    if ($update_pr_attr_consecutive) {
                        $this->site->updateCodeConsecutive('variant', ($pr_attr['code']+1));
                    }
                    //variant code consecutive
                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity'], 'last_update' => date('Y-m-d H:i:s')));
                        if ($DB2) {
                            $DB2->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity'], 'last_update' => date('Y-m-d H:i:s')));
                        }

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }
                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'quantity_received' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                        $this->db->insert('purchase_items', $item);
                        if ($DB2) {
                            $DB2->insert('purchase_items', $item);
                        }
                    }

                    foreach ($warehouses as $warehouse) {
                        if (!$this->getWarehouseProductVariant($warehouse->id, $product_id, $option_id)) {
                            $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0, 'last_update' => date('Y-m-d H:i:s')));
                            if ($DB2) {
                                $DB2->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0, 'last_update' => date('Y-m-d H:i:s')));
                            }
                        }
                    }

                    $this->site->syncVariantQty($option_id, $variant_warehouse_id);
                }
            }
            if ($product_preferences) {
                foreach ($product_preferences as $prf => $prf_id) {
                    $preference = $this->get_preference_by_id($prf_id);
                    $this->db->insert('product_preferences',
                                        array(
                                            'product_id' => $product_id,
                                            'name' => $preference->name,
                                            'preference_id' => $prf_id,
                                            'preference_category_id' => $preference->category_id,
                                            )
                                    );
                    if ($DB2) {
                        $DB2->insert('product_preferences',
                                        array(
                                            'product_id' => $product_id,
                                            'name' => $preference->name,
                                            'preference_id' => $prf_id,
                                            'preference_category_id' => $preference->category_id,
                                            )
                                    );
                    }
                }
            }
            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo, 'last_update' => date('Y-m-d H:i:s')));
                    if ($DB2) {
                        $DB2->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo, 'last_update' => date('Y-m-d H:i:s')));
                    }
                }
            }

            $this->Settings->last_products_exclusivity_update = $time = date('Y:m:d H:i:s');
            $this->db->update('settings', ['last_products_exclusivity_update' => $time], ['setting_id'=> 1]);
            $this->site->syncQuantity(NULL, NULL, NULL, $product_id);
            if ($billers && count($billers) > 0) {
                foreach ($billers as $bkey => $biller_id) { //se registra relación a sucursal seleccionada
                    $this->db->insert('products_billers', ['product_id' => $product_id, 'biller_id' => $biller_id]);
                    $this->site->create_temporary_product_billers_assoc($biller_id);
                }
            } else { //No cambiaron las sucursales
                $add_billers = $this->site->getAllCompaniesWithState('biller');
                foreach ($add_billers as $add_biller) {
                    $this->site->create_temporary_product_billers_assoc($add_biller->id);
                }
            }
            return $product_id;
        }
        return false;
    }

    public function getPrductVariantByPIDandName($product_id, $name)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id, 'name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addAjaxProduct($data)
    {
        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();
            return $this->getProductByID($product_id);
        }
        return false;
    }

    public function add_products($products = array(), $pimportvariants = [])
    {
        $products_added = 0;
        if (!empty($products)) {
            foreach ($products as $product) {
                $billers_exclusivity = $product['billers_exclusivity'];
                unset($product['billers_exclusivity']);
                $this->db->insert('products', $product);
                $product_id = $this->db->insert_id();
                if ($billers_exclusivity) {
                    foreach ($billers_exclusivity as $biller_id => $biller_data) {
                        $this->db->insert('products_billers', ['product_id' => $product_id, 'biller_id' => $biller_id]);
                    }
                }
                if ($product['product_code_consecutive'] == 1) {
                    $this->db->update('order_ref', ['product_code'=>($product['code'] + 1)], ['ref_id'=>1]);
                }
                $pgs = $this->db->get('price_groups');
                if ($pgs->num_rows() > 0) {
                    foreach (($pgs->result()) as $pg_row) {
                        $this->db->insert('product_prices', [
                                    'product_id' => $product_id,
                                    'price_group_id' => $pg_row->id,
                                    'price' => $product['price'],
                                    'last_update' => date('Y-m-d H:i:s'),
                                ]);
                    }
                }
                $products_added++;
            }
            $add_billers = $this->site->getAllCompaniesWithState('biller');
            $msg = "";
            foreach ($add_billers as $add_biller) {
                $this->Settings->last_products_exclusivity_update = $time = date('Y:m:d H:i:s');
                $this->db->update('settings', ['last_products_exclusivity_update' => $time], ['setting_id'=> 1]);
                $this->site->create_temporary_product_billers_assoc($add_biller->id);
            }
        }
        $txt_errors = "";
        if (!empty($pimportvariants)) {
            foreach ($pimportvariants as $piv) {
                if ($product_data = $this->getProductByCode($piv['Codigo_Producto'])) {
                    if ($product_data->attributes == '0') {
                        if ($this->products_model->productHasMovements($product_data->id, true)) {
                            $product_error_msg = lang('code')." ".lang('variant')." ".$piv['Codigo_Variante']." ".lang('with_following_errors')." : ".lang('product_has_movements_without_variants');
                            $txt_errors .= $product_error_msg."\n";
                            continue;
                        }
                    }
                    if ($piv['Codigo_Variante'] && $this->site->get_product_variant_by_code($piv['Codigo_Variante'])) {
                        $product_error_msg = lang('code')." ".lang('variant')." ".$piv['Codigo_Variante']." ".lang('with_following_errors')." : ".lang('already_assigned_to_product');
                        $txt_errors .= $product_error_msg."\n";
                        continue;
                    } else {
                        if (!$this->get_variant_by_name($piv['Nombre_Variante'])) {
                            $this->db->insert('variants', ['name'=>$piv['Nombre_Variante']]);
                        }
                        $this->db->insert('product_variants', [
                                                    'product_id' => $product_data->id,
                                                    'name' => $piv['Nombre_Variante'],
                                                    'price' => $piv['Sobreprecio_Variante'] > 0 ? $piv['Sobreprecio_Variante'] : 0,
                                                    'last_update' => date('Y-m-d H:i:s'),
                                                    'code' => $piv['Codigo_Variante'],
                                                    'pv_code_consecutive' => $piv['Codigo_Consecutivo'],
                                                    'status' => 1,
                                                ]);

                        if ($piv['Codigo_Consecutivo'] == 1) {
                            $this->db->update('order_ref', ['variant_code'=>($piv['Codigo_Variante'] + 1)], ['ref_id'=>1]);
                        }
                        $this->db->update('products', ['attributes'=>1], ['id'=>$product_data->id]);
                    }
                } else {
                    $product_error_msg = lang('code')." ".lang('variant')." ".$piv['Codigo_Variante']." ".lang('with_following_errors')." : ".lang('product_code_not_exists');
                    $txt_errors .= $product_error_msg."\n";
                    continue;
                }
            }

        }

        return ['error'=>($txt_errors && $txt_errors != "" ? true : false), 'txt_errors'=>($txt_errors && $txt_errors != "" ? $txt_errors : false), 'products_added' => $products_added];
    }

    public function getProductNames($term)
    {
        $limit = $this->Settings->max_num_results_display;
        $this->db->select($this->db->dbprefix('products') . '.id,
                            ' . $this->db->dbprefix('products') . '.code,
                            ' . $this->db->dbprefix('products') . '.name as name,
                            ' . $this->db->dbprefix('products') . '.price as price,
                            ' . $this->db->dbprefix('products') . '.cost as cost,
                            ' . $this->db->dbprefix('products') . '.unit as unit,
                            ' . $this->db->dbprefix('products') . '.tax_rate as tax_rate,
                            ' . $this->db->dbprefix('product_variants') . '.name as vname')
            ->where("type != 'combo' AND type != 'digital' AND type != 'service' AND (" . $this->db->dbprefix('products') . ".name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR " . $this->db->dbprefix('products') . ".code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', " . $this->db->dbprefix('products') . ".code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")");
        $this->db->join('product_variants', 'product_variants.product_id=products.id', 'left')
            ->where('' . $this->db->dbprefix('product_variants') . '.name', NULL)
            ->group_by('products.id')->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getQASuggestions($term, $biller_id, $warehouse_id)
    {
        if ($this->Settings->variant_code_search == 0) {
            return $this->get_qa_names($term, $biller_id, $warehouse_id);
        } else {
            if ($data = $this->get_qa_variant_names($term, $biller_id, $warehouse_id)) {
                return $data;
            } else {
                return $this->get_qa_names($term, $biller_id, $warehouse_id);
            }
        }
    }

    public function get_qa_names($term, $biller_id, $warehouse_id){
        $limit = $this->Settings->max_num_results_display;
        $this->site->create_temporary_product_billers_assoc($biller_id);
        $this->db->select('' . $this->db->dbprefix('products') . '.id,
                            code,
                            attributes,
                            ' . $this->db->dbprefix('products') . '.name as name,
                            ' . $this->db->dbprefix('products') . '.cost as cost,
                            ' . $this->db->dbprefix('products') . '.avg_cost as avg_cost,
                            tax_rate,
                            ('.($warehouse_id ? "FWP.quantity" : "{$this->db->dbprefix('products')}.quantity").($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') AS quantity,
                            '.($this->Settings->include_pending_order_quantity_stock == 1 ? "COALESCE(OP.pending_quantity, 0) AS op_pending_quantity, " : "").'
                         ')
                ->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
                ->where("type != 'combo' AND "
                . "(" . $this->db->dbprefix('products') . ".name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")")
            ->limit($limit);
        if ($warehouse_id) {
            $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";
            $this->db->join($wp, 'FWP.product_id=products.id  AND FWP.warehouse_id = ' . $warehouse_id, 'left');
        }
        if ($this->Settings->include_pending_order_quantity_stock == 1) {
            $pending_order_sql = "(SELECT 
                                    OSI.product_id,
                                    SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
                                FROM sma_order_sale_items OSI
                                    INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
                                WHERE OS.sale_status != 'cancelled'
                                ".($warehouse_id ? ' AND OSI.warehouse_id = ' . $warehouse_id : '')."
                                GROUP BY OSI.product_id) OP";
            $this->db->join($pending_order_sql, 'OP.product_id=products.id', 'left');
        }
        $this->db->where('products.discontinued', '0');
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return false;
    }

    public function get_qa_variant_names($term, $biller_id, $warehouse_id){
        $limit = $this->Settings->max_num_results_display;
        $this->site->create_temporary_product_billers_assoc($biller_id);
        $this->db->select('' . $this->db->dbprefix('products') . '.id,
                            ' . $this->db->dbprefix('products') . '.code,
                            attributes,
                            ' . $this->db->dbprefix('products') . '.name as name,
                            ' . $this->db->dbprefix('products') . '.cost as cost,
                            ' . $this->db->dbprefix('products') . '.avg_cost as avg_cost,
                            tax_rate,
                            product_variants.id as variant_selected,
                            ('.($warehouse_id ? "FWP.quantity" : "{$this->db->dbprefix('products')}.quantity").($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') AS quantity,
                            '.($this->Settings->include_pending_order_quantity_stock == 1 ? "COALESCE(OP.pending_quantity, 0) AS op_pending_quantity, " : "").'
                         ')
                ->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
                ->join('product_variants', 'product_variants.product_id=products.id', 'left')
                ->where("type != 'combo' AND "
                . "(
                    {$this->db->dbprefix('product_variants')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                )")
            ->limit($limit);
        if ($warehouse_id) {
            $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";
            $this->db->join($wp, 'FWP.product_id=products.id  AND FWP.warehouse_id = ' . $warehouse_id, 'left');
        }
        if ($this->Settings->include_pending_order_quantity_stock == 1) {
            $pending_order_sql = "(SELECT 
                                    OSI.product_id,
                                    SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
                                FROM sma_order_sale_items OSI
                                    INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
                                WHERE OS.sale_status != 'cancelled'
                                ".($warehouse_id ? ' AND OSI.warehouse_id = ' . $warehouse_id : '')."
                                GROUP BY OSI.product_id) OP";
            $this->db->join($pending_order_sql, 'OP.product_id=products.id', 'left');
        }
        $this->db->where('products.discontinued', '0');
        $this->db->where('product_variants.status', '1');
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return false;
    }

    public function getProductDataByWarehouse($data)
    {
        $q = $this->db->get_where('warehouses_products', $data);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getProductDataByWarehouseSUM($data)
    {
        $this->db->select("SUM(quantity) AS quantity");
        $q = $this->db->get_where('warehouses_products', $data);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getProductsForPrinting($term)
    {
        $limit = $this->Settings->max_num_results_display;

        $this->db->select('' . $this->db->dbprefix('products') . '.id, ' . $this->db->dbprefix('products') . '.code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price, product_variants.id as variant_selected')
            ->join('product_variants', 'product_variants.product_id=products.id', 'left')
            ->where("(
                        {$this->db->dbprefix('product_variants')}.code LIKE '%" . $term . "%'
                    )  AND {$this->db->dbprefix('products')}.discontinued = 0 ")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price')
            ->where("(
                        " . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR
                        code LIKE '%" . $term . "%' OR
                        concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%'
                    )  AND {$this->db->dbprefix('products')}.discontinued = 0 ")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function updateProduct($arr_data)
    {
        $id = isset($arr_data['id']) ? $arr_data['id'] : NULL;
        $wh_qty = isset($arr_data['wh_qty']) ? $arr_data['wh_qty'] : NULL;
        $data = isset($arr_data['data']) ? $arr_data['data'] : NULL;
        $items = isset($arr_data['items']) ? $arr_data['items'] : NULL;
        $warehouse_qty = isset($arr_data['warehouse_qty']) ? $arr_data['warehouse_qty'] : NULL;
        $product_attributes = isset($arr_data['product_attributes']) ? $arr_data['product_attributes'] : NULL;
        $photos = isset($arr_data['photos']) ? $arr_data['photos'] : NULL;
        $old_attr_suffix = isset($arr_data['old_attr_suffix']) ? $arr_data['old_attr_suffix'] : NULL;
        $product_preferences = isset($arr_data['product_preferences']) ? $arr_data['product_preferences'] : NULL;
        $wh_locations = isset($arr_data['wh_locations']) ? $arr_data['wh_locations'] : NULL;
        $price_groups_data = isset($arr_data['price_groups_data']) ? $arr_data['price_groups_data'] : NULL;
        $multiple_units = isset($arr_data['multiple_units']) ? $arr_data['multiple_units'] : NULL;
        $hybrid_prices = isset($arr_data['hybrid_prices']) ? $arr_data['hybrid_prices'] : NULL;
        $billers = isset($arr_data['billers']) ? $arr_data['billers'] : NULL;
        $deleted_images = isset($arr_data['deleted_images']) ? $arr_data['deleted_images'] : NULL;
        $deleted_main_image = isset($arr_data['deleted_main_image']) ? $arr_data['deleted_main_image'] : NULL;
        $pv_old_data = isset($arr_data['pv_old_data']) ? $arr_data['pv_old_data'] : NULL;
        $data['last_update'] = date('Y-m-d H:i:s');
        $DB2 = false;
        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
            $DB2 = $this->settings_model->connect_new_year_database();
        }
        $update_pcode_consecutive = false;
        if (isset($data['product_code_consecutive']) && $data['product_code_consecutive'] == 1) {
            $product_code_consecutive = $data['product_code_consecutive'];
            $ccode = $this->site->getCodeConsecutive('product');
            $data['code'] = $ccode;
            $update_pcode_consecutive = true;
        } else if ($data['product_code_consecutive'] == NULL) {
            unset($data['product_code_consecutive']);
        }
        if ($this->db->update('products', $data, array('id' => $id))) {
            if ($DB2) {
                $DB2->update('products', $data, array('id' => $id));
            }
            if ($update_pcode_consecutive) {
                $this->site->updateCodeConsecutive('product', ($data['code']+1));
            }
            if ($items) {
                $new_ci_items = $items;
                $old_ci_items = $this->db->get_where('combo_items', ['product_id'=>$id]);
                $old_ci_items_changed = false;
                $old_ci_items_txt = $this->session->first_name." ".$this->session->last_name.' cambió los productos del combo : ';
                if ($old_ci_items->num_rows() > 0) {
                    $old_ci_items = $old_ci_items->result();
                    foreach ($new_ci_items as $new_ci_item) {
                        $cnt = 0;
                        foreach ($old_ci_items as $old_ci_item_key => $old_ciitem) {
                            if ($new_ci_item['item_code'] == $old_ciitem->item_code) {
                                if ($new_ci_item['quantity'] != $old_ciitem->quantity || $new_ci_item['unit'] != $old_ciitem->unit) {
                                    $old_ci_items_txt.=" ".$new_ci_item['item_code'];
                                    if ($new_ci_item['quantity'] != $old_ciitem->quantity) {
                                        $old_ci_items_txt.=" cantidad cambió de ".$old_ciitem->quantity." a ".$new_ci_item['quantity'];
                                        $old_ci_items_changed = true;
                                    }
                                    if ($new_ci_item['unit'] != $old_ciitem->unit) {
                                        $old_ci_items_txt.=" unidad cambió de ".$old_ciitem->unit." a ".$new_ci_item['unit'];
                                        $old_ci_items_changed = true;
                                    }
                                    $old_ci_items_txt.=", ";
                                }
                                unset($old_ci_items[$old_ci_item_key]);
                                $cnt++;
                            }
                        }
                        if ($cnt == 0) {
                            $old_ci_items_txt.="agregó producto ".$new_ci_item['item_code']." cant: ".$new_ci_item['quantity'].", ";
                            $old_ci_items_changed = true;
                        }
                    }
                }
                // exit(var_dump($old_ci_items_txt));
                if ($old_ci_items_changed) {
                    $this->db->insert('user_activities', [
                        'date' => date('Y-m-d H:i:s'),
                        'type_id' => 1,
                        'table_name' => 'products',
                        'record_id' => $id,
                        'user_id' => $this->session->userdata('user_id'),
                        'module_name' => $this->m,
                        'description' => $old_ci_items_txt,
                    ]);
                }
                $this->db->delete('combo_items', array('product_id' => $id));
                foreach ($items as $item) {
                    $item['product_id'] = $id;
                    $this->db->insert('combo_items', $item);
                }
            }
            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);
            foreach ($wh_locations as $wh_id => $wh_location) {
                $this->db->update('warehouses_products', array('warehouse_location' => $wh_location, 'last_update' => date('Y-m-d H:i:s')), array('product_id' => $id, 'warehouse_id' => $wh_id));
            }
            foreach ($wh_qty as $wh_id => $wh_arr) {
                $whQt = $this->db->get_where('warehouses_products', ['product_id' => $id, 'warehouse_id' => $wh_id]);
                if ($whQt->num_rows() > 0) {
                    $this->db->update('warehouses_products', ['min_quantity'=> $wh_arr['min'], 'max_quantity'=> $wh_arr['max']], ['product_id' => $id, 'warehouse_id' => $wh_id]);
                } else {
                    $this->db->insert('warehouses_products', ['min_quantity'=> $wh_arr['min'], 'max_quantity'=> $wh_arr['max'], 'product_id' => $id, 'warehouse_id' => $wh_id]);
                }
            }

            $items_base[] = array(
                            'product_id' => $id,
                            'price' => $data['price'],
                        );
            if ($price_groups_data) {
                foreach ($price_groups_data as $pg) {
                    $pg['price'] = ($pg['price'] > 0 ? $pg['price'] : 0);
                    $ppr = $this->db->where(['product_id' => $id, 'price_group_id' => $pg['id']])->get('product_prices');
                    if (($this->Settings->allow_zero_price_products == 1 && $pg['price'] >= 0) || ($this->Settings->allow_zero_price_products == 0 && $pg['price'] > 0)) {
                        if ($this->Settings->prioridad_precios_producto == 10) {
                            $pg_unit[$pg['id']] = $pg['price'];
                        }
                        if ($ppr->num_rows() > 0) {
                            $ppr = $ppr->row();
                            $ppr_id = $ppr->id;
                            $this->db->update('product_prices', ['price' => ($pg['price']-$data['consumption_sale_tax'])], ['id' => $ppr_id]);
                        } else {
                            $this->db->insert('product_prices', ['product_id' => $id, 'price' => ($pg['price']-$data['consumption_sale_tax']), 'price_group_id' => $pg['id']]);
                        }
                    }
                }
            }
            // $this->sma->print_arrays($price_groups_data);

            $this->updateProductsPriceGroupBase($items_base);
            if ($hybrid_prices) {
                foreach ($hybrid_prices as $unit_id => $prices) {
                    foreach ($prices as $price_group_id => $price) {
                       $this->site->update_product_hybrid_price($id, $price_group_id, $unit_id, $price);
                    }
                }
            } else {
                if ($multiple_units) {

                    $this->db->delete('unit_prices', ['id_product' => $id, 'unit_id' => $data['unit']]);
                    $product_units = $this->get_product_unit_prices($id);
                    $puntdata = [];
                    if ($product_units) {
                        foreach ($product_units as $punit) {
                            $puntdata[$punit->unit_id] = 1;
                        }
                    }
                    foreach ($multiple_units as $munit) {
                        if ($this->Settings->prioridad_precios_producto == 10) {
                            $unit = $this->getUnitById($munit['unit_id']);
                            if ($unit->price_group_id) {
                                if (isset($pg_unit[$unit->price_group_id])) {
                                    $munit['price'] = $pg_unit[$unit->price_group_id] - ($data['consumption_sale_tax'] * ($unit->operation_value > 0 ? $unit->operation_value : 1));
                                }
                                $this->db->update('product_prices', ['price'=>$munit['price']], ['product_id'=>$id, 'price_group_id'=>$unit->price_group_id]);
                            }
                        } else {
                            $munit['price'] -= $munit['unit_ipoconsumo'];
                        }
                        if (isset($puntdata[$munit['unit_id']])) {
                            $this->db->update('unit_prices', ['valor_unitario' => ($munit['price']), 'cantidad' => $munit['cantidad'], 'margin_update_price' => $munit['margin_update_price'], 'status' => $munit['status']], ['id_product' => $id, 'unit_id' => $munit['unit_id']]);
                        } else {
                            $this->db->insert('unit_prices', ['id_product' => $id, 'unit_id' => $munit['unit_id'], 'valor_unitario' => ($munit['price']), 'cantidad' => $munit['cantidad'], 'margin_update_price' => $munit['margin_update_price'], 'status' => $munit['status']]);
                        }

                    }
                }  else {
                    if ($this->Settings->prioridad_precios_producto == 10 && count($pg_unit) > 0) {
                        foreach ($pg_unit as $pg_id => $price) {
                            $units = $this->site->get_units_by_price_group($pg_id, $id);
                            if ($units) {
                                foreach ($units as $unit) {
                                    $this->db->update('unit_prices', ['valor_unitario' => ($price - ($this->Settings->ipoconsumo ? ($data['consumption_sale_tax'] * ($unit->operation_value > 0 ? $unit->operation_value : 1)) : 0))], ['id_product' => $id, 'unit_id' => $unit->id]);
                                }
                            }
                        }
                    }
                }
            }
            if (!empty($old_attr_suffix)) {
                foreach ($old_attr_suffix as $pv_id => $suffix) {
                    $this->db->update('product_variants', ['suffix' => $suffix, 'last_update' => date('Y-m-d H:i:s')], ['product_id' => $id, 'id' => $pv_id]);
                }
            }
            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $id, 'photo' => $photo, 'last_update' => date('Y-m-d H:i:s')));
                }
            }
            if ($deleted_images) {
                foreach ($deleted_images as $dkey => $did) {
                    $dphoto = $this->db->get_where('product_photos', ['id'=>$did])->row();
                    unlink('assets/uploads/' . $dphoto->photo);
                    $this->db->delete('product_photos', ['id'=>$did]);
                }
            }
            if ($deleted_main_image == 1) {
                $pdata = $this->db->get_where('products', ['id'=>$id])->row();
                unlink('assets/uploads/' . $pdata->image);
                $this->db->update('products', ['image'=> NULL], ['id'=>$id]);
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {
                    //variant code consecutive
                    $update_pr_attr_consecutive = false;
                    if (isset($pr_attr['attr_code_consecutive']) && $pr_attr['attr_code_consecutive'] == 1) {
                        $pr_attr['pv_code_consecutive'] = $attr_code_consecutive = $pr_attr['attr_code_consecutive'];
                        unset($pr_attr['attr_code_consecutive']);
                        $pr_attr['code'] = $this->site->getCodeConsecutive('variant');
                        $update_pr_attr_consecutive = true;
                    }
                    unset($pr_attr['attr_code_consecutive']);
                    //variant code consecutive
                    $pr_attr['product_id'] = $id;
                    $pr_attr['last_update'] = date('Y-m-d H:i:s');
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    $this->db->insert('product_variants', $pr_attr);
                    $option_id = $this->db->insert_id();
                    //variant code consecutive
                    if ($update_pr_attr_consecutive) {
                        $this->site->updateCodeConsecutive('variant', ($pr_attr['code']+1));
                    }
                    //variant code consecutive
                    // $this->syncAddProductVariant($id, $option_id);
                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'quantity_received' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                        $this->db->insert('purchase_items', $item);

                    }
                }
            }

            $this->db->delete('product_preferences', array('product_id' => $id));
            if ($product_preferences) {
                foreach ($product_preferences as $prf => $prf_id) {
                    $preference = $this->get_preference_by_id($prf_id);
                    $this->db->insert('product_preferences',
                                        array(
                                            'product_id' => $id,
                                            'name' => $preference->name,
                                            'preference_id' => $prf_id,
                                            'preference_category_id' => $preference->category_id,
                                            )
                                    );
                }
            }

            $this->Settings->last_products_exclusivity_update = $time = date('Y:m:d H:i:s');
            $this->db->update('settings', ['last_products_exclusivity_update' => $time], ['setting_id'=> 1]);
            $this->db->delete('products_billers', ['product_id' => $id]); //Se borra relacion en todas las sucursales
            $add_billers = $this->site->getAllCompaniesWithState('biller');
            if ($arr_data['billers_changed'] == 1) { //cambiaron las sucursales
                if ($billers && count($billers) > 0) { //hay sucursales asignadas
                    foreach ($billers as $bkey => $biller_id) { //se registra relación a sucursal seleccionada
                        $this->db->insert('products_billers', ['product_id' => $id, 'biller_id' => $biller_id]);
                    }
                }
                foreach ($add_billers as $add_biller) { //se recorren todas las sucursales para actualizar relaciones temporales
                    $this->site->create_temporary_product_billers_assoc($add_biller->id);
                }
            } else { //No cambiaron las sucursales
                if (($billers && count($billers) == 0) || !$billers) {//Sin sucursales asignadas
                    foreach ($add_billers as $add_biller) {
                        $this->site->create_temporary_product_billers_assoc($add_biller->id);
                    }
                }
            }

            if ($pv_old_data) {
                foreach ($pv_old_data as $pv_id => $pv_data) {
                    $update_pv_data_consecutive = false;
                    if (isset($pv_data['p_variant_code_consecutive']) && $pv_data['p_variant_code_consecutive'] == 1) {
                        $pv_data['pv_code_consecutive'] = $p_variant_code_consecutive = $pv_data['p_variant_code_consecutive'];
                        $update_pv_data_consecutive = true;
                        $pv_data['code'] = $this->site->getCodeConsecutive('variant');
                    }
                    unset($pv_data['p_variant_code_consecutive']);
                    // $this->sma->print_arrays($pv_data);
                    $this->db->update('product_variants', $pv_data, ['id' => $pv_id]);
                    if ($update_pv_data_consecutive) {
                        $this->site->updateCodeConsecutive('variant', ($pv_data['code']+1));
                    }

                    if (isset($pv_data['image'])) {
                        $this->db->update('product_variants', ['image' => $pv_data['image']], ['id' => $pv_id]);
                    }
                }
            }
            $this->site->syncQuantity(NULL, NULL, NULL, $id);
            return true;
        } else {
            return false;
        }
    }

    private function getStatusProductVariant($data)
    {
        if ($data->status == ACTIVE) {
            $quantity = $this->getProductVariantQuantities(["option_id"=>$data->id, "product_id" => $data->product_id]);
            if (!empty($quantity->quantity)) {
                return ACTIVE;
            }
            return INACTIVE;
        }
        return INACTIVE;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            if ($this->db->update('warehouses_products_variants', array('quantity' => $quantity), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function updatePrice($data = array())
    {
        if ($this->db->update_batch('products', $data, 'code')) {
            return true;
        }
        return false;
    }

    public function deleteProduct($id)
    {
        if ($this->productHasMovements($id, false) || $this->productBelongsToCombo($id)) {
            return false;
        }

        if ($this->db->delete('products', array('id' => $id)) && $this->db->delete('warehouses_products', array('product_id' => $id))) {
            $this->db->delete('warehouses_products_variants', array('product_id' => $id));
            $this->db->delete('product_variants', array('product_id' => $id));
            $this->db->delete('product_photos', array('product_id' => $id));
            $this->db->delete('product_prices', array('product_id' => $id));
            return true;
        }
        return FALSE;
    }


    public function totalCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));
        return $q->num_rows();
    }

    public function getCategoryByCode($code)
    {
        $q = $this->db->get_where('categories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByCode($code)
    {
        $q = $this->db->get_where('tax_rates', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAdjustmentByID($id, $arr = false)
    {
        $q = $this->db->get_where('adjustments', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            if ($arr) {
                return $q->row_array();
            } else {
                return $q->row();
            }
        }
        return FALSE;
    }

    public function getAdjustmentItems($adjustment_id, $arr = false)
    {
        $this->db->select('
                        adjustment_items.*,
                        adjustment_items.id as adjustment_item_id,
                        products.code as product_code,
                        products.name as product_name,
                        products.code,
                        products.name as name,
                        products.avg_cost as price,
                        products.cost as cost,
                        products.tax_rate,
                        products.unit,
                        products.type as ptype,
                        products.image,
                        units.code as unit_code,
                        products.details as details,
                        IF('.$this->db->dbprefix('units').'.code IS NOT NULL, '.$this->db->dbprefix('units').'.code, '.$this->db->dbprefix('products').'.unit) as product_unit_code,
                        product_variants.name as variant')
            ->join('products', 'products.id=adjustment_items.product_id', 'left')
            ->join('units', 'products.unit=units.id', 'left')
            ->join('product_variants', 'product_variants.id=adjustment_items.option_id', 'left')
            ->group_by('adjustment_items.id')
            ->order_by('adjustment_items.combo_item_parent_id', 'desc');
        $this->db->where('adjustment_id', $adjustment_id);
        $q = $this->db->get('adjustment_items');
        if ($q->num_rows() > 0) {
            if ($arr) {
                foreach (($q->result_array()) as $row) {
                    $data[] = $row;
                }
            } else {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            }
            return $data;
        }
        return FALSE;
    }

    public function syncAdjustment($data = array())
    {
        if(! empty($data)) {
            $clause = array('product_id' => $data['product_id'], 'option_id' => $data['option_id'], 'warehouse_id' => $data['warehouse_id'], 'status' => 'received');
            $qty = $data['type'] == 'subtraction' ? 0 - $data['quantity'] : 0 + $data['quantity'];
            $this->site->setPurchaseItem($clause, $qty);

            $this->site->syncProductQty($data['product_id'], $data['warehouse_id']);
            if ($data['option_id']) {
                $this->site->syncVariantQty($data['option_id'], $data['warehouse_id'], $data['product_id']);
            }
        }
    }

    public function reverseAdjustment($id)
    {
        if ($products = $this->getAdjustmentItems($id)) {
            foreach ($products as $adjustment) {
                $clause = array('product_id' => $adjustment->product_id, 'warehouse_id' => $adjustment->warehouse_id, 'option_id' => $adjustment->option_id, 'status' => 'received');
                $qty = $adjustment->type == 'subtraction' ? (0+$adjustment->quantity) : (0-$adjustment->quantity);
                $this->site->setPurchaseItem($clause, $qty);
                $this->site->syncProductQty($adjustment->product_id, $adjustment->warehouse_id);
                if ($adjustment->option_id) {
                    $this->site->syncVariantQty($adjustment->option_id, $adjustment->warehouse_id, $adjustment->product_id);
                }
            }
        }
    }

    public function addAdjustment($data, $products, $productNames = [], $consecutive_update = true)
    {
        if ($consecutive_update) {
            $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
            if($referenceBiller) {
                $reference = $referenceBiller;
            } else {
                $reference = $this->site->getReference('qa');
            }
            $data['reference_no'] = $reference;
            $ref = explode("-", $reference);
            $consecutive = $ref[1];
        }
        if (!isset($data['status'])) {
            $data['status'] = 'completed';
        }
        if ($data['status'] == 'completed') {
            $data['completed_at'] = date('Y-m-d H:i:s');
        }
        if ($this->Settings->overselling == 0) {  // agregamos esta validacion si y solo si las sobreventas estan desactivadas
            $productArrayAux = $this->validateItemAddition($products); // para manejar el tipo de agregación de items 
            foreach ($productArrayAux as $product) {
                if ($product['type'] == 'subtraction') {
                    if (!empty($product['option_id'])) {
                        // exit('A');
                        $wh = $this->db->get_where('warehouses_products_variants',
                                                        [
                                                            'option_id'=> $product['option_id'],
                                                            'product_id'=> $product['product_id'],
                                                            'warehouse_id'=> $product['warehouse_id'],
                                                        ]);
                    } else {
                        $wh = $this->db->get_where('warehouses_products',
                                                        [
                                                            'product_id'=> $product['product_id'],
                                                            'warehouse_id'=> $product['warehouse_id'],
                                                        ]);
                    }
                    if ($wh->num_rows() > 0) {
                        $wh = $wh->row();
                        if ($wh->quantity < $product['quantity']) {
                            $ptr_product_data = $this->site->getProductByID($product['product_id']);
                            // $this->sma->print_arrays($product, $wh);
                            $this->session->set_flashdata('error', $ptr_product_data->name.' sin cantidad suficiente');
                            return false;
                        }
                    }
                }
            }
        }

        if ($this->db->insert('adjustments', $data)) {
            $adjustment_id = $this->db->insert_id();
            if ($consecutive_update) {
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            }
            $totalAdjustmenCost = 0;
            foreach ($products as $product) {
                if ($this->Settings->product_variant_per_serial == 1 && !empty($product['serial_no']) && $product['type'] == 'addition') {
                        $v_exist = $this->db->get_where('product_variants', ['name'=>$product['serial_no'], 'product_id' => $product['product_id']]);
                        if ($v_exist->num_rows() > 0) {
                            $v_exist = $v_exist->row();
                            $product['option_id'] =  $v_exist->id;
                        } else {
                            $DB2 = false;
                            $new_id = NULL;
                            if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
                                $DB2 = $this->settings_model->connect_new_year_database();
                                if (!$this->settings_model->check_same_id_insert($DB2, 'product_variants')) {
                                    $new_id = $this->settings_model->get_new_year_new_id_for_insertion($DB2, 'product_variants');
                                }
                            }
                            $this->db->insert('product_variants', [
                                                            'id' => $new_id,
                                                            'product_id' => $product['product_id'],
                                                            'name' => $product['serial_no'],
                                                            'cost' => $product['avg_cost'],
                                                            'price' => 0,
                                                            'quantity' => $product['quantity'],
                                                            'last_update' => date('Y-m-d H:i:s'),
                                            ]);
                            if ($DB2) {
                                $DB2->insert('product_variants', [
                                                        'id' => $new_id,
                                                        'product_id' => $product['product_id'],
                                                        'name' => $product['serial_no'],
                                                        'cost' => $product['avg_cost'],
                                                        'price' => 0,
                                                        'quantity' => $product['quantity'],
                                                        'last_update' => date('Y-m-d H:i:s'),
                                            ]);
                            }
                            $product['option_id'] =  $this->db->insert_id();
                            $this->db->insert('warehouses_products_variants', [
                                                'product_id' => $product['product_id'],
                                                'warehouse_id' => $product['warehouse_id'],
                                                'option_id' => $product['option_id'],
                                                ]);
                        }
                }
                $product['adjustment_id'] = $adjustment_id;
                if ($data['status'] == 'completed') {
                    $totalAdjustmenCost += (Double) $product['avg_cost'] * (Double) $product['quantity'];
                    if (($product['type'] == 'addition' && $this->Settings->set_adjustment_cost)) {
                        $update = $this->db->update('products', array('cost' => ($this->Settings->tax_method == 0 ? $product['avg_cost'] : $product['net_adjustment_cost'])), array('id' => $product['product_id']));
                    } else if ($product['type'] == 'addition' && isset($product['unit_cost'])) {
                        $update = $this->db->update('products', array('cost' => $product['unit_cost']), array('id' => $product['product_id']));
                        unset($product['unit_cost']);
                    }
                } else {
                    if (isset($product['unit_cost'])) {
                        unset($product['unit_cost']);
                    }
                }

                $this->db->insert('adjustment_items', $product);
                $adjustment_item_id = $this->db->insert_id();
                $product['adjustment_item_id'] = $adjustment_item_id;

                if ($data['status'] == 'completed') {
                    if ($product['type'] == 'addition') {
                        $this->site->updateAVCO(array('product_id' => $product['product_id'], 'warehouse_id' => $product['warehouse_id'], 'quantity' => ($product['type'] == 'addition' ? $product['quantity'] : $product['quantity'] * -1), 'cost' => $product['avg_cost'], 'adjustment_item_id' => $product['adjustment_item_id']));
                    }
                    $this->syncAdjustment($product);
                }
            }
            $this->site->syncStoreProductsQuantityMovement($products, 1);
            if ($data['status'] == 'completed') {
                $data['grand_total'] = $totalAdjustmenCost;
                $conta = $this->site->wappsiContabilidadAjustes($data, $products, $productNames);
            }
            return true;
        }
        return false;
    }


    public function updateAdjustment($id, $data, $products, $productNames = [], $consecutive_update = true)
    {
        if (!isset($data['status'])) {
            $data['status'] = 'completed';
        }
        if ($data['status'] == 'completed') {
            $data['completed_at'] = date('Y-m-d H:i:s');
        }
        if ($this->db->update('adjustments', $data, ['id'=>$id])) {
            $totalAdjustmenCost = 0;
            foreach ($products as $product) {
                if ($this->Settings->product_variant_per_serial == 1 && !empty($product['serial_no']) && $product['type'] == 'addition') {
                        $v_exist = $this->db->get_where('product_variants', ['name'=>$product['serial_no'], 'product_id' => $product['product_id']]);
                        if ($v_exist->num_rows() > 0) {
                            $v_exist = $v_exist->row();
                            $product['option_id'] =  $v_exist->id;
                        } else {
                            $DB2 = false;
                            $new_id = NULL;
                            if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
                                $DB2 = $this->settings_model->connect_new_year_database();
                                if (!$this->settings_model->check_same_id_insert($DB2, 'product_variants')) {
                                    $new_id = $this->settings_model->get_new_year_new_id_for_insertion($DB2, 'product_variants');
                                }
                            }
                            $this->db->insert('product_variants', [
                                                            'id' => $new_id,
                                                            'product_id' => $product['product_id'],
                                                            'name' => $product['serial_no'],
                                                            'cost' => $product['avg_cost'],
                                                            'price' => 0,
                                                            'quantity' => $product['quantity'],
                                                            'last_update' => date('Y-m-d H:i:s'),
                                            ]);
                            if ($DB2) {
                                $DB2->insert('product_variants', [
                                                        'id' => $new_id,
                                                        'product_id' => $product['product_id'],
                                                        'name' => $product['serial_no'],
                                                        'cost' => $product['avg_cost'],
                                                        'price' => 0,
                                                        'quantity' => $product['quantity'],
                                                        'last_update' => date('Y-m-d H:i:s'),
                                            ]);
                            }
                            $product['option_id'] =  $this->db->insert_id();
                            $this->db->insert('warehouses_products_variants', [
                                                'product_id' => $product['product_id'],
                                                'warehouse_id' => $product['warehouse_id'],
                                                'option_id' => $product['option_id'],
                                                ]);
                        }
                }
                $product['adjustment_id'] = $id;
                if ($data['status'] == 'completed') {
                    $totalAdjustmenCost += (Double) $product['avg_cost'] * (Double) $product['quantity'];
                    if (($product['type'] == 'addition' && $this->Settings->set_adjustment_cost)) {
                        $update = $this->db->update('products', array('cost' => $product['avg_cost']), array('id' => $product['product_id']));
                    } else if ($product['type'] == 'addition' && isset($product['unit_cost'])) {
                        $update = $this->db->update('products', array('cost' => $product['unit_cost']), array('id' => $product['product_id']));
                        unset($product['unit_cost']);
                    }
                } else {
                    if (isset($product['unit_cost'])) {
                        unset($product['unit_cost']);
                    }
                }
                $item_id = $product['item_id'];
                unset($product['item_id']);
                $this->db->update('adjustment_items', $product, ['id'=> $item_id]);
                $product['adjustment_item_id'] = $item_id;

                if ($data['status'] == 'completed') {
                    if ($product['type'] == 'addition') {
                        $this->site->updateAVCO(array('product_id' => $product['product_id'], 'warehouse_id' => $product['warehouse_id'], 'quantity' => ($product['type'] == 'addition' ? $product['quantity'] : $product['quantity'] * -1), 'cost' => $product['avg_cost'], 'adjustment_item_id' => $product['adjustment_item_id']));
                    }
                    $this->syncAdjustment($product);
                }
            }
            if ($data['status'] == 'completed') {
                $data['grand_total'] = $totalAdjustmenCost;
                $this->products_model->recontabilizarAjuste($id);
            }
            // $this->sma->print_arrays($data, $products);
            return true;
        }
        return false;
    }

    public function deleteAdjustment($id)
    {
        $this->reverseAdjustment($id);
        if ( $this->db->delete('adjustments', array('id' => $id)) &&
            $this->db->delete('adjustment_items', array('adjustment_id' => $id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array();
        }
        return FALSE;
    }

    /* --------------------------------------------------------------------------------------------- */

    //Wappsi code

    public function getSCSuggestions($term, $limit, $categories , $brands )
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name')
        ->where("type != 'combo' AND " . $this->db->dbprefix('products') . ".code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."")
        ->limit($limit);
            if ($categories) {
                $r = 1;
                $this->db->group_start();
                foreach ($categories as $category) {
                    if ($r == 1) {
                        $this->db->where('products.category_id', $category);
                    } else {
                        $this->db->or_where('products.category_id', $category);
                    }
                    $r++;
                }
                $this->db->group_end();
            }
            if ($brands) {
                $r = 1;
                $this->db->group_start();
                foreach ($brands as $brand) {
                    if ($r == 1) {
                        $this->db->where('products.brand', $brand);
                    } else {
                        $this->db->or_where('products.brand', $brand);
                    }
                    $r++;
                }
                $this->db->group_end();
            }
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                //$row->name = $categories[0];
                //$row->name = $limit;
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function addSequentialCount($data, $products)
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        if ($this->db->insert('sequential_count', $data)) {
            $adjustment_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($products as $product) {
                $product['adjustment_id'] = $adjustment_id;
                $this->db->insert('sequential_count_items', $product);
            }
            return true;
        }
        return false;
    }

    public function updateSequentialCount($id)
    {
        $data = array(
          'apply' => 1
        );

        if ($this->db->update('sequential_count', $data, array('id' => $id))) {
          return true;
        }
        return false;
    }

    public function updateEditSequentialCount($id, $data, $products)
    {
        if ($this->db->update('sequential_count', $data, array('id' => $id)) &&
            $this->db->delete('sequential_count_items', array('adjustment_id' => $id))) {
            foreach ($products as $product) {
                $product['adjustment_id'] = $id;
                $this->db->insert('sequential_count_items', $product);
            }
            return true;
        }
        return false;
    }

    public function deleteSequentialCount($id)
    {
        if ( $this->db->delete('sequential_count', array('id' => $id)) &&
            $this->db->delete('sequential_count_items', array('adjustment_id' => $id))) {
            return true;
        }
        return false;
    }

    public function getSecuencialCountByID($id)
    {
        $q = $this->db->get_where('sequential_count', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSecuencialCountItems($adjustment_id)
    {
        $this->db->select('sequential_count_items.*,
        products.code as product_code,
        products.name as product_name,
        products.image,
        products.details as details,
        products.cost as cost,
        warehouses_products.quantity as warehouse_quantity
        ')
        ->join('products', 'products.id=sequential_count_items.product_id', 'left')
        ->join('warehouses_products', 'products.id=warehouses_products.product_id and warehouses_products.warehouse_id = sequential_count_items.warehouse_id ', 'left')
        ->group_by('sequential_count_items.id')
        ->order_by('id', 'asc');
        $this->db->where('adjustment_id', $adjustment_id);
        $q = $this->db->get('sequential_count_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSecuencialNoCountItems($adjustment_id,$categories,$brands)
    {
        $pos = strpos($categories, ',');
        if ($pos !== false) {
            $categories = explode(", ", $categories);
        }else{
            $categories = null;
        }

        $pos = strpos($brands, ',');
        if ($pos !== false) {
            $brands = explode(", ", $brands);
        }else{
            $brands = null;
        }
        //$this->sma->print_arrays($categories);

        $this->db->select('products.id as product_id')
        ->join('products', 'products.id=sequential_count_items.product_id', 'left')
        ->join('warehouses_products', 'products.id=warehouses_products.product_id and warehouses_products.warehouse_id = sequential_count_items.warehouse_id ', 'left')
        ->group_by('sequential_count_items.id');

        $this->db->where('adjustment_id', $adjustment_id);
        $q = $this->db->get('sequential_count_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $ids[] = $row->product_id;
            }
            //return $data;
        }
        //$this->sma->print_arrays($ids);

        $this->db->select('
        products.id as product_id,
        products.code as product_code,
        products.name as product_name,
        warehouses_products.quantity as product_quantity
        ')
        ->join('warehouses_products', 'products.id = warehouses_products.product_id', 'left')
        ->join('sequential_count_items', 'sequential_count_items.warehouse_id = warehouses_products.warehouse_id', 'left')
        ->group_by('products.id');

        $this->db->where('sequential_count_items.adjustment_id', $adjustment_id);
        $this->db->where_not_in('products.id', $ids);
        if ($categories) {
            $r = 1;
            $this->db->group_start();
            foreach ($categories as $category) {
                if ($r == 1) {
                    $this->db->where('products.category_id', $category);
                } else {
                    $this->db->or_where('products.category_id', $category);
                }
                $r++;
            }
            $this->db->group_end();
        }
        if ($brands) {
            $r = 1;
            $this->db->group_start();
            foreach ($brands as $brand) {
                if ($r == 1) {
                    $this->db->where('products.brand', $brand);
                } else {
                    $this->db->or_where('products.brand', $brand);
                }
                $r++;
            }
            $this->db->group_end();
        }
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        //$this->sma->print_arrays('hola');
        return FALSE;
    }
    //Termina Wappsi code

    /* --------------------------------------------------------------------------------------------- */

    public function addQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {

        if ($this->getProductQuantity($product_id, $warehouse_id)) {
            if ($this->updateQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        } else {
            if ($this->insertQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {
        $product = $this->site->getProductByID($product_id);
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity, 'rack' => $rack, 'avg_cost' => $product->cost, 'last_update' => date('Y-m-d H:i:s')))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {
        $data = $rack ? array('quantity' => $quantity, 'rack' => $rack) : $data = array('quantity' => $quantity);
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->update('warehouses_products', $data, array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function products_count($category_id, $subcategory_id = NULL)
    {
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->from('products');
        return $this->db->count_all_results();
    }

    public function fetch_products($category_id, $limit, $start, $subcategory_id = NULL)
    {

        $this->db->limit($limit, $start);
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->order_by("id", "asc");
        $query = $this->db->get("products");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductVariantQuantities($data)
    {
        $this->db->select("SUM(quantity) AS quantity");
        $this->db->where($data);
        $q = $this->db->get('warehouses_products_variants');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncVariantQty($option_id)
    {
        $wh_pr_vars = $this->getProductWarehouseOptions($option_id);
        $qty = 0;
        foreach ($wh_pr_vars as $row) {
            $qty += $row->quantity;
        }
        if ($this->db->update('product_variants', array('quantity' => $qty), array('id' => $option_id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductWarehouseOptions($option_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function setRack($data)
    {
        if ($this->db->update('warehouses_products', array('rack' => $data['rack'], 'last_update' => date('Y-m-d H:i:s')), array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id']))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getSoldQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('sale_items') . ".quantity ) as sold, SUM( " . $this->db->dbprefix('sale_items') . ".subtotal ) as amount")
            ->from('sales')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('sale_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('purchase_items') . ".quantity ) as purchased, SUM( " . $this->db->dbprefix('purchase_items') . ".subtotal ) as amount")
            ->from('purchases')
            ->join('purchase_items', 'purchases.id=purchase_items.purchase_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('purchase_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllVariants()
    {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouseProductVariant($warehouse_id, $product_id, $option_id = NULL)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('product_id' => $product_id, 'option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchaseItems($purchase_id)
    {
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTransferItems($transfer_id)
    {
        $q = $this->db->get_where('purchase_items', array('transfer_id' => $transfer_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUnitByCode($code)
    {
        $q = $this->db->get_where("units", array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUnitById($id)
    {
        $q = $this->db->get_where("units", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getBrand($data)
    {
        $this->db->where($data);
        $q = $this->db->get_where('brands');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getBrandByName($name)
    {
        $q = $this->db->get_where('brands', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getBrandByCode($code)
    {
        $q = $this->db->get_where('brands', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getStockCountProducts($warehouse_id, $type, $categories = NULL, $brands = NULL)
    {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('warehouses_products')}.quantity as quantity, {$this->db->dbprefix('products')}.discontinued as discontinued")
        ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
        ->where('warehouses_products.warehouse_id', $warehouse_id)
        ->where('products.type', 'standard')
        ->order_by('products.code', 'asc');
        if ($categories) {
            $r = 1;
            $this->db->group_start();
            foreach ($categories as $category) {
                if ($r == 1) {
                    $this->db->where('products.category_id', $category);
                } else {
                    $this->db->or_where('products.category_id', $category);
                }
                $r++;
            }
            $this->db->group_end();
        }
        if ($brands) {
            $r = 1;
            $this->db->group_start();
            foreach ($brands as $brand) {
                if ($r == 1) {
                    $this->db->where('products.brand', $brand);
                } else {
                    $this->db->or_where('products.brand', $brand);
                }
                $r++;
            }
            $this->db->group_end();
        }

        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStockCountProductVariants($warehouse_id, $product_id)
    {
        $this->db->select("{$this->db->dbprefix('product_variants')}.name, {$this->db->dbprefix('warehouses_products_variants')}.quantity as quantity")
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $product_id, 'warehouses_products_variants.warehouse_id' => $warehouse_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function addStockCount($data)
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        if ($this->db->insert('stock_counts', $data)) {
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            return TRUE;
        }
        return FALSE;
    }

    public function finalizeStockCount($id, $data, $products)
    {
        if ($this->db->update('stock_counts', $data, array('id' => $id))) {
            foreach ($products as $product) {
                if (isset($product['activo'])) {
                    unset($product['activo']);
                }
                $this->db->insert('stock_count_items', $product);
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getStouckCountByID($id)
    {
        $q = $this->db->get_where("stock_counts", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getStockCountItems($stock_count_id)
    {
        $q = $this->db->get_where("stock_count_items", array('stock_count_id' => $stock_count_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return NULL;
    }

    public function getAdjustmentByCountID($count_id)
    {
        $q = $this->db->get_where('adjustments', array('count_id' => $count_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductVariantID($product_id, $name)
    {
        $q = $this->db->get_where("product_variants", array('product_id' => $product_id, 'name' => $name), 1);
        if ($q->num_rows() > 0) {
            $variant = $q->row();
            return $variant->id;
        }
        return NULL;
    }

    public function getProductVariantByCode($product_id, $code)
    {
        $q = $this->db->get_where("product_variants", array('product_id' => $product_id, 'code' => $code), 1);
        if ($q->num_rows() > 0) {
            $variant = $q->row();
            return $variant;
        }
        return NULL;
    }

    public function findProductVariant($data)
    {
        $this->db->where($data);
        $q = $this->db->get("product_variants");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function findProductVariants($data)
    {
        $this->db->where($data);
        $q = $this->db->get("product_variants");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function updateProductsCost($products, $month){

        $fecha = new DateTime(date('Y-'.$month.'-d'));
        $fecha->modify('first day of this month');
        $fecha_inicio = $fecha->format('Y-m-d');
        $fecha->modify('last day of this month');
        $fecha_fin = $fecha->format('Y-m-d');

        // $this->syncProductCostingByMonth();

        foreach ($products as $product) {

            $this->db->update('costing',
                                        array('purchase_net_unit_cost' => $product['M'.$month]),
                                        array('product_id' => $product['product_id'],
                                              'date >=' => $fecha_inicio,
                                              'date <=' => $fecha_fin,
                                            )
                             );

            $p_exists = $this->db->where('product_id', $product['product_id'])
                                 ->where('year', date('Y'))
                                 ->get('cost_per_month');

            if ($p_exists->num_rows() > 0) {
                $this->db->update('cost_per_month', $product, array('product_id' => $product['product_id'], 'year' => date('Y')));
            } else {
                $this->db->insert('cost_per_month', $product);
            }
        }

        return true;
    }

    public function getAllProductsWithCostingByMonth($m){
        $fecha = new DateTime(date('Y-'.$m.'-d'));
        $fecha->modify('first day of this month');
        $fecha_inicio = $fecha->format('Y-m-d');
        $fecha->modify('last day of this month');
        $fecha_fin = $fecha->format('Y-m-d');
        $pc = "(SELECT C.product_id, C.purchase_unit_cost FROM ".$this->db->dbprefix('costing')." C WHERE C.date >= '{$fecha_inicio}' AND C.date <= '{$fecha_fin}' GROUP BY C.product_id) AS PC";
        $q = $this->db->select('PC.purchase_unit_cost, products.code, products.id')
                      ->join($pc, 'products.id = PC.product_id', 'left')
                      ->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $p_costing) {
                $data[] = $p_costing;
            }
            return $data;
        }
        return false;
    }


    public function syncProductCostingByMonth(){

        $products = $this->db->get('products');
        $pc = $this->db->get_where('cost_per_month', array('year' => date('Y')));
        $product_costing = [];
        if ($pc->num_rows() > 0) {
            foreach (($pc->result()) as $pcosting) {
                $product_costing[$pcosting->product_id] = 1;
            }
        }

        foreach (($products->result()) as $product) {
            if (!isset($product_costing[$product->id])) {
                $data = array(
                            'product_id' => $product->id,
                            'year' => date('Y'),
                            );
                $this->db->insert('cost_per_month', $data);
            }
        }
        return true;
    }

    public function addProductCostingByMonth($product_id){

        $data = array(
                        'product_id' => $product_id,
                        'year' => date('Y'),
                    );

        if ($this->db->insert('cost_per_month', $data)) {
            return true;
        }

        return false;
    }

    public function getProductPriceGroups($product_id){

        $q = $this->db->order_by('price_group_base desc')->get('price_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->id] = (object) array('id' => $row->id,  'name' => $row->name, 'price' => 0);
            }
            $biller_id = $this->session->userdata('biller_id');
            if ($this->Settings->prioridad_precios_producto == 11) {
                $q = $this->db->query("select
                                        PG.id,
                                        PG.name AS name,
                                        IF(PG.price_group_base = 1, PPR.price, UP.valor_unitario) AS price,
                                        PG.price_group_base,
                                        U.operator AS unit_operator,
                                        U.operation_value AS unit_operation_value
                                        from {$this->db->dbprefix('products')} P
                                            inner join {$this->db->dbprefix('product_prices')} PPR ON PPR.product_id = P.id
                                            inner join {$this->db->dbprefix('price_groups')} PG ON PG.id = PPR.price_group_id
                                            left join {$this->db->dbprefix('unit_prices')} UP ON UP.price_group_id = PPR.price_group_id AND UP.id_product = PPR.product_id
                                            left join {$this->db->dbprefix('units')} U ON (IF(PG.price_group_base = 1 AND U.id IS NULL, U.id = P.unit, U.id = UP.unit_id))
                                        WHERE P.id = {$product_id}");
            } else {
                $q = $this->db->select('
                                            price_groups.id,
                                            price_groups.name as name,
                                            product_prices.price as price,
                                            price_groups.price_group_base,
                                            units.operator as unit_operator,
                                            units.operation_value as unit_operation_value
                                        ')
                              ->join('price_groups', 'price_groups.id = product_prices.price_group_id', 'inner')
                              ->join('unit_prices', 'unit_prices.id_product = product_prices.product_id', 'left')
                              ->join('units', 'units.id = unit_prices.unit_id AND units.price_group_id = price_groups.id', 'left')
                              ->where('product_prices.product_id', $product_id)
                              ->where('('.$this->db->dbprefix('units').'.operator IS NOT NULL OR '.$this->db->dbprefix('price_groups').'.price_group_base)')
                              ->group_by('price_groups.id')
                              ->order_by('price_groups.id asc')
                              ->get('product_prices');
            }
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[$row->id] = $row;
                }
                if ((isset($this->GP) && !$this->GP['system_settings-update_products_group_prices']) && $this->session->userdata('biller_id') && $this->Settings->users_can_view_price_groups == 0) {
                    $datab = [];
                    $biller_id = $this->session->userdata('biller_id');
                    $q = $this->db->get_where('biller_data', array('biller_id' => $biller_id));
                    if ($q->num_rows() > 0) {
                        $q = $q->row();
                        if (isset($data[$q->default_price_group])) {
                            $datab[] = $data[$q->default_price_group];
                        }
                    }
                    return $datab;
                } else {
                    foreach ($data as $dpgrkey => $dpgrvalue) {
                        if ($data[$dpgrkey]->price == 0) {
                           $pgpricedata = $this->getProductPrice(['product_id'=>$product_id, 'price_group_id'=>$dpgrvalue->id]);
                            $data[$dpgrkey]->price = $pgpricedata ? $pgpricedata->price : 0;
                        }
                    }
                    return $data;
                }
            }
        }
        return false;

    }

    public function updateProductsPriceGroupBase($items = array()){

        if (count($items) == 0) {
            $products = $this->getAllProducts();
            $items = [];
            foreach ($products as $product) {
                $items[] = array(
                                'product_id' => $product->id,
                                'price' => $product->price,
                            );
            }
        }

        $q = $this->db->get_where('price_groups', array('price_group_base' => 1));

        if ($q->num_rows() > 0) {
            $q = $q->row();
            $price_group_base_id = $q->id;
        } else {
            $this->db->insert('price_groups', array(
                                                    'name' => 'PVP Base',
                                                    'price_group_base' => '1',
                                                    )
                            );
            $price_group_base_id = $this->db->insert_id();
        }

        foreach ($items as $item) {
            $datos = array(
                            'product_id' => $item['product_id'],
                            'price_group_id' => $price_group_base_id,
                            'price' => $item['price'],
                        );

            $q = $this->db->get_where('product_prices', array('product_id' => $item['product_id'], 'price_group_id' => $price_group_base_id));
            if ($q->num_rows() == 0) {
                $this->db->insert('product_prices', $datos);
            } else {
                $q = $q->row();
                $this->db->update('product_prices', $datos, array('id' => $q->id));
            }

        }

        return true;

    }

    public function productHasMovements($product_id, $no_option = true){
        $conditions = ['product_id' => $product_id];
        if ($no_option) {
            $conditions = ['product_id' => $product_id, 'option_id' => NULL];
        }
        $conditions['status !='] = 'service_received';
        $purchases = $this->db->get_where('purchase_items', $conditions);
        if ($purchases->num_rows() > 0) {
            return true;
        }
        unset($conditions['status !=']);
        $sales = $this->db->get_where('sale_items', $conditions);
        if ($sales->num_rows() > 0) {
            return true;
        }
        $adjustmens = $this->db->get_where('adjustment_items', $conditions);
        if ($adjustmens->num_rows() > 0) {
            return true;
        }
        $tranfers = $this->db->get_where('transfer_items', $conditions);
        if ($tranfers->num_rows() > 0) {
            return true;
        }
        $orderSale = $this->db->get_where('order_sale_items', $conditions);
        if ($orderSale->num_rows() > 0) {
            return true;
        }
        $quotes = $this->db->get_where('quote_items', $conditions);
        if ($quotes->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function productBelongsToCombo($product_id){

        $product = $this->getProductByID($product_id);

        $q = $this->db->get_where('combo_items', array('item_code' => $product->code));
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function getPORSuggestions($term, $biller_id)
    { $limit = $this->Settings->max_num_results_display;
        $this->site->create_temporary_product_billers_assoc($biller_id);
        $this->db->select($this->db->dbprefix('products') . '.id,
                          code,
                          ' . $this->db->dbprefix('products') . '.name as name,
                          ' . $this->db->dbprefix('products') . '.avg_cost as price,
                          ' . $this->db->dbprefix('products') . '.cost as cost,
                          tax_rate,
                          unit,
                          type')
            ->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
            ->where("(type = 'subproduct' or type = 'pfinished') AND "
                . "(" . $this->db->dbprefix('products') . ".name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return false;
    }

    public function get_unit_by_price_group($pg_id)
    {
        $q = $this->db->get_where("units", array('price_group_id' => $pg_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_kardex_report($product, $warehouse_id, $start_date, $end_date, $option_id){

        $sql_purchases = "SELECT
                                {$this->db->dbprefix('purchases')}.id as id,
                                {$this->db->dbprefix('purchases')}.date as date,
                                {$this->db->dbprefix('purchases')}.registration_date as registration_date,
                                {$this->db->dbprefix('purchases')}.reference_no as reference_no,
                                {$this->db->dbprefix('purchases')}.supplier_id as company,
                                WH.name as warehouse,
                                IF({$this->db->dbprefix('purchase_items')}.quantity > 0, {$this->db->dbprefix('purchase_items')}.quantity, 0) as quantity_in,
                                IF({$this->db->dbprefix('purchase_items')}.quantity < 0, ({$this->db->dbprefix('purchase_items')}.quantity * -1), 0) as quantity_out,
                                COALESCE({$this->db->dbprefix('purchase_items')}.unit_cost, 0) unit_cost,
                                0 as avg_cost,
                                0 as balance,
                                (SELECT name FROM {$this->db->dbprefix('product_variants')} WHERE id = {$this->db->dbprefix('purchase_items')}.option_id ) AS nameVarian
                            FROM {$this->db->dbprefix('purchase_items')}
                                INNER JOIN {$this->db->dbprefix('purchases')} ON
                                                    {$this->db->dbprefix('purchase_items')}.purchase_id = {$this->db->dbprefix('purchases')}.id
                                                    AND {$this->db->dbprefix('purchase_items')}.product_id = {$product}
                                                    AND {$this->db->dbprefix('purchase_items')}.status != 'service_received'
                                INNER JOIN {$this->db->dbprefix('warehouses')} WH ON WH.id = {$this->db->dbprefix('purchases')}.warehouse_id
                            WHERE {$this->db->dbprefix('purchases')}.purchase_type = 1 ";
        $sql_sales = "SELECT
                            {$this->db->dbprefix('sales')}.id as id,
                            {$this->db->dbprefix('sales')}.date as date,
                            {$this->db->dbprefix('sales')}.registration_date as registration_date,
                            {$this->db->dbprefix('sales')}.reference_no as reference_no,
                            {$this->db->dbprefix('sales')}.customer_id as company,
                            WH.name as warehouse,
                            IF({$this->db->dbprefix('sale_items')}.quantity < 0, ({$this->db->dbprefix('sale_items')}.quantity * -1), 0) as quantity_in,
                            IF({$this->db->dbprefix('sale_items')}.quantity > 0, {$this->db->dbprefix('sale_items')}.quantity, 0) as quantity_out,
                            0 as unit_cost,
                            IF ({$this->db->dbprefix('sale_items')}.quantity > 0, costing.purchase_unit_cost, return_costing.purchase_unit_cost) as avg_cost,
                            0 as balance,
                            (SELECT name FROM {$this->db->dbprefix('product_variants')} WHERE id = {$this->db->dbprefix('sale_items')}.option_id ) AS nameVarian
                        FROM {$this->db->dbprefix('sale_items')}
                            INNER JOIN {$this->db->dbprefix('sales')} ON
                                                    {$this->db->dbprefix('sale_items')}.sale_id = {$this->db->dbprefix('sales')}.id
                                                    AND {$this->db->dbprefix('sale_items')}.product_id = {$product}
                            INNER JOIN {$this->db->dbprefix('warehouses')} WH ON WH.id = {$this->db->dbprefix('sales')}.warehouse_id
                            LEFT JOIN (SELECT {$this->db->dbprefix('costing')}.sale_item_id, MAX({$this->db->dbprefix('costing')}.purchase_unit_cost) purchase_unit_cost FROM {$this->db->dbprefix('costing')} GROUP BY {$this->db->dbprefix('costing')}.sale_item_id) costing ON costing.sale_item_id = {$this->db->dbprefix('sale_items')}.id
                            LEFT JOIN (SELECT {$this->db->dbprefix('sale_items')}.product_id, {$this->db->dbprefix('sale_items')}.sale_id, {$this->db->dbprefix('costing')}.purchase_unit_cost FROM {$this->db->dbprefix('sale_items')} INNER JOIN {$this->db->dbprefix('costing')} ON {$this->db->dbprefix('costing')}.sale_item_id = {$this->db->dbprefix('sale_items')}.id GROUP BY {$this->db->dbprefix('sale_items')}.product_id, {$this->db->dbprefix('sale_items')}.sale_id) return_costing ON return_costing.product_id = {$this->db->dbprefix('sale_items')}.product_id AND return_costing.sale_id = {$this->db->dbprefix('sales')}.sale_id";

        $sql_adjustments = "SELECT
                                    {$this->db->dbprefix('adjustments')}.id as id,
                                    {$this->db->dbprefix('adjustments')}.date as date,
                                    {$this->db->dbprefix('adjustments')}.registration_date as registration_date,
                                    {$this->db->dbprefix('adjustments')}.reference_no as reference_no,
                                    {$this->db->dbprefix('adjustments')}.companies_id as company,
                                    WH.name as warehouse,
                                    IF({$this->db->dbprefix('adjustment_items')}.type = 'addition', {$this->db->dbprefix('adjustment_items')}.quantity, 0) as quantity_in,
                                    IF({$this->db->dbprefix('adjustment_items')}.type = 'subtraction', {$this->db->dbprefix('adjustment_items')}.quantity, 0) as quantity_out,

                                    IF({$this->db->dbprefix('adjustment_items')}.type = 'addition', COALESCE({$this->db->dbprefix('adjustment_items')}.adjustment_cost, {$this->db->dbprefix('adjustment_items')}.avg_cost), 0) as unit_cost,
                                    IF({$this->db->dbprefix('adjustment_items')}.type = 'subtraction', COALESCE({$this->db->dbprefix('adjustment_items')}.adjustment_cost, {$this->db->dbprefix('adjustment_items')}.avg_cost), 0) as avg_cost,
                                    0 as balance,
                                    (SELECT name FROM {$this->db->dbprefix('product_variants')} WHERE id = {$this->db->dbprefix('adjustment_items')}.option_id ) AS nameVarian
                                FROM {$this->db->dbprefix('adjustment_items')}
                                    INNER JOIN {$this->db->dbprefix('adjustments')} ON
                                                    {$this->db->dbprefix('adjustment_items')}.adjustment_id = {$this->db->dbprefix('adjustments')}.id
                                                    AND {$this->db->dbprefix('adjustment_items')}.product_id = {$product}
                                    INNER JOIN {$this->db->dbprefix('warehouses')} WH ON WH.id = {$this->db->dbprefix('adjustments')}.warehouse_id";

        if ($warehouse_id) {
            $sql_transfers = "SELECT
                        {$this->db->dbprefix('transfers')}.id as id,
                        {$this->db->dbprefix('transfers')}.date as date,
                        {$this->db->dbprefix('transfers')}.fecha_registro as registration_date,
                        {$this->db->dbprefix('transfers')}.reference_no as reference_no,
                        NULL as company,
                        CONCAT('DE : ', FRWH.name, ' A : ', TOWH.name) as warehouse,
                        IF({$this->db->dbprefix('transfers')}.to_warehouse_id = ".$warehouse_id.", {$this->db->dbprefix('purchase_items')}.quantity, 0) as quantity_in,
                        IF({$this->db->dbprefix('transfers')}.from_warehouse_id = ".$warehouse_id.", {$this->db->dbprefix('purchase_items')}.quantity, 0) as quantity_out,
                        {$this->db->dbprefix('purchase_items')}.unit_cost,
                        0 as avg_cost,
                        0 as balance,
                        (SELECT name FROM {$this->db->dbprefix('product_variants')} WHERE id = {$this->db->dbprefix('purchase_items')}.option_id ) AS nameVarian
                    FROM {$this->db->dbprefix('purchase_items')}
                        INNER JOIN {$this->db->dbprefix('transfers')} ON
                                                    {$this->db->dbprefix('purchase_items')}.transfer_id = {$this->db->dbprefix('transfers')}.id
                                                    AND {$this->db->dbprefix('purchase_items')}.product_id = {$product}
                        INNER JOIN {$this->db->dbprefix('warehouses')} TOWH ON TOWH.id = {$this->db->dbprefix('transfers')}.to_warehouse_id
                        INNER JOIN {$this->db->dbprefix('warehouses')} FRWH ON FRWH.id = {$this->db->dbprefix('transfers')}.from_warehouse_id
                    WHERE ({$this->db->dbprefix('transfers')}.to_warehouse_id = ".$warehouse_id." OR {$this->db->dbprefix('transfers')}.from_warehouse_id = ".$warehouse_id.")
                        ";

            $sql_purchases.=" AND {$this->db->dbprefix('purchase_items')}.warehouse_id = ".$warehouse_id;
            $sql_sales.=" WHERE {$this->db->dbprefix('sale_items')}.warehouse_id = ".$warehouse_id;
            $sql_adjustments.=" WHERE {$this->db->dbprefix('adjustment_items')}.warehouse_id = ".$warehouse_id;
        } else {
            $sql_transfers = "SELECT
                        {$this->db->dbprefix('transfers')}.id as id,
                        {$this->db->dbprefix('transfers')}.date as date,
                        {$this->db->dbprefix('transfers')}.fecha_registro as registration_date,
                        {$this->db->dbprefix('transfers')}.reference_no as reference_no,
                        NULL as company,
                        CONCAT('DE : ', FRWH.name, ' A : ', TOWH.name) as warehouse,
                        0 as quantity_in,
                        0 as quantity_out,
                        0 as unit_cost,
                        0 as avg_cost,
                        0 as balance,
                        (SELECT name FROM {$this->db->dbprefix('product_variants')} WHERE id = {$this->db->dbprefix('purchase_items')}.option_id ) AS nameVarian
                    FROM {$this->db->dbprefix('purchase_items')}
                        INNER JOIN {$this->db->dbprefix('transfers')} ON
                                                    {$this->db->dbprefix('purchase_items')}.transfer_id = {$this->db->dbprefix('transfers')}.id
                                                    AND {$this->db->dbprefix('purchase_items')}.product_id = {$product}
                        INNER JOIN {$this->db->dbprefix('warehouses')} TOWH ON TOWH.id = {$this->db->dbprefix('transfers')}.to_warehouse_id
                        INNER JOIN {$this->db->dbprefix('warehouses')} FRWH ON FRWH.id = {$this->db->dbprefix('transfers')}.from_warehouse_id";
        }

        if ($start_date) {
            if ($warehouse_id) {
                $sql_sales .= " AND ";
                $sql_adjustments .= " AND ";
                $sql_transfers .= " AND ";
            } else {
                $sql_sales .= " WHERE ";
                $sql_adjustments .= " WHERE ";
                $sql_transfers .= " WHERE ";
            }
            $sql_sales .= "  {$this->db->dbprefix('sales')}.date >= '".$start_date."' AND {$this->db->dbprefix('sales')}.date <= '".$end_date."'";
            $sql_purchases .= " AND  {$this->db->dbprefix('purchases')}.date >= '".$start_date."' AND {$this->db->dbprefix('purchases')}.date <= '".$end_date."'";
            $sql_adjustments .= "  {$this->db->dbprefix('adjustments')}.date >= '".$start_date."' AND {$this->db->dbprefix('adjustments')}.date <= '".$end_date."'";
            $sql_transfers .= "  {$this->db->dbprefix('transfers')}.date >= '".$start_date."' AND {$this->db->dbprefix('transfers')}.date <= '".$end_date."'";
        }
        if ($option_id) {
            if ($warehouse_id || $start_date) {
                $sql_sales .= " AND ";
                $sql_adjustments .= " AND ";
                $sql_transfers .= " AND ";
            } else {
                $sql_sales .= " WHERE ";
                $sql_adjustments .= " WHERE ";
                $sql_transfers .= " WHERE ";
            }
            $sql_sales .= "  {$this->db->dbprefix('sale_items')}.option_id = '".$option_id."'";
            $sql_purchases .= " AND {$this->db->dbprefix('purchase_items')}.option_id = '".$option_id."'";
            $sql_adjustments .= "  {$this->db->dbprefix('adjustment_items')}.option_id = '".$option_id."'";
            $sql_transfers .= "  {$this->db->dbprefix('purchase_items')}.option_id = '".$option_id."'";
        }

        $sql = $sql_sales." UNION ALL ".$sql_purchases." UNION ALL ".$sql_adjustments." UNION ALL ".$sql_transfers." ORDER BY registration_date ASC, id ASC";
        // exit($sql);
        return $this->db->query($sql);
    }

    public function get_kardex_report_SI($product, $warehouse_id, $start_date, $end_date, $option_id){

        if ($start_date) {
            $sql_purchases = "SELECT
                                    SUM({$this->db->dbprefix('purchase_items')}.quantity) as balance
                                FROM {$this->db->dbprefix('purchase_items')}
                                    INNER JOIN {$this->db->dbprefix('purchases')} ON
                                                        {$this->db->dbprefix('purchase_items')}.purchase_id = {$this->db->dbprefix('purchases')}.id
                                                        AND {$this->db->dbprefix('purchase_items')}.product_id = {$product}
                                                        AND {$this->db->dbprefix('purchase_items')}.status != 'service_received'
                            WHERE {$this->db->dbprefix('purchases')}.purchase_type = 1 ";

            $sql_sales = "SELECT
                                SUM({$this->db->dbprefix('sale_items')}.quantity * -1) as balance
                            FROM {$this->db->dbprefix('sale_items')}
                                INNER JOIN {$this->db->dbprefix('sales')} ON
                                                        {$this->db->dbprefix('sale_items')}.sale_id = {$this->db->dbprefix('sales')}.id
                                                        AND {$this->db->dbprefix('sale_items')}.product_id = {$product}";

            $sql_adjustments = "SELECT
                                      SUM(IF({$this->db->dbprefix('adjustment_items')}.type = 'addition', {$this->db->dbprefix('adjustment_items')}.quantity, ({$this->db->dbprefix('adjustment_items')}.quantity * -1))) as balance
                                    FROM {$this->db->dbprefix('adjustment_items')}
                                        INNER JOIN {$this->db->dbprefix('adjustments')} ON
                                                        {$this->db->dbprefix('adjustment_items')}.adjustment_id = {$this->db->dbprefix('adjustments')}.id
                                                        AND {$this->db->dbprefix('adjustment_items')}.product_id = {$product}";

            if ($warehouse_id) {

                $sql_transfers = "SELECT
                                        SUM(IF({$this->db->dbprefix('transfers')}.to_warehouse_id = ".$warehouse_id.", {$this->db->dbprefix('purchase_items')}.quantity, ({$this->db->dbprefix('purchase_items')}.quantity * -1))) as balance
                                    FROM {$this->db->dbprefix('purchase_items')}
                                        INNER JOIN {$this->db->dbprefix('transfers')} ON
                                                                    {$this->db->dbprefix('purchase_items')}.transfer_id = {$this->db->dbprefix('transfers')}.id
                                                                    AND {$this->db->dbprefix('purchase_items')}.product_id = {$product}
                                    WHERE ({$this->db->dbprefix('transfers')}.to_warehouse_id = ".$warehouse_id." OR {$this->db->dbprefix('transfers')}.from_warehouse_id = ".$warehouse_id.") ";

                $sql_purchases.=" AND {$this->db->dbprefix('purchase_items')}.warehouse_id = ".$warehouse_id;
                $sql_sales.=" WHERE {$this->db->dbprefix('sale_items')}.warehouse_id = ".$warehouse_id;
                $sql_adjustments.=" WHERE {$this->db->dbprefix('adjustment_items')}.warehouse_id = ".$warehouse_id;
            } else {
                $sql_transfers = "SELECT
                            0 as balance
                        FROM {$this->db->dbprefix('purchase_items')}
                            INNER JOIN {$this->db->dbprefix('transfers')} ON
                                                        {$this->db->dbprefix('purchase_items')}.transfer_id = {$this->db->dbprefix('transfers')}.id
                                                        AND {$this->db->dbprefix('purchase_items')}.product_id = {$product}";
            }

            if ($start_date) {
                if ($warehouse_id) {
                    $sql_sales .= " AND ";
                    $sql_adjustments .= " AND ";
                    $sql_transfers .= " AND ";
                } else {
                    $sql_sales .= " WHERE ";
                    $sql_adjustments .= " WHERE ";
                    $sql_transfers .= " WHERE ";
                }
                $sql_sales .= "  {$this->db->dbprefix('sales')}.date < '".$start_date."'";
                $sql_purchases .= " AND {$this->db->dbprefix('purchases')}.date < '".$start_date."'";
                $sql_adjustments .= "  {$this->db->dbprefix('adjustments')}.date < '".$start_date."'";
                $sql_transfers .= "  {$this->db->dbprefix('transfers')}.date < '".$start_date."'";
            }
            if ($option_id) {
                if ($warehouse_id || $start_date) {
                    $sql_sales .= " AND ";
                    $sql_adjustments .= " AND ";
                    $sql_transfers .= " AND ";
                } else {
                    $sql_sales .= " WHERE ";
                    $sql_adjustments .= " WHERE ";
                    $sql_transfers .= " WHERE ";
                }
                $sql_sales .= "  {$this->db->dbprefix('sale_items')}.option_id = '".$option_id."'";
                $sql_purchases .= " AND {$this->db->dbprefix('purchase_items')}.option_id = '".$option_id."'";
                $sql_adjustments .= "  {$this->db->dbprefix('adjustment_items')}.option_id = '".$option_id."'";
                $sql_transfers .= "  {$this->db->dbprefix('purchase_items')}.option_id = '".$option_id."'";
            }

            $sql = $sql_sales." UNION ALL ".$sql_purchases." UNION ALL ".$sql_adjustments." UNION ALL ".$sql_transfers." ";

            $q = $this->db->query($sql);

            $balance = 0;
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $balance += $row->balance;
                }
            }
            return $balance;
        } else {
            return 0;
        }

    }

    public function get_product_unit_prices($product_id){
        $q = $this->db->select('
                                unit_prices.unit_id,
                                units.name,
                                units.operator,
                                units.operation_value,
                                unit_prices.valor_unitario,
                                unit_prices.status,
                                ')
                      ->join('units', 'units.id = unit_prices.unit_id')
                      ->join('products', 'products.id = unit_prices.id_product')
                      ->where('unit_prices.id_product', $product_id)
                      ->order_by('units.operation_value asc')
                      ->get('unit_prices');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function validate_product_unit_movements($product_id, $punit_id){
        $sl = $this->db->get_where('sale_items', ['product_id' => $product_id, 'product_unit_id' => $punit_id]);
        if ($sl->num_rows() > 0) {
            return false;
        }

        $po = $this->db->get_where('purchase_items', ['product_id' => $product_id, 'product_unit_id' => $punit_id]);
        if ($po->num_rows() > 0) {
            return false;
        }

        $osl = $this->db->get_where('order_sale_items', ['product_id' => $product_id, 'product_unit_id' => $punit_id]);
        if ($osl->num_rows() > 0) {
            return false;
        }

        $qu = $this->db->get_where('quote_items', ['product_id' => $product_id, 'product_unit_id' => $punit_id]);
        if ($qu->num_rows() > 0) {
            return false;
        }

        return true;

    }

    public function delete_product_unit($product_id, $punit_id){
        if ($this->db->delete('unit_prices', ['id_product' => $product_id, 'unit_id' => $punit_id])) {
            return true;
        }
        return false;
    }

    public function getPTRSuggestions($term, $warehouse_id, $biller_id, $destination_biller_id, $search_type, $all = false)
    {
        if (!$all) {
            $limit = $this->Settings->max_num_results_display;
        }
        $biller_id = ($search_type == 1 ? $biller_id : $destination_biller_id) ;
        $this->site->create_temporary_product_billers_assoc($biller_id);
        $this->db->select($this->db->dbprefix('products') . '.id');
        $this->db->select('code');
        $this->db->select($this->db->dbprefix('products') . '.name AS name');
        $this->db->select($this->db->dbprefix('products') . '.avg_cost AS price');
        $this->db->select($this->db->dbprefix('products') . '.cost AS cost');
        $this->db->select($this->db->dbprefix('products') . '.attributes AS attributes');
        $this->db->select($this->db->dbprefix('products') . '.reference AS reference');
        $this->db->select('tax_rate');
        $this->db->select('unit');
        $this->db->select('type');
        $this->db->from('products');
        $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
        $this->db->where("products.type !=", "service");
        if (!$all) {
            $this->db->where(
             "(" . $this->db->dbprefix('products') . ".name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
            concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")");
        }
        $this->db->where('products.discontinued', 0);
        if ($search_type == 1 && $this->Settings->display_all_products == 0) {
            $this->db->where('products.quantity >', 0);
        }
        if ($all && $this->Settings->overselling == 0) {
            // $this->db->where('products.quantity >', 0);
            $this->db->where("( {$this->db->dbprefix('products')}.id IN (SELECT product_id FROM {$this->db->dbprefix('warehouses_products')} WHERE warehouse_id = {$warehouse_id} AND quantity > 0 ) )");
        }
        $this->db->order_by('products.name ASC');
        if (!$all) {
            $this->db->limit($limit);
        }
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $ptr_whs = $this->db->get_where('warehouses_products', ['product_id'=>$row->id]);
                $ptr_whs_arr = [];
                if ($ptr_whs->num_rows() > 0) {
                    foreach (($ptr_whs->result()) as $wh_row) {
                        $ptr_whs_arr[$wh_row->warehouse_id] = $wh_row;
                    }
                }
                $row->wh_data = $ptr_whs_arr;
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return false;
    }

    public function addProductTransformation($data, $data_2, $products_origin, $products_destination, $productNames)
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['origin_document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['origin_reference_no'] = $reference;
        if ($data_2  != false) {
            $data_2['origin_reference_no'] = $reference;
        }
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        $adj_referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($adj_referenceBiller){
            $adj_reference = $adj_referenceBiller;
        }
        $data['reference_no'] = $adj_reference;
        $adj_ref = explode("-", $adj_reference);
        $adj_consecutive = $adj_ref[1];
        if ($this->Settings->overselling == 0) {  // agregamos esta validacion si y solo si las sobreventas estan desactivadas
            $productArrayAux = $this->validateItemAddition($products_origin);
            foreach ($productArrayAux as $product) {
                if (!empty($product['option_id'])) {
                    $wh = $this->db->get_where('warehouses_products_variants',
                                                    [
                                                        'option_id'=> $product['option_id'],
                                                        'product_id'=> $product['product_id'],
                                                        'warehouse_id'=> $product['warehouse_id'],
                                                    ]);
                } else {
                    $wh = $this->db->get_where('warehouses_products',
                                                    [
                                                        'product_id'=> $product['product_id'],
                                                        'warehouse_id'=> $product['warehouse_id'],
                                                    ]);
                }
                if ($wh->num_rows() > 0) {
                    $wh = $wh->row();
                    if ($wh->quantity < $product['quantity']) {
                        $ptr_product_data = $this->site->getProductByID($product['product_id']);
                        $this->session->set_flashdata('error', $ptr_product_data->name.' sin cantidad suficiente');
                        return false;
                    }
                }
            }
        }

        if ($this->db->insert('adjustments', $data)) {
            $adjustment_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['origin_document_type_id'], $consecutive+1);
            $this->site->updateBillerConsecutive($data['document_type_id'], $adj_consecutive+1);
            $totalAdjustmenCost = 0;
            foreach ($products_origin as $product) {
                $totalAdjustmenCost += (Double) $product['avg_cost'] * (Double) $product['quantity'];
                $product['adjustment_id'] = $adjustment_id;
                $this->db->insert('adjustment_items', $product);
                $this->syncAdjustment($product);
            }
            if ($data_2 != false) {
                $adj_referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
                if($adj_referenceBiller){
                    $adj_reference = $adj_referenceBiller;
                }
                $data_2['reference_no'] = $adj_reference;
                $adj_ref = explode("-", $adj_reference);
                $adj_consecutive = $adj_ref[1];
                if ($this->db->insert('adjustments', $data_2)) {
                    $adjustment_id = $this->db->insert_id();
                    $this->site->updateBillerConsecutive($data['document_type_id'], $adj_consecutive+1);
                }
            }
            $totalAdjustmenCost2 = 0;
            foreach ($products_destination as $product) {
                $totalAdjustmenCost2 += (Double) $product['avg_cost'] * (Double) $product['quantity'];
                $product['adjustment_id'] = $adjustment_id;
                $update = $this->db->update('products', array('cost' => $product['avg_cost']), array('id' => $product['product_id']));
                if ($this->Settings->product_variant_per_serial == 1 && !empty($product['serial_no'])) {
                    $v_exist = $this->db->get_where('product_variants', ['name'=>$product['serial_no'], 'product_id' => $product['product_id']]);
                    if ($v_exist->num_rows() > 0) {
                        $v_exist = $v_exist->row();
                        $product['option_id'] =  $v_exist->id;
                    } else {
                        $DB2 = false;
                        $new_id = NULL;
                        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
                            $DB2 = $this->settings_model->connect_new_year_database();
                            if (!$this->settings_model->check_same_id_insert($DB2, 'product_variants')) {
                                $new_id = $this->settings_model->get_new_year_new_id_for_insertion($DB2, 'product_variants');
                            }
                        }
                        $this->db->insert('product_variants', [
                                                        'id' => $new_id,
                                                        'product_id' => $product['product_id'],
                                                        'name' => $product['serial_no'],
                                                        'cost' => $product['avg_cost'],
                                                        'price' => 0,
                                                        'quantity' => $product['quantity'],
                                                        'last_update' => date('Y-m-d H:i:s'),
                                                            ]);
                        if ($DB2) {
                            $DB2->insert('product_variants', [
                                                    'id' => $new_id,
                                                    'product_id' => $product['product_id'],
                                                    'name' => $product['serial_no'],
                                                    'cost' => $product['avg_cost'],
                                                    'price' => 0,
                                                    'quantity' => $product['quantity'],
                                                    'last_update' => date('Y-m-d H:i:s'),
                                            ]);
                        }
                        $product['option_id'] =  $this->db->insert_id();
                        $this->db->insert('warehouses_products_variants', [
                                            'product_id' => $product['product_id'],
                                            'warehouse_id' => $product['warehouse_id'],
                                            'option_id' => $product['option_id'],
                                            ]);
                    }
                }
                $this->db->insert('adjustment_items', $product);
                $adjustment_item_id = $this->db->insert_id();
                if ($product['type'] == 'addition') {
                    $this->site->updateAVCO(array('product_id' => $product['product_id'], 'warehouse_id' => $product['warehouse_id'], 'quantity' => ($product['type'] == 'addition' ? $product['quantity'] : $product['quantity'] * -1), 'cost' => $product['avg_cost'], 'adjustment_item_id' => $adjustment_item_id));
                }
                $this->syncAdjustment($product);
            }
            $data['grand_total'] = $totalAdjustmenCost;
            if ($data_2 != false) {
                $data_2['grand_total'] = $totalAdjustmenCost2;
                $conta = $this->site->wappsiContabilidadAjustes($data, $products_origin, $productNames);
                $conta = $this->site->wappsiContabilidadAjustes($data_2, $products_destination, $productNames);
            } else {
                $data['grand_total'] += $totalAdjustmenCost2;
                $products = array_merge($products_origin, $products_destination);
                $conta = $this->site->wappsiContabilidadAjustes($data, $products, $productNames);
            }
            return true;
        }
        return false;
    }

    public function getProductTransformationByRef($ref)
    {
        $q = $this->db->get_where('adjustments', array('origin_reference_no' => $ref));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductTransformationItemsByRef($ref){
        $q = $this->db->select('products.name, products.code, adjustment_items.*')
                ->join('adjustment_items', 'adjustment_items.adjustment_id = adjustments.id')
                ->join('products', 'products.id = adjustment_items.product_id')
                ->where('adjustments.origin_reference_no', $ref)
                ->get('adjustments');
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[$row->adjustment_id][] = $row;
            }
            return $data;
        }

    }

    public function getProductPreferencesSuggestions($term, $product_id = NULL, $ps = NULL, $preference_category = NULL){
        $this->db->select('preferences.*, preferences_categories.name as prf_cat_name')
                 ->join('preferences_categories', 'preferences_categories.id = preferences.category_id', 'inner');
        if ($product_id) {
            $this->db->join('product_preferences', 'product_preferences.preference_id = preferences.id AND product_preferences.product_id = '.$product_id, 'left');
        }
        $this->db->where('(preferences_categories.name LIKE "%'.$term.'%" OR preferences.name LIKE "%'.$term.'%")');
        if ($product_id) {
            $this->db->where('product_preferences.id IS NULL');
        }
        if ($ps) {
            $this->db->where('preferences.id NOT IN ('.$ps.')');
        }
        if ($preference_category) {
            $this->db->where('preferences.category_id', $preference_category);
        }
        $q =  $this->db->get('preferences');
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_preference_by_id($id){
        $q = $this->db->get_where('preferences', ['id'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function recontabilizarAjuste($adjustment_id)
    {
        $data = $this->getAdjustmentByID($adjustment_id, true);
        $products = $this->getAdjustmentItems($adjustment_id, true);
        $grand_total = 0;
        foreach ($products as $product) {
            $productsNames[$product['product_id']] = $product['product_name'];
            $grand_total += (Double) $product['avg_cost'] * (Double) $product['quantity'];
        }
        $data['grand_total'] = $grand_total;
        return $this->site->wappsiContabilidadAjustes($data, $products, $productsNames);
    }

    public function process_product_recosting($data, $max_days = 1){

        foreach ($data['products_id'] as $product_id) {

            $after_products_recosting = $this->db->where('end_date >', $data['end_date'])
                                                 ->where('product_id', $product_id)
                                                 // ->where('warehouse_id', $data['warehouse_id'])
                                                 ->order_by('end_date', 'desc')
                                                 ->get('products_recosting');

            if ($after_products_recosting->num_rows() > 0) {
                continue;
            }

            $aprox_products_recosting = $this->db->where('DATEDIFF(end_date, "'.$data['start_date'].'") <= '.$max_days)
                                                 ->where('product_id', $product_id)
                                                 // ->where('warehouse_id', $data['warehouse_id'])
                                                 ->order_by('end_date', 'desc')
                                                 ->get('products_recosting');

            if ($aprox_products_recosting->num_rows() == 0) { //NO HAY BITACORAS DE RECOSTEO

                $purchase_items = $this->db->select('purchases.date as purchase_date, purchase_items.*')
                        ->join('purchases', 'purchases.id = purchase_items.purchase_id')
                        ->where('purchases.date >=', $data['start_date'])
                        ->where('purchases.date <=', $data['end_date'])
                        ->where('purchases.purchase_type', 1)
                        ->where('purchase_items.product_id', $product_id)
                        // ->where('purchases.warehouse_id', $data['warehouse_id'])
                        ->order_by('purchases.date', 'DESC')
                        ->get('purchase_items');

                $adjustment_items = $this->db->select('adjustments.date as adjustment_date, adjustment_items.*')
                        ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id')
                        ->where('adjustments.date >=', $data['start_date'])
                        ->where('adjustments.date <=', $data['end_date'])
                        ->where('adjustment_items.product_id', $product_id)
                        // ->where('adjustment_items.warehouse_id', $data['warehouse_id'])
                        ->where('adjustment_items.type', 'addition')
                        ->order_by('adjustments.date', 'DESC')
                        ->get('adjustment_items');

                if ($purchase_items->num_rows() == 0 && $adjustment_items->num_rows() == 0) { // NO TIENE MOVIMIENTOS DE ENTRADA CON FECHA MAYOR AL INICIO DEL RECOSTEO

                    $aprox_purchase_items = $this->db->select('purchases.date as purchase_date, purchase_items.*')
                            ->join('purchases', 'purchases.id = purchase_items.purchase_id')
                            ->where('DATEDIFF('.$this->db->dbprefix('purchases').'.date, "'.$data['start_date'].'") <= '.$max_days)
                            ->where('purchases.date <=', $data['end_date'])
                            ->where('purchases.purchase_type', 1)
                            ->where('purchase_items.product_id', $product_id)
                            // ->where('purchases.warehouse_id', $data['warehouse_id'])
                            ->order_by('purchases.date', 'DESC')
                            ->get('purchase_items');

                    $aprox_adjustment_items = $this->db->select('adjustments.date as adjustment_date, adjustment_items.*')
                            ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id')
                            ->where('DATEDIFF('.$this->db->dbprefix('adjustments').'.date, "'.$data['start_date'].'") <= '.$max_days)
                            ->where('adjustments.date <=', $data['end_date'])
                            ->where('adjustment_items.product_id', $product_id)
                            // ->where('adjustment_items.warehouse_id', $data['warehouse_id'])
                            ->where('adjustment_items.type', 'addition')
                            ->order_by('adjustments.date', 'DESC')
                            ->get('adjustment_items');

                    if ($aprox_purchase_items->num_rows() == 0 && $aprox_adjustment_items->num_rows() == 0) { // NO TIENE MOVIMIENTOS DE ENTRADA CON FECHA APROXIMADA AL INICIO DEL RECOSTEO

                        $product_without_movements_recosting = $this->db->where('product_id', $product_id)
                                    ->order_by('end_date', 'desc')
                                    ->get('products_recosting');
                        if ($product_without_movements_recosting->num_rows() > 0) {
                            $product_without_movements_recosting = $product_without_movements_recosting->row();
                            $product_without_movements_recosting->pwm = true;
                        } else {
                            $product_without_movements_recosting = false;
                        }

                        if ($product_without_movements_recosting && $product_without_movements_recosting->pwm == true && $this->products_model->productHasMovements($product_id)) {
                            $product_without_movements_recosting->pwm = false;
                        }
                        if (!$this->recosting_validate_prev_movements($data['start_date'], $product_id)) { //NO EXISTEN MOVIMIENTOS ANTES DE LA FECHA DE INICIO
                            $this->recosting_calculate_avg_cost($data['start_date'], $data['end_date'], $product_id, $data['show_recosting_documentation'], $product_without_movements_recosting);
                        } else {
                            continue;
                        }

                    } else if ($aprox_purchase_items->num_rows() > 0 || $aprox_adjustment_items->num_rows() > 0) {
                        $aprox_start_date = NULL;
                        if ($aprox_purchase_items->num_rows() > 0) {
                            $aprox_result = $aprox_purchase_items->last_row();
                            $aprox_start_date = $aprox_result->purchase_date;
                        }
                        if ($aprox_adjustment_items->num_rows() > 0) {
                            $aprox_result = $aprox_adjustment_items->last_row();
                            if ($aprox_start_date == NULL || $aprox_result->adjustment_date < $aprox_start_date) {
                                $aprox_start_date = $aprox_result->adjustment_date;
                            }
                        }

                        if (!$this->recosting_validate_prev_movements($aprox_start_date, $product_id)) { //NO EXISTEN MOVIMIENTOS ANTES DE LA FECHA DE INICIO
                            $this->recosting_calculate_avg_cost($aprox_start_date, $data['end_date'], $product_id, $data['show_recosting_documentation']);
                        } else {
                            continue;
                        }

                    }

                } else if ($purchase_items->num_rows() > 0 || $adjustment_items->num_rows() > 0) {
                    $start_date = NULL;
                    if ($purchase_items->num_rows() > 0) {
                        $result = $purchase_items->last_row();
                        $start_date = $result->purchase_date;
                    }
                    if ($adjustment_items->num_rows() > 0) {
                        $result = $adjustment_items->last_row();
                        if ($start_date == NULL || $result->adjustment_date < $start_date) {
                            $start_date = $result->adjustment_date;
                        }
                    }
                    if (!$this->recosting_validate_prev_movements($start_date, $product_id)) { //NO EXISTEN MOVIMIENTOS ANTES DE LA FECHA DE INICIO
                        $this->recosting_calculate_avg_cost($start_date, $data['end_date'], $product_id, $data['show_recosting_documentation']);
                    } else {
                        continue;
                    }

                }

            } else if ($aprox_products_recosting->num_rows() > 0) {

                $aprox_start_date = NULL;
                if ($aprox_products_recosting->num_rows() > 0) {
                    $aprox_result = $aprox_products_recosting->last_row();
                    $aprox_start_date = $aprox_result->end_date;
                    $aprox_result->pwm = !$this->products_model->productHasMovements($product_id) ? true : false;
                    $this->recosting_calculate_avg_cost($aprox_start_date, $data['end_date'], $product_id, $data['show_recosting_documentation'], $aprox_result);
                } else {
                    continue;
                }
            }
        }
        return true;
    }

    public function recosting_validate_prev_movements($start_date, $product_id){
        $sl = $this->db->select('sale_items.*')
                 ->join('sales', 'sales.id = sale_items.sale_id')
                 ->where('sales.date <', $start_date)
                 // ->where('sales.warehouse_id', $warehouse_id)
                 ->where('sale_items.product_id', $product_id)
                 ->get('sale_items');
        $po = $this->db->select('purchase_items.*')
                 ->join('purchases', 'purchases.id = purchase_items.purchase_id')
                 ->where('purchases.date <', $start_date)
                 ->where('purchases.purchase_type', 1)
                 // ->where('purchases.warehouse_id', $warehouse_id)
                 ->where('purchase_items.product_id', $product_id)
                 ->get('purchase_items');
        $qa = $this->db->select('adjustment_items.*')
                 ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id')
                 ->where('adjustments.date <', $start_date)
                 // ->where('adjustment_items.warehouse_id', $warehouse_id)
                 ->where('adjustment_items.product_id', $product_id)
                 ->get('adjustment_items');
        if ($sl->num_rows() > 0 || $po->num_rows() > 0 || $qa->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function recosting_calculate_avg_cost($start_date, $end_date, $product_id, $show_documentation = false, $bitacora_data = NULL){
        $movements_array = [];
        $txt_without_movements = false;
        $sl = $this->db->select('sales.date, sales.reference_no, sales.return_sale_total, sales.sale_id as return_sale_id, sales.document_type_id, sale_items.*')
                 ->join('sales', 'sales.id = sale_items.sale_id')
                 ->where('sales.date >=', $start_date)
                 ->where('sales.date <=', $end_date)
                 // ->where('sales.warehouse_id', $warehouse_id)
                 ->where('sale_items.product_id', $product_id)
                 ->order_by('date asc')
                 ->get('sale_items');
        if ($sl->num_rows() > 0) {
            foreach (($sl->result()) as $row) {
                $movements_array[]= $row;
            }
        }
        $po = $this->db->select('purchases.date as purchase_date, purchases.reference_no, purchases.document_type_id, purchase_items.*')
                 ->join('purchases', 'purchases.id = purchase_items.purchase_id')
                 ->where('purchases.date >=', $start_date)
                 ->where('purchases.date <=', $end_date)
                 ->where('purchases.purchase_type', 1)
                 // ->where('purchases.warehouse_id', $warehouse_id)
                 ->where('purchase_items.product_id', $product_id)
                 ->get('purchase_items');
        if ($po->num_rows() > 0) {
            foreach (($po->result()) as $row) {
                $row->date = $row->purchase_date;
                $movements_array[]= $row;
            }
        }
        $tf = $this->db->select('transfers.date, transfers.reference_no as reference_no, transfers.document_type_id, purchase_items.*')
                 ->join('transfers', 'transfers.id = purchase_items.transfer_id')
                 ->where('transfers.date >=', $start_date)
                 ->where('transfers.date <=', $end_date)
                 // ->where('purchase_items.warehouse_id', $warehouse_id)
                 ->where('purchase_items.product_id', $product_id)
                 ->get('purchase_items');
        if ($tf->num_rows() > 0) {
            foreach (($tf->result()) as $row) {
                $movements_array[]= $row;
            }
        }
        $tfout = $this->db->select('transfers.date, transfers.reference_no as reference_no, transfers.document_type_id, purchase_items.*')
                 ->join('transfers', 'transfers.id = purchase_items.transfer_id')
                 ->where('transfers.date >=', $start_date)
                 ->where('transfers.date <=', $end_date)
                 // ->where('transfers.from_warehouse_id', $warehouse_id)
                 ->where('purchase_items.product_id', $product_id)
                 ->get('purchase_items');
        if ($tf->num_rows() > 0) {
            foreach (($tf->result()) as $row) {
                $movements_array[]= $row;
            }
        }
        $qa = $this->db->select('adjustments.date, adjustments.reference_no, adjustments.document_type_id, adjustments.origin_document_type_id, adjustments.origin_reference_no, adjustment_items.*')
                 ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id')
                 ->where('adjustments.date >=', $start_date)
                 ->where('adjustments.date <=', $end_date)
                 // ->where('adjustment_items.warehouse_id', $warehouse_id)
                 ->where('adjustment_items.product_id', $product_id)
                 ->get('adjustment_items');

        if ($qa->num_rows() > 0) {
            foreach (($qa->result()) as $row) {
                $movements_array[]= $row;
            }
        }
        $movements_order = [];
        foreach ($movements_array as $marr_row) {
            $movements_order[] = $marr_row->date;
        }
        $product = $this->getProductByID($product_id);
        array_multisort($movements_order, SORT_ASC, $movements_array);
        $total_qty = $bitacora_data ? $bitacora_data->calculated_quantity : 0;
        $total_avg_cost = $bitacora_data ? $bitacora_data->calculated_avg_cost : 0;
        $total_avg_net_cost = $this->sma->remove_tax_from_amount($product->tax_rate, $total_avg_cost);
        $last_cost = $total_avg_cost;
        $last_net_cost = $total_avg_net_cost;
        $last_qty = $total_qty;
        $txt_without_movements = ($bitacora_data && isset($bitacora_data->pwm) ? $bitacora_data->pwm : false) ? "(".$product->code.") ".$product->name.", " : "";
        $sql_SI_update = "UPDATE ".$this->db->dbprefix('sale_items')." SET avg_net_unit_cost = CASE ";
        $sql_SI_update_INS = "";
        $sql_net_costing_update = "UPDATE ".$this->db->dbprefix('costing')." SET purchase_net_unit_cost = CASE ";
        $sql_net_costing_update_INS = "";
        $sql_costing_update = "UPDATE ".$this->db->dbprefix('costing')." SET purchase_unit_cost = CASE ";
        $sql_costing_update_INS = "";
        $html_updated = "
                        <p>Rango fechas : ".$start_date." hasta ".$end_date."</p>
                        <p>Producto : ".$product->name." (".$product->code.")</p>
                        <table border='1'>
                            <tr>
                                <th>Fecha</th>
                                <th>Tipo de documento</th>
                                <th>Documento</th>
                                <th>Cant. Entrada</th>
                                <th>Cant. Salida</th>
                                <th>Cant. Saldo</th>
                                <th>Ultimo costo</th>
                                <th>Costo promedio</th>
                            </tr>";
        // $this->sma->print_arrays($movements_array);
        $sales_returned_net_avg_cost = [];
        foreach ($movements_array as $marr_row) {
            $dt_data = $this->site->getDocumentTypeById($marr_row->document_type_id);
            if (isset($marr_row->purchase_id) || (isset($marr_row->adjustment_id) && $marr_row->type == 'addition') || (isset($marr_row->sale_id) && $marr_row->quantity < 0)) {
                $consumption_purchase_tax = ($product->consumption_purchase_tax > 0 ? $product->consumption_purchase_tax : 0);
                if ($marr_row->quantity > 0) {
                    $last_cost = isset($marr_row->unit_cost) ? $marr_row->unit_cost - $consumption_purchase_tax : $marr_row->avg_cost;
                    $last_net_cost = isset($marr_row->net_unit_cost) ? $marr_row->net_unit_cost : $marr_row->avg_cost;
                }
                if ((isset($marr_row->sale_id) && $marr_row->quantity < 0)) {
                    $marr_row->quantity = ($marr_row->quantity * -1);
                    $pdata = $this->site->getProductByID($marr_row->product_id);
                    $tax_details = $this->site->getTaxRateByID($pdata->tax_rate);
                    $rsavg_net_cost = isset($sales_returned_net_avg_cost[$marr_row->return_sale_id]) ? $sales_returned_net_avg_cost[$marr_row->return_sale_id] : $marr_row->avg_net_unit_cost;
                    $rsavg_tax = $this->site->calculateTax($pdata, $tax_details, $rsavg_net_cost, NULL, 1);
                    $marr_row->unit_cost = ($rsavg_net_cost + $rsavg_tax['amount']) > 0 ? ($rsavg_net_cost + $rsavg_tax['amount']) : $total_avg_cost;
                    $sql_SI_update .= " WHEN id = ".$marr_row->id." THEN ".$rsavg_net_cost." ";
                    $sql_SI_update_INS .= $marr_row->id.", ";
                    $consumption_purchase_tax = 0;
                }
                $last_qty = $marr_row->quantity;
                $actual_total = $this->sma->formatDecimal($total_avg_cost * $total_qty);
                $prev_total_avg_cost = $total_avg_cost;
                // $addition_total = ($marr_row->quantity * $this->sma->formatDecimal(isset($marr_row->unit_cost) ? $marr_row->unit_cost - ($consumption_purchase_tax) : $marr_row->avg_cost));
                $addition_total = ($marr_row->quantity * $this->sma->formatDecimal(isset($marr_row->unit_cost) ? $marr_row->unit_cost : $marr_row->avg_cost));
                $tt_qty = ($total_qty > 0 ? $total_qty : 0) + $marr_row->quantity;
                $tt_cost = ($actual_total > 0 ? $actual_total : 0) + ($addition_total);
                $tt_avg_cost = $this->sma->formatDecimal($tt_cost) / $tt_qty;
                $total_avg_cost = $this->sma->formatDecimal($tt_avg_cost);
                if (!is_numeric($total_avg_cost)) {
                    $total_avg_cost = $prev_total_avg_cost;
                }
                $total_qty += $marr_row->quantity;
                $total_avg_net_cost = $this->sma->remove_tax_from_amount($product->tax_rate, $total_avg_cost);
                $html_updated .= " <tr>
                                        <th style='text-align:left;'>".$marr_row->date."</th>
                                        <th style='text-align:left;'>".($dt_data ? $dt_data->nombre : 'Error')."</th>
                                        <th style='text-align:left;'>".$marr_row->reference_no."</th>
                                        <th style='text-align:right;'>".$this->sma->formatQuantity($marr_row->quantity > 0 ? $marr_row->quantity : 0)."</th>
                                        <th style='text-align:right;'>".$this->sma->formatQuantity($marr_row->quantity < 0 ? ($marr_row->quantity * -1) : 0)."</th>
                                        <th style='text-align:right;'>".$this->sma->formatQuantity($total_qty)."</th>
                                        <th style='text-align:right;'>".$this->sma->formatMoney(isset($marr_row->unit_cost) ? $marr_row->unit_cost - $consumption_purchase_tax : $marr_row->avg_cost)."</th>
                                        <th style='text-align:right;'>".$this->sma->formatMoney($total_avg_cost)."</th>
                                    </tr>";
                if (isset($marr_row->purchase_id)) {
                    $this->db->update('purchase_items', ['calculated_avg_cost'=>$total_avg_cost], ['id'=>$marr_row->id]);
                }
                if (isset($marr_row->adjustment_id)) {
                    $this->db->update('adjustment_items', ['calculated_avg_cost'=>$total_avg_cost], ['id'=>$marr_row->id]);
                }
            } else if ((isset($marr_row->adjustment_id) && $marr_row->type == 'subtraction')) {
                $total_qty -= $marr_row->quantity;
                $html_updated .= " <tr>
                                <td style='text-align:left;'>".$marr_row->date."</td>
                                        <td style='text-align:left;'>".($dt_data ? $dt_data->nombre : 'Error')."</td>
                                <td style='text-align:left;'>".$marr_row->reference_no."</td>
                                <td style='text-align:right;'>0</td>
                                <td style='text-align:right;'>".$this->sma->formatQuantity($marr_row->quantity)."</td>
                                <td style='text-align:right;'>".$this->sma->formatQuantity($total_qty)."</td>
                                <td style='text-align:right;'>0</td>
                                <td style='text-align:right;' colspan='3'>".$this->sma->formatMoney($marr_row->avg_cost)."</td>
                            </tr>";
                if ($marr_row->origin_document_type_id > 0 && $marr_row->type == 'subtraction') {
                    $qa_script = "UPDATE {$this->db->dbprefix('adjustment_items')} AI
                                        INNER JOIN
                                    {$this->db->dbprefix('adjustments')} A ON A.id = AI.adjustment_id
                                        INNER JOIN
                                    (SELECT
                                        SUM(ADJ_IN.quantity) AS total_adjustment_quantity,
                                            ADJ_IN.product_id,
                                            ADJ_IN.adjustment_id
                                    FROM
                                        {$this->db->dbprefix('adjustment_items')} ADJ_IN
                                    WHERE
                                        ADJ_IN.type = 'addition'
                                    GROUP BY ADJ_IN.adjustment_id) ADJ_IN ON ADJ_IN.adjustment_id = AI.adjustment_id
                                        LEFT JOIN
                                    (SELECT
                                        SUM((COALESCE(ADJ_OUT.quantity, 0)) * (COALESCE(ADJ_OUT.avg_cost, 0))) AS total_adjustment_cost,
                                            ADJ_OUT.adjustment_id
                                    FROM
                                        {$this->db->dbprefix('adjustment_items')} ADJ_OUT
                                    WHERE
                                        ADJ_OUT.type = 'subtraction'
                                        ".($marr_row->option_id ? "AND ADJ_OUT.option_id != {$marr_row->option_id}" : "")."
                                        AND ADJ_OUT.adjustment_id = {$marr_row->adjustment_id}
                                    GROUP BY ADJ_OUT.adjustment_id) ADJ_OUT ON ADJ_OUT.adjustment_id = {$marr_row->adjustment_id}
                                SET
                                    avg_cost = (((({$total_avg_cost} * {$marr_row->quantity}) + (COALESCE(ADJ_OUT.total_adjustment_cost, 0))) * (AI.quantity / ADJ_IN.total_adjustment_quantity)) / AI.quantity),
                                    adjustment_cost = (((({$total_avg_cost} * {$marr_row->quantity}) + (COALESCE(ADJ_OUT.total_adjustment_cost, 0))) * (AI.quantity / ADJ_IN.total_adjustment_quantity)) / AI.quantity)
                                WHERE
                                    A.origin_reference_no = '{$marr_row->origin_reference_no}' AND A.id != {$marr_row->adjustment_id};";
                    $html_updated .=$qa_script."</br>";
                    if ($this->db->query($qa_script)) {
                        $this->db->update('adjustment_items', ['avg_cost'=>$total_avg_cost, 'adjustment_cost'=>$total_avg_cost], ['id'=>$marr_row->id]);
                    } else {
                        exit('error');
                    }
                }

            } else if (isset($marr_row->sale_id) && $marr_row->quantity > 0) {
                $total_avg_net_cost = $this->sma->remove_tax_from_amount($product->tax_rate, $total_avg_cost);
                $sql_SI_update .= " WHEN id = ".$marr_row->id." THEN ".$total_avg_net_cost." ";
                $sql_SI_update_INS .= $marr_row->id.", ";
                $sql_net_costing_update .= " WHEN sale_item_id = ".$marr_row->id." THEN ".$total_avg_net_cost." ";
                $sql_net_costing_update_INS .= $marr_row->id.", ";
                $sql_costing_update .= " WHEN sale_item_id = ".$marr_row->id." THEN ".$total_avg_cost." ";
                $sql_costing_update_INS .= $marr_row->id.", ";
                $total_qty -= $marr_row->quantity;
                $pdata = $this->site->getProductByID($marr_row->product_id);
                $tax_details = $this->site->getTaxRateByID($pdata->tax_rate);
                $mrow_cost = $this->site->calculateTax($pdata, $tax_details, $marr_row->avg_net_unit_cost, null, 1);
                $marr_row->avg_unit_cost = $marr_row->avg_net_unit_cost + $mrow_cost['amount'];
                if ($marr_row->return_sale_total != 0) {
                    $sales_returned_net_avg_cost[$marr_row->sale_id] = ($total_avg_net_cost > 0 ? $total_avg_net_cost : 0);
                }
                $html_updated .= " <tr>
                                <td style='text-align:left;'>".$marr_row->date."</td>
                                        <td style='text-align:left;'>".($dt_data ? $dt_data->nombre : 'Error')."</td>
                                <td style='text-align:left;'>".$marr_row->reference_no."</td>
                                <td style='text-align:right;'>0</td>
                                <td style='text-align:right;'>".$this->sma->formatQuantity($marr_row->quantity)."</td>
                                <td style='text-align:right;'>".$this->sma->formatQuantity($total_qty)."</td>
                                <td style='text-align:right;'>0</td>
                                <td style='text-align:right;'>".$this->sma->formatMoney($total_avg_cost)."</td>
                            </tr>";
            }
        }
        if (!empty($sql_SI_update_INS)) {
            $sql_SI_update_INS = trim($sql_SI_update_INS, ", ");
            $html_updated .= "</table>";
            $sql_SI_update .= " ELSE id END WHERE id IN (".$sql_SI_update_INS.")";
            $sql_net_costing_update .= " ELSE sale_item_id END WHERE sale_item_id IN (".$sql_SI_update_INS.")";
            $sql_costing_update .= " ELSE sale_item_id END WHERE sale_item_id IN (".$sql_SI_update_INS.")";
            $this->db->query($sql_SI_update);
            $this->db->query($sql_net_costing_update);
            $this->db->query($sql_costing_update);
        }
        $this->db->insert('products_recosting',
                [
                    'product_id' => $product_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'calculated_avg_cost' => $total_avg_cost,
                    'calculated_quantity' => $total_qty,
                    'updated_by_csv' => 0,
                ]);
        if ((date('Y-m-d', strtotime($end_date)) == date('Y-m-d')) || count($movements_array) == 0) {
            $this->db->update('products', ['avg_cost' => ($this->Settings->tax_method == 1 ? $total_avg_net_cost : $total_avg_cost), 'cost' => ($this->Settings->tax_method == 1 ? $last_net_cost : $last_cost)], ['id'=>$product_id]);
            $this->db->update('warehouses_products', ['avg_cost' => $total_avg_cost, 'last_update' => date('Y-m-d H:i:s')], ['product_id'=>$product_id]);
            // $this->db->update('warehouses_products_variants', ['avg_cost' => $total_avg_net_cost], ['product_id'=>$product_id]);
        }
        if ($txt_without_movements) {
            $this->session->set_userdata('products_without_movements', trim($txt_without_movements, ", "));
        }
        if ($show_documentation) {
            exit(var_dump($html_updated));
        }
    }

    public function insert_recosting_register_by_csv($recosting){
        foreach ($recosting as $rec) {
            if (!$this->db->insert('products_recosting', $rec)) {
                return false;
            }
        }
        return true;
    }

    public function validate_recosting_register_exist($recosting_row){
        $q = $this->db->select('products_recosting.*')
                ->where('end_date >=', $recosting_row['end_date'])
                ->where('product_id', $recosting_row['product_id'])
                ->where('warehouse_id', $recosting_row['warehouse_id'])
                ->get('products_recosting');
        if ($q->num_rows() > 0) {
            return false;
        }
        return true;
    }

    public function fix_products_cost($data){

        // ARREGLA AJUSTES DE ENTRADA CON EL ÚLTIMO COSTO DE LA COMPRA QUE ENCUENTRA O CON EL DEL EXCEL SI NO ENCONTRO COMPRAS, SI SE DETECTA QUE ES DE ENTRADA POR SER DE TRANSFORMACIÓN, ARREGLA LOS DE SALIDA Y PRORRATEA SEGÚN LOS DE ENTRADA
        foreach ($data as $row) {
            $product = $row['product'];
            $cost = $row['cost'];
            $start_date = $row['start_date'];
            $end_date = $row['end_date'];
            $document_types = $row['document_types'];
            $prioritize_last_cost = $row['prioritize_last_cost'];
            $affect_until_first_purchase = TRUE;
            $affect_adjustment_costs = FALSE;
            $purchases = false;
            $movements = [];
            $q = $this->db->select('purchases.*, purchase_items.real_unit_cost')
                         ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'inner')
                         ->where('purchase_items.product_id', $product->id)
                         ->where('purchases.date >=', $start_date)
                         ->where('purchases.date <=', $end_date)
                         ->where('purchases.status !=', 'returned')
                         ->order_by('purchases.date', 'asc')
                         ->group_by('purchases.id')
                         ->get('purchase_items');
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $data) {
                    $purchases[] = $data;
                }
            }
            unset($doit);
            $scripts = "";
            if ($prioritize_last_cost && $purchases) {
                // $this->sma->print_arrays($purchases);
                foreach ($purchases as $pkey => $purchase) {
                    if (!isset($doit)) {
                       $this->db->select('adjustments.*, adjustment_items.*, adjustment_items.id as adjustment_item_id')
                                ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id', 'inner')
                                ->where('adjustments.date >= "'.$start_date.'"')
                                ->where('adjustments.date < "'.$purchase->date.'"')
                                ->where('adjustment_items.product_id', $product->id);
                        // foreach ($document_types as $dt_id) {
                            $this->db->where_in('adjustments.document_type_id', $document_types);
                        // }
                        $q = $this->db->from('adjustment_items')->get();
                        $doit = true;
                        if ($q->num_rows() > 0 && $doit) {
                            foreach (($q->result()) as $row) {
                                if ($row->origin_document_type_id > 0 && $row->type == 'subtraction') {
                                    //proceso removido a recosteo de promedio
                                } else if ($row->origin_document_type_id == 0 || $row->origin_document_type_id == NULL) {
                                    $this->db->update('adjustment_items', ['avg_cost'=>$cost, 'adjustment_cost'=>$cost], ['id'=>$row->id]);
                                }
                            }
                        } else {
                            // exit('SIN AJUSTES');
                        }
                    }

                    $this->db->select('adjustments.*, adjustment_items.*, adjustment_items.id as adjustment_item_id')
                                ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id', 'inner')
                                ->where('adjustments.date >= "'.$purchase->date.'"')
                                ->where('adjustments.date <= "'.(isset($purchases[$pkey+1]->date) ? $purchases[$pkey+1]->date : $end_date).'"')
                                ->where('adjustment_items.product_id', $product->id);
                    // foreach ($document_types as $dt_id) {
                        $this->db->where_in('adjustments.document_type_id', $document_types);
                    // }
                    $q = $this->db->from('adjustment_items')->get();
                    $doit = true;
                    if ($q->num_rows() > 0 && $doit) {
                        foreach (($q->result()) as $row) {
                            if ($row->origin_document_type_id > 0 && $row->type == 'subtraction') {
                                    //proceso removido a recosteo de promedio
                            } else if ($row->origin_document_type_id == 0 || $row->origin_document_type_id == NULL) {
                                $this->db->update('adjustment_items', ['avg_cost'=>$purchase->real_unit_cost, 'adjustment_cost'=>$purchase->real_unit_cost], ['id'=>$row->id]);
                            }
                        }
                    } else {
                        // exit('SIN AJUSTES');
                    }
                    // $end_date = $purchase->date;
                }
            } else {
                $this->db->select('adjustments.*, adjustment_items.*, adjustment_items.id as adjustment_item_id')
                            ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id', 'inner')
                            ->where('adjustments.date >= "'.$start_date.'"')
                            ->where('adjustments.date <= "'.$end_date.'"')
                            ->where('adjustment_items.product_id', $product->id);
                // foreach ($document_types as $dt_id) {
                    $this->db->where_in('adjustments.document_type_id', $document_types);
                // }
                $q = $this->db->from('adjustment_items')->get();
                $doit = true;
                if ($q->num_rows() > 0 && $doit) {
                    foreach (($q->result()) as $row) {
                        if ($row->origin_document_type_id > 0 && $row->type == 'subtraction') {
                            //proceso removido a recosteo de promedio
                        } else if ($row->origin_document_type_id == 0 || $row->origin_document_type_id == NULL) {
                            $this->db->update('adjustment_items', ['avg_cost'=>$cost, 'adjustment_cost'=>$cost], ['id'=>$row->id]);
                        }
                    }
                } else {
                    // exit('SIN AJUSTES');
                }
            }

            $scripts.=" ///////////////////////// ";
            $first_purchase = false;
            $q = $this->db->select('purchases.*, purchase_items.real_unit_cost')
                         ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'inner')
                         ->where('purchase_items.product_id', $product->id)
                         ->where('purchases.status !=', 'returned')
                         ->where('purchases.purchase_type', 1)
                         ->where('purchases.date >', $end_date)
                         ->order_by('purchases.id', 'asc')
                         ->limit(1)
                         ->get('purchase_items');
            if ($q->num_rows() > 0) {
                    $first_purchase = $q->row();
            }
            if ($affect_until_first_purchase && $first_purchase && $doit) {
                $this->db->select('adjustments.*, adjustment_items.*, adjustment_items.id as adjustment_item_id')
                    ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id', 'inner')
                    ->where('adjustments.date > "'.$end_date.'"')
                    ->where('adjustments.date <= "'.$first_purchase->date.'"')
                    ->where('adjustment_items.product_id', $product->id);
                // foreach ($document_types as $dt_id) {
                    $this->db->where_in('adjustments.document_type_id', $document_types);
                // }
                $qa = $this->db->from('adjustment_items')->get();
                if ($qa->num_rows() > 0) {
                    foreach (($qa->result()) as $row) {
                        if ($row->origin_document_type_id > 0 && $row->type == 'subtraction') {
                            //proceso removido a recosteo de promedio
                        } else if ($row->origin_document_type_id == 0 || $row->origin_document_type_id == NULL) {
                            $this->db->update('adjustment_items', ['avg_cost'=>$cost, 'adjustment_cost'=>$cost], ['id'=>$row->id]);
                        }
                    }
                }
            }
            $scripts.=" ///////////////////////// ";
            if ($affect_adjustment_costs && $first_purchase) {
                $movements[] = $first_purchase;
                $adjustments = [];
                $q = $this->db->select('purchases.*, purchase_items.real_unit_cost')
                             ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'inner')
                             ->where('purchase_items.product_id', $product->id)
                             ->where('purchases.status !=', 'returned')
                             ->where('purchases.purchase_type', 1)
                             ->where('purchases.date >', $first_purchase->date)
                             ->order_by('purchases.id', 'asc')
                             ->get('purchase_items');
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row) {
                        $movements[] = $row;
                    }
                }
                $this->db->select('adjustments.*, adjustment_items.*, adjustment_items.id as adjustment_item_id')
                    ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id', 'inner')
                    ->where('adjustments.date > "'.$first_purchase->date.'"')
                    ->where('adjustment_items.product_id', $product->id);
                // foreach ($document_types as $dt_id) {
                    $this->db->where_in('adjustments.document_type_id', $document_types);
                // }
                $qa = $this->db->from('adjustment_items')->get();
                if ($qa->num_rows() > 0) {
                    foreach (($qa->result()) as $row) {
                        $movements[] = $row;
                    }
                }
                $movements_order = [];
                foreach ($movements as $marr_row) {
                    $movements_order[] = $marr_row->date;
                }
                array_multisort($movements_order, SORT_ASC, $movements);
                $last_cost = false;
                foreach ($movements as $row) {
                    if ((isset($row->supplier_id) && $last_cost == false) || (isset($row->supplier_id) && $last_cost != $row->real_unit_cost)) {

                        $last_cost = $row->real_unit_cost;

                    } else if (!isset($row->supplier_id) && $last_cost != false) {

                        if ($row->origin_document_type_id > 0 && $row->type == 'subtraction') {
                            //proceso removido a recosteo de promedio
                        } else if ($row->origin_document_type_id == 0 || $row->origin_document_type_id == NULL) {
                            $this->db->update('adjustment_items', ['avg_cost'=>$last_cost, 'adjustment_cost'=>$last_cost], ['id'=>$row->id]);
                        }

                    } else if (!isset($row->supplier_id) && $last_cost == false) {
                        # code...
                    }
                }
                if ($last_cost != false) {
                    $this->db->update('products', ['cost'=>$last_cost], ['id'=> $product->id]);
                }
            }
        }
        // exit(var_dump($scripts));
        return true;
    }

    public function getPackingOrderItems($packing_id){
            $this->db
            ->select("
                        por_pfinished.product_id,
                        por_pfinished.option_id,
                        ciq_pfinished_complete_packing.complete_packing_quantity_completed as quantity,
                    ")
            ->from('packing')
            ->join('users', 'users.id=packing.created_by', 'left')
            ->join('packing_detail', 'packing_detail.packing_id=packing.id', 'left')
            ->join('companies', 'companies.id=packing.supplier_id', 'left')
            ->join('production_order_detail', 'production_order_detail.id=packing_detail.production_order_pid', 'left')
            ->join('production_order_detail por_pfinished', 'por_pfinished.id=production_order_detail.production_order_pfinished_item_id', 'left')
            ->join('products pfinished_product', 'pfinished_product.id=por_pfinished.product_id', 'left')
            ->join("(SELECT
                            production_order_pfinished_item_id,
                            packing_id,
                            MIN(packing_composition_complete_quantity) AS complete_packing_quantity,
                            MIN(packing_composition_complete_quantity_completed) AS complete_packing_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (
                            SELECT
                                POD.production_order_id,
                                POD.product_id,
                                CD.packing_id,
                                POD.production_order_pfinished_item_id,
                                CD.assemble_id_pid,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_packing_quantity,
                                (SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS packing_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_packing_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS packing_composition_complete_quantity_completed
                            FROM
                                {$this->db->dbprefix('packing_detail')} CD
                            INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                            INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                            INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                            INNER JOIN (SELECT
                                COUNT(CI.id) AS num_composition_items, CI.product_id
                            FROM
                                {$this->db->dbprefix('combo_items')} CI
                            GROUP BY CI.product_id
                        ) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY packing_id, product_id) AS packing_table
                        GROUP BY packing_id) ciq_pfinished_complete_packing", "ciq_pfinished_complete_packing.production_order_pfinished_item_id = por_pfinished.id AND ciq_pfinished_complete_packing.packing_id = packing.id ", "left")
            ->join('adjustments', 'adjustments.origin_reference_no = packing.id ', 'left')
            ->join("(
                    SELECT AI.id,  AI.adjustment_id, AI.product_id, AI.warehouse_id, SUM(AI.quantity) AS AI_qty FROM {$this->db->dbprefix('adjustment_items')} AI
                    GROUP BY AI.adjustment_id, AI.product_id
                    ) AI", 'AI.adjustment_id = adjustments.id AND AI.product_id = por_pfinished.product_id', 'left')
            ->join('warehouses', 'warehouses.id = AI.warehouse_id','left')
            ->where("packing.id", $packing_id)
            ->group_by("packing.id");
        $q = $this->db->get('assemble_detail');
        if ($q->num_rows() > 0) {
            $data = [];
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function inactivateProduct($id, $status)
    {
        if ($this->db->update('products', ['discontinued' => $status], ['id' => $id])) {
            return true;
        }
        return FALSE;
    }

    public function get_product_option_by_id($product_option_id){
        $q = $this->db->get_where('product_variants', ['id' => $product_option_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_products_billers($product_id){
        $q = $this->db->get_where('products_billers', ['product_id' => $product_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $pb) {
                $billers_selected[$pb->biller_id] = 1;
            }
            return $billers_selected;
        }
        return false;
    }

    public function getProductFormat(){
        $this->db->select(" categories.code AS categoria,
                            products.code AS codigo,
                            products.id AS id,
                            products.name AS nombre,
                            COALESCE((".$this->db->dbprefix('warehouses_products').".quantity), 0) AS cantidad,
                            units.name AS unidad,
                            COALESCE(".$this->db->dbprefix('unit_prices').".cantidad, 0) AS factor,
                            unit_prices.unit_id AS idUnidad "
                            )
                ->join('warehouses_products', 'products.id = warehouses_products.product_id', 'left')
                ->join('unit_prices', 'products.id = unit_prices.id_product', 'left')
                ->join('units', 'units.id = unit_prices.unit_id', 'left')
                ->join('categories', 'products.category_id = categories.id', 'inner')
                ->group_by('products.id, unit_prices.id')
                ->order_by('products.code ASC, units.operation_value DESC');
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            $data = [];
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getNameUnitMinor($id){
        $this->db->select("units.name");
        $this->db->join('products', 'products.unit = units.id ', 'inner');
        $this->db->where("products.id", $id);
        $q = $this->db->get('units');
        $data = '';
        if ($q->num_rows() > 0) {
            foreach (($q->row()) as $row) {
                $data = $row;
            }
            return $data;
        }
        return $data;
    }

    public function get_preferences_categories(){
        $q = $this->db->get('preferences_categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_variants_suggestions($term, $limit = 10)
    {
        $this->db->select("name as id, name as text, name as value", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%'
        OR name LIKE '%" . $term . "%') ");
        $q = $this->db->get('variants', $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function get_variant_by_name($name){
        $q = $this->db->get_where('variants', ['name'=>$name])->row();
        return $q;
    }

    public function get_last_user_edition($id_product){
        $last_user_activity = $this->db->select('users.*')
                                       ->where('record_id', $id_product)
                                       ->where('table_name', 'products')
                                       ->order_by('user_activities.id', 'desc')
                                       ->join('users', 'users.id = user_activities.user_id')
                                       ->limit(1)
                                       ->get('user_activities');
        if ($last_user_activity->num_rows() > 0) {
            return $last_user_activity->row();
        } else {
            return false;
        }
    }

    public function set_featuring($id)
    {
        $product = $this->getProductByID($id);
        $featured = 1;
        if ($product->featured == 1) {
            $featured = 0;
        }
        if ($this->db->update('products', ['featured' => $featured], ['id' => $id])) {
            return ['featured'=>$featured];
        }
        return false;
    }

    public function set_status($id)
    {
        $product = $this->getProductByID($id);
        $discontinued = 1;
        if ($product->discontinued == 1) {
            $discontinued = 0;
        }
        if ($this->db->update('products', ['discontinued' => $discontinued], ['id' => $id])) {
            return ['discontinued'=>$discontinued];
        }
        return false;
    }

    public function get_colors(){
        $q = $this->db->get_where('colors', ['status'=>1]);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function get_materials(){
        $q = $this->db->get_where('materials', ['status'=>1]);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function get_product_by_code_reference($product){
        $q = $this->db->select('*')
                ->where('(code = "'.$product['code'].'" '.(isset($product['reference']) && $product['reference'] ? 'AND reference = "'.$product['reference'].'"' : '').')')
                // ->where('code', $product['code'])
                ->get('products');
        if ($q->num_rows() > 0) {
            return $q->row_array();
        }
        return false;
    }

    public function getProductPrice($data)
    {
        $this->db->where($data);
        $q = $this->db->get("product_prices");
        return $q->row();
    }

    public function getProductPrices($data)
    {
        $this->db->join("price_groups", "price_groups.id = product_prices.price_group_id");
        $this->db->where($data);
        $this->db->order_by("price_group_id");
        $this->db->limit(5);
        $q = $this->db->get("product_prices");
        return $q->result();
    }

    public function getTags($data = null)
    {
        if (!empty($data)) {
            $this->db->where($data);
        }
        $this->db->where('status', ACTIVE);
        $q = $this->db->get("tags");
        return $q->result();
    }

    public function findTags($data)
    {
        $this->db->where($data);
        $q = $this->db->get("tags");
        return $q->row();
    }

    public function updateStatusSyncStore($status, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update('products', ["synchronized_store" => $status])) {
            return true;
        }
        return false;
    }
    public function getColorByName($name)
    {
        $q = $this->db->get_where('colors', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getMaterialByName($name)
    {
        $q = $this->db->get_where('materials', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSecondaryTaxRateByCode($code)
    {
        $q = $this->db->get_where('tax_secondary', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function  getTaxSecundaryPercentage($data)
    {
        if (!empty($data)) {
            $this->db->where($data);
        }

        $q = $this->db->get('tax_secondary_percentage');
        return $q->result();
    }
    public function getOptionsSuggestions($term, $limit){
        $q = $this->db->select('name as id, name as text')->where('name', $term)->limit($limit)->get('variants');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }
    public function getOptionByName($name){
        $q = $this->db->where('name', $name)->get('variants');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getQuantityProductByWarehouse($data)
    {
        $this->db->select('quantity');
        $this->db->where($data);
        $q = $this->db->get('warehouses_products');
        return $q->row();
    }

    public function getQuantityProductVariantsByWarehouse($data)
    {
        $this->db->select('quantity');
        $this->db->where($data);
        $q = $this->db->get('warehouses_products_variants');
        return $q->row();
    }

    public function find($data, $like = false)
    {
        if ($like) {
            $keys = array_keys($data);
            $clave = $keys[0];
            $this->db->like($clave, $data[$clave]);
        } else {
            $this->db->where($data);
        }
        $q = $this->db->limit(1)->get('products');
        return $q->row();
    }

    public function findVariant($data)
    {
        $this->db->select('products.*,
            products.image,
            products.name,
            products.price,
            products.type,
            product_variants.id option_id,
            product_variants.code option_code,
            products.brand');
        $this->db->where($data);
        $this->db->join('product_variants', 'product_variants.product_id = products.id');
        $q = $this->db->get('products');
        return $q->row();
    }

    public function getAllWarehousesWithPVQ($option_id)
    {
        $this->db->select($this->db->dbprefix('warehouses') . '.*, ' .
            $this->db->dbprefix('warehouses_products_variants') . '.quantity')
            ->join('warehouses_products_variants', 'warehouses_products_variants.warehouse_id=warehouses.id', 'left')
            ->where('warehouses_products_variants.option_id', $option_id)
            ->group_by('warehouses.id');
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptionsWithWHV($product_id, $option_code)
    {
        $this->db->select(
            $this->db->dbprefix('product_variants') . '.*, ' .
            $this->db->dbprefix('warehouses') . '.name as wh_name, ' .
            $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' .
            $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
            ->group_by(array('' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'))
            ->order_by('product_variants.id');
        $q = $this->db->get_where('product_variants', ['product_variants.product_id' => $product_id, 'warehouses_products_variants.quantity !=' => NULL, 'product_variants.code' => $option_code]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductOptionsV($option_id)
    {
        $this->db->where('product_variants.code', $option_id);
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategorySuggestions($term, $limit = 10, $parentId = null, $subcategoryId = null)
    {
        $this->db->select("id, name AS text");
        $this->db->where("(id LIKE '%{$term}%' OR name LIKE '%{$term}%' OR name LIKE '%{$term}%') ");
        $this->db->where("parent_id", $parentId);
        if (!empty($subcategoryId)) {
            $this->db->where("subcategory_id", $subcategoryId);
        }
        $this->db->limit($limit);
        $q = $this->db->get('categories');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getProductSuggestions($term, $limit = 10)
    {
        $this->db->select("id, name AS text");
        $this->db->where("(id LIKE '%{$term}%' OR name LIKE '%{$term}%') ");
        $this->db->limit($limit);
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getProductCodesSuggestions($term, $limit = 10)
    {
        $this->db->select("id, name AS text, code");
        $this->db->where("(id LIKE '%{$term}%' OR code LIKE '%{$term}%') ");
        $this->db->limit($limit);
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getServicesSuggestions($term, $limit = 10)
    {
        $this->db->select("id, name AS text");
        $this->db->where("(id LIKE '%{$term}%' OR name LIKE '%{$term}%') ");
        $this->db->where("type", "service");
        $this->db->limit($limit);
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getReferenceSuggestions($term, $limit = 10)
    {
        $this->db->select("id, name AS text");
        $this->db->where("(reference LIKE '%{$term}%' OR code LIKE '%{$term}%') ");
        $this->db->limit($limit);
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getReference_suggestions($term, $limit = 10){
        $limit = $this->Settings->max_num_results_display;
        $this->db->select('id, name, reference')
                 ->where('(reference LIKE "%'.$term.'%")');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariantsSuggestions($term, $limit = 10)
    {
        $this->db->select("name AS id, name AS text");
        $this->db->where("(name LIKE '%{$term}%') ");
        $this->db->limit($limit);
        $q = $this->db->get('variants');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getColorSuggestions($term, $limit = 10)
    {
        $this->db->select("id, name AS text");
        $this->db->where("(name LIKE '%{$term}%') ");
        $this->db->limit($limit);
        $q = $this->db->get('colors');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getBrandSuggestions($term, $limit = 10)
    {
        $this->db->select("id, name AS text");
        $this->db->where("(name LIKE '%{$term}%' OR code LIKE '%{$term}%') ");
        $this->db->limit($limit);
        $q = $this->db->get('brands');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getMaterialSuggestions($term, $limit = 10)
    {
        $this->db->select("id, name AS text");
        $this->db->where("(name LIKE '%{$term}%' OR code LIKE '%{$term}%') ");
        $this->db->limit($limit);
        $q = $this->db->get('materials');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function getMultipleSearchProducts($data)
    {
        $conditionReference = '';
        if (!empty($data['reference'])) {
            $conditionReference = "AND gt.productId = {$data['reference']}";
        }

        $conditionVariant = '';
        if (!empty($data['variants'])) {
            $conditionVariant = "AND gt.variantName = {$data['variants']}";
        }

        $conditionColor = '';
        if (!empty($data['color'])) {
            $conditionColor = "AND gt.productColor = {$data['color']}";
        }

        $conditionBrand = '';
        if (!empty($data['brand'])) {
            $conditionBrand = "AND gt.productBrand = {$data['brand']}";
        }

        $conditionMaterial = '';
        if (!empty($data['material'])) {
            $conditionMaterial = "AND gt.productBrand = {$data['material']}";
        }

        $conditionCategory = '';
        $conditionSubCategory = '';
        $conditionSecondLevelSubCategory = '';
        if (!empty($data['category'])) {
            $conditionCategory = "AND gt.productCategory = {$data['category']}";

            if (!empty($data['subcategory'])) {
                $conditionSubCategory = "AND gt.productSubCategory = {$data['subcategory']}";

                if (!empty($data['subsubcategory'])) {
                    $conditionSecondLevelSubCategory = "AND gt.productSecondLevelSubcategory = {$data['subsubcategory']}";
                }
            }
        }

        $sql = "SELECT * FROM ((SELECT
                p.id AS productId,
                p.code AS productCode,
                p.reference AS productReference,
                p.name AS productName,
                pp.price AS productPrice,
                SUM(wp.quantity) AS productQuantity,
                w.id AS productWarehouseId,
                w.name AS productWarehouseName,
                color_id AS productColor,
                brand AS productBrand,
                material AS productMaterial,
                category_id AS productCategory,
                subcategory_id AS productSubCategory,
                second_level_subcategory_id AS productSecondLevelSubcategory,
                NULL AS variantName,
                NULL AS variantCode,
                NULL AS variantWarehouseId,
                NULL AS variantWarehouseName,
                NULL AS variantQuantity,
                NULL AS variantPrice
            FROM
                sma_products p
                    INNER JOIN
                sma_warehouses_products wp ON wp.product_id = p.id
                    INNER JOIN
                sma_warehouses w ON w.id = wp.warehouse_id
                    INNER JOIN
                sma_product_prices pp ON pp.product_id = p.id
                    INNER JOIN
                sma_price_groups pg ON pg.id = pp.price_group_id
            WHERE
                wp.quantity <> 0
            GROUP BY p.id, w.id
            ORDER BY p.code)

            UNION ALL

            (SELECT
                p.id AS productId,
                p.code AS productCode,
                p.reference AS productReference,
                p.name AS productName,
                pp.price AS productPrice,
                NULL AS productQuantity,
                NULL AS productWarehouseId,
                NULL AS productWarehouseName,
                color_id AS productColor,
                brand AS productBrand,
                material AS productMaterial,
                category_id AS productCategory,
                subcategory_id AS productSubCategory,
                second_level_subcategory_id AS productSecondLevelSubcategory,
                pv.name AS variantName,
                pv.code AS variantCode,
                wpv.warehouse_id AS variantWarehouseId,
                w.name AS variantWarehouseName,
                SUM(wpv.quantity) AS variantQuantity,
                pv.price AS variantPrice
            FROM
                sma_products p
                    INNER JOIN
                sma_product_prices pp ON pp.product_id = p.id
                    INNER JOIN
                sma_price_groups pg ON pg.id = pp.price_group_id
                    LEFT JOIN
                sma_product_variants pv ON pv.product_id = p.id
                    LEFT JOIN
                sma_warehouses_products_variants wpv ON wpv.option_id = pv.id
                    INNER JOIN
                sma_warehouses w ON w.id = wpv.warehouse_id
            WHERE
                pv.quantity <> 0
                AND wpv.product_id = p.id
            GROUP BY wpv.warehouse_id, p.id, pv.code
            ORDER BY p.code , pv.name ,  wpv.warehouse_id)) AS gt
        WHERE 1 = 1
            {$conditionReference}
            {$conditionVariant}
            {$conditionColor}
            {$conditionBrand}
            {$conditionMaterial}
            {$conditionCategory}
            {$conditionSubCategory}
            {$conditionSecondLevelSubCategory} ";

        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }

    public function update_promotion_price($data)
    {
        if ($this->db->update_batch('products', $data, 'code')) {
            return true;
        }
        return false;
    }

    private function validateItemAddition($products_origin){
        if ($this->Settings->item_addition == '1') { // <- cuando es 1 ya los envía sumados 
            return $products_origin;
        }
        $result = [];
        foreach ($products_origin as $item) {
            $productId = $item['product_id'].(isset($item['option_id']) ? $item['option_id'] : '');
            if (!isset($result[$productId])) {
                $result[$productId] = $item;  // Si no existe en el resultado, inicialízalo
            } else {
                $result[$productId]['quantity'] += $item['quantity'];
            }
        }
        $result = array_values($result);
        return $result; 

    }

    public function finish_express_count($count_id){
        $count = $this->db->get_where('stock_counts', ['id' => $count_id])->row();
        $count_items = $this->db->get_where('stock_count_items', ['stock_count_id' => $count_id]);
        if ($count_items->num_rows() > 0) {
            $count_items = $count_items->result();
        } else {
            return false;
        }
        $adjustment = array(
            'date' => $count->date,
            'reference_no' => $count->reference_no,
            'warehouse_id' => $count->warehouse_id,
            'note' => $count->note,
            'created_by' => $count->created_by,
            'biller_id' => $count->biller_id,
            'document_type_id' => $count->document_type_id,
            'companies_id' => $count->company_id,
            'status' => 'completed',
        );
        foreach ($count_items as $row) {
            $diference = 0;
            $wh_data = $this->products_model->getProductDataByWarehouse(['product_id' => $row->product_id, 'warehouse_id' => $count->warehouse_id]);
            $diference = $row->counted - $wh_data->quantity;
            if ($diference < 0) {
                $typeAjustment = 'subtraction';
                $diference = $diference * -1;
            } else {
                $typeAjustment = 'addition';
            }
            $adjustmentItems[] = array(
                'product_id' => $row->product_id,
                'quantity' => $diference,
                'warehouse_id' => $count->warehouse_id,
                'type' => $typeAjustment,
                'option_id' => "",
                'serial_no' => "",
                'avg_cost' => $row->cost
            );
        }
        $this->products_model->addAdjustment($adjustment, $adjustmentItems, [], false);
        $this->db->update('stock_counts', ['finalized' => 1, 'updated_at' => date('Y-m-d H:i:s')], ['id' => $count_id]);
        return true;
    }
}