<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="location.href = '<?= admin_url('products/print_production_order/'.$inv->id) ?>'">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <!-- <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/view_adjustment_pos/'.$inv->id) ?>" target="_blank">
                <i class="fa fa-print"></i> POS
            </a> -->
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>" alt="<?= !empty($biller->company) ? $biller->company : $biller->name; ?>">
            </div>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-5">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("ref"); ?>: <?= $inv->reference_no; ?><br>
                        <?= lang("status"); ?>: <?= lang($inv->status); ?><br>
                    </p>
                    </div>
                    <div class="col-xs-7 text-right order_barcodes">

                    </div>
                    <div class="clearfix"></div>
                    <?php if (isset($cost_center) && $cost_center): ?>
                        <div class="col-xs-6">
                            <p class="bold">
                                <?= lang("cost_center"); ?> : <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                            </p>
                        </div>
                    <?php endif ?>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="col-xs-6">
                <div class="table-responsive">
                    <label class="table-label"><?= lang("products"); ?> *</label>
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("description"); ?></th>
                                <th><?= lang("unit"); ?></th>
                                <th><?= lang("type"); ?></th>
                                <th><?= lang("quantity"); ?></th>
                                <th><?= lang("production_cost"); ?></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $r = 1;
                            $tTotal = 0;
                            foreach ($rows as $row):
                                if ($row->type != 'addition') {
                                    continue;
                                }
                            ?>
                                <tr>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                        <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                    </td>
                                    <th><?= $row->unit_code; ?></th>
                                    <th><?= lang($row->type); ?></th>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatNumber($row->quantity); ?></td>
                                    <th class="text-right"><?= $this->sma->formatMoney($row->quantity * ($row->adjustment_cost ? $row->adjustment_cost : $row->avg_cost)); ?></th>
                                    <?php $tTotal += $row->quantity * ($row->adjustment_cost ? $row->adjustment_cost : $row->avg_cost); ?>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="text-right"><?= lang('total') ?></th>
                                <th class="text-right"><?= $this->sma->formatMoney($tTotal); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="table-responsive">
                    <label class="table-label"><?= lang("components"); ?> *</label>
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("description"); ?></th>
                                <th><?= lang("unit"); ?></th>
                                <th><?= lang("type"); ?></th>
                                <th><?= lang("quantity"); ?></th>
                                <th><?= lang("production_cost"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $r = 1;
                            $tTotal = 0;
                            foreach ($rows as $row):
                                if ($row->type != 'subtraction') {
                                    continue;
                                }
                            ?>
                                <tr>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                        <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                    </td>
                                    <th><?= $row->unit_code; ?></th>
                                    <th><?= lang($row->type); ?></th>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatNumber($row->quantity); ?></td>
                                    <th class="text-right"><?= $this->sma->formatMoney($row->quantity * ($row->adjustment_cost ? $row->adjustment_cost : $row->avg_cost)); ?></th>
                                    <?php $tTotal += $row->quantity * ($row->adjustment_cost ? $row->adjustment_cost : $row->avg_cost); ?>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="text-right"><?= lang('total') ?></th>
                                <th class="text-right"><?= $this->sma->formatMoney($tTotal); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-7">
                    <?php if ($inv->note || $inv->note != "") { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang("note"); ?>:</p>
                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php } ?>
                </div>

                <div class="col-xs-5 pull-right">
                    <div class="well well-sm">
                        <p>
                            <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                        </p>
                        <?php if ($inv->updated_by) { ?>
                        <p>
                            <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                            <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                        </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
