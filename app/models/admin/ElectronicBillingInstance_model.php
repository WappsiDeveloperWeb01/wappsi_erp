<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class ElectronicBillingInstance_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get("electronic_billing_instances");
        if ($q->result() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getAll()
    {
        $q = $this->db->get("electronic_billing_instances");
        if ($q->result() > 0) {
            return $q->result();
        }
        return false;
    }
}