<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 *  ==============================================================================
 *  Author  : Mian Saleem
 *  Email   : saleem@tecdiary.com
 *  For     : mPDF
 *  Web     : https://github.com/mpdf/mpdf
 *  License : GNU General Public License v2.0
 *          : https://github.com/mpdf/mpdf/blob/development/LICENSE.txt
 *  ==============================================================================
 */

error_reporting(0); 
require_once FCPATH . "vendor/autoload.php";

use Mpdf\Mpdf;

class Tec_mpdf
{
    public function __construct() {
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    public function generate($content, $name = 'download.pdf', $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, $orientation = 'P') {

        $mpdf = new Mpdf();
        $mpdf->SetFont('helvetica', '', 12);
        $mpdf->AddPage();
        $mpdf->WriteHTML($content);
        $mpdf->Output();
        
        // if ($output_type == 'S') {
        //     $file_content = $mpdf->Output('', 'S');
        //     write_file('assets/uploads/' . $name, $file_content);
        //     return 'assets/uploads/' . $name;
        // } else {
        //     $mpdf->Output($name, $output_type);
        // }
    }

    public function generate_pos_inv($content, $name = 'download.pdf', $output_type = 'S') {
        $mpdfConfig = array(
            'margin_left' => 10,     // 15 margin_left
            'margin_right' => 10,        // 15 margin right
            'format' => [120, 600],
        );
        $mpdf = new Mpdf($mpdfConfig);
        $mpdf->SetFont('helvetica', '', 12);
        $mpdf->AddPage();
        $mpdf->WriteHTML($content);
        if ($output_type == 'S') {
            $file_content = $mpdf->Output(FCPATH.'files/'.$name.".pdf", 'F');
            return FCPATH . $name.".pdf";
        } else if ($output_type == 'D') {
            $mpdf->Output($name.".pdf", 'D');
        } else {
            $mpdf->Output($name.".pdf", 'I');
        }
    }
}
