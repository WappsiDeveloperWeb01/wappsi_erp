<?php

/***********

FORMATO DE FACTURACIÓN ELECTRÓNICA, AGRUPADO POR REFERENCIA DE PRODUCTO

************/
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de compra'));
        //izquierda
        $this->RoundedRect(13, 7, 117, 29, 3, '1234', '');
        //izquierda
        if ($this->biller_logo == 2) {
            $this->Image((isset($this->logo) ? base_url().'assets/uploads/logos/'.$this->logo : ''),16,9,25);
        } else {
            $this->Image((isset($this->logo) ? base_url().'assets/uploads/logos/'.$this->logo : ''),16,9,25);
        }
        $cx = 45;
        $cy = 13;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode($this->biller->company != "-" ? $this->biller->company : $this->biller->name),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->digito_verificacion > 0 ? '-'.$this->biller->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address." - ".ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 29, 3, '1234', '');

        $cx = 133;

        //derecha
        $cy = 10;
        $this->setXY($cx, $cy);
        $this->MultiCell(48, 5 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'FACTURA DE COMPRA'),0,'L');
        $cy +=15;
        $this->setXY($cx, $cy);
        $this->Cell(48, 5 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'L');
        $this->Image(base_url().'themes/default/admin/assets/images/qr_code/QRCodeDS.png', 182, 9, 25);

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 38, 118, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 43, 118, 30, 3, '4', '');
        $cx = 13;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('INFORMACIÓN DEL CLIENTE'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->setXY($cx, $cy);
        // $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->supplier->name != '-' ? $this->supplier->name : $this->supplier->company)),'',1,'L');
        $supplier_name = $this->supplier->name != '-' ? $this->supplier->name : $this->supplier->company;
        if ($this->supplier->type_person == 2) {
            $supplier_name = $this->supplier->first_name.($this->supplier->second_name != '' ? ' '.$this->supplier->second_name : '').($this->supplier->first_lastname != '' ? ' '.$this->supplier->first_lastname : '').($this->supplier->second_lastname != '' ? ' '.$this->supplier->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($supplier_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 7;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->supplier->vat_no.($this->supplier->digito_verificacion != '' ? '-'.$this->supplier->digito_verificacion : '')),'',1,'L');
        // $cy += 3;
        // $this->setXY($cx, $cy);
        // $this->SetFont('Arial','B',$this->fuente+1.5);
        // $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        // $this->SetFont('Arial','',$this->fuente+1.5);
        // $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->supplier->direccion),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->supplier->address.", ".(ucfirst(mb_strtolower($this->supplier->city)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->supplier->phone),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->supplier->email),'',1,'L');

        //derecha


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(131, 38, 78, 5, 3, '2', 'DF');
        $this->RoundedRect(131, 43, 78, 30, 3, '3', '');
        $cx = 131;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA FACTURA'),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('TOTAL'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente+1.5);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatMoney(($this->factura->grand_total -
            ($this->factura->rete_fuente_assumed ? 0 : $this->factura->rete_fuente_total) -
            ($this->factura->rete_iva_assumed ? 0 : $this->factura->rete_iva_total) -
            ($this->factura->rete_ica_assumed ? 0 : $this->factura->rete_ica_total) -
            ($this->factura->rete_ica_assumed ? 0 : $this->factura->rete_bomberil_total) -
            ($this->factura->rete_ica_assumed ? 0 : $this->factura->rete_autoaviso_total) -
            ($this->factura->rete_other_assumed ? 0 : $this->factura->rete_other_total)
            ) * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA VENCE'),'LBR',1,'C',1);
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('PAGO'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".$this->factura->payment_term." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $payment_text = $this->factura->payment_status != 'paid' ? lang('credit').", " : '';
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($payment_text.$this->factura->payment_term ." ".($this->factura->payment_term > 1 ? "Días" : "Día")),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode('Factura del proveedor'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $cy += $altura;
        $this->setXY($cx, $cy);
        // if ($this->seller) {
            // $this->Cell(78, $altura , $this->sma->utf8Decode(ucwords(mb_strtolower($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');
        // } else {
            $this->Cell(78, $altura , $this->sma->utf8Decode($this->factura->consecutive_supplier),'',1,'C');
        // }

        $this->ln();


        $this->SetFont('Arial','B',$this->fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $current_X = $this->getX();
        $current_Y = $this->getY();
        $this->Cell(9.6 , 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(13.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(53.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(11.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBLR',1,'C',1);

        $this->setXY($current_X, $current_Y);

        $this->Cell(9.6 , 8, $this->sma->utf8Decode('Ítem'),'',0,'C',0);
        $this->Cell(13.6, 8, $this->sma->utf8Decode('Código'),'',0,'C',0);
        $this->Cell(53.6, 8, $this->sma->utf8Decode('Nombre producto'),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(10.6, 8, $this->sma->utf8Decode('Dcto'),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(10.6, 8, $this->sma->utf8Decode('IVA'),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(11.6, 8, $this->sma->utf8Decode('Cantidad'),'',0,'C',0);
        $this->Cell(21.6, 8, $this->sma->utf8Decode('Subtotal'),'',1,'C',0);

        $this->setXY($current_X, $current_Y+4);

        $this->Cell(9.6 , 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(13.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(53.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Bruto'),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Con dcto.'),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('IVA incl.'),'',0,'C',0);
        $this->Cell(11.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode(''),'',1,'C',0);
    }

    function Footer()
    {
        // Print centered page number
        $this->RoundedRect(13, 245, 196, 20.5, 3, '1234', '');
        $this->SetXY(13, -34);
        $this->Cell(196, 5, $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
        $this->Cell(196, 5, $this->sma->utf8Decode('CUDS: '. $this->uuid),'',1,'C');

        if (isset($this->cost_center) && $this->cost_center) {
            $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2( "Centro de costo : ".$this->cost_center->name." (".$this->cost_center->code.") \n".$this->description2)), 0, 'L');
            $this->Cell(196,3,$this->sma->utf8Decode(lang('created_by') .': '.('       '.$this->created_by->first_name." ".$this->created_by->last_name)),'', 1, 'L');
            $this->Cell(196,3,$this->sma->utf8Decode(lang('creation_date') .': '.($this->sma->hrld($this->factura->registration_date))), '', 0, 'L');
        } else {
            $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
        }
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 805) {
            $text = substr($text, 0, 800);
            $text.="...";
            return $text;
        }
        return $text;
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->biller_logo = $biller_logo;
$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->supplier = $supplier;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = 1;
$pdf->uuid = $inv->uuid;
$pdf->created_by = $created_by;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();

$maximo_footer = 235;


$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);

$taxes = [];
$total_bruto = 0;
$total_flete = 0;
$references = [];
    // $this->sma->print_arrays($rows);
    foreach ($rows as $item) {

        $total_flete += $item->shipping_unit_cost;

        $total_bruto += (($item->net_unit_cost + ($item->item_discount / ($item->quantity > 0 ? $item->quantity : 1))) * ($item->quantity > 0 ? $item->quantity : 1)) * $trmrate;

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_cost * ($item->quantity > 0 ? $item->quantity : 1);
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_cost * ($item->quantity > 0 ? $item->quantity : 1);
        }

        // if ($item->tax_rate_2_id) {
        //     if (!isset($taxes[$item->tax_rate_2_id])) {
        //         $taxes[$item->tax_rate_2_id]['tax'] = $item->item_tax;
        //         $taxes[$item->tax_rate_2_id]['base'] = $item->net_unit_cost * ($item->quantity > 0 ? $item->quantity : 1);
        //     } else {
        //         $taxes[$item->tax_rate_2_id]['tax'] += $item->item_tax;
        //         // $taxes[$item->tax_rate_2_id]['base'] += $item->net_unit_cost * ($item->quantity > 0 ? $item->quantity : 1);
        //     }
        // }

        if (isset($references[$item->id])) {
            // $references[$item->id]['unit_cost_1'] += ($item->net_unit_cost + ($item->item_discount / ($item->quantity > 0 ? $item->quantity : 1))) * $trmrate;
            // $references[$item->id]['unit_cost_2'] += $item->net_unit_cost * $trmrate;
            // $references[$item->id]['unit_cost_3'] += $item->unit_cost * $trmrate;
            $references[$item->id]['pr_subtotal'] += $item->subtotal * $trmrate;
            $references[$item->id]['pr_quantity'] += ($item->quantity > 0 ? $item->quantity : 1);
            // $references[$item->id]['pr_individual_discount'] += $item->item_discount / ($item->quantity > 0 ? $item->quantity : 1);
            // $references[$item->id]['pr_individual_tax'] += $item->item_tax / ($item->quantity > 0 ? $item->quantity : 1);
        } else {
            $references[$item->id]['pr_code'] = ($show_code == 1 ? $item->product_code : $item->reference);
            $pr_name = $pdf->sma->reduce_text_length(
                                                $item->product_name.
                                                (!is_null($item->variant) ? "( ".$item->variant." )" : '').
                                                (!is_null($item->serial_no) ? " - ".$item->serial_no : '')
                                            , 39);
            $references[$item->id]['pr_name'] = $pr_name;
            $references[$item->id]['pr_discount'] = $item->discount;
            $references[$item->id]['pr_individual_discount'] = $item->item_discount / ($item->quantity > 0 ? $item->quantity : 1);
            $references[$item->id]['pr_tax'] = $item->tax;
            $references[$item->id]['pr_individual_tax'] = $item->item_tax / ($item->quantity > 0 ? $item->quantity : 1);
            $references[$item->id]['unit_cost_1'] = ($item->net_unit_cost + ($item->item_discount / ($item->quantity > 0 ? $item->quantity : 1))) * $trmrate;
            $references[$item->id]['unit_cost_2'] = $item->net_unit_cost * $trmrate;
            $references[$item->id]['unit_cost_3'] = ($item->unit_cost - $item->shipping_unit_cost) * $trmrate;
            $references[$item->id]['pr_subtotal'] = $item->subtotal * $trmrate;
            $references[$item->id]['pr_quantity'] = ($item->quantity > 0 ? $item->quantity : 1);
        }
    }

    // $this->sma->print_arrays($references);

    $cnt = 1;
    foreach ($references as $reference => $data) {
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }
        $pdf->Cell( 9.6, 5, $this->sma->utf8Decode($cnt),'BL',0,'C',0);
        $pdf->Cell(13.6, 5, $this->sma->utf8Decode($data['pr_code']),'BL',0,'C',0);
        $pdf->Cell(53.6, 5, $this->sma->utf8Decode($data['pr_name']),'BL',0,'L',0);
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatMoney($data['unit_cost_1'])),'BL',0,'C',0);
        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($data['pr_discount']),'BL',0,'C',0);
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatMoney($data['unit_cost_2'])),'BL',0,'C',0);
        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($data['pr_tax']),'BL',0,'C',0);
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatMoney($data['unit_cost_3'])),'BL',0,'C',0);
        $pdf->Cell(11.6, 5, $this->sma->utf8Decode($this->sma->formatQuantity($data['pr_quantity'])),'BL',0,'C',0);
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatMoney($data['pr_subtotal'])),'BLR',1,'C',0);
        $cnt++;
    }

// if ($inv->order_tax > 0) {
//     if (!isset($taxes[$inv->order_tax_id])) {
//         $taxes[$inv->order_tax_id]['tax'] = $inv->order_tax;
//         $taxes[$inv->order_tax_id]['base'] = $inv->total;
//     } else {
//         $taxes[$inv->order_tax_id]['tax'] += $inv->order_tax;
//         $taxes[$inv->order_tax_id]['base'] += $inv->total;
//     }
// }



// $pdf->Cell(39.2,5, $this->sma->utf8Decode($pdf->getY()),'BR',1,'R');

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(1,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(115.6,5, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->SetFont('Arial','',$fuente);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(14);
$pdf->SetFont('Arial','',$fuente);
// $pdf->Cell(115.6,6.125, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total, $currencies_names[$inv->purchase_currency])),'',1,'L');
$pdf->MultiCell(115.6, 3, $this->sma->utf8Decode($number_convert->convertir((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total) * $trmrate), (isset($currencies_names[$inv->purchase_currency]) ? $currencies_names[$inv->purchase_currency] : $this->Settings->default_currency))), 0, 'L');
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->setX(13);
$pdf->setXY($current_x, $current_y+8);
$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Impuesto'),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Total'),'',1,'C');
// $pdf->Cell(38.53,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');
$pdf->SetFont('Arial','',$fuente+1.5);
$total = $inv->total;
// $this->sma->print_arrays($taxes);
foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $total+=$arr['tax'];

    $pdf->Cell(28.9,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : ''),'',1,'C');

    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($arr['base'] * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($arr['tax'] * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($total)),'',1,'C');
}

$total+=$inv->order_tax;

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$total+=$arr['tax'];
if ($inv->order_tax > 0) {
    $pdf->Cell(28.9,3, $this->sma->utf8Decode("(SP) ".$this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id]." %" : "0%"))),'',1,'C');
    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->total * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->order_tax * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($total * $trmrate)),'',1,'C');
}


if ($inv->consumption_purchase > 0) {
    $pdf->Cell(28.9,3, $this->sma->utf8Decode("Impoconsumo (L&C)"),'',1,'C');
    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney(0 * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->consumption_purchase * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->consumption_purchase * $trmrate)),'',1,'C');
}

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 3;

//derecha

$pdf->setXY($cX_items_finished+124, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

if ($inv->product_discount > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Total bruto'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue(null, ($inv->total+ $inv->product_discount ) * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

if ($inv->product_discount > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Descuento a productos'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->product_discount > 0 ? "- " : "").$this->sma->formatValue(null, ($inv->product_discount) * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Subtotal'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue(null, $inv->total * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue(null, $inv->total_tax * $trmrate)), 'TBR', 1, 'R');

if ($inv->order_discount > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Descuento general'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->order_discount > 0 ? "- " : "").$this->sma->formatValue(null, ($inv->order_discount) * $trmrate)), 'TBR', 1, 'R');
}


if ($inv->rete_fuente_assumed == 0 && $inv->rete_fuente_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención en la fuente'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_fuente_total > 0 ? "- " : "").$this->sma->formatValue(null, $inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');
}


if ($inv->rete_iva_assumed == 0 && $inv->rete_iva_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención al IVA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_iva_total > 0 ? "- " : "").$this->sma->formatValue(null, $inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_assumed == 0 && $inv->rete_ica_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención al ICA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_ica_total > 0 ? "- " : "").$this->sma->formatValue(null, $inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_other_assumed == 0 && $inv->rete_other_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode(' '.lang('rete_other')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_other_total > 0 ? "- " : "").$this->sma->formatValue(null, $inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_assumed == 0 && $inv->rete_bomberil_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Tasa Bomberil'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_bomberil_total > 0 ? "- " : "").$this->sma->formatValue(null, $inv->rete_bomberil_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_assumed == 0 && $inv->rete_autoaviso_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Auto Avisos y Tableros'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_autoaviso_total > 0 ? "- " : "").$this->sma->formatValue(null, $inv->rete_autoaviso_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->shipping > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Domicilio'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue(null, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;

$total_retentions =
            ($inv->rete_fuente_assumed == 0 ? $inv->rete_fuente_total : 0) +
            ($inv->rete_iva_assumed == 0 ? $inv->rete_iva_total : 0) +
            ($inv->rete_ica_assumed == 0 ? $inv->rete_ica_total : 0) +
            ($inv->rete_ica_assumed == 0 ? $inv->rete_bomberil_total : 0) +
            ($inv->rete_ica_assumed == 0 ? $inv->rete_autoaviso_total : 0) +
            ($inv->rete_other_assumed == 0 ? $inv->rete_other_total : 0);

$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  TOTAL A PAGAR'), 'TBLR', 1, 'L', 1);
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue(NULL, ($inv->grand_total - $total_retentions) * $trmrate)), 'TBR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente+1.5);

$pdf->Ln(5);


//FIRMAS

//izquierda
$pdf->ln(8);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;
$pdf->cell(1,3,"",0,'L');
$pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

//derecha
$pdf->setXY($current_x+78.4, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Representante Legal'), '', 1, 'L');

if ($signature_root) {
    $pdf->Image($signature_root, $current_x+35,$current_y+1,35);
}

// $descargar = false;

if ($inv->status == 'pending') {
    $pdf->setXY(90, 90);
    $pdf->SetFont('Arial','B',50);
    $pdf->SetTextColor(125,125,125);
    $pdf->RotatedImage(base_url().'assets/images/not_approved.png',1,240,293,16,45);
    $pdf->SetFont('Arial','',$fuente);
    $pdf->SetTextColor(0,0,0);
}

if ($download || $for_email) {
    if ($download) {
        $pdf->Output("factura_compra.pdf", "I");
    } else if ($for_email) {
        $pdf->Output("assets/uploads/". $inv->reference_no .".pdf", "I");
    }
} else {
  $pdf->Output("factura_compra.pdf", "I");
}