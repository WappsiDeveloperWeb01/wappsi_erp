<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight no-print">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('documentsReception/index', ['id'=>'documentReceptionfilter']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('date_records_filter') ?></label>
                                            <select class="form-control" name="date_records_filter" id="date_records_filter_dh">
                                                <?php $dateRecordsFilter = (isset($_POST["date_records_filter"]) ? $_POST["date_records_filter"] : ''); ?>
                                                <?= $this->sma->get_filter_options($dateRecordsFilter); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('supplier') ?></label>
                                            <?php $provider = (isset($_POST["provider"])) ? $_POST["provider"] : ''; ?>
                                            <input class="form-control" type="hidden" name="provider" value="<?= $provider ?>" id="provider" placeholder="<?= $this->lang->line('alls') ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('events') ?></label>
                                            <?php $viewPendingEventsId = (isset($_POST["viewPendingEvents"])) ? $_POST["viewPendingEvents"] : ''; ?>
                                            <select class="form-control" name="viewPendingEvents" id="viewPendingEvents">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($viewPendingEvents as $viewPendingEvent) : ?>
                                                    <option value="<?= $viewPendingEvent->id ?>" <?= ($viewPendingEvent->id == $viewPendingEventsId) ? 'selected' : '' ?>><?= $viewPendingEvent->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="date_controls_dh">
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for="start_date_dh"><?= $this->lang->line('start_date') ?></label>
                                                <?php $startDate = isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>
                                                <input class="form-control datetime" type="text" name="start_date" id="start_date_dh" value="<?= $startDate ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for="end_date_dh"><?= $this->lang->line('end_date') ?></label>
                                                <?php $endDate = isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>
                                                <input class="form-control datetime" type="text" name="end_date" id="end_date_dh" value="<?= $endDate ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="documentsReceptionTable" class="table table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkth" type="checkbox" name="check" />
                                            </th>
                                            <th><?= $this->lang->line('document_type') ?></th>
                                            <th><?= $this->lang->line('document_number') ?></th>
                                            <th><?= $this->lang->line('supplier') ?></th>
                                            <th><?= $this->lang->line('expedition_date') ?></th>
                                            <th><?= $this->lang->line('total') ?></th>
                                            <th><?= $this->lang->line('attached') ?></th>
                                            <th><?= $this->lang->line('pdf') ?></th>
                                            <th><?= $this->lang->line('xml') ?></th>
                                            <th><?= $this->lang->line('acknowledgment_receipt') ?></th>
                                            <th><?= $this->lang->line('reception_good_service') ?></th>
                                            <th><?= $this->lang->line('express_acceptance') ?></th>
                                            <th><?= $this->lang->line('claim') ?></th>
                                            <th><?= $this->lang->line('actions') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        loadDataTable();
        loadSupplierInput();

        $(document).on('click', '.viewJson', function(event) { showViewJson(event, $(this)); });
        $(document).on('click', '.errorMessage', function(event) { showErrorMessage($(this)); });
        $(document).on('click', '.sendEvent', function(event) { confirmSendEvent(event, $(this)); });
        $(document).on('click', '.sendEvents', function(event) { confirmSendEvents(event, $(this)); });
        $(document).on('click', '.checkStatus', function(event) { confirmCheckStatus(event, $(this)); });
        $(document).on('click', '#syncButton', function(event) { event.preventDefault(); confirmSync($(this)); });

        $('[data-toggle="tooltip"]').tooltip();
    })

    function loadDataTable() {
        var nums = [ [10, 25],  [10, 25] ];
        if ($(window).width() > 1000) {
            nums = [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ];
        }

        $('#documentsReceptionTable').dataTable({
            'aaSorting': [ [4, 'desc'] ],
            'aLengthMenu': nums,
            'iDisplayLength': $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            // 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('documentsReception/getDocuments/') ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });

                <?php if (isset($_POST['start_date'])) : ?>
                    aoData.push({
                        "name": "start_date",
                        "value": "<?= $_POST['start_date'] ?>"
                    });
                <?php endif ?>

                <?php if (isset($_POST['end_date'])) : ?>
                    aoData.push({
                        "name": "end_date",
                        "value": "<?= $_POST['end_date'] ?>"
                    });
                <?php endif ?>

                <?php if (isset($_POST['provider'])) : ?>
                    aoData.push({
                        "name": "supplier",
                        "value": "<?= $_POST['provider'] ?>"
                    });
                <?php endif ?>

                <?php if (isset($_POST['viewPendingEvents'])) : ?>
                    aoData.push({
                        "name": "viewPendingEvents",
                        "value": "<?= $_POST['viewPendingEvents'] ?>"
                    });
                <?php endif ?>

                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            drawCallback: function(settings) {
                var syncButton = '';
                var createButton = '';
                var version = '<?= $this->Settings->version ?>';
                var systemYear = version.split('.')[0];

                <?php if ($this->Admin || $this->Owner) { ?>
                    createButton = '<a href="<?= admin_url('documentsReception/add') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>';

                    sendEventButton = '<li style="padding-left: 20px; padding-top: 10px"><label><?= $this->lang->line('sendEvents') ?></label></li>' +
                        '<li>'+
                            '<a class="sendEvents" href="<?= admin_url('documentsReception/sendEvents/') ?>" eventType="<?= $ACKNOWLEDGMENT_RECEIPT ?>"><i class="fa fa-share-square-o"></i> <?= $this->lang->line("acknowledgment_receipt") ?></a>' +
                        '</li>' +
                        '<li>' +
                            '<a class="sendEvents" href="<?= admin_url('documentsReception/sendEvents/') ?>" eventType="<?= $RECEPTION_GOOD_SERVICE ?>"><i class="fa fa-share-square-o"></i> <?= $this->lang->line("reception_good_service") ?></a>' +
                        '</li>' +
                        '<li>' +
                            '<a class="sendEvents" href="<?= admin_url('documentsReception/sendEvents/') ?>" eventType="<?= $EXPRESS_ACCEPTANCE ?>"><i class="fa fa-share-square-o"></i> <?= $this->lang->line("express_acceptance") ?></a>'+
                        '</li>';
                <?php } else { ?>
                    sendEventButton = '<li style="padding-left: 20px; padding-top: 10px"><label><?= $this->lang->line('sendEvents') ?></label></li>';
                    <?php if ($this->GP['documentsReception-acknowledgment']) { ?>
                        sendEventButton += '<li>' +
                            '<a class="sendEvents" href="<?= admin_url('documentsReception/sendEvents/') ?>" eventType="<?= $ACKNOWLEDGMENT_RECEIPT ?>"><i class="fa fa-share-square-o"></i> <?= $this->lang->line("acknowledgment_receipt") ?></a>' +
                        '</li>';
                    <?php } ?>

                    <?php if ($this->GP['documentsReception-reception_good_service']) { ?>
                        sendEventButton += '<li>' +
                            '<a class="sendEvents" href="<?= admin_url('documentsReception/sendEvents/') ?>" eventType="<?= $RECEPTION_GOOD_SERVICE ?>"><i class="fa fa-share-square-o"></i> <?= $this->lang->line("reception_good_service") ?></a>' +
                        '</li>';
                    <?php } ?>

                    <?php if ($this->GP['documentsReception-express_acceptance']) { ?>
                        sendEventButton += '<li>' +
                            '<a class="sendEvents" href="<?= admin_url('documentsReception/sendEvents/') ?>" eventType="<?= $EXPRESS_ACCEPTANCE ?>"><i class="fa fa-share-square-o"></i> <?= $this->lang->line("express_acceptance") ?></a>' +
                        '</li>';
                    <?php } ?>
                <?php } ?>

                <?php if ($this->Settings->years_database_management == 2) { ?>
                    const fecha = new Date();
                    if (systemYear == fecha.getFullYear()) {
                        syncButton = '<li><a id="syncButton" href="<?= admin_url('documentsReception/sync') ?>"><i class="fa fa-refresh"></i> <?= $this->lang->line('sync_documents') ?></a></li>';
                    }
                <?php } else { ?>
                    syncButton = '<li><a id="syncButton" href="<?= admin_url('documentsReception/sync') ?>"><i class="fa fa-refresh"></i> <?= $this->lang->line('sync_documents') ?></a></li>';
                <?php } ?>

                $('.actionsButtonContainer').html(`${createButton}
                <div class="dropdown pull-right">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu">
                        ${syncButton} 
                        ${sendEventButton} 
                    </ul>
                </div>`);

                $('[data-toggle-second="tooltip"]').tooltip();
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue',
                    increaseArea: '20%'
                });
            },
            columns: [{
                    bSortable: false,
                    mRender: function(data, type, row) {
                        return checkbox(row.id);
                    }
                },
                {
                    data: 'documentTypeName'
                },
                {
                    data: 'documentId'
                },
                {
                    data: 'supplierName'
                },
                {
                    data: 'documentDate'
                },
                {
                    data: 'grandTotal',
                    className: 'text-right'
                },
                {
                    data: 'attachedDocumentLink',
                    className: 'text-center'
                },
                {
                    data: 'pdfUrlLink',
                    className: 'text-center'
                },
                {
                    data: 'xmlUrlLink',
                    className: 'text-center'
                },
                {
                    data: 'acknowledgment',
                    className: 'text-center'
                },
                {
                    data: 'acknowledgmentGoodService',
                    className: 'text-center'
                },
                {
                    data: 'expressAcceptance',
                    className: 'text-center'
                },
                {
                    data: 'claim',
                    className: 'text-center'
                },
                {
                    data: 'actions',
                    className: 'text-center'
                }
            ]
        });
    }

    function confirmSendEvent(event, element) {
        event.preventDefault();

        var eventType = element.attr('eventtype');
        var documentId = element.attr('documentid');
        var route_resend_invoice = element.attr('href');
        var eventTypeName = getEventTypeName(eventType);
        var claimCode = (eventType == '<?= $CLAIM ?>') ? element.attr('claimCode') : '';
        var eventId = (element.attr('eventid') !== undefined) ? element.attr('eventid') : '';

        swal({
            title: 'Envio de evento',
            text: '¿Está seguro de enviar el evento: ' + eventTypeName + '?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Enviando ' + eventTypeName + '...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            if (eventId == '') {
                window.location.href = route_resend_invoice + eventType + '/' + documentId + '/' + claimCode;
            } else {
                window.location.href = route_resend_invoice + documentId + '/' + eventId;
            }
        });
    }

    function getEventTypeName(eventType) {
        if (eventType == '<?= $ACKNOWLEDGMENT_RECEIPT ?>') {
            evenTypeName = '<?= $this->lang->line('acknowledgment_receipt') ?>'
        } else if (eventType == '<?= $RECEPTION_GOOD_SERVICE ?>') {
            evenTypeName = '<?= $this->lang->line('reception_good_service') ?>'
        } else if (eventType == '<?= $CLAIM ?>') {
            evenTypeName = '<?= $this->lang->line('claim') ?>'
        } else if (eventType == '<?= $EXPRESS_ACCEPTANCE ?>') {
            evenTypeName = '<?= $this->lang->line('express_acceptance') ?>'
        }

        return evenTypeName;
    }

    function showErrorMessage(element) {
        var eventId = element.attr('eventid');
        $.ajax({
            type: "post",
            url: site.base_url + 'documentsReception/getMessageEvent',
            data: {
                'eventId': eventId,
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>'
            },
            dataType: "json",
            success: function(response) {
                swal({
                    title: response.title,
                    text: response.message,
                    type: 'info',
                    showCancelButton: true,
                    showConfirmButton: false,
                    cancelButtonText: "Ok",
                    closeOnClickOutside: false
                });
            }
        });
    }

    function confirmCheckStatus(event, element) {
        event.preventDefault();

        var eventType = element.attr('eventtype');
        var routeResend = element.attr('href');
        var eventTypeName = getEventTypeName(eventType);

        swal({
            title: 'Consulta estado del evento',
            text: '¿Está seguro de consultar el estado del evento: ' + eventTypeName + '?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Consultado estado del evento ' + eventTypeName + '...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            // window.location.href = routeResend;
            checkStatus(routeResend)
        });
    }

    function checkStatus(routeResend) {
        $.ajax({
            type: "POST",
            url: routeResend,
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: "json",
            success: function(response) {
                console.log(response);
                if (response.status == false) {
                    Command: toastr.error(response.message, '<?= lang("toast_error_title"); ?>', {
                        onHidden: function() {
                            swal.close();
                        }
                    });
                }
                else {
                    Command: toastr.success(response.message, '<?= lang("toast_success_title"); ?>', {
                        onHidden: function() {
                            $('#documentReceptionfilter').submit();
                        }
                    });
                }
            }
        });
    }

    function confirmSendEvents(event, element) {
        event.preventDefault();

        if ($('.multi-select:checked').length == 0) {
            swal({
                title: '<?= $this->lang->line("noDocumentsSelected"); ?>',
                text: 'Por favor selecciones los documentos que desea enviar para el evento seleccionado',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "Ok",
                showConfirmButton: false,
                closeOnConfirm: false
            });
        } else {
            var eventType = element.attr('eventtype');
            var eventTypeName = getEventTypeName(eventType);

            swal({
                title: 'Envio de eventos de documentos',
                text: '¿Está seguro de enviar los evento: ' + eventTypeName + '?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "No",
                confirmButtonText: "¡Sí!",
                closeOnConfirm: false
            }, function() {
                swal({
                    title: 'Enviando eventos ' + eventTypeName + '...',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false
                });

                sendEvents(element);
            });
        }
    }

    function sendEvents(element) {
        var documentIds = [];
        var maximumNumberEvents = 10;
        var urlRequest = element.attr('href');
        var eventType = element.attr('eventtype');

        $('.multi-select:checked').each(function(index, element) {
            if ((index + 1) <= maximumNumberEvents) {
                documentIds.push($(this).val());
            }
        });

        $.ajax({
            type: "POST",
            url: urlRequest,
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'eventType': eventType,
                'documentIds': JSON.stringify(documentIds)
            },
            dataType: "json",
            success: function(response) {
                if (response.status == false) {
                    Command: toastr.error(response.message, '<?= lang("toast_error_title"); ?>', {
                        onHidden: function() {
                            swal.close();
                        }
                    });
                }
                else {
                    Command: toastr.success(response.message, '<?= lang("toast_success_title"); ?>', {
                        onHidden: function() {
                            $('#documentReceptionfilter').submit();
                        }
                    });
                }
            }
        });
    }

    function confirmSync(element) {
        swal({
            title: 'Proceso de Sincronización de Documentos',
            text: '¿Está seguro de realizar la sincronización de documentos?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Realizando sincronización...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            sync(element);
        });
    }

    function sync(element) {
        $.ajax({
            type: "POST",
            url: element.attr('href'),
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: "json",
            success: function(response) {
                swal.close();
                if (response.status == true) {
                    Command: toastr.success(response.message, '<?= lang("toast_success_title"); ?>', {
                        onHidden: function(reponse) {
                            if (response.sendEvent == true) {
                                confirmSendAutoAcknowledgmentEvent();
                            }
                        }
                    });
                } else {
                    Command: toastr.error(response.message, '<?= lang("toast_error_title"); ?>');
                }
            }
        });
    }

    function confirmSendAutoAcknowledgmentEvent() {
        swal({
            title: 'Envio automático de Acuse de recibido',
            text: 'Se realizará el procedimiento para el envío automático de acuse de documentos sincronizados',
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Enviando Acuse automáticos...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            sendAutoAcknowledgmentEvent()
        });
    }

    function sendAutoAcknowledgmentEvent() {
        $.ajax({
            type: "POST",
            url: site.base_url + 'documentsReception/sendAutoAcknowledgmentEvent',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: "json",
            success: function(response) {
                if (response.status == true) {
                    Command: toastr.success(response.message, '<?= lang("toast_success_title"); ?>', {
                        onHidden: function(reponse) {
                            $('#documentReceptionfilter').submit();
                        }
                    });
                }
            }
        });
    }

    function loadSupplierInput() {
        $('#provider').val('<?= $provider ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        support_document: true
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }
</script>