<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h3 class="modal-title" id="myModalLabel"><?php echo lang('accept_transfer'); ?> <?= $inv->reference_no ?></h3>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("transfers/received/" . $transfer_id, $attrib); ?>
        <div class="modal-body">
            <p><?php echo lang('accept_transfer_heading'); ?></p>
        </div>
        <div class="modal-footer">
            <?php if ($can_accept): ?>
                <?php echo form_submit('accept_transfer', lang('accept_transfer'), 'class="btn btn-primary"'); ?>
            <?php else: ?>
                <em class="text-bold text-danger"  data-toggle="tooltip" data-placement="top" title="<?= lang('cant_receive_transfer_by_permissions_description') ?>"> <i class="fa-solid fa-circle-exclamation"></i> <?= lang('cant_receive_transfer_by_permissions') ?></em>
            <?php endif ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<?= $modal_js ?>