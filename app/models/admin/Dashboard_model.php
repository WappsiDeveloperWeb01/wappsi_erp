<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function delete_view_if_exist($view_name)
	{
		if ($this->db->query("DROP VIEW IF EXISTS {$this->db->dbprefix($view_name)}")) {
			return TRUE;
		}

		return FALSE;
	}

    public function check_view_exist($view_name)
    {
        if ($this->db->table_exists($view_name)) {
            return TRUE;
        }

        return FALSE;
    }

	public function delete_table_if_exist($view_name)
	{
		if ($this->db->query("DROP TABLE IF EXISTS {$this->db->dbprefix($view_name)}")) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_view AS
						SELECT
							count(*) AS number_transactions,
						    s.biller_id,
						    YEAR(s.date) AS year,
						    MONTH(s.date) AS month,
						    DAY(s.date) as day,
						    SUM(COALESCE(IF(s.sale_status != 'returned', s.total, 0),
						            0)) AS subtotal,
						    SUM(COALESCE(IF(s.sale_status != 'returned',
						                s.product_tax,
						                0),
						            0)) AS tax,
						    SUM(COALESCE(IF(s.sale_status != 'returned',
						                s.shipping,
						                0),
						            0)) AS shipping,
						    SUM(COALESCE(IF(s.sale_status != 'returned',
						                s.order_discount,
						                0),
						            0)) AS discount,
						    SUM(COALESCE(IF(s.sale_status = 'returned',
						               s.grand_total,
						                0),
						            0)) AS returned_total,
						    0 as cost_subtotal,
        					0 as cost_total
						FROM
						    sma_sales s
						WHERE
							YEAR(s.date) = {$year} AND s.sale_status != 'returned'
							GROUP BY s.biller_id, MONTH(s.date), DAY(s.date)
						UNION ALL
						SELECT
							count(*) AS number_transactions,
						    s.biller_id,
						    YEAR(s.date) AS year,
						    MONTH(s.date) AS month,
						    DAY(s.date) as day,
						    SUM(COALESCE(IF(s.sale_status != 'returned', s.total, 0),
						            0)) AS subtotal,
						    SUM(COALESCE(IF(s.sale_status != 'returned',
						                s.product_tax,
						                0),
						            0)) AS tax,
						    SUM(COALESCE(IF(s.sale_status != 'returned',
						                s.shipping,
						                0),
						            0)) AS shipping,
						    SUM(COALESCE(IF(s.sale_status != 'returned',
						                s.order_discount,
						                0),
						            0)) AS discount,
						    SUM(COALESCE(IF(s.sale_status = 'returned',
						               s.grand_total,
						                0),
						            0)) AS returned_total,
						    0 as cost_subtotal,
        					0 as cost_total
						FROM
						    sma_sales s
						WHERE
							YEAR(s.date) = {$year} AND s.sale_status = 'returned'
							GROUP BY s.biller_id, MONTH(s.date), DAY(s.date)
						UNION ALL
							SELECT
								0 AS number_transactions,
							    s.biller_id,
							    YEAR(s.date) AS year,
							    MONTH(s.date) AS month,
							    DAY(s.date) AS day,
								0 AS subtotal,
							    0 AS tax,
							    0 AS shipping,
							    0 AS discount,
							    0 AS returned_total,
							    SUM(quantity * purchase_net_unit_cost)  as cost_subtotal,
							    SUM(quantity * purchase_unit_cost) as cost_total
							FROM
							    sma_sales s
							    inner join sma_costing c on c.sale_id = s.id
							WHERE
								YEAR(s.date) = {$year}
							    GROUP BY s.biller_id , MONTH(s.date) , DAY(s.date)");

		$this->db->select('*');
		$query = $this->db->get('sales_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_customers_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_customers_view AS
						SELECT
							MONTH(date) AS month,
							biller_id,
							customer_id,
							customer AS name,
							SUM(total + product_tax) AS subtotal,
							COUNT(customer_id) AS number_transactions
						FROM
							sma_sales
						WHERE
							YEAR(date) = {$year}
							AND sale_status != 'returned'
						GROUP BY MONTH(date) , biller_id , customer_id;");

		$this->db->select('*');
		$query = $this->db->get('sales_customers_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_products_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_products_view AS
						SELECT
						    MONTH(date) AS month,
						    biller_id,
						    product_id,
    						p.name,
						    SUM(si.unit_price * si.quantity) AS subtotal,
						    COUNT(s.id) AS number_transactions,
                            p.reference
						FROM
						    sma_sales s
						    INNER JOIN sma_sale_items si ON si.sale_id = s.id
						    INNER JOIN sma_products p ON p.id = si.product_id
						WHERE
							YEAR(s.date) = {$year}
							AND sale_status != 'returned'
						GROUP BY MONTH(date) , biller_id , product_id;");

		$this->db->select('*');
		$query = $this->db->get('sales_products_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_sellers_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_sellers_view AS
						SELECT
						    MONTH(s.date) AS month,
						    s.biller_id,
						    s.seller_id,
						    c.name,
						    SUM(s.total + product_tax) AS subtotal,
						    COUNT(s.seller_id) AS number_transactions
						FROM
						    sma_sales s
						    INNER JOIN sma_companies c ON c.id = s.seller_id
						WHERE
							YEAR(s.date) = {$year}
							AND sale_status != 'returned'
						GROUP BY MONTH(s.date), s.biller_id , s.seller_id;");

		$this->db->select('*');
		$query = $this->db->get('sales_sellers_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_document_type_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_document_type_view AS
						SELECT
							MONTH(date) AS month,
						    s.document_type_id,
						    s.biller_id,
						    dt.sales_prefix,
						    SUM(s.total) AS subtotal,
						    SUM(s.product_tax) AS tax
						FROM
						    sma_sales s
						    INNER JOIN sma_documents_types dt ON dt.id = s.document_type_id
						WHERE
							YEAR(s.date) = {$year}
							AND sale_status != 'returned'
						GROUP BY s.biller_id, dt.id, MONTH(date)");

		$this->db->select('*');
		$query = $this->db->get('sales_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_tax_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_tax_view AS
						SELECT
							MONTH(date) AS month,
						    s.biller_id,
						    tax_rate_id,
						    t.name AS tax_name,
						    SUM(si.item_tax) AS total_tax
						FROM
							sma_sales s
						    INNER JOIN sma_sale_items si ON si.sale_id = s.id
						    INNER JOIN sma_tax_rates t ON t.id = si.tax_rate_id
						WHERE
							YEAR(s.date) = {$year}
							AND sale_status != 'returned'
						GROUP BY s.biller_id, MONTH(date), tax_rate_id");

		$this->db->select('*');
		$query = $this->db->get('sales_tax_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_mean_payments_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_mean_payments_view AS
						SELECT
							MONTH(s.date) AS month,
						    s.biller_id,
						    pm.code,
						    pm.name,
						    SUM(s.total + product_tax) AS subtotal
						FROM
						    sma_sales s
						    INNER JOIN sma_payments p ON p.sale_id = s.id
						    INNER JOIN sma_payment_methods pm ON pm.code= p.paid_by
						WHERE
							YEAR(s.date) = {$year}
							AND sale_status != 'returned'
						GROUP BY MONTH(s.date), s.biller_id, pm.code;");

		$this->db->select('*');
		$query = $this->db->get('sales_mean_payments_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_brands_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_brands_view AS
						SELECT
							MONTH(s.date) AS month,
							s.biller_id,
							p.brand,
							b.name,
							SUM(si.unit_price * si.quantity) as subtotal,
							COUNT(p.brand) AS number_transactions
						FROM
							sma_sales s
							INNER JOIN sma_sale_items si ON si.sale_id = s.id
							INNER JOIN sma_products p ON p.id = si.product_id
							INNER JOIN sma_brands b ON b.id = p.brand
						WHERE
							YEAR(s.date) = {$year}
								AND sale_status != 'returned'
						GROUP BY MONTH(s.date) , s.biller_id , p.brand;");

		$this->db->select('*');
		$query = $this->db->get('sales_brands_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function create_sales_categories_view()
	{
        $year = date('Y');
		$this->db->query("CREATE VIEW sma_sales_categories_view AS
						SELECT
						    MONTH(s.date) AS month,
						    s.biller_id,
						    p.category_id,
						    c.name,
						    SUM(si.unit_price * si.quantity) AS subtotal,
						    COUNT(p.brand) AS number_transactions
						FROM
						    sma_sales s
					        INNER JOIN sma_sale_items si ON si.sale_id = s.id
					        INNER JOIN sma_products p ON p.id = si.product_id
					    	INNER JOIN sma_categories c ON c.id = p.category_id
					   	WHERE
							YEAR(s.date) = {$year}
								AND sale_status != 'returned'
						GROUP BY MONTH(s.date) , s.biller_id , p.category_id;");

		$this->db->select('*');
		$query = $this->db->get('sales_categories_view');

		if ($query->num_rows() > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function get_sales($months, $month, $branch_office)
	{
		$this->db->select("day,
							SUM(COALESCE(IF(returned_total = 0, number_transactions, 0), 0)) AS amountSales,
							SUM(COALESCE(IF(returned_total < 0, number_transactions, 0), 0)) AS amountNotes,
							SUM(number_transactions) AS number_transactions,
							CAST(SUM(subtotal) AS DECIMAL(25,2)) AS subtotal,
							CAST(SUM(tax) AS DECIMAL(25,2)) AS tax,
							CAST((SUM(subtotal) + SUM(tax)) AS DECIMAL(25,2)) AS total,
							CAST(SUM(shipping) AS DECIMAL(25,2)) AS shipping,
							CAST(SUM(discount) AS DECIMAL(25,2)) AS discount,
							CAST(SUM(returned_total) AS DECIMAL(25,2)) AS return_total,
							CAST(SUM(cost_subtotal) AS DECIMAL(25,2)) AS cost_subtotal,
							CAST(SUM(cost_total) AS DECIMAL(25,2)) AS cost_total");
		$this->db->from('sales_view');

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		$this->db->group_by('day');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_sales_budget($months, $month, $branch_office)
	{
		$this->db->select("SUM(amount) AS amount");
		$this->db->from("sales_budget");
		$this->db->where("year", date("Y"));

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		$query = $this->db->get();

		return $query->row();
	}

	public function get_invoices_type($months, $month, $branch_office, $pos, $fe = 0)
	{
		$this->db->select("COUNT(*) AS amount");
		$this->db->from("sales s");
		$this->db->where("YEAR(date)", date("Y"));
		$this->db->join("documents_types dt", "dt.id = s.document_type_id");

		if (!empty($months) && empty($month)) {
			$this->db->where_in('MONTH(date)', $months);
		}

		if (!empty($month)) {
			$this->db->where('MONTH(date)', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		if ($pos == 1 || $pos == 0) {
			$this->db->where("pos", $pos);
		}

		if ($fe == 1 || $fe == 0) {
			$this->db->where("factura_electronica", $fe);
		}

		$query = $this->db->get();
		return $query->row();
	}

	public function get_customers($months, $month, $branch_office, $customers_search_option, $limit)
	{
		$this->db->select("LOWER(name) as name,
						    SUM(subtotal) AS subtotal,
						    SUM(number_transactions) AS number_transactions");
		$this->db->from('sales_customers_view');

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		$this->db->group_by("customer_id");

		if ($customers_search_option == 1) {
			$this->db->order_by('subtotal', 'DESC');
		} else {
			$this->db->order_by('number_transactions', 'DESC');
		}

		$this->db->limit($limit);

		$query = $this->db->get();

		return $query->result();
	}

	public function get_products($months, $month, $branch_office,  $products_search_option, $groupBy, $limit)
	{
        if ($groupBy == 1) {
		    $this->db->select("LOWER(name) as name,
							SUM(subtotal) AS subtotal,
							SUM(number_transactions) AS number_transactions");
        } else {
            $this->db->select("LOWER(reference) as name,
                SUM(subtotal) AS subtotal,
                SUM(number_transactions) AS number_transactions");
        }

		$this->db->from('sales_products_view');

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

        if ($groupBy == 1) {
		    $this->db->group_by('product_id');
        } else {
		    $this->db->group_by('reference');
        }

		if ($products_search_option == 1) {
			$this->db->order_by('subtotal', 'DESC');
		} else {
			$this->db->order_by('number_transactions', 'DESC');
		}

		$this->db->limit($limit);

		$query = $this->db->get();

		return $query->result();
	}

	public function get_sellers($months, $month, $branch_office, $sellers_search_option, $limit)
	{
		$this->db->select("LOWER(name) as name,
						    sUM(subtotal) AS subtotal,
						    SUM(number_transactions) AS number_transactions");
		$this->db->from('sales_sellers_view');

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		$this->db->group_by("seller_id");

		if ($sellers_search_option == 1) {
			$this->db->order_by('subtotal', 'DESC');
		} else {
			$this->db->order_by('number_transactions', 'DESC');
		}

		$this->db->limit($limit);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_brands($months, $month, $branch_office, $brands_search_option, $limit)
	{
		$this->db->select("LOWER(name) as name,
							sUM(subtotal) AS subtotal,
							SUM(number_transactions) AS number_transactions");
		$this->db->from('sales_brands_view');

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		$this->db->group_by("brand");

		if ($brands_search_option == 1) {
			$this->db->order_by('subtotal', 'DESC');
		} else {
			$this->db->order_by('number_transactions', 'DESC');
		}

		$this->db->limit($limit);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_categories($months, $month, $branch_office, $categories_search_option, $limit)
	{
		$this->db->select("LOWER(name) as name,
							SUM(subtotal) AS subtotal,
							SUM(number_transactions) AS number_transactions");
		$this->db->from('sales_categories_view');

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		$this->db->group_by("category_id");

		if ($categories_search_option == 1) {
			$this->db->order_by('subtotal', 'DESC');
		} else {
			$this->db->order_by('number_transactions', 'DESC');
		}

		$this->db->limit($limit);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_tax($months, $month, $branch_office)
	{
		$this->db->select("tax_name, CAST(SUM(total_tax) AS DECIMAL(25,2)) AS total_tax");
		$this->db->from('sales_tax_view');

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		$this->db->group_by("tax_rate_id");

		$this->db->order_by('total_tax', 'DESC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_payment_means($months, $month, $branch_office)
	{
		$this->db->select("LOWER(name) as name,
							CAST(SUM(subtotal) AS DECIMAL(25,2)) AS subtotal");
		$this->db->from('sales_mean_payments_view');

		if (!empty($months) && empty($month)) {
			$this->db->where_in('month', $months);
		}

		if (!empty($month)) {
			$this->db->where('month', $month);
		}

		if (!empty($branch_office)) {
			$this->db->where('biller_id', $branch_office);
		}

		$this->db->group_by("code");

		$this->db->order_by('subtotal', 'DESC');

		$this->db->limit(15);
		$query = $this->db->get();

		return $query->result();
	}
}

/* End of file Dashboard_model.php */
/* Location: .//C/xampp/htdocs/wappsi_inspinia_implementation/app/models/admin/Dashboard_model.php */