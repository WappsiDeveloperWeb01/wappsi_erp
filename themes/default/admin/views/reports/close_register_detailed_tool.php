<?php

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

//$this->SetTextColor( 255, 0, 0); color rojo
//$this->SetTextColor( 0, 0, 0); color negro

// var_dump($POST);

class PDF extends FPDF
  {

    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Informe de flujo de caja detallado'));
        $this->Image($this->Logo,20,7,38,0);
        $this->SetFont('Arial','B', $this->fontsize_title+1);
        $this->setXY(67.35, 10);
        $this->cell(67.34, 5, $this->sma->utf8Decode('Informe de flujo de caja detallado'), 0, 0, 'C');
        $this->SetFont('Arial','B', $this->fontsize_body);
        $this->setXY(136, 7);
        $this->cell(67.34, 4, $this->sma->utf8Decode('Fecha apertura : ' . $this->sma->hrld($this->register_open_time)), '', 1, 'L');
        $this->setXY(136, 11);
        $this->cell(67.34, 4, $this->sma->utf8Decode('Fecha cierre : ' . $this->sma->hrld($this->register_closed_time)), '', 1, 'L');
        $this->setXY(136, 15);
        $this->cell(67.34, 4, $this->sma->utf8Decode('Responsable cierre : '.$this->username), '', 1, 'L');
        $this->cell(202,4,'','B',1,'C');
        $this->ln(5);
    }

    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina '.$this->PageNo()." de ".'{nb}'),'',1,'C');

    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

}


$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);
$pdf->fontsize_body = 8;
$pdf->fontsize_title = 10;
$pdf->settings = $settings;
$pdf->sma = $this->sma;
$pdf->Logo = base_url('assets/uploads/logos/'.$biller->logo);

$pdf->register_open_time = $register_open_time ? $register_open_time : $this->session->userdata('register_open_time');
$pdf->register_closed_time = $register_closed_time ? $register_closed_time : date('Y-m-d H:i:s');
$pdf->username = $this->session->userdata('username');
$pdf->AddPage();

$total_ingresos = $cash_in_hand;
$total_egresos = 0;

$pdf->SetFont('Arial','B', $pdf->fontsize_title);
$pdf->cell(25, 5, $this->sma->utf8Decode('Saldo inicial :'), '', 0, 'L');
$pdf->cell(182, 5, $this->sma->utf8Decode($this->sma->formatMoney($cash_in_hand)), '', 1, 'L');
$pdf->ln(3);
$pdf->cell(20, 5, $this->sma->utf8Decode('INGRESOS'), '', 1, 'L');
$pdf->ln(3);

foreach ($popts as $popt) {

    if ($popts_data[$popt->code]['RC']) {
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);
        $pdf->cell(202, 5, $this->sma->utf8Decode('Pagos '.$popt->name), 'B', 1, 'L');
        $pdf->SetFont('Arial','B', $pdf->fontsize_body);
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Ref. Factura'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Fecha Factura'), 'B', 1, 'C');
        $pdf->SetFont('Arial','', $pdf->fontsize_body);
        $total_pm = 0;
        $total_pm_dev = 0;
        foreach ($popts_data[$popt->code]['RC'] as $poptsales) {
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->payment_reference), '', 0, 'C');
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($this->sma->formatMoney($poptsales->payment_amount)), '', 0, 'C');
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->payment_date), '', 0, 'C');
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->sale_reference), '', 0, 'C');
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->sale_date), '', 1, 'C');
            $total_ingresos += $poptsales->payment_amount;
            $total_pm += $poptsales->payment_amount > 0 ? $poptsales->payment_amount : 0;
            $total_pm_dev += $poptsales->payment_amount < 0 ? $poptsales->payment_amount : 0;
        }
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_pm)), '', 0, 'C');
        $pdf->ln(3);
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_pm_dev)), '', 0, 'C');
        $pdf->ln(3);
    }
    
    if ($popts_data[$popt->code]['Sales']) {
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);
        $pdf->cell(202, 5, $this->sma->utf8Decode('Ventas '.$popt->name), 'B', 1, 'L');
        $pdf->SetFont('Arial','B', $pdf->fontsize_body);
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Ref. Factura'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Fecha Factura'), 'B', 1, 'C');
        $pdf->SetFont('Arial','', $pdf->fontsize_body);
        $total_pm = 0;
        $total_pm_dev = 0;
        foreach ($popts_data[$popt->code]['Sales'] as $poptsales) {
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->payment_reference), '', 0, 'C');
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($this->sma->formatMoney($poptsales->payment_amount)), '', 0, 'C');
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->payment_date), '', 0, 'C');
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->sale_reference), '', 0, 'C');
            // $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->sale_date), '', 1, 'C');
            $total_ingresos += $poptsales->payment_amount;
            $total_pm += $poptsales->payment_amount > 0 ? $poptsales->payment_amount : 0;
            $total_pm_dev += $poptsales->payment_amount < 0 ? $poptsales->payment_amount : 0;
        }
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_pm)), '', 0, 'C');
        $pdf->ln(3);
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_pm_dev)), '', 0, 'C');
        $pdf->ln(3);
    }
    
}

if ($customer_deposits) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_title);
    $pdf->cell(202, 5, $this->sma->utf8Decode('Anticipos de clientes'), 'B', 1, 'L');
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Cliente'), 'B', 1, 'C');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    foreach ($customer_deposits as $cdeposits) {
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($cdeposits->reference_no), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($cdeposits->amount)), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($cdeposits->date), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($cdeposits->name), 0, 1, 'C');
        $total_ingresos += $cdeposits->amount;
    }
    $pdf->ln(3);
}

if ($pos_register_movements) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_title);
    $pdf->cell(202, 5, $this->sma->utf8Decode('Movimientos de caja'), 'B', 1, 'L');
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(67.34, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
    $pdf->cell(67.34, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
    $pdf->cell(67.34, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 1, 'C');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    foreach ($pos_register_movements as $prmovements) {
        if ($prmovements->amount_in > 0) {
            $pdf->cell(67.34, 5, $this->sma->utf8Decode($prmovements->reference_no), 0, 0, 'C');
            $pdf->cell(67.34, 5, $this->sma->utf8Decode($this->sma->formatMoney($prmovements->amount_in)), 0, 0, 'C');
            $pdf->cell(67.34, 5, $this->sma->utf8Decode($prmovements->date), 0, 1, 'C');
            $total_ingresos += $prmovements->amount_in;
        }
    }
    $pdf->ln(3);
}

$pdf->SetFont('Arial','B', $pdf->fontsize_title);
$pdf->cell(30, 5, $this->sma->utf8Decode('Total ingresos : '), 'B', 0, 'L');
$pdf->cell(172, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_ingresos)), 'B', 1, 'L');
$pdf->ln(3);

$pdf->cell(20, 5, $this->sma->utf8Decode('EGRESOS'), '', 1, 'L');
$pdf->ln(3);

foreach ($popts as $popt) {

    if ($popts_data[$popt->code]['Returned']) {
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);
        $pdf->cell(202, 5, $this->sma->utf8Decode('Devoluciones '.$popt->name), 'B', 1, 'L');
        $pdf->SetFont('Arial','B', $pdf->fontsize_body);
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Ref. Factura'), 'B', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode('Fecha Factura'), 'B', 1, 'C');
        $pdf->SetFont('Arial','', $pdf->fontsize_body);
        foreach ($popts_data[$popt->code]['Returned'] as $poptsales) {
            $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->payment_reference), '', 0, 'C');
            $pdf->cell(40.4, 5, $this->sma->utf8Decode($this->sma->formatMoney($poptsales->payment_amount)), '', 0, 'C');
            $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->payment_date), '', 0, 'C');
            $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->sale_reference), '', 0, 'C');
            $pdf->cell(40.4, 5, $this->sma->utf8Decode($poptsales->sale_date), '', 1, 'C');
            $total_egresos += $poptsales->payment_amount;
        }
        $pdf->ln(3);
    }
    
}

if ($ppayments) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_title);
    $pdf->cell(202, 5, $this->sma->utf8Decode('Pagos a Compras '), 'B', 1, 'L');
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(40.4, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
    $pdf->cell(40.4, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
    $pdf->cell(40.4, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
    $pdf->cell(40.4, 5, $this->sma->utf8Decode('Ref. Factura'), 'B', 0, 'C');
    $pdf->cell(40.4, 5, $this->sma->utf8Decode('Fecha Factura'), 'B', 1, 'C');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    foreach ($ppayments as $ppayment) {
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($ppayment->payment_reference), '', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($this->sma->formatMoney($ppayment->payment_amount)), '', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($ppayment->payment_date), '', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($ppayment->purchase_reference), '', 0, 'C');
        $pdf->cell(40.4, 5, $this->sma->utf8Decode($ppayment->purchase_date), '', 1, 'C');
        $total_egresos += $ppayment->payment_amount;
    }
    $pdf->ln(3);
}

if ($supplier_deposits) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_title);
    $pdf->cell(202, 5, $this->sma->utf8Decode('Anticipos de proveedores'), 'B', 1, 'L');
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Proveedor'), 'B', 1, 'C');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    foreach ($supplier_deposits as $sdeposits) {
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($sdeposits->reference_no), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($sdeposits->amount)), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($sdeposits->date), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($sdeposits->name), 0, 1, 'C');
        $total_egresos += $sdeposits->amount;
    }
    $pdf->ln(3);
}

if ($expenses) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_title);
    $pdf->cell(202, 5, $this->sma->utf8Decode('Gastos POS'), 'B', 1, 'L');
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Proveedor'), 'B', 1, 'C');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    foreach ($expenses as $expense) {
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->reference), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($expense->amount)), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->date), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->supplier_name), 0, 1, 'C');
        $total_egresos += $expense->amount;
    }
    $pdf->ln(3);
}
// exit(var_dump($purchases_expenses));
if ($purchases_expenses) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_title);
    $pdf->cell(202, 5, $this->sma->utf8Decode('Gastos tipo compra'), 'B', 1, 'L');
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Proveedor'), 'B', 1, 'C');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    foreach ($purchases_expenses as $expense) {
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->reference_no), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($expense->amount)), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->date), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->supplier_name), 0, 1, 'C');
        $total_egresos += $expense->amount;
    }
    $pdf->ln(3);
}
if ($purchases_expenses_payments) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_title);
    $pdf->cell(202, 5, $this->sma->utf8Decode('Pagos a Gastos tipo compra'), 'B', 1, 'L');
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 0, 'C');
    $pdf->cell(50.5, 5, $this->sma->utf8Decode('Proveedor'), 'B', 1, 'C');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    foreach ($purchases_expenses_payments as $expense) {
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->reference_no), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($expense->amount)), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->date), 0, 0, 'C');
        $pdf->cell(50.5, 5, $this->sma->utf8Decode($expense->supplier_name), 0, 1, 'C');
        $total_egresos += $expense->amount;
    }
    $pdf->ln(3);
}

if ($pos_register_movements) {
    $pdf->SetFont('Arial','B', $pdf->fontsize_title);
    $pdf->cell(202, 5, $this->sma->utf8Decode('Movimientos de caja'), 'B', 1, 'L');
    $pdf->SetFont('Arial','B', $pdf->fontsize_body);
    $pdf->cell(67.34, 5, $this->sma->utf8Decode('Ref. Pago'), 'B', 0, 'C');
    $pdf->cell(67.34, 5, $this->sma->utf8Decode('Valor Pago'), 'B', 0, 'C');
    $pdf->cell(67.34, 5, $this->sma->utf8Decode('Fecha Pago'), 'B', 1, 'C');
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    foreach ($pos_register_movements as $prmovements) {
        if ($prmovements->amount_out < 0) {
            $pdf->cell(67.34, 5, $this->sma->utf8Decode($prmovements->reference_no), 0, 0, 'C');
            $pdf->cell(67.34, 5, $this->sma->utf8Decode($this->sma->formatMoney($prmovements->amount_out)), 0, 0, 'C');
            $pdf->cell(67.34, 5, $this->sma->utf8Decode($prmovements->date), 0, 1, 'C');
            $total_egresos += $prmovements->amount_out;
        }
    }
    $pdf->ln(3);
}

$pdf->SetFont('Arial','B', $pdf->fontsize_title);
$pdf->cell(30, 5, $this->sma->utf8Decode('Total egresos : '), 'B', 0, 'L');
$pdf->cell(172, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_egresos)), 'B', 1, 'L');
$pdf->cell(30, 5, $this->sma->utf8Decode('Estado final : '), 'B', 0, 'L');
$pdf->cell(172, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_ingresos + $total_egresos)), 'B', 1, 'L');

$descargar = false;
if ($descargar) {
  $pdf->Output("FLUJO_CAJA.pdf", "D");
} else {
  $pdf->Output("FLUJO_CAJA.pdf", "I");
}