<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?= admin_form_open('system_settings/expense_category_actions', 'id="action-form"') ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <?php if ($this->Owner || $this->Admin) { ?>
                                        <li>
                                            <a href="<?php echo admin_url('system_settings/add_expense_category'); ?>" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus"></i> <?= lang('add_expense_category') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo admin_url('system_settings/import_expense_categories'); ?>" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus"></i> <?= lang('import_expense_categories') ?>
                                            </a>
                                        </li>
                                    <?php } else if ($GP['expense_categories-add']) { ?>
                                        <li>
                                            <a href="<?php echo admin_url('system_settings/add_expense_category'); ?>" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus"></i> <?= lang('add_expense_category') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo admin_url('system_settings/import_expense_categories'); ?>" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus"></i> <?= lang('import_expense_categories') ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="CategoryTable" class="table table-bordered table-hover reports-table">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkth" type="checkbox" name="check"/>
                                            </th>
                                            <th><?= $this->lang->line("category_code"); ?></th>
                                            <th><?= $this->lang->line("category_name"); ?></th>
                                            <?php if ($this->Owner || $this->Admin) { ?>
                                                <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                                            <?php } else { ?>
                                                <?php if ($this->GP['expense_categories-edit']) { ?>
                                                    <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                                                <?php }  ?>
                                            <?php }  ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="4" class="dataTables_empty">
                                                <?= lang('loading_data_from_server') ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>

<script>
    $(document).ready(function () {
        $('#CategoryTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('system_settings/getExpenseCategories') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                null,
                <?php if ($this->Owner || $this->Admin) { ?>
                    {"bSortable": false}
                <?php } else { ?>
                    <?php if ($this->GP['expense_categories-edit']) { ?>
                        {"bSortable": false}
                    <?php }  ?>
                <?php }  ?>
            ]
        });

        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

    });
</script>