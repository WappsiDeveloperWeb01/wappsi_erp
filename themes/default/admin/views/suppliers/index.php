<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('suppliers/index', ['id'=>'supplierFilterForm']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="supplier_type_filter"><?= $this->lang->line('supplier_type') ?></label>
                                            <?php $supplierType = (isset($_POST['supplier_type_filter']) ? $_POST['supplier_type_filter'] : '2') ?>
                                            <select class="form-control" name="supplier_type_filter" id="supplier_type_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($typeSuppliers)) : ?>
                                                    <?php foreach ($typeSuppliers as $typeSupplier) : ?>
                                                        <option value="<?= $typeSupplier->id ?>" <?= ($typeSupplier->id == $supplierType) ? 'selected' : '' ?>><?= $typeSupplier->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="type_person_filter"><?= $this->lang->line('type_person') ?></label>
                                            <?php $typePerson = (isset($_POST['type_person_filter']) ? $_POST['type_person_filter'] : '') ?>
                                            <select class="form-control" name="type_person_filter" id="type_person_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($typesPerson)) : ?>
                                                    <?php foreach ($typesPerson as $typeperson) : ?>
                                                        <option value="<?= $typeperson->id ?>" <?= ($typeperson->id == $typePerson) ? 'selected' : '' ?>><?= $typeperson->description ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="tipo_regimen_filter"><?= $this->lang->line('tipo_regimen') ?></label>
                                            <?php $typeRegime = (isset($_POST['tipo_regimen_filter']) ? $_POST['tipo_regimen_filter'] : '') ?>
                                            <select class="form-control" name="tipo_regimen_filter" id="tipo_regimen_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($typesRegime)) : ?>
                                                    <?php foreach ($typesRegime as $typeregime) : ?>
                                                        <option value="<?= $typeregime->id ?>" <?= ($typeregime->id == $typeRegime) ? 'selected' : '' ?> code="<?= $typeregime->codigo ?>"><?= $typeregime->description ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="city_filter"><?= $this->lang->line("city"); ?></label>
                                            <?php $cityId = (isset($_POST['city_filter'])) ? $_POST['city_filter']: '' ?>
                                            <select class="form-control" name="city_filter" id="city_filter">
                                                <option value=""><?= $this->lang->line("allsf") ?></option>
                                                <?php if (!empty($cities)) : ?>
                                                    <?php foreach ($cities as $city): ?>
                                                        <option value="<?= $city->DESCRIPCION ?>" data-code="<?= $city->CODIGO ?>" <?= ($city->DESCRIPCION == $cityId) ? 'selected' : '' ?>><?= $city->DESCRIPCION ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                            <input type="hidden" name="cityCode" id="cityCode" value="<?= $_POST['cityCode'] ?? '' ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="supplier_payment_type_filter"><?= $this->lang->line('supplier_payment_type') ?></label>
                                            <?php $customerPaymentTypeId = (isset($_POST['supplier_payment_type_filter']) ? $_POST['supplier_payment_type_filter'] : '') ?>
                                            <select class="form-control" name="supplier_payment_type_filter" id="supplier_payment_type_filter">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php if (!empty($supplierPaymentsTypes)) : ?>
                                                    <?php foreach ($supplierPaymentsTypes as $supplierPaymentType) : ?>
                                                        <option value="<?= $supplierPaymentType->id ?>" <?= ($supplierPaymentType->id == $customerPaymentTypeId) ? 'selected' : '' ?>><?= $supplierPaymentType->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="generateSupportingDocument" style="margin-top: 25px">
                                                <?php $generateSupportingDocument = (isset($_POST["generateSupportingDocument"])) ? $_POST["generateSupportingDocument"] : ''; ?>
                                                <input type="checkbox" name="generateSupportingDocument" id="generateSupportingDocument" <?= (!empty($generateSupportingDocument) ? 'checked' : '') ?>> <?= $this->lang->line('supporting_document') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="supplierFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="category_filter"><?= $this->lang->line('category'); ?></label>
                                            <?php $categoryId = (isset($_POST['category_filter'])) ? $_POST['category_filter']: '' ?>
                                            <select class="form-control select" name="category_filter" id="category_filter">
                                                <option value=""><?= $this->lang->line("allsf") ?></option>
                                                <?php if (!empty($categories)) : ?>
                                                    <?php foreach ($categories as $category): ?>
                                                        <option value="<?= $category->id ?>" <?= ($category->id == $categoryId) ? 'selected' : '' ?>><?= $category->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="subcategory_filter"><?= $this->lang->line('subcategory'); ?></label>
                                            <?php $subcategoryId = (isset($_POST['subcategory_filter'])) ? $_POST['subcategory_filter']: '' ?>
                                            <select class="form-control select" name="subcategory_filter" id="subcategory_filter">
                                                <option value=""><?= $this->lang->line("allsf") ?></option>
                                                <?php if (!empty($subcategories)) : ?>
                                                    <?php foreach ($subcategories as $subcategory): ?>
                                                        <option value="<?= $subcategory->id ?>" <?= ($subcategory->id == $subcategoryId) ? 'selected' : '' ?>><?= $subcategory->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="subcategorySecondLevel_filter"><?= $this->lang->line('second_level_subcategory_id'); ?></label>
                                            <?php $subcategorySecondLevelId = (isset($_POST['subcategorySecondLevel_filter'])) ? $_POST['subcategorySecondLevel_filter']: '' ?>
                                            <select class="form-control select" name="subcategorySecondLevel_filter" id="subcategorySecondLevel_filter">
                                                <option value=""><?= $this->lang->line("allsf") ?></option>
                                                <?php if (!empty($subcategoriesSecondLevel)) : ?>
                                                    <?php foreach ($subcategoriesSecondLevel as $subcategorySecondLevel): ?>
                                                        <option value="<?= $subcategorySecondLevel->id ?>" <?= ($subcategorySecondLevel->id == $subcategorySecondLevelId) ? 'selected' : '' ?>><?= $subcategorySecondLevel->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="withHoldings_filter"><?= $this->lang->line('withholdings') ?></label>
                                            <?php $withHoldingsArrays = (isset($_POST['withHoldings_filter'])) ? $_POST['withHoldings_filter']: [] ?>
                                            <select class="form-control select" multiple name="withHoldings_filter[]" id="withHoldings_filter">
                                                <option value=""><?= $this->lang->line("allsf") ?></option>
                                                <?php if (!empty($typesWithholdings)) : ?>
                                                    <?php foreach ($typesWithholdings as $typesWithholding): ?>
                                                        <option value="<?= $typesWithholding->id ?>" <?= (in_array($typesWithholding->id, $withHoldingsArrays) ? 'selected' : '') ?>><?= $typesWithholding->name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="taxResident_filter" style="margin-top: 25px">
                                                <?php $taxResident = (isset($_POST["taxResident_filter"])) ? $_POST["taxResident_filter"] : ''; ?>
                                                <input type="checkbox" name="taxResident_filter" id="taxResident_filter" <?= (!empty($taxResident) ? 'checked' : '') ?>> <?= $this->lang->line('tax_resident') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>

    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('suppliers/supplier_actions', 'id="action-form"');
    } ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="SupData" class="table table-hover">
                                        <thead>
                                            <tr class="primary">
                                                <th style="padding-right: 8px">
                                                    <input class="checkbox checkth" type="checkbox" name="check" />
                                                </th>
                                                <th><?= lang("image") ?></th>
                                                <th><?= lang("company"); ?></th>
                                                <th><?= lang("name"); ?></th>
                                                <th><?= lang("email_address"); ?></th>
                                                <th><?= lang("phone"); ?></th>
                                                <th><?= lang("city"); ?></th>
                                                <th><?= lang("country"); ?></th>
                                                <th><?= lang("vat_no"); ?></th>
                                                <th style="width:85px; text-align:center;"><?= lang("actions"); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
        <div style="display: none;">
            <input type="hidden" name="form_action" value="" id="form_action"/>
            <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
        </div>
        <?= form_close() ?>
    <?php } ?>
</div>

<script>
    $(document).ready(function() {
        _tabFilterFill = false;
        var option_filter = false;

        loadDataTables();

        $(document).on('change', '#category_filter', function() { loadSubcategory($(this)) });
        $(document).on('change', '#subcategory_filter', function() { loadSubcategorySecondLevel($(this)) });

        $('[data-toggle="tooltip"]').tooltip();
    });

    function loadDataTables()
    {
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            var nums = [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ];
        }

        oTable = $('#SupData').dataTable({
            aaSorting: [[1, "asc"]],
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            sAjaxSource: '<?= admin_url('suppliers/getSuppliers') ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({"name": "<?= $this->security->get_csrf_token_name() ?>", "value": "<?= $this->security->get_csrf_hash() ?>"});
                aoData.push({"name": "supplier_type_filter", "value": $('#supplier_type_filter').val()});
                aoData.push({"name": "type_person_filter", "value": $('#type_person_filter').val()});
                aoData.push({"name": "tipo_regimen_filter", "value": $('#tipo_regimen_filter').val()});
                aoData.push({"name": "city_filter", "value": $('#city_filter').val()});
                aoData.push({"name": "supplier_payment_type_filter", "value": $('#supplier_payment_type_filter').val()});
                aoData.push({"name": "generateSupportingDocument", "value": ($('#generateSupportingDocument').is(':checked')) ? 1 : 0 });
                aoData.push({"name": "category_filter", "value": $('#category_filter').val()});
                aoData.push({"name": "subcategory_filter", "value": $('#subcategory_filter').val()});
                aoData.push({"name": "subcategorySecondLevel_filter", "value": $('#subcategorySecondLevel_filter').val()});
                aoData.push({"name": "withHoldings_filter", "value": JSON.stringify($('#withHoldings_filter').val())});
                aoData.push({"name": "taxResident_filter", "value": ($('#taxResident_filter').is(':checked')) ? 1 : 0 });
                aoData.push({"name": "option_filter", "value": option_filter });

                $.ajax({ 'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback });
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "supplier_details_link";
                return nRow;
            },
            aoColumns: [
                { "bSortable": false,"mRender": checkbox },
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        if (data !== null) {
                            return '<img class="img-circle" src="assets/uploads/avatars/'+ data +'" style="width: 48px; height: 48px;"/>';
                        } else {
                            return '<img class="img-circle" src="assets/uploads/avatars/defaultUser.jpg" style="width: 48px; height: 48px;"/>';
                        }
                    }
                },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { "bSearchable": false, "bSortable": false }
            ],
            drawCallback: function(settings) {
                if (!_tabFilterFill) {
                    $('.additionalControlsContainer').html('<div class="wizard">' +
                        '<div class="steps clearfix">' +
                            '<ul role="tablist">' +
                                '<li role="tab" class="first current index_list" aria-disabled="false"  aria-selected="true" style="width:16.66%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="active" href="#">'+
                                        '<span class="active_span">0</span><br><?= lang('active_products') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" style="width:16.66%;">' +
                                    '<a class="wizard_index_step" data-optionfilter="all" aria-controls="edit_biller_form-p-0">' +
                                        '<span class="all_span">0</span><br><?= lang('all_products') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.66%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="inactive" href="#"><span class="inactive_span">0</span><br><?= lang('inactive_products') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.66%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="advances" href="#"><span class="advances_span">0</span><br>Anticipos' +
                                    '</a>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>').addClass('col-without-padding');

                    _tabFilterFill = true;
                }

                let exportExcel;
                <?php if ($Owner || $Admin) : ?>
                    exportExcel = '<li>'+
                        '<a href="#" id="excel" data-action="export_excel">'+
                            '<i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>'+
                        '</a>'+
                    '</li>';
                <?php endif ?>

                $('.actionsButtonContainer').html('<a href="<?= admin_url('suppliers/add') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>' +
                    '<div class="pull-right dropdown">'+
                    '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                    '<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">'+
                        '<li>'+
                            '<a href="<?= admin_url('suppliers/import_csv'); ?>" data-toggle="modal" data-target="#myModal">'+
                                '<i class="fa fa-plus-circle"></i> <?= lang("import_by_csv"); ?>'+
                            '</a>'+
                        '</li>'+
                        exportExcel ?? '' +
                    '</ul>'+
                '</div>');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });

                loadDataTabFilters();
            }
        });
    }


    function loadDataTabFilters()
    {
        $.ajax({
            url : site.base_url + 'suppliers/getSuppliers',
            dataType : 'JSON',
            type : 'POST',
            data : {
                '<?=$this->security->get_csrf_token_name()?>' : '<?=$this->security->get_csrf_hash()?>',
                'supplier_type_filter': $('#supplier_type_filter').val(),
                'type_person_filter': $('#type_person_filter').val(),
                'tipo_regimen_filter': $('#tipo_regimen_filter').val(),
                'city_filter': $('#city_filter').val(),
                'supplier_payment_type_filter': $('#supplier_payment_type_filter').val(),
                'generateSupportingDocument': $('#generateSupportingDocument').is(':checked') ? 1 : 0,
                'category_filter': $('#category_filter').val(),
                'subcategory_filter': $('#subcategory_filter').val(),
                'subcategorySecondLevel_filter': $('#subcategorySecondLevel_filter').val(),
                'withHoldings_filter': JSON.stringify($('#withHoldings_filter').val()),
                'taxResident_filter': $('#taxResident_filter').is(':checked') ? 1 : 0,
                'option_filter': option_filter,
                'get_json' : true
            }
        }).done(function(data){
            $('.active_span').text(data.active);
            $('.all_span').text(data.all);
            $('.inactive_span').text(data.inactive);
            $('.advance_span').text(data.inactive);
        });
    }

    function loadSubcategory(element)
    {
        option = '<option value=""><?= $this->lang->line('allsf') ?></option>';
        $('#subcategory_filter').select2('val', '');

        $.ajax({
            type: "GET",
            url: site.base_url + 'products/getSubCategories/'+ element.val(),
            dataType: "JSON",
            success: function (response) {
                if (response) {
                    response.forEach(subcategory => {
                        option += '<option value="'+subcategory.id+'">'+subcategory.text+'</option>'
                    });
                }
                $('#subcategory_filter').html(option);
            }
        });

    }

    function loadSubcategorySecondLevel(element)
    {
        option = '<option value=""><?= $this->lang->line('allsf') ?></option>';
        $('#subcategorySecondLevel_filter').select2('val', '');

        $.ajax({
            type: "GET",
            url: site.base_url + 'products/getSubCategoriesSecondLevel/'+ element.val(),
            dataType: "JSON",
            success: function (response) {
                if (response) {
                    response.forEach(subcategory => {
                        option += '<option value="'+subcategory.id+'">'+subcategory.text+'</option>'
                    });
                }
                $('#subcategorySecondLevel_filter').html(option);
            }
        });
    }
</script>