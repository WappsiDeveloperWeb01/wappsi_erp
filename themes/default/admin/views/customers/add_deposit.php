<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
                <h4 class="modal-title" id="addDepositLabel"><?php echo lang('add_deposit'); ?></h4>
            </div>
            <?php $attrib = array('id'=>'add_deposit', 'target'=>'_blank');
            echo admin_form_open("customers/add_deposit", $attrib); ?>
            <div class="modal-body">
                <p><?= lang('enter_info'); ?></p>
                <div class="row">
                    <div class="col-sm-12">
                        <?php if ($Owner || $Admin) { ?>
                        <div class="form-group">
                            <?php echo lang('date', 'date'); ?>
                            <div class="controls">
                                <?php echo form_input('date', set_value('date', date($dateFormats['php_ldate'])), 'class="form-control datetime" id="date" required="required"'); ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php
                            $bl[""] = "";
                            $bldata = [];
                            foreach ($billers as $biller) {
                                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                $bldata[$biller->id] = $biller;
                            }
                        ?>
                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="form-group">
                                <?= lang("biller", "cdepbiller"); ?>
                                <select name="biller" class="form-control" id="cdepbiller" required="required">
                                    <?php foreach ($billers as $biller): ?>
                                      <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>"><?= $biller->company ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        <?php } else { ?>
                            <div class="form-group">
                                <?= lang("biller", "cdepbiller"); ?>
                                <select name="biller" class="form-control" id="cdepbiller">
                                    <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                        $biller = $bldata[$this->session->userdata('biller_id')];
                                        ?>
                                        <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>" selected><?= $biller->company ?></option>
                                    <?php endif ?>
                                </select>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <?= lang("reference_no", "document_type_id"); ?>
                            <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                        </div>
                        <?php if (isset($cost_centers)): ?>
                            <div class="form-group">
                                <?= lang('cost_center', 'cost_center_id') ?>
                                <?php
                                $ccopts[''] = lang('select');
                                if ($cost_centers) {
                                    foreach ($cost_centers as $cost_center) {
                                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                    }
                                }
                                 ?>
                                 <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                            </div>
                        <?php endif ?>
                        <div class="form-group">
                            <?= lang("customer", "customer"); ?>
                                <?php
                                echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                ?>
                        </div>
                        <div class="form-group">
                            <?php echo lang('value', 'amount'); ?>
                            <div class="controls">
                                <?php echo form_input('amount', set_value('amount'), 'class="form-control" id="amount" required="required"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo lang('paid_by', 'paid_by'); ?>
                            <div class="controls">
                                <select name="paid_by" class="form-control" id="paid_by" required>
                                    <?= $this->sma->paid_opts(NULL, FALSE, FALSE, TRUE); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo lang('note', 'note'); ?>
                            <div class="controls">
                                <?php echo form_textarea('note', set_value('note'), 'class="form-control" id="note"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary add_deposit" type="button"><?= lang('add_deposit') ?></button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<?= $modal_js ?>

<script type="text/javascript">

    <?php if ($company): ?>
        $('#customer').val("<?= $company->id ?>").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
              $.ajax({
                type: "get", async: false,
                url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val(),
                dataType: "json",
                success: function (data) {
                  callback(data[0]);
                }
              });
            },
            ajax: {
              url: site.base_url + "customers/suggestions",
              dataType: 'json',
              quietMillis: 15,
              data: function (term, page) {
                return {
                  term: term,
                  limit: 10
                };
              },
              results: function (data, page) {
                if (data.results != null) {
                  return {results: data.results};
                } else {
                  return {results: [{id: '', text: lang.no_match_found}]};
                }
              }
            }
        });
        $('#customer').select2('readonly', true);
    <?php else: ?>
         $('#customer').select2({
            minimumInputLength: 1,
            ajax: {
                url:  "<?= admin_url('customers/suggestions') ?>",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    <?php endif ?>

    $(document).on('change', '#cdepbiller',function(){
        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/30/") ?>'+$('#cdepbiller').val(),
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        });
    });

    $(document).ready(function(){
        $('#cdepbiller').trigger('change');
    });

    $(document).on('click', '.add_deposit', function() {
        $("#add_deposit").validate({
            ignore: []
        });
        if ($('#add_deposit').valid()) {
            $('.add_deposit').prop('disabled', true);
            $('#add_deposit').submit();
            location.href = site.base_url+'customers/list_deposits';
        }
    });
</script>