<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var is_edit = false;
    var count = 1, an = 1, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>, allow_discount = <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? 1 : 0; ?>,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, shipping = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>;
    var audio_success = new Audio('<?=$assets?>sounds/sound2.mp3');
    var audio_error = new Audio('<?=$assets?>sounds/sound3.mp3');
    $(document).ready(function () {
        <?php if($this->input->get('customer')) { ?>
        if (!localStorage.getItem('quitems')) {
            localStorage.setItem('qucustomer', <?=$this->input->get('customer');?>);
        }
        <?php } ?>
        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('qudate')) {
            $("#qudate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#qudate', function (e) {
            localStorage.setItem('qudate', $(this).val());
        });
        if (qudate = localStorage.getItem('qudate')) {
            $('#qudate').val(qudate);
        }
        <?php } ?>
        $(document).on('change', '#qubiller', function (e) {
            localStorage.setItem('qubiller', $(this).val());
        });
        if (qubiller = localStorage.getItem('qubiller')) {
            $('#qubiller').val(qubiller);
        }
        if (!localStorage.getItem('qutax2')) {
            localStorage.setItem('qutax2', <?=$Settings->default_tax_rate2;?>);
        }
        ItemnTotals();
        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#qucustomer').val() || !$('#qucustomerbranch').val() || !$('#document_type_id').val()) {
                   var msg = "";

                    if (!$('#qucustomer').val()) {
                        msg = "</br><?= lang('customer') ?>";
                    }

                    if (!$('#qucustomerbranch').val()) {
                        msg += "</br><?= lang('customer_branch') ?>";
                    }

                    if (!$('#document_type_id').val()) {
                        msg += "</br><?= lang('reference_no') ?>";
                    }

                    $('#add_item').val('').removeClass('ui-autocomplete-loading');


                    command: toastr.warning('<?=lang('select_above');?> : '+msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    //response('');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('quotes/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#quwarehouse").val(),
                        biller_id: $("#qubiller").val(),
                        customer_id: $("#qucustomer").val(),
                        address_id: $("#qucustomerbranch").val(),
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {

                    if (ui.item.row.cnt_units_prices !== undefined && ui.item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && ui.item.row.promotion != 1) {
                        var item_id = ui.item.item_id;
                        var warehouse_id = $('#quwarehouse').val();
                        $('.product_name_spumodal').text(ui.item.label);
                        $('#sPUModal').appendTo("body").modal('show');
                        add_item_unit(item_id, warehouse_id);
                        $(this).val('');
                    } else {
                        var row = add_invoice_item(ui.item);
                        if (row)
                        $(this).val('');
                    }

                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open_multipart("quotes/add", array('data-toggle' => 'validator', 'role' => 'form')) ?>
                                <input type="hidden" name="tax_exempt_customer" id="tax_exempt_customer">
                                <div class="row">
                                    <?php if ($Owner || $Admin) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("date", "qudate"); ?>
                                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="qudate" required="required"'); ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php
                                        $bl[""] = "";
                                        $bldata = [];
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                            $bldata[$biller->id] = $biller;
                                        }
                                    ?>

                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "qubiller"); ?>
                                                <select name="biller" class="form-control" id="qubiller" required="required">
                                                    <?php foreach ($billers as $biller): ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } else { ?>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "qubiller"); ?>
                                                <select name="biller" class="form-control" id="qubiller">
                                                    <?php
                                                    if (isset($bldata[$this->session->userdata('biller_id')])):

                                                        $biller = $bldata[$this->session->userdata('biller_id')];
                                                        ?>
                                                        <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" selected><?= $biller->company ?></option>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                        </div>

                                    <?php } ?>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("reference_no", "quref"); ?>
                                            <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                        </div>
                                    </div>

                                    <?php if ($Settings->tax2) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("order_tax", "qutax2"); ?>
                                                <?php
                                                $tr[""] = "";
                                                foreach ($tax_rates as $tax) {
                                                    $tr[$tax->id] = $tax->name;
                                                }
                                                echo form_dropdown('order_tax', $tr, (isset($_POST['tax2']) ? $_POST['tax2'] : $Settings->default_tax_rate2), 'id="qutax2" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("order_tax") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="clearfix"></div>

                                    <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("warehouse", "quwarehouse"); ?>
                                                <?php
                                                $wh[''] = '';
                                                foreach ($warehouses as $warehouse) {
                                                    $wh[$warehouse->id] = $warehouse->name;
                                                }
                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="quwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } else {
                                        $warehouse_input = array(
                                            'type' => 'hidden',
                                            'name' => 'warehouse',
                                            'id' => 'quwarehouse',
                                            'value' => $this->session->userdata('warehouse_id'),
                                        );
                                        echo form_input($warehouse_input);
                                    } ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("customer", "qucustomer"); ?>
                                            <div class="input-group">
                                                <?php
                                                echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="qucustomer" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("customer") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                                ?>
                                                <div class="input-group-addon no-print" style="padding: 2px 8px; border-left: 0;">
                                                    <a href="#" id="toogle-customer-read-attr" class="external">
                                                        <i class="fa fa-pencil" id="addIcon" style="font-size: 1.2em;"></i>
                                                    </a>
                                                </div>
                                                <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
                                                    <a id="view-customer">
                                                        <i class="fa fa-eye" id="addIcon" style="font-size: 1.2em;"></i>
                                                    </a>
                                                </div>
                                                <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                    <a href="<?= admin_url('customers/add'); ?>" id="add-customer"class="external" data-toggle="modal" data-target="#myModal">
                                                        <i class="fa fa-plus-circle" id="addIcon" style="font-size: 1.2em;"></i>
                                                    </a>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("customer_branch", "qucustomerbranch"); ?>
                                            <?php
                                            echo form_dropdown('address_id', array('0' => lang("select") . ' ' . lang("customer")), (isset($_POST['address_id']) ? $_POST['address_id'] : ''), 'id="qucustomerbranch" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>

                                    <?php if ($this->Owner || $this->Admin || ($user_group_name != 'seller' && !$this->sma->keep_seller_from_user())): ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("seller", "quseller"); ?>
                                                <?php
                                                echo form_dropdown('seller_id', array('0' => lang("select") . ' ' . lang("biller")), (isset($_POST['seller']) ? $_POST['seller'] : ''), 'id="quseller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                ?>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <?php if ($this->sma->keep_seller_from_user()): ?>
                                            <input type="hidden" name="seller_id"  value="<?= $this->session->userdata('seller_id') ?>">
                                        <?php else: ?>
                                            <input type="hidden" name="seller_id"  value="<?= $this->session->userdata('company_id') ?>">
                                        <?php endif ?>
                                    <?php endif ?>

                                    <div class="col-md-4 row">
                                        <?php if (count($currencies) > 1): ?>
                                            <div class="col-md-6 form-group">
                                                <?= lang('currency', 'currency') ?>
                                                <select name="currency" class="form-control" id="currency">
                                                    <?php foreach ($currencies as $currency): ?>
                                                        <option value="<?= $currency->code ?>" data-rate="<?= $currency->rate ?>" <?= $currency->code == $this->Settings->default_currency ? "selected='selected'" : "" ?>><?= $currency->code ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php else: ?>
                                            <input type="hidden" name="currency" value="<?= $this->Settings->default_currency ?>" id="currency">
                                        <?php endif ?>
                                        <div class="col-md-6 form-group trm-control" style="display: none;">
                                            <?= lang('trm', 'trm') ?>
                                            <input type="number" name="trm" id="trm" class="form-control" required="true" step="0.01">
                                        </div>
                                    </div>



                                    <div class="col-md-12" id="sticker">
                                        <div class="well well-sm">
                                            <div class="form-group" style="margin-bottom:0;">
                                                <div class="input-group wide-tip">
                                                    <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                        <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_product_to_order") . '"'); ?>
                                                        <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                            <a href="#" id="addManually" class="tip"
                                                            title="<?= lang('add_product_manually') ?>"><i
                                                                    class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i></a></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("order_items"); ?> *</label>

                                            <div class="controls table-controls">
                                                <table id="quTable"
                                                    class="table items table-bordered table-condensed table-hover sortable_table">
                                                    <thead>
                                                    <tr>
                                                        <th class="col-md-4"><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                                                        <?php
                                                        if ($Settings->product_serial) {
                                                            echo '<th class="col-md-2">' . lang("serial_no") . '</th>';
                                                        }
                                                        ?>
                                                        <th class="col-md-1"><?= lang('gross_net_unit_price') ?></th>
                                                        <?php
                                                        if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {
                                                            echo '<th class="col-md-1">' . lang("discount") . '</th>';
                                                        }
                                                        ?>
                                                        <th class="col-md-1"><?= lang("price_x_discount"); ?></th>
                                                        <?php
                                                        if ($Settings->tax1) {
                                                            echo '<th class="col-md-1">' . lang("product_tax") . '</th>';
                                                        }
                                                        if ($Settings->ipoconsumo == 1) {
                                                            echo '<th class="col-md-1">' . lang("second_product_tax") . '</th>';
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($Settings->tax1) {
                                                            echo '<th class="col-md-1">' . lang("price_x_tax") . '</th>';
                                                        }
                                                        ?>
                                                        <th class="col-md-1"><?= lang("quantity"); ?></th>
                                                        <!-- <th>
                                                            <?= lang("subtotal"); ?>
                                                            (<span class="currency"><?= $default_currency->code ?></span>)
                                                        </th> -->
                                                        <th>
                                                            <?= lang("total"); ?>
                                                            (<span class="currency"><?= $default_currency->code ?></span>)
                                                        </th>
                                                        <th style="width: 30px !important; text-align: center;">
                                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="total_items" value="" id="total_items" required="required"/>

                                    <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("discount", "qudiscount"); ?>
                                            <?php echo form_input('discount', '', 'class="form-control input-tip" id="qudiscount"'); ?>
                                        </div>
                                    </div>
                                    <?php } ?>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("shipping", "qushipping"); ?>
                                            <?php echo form_input('shipping', '', 'class="form-control input-tip" id="qushipping"'); ?>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("status", "qustatus"); ?>
                                            <?php $st = array('pending' => lang('pending'), 'sent' => lang('sent'));
                                            echo form_dropdown('status', $st, '', 'class="form-control input-tip" id="qustatus"'); ?>

                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("quote_payment_status", "qu_payment_status"); ?>
                                            <?php $st = array('1' => lang('payment_type_cash'), '0' => lang('due'));
                                            echo form_dropdown('payment_status', $st, '', 'class="form-control input-tip" id="qu_payment_status"'); ?>

                                        </div>
                                    </div>

                                    <div class="col-md-4 div_payment_term" style="display: none;">
                                        <div class="form-group">
                                            <?= lang("quote_payment_term", "qu_payment_term"); ?>
                                            <?php echo form_input('payment_term', 1, 'class="form-control input-tip" id="qu_payment_term"'); ?>

                                        </div>
                                    </div>

                                    <div class="col-sm-4 div_paid_by">
                                        <div class="form-group">
                                            <?= lang("paying_by", "paid_by_1"); ?>
                                            <select name="paid_by" id="paid_by_1" data-pbnum="1" class="form-control paid_by">
                                                <?= $this->sma->paid_opts(); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("document", "document") ?>
                                            <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                                data-show-preview="false" class="form-control file">
                                        </div>
                                    </div>

                                    <div class="row" id="bt">
                                        <div class="col-sm-12">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <?= lang("note", "qunote"); ?>
                                                    <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qunote" style="margin-top: 10px; height: 100px;"'); ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-12">
                                        <div class="fprom-group"><?php echo form_submit('add_quote', $this->lang->line("submit"), 'id="add_quote" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?>
                                        </div>
                                        <div class="error_min_sale_amount" style="display: none;">
                                            <br>
                                            <em class="text-danger bold"><?= sprintf(lang('min_sale_amount_error'), lang('quote')) ?></em>
                                            <br>
                                            <br>
                                        </div>
                                    </div>

                                </div>
                                <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                        <tr class="warning">
                                            <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                            <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                            <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                                            <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                                            <?php } ?>
                                            <?php if ($Settings->tax2) { ?>
                                                <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                                            <?php } ?>
                                            <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                                            <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                        </tr>
                                    </table>
                                </div>

                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="pname" class="col-sm-4 control-label"><?= lang('product_name') ?></label>
                        <div class="col-sm-8">
                            <?= form_input('pname', '', 'id="pname" class="form-control"') ?>
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <?php if ($this->Settings->ipoconsumo == 1): ?>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label class="control-label"><?= lang('second_product_tax') ?></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="ptax2" id="ptax2" class="form-control" readonly>
                                </div>
                            </div>
                        <?php endif ?>
                            
                    <?php } ?>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pserial">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <div class="form-group punit_div">
                        <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                        <div class="col-sm-8">
                            <div id="punits-div"></div>
                        </div>
                    </div>
                    <div class="form-group poptions_div">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    </br>
                    <div class="form-group ppreferences_div">
                        <label for="ppreferences" class="col-sm-4 control-label"><?=lang('product_preferences')?></label>
                        <div class="col-sm-8">
                            <div id="ppreferences-div"></div>
                        </div>
                    </div>
                    <div class="col-sm-12"></div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "style='display:none;'" : "" ?>>
                        <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                        </div>
                    </div>
                    <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "" : "style='display:none;'" ?>>
                        <label for="pproduct_unit_price" class="col-sm-4 control-label"><?= lang('sale_form_edit_product_unit_price') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pproduct_unit_price" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                        </div>
                    </div>
                    <?php if ($Settings->ipoconsumo): ?>
                        <div class="form-group">
                            <label for="pprice_ipoconsumo" class="col-sm-4 control-label"><?= lang('unit_price_ipoconsumo') ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pprice_ipoconsumo"><br>
                                <em><?= lang('unit_price_ipoconsumo_detail') ?></em>
                            </div>
                        </div>
                    <?php endif ?>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="net_price"></span></th>
                            <th style="width:25%;"><?= lang('product_taxes'); ?></th>
                            <th style="width:25%;"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_price" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>

                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="mdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?= lang('unit_price') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="mnet_price"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="sPUModal" tabindex="-1" role="dialog" aria-labelledby="sPUModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_spumodal"></h2>
                <h3><?= lang('unit_prices') ?></h3>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 10%;"></th>
                            <th style="width: 35%;"><?= lang('unit') ?></th>
                            <th style="width: 35%;"><?= lang('price') ?></th>
                            <th style="width: 20%;"><?= lang('quantity') ?></th>
                        </tr>
                    </thead>
                </table>
                <table class="table" id="unit_prices_table" style="width: 100%;">
                    <tbody>

                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <div class="col-sm-3">
                    <?= lang('unit_quantity', 'unit_quantity') ?>
                    <input type="text" name="unit_quantity" id="unit_quantity" class="form-control">
                </div>
                <div class="col-sm-9">
                    <button class="btn btn-success send_item_unit_select" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){

        $(document).on('change', '#qubiller', function (e) {
            localStorage.setItem('qubiller', $(this).val());
            customer = $('#qubiller option:selected').data('customerdefault');
            warehouse = $('#qubiller option:selected').data('warehousedefault');
            seller = $('#qubiller option:selected').data('sellerdefault');
            pricegroup = $('#qubiller option:selected').data('pricegroupdefault');
            lcustomer = localStorage.getItem('qucustomer');
            if (customer != '' && customer !== undefined && lcustomer === null) {
                $('#qucustomer').val(customer).select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                      $.ajax({
                        type: "get", async: false,
                        url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                          callback(data[0]);
                        }
                      });
                    },
                    ajax: {
                      url: site.base_url + "customers/suggestions",
                      dataType: 'json',
                      quietMillis: 15,
                      data: function (term, page) {
                        return {
                          term: term,
                          limit: 10
                        };
                      },
                      results: function (data, page) {
                        if (data.results != null) {
                          return {results: data.results};
                        } else {
                          return {results: [{id: '', text: lang.no_match_found}]};
                        }
                      }
                    }
                    });
                localStorage.setItem('qucustomer', customer);
                $('#qucustomer').trigger('change');
            }
            biller = $('#qubiller');
            $.ajax({
                url : site.base_url+"sales/getSellers",
                type : "get",
                data : {"biller_id" : biller.val()}
            }).done(function(data){
                if (data != false) {
                    $('#quseller').html(data).trigger('change');
                } else {
                    $('#quseller').select2('val', '').trigger('change');
                }
            }).fail(function(data){
                console.log(data);
            });
            $.ajax({
              url:'<?= admin_url("billers/getBillersDocumentTypes/7/") ?>'+$('#qubiller').val(),
              type:'get',
              dataType:'JSON'
            }).done(function(data){
              response = data;
              $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                localStorage.setItem('locked_for_biller_resolution', 1);
              }
                $('#document_type_id').trigger('change');
            });
            setTimeout(function() {
                if (warehouse != '' && warehouse !== undefined) {
                    $('#quwarehouse option').each(function(index, option){
                        option = $(this);
                        option.prop('disabled', false);
                    });
                    $('#quwarehouse').select2('val', warehouse);
                }
                if (seller != '') {
                    $('#quseller option').each(function(index, option){
                        if ($(option).val() == seller) {
                            option.setAttribute('selected', true);
                        }
                    });
                    $('#quseller').select2();
                }
                set_biller_warehouses_related();
            }, 850);
        });
        $('#qucustomer').on('change', function(){
            customer = $(this);
            // validate_fe_customer_data();
            $.ajax({
                url: site.base_url+"sales/getCustomerAddresses",
                type: "get",
                data: {"customer_id" : customer.val()}
            }).done(function(data){
                if (data != false) {
                    $('#qucustomerbranch').html(data).select2().trigger('change');
                } else {
                    $('#qucustomerbranch').select2('val', '');
                }
            });
            customer_special_discount();
        });
        $('#qucustomerbranch').on('change', function(){
            setTimeout(function() {
                seller = $('#qucustomerbranch option:selected').data('sellerdefault');
                if (seller) {
                    $('#quseller').select2('val', seller);
                }
            }, 1300);
        });
        $('#qubiller').trigger('change');
    });

    function add_item_unit(item_id, warehouse_id){
            // $('#sPModal').modal('hide');

            var ooTable = $('#unit_prices_table').dataTable({
                aaSorting: [[1, "asc"]],
                aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                iDisplayLength: <?= $Settings->rows_per_page ?>,
                bProcessing: true, 'bServerSide': true,
                sAjaxSource: site.base_url+"pos/itemSelectUnit/"+item_id+"/"+warehouse_id+"/"+$('#qucustomer').val(),
                "bDestroy": true,
                fnServerData: function (sSource, aoData, fnCallback)
                {
                aoData.push({
                  name: "<?= $this->security->get_csrf_token_name() ?>",
                  value: "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback/*, error: function(data) {console.log(data.responseText)}*/ });
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var oSettings = ooTable.fnSettings();
                    //$("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
                    // nRow.id = aData[0];
                    nRow.setAttribute('data-itemunitid', aData[4]);
                    nRow.setAttribute('data-productid', aData[5]);
                    nRow.className = "add_item_unit";
                    //if(aData[7] > aData[9]){ nRow.className = "product_link warning"; } else { nRow.className = "product_link"; }
                    return nRow;
                },
                aoColumns: [
                {bSortable: false, "mRender": radio_2},
                {bSortable: false},
                {bSortable: false},
                {bSortable: false, className : "text-right"},
                {bSortable: false,  bVisible: false},
                {bSortable: false,  bVisible: false},
                ],
                initComplete: function(settings, json)
                {
                console.log(json);
                $('#sPUModal').modal('show');
                }
                }).dtFilter([
                {column_number: 0, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", mdata: []},
                {column_number: 1, filter_default_label: "[<?=lang('price');?>]", filter_type: "text", data: []},
                {column_number: 2, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
                ], "footer");

                $('#unit_prices_table thead').remove();
        }

        $(document).on('click', '.add_item_unit', function() {

            var product_id = $(this).data('productid');
            var unit_price_id = $(this).data('itemunitid');

            var unit_data = {
                                'product_id' : product_id,
                                'unit_price_id' : unit_price_id,
                            };

            localStorage.setItem('unit_data', JSON.stringify(unit_data));

            $('#unit_quantity').val(1).select();

        });

        $(document).on('keyup', '#unit_quantity', function(e){
            if (e.keyCode == 13) {
                if (unit_data = JSON.parse(localStorage.getItem('unit_data'))) {
                    localStorage.removeItem('unit_data');
                    var warehouse_id = $('#quwarehouse').val();
                    var unit_quantity = $(this).val();
                    $.ajax({
                        url:site.base_url+"quotes/iusuggestions",
                        type:"get",
                        data:{
                                'product_id' : unit_data.product_id,
                                'unit_price_id' : unit_data.unit_price_id,
                                'warehouse_id' : warehouse_id,
                                'unit_quantity' : unit_quantity,
                                'biller_id' : $('#qubiller').val(),
                                'customer_id' : $('#qucustomer').val(),
                                'address_id' : $('#qucustomerbranch').val(),
                            }
                    }).done(function(data){
                        add_invoice_item(data);
                        $('#sPUModal').modal('hide');
                    });
                } else {
                    $('#unit_quantity').val(1).select();
                }
            }
        });

        $('#sPUModal').on('shown.bs.modal', function () {
            $('#unit_prices_table_length').remove();
            $('#unit_prices_table_filter').remove();
            $('#unit_prices_table_info').remove();
            $('#unit_prices_table_paginate').remove();
            $('#unit_quantity').val(1);
            $('.select_auto_2:first').iCheck('check').focus();
        });

        $(document).on('hide.bs.modal', '#sPUModal', function (e) {
          e.stopPropagation() // stops modal from being shown
        });

        $(document).on('ifClicked', '.select_auto_2', function(e){
            var index = $('.select_auto_2').index($(this));
            $('.add_item_unit').eq(index).trigger('click');
        });

        $(document).on('keyup', '.select_auto_2', function(e){
            if (e.keyCode == 13) {
                $(this).closest('.add_item_unit').trigger('click');
            }
        });


        $(document).on('click', '.send_item_unit_select', function(){

            var itemunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('itemunitid');
            var productid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productid');

            var unit_data = {
                                'product_id' : productid,
                                'unit_price_id' : itemunitid,
                            };
            var warehouse_id = $('#quwarehouse').val();
            var unit_quantity = $('#unit_quantity').val();
            $.ajax({
                url:site.base_url+"quotes/iusuggestions",
                type:"get",
                data:{
                        'product_id' : unit_data.product_id,
                        'unit_price_id' : unit_data.unit_price_id,
                        'warehouse_id' : warehouse_id,
                        'unit_quantity' : unit_quantity,
                        'biller_id' : $('#qubiller').val(),
                        'customer_id' : $('#qucustomer').val(),
                        'address_id' : $('#qucustomerbranch').val(),
                    }
            }).done(function(data){
                add_invoice_item(data);
                $('#sPUModal').modal('hide');
            });

        });
</script>