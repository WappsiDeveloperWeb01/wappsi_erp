<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $payroll_status = isset($payroll) && !empty($payroll) ? $payroll->status : NULL; ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="payrollDetailTable" class="table table-hover " style="width: 100%">
                                    <thead>
                                        <tr class="primary">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th><?= lang("creation_date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("to_affect") ?></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th><?= lang("employee"); ?></th>
                                            <th><?= lang("payroll_management_earnings"); ?></th>
                                            <th><?= lang("payroll_management_deductions"); ?></th>
                                            <th><?= lang("payroll_management_total_payment"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody class="pointer">
                                        <tr>
                                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        load_datatable();

        $(document).on('click', '.see_employee_concepts td:not(:last-child, :nth-child(9))', function() {
            var electronic_payroll_employee_id = $(this).parent('.see_employee_concepts').attr('electronic_payroll_employee_id');
            var employee_id = $(this).parent('.see_employee_concepts').attr('employee_id');
            var contract_id = $(this).parent('.see_employee_concepts').attr('contract_id');

            $('#myModal').modal({remote: '<?= admin_url('payroll_electronic/see_employee_concepts/') ?>'+ electronic_payroll_employee_id +'/'+ employee_id +'/'+ contract_id});
            $('#myModal').modal('show');
        });

        $(document).on('click', '.information_label', function() { get_electronic_payroll_information($(this).data('id')); });
        $(document).on('click', '.send_electronic_payroll, .send_delete_electronic_payroll, .send_adjustment_electronic_payroll_document', function () { open_wait_modal(); });
        $(document).on('click', '.delete_electronic_payroll_document a', function(e) { delete_electronic_payroll(e, $(this)); });
        $(document).on('click', '.adjustment_electronic_payroll_document a', function(e) { adjustment_electronic_payroll_document(e, $(this)); });
        $(document).on('click', '#multiple_send_electronic_payroll', function(e) { multiple_send_electronic_payroll(e); });
    });

    function load_datatable()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#payrollDetailTable').dataTable({
            aaSorting: [[4, "desc"]],
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true,
            bServerSide: true,
            sAjaxSource: '<?= admin_url('payroll_electronic/get_electronic_payroll_items_datatables/'. $electronic_payroll_id) ?>',
            dom: '<"col-sm-7 containerBtn"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6">',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                nRow.setAttribute("electronic_payroll_employee_id", aData[0]);
                nRow.setAttribute("employee_id", aData[2]);
                nRow.setAttribute("contract_id", aData[16]);
                nRow.className = "see_employee_concepts";
                return nRow;
            },
            drawCallback: function(settings) {
                <?php if ($this->Owner || $this->Admin) { ?>
                    <?php if ($payroll_status == APPROVED || $payroll_status == NULL) { ?>
                        $('.actionsButtonContainer').html(`<div class="dropdown pull-right" style="margin-top: 5px; margin-right: 1px;">
                            <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>">
                                <i class="fas fa-ellipsis-v fa-lg"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu">
                                <li>
                                    <a id="multiple_send_electronic_payroll" href="<?= admin_url("payroll_electronic/multiple_send_electronic_payroll") ?>">
                                        <i class="fa fa-paper-plane"></i> <?= lang('multiple_send_electronic_payroll') ?>
                                    </a>
                                </li>
                            </ul>
                        </div>`);
                    <?php } ?>
                <?php } else { ?>
                    <?php if ($GP['payroll_management-add']) { ?>
                        <?php if ($payroll_status == APPROVED || $payroll_status == NULL) { ?>
                            $('.actionsButtonContainer').html(`<div class="dropdown pull-right" style="margin-top: 5px; margin-right: 1px;">
                                <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>">
                                    <i class="fas fa-ellipsis-v fa-lg"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu">
                                    <li>
                                        <a id="multiple_send_electronic_payroll" href="<?= admin_url("payroll_electronic/multiple_send_electronic_payroll") ?>">
                                            <i class="fa fa-paper-plane"></i> <?= lang('multiple_send_electronic_payroll') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>`);
                        <?php } ?>
                    <?php } ?>
                <?php } ?>

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            },
            aoColumns: [
                { bSortable: false, mRender: checkbox},
                { visible: false},
                { visible: false},
                { className: 'text-center' },
                { className: 'text-center' },
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        var type = row[14];
                        var support_document_adjustment = data;
                        var support_document_elimination = row[6];
                        var adjustment_document = row[7];
                        var elimination_document = row[8];
                        if (type == <?= SUPPORT_DOCUMENT ?>) {
                            return ((support_document_adjustment !== null) ? support_document_adjustment+',' : '') + ((support_document_elimination !== null) ? support_document_elimination+',' : '').replace(/,\s*$/, "");
                        } else if (type == <?= ADJUSTMENT_DOCUMENT ?>) {
                            return adjustment_document;
                        } else if (type == <?= ELIMINATION_DOCUMENT ?>) {
                            return elimination_document;
                        }
                    }
                },
                { visible: false},
                { visible: false},
                { visible: false},
                { className: 'text-center, text-capitalize' },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        var status = data;
                        var payroll_electronic_employee_id = row[0];

                        if (status == <?= NOT_SENT ?>) {
                            information_label = '<span class="label label-danger" data-id="'+payroll_electronic_employee_id+'"><?= lang("not_sent") ?></span>';
                        } else if (status == <?= PENDING ?>) {
                            information_label = '<span class="label label-warning information_label" data-id="'+payroll_electronic_employee_id+'"><?= lang("pending") ?></span>';
                        } else {
                            information_label = '<span class="label label-success information_label" data-id="'+payroll_electronic_employee_id+'"><?= lang("accepted") ?></span>';
                        }
                        return information_label;
                    }
                },
                { visible: false},
                { visible: false},
                { visible: false},
                {
                    bSortable: false,
                    render: function(data, type, row) {
                        var actions = $(data);
                        var status = row[13];
                        var type = row[14];
                        var adjustment_document = row[5];
                        var elimination_document = row[6];
                        var warnings = row[15];

                        <?php if ($this->Owner || $this->Admin) { ?>
                            if (type == '<?= SUPPORT_DOCUMENT ?>') {
                                actions.find('.dropdown-menu').find('.nee_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.nea_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.send_delete_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.send_adjustment_electronic_payroll_document a').remove();
                                if (status == '<?= ACCEPTED ?>') {
                                    actions.find('.dropdown-menu').find('.send_electronic_payroll a').remove();
                                    if (elimination_document !== null) {
                                        actions.find('.dropdown-menu').find('.delete_electronic_payroll_document a').remove();
                                    }
                                } else {
                                    actions.find('.dropdown-menu').find('.delete_electronic_payroll_document a').remove();
                                    actions.find('.dropdown-menu').find('.adjustment_electronic_payroll_document a').remove();
                                    actions.find('.dropdown-menu').find('.pay_slip a').remove();
                                }
                            } else if (type == '<?= ADJUSTMENT_DOCUMENT ?>') {
                                actions.find('.dropdown-menu').find('.pay_slip a').remove();
                                actions.find('.dropdown-menu').find('.check_document_status a').remove();
                                actions.find('.dropdown-menu').find('.ne_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.nee_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.send_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.send_delete_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.delete_electronic_payroll_document a').remove();
                                actions.find('.dropdown-menu').find('.adjustment_electronic_payroll_document a').remove();

                                if (status == '<?= ACCEPTED ?>') {
                                    actions.find('.dropdown-menu').find('.send_adjustment_electronic_payroll_document a').remove();
                                }
                            } else if (type == '<?= ELIMINATION_DOCUMENT ?>') {
                                actions.find('.dropdown-menu').find('.pay_slip a').remove();
                                actions.find('.dropdown-menu').find('.ne_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.nea_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.see_employee_concepts a').remove();
                                actions.find('.dropdown-menu').find('.check_document_status a').remove();
                                actions.find('.dropdown-menu').find('.send_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.delete_electronic_payroll_document a').remove();
                                actions.find('.dropdown-menu').find('.adjustment_electronic_payroll_document a').remove();
                                actions.find('.dropdown-menu').find('.send_adjustment_electronic_payroll_document a').remove();

                                if (status == '<?= ACCEPTED ?>') {
                                    actions.find('.dropdown-menu').find('.send_delete_electronic_payroll a').remove();
                                }
                            }
                        <?php } else { ?>
                            <?php if ($GP['payroll_management-add'] == 0) { ?>
                                actions.find('.dropdown-menu').find('.ne_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.nee_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.nea_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.see_employee_concepts a').remove();
                                actions.find('.dropdown-menu').find('.check_document_status a').remove();
                                actions.find('.dropdown-menu').find('.send_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.send_delete_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.delete_electronic_payroll_document a').remove();
                                actions.find('.dropdown-menu').find('.adjustment_electronic_payroll_document a').remove();
                                actions.find('.dropdown-menu').find('.send_adjustment_electronic_payroll_document a').remove();
                            <?php } else {?>
                                if (type == '<?= SUPPORT_DOCUMENT ?>') {
                                actions.find('.dropdown-menu').find('.nee_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.nea_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.send_delete_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.send_adjustment_electronic_payroll_document a').remove();
                                if (status == '<?= ACCEPTED ?>') {
                                    actions.find('.dropdown-menu').find('.send_electronic_payroll a').remove();
                                    if (elimination_document !== null) {
                                        actions.find('.dropdown-menu').find('.delete_electronic_payroll_document a').remove();
                                    }
                                    if (adjustment_document !== null) {
                                        actions.find('.dropdown-menu').find('.adjustment_electronic_payroll_document a').remove();
                                    }
                                } else {
                                    actions.find('.dropdown-menu').find('.delete_electronic_payroll_document a').remove();
                                    actions.find('.dropdown-menu').find('.adjustment_electronic_payroll_document a').remove();
                                }
                            } else if (type == '<?= ADJUSTMENT_DOCUMENT ?>') {
                                actions.find('.dropdown-menu').find('.ne_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.nee_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.send_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.send_delete_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.adjustment_electronic_payroll_document a').remove();

                                if (status == '<?= ACCEPTED ?>') {
                                    actions.find('.dropdown-menu').find('.send_adjustment_electronic_payroll_document a').remove();
                                }
                            } else if (type == '<?= ELIMINATION_DOCUMENT ?>') {
                                actions.find('.dropdown-menu').find('.ne_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.nea_previous_json a').remove();
                                actions.find('.dropdown-menu').find('.see_employee_concepts a').remove();
                                actions.find('.dropdown-menu').find('.check_document_status a').remove();
                                actions.find('.dropdown-menu').find('.send_electronic_payroll a').remove();
                                actions.find('.dropdown-menu').find('.delete_electronic_payroll_document a').remove();
                                actions.find('.dropdown-menu').find('.adjustment_electronic_payroll_document a').remove();
                                actions.find('.dropdown-menu').find('.send_adjustment_electronic_payroll_document a').remove();

                                if (status == '<?= ACCEPTED ?>') {
                                    actions.find('.dropdown-menu').find('.send_delete_electronic_payroll a').remove();
                                }
                            }
                            <?php } ?>
                        <?php } ?>

                        var data = actions.prop('outerHTML');
                        return data;
                    }
                }
            ]
        });
    }

    function get_electronic_payroll_information(id)
    {
        $.ajax({
            type: "GET",
            url: "<?= admin_url("payroll_electronic/get_electronic_payroll_information"); ?>",
            data: {
                "id": id
            },
            dataType: "JSON",
            success: function (response) {
                message_type = (response.statusCode == '200') ? 'success' : 'warning';
                message_text = (response.statusCode == '200') ? response.messageDescription :
                swal({
                    title: response.messageTitle,
                    text: response.messageDescription,
                    type: message_type,
                    confirmButtonText: "¡Ok!",
                    closeOnConfirm: true,
                    closeOnClickOutside: false,
                });
            }
        });
    }

    function open_wait_modal()
    {
        swal({
            title: 'Enviando Documento.',
            text: 'Por favor, espere un momento',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });
    }

    function delete_electronic_payroll(e, element)
    {
        e.preventDefault();
        swal({
            title: "¿Seguro de Eliminar la Nómina electrónica?",
            text: "Después de eliminar no se podrá enviar el documento.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Eliminar!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: 'Enviando Documento de eliminación.',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });
            window.location.href = element.attr('href');
        });
    }

    function adjustment_electronic_payroll_document(event, element)
    {
        event.preventDefault();
        swal({
                title: "¿Seguro de Ajustar la Nómina electrónica?",
                text: "Se creará un nuevo documento de ajuste para la Nómina seleccionada",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "Cancelar",
                confirmButtonText: "¡Si, continuar!",
                closeOnConfirm: false,
            }, function () {
                swal({
                    title: 'Redireccionando al formulario documento de ajuste.',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false,
                });
                window.location.href = element.attr('href');
            });
    }

    function multiple_send_electronic_payroll(event)
    {
        event.preventDefault();

        if (validateSelectedCredits()) {
            let selectedCredits = getSelectedCredits()
            $.ajax({
                type: 'post',
                url: site.base_url+'Payroll_electronic/multiple_send_electronic_payroll',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'payrolls': selectedCredits
                },
                dataType: 'json',
                success: function (response) {
                    message = (response.message != '') ? response.message: ``
                    swal({
                        title: `<?= lang('toast_warning_title') ?>`,
                        text: `<?= lang('Proceso de Notificación exitoso.') ?> ${message}`,
                        type: `success`,
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: `Ok`
                    }, function(result) {
                        window.location.reload()
                    })
                }
            });
        }
    }

    function validateSelectedCredits() {
        let checksSelected = 0

        $('.multi-select').each(function(index, check) {
            if ($(check).is(':checked')) {
                checksSelected++
            }
        })

        if (checksSelected == 0) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= lang('No se ha seleccionado ninguna Nómina para realizar el proceso.') ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                return false
            })
        } else {
            return true
        }
    }

    function getSelectedCredits() {
        let selected = []

        $('.multi-select').each(function(index, check) {
            if ($(check).is(':checked')) {
                selected.push($(this).val())
            }
        })

        return selected
    }
</script>
