<?php
/**
 *
 */
class Mobile extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('pos_model');
        $this->load->admin_model('products_model');
        $this->load->admin_model('sales_model');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('Electronic_billing_model');

        $this->load->library('qr_code');
    }

    public function consume(){
        var_dump($this->app_add_sale());
    }

    public function app_add_sale()
    {
        $from_app = $this->input->post('from_app');
        $created_by = $this->input->post('created_by');
        if ($from_app == 1) {
            if (!$this->input->post('document_type_id')) {
                echo json_encode(['error' => 1, 'message' => 'No se especificó tipo de documento para la venta']);
                exit();
            }
            if (!$this->input->post('payment_document_type_id')) {
                echo json_encode(['error' => 1, 'message' => 'No se especificó tipo de documento para el pago']);
                exit();
            }
            if (!$this->input->post('biller')) {
                echo json_encode(['error' => 1, 'message' => 'No se especificó Sucursal']);
                exit();
            }
            if (!$this->input->post('warehouse')) {
                echo json_encode(['error' => 1, 'message' => 'No se especificó Bodega']);
                exit();
            }
            if (!$this->input->post('customer')) {
                echo json_encode(['error' => 1, 'message' => 'No se especificó Cliente']);
                exit();
            }
            if (!$this->input->post('address_id')) {
                echo json_encode(['error' => 1, 'message' => 'No se especificó Sucursal de cliente']);
                exit();
            }
            $date = date('Y-m-d H:i:s');
            $except_category_taxes = $this->input->post('except_category_taxes') ? true : false;
            $tax_exempt_customer = $this->input->post('tax_exempt_customer') ? true : false;
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $address_id = $this->input->post('address_id');
            $sale_currency = $this->input->post('sale_currency');
            // exit($customer_id.", ".$address_id);
            if (!$this->site->getCustomerBranchByCustomerAndAddresId($customer_id, $address_id)) {
                echo json_encode(['error' => 1, 'message' => lang('invalid_customer_branch')]);
                exit();
            }
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                echo json_encode(['error' => 1, 'message' => lang('biller_without_cost_center')]);

                exit();
            }
            $sale_tip_amount = $this->input->post('sale_tip_amount');
            $restobar_table = $this->input->post('restobar_table');
            $shipping_in_grand_total = $this->input->post('shipping_in_grand_total');
            $seller_id = $this->input->post('seller_id');
            $total_items = $this->input->post('total_items');
            $document_type_id = $this->input->post('document_type_id');
            $sale_status = 'completed';
            $payment_status = 'due';
            $payment_term = 0;
            $due_date = date('Y-m-d', strtotime('+' . $payment_term . ' days'));
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = $customer_details->name != '-'  ? $customer_details->name : $customer_details->company;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('pos_note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $digital = FALSE;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            $txt_pprices_changed = false;
            $new_products = [];
            $cancelled_products = [];
            $preparation_area_products = NULL;
            for ($r = 0; $r < $i; $r++) {
                $item_state_readiness = NULL;
                $item_id = $_POST['product_id'][$r] ?? null;
                $ignore_hide_parameters = isset($_POST['ignore_hide_parameters'][$r]) ? $_POST['ignore_hide_parameters'][$r] : NULL;
                $item_type = $_POST['product_type'][$r] ?? null;
                $item_code = $_POST['product_code'][$r] ?? null;
                $item_name = $_POST['product_name'][$r] ?? null;
                $item_comment = $_POST['product_comment'][$r] ?? null;
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : NULL;
                $real_unit_price = $_POST['real_unit_price'][$r] ?? null;
                $unit_price = $_POST['unit_price'][$r] ?? null;
                $item_unit_quantity = $_POST['quantity'][$r] ?? null;
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : 1;
                $product_tax_rate = isset($_POST['product_tax_rate'][$r]) ? $_POST['product_tax_rate'][$r] : 1; /**/
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null; /**/
                $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                $product_tax_rate_2 = isset($_POST['product_tax_rate_2'][$r]) ? $_POST['product_tax_rate_2'][$r] : null; /**/
                $item_unit_tax_val_2 = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : null; /**/
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : NULL;
                $item_unit = $_POST['product_unit'][$r] ?? null;
                $item_quantity = $_POST['product_base_quantity'][$r] ?? null;
                $under_cost_authorized = $_POST['under_cost_authorized'][$r] ?? null;
                $item_aquantity = $_POST['product_aqty'][$r] ?? null;
                $item_preferences = isset($_POST['product_preferences_text'][$r]) ? $_POST['product_preferences_text'][$r] : null;
                $item_is_new = isset($_POST['product_is_new'][$r]) ? $_POST['product_is_new'][$r] : NULL;
                $item_net_price = $_POST['net_price'][$r] ?? null; /**/
                $pr_item_discount = isset($_POST['product_discount_val'][$r]) ? $_POST['product_discount_val'][$r] : null; /**/
                $product_unit_id_selected = isset($_POST['product_unit_id_selected'][$r]) ? $_POST['product_unit_id_selected'][$r] : null; /**/
                $price_before_promo = isset($_POST['price_before_promo'][$r]) ? $_POST['price_before_promo'][$r] : null; /**/
                if ($item_is_new == 'true') {
                    $new_products[$item_id] = 1;
                }
                if ($item_state_readiness == 3) {
                    $cancelled_products[$item_id] = 1;
                }
                if (($item_code  && $item_quantity && ($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2) > 0) || $ignore_hide_parameters == 1) {
                    $product_details = $item_type != 'manual' ? $this->pos_model->getProductByCode($item_code) : NULL;
                    if ($item_type == 'digital') {
                        $digital = TRUE;
                    }
                    $pr_item_tax = $this->sma->formatDecimal((($item_unit_tax_val) * $item_quantity));
                    $pr_item_tax_2 = $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_quantity));
                    $unit = $this->site->getUnitByID($item_unit);
                    $subtotal = $this->sma->formatDecimalNoRound(($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2) * $item_quantity);
                    $product_discount += $pr_item_discount;
                    $product_tax += ($pr_item_tax + $pr_item_tax_2);
                    $product_data = $this->site->getProductByID($item_id);
                    if (!$unit) {
                        $unit = $this->site->getUnitByID($product_data->unit);
                    }
                    $p_unit_id = NULL;
                    if ($product_unit_id_selected > 0) {
                        $p_unit_id = $product_unit_id_selected;
                    } else if ($unit) {
                        $p_unit_id = $unit->id;
                    }


                    $product = array(
                        'product_id'      => $item_id,
                        'product_code'    => $item_code,
                        'product_name'    => $item_name,
                        'net_unit_price'  => $item_net_price,
                        'unit_price'      => $this->sma->formatDecimal($item_net_price + $item_unit_tax_val),
                        'quantity'        => $item_quantity,
                        'warehouse_id'    => $warehouse_id,
                        'item_tax'        => $pr_item_tax,
                        'tax_rate_id'     => $item_tax_rate,
                        'tax'             => $product_tax_rate,
                        'item_tax_2'      => $pr_item_tax_2,
                        'tax_rate_2_id'   => $item_tax_rate_2,
                        'tax_2'           => $product_tax_rate_2,
                        'discount'        => $item_discount,
                        'item_discount'   => $pr_item_discount,
                        'subtotal'        => $subtotal,
                        'serial_no'       => $item_serial,
                        'option_id'       => $item_option,
                        'product_type'    => $item_type,
                        'real_unit_price' => $real_unit_price,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity'   => $item_unit_quantity,
                        'comment'         => $item_comment,
                        'preferences'     => $item_preferences,
                        'state_readiness' => empty($item_state_readiness) ? '1' : $item_state_readiness,
                        'price_before_tax' => $item_net_price + ($pr_item_discount / $item_quantity),
                        'product_unit_id_selected'   => $p_unit_id,
                        'consumption_sales'   => $item_unit_tax_val_2,
                        'price_before_promo'   => $price_before_promo,
                        'under_cost_authorized'   => $under_cost_authorized,
                    );
                    if ($item_fixed = $this->site->fix_item_tax($document_type_id, $product)) {
                        $product_tax -= ($pr_item_tax + $pr_item_tax_2);
                        unset($product);
                        $product = $item_fixed;
                        $product_tax += ($item_fixed['item_tax'] + $item_fixed['item_tax_2']);
                        $item_net_price = $item_fixed['net_unit_price'];
                    }
                    $products[] = ($product + $gst_data);
                    if ($this->pos_settings->restobar_mode == '1') {
                        if ($item_state_readiness != CANCELLED) {
                            $total += $this->sma->formatDecimalNoRound(($item_net_price * $item_quantity));
                        }
                    } else {
                        $total += $this->sma->formatDecimalNoRound(($item_net_price * $item_quantity));
                    }
                    // ORDENES DE PREPARACION A ÁREAS DE PREPARACIÓN
                    if ($this->pos_settings->restobar_mode == 1 && $this->pos_settings->order_print_mode == 2) {
                        if ($item_is_new == 'true' && isset($_POST['preparation_area'][$r]) && $_POST['preparation_area'][$r] && $_POST['preparation_area'][$r] != 'null') {
                            $preparation_area_products[$_POST['preparation_area'][$r]][] = $product;
                        }
                    }
                    // ORDENES DE PREPARACION A ÁREAS DE PREPARACIÓN
                } else {
                    echo json_encode(['error' => 1, 'message' => lang('invalid_products_data')]);
                    exit();
                }
            } //FIN RECORRIDO ITEMS

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } elseif ($this->pos_settings->item_order == 1) {
                krsort($products);
            }
            $order_discount = $this->site->calculateDiscount($this->input->post('discount'), ($total));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimalNoRound(($total + $total_tax + $this->sma->formatDecimal($shipping_in_grand_total == 1 ? $shipping : 0) - $order_discount + $sale_tip_amount));
            // exit(var_dump($grand_total));
            $rounding = 0;
            if ($this->pos_settings->rounding) {
                $round_total = $this->sma->roundNumber($grand_total, $this->pos_settings->rounding);
                $rounding = $this->sma->formatMoney($round_total - $grand_total);
            }
            // Retenciones
            $paid = $this->input->post('amount-paid') ? $this->input->post('amount-paid') : 0;
            $rete_applied = $this->input->post('rete_applied');
            if ($rete_applied) {
                $paid = $paid;
            }
            // Retenciones
            $document_type = $this->site->getDocumentTypeById($document_type_id);
            if ($this->pos_settings->activate_electronic_pos == 1 && (($this->pos_settings->mode_electronic_pos == 2 && $total > $this->pos_settings->uvt_value) || $this->pos_settings->mode_electronic_pos == 1) ) {
                if ($document_type->factura_electronica == 0) {
                    $dt_fe = $this->db->select('documents_types.*')
                                ->join('documents_types', 'documents_types.id = biller_documents_types.document_type_id')
                                ->where('biller_documents_types.biller_id', $biller_id)
                                ->where('documents_types.factura_electronica', 1)
                                ->where('documents_types.module', 1)
                                ->limit(1)->get('biller_documents_types');
                    if ($dt_fe->num_rows() > 0) {
                        $dt_fe = $dt_fe->row();
                        $document_type_id = $dt_fe->id;
                        $document_type = $this->site->getDocumentTypeById($document_type_id);
                    } else {
                        echo json_encode(['error' => 1, 'message' => lang('sale_total_required_fe_dt_not_exists')]);
                        exit();
                    }
                }
            }


            $suspend = $this->input->post('suspend_sale_id') ? true : false;
            if (!$suspend) {
                $resolucion = "";
                if ($document_type->save_resolution_in_sale == 1) {
                    $resolucion = $this->site->textoResolucion($document_type);
                }
            } else {
                $resolucion = "";
            }
            $payment_term = $this->input->post('payment_term');
            if ($except_category_taxes) {
                $note.=lang('except_category_taxes_text');
            }
            $autorrete_amount = 0;
            $autorrete_perc = 0;
            if ($this->Settings->self_withholding_percentage > 0 && $document_type->key_log == 1) {
                $autorrete_perc_op = $this->Settings->self_withholding_percentage / 100;
                $autorrete_amount = $total * $autorrete_perc_op;
                $autorrete_perc = $this->Settings->self_withholding_percentage;
            }

            $data = array('date'  => $date,
                'customer_id'       => $customer_id,
                'customer'          => $customer,
                'address_id'        => $address_id,
                'biller_id'         => $biller_id,
                'biller'            => $biller,
                'seller_id'         => $seller_id,
                'warehouse_id'      => $warehouse_id,
                'note'              => $note,
                'staff_note'        => $staff_note,
                'total'             => $total,
                'product_discount'  => $product_discount,
                'order_discount_id' => $this->input->post('discount'),
                'order_discount'    => $order_discount,
                'total_discount'    => $total_discount,
                'product_tax'       => $product_tax,
                'order_tax_id'      => $this->input->post('order_tax'),
                'order_tax'         => $order_tax,
                'total_tax'         => $total_tax,
                'shipping'          => $this->sma->formatDecimal($shipping),
                'grand_total'       => $this->sma->formatDecimal($grand_total, 2),
                'payment_term'      => $payment_term,
                'total_items'       => $total_items,
                'sale_status'       => $sale_status,
                'payment_status'    => $payment_status,
                'payment_term'      => $payment_term,
                'rounding'          => $rounding,
                'suspend_note'      => $this->input->post('suspend_note'),
                'pos'               => 1,
                'paid'              => $paid,
                // 'created_by' => $created_by,
                'created_by'        => $created_by,
                'hash'              => hash('sha256', microtime() . mt_rand()),
                'resolucion'        => $resolucion,
                'document_type_id'  => $document_type_id,
                'tip_amount'        => $sale_tip_amount,
                'shipping_in_grand_total' => $shipping_in_grand_total,
                'restobar_table_id' => isset($restobar_table) && $restobar_table ? $restobar_table : NULL,
                'self_withholding_amount' => $autorrete_amount,
                'self_withholding_percentage' => $autorrete_perc,
                'sale_origin_reference_no' => $this->input->post('sale_origin_reference_no'),
                'sale_origin' => 'suspend_sale',
                'sale_currency' => !empty($sale_currency) ? $sale_currency : $this->Settings->default_currency
            );

            $biller_data = $this->site->getAllCompaniesWithState('biller', $biller_id);
            if ($this->Settings->modulary && $biller_data->rete_autoica_percentage > 0) {
                $autoica_perc_op = $biller_data->rete_autoica_percentage / 100;
                $autoica_amount = $total * $autoica_perc_op;
                $data['rete_autoica_percentage'] = $biller_data->rete_autoica_percentage;
                $data['rete_autoica_total'] = $autoica_amount;
                $data['rete_autoica_account'] = $biller_data->rete_autoica_account;
                $data['rete_autoica_account_counterpart'] = $biller_data->rete_autoica_account_counterpart;
                $data['rete_autoica_base'] = $total;
                if ($biller_data->rete_bomberil_percentage > 0) {
                    $bomberil_perc_op = $biller_data->rete_bomberil_percentage / 100;
                    $bomberil_amount = $autoica_amount * $bomberil_perc_op;
                    $data['rete_bomberil_percentage'] = $biller_data->rete_bomberil_percentage;
                    $data['rete_bomberil_total'] = $bomberil_amount;
                    $data['rete_bomberil_account'] = $biller_data->rete_bomberil_account;
                    $data['rete_bomberil_account_counterpart'] = $biller_data->rete_bomberil_account_counterpart;
                    $data['rete_bomberil_base'] = $autoica_amount;
                }
                if ($biller_data->rete_autoaviso_percentage > 0) {
                    $autoaviso_perc_op = $biller_data->rete_autoaviso_percentage / 100;
                    $autoaviso_amount = $autoica_amount * $autoaviso_perc_op;
                    $data['rete_autoaviso_percentage'] = $biller_data->rete_autoaviso_percentage;
                    $data['rete_autoaviso_total'] = $autoaviso_amount;
                    $data['rete_autoaviso_account'] = $biller_data->rete_autoaviso_account;
                    $data['rete_autoaviso_account_counterpart'] = $biller_data->rete_autoaviso_account_counterpart;
                    $data['rete_autoaviso_base'] = $autoica_amount;
                }
            }
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }
            $p = isset($_POST['amount']) ? count($_POST['amount'])+1 : 0;
            $paid = 0;
            $sale_payment_method = NULL;
            $pmnt_cnt = 0;
            for ($r = 0; $r < $p; $r++) {
                $payment_reference_no = $this->input->post('payment_document_type_id');
                if (isset($_POST['due_payment'][$r]) && $_POST['due_payment'][$r] == 1) {
                    $pm = $this->site->getPaymentMethodByCode($_POST['paid_by'][$r]);
                    $data['due_payment_method_id'] = $pm->id;
                    $pmnt_cnt++;
                    $sale_payment_method = $_POST['paid_by'][$r] ?? null;
                } else {
                    if (isset($_POST['amount'][$r]) && !empty($_POST['amount'][$r]) && isset($_POST['paid_by'][$r]) && !empty($_POST['paid_by'][$r])) {
                        $sale_payment_method = $_POST['paid_by'][$r] ?? null;
                        $pmnt_cnt++;
                        $amount = $this->sma->formatDecimal(($_POST['balance_amount'][$r] > 0 ? $_POST['amount'][$r] - $_POST['balance_amount'][$r] : $_POST['amount'][$r]), 2);
                        $paid += $amount;
                        if ($_POST['paid_by'][$r] == 'deposit') {
                            //Verificación balance del depósito del cliente
                            if ( ! $this->site->check_customer_deposit($customer_id, $_POST['amount'][$r])) {
                                echo json_encode(['error' => 1, 'message' => lang('amount_greater_than_deposit')]);
                                exit();
                            }
                        }
                        if ($_POST['paid_by'][$r] == 'gift_card') {
                            $gc = $this->site->getGiftCardByNO($_POST['paying_gift_card_no'][$r]);
                            if (!isset($_POST['paying_gift_card_no'][$r]) || empty($_POST['paying_gift_card_no'][$r])) {
                                echo json_encode(['error' => 1, 'message' => lang('payment_by_gift_card_withoud_number')]);
                                exit();
                            } else if (!$gc) {
                                echo json_encode(['error' => 1, 'message' => lang('payment_by_gift_card_invalid_number')]);
                                exit();
                            }
                            $amount_paying = $_POST['amount'][$r] ?? null >= $gc->balance ? $gc->balance : $_POST['amount'][$r];
                            $gc_balance = $gc->balance - $amount_paying;
                            $payment[] = array(
                                'date'         => $date,
                                // 'reference_no' => $reference_payment,
                                'amount'       => $amount,
                                'paid_by'      => $_POST['paid_by'][$r],
                                'cheque_no'    => $_POST['cheque_no'][$r],
                                'cc_no'        => $_POST['paying_gift_card_no'][$r],
                                'cc_holder'    => $_POST['cc_holder'][$r],
                                'cc_month'     => $_POST['cc_month'][$r],
                                'cc_year'      => $_POST['cc_year'][$r],
                                'cc_type'      => $_POST['cc_type'][$r],
                                'cc_cvv2'      => $_POST['cc_cvv2'][$r],
                                'created_by' => $created_by,
                                'type'         => 'received',
                                'mean_payment_code_fe' =>$this->input->post('mean_payment_code_fe'),
                                'note'         => $_POST['payment_note'][$r],
                                'pos_paid'     => $_POST['amount'][$r],
                                'pos_balance'  => $_POST['balance_amount'][$r],
                                'gc_balance'  => $gc_balance,
                                'document_type_id' => $payment_reference_no,
                            );
                        } else {
                            $referenceBiller = $this->site->getReferenceBiller($biller_id, $payment_reference_no);
                            if($referenceBiller){
                                $reference_payment = $referenceBiller;
                            }else{
                                $reference_payment = $this->site->getReference('rc');
                            }

                            $payment[] = array(
                                'date'         => $date,
                                'reference_no' => $reference_payment,
                                'amount'       => $amount,
                                'paid_by'      => $_POST['paid_by'][$r],
                                'cheque_no'    => $_POST['cheque_no'][$r],
                                'cc_no'        => $_POST['cc_no'][$r],
                                'cc_holder'    => $_POST['cc_holder'][$r],
                                'cc_month'     => $_POST['cc_month'][$r],
                                'cc_year'      => $_POST['cc_year'][$r],
                                'cc_type'      => $_POST['cc_type'][$r],
                                'cc_cvv2'      => $_POST['cc_cvv2'][$r],
                                'created_by' => $created_by,
                                'type'         => 'received',
                                'mean_payment_code_fe' =>$this->input->post('mean_payment_code_fe'),
                                'note'         => $_POST['payment_note'][$r],
                                'pos_paid'     => $_POST['amount'][$r],
                                'pos_balance'  => $_POST['balance_amount'][$r],
                                'document_type_id' => $payment_reference_no,
                            );
                        }
                    }
                }
            }
            if ($pmnt_cnt > 1) {
                $sale_payment_method = 'mixed';
            }
            $data['payment_method'] = $sale_payment_method;
            if ($paid == 0 && ($this->input->post('payment_term') <= 0  || !$this->input->post('payment_term'))) {
                echo json_encode(['error' => 1, 'message' => lang('payment_term_almost_be_greather_than_zero')]);
                exit();
            }
            if ($this->sma->formatDecimal($paid) > ($data['grand_total'] + $data['rounding'])) {
                echo json_encode(['error' => 1, 'message' => lang('payment_amount_cant_be_greather_than_invoice_total')]);
                exit();
            }
            if (!isset($payment) || empty($payment)) {
                $payment = array();
            }
            // Condición que valida el método de pago.
            if ($this->input->post('paid_by_1') && $this->input->post('paid_by_1') > 0) {
                if ($this->input->post('paid_by_1') < $grand_total) {
                    $data['payment_method_fe'] = CREDIT;
                } else {
                    $data['payment_method_fe'] = CASH;
                }
            } else {
                $data['payment_method_fe'] = CREDIT;
            }
            // Condición que valida el medio del pago
            if ($this->input->post('mean_payment_code_fe')) {
                $data['payment_mean_fe'] = $this->input->post('mean_payment_code_fe');
            } else {
                $data['payment_mean_fe'] = 'ZZZ';
            }

            $txt_pprices_changed = trim($txt_pprices_changed, ', ');
            // $this->sma->print_arrays($data, $document_type_id, $products, $payment);
            if ($sale = $this->pos_model->addSale($data, $document_type_id, $products, $payment)) {
                /**********************************************************************************************************/
                    $saleId = $sale['sale_id'];
                    if ($document_type->factura_electronica == YES) {
                        $saleData = $this->site->getSaleByID($saleId);
                        $documentElectronicData = [
                            'sale_id' => $saleId,
                            'biller_id' => $data['biller_id'],
                            'customer_id' => $data['customer_id'],
                            'reference' => $sale['reference_no'],
                            'attachment' => NULL
                        ];
                        $this->createDocumentElectronic($documentElectronicData);

                    }
                /**********************************************************************************************************/
                $data['error'] = 0;
                $data['message'] = lang('sale_added');
                $data['reference_no'] = $sale['reference_no'];
                $data['sale_id'] = $sale['sale_id'];
                $saleData = $this->site->getSaleByID($saleId);
                $data['fe_xml'] = $saleData->fe_xml;
                $data['cufe'] = $saleData->cufe;
                $data['fe_aceptado'] = $saleData->fe_aceptado;
                $data['fe_mensaje'] = $saleData->fe_mensaje;
                $data['fe_mensaje_soporte_tecnico'] = $saleData->fe_mensaje_soporte_tecnico;
                echo json_encode($data);
                exit();
            } else {
                echo json_encode(['error' => 1, 'message' => 'Ocurrió un error al agregar']);
                exit();
            }
        } else {
            echo json_encode(['error' => 1, 'message' => lang('invalid_request')]);
            exit();
        }

    }

    public function createDocumentElectronic($invoiceData)
    {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            return true;
            return true;
            return true;
        }
        $invoiceData = (object) $invoiceData;
        $createdFile = $this->Electronic_billing_model->send_document_electronic($invoiceData);
        if ($createdFile->response != 0) {
            $this->sendEmail($invoiceData);
        }
    }

    public function sendEmail($invoiceData)
    {
        $technologyProvider = $this->pos_settings->technology_provider_id;
        $filename = $this->site->getFilename($invoiceData->sale_id);

        if ($technologyProvider == CADENA || $technologyProvider == BPM) {
            $this->downloadFeXml($invoiceData->sale_id, TRUE);
            $this->downloadFePdf($invoiceData->sale_id, TRUE);

            if (file_exists('files/electronic_billing/' . $filename . '.xml') && file_exists('files/electronic_billing/' . $filename . '.pdf')) {
                $zip = new ZipArchive();

                $zip->open('files/electronic_billing/' . $filename . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
                $zip->addFile("files/electronic_billing/" . $filename . '.xml', $filename . ".xml");
                $zip->addFile("files/electronic_billing/" . $filename . '.pdf', $filename . ".pdf");

                $zip->close();

                unlink('files/electronic_billing/' . $filename . '.xml');
                unlink('files/electronic_billing/' . $filename . '.pdf');

                $this->Electronic_billing_model->receipt_delivery($invoiceData, $filename);
            }
        }
    }

    public function downloadFeXml($documentId, $internal_download = FALSE)
    {
        $sale = $this->site->getSaleByID($documentId);
        $technologyProvider = $this->pos_settings->technology_provider_id;
        $filename = $this->site->getFilename($documentId);

        if ($technologyProvider == CADENA) {
            if (!empty($sale->fe_xml)) {
                $xml_file = base64_decode($sale->fe_xml);

                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $path = 'files/electronic_billing';
                    if (!file_exists($path)) {
                        mkdir($path, 0777);
                    }

                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                } else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            }
            /* else {
                $prefix = strstr($sale->reference_no, '-', TRUE);
                $reference = str_replace('-', '', $sale->reference_no);
                $document_type_code = ($sale->total > 0) ? '01' : '92';

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/documentos?nit_emisor=' . $this->Settings->numero_documento . '&id_documento=' . $reference . '&codigo_tipo_documento=' . $document_type_code . '&prefijo=' . $prefix,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => [
                        'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                        'Content-Type: application/json',
                        'Partnership-Id: 901090070'
                    ],
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                $response = json_decode($response);
                if (!empty($response)) {
                    if (isset($response->statusCode)) {
                        if ($response->statusCode == 200) {
                            $updated_sale = $this->site->updateSale([
                                'fe_aceptado' => 2,
                                'fe_mensaje' => 'Documento aceptado por la DIAN',
                                'fe_mensaje_soporte_tecnico' => 'La Factura electrónica ' . $reference . ', ha sido autorizada. Código es estado ' . $response->statusCode,
                                'fe_xml' => $response->document
                            ], $documentId);

                            if ($updated_sale == TRUE) {
                                $xml_file = base64_decode($response->document);

                                if ($internal_download === TRUE) {
                                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                                    $xml->loadXML($xml_file);

                                    $path = 'files/electronic_billing';
                                    if (!file_exists($path)) {
                                        mkdir($path, 0777);
                                    }

                                    $xml->save('files/electronic_billing/' . $sale->reference_no . '.xml');
                                } else {
                                    header('Content-Type: application/xml;');
                                    header('Content-Disposition: attachment; filename="' . $sale->reference_no . '.xml"');
                                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                                    $xml->loadXML($xml_file);
                                    echo $xml->saveXML();
                                }
                            } else {
                                $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML.');
                                admin_redirect('sales/fe_index');
                            }
                        } else if ($response->statusCode == 400) {
                            $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML. El prefijo no coincide con el prefijo del id del documento.');
                            admin_redirect('sales/fe_index');
                        } else if ($response->statusCode == 404) {
                            $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML. Registro no encontrado con los parámetros de búsqueda enviados.');
                            admin_redirect('sales/fe_index');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Se ha perdido la conexión con el Web Services.');
                        admin_redirect('sales/fe_index');
                    }
                } else {
                    $this->session->set_flashdata('error', 'No es posible realizar conexión con Web Services.');
                    admin_redirect('sales/fe_index');
                }
            } */
        } else if ($technologyProvider == DELCOP) {
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
            $response_token = $this->Electronic_billing_model->delcopGetAuthorizationoken();
            if ($response_token->success == FALSE) {
                $this->session->set_flashdata('error', "Los campos de Usuario o Contraseña DELCOP no son correctos. " . $response_token->error);
                admin_redirect('sales/fe_index');
            }

            if ($this->Settings->fe_work_environment == TEST) {
                $url = "https://www-prueba.titanio.com.co/PDE/public/api/PDE/descargar";
            } else {
                $url = "https://www.titanio.com.co/PDE/public/api/PDE/descargar";
            }

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => '{"token": "' . $response_token->token . '", "documentos": [{"transaccion_id": "' . $sale->fe_id_transaccion . '", "tipo_descarga": "1"}]}',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $response = json_decode($response);

            if ($response->error_id == 0) {
                $sale_data["fe_xml"] = $response->documentos[0]->data;
                $updated_sale = $this->site->updateSale($sale_data, $sale->id);

                $xml_file = base64_decode($response->documentos[0]->data);
                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $xml->save('files/electronic_billing/' . $sale->reference_no . '.xml');
                } else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $sale->reference_no . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            }
        } else if ($technologyProvider == BPM) {
            $xml_file = base64_decode($sale->fe_xml);

            if ($internal_download === TRUE) {
                $xml = new DOMDocument("1.0", "ISO-8859-15");
                $xml->loadXML($xml_file);

                $path = 'files/electronic_billing';
                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }

                $xml->save('files/electronic_billing/' . $filename . '.xml');
            } else {
                header('Content-Type: application/xml;');
                header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                $xml = new DOMDocument("1.0", "ISO-8859-15");

                $xml->loadXML($xml_file);
                echo $xml->saveXML();
            }
        }
    }

    public function downloadFePdf($sale_id, $internal_download = FALSE)
    {
        if ($this->pos_settings->technology_provider_id == CADENA) {

            $this->saleViewMobile($sale_id, $internal_download);
        }
    }

    public function fix_item_tax($document_type_id, $item)
    {
        $document_type = $this->site->getDocumentTypeById($document_type_id);
        $pdata = $this->site->getProductByID($item['product_id']);
        $category = $this->site->getCategoryByID($pdata->category_id);
        $subcategory = $this->site->getCategoryByID($pdata->subcategory_id);
        $except_category = $this->sma->validate_except_category_taxes($category, $subcategory);
        if ($except_category == false && $document_type->key_log == 1) {
            $pr = $this->site->getProductByID($item['product_id']);
            if ($pr->tax_rate != $item['tax_rate_id'])  {
                $tax_details = $this->site->getTaxRateByID($pr->tax_rate);
                $tx = $this->site->calculateTax($pr, $tax_details, $item['real_unit_price']);
                if ($pr->tax_method == 0) {
                    $item['net_unit_price'] = $item['real_unit_price'] - $tx['amount'];
                    $item['unit_price'] = $item['real_unit_price'];
                } else if ($pr->tax_method == 1) {
                    $item['net_unit_price'] = $item['real_unit_price'];
                    $item['unit_price'] = $item['real_unit_price'] + $tx['amount'];
                }
                $item['item_tax'] = ($tx['amount'] * $item['quantity']);
                $item['tax_rate_id'] = $pr->tax_rate;
                $item['tax'] = $tx['tax'];
                return $item;
            }
        }
        return false;
    }

    public function app_pos_register_add_movement ()
    {
        $date = date('Y-m-d H:i:s');
        $biller_id = $this->input->post('mv_biller');
        $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
        $movement_type = $this->input->post('mv_movement_type');
        $amount = $this->input->post('mv_amount');
        $note = $this->input->post('mv_note');
        $document_type_id = $this->input->post('mv_document_type_id');
        $mv_origin_paid_by = $this->input->post('mv_origin_paid_by');
        $mv_destination_paid_by = $this->input->post('mv_destination_paid_by');
        $destination_biller_id = $this->input->post('destination_mv_biller');
        $destination_user_id = $this->input->post('mv_user');
        $created_by = $this->input->post('created_by');

        if (!$this->input->post('mv_document_type_id')) {
            echo json_encode(['error' => 1, 'message' => 'No se especificó tipo de documento para el movimiento']);
            exit();
        }
        if (!$this->input->post('mv_biller')) {
            echo json_encode(['error' => 1, 'message' => 'No se especificó sucursal para el movimiento']);
            exit();
        }
        if (!$this->input->post('mv_movement_type')) {
            echo json_encode(['error' => 1, 'message' => 'No se especificó el tipo de transacción para el movimiento']);
            exit();
        }
        if (!$this->input->post('mv_amount')) {
            echo json_encode(['error' => 1, 'message' => 'No se especificó el valor para el movimiento']);
            exit();
        }
        if (!$this->input->post('mv_origin_paid_by')) {
            echo json_encode(['error' => 1, 'message' => 'No se especificó la forma de pago de origen para el movimiento']);
            exit();
        }
        if (!$this->input->post('mv_destination_paid_by')) {
            echo json_encode(['error' => 1, 'message' => 'No se especificó la forma de pago de destino para el movimiento']);
            exit();
        }

        if ($this->Settings->cashier_close != 2 && $movement_type == 2) {
            if (!$this->pos_model->getRegisterState($amount, $created_by)) {
                echo json_encode(['error' => 1, 'message' => lang('not_enough_cash_register')]);
                exit();
            }
        }
        if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
            echo json_encode(['error' => 1, 'message' => lang('biller_without_cost_center')]);
            exit();
        }
        $data = array(
                    'date' => $date,
                    'movement_type' => $movement_type,
                    'amount' => $amount,
                    'note' => $note,
                    'origin_paid_by' => $mv_origin_paid_by,
                    'destination_paid_by' => $mv_destination_paid_by,
                    'biller_id' => $biller_id,
                    'document_type_id' => $document_type_id,
                    'destination_biller_id' => $movement_type == 3 ? $destination_biller_id : NULL,
                    'destination_user_id' => $movement_type == 3 ? $destination_user_id : NULL,
                    'created_by' => $created_by,
                );
        $destination_data = [];
        if ($movement_type == 3) {
            $destination_data = array(
                        'date' => $date,
                        'movement_type' => 1,
                        'amount' => $amount,
                        'note' => $note,
                        'origin_paid_by' => $mv_destination_paid_by ,
                        'destination_paid_by' => $mv_origin_paid_by,
                        'biller_id' => $destination_biller_id,
                        'document_type_id' => $document_type_id,
                        'created_by' => $destination_user_id,
                    );
        }
        if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
            $data['cost_center_id'] = $biller_cost_center->id;
        } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('mv_cost_center_id')) {
            $data['cost_center_id'] = $this->input->post('mv_cost_center_id');
        }

        if ($reference = $this->pos_model->add_movement($data, $destination_data)) {
            echo json_encode(['error' => 0, 'message' => lang('pos_register_movement_added'), 'reference_no' => $reference, 'date' => $date]);
            exit();
        } else {
            echo json_encode(['error' => 1, 'message' => 'Ocurrió un error al agregar']);
            exit();
        }
    }

    public function saleViewMobile($id = null, $internal_download = FALSE, $download = FALSE, $for_email = FALSE,  $quickPrintFromPos = FALSE, $quickPrint = FALSE)
    {
        $inv = $this->sales_model->getSaleByID($id);
        $document_type = $this->site->getDocumentTypeById($inv->document_type_id);
        if (!empty($inv->sale_id)) {
            if (!empty($inv->year_database)) {
                $affected_bill = $this->site->get_past_year_sale($inv->return_sale_ref, $inv->year_database);
                // $resolution_data_referenced_invoice = $this->site->get_past_year_document_type_by_id($reference_invoice->document_type_id, $inv->year_database);
            } else {
                $affected_bill = $this->sales_model->getSaleByID($inv->sale_id);
            }

            $this->data['affected_bill'] = $affected_bill;
        }

        // if (!$this->session->userdata('view_right')) {
        //     $this->sma->view_rights($inv->created_by);
        // }

        //Declaramos una carpeta temporal para guardar la imagenes generadas
        $dir = 'themes/default/admin/assets/images/qr_code/';
        //Declaramos la ruta y nombre del archivo a generar
        // $filename = $dir . 'test.png';
        $filename = $dir . $inv->reference_no.'.png';
        //Si no existe la carpeta la creamos
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }
        QRcode::png($inv->codigo_qr, $filename, 0, 3, 0);


        $quickPrintFormatId = $document_type->quick_print_format_id;
        if ($quickPrint == FALSE) {
            $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
        } else {
            if (!empty($quickPrintFormatId)) {
                $document_type_invoice_format = $this->site->getInvoiceFormatById($document_type->quick_print_format_id);
            } else {
                $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
            }
        }

        if (!empty($inv->fe_xml)) {
            $xml_file = base64_decode($inv->fe_xml);

            $xml = new DOMDocument("1.0", "ISO-8859-15");
            $xml->loadXML($xml_file);

            $path = 'files/electronic_billing';
            if (!file_exists($path)) {
                mkdir($path, 0777);
            }
            $xml->save('files/electronic_billing/' . $inv->reference_no . '.xml');

            $positionDate = strpos($xml_file, '<cbc:ValidationDate>');
            $substringDate = substr($xml_file, $positionDate);
            $substringDate2 = substr($substringDate, 20, 10);

            $positionHour = strpos($xml_file, '<cbc:ValidationTime>');
            $substringHour = substr($xml_file, $positionHour);
            $substringHour2 = substr($substringHour, 20, 8);

            $validationDateTime = $substringDate2 .' '. $substringHour2;

            $this->data['validationDateTime'] = $validationDateTime;

            if (file_exists(FCPATH. 'files/electronic_billing/'.$inv->reference_no.'.xml')) {
                unlink(FCPATH. 'files/electronic_billing/'.$inv->reference_no.'.xml');
            }
        } else {
            $this->data['validationDateTime'] = '';
        }

        $this->data['modal'] = FALSE;
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['quickPrintFormatId'] = $quickPrintFormatId;
        $this->data['quickPrintFromPos'] = $quickPrintFromPos;
        $this->data['internal_download'] = $internal_download;
        $this->data['qr_code'] = $filename;
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['page_title'] = $this->lang->line("invoice");
        // $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['payments'] = $this->pos_model->getInvoicePayments($id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $inv->biller_id, false);
        // $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        // $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id, NULL, false, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
        $this->data['rows'] = $this->pos_model->getAllInvoiceItems($id, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
		$this->data['paypal'] = $this->sales_model->getPaypalSettings();
		$this->data['skrill'] = $this->sales_model->getSkrillSettings();
		$this->data['settings'] = $this->Settings;
		$this->data['seller'] = $this->site->getSellerById(!empty($inv->seller_id) ? $inv->seller_id : $this->data['biller']->default_seller_id);
        $this->data['filename'] = $this->site->getFilename($id);
        $ciius_rows = $this->site->get_ciiu_code_by_ids(explode(",", $this->Settings->ciiu_code));
        $ciius = "";
        if ($ciius_rows) {
            foreach ($ciius_rows as $ciiu) {
                $ciius.= $ciiu->code.", ";
            }
        }
        $this->data['ciiu_code'] = trim($ciius, ", ");
		$this->data['sma'] = $this->sma;
		$this->data['document_type'] = $document_type;

        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        if ($this->Settings->great_contributor == 1) {
            $this->data['tipo_regimen'] = lang('great_contributor');
        } else {
            $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        }
        $payments = $this->sales_model->getPaymentsForSale($id);
        $original_payment_method = '';
        $original_payment_method_with_amount = '';
        $total_original_payments_amount = 0;
        $direct_payments_num = 0;

        if ($payments) {
            foreach ($payments as $payment) {
                if ($payment->paid_by == "retencion") {
                    $total_original_payments_amount += $payment->amount;
                }
                if (date('Y-m-d H', strtotime($payment->date)) == date('Y-m-d H', strtotime($inv->date)) && $payment->paid_by != "retencion") {
                    if ($original_payment_method == '') {
                        $original_payment_method = lang($payment->paid_by) . ", ";
                    } else {
                        $original_payment_method .= lang($payment->paid_by) . ", ";
                    }
                    if ($original_payment_method_with_amount == '') {
                        $original_payment_method_with_amount = lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    } else {
                        $original_payment_method_with_amount .= lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    }
                    $direct_payments_num++;
                    $total_original_payments_amount += $payment->amount;
                }
            }
            if ($direct_payments_num > 1) {
                $inv->note .= $original_payment_method_with_amount;
                $original_payment_method = 'Varios, ver nota';
            } else {
            }
        }
        if ($original_payment_method != '') {
            $original_payment_method = trim($original_payment_method, ", ");
        }
        if ($original_payment_method == '' || ($total_original_payments_amount < $inv->grand_total)) {
            $original_payment_method .= ($original_payment_method != '' ? ", " : "") . lang('due') . " " . $inv->payment_term . ($inv->payment_term > 1 ? " Días" : " Día");
        }

        $currencies = $this->site->getAllCurrencies();
        $currencies_names = [];
        if ($currencies) {
            foreach ($currencies as $currency) {
                $currencies_names[$currency->code] = $currency->name;
            }
        }

        $this->data['currencies_names'] = $currencies_names;
        $this->data['sale_payment_method'] = $original_payment_method;
        $prueba = false;
        $url_format = "sales/sale_view";
        $view_tax = true;
        $tax_inc = true;
        $this->data['document_type_invoice_format'] = false;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['value_decimals'] = $this->Settings->qty_decimals;
        $this->data['biller_logo'] = 2;
        $this->data['show_code'] = 1;
        $this->data['product_detail_promo'] = 1;
        $this->data['show_document_type_header'] = 1;
        $this->data['show_product_preferences'] = 1;
        $this->data['tax_indicator'] = 0;
        $this->data['product_detail_font_size'] = 0;
        $this->data['show_award_points'] = 1;
        $this->data['pos'] = $this->pos_model->getSetting();
        if (!$prueba) {
            $url_format = "sales/sale_view";
            if ($inv->sale_status == 'returned') {
                $url_format = "sales/return_sale_view";
            }
            if ($document_type_invoice_format) {
                $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
                $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
                $this->data['biller_logo'] = $document_type_invoice_format->logo;
                $this->data['show_code'] = $document_type_invoice_format->product_show_code;
                $this->data['product_detail_promo'] = $document_type_invoice_format->product_detail_promo;
                $this->data['show_document_type_header'] = $document_type_invoice_format->show_document_type_header;
                $this->data['document_type_invoice_format'] = $document_type_invoice_format;
                $this->data['show_product_preferences'] = $document_type_invoice_format->show_product_preferences;
                $this->data['tax_indicator'] = $document_type_invoice_format->tax_indicator;
                $this->data['product_detail_font_size'] = $document_type_invoice_format->product_detail_font_size > 0 ? $document_type_invoice_format->product_detail_font_size : 0;
				$url_format = $document_type_invoice_format->format_url;
				$view_tax = $document_type_invoice_format->view_item_tax ? true : false;
				$tax_inc = $document_type_invoice_format->tax_inc ? true : false;
    		}
        }

        $this->data['view_tax'] = $view_tax;
        $this->data['tax_inc'] = $tax_inc;
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = (!$document_type_invoice_format || ($document_type_invoice_format && $document_type_invoice_format->tax_indicator == 1) ? "(" . $tax->tax_indicator . ") " : "") . $tax->name;
        }

        $this->data['taxes_details'] = $taxes_details;
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->customer_id);
        }
        $this->data['signature_root'] = is_file("assets/uploads/signatures/" . $this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : false;
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['invoice_header'] = $this->site->getInvoiceHeader($inv->document_type_id, $inv->reference_no);
        $currency = $this->site->getCurrencyByCode($inv->sale_currency);
        $trmrate = 1;
        if (!empty($inv->sale_currency) && $inv->sale_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->sale_currency_trm;
        }

        $this->data['trmrate'] = $trmrate;
        $this->data['download'] = $download;
        $this->data['for_email'] = $for_email === true ? $for_email : false;
        $this->data["technologyProviderLogo"] = ($this->Settings->fe_technology_provider == CADENA) ? "assets/images/cadena_logo.jpeg" : "assets/images/bpm_logo.jpeg";

        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;
        $this->data['print_directly'] = $print_directly;

        $this->load_view($this->theme . $url_format, $this->data);
    }

    public function set_customers_data($id_customer_database = FALSE, $code = NULL){


        $query = "SELECT
            sma_settings.numero_documento as id_customer,
            sma_settings.wappsi_customer_address_id as id_sucursal_cliente,
            CONCAT(sma_settings.nombre_comercial, ' - ', sma_settings.site_name) AS customer_name,
            current_timestamp() AS update_at,
            CASE
            WHEN sma_documents_types.module IN ('1' ,'3' ,'19' ,'2' ,'4' ,'13' ,'31' ,'14' ,'26' ,'27' ,'28' ,'29' ,'33' ,'36' ,'37' ,'53' ) THEN 'ventas'
            WHEN sma_documents_types.module IN ('5' ,'35' ,'6' ,'20' ,'32' ,'17' ,'21' ,'22' ,'23' ,'24' ,'34' ,'52' , 49, 50, 51, 48) THEN 'compras'
            WHEN sma_documents_types.module IN ('7') THEN 'cotizacion'
            WHEN sma_documents_types.module IN ('8' ,'9' ,'10' ,'18') THEN 'ordenes'
            WHEN sma_documents_types.module IN ('15' ,'30') THEN 'anticipos'
            WHEN sma_documents_types.module IN ('11' ,'12' ,'25' ,'38' ,'39' ,'40' ,'41' ,'42') THEN 'productos'
            WHEN sma_documents_types.module IN ('43' ,'44' ,'45' ) THEN 'nómina electrónica'
            WHEN sma_documents_types.module IN ('47') THEN 'huéspedes'
            ELSE 'error' END AS module,
            sma_documents_types.module AS submodule_id,
            sma_documents_types.sales_prefix AS document_type,
            sma_documents_types.factura_electronica AS e_document,
            1 AS first_no,
            sma_documents_types.sales_consecutive AS current_no,
            CASE
            WHEN sma_documents_types.module IN (1,3,2,4,26,27) THEN COALESCE(COUNT(IF(sma_sales.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (5,35,6,21,22,52) THEN COALESCE(COUNT(IF(sma_purchases.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(IF(sma_quotes.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (19,13,31,14,28,29,33,20,32,17,23,24,34) THEN COALESCE(COUNT(IF(sma_payments.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (36) THEN COALESCE(COUNT(IF(sma_expenses.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (37) THEN COALESCE(COUNT(IF(sma_pos_register_movements.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (53) THEN COALESCE(COUNT(IF(sma_gift_card_topups.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (46) THEN COALESCE(COUNT(IF(sma_deliveries.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (49,50,51,48) THEN COALESCE(COUNT(IF(sma_documents_reception_events.createdAt >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(IF(sma_quotes.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (8) THEN COALESCE(COUNT(IF(sma_order_sales.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (18) THEN COALESCE(COUNT(IF(sma_production_order.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (15,30) THEN COALESCE(COUNT(IF(sma_deposits.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (11,25,40,42) THEN COALESCE(COUNT(IF(sma_adjustments.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (12) THEN COALESCE(COUNT(IF(sma_transfers.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (38) THEN COALESCE(COUNT(IF(sma_stock_counts.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, 0)) + COALESCE(COUNT(IF(sma_sequential_count.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0))
            WHEN sma_documents_types.module IN (39) THEN COALESCE(COUNT(IF(sma_production_order.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (41) THEN COALESCE(COUNT(IF(sma_wms_picking.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (43,44,45) THEN COALESCE(COUNT(IF(sma_payroll_electronic_employee.creation_date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (47) THEN COALESCE(COUNT(IF(sma_guests_register.registration_date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY), 1, NULL)), 0)
            ELSE 'error' END AS last30day_sum,
            CASE
            WHEN sma_documents_types.module IN (1,3,2,4,26,27) THEN COALESCE(COUNT(IF(sma_sales.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (5,35,6,21,22,52) THEN COALESCE(COUNT(IF(sma_purchases.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(IF(sma_quotes.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (19,13,31,14,28,29,33,20,32,17,23,24,34) THEN COALESCE(COUNT(IF(sma_payments.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (36) THEN COALESCE(COUNT(IF(sma_expenses.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (37) THEN COALESCE(COUNT(IF(sma_pos_register_movements.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (53) THEN COALESCE(COUNT(IF(sma_gift_card_topups.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (46) THEN COALESCE(COUNT(IF(sma_deliveries.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (49,50,51,48) THEN COALESCE(COUNT(IF(sma_documents_reception_events.createdAt >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(IF(sma_quotes.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (8) THEN COALESCE(COUNT(IF(sma_order_sales.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (18) THEN COALESCE(COUNT(IF(sma_production_order.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (15,30) THEN COALESCE(COUNT(IF(sma_deposits.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (11,25,40,42) THEN COALESCE(COUNT(IF(sma_adjustments.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (12) THEN COALESCE(COUNT(IF(sma_transfers.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (38) THEN COALESCE(COUNT(IF(sma_stock_counts.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, 0)) + COALESCE(COUNT(IF(sma_sequential_count.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0))
            WHEN sma_documents_types.module IN (39) THEN COALESCE(COUNT(IF(sma_production_order.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (41) THEN COALESCE(COUNT(IF(sma_wms_picking.date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (43,44,45) THEN COALESCE(COUNT(IF(sma_payroll_electronic_employee.creation_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (47) THEN COALESCE(COUNT(IF(sma_guests_register.registration_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY), 1, NULL)), 0)
            ELSE 'error' END AS lastday_no,
            CASE
            WHEN sma_documents_types.module IN (1,3,2,4,26,27) THEN COALESCE(COUNT(IF(sma_documents_types.factura_electronica = 1 AND (sma_sales.fe_aceptado = 0 OR sma_sales.fe_aceptado = 1), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (5,35,6,21,22,52) THEN COALESCE(COUNT(IF(sma_documents_types.factura_electronica = 1 AND (sma_purchases.statusCode = 0 OR sma_purchases.statusCode = 1), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (49,50,51,48) THEN COALESCE(COUNT(IF(sma_documents_types.factura_electronica = 1 AND (sma_documents_reception_events.eventStatus = 0 OR sma_documents_reception_events.eventStatus = 1), 1, NULL)), 0)
            WHEN sma_documents_types.module IN (43,44,45) THEN COALESCE(COUNT(IF(sma_documents_types.factura_electronica = 1 AND (sma_payroll_electronic_employee.status = 0 OR sma_payroll_electronic_employee.status = 1), 1, NULL)), 0)
            ELSE 0 END AS DIAN_pending
            FROM sma_documents_types
            INNER JOIN sma_settings ON setting_id = 1
            LEFT JOIN sma_sales ON sma_sales.document_type_id = sma_documents_types.id AND sma_sales.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_purchases ON sma_purchases.document_type_id = sma_documents_types.id AND sma_purchases.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_payments ON sma_payments.document_type_id = sma_documents_types.id AND sma_payments.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_transfers ON sma_transfers.document_type_id = sma_documents_types.id AND sma_transfers.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_adjustments ON sma_adjustments.document_type_id = sma_documents_types.id AND sma_adjustments.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_order_sales ON sma_order_sales.document_type_id = sma_documents_types.id AND sma_order_sales.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_deliveries ON sma_deliveries.document_type_id = sma_documents_types.id AND sma_deliveries.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_deposits ON sma_deposits.document_type_id = sma_documents_types.id AND sma_deposits.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_quotes ON sma_quotes.document_type_id = sma_documents_types.id AND sma_quotes.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_sequential_count ON sma_sequential_count.document_type_id = sma_documents_types.id AND sma_sequential_count.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_expenses ON sma_expenses.document_type_id = sma_documents_types.id AND sma_expenses.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_pos_register_movements ON sma_pos_register_movements.document_type_id = sma_documents_types.id AND sma_pos_register_movements.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_gift_card_topups ON sma_gift_card_topups.document_type_id = sma_documents_types.id AND sma_gift_card_topups.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_documents_reception_events ON sma_documents_reception_events.document_type_id = sma_documents_types.id AND sma_documents_reception_events.createdAt >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_production_order ON sma_production_order.document_type_id = sma_documents_types.id AND sma_production_order.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_stock_counts ON sma_stock_counts.document_type_id = sma_documents_types.id AND sma_stock_counts.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_wms_picking ON sma_wms_picking.document_type_id = sma_documents_types.id AND sma_wms_picking.date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_payroll_electronic_employee ON sma_payroll_electronic_employee.document_type_id = sma_documents_types.id AND sma_payroll_electronic_employee.creation_date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            LEFT JOIN sma_guests_register ON sma_guests_register.document_type_id = sma_documents_types.id AND sma_guests_register.registration_date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)
            GROUP BY sma_documents_types.id
            ;
            ";
        $query2 = "SELECT
            sma_settings.numero_documento as id_customer,
            CONCAT(sma_settings.nombre_comercial, ' - ', sma_settings.razon_social) AS customer_name,
            sma_settings.version AS sw_version,
            'customer_data' AS module,
            COALESCE(COUNT(IF(sma_biller_data.branch_type = 1, 1, NULL)), 0) AS physicalbillers_qty,
            COALESCE(COUNT(IF(sma_biller_data.branch_type = 2, 1, NULL)), 0) AS virtualbillers_qty,
            tbl.wappsiusers_qty AS wappsiusers_qty,
            tbl.customerusers_qty AS customerusers_qty,
            tbl2.num_wh AS warehouses_no,
            tbl3.num_customer AS customers_qty,
            tbl4.num_supplier AS suppliers_qty,
            tbl5.num_products AS products_qty,
            IF(tbl6.num_variants > 0, 1, 0) AS variants,
            sma_settings.product_variant_per_serial AS serial_numbers,
            sma_settings.overselling,
            sma_pos_settings.restobar_mode AS tables,
            sma_settings.prioridad_precios_producto AS prices_policy,
            sma_settings.fe_technology_provider AS tech_supplier,
            current_timestamp() AS update_at,
            tbl7.reference_no AS last_payroll_sent
            FROM sma_companies
            LEFT JOIN sma_biller_data ON sma_biller_data.biller_id = sma_companies.id
            LEFT JOIN ( SELECT
            1 as id,
            COALESCE(COUNT(IF(sma_groups.name != 'customer', 1, NULL)), 0) AS wappsiusers_qty,
            COALESCE(COUNT(IF(sma_groups.name = 'customer', 1, NULL)), 0) AS customerusers_qty
            FROM sma_users
            LEFT JOIN sma_groups ON sma_groups.id = sma_users.group_id
            WHERE sma_users.active = 1) AS tbl ON tbl.id = 1
            LEFT JOIN (
            SELECT 1 as id, COUNT(id) as num_wh FROM sma_warehouses WHERE status = 1
            ) tbl2 ON tbl2.id = 1
            LEFT JOIN (
            SELECT 1 as id, COUNT(id) as num_customer FROM sma_companies WHERE group_name = 'customer'
            ) tbl3 ON tbl3.id = 1
            LEFT JOIN (
            SELECT 1 as id, COUNT(id) as num_supplier FROM sma_companies WHERE group_name = 'supplier'
            ) tbl4 ON tbl4.id = 1
            LEFT JOIN (
            SELECT 1 as id, COUNT(id) as num_products FROM sma_products WHERE discontinued = 0
            ) tbl5 ON tbl5.id = 1
            LEFT JOIN (
            SELECT 1 as id, COUNT(id) as num_variants FROM sma_product_variants
            ) tbl6 ON tbl6.id = 1
            LEFT JOIN (
            SELECT 1 as id2, reference_no  FROM sma_payroll_electronic_employee ORDER BY id DESC LIMIT 1
            ) tbl7 ON tbl7.id2 = 1
            LEFT JOIN sma_settings ON sma_settings.setting_id = 1
            LEFT JOIN sma_pos_settings ON sma_pos_settings.pos_id = 1
            WHERE sma_companies.group_name = 'biller';";
        $query3 = "SELECT
            sma_settings.numero_documento as id_customer,
            sma_settings.wappsi_customer_address_id as id_sucursal_cliente,
            sma_documents_types.module AS submodule_id,
            sma_documents_types.sales_prefix AS document_type,
            CASE
            WHEN sma_documents_types.module IN (1,3,2,4,26,27) THEN COALESCE(COUNT(sma_sales.id), 0)
            WHEN sma_documents_types.module IN (5,35,6,21,22,52) THEN COALESCE(COUNT(sma_purchases.id), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(sma_quotes.id), 0)
            WHEN sma_documents_types.module IN (19,13,31,14,28,29,33,20,32,17,23,24,34) THEN COALESCE(COUNT(sma_payments.id), 0)
            WHEN sma_documents_types.module IN (36) THEN COALESCE(COUNT(sma_expenses.id), 0)
            WHEN sma_documents_types.module IN (37) THEN COALESCE(COUNT(sma_pos_register_movements.id), 0)
            WHEN sma_documents_types.module IN (53) THEN COALESCE(COUNT(sma_gift_card_topups.id), 0)
            WHEN sma_documents_types.module IN (46) THEN COALESCE(COUNT(sma_deliveries.id), 0)
            WHEN sma_documents_types.module IN (49,50,51,48) THEN COALESCE(COUNT(sma_documents_reception_events.id), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(sma_quotes.id), 0)
            WHEN sma_documents_types.module IN (8) THEN COALESCE(COUNT(sma_order_sales.id), 0)
            WHEN sma_documents_types.module IN (18) THEN COALESCE(COUNT(sma_production_order.id), 0)
            WHEN sma_documents_types.module IN (15,30) THEN COALESCE(COUNT(sma_deposits.id), 0)
            WHEN sma_documents_types.module IN (11,25,40,42) THEN COALESCE(COUNT(sma_adjustments.id), 0)
            WHEN sma_documents_types.module IN (12) THEN COALESCE(COUNT(sma_transfers.id), 0)
            WHEN sma_documents_types.module IN (38) THEN COALESCE(COUNT(sma_stock_counts.id), 0) + COALESCE(COUNT(sma_sequential_count.id), 0)
            WHEN sma_documents_types.module IN (39) THEN COALESCE(COUNT(sma_production_order.id), 0)
            WHEN sma_documents_types.module IN (41) THEN COALESCE(COUNT(sma_wms_picking.id), 0)
            WHEN sma_documents_types.module IN (43,44,45) THEN COALESCE(COUNT(sma_payroll_electronic_employee.id), 0)
            WHEN sma_documents_types.module IN (47) THEN COALESCE(COUNT(sma_guests_register.id), 0)
            ELSE 'error' END AS lastyear_sum
            FROM sma_documents_types
            INNER JOIN sma_settings ON setting_id = 1
            LEFT JOIN sma_sales ON sma_sales.document_type_id = sma_documents_types.id AND sma_sales.date >= '{INI_DATE}' AND sma_sales.date <= '{END_DATE}'
            LEFT JOIN sma_purchases ON sma_purchases.document_type_id = sma_documents_types.id AND sma_purchases.date >= '{INI_DATE}' AND sma_purchases.date <= '{END_DATE}'
            LEFT JOIN sma_payments ON sma_payments.document_type_id = sma_documents_types.id AND sma_payments.date >= '{INI_DATE}' AND sma_payments.date <= '{END_DATE}'
            LEFT JOIN sma_transfers ON sma_transfers.document_type_id = sma_documents_types.id AND sma_transfers.date >= '{INI_DATE}' AND sma_transfers.date <= '{END_DATE}'
            LEFT JOIN sma_adjustments ON sma_adjustments.document_type_id = sma_documents_types.id AND sma_adjustments.date >= '{INI_DATE}' AND sma_adjustments.date <= '{END_DATE}'
            LEFT JOIN sma_order_sales ON sma_order_sales.document_type_id = sma_documents_types.id AND sma_order_sales.date >= '{INI_DATE}' AND sma_order_sales.date <= '{END_DATE}'
            LEFT JOIN sma_deliveries ON sma_deliveries.document_type_id = sma_documents_types.id AND sma_deliveries.date >= '{INI_DATE}' AND sma_deliveries.date <= '{END_DATE}'
            LEFT JOIN sma_deposits ON sma_deposits.document_type_id = sma_documents_types.id AND sma_deposits.date >= '{INI_DATE}' AND sma_deposits.date <= '{END_DATE}'
            LEFT JOIN sma_quotes ON sma_quotes.document_type_id = sma_documents_types.id AND sma_quotes.date >= '{INI_DATE}' AND sma_quotes.date <= '{END_DATE}'
            LEFT JOIN sma_sequential_count ON sma_sequential_count.document_type_id = sma_documents_types.id AND sma_sequential_count.date >= '{INI_DATE}' AND sma_sequential_count.date <= '{END_DATE}'
            LEFT JOIN sma_expenses ON sma_expenses.document_type_id = sma_documents_types.id AND sma_expenses.date >= '{INI_DATE}' AND sma_expenses.date <= '{END_DATE}'
            LEFT JOIN sma_pos_register_movements ON sma_pos_register_movements.document_type_id = sma_documents_types.id AND sma_pos_register_movements.date >= '{INI_DATE}' AND sma_pos_register_movements.date <= '{END_DATE}'
            LEFT JOIN sma_gift_card_topups ON sma_gift_card_topups.document_type_id = sma_documents_types.id AND sma_gift_card_topups.date >= '{INI_DATE}' AND sma_gift_card_topups.date <= '{END_DATE}'
            LEFT JOIN sma_documents_reception_events ON sma_documents_reception_events.document_type_id = sma_documents_types.id AND sma_documents_reception_events.createdAt >= '{INI_DATE}' AND sma_documents_reception_events.createdAt <= '{END_DATE}'
            LEFT JOIN sma_production_order ON sma_production_order.document_type_id = sma_documents_types.id AND sma_production_order.date >= '{INI_DATE}' AND sma_production_order.date <= '{END_DATE}'
            LEFT JOIN sma_stock_counts ON sma_stock_counts.document_type_id = sma_documents_types.id AND sma_stock_counts.date >= '{INI_DATE}' AND sma_stock_counts.date <= '{END_DATE}'
            LEFT JOIN sma_wms_picking ON sma_wms_picking.document_type_id = sma_documents_types.id AND sma_wms_picking.date >= '{INI_DATE}' AND sma_wms_picking.date <= '{END_DATE}'
            LEFT JOIN sma_payroll_electronic_employee ON sma_payroll_electronic_employee.document_type_id = sma_documents_types.id AND sma_payroll_electronic_employee.creation_date >= '{INI_DATE}' AND sma_payroll_electronic_employee.creation_date <= '{END_DATE}'
            LEFT JOIN sma_guests_register ON sma_guests_register.document_type_id = sma_documents_types.id AND sma_guests_register.registration_date >= '{INI_DATE}' AND sma_guests_register.registration_date <= '{END_DATE}'
            GROUP BY sma_documents_types.id
            ;";
        $query4 = "SELECT
            sma_settings.numero_documento as id_customer,
            sma_settings.wappsi_customer_address_id as id_sucursal_cliente,
            sma_documents_types.module AS submodule_id,
            sma_documents_types.sales_prefix AS document_type,
            CASE
            WHEN sma_documents_types.module IN (1,3,2,4,26,27) THEN COALESCE(COUNT(sma_sales.id), 0)
            WHEN sma_documents_types.module IN (5,35,6,21,22,52) THEN COALESCE(COUNT(sma_purchases.id), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(sma_quotes.id), 0)
            WHEN sma_documents_types.module IN (19,13,31,14,28,29,33,20,32,17,23,24,34) THEN COALESCE(COUNT(sma_payments.id), 0)
            WHEN sma_documents_types.module IN (36) THEN COALESCE(COUNT(sma_expenses.id), 0)
            WHEN sma_documents_types.module IN (37) THEN COALESCE(COUNT(sma_pos_register_movements.id), 0)
            WHEN sma_documents_types.module IN (53) THEN COALESCE(COUNT(sma_gift_card_topups.id), 0)
            WHEN sma_documents_types.module IN (46) THEN COALESCE(COUNT(sma_deliveries.id), 0)
            WHEN sma_documents_types.module IN (49,50,51,48) THEN COALESCE(COUNT(sma_documents_reception_events.id), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(sma_quotes.id), 0)
            WHEN sma_documents_types.module IN (8) THEN COALESCE(COUNT(sma_order_sales.id), 0)
            WHEN sma_documents_types.module IN (18) THEN COALESCE(COUNT(sma_production_order.id), 0)
            WHEN sma_documents_types.module IN (15,30) THEN COALESCE(COUNT(sma_deposits.id), 0)
            WHEN sma_documents_types.module IN (11,25,40,42) THEN COALESCE(COUNT(sma_adjustments.id), 0)
            WHEN sma_documents_types.module IN (12) THEN COALESCE(COUNT(sma_transfers.id), 0)
            WHEN sma_documents_types.module IN (38) THEN COALESCE(COUNT(sma_stock_counts.id), 0) + COALESCE(COUNT(sma_sequential_count.id), 0)
            WHEN sma_documents_types.module IN (39) THEN COALESCE(COUNT(sma_production_order.id), 0)
            WHEN sma_documents_types.module IN (41) THEN COALESCE(COUNT(sma_wms_picking.id), 0)
            WHEN sma_documents_types.module IN (43,44,45) THEN COALESCE(COUNT(sma_payroll_electronic_employee.id), 0)
            WHEN sma_documents_types.module IN (47) THEN COALESCE(COUNT(sma_guests_register.id), 0)
            ELSE 'error' END AS lastmonth_sum
            FROM sma_documents_types
            INNER JOIN sma_settings ON setting_id = 1
            LEFT JOIN sma_sales ON sma_sales.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_sales.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_purchases ON sma_purchases.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_purchases.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_payments ON sma_payments.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_payments.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_transfers ON sma_transfers.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_transfers.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_adjustments ON sma_adjustments.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_adjustments.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_order_sales ON sma_order_sales.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_order_sales.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_deliveries ON sma_deliveries.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_deliveries.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_deposits ON sma_deposits.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_deposits.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_quotes ON sma_quotes.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_quotes.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_sequential_count ON sma_sequential_count.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_sequential_count.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_expenses ON sma_expenses.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_expenses.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_pos_register_movements ON sma_pos_register_movements.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_pos_register_movements.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_gift_card_topups ON sma_gift_card_topups.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_gift_card_topups.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_documents_reception_events ON sma_documents_reception_events.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_documents_reception_events.createdAt, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_production_order ON sma_production_order.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_production_order.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_stock_counts ON sma_stock_counts.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_stock_counts.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_wms_picking ON sma_wms_picking.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_wms_picking.date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_payroll_electronic_employee ON sma_payroll_electronic_employee.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_payroll_electronic_employee.creation_date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            LEFT JOIN sma_guests_register ON sma_guests_register.document_type_id = sma_documents_types.id AND DATE_FORMAT(sma_guests_register.registration_date, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), '%Y-%m')
            GROUP BY sma_documents_types.id
            ;";
        $query5 = "SELECT
            sma_settings.numero_documento as id_customer,
            sma_settings.wappsi_customer_address_id as id_sucursal_cliente,
            CONCAT(sma_settings.nombre_comercial, ' - ', sma_settings.site_name) AS customer_name,
            current_timestamp() AS update_at,
            CASE
            WHEN sma_documents_types.module IN ('1' ,'3' ,'19' ,'2' ,'4' ,'13' ,'31' ,'14' ,'26' ,'27' ,'28' ,'29' ,'33' ,'36' ,'37' ,'53' ) THEN 'ventas'
            WHEN sma_documents_types.module IN ('5' ,'35' ,'6' ,'20' ,'32' ,'17' ,'21' ,'22' ,'23' ,'24' ,'34' ,'52' , 49, 50, 51, 48) THEN 'compras'
            WHEN sma_documents_types.module IN ('7') THEN 'cotizacion'
            WHEN sma_documents_types.module IN ('8' ,'9' ,'10' ,'18') THEN 'ordenes'
            WHEN sma_documents_types.module IN ('15' ,'30') THEN 'anticipos'
            WHEN sma_documents_types.module IN ('11' ,'12' ,'25' ,'38' ,'39' ,'40' ,'41' ,'42') THEN 'productos'
            WHEN sma_documents_types.module IN ('43' ,'44' ,'45' ) THEN 'nómina electrónica'
            WHEN sma_documents_types.module IN ('47') THEN 'huéspedes'
            ELSE 'error' END AS module,
            sma_documents_types.module AS submodule_id,
            sma_documents_types.sales_prefix AS document_type,
            sma_documents_types.factura_electronica AS e_document,
            1 AS first_no,
            sma_documents_types.sales_consecutive AS current_no,
            CASE
            WHEN sma_documents_types.module IN (1,3,2,4,26,27) THEN COALESCE(COUNT(sma_sales.id), 0)
            WHEN sma_documents_types.module IN (5,35,6,21,22,52) THEN COALESCE(COUNT(sma_purchases.id), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(sma_quotes.id), 0)
            WHEN sma_documents_types.module IN (19,13,31,14,28,29,33,20,32,17,23,24,34) THEN COALESCE(COUNT(sma_payments.id), 0)
            WHEN sma_documents_types.module IN (36) THEN COALESCE(COUNT(sma_expenses.id), 0)
            WHEN sma_documents_types.module IN (37) THEN COALESCE(COUNT(sma_pos_register_movements.id), 0)
            WHEN sma_documents_types.module IN (53) THEN COALESCE(COUNT(sma_gift_card_topups.id), 0)
            WHEN sma_documents_types.module IN (46) THEN COALESCE(COUNT(sma_deliveries.id), 0)
            WHEN sma_documents_types.module IN (49,50,51,48) THEN COALESCE(COUNT(sma_documents_reception_events.id), 0)
            WHEN sma_documents_types.module IN (7,9,10) THEN COALESCE(COUNT(sma_quotes.id), 0)
            WHEN sma_documents_types.module IN (8) THEN COALESCE(COUNT(sma_order_sales.id), 0)
            WHEN sma_documents_types.module IN (18) THEN COALESCE(COUNT(sma_production_order.id), 0)
            WHEN sma_documents_types.module IN (15,30) THEN COALESCE(COUNT(sma_deposits.id), 0)
            WHEN sma_documents_types.module IN (11,25,40,42) THEN COALESCE(COUNT(sma_adjustments.id), 0)
            WHEN sma_documents_types.module IN (12) THEN COALESCE(COUNT(sma_transfers.id), 0)
            WHEN sma_documents_types.module IN (38) THEN COALESCE(COUNT(sma_stock_counts.id), 0) + COALESCE(COUNT(sma_sequential_count.id), 0)
            WHEN sma_documents_types.module IN (39) THEN COALESCE(COUNT(sma_production_order.id), 0)
            WHEN sma_documents_types.module IN (41) THEN COALESCE(COUNT(sma_wms_picking.id), 0)
            WHEN sma_documents_types.module IN (43,44,45) THEN COALESCE(COUNT(sma_payroll_electronic_employee.id), 0)
            WHEN sma_documents_types.module IN (47) THEN COALESCE(COUNT(sma_guests_register.id), 0)
            ELSE 'error' END AS last365days_sum
            FROM sma_documents_types
            INNER JOIN sma_settings ON setting_id = 1
            LEFT JOIN sma_sales ON sma_sales.document_type_id = sma_documents_types.id AND sma_sales.date >= '{INI_DATE}' AND sma_sales.date <= '{END_DATE}'
            LEFT JOIN sma_purchases ON sma_purchases.document_type_id = sma_documents_types.id AND sma_purchases.date >= '{INI_DATE}' AND sma_purchases.date <= '{END_DATE}'
            LEFT JOIN sma_payments ON sma_payments.document_type_id = sma_documents_types.id AND sma_payments.date >= '{INI_DATE}' AND sma_payments.date <= '{END_DATE}'
            LEFT JOIN sma_transfers ON sma_transfers.document_type_id = sma_documents_types.id AND sma_transfers.date >= '{INI_DATE}' AND sma_transfers.date <= '{END_DATE}'
            LEFT JOIN sma_adjustments ON sma_adjustments.document_type_id = sma_documents_types.id AND sma_adjustments.date >= '{INI_DATE}' AND sma_adjustments.date <= '{END_DATE}'
            LEFT JOIN sma_order_sales ON sma_order_sales.document_type_id = sma_documents_types.id AND sma_order_sales.date >= '{INI_DATE}' AND sma_order_sales.date <= '{END_DATE}'
            LEFT JOIN sma_deliveries ON sma_deliveries.document_type_id = sma_documents_types.id AND sma_deliveries.date >= '{INI_DATE}' AND sma_deliveries.date <= '{END_DATE}'
            LEFT JOIN sma_deposits ON sma_deposits.document_type_id = sma_documents_types.id AND sma_deposits.date >= '{INI_DATE}' AND sma_deposits.date <= '{END_DATE}'
            LEFT JOIN sma_quotes ON sma_quotes.document_type_id = sma_documents_types.id AND sma_quotes.date >= '{INI_DATE}' AND sma_quotes.date <= '{END_DATE}'
            LEFT JOIN sma_sequential_count ON sma_sequential_count.document_type_id = sma_documents_types.id AND sma_sequential_count.date >= '{INI_DATE}' AND sma_sequential_count.date <= '{END_DATE}'
            LEFT JOIN sma_expenses ON sma_expenses.document_type_id = sma_documents_types.id AND sma_expenses.date >= '{INI_DATE}' AND sma_expenses.date <= '{END_DATE}'
            LEFT JOIN sma_pos_register_movements ON sma_pos_register_movements.document_type_id = sma_documents_types.id AND sma_pos_register_movements.date >= '{INI_DATE}' AND sma_pos_register_movements.date <= '{END_DATE}'
            LEFT JOIN sma_gift_card_topups ON sma_gift_card_topups.document_type_id = sma_documents_types.id AND sma_gift_card_topups.date >= '{INI_DATE}' AND sma_gift_card_topups.date <= '{END_DATE}'
            LEFT JOIN sma_documents_reception_events ON sma_documents_reception_events.document_type_id = sma_documents_types.id AND sma_documents_reception_events.createdAt >= '{INI_DATE}' AND sma_documents_reception_events.createdAt <= '{END_DATE}'
            LEFT JOIN sma_production_order ON sma_production_order.document_type_id = sma_documents_types.id AND sma_production_order.date >= '{INI_DATE}' AND sma_production_order.date <= '{END_DATE}'
            LEFT JOIN sma_stock_counts ON sma_stock_counts.document_type_id = sma_documents_types.id AND sma_stock_counts.date >= '{INI_DATE}' AND sma_stock_counts.date <= '{END_DATE}'
            LEFT JOIN sma_wms_picking ON sma_wms_picking.document_type_id = sma_documents_types.id AND sma_wms_picking.date >= '{INI_DATE}' AND sma_wms_picking.date <= '{END_DATE}'
            LEFT JOIN sma_payroll_electronic_employee ON sma_payroll_electronic_employee.document_type_id = sma_documents_types.id AND sma_payroll_electronic_employee.creation_date >= '{INI_DATE}' AND sma_payroll_electronic_employee.creation_date <= '{END_DATE}'
            LEFT JOIN sma_guests_register ON sma_guests_register.document_type_id = sma_documents_types.id AND sma_guests_register.registration_date >= '{INI_DATE}' AND sma_guests_register.registration_date <= '{END_DATE}'
            GROUP BY sma_documents_types.id
            ;";

        set_time_limit(0);

        // Nombre del archivo basado en la fecha y hora
        $nombre_archivo = "customers_data_$code.txt";
        // Ruta del archivo
        $ruta_archivo = "app/logs/$nombre_archivo";
        // Abrir el archivo en modo escritura

        $url = admin_url()."mobile/set_customers_data";
        $DBGLOBAL = $this->site->get_erp_db();
        if ($DBGLOBAL) {
            if (!$id_customer_database) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $code = '';
                $length = 6;
                for ($i = 0; $i < $length; $i++) {
                    $randomIndex = rand(0, strlen($characters) - 1);
                    $code .= $characters[$randomIndex];
                }
                $DBGLOBAL->update('wp_app_settings', ['processing_code'=> $code]);
                $wappsi_customers_databases = $DBGLOBAL->not_like('wappsi_customers_databases.database', 'prueba')
                           ->not_like('wappsi_customers_databases.database', 'demo')
                           ->where('customer_data', 1)
                           ->where('(processing_code != "'.$code.'" OR processing_code IS NULL)')
                           ->order_by('id asc')->limit(1)
                           ->get('wappsi_customers_databases');
                if ($wappsi_customers_databases->num_rows() > 0) {
                    $wcd = $wappsi_customers_databases->row();
                    $targetUrl = $url."/".$wcd->id."/".$code;
                    header("Location: $targetUrl");
                    exit;
                } else {
                    $this->redirect_cd(false, false, false);
                }

            } else {
                $wappsi_customers_databases = $DBGLOBAL->where('id', $id_customer_database)
                                                        ->get('wappsi_customers_databases');

                $wappsi_customers_databases2 = $DBGLOBAL->not_like('wappsi_customers_databases.database', 'prueba')
                           ->not_like('wappsi_customers_databases.database', 'demo')
                           ->where('customer_data', 1)
                           ->where('(processing_code != "'.$code.'" OR processing_code IS NULL)')
                           ->order_by('id asc')->limit(1)
                           ->get('wappsi_customers_databases');
                $wcd2 = false;
                if ($wappsi_customers_databases2->num_rows() > 0) {
                    $wcd2 = $wappsi_customers_databases2->row();
                }
            }

            if ($wappsi_customers_databases->num_rows() > 0) {
                $wcd = $wappsi_customers_databases->row();
                if ($this->sma->wappsi_decrypt($wcd->hostname, 'wappsi') != 'localhost') {
                    $this->write_cd_log($ruta_archivo, "INICIO ".date('H:i:s')." Base de datos ".$wcd->database." Host ".$this->sma->wappsi_decrypt($wcd->hostname, 'wappsi')."  \n");
                    $wcdDB_q_new_config['hostname'] = $this->sma->wappsi_decrypt($wcd->hostname, 'wappsi');
                    $wcdDB_q_new_config['username'] = $this->sma->wappsi_decrypt($wcd->username, 'wappsi');
                    $wcdDB_q_new_config['password'] = $this->sma->wappsi_decrypt($wcd->password, 'wappsi');
                    $wcdDB_q_new_config['database'] = $wcd->database;
                    $wcdDB_q_new_config['dbdriver'] = $this->db->dbdriver;
                    $wcdDB_q_new_config['dbprefix'] = '';
                    $wcdDB_q_new_config['db_debug'] = $this->db->db_debug;
                    $wcdDB_q_new_config['cache_on'] = $this->db->cache_on;
                    $wcdDB_q_new_config['cachedir'] = $this->db->cachedir;
                    $wcdDB_q_new_config['port']     = $this->db->port;
                    $wcdDB_q_new_config['char_set'] = $this->db->char_set;
                    $wcdDB_q_new_config['dbcollat'] = $this->db->dbcollat;
                    $wcdDB_q_new_config['pconnect'] = $this->db->pconnect;
                    try {
                        $conn = new mysqli($wcdDB_q_new_config['hostname'] , $wcdDB_q_new_config['username'] , $wcdDB_q_new_config['password'] , $wcdDB_q_new_config['database']);
                        // Verificar si la conexión tiene errores
                        if ($conn->connect_error) {
                            $this->write_cd_log($ruta_archivo, "-- ERROR ".date('H:i:s')." CONEXIÓN Base de datos ".$wcd->database."\n");
                            $this->redirect_cd($wcd2, $code, $url, $DBGLOBAL);
                        }
                        // Puedes realizar consultas y operaciones con la base de datos aquí
                        $conn->close();
                    } catch (Exception $e) {
                        $this->write_cd_log($ruta_archivo, "-- ERROR ".date('H:i:s')." CONEXIÓN Base de datos ".$wcd->database."\n");
                        $this->redirect_cd($wcd2, $code, $url, $DBGLOBAL);
                    }
                    $wcdDB = $this->load->database($wcdDB_q_new_config, TRUE);
                    $wcdDB_settings = $wcdDB->query('Select * from sma_settings');
                    if ($wcdDB_settings->num_rows() > 0) {
                        $wcdDB_settings = $wcdDB_settings->row();
                        if (!property_exists($wcdDB_settings, 'wappsi_customer_address_id')) {
                            $this->write_cd_log($ruta_archivo, "-- FALTA CAMPO ".date('H:i:s')." wappsi_customer_address_id Base de datos ".$wcd->database."\n");
                            $this->redirect_cd($wcd2, $code, $url, $DBGLOBAL);
                        }
                    }
                    //CONSULTA DOCUMENTOS
                    $wcdDB_q = $wcdDB->query($query);
                    $first_customer_data = true;
                    if ($wcdDB_q->num_rows() > 0) {
                        foreach (($wcdDB_q->result()) as $wcdDB_row) {
                            if ($first_customer_data) {
                                $DBGLOBAL->delete('wappsi_customers_data', ['id_customer' => $wcdDB_row->id_customer,'id_sucursal_cliente' => $wcdDB_row->id_sucursal_cliente]);
                                $first_customer_data = false;
                            }
                            $DBGLOBAL->insert('wappsi_customers_data', $wcdDB_row);
                        }
                    }
                    //CONSULTA DOCUMENTOS
                    //CONSULTA DATOS DE PLATAFORMA
                    $wcdDB_q2 = $wcdDB->query($query2);
                    if ($wcdDB_q2->num_rows() > 0) {
                        foreach (($wcdDB_q2->result()) as $wcdDB_row2) {
                            $DBGLOBAL->insert('wappsi_customers_data', $wcdDB_row2);

                            $db_name = $wcdDB_q_new_config['database'];
                            $db_name_arr = explode("_", $db_name);
                            $db_name_base = $db_name_arr[0]."_".$db_name_arr[1];
                            $wcdDB_q = $wcdDB->query("SHOW DATABASES LIKE '%{$db_name_base}%';");
                            if ($wcdDB_q->num_rows() > 0) {
                                $DBGLOBAL->update('wappsi_customers_data', ['databases_no'=>$wcdDB_q->num_rows()], ['id_customer'=>$wcdDB_row2->id_customer,'id_sucursal_cliente' => $wcdDB_row->id_sucursal_cliente]);
                            }

                        }
                    }
                    //CONSULTA DATOS DE PLATAFORMA
                    //CONSULTA lastmonth_sum
                    $wcdDB_q4 = $wcdDB->query($query4);
                    if ($wcdDB_q4->num_rows() > 0) {
                        foreach (($wcdDB_q4->result()) as $wcdDB_row4) {
                            $DBGLOBAL->update('wappsi_customers_data', ['lastmonth_sum'=>$wcdDB_row4->lastmonth_sum], [
                                'id_customer' => $wcdDB_row4->id_customer,
                                'id_sucursal_cliente' => $wcdDB_row4->id_sucursal_cliente,
                                'submodule_id' => $wcdDB_row4->submodule_id,
                                'document_type' => $wcdDB_row4->document_type,
                            ]);
                        }
                    }
                    //CONSULTA lastmonth_sum

                    //CONSULTA lastyear_sum
                    $anioActual = date('Y');
                    $anioAnterior = $anioActual - 1;
                    // Recorrer los meses del año
                    for ($mes = 1; $mes <= 12; $mes++) {
                        // Obtener el primer y último día del mes
                        $mes = $mes < 10 ? "0".$mes : $mes;
                        $primerDiaMes = "$anioAnterior-$mes-01";
                        $ultimoDiaMes = date('Y-m-t', strtotime($primerDiaMes));
                        // Construir la consulta SQL
                        $consulta = str_replace("{INI_DATE}", $primerDiaMes, $query3);
                        $consulta = str_replace("{END_DATE}", $ultimoDiaMes, $consulta);
                        $wcdDB_q3 = $wcdDB->query($consulta);
                        // Ejecutar la consulta
                        if ($wcdDB_q3->num_rows() > 0) {
                            foreach (($wcdDB_q3->result()) as $wcdDB_row3) {
                                $DBGLOBAL->update('wappsi_customers_data', ['lastyear_sum'=> 'lastyear_sum + '.$wcdDB_row3->lastyear_sum], [
                                    'id_customer' => $wcdDB_row3->id_customer,
                                    'id_sucursal_cliente' => $wcdDB_row3->id_sucursal_cliente,
                                    'submodule_id' => $wcdDB_row3->submodule_id,
                                    'document_type' => $wcdDB_row3->document_type,
                                ]);
                            }
                        }
                    }
                    //CONSULTA lastyear_sum


                    //CONSULTA last365days_sum
                    for ($i = 365; $i >= 1; $i -= 100) {
                        $fechaInicio = date('Y-m-d', strtotime("-$i days"));
                        $fechaFin = date('Y-m-d', strtotime("-" . ($i - 99) . " days"));
                        // Construir la consulta SQL
                        $consulta = str_replace("{INI_DATE}", $fechaInicio, $query5);
                        $consulta = str_replace("{END_DATE}", $fechaFin, $consulta);
                        $wcdDB_q5 = $wcdDB->query($consulta);
                        // Ejecutar la consulta
                        if ($wcdDB_q5->num_rows() > 0) {
                            foreach (($wcdDB_q5->result()) as $wcdDB_row5) {
                                $DBGLOBAL->update('wappsi_customers_data', ['last365days_sum'=> 'last365days_sum + '.$wcdDB_row5->last365days_sum], [
                                    'id_customer' => $wcdDB_row5->id_customer,
                                    'id_sucursal_cliente' => $wcdDB_row5->id_sucursal_cliente,
                                    'submodule_id' => $wcdDB_row5->submodule_id,
                                    'document_type' => $wcdDB_row5->document_type,
                                ]);
                            }
                        }
                    }
                    //CONSULTA last365days_sum

                }
                $this->write_cd_log($ruta_archivo, "FIN ".date('H:i:s')." Base de datos ".$wcd->database."\n");
                $this->redirect_cd($wcd2, $code, $url, $DBGLOBAL);
            }
        }

    }

    function redirect_cd($new_cd = false, $code = false, $url = false, $DBGLOBAL = false){
            if ($new_cd) {
                $DBGLOBAL->update('wappsi_customers_databases', ['processing_code'=>$code], ['id'=>$new_cd->id]);
                $targetUrl = $url."/".$new_cd->id."/".$code;
                header("Location: $targetUrl");
                exit;
            } else {
                echo "fin";
                exit;
            }
    }

    function write_cd_log($ruta_archivo, $contenido){
        if (file_exists($ruta_archivo)) {
            // Si el archivo ya existe, abrimos el archivo en modo append (agregar).
            $archivo = fopen($ruta_archivo, 'a');
            if ($archivo) {
                fwrite($archivo, $contenido);
                fclose($archivo);
            }
        } else {
            // Si el archivo no existe, lo creamos y escribimos el contenido.
            $archivo = fopen($ruta_archivo, 'w');
            if ($archivo) {
                fwrite($archivo, $contenido);
                fclose($archivo);
            }
        }
    }
}
?>