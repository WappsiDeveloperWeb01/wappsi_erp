<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentReception_model extends CI_Model {

    /**
     * Métodos para el servidor de recepción de documentos
     */
    public function newInstance()
    {
        $newConfiguration['hostname'] = $this->settings->hostname;
        $newConfiguration['username'] = $this->settings->hostUserName;
        $newConfiguration['password'] = $this->settings->hostPassword;
        $newConfiguration['database'] = $this->settings->hostnameDatabase;
        $newConfiguration['dbdriver'] = 'mysqli';

        if ($this->site->check_database($newConfiguration)) {
            $newDB = $this->load->database($newConfiguration, true);
            return $newDB;
        }
        return false;
    }

    public function getDocumentsReceived($receiverNit, $documentStatus)
    {
        $newDb = $this->newInstance();

        $newDb->where('receiverNit', $receiverNit);
        $newDb->where('documentStatus', $documentStatus);
        $q = $newDb->get('document_electronic');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function getDocumentEvent($receiverNit, $eventStatus)
    {
        $newDb = $this->newInstance();

        $newDb->select('events.*, document_electronic.documentStatus');
        $newDb->where('receiverNit', $receiverNit);
        $newDb->where('eventStatus', $eventStatus);
        $newDb->join('document_electronic', 'document_electronic.documentId = events.documentElectronic_documentId
                    AND document_electronic.supplierNit = events.documentElectronic_supplierNit
                    AND document_electronic.receiverNit = events.documentElectronic_receiverNit', 'left');
        $q = $newDb->get('events');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function updateDocumentReceived($data, $id)
    {
        $newDb = $this->newInstance();

        $newDb->where('id', $id);
        if ($newDb->update('document_electronic', $data)) {
            return true;
        }
        return false;
    }

    public function updateEventDocument($data, $id)
    {
        $newDb = $this->newInstance();

        $newDb->where('id', $id);
        if ($newDb->update('events', $data)) {
            return true;
        }
        return false;
    }

    /**
     * Métodos para el cliente
     */

     public function getAllDocumentsReceivedClient()
     {
         $q = $this->db->get('documents_reception');
         if ($q->num_rows() > 0) {
             return $q->result();
         }
         return false;
     }

    public function getDocumentsReceivedClient($start_date, $end_date, $supplier = NULL)
    {
        $this->db->select("documents_reception.*,
            TIMESTAMPDIFF(HOUR, createdAt, NOW()) AS hoursDiff,
            companies.name AS supplierName,
            (SELECT grand_total FROM sma_purchases WHERE SUBSTRING(reference_no, LOCATE('-', reference_no) + 1) = documentId AND supplier_id = supplierNit) AS grandTotal,
            (SELECT id FROM sma_documents_reception_events WHERE documentElectronic_documentId = documentId AND documentElectronic_supplierNit = supplierNit AND documentElectronic_receiverNit = receiverNit AND statusCode = '030' LIMIT 1) AS idAcknowledgment,
            (SELECT id FROM sma_documents_reception_events WHERE documentElectronic_documentId = documentId AND documentElectronic_supplierNit = supplierNit AND documentElectronic_receiverNit = receiverNit AND statusCode = '031' LIMIT 1) AS idClaim,
            (SELECT id FROM sma_documents_reception_events WHERE documentElectronic_documentId = documentId AND documentElectronic_supplierNit = supplierNit AND documentElectronic_receiverNit = receiverNit AND statusCode = '032' LIMIT 1) AS idAcknowledgmentGoodService,
            (SELECT id FROM sma_documents_reception_events WHERE documentElectronic_documentId = documentId AND documentElectronic_supplierNit = supplierNit AND documentElectronic_receiverNit = receiverNit AND statusCode = '033' LIMIT 1) AS idExpressAcceptance
        ");
        $this->db->join('companies', 'companies.vat_no = documents_reception.supplierNit AND companies.status = 1', 'left');
        $this->db->join('groups', "groups.id = companies.group_id AND groups.name = 'supplier'", 'inner');
        $this->db->where_in('documentTypeCode', ['01', '02', '03', '04']);

        if (!empty($start_date)) {
            $this->db->where("documents_reception.documentDate >=", $start_date);
        }

        if (!empty($end_date)) {
            $this->db->where("documents_reception.documentDate <=", $end_date);
        }

        if (!empty($supplier)) {
            $this->db->where("companies.id", $supplier);
        }

        $q = $this->db->get('documents_reception');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function getDocumentReceivedClient($parentData)
    {
        $this->db->where($parentData);
        $q = $this->db->get('documents_reception');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getDocumentReceivedByIdClient($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get('documents_reception');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getDocumentReceivedWithOutAcknowledgmentClient()
    {
        $this->db->where('documents_reception.acknowledgment_receipt', NOT);
        $q = $this->db->get('documents_reception');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function getDocumentEventClient($parentData, $eventStatus = NULL)
    {
        $this->db->where($parentData);
        if (!empty($eventStatus)) {
            $this->db->where('statusCode', $eventStatus);
        }
        $q = $this->db->get('documents_reception_events');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getDocumentEventClientById($eventId)
    {
        $this->db->where('id', $eventId);
        $q = $this->db->get('documents_reception_events');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getSettings()
    {
        $q = $this->db->get('documents_reception_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getPurchaseByReferenceNo($referenceNo)
    {
        // $this->db->like('reference_no', $referenceNo);
        $this->db->where("REPLACE(reference_no, ' ', '') LIKE '%{$referenceNo}'");
        $q = $this->db->get("purchases");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function saveDocumentClient($data)
    {
        if ($this->db->insert('documents_reception', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function saveDocumentEventClient($data)
    {
        if ($this->db->insert('documents_reception_events', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function saveSettings($data)
    {
        if ($this->db->insert('documents_reception_settings', $data)) {
            return true;
        }
        return false;
    }

    public function updateDocumentClient($data, $id)
    {
        $this->db->where('id', $id);
        if ($this->db->update('documents_reception', $data)) {
            return true;
        }
        return false;
    }

    public function updateEventDocumentClient($data, $id)
    {
        $this->db->where('id', $id);
        if ($this->db->update('documents_reception_events', $data)) {
            return true;
        }
        return false;
    }

    public function updateSettings($data, $id)
    {
        $this->db->where('id', $id);
        if ($this->db->update('documents_reception_settings', $data)) {
            return true;
        }
        return false;
    }

    public function updatePurchase($data, $id)
    {
        $this->db->where('id', $id);
        if ($this->db->update('purchases', $data)) {
            return true;
        }
        return false;
    }
}

/* End of file DocumentReception_model */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/DocumentReception_model */