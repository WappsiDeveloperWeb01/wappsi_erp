<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
 *  ==============================================================================
 *  Author    : Mian Saleem
 *  Email     : saleem@tecdiary.com
 *  For       : Stock Manager Advance
 *  Web       : http://tecdiary.com
 *  ==============================================================================
 */

class Gscat
{

    public function __construct() {
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    function summary($rows = [], $return_rows = [], $product_tax = 0, $onCost = false, $item_order = "0") {
        $code = '';
        if ($this->Settings->invoice_view > 0 && !empty($rows)) {
            $tax_summary = $this->taxSummary($rows, $onCost, $item_order);
            if (!empty($return_rows)) {
                $return_tax_summary = $this->taxSummary($return_rows, $onCost);
                $tax_summary = $tax_summary + $return_tax_summary;
            }
            $code = $this->genHTML($tax_summary, $product_tax, $item_order);
        }
        return $code;
    }

    function taxSummary($rows = [], $onCost = false, $item_order = NULL) {
        $tax_summary = [];
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $indice_orden_item = isset($row->category_id) ? $row->category_id : $row->brand_id;
                if (isset($tax_summary[$indice_orden_item])) {
                    $tax_summary[$indice_orden_item]['items'] += $row->quantity;
                    $tax_summary[$indice_orden_item]['tax'] += $row->item_tax;
                    $tax_summary[$indice_orden_item]['amt'] += ($row->quantity * ($row->unit_price));
                } else {
                    $tax_summary[$indice_orden_item]['items'] = $row->quantity;
                    $tax_summary[$indice_orden_item]['tax'] = $row->item_tax;
                    $tax_summary[$indice_orden_item]['amt'] = ($row->quantity * ($row->unit_price));
                    $tax_summary[$indice_orden_item]['name'] = isset($row->category_id) ? $row->category_name : $row->brand_name;
                    $tax_summary[$indice_orden_item]['code'] = isset($row->category_id) ? $row->category_id : $row->brand_id;
                    $tax_summary[$indice_orden_item]['rate'] = $row->tax_rate;
                }
            }
        }
        return $tax_summary;
    }

    function genHTML($tax_summary = [], $product_tax = 0, $item_order = null) {
        $html = '';
        $total_base = 0;
        if (!empty($tax_summary)) {
            $html .= '<div style="text-align:left;"><h4 style="font-weight:bold;">' . (($item_order == "1") ? sprintf(lang('category_summary'), lang('category')) : sprintf(lang('brand_summary'), lang('brand'))) . '</h4></div>';
            $html .= '<table class="table print-table order-table table-condensed">
            <thead>
                <tr>
                    <th>' . (($item_order == "1") ? lang('category') : lang('brands')) . '</th>
                    <th>' . lang('value') . '</th>
                </tr>
            </thead>
            <tbody>';
            foreach ($tax_summary as $summary) {
                $total_base+=$summary['amt'];

                $html .= '<tr>
                            <td>' . ((!empty($summary['name'])) ? $summary['name']: strtoupper("Sin marca")) . '</td>
                            <td style="text-align:right;">' . $this->sma->formatMoney($summary['amt']) . '</td>
                        </tr>';
            }
            $html .= '</tbody></table>';
        }
        return $html;
    }


}
