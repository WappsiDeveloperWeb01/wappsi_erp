<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_creditor'); ?></h4>
        </div>
        <?= admin_form_open_multipart("creditors/edit/" . $creditor->id, ['id' => 'edit-creditor-form']); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="row">
                    <div class="form-group col-md-6">
                        <label>Tipo de persona</label>
                        <?php
                        $tpersons[1] = "Natural";
                        $tpersons[0] = "Jurídica";
                        echo form_dropdown('type_person', $tpersons, $creditor->type_person, 'class="form-control select" id="type_person" style="width:100%;" required="required"');
                         ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= form_label(lang("label_type_vat_regime"), "type_vat_regime"); ?>
                        <?php
                          $types_vat_regime_options[""] = lang('select');
                          foreach ($types_vat_regime as $type_vat_regime)
                          {
                            $types_vat_regime_options[$type_vat_regime->id] = lang($type_vat_regime->description);
                          }
                        ?>
                        <?= form_dropdown(["name"=>"tipo_regimen", "id"=>"tipo_regimen", "class"=>"form-control select", "required"=>TRUE], $types_vat_regime_options, $creditor->tipo_regimen); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= lang("id_document_type", "id_document_type"); ?>
                        <?php
                        $idts[''] = lang('select').' '.lang('id_document_type');
                        foreach ($id_document_types as $idt) {
                            $idts[$idt->id] = lang($idt->nombre);
                        }
                        echo form_dropdown('tipo_documento', $idts, $creditor->tipo_documento, 'class="form-control select" id="tipo_documento" style="width:100%;" required="required"');
                        ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= lang("vat_no", "vat_no"); ?>
                        <?php echo form_input('vat_no', $creditor->vat_no, 'class="form-control" id="vat_no" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-6 digito-verificacion" style="display: none;">
                            <?= lang("check_digit", "check_digit"); ?>
                            <?php echo form_input('digito_verificacion', $creditor->digito_verificacion, 'class="form-control" id="digito_verificacion" readonly'); ?>
                    </div>

                    <div class="form-group col-md-6 person juridical_person">
                        <?= lang("name", "name"); ?>
                        <?php echo form_input('name', $creditor->name, 'class="form-control tip required" id="name" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group person natural_person" style="display: none;">

                      <div class="col-md-6">
                        <label>Primer Nombre</label><span class='input_required'> *</span>
                        <input type="text" name="first_name" id="first_name" value="<?= $creditor->first_name ?>" class="form-control">
                      </div>
                      <div class="col-md-6">
                        <label>Segundo Nombre</label>
                        <input type="text" name="second_name" id="second_name" value="<?= $creditor->second_name ?>" class="form-control">
                      </div>
                      <div class="col-md-6">
                        <label>Primer Apellido</label><span class='input_required'> *</span>
                        <input type="text" name="first_lastname" id="first_lastname" value="<?= $creditor->first_lastname ?>" class="form-control">
                      </div>
                      <div class="col-md-6">
                        <label>Segundo Apellido</label>
                        <input type="text" name="second_lastname" id="second_lastname" value="<?= $creditor->second_lastname ?>" class="form-control">
                      </div>

                    </div>

                    <div class="form-group col-md-6">
                        <?= lang("email_address", "email_address"); ?>
                        <input type="email" name="email" class="form-control" required="required" value="<?= $creditor->email ?>" id="email_address"/>
                    </div>
                    <div class="form-group col-md-6">
                        <?= lang("phone", "phone"); ?>
                        <input type="tel" name="phone" class="form-control" required="required" value="<?= $creditor->phone ?>" id="phone"/>
                    </div>
                    <div class="form-group col-md-6">
                        <?= lang("address", "address"); ?>
                        <?php echo form_input('address', $creditor->address, 'class="form-control" id="address" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= lang("postal_code", "postal_code"); ?>
                        <?php echo form_input('postal_code', $creditor->postal_code, 'class="form-control postal_code" id="postal_code"'); ?>
                    </div>

                    <div class="form-group col-md-6">
                    <?= lang("country", "country"); ?>
                      <select class="form-control select" name="country" id="country" required>
                        <option value="">Seleccione...</option>
                        <?php foreach ($countries as $row => $country): ?>
                          <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= ($country->NOMBRE == $creditor->country) ? "selected='selected'" : "" ?> ><?= $country->NOMBRE ?></option>
                        <?php endforeach ?>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                        <?= lang("state", "state"); ?>
                      <select class="form-control select" name="state" id="state" required>
                        <option value="">Seleccione...</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                        <?= lang("city", "city"); ?>
                      <select class="form-control select" name="city" id="city" required>
                        <option value="">Seleccione...</option>
                        <!-- <option value="<?= $creditor->city ?>"><?= $creditor->city ?></option> -->
                      </select>
                      <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
                    </div>
                    <!-- <div class="form-group col-md-6">
                        <label>
                            <input type="checkbox" name="customer_validate_min_base_retention" id="customer_validate_min_base_retention"  <?= $creditor->customer_validate_min_base_retention ? 'checked="checked"' : '' ?>>
                            <?= lang('customer_validate_min_base_retention') ?>
                        </label>
                    </div> -->
              </div>
            <div class="row">
              <div class="form-group col-md-6">
                  <?= lang('creditor_payment_type', 'creditor_payment_type') ?>
                  <?php
                    $cptypes = [
                      '1' => lang('payment_type_cash'),
                      '0' => lang('credit'),
                    ];
                  ?>
                  <?= form_dropdown('creditor_payment_type', $cptypes, $creditor->customer_payment_type, 'class="form-control select" id="creditor_payment_type" style="width:100%;" required="required"'); ?>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6 credit_customer" style="display: none;">
                  <?= lang('creditor_credit_limit', 'creditor_credit_limit') ?>
                  <input type="text" name="creditor_credit_limit" id="creditor_credit_limit" class="form-control only_number" value="<?= $creditor->customer_credit_limit ?>">
              </div>
              <div class="form-group col-md-6 credit_customer" style="display: none;">
                  <?= lang('creditor_payment_term', 'creditor_payment_term') ?>
                  <input type="text" name="creditor_payment_term" id="creditor_payment_term" class="form-control only_number" value="<?= $creditor->customer_payment_term ?>">
              </div>
            </div>
              <div class="row">
                <div class="form-group col-md-12">
                  <table class="table">
                    <thead>
                      <tr>
                        <th><?= lang('retention') ?></th>
                        <th><?= lang('percentage') ?></th>
                        <th><?= lang('min_base') ?></th>
                        <th><?= lang('ledger') ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (count($retentions) > 0): ?>
                        <?php if (isset($retentions['FUENTE'])): ?>
                          <tr class="row_rete_fuente" <?= $creditor->fuente_retainer ? 'style="display:none;"' : '' ?>>
                            <td>
                              <select name="default_rete_fuente_id" id="default_rete_fuente_id" class="form-control">
                                <option value=""><?= lang('no_rete_fuente_default') ?></option>
                                <?php foreach ($retentions['FUENTE'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= $row['ledger_name'] ?>" data-minbase="<?= $row['min_base'] ?>" <?= $creditor->default_rete_fuente_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['IVA'])): ?>
                          <tr class="row_rete_iva" <?= $creditor->iva_retainer ? 'style="display:none;"' : '' ?>>
                            <td>
                              <select name="default_rete_iva_id" id="default_rete_iva_id" class="form-control">
                                <option value=""><?= lang('no_rete_iva_default') ?></option>
                                <?php foreach ($retentions['IVA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= $row['ledger_name'] ?>" data-minbase="<?= $row['min_base'] ?>" <?= $creditor->default_rete_iva_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['ICA'])): ?>
                          <tr class="row_rete_ica" <?= $creditor->ica_retainer ? 'style="display:none;"' : '' ?>>
                            <td>
                              <select name="default_rete_ica_id" id="default_rete_ica_id" class="form-control">
                                <option value=""><?= lang('no_rete_ica_default') ?></option>
                                <?php foreach ($retentions['ICA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= $row['ledger_name'] ?>" data-minbase="<?= $row['min_base'] ?>" <?= $creditor->default_rete_ica_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['OTRA'])): ?>
                          <tr class="row_rete_other">
                            <td>
                              <select name="default_rete_other_id" id="default_rete_other_id" class="form-control">
                                <option value=""><?= lang('no_rete_other_default') ?></option>
                                <?php foreach ($retentions['OTRA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= $row['ledger_name'] ?>" data-minbase="<?= $row['min_base'] ?>" <?= $creditor->default_rete_other_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_other_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_other_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_other_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                      <?php endif ?>
                    </tbody>
                  </table>
                </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-success" id="submit_edit_creditor" type="button"><?= lang('edit_creditor') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {

      $("#edit-creditor-form").validate({
          ignore: []
        });

      $(document).on('click', '#submit_edit_creditor', function(){
        form = $('#edit-creditor-form');
        if (form.valid()) {
          form.submit();
        }
      });

        $('#tipo_documento').on('change', function(){
          if ($(this).val() == 6 && site.settings.get_companies_check_digit == 1) {
            $('.digito-verificacion').css('display', '');
            $('.digito-verificacion input').attr('required', true);
          } else {
            $('.digito-verificacion').css('display', 'none');
            $('.digito-verificacion input').attr('required', false);
          }
        });

        $('#type_person').on('change', function(){
          tp = $(this).val();

          if (tp == 1) {

            $('input[name="first_name"]').attr('required', true);
            $('input[name="first_lastname"]').attr('required', true);

            var name_arr = $('#name').val().split(' ');
            console.log(name_arr);

            // if (name_arr.length == 2) {
            //   $('#first_name').val(name_arr[0]);
            //   $('#first_lastname').val(name_arr[1]);
            // } else if (name_arr.length == 3) {
            //   $('#first_name').val(name_arr[0]);
            //   $('#first_lastname').val(name_arr[1]);
            //   $('#second_lastname').val(name_arr[2]);
            // } else if (name_arr.length == 4) {
            //   $('#first_name').val(name_arr[0]);
            //   $('#second_name').val(name_arr[1]);
            //   $('#first_lastname').val(name_arr[2]);
            //   $('#second_lastname').val(name_arr[3]);
            // } else if (name_arr.length > 4) {
            //   $('#first_name').val('');
            //   $('#second_name').val('');
            //   $('#first_lastname').val('');
            //   $('#second_lastname').val('');
            // }

            $('.natural_person').css('display', '');

            $.each($('.natural_person').find('.required'), function(){
              $(this).attr('required', true);
            });

            $('.juridical_person').css('display', 'none');

            $.each($('.juridical_person').find('.required'), function(){
              $(this).attr('required', false);
            });

            $('#name').val('xx');

          } else if (tp == 0){

            $('input[name="first_name"]').removeAttr('required');
            $('input[name="first_lastname"]').removeAttr('required');

            $('.natural_person').css('display', 'none');

            $.each($('.natural_person').find('.required'), function(){
              $(this).attr('required', false);
            });

            $('.juridical_person').css('display', '');

            $.each($('.juridical_person').find('.required'), function(){
              $(this).attr('required', true);
            });

            $('#name').val('<?= $creditor->name ?>');
          }

        });

        $('#state').on('change', function(){
          dpto = $('#state option:selected').data('code');

          // $('.postal_code').val($('.postal_code').val()+dpto);

          $.ajax({
            url:"<?= admin_url() ?>customers/get_cities/"+dpto,
          }).done(function(data){
            $('#city').html(data);
            console.log(data);
          }).fail(function(data){
            console.log(data);
          });

        });

        $('#country').on('change', function(){
          dpto = $('#country option:selected').data('code');

          // $('.postal_code').val(dpto);

          $.ajax({
            url:"<?= admin_url() ?>customers/get_states/"+dpto,
          }).done(function(data){
            $('#state').html(data);
            console.log(data);
          }).fail(function(data){
            console.log(data);
          });

        });

        $('#city').on('change', function(){
          code = $('#city option:selected').data('code');
          $('.postal_code').val(code);
          $('#city_code').val(code);
        });

        $('#vat_no').on('change', function(){
            nit = $(this).val();
            if (site.settings.get_companies_check_digit == 1) {
              dvf = calcularDigitoVerificacion(nit);
              $('#digito_verificacion').val(dvf);
            }
        });

    });

  

    $('#type_person').trigger('change');
    $('#tipo_documento').trigger('change');

    set_ubication();

    function set_ubication(){

      country = $('#country option:selected').data('code');
      state = "<?= $creditor->state ?>";
      city = "<?= $creditor->city ?>";

      $.ajax({

        url:"<?= admin_url() ?>customers/get_states/"+country+"/"+state,

      }).done(function(data){

        $('#state').html(data);
        $('#state').select2();

        state = $('#state option:selected').data('code');
        $.ajax({

          url:"<?= admin_url() ?>customers/get_cities/"+state+"/"+city,

        }).done(function(data){

          $('#city').html(data);
          $('#city').select2();
          console.log(data);

        }).fail(function(data){
          console.log(data);
        });

        console.log(data);

      }).fail(function(data){
        console.log(data);
      });
    }

    $(document).on('change', '#default_rete_fuente_id', function(){
      var rete_fuente = $('#default_rete_fuente_id option:selected');

      var percentage = rete_fuente.data('percentage');
      var min_base = rete_fuente.data('minbase');
      var ledger = rete_fuente.data('account');

      $('.rete_fuente_percentage').val(percentage);
      $('.rete_fuente_minbase').val(min_base);
      $('.rete_fuente_ledger').val(ledger);

    });
    $(document).on('change', '#default_rete_iva_id', function(){
      var rete_iva = $('#default_rete_iva_id option:selected');

      var percentage = rete_iva.data('percentage');
      var min_base = rete_iva.data('minbase');
      var ledger = rete_iva.data('account');

      $('.rete_iva_percentage').val(percentage);
      $('.rete_iva_minbase').val(min_base);
      $('.rete_iva_ledger').val(ledger);

    });
    $(document).on('change', '#default_rete_ica_id', function(){
      var rete_ica = $('#default_rete_ica_id option:selected');

      var percentage = rete_ica.data('percentage');
      var min_base = rete_ica.data('minbase');
      var ledger = rete_ica.data('account');

      $('.rete_ica_percentage').val(percentage);
      $('.rete_ica_minbase').val(min_base);
      $('.rete_ica_ledger').val(ledger);

    });
    $(document).on('change', '#default_rete_other_id', function(){
      var rete_other = $('#default_rete_other_id option:selected');

      var percentage = rete_other.data('percentage');
      var min_base = rete_other.data('minbase');
      var ledger = rete_other.data('account');

      $('.rete_other_percentage').val(percentage);
      $('.rete_other_minbase').val(min_base);
      $('.rete_other_ledger').val(ledger);

    });

    

    $('#default_rete_fuente_id').trigger('change');
    $('#default_rete_iva_id').trigger('change');
    $('#default_rete_ica_id').trigger('change');
    $('#default_rete_other_id').trigger('change');

    $('#creditor_payment_type').on('change', function(){
      val = $(this).val();
      if (val == 0) {
        $('.credit_customer').css('display', '');
        $('#creditor_credit_limit').prop('required', true).prop('min', 1);
        $('#creditor_payment_term').prop('required', true).prop('min', 1);
      } else if (val == 1) {
        $('.credit_customer').css('display', 'none');
        $('#creditor_credit_limit').prop('required', false).prop('min', false);
        $('#creditor_payment_term').prop('required', false).prop('min', false);
      }
    });


    $('#creditor_payment_type').trigger('change');
</script>
<?= $modal_js ?>
