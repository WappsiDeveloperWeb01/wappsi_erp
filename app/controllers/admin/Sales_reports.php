<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sales_reports extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('budget', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('Budget_model');

        $this->months =  json_decode(json_encode([
            ['id' => 1, 'name' => "Enero"],
            ['id' => 2, 'name' => "Febrero"],
            ['id' => 3, 'name' => "Marzo"],
            ['id' => 4, 'name' => "Abril"],
            ['id' => 5, 'name' => "Mayo"],
            ['id' => 6, 'name' => "Junio"],
            ['id' => 7, 'name' => "Julio"],
            ['id' => 8, 'name' => "Agosto"],
            ['id' => 9, 'name' => "Septiembre"],
            ['id' => 10, 'name' => "Octubre"],
            ['id' => 11, 'name' => "Noviembre"],
            ['id' => 12, 'name' => "Diciembre"]
        ]));
    }

    public function budget_execution()
    {
        $this->sma->checkPermissions();

        $data = [
            'year' => ($this->input->post('year')) ? $this->input->post('year') : date('Y'),
            'month' => ($this->input->post('month')) ? $this->input->post('month') : date('n')
        ];

        $this->data['months'] = $this->months;
        $this->data['billers'] = $this->Budget_model->getBranchesFromBudget($data);

        if ($this->input->post('biller_id')) {
            $data['biller_id'] = $this->input->post('biller_id');
            $this->data['sellers'] = $this->Budget_model->getSellerByBillerFromBudget($data);

            if ($this->input->post('seller_id')) {
                $data['seller_id'] = $this->input->post('seller_id');
                $this->data['categories'] = $this->Budget_model->getCategoriesBySellerFromBudget($data);
            }
        }

        $this->data['advancedFiltersContainer'] = FALSE;

        $this->page_construct('sales/reports/sales_budget_execution', ["page_title" => $this->lang->line("sales_budget_execution_report")], $this->data);
    }

    public function getDataTablesBudgetExecution()
    {
        $this->load->library('datatables');

        $unitsSelect = "";
        $today = date("j");
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $biller_id = $this->input->post('biller_id');
        $seller_id = $this->input->post('seller_id');
        $see_units = $this->input->post('see_units');
        $category_id = $this->input->post('category_id');
        $billers = $this->Budget_model->getBranchesFromBudget();
        $sellers = $this->Budget_model->getSellerByBillerFromBudget();
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        if (!empty($billers)) {
            $billerSelect = "CONCAT(UCASE(SUBSTRING(b.name, 1, 1)), LCASE(SUBSTRING(b.name, 2))) AS billerName,";
        } else {
            $billerSelect = "'Todas' AS billerName,";
        }
        if (!empty($sellers)) {
            $sellerSelect = "CONCAT(UCASE(SUBSTRING(s.name, 1, 1)), LCASE(SUBSTRING(s.name, 2))) AS sellerName,";
        } else {
            $sellerSelect = "'Todos' AS sellerName,";
        }
        if (!empty($category_id)) {
            $categorySelect = "CONCAT(UCASE(SUBSTRING(c.name, 1, 1)), LCASE(SUBSTRING(c.name, 2))) AS categoryName,";
        } else {
            $categorySelect = "'Todas' AS categoryName,";
        }
        if (!empty($see_units)) {
            $unitsSelect = ", COALESCE({$this->db->dbprefix('sales_budget')}.units, 0) AS amountUnitsBudget,
                COALESCE(totals.totalUnits, 0) AS amountUnits,
                COALESCE((totals.totalUnits - {$this->db->dbprefix('sales_budget')}.units), 0) AS amountUnitsDiff,
                COALESCE((totals.totalUnits / {$this->db->dbprefix('sales_budget')}.units) * 100, 0) AS percentageUnits,
                COALESCE((({$daysInMonth} * totals.totalUnits / {$today}) / {$this->db->dbprefix('sales_budget')}.units) * 100, 0) AS projectionUnits";
        }

        $croosJoin = $this->getSalesQuery();

        $this->datatables->select("
            $billerSelect
            $sellerSelect
            $categorySelect
            COALESCE({$this->db->dbprefix('sales_budget')}.amount, 0) AS amountBudget,
            COALESCE(totals.total, 0) AS amountSales,
            COALESCE((totals.total - SUM(COALESCE({$this->db->dbprefix('sales_budget')}.amount, 0))), 0) AS amountDiff,
            COALESCE(((totals.total / SUM(COALESCE({$this->db->dbprefix('sales_budget')}.amount, 0))) * 100), 0) AS percentage,
            COALESCE(((({$daysInMonth} * totals.total / {$today}) / SUM(COALESCE({$this->db->dbprefix('sales_budget')}.amount, 0))) * 100), 0) AS projection
            $unitsSelect");
        $this->datatables->from("sales_budget");

        if (!empty($billers)) {
            $this->datatables->join('companies AS b', "b.id={$this->db->dbprefix('sales_budget')}.biller_id", 'inner');

            if (!empty($biller_id)) {
                $this->datatables->where("{$this->db->dbprefix('sales_budget')}.biller_id", $biller_id);
            }
        }

        if (!empty($sellers)) {
            $this->datatables->join('companies AS s', "s.id = {$this->db->dbprefix('sales_budget')}.seller_id", 'left');
            if (!empty($seller_id)) {
                $this->datatables->where("{$this->db->dbprefix('sales_budget')}.seller_id", $seller_id);
            }
        }

        if (!empty($category_id)) {
            $this->datatables->where('sales_budget.category_id', $category_id);
        }

        $this->datatables->join($croosJoin->query ."AS totals", $croosJoin->on, "left");
        $this->datatables->where("{$this->db->dbprefix('sales_budget')}.year", $year);
        $this->datatables->where("{$this->db->dbprefix('sales_budget')}.month", $month);

        if (!empty($billers)) {
            $this->datatables->group_by("{$this->db->dbprefix('sales_budget')}.biller_id");
        }

        if (!empty($sellers)) {
            $this->datatables->group_by("{$this->db->dbprefix('sales_budget')}.seller_id");
        }

        $this->datatables->order_by("{$this->db->dbprefix('sales_budget')}.biller_id , {$this->db->dbprefix('sales_budget')}.seller_id , {$this->db->dbprefix('sales_budget')}.category_id");


        echo $this->datatables->generate();
    }

    private function getSalesQuery()
    {
        $groupBy = "";
        $joinUnits = "";
        $whereUnits = "";
        $selectUnits = "";
        $sellerConditions = "";
        $includeCondition = "";
        $on = "totals.biller_id > 0";
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $biller_id = $this->input->post('biller_id');
        $seller_id = $this->input->post('seller_id');
        $see_units = $this->input->post('see_units');
        $include_credit_notes = $this->input->post('include_credit_notes');

        $billers = $this->Budget_model->getBranchesFromBudget();
        $sellers = $this->Budget_model->getSellerByBillerFromBudget();

        if (!empty($billers)) {
            $groupBy = "GROUP BY biller_id";
            $on = "totals.biller_id = {$this->db->dbprefix('sales_budget')}.biller_id";

            $whereUnits = "AND s.biller_id = v.biller_id";
        }
        if (!empty($sellers)) {
            $groupBy .= ", seller_id";
            $on .= " AND totals.seller_id = {$this->db->dbprefix('sales_budget')}.seller_id";

            if (!empty($sellers)) {
                $in = "";
                foreach ($sellers as $seller) {
                    $in .= "{$seller->id},";
                }
                $in = trim($in, ",");
                $sellerConditions = " AND seller_id IN ($in)";
            }

            $whereUnits = "AND s.seller_id = v.seller_id";
        }
        if (!empty($see_units)) {
            $selectUnits = ", (SELECT
                COALESCE(SUM(si.unit_quantity), 0)
            FROM sma_sale_items si
            INNER JOIN
                sma_sales s ON s.id = si.sale_id
            WHERE
                YEAR(s.date) = $year
                AND MONTH(s.date) = $month
                $whereUnits
            ) AS totalUnits";
        }

        if (empty($include_credit_notes)) {
            $includeCondition = "AND v.grand_total > 0";
        }

        $result = "(SELECT
            biller_id,
            v.seller_id,
            COALESCE(SUM(v.grand_total), 0) AS total
            $selectUnits
        FROM
            sma_sales v
            $joinUnits
        WHERE
            YEAR(date) = $year
                AND MONTH(date) = $month
                $includeCondition
                $sellerConditions
        $groupBy)";

        return (object) ["query"=>$result, "on"=>$on];
    }

    // public function getSellersConcatedToDataTables($year, $month)
    // {
    //     $this->db->select("GROUP_CONCAT(seller_id) AS ids");
    //     $this->db->where("year", $year);
    //     $this->db->where("month", $month);
    //     $q = $this->db->get("sales_budget");
    //     return $q->row();
    // }

    public function getBranchesFromBudget()
    {
        $data = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month')
        ];

        $billers = $this->Budget_model->getBranchesFromBudget($data);
        echo json_encode($billers);
    }

    public function getSellerByBillerFromBudget()
    {
        $data = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'biller_id' => $this->input->post('billerId')
        ];

        $sellers = $this->Budget_model->getSellerByBillerFromBudget($data);
        echo json_encode($sellers);
    }

    public function getCategoriesBySellerFromBudget()
    {
        $data = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'biller_id' => $this->input->post('billerId'),
            'seller_id' => $this->input->post('sellerId')
        ];

        $categories = $this->Budget_model->getCategoriesBySellerFromBudget($data);
        echo json_encode($categories);
    }
}