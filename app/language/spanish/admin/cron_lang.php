<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Cron Job
 * 
  * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */


$lang['cron_job'] = "CRON JOB";
$lang['order_ref_updated'] = "Se han restablecido los números de referencia de los pedidos";
$lang['x_pending_to_due'] = "%d el estado de las facturas pendientes se ha cambiado a vencido.";
$lang['x_partial_to_due'] = "El estado de%d facturas parcialmente pagadas se ha cambiado a vencidas.";
$lang['x_purchases_changed'] = "%d compras pendientes / parcialmente pagadas se han cambiado a vencidas.";
$lang['x_products_expired'] = "%d producto (s) con una cantidad total de %d han caducado.";
$lang['x_promotions_expired'] = "%d promociones de productos caducadas.";
$lang['user_login_deleted'] = "Se han eliminado los registros de inicio de sesión del usuario anteriores a% s.";
$lang['backup_done'] = "La copia de seguridad de la base de datos se realizó correctamente y las copias de seguridad anteriores a 30 días se eliminaron.";
$lang['update_available'] = "Nuevas actualizaciones disponibles, visite el menú de actualizaciones en la configuración.";
$lang['all_warehouses'] = "Todos los almacenes";
$lang['products_sale'] = "Ingresos por productos";
$lang['order_discount'] = "Descuento de pedido";
$lang['products_cost'] = "Costo de los productos";
$lang['expensas'] = "Gastos";
$lang['profit'] = "Beneficio";
$lang['total_sales'] = "Ventas totales";
$lang['return_sales'] = "Devolución de ventas";
$lang['total_purchases'] = "Compras totales";
$lang['general_ledger'] = "Libro mayor";
$lang['recibido'] = "Recibido";
$lang['pagado'] = "Pagado";
$lang['impuestos'] = "Impuestos";
