<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";
?>
<script type="text/javascript">
    $(document).ready(function () {

        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php if ($this->Settings->big_data_limit_reports == 0 || ($this->Settings->big_data_limit_reports == 1 && $can_filter)): ?>
                                <?php echo form_open('admin/reports/portfolio_report_2' , 'id="form_filter"'); ?>
                                <input type="hidden" name="filter_action" id="filter_action" value="0">
                                <input type="hidden" name="action" id="action">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-3">
                                            <label for="Sucursal" class="control-label"> <?= lang('branch') ?></label>
                                            <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                                <select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;" required>
                                                    <option value="" > <?= lang('select').' '.lang('branch') ?> </option>
                                                    <?php foreach ($billers as $biller): ?>
                                                        <option value="<?php echo $biller->id; ?>"><?php echo $biller->name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            <?php else: ?>
                                                <div class="form-group">
                                                    <br>
                                                    <?php if ((!$this->session->userdata('biller_id'))): ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="valueSucursal" id="biller1" value="Todas" <?= !isset($_POST['valueSucursal']) || (isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
                                                        </label>
                                                    <?php endif ?>
                                                        
                                                    <label class="radio-inline">
                                                        <input type="radio" name="valueSucursal" id="biller2" value="Una"  <?= ((isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Una') || ($this->session->userdata('biller_id'))) ? 'checked' : '' ?>> <?= lang('aa') ?>
                                                    </label>
                                                </div>
                                                
                                                <?php
                                                $bl[""] = "";
                                                $bldata = [];
                                                foreach ($billers as $biller) {
                                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                    $bldata[$biller->id] = $biller;
                                                }
                                                // exit(var_dump($this->session->userdata('biller_id')));
                                                ?>

                                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                                    <div class="collapse" id="collapseBiller">
                                                        <select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;">
                                                            <option value="all" > <?= lang('select').' '.lang('branch') ?> </option>
                                                            <?php foreach ($billers as $biller): ?>
                                                                <option value="<?php echo $biller->id; ?>" <?= (isset($_POST['Sucursal']) && $_POST['Sucursal'] == $biller->id ? "selected" : "") ?> ><?php echo $biller->name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>    
                                                <?php } else { ?>
                                                        <select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;">
                                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                                ?>
                                                                    <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                                                <?php endif ?>
                                                            </select>
                                                <?php } ?>
                                                
                                                
                                            <?php endif ?>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label for="inputEmail3" class="control-label"> <?= lang('seller') ?> </label><br>
                                                    <?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="valueVendedor" id="seller2" value="Un" checked> <?= lang('a').' '.lang('seller') ?>
                                                        </label>
                                                    <?php else: ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="valueVendedor" id="seller1" value="Todas" <?= !isset($_POST['valueVendedor']) || (isset($_POST['valueVendedor']) && $_POST['valueVendedor'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="valueVendedor" id="seller2" value="Un" <?= isset($_POST['valueCliente']) || isset($_POST['valueVendedor']) && $_POST['valueVendedor'] == 'Un' ? 'checked' : '' ?>> <?= lang('a').' '.lang('seller') ?>
                                                        </label>
                                                    <?php endif ?>
                                            </div>
                                            <div class="collapse" id="collapseSeller">
                                                <select class="form-control " onchange="Search_Vendedor(this.value);" name="Vendedor" id="Vendedor" style="width:100%;">
                                                    <?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
                                                            <?php foreach ($sellers as $seller): ?>
                                                                <?php if ($seller['cod_seller'] == $this->session->userdata('company_id')): ?>
                                                                    <option value="<?php echo $seller['cod_seller']; ?>"><?php echo $seller['nombre']; ?></option>
                                                                <?php endif ?>
                                                            <?php endforeach; ?>
                                                        <?php else: ?>
                                                            <option value="all" > <?= lang('select').' '.lang('seller') ?> </option>
                                                            <?php foreach ($sellers as $seller): ?>
                                                                <option value="<?php echo $seller['cod_seller']; ?>"><?php echo $seller['nombre']; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label for="inputEmail3" class="control-label"> <?= lang('view_customer_total') ?> </label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="valueCliente" id="Cliente1" value="Todas" <?= !isset($_POST['valueCliente']) ||  (isset($_POST['valueCliente']) && $_POST['valueCliente'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="valueCliente" id="Cliente2" value="Un" <?= isset($_POST['valueCliente']) && $_POST['valueCliente'] == 'Un' ? 'checked' : '' ?>> <?= lang('a').' '.lang('view_customer_total') ?>
                                                </label>
                                            </div>
                                            <div class="collapse" id="collapseBustomer">
                                                <select class="form-control " onchange="Search_Cliente(this.value);" name="Cliente" id="Cliente" style="width:100%;">
                                                    <option value="all" > <?= lang('select').' '.lang('view_customer_total') ?> </option>
                                                    <?php foreach ($customers as $customer): ?>
                                                        <option value="<?php echo $customer['cod_id']; ?>" <?= isset($_POST['Cliente']) && $_POST['Cliente'] == $customer['cod_id'] ? 'selected="selected"' : '' ?>><?php echo $customer['nombre']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-3">
                                            <?= lang('order_by', 'order_by') ?>
                                            <select name="order_by" id="order_by" class="form-control">
                                                <option value="">Según agrupamiento</option>
                                                <option value="customer_asc" selected>Nombre cliente ascendente</option>
                                                <option value="customer_desc">Nombre cliente descendente</option>
                                                <option value="customer_city_asc">Ciudad cliente ascendente</option>
                                                <option value="customer_city_desc">Ciudad cliente descendente</option>
                                                <option value="seller_asc">Nombre vendedor ascendente</option>
                                                <option value="seller_desc">Nombre vendedor descendente</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-4">
                                          <label><?= lang('cut_off_date') ?></label>
                                          <input type="date" name="end_date" id="end_date" class="form-control" value="<?= isset($_POST['end_date']) && !empty($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d') ?>" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <label>
                                                <input type="checkbox" name="group_by_biller" id="group_by_biller" <?= isset($_POST['group_by_biller']) ? 'checked="checked"' : '' ?>>
                                                <?= lang('group_by_biller') ?>
                                            </label>
                                            <label>
                                                <input type="checkbox" name="group_by_city" id="group_by_city" <?= isset($_POST['group_by_city']) ? 'checked="checked"' : '' ?>>
                                                <?= lang('group_by_city') ?>
                                            </label>
                                            <label>
                                                <input type="checkbox" name="group_by_seller" id="group_by_seller" <?= isset($_POST['group_by_seller']) ? 'checked="checked"' : '' ?>>
                                                <?= lang('group_by_seller') ?>
                                            </label>
                                            <label>
                                                <input type="checkbox" name="group_by_customer" id="group_by_customer" <?= isset($_POST['group_by_customer']) ? 'checked="checked"' : '' ?>>
                                                <?= lang('group_by_customer') ?>
                                            </label>
                                            <label>
                                                <input type="checkbox" name="group_by_customer_address" id="group_by_customer_address" <?= isset($_POST['group_by_customer_address']) ? 'checked="checked"' : '' ?>>
                                                <?= lang('group_by_customer_address') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-3">
                                                <button type="button" class="btn btn-success" class="Buscar" id="Buscar" ><?php echo lang('send');?></button>
                                            </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                        </div>
                                        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
                                            <div class="col-xs-12 col-sm-6 col-md-3">
                                                <div class="pull-right dropdown">
                                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                                        <li>
                                                            <a class="ExportPdf" id="ExportPdf"> PDF </a>
                                                        </li>
                                                        <li>
                                                            <a class="ExportXls" id="ExportXls"> Excel </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                    <br>
                                <?php echo form_close(); ?>
                            <?php else: ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h3><?= lang('portfolio_report_temp_tables_title') ?></h3>
                                        <p><?= lang('portfolio_report_temp_tables_description') ?></p>
                                    </div>
                                    <div class="col-sm-12">
                                        <h4><?= lang('portfolio_report_temp_tables_1_step_title') ?></h4>
                                        <p><?= sprintf(lang('portfolio_report_temp_tables_1_step_description'), $this->Settings->system_start_date) ?></p>
                                    </div>
                                    <div class="form-group col-md-4">
                                      <label><?= lang('cut_off_date') ?></label>
                                      <input type="date" name="end_date" id="end_date" class="form-control" value="<?= isset($_POST['end_date']) && !empty($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d') ?>" >
                                    </div>
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary" id="generate_temp_tables"><?= lang('generate_temp_tables') ?></button>
                                    </div>
                                    <div class="col-sm-12 divpbar" style="display:none;">
                                        <hr>
                                        <label>Progreso</label>
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                            0%
                                          </div>
                                        </div>
                                        <br>
                                        <em class="text_pb">Generando....</em>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>

                        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
                            <div class="col-lg-12" id="ajax-content-container">
                                <table id="TableData" class="table table-stripped" >
                                    <thead>
                                        <tr>
                                            <td class="text-center"> <b><?= lang('ref') ?></b></td>
                                            <td class="text-center"> <b><?= lang('customer') ?></b></td>
                                            <td class="text-center"> <b><?= lang('date').' '.lang('sale') ?></b></td>
                                            <td class="text-center"> <b><?= lang('days_30') ?></b></td>
                                            <td class="text-center"> <b><?= lang('days_60') ?></b></td>
                                            <td class="text-center"> <b><?= lang('days_90') ?></b></td>
                                            <td class="text-center"> <b><?= lang('days_120') ?></b></td>
                                            <td class="text-center"> <b><?= lang('days_more') ?></b></td>
                                            <td class="text-center"> <b><?= lang('due_date') ?></b></td>
                                            <td class="text-center"> <b><?= lang('day_arrears') ?></b></td>
                                            <td class="text-center"> <b><?= lang('value') ?></b></td>
                                            <td class="text-center"> <b><?= lang('sbalance').' '.lang('paid') ?></b></td>
                                            <td class="text-center"> <b><?= lang('sbalance').' '.lang('pending') ?></b></td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                            $subtotal_biller = $subtotal_pago_biller = $subtotal_saldo_biller = 0;
                                            $subtotal_seller = $subtotal_pago_seller = $subtotal_saldo_seller = 0;
                                            $subtotal_state = $subtotal_pago_state = $subtotal_saldo_state = 0;
                                            $subtotal_city = $subtotal_pago_city = $subtotal_saldo_city = 0;
                                            $subtotal_customer = $subtotal_pago_customer = $subtotal_saldo_customer = 0;
                                            $subtotal_address = $subtotal_pago_address = $subtotal_saldo_address = 0;
                                            $subtotal_total = $subtotal_pago_total = $subtotal_saldo_total = 0;
                                            $biller = $seller = $customer = $state = $city = $address = null;
                                            $saldo_city_30 = $saldo_city_60 = $saldo_city_90 = $saldo_city_120 = $saldo_city_mas = 0;
                                            $saldo_state_30 = $saldo_state_60 = $saldo_state_90 = $saldo_state_120 = $saldo_state_mas = 0;
                                            $saldo_customer_30 = $saldo_customer_60 = $saldo_customer_90 = $saldo_customer_120 = $saldo_customer_mas = 0;
                                            $saldo_address_30 = $saldo_address_60 = $saldo_address_90 = $saldo_address_120 = $saldo_address_mas = 0;
                                            $saldo_seller_30 = $saldo_seller_60 = $saldo_seller_90 = $saldo_seller_120 = $saldo_seller_mas = 0;
                                        ?>
                                        <?php foreach ($sales as $sale): ?>
                                            <?php if ($group_data && $group_by_biller && (!$biller || ($biller && $biller != $sale['Sucursal']))) { ?>
                                                    <?php if ($subtotal_saldo_address > 0 && $group_by_customer_address): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer_branch') ?> <?= $address ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_address);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_address = $subtotal_pago_address = $subtotal_saldo_address = 0;
                                                            $saldo_address_30 = $saldo_address_60 = $saldo_address_90 = $saldo_address_120 = $saldo_address_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_customer > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer') ?> <?= $customer ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_customer);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_customer = $subtotal_pago_customer = $subtotal_saldo_customer = 0;
                                                            $saldo_customer_30 = $saldo_customer_60 = $saldo_customer_90 = $saldo_customer_120 = $saldo_customer_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_city > 0 && $group_by_city): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('city') ?> <?= $city ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_city);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_city);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_city);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_city = $subtotal_pago_city = $subtotal_saldo_city = 0;
                                                            $saldo_city_30 = $saldo_city_60 = $saldo_city_90 = $saldo_city_120 = $saldo_city_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_state > 0 && $group_by_city): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('state') ?> <?= $state ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_state);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_state);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_state);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_state = $subtotal_pago_state = $subtotal_saldo_state = 0;
                                                            $saldo_state_30 = $saldo_state_60 = $saldo_state_90 = $saldo_state_120 = $saldo_state_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_seller > 0 && $group_by_seller): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('seller') ?> <?= $seller ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_seller);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_seller);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_seller);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_seller = $subtotal_pago_seller = $subtotal_saldo_seller = 0;
                                                            $saldo_seller_30 = $saldo_seller_60 = $saldo_seller_90 = $saldo_seller_120 = $saldo_seller_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_biller > 0): ?>
                                                        <tr>
                                                        <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('biller') ?> <?= $biller ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_biller);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_biller);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_biller);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_biller = $subtotal_pago_biller = $subtotal_saldo_biller = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php $biller = $sale['Sucursal']; ?>
                                                    <tr class="active">
                                                        <td style="border-right: 1px solid transparent;">
                                                            <b> <?= lang('biller') ?> :</b> <?php echo $sale['Sucursal']; ?>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                <?php $seller = $customer = $state = $city = $address = null;
                                                }
                                            ?>
                                            <?php if ($group_by_seller && (!$seller || ($seller && $seller != $sale['nom_seller']))) { ?>
                                                    <?php if ($subtotal_saldo_address > 0 && $group_by_customer_address): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer_branch') ?> <?= $address ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_address);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_address = $subtotal_pago_address = $subtotal_saldo_address = 0;
                                                            $saldo_address_30 = $saldo_address_60 = $saldo_address_90 = $saldo_address_120 = $saldo_address_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_customer > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer') ?> <?= $customer ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_customer);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_customer = $subtotal_pago_customer = $subtotal_saldo_customer = 0;
                                                            $saldo_customer_30 = $saldo_customer_60 = $saldo_customer_90 = $saldo_customer_120 = $saldo_customer_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_city > 0 && $group_by_city): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('city') ?> <?= $city ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_city);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_city);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_city);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_city = $subtotal_pago_city = $subtotal_saldo_city = 0;
                                                            $saldo_city_30 = $saldo_city_60 = $saldo_city_90 = $saldo_city_120 = $saldo_city_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_state > 0 && $group_by_city): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('state') ?> <?= $state ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_state);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_state);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_state);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_state = $subtotal_pago_state = $subtotal_saldo_state = 0;
                                                            $saldo_state_30 = $saldo_state_60 = $saldo_state_90 = $saldo_state_120 = $saldo_state_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_seller > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('seller') ?> <?= $seller ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_seller);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_seller);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_seller);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_seller = $subtotal_pago_seller = $subtotal_saldo_seller = 0;
                                                            $saldo_seller_30 = $saldo_seller_60 = $saldo_seller_90 = $saldo_seller_120 = $saldo_seller_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php $seller = $sale['nom_seller']; ?>
                                                    <tr class="active">
                                                        <td style="border-right: 1px solid transparent;">
                                                            <b> <?= lang('seller') ?> :</b> <?php echo $sale['nom_seller']; ?>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                <?php $customer = $state = $city = $address = null;
                                                    }
                                            ?>
                                            <?php if ($group_by_city && (!$state || ($state && $state != $sale['customer_state']))) { ?>
                                                    <?php if ($subtotal_saldo_address > 0 && $group_by_customer_address): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer_branch') ?> <?= $address ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_address);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_address = $subtotal_pago_address = $subtotal_saldo_address = 0;
                                                            $saldo_address_30 = $saldo_address_60 = $saldo_address_90 = $saldo_address_120 = $saldo_address_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_customer > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer') ?> <?= $customer ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_customer);?> </th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_customer = $subtotal_pago_customer = $subtotal_saldo_customer = 0;
                                                            $saldo_customer_30 = $saldo_customer_60 = $saldo_customer_90 = $saldo_customer_120 = $saldo_customer_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_city > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('city') ?> <?= $city ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_city);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_city);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_city);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_city = $subtotal_pago_city = $subtotal_saldo_city = 0;
                                                            $saldo_city_30 = $saldo_city_60 = $saldo_city_90 = $saldo_city_120 = $saldo_city_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_state > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('state') ?> <?= $state ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_state);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_state);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_state);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_state = $subtotal_pago_state = $subtotal_saldo_state = 0;
                                                            $saldo_state_30 = $saldo_state_60 = $saldo_state_90 = $saldo_state_120 = $saldo_state_mas = 0;
                                                        ?>
                                                    <?php endif ?>

                                                    <?php $state = $sale['customer_state']; ?>
                                                    <tr class="active">
                                                        <td style="border-right: 1px solid transparent;">
                                                            <b> <?= lang('state') ?> :</b> <?php echo $sale['customer_state']; ?>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                <?php }
                                            ?>
                                            <?php if ($group_by_city && (!$city || ($city && $city != $sale['customer_city']))) { ?>
                                                    <?php if ($subtotal_saldo_address > 0 && $group_by_customer_address): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer_branch') ?> <?= $address ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_address);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_address = $subtotal_pago_address = $subtotal_saldo_address = 0;
                                                            $saldo_address_30 = $saldo_address_60 = $saldo_address_90 = $saldo_address_120 = $saldo_address_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_customer > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer') ?> <?= $customer ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_customer);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_customer = $subtotal_pago_customer = $subtotal_saldo_customer = 0;
                                                            $saldo_customer_30 = $saldo_customer_60 = $saldo_customer_90 = $saldo_customer_120 = $saldo_customer_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_city > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('city') ?> <?= $city ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_city);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_city);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_city);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_city = $subtotal_pago_city = $subtotal_saldo_city = 0;
                                                            $saldo_city_30 = $saldo_city_60 = $saldo_city_90 = $saldo_city_120 = $saldo_city_mas = 0;
                                                        ?>
                                                    <?php endif ?>

                                                    <?php $city = $sale['customer_city']; ?>
                                                    <tr class="active">
                                                        <td style="border-right: 1px solid transparent;">
                                                            <b> <?= lang('city') ?> :</b> <?php echo $sale['customer_city']; ?>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                <?php }
                                            ?>
                                            <?php if ($group_data && (!$customer || ($customer && $customer != $sale['nom_companie']))) { ?>
                                                    <?php if ($subtotal_saldo_address > 0 && $group_by_customer_address): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer_branch') ?> <?= $address ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_address);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_address = $subtotal_pago_address = $subtotal_saldo_address = 0;
                                                            $saldo_address_30 = $saldo_address_60 = $saldo_address_90 = $saldo_address_120 = $saldo_address_mas = 0;
                                                        ?>
                                                    <?php endif ?>
                                                    <?php if ($subtotal_saldo_customer > 0): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer') ?> <?= $customer ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_customer);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_customer);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_customer = $subtotal_pago_customer = $subtotal_saldo_customer = 0;
                                                            $saldo_customer_30 = $saldo_customer_60 = $saldo_customer_90 = $saldo_customer_120 = $saldo_customer_mas = 0;
                                                        ?>
                                                    <?php endif ?>

                                                    <?php $customer = $sale['nom_companie']; ?>
                                                    <tr class="active">
                                                        <td style="border-right: 1px solid transparent;">
                                                            <b> <?= lang('customer') ?> :</b> <?php echo $sale['nom_companie']; ?>
                                                        </td>
                                                        <td></td>
                                                        <td style="border-right: 1px solid transparent;">
                                                            <b>NIT:</b> <?php echo $sale['documento']; ?>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                <?php }
                                            ?>
                                            <?php if ($group_data && (!$address || ($address && $address != $sale['customer_sucursal']))) { ?>
                                                    <?php if ($subtotal_saldo_address > 0 && $group_by_customer_address): ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_30); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_60); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_90); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_120); ?></th>
                                                            <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_mas); ?></th>
                                                            <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer_branch') ?> <?= $address ?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_address);?></th>
                                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_address);?></th>
                                                        </tr>
                                                        <?php
                                                            $subtotal_address = $subtotal_pago_address = $subtotal_saldo_address = 0;
                                                            $saldo_address_30 = $saldo_address_60 = $saldo_address_90 = $saldo_address_120 = $saldo_address_mas = 0;
                                                        ?>
                                                    <?php endif ?>

                                                    <?php $address = $sale['customer_sucursal']; ?>
                                                    <tr class="active">
                                                        <td style="border-right: 1px solid transparent;">
                                                            <b> <?= lang('customer_branch') ?> :</b> <?php echo $sale['customer_sucursal']; ?>
                                                        </td>
                                                        <td></td>
                                                        <td style="border-right: 1px solid transparent;">
                                                            <b>NIT:</b> <?php echo $sale['documento']; ?>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                <?php }
                                            ?>
                                            <?php
                                                
                                                $fecha = substr(!empty($sale['due_date']) ? $sale['due_date'] : $sale['Fecha_Creacion'],0,10);
                                                $fecha_now = isset($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d');
                                                $dias   = (strtotime($fecha_now)-strtotime($fecha))/86400;
                                                $dias   = $dias < 0 ? 0 : $dias; 
                                                $dias   = floor($dias);
                                                $Saldo = number_format($sale['Saldo'],0,'','');
                                                $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 = $saldo_mas = 0;
                                                if ($dias <=30)
                                                {
                                                    $saldo_30 = $sale['Saldo'];
                                                    $saldo_customer_30 += $sale['Saldo'];
                                                    $saldo_seller_30 += $sale['Saldo'];
                                                    $saldo_city_30 += $sale['Saldo'];
                                                    $saldo_state_30 += $sale['Saldo'];
                                                    $saldo_address_30 += $sale['Saldo'];
                                                }
                                                else if ($dias <=60)
                                                {
                                                    $saldo_60 = $sale['Saldo'];
                                                    $saldo_customer_60 += $sale['Saldo'];
                                                    $saldo_seller_60 += $sale['Saldo'];
                                                    $saldo_city_60 += $sale['Saldo'];
                                                    $saldo_state_60 += $sale['Saldo'];
                                                    $saldo_address_60 += $sale['Saldo'];
                                                }
                                                else if ($dias <=90)
                                                {
                                                    $saldo_90 = $sale['Saldo'];
                                                    $saldo_customer_90 += $sale['Saldo'];
                                                    $saldo_seller_90 += $sale['Saldo'];
                                                    $saldo_city_90 += $sale['Saldo'];
                                                    $saldo_state_90 += $sale['Saldo'];
                                                    $saldo_address_90 += $sale['Saldo'];
                                                }
                                                elseif ($dias <=120)
                                                {
                                                    $saldo_120 = $sale['Saldo'];
                                                    $saldo_customer_120 += $sale['Saldo'];
                                                    $saldo_seller_120 += $sale['Saldo'];
                                                    $saldo_city_120 += $sale['Saldo'];
                                                    $saldo_state_120 += $sale['Saldo'];
                                                    $saldo_address_120 += $sale['Saldo'];
                                                }
                                                else
                                                {
                                                    $saldo_mas = $sale['Saldo'];
                                                    $saldo_customer_mas += $sale['Saldo'];
                                                    $saldo_seller_mas += $sale['Saldo'];
                                                    $saldo_city_mas += $sale['Saldo'];
                                                    $saldo_state_mas += $sale['Saldo'];
                                                    $saldo_address_mas += $sale['Saldo'];
                                                }
                                            ?>
                                            <tr>
                                                <td scope="row"><?php echo $sale['Referencia']; ?></td>
                                                <td><?php echo $sale['nom_client']; ?></td>
                                                <td class="text-center"><?php echo substr($sale['Fecha_Creacion'],0,10); ?></td>
                                                <td class="text-center"><?php echo $this->sma->formatMoney($saldo_30); ?></td>
                                                <td class="text-center"><?php echo $this->sma->formatMoney($saldo_60); ?></td>
                                                <td class="text-center"><?php echo $this->sma->formatMoney($saldo_90); ?></td>
                                                <td class="text-center"><?php echo $this->sma->formatMoney($saldo_120); ?></td>
                                                <td class="text-center"><?php echo $this->sma->formatMoney($saldo_mas); ?></td>
                                                <td class="text-center"><?php echo $sale['due_date']; ?></td>
                                                <td class="text-center"><?php echo $dias; ?></td>
                                                <td class="text-right"><?php echo $this->sma->formatMoney($sale['total']);?></td>
                                                <td class="text-right"><?php echo $this->sma->formatMoney($sale['val_pago']);?></td>
                                                <td class="text-right"><?php echo $this->sma->formatMoney($sale['Saldo']);?></td>
                                            </tr>
                                            <?php
                                                $subtotal_customer += $sale['total'];
                                                $subtotal_seller += $sale['total'];
                                                $subtotal_state += $sale['total'];
                                                $subtotal_city += $sale['total'];
                                                $subtotal_address += $sale['total'];
                                                $subtotal_total += $sale['total'];
                                                $subtotal_pago_customer += $sale['val_pago'];
                                                $subtotal_pago_seller += $sale['val_pago'];
                                                $subtotal_pago_state += $sale['val_pago'];
                                                $subtotal_pago_city += $sale['val_pago'];
                                                $subtotal_pago_address += $sale['val_pago'];
                                                $subtotal_pago_total += $sale['val_pago'];
                                                $subtotal_saldo_customer += $sale['Saldo'];
                                                $subtotal_saldo_seller += $sale['Saldo'];
                                                $subtotal_saldo_state += $sale['Saldo'];
                                                $subtotal_saldo_city += $sale['Saldo'];
                                                $subtotal_saldo_address += $sale['Saldo'];
                                                $subtotal_saldo_total += $sale['Saldo'];
                                             ?>
                                        <?php endforeach ?>

                                        <?php if ($group_data && $subtotal_saldo_address > 0): ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_30); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_60); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_90); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_120); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_address_mas); ?></th>
                                                <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer_branch') ?> <?= $address ?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_address);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_address);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_address);?></th>
                                            </tr>
                                            <?php
                                                $subtotal_address = $subtotal_pago_address = $subtotal_saldo_address = 0;
                                                $saldo_address_30 = $saldo_address_60 = $saldo_address_90 = $saldo_address_120 = $saldo_address_mas = 0;
                                            ?>
                                        <?php endif ?>
                                        <?php if ($group_data && $subtotal_saldo_customer > 0): ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_30); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_60); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_90); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_120); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_customer_mas); ?></th>
                                                <th class="text-right" > <?= lang('subtotal') ?> <?= lang('customer') ?> <?= $customer ?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_customer);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_customer);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_customer);?></th>
                                            </tr>
                                            <?php
                                                $subtotal_customer = $subtotal_pago_customer = $subtotal_saldo_customer = 0;
                                                $saldo_customer_30 = $saldo_customer_60 = $saldo_customer_90 = $saldo_customer_120 = $saldo_customer_mas = 0;
                                            ?>
                                        <?php endif ?>
                                        <?php if ($group_data && $subtotal_saldo_city > 0 && $group_by_city): ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_30); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_60); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_90); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_120); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_city_mas); ?></th>
                                                <th class="text-right" > <?= lang('subtotal') ?> <?= lang('city') ?> <?= $city ?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_city);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_city);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_city);?></th>
                                            </tr>
                                            <?php
                                                $subtotal_city = $subtotal_pago_city = $subtotal_saldo_city = 0;
                                                $saldo_city_30 = $saldo_city_60 = $saldo_city_90 = $saldo_city_120 = $saldo_city_mas = 0;
                                            ?>
                                        <?php endif ?>
                                        <?php if ($group_data && $subtotal_saldo_state > 0 && $group_by_city): ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_30); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_60); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_90); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_120); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_state_mas); ?></th>
                                                <th class="text-right" > <?= lang('subtotal') ?> <?= lang('state') ?> <?= $state ?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_state);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_state);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_state);?></th>
                                            </tr>
                                            <?php
                                                $subtotal_state = $subtotal_pago_state = $subtotal_saldo_state = 0;
                                                $saldo_state_30 = $saldo_state_60 = $saldo_state_90 = $saldo_state_120 = $saldo_state_mas = 0;
                                            ?>
                                        <?php endif ?>
                                        <?php if ($group_data && $subtotal_saldo_seller > 0 && $group_by_seller): ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_30); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_60); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_90); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_120); ?></th>
                                                <th class="text-tight"><?= $this->sma->formatMoney($saldo_seller_mas); ?></th>
                                                <th class="text-right" > <?= lang('subtotal') ?> <?= lang('seller') ?> <?= $seller ?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_seller);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_seller);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_seller);?></th>
                                            </tr>
                                            <?php
                                                $subtotal_seller = $subtotal_pago_seller = $subtotal_saldo_seller = 0;
                                                $saldo_seller_30 = $saldo_seller_60 = $saldo_seller_90 = $saldo_seller_120 = $saldo_seller_mas = 0;
                                            ?>
                                        <?php endif ?>
                                        <?php if ($group_data && $subtotal_saldo_biller > 0): ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <th class="text-right" > <?= lang('subtotal') ?> <?= lang('biller') ?> <?= $biller ?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_biller);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_biller);?></th>
                                                <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_biller);?></th>
                                            </tr>
                                            <?php
                                                $subtotal_biller = $subtotal_pago_biller = $subtotal_saldo_biller = 0;
                                            ?>
                                        <?php endif ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th class="text-right" > <?= lang('total') ?> </th>
                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_total);?></th>
                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_pago_total);?></th>
                                            <th class="text-right"><?php echo $this->sma->formatMoney($subtotal_saldo_total);?></th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script >
    function ActualizarVendedores(valor, selected = 'all') {
        <?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
        <?php else: ?>
            $("#Vendedor option").remove();
            $('#Vendedor').html('<option value="all" selected="selected">-- Seleccione Vendedor --</option>');
            jQuery.ajax({
                url: site.base_url+'reports/search_Seller/'+valor + '/',
                type: 'GET',
                dataType: 'json',
                success: function(data, textStatus, xhr) {
                    var obj = Object.values(data.sellers);
                    obj.forEach(function(element) {
                        $('#Vendedor').append('<option value="'+element.cod_seller+'" >'+element.nombre+'</option>');
                    });

                    if (selected != 'all') {
                        $('#Vendedor').select2('val', selected);
                    }

                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#ajax-content-container').html('<div class="alert alert-danger" role="alert"><strong>Error:</strong> Lo sentimos, se presento un error por favor refrescar la pantalla volver a intentarlo.</div>');
                }
            });
        <?php endif ?>
    }

    function Search_Vendedor(valor, selected = 'all') {
        $("#Cliente option").remove();
        $('#Cliente').append('<option value="all" selected="selected">-- Seleccione Cliente --</option>');
        var sucursal = $("#Sucursal option:selected").val();
        jQuery.ajax({
            url: site.base_url+'reports/search_customer/'+sucursal+'/'+valor + '/',
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, xhr) {
                var obj = Object.values(data.sellers);
                obj.forEach(function(element) {
                    $('#Cliente').append('<option value="'+element.cod_id+'" >'+element.nombre+'</option>');
                });
                if (selected != 'all') {
                    $('#Cliente').select2('val', selected);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                $('#ajax-content-container').html('<div class="alert alert-danger" role="alert"><strong>Error:</strong> Lo sentimos, se presento un error por favor refrescar la pantalla volver a intentarlo.</div>');
            }
        });

    }

    function Search_Cliente(valor) {
        var sucursal = $("#Sucursal option:selected").val();
        var vendedor = $("#Vendedor option:selected").val();
    }



$('#collapseBiller').collapse('hide');
<?php if ((!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id'))): ?>
    $('#collapseSeller').collapse('show');
<?php else: ?>
    $('#collapseSeller').collapse('hide');
<?php endif ?>
$('#collapseBustomer').collapse('hide');

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $("#form_filter").validate({
            ignore: []
        });

        if ("<?= isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Una' ?>") {
            $('#collapseBiller').collapse('show');
        }

        if ("<?= isset($_POST['valueVendedor']) && $_POST['valueVendedor'] == 'Un' ?>") {
            $('#collapseSeller').collapse('show');
        }

        if ("<?= isset($_POST['valueCliente']) && $_POST['valueCliente'] == 'Un' ?>") {
            $('#collapseBustomer').collapse('show');
        }

        $('#TableData').DataTable({
            searching: true,
            ordering:  false,
            pageLength: 100,
            responsive: true,
            "aaSorting": [],
            "aoColumns": [
                {
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                },{
                    "bSortable": false
                }],
            dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro" f>tip',
            buttons : [{extend:'excel', title:'Menus', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
            oLanguage: <?php echo $dt_lang; ?>
        });
        /*AQUI <?= lang('actions') ?>*/
        $("#ExportPdf").click(function() {
            $('#action').val('pdf');
            form_action();
        });
        $("#ExportXls").click(function() {
            $('#action').val('xls');
            form_action();
        });

        function form_action(){
            $('#form_filter').prop('target', '_blank').submit();
            $('#form_filter').prop('target', false);
            $('#action').val('');
        }

        <?php if (isset($_POST['Sucursal']) && $_POST['Sucursal'] != 'all' ) { ?>
            $('#Sucursal').val("<?= $_POST['Sucursal'] ?>");
            ActualizarVendedores("<?= $_POST['Sucursal'] ?>", "<?= $_POST['Vendedor'] ?>");
        <?php } ?>

        <?php if (isset($_POST['Vendedor']) && $_POST['Vendedor'] != 'all' ) { ?>
            $('#Vendedor').val("<?= $_POST['Vendedor'] ?>");
            Search_Vendedor("<?= $_POST['Vendedor'] ?>", "<?= $_POST['Cliente'] ?>");
        <?php } ?>
        <?php if (isset($_POST['order_by'])): ?>
            setTimeout(function() {
                $('#order_by').select2('val', '<?= $_POST['order_by'] ?>');
            }, 1200);
        <?php endif ?>

    } );

    // Sucursal
$('#biller2').on('ifChecked', function(event){
    $('#Sucursal').select2('readonly', false);
    var Sucursal = $("#Sucursal option:selected").val();
    ActualizarVendedores(Sucursal);
    $('#collapseBiller').collapse('show');
});
$('#biller1').on('ifChecked', function(event){
    $("#Sucursal").val('all');
    $('#collapseBiller').collapse('hide');
    ActualizarVendedores('all');
    $('#Sucursal').select2('val', 'all').trigger('change');
    $('#Sucursal').select2('readonly', true);
});
// Vendedor
$('#seller2').on('ifChecked', function(event){
    $('#Vendedor').select2('readonly', false);
    $('#collapseSeller').collapse('show');
    var Vendedor = $("#Vendedor option:selected").val();
    Search_Vendedor(Vendedor);
    var Sucursal = $("#Sucursal option:selected").val();
    if(Sucursal=='all')
    {
        $("input[name=valueSucursal]").attr('readonly', true);
        $("#Sucursal").val('all');
    }
});
$('#seller1').on('ifChecked', function(event){
    $("#Vendedor").val('all');
    $('#collapseSeller').collapse('hide');
    Search_Vendedor('all');
    $('#Vendedor').prop('selectedIndex',0);
    $('#Vendedor').select2('val', 'all').trigger('change');
    $('#Vendedor').select2('readonly', true);
});
// cliente
$('#Cliente2').on('ifChecked', function(event){
    $('#Cliente').select2('readonly', false);
    var vendedor = $("#Vendedor option:selected").val();
    if(vendedor=='all')
    {
        $("input[name=valueSucursal]").attr('readonly', true);
        $("input[name=valueVendedor]").attr('readonly', true);
        $("#Vendedor").val('all');
        $("#Sucursal").val('all');
    }
    $('#collapseBustomer').collapse('show');
    var Cliente = $("#Cliente option:selected").val();
    Search_Cliente(Cliente);
});
$('#Cliente1').on('ifChecked', function(event){
    $('#Vendedor').select2('readonly', false);
    $('#Sucursal').select2('readonly', false);
    $("input[name=valueSucursal]").attr('disabled', false);
    $("input[name=valueVendedor]").attr('disabled', false);
    $("#Cliente").val('all');
    $('#collapseBustomer').collapse('hide');
    Search_Cliente('all');
    $('#Cliente').prop('selectedIndex',0);
    $('#Cliente').select2('val', 'all').trigger('change');
    $('#Cliente').select2('readonly', true);
});


$('#group_by_city').on('ifChecked', function(event){
    disable_order_by();
});
$('#group_by_seller').on('ifChecked', function(event){
    disable_order_by();
});
$('#group_by_customer').on('ifChecked', function(event){
    disable_order_by();
});
$('#group_by_customer_address').on('ifChecked', function(event){
    disable_order_by();
});

$('#group_by_city').on('ifUnchecked', function(event){
    enable_order_by();
});
$('#group_by_seller').on('ifUnchecked', function(event){
    enable_order_by();
});
$('#group_by_customer').on('ifUnchecked', function(event){
    enable_order_by();
});
$('#group_by_customer_address').on('ifUnchecked', function(event){
    enable_order_by();
});

function disable_order_by(){
    $('#order_by').select2('val', '').select2('readonly', true);
    $('#order_by option').each(function(index, option){
        if ($(option).val() != "") {
            $(option).prop('disabled', true);
        }
    });
}

function enable_order_by(){
    var group_by_city = $('#group_by_city').is(':checked');
    var group_by_seller = $('#group_by_seller').is(':checked');
    var group_by_customer = $('#group_by_customer').is(':checked');
    if (!group_by_city && !group_by_seller && !group_by_customer) {
        $('#order_by').select2('val', 'customer_asc').select2('readonly', false);
        $('#order_by option').each(function(index, option){
            if ($(option).val() != "") {
                $(option).prop('disabled', false);
            }
        });
    }
}

$('#Buscar').on('click', function(e){
    if ($('#form_filter').valid()) {
        $('#filter_action').val(1);
        $('#form_filter').submit();
    }
});

var filter_year_options = JSON.parse('<?= json_encode($this->filter_year_options) ?>');
var years = [];
var yn = 1;
$.each(filter_year_options, function(index, year){
    years[yn] = year;
    yn++;
});
var periodos = JSON.parse('<?= $periodos ?>');
var num_periodos = Object.keys(periodos).length;
var num_anos = Object.keys(filter_year_options).length;
var max_iterations = num_anos * num_anos;
var periodo_actual = 1;
var ano_actual = 1;
var total_pb = 100/(num_periodos * num_anos);
var executed_pb = 0;

$(document).on('click', '#generate_temp_tables', function(){
    $('.divpbar').fadeIn();
    $('#generate_temp_tables').html("Generando, por favor espere ... <i class='fa fa-spinner fa-spin'></i>");
    $('#generate_temp_tables').prop('disabled', true);
    execute_ajax();
});

function execute_ajax(){
    text_pb = "Generando de "+parseInt(years[ano_actual])+periodos[periodo_actual].start_month+" a "+parseInt(years[ano_actual])+periodos[periodo_actual].end_month;
    $('.text_pb').text(text_pb);
    $.ajax({
        url : site.base_url+"reports/portfolio_report_2_generate_temp",
        dataType : "JSON",
        type : "GET",
        data : {
                end_month : periodos[periodo_actual].end_month,
                start_month : periodos[periodo_actual].start_month,
                end_date : $('#end_date').val(),
                'year_to_execute': parseInt(years[ano_actual])
            }
    }).done(function(data){
        if (data.success == 0) {
            return;
        } else {
            $('.text_pb').text("Generado");
            if (ano_actual <= num_anos) {
                periodo_actual++;
                if (periodo_actual <= num_periodos) {
                    execute_ajax();
                } else if (periodo_actual > num_periodos) {
                    ano_actual++;
                    periodo_actual = 1;
                    execute_ajax();
                }
                executed_pb += total_pb;
                $('.progress-bar').css('width', executed_pb+"%").text(formatDecimal(executed_pb, 0)+"%");
            } else if (ano_actual > num_anos) {
                location.reload();
            }
        }
    });
}
<?php if ($this->session->userdata('portfolio_temp_end_date')): ?>
    $('#end_date').val('<?= $this->session->userdata('portfolio_temp_end_date') ?>').prop('readonly', true);
<?php endif ?>
</script>
