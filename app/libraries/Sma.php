<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
 *  ==============================================================================
 *  Author    : Mian Saleem
 *  Email     : saleem@tecdiary.com
 *  For       : Stock Manager Advance
 *  Web       : http://tecdiary.com
 *  ==============================================================================
 */
#[\AllowDynamicProperties]
class Sma
{

    public function __construct()
    {
        $this->tabindex = 0;
    }

    public function __get($var)
    {
        return get_instance()->$var;
    }

    private function _rglobRead($source, &$array = array())
    {
        if (!$source || trim($source) == "") {
            $source = ".";
        }
        foreach ((array) glob($source . "/*/") as $key => $value) {
            $this->_rglobRead(str_replace("//", "/", $value), $array);
        }
        $hidden_files = glob($source . ".*") and $htaccess = preg_grep('/\.htaccess$/', $hidden_files);
        $files = array_merge(glob($source . "*.*"), $htaccess);
        foreach ($files as $key => $value) {
            $array[] = str_replace("//", "/", $value);
        }
    }

    private function _zip($array, $part, $destination, $output_name = 'sma')
    {
        $zip = new ZipArchive;
        @mkdir($destination, 0777, true);

        if ($zip->open(str_replace("//", "/", "{$destination}/{$output_name}" . ($part ? '_p' . $part : '') . ".zip"), ZipArchive::CREATE)) {
            foreach ((array) $array as $key => $value) {
                $zip->addFile($value, str_replace(array("../", "./"), null, $value));
            }
            $zip->close();
        }
    }

    public function formatMoney($number, $symbol = false)
    {
        if ($symbol !== 'none' && $symbol !== false) { $symbol = $symbol ? $symbol : $this->Settings->symbol; } else { $symbol = ( $this->Settings->symbol) ?  $this->Settings->symbol : null; }
        if ($this->Settings->sac) {
            return ((($this->Settings->display_symbol == 1 || $symbol) && $this->Settings->display_symbol != 2) ? $symbol : '') .
            $this->formatSAC($this->formatDecimal($number)) .
            ($this->Settings->display_symbol == 2 ? $symbol : '');
        }
        $decimals = $this->Settings->decimals;
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return ((($this->Settings->display_symbol == 1 || $symbol && $number != 0) && $this->Settings->display_symbol != 2) ? $symbol : '') .
        number_format(($number ? $number : 0), $decimals, $ds, $ts) .
        ($this->Settings->display_symbol == 2 && $number != 0 ? $symbol : '');
    }

    public function formatValue($decimals = null, $number = null)
    {
        if ($decimals === null) {
            $decimals = $this->Settings->decimals;
        }
        // $decimals = 2;
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        $number = ($this->Settings->display_symbol == 1 ? $this->Settings->symbol.' ' : '').number_format($number, $decimals, $ds, $ts).($this->Settings->display_symbol == 2 ? ' '.$this->Settings->symbol : '');
        return $number;
    }

    public function formatQuantity($number, $decimals = null)
    {
        if ($decimals === null) {
            $decimals = $this->Settings->qty_decimals;
        }
        if ($this->Settings->sac) {
            return $this->formatSAC($this->formatDecimal($number, $decimals));
        }
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return number_format(($number ? $number : 0), $decimals, $ds, $ts);
    }

    public function formatQuantityDecimal($number, $decimals = null)
    {
        if (!$decimals) {
            $decimals = $this->Settings->qty_decimals;
        }
        return number_format($number, $decimals, '.', '');
    }

    public function formatNumber($number, $decimals = null)
    {
        if (!$decimals) {
            $decimals = $this->Settings->decimals;
            // $decimals = 2;
        }
        if ($this->Settings->sac) {
            return $this->formatSAC($this->formatDecimal($number, $decimals));
        }
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return number_format($number, $decimals, $ds, $ts);
    }

    public function formatNumberWithoutDecimals($number, $decimals = null)
    {
        if ($decimals === null) {
            $decimals = $this->Settings->decimals;
        }
        if ($this->Settings->sac) {
            return $this->formatSAC($this->formatDecimal($number, $decimals));
        }
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return number_format($number, $decimals, $ds, $ts);
    }

    public function formatDecimal($number, $decimals = null) //para cálculos
    {
        if (!is_numeric($number)) {
            return null;
        }
        if ($decimals === null) {
            $decimals = $this->Settings->decimals;
            // $decimals = 2;
            if ($this->Settings->rounding) {
                $decimals +=2;
            }
        }
        return number_format($number, $decimals, '.', '');
    }

    public function formatDecimals($number, $decimals = null) // para visualizaciones
    {
        if (!is_numeric($number)) {
            return 0;
        }
        if (!$decimals && $decimals !== 0) {
            $decimals = $this->Settings->decimals;
            // $decimals = 2;
        }
        if (empty($number)) {
            return 0;
        }
        return number_format($number, $decimals, '.', '');
    }

    public function formatDecimalNoRound($number, $decimals = null)
    {
        if (!is_numeric($number)) {
            return null;
        }
        if (!$decimals && $decimals !== 0) {
            $decimals = $this->Settings->decimals;
            // $decimals = 2;
            if ($this->Settings->rounding) {
                $decimals +=2;
            }
        }
        return number_format(bcdiv($number, 1, $decimals), $decimals, '.', '');
    }

    public function clear_tags($str)
    {
        return strip_tags($this->decode_html($str));
    }

    public function decode_html($str)
    {
        $str = str_replace("</p>", "\n", $str);
        $str = str_replace("<p>", "", $str);
        return html_entity_decode($str, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
    }

    public function roundMoney($num, $nearest = 0.05)
    {
        return round($num * (1 / $nearest)) * $nearest;
    }

    public function roundNumber($number, $toref = null)
    {
        switch ($toref) {
            case 1:
                $rn = round($number * 20) / 20;
                break;
            case 2:
                $rn = round($number * 2) / 2;
                break;
            case 3:
                $rn = round($number);
                break;
            case 4:
                $rn = ceil($number);
                break;
            default:
                $rn = $number;
        }
        return $rn;
    }

    public function unset_data($ud)
    {
        if ($this->session->userdata($ud)) {
            $this->session->unset_userdata($ud);
            return true;
        }
        return false;
    }

    public function hrsd($sdate)
    {
        if ($sdate) {
            return date($this->dateFormats['php_sdate'], strtotime($sdate));
        } else {
            return '0000-00-00';
        }
    }

    public function hrld($ldate)
    {
        if ($ldate) {
            return date($this->dateFormats['php_ldate'], strtotime($ldate));
        } else {
            return '0000-00-00 00:00:00';
        }
    }

    public function fsd($inv_date)
    {
        if ($inv_date) {
            $jsd = $this->dateFormats['js_sdate'];
            if ($jsd == 'dd-mm-yyyy' || $jsd == 'dd/mm/yyyy' || $jsd == 'dd.mm.yyyy') {
                $date = substr($inv_date, -4) . "-" . substr($inv_date, 3, 2) . "-" . substr($inv_date, 0, 2);
            } elseif ($jsd == 'mm-dd-yyyy' || $jsd == 'mm/dd/yyyy' || $jsd == 'mm.dd.yyyy') {
                $date = substr($inv_date, -4) . "-" . substr($inv_date, 0, 2) . "-" . substr($inv_date, 3, 2);
            } else {
                $date = $inv_date;
            }
            return $date;
        } else {
            return '0000-00-00';
        }
    }

    public function fld($ldate)
    {
        if ($ldate) {
            $date = explode(' ', $ldate);
            $jsd = $this->dateFormats['js_sdate'];
            $inv_date = $date[0];
            $time = isset($date[1]) ? $date[1] : "00:00";
            if ($jsd == 'dd-mm-yyyy' || $jsd == 'dd/mm/yyyy' || $jsd == 'dd.mm.yyyy') {
                $date = substr($inv_date, -4) . "-" . substr($inv_date, 3, 2) . "-" . substr($inv_date, 0, 2) . " " . $time;
            } elseif ($jsd == 'mm-dd-yyyy' || $jsd == 'mm/dd/yyyy' || $jsd == 'mm.dd.yyyy') {
                $date = substr($inv_date, -4) . "-" . substr($inv_date, 0, 2) . "-" . substr($inv_date, 3, 2) . " " . $time;
            } else {
                $date = $inv_date;
            }
            return $date;
        } else {
            return '0000-00-00 00:00:00';
        }
    }

    public function send_email($to, $subject, $message, $from = null, $from_name = null, $attachment = null, $cc = null, $bcc = null)
    {
        list($user, $domain) = explode('@', $to);
        if ($domain != 'tecdiary.com') {
            if ($this->Settings->protocol == 'smtp') {
                $this->load->library('tec_mail');
                return $this->tec_mail->send_mail($to, $subject, $message, $from, $from_name, $attachment, $cc, $bcc);
            }
            $this->load->library('email');

            $config = [
                'useragent'=>"Stock Manager Advance",
                'mailtype'=>'html',
                'crlf' => "\r\n",
                "newline" => "\r\n"
            ];

            if (ENVIRONMENT == 'development') {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'smtp.mailtrap.io';
                $config['smtp_port'] = 2525;
                $config['smtp_user'] = '7c9d12baf82755';
                $config['smtp_pass'] = '216096a8963c90';
            } else {
                $config['protocol'] = $this->Settings->protocol;
            }

            if ($this->Settings->protocol == 'sendmail') {
                $config['mailpath'] = $this->Settings->mailpath;
            }

            $this->email->initialize($config);

            if ($from && $from_name) {
                $this->email->from($from, $from_name);
            } elseif ($from) {
                $this->email->from($from, $this->Settings->site_name);
            } else {
                $this->email->from($this->Settings->default_email, $this->Settings->site_name);
            }

            $this->email->to($to);
            if ($cc) {
                $this->email->cc($cc);
            }
            if ($bcc) {
                $this->email->bcc($bcc);
            }
            $this->email->subject($subject);
            $this->email->message($message);
            if ($attachment) {
                if (is_array($attachment)) {
                    foreach ($attachment as $file) {
                        $this->email->attach($file);
                    }
                } else {
                    $this->email->attach($attachment);
                }
            }

            if ($this->email->send()) {
                return true;
            } else {
                throw new Exception($this->email->print_debugger(array('headers', 'subject')));
                return false;
            }
        }
        return true;
    }

    public function checkPermissions($action = null, $js = null, $module = null)
    {
        if (!$this->actionPermissions($action, $module)) {
            $this->session->set_flashdata('error', lang("access_denied"));
            if ($js) {
                die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 10);</script>");
            } else {
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
            }
        }
    }

    public function actionPermissions($action = null, $module = null)
    {
        if ($this->session->userdata('account_locked') == 2) {
            $disabled_v = [
                'add', 'create', 'edit', 'delete', 'settings', 'return_sale', 'return_purchase', 'csv', 'sequentialCount', 'count_stock', 'update_monthly_cost', 'close_register'
            ];

            foreach ($disabled_v as $row => $action) {
                $finded = strpos($this->v, $action);
                if ($finded !== false) {
                    return false;
                }
            }

            $disabled_m= ['system_settings'];

            foreach ($disabled_m as $row => $action) {
                if (strpos($this->m, $action) !== false) {
                    return false;
                }
            }

            if (($this->m == 'pos' && $this->v == 'index') || ($this->m == 'shop_settings' && $this->v == 'index')) {
                return false;
            }
        }

        if ($this->Owner || $this->Admin) {
            if ($this->Admin && stripos($action ?? '', 'delete') !== false) {
                if ($this->m == "payroll_electronic") {
                    return true;
                }

                return false;
            }

            return true;
        } elseif ($this->Customer || $this->Supplier) {
            return false;
        } else {
            if (!$module) {
                $module = $this->m;
            }

            if (!$action) {
                $action = $this->v;
            }

            if ($this->GP[$module . '-' . $action] == 1) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function barcode($text = null, $bcs = 'code128', $height = 74, $stext = 1, $get_be = false, $re = false)
    {
        $drawText = ($stext != 1) ? false : true;
        $this->load->library('tec_barcode', '', 'bc');
        return $this->bc->generate($text, $bcs, $height, $drawText, $get_be, $re);
    }

    public function qrcode($type = 'text', $text = 'http://tecdiary.com', $size = 2, $level = 'H', $sq = null)
    {
        $file_name = 'assets/uploads/qrcode' . $this->session->userdata('user_id') . ($sq ? $sq : '') . ($this->Settings->barcode_img ? '.png' : '.svg');
        if ($type == 'link') {
            $text = urldecode($text);
        }
        $this->load->library('tec_qrcode', '', 'qr');
        $config = array('data' => $text, 'size' => $size, 'level' => $level, 'savename' => $file_name);
        $this->qr->generate($config);
        $imagedata = file_get_contents($file_name);
        return "<img src='data:image/png;base64,".base64_encode($imagedata)."' alt='{$text}' class='qrimg' />";
    }

    public function generate_pdf($content, $name = 'download.pdf', $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, $orientation = 'P')
    {
        if ($this->Settings->pdf_lib == 'dompdf' && 1 == 2) {
            $this->load->library('tec_dompdf', '', 'pdf');
        } else {
            $this->load->library('tec_mpdf', '', 'pdf');
        }

        return $this->pdf->generate($content, $name, $output_type, $footer, $margin_bottom, $header, $margin_top, $orientation);
    }

    public function generate_pos_inv_pdf($content, $name = 'download.pdf', $output_type = 'S')
    {
        $this->load->library('tec_mpdf', '', 'pdf');
        return $this->pdf->generate_pos_inv($content, $name, $output_type);
    }

    public function print_arrays()
    {
        $args = func_get_args();
        echo "<pre>";
        foreach ($args as $arg) {
            print_r($arg);
        }
        echo "</pre>";
        die();
    }

    public function logged_in()
    {
        return (bool) $this->session->userdata('identity');
    }

    public function in_group($check_group, $id = false)
    {
        if ( ! $this->logged_in()) {
            return false;
        }
        $id || $id = $this->session->userdata('user_id');
        $group = $this->site->getUserGroup($id);
        if ($group->name === $check_group) {
            return true;
        }
        return false;
    }

    public function log_payment($type, $msg, $val = null)
    {
        $this->load->library('logs');
        return (bool) $this->logs->write($type, $msg, $val);
    }

    public function update_award_points($data, $scope = null)
    {

        $total = $data['grand_total'];
        $customer = $data['customer_id'];
        $user = isset($data['created_by']) ? $data['created_by'] : $this->session->userdata('user_id');
        $company = $this->site->getCompanyByID($customer);
        if ($company->award_points_no_management == 0) {
            if (!empty($this->Settings->each_spent) && abs($total) >= $this->Settings->each_spent) {
                if (((double)$total) < 0) {
                    $points = ceil(($total / $this->Settings->each_spent) * $this->Settings->ca_point);
                }else{
                    $points = floor(($total / $this->Settings->each_spent) * $this->Settings->ca_point);
                }
                $total_points = $scope ? $company->award_points - $points : $company->award_points + $points;
                $this->db->update('companies', array('award_points' => $total_points), array('id' => $customer));
                if ($data) {
                    $this->db->insert('award_points',
                        [
                            'date' => date('Y-m-d H:i:s'),
                            'type' => ($total < 0 ? 2 : 1),
                            'created_by' => $user,
                            'customer_id' => $customer,
                            'biller_id' => $data['biller_id'],
                            'sale_id' => isset($data['sale_id']) ? $data['sale_id'] : NULL,
                            'gift_card_id' => NULL,
                            'movement_value' => $total,
                            'movement_points' => $points
                        ]
                    );
                }

            }
            if ($user && !empty($this->Settings->each_sale) && !$this->Customer && abs($total) >= $this->Settings->each_sale) {
                $staff = $this->site->getUser($user);
                $points = floor(($total / $this->Settings->each_sale) * $this->Settings->sa_point);
                $total_points = $scope ? $staff->award_points - $points : $staff->award_points + $points;
                $this->db->update('users', array('award_points' => $total_points), array('id' => $user));
            }
        }
        return true;
    }

    public function zip($source = null, $destination = "./", $output_name = 'sma', $limit = 5000)
    {
        if (!$destination || trim($destination) == "") {
            $destination = "./";
        }

        $this->_rglobRead($source, $input);
        $maxinput = count($input);
        $splitinto = (($maxinput / $limit) > round($maxinput / $limit, 0)) ? round($maxinput / $limit, 0) + 1 : round($maxinput / $limit, 0);

        for ($i = 0; $i < $splitinto; $i++) {
            $this->_zip(array_slice($input, ($i * $limit), $limit, true), $i, $destination, $output_name);
        }

        unset($input);
        return;
    }

    public function unzip($source, $destination = './')
    {

        // @chmod($destination, 0777);
        $zip = new ZipArchive;
        if ($zip->open(str_replace("//", "/", $source)) === true) {
            $zip->extractTo($destination);
            $zip->close();
        }
        // @chmod($destination,0755);

        return true;
    }

    public function view_rights($check_id, $js = null)
    {
        if (!$this->Owner && !$this->Admin) {
            if ($check_id != $this->session->userdata('user_id')) {
                $this->session->set_flashdata('warning', lang('access_denied'));
                if ($js) {
                    die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome') . "'; }, 10);</script>");
                } else {
                    redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
                }
            }
        }
        return true;
    }

    public function makecomma($input)
    {
        if (strlen($input) <= 2) {return $input;}
        $length = substr($input, 0, strlen($input) - 2);
        $formatted_input = $this->makecomma($length) . "," . substr($input, -2);
        return $formatted_input;
    }

    public function formatSAC($num)
    {
        $pos = strpos((string) $num, ".");
        if ($pos === false) {$decimalpart = "00";} else {
            $decimalpart = substr($num, $pos + 1, 2);
            $num = substr($num, 0, $pos);}

        if (strlen($num) > 3 & strlen($num) <= 12) {
            $last3digits = substr($num, -3);
            $numexceptlastdigits = substr($num, 0, -3);
            $formatted = $this->makecomma($numexceptlastdigits);
            $stringtoreturn = $formatted . "," . $last3digits . "." . $decimalpart;
        } elseif (strlen($num) <= 3) {
            $stringtoreturn = $num . "." . $decimalpart;
        } elseif (strlen($num) > 12) {
            $stringtoreturn = number_format($num, 2);
        }

        if (substr($stringtoreturn, 0, 2) == "-,") {$stringtoreturn = "-" . substr($stringtoreturn, 2);}

        return $stringtoreturn;
    }

    public function md($page = FALSE)
    {
        die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . ($page ? site_url($page) : (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome')) . "'; }, 10);</script>");
    }

    public function analyze_term($term)
    {
        $spos = strpos($term, $this->Settings->barcode_separator);
        if ($spos !== false) {
            $st = explode($this->Settings->barcode_separator, $term);
            $sr = trim($st[0]);
            $option_id = trim($st[1]);
        } else {
            $sr = $term;
            $option_id = false;
        }
        return array('term' => $sr, 'option_id' => $option_id);
    }

    public function paid_opts($paid_by = null, $purchase = false, $empty_opt = false, $adding_deposit = false, $ignore_credit = false, $only_credit = false, $ignore_gift_card = false, $ignore_cash = false)
    {
        $opts = '';
        if ($empty_opt) {
            $opts .= '<option value="">'.lang('select').'</option>';
        }

        $popts = $this->site->getPaidOpts();
        if ($popts) {
            foreach ($popts as $row) {
                $selected = '';
                if ($paid_by && $paid_by == $row->code) {
                    $selected = 'selected="selected"';
                }
                if ($adding_deposit && $row->code == 'deposit') {
                    continue;
                }
                if ($ignore_credit && in_array($row->code, ['Credito', 'CRESIS'])) {
                    continue;
                }
                if ($only_credit && $row->due_payment == 0) {
                    continue;
                }
                if ($ignore_gift_card && $row->code == 'gift_card') {
                    continue;
                }
                if ($ignore_cash && $row->code == 'cash') {
                    continue;
                }
                if (($purchase && $row->state_purchase) || (!$purchase && $row->state_sale)) {
                    $txt_retcom = '';
                    if ($this->Settings->payments_methods_retcom == 1) {
                        $supplier = $this->site->getCompanyByID($row->supplier_id);
                        $txt_retcom = 'data-commisionvalue="'.$row->commision_value.'" data-retefuentevalue="'.$row->retefuente_value.'" data-reteivavalue="'.$row->reteiva_value.'" data-reteicavalue="'.$row->reteica_value.'" data-suppliername="'.($supplier ? $supplier->company : '').'"';

                    }
                    $opts.='<option value="'.$row->code.'" data-code_fe="'.$row->code_fe.'" data-duepayment="'.$row->due_payment.'" '.$txt_retcom.' '.$selected.' data-img-src="'. base_url('assets/payment_methods_icons/'.$row->icon).'" >'.$row->name.'</option>';
                }
            }
        }
        return $opts;
    }

    public function send_json($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data));
        exit;
    }

    public function convertMoney($amount, $format = TRUE, $symbol = TRUE)
    {
        if ($this->Settings->selected_currency != $this->Settings->default_currency) {
            $amount  = $this->formatDecimal(($amount * $this->selected_currency->rate), 4);
        }
        return ($format ? $this->formatMoney($amount, $this->Settings->symbol) : $amount);
    }

    public function slug($title, $type = NULL, $r = 1)
    {
        $this->load->helper('text');
        $slug = url_title(convert_accented_characters($title), '-', TRUE);
        $check_slug = $this->site->checkSlug($slug, $type);
        if (!empty($check_slug)) {
            $slug = $slug.$r; $r++;
            $this->slug($slug, $type, $r);
        }
        return $slug;
    }

    public function base64url_encode($data, $pad = null) {
        $data = str_replace(array('+', '/'), array('-', '_'), base64_encode($data));
        if (!$pad) {
            $data = rtrim($data, '=');
        }
        return $data;
    }

    public function base64url_decode($data) {
        return base64_decode(str_replace(array('-', '_'), array('+', '/'), $data));
    }

    public function getCardBalance($number) {
        if ($card = $this->site->getGiftCardByNO($number)) {
            return $card->balance;
        }
        return 0;
    }

    public function isPromo($product) {
        if (is_array($product)) {
            $product = json_decode(json_encode($product), false);
        }
        $today = date('Y-m-d H:i:s');
        return $product->promotion && $product->start_date <= $today && $product->end_date >= $today && $product->promo_price;
    }

    public function setCustomerGroupPrice($price, $customer_group) {
        if (!isset($customer_group) || empty($customer_group)) {
            return $price;
        }
        return $this->formatDecimal($price + (($price * $customer_group->percent) / 100));
    }

    public function reduce_text_length($text, $max_length){
        if (strlen($text) > $max_length) {
            $text = substr($text, 0, $max_length-5);
            $text.="...";
        }
        return $text;
    }

    public function remove_tax_from_amount($tax_id, $amount){
        $tax_rate = $this->site->getTaxRateByID($tax_id);
        if ($tax_rate && $tax_rate->rate > 0) {
            $rate = ($tax_rate->rate / 100) + 1;
            $new_amount = $amount / $rate;
            return $this->formatDecimal($new_amount);
        }
        return $amount;
    }

    public function calculateTax($tax_id, $amt, $met) {
        $tax = $this->site->getTaxRateByID($tax_id);
        $amt = ($amt);
        if ($met == 0) {
            $tax_val = $amt * ($tax->rate / 100);
            return $tax_val;
        }
        return 0;
    }

    public function calculate_tax($tax_id, $amt, $met){
        $tax = $this->site->getTaxRateByID($tax_id);
        $tax_val = 0;
        if ($met == 0) {
            $tax_val = $amt - ($amt / (1 + ($tax->rate/100)));
        } else if ($met == 1) {
            $tax_val = $amt * ($tax->rate / 100);
        }
        return $tax_val;

    }

    public function get_percentage_from_amount($partial_amount, $total_amount){

        if (strpos($partial_amount, "%") !== FALSE) {
            $percentage = str_replace("%", "", $partial_amount);
        } else {
            $percentage = ((Double) $partial_amount * 100) / $total_amount;
        }

        return $this->formatDecimalNoRound($percentage)." %";

    }

    public function get_last_day_of_month($date){
        $fecha = new DateTime($date);
        $fecha->modify('last day of this month');
        $d = $fecha->format('d');
        $m = $fecha->format('m');
        $Y = $fecha->format('Y');
        return $Y."-".$m."-".$d;
    }

    public function formatNumberMask($number, $decimals = null){

        $number = str_replace(",", "", $number);

        if (!$decimals && $decimals !== 0) {
            $decimals = $this->Settings->decimals;
            // $decimals = 2;
            if ($this->Settings->rounding) {
                $decimals +=2;
            }
        }
        return number_format($number, $decimals, '.', '');
    }

    public function number_rounding($monto, $rounding){
        /*
            1. Próximo entero
            2. Próximos 50
            3. Próximos 100
            4. Próximos 1000
            5. Próximos 10000
        */

        if ($rounding == 1) {
            return ceil($monto);
        } else if ($rounding == 2) {
            $numaproximar = 50;
        } else if ($rounding == 3) {
            $numaproximar = 100;
        } else if ($rounding == 4) {
            $numaproximar = 1000;
        } else if ($rounding == 5) {
            $numaproximar = 10000;
        } else {
            return $monto;
        }

        if ($monto > 0) {
            $num = floor($monto);
            $res1 = $num / $numaproximar ;
            if($monto % $numaproximar != 0){
                $intres1 = floor($res1) * $numaproximar;
                $res2 = $num - $intres1;
                $falta = $numaproximar - $res2;
                $numsies = $num + $falta;
            } else {
                $numsies = $num;
            }
        } else {
            $numsies = $monto;
        }
        return $numsies;
    }

    public function validate_only_number($text){

        for ($i='a'; $i <= 'z' ; $i++) {
            if (strpos($text, $i) !== false || strpos($text, mb_strtoupper($i)) !== false) {
                return true;
            }
        }

        if (strpos($text, '.') !== false) { return true; }
        if (strpos($text, '/') !== false) { return true; }
        if (strpos($text, ',') !== false) { return true; }
        if (strpos($text, '-') !== false) { return true; }
        if (strpos($text, '_') !== false) { return true; }
        if (strpos($text, ' ') !== false) { return true; }

        return false;

    }



    public function update_user_login_hour(){
        $user_id = $this->session->userdata('user_id');
        $user = $this->db->get_where('users', ['id' => $user_id]);
        if ($user->num_rows() > 0) {
            $user = $user->row();
            // exit($this->session->userdata('login_code')." - ".$user->login_code);
            if ($user->login_status == 0 || ($this->session->userdata('login_code') && $this->session->userdata('login_code') != $user->login_code)) {
                return false;
            } else {
                $this->db->update('users', [
                                            'login_hour' => date('Y-m-d H:i:s'),
                                            ],
                                          [
                                            'id' => $user_id
                                          ]);
                return true;
            }
        }
    }

    public function update_users_session_expirated(){
        // $q = $this->db->get_where('users', ['login_status' => 1, 'remember_code' => null]);
        $this->db->query('DELETE FROM sma_sessions WHERE timestamp < UNIX_TIMESTAMP(NOW()) - 86400;');
        $q = $this->db->get_where('users', ['login_status' => 1]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $user) {
                $fecha1 = new DateTime($user->login_hour);//fecha inicial
                $fecha2 = new DateTime(date('Y-m-d H:i:s'));//fecha de cierre
                $intervalo = $fecha1->diff($fecha2);
                $minutes = $intervalo->format('%i') + ($intervalo->format('%h') * 60) + ($intervalo->format('%d') * 1440);
                $session_expiration_time = (MAX_TIME_SESSION / 3600) * 60;
                if ($minutes > $session_expiration_time && $user->login_hour != null) {
                    $this->db->update('users', [
                                                'login_hour' => null,
                                                'login_status' => 0,
                                                'login_code' => 0,
                                                'remember_code' => null,
                                                ],
                                              [
                                                'id' => $user->id
                                              ]);
                }
            }
        }
    }

    public function keep_seller_from_user(){
        if ($this->Settings->keep_seller_from_user == 1 && $this->session->userdata('seller_id')) {
            return true;
        }
        return false;
    }

    public function validate_except_category_taxes($category, $subcategory){
        if ($this->Settings->except_category_taxes == 1 && (date('Y-m-d') >= $this->Settings->category_tax_exception_start_date && date('Y-m-d') <= $this->Settings->category_tax_exception_end_date)) {
            if (($subcategory && $subcategory->except_category_taxes == 1) || ($category->except_category_taxes == 1)) {
                return true;
            }
        }
        return false;
    }


    public function zero_left_consecutive($consecutive, $end_consecutive){
        $length = strlen($end_consecutive);
        $actual_length = strlen($consecutive);
        $remaining_length = $length - $actual_length;
        if ($remaining_length > 0) {
            for ($i=1; $i <= $remaining_length  ; $i++) {
                $consecutive = "0".$consecutive;
            }
        }
        return $consecutive;
    }

    public function clean_json_text($text){
        $text = str_replace('"', '-', $text);
        $text = str_replace("{", "-", $text);
        $text = str_replace("}", "-", $text);
        $text = str_replace("[", "-", $text);
        $text = str_replace("]", "-", $text);
        $text = str_replace("\\", "-", $text);
        $text = str_replace("\n", "-", $text);
        $text = str_replace(",", "-", $text);
        $text = str_replace("   ", "-", $text);
        return $text;
    }

    public function remove_linebreak_sql_text($text){

        $text = preg_replace('[\n|\r|\n\r|\t|\0|\x0B]', '', $text);
        for ($i=1; $i < 10; $i++) {
            $text = str_replace("  ", " ", $text);
        }
        return $text;
    }

    public function get_filter_options($selectedOption = NULL){
        $types_filter_records = array(
              1 =>lang('filter_options_today'),
              2 =>lang('filter_options_current_month'),
              3 =>lang('filter_options_current_quarter'),
              8 =>lang('filter_options_current_semester'),
              4 =>lang('filter_options_current_year'),
              6 =>lang('filter_options_last_month'),
              9 =>lang('filter_options_last_quarter'),
              10 =>lang('filter_options_last_semester'),
              7 =>lang('filter_options_last_year'),
              11 =>lang('filter_options_last_three_months'),
              5 =>lang('filter_options_date_range'),
              0 =>lang('filter_options_all'),
        );
        $html = "";
        if ($this->Settings->big_data_limit_reports == 1) {
            unset($types_filter_records[0]);
        }
        foreach ($types_filter_records as $key => $value) {
            if (!empty($selectedOption)) {
                $selected = $selectedOption == $key ? 'selected="selected"' : "";
            } else {
                $selected = $this->Settings->default_records_filter == $key ? 'selected="selected"' : "";
            }
            $html.="<option value=".$key." ".$selected.">".$value."</option>";
        }
        echo $html;
    }

    public function preferences_selection($arr){
        $arr = json_decode($arr);
        foreach ($arr as $key => $value) {
            if ($value && count($value) == 0 || !$value) {
                unset($arr[$key]);
            }
        }
        return json_encode($arr);
    }

    public function print_preference_selection($arr_txt, $report = false){
        if (!empty($arr_txt)) {
            $arr = json_decode($arr_txt);
            if (is_array($arr) || is_object($arr)) {
                $prf_text = "";
                foreach ($arr as $cat_id => $prf_arr) {
                    $prf_cat = $this->site->get_product_preference_category_by_id($cat_id);
                    $prf_text .= "<b>".$prf_cat->name."</b> : ";
                    foreach ($prf_arr as $prf_pos => $prf_id) {
                        $prf_data = $this->site->get_product_preference_by_id($prf_id);
                        $prf_text .= $prf_data ? $prf_data->name.", " : "";
                    }
                }
                return trim($prf_text, ", ");
            } else {
                return $arr_txt." (Anterior)";
            }
        } else {
            return null;
        }
    }

    public function cf_arr_to_text($arr){
        if (is_array($arr)) {
            $txt = "";
            foreach ($arr as $key => $value) {
                $txt.=$value.", ";
            }
            return trim($txt, ", ");
        } else {
            return $arr;
        }
    }

    public function eliminar_archivos_antiguos($rutas, $extensiones, $dias = 60) {
        $fecha_limite = time() - $dias * 24 * 60 * 60; // calcula la fecha límite en segundos

        foreach ($rutas as $ruta) {
            if (is_dir($ruta)) { // verifica si la ruta es un directorio válido
                $archivos = glob($ruta . '/*.{'.implode(',', $extensiones).'}', GLOB_BRACE); // obtiene todos los archivos con las extensiones definidas en $extensiones

                foreach ($archivos as $archivo) {
                    if (filemtime($archivo) < $fecha_limite) { // verifica si la fecha de modificación del archivo es anterior a la fecha límite
                        unlink($archivo); // elimina el archivo
                    }
                }
            }
        }
    }

    public function vatNoIsIncorrect($typeVatNoFe, $vatNo)
    {
        if ($typeVatNoFe == 21 || $typeVatNoFe == 22 || $typeVatNoFe == 42 || $typeVatNoFe == 50) {
            return false;
        }  else {
            return $this->validate_only_number($vatNo);
        }
    }

    public function get_var_null($var){
        if (!empty($var)) {
            if ((is_string($var) && strtolower($var) == 'null') || $var == NULL) {
                return NULL;
            } else if ($var != NULL && $var != 'null') {
                return $var;
            }
        } else {
            return NULL;
        }
    }


    // public function decode () {
    //     // Ejemplo de uso
    //     $textoOriginal = "Hola, mundo!";
    //     $clave = "clave_secreta";

    //     $textoCodificado = $this->encrypt($textoOriginal, $clave);
    //     echo "Texto codificado: " . $textoCodificado . "\n";

    //     $textoDecodificado = $this->decrypt($textoCodificado, $clave);
    //     echo "Texto decodificado: " . $textoDecodificado . "\n";
    // }

    function wappsi_encrypt($texto, $clave) {
        $ivLength = openssl_cipher_iv_length('AES-256-CBC');
        $iv = openssl_random_pseudo_bytes($ivLength);
        $encrypted = openssl_encrypt($texto, 'AES-256-CBC', $clave, OPENSSL_RAW_DATA, $iv);
        $ciphertext = base64_encode($iv . $encrypted);
        return $ciphertext;
    }

    function wappsi_decrypt($ciphertext, $clave) {
        $ciphertext = base64_decode($ciphertext);
        $ivLength = openssl_cipher_iv_length('AES-256-CBC');
        $iv = substr($ciphertext, 0, $ivLength);
        $encrypted = substr($ciphertext, $ivLength);
        $decrypted = openssl_decrypt($encrypted, 'AES-256-CBC', $clave, OPENSSL_RAW_DATA, $iv);
        return $decrypted;
    }

    function obtener_array_comillas($texto) {
        $patron = '/"(.*?)"/'; // Expresión regular para encontrar partes entre comillas
        preg_match_all($patron, $texto, $coincidencias);

        $array_resultante = $coincidencias[1]; // Las partes entre comillas se encuentran en el índice 1

        return $array_resultante;
    }

    function isNew($product) {
        // Convertir las fechas a objetos DateTime
        $fechaObj1 = new DateTime($product->registration_date);
        $fechaObj2 = new DateTime(date('Y-m-d'));
        // Calcular la diferencia entre las fechas en días
        $diferencia = $fechaObj1->diff($fechaObj2);
        // Obtener la diferencia total en días
        $diasDiferencia = $diferencia->days;
        // Comprobar si la diferencia es mayor o igual al número de días de antigüedad requerido
        if ($diasDiferencia <= $this->Settings->days_to_new_product) {
            return true;
        } else {
            return false;
        }
    }

    function replace_comillas($var){
        if (is_array($var)) {
            foreach ($var as $key => $value) {
                $var[$key] = $this->replace_comillas($value);
            }
            return $var;
        } else if (is_object($var)) {
            foreach ($var as $key => $value) {
                $var->$key = $this->replace_comillas($value);
            }
            return $var;
        } else {
            $var = str_replace("\n", '%c', $var);
            $var = str_replace('"', '%c', $var);
            $var = str_replace("'", "%c", $var);
            return $var;
        }
    }
    function set_tabindex($set_text = true){
        $this->tabindex++;
        if ($set_text == true) {
            return " tabindex='".$this->tabindex."' ";
        } else {
            return $this->tabindex;
        }
    }
    function reset_tabindex(){
        $this->tabindex = 0;
    }
    function get_img_url($url, $thumb = false){
        $img_url = 'assets/uploads/no_image.png';
        if ($this->Settings->images_from_tpro != 0) {
            // $img_url = is_file('https://'.$url) ? 'https://'.$url : base_url().'assets/uploads/no_image.png';
            if ($this->Settings->images_from_tpro == 2) {
                $img_url = ($url != 'no_image.png') ? "../../../../../../public/uploads/all/".basename($url) : base_url().'assets/uploads/no_image.png';
            }
            if ($this->Settings->images_from_tpro == 1) {
                $img_url = ($url != 'no_image.png') ? 'https://'.$url : base_url().'assets/uploads/no_image.png';
            }

            if ($thumb) {
            }
        } else {
            if ($thumb) {
                $img_url = (is_file("assets/uploads/thumbs/".$url) ? base_url().'assets/uploads/thumbs/'.$url : base_url().'assets/uploads/no_image.png');
            } else {
                $img_url = base_url().'assets/uploads/'.((is_file("assets/uploads/".$url) ? $url : 'no_image.png'));
            }
        }
        return $img_url;
    }
    function get_float_from_percentage_string($string){
        if (strpos($string, "%") !== false) {
            $string = $this->formatDecimal(str_replace("%", "", $string));
            if ($string > 0) {
                $string = $this->formatDecimal($string / 100);
                return $string;
            }
        }
        return false;
    }
    function import_field_val($val){
        if ($val) {
            if (mb_strtolower($val) == 'n/a') {
                return NULL;
            }
            if (mb_strtolower($val) == 'si') {
                return 1;
            }
            if (mb_strtolower($val) == 'no') {
                return 0;
            }
            return $this->site->cleanRowCsvImport($val);
        }
        return NULL;
    }

    function calculate_second_tax($second_tax, $amount, $tax_rate, $tax_method, $st_included = false, $discount = 0) {
        if ($second_tax) {
            $second_tax = strval($second_tax);
            $percentage = false;
            $ptr = false;
            if ($tax_rate) {
                $ptr = $this->sma->getTaxRateByID($tax_rate ? $tax_rate : $this->Settings->product_default_exempt_tax_rate);
            }
            $amount_base = $amount;
            if (strpos($second_tax, "%") !== false) {
                $percentage = floatval(str_replace('%', '', $second_tax)) / 100;
                if ($st_included) {
                    $tax_perc = $ptr ? ($ptr->rate / 100) : 0;
                    $amount_base = ($amount / (1 + ($percentage + $tax_perc))) - $discount;
                    $second_tax_val = $this->formatDecimal($amount_base * $percentage);
                } else {
                    if ($ptr) {
                        $tax_val = calculate_tax($tax_rate, $amount, $tax_method);
                        $amount_base = $amount - $tax_val - $discount;
                    } else {
                        $amount_base = $amount - $discount;
                    }
                    $second_tax_val = $this->formatDecimal($amount_base * $percentage);
                }
            } else {
                $second_tax_val = $second_tax;
            }
            return [$this->formatDecimal($amount_base), $this->formatDecimal($second_tax_val)];
        } else {
            return [0, 0];
        }
    }

    function validarCodigosContinuos($productos, $field) {
        $this->vccfield = $field;
        $codigos = array_map(function($producto) {
            return intval($producto[$this->vccfield]);
        }, $productos);

        sort($codigos); // Ordenar los códigos

        $ultimoCodigo = null;
        foreach ($codigos as $codigo) {
            if ($ultimoCodigo !== null && $codigo != $ultimoCodigo + 1) {
                return false; // Si hay un salto, retornar falso
            }
            $ultimoCodigo = $codigo;
        }
        return true; // Si no hay saltos, retornar true
    }
    function unique_field($longitud = 20) {
        $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longitudCaracteres = strlen($caracteres);
        $cadenaAleatoria = '';
        
        for ($i = 0; $i < $longitud; $i++) {
            $cadenaAleatoria .= $caracteres[random_int(0, $longitudCaracteres - 1)];
        }
        
        return $cadenaAleatoria;
    }

    function seller_restricted(){
        $seller_restricted = FALSE;
        if ($this->Owner || $this->Admin) {
            return $seller_restricted;
        } else {
            if (!$this->session->userdata('view_right') && $this->session->userdata('seller_id')) {
                $seller_restricted = $this->session->userdata('seller_id');
            } else if (!$this->session->userdata('view_right') && $this->session->userdata('company_id')) {
                $seller_restricted = $this->session->userdata('company_id');
            }
        }
        return $seller_restricted;
    }

    function utf8Decode($text){
        return mb_convert_encoding($text, 'ISO-8859-1', 'UTF-8');
    }

    function utf8Encode($text){
        return mb_convert_encoding($text, 'UTF-8', 'ISO-8859-1');
    }

    function field_is_filled($field, $type = 'numeric'){
        if (!empty($field) && !is_null($field) && $field != 'null' && $field != 'false') {
            if ($type == 'numeric' && is_numeric($field) && $field != 0) {
                return true;
            } else if ($type == 'text' && $type != '') {
                return true;
            }
        }
    }
}