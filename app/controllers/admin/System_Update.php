<?php 
class System_update extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
    }

    public function maintenance($id = NULL)
    {
        $this->Settings = $this->site->get_setting();
        $this->theme = $this->Settings->theme.'/admin/views/';
        $this->load_view($this->theme . 'settings/maintenance', []);
    }
}

 ?>