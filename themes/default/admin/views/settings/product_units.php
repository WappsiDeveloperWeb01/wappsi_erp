<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {

      $('body').keyup(function(e) {
        if(e.keyCode == 13) {
          console.log('Has presionado ENTER');
          e.preventDefault();
        }
      });

        var ti = 0;
        $(document).on('change', '.price', function () {
            var row = $(this).closest('tr');
            row.first('td').find('input[type="checkbox"]').iCheck('check');
            var price = row.find('.form-submit').trigger('click');
        });
        $(document).on('click', '.form-submit', function () {
            var btn = $(this);
            btn.html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
            var row = btn.closest('tr');
            var product_id = row.attr('id');
            var price = row.find('.price').val();
            var cantidad = $('#cantidad').val();
            console.log('Actualización, cantidad: '+cantidad);
            $.ajax({
                type: 'post',
                url: '<?= admin_url('system_settings/update_product_unit_price/'.$unit->id); ?>',
                dataType: "json",
                data: {
                    <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>',
                    product_id: product_id, price: price, cantidad: cantidad
                },
                success: function (data) {
                    if (data.status != 1) {
                        command: toastr.error('Por favor revise que haya digitado bien el valor, si se trata de algún error contáctese con soporte.', 'No se actualizó', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                        btn.removeClass('btn-primary').addClass('btn-danger').html('<i class="fa fa-times"></i>');
                    }else{
                        btn.removeClass('btn-primary').removeClass('btn-danger').addClass('btn-success').html('<i class="fa fa-check"></i>');
                    }
                },
                error: function (data) {
                    btn.removeClass('btn-primary').addClass('btn-danger').html('<i class="fa fa-times"></i>');
                }
            });
            // btn.html('<i class="fa fa-check"></i>');
        });
        function price_input(x) {
            ti = ti+1;
            var v = x.split('__');
            return "<div class=\"text-center\"><input type=\"text\" name=\"price"+v[0]+"\" value=\""+(v[1] != '' ? formatDecimals(v[1]) : '')+"\" class=\"form-control text-center price\" tabindex=\""+(ti)+"\" style=\"padding:2px;height:auto;\"></div>"; // onclick=\"this.select();\"
        }
        function cantidad_input(x) {
            ti = ti+1;
            var v = x.split('__');
            return "<div class=\"text-center\"><input type=\"text\" name=\"cantidad"+v[0]+"\" value=\""+(v[1] != '' ? formatDecimals(v[1]) : '')+"\" class=\"form-control text-center cantidad\" tabindex=\""+(ti)+"\" style=\"padding:2px;height:auto;\"></div>"; // onclick=\"this.select();\"
        }


        $('#CGData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('system_settings/wGetProducts/'.$unit->id.'/'.$unit->operation_value) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                console.log(aData);
                nRow.id = aData[0];
                nRow.className = "product_group_price_id";
                return nRow;
            },
            "aoColumns": [
                            {"bSortable": false, "mRender": checkbox}, 
                            null, 
                            null, 
                            {"bSortable": false, "mRender": price_input},
                            {"bSortable": false}
                        ]
        }).fnSetFilteringDelay();
    });
    //{"bSortable": false, "mRender": cantidad_input}
</script>
<?= admin_form_open('system_settings/product_unit_price_actions/'.$unit->id.'/'.$unit->operation_value, 'id="action-form"') ?>



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>
            <!-- <i class="fa-fw fa fa-building"></i> -->
            <?= $page_title ?> (<?= $unit->name; ?>)
<?php //var_dump($_POST); ?>
<?php //var_dump($_GET); ?>
<?php //var_dump($unit); ?>


<?php if(!$unit->operation_value){ $unit->operation_value = 1; } ?>





        </h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
          </ol> -->
    </div>
</div>
<!-- Termina la cabecera -->

<input type="hidden" name="cantidad" id="cantidad" value="<?php echo $unit->operation_value; ?>">
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">                            
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle"> <?= lang('actions') ?> <span class="caret"></span> </button> 
                                <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="#" id="update_price" data-action="update_price">
                                            <i class="fa fa-dollar"></i> Actualizar precios
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo admin_url('system_settings/update_unit_prices_csv/'.$unit->id); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-upload"></i> Actualizar precios con CSV
                                        </a>
                                    </li>        
                                </ul>
                            </div>                       
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">


                <!-- <p class="introtext"><?= lang("list_results"); ?>***</p> -->

                <div class="table-responsive">
                    <table id="CGData" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                            </th>
                            <th class="col-xs-3"><?= lang("product_code"); ?></th>
                            <th class="col-xs-4"><?= lang("product_name"); ?></th>
                            <th><?= lang("price"); ?></th>
                            <th style="width:85px;"><?= lang("update"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>

                        </tbody>
                    </table>
                </div>


                        </div>
                    </div>
                </div><!-- /.ibox-content -->
            </div><!-- /.ibox -->
        </div>
    </div>
</div>
<div style="display: none;">
    <!-- <input type="hidden" name="unit_id" value="<?php //echo $unit->id; ?>" id="unit_id"/> -->
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>
<script language="javascript">
    $(document).ready(function () {

        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#update_price').click(function (e) {
            e.preventDefault();
            console.log('Click en actualizar Precios para multiples registros.');
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

    });
</script>
