<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-action">
                                <a href="<?php echo base_url(); ?>assets/csv/subir_productos_variantes.xlsx" class="btn btn-primary pull-right" style="margin-left: 10px;"> <i class="fa-solid fa-download"></i> <?= lang("download_sample_file_product_variants") ?></a>
                                <a href="<?php echo base_url(); ?>assets/csv/subir_productos.xlsx" class="btn btn-primary pull-right"> <i class="fa-solid fa-download"></i> <?= lang("download_sample_file_products") ?></a>
                            </div>
                        </div>
                    </div>

                    <?= admin_form_open_multipart("products/import_csv", ['class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form', 'id'=> 'import_csv']); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <span><b><?= lang('product_code')." ".lang('current') ?> : </b><?= $order_ref->product_code ?></span>
                            <br>
                            <span><b><?= lang('variant_code')." ".lang('current') ?> : </b><?= $order_ref->variant_code ?></span>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="xls_file"><?= lang("products_file"); ?></label>
                                    <input type="file" data-browse-label="<?= lang('browse'); ?>" name="xls_file" class="form-control file" data-show-upload="false" data-show-preview="false" id="xls_file" required="required"/>
                                </div>
                                <div class="form-group">
                                    <label for="pv_xls_file"><?= lang("product_variants_file"); ?></label>
                                    <input type="file" data-browse-label="<?= lang('browse'); ?>" name="pv_xls_file" class="form-control file" data-show-upload="false" data-show-preview="false" id="pv_xls_file"/>
                                </div>
                                <div class="form-group">
                                    <?php echo form_submit('import', $this->lang->line("import"), 'class="btn btn-primary"'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
     $("#import_csv").submit(function(e){
        if ($('#import_csv').valid()) {
            $('#loading').fadeIn();
            command: toastr.warning('La información está siendo procesado, por favor sea paciente.', '¡Procesando información!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "8000",
                    "extendedTimeOut": "1000",
                });
            setTimeout(function() {
                location.href = site.base_url+'products';
            }, 8000);
        }
            
    });
</script>