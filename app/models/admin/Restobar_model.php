<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restobar_model extends CI_Model {

	public function get_tables($area_id)
	{
		$this->db->distinct();
		$this->db->select('tables.*, suspended_bills.id as suspended_bill_id, suspended_bills.biller_id AS biller_id');
		$this->db->from('tables');
		$this->db->join('suspended_bills', 'suspended_bills.table_id = tables.id', 'left');
		$this->db->where('area_id', $area_id);
		$this->db->order_by('id');
		$response = $this->db->get();

		return $response->result();
	}

	public function get_table_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('tables');
		$this->db->where('id', $id);
		$response = $this->db->get();

		return $response->row();
	}

	public function get_table_by_number($number, $area_id, $table_id = NULL)
	{
		$this->db->select('*');
		$this->db->from('tables');
		$this->db->where('area_id', $area_id);
		$this->db->where('numero', $number);

		if  (! empty($table_id)) {
			$this->db->where('id !=', $table_id);
		}

		$response = $this->db->get();

		return $response->row();
	}

	public function get_branches()
	{
		$this->db->select('id, name AS nombre');
		$this->db->from('companies');
		$this->db->where('group_name', 'biller');
		$this->db->order_by('name');
		$response = $this->db->get();

		return $response->result();
	}

	public function get_branch_areas($branch_id)
	{
		$this->db->select('*');
		$this->db->from('branch_areas');
		$this->db->where('sucursal_id', $branch_id);
		$response = $this->db->get();

		return $response->result();
	}

	public function get_table_by_restobar_module($module)
	{
        if ($module == 1) {
		    $tipo = "M";
        } else if ($module == "2") {
            $tipo = "D";
        } else {
            $tipo ="LL";
        }

		$this->db->select("*");
		$this->db->from("tables");
		$this->db->where("tipo", $tipo);
		$this->db->where("estado", 1);
		$this->db->order_by("numero", "asc");
		$this->db->limit(1);

		$response = $this->db->get();

		return $response->row();
	}

    public function getAvailableTables($data = [])
    {
        if (!empty($data)) {
            $this->db->where($data);
        }
        $response = $this->db->get("tables");

		return $response->result();
    }

    public function getSuspendedeBill($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('suspended_bills');

        return $query->row();
    }

	public function get_existing_products_not_dispatched($suspended_sales_id)
	{
		$this->db->where('suspend_id', $suspended_sales_id);
		$this->db->where('state_readiness < ', DISPATCHED);
		$query = $this->db->get('suspended_items');

		return $query->result();
	}

	public function get_existing_products_dispatched($suspended_sales_id)
	{
		$this->db->where('suspend_id', $suspended_sales_id);
		$this->db->where('state_readiness = ', DISPATCHED);
		$query = $this->db->get('suspended_items');

		return $query->result();
	}

	public function get_consecutive_table_number($sucursal_id)
	{
		$this->db->select('*');
		$this->db->from('tables');
		$this->db->join('branch_areas', 'branch_areas.id = tables.area_id', 'inner');
		$this->db->where('sucursal_id', $sucursal_id);
		$this->db->where('tipo', 'M');
		$this->db->order_by('tipo');
		$this->db->order_by('numero', 'desc');
		$this->db->limit(1);
		$query1 = $this->db->get()->result();

		$this->db->select('*');
		$this->db->from('tables');
		$this->db->join('branch_areas', 'branch_areas.id = tables.area_id', 'inner');
		$this->db->where('sucursal_id', $sucursal_id);
		$this->db->where('tipo', 'D');
		$this->db->order_by('tipo');
		$this->db->order_by('numero', 'desc');
		$this->db->limit(1);
		$query2 = $this->db->get()->result();

		$this->db->select('*');
		$this->db->from('tables');
		$this->db->join('branch_areas', 'branch_areas.id = tables.area_id', 'inner');
		$this->db->where('sucursal_id', $sucursal_id);
		$this->db->where('tipo', 'LL');
		$this->db->order_by('tipo');
		$this->db->order_by('numero', 'desc');
		$this->db->limit(1);
		$query3 = $this->db->get()->result();


		$query = array_merge($query1, $query2, $query3);
		return $query;
	}

	public function insert_table($data)
	{
		if ($this->db->insert('tables', $data)) {
			return TRUE;
		}

		return FALSE;
	}

	public function insert_batch_table($data)
	{
		if ($this->db->insert_batch('tables', $data) > 0) {
			return TRUE;
		}

		return FALSE;
	}

	public function insert_suspended_sale($data)
	{
		if ($this->db->insert('suspended_bills', $data)) {
			return $this->db->insert_id();
		}

		return FALSE;
	}

	public function update_table($data, $id) {
		$this->db->where('id', $id);
		if ($this->db->update('tables', $data)) {
			return TRUE;
		}

		return FALSE;
	}

	public function update_suspended_bills($data, $id) {
		$this->db->where('id', $id);
		if ($this->db->update('suspended_bills', $data)) {
			return TRUE;
		}

		return FALSE;
	}

	public function delete_suspend_sale($id)
	{
		$this->db->where('id', $id);
		if ($this->db->delete("suspended_bills")) {
			return $this->delete_suspend_sale_items($id);
		} else {
			return FALSE;
		}
	}

	public function delete_suspend_sale_items($suspended_sale_id)
	{
		$this->db->where('suspend_id', $suspended_sale_id);
		if ($this->db->delete("suspended_items")) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

/* End of file Restobar_model.php */
/* Location: .//opt/lampp/htdocs/wappsi/app/models/admin/restobar_model.php */