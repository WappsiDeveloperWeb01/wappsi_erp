var type_pos = 2;

if (localStorage.getItem('posshipping')) {
    localStorage.removeItem('posshipping');
}
if (localStorage.getItem('subtotal')) {
    localStorage.removeItem('subtotal');
}
if (localStorage.getItem('tax')) {
    localStorage.removeItem('tax');
}
if (localStorage.getItem('total')) {
    localStorage.removeItem('total');
}
if (localStorage.getItem('discount')) {
    localStorage.removeItem('discount');
}
if (localStorage.getItem('gtotal')) {
    localStorage.removeItem('gtotal');
}
if (localStorage.getItem('customer_validate_min_base_retention')) {
    localStorage.removeItem('customer_validate_min_base_retention');
}


var updated_items = 0;
var prices_checked = false;

$(document).ready(function() {
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    $(document).on('click', '#toogle-customer-read-attr', function () {
        var nst = $('#poscustomer').is('[readonly]') ? false : true;
        console.log('read '+nst);
        $('#poscustomer').select2("readonly", nst);
        return false;
    });
    $(".open-brands").click(function () {
        $('#brands-slider').toggle('slide', { direction: 'up' }, 700);
    });
    $(".open-category").click(function () {
        $('#category-slider').toggle('slide', { direction: 'up' }, 700);
    });
    $(".open-subcategory").click(function () {
        $('#subcategory-slider').toggle('slide', { direction: 'up' }, 700);
    });
    $(".open-promotions").click(function () {
        $('#promotions-slider').toggle('slide', { direction: 'up' }, 700);
    });
    $(document).on('click', function(e){
        if (!$(e.target).is(".open-brands, .cat-child") && !$(e.target).parents("#brands-slider").size() && $('#brands-slider').is(':visible')) {
            $('#brands-slider').toggle('slide', { direction: 'up' }, 700);
        }
        if (!$(e.target).is(".open-category, .cat-child") && !$(e.target).parents("#category-slider").size() && $('#category-slider').is(':visible')) {
            $('#category-slider').toggle('slide', { direction: 'up' }, 700);
        }
        if (!$(e.target).is(".open-subcategory, .cat-child") && !$(e.target).parents("#subcategory-slider").size() && $('#subcategory-slider').is(':visible')) {
            $('#subcategory-slider').toggle('slide', { direction: 'up' }, 700);
        }
        if (!$(e.target).is(".open-promotions, .cat-child") && !$(e.target).parents("#promotions-slider").size() && $('#promotions-slider').is(':visible')) {
            $('#promotions-slider').toggle('slide', { direction: 'up' }, 700);
        }
    });
    $('.po').popover({html: true, placement: 'right', trigger: 'click'}).popover();
    $('#inlineCalc').calculator({layout: ['_%+-CABS','_7_8_9_/','_4_5_6_*','_1_2_3_-','_0_._=_+'], showFormula:true});
    $('.calc').click(function(e) { e.stopPropagation();});
    $(document).on('click', '[data-toggle="ajax"]', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $.get(href, function( data ) {
            $("#myModal").html(data).modal();
        });
    });
    $(document).on('click', '.sname', function(e) {
        var row = $(this).closest('tr');
        var positems_id = $(row).data('item-id');
        var itemid = row.find('.rid').val();
        if (site.settings.product_clic_action == 1) {
            $('#myModal').modal({remote: site.base_url + 'products/modal_view/' + itemid});
            $('#myModal').modal('show');
        } else if (site.settings.product_clic_action == 2) {

            if (positems[positems_id].options) {
                $('#myModal').modal({remote: site.base_url + 'pos/product_variants_selection/' + itemid + "/" + positems_id + "/"+$('#poswarehouse').val()});
                $('#myModal').modal('show');
            } else {
                if (positems[positems_id].preferences) {
                    $('#myModal').modal({remote: site.base_url + 'pos/product_preferences_selection/' + itemid + "/" + positems_id, 
                    backdrop: 'static',
                    keyboard: false});
                    $('#myModal').modal('show');
                }
            }

        } else if (site.settings.product_clic_action == 3) {
            row.find('.edit').trigger('click');
        }
    });
    resize_pos();
    
    // get_customer_branches();
    setTimeout(function() {
        check_prices();
        if (pos_settings.show_suspended_bills_automatically == 1 && pos_settings.restobar_mode != 1 && !localStorage.getItem('positems') && count_suspended_bills > 0) {
            $('.opened_bills_sup').click();
        }
        $("#add_item").focus();
    }, 1200);

    
});

$(document).ready(function () {

    if (posbiller = localStorage.getItem('posbiller')) {
        $('#posbiller').select2('val', posbiller).trigger('change');
    } else {
        $('#posbiller').trigger('change');
    }

    // Order level shipping and discount localStorage
    if (posdiscount = localStorage.getItem('posdiscount')) {
        $('#posdiscount').val(posdiscount);
    }
    $(document).on('change', '#ppostax2', function () {
        localStorage.setItem('postax2', $(this).val());
        $('#postax2').val($(this).val());
    });

    if (postax2 = localStorage.getItem('postax2')) {
        $('#postax2').val(postax2);
    }

    $(document).on('blur', '#sale_note', function () {
        localStorage.setItem('posnote', $(this).val());
        $('#sale_note').val($(this).val());
    });

    if (posnote = localStorage.getItem('posnote')) {
        $('#sale_note').val(posnote);
    }

    $(document).on('blur', '#staffnote', function () {
        localStorage.setItem('staffnote', $(this).val());
        $('#staffnote').val($(this).val());
    });

    if (staffnote = localStorage.getItem('staffnote')) {
        $('#staffnote').val(staffnote);
    }

    if (posshipping = localStorage.getItem('posshipping')) {
        $('#posshipping').val(posshipping);
        shipping = parseFloat(posshipping);
    }
    $("#pshipping").click(function(e) {
        e.preventDefault();
        shipping = $('#posshipping').val() ? $('#posshipping').val() : shipping;
        $('#shipping_input').val(shipping);
        $('#sModal').modal();
    });
    $('#sModal').on('shown.bs.modal', function() {
        $(this).find('#shipping_input').select().focus();
    });
    $(document).on('click', '#updateShipping', function() {
        var s = parseFloat($('#shipping_input').val() ? $('#shipping_input').val() : '0');
        if (is_numeric(s)) {
            $('#posshipping').val(s);
            localStorage.setItem('posshipping', s);
            shipping = s;
            loadItems();
            $('#sModal').modal('hide');
        } else {
            bootbox.alert(lang.unexpected_value);
        }
    });

    $("#ptip").click(function(e) {
        e.preventDefault();
        tip_amnt = $('#sale_tip_amount').val() ? $('#sale_tip_amount').val() : tip_amount;
        $('#tip_amount_input').val(tip_amnt);
        $('#tipModal').modal();
    });
    $('#tipModal').on('shown.bs.modal', function() {
        $(this).find('#tip_amount_input').select().focus();
    });

    $(document).on('click', '#updateTip', function() {
        var s = $('#tip_amount_input').val() ? $('#tip_amount_input').val() : '0';
        if (is_numeric(s)) {
            $('#sale_tip_amount').val(s);
            localStorage.setItem('tip_amount', s);
            localStorage.removeItem('delete_tip_amount');
            tip_amount = s;
            loadItems();
            $('#tipModal').modal('hide');
        } else {
            bootbox.alert(lang.unexpected_value);
        }
    });

    $(document).on('click', '#recalculateTip', function() {
        localStorage.removeItem('delete_tip_amount');
        localStorage.setItem('tip_amount', 0);
        loadItems();
        $('#tipModal').modal('hide');
    });

    $(document).on('click', '#deleteTip', function() {
        localStorage.setItem('delete_tip_amount', 1);
        loadItems();
        $('#tipModal').modal('hide');
    });

    // JS Retenciones
    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_fuente_option').select2('val', '').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_iva_option').select2('val', '').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_ica_option').select2('val', '').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_otros_option').select2('val', '').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option', function(){
        rete_fuente_id = $(this).val();
        $('#rete_fuente_id').val(rete_fuente_id);
        prevamnt = $('#rete_fuente_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_fuente_option option:selected').data('percentage');
        apply = $('#rete_fuente_option option:selected').data('apply');
        account = $('#rete_fuente_option option:selected').data('account');
        $('#rete_fuente_account').val(account);
        min_base = $('#rete_fuente_option option:selected').data('minbase');
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
        key_log = localStorage.getItem('poskeylog');
        if (key_log != 3 && (amount >= min_base || customer_validate_min_base_retention == 'false')) {
            $('#rete_fuente_base').val(amount);
            cAmount = amount * (percentage / 100);
            cAmount = Math.round(cAmount);
            $('#rete_fuente_tax').val(percentage);
            $('#rete_fuente_valor').val(cAmount);
            rete_fuente_amount = formatMoney(cAmount);
            setReteTotalAmount(rete_fuente_amount, '+');
        }
    });

    $(document).on('change', '#rete_iva_option', function(){
        rete_iva_id = $(this).val();
        $('#rete_iva_id').val(rete_iva_id);
        prevamnt = $('#rete_iva_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_iva_option option:selected').data('percentage');
        apply = $('#rete_iva_option option:selected').data('apply');
        account = $('#rete_iva_option option:selected').data('account');
        $('#rete_iva_account').val(account);
        min_base = $('#rete_iva_option option:selected').data('minbase');
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
        key_log = localStorage.getItem('poskeylog');
        if (key_log != 3 && (amount >= min_base || customer_validate_min_base_retention == 'false')) {
            $('#rete_iva_base').val(amount);
            cAmount = amount * (percentage / 100);
            cAmount = Math.round(cAmount);
            $('#rete_iva_tax').val(percentage);
            $('#rete_iva_valor').val(cAmount);
            rete_iva_amount = formatMoney(cAmount);
            setReteTotalAmount(rete_iva_amount, '+');
        }
    });

    $(document).on('change', '#rete_ica_option', function(){
        rete_ica_id = $(this).val();
        $('#rete_ica_id').val(rete_ica_id);
        prevamnt = $('#rete_ica_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_ica_option option:selected').data('percentage');
        apply = $('#rete_ica_option option:selected').data('apply');
        account = $('#rete_ica_option option:selected').data('account');
        $('#rete_ica_account').val(account);
        min_base = $('#rete_ica_option option:selected').data('minbase');
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
        key_log = localStorage.getItem('poskeylog');
        if (key_log != 3 && (amount >= min_base || customer_validate_min_base_retention == 'false')) {
            $('#rete_ica_base').val(amount);
            cAmount = amount * (percentage / 100);
            cAmount = Math.round(cAmount);
            $('#rete_ica_tax').val(percentage);
            $('#rete_ica_valor').val(cAmount);
            rete_ica_amount = formatMoney(cAmount);
            setReteTotalAmount(rete_ica_amount, '+');
        }
    });

    $(document).on('change', '#rete_otros_option', function(){
        rete_otros_id = $(this).val();
        $('#rete_otros_id').val(rete_otros_id);
        prevamnt = $('#rete_otros_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_otros_option option:selected').data('percentage');
        apply = $('#rete_otros_option option:selected').data('apply');
        account = $('#rete_otros_option option:selected').data('account');
        $('#rete_otros_account').val(account);
        min_base = $('#rete_otros_option option:selected').data('minbase');
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
        key_log = localStorage.getItem('poskeylog');
        if (key_log != 3 && (amount >= min_base || customer_validate_min_base_retention == 'false')) {
            $('#rete_otros_base').val(amount);
            cAmount = amount * (percentage / 100);
            cAmount = Math.round(cAmount);
            $('#rete_otros_tax').val(percentage);
            $('#rete_otros_valor').val(cAmount);
            rete_otros_amount = formatMoney(cAmount);
            setReteTotalAmount(rete_otros_amount, '+');
        }
    });

    function getReteAmount(apply){

        amount = 0;

        if (apply == "ST") {
            amount = $('#subtotal').text();
        } else if (apply == "TX") {
            amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = $('#total').text();
        }

        return amount;
    }

    function setReteTotalAmount(amount, action){

        // console.log('Monto : '+amount+' acción : '+action);

        if (action == "+") {
            tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) + formatDecimal(amount);
        } else if (action == "-") {
            tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) - formatDecimal(amount);
        }

        if (tra < 0) {
            tra = 0;
        }

        $('#total_rete_amount').text(formatMoney(tra));
    }

    // JS Retenciones

    $(document).on('click', '.view_order_detail', function(){
        btn = $(this);

        if (btn.hasClass('detail_close')) {
            $('#product-list').css('height', '210px');
            $('#left-bottom').css('padding-top', 0);
            btn.removeClass('detail_close').addClass('detail_open');
            $('#icon_detail').removeClass('fa-bars').addClass('fa-minus');
            // $('.order_detail').css('display', '');
            $('.order_detail').fadeIn();
        } else if (btn.hasClass('detail_open')) {
            // $('#left-bottom').css('padding-top', 150);
            $('#left-bottom').css('padding-top', 0);
            btn.removeClass('detail_open').addClass('detail_close');
            $('#icon_detail').removeClass('fa-minus').addClass('fa-bars');
            // $('.order_detail').css('display', 'none');
            $('.order_detail').fadeOut(100);
            $('#product-list').css('height', '');
        }

    });

    /* ----------------------
         * Order Discount Handler
         * ---------------------- */
         $("#ppdiscount").click(function(e) {
            e.preventDefault();
            var dval = $('#posdiscount').val() ? $('#posdiscount').val() : '0';
            $('#order_discount_input').val(dval);
            $('#dsModal').modal();
         });
         $('#dsModal').on('shown.bs.modal', function() {
            $(this).find('#order_discount_input').select().focus();
            $('#order_discount_input').bind('keypress', function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    var ds = $('#order_discount_input').val();
                    if (is_valid_discount(ds)) {
                        $('#posdiscount').val(ds);
                        localStorage.removeItem('posdiscount');
                        localStorage.setItem('posdiscount', ds);
            // $('#payment').prop('disabled', true);
                        loadItems();
                    } else {
                        bootbox.alert(lang.unexpected_value);
                    }
                    $('#dsModal').modal('hide');
                }
            });
         });
         $(document).on('click', '#updateOrderDiscount', function() {
            var ds = $('#order_discount_input').val() ? $('#order_discount_input').val() : '0';
            if (is_valid_discount(ds)) {
                $('#posdiscount').val(ds);
                localStorage.removeItem('posdiscount');
                localStorage.setItem('posdiscount', ds);
                // $('#payment').prop('disabled', true);
                loadItems();
            } else {
                bootbox.alert(lang.unexpected_value);
            }
            $('#dsModal').modal('hide');
         });
    /* ----------------------
     * Order Tax Handler
     * ---------------------- */
     $("#pptax2").click(function(e) {
        e.preventDefault();
        var postax2 = localStorage.getItem('postax2');
        $('#order_tax_input').select2('val', postax2);
        $('#txModal').modal();
     });

     /* retención en la fuente */

     $("#pprete").click(function(e) {
        e.preventDefault();
        // var postax2 = localStorage.getItem('postax2');
        // $('#order_tax_input').select2('val', postax2);
        $('#rtModal').modal();
     });

     /* retención en la fuente */
     $('#txModal').on('shown.bs.modal', function() {
        $(this).find('#order_tax_input').select2('focus');
     });
     $('#txModal').on('hidden.bs.modal', function() {
        var ts = $('#order_tax_input').val();
        $('#postax2').val(ts);
        localStorage.setItem('postax2', ts);
        loadItems();
     });
     $(document).on('click', '#updateOrderTax', function () {
        var ts = $('#order_tax_input').val();
        $('#postax2').val(ts);
        localStorage.setItem('postax2', ts);
        loadItems();
        $('#txModal').modal('hide');
     });

     $(document).on('click', '#updateOrderRete', function () {
        // var ts = $('#order_tax_input').val();
        // $('#postax2').val(ts);

        $('#ajaxCall').fadeIn();

        var retenciones = {
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            };

        retenciones.total_discounted = formatMoney(localStorage.gtotal - parseFloat(formatDecimal($('#total_rete_amount').text())));


        setTimeout(function() {
            $('#gtotal').text(retenciones.total_discounted);
            $('#ajaxCall').fadeOut();
        }, 1000);

        localStorage.setItem('retenciones', JSON.stringify(retenciones));
        // loadItems();


        if (parseFloat(formatDecimal(retenciones.total_rete_amount)) > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(retenciones.total_rete_amount)))
        } else {
            $('#rete_applied').val(0);
        }

        $('#rete').text(retenciones.total_rete_amount);
        $('#rtModal').modal('hide');
     });

     $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('retenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
     });

     $(document).on('change', '.rserial', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        positems[item_id].row.serial = $(this).val();
        localStorage.setItem('positems', JSON.stringify(positems));
     });

// If there is any item in localStorage
    if (localStorage.getItem('positems')) {
        loadItems();
    }

    // clear localStorage and reload
    $('#reset').click(function (e) {
        if (protect_delete == 1) {
            biller_pin_code = billers_data[$('#posbiller').val()].pin_code;
            var boxd = bootbox.dialog({
                title: "<i class='fa fa-key'></i> Código de autorización",
                message: '<input id="pos_pin" name="pos_pin" type="password" placeholder="Código de autorización" class="form-control"> ',
                buttons: {
                    success: {
                        label: "<i class='fa fa-tick'></i> OK",
                        className: "btn-success verify_pin",
                        callback: function () {
                            var pos_pin = md5($('#pos_pin').val());
                            if(pos_pin == biller_pin_code) {

                                if (localStorage.getItem('positems')) {
                                    localStorage.removeItem('positems');
                                }
                                if (localStorage.getItem('posdiscount')) {
                                    localStorage.removeItem('posdiscount');
                                }
                                if (localStorage.getItem('postax2')) {
                                    localStorage.removeItem('postax2');
                                }
                                if (localStorage.getItem('posshipping')) {
                                    localStorage.removeItem('posshipping');
                                }
                                if (localStorage.getItem('posref')) {
                                    localStorage.removeItem('posref');
                                }
                                if (localStorage.getItem('poswarehouse')) {
                                    localStorage.removeItem('poswarehouse');
                                }
                                if (localStorage.getItem('posnote')) {
                                    localStorage.removeItem('posnote');
                                }
                                if (localStorage.getItem('posinnote')) {
                                    localStorage.removeItem('posinnote');
                                }
                                if (localStorage.getItem('poscustomer')) {
                                    localStorage.removeItem('poscustomer');
                                }
                                if (localStorage.getItem('poscurrency')) {
                                    localStorage.removeItem('poscurrency');
                                }
                                if (localStorage.getItem('posdate')) {
                                    localStorage.removeItem('posdate');
                                }
                                if (localStorage.getItem('posstatus')) {
                                    localStorage.removeItem('posstatus');
                                }
                                if (localStorage.getItem('posbiller')) {
                                    localStorage.removeItem('posbiller');
                                }
                                if (localStorage.getItem('retenciones')) {
                                    localStorage.removeItem('retenciones');
                                }
                                if (localStorage.getItem('tip_amount')) {
                                    localStorage.removeItem('tip_amount');
                                }
                                if (localStorage.getItem('delete_tip_amount')) {
                                    localStorage.removeItem('delete_tip_amount');
                                }
                                if (localStorage.getItem('pos_wholesale_keep_prices')) {
                                    localStorage.removeItem('pos_wholesale_keep_prices');
                                }

                                $('#modal-loading').show();
                                if (is_order) {
                                    window.location.href = site.base_url+"sales/orders";
                                } else {
                                    window.location.href = site.base_url+"pos/add_wholesale";
                                }
                            } else {
                                bootbox.alert('Código de autorización incorrecto');
                            }
                        }
                    }
                }
            });
        } else {
            bootbox.confirm(lang.r_u_sure, function (result) {
                if (result) {
                    if (localStorage.getItem('positems')) {
                        localStorage.removeItem('positems');
                    }
                    if (localStorage.getItem('posdiscount')) {
                        localStorage.removeItem('posdiscount');
                    }
                    if (localStorage.getItem('postax2')) {
                        localStorage.removeItem('postax2');
                    }
                    if (localStorage.getItem('posshipping')) {
                        localStorage.removeItem('posshipping');
                    }
                    if (localStorage.getItem('posref')) {
                        localStorage.removeItem('posref');
                    }
                    if (localStorage.getItem('poswarehouse')) {
                        localStorage.removeItem('poswarehouse');
                    }
                    if (localStorage.getItem('posnote')) {
                        localStorage.removeItem('posnote');
                    }
                    if (localStorage.getItem('posinnote')) {
                        localStorage.removeItem('posinnote');
                    }
                    if (localStorage.getItem('poscustomer')) {
                        localStorage.removeItem('poscustomer');
                    }
                    if (localStorage.getItem('poscurrency')) {
                        localStorage.removeItem('poscurrency');
                    }
                    if (localStorage.getItem('posdate')) {
                        localStorage.removeItem('posdate');
                    }
                    if (localStorage.getItem('posstatus')) {
                        localStorage.removeItem('posstatus');
                    }
                    if (localStorage.getItem('posbiller')) {
                        localStorage.removeItem('posbiller');
                    }

                    if (localStorage.getItem('retenciones')) {
                        localStorage.removeItem('retenciones');
                    }
                    if (localStorage.getItem('tip_amount')) {
                        localStorage.removeItem('tip_amount');
                    }
                    if (localStorage.getItem('delete_tip_amount')) {
                        localStorage.removeItem('delete_tip_amount');
                    }
                    if (localStorage.getItem('pos_wholesale_keep_prices')) {
                        localStorage.removeItem('pos_wholesale_keep_prices');
                    }

                    $('#modal-loading').show();
                    if (is_order) {
                        window.location.href = site.base_url+"sales/orders";
                    } else {
                        window.location.href = site.base_url+"pos/add_wholesale";
                    }
                }
            });
        }
    });

// save and load the fields in and/or from localStorage

$('#poswarehouse').change(function (e) {
    localStorage.setItem('poswarehouse', $(this).val());
});

if (poswarehouse = localStorage.getItem('poswarehouse')) {
    $('#poswarehouse').select2('val', poswarehouse);
}

    //$(document).on('change', '#posnote', function (e) {
        $('#posnote').redactor('destroy');
        $('#posnote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('posnote', v);
            }
        });
        if (posnote = localStorage.getItem('posnote')) {
            $('#posnote').redactor('set', posnote);
        }

        $('#poscustomer').change(function (e) {
            localStorage.setItem('poscustomer', $(this).val());
            validate_fe_customer_data();
        });


// prevent default action upon enter
$('body').not('textarea').bind('keypress', function (e) {
    if (e.keyCode == 13) {
        console.log('Presionaron enter');
        //e.preventDefault();
        //return false;
    }
});

// Order tax calculation
if (site.settings.tax2 != 0) {
    $('#postax2').change(function () {
        localStorage.setItem('postax2', $(this).val());
        loadItems();
        return;
    });
}

    // Order discount calculation
    var old_posdiscount;
    $('#posdiscount').focus(function () {
        old_posdiscount = $(this).val();
    }).change(function () {
        var new_discount = $(this).val() ? $(this).val() : '0';
        if (is_valid_discount(new_discount)) {
            localStorage.removeItem('posdiscount');
            localStorage.setItem('posdiscount', new_discount);
            loadItems();
            return;
        } else {
            $(this).val(old_posdiscount);
            bootbox.alert(lang.unexpected_value);
            return;
        }

    });

    /* ----------------------
     * Delete Row Method
     * ---------------------- */
     var pwacc = false;
     $(document).on('click', '.posdel', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        if(protect_delete == 1) {
            biller_pin_code = billers_data[$('#posbiller').val()].pin_code;
            var boxd = bootbox.dialog({
                title: "<i class='fa fa-key'></i> Código de autorización",
                message: '<input id="pos_pin" name="pos_pin" type="password" placeholder="Código de autorización" class="form-control"> ',
                buttons: {
                    success: {
                        label: "<i class='fa fa-tick'></i> OK",
                        className: "btn-success verify_pin",
                        callback: function () {
                            var pos_pin = md5($('#pos_pin').val());
                            if(pos_pin == biller_pin_code) {
                                delete positems[item_id];
                                row.remove();
                                if(positems.hasOwnProperty(item_id)) { } else {
                                    localStorage.setItem('positems', JSON.stringify(positems));
                        // $('#payment').prop('disabled', true);
                                    loadItems();
                                }
                            } else {
                                bootbox.alert('Código de autorización incorrecto');
                            }
                        }
                    }
                }
            });
            boxd.on("shown.bs.modal", function() {
                $( "#pos_pin" ).focus().keypress(function(e) {
                    if (e.keyCode == 13) {
                        e.preventDefault();
                        $('.verify_pin').trigger('click');
                        return false;
                    }
                });
            });
        } else {
            delete positems[item_id];
            row.remove();
            if(positems.hasOwnProperty(item_id)) { } else {
                localStorage.setItem('positems', JSON.stringify(positems));
    // $('#payment').prop('disabled', true);
                loadItems();
            }
        }
        return false;
     });

    /* -----------------------
     * Edit Row Modal Hanlder
     ----------------------- */
    $(document).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        item_id = row.attr('data-item-id');
        item = positems[item_id];
        var qty = row.children().children('.rquantity').val(),
        product_option = row.children().children('.roption').val(),
        unit_price = formatDecimal(item.row.real_unit_price),
        discount = row.children().children('.rdiscount').val();
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                    unit_price = parseFloat(item.row.real_unit_price)+parseFloat(this.price);
                }
            });
        }

        var real_unit_price = item.row.real_unit_price;
        var net_price = unit_price;
        $('#prModalLabel').text(item.row.code + ' - ' + item.row.name);
        $('#pname').val(item.row.name);
        if (site.settings.tax1) {
            $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax2').val(item.row.consumption_sale_tax);
            if (site.settings.allow_change_sale_iva == 0) {
                $('#ptax').select2('readonly', true);
            }
            $('#old_tax').val(item.row.tax_rate);

            var pr_tax = item.row.tax_rate, pr_tax_val = 0;
            if (pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {

                            if (positems[item_id].row.tax_method == 0) {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                net_price -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / 100));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }

                        } else if (this.type == 2) {

                            pr_tax_val = parseFloat(this.rate);
                            pr_tax_rate = this.rate;

                        }
                    }
                });
            }

            var item_discount = 0, ds = discount ? discount : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal(parseFloat(((net_price) * parseFloat(pds[0])) / 100));
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }
            net_price -= item_discount;
        }
        if (site.settings.product_serial !== 0) {
            $('#pserial').val(row.children().children('.rserial').val());
        }
        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.options !== false) {
            var o = 1;
            opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
            $.each(item.options, function () {
                if(o == 1) {
                    if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                }
                $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                o++;
            });
        } else {
            product_variant = 0;
        }
        var pref = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.preferences !== null && item.preferences !== undefined && item.preferences !== false) {
            pref = $("<select id=\"ppreferences\" name=\"ppreferences\" class=\"form-control select\" multiple />");
            $.each(item.preferences, function () {
                $("<option />", {value: this.id, text: "("+this.category_name+") "+this.name}).appendTo(pref);
            });
        }
        if (item.units !== false) {
            uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
            $.each(item.units, function () {
                if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                    if(this.product_unit_id == item.row.product_unit_id_selected) {
                        $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.product_unit_id, text: this.name}).appendTo(uopt);
                    }
                } else {
                    if(this.id == item.row.unit) {
                        $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                    }
                }
            });
        } else {
            uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        }

        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        $('#poptions-div').html(opt);
        $('#ppreferences-div').html(pref);
        if (item.preferences_selected !== undefined && item.preferences_selected !== null) {
            $('#ppreferences').val(item.preferences_selected);
        }
        $('#punits-div').html(uopt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#pquantity').val(qty);
        $('#old_qty').val(qty);
        $('#pprice').val(formatDecimals(unit_price));
        $('#pprice_ipoconsumo').val('');
        $('#punit_price').val(formatDecimal(parseFloat(unit_price)+parseFloat(pr_tax_val)+parseFloat(pr_tax_2_val)));
        $('#poption').select2('val', item.row.option);
        $('#old_price').val(unit_price);
        $('#row_id').val(row_id);
        $('#item_id').val(item_id);
        $('#pserial').val(row.children().children('.rserial').val());
        $('#pdiscount').val(discount);
        $('#net_price').text(formatMoney(net_price));
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val)+parseFloat(pr_tax_2_val)));
        $('#prModal').appendTo("body").modal('show');

        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            units = item.units;
            unit_selected = units[item.row.product_unit_id_selected];
            unit_selected_real_quantity = 1;
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty / unit_selected.operation_value;
                unit_selected_real_quantity = 1 * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty * unit_selected.operation_value;
                unit_selected_real_quantity = 1 / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            }
            $('#pquantity').val(unit_selected_quantity);
            $('#punit').select2('val', item.row.product_unit_id_selected).trigger('change');
            $('#pproduct_unit_price').val((unit_price + parseFloat(pr_tax_2_val)) * unit_selected_real_quantity);
            $('#pproduct_unit_price').trigger('change');
        }

        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
        $('#psubtotal').val('');
    });

    $(document).on('click', '.comment', function () {
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        item_id = row.attr('data-item-id');
        item = positems[item_id];
        $('#irow_id').val(row_id);
        $('#icomment').val(item.row.comment);
        $('#iordered').val(item.row.ordered);
        $('#iordered').select2('val', item.row.ordered);
        $('#cmModalLabel').text(item.row.code + ' - ' + item.row.name);
        $('#cmModal').appendTo("body").modal('show');
    });

    $(document).on('click', '#editComment', function () {
        var row = $('#' + $('#irow_id').val());
        var item_id = row.attr('data-item-id');
        positems[item_id].row.order = parseFloat($('#iorders').val()),
        positems[item_id].row.comment = $('#icomment').val() ? $('#icomment').val() : '';
        localStorage.setItem('positems', JSON.stringify(positems));
        $('#cmModal').modal('hide');
        loadItems();
        return;
    });

    $('#prModal').on('shown.bs.modal', function (e) {
        if($('#poption').select2('val') != '') {
            $('#poption').select2('val', product_variant);
            product_variant = 0;
        }
    });

    $(document).on('change', '#pprice, #pprice_ipoconsumo, #ptax, #pdiscount, #paddprice', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var unit_price = parseFloat($('#pprice').val());
        var pprice_ipoconsumo = parseFloat($('#pprice_ipoconsumo').val());
        var additional_price = parseFloat($('#paddprice').val());

        var item = positems[item_id];
        if (pprice_ipoconsumo > 0 && parseFloat(pprice_ipoconsumo) > parseFloat(item.row.consumption_sale_tax)) {
            pprice_ipoconsumo = pprice_ipoconsumo - parseFloat(item.row.consumption_sale_tax);
            var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
            if (item.row.tax_method == 1 && pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            unit_price = pprice_ipoconsumo / ((parseFloat(this.rate) / 100) + 1);
                        } else if (this.type == 2) {
                            unit_price = pprice_ipoconsumo - parseFloat(this.rate);
                        }
                    }
                });
            } else if (item.row.tax_method == 0) {
                unit_price = pprice_ipoconsumo;
            }
        }

        if (additional_price > 0) {
            $('#paddprice').val(0);
            unit_price += additional_price;
            $('#pprice').val(unit_price);
        }
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / 100);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }
        
        if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 1  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(unit_price));
        } else if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 0  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(pprice_ipoconsumo));
        } else if (parseFloat($('#pprice_ipoconsumo').val()) <= parseFloat(item.row.consumption_sale_tax)) {
            Command: toastr.error('Por favor ingrese un precio válido', 'Valores incorrectos', {onHidden : function(){}})
            $('#pprice_ipoconsumo').val(0);
        }

        $('#net_price').text(formatMoney(unit_price));
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax)));
    });
    $(document).on('change', '#punit', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }

        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 1) {
                valor = unit_selected.operation_value;
                var ipoconsumo_um = 0;
                if (unit_selected.operator == "*") {
                    valor =  parseFloat(unit_selected.valor_unitario) / parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) * parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  parseFloat(unit_selected.valor_unitario) * parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) / parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor);
                $('#pproduct_unit_price').val(parseFloat(unit_selected.valor_unitario) + parseFloat(ipoconsumo_um));
            } else {
                $('#pprice').val(unit_selected.valor_unitario);
                $('#pproduct_unit_price').val(unit_selected.valor_unitario);
            }
            $('#pprice').change();
        } else if(site.settings.prioridad_precios_producto == 11){

        } else {
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                        aprice = parseFloat(this.price);
                    }
                });
            }
            if(item.units && unit != positems[item_id].row.base_unit) {
                $.each(item.units, function(){
                    if (this.id == unit) {
                        base_quantity = unitToBaseQty($('#pquantity').val(), this);
                        $('#pprice').val(formatDecimal(((parseFloat(item.row.base_unit_price+aprice))*unitToBaseQty(1, this)))).change();
                    }
                });
            } else {
                $('#pprice').val(formatDecimal(item.row.base_unit_price+aprice)).change();
            } //¡¡¡¡ NO BORRAR !!!!
        }
        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
    });

    $(document).on('change', '#pproduct_unit_price', function(){
        p_unit_price = $(this).val();
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 1) {
                valor = unit_selected.operation_value;
                ipoconsumo_um = 0;
                if (unit_selected.operator == "*") {
                    valor =  (parseFloat(p_unit_price)) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  (parseFloat(p_unit_price)) * parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor - pr_tax_2_val);
            } else {
                $('#pprice').val(p_unit_price - pr_tax_2_val);
            }
            $('#pprice').change();
        }

    });

    $(document).on('click', '#calculate_unit_price', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        tax_rate = formatDecimal($('#ptax option:selected').data('taxrate')) / 100;
        tax_rate = tax_rate != 0 ? tax_rate + 1 : 1;
        var subtotal = parseFloat($('#psubtotal').val()),
        qty = parseFloat($('#pquantity').val());
        if (item.row.tax_method == 1) { tax_rate = 1; }
        $('#pprice').val(formatDecimal(((subtotal) * tax_rate))).change();
        return false;
    });
    /* -----------------------
     * Edit Row Method
     ----------------------- */
     $(document).on('click', '#editItem', function () {
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_2 = $('#ptax2').val(), new_pr_tax_rate = false, new_pr_tax_2_rate = false;
        if (new_pr_tax) {
            $.each(tax_rates, function () {
                if (this.id == new_pr_tax) {
                    new_pr_tax_rate = this;
                }
            });
        }
        var price = parseFloat($('#pprice').val());
        if(item.options !== false) {
            var opt = $('#poption').val();
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    price = price-parseFloat(this.price);
                }
            });
        }
        if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
            if(!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > price) {
                bootbox.alert(lang.unexpected_value);
                return false;
            }
        }
        if (!is_numeric($('#pquantity').val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var unit = $('#punit').val();
        var base_quantity = parseFloat($('#pquantity').val());
        if(unit != positems[item_id].row.base_unit) {
            $.each(positems[item_id].units, function(){
                if (this.id == unit) {
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        positems[item_id].row.fup = 1,
        positems[item_id].row.qty = parseFloat($('#pquantity').val()),
        positems[item_id].row.base_quantity = parseFloat(base_quantity),
        positems[item_id].row.real_unit_price = price / trmrate,
        positems[item_id].row.unit = unit,
        positems[item_id].row.tax_rate = new_pr_tax,
        positems[item_id].tax_rate = new_pr_tax_rate,
        positems[item_id].row.discount = $('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '',
        positems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '',
        positems[item_id].row.serial = $('#pserial').val();
        positems[item_id].row.name = $('#pname').val();
        prev_product_unit_id = positems[item_id].row.product_unit_id;
        positems[item_id].row.product_unit_id = $('#punit').val();
        positems[item_id].row.product_unit_id_selected = $('#punit').val();

        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            var ditem = positems[item_id];
            unit_selected = ditem.units[unit];
            qty = parseFloat($('#pquantity').val());
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            }
            positems[item_id].row.qty = unit_selected_quantity;
        } else {
            positems[item_id].row.unit_price_id = $('#punit').val();
        }
        if (site.settings.prioridad_precios_producto == 11 && prev_product_unit_id !== undefined && prev_product_unit_id != positems[item_id].row.product_unit_id) {
            wappsiPreciosUnidad(positems[item_id].row.id,item_id,$('#pquantity').val(), true);
        }

        var preferences_text = '';
        var preferences_selected = [];

        $('#ppreferences option:selected').each(function(index, option){
            preferences_selected.push($(option).val());
            preferences_text += $(option).text() + (index < $('#ppreferences option:selected').length - 1 ? ", " : "");
        });

        if (preferences_text != '') {
            positems[item_id].preferences_text = preferences_text;
            positems[item_id].preferences_selected = preferences_selected;
        }
        
        localStorage.setItem('positems', JSON.stringify(positems));
        $('#prModal').modal('hide');

        loadItems();
        return;
    });

    /* -----------------------
     * Product option change
     ----------------------- */
    $(document).on('change', '#poption', function () {
        var row = $('#' + $('#row_id').val()), opt = $(this).val();
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        var unit = $('#punit').val(), base_quantity = parseFloat($('#pquantity').val()), base_unit_price = item.row.base_unit_price;
        if(unit != positems[item_id].row.base_unit) {
            $.each(positems[item_id].units, function(){
                if (this.id == unit) {
                    base_unit_price = formatDecimal((parseFloat(item.row.base_unit_price)*(unitToBaseQty(1, this))))
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        $('#pprice').val(parseFloat(base_unit_price)).trigger('change');
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    $('#pprice').val(parseFloat(base_unit_price)+(parseFloat(this.price))).trigger('change');
                }
            });
        }
    });

     /* ------------------------------
     * Sell Gift Card modal
     ------------------------------- */
     $(document).on('click', '#sellGiftCard', function (e) {
        if (count == 1) {
            positems = {};
            if ($('#poswarehouse').val() && $('#poscustomer').val()) {
                $('#poscustomer').select2("readonly", true);
                $('#poswarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('.gcerror-con').hide();
        $('#gcModal').appendTo("body").modal('show');
        return false;
     });

     $('#gccustomer').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url+"customers/suggestionsws",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if(data.results != null) {
                    return { results: data.results };
                } else {
                    return { results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
     });

     $('#genNo').click(function(){
        var no = generateCardNo();
        $(this).parent().parent('.input-group').children('input').val(no);
        return false;
     });
     $('.date').datetimepicker({format: site.dateFormats.js_sdate, fontAwesome: true, language: 'sma', todayBtn: 1, autoclose: 1, minView: 2 });
     $(document).on('click', '#addGiftCard', function (e) {
        var mid = (new Date).getTime(),
        gccode = $('#gccard_no').val(),
        gcname = $('#gcname').val(),
        gcvalue = $('#gcvalue').val(),
        gccustomer = $('#gccustomer').val(),
        gcexpiry = $('#gcexpiry').val() ? $('#gcexpiry').val() : '',
        gcprice = parseFloat($('#gcprice').val());
        if(gccode == '' || gcvalue == '' || gcprice == '' || gcvalue == 0 || gcprice == 0) {
            $('#gcerror').text('Please fill the required fields');
            $('.gcerror-con').show();
            return false;
        }

        var gc_data = new Array();
        gc_data[0] = gccode;
        gc_data[1] = gcvalue;
        gc_data[2] = gccustomer;
        gc_data[3] = gcexpiry;
        //if (typeof positems === "undefined") {
        //    var positems = {};
        //}

        $.ajax({
            type: 'get',
            url: site.base_url+'sales/sell_gift_card',
            dataType: "json",
            data: { gcdata: gc_data },
            success: function (data) {
                if(data.result === 'success') {
                    positems[mid] = {"id": mid, "item_id": mid, "label": gcname + ' (' + gccode + ')', "row": {"id": mid, "code": gccode, "name": gcname, "quantity": 1, "base_quantity": 1, "price": gcprice, "real_unit_price": gcprice, "tax_rate": "1", "qty": 1, "type": "manual", "discount": "0", "serial": "", "option":""}, "tax_rate": {"id":"1","name":"Excento","code":"IVA0","rate":"0","type":"1"}, "options":false, "units":false};
                    localStorage.setItem('positems', JSON.stringify(positems));
        // $('#payment').prop('disabled', true);
                    loadItems();
                    $('#gcModal').modal('hide');
                    $('#gccard_no').val('');
                    $('#gcvalue').val('');
                    $('#gcexpiry').val('');
                    $('#gcprice').val('');
                } else {
                    $('#gcerror').text(data.message);
                    $('.gcerror-con').show();
                }
            }
        });
        return false;
    });

    /* ------------------------------
     * Show manual item addition modal
     ------------------------------- */
     $(document).on('click', '#addManually', function (e) {
        if (count == 1) {
            positems = {};
            if ($('#poswarehouse').val() && $('#poscustomer').val()) {
                $('#poscustomer').select2("readonly", true);
                $('#poswarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#mnet_price').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal').appendTo("body").modal('show');
        return false;
    });

     $(document).on('click', '#addItemManually', function (e) {
        var mid = (new Date).getTime(),
        mcode = $('#mcode').val(),
        mname = $('#mname').val(),
        mtax = parseInt($('#mtax').val()),
        mqty = parseFloat($('#mquantity').val()),
        mdiscount = $('#mdiscount').val() ? $('#mdiscount').val() : '0',
        unit_price = parseFloat($('#mprice').val()),
        mtax_rate = {};
        if (mcode && mname && mqty && unit_price) {
            $.each(tax_rates, function () {
                if (this.id == mtax) {
                    mtax_rate = this;
                }
            });

            positems[mid] = {"id": mid, "item_id": mid, "label": mname + ' (' + mcode + ')', "row": {"id": mid, "code": mcode, "name": mname, "quantity": mqty, "base_quantity": mqty, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": mtax, "tax_method": 0, "qty": mqty, "type": "manual", "discount": mdiscount, "serial": "", "option":""}, "tax_rate": mtax_rate, 'units': false, "options":false};
            localStorage.setItem('positems', JSON.stringify(positems));
// $('#payment').prop('disabled', true);
            loadItems();
        }
        $('#mModal').modal('hide');
        $('#mcode').val('');
        $('#mname').val('');
        $('#mtax').val('');
        $('#mquantity').val('');
        $('#mdiscount').val('');
        $('#mprice').val('');
        return false;
    });

    $(document).on('change', '#mprice, #mtax, #mdiscount', function () {
        var unit_price = parseFloat($('#mprice').val());
        var ds = $('#mdiscount').val() ? $('#mdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#mtax').val(), item_tax_method = 0;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / 100);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        $('#mnet_price').text(formatMoney(unit_price));
        $('#mpro_tax').text(formatMoney(pr_tax_val));
    });

    /* --------------------------
     * Edit Row Quantity Method
    --------------------------- */
    var old_row_qty;
    var controladorTiempo = "";

    $(document).on("focus", '.rquantity', function () {
      old_row_qty = $(this).val();
    }).on('keypress', '.rquantity', function(e){
        var elemento = this;
        if(document.all)
          tecla=event.keyCode;
        else
        {
          tecla=e.which;
        }
        if (tecla == 13) {
            localStorage.setItem('enter_pressed_for_quantity', true);
            setTimeout(function() {
                localStorage.removeItem('enter_pressed_for_quantity');
            }, 2000);
            clearTimeout(controladorTiempo);
            load_new_quantity(elemento);
            e.preventDefault();
            return false;
        } else {
          clearTimeout(controladorTiempo);
          $('#payment').prop('disabled', true);
          controladorTiempo = setTimeout(function(){
            load_new_quantity(elemento);
            return false;
          }, (parseFloat(pos_settings.time_focus_quantity) * 1000));
        }
    });


// end ready function
});

function load_new_quantity(elemento){
    var row = $(elemento).closest('tr');
    if (!is_numeric($(elemento).val()) || parseFloat($(elemento).val()) < 0) {
      $(this).val(old_row_qty);
      bootbox.alert(lang.unexpected_value);
      return;
    }
    var new_qty = formatDecimal($(elemento).val()),
    item_id = row.attr('data-item-id');
    positems[item_id].row.base_quantity = new_qty;
    if(positems[item_id].row.unit != positems[item_id].row.base_unit) {
      $.each(positems[item_id].units, function(){
        if (elemento.id == positems[item_id].row.unit) {
          positems[item_id].row.base_quantity = unitToBaseQty(new_qty, elemento);
        }
      });
    }

    //Wappsi precio
    // Cuando llama al loadItems parece que hace las multiplicaciones del caso
    wappsiPreciosUnidad(positems[item_id].row.id,item_id,new_qty, true);
}


// Wappsi Function
// La siguiente función es llamada cada vez que se hace un cambio de cantidades la idea es
// usar el ajax para buscar en una tabla creada por nosotros los precios por unidades
// este js es llamado desde la vista add que corresponde a POS
function wappsiPreciosUnidad(id,item_id,new_qty, load_items = false) {
    // if (site.settings.prioridad_precios_producto != 7 && site.settings.prioridad_precios_producto != 10) {
        $('#payment').prop('disabled', true);
        var pos_keep_prices = localStorage.getItem('pos_keep_prices');

        if (pos_keep_prices == 'true' || positems[item_id].row.fup == true) {
            positems[item_id].row.qty = new_qty;
            localStorage.setItem('price_updated', true);
            localStorage.setItem('positems', JSON.stringify(positems));
            loadItems();
            return false;
        } else {
            var customer = $('#poscustomer').val();
            var biller_price_group = $('#posbiller option:selected').data('pricegroupdefault');
            var customer = $('#poscustomer').val();
            var address_id = $('#poscustomer_branch').val();
            var biller = $('#posbiller').val();
            var unit_price_id = null;

            if (site.settings.precios_por_unidad_presentacion == 2 || site.settings.prioridad_precios_producto == 11) {
                if (item_selected = positems[item_id]) {
                    unit_price_id = item_selected.row.product_unit_id_selected;
                }
            }

            var datos = {
                            id : id,
                            new_qty : new_qty,
                            price_group : biller_price_group,
                            customer : customer,
                            address_id : address_id,
                            biller : biller,
                            prioridad : site.settings.prioridad_precios_producto,
                            unit_price_id : unit_price_id
                        };

            $.ajax({
                type: 'get',
                url: site.base_url+"sales/preciosUnidad",
                dataType: "json",
                data: datos,
                success: function (data) {
                    $(this).removeClass('ui-autocomplete-loading');

                    if (data !== null && data[0] !== null && data[0] != 0) {
                        var valor = data[0];

                        if (positems[item_id]) {
                            positems[item_id].row.real_unit_price = valor;
                            if (is_suspend_sale) {
                                positems[item_id].row.unit_price = valor;
                            }
                            if (site.settings.precios_por_unidad_presentacion == 2) {
                                positems[item_id].row.unit_price_quantity = data[1];
                                if (data[1] !== undefined) {
                                    positems[item_id].row.real_unit_price = valor / data[1];
                                }
                            }
                            if (data[2] !== undefined) {
                                positems[item_id].row.discount = data[2];
                            }
                            if (data[3] !== undefined) {
                                positems[item_id].row.tax_rate = data[3];
                                positems[item_id].tax_rate = data[4];
                                positems[item_id].row.tax_exempt_customer = true;
                            }
                        }
                    } else {
                        positems[item_id].row.real_unit_price = positems[item_id].row.real_unit_price;
                        if (is_suspend_sale) {
                            positems[item_id].row.unit_price = positems[item_id].row.real_unit_price;
                        }
                    }
                    positems[item_id].row.qty = new_qty;

                    localStorage.setItem('positems', JSON.stringify(positems));
                    $('#payment').prop('disabled', true);

                    updated_items++;
                    if (updated_items == Object.keys(positems).length) {
                        updated_items = 0;
                        loadItems();
                        if (prices_checked == false) {
                            check_prices();
                            prices_checked = true;
                        }
                    } else if (load_items == true) {
                        updated_items = 0;
                        loadItems();
                    }
                }
            });
        }
    // }
}
// Termina wappsiPreciosUnidad

/* -----------------------
* aca load
 * Load all items
 ----------------------- */

function check_prices() {
    positems = JSON.parse(localStorage.getItem('positems'));
    sortedItems = positems;
    var pos_wholesale_keep_prices = localStorage.getItem('pos_wholesale_keep_prices');
    if (!pos_wholesale_keep_prices && is_quote) {
        var prices_changed = false;
        var prices_changed_html = '';
        $.each(sortedItems, function () {;
            var item = this;
            var actual_price = item.row.real_unit_price;
            var item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            
            var key_log = localStorage.getItem('poskeylog');
            var poscustomerspecialdiscount = localStorage.getItem('poscustomerspecialdiscount');

            if (key_log == 1 && poscustomerspecialdiscount == 1) {
                item_price = item.row.real_unit_price;
                if (item_tax_method == 1) {
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 0))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                } else {
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                }
                actual_price = item_price;
            }
            if (actual_price != parseFloat(item.row.prev_unit_price)) {
                prices_changed = true;
                prices_changed_html += '<tr>'+
                                            '<td>'+item_name+'</td>'+
                                            '<td class="text-right">'+formatMoney(item.row.prev_unit_price)+'</td>'+
                                            '<td class="text-right">'+formatMoney(actual_price)+'</td>'+
                                       '</tr>';
            }
        });
        if (prices_changed) {
            $('#prices_changed_table').html(prices_changed_html);
            $('#pricesChanged').modal('show');
            return false;
        }
    }
}

function loadItems() {
    if (localStorage.getItem('positems')) {
        total = 0;
        total_after_tax = 0;
        count = 1;
        an = 1;
        product_tax = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        total_discount = 0;
        order_data = {};
        bill_data = {};
        total_tax = 0;
        tip_amount = 0;

        var key_log = localStorage.getItem('poskeylog');
        var poscustomerspecialdiscount = localStorage.getItem('poscustomerspecialdiscount');
        var pos_wholesale_keep_prices = JSON.parse(localStorage.getItem('pos_wholesale_keep_prices'));
        var price_updated = JSON.parse(localStorage.getItem('price_updated'));
        var actual_price = false;
        if (pos_wholesale_keep_prices && price_updated) {
            actual_price = true;
        }

        $("#posTable tbody").empty();
        var time = ((new Date).getTime())/1000;
        if (pos_settings.remote_printing != 1) {
            store_name = (biller && biller.company != '-' ? biller.company : biller.name);
            order_data.store_name = store_name;
            bill_data.store_name = store_name;
            order_data.header = "\n"+lang.order+"\n\n";
            bill_data.header = "\n"+lang.bill+"\n\n";

            var pos_customer = 'C: '+$('#select2-chosen-1').text()+ "\n";
            var hr = 'R: '+$('#reference_note').val()+ "\n";
            var user = 'U: '+username+ "\n";
            var pos_curr_time = 'T: '+date(site.dateFormats.php_ldate, time)+ "\n";
            var ob_info = pos_customer+hr+user+pos_curr_time+ "\n";
            order_data.info = ob_info;
            bill_data.info = ob_info;
            var o_items = '';
            var b_items = '';

        } else {
            $("#order_span").empty(); $("#bill_span").empty();
            var styles = '<style>table, th, td { border-collapse:collapse; border-bottom: 1px solid #CCC; } .no-border { border: 0; } .bold { font-weight: bold; }</style>';
            var pos_head1 = '<span style="text-align:center;"><h3>'+site.settings.site_name+'</h3><h4>';
            var pos_head2 = '</h4><p class="text-left">C: '+$('#select2-chosen-1').text()+'<br>R: '+$('#reference_note').val()+'<br>U: '+username+'<br>T: '+date(site.dateFormats.php_ldate, time)+'</p></span>';
            $("#order_span").prepend(styles + pos_head1+' '+lang.order+' '+pos_head2);
            $("#bill_span").prepend(styles + pos_head1+' '+lang.bill+' '+pos_head2);
            $("#order-table").empty(); $("#bill-table").empty();
        }
        positems = JSON.parse(localStorage.getItem('positems'));

        if (pos_settings.item_order == 1) {
            sortedItems = _.sortBy(positems, function(o) { return [parseInt(o.category.id), parseInt(o.order)]; });
        } else {
            sortedItems = _.sortBy(positems, function(o) { return [parseInt(o.order)]; });
            if (site.settings.product_order == 2) {
                sortedItems.reverse();
            }
        }
        var category = 0, print_cate = false;

        lock_submit = false;
        var except_category_taxes = false;
        var tax_exempt_customer = false;
        var diferent_tax_alert = false;

        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            positems[item_id] = item;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id;
            var item_type = item.row.type;
            var combo_items = item.combo_items;
            var item_price = item.row.price;
            var item_qty = item.row.qty;
            var item_aqty = item.row.quantity;
            var item_tax_method = item.row.tax_method;
            var item_ds = item.row.discount;
            var item_discount = 0;
            var item_option = item.row.option;
            var item_code = item.row.code;
            var item_serial = item.row.serial;
            var item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var item_state_readiness = (item.state_readiness === undefined ? '': item.state_readiness);
            if (item.prev_state_readiness === undefined) {
                item.prev_state_readiness = item.state_readiness;
            }
            var item_state_readiness_prev = (item.state_readiness === undefined ? '': item.state_readiness);
            var product_unit = item.row.product_unit_id_selected;
            var base_quantity = item.row.base_quantity;
            var unit_price = actual_price ? item.row.unit_price : item.row.real_unit_price;
            // if (is_suspend_sale) {
            //     unit_price = item.row.unit_price;
            //     if (localStorage.getItem('diferent_tax_alert_fixed') && item.row.diferent_tax_alert) {
            //         unit_price = item.row.real_unit_price;
            //     }
            // }
            var item_comment = item.row.comment ? item.row.comment : '';
            var item_ordered = item.row.ordered ? item.row.ordered : 0;
            var ordered_product_id = item.ordered_product_id;
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                        item_price = parseFloat(unit_price)+(parseFloat(this.price));
                        unit_price = item_price;
                    }
                });
            }
            if (item.row.except_category_taxes) {
                except_category_taxes = true;
            }
            if (item.row.tax_exempt_customer && site.settings.enable_customer_tax_exemption == 1) {
                tax_exempt_customer = true;
            }
            if (item.row.diferent_tax_alert) {
                diferent_tax_alert = true;
            }
            pr_t = {id : site.except_tax_rate_id, rate : 0};
            var pr_tax = item.tax_rate ? item.tax_rate : pr_t;
            var pr_tax_val = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_price, item_tax_method))) {
                pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
            }
            item_price = item_tax_method == 1 ? unit_price : unit_price - (pr_tax_val);
            var ds = item_ds ? item_ds : '0';
            item_discount = calculateDiscount(ds, item_price, pr_tax, item_tax_method);
            item_price -= item_discount; //precio sin IVA - Descuento
            if (item_discount > 0) {
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 1))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
            }
            if (pr_tax.type == 2 && item_price < 0) {
                item_price = 0;
            }
            unit_price = formatDecimal((unit_price+item_discount));
            var sel_opt = '';
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.name;
                }
            });
            if (key_log == 1 && poscustomerspecialdiscount == 1) {
                item_price = item.row.real_unit_price;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, (item_tax_method == 1 ? 0 : item_tax_method)))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                item_price -= pr_tax_val;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                if (item_tax_method == 0) {
                    item_price -= pr_tax_val;
                }
            } else if (key_log == 2) {
                item_price = item.row.real_unit_price;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (item_tax_method == 1) {
                    var pr_tax_val = 0;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 1))) {
                        pr_tax_val = ptax[0];
                    }
                    item_price = parseFloat(formatDecimal(item_price)) + parseFloat(formatDecimal(pr_tax_val));
                }
                pr_tax_rate = 0;
                pr_tax_val = 0;
                pr_tax_2_rate = 0;
                product_tax = 0;
                product_tax_2 = 0;
                unit_price = item.row.real_unit_price;
            } else if (key_log == 3) {
                item_price = item.row.real_unit_price;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                if (item_tax_method != 1) {
                    item_price -= pr_tax_val;
                }
                pr_tax_rate = 0;
                pr_tax_val = 0;
                pr_tax_2_rate = 0;
                product_tax = 0;
                product_tax_2 = 0;
                unit_price = item.row.real_unit_price;
            }
            pr_tax_2_val = item.row.consumption_sale_tax;
            pr_tax_2_rate = item.row.consumption_sale_tax;
            if (pos_settings.item_order == 1 && category != item.row.category_id) {
                category = item.row.category_id;
                print_cate = true;
                var newTh = $('<tr></tr>');
                newTh.html('<td colspan="100%"><strong>'+item.row.category_name+'</strong></td>');
                newTh.appendTo("#posTable");
            } else {
                print_cate = false;
            }

            cancelled_class = ''; //SE DEFINE VARIABLE POR ERROR DE UNDEFINED

            if (localStorage.getItem('restobar_mode') == '1') {
                cancelled_class = (item.state_readiness == 3) ? 'line-through' : (item.state_readiness == 2) ? 'success': '';
            }

            var msg_error = false;
            /* profitability_margin */
            if (site.settings.profitability_margin_validation > 0) {
                var margin_cost = item.row.cost;
                var margin_price = parseFloat(item_price) + parseFloat(pr_tax_val);
                var tax_method = item_tax_method;
                var margin_tax_rate = item.tax_rate.id;

                var margin = 0;

                if (margin_cost > 0 && margin_price > 0 && margin_tax_rate > 0 && tax_method == 0) {
                    var margin_pr_tax = tax_rates[margin_tax_rate];
                    margin_pr_tax_val = 0;
                    if (ptax = calculateTax(margin_pr_tax, margin_price, tax_method)) {
                        margin_pr_tax_val = ptax[0];
                    }
                    var margin_net_price = margin_price - margin_pr_tax_val;
                    if (ptax = calculateTax(margin_pr_tax, margin_cost, tax_method)) {
                        margin_pr_tax_val = ptax[0];
                    }
                    var margin_net_cost = margin_cost - margin_pr_tax_val;
                } else {
                    var margin_net_price = margin_price;
                    var margin_net_cost = margin_cost;
                }
                if (margin_net_price > 0 && margin_net_cost > 0) {

                    if (site.settings.profitability_margin_validation == 1 && item.category !== undefined) {
                        setted_margin = item.category.profitability_margin;
                    } else if(site.settings.profitability_margin_validation == 2 && item.subcategory !== undefined) {
                        setted_margin = item.subcategory.profitability_margin;
                    }

                    if (setted_margin == 0) {
                        if (item.subcategory !== undefined && item.subcategory.profitability_margin > 0) {
                            setted_margin = item.subcategory.profitability_margin;
                        } else if (item.category !== undefined && item.category.profitability_margin > 0) {
                            setted_margin = item.category.profitability_margin;
                        }
                    }

                    if (setted_margin > 0) {
                        margin = formatDecimals((((margin_net_price - margin_net_cost) / margin_net_price) * 100), 2);
                        if (parseFloat(margin) <= parseFloat(setted_margin)) {
                            lock_submit = true;
                            msg_error = 'El margen mínimo establecido para el producto es de '+formatDecimals(setted_margin)+'%, margen actual '+margin+'%;';
                        }
                    }
                }

                if (margin_price < margin_cost) {
                    lock_submit = true;
                    if (msg_error !== false) {
                        msg_error += '\n El precio del producto es menor al costo, el costo es de '+formatMoney(margin_cost)+';';
                    } else {
                        msg_error = 'El precio del producto es menor al costo, el costo es de '+formatMoney(margin_cost)+';';
                    }
                }
            }

            /* profitability_margin */

            if (item.row.ignore_hide_parameters == 0 && site.settings.invoice_values_greater_than_zero == 1 && formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty))) == 0) {
                lock_submit = true;
                if (msg_error !== false) {
                    msg_error += 'El total de producto no puede ser igual a 0';
                } else {
                    msg_error = 'El total de producto no puede ser igual a 0';
                }
            }


            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + ' '+ cancelled_class +'" data-item-id="' + item_id + '"></tr>');


            tr_html = '<td class="text-center text-danger'+ cancelled_class +' posdel" id="' + row_no + '" title="Remove"  style="cursor:pointer;">'+
                            '<i class="fa fa-trash fa-lg tip pointer"></i>'+
                        '</td>';
            tr_html += '<td class="'+ cancelled_class +'">'+
                        '<input name="product_ordered_product_id[]" type="hidden" value="'+ ordered_product_id +'">'+
                        '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                        '<input name="ignore_hide_parameters[]" type="hidden" class="ignore_hide_parameters" value="' + item.row.ignore_hide_parameters + '">'+
                        '<input name="product_type[]" type="hidden" class="rtype" value="'+ item_type +'">'+
                        '<input name="product_code[]" type="hidden" class="rcode" value="'+ item_code +'">'+
                        '<input name="product_name[]" type="hidden" class="rname" value="'+ item_name +'">'+
                        '<input name="product_is_new[]" type="hidden" class="is_new" value="'+ (item.is_new !== undefined || item.prev_state_readiness !== undefined && item.prev_state_readiness != item_state_readiness ? true : false) +'">'+
                        '<input name="product_option[]" type="hidden" class="roption" value="'+ item_option +'">'+
                        '<input name="product_comment[]" type="hidden" class="rcomment" value="'+ item_comment +'">'+
                        '<input name="state_readiness[]" type="hidden" class="rstate_readiness" id="state_readiness'+ item_id +'" value="'+ item_state_readiness +'" />'+
                        '<span class="sname" id="name_'+ row_no +'">'+ item_code +' - '+ item_name + (sel_opt != '' ? ' ('+sel_opt+')' : '') +'</span>'+
                        '<span class="lb"></span>'+
                        '<i class="pull-right fa fa-edit fa-bx tip pointer edit" id="'+ row_no +'" data-item="'+ item_id +'" title="Edit" style="cursor:pointer;"></i>'+
                        '<i class="pull-right fa fa-comment fa-bx'+ (item_comment != '' ? '' :'-o') +' tip pointer comment" id="'+ row_no +'" data-item="'+ item_id +'" title="Comment" style="cursor:pointer;margin-right:5px;"></i>'+
                    '</td>';

            if (msg_error == false) {
                tr_html += '<td class="text-right '+ cancelled_class +'">';
            } else {
                lock_submit = true;
                tr_html += '<td class="text-right text-danger danger bold '+ cancelled_class +'" data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+msg_error+'">';
            }
            if (site.settings.product_serial == 1) {
                tr_html += '<input class="form-control input-sm rserial" name="serial[]" type="hidden" id="serial_' + row_no + '" value="'+item_serial+'">';
            }

            if (site.settings.product_discount == 1) {
                tr_html += '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '">'+
                                '<input class="form-control input-sm" name="product_discount_val[]" type="hidden" value="' + (item_discount * item_qty) + '">';
            }

            if (site.settings.tax1 == 1) {
                tr_html += '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax.id : site.except_tax_rate_id) + '">'+
                            '<input type="hidden" class="sproduct_tax" id="sproduct_tax_' + row_no + '" value="' + formatMoney(pr_tax_val * item_qty) + '">'+
                            '<input class="form-control input-sm text-right" name="product_tax_rate[]" type="hidden" value="' + (key_log == '1' ? pr_tax_rate : 1) + '">'+
                            '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax_val : 0) + '">'+
                            '<input class="form-control input-sm text-right" name="product_tax_val[]" type="hidden" value="' + (key_log == '1' ? pr_tax_val : 0) + '">';
                tr_html += '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_2[]" type="hidden" id="product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_rate : 1) + '">'+
                            '<input type="hidden" class="sproduct_tax" id="sproduct_tax_' + row_no + '_2" value="' + formatMoney(pr_tax_2_val * item_qty) + '">'+
                            '<input class="form-control input-sm text-right" name="product_tax_rate_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_rate : 1) + '">'+
                            '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_val : 0) + '">'+
                            '<input class="form-control input-sm text-right" name="product_tax_val_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_val : 0) + '">';
            }

            tr_html += '<input class="rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '">'+
                        '<input class="ruprice" name="unit_price[]" type="hidden" value="' + unit_price + '">'+
                        '<input class="realuprice" name="real_unit_price[]" type="hidden" value="' + item.row.real_unit_price + '">'+
                        '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) + '</span>'+
                    '</td>';
            tr_html += '<td class="'+ cancelled_class +'">'+
                            '<input class="form-control input-sm kb-pad text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatQuantity2(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                            '<input name="product_unit_id_selected[]" type="hidden" value="' + item.row.product_unit_id_selected + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + item_qty + '">'+
                            '<input name="product_aqty[]" type="hidden" class="item_aqty" value="' + item_aqty + '">'+
                        '</td>';

            tr_html += '<td class="text-right '+ cancelled_class +'">'+
                            '<span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty))) +'</span>'+
                        '</td>';

            newTr.html(tr_html);

            if (item.preferences_text !== null && item.preferences_text !== undefined) {
                var newTr2 = $('<tr class="tr_preferences"></tr>');
                tr_html = '<td colspan="5">'+
                            '<input type="hidden" name="product_preferences_text['+product_id+']" value="'+item.preferences_text+'">' +
                            item.preferences_text +
                          '</td>';
                newTr2.html(tr_html);
                // if (pos_settings.item_order == 1) {
                    newTr2.appendTo("#posTable");
                // } else {
                    // newTr2.prependTo("#posTable");
                // }
            }

            // if (pos_settings.item_order == 1) {
                newTr.appendTo("#posTable");
            // } else {
                // newTr.prependTo("#posTable");
            // }


            if (localStorage.getItem('restobar_mode') == '1') {
                if (item.state_readiness != 3) {
                    total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
                    total_after_tax += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
                    total_tax += ((parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty));
                    count += parseFloat(item_qty);
                }
            } else {
                total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val)+ parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
                total_after_tax += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
                total_tax += ((parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty));
                count += parseFloat(item_qty);
            }
            an++;

            if (item_type == 'standard' && item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item_option && base_quantity > this.quantity) {
                        $('#row_' + row_no).addClass('danger');
                    }
                });
            } else if(item_type == 'standard' && item_qty > item_aqty) {
                $('#row_' + row_no).addClass('danger');
            } else if (item_type == 'combo') {
                if(combo_items === false) {
                    $('#row_' + row_no).addClass('danger');
                } else {
                    $.each(combo_items, function(){
                        if(parseFloat(this.quantity) < (parseFloat(this.qty)*base_quantity) && this.type == 'standard') {
                            $('#row_' + row_no).addClass('danger');
                        }
                    });
                }
            }

            var comments = item_comment.split(/\r?\n/g);
            if (pos_settings.remote_printing != 1) {

                b_items += product_name("#"+(an-1)+" "+ item_code + " - " + item_name) + "\n";
                for (var i = 0, len = comments.length; i < len; i++) {
                    b_items += (comments[i].length > 0 ? "   * "+comments[i]+"\n" : "");
                }
                b_items += printLine("   "+formatDecimal(item_qty) + " x " + formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val))+": "+ formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty)))) + "\n";
                o_items += printLine(product_name("#"+(an-1)+" "+ item_code + " - " + item_name) + ": [ "+ (item_ordered != 0 ? 'xxxx' : formatDecimal(item_qty))) + " ]\n";
                for (var i = 0, len = comments.length; i < len; i++) {
                    o_items += (comments[i].length > 0 ? "   * "+comments[i]+"\n" : "");
                }
                o_items += "\n";

            } else {
                if (pos_settings.item_order == 1 && print_cate) {
                    var bprTh = $('<tr></tr>');
                    bprTh.html('<td colspan="100%" class="no-border"><strong>'+item.row.category_name+'</strong></td>');
                    var oprTh = $('<tr></tr>');
                    oprTh.html('<td colspan="100%" class="no-border"><strong>'+item.row.category_name+'</strong></td>');
                    $("#order-table").append(oprTh);
                    $("#bill-table").append(bprTh);
                }
                var bprTr = '<tr class="row_' + item_id + '" data-item-id="' + item_id + '"><td colspan="2" class="no-border">#'+(an-1)+' '+ item_code + " - " + item_name + '';
                for (var i = 0, len = comments.length; i < len; i++) {
                    bprTr += (comments[i] ? '<br> <b>*</b> <small>'+comments[i]+'</small>' : '');
                }
                bprTr += '</td></tr>';
                bprTr += '<tr class="row_' + item_id + '" data-item-id="' + item_id + '"><td>(' + formatDecimal(item_qty) + ' x ' + (item_discount != 0 ? '<del>'+formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val) + item_discount)+'</del>' : '') + formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val))+ ')</td><td style="text-align:right;">'+ formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty))) +'</td></tr>';
                var oprTr = '<tr class="row_' + item_id + '" data-item-id="' + item_id + '"><td>#'+(an-1)+' ' + item_code + " - " + item_name + '';
                for (var i = 0, len = comments.length; i < len; i++) {
                    oprTr += (comments[i] ? '<br> <b>*</b> <small>'+comments[i]+'</small>' : '');
                }
                oprTr += '</td><td>[ ' + (item_ordered != 0 ? 'xxxx' : formatDecimal(item_qty)) +' ]</td></tr>';
                $("#order-table").append(oprTr);
                $("#bill-table").append(bprTr);
            }

            positems[item_id].end_price = formatDecimal(item_price) + formatDecimal(pr_tax_val);
            positems[item_id].end_tax = formatDecimal(pr_tax_val);
            positems[item_id].end_discount = formatDecimal(item_discount);

        });

        localStorage.setItem('positems', JSON.stringify(positems));

        //END EACH ITEMS

        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (postax2 = localStorage.getItem('postax2')) {
                $.each(tax_rates, function () {
                    if (this.id == postax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        }
                        if (this.type == 1) {
                            invoice_tax = formatDecimal((((total_after_tax - order_discount) * this.rate) / 100));
                        }
                    }
                });
            }
        }

        total = formatDecimal(total);
        product_tax = formatDecimal(product_tax);
        total_discount = formatDecimal(order_discount + product_discount);
        shipping = formatDecimal(localStorage.getItem('posshipping'));
        if (pos_settings.home_delivery_in_invoice == 0  && (restobar_mode_module == false || (restobar_mode_module != false && restobar_mode_module == 2))) {
            var setted_shipping = shipping;
            shipping = 0;
        }

        // Totals calculations after item addition
        subtotalwtax = total - total_tax;

        // Order level discount calculations
        if (posdiscount = localStorage.getItem('posdiscount')) {
            var ds = posdiscount;
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    order_discount = formatDecimal((parseFloat(((subtotalwtax) * parseFloat(pds[0])) / 100)));
                } else {
                    order_discount = parseFloat(ds);
                }
            } else {
                order_discount = parseFloat(ds);
            }
        }

        var gtotal = parseFloat(((total + invoice_tax) - order_discount) + parseFloat(shipping));
        // console.log('gtotal '+gtotal);
        $('#subtotal').text(formatMoney(subtotalwtax));
        $('.product_number').text("("+(an-1)+")");

        $('#ivaamount').text(formatMoney(total_tax));
        $('#total').text(formatMoney(total));
        $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
        $('#total_items').val((parseFloat(count) - 1));
        $('#tds').text(formatMoney(product_discount+order_discount));
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(invoice_tax));
        }
        $('#tship').text(parseFloat(shipping) > 0 ? formatMoney(shipping) : '');

        $('#shipping_in_grand_total').val(1);
        // if (pos_settings.home_delivery_in_invoice == 0 && restobar_mode_module != false) {
        if (pos_settings.home_delivery_in_invoice == 0) {
            $('#tship').text(formatMoney(setted_shipping));
            $('#shipping_in_grand_total').val(0);
        }

        // console.log('gtotal '+gtotal);
        $('#gtotal').text(formatMoney(gtotal));

        /* SALE TIP */
        $('#tips').text(formatMoney(0));
        $('#sale_tip_amount').val(0);
        no_tip = false;
        // if (localStorage.getItem('delete_tip_amount') || restobar_mode_module != 1) {
        if (localStorage.getItem('delete_tip_amount')) {
            no_tip = true;
        }
        // if (pos_settings.apply_suggested_tip > 0 && !no_tip) {

        //     tip_amount = gtotal * (pos_settings.apply_suggested_tip / 100);

        //     if (localStorage.getItem('tip_amount') && formatDecimal(localStorage.getItem('tip_amount')) > 0) {
        //         tip_amount = formatDecimal(localStorage.getItem('tip_amount'));
        //     } else {
        //         localStorage.setItem('tip_amount', tip_amount);
        //     }

        //     $('#tips').text(formatMoney(tip_amount));
        //     gtotal = gtotal + tip_amount;
        //     $('#gtotal').text(formatMoney(gtotal));
        //     $('#sale_tip_amount').val(formatDecimal(tip_amount));
        //     // if (tip_amount > 0) {
        //     //     localStorage.setItem('tip_amount', tip_amount);
        //     // }
        // } else {
            if (localStorage.getItem('tip_amount')) {
                localStorage.removeItem('tip_amount');
            }
        // }
        /* SALE TIP */


        localStorage.setItem('subtotal', subtotalwtax);
        localStorage.setItem('tax', total_tax);
        localStorage.setItem('total', total);
        localStorage.setItem('discount', product_discount+order_discount);
        localStorage.setItem('gtotal', gtotal);

        if (pos_settings.remote_printing != 1) {

            order_data.items = o_items;
            bill_data.items = b_items;
            var b_totals = '';
            b_totals += printLine(lang.total+': '+ formatMoney(total)) +"\n";
            if(order_discount > 0 || product_discount > 0) {
                b_totals += printLine(lang.discount+': '+ formatMoney(order_discount+product_discount)) +"\n";
            }
            if (site.settings.tax2 != 0 && invoice_tax != 0) {
                b_totals += printLine(lang.order_tax+': '+ formatMoney(invoice_tax)) +"\n";
            }
            b_totals += printLine(lang.grand_total+': '+ formatMoney(gtotal)) +"\n";
            if(pos_settings.rounding != 0) {
                round_total = roundNumber(gtotal, parseInt(pos_settings.rounding));
                var rounding = formatDecimal(round_total - gtotal);
                b_totals += printLine(lang.rounding+': '+ formatMoney(rounding)) +"\n";
                b_totals += printLine(lang.total_payable+': '+ formatMoney(round_total)) +"\n";
            }
            b_totals += "\n"+ lang.items+': '+ (an - 1) + ' (' + (parseFloat(count) - 1) + ')' +"\n";
            bill_data.totals = b_totals;
            bill_data.footer = "\n"+ lang.merchant_copy+"\n";

        } else {

            var bill_totals = '';
            bill_totals += '<tr class="bold"><td>'+lang.total+'</td><td style="text-align:right;">'+formatMoney(total)+'</td></tr>';

            if(order_discount > 0 || product_discount > 0) {
                bill_totals += '<tr class="bold"><td>'+lang.discount+'</td><td style="text-align:right;">'+formatMoney(order_discount+product_discount)+'</td></tr>';
            }
            if (site.settings.tax2 != 0 && invoice_tax != 0) {
                bill_totals += '<tr class="bold"><td>'+lang.order_tax+'</td><td style="text-align:right;">'+formatMoney(invoice_tax)+'</td></tr>';
            }
            bill_totals += '<tr class="bold"><td>'+lang.grand_total+'</td><td style="text-align:right;">'+formatMoney(gtotal)+'</td></tr>';
            if(pos_settings.rounding != 0) {
                round_total = roundNumber(gtotal, parseInt(pos_settings.rounding));
                var rounding = formatDecimal(round_total - gtotal);
                bill_totals += '<tr class="bold"><td>'+lang.rounding+'</td><td style="text-align:right;">'+formatMoney(rounding)+'</td></tr>';
                bill_totals += '<tr class="bold"><td>'+lang.total_payable+'</td><td style="text-align:right;">'+formatMoney(round_total)+'</td></tr>';
            }
            bill_totals += '<tr class="bold"><td>'+lang.items+'</td><td style="text-align:right;">'+(an - 1) + ' (' + (parseFloat(count) - 1) + ')</td></tr>';
            $('#bill-total-table').empty();
            $('#bill-total-table').append(bill_totals);
            $('#bill_footer').append('<p class="text-center"><br>'+lang.merchant_copy+'</p>');

        }
        if(count > 1) {
            $('#poscustomer').select2("readonly", true);
            $('#poswarehouse').select2("readonly", true);
        } else {
            $('#poscustomer').select2("readonly", false);
            $('#poswarehouse').select2("readonly", false);
        }
        if (KB) { display_keyboards(); }
        
        if (localStorage.getItem('balance_label')) {
            $('#add_item').attr('tabindex', 1);
            $('#add_item').focus();
        } else {
            if (site.settings.product_order == 2) {
                an = 2;
            }
            set_page_focus(an);
        }
        recalcular_retenciones();
        hide_invoice_footer_items_values();
        hide_invoice_footer_items_values(true);
        validate_fe_customer_data();
        $('[data-toggle="tooltip"]').tooltip();
        if (lock_submit !== false) {
            $('#submit-sale').fadeOut();
        } else {
            $('#submit-sale').fadeIn();
        }
        localStorage.setItem('lock_submit', lock_submit);
        if (except_category_taxes) {
            $('#except_category_taxes').val(1);
        }
        if (tax_exempt_customer) {
            $('#tax_exempt_customer').val(1);
        }

        if (diferent_tax_alert) {
            fix_different_taxes();
        }
    }
}
function fix_different_taxes(){
    if (localStorage.getItem('positems') && !localStorage.getItem('diferent_tax_alert')) {
        localStorage.setItem('diferent_tax_alert', 1);
        bootbox.confirm("Se detectó uno o varios productos con el impuesto incorrecto, ¿Desea corregirlo?", function(result) {
            if(result == true) {
                positems = JSON.parse(localStorage.getItem('positems'));
                $.each(positems, function(index, item){
                    if (item.row.diferent_tax_alert) {
                        item.row.tax_rate = item.row.real_tax_rate;
                        item.tax_rate = item.real_tax_rate;
                        localStorage.setItem('diferent_tax_alert_fixed', 1);
                    }
                });
                localStorage.setItem('positems', JSON.stringify(positems));
                loadItems();
            }
        });
    }
}


//retenciones
function recalcular_retenciones(){

    if (retenciones = localStorage.getItem('retenciones')) {
            retenciones = JSON.parse(retenciones);
            if (retenciones !== null && retenciones.id_rete_fuente != null && retenciones.id_rete_fuente.length > 0 && parseInt(retenciones.id_rete_fuente)) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').trigger('click');
                }
            }

            if (retenciones !== null && retenciones.id_rete_iva != null && retenciones.id_rete_iva.length > 0 && parseInt(retenciones.id_rete_iva)) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').trigger('click');
                }
            }

            if (retenciones !== null && retenciones.id_rete_ica != null && retenciones.id_rete_ica.length > 0 && parseInt(retenciones.id_rete_ica)) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').trigger('click');
                }
            }

            if (retenciones !== null && retenciones.id_rete_otros != null && retenciones.id_rete_otros.length > 0 && parseInt(retenciones.id_rete_otros)) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').trigger('click');
                }
            }

            setTimeout(function() {

                $.each($('#rete_fuente_option option'), function(index, value){
                    if(retenciones !== null && retenciones.id_rete_fuente != null && retenciones.id_rete_fuente.length > 0 && parseInt(retenciones.id_rete_fuente) && value.value == retenciones.id_rete_fuente){
                        $('#rete_fuente_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_iva_option option'), function(index, value){
                    if(retenciones !== null && retenciones.id_rete_iva != null && retenciones.id_rete_iva.length > 0 && parseInt(retenciones.id_rete_iva) && value.value == retenciones.id_rete_iva){
                        $('#rete_iva_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_ica_option option'), function(index, value){
                    if(retenciones !== null && retenciones.id_rete_ica != null && retenciones.id_rete_ica.length > 0 && parseInt(retenciones.id_rete_ica) && value.value == retenciones.id_rete_ica){
                        $('#rete_ica_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_otros_option option'), function(index, value){
                    if(retenciones !== null && retenciones.id_rete_otros != null && retenciones.id_rete_otros.length > 0 && parseInt(retenciones.id_rete_otros) && value.value == retenciones.id_rete_otros){
                        $('#rete_otros_option').select2('val', value.value).trigger('change');
                    }
                });

                // $('#rete_otros_option').select2().trigger('change');

                $('#updateOrderRete').trigger('click');

            }, 1100);
    }
}
//retenciones

function printLine(str) {
    var size = pos_settings.char_per_line;
    var len = str.length;
    var res = str.split(":");
    var newd = res[0];
    for(i=1; i<(size-len); i++) {
        newd += " ";
    }
    newd += res[1];
    return newd;
}

/* -----------------------------
 * Add Purchase Iten Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */

 function add_invoice_item(item) {

    if (count == 1) {
        positems = {};
        if ($('#poswarehouse').val() && $('#poscustomer').val()) {
            $('#poscustomer').select2("readonly", true);
            $('#poswarehouse').select2("readonly", true);
        } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }
    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (positems[item_id]) {

        var new_qty = parseFloat(positems[item_id].row.qty) + (item.row.qty ? parseFloat(item.row.qty) : 1);
        positems[item_id].row.base_quantity = new_qty;
        if(positems[item_id].row.unit != positems[item_id].row.base_unit) {
            $.each(positems[item_id].units, function(){
                if (this.id == positems[item_id].row.unit) {
                    positems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        positems[item_id].row.qty = new_qty;

    } else {
        positems[item_id] = item;
    }
    positems[item_id].order = new Date().getTime();
    localStorage.setItem('positems', JSON.stringify(positems));

    wappsiPreciosUnidad(positems[item_id].row.id,item_id,positems[item_id].row.qty, true);
    setTimeout(function() {
        if (site.pos_settings.show_variants_and_preferences == 1) {
            $('#row_'+item_id).find('.sname').trigger('click');
        }
    }, 1500);
    return true;
 }


 if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
 }

 function display_keyboards() {

    $('.kb-text').keyboard({
        autoAccept: true,
        alwaysOpen: false,
        openOn: 'focus',
        usePreview: false,
        layout: 'custom',
        //layout: 'qwerty',
        display: {
            'bksp': "\u2190",
            'accept': 'return',
            'default': 'ABC',
            'meta1': '123',
            'meta2': '#+='
        },
        customLayout: {
            'default': [
            'q w e r t y u i o p {bksp}',
            'a s d f g h j k l {enter}',
            '{s} z x c v b n m , . {s}',
            '{meta1} {space} {cancel} {accept}'
            ],
            'shift': [
            'Q W E R T Y U I O P {bksp}',
            'A S D F G H J K L {enter}',
            '{s} Z X C V B N M / ? {s}',
            '{meta1} {space} {meta1} {accept}'
            ],
            'meta1': [
            '1 2 3 4 5 6 7 8 9 0 {bksp}',
            '- / : ; ( ) \u20ac & @ {enter}',
            '{meta2} . , ? ! \' " {meta2}',
            '{default} {space} {default} {accept}'
            ],
            'meta2': [
            '[ ] { } # % ^ * + = {bksp}',
            '_ \\ | &lt; &gt; $ \u00a3 \u00a5 {enter}',
            '{meta1} ~ . , ? ! \' " {meta1}',
            '{default} {space} {default} {accept}'
            ]}
        });
    $('.kb-pad').keyboard({
        restrictInput: true,
        preventPaste: true,
        autoAccept: true,
        alwaysOpen: false,
        openOn: 'click',
        usePreview: false,
        layout: 'custom',
        display: {
            'b': '\u2190:Backspace',
        },
        customLayout: {
            'default': [
            '1 2 3 {b}',
            '4 5 6 . {clear}',
            '7 8 9 0 %',
            '{accept} {cancel}'
            ]
        }
    });
    var cc_key = (site.settings.decimals_sep == ',' ? ',' : '{clear}');
    $('.kb-pad1').keyboard({
        restrictInput: true,
        preventPaste: true,
        autoAccept: true,
        alwaysOpen: false,
        openOn: 'click',
        usePreview: false,
        layout: 'custom',
        display: {
            'b': '\u2190:Backspace',
        },
        customLayout: {
            'default': [
            '1 2 3 {b}',
            '4 5 6 . '+cc_key,
            '7 8 9 0 %',
            '{accept} {cancel}'
            ]
        }
    });

 }

/*$(window).bind('beforeunload', function(e) {
    if(count > 1){
    var msg = 'You will loss the sale data.';
        (e || window.event).returnValue = msg;
        return msg;
    }
});
*/
if(site.settings.auto_detect_barcode == 1) {
    $(document).ready(function() {
        var pressed = false;
        var chars = [];
        $(window).keypress(function(e) {
            if(e.key == '%') { pressed = true; }
            chars.push(String.fromCharCode(e.which));
            if (pressed == false) {
                setTimeout(function(){
                    if (chars.length >= 8) {
                        var barcode = chars.join("");
                        $( "#add_item" ).focus().autocomplete( "search", barcode );
                    }
                    chars = [];
                    pressed = false;
                },200);
            }
            pressed = true;
        });
    });
}

$(document).ready(function() {
    read_card();
});

function generateCardNo(x) {
    if(!x) { x = 16; }
    chars = "1234567890";
    no = "";
    for (var i=0; i<x; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        no += chars.substring(rnum,rnum+1);
    }
    return no;
}
function roundNumber(number, toref) {
    switch(toref) {
        case 1:
            var rn = formatDecimal(Math.round(number * 20)/20);
            break;
        case 2:
            var rn = formatDecimal(Math.round(number * 2)/2);
            break;
        case 3:
            var rn = formatDecimal(Math.round(number));
            break;
        case 4:
            var rn = formatDecimal(Math.ceil(number));
            break;
        default:
            var rn = number;
    }
    return rn;
}
function getNumber(x) {
    return accounting.unformat(x);
}
function formatQuantity(x) {
    return (x != null) ? '<div class="text-center">'+formatNumber(x, site.settings.qty_decimals)+'</div>' : '';
}
function formatQuantity2(x) {
    return (x != null) ? formatQuantityNumber(x, site.settings.qty_decimals) : '';
}
function formatQuantityNumber(x, d) {
    if (!d) { d = site.settings.qty_decimals; }
    return parseFloat(accounting.formatNumber(x, d, '', '.'));
}
function formatQty(x) {
    return (x != null) ? formatNumber(x, site.settings.qty_decimals) : '';
}
function formatNumber(x, d) {
    if(!d && d != 0) { d = site.settings.decimals; }
    if(site.settings.sac == 1) {
        return formatSA(parseFloat(x).toFixed(d));
    }
    return accounting.formatNumber(x, d, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep);
}
// function formatMoney(x, symbol) {
//     if(!symbol) { symbol = ""; }
//     if(site.settings.sac == 1) {
//         return symbol+''+formatSA(parseFloat(x).toFixed(site.settings.decimals));
//     }
//     return accounting.formatMoney(x, symbol, site.settings.decimals, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep, "%s%v");
// }
function formatCNum(x) {
    if (site.settings.decimals_sep == ',') {
        var x = x.toString();
        var x = x.replace(",", ".");
        return parseFloat(x);
    }
    return x;
}
function hrsd(sdate) {
    return moment().format(site.dateFormats.js_sdate.toUpperCase())
}

function hrld(ldate) {
    return moment().format(site.dateFormats.js_sdate.toUpperCase()+' H:mm')
}
function is_valid_discount(mixed_var) {
    return (is_numeric(mixed_var) || (/([0-9]%)/i.test(mixed_var))) ? true : false;
}
function is_numeric(mixed_var) {
    var whitespace =
    " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
    return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -
        1)) && mixed_var !== '' && !isNaN(mixed_var);
}
function is_float(mixed_var) {
    return +mixed_var === mixed_var && (!isFinite(mixed_var) || !! (mixed_var % 1));
}
function currencyFormat(x) {
    return formatMoney(x != null ? x : 0);
}
function formatSA (x) {
    x=x.toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
       afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;

    return res;
}

function unitToBaseQty(qty, unitObj) {
    switch(unitObj.operator) {
        case '*':
            return parseFloat(qty)*parseFloat(unitObj.operation_value);
            break;
        case '/':
            return parseFloat(qty)/parseFloat(unitObj.operation_value);
            break;
        case '+':
            return parseFloat(qty)+parseFloat(unitObj.operation_value);
            break;
        case '-':
            return parseFloat(qty)-parseFloat(unitObj.operation_value);
            break;
        default:
            return parseFloat(qty);
    }
}

function baseToUnitQty(qty, unitObj) {
    switch(unitObj.operator) {
        case '*':
            return parseFloat(qty)/parseFloat(unitObj.operation_value);
            break;
        case '/':
            return parseFloat(qty)*parseFloat(unitObj.operation_value);
            break;
        case '+':
            return parseFloat(qty)-parseFloat(unitObj.operation_value);
            break;
        case '-':
            return parseFloat(qty)+parseFloat(unitObj.operation_value);
            break;
        default:
            return parseFloat(qty);
    }
}

function read_card() {
    var typingTimer;

    $('.swipe').keyup(function (e) {
        e.preventDefault();
        var self = $(this);
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function() {
            var payid = self.attr('id');
            var id = payid.substr(payid.length - 1);
            var v = self.val();
            var p = new SwipeParserObj(v);

            if(p.hasTrack1) {
                var CardType = null;
                var ccn1 = p.account.charAt(0);
                if(ccn1 == 4)
                    CardType = 'Visa';
                else if(ccn1 == 5)
                    CardType = 'MasterCard';
                else if(ccn1 == 3)
                    CardType = 'Amex';
                else if(ccn1 == 6)
                    CardType = 'Discover';
                else
                    CardType = 'Visa';

                $('#pcc_no_'+id).val(p.account).change();
                $('#pcc_holder_'+id).val(p.account_name).change();
                $('#pcc_month_'+id).val(p.exp_month).change();
                $('#pcc_year_'+id).val(p.exp_year).change();
                $('#pcc_cvv2_'+id).val('');
                $('#pcc_type_'+id).val(CardType).change();
                self.val('');
                $('#pcc_cvv2_'+id).focus();
            } else {
                $('#pcc_no_'+id).val('');
                $('#pcc_holder_'+id).val('');
                $('#pcc_month_'+id).val('');
                $('#pcc_year_'+id).val('');
                $('#pcc_cvv2_'+id).val('');
                $('#pcc_type_'+id).val('');
            }
        }, 100);
    });

    $('.swipe').keydown(function (e) {
        clearTimeout(typingTimer);
    });
}

function check_add_item_val() {
    $('#add_item').bind('keypress', function (e) {
        if (e.keyCode == 13 || e.keyCode == 9) {
            e.preventDefault();
            $(this).autocomplete("search");
        }
    });
}
function nav_pointer() {
    var pp = p_page == 'n' ? 0 : p_page;
    (pp == 0) ? $('#previous').attr('disabled', true) : $('#previous').attr('disabled', false);
    (pp == 0) ? $('#first').attr('disabled', true) : $('#first').attr('disabled', false);
    ((pp+pro_limit) > tcp) ? $('#next').attr('disabled', true) : $('#next').attr('disabled', false);
    ((pp+pro_limit) > tcp) ? $('#last').attr('disabled', true) : $('#last').attr('disabled', false);
}

function product_name(name, size) {
    if (!size) { size = 42; }
    return name.substring(0, (size-7));
}

$.extend($.keyboard.keyaction, {
    enter : function(base) {
        if (base.$el.is("textarea")){
            base.insertText('\r\n');
        } else {
            base.accept();
        }
    }
});

$(document).ajaxStart(function(){
  $('#ajaxCall').show();
}).ajaxStop(function(){
  $('#ajaxCall').hide();
});

$(document).ready(function(){
    // nav_pointer();
    $('#myModal').on('hidden.bs.modal', function() {
        $(this).find('.modal-dialog').empty();
        $(this).removeData('bs.modal');
    });
    $('#myModal2').on('hidden.bs.modal', function () {
        $(this).find('.modal-dialog').empty();
        $(this).removeData('bs.modal');
        $('#myModal').css('zIndex', '1050');
        $('#myModal').css('overflow-y', 'scroll');
    });
    $('#myModal2').on('show.bs.modal', function () {
        $('#myModal').css('zIndex', '1040');
    });
    $('.modal').on('hidden.bs.modal', function() {
        $(this).removeData('bs.modal');
    });
    $('.modal').on('show.bs.modal', function () {
        $('#modal-loading').show();
        $('.blackbg').css('zIndex', '1041');
        $('.loader').css('zIndex', '1042');
    }).on('hide.bs.modal', function () {
        $('#modal-loading').hide();
        $('.blackbg').css('zIndex', '3');
        $('.loader').css('zIndex', '4');
    });
    $('#clearLS').click(function(event) {
        bootbox.confirm("Are you sure?", function(result) {
        if(result == true) {
            localStorage.clear();
            location.reload();
        }
        });
        return false;
    });
});

$(document).on('change', '.paid_by', function(){
    payment = $(this).closest('.payment');
    paid_by = $(this).val();
    amount_to_pay = payment.find('.amount').val();
    customer = $('#poscustomer').val();
    payment.find('.deposit_message').remove();

    if (paid_by == "deposit") {
        $.ajax({
            url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    }
});

$(document).on('keyup', '.amount', function(){
    payment = $(this).closest('.payment');
    paid_by = payment.find('select.paid_by');
    amount_to_pay = payment.find('.amount').val();
    customer = $('#poscustomer').val();
    if (paid_by.val() == "deposit") {
        $.ajax({
            url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    }
});

//PAYMENT TERM



//PAYMENT TERM

//$.ajaxSetup ({ cache: false, headers: { "cache-control": "no-cache" } });
if(pos_settings.focus_add_item != '') {
    shortcut.add(pos_settings.focus_add_item, function() {
            if (site.settings.prioridad_precios_producto == 5) {
                $("#searchProduct").click();
            } else {
                $("#add_item").focus();
            }
        },
        {'type':'keydown', 'propagate':false, 'target':document}
    );
}
if(pos_settings.add_manual_product != '') { shortcut.add(pos_settings.add_manual_product, function() { $("#addManually").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.customer_selection != '') { shortcut.add(pos_settings.customer_selection, function() { $("#poscustomer").select2("open"); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.add_customer != '') { shortcut.add(pos_settings.add_customer, function() { $("#add-customer").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.toggle_category_slider != '') { shortcut.add(pos_settings.toggle_category_slider, function() { $("#open-category").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.toggle_brands_slider != '') { shortcut.add(pos_settings.toggle_brands_slider, function() { $("#open-brands").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.toggle_subcategory_slider != '') { shortcut.add(pos_settings.toggle_subcategory_slider, function() { $("#open-subcategory").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }

if(pos_settings.toggle_subcategory_slider != '') { shortcut.add(pos_settings.toggle_subcategory_slider, function() { $("#open-promotions").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }

if(pos_settings.cancel_sale != '') { shortcut.add(pos_settings.cancel_sale, function() { $("#reset").click(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.suspend_sale != '') { shortcut.add(pos_settings.suspend_sale, function() { $("#suspend").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.print_items_list != '') { shortcut.add(pos_settings.print_items_list, function() { $("#print_btn").click(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.finalize_sale != '') {
    shortcut.add(pos_settings.finalize_sale, function() {
        if ($('#paymentModal').is(':visible')) {
            $("#submit-sale").click();
        } else {
            $("#payment").trigger('click');
        }
    }, {'type':'keydown', 'propagate':false, 'target':document});
}
if(pos_settings.today_sale != '') { shortcut.add(pos_settings.today_sale, function() { $("#today_sale").click(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.open_hold_bills != '') { shortcut.add(pos_settings.open_hold_bills, function() { $("#opened_bills").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.close_register != '') { shortcut.add(pos_settings.close_register, function() { $("#close_register").click(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
shortcut.add("ESC", function() { $("#cp").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} );

if (site.settings.set_focus != 1) {
    $(document).ready(function(){ $('#add_item').focus(); });
}

$(window).on('resize', function(){
    resize_pos();
});

function resize_pos(){
    var height = $('#page-wrapper').height() - 100;
    $('.pos_area').css('min-height', height); //set max height
}

function hide_invoice_footer_items_values(show = false){
    $('.order_detail > td.text-right > span').each(function(index, span){
        val = formatDecimal($(span).text());
        if (show == false) {
            if (val < 1) {
                $('.order_detail').eq(index).fadeOut();
            }
        } else {
            if (val > 0) {
                $('.order_detail').eq(index).fadeIn();
            }
        }
    });
}

 $(document).on('click', '.send_item_unit_select', function(){

    var itemunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('itemunitid');
    var productid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productid');
    var productunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productunitid');

    var unit_data = {
                        'product_id' : productid,
                        'unit_price_id' : itemunitid,
                        'product_unit_id' : productunitid,
                    };
    var warehouse_id = $('#poswarehouse').val();
    var unit_quantity = $('#unit_quantity').val();
    $.ajax({
        url:site.base_url+"sales/iusuggestions",
        type:"get",
        data:{
                'product_id' : unit_data.product_id,
                'unit_price_id' : unit_data.unit_price_id,
                'product_unit_id' : unit_data.product_unit_id,
                'warehouse_id' : warehouse_id,
                'unit_quantity' : unit_quantity,
                'biller_id' : $('#posbiller').val(),
                'address_id' : $('#poscustomerbranch').val(),
                'customer_id' : $('#poscustomer').val()
            }
    }).done(function(data){
        add_invoice_item(data);
        $('#sPUModal').modal('hide');
    });

});

var sended = false;

$('.amount').on('keypress', function(e){

    submit = $('#submit-sale').prop('disabled');
    lock_submit = localStorage.getItem('lock_submit');
    if (e.keyCode == 13 && !submit && !sended && lock_submit == 'false') {
        sended = true;
        setTimeout(function() {
            $('.send-submit-sale').focus();
        }, 800);

        bootbox.confirm({
            message: "¿Está seguro de guardar la factura actual?",
            buttons: {
                confirm: {
                    label: 'Enviar',
                    className: 'btn-success send-submit-sale'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#submit-sale').click();
                } else {
                    sended = false;
                }
            }
        });

    }
});

function validate_key_log(){
    var documnet_type_id = $('#doc_type_id').val();

    if (documnet_type_id != '') {
        $.ajax({
            type: "get", async: false,
            url: site.base_url+"sales/validate_key_log/" + $('#doc_type_id').val(),
            dataType: "json",
            success: function (data) {
                localStorage.setItem('poskeylog', data);
                loadItems();
            }
        });
    }
}

$(document).on('change', '#payment_reference_no', function(){
    var val = $(this).val();
    $('#payment_document_type_id').val(val);
});

$(document).on('change', '#poscustomer', function(){
    get_customer_branches();
    var prev_customer = localStorage.getItem('poscustomer');
    var current_customer = $(this).val();

    positems = JSON.parse(localStorage.getItem('positems'));

    if (positems && prev_customer != current_customer && site.settings.prioridad_precios_producto != 1 && site.settings.prioridad_precios_producto != 2) {
        bootbox.confirm({
            message: "Al cambiar el cliente, los precios se actualizarán de acuerdo con las condiciones del mismo. Que desea hacer ?",
            buttons: {
                confirm: {
                    label: 'Cambiar cliente',
                    className: 'btn-success send-submit-sale'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $.each(positems, function(id, arr){
                        wappsiPreciosUnidad(positems[id].row.id,id,positems[id].row.qty);
                    });
                    localStorage.setItem('poscustomer', current_customer);
                    customer_retentions();
                } else {
                    $('#poscustomer').select2('val', prev_customer);
                    customer_retentions();
                }
            }
        });
    } else {
        localStorage.setItem('poscustomer', current_customer);
        customer_retentions();
    }
    customer_special_discount();
});

function validate_fe_customer_data()
{
    var is_fe = $('#doc_type_id option:selected').data('fe');
    var customer_id = $('#poscustomer').val();

    if (is_fe) {
        $.ajax({
            url : site.base_url+"pos/validate_fe_customer_data/"+customer_id
        }).done(function(data){
            if (data != '') {
                localStorage.setItem('locked_for_customer_fe_data', 1);
                $('.txt-error').html(data).css('display', '');

                command: toastr.error(data, 'Mensaje de validación', { onHidden : function(){} });
                verify_locked_submit();
            } else if (data == ''){
                if (localStorage.getItem('locked_for_customer_fe_data')) {
                    localStorage.removeItem('locked_for_customer_fe_data');
                }
                $('.txt-error').html('').css('display', 'none');
                verify_locked_submit();
            }
        }).fail(function (data){ console.log(data.responseText); });
    } else {
        if (localStorage.getItem('locked_for_customer_fe_data')) {
            localStorage.removeItem('locked_for_customer_fe_data');
        }
        $('.txt-error').html('').css('display', 'none');
        verify_locked_submit();
    }
}

function verify_locked_submit(){
    if (localStorage.getItem('locked_for_customer_fe_data')) {
        $('#payment').attr('disabled', true);
    } else {
        $('#payment').attr('disabled', false);
    }
}

function customer_special_discount(){

    var customer_id = $('#poscustomer').val();
    $.ajax({
        url : site.base_url+"customers/get_customer_special_discount/"+customer_id,
        dataType : "JSON"
    }).done(function(data){

        localStorage.setItem('poscustomerspecialdiscount', data.special_discount);
        if (data.special_discount == 1) {
                loadItems();
                command: toastr.warning('Cliente con descuento especial asignado', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        }


        localStorage.removeItem('poscustomerdepositamount');
        if (data.deposit_amount > 0) {
            localStorage.setItem('poscustomerdepositamount', parseFloat(data.deposit_amount));
        }

    });

}

$(document).on('click', '#pos_wholesale_keep_prices', function(){
    localStorage.setItem('pos_wholesale_keep_prices', true);
    localStorage.setItem('pos_wholesale_keep_prices_quote_id', $('#quote_id').val());
    location.reload();
});

$(document).on('click', '#update_prices', function(){
    localStorage.setItem('pos_wholesale_keep_prices', false);
    localStorage.setItem('pos_wholesale_keep_prices_quote_id', $('#quote_id').val());
    location.reload();
});

function customer_retentions(){
    var customer_validate_min_base_retention = false;
    $.ajax({
        url : site.base_url+"customers/get_customer_by_id/"+$('#poscustomer').val(),
        dataType : "JSON"
    }).done(function(data){
        var key_log = localStorage.getItem('poskeylog');
        if (data.customer_validate_min_base_retention == 0) {
            customer_validate_min_base_retention = true;
        }
        localStorage.setItem('customer_validate_min_base_retention', customer_validate_min_base_retention);

        setTimeout(function() {
            prev_retention = JSON.parse(localStorage.getItem('retenciones'));
            if (prev_retention !== null && (prev_retention.total_rete_amount === null || prev_retention.total_rete_amount == 0)) {
                $('#cancelOrderRete').trigger('click');
                if((data.default_rete_fuente_id || data.default_iva_fuente_id || data.default_ica_fuente_id || data.default_other_fuente_id) && key_log == 1) {
                    var retenciones = {
                        'gtotal' : $('#gtotal').text(),
                        'id_rete_fuente' : data.default_rete_fuente_id,
                        'id_rete_iva' : data.default_rete_iva_id,
                        'id_rete_ica' : data.default_rete_ica_id,
                        'id_rete_otros' : data.default_rete_other_id,
                        };
                    localStorage.setItem('retenciones', JSON.stringify(retenciones));
                    recalcular_retenciones();
                }
            }
        }, 1600);


    });
}

$(document).on('change', '#cost_center', function(){
    $('#cost_center_id').val($(this).val());
});


function set_customer_payment(){
    var customer_id = $('#poscustomer').val();
    $('#pospayment_status').select2('val', 'pending').trigger('change');
    $('#pospayment_term').val('');
    $.ajax({
        url : site.base_url+"customers/get_payment_type/"+customer_id,
        dataType : "JSON"
    }).done(function(data){
        setTimeout(function() {
            if (data.payment_type == 0 && $('#paid_by_1 option[value="Credito"]').length > 0) {
                $('#pospayment_status').select2('val', 'pending').trigger('change');
                $('#paid_by_1').val('Credito').trigger('change');
                $('#pospayment_term').val(data.payment_term > 0 ? data.payment_term : 1);
                $('.payment_term_val').val(data.payment_term > 0 ? data.payment_term : 1);
            } else {
                if ($('.paid_by option[value="cash"]:not([disabled])').length == 0) {
                    $('.paid_by').select2('val', $('.paid_by option:not([disabled])').eq(1).val()).trigger('change');
                } else {
                    $('#paid_by_1').select2('val', 'cash').trigger('change');
                }
            }
        }, 850);
    }).fail(function(data){
        $('#paid_by_1').select2('val', 'cash').trigger('change');
    });
}