<?php
    $Credito = 0;
    $Credito_debo = 0;
    foreach ($total_payments as $payment)
    {
        ${$payment['name']} = 0;
        ${$payment['name'].'_debo'} = 0;
    }
    $detail_font_size = "13px";
    $title_font_size = "15px";
?>

<div  <?= isset($cron_job) ? "style='border:1px solid #DDD; padding:10px; margin:10px 0;'" : "" ?>>
    <table style="width: 100%;font-family: Arial; font-size: <?= $detail_font_size ?>;">
        <?php if ($biller): ?>
            <tr>
                <th align="center"><?= $biller->company ?></th>
            </tr>
            <tr>
                <th align="center"><?= lang("nit") .": ".$this->Settings->numero_documento ?></th>
            </tr>
            <tr>
                <th align="center"><?= ucwords(mb_strtolower($biller->address.", ".$biller->city." - ".$biller->state)) ?></th>
            </tr>
        <?php endif ?>
        <tr>
            <td align="center"><b><?= lang("summary_zeta_report") ?></b></td>
        </tr>
    </table>
    <table style="width: 100%;font-family: Arial; font-size: <?= $detail_font_size ?>;">
            <tr>
                <td align="left" colspan="2"> <b><?= lang("initial_date") ?></b></td>
                <td align="center" colspan="2"> <?php echo $input['fech_ini'];?></td>
            </tr>
            <tr>
                <td align="left" colspan="2"> <b><?= lang("end_date") ?></b></td>
                <td align="center" colspan="2"> <?php echo $input['fech_fin'];?></td>
            </tr>
            <tr>
                <td align="left" colspan="2"> <b><?= lang("initial_number") ?></b></td>
                <td align="center" colspan="2"> <?php if(count($billings) > 1) echo $billings[0]['reference_no'];?></td>
            </tr>
            <tr>
                <td align="left" colspan="2"> <b><?= lang("end_number") ?></b></td>
                <td align="center" colspan="2"> <?php if(count($billings) > 1) echo $billings[count($billings)-1]['reference_no'];?></td>
            </tr>
            <tr>
                <td align="left" colspan="2"> <b>Cantidad Registros </b></td>
                <td align="center" colspan="2"> <?php if(count($billings) > 1) echo count($billings);?></td>
            </tr>
            <tr>
                <td colspan="4"><br></td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang("sales") ?></b></td>
            </tr>
    </table>
    <table style="width: 100%;font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
        <tbody>
            <?php
            $arrayReplace = array(0,1,2,3,4,5,6,7,8,9,'.','/' );
            $arrayReplaceA_Z = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z','.','/' );
            $total_subtotal = $total_iva  =  $total_total  = 0;
            $count = 0;
            $prefijo_old = $referencia_old = '';
            $array_prefijo = array();
            $cantidad = 0;
            foreach ($billings as $billing)
            {
                $referencia = $billing['reference_no'];
                $prefijo = str_replace($arrayReplace,'',$referencia);

                if($prefijo_old!=$prefijo)
                {
                    $count++;
                    if($count > 1)
                    {
                        $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                        $array_prefijo[$prefijo_old]['count'] = $cantidad;
                    }
                    $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old , "count" => $cantidad );
                    $prefijo_old = $prefijo;
                    $cantidad = 0;
                }
                $referencia_old = $referencia;
                $cantidad++;
            }
            if(count($billings) > 1)
            {
                $array_prefijo[$prefijo]['Fin'] = $billings[count($billings)-1]['reference_no'];
                $array_prefijo[$prefijo]['count'] = $cantidad;
            }
            $prefijo_old = $referencia_old = '';
            $cantidad = 0;
            $total_subtotal = $total_iva  =  $total_total  = 0;
            $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;

            foreach ($billings as $billing)
            {
                $subtotal = $billing['total'];
                $iva = $billing['total_tax'];
                $total = $billing['grand_total'];
                $fecha = substr($billing['fecha'],0,10);
                $fecha_pay = substr($billing['fecha_pay'],0,10);
                if($billing['cantidad'] > 1 ) $Pago = $billing['name'].', Otros';
                else $Pago = $billing['name'];

                $referencia = $billing['reference_no'];
                $saldo = $billing['saldo'];

                if($saldo > 1)
                {
                    if($billing['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                    else $Pago = 'Credito';
                    $Credito += $total;
                }
                else
                {
                    if($fecha!=$fecha_pay)
                    {
                        if($billing['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                        else $Pago = 'Credito';
                        $Credito += $total;
                    }
                    else ${$billing['name']} += $total;
                }

                $prefijo = str_replace($arrayReplace,'',$referencia);

                if($prefijo_old!=$prefijo)
                {
                    if($prefijo_old!='')
                    {
                        ?>
                        <tr  >
                            <td align="center" style="padding: 2px;border-top: 1px solid;" > <b><?= lang("total") ?> </b> </td>
                            <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                            <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_prefijo_subtotal + $total_prefijo_iva,0,',','.'); ?></b> </td>
                            <!-- <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_prefijo_iva,0,',','.'); ?></b> </td> -->
                        </tr>
                        <?php
                        $total_prefijo_subtotal = $total_prefijo_iva  =  0;
                    }
                    $array_prefijo[$prefijo];
                    $Fin = $array_prefijo[$prefijo]['Fin'];
                    $Ini = $array_prefijo[$prefijo]['Ini'];
                    $count = $array_prefijo[$prefijo]['count'];
                    ?>
                    <tr>
                        <td colspan="4" style="padding-top: 2px;">
                            <table style="width: 100%;font-size: <?= $detail_font_size ?>;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" > <b> <?= lang("prefix") ?>: </b></td>
                                    <td align="center" > <?php echo $prefijo; ?> </td>
                                    <td align="center" > <b> <?= lang("records") ?>: </b></td>
                                    <td align="center" > <?php echo $count; ?> </td>
                                </tr>
                                <tr>
                                    <td align="center" > <b> <?= lang("ini") ?></b></td>
                                    <td align="center" > <?php echo $Ini; ?> </td>
                                    <td align="center" > <b> <?= lang("end") ?> </b></td>
                                    <td align="center" > <?php echo $Fin; ?> </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php
                    if($input['Tipo']=='Detallado')
                    {
                        ?>
                        <tr>
                            <td align="center" style="padding-top: 2px;"> <b><?= lang("reference_no") ?></b></td>
                            <td align="center" style="padding-top: 2px;"> <b><?= lang("date") ?></b></td>
                            <td align="right" style="padding-top: 2px;"> <b><?= lang("total") ?></b></td>
                            <!-- <td align="right" style="padding-top: 2px;"> <b>Iva</b></td> -->
                        </tr>
                        <?php
                    }
                    $prefijo_old = $prefijo;
                }
                $referencia = str_replace($arrayReplaceA_Z,'',strtolower($referencia));
                if($input['Tipo']=='Detallado')
                {
                    ?>
                    <tr>
                        <td align="center" > <?php echo $referencia; ?> </td>
                        <td align="center" style="font-size: 10px;" > <?php echo $fecha; ?> </td>
                        <td align="right" > <?php echo number_format($total,0,',','.'); ?> </td>
                        <!-- <td align="right" > <?php echo number_format($iva,0,',','.'); ?></td> -->
                    </tr>
                    <?php
                }
                $total_subtotal += $subtotal;
                $total_iva  += $iva;
                $total_prefijo_subtotal += $subtotal;
                $total_prefijo_iva  += $iva;
            }
            ?>
            <tr  >
                <td align="center" style="padding: 2px;border-top: 1px solid;" > <b><?= lang("total") ?></b> </td>
                <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_prefijo_subtotal + $total_prefijo_iva,0,',','.'); ?></b> </td>
                <!-- <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_prefijo_iva,0,',','.'); ?></b> </td> -->
            </tr>
            <tr  >
                <td align="center" style="padding: 2px;border-top: 1px solid;" > <b><?= lang("total") ?></b> </td>
                <td align="left" style="padding: 2px;border-top: 1px solid;" > <b><?= lang("sales") ?></b> </td>
                <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_subtotal + $total_iva,0,',','.'); ?></b> </td>
                <!-- <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_iva,0,',','.'); ?></b> </td> -->
            </tr>
            <tr>
                <td colspan="4"><br><br></td>
            </tr>
        </tbody>
    </table>
    <?php if ($this->pos_settings->apply_suggested_tip > 0): ?>
        <table style="width: 100%;  font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="4" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;"> <b><?= lang("tips") ?></b></td>
            </tr>
        </table>
        <table style="width: 100%;font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
            <tbody>
                <?php
                $arrayReplace = array(0,1,2,3,4,5,6,7,8,9,'.','/' );
                $arrayReplaceA_Z = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z','.','/' );
                $total_propina = 0;
                $count = 0;
                $prefijo_old = $referencia_old = '';
                $array_prefijo = array();
                $cantidad = 0;
                foreach ($billings as $billing)
                {
                    $referencia = $billing['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);

                    if($prefijo_old!=$prefijo)
                    {
                        $count++;
                        if($count > 1)
                        {
                            $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                            $array_prefijo[$prefijo_old]['count'] = $cantidad;
                        }
                        $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old , "count" => $cantidad );
                        $prefijo_old = $prefijo;
                        $cantidad = 0;
                    }
                    $referencia_old = $referencia;
                    $cantidad++;
                }
                if(count($billings) > 1)
                {
                    $array_prefijo[$prefijo]['Fin'] = $billings[count($billings)-1]['reference_no'];
                    $array_prefijo[$prefijo]['count'] = $cantidad;
                }
                $prefijo_old = $referencia_old = '';
                $cantidad = 0;
                $total_propina = 0;
                $total_prefijo_propina = 0;

                foreach ($billings as $billing)
                {
                    if ($billing['tip_amount'] == 0 || $billing['tip_amount'] == NULL) {
                        continue;
                    }

                    $propina = $billing['tip_amount'];
                    $fecha = substr($billing['fecha'],0,10);
                    $fecha_pay = substr($billing['fecha_pay'],0,10);
                    if($billing['cantidad'] > 1 ) $Pago = $billing['name'].', Otros';
                    else $Pago = $billing['name'];

                    $referencia = $billing['reference_no'];
                    $saldo = $billing['saldo'];

                    if($saldo > 1)
                    {
                        if($billing['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                        else $Pago = 'Credito';
                        $Credito += $total;
                    }
                    else
                    {
                        if($fecha!=$fecha_pay)
                        {
                            if($billing['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                            else $Pago = 'Credito';
                            $Credito += $total;
                        }
                        else ${$billing['name']} += $total;
                    }

                    $prefijo = str_replace($arrayReplace,'',$referencia);

                    if($prefijo_old!=$prefijo)
                    {
                        if($prefijo_old!='')
                        {
                            ?>
                            <tr  >
                                <td align="center" style="padding: 2px;border-top: 1px solid;" > <b><?= lang("total") ?> </b> </td>
                                <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                                <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_prefijo_propina,0,',','.'); ?></b> </td>
                            </tr>
                            <?php
                            $total_prefijo_propina = 0;
                        }
                        $array_prefijo[$prefijo];
                        $Fin = $array_prefijo[$prefijo]['Fin'];
                        $Ini = $array_prefijo[$prefijo]['Ini'];
                        $count = $array_prefijo[$prefijo]['count'];
                        ?>
                        <tr>
                            <td colspan="4" style="padding-top: 2px;">
                                <table style="width: 100%;font-size: <?= $detail_font_size ?>;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center" > <b> <?= lang("prefix") ?>: </b></td>
                                        <td align="center" > <?php echo $prefijo; ?> </td>
                                        <td align="center" > <b> <?= lang("records") ?>: </b></td>
                                        <td align="center" > <?php echo $count; ?> </td>
                                    </tr>
                                    <tr>
                                        <td align="center" > <b> <?= lang("ini") ?></b></td>
                                        <td align="center" > <?php echo $Ini; ?> </td>
                                        <td align="center" > <b> <?= lang("end") ?> </b></td>
                                        <td align="center" > <?php echo $Fin; ?> </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php
                        if($input['Tipo']=='Detallado')
                        {
                            ?>
                            <tr>
                                <td align="center" style="padding-top: 2px;"> <b><?= lang("reference_no") ?></b></td>
                                <td align="center" style="padding-top: 2px;"> <b><?= lang("date") ?></b></td>
                                <td align="right" style="padding-top: 2px;"> <b><?= lang("tip") ?></b></td>
                            </tr>
                            <?php
                        }
                        $prefijo_old = $prefijo;
                    }
                    $referencia = str_replace($arrayReplaceA_Z,'',strtolower($referencia));
                    if($input['Tipo']=='Detallado')
                    {
                        ?>
                        <tr>
                            <td align="center" > <?php echo $referencia; ?> </td>
                            <td align="center" style="font-size: 10px;" > <?php echo $fecha; ?> </td>
                            <td align="right" > <?php echo number_format($propina,0,',','.'); ?> </td>
                        </tr>
                        <?php
                    }
                    $total_propina += $propina;
                    $total_prefijo_propina += $propina;
                }
                ?>
                <tr  >
                    <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
                    <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                    <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_prefijo_propina,0,',','.'); ?></b> </td>
                </tr>
                <tr  >
                    <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
                    <td align="left" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("sales") ?></b> </td>
                    <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_propina,0,',','.'); ?></b> </td>
                    <!-- <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_iva,0,',','.'); ?></b> </td> -->
                </tr>
                <tr>
                    <td colspan="4"><br><br></td>
                </tr>
            </tbody>
        </table>
    <?php endif ?>

    <?php if ($this->pos_settings->apply_suggested_home_delivery_amount > 0): ?>
        <table style="width: 100%;  font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="4" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;"> <b><?= lang("home_delivery") ?></b></td>
            </tr>
        </table>
        <table style="width: 100%;font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
            <tbody>
                <?php
                $arrayReplace = array(0,1,2,3,4,5,6,7,8,9,'.','/' );
                $arrayReplaceA_Z = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z','.','/' );
                $total_domicilio = 0;
                $count = 0;
                $prefijo_old = $referencia_old = '';
                $array_prefijo = array();
                $cantidad = 0;
                foreach ($billings as $billing)
                {
                    $referencia = $billing['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);

                    if($prefijo_old!=$prefijo)
                    {
                        $count++;
                        if($count > 1)
                        {
                            $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                            $array_prefijo[$prefijo_old]['count'] = $cantidad;
                        }
                        $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old , "count" => $cantidad );
                        $prefijo_old = $prefijo;
                        $cantidad = 0;
                    }
                    $referencia_old = $referencia;
                    $cantidad++;
                }
                if(count($billings) > 1)
                {
                    $array_prefijo[$prefijo]['Fin'] = $billings[count($billings)-1]['reference_no'];
                    $array_prefijo[$prefijo]['count'] = $cantidad;
                }
                $prefijo_old = $referencia_old = '';
                $cantidad = 0;
                $total_domicilio = 0;
                $total_prefijo_domicilio = 0;

                foreach ($billings as $billing)
                {
                    if ($billing['shipping'] == 0 || $billing['shipping'] == NULL) {
                        continue;
                    }

                    $domicilio = $billing['shipping'];
                    $fecha = substr($billing['fecha'],0,10);
                    $fecha_pay = substr($billing['fecha_pay'],0,10);
                    if($billing['cantidad'] > 1 ) $Pago = $billing['name'].', Otros';
                    else $Pago = $billing['name'];

                    $referencia = $billing['reference_no'];
                    $saldo = $billing['saldo'];

                    if($saldo > 1)
                    {
                        if($billing['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                        else $Pago = 'Credito';
                        $Credito += $total;
                    }
                    else
                    {
                        if($fecha!=$fecha_pay)
                        {
                            if($billing['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                            else $Pago = 'Credito';
                            $Credito += $total;
                        }
                        else ${$billing['name']} += $total;
                    }

                    $prefijo = str_replace($arrayReplace,'',$referencia);

                    if($prefijo_old!=$prefijo)
                    {
                        if($prefijo_old!='')
                        {
                            ?>
                            <tr  >
                                <td align="center" style="padding: 2px;border-top: 1px solid;" > <b><?= lang("total") ?> </b> </td>
                                <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                                <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_prefijo_domicilio,0,',','.'); ?></b> </td>
                            </tr>
                            <?php
                            $total_prefijo_domicilio = 0;
                        }
                        $array_prefijo[$prefijo];
                        $Fin = $array_prefijo[$prefijo]['Fin'];
                        $Ini = $array_prefijo[$prefijo]['Ini'];
                        $count = $array_prefijo[$prefijo]['count'];
                        ?>
                        <tr>
                            <td colspan="4" style="padding-top: 2px;">
                                <table style="width: 100%;font-size: <?= $detail_font_size ?>;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center" > <b> <?= lang("prefix") ?>: </b></td>
                                        <td align="center" > <?php echo $prefijo; ?> </td>
                                        <td align="center" > <b> <?= lang("records") ?>: </b></td>
                                        <td align="center" > <?php echo $count; ?> </td>
                                    </tr>
                                    <tr>
                                        <td align="center" > <b> <?= lang("ini") ?></b></td>
                                        <td align="center" > <?php echo $Ini; ?> </td>
                                        <td align="center" > <b> <?= lang("end") ?> </b></td>
                                        <td align="center" > <?php echo $Fin; ?> </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php
                        if($input['Tipo']=='Detallado')
                        {
                            ?>
                            <tr>
                                <td align="center" style="padding-top: 2px;"> <b><?= lang("reference_no") ?></b></td>
                                <td align="center" style="padding-top: 2px;"> <b><?= lang("date") ?></b></td>
                                <td align="right" style="padding-top: 2px;"> <b><?= lang("tip") ?></b></td>
                            </tr>
                            <?php
                        }
                        $prefijo_old = $prefijo;
                    }
                    $referencia = str_replace($arrayReplaceA_Z,'',strtolower($referencia));
                    if($input['Tipo']=='Detallado')
                    {
                        ?>
                        <tr>
                            <td align="center" > <?php echo $referencia; ?> </td>
                            <td align="center" style="font-size: 10px;" > <?php echo $fecha; ?> </td>
                            <td align="right" > <?php echo number_format($domicilio,0,',','.'); ?> </td>
                        </tr>
                        <?php
                    }
                    $total_domicilio += $domicilio;
                    $total_prefijo_domicilio += $domicilio;
                }
                ?>
                <tr  >
                    <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
                    <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                    <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_prefijo_domicilio,0,',','.'); ?></b> </td>
                </tr>
                <tr  >
                    <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
                    <td align="left" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("sales") ?></b> </td>
                    <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_domicilio,0,',','.'); ?></b> </td>
                    <!-- <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?= number_format($total_iva,0,',','.'); ?></b> </td> -->
                </tr>
                <tr>
                    <td colspan="4"><br><br></td>
                </tr>
            </tbody>
        </table>
    <?php endif ?>

    <?php if (isset($deposits) && count($deposits) > 0) : ?>
        <table style="width: 100%;  font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="4" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang('manual_deposits') ?></b></td>
            </tr>
        </table>
        <table style="width: 100%;  font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
            <?php
            $count = 0;
            $prefijo_old = $referencia_old = '';
            $array_prefijo = array();
            $cantidad = 0;
            $manualDeposit = [];
            foreach ($deposits as $deposit)
            {
                if ($deposit['origen'] === '1') {
                    $manualDeposit[$deposit['paid_by']] = isset($manualDeposit[$deposit['paid_by']]) ? $manualDeposit[$deposit['paid_by']] + $deposit['amount'] : $deposit['amount'];
                    $referencia = $deposit['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);
                    if($prefijo_old!=$prefijo)
                    {
                        $count++;
                        if($count > 1)
                        {
                            $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                            $array_prefijo[$prefijo_old]['count'] = $cantidad;
                        }
                        $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old , "count" => $cantidad );
                        $prefijo_old = $prefijo;
                        $cantidad = 0;
                    }
                    $referencia_old = $referencia;
                    $cantidad++;
                }
            }
            if(count($billings) > 1)
            {
                $array_prefijo[$prefijo]['Fin'] = $billings[count($billings)-1]['reference_no'];
                $array_prefijo[$prefijo]['count'] = $cantidad;
            }
            $prefijo_old = $referencia_old = '';
            $total_deposits = 0;
            $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;

            foreach ($deposits as $deposit)
            {
                if ($deposit['origen'] === '1') {
                    $total = $deposit['amount'];
                    $fecha = substr($deposit['fecha'],0,10);
                    $Pago = $deposit['name'];
                    $referencia = $deposit['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);

                    if ($deposit['code'] !== 'deposit') {
                        if (!isset($paids_deposits[$deposit['name']])) {
                            $paids_deposits[$deposit['name']] = $deposit['amount'];
                        }
                        else{
                            $paids_deposits[$deposit['name']] += $deposit['amount'];
                        }
                    }

                    if($prefijo_old!=$prefijo)
                    {
                        if($prefijo_old!='')
                        {
                            ?>
                            <tr  >
                                <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
                                <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                                <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_prefijo_subtotal + $total_prefijo_iva,0,',','.'); ?></b> </td>
                            </tr>
                            <?php
                            $total_prefijo_subtotal = $total_prefijo_iva  =  0;
                        }
                        $array_prefijo[$prefijo];
                        $Fin = $array_prefijo[$prefijo]['Fin'];
                        $Ini = $array_prefijo[$prefijo]['Ini'];
                        $count = $array_prefijo[$prefijo]['count'];
                        ?>
                        <tr>
                            <td colspan="4" style="padding-top: 2px;">
                                <table style="width: 100%;font-size: <?= $detail_font_size ?>;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center" > <b> <?= lang("prefix") ?>: </b></td>
                                        <td align="center" > <?php echo $prefijo; ?> </td>
                                        <td align="center" > <b> <?= lang("records") ?>: </b></td>
                                        <td align="center" > <?php echo $count; ?> </td>
                                    </tr>
                                    <tr>
                                        <td align="center" > <b> <?= lang("ini") ?></b></td>
                                        <td align="center" > <?php echo $Ini; ?> </td>
                                        <td align="center" > <b> <?= lang("end") ?> </b></td>
                                        <td align="center" > <?php echo $Fin; ?> </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php
                        if($input['Tipo']=='Detallado')
                        {
                            ?>
                            <tr>
                                <td align="center" style="padding-top: 2px;"> <b><?= lang("reference_no") ?></b></td>
                                <td align="center" style="padding-top: 2px;"> <b><?= lang("date") ?></b></td>
                                <td align="right" style="padding-top: 2px;"> <b><?= lang("subtotal") ?></b></td>
                                <!-- <td align="right" style="padding-top: 2px;"> <b>Iva</b></td> -->
                            </tr>
                            <?php
                        }
                        $prefijo_old = $prefijo;
                    }
                    $referencia = str_replace($arrayReplaceA_Z,'',strtolower($referencia));
                    if($input['Tipo']=='Detallado')
                    {
                        ?>
                        <tr>
                            <td align="center" > <?php echo $referencia; ?> </td>
                            <td align="center" style="font-size:10px;"> <?php echo $fecha; ?> </td>
                            <td align="right" > <?php echo number_format($total,0,',','.'); ?> </td>
                            <!-- <td align="right" > <?php echo number_format($iva,0,',','.'); ?></td> -->
                        </tr>
                        <?php
                    }
                    $total_deposits += $total;
                }
            }
            ?>
            <tr >
                <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
                <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_deposits, 0, ',', '.'); ?></b> </td>
            </tr>
            <tr >
                <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?></b> </td>
                <td align="left" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang('deposits') ?></b></td>
                <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_deposits, 0, ',', '.'); ?></b> </td>
            </tr>
            <tr>
                <td colspan="4"><br></td>
            </tr>
        </table>
    <?php endif ?>

    <table style="width: 100%;  font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" colspan="4" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang("returns") ?></b></td>
        </tr>
    </table>
    <?php
        $Factura_subtotal = $total_subtotal;
        $Factura_iva  = $total_iva;
    ?>
    <table style="width: 100%;  font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
        <?php
        $total_subtotal = $total_iva  =  $total_total  = 0;
        $count = 0;
        $prefijo_old = $referencia_old = '';
        $array_prefijo = array();
        $cantidad = 0;
        foreach ($returns as $return)
        {
            $referencia = $return['reference_no'];
            $prefijo = str_replace($arrayReplace,'',$referencia);
            if($prefijo_old!=$prefijo)
            {
                $count++;
                if($count > 1)
                {
                    $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                    $array_prefijo[$prefijo_old]['count'] = $cantidad;
                }
                $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old , "count" => $cantidad );
                $prefijo_old = $prefijo;
                $cantidad = 0;
            }
            $referencia_old = $referencia;
            $cantidad++;
        }
        if(count($billings) > 1)
        {
            $array_prefijo[$prefijo]['Fin'] = $billings[count($billings)-1]['reference_no'];
            $array_prefijo[$prefijo]['count'] = $cantidad;
        }
        $prefijo_old = $referencia_old = '';
        $total_subtotal = $total_iva  =  $total_total  = 0;
        $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;

        foreach ($returns as $return)
        {

            $subtotal = $return['total'];
            $iva = $return['total_tax'];
            $total = $return['grand_total'];
            $fecha = substr($return['fecha'],0,10);
            $fecha_pay = substr($return['fecha_pay'],0,10);
            if($return['cantidad'] > 1 ) $Pago = $return['name'].', Otros';
            else $Pago = $return['name'];

            $referencia = $return['reference_no'];
            $saldo = $return['saldo'];

            if($saldo > 1)
            {
                if($return['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                else $Pago = 'Credito';
                $Credito_debo += $total;
            }
            else
            {
                if($fecha!=$fecha_pay)
                {
                    if($return['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                    else $Pago = 'Credito';
                    $Credito_debo += $total;
                }
                else ${$return['name'].'_debo'} += $total;
            }

            $prefijo = str_replace($arrayReplace,'',$referencia);

            if($prefijo_old!=$prefijo)
            {
                if($prefijo_old!='')
                {
                    ?>
                    <tr  >
                        <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
                        <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
                        <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_prefijo_subtotal + $total_prefijo_iva,0,',','.'); ?></b> </td>
                        <!-- <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_prefijo_iva,0,',','.'); ?></b> </td> -->
                    </tr>
                    <?php
                    $total_prefijo_subtotal = $total_prefijo_iva  =  0;
                }
                $array_prefijo[$prefijo];
                $Fin = $array_prefijo[$prefijo]['Fin'];
                $Ini = $array_prefijo[$prefijo]['Ini'];
                $count = $array_prefijo[$prefijo]['count'];
                ?>
                <tr>
                    <td colspan="4" style="padding-top: 2px;">
                        <table style="width: 100%;font-size: <?= $detail_font_size ?>;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" > <b> <?= lang("prefix") ?>: </b></td>
                                <td align="center" > <?php echo $prefijo; ?> </td>
                                <td align="center" > <b> <?= lang("records") ?>: </b></td>
                                <td align="center" > <?php echo $count; ?> </td>
                            </tr>
                            <tr>
                                <td align="center" > <b> <?= lang("ini") ?></b></td>
                                <td align="center" > <?php echo $Ini; ?> </td>
                                <td align="center" > <b> <?= lang("end") ?> </b></td>
                                <td align="center" > <?php echo $Fin; ?> </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php
                if($input['Tipo']=='Detallado')
                {
                    ?>
                    <tr>
                        <td align="center" style="padding-top: 2px;"> <b><?= lang("reference_no") ?></b></td>
                        <td align="center" style="padding-top: 2px;"> <b><?= lang("date") ?></b></td>
                        <td align="right" style="padding-top: 2px;"> <b><?= lang("subtotal") ?></b></td>
                        <!-- <td align="right" style="padding-top: 2px;"> <b>Iva</b></td> -->
                    </tr>
                    <?php
                }
                $prefijo_old = $prefijo;
            }
            $referencia = str_replace($arrayReplaceA_Z,'',strtolower($referencia));
            if($input['Tipo']=='Detallado')
            {
                ?>
                <tr>
                    <td align="center" > <?php echo $referencia; ?> </td>
                    <td align="center" style="font-size:10px;"> <?php echo $fecha; ?> </td>
                    <td align="right" > <?php echo number_format($subtotal + $iva,0,',','.'); ?> </td>
                    <!-- <td align="right" > <?php echo number_format($iva,0,',','.'); ?></td> -->
                </tr>
                <?php
            }
            $total_subtotal += $subtotal;
            $total_iva  += $iva;
            $total_prefijo_subtotal += $subtotal;
            $total_prefijo_iva  += $iva;
        }
        ?>
        <tr >
            <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;" > </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_prefijo_subtotal + $total_prefijo_iva,0,',','.'); ?></b> </td>
            <!-- <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_prefijo_iva,0,',','.'); ?></b> </td> -->
        </tr>
        <tr >
            <td align="center" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?></b> </td>
            <td align="left" style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("returns") ?></b></td>
            <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_subtotal + $total_iva,0,',','.'); ?></b> </td>
            <!-- <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($total_iva,0,',','.'); ?></b> </td> -->
        </tr>
        <tr>
            <td colspan="4"><br></td>
        </tr>
    </table>
    <table style="width: 100%;  font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0" >
        <tr>
            <td align="center" colspan="5" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid; font-size: <?= $title_font_size ?>;"> <b><?= lang("summary_general") ?></b></td>
        </tr>
        <tr >
            <td align="center" colspan="2"  > </td>
            <td align="center" > <b><?= lang("subtotal") ?> </b> </td>
            <td align="center" > <b><?= lang("taxes") ?></b> </td>
            <td align="center" > <b><?= lang("total") ?></b> </td>
        </tr>
        <tr >
            <td align="left"  colspan="2" ><?= lang("sales") ?>  </td>
            <td align="right" > <?php echo number_format($Factura_subtotal,0,',','.'); ?> </td>
            <td align="right" > <?php echo number_format($Factura_iva,0,',','.'); ?> </td>
            <td align="right" > <?php echo number_format($Factura_subtotal+$Factura_iva,0,',','.'); ?> </td>
        </tr>
        <tr >
            <td align="left" colspan="2"  ><?= lang("returns") ?> </td>
            <td align="right" > <?php echo number_format($total_subtotal,0,',','.'); ?> </td>
            <td align="right" > <?php echo number_format($total_iva,0,',','.'); ?> </td>
            <td align="right" > <?php echo number_format($total_subtotal+$total_iva,0,',','.'); ?> </td>
        </tr>
        <?php if ($this->pos_settings->apply_suggested_tip > 0): ?>
            <tr >
                <td align="left" colspan="3"  ><?= lang("tips") ?> </td>
                <td align="right" > <?php echo number_format($total_propina,0,',','.'); ?> </td>
                <td align="right" >  </td>
            </tr>
        <?php endif ?>
        <?php if ($this->pos_settings->apply_suggested_home_delivery_amount > 0): ?>
            <tr >
                <td align="left" colspan="3"  ><?= lang("home_delivery") ?> </td>
                <td align="right" > <?php echo number_format($total_domicilio,0,',','.'); ?> </td>
                <td align="right" >  </td>
            </tr>
        <?php endif ?>
        <tr >
            <td align="center" colspan="2"  style="padding: 2px;border-top: 1px solid;" > <b> <?= lang("total") ?> </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($Factura_subtotal-($total_subtotal*-1)+(isset($total_propina) ? $total_propina : 0)+(isset($total_domicilio) ? $total_domicilio : 0),0,',','.'); ?></b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format($Factura_iva-($total_iva*-1),0,',','.'); ?></b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;" > <b><?php echo number_format(($Factura_subtotal-($total_subtotal*-1)+(isset($total_propina) ? $total_propina : 0)+(isset($total_domicilio) ? $total_domicilio : 0))+($Factura_iva-($total_iva*-1)),0,',','.'); ?></b> </td>
        </tr>
        <tr>
            <td colspan="5"><br></td>
        </tr>
        <tr>
            <td align="center" colspan="5" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang("summary_payment_methods") ?></b></td>
        </tr>
        <tr >
            <td align="center" colspan="3"  style="padding-top: 3px;" > <b><?= lang("form") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("sales") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("returns") ?></b> </td>
        </tr>
        <?php
            $total_pago =  0;
            $total_pago_debo =  0;
            foreach ($total_payments as $payment)
            {
                $name = $payment['name'];
                $name = str_replace('Tarjeta de','T. de',$name);
                if (isset($paids_deposits[$payment['name']])) {
                    $tpaid = $payment['total_payment'] + $paids_deposits[$payment['name']];
                }
                else{
                    $tpaid = $payment['total_payment'];
                }
                ?>
                <tr >
                    <td align="left" colspan="3"  > <?php echo $name; ?> </td>
                    <td align="right" > <?php echo number_format($tpaid,0,',','.'); ?>  </td>
                    <td align="right" > <?php echo number_format($payment['total_return_payment'],0,',','.'); ?> </td>
                </tr>
                <?php
                // $total_pago +=  ${$payment['name']};
                $total_pago +=  $tpaid;
                $total_pago_debo +=  ${$payment['name'].'_debo'};
            }
            $total_pago +=  $Credito;
            $total_pago_debo +=  $Credito_debo;
        ?>
         <tr >
            <td align="left" colspan="3"  > <?= lang("credits") ?> </td>
            <td align="right" > <?php echo number_format($Credito,0,',','.'); ?>  </td>
            <td align="right" > <?php echo number_format($Credito_debo,0,',','.'); ?> </td>
        </tr>
        <tr >
            <td align="center" colspan="3"  style="padding: 2px;border-top: 1px solid;">  <b> <?= lang("total") ?>  </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($total_pago,0,',','.'); ?>  </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($total_pago_debo,0,',','.'); ?>  </b> </td>
        </tr>
        <tr>
            <td colspan="5"><br></td>
        </tr>
        <tr>
            <td align="center" colspan="5" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang("summary_tax") ?></b></td>
        </tr>
        <tr >
            <td align="center" colspan="3"  style="padding-top: 3px;" > <b><?= lang("rate") ?></b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("tax_excl") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("vat") ?></b> </td>
        </tr>
        <?php
        $unisubtotal = $item_tax =  0;
        foreach ($total_tax_rates as $tax_rate)
        {
            ?>
            <tr >
                <td align="left" colspan="3"  > <?php echo $tax_rate['name']; ?>  </td>
                <td align="right" > <?php echo number_format($tax_rate['unisubtotal'],0,',','.'); ?>  </td>
                <td align="right" > <?php echo number_format($tax_rate['item_tax'],0,',','.'); ?>  </td>
            </tr>
            <?php
            $unisubtotal += $tax_rate['unisubtotal'];
            $item_tax += $tax_rate['item_tax'];
        }
        ?>
        <tr >
            <td align="center" colspan="3"  style="padding: 2px;border-top: 1px solid;">  <b><?= lang("total") ?> </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($unisubtotal,0,',','.'); ?> </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($item_tax,0,',','.'); ?>  </b> </td>
        </tr>
        <tr>
            <td colspan="4"><br></td>
        </tr>
        <tr>
            <td align="center" colspan="5" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang("summary_categories") ?></b></td>
        </tr>
        <tr >
            <td align="left" style="padding-top: 3px;" > <b><?= lang("category") ?></b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("trans") ?></b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("items") ?></b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("subtotal") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("vat") ?></b> </td>
        </tr>
        <?php
        $unisubtotal = $item_tax = 0;
        foreach ($categories_tax as $categori_tax)
        {
            if($categori_tax['Nombre']=='') $Nombre = 'Categoria General';
            else  $Nombre = $categori_tax['Nombre'];
            $contador = count(str_split($Nombre)) ;
            $retVal = ($contador>12) ? '.' : ' ' ;
            ?>
            <tr >
                <td align="left" > <?php echo substr($Nombre,0,12).$retVal; ?>  </td>
                <td align="right" > <?php echo $this->sma->formatQuantity($categori_tax['trans']); ?>  </td>
                <td align="right" > <?php echo $this->sma->formatQuantity($categori_tax['total_items']); ?>  </td>
                <td align="right" > <?php echo number_format($categori_tax['unisubtotal'],0,',','.'); ?>  </td>
                <td align="right" > <?php echo number_format($categori_tax['item_tax'],0,',','.'); ?>  </td>
            </tr>
            <?php
            $unisubtotal += $categori_tax['unisubtotal'];
            $item_tax += $categori_tax['item_tax'];
        }
        ?>
        <tr >
            <td align="center" colspan="3" style="padding: 2px;border-top: 1px solid;">  <b><?= lang("total") ?> <b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;"> <b><?php echo number_format($unisubtotal,0,',','.'); ?>  </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;"> <b><?php echo number_format($item_tax,0,',','.'); ?>  </b> </td>
        </tr>
        <tr>
            <td colspan="4"><br></td>
        </tr>
    </table>
    <table style="width: 100%;font-family: Arial; font-size: <?= $detail_font_size ?>;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0" >
        <tr>
            <td align="center" colspan="6" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang("summary_users") ?></b></td>
        </tr>
        <tr >
            <td align="left" style="padding-top: 3px;" > <b><?= lang("user") ?></b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("trans") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("items") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("sales_x") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("subtotal") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("total") ?> </b> </td>
        </tr>
        <?php
        $unisubtotal = $unitotal = 0;
        foreach ($total_users_taxs as $total_users_tax)
        {
            ?>
            <tr >
                <?php
                $first_name = explode(" ", ucfirst(mb_strtolower($total_users_tax['first_name'])));
                $username =  $first_name[0]." ".substr(mb_strtoupper($total_users_tax['last_name']), 0, 1);
                ?>
                <td align="left" style="font-size: <?= $detail_font_size ?>;" > <?php echo $username; ?>  </td>
                <td align="right" > <?php echo $this->sma->formatQuantity($total_users_tax['Cantidad']); ?>  </td>
                <td align="right" > <?php echo $this->sma->formatQuantity($total_users_tax['total_items']); ?>  </td>
                <td align="right" > <?php echo $this->sma->formatQuantity(($total_users_tax['total_items'] / $total_users_tax['Cantidad']),4); ?>  </td>
                <td align="right" > <?php echo number_format($total_users_tax['total'],0,',','.'); ?>  </td>
                <td align="right" > <?php echo number_format($total_users_tax['grand_total'],0,',','.'); ?>  </td>
            </tr>
            <?php
            $unisubtotal += $total_users_tax['total'];
            $unitotal += $total_users_tax['grand_total'];
        }
        ?>
        <tr >
            <td align="center" style="padding: 2px;border-top: 1px solid;" colspan="4">  <b><?= lang("total") ?> </b>  </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($unisubtotal,0,',','.'); ?>  </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($unitotal,0,',','.'); ?>  </b> </td>
        </tr>
        <tr>
            <td align="center" colspan="6" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang("summary_sellers") ?></b></td>
        </tr>
        <tr >
            <td align="left" style="padding-top: 3px;" > <b><?= lang("sellers") ?></b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("trans") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("items") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("sales_x") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("subtotal") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("total") ?> </b> </td>
        </tr>
        <?php
        $unisubtotal = $item_tax = 0;
        foreach ($total_sellers_taxs as $total_sellers_tax)
        {
            ?>
            <tr >
                <?php
                $first_name = explode(" ", ucfirst(mb_strtolower($total_sellers_tax['first_name'])));
                $username =  $first_name[0]." ".substr(mb_strtoupper($total_sellers_tax['first_lastname']), 0, 1);
                ?>
                <td align="left" style="font-size: <?= $detail_font_size ?>;" > <?php echo $username; ?>  </td>
                <td align="right" > <?php echo $this->sma->formatQuantity($total_sellers_tax['Cantidad']); ?>  </td>
                <td align="right" > <?php echo $this->sma->formatQuantity($total_sellers_tax['total_items']); ?>  </td>
                <td align="right" > <?php echo $this->sma->formatQuantity(($total_sellers_tax['total_items'] / $total_sellers_tax['Cantidad']),4); ?>  </td>
                <td align="right" > <?php echo number_format($total_sellers_tax['total'],0,',','.'); ?>  </td>
                <td align="right" > <?php echo number_format($total_sellers_tax['grand_total'],0,',','.'); ?>  </td>
            </tr>
            <?php
            $unisubtotal += $total_sellers_tax['total'];
            $unitotal += $total_sellers_tax['grand_total'];
        }
        ?>
        <tr >
            <td align="center" style="padding: 2px;border-top: 1px solid;" colspan="4">  <b><?= lang("total") ?> </b>  </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($unisubtotal,0,',','.'); ?>  </b> </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($unitotal,0,',','.'); ?>  </b> </td>
        </tr>

        <!--  -->

        <?php
        if($input['Tipo']=='Detallado')
        { ?>
            <tr>
                <td align="center" colspan="6" style="border-bottom: 1px solid;padding: 1px;border-top: 1px solid;font-size: <?= $title_font_size ?>;"> <b><?= lang("summary_customer") ?></b></td>
            </tr>
        <tr >
            <td align="left" style="padding-top: 3px;" colspan="4"> <b><?= lang("customers") ?></b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("amount") ?> </b> </td>
            <td align="right" style="padding-top: 3px;" > <b><?= lang("subtotal") ?> </b> </td>
        </tr>
        <?php
        $unisubtotal = $item_tax = 0;
        foreach ($total_customers_taxs as $total_customers_tax)
        {
            ?>
            <tr >
                <td align="left" style="font-size: <?= $detail_font_size ?>;" colspan="4" > <?php echo $total_customers_tax['name']; ?>  </td>
                <td align="right" > <?php echo $total_customers_tax['Cantidad']; ?>  </td>
                <td align="right" > <?php echo number_format($total_customers_tax['total'],0,',','.'); ?>  </td>
            </tr>
            <?php
            $unisubtotal += $total_customers_tax['total'];
        }
        ?>
        <tr >
            <td align="center" style="padding: 2px;border-top: 1px solid;" colspan="4">  <b><?= lang("total") ?> </b>  </td>
            <td align="right" style="padding: 2px;border-top: 1px solid;">  <b><?php echo number_format($unisubtotal,0,',','.'); ?>  </b> </td>
        </tr>
        <?php } ?>
    </table>
    <br>
    <table style="width: 100%;font-family: Arial; font-size: 10px;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" >
                Usuario: <?php echo $input['users'];?> <br>
            </td>
        </tr>
        <tr>
            <td align="left" >
                Fecha Impresion: <?php echo date('d-m-Y h:i A');?> <br>
            </td>
        </tr>
        <tr>
            <td align="left" >
                <?= lang("report_z_footer") ?>
            </td>
        </tr>
    </table>

</div>


    <?php if (!isset($cron_job)): ?>
        <script type="text/javascript">
                window.print();
                setTimeout(function() {
                    window.close();
                }, 850);
        </script>
    <?php endif ?>
