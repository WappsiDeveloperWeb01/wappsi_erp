<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SettingsModel extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getSettings() {
        return $this->db->get('settings')->row();
    }

    public function getShopSettings() {
        return $this->db->get('shop_settings')->row();
    }
}
