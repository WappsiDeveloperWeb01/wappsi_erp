<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $_post = $this->session->flashdata("post") ? $this->session->flashdata("post") : ""; ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row" id="sortable-view">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <?= admin_form_open_multipart("payroll_contracts/update", ["class" => "wizard-big wizard", "id" => "edit_contract_form"]); ?>
                        <?= form_hidden('employee_id', $employee->id); ?>
                        <?= form_hidden('module', $module); ?>
                        <?= form_hidden('contract_id', $contract->id); ?>

                        <h1><?= lang("contract") ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("internal_code"), "internal_code"); ?>
                                        <?php $internal_code = (!empty($_post)) ? $_post->internal_code : $contract->internal_code; ?>
                                        <?= form_input(["name"=>"internal_code", "id"=>"internal_code", "class"=>"form-control", "required"=>TRUE], $internal_code); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("contract_type"), "contract_type"); ?>
                                        <?php
                                            $topts[""] = lang('select');
                                            foreach ($types_contracts as $type_contract):
                                                $topts[$type_contract->id] = $type_contract->description;
                                            endforeach
                                        ?>
                                        <?php $contract_type = (!empty($_post)) ? $_post->contract_type : $contract->contract_type; ?>
                                        <?= form_dropdown(["name"=>"contract_type", "id"=>"contract_type", "class"=>"form-control not_select", "required"=>TRUE], $topts, $contract_type); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("contracts_start_date"), "start_date"); ?>
                                        <?php $start_date = (!empty($_post)) ? $_post->start_date : $contract->start_date; ?>
                                        <?php
                                            $readonly = (!$this->Owner) ? ((!empty($approved_payroll)) ? 'readonly' : '') : '';
                                        ?>
                                        <?= form_input(["name"=>"start_date", "id"=>"start_date", "type"=>"date", "class"=>"form-control", "required"=>TRUE], $start_date, $readonly); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("contracts_end_date"), "end_date"); ?>
                                        <?php $end_date = (!empty($_post)) ? $_post->end_date : $contract->end_date; ?>
                                        <?php $readonly = (!$this->Owner) ? ((!empty($approved_payroll)) ? 'readonly' : '') : ''; ?>
                                        <?= form_input(["name"=>"end_date", "id"=>"end_date", "type"=>"date", "class"=>"form-control", "required"=>TRUE], $end_date, $readonly); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("contracts_settlement_date"), "settlement_date"); ?>
                                        <?php $settlement_date = (!empty($_post)) ? $_post->settlement_date : $contract->settlement_date; ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"settlement_date", "id"=>"settlement_date", "type"=>"date", "class"=>"form-control", "disabled"=>TRUE, "max"=>date('Y-m-d')], $settlement_date); ?>
                                            <span class="input-group-btn">
                                                <?= form_button(["id"=>"add_settlement_date", "class"=>"btn btn-primary", "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>"Agregar fecha de liquidación", "style"=>"height: 36px"], '<i class="fa fa-hourglass-end" aria-hidden="true"></i>'); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("workday"), "workday"); ?>
                                        <?php
                                            $wopts[""] = lang('select');
                                            foreach ($workdays as $workday) {
                                                if ($workday->id == 1 || $workday->id == 2) {
                                                    $wopts[$workday->id] = $workday->name;
                                                }
                                            }
                                        ?>
                                        <?php $workday = (!empty($_post)) ? $_post->workday : $contract->workday; ?>
                                        <?= form_dropdown(["name"=>"workday", "id"=>"workday", "class"=>"form-control not_select", "required"=>TRUE], $wopts, $workday); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("payroll_contracts_biller"), "biller_id"); ?>
                                        <?php
                                            $bopts[""] = lang('select');
                                            $biller_id = '';
                                            $number_billers = count($billers);
                                            foreach ($billers as $key => $biller):
                                                $bopts[$biller->id] = "$biller->name ($biller->company)";
                                                if ($number_billers == 1 && $key == 0) {
                                                    $biller_id = $biller->id;
                                                }
                                            endforeach
                                        ?>
                                        <?php $biller_id = (!empty($_post)) ? $_post->biller_id : $contract->biller_id; ?>
                                        <?= form_dropdown(["name"=>"biller_id", "id"=>"biller_id", "class"=>"form-control not_select", "required"=>TRUE], $bopts, $biller_id); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("retired_risk"), 'retired_risk'); ?>
                                        <?php $ropts = [0=>lang("no"), 1=>lang("yes")]; ?>
                                        <?php $retired_risk = (!empty($_post)) ? $_post->retired_risk : $contract->retired_risk; ?>
                                        <?= form_dropdown(["name"=>"retired_risk", "id"=>"retired_risk", "class"=>"form-control not_select", "required"=>TRUE], $ropts, $retired_risk); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("area"), "area"); ?>
                                        <?php
                                            $aopts[""] = lang("select");
                                            foreach ($areas as $area):
                                                $aopts[$area->id] = $area->name;
                                            endforeach
                                        ?>
                                        <?php $area = (!empty($_post)) ? $_post->area : $contract->area; ?>
                                        <?= form_dropdown(["name"=>"area", "id"=>"area", "class"=>"form-control not_select", "required"=>TRUE], $aopts, $area); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("professional_position"), "professional_position"); ?>
                                        <?php $popts = ["" => lang("select")] ?>
                                        <div class="input-group">
                                            <?= form_dropdown(["name"=>"professional_position", "id"=>"professional_position", "class"=>"form-control not_select", "required"=>TRUE], $popts); ?>
                                            <span class="input-group-btn">
                                                <?= form_button(["id"=>"add_professional_position", "class"=>"btn btn-primary", "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>"Agregar cargo", "style"=>"height: 32px"], '<i class="fa fa-plus tol" aria-hidden="true"></i>'); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h1><?= lang("contracts_salary") ?></h1>
                        <section>
                            <div id="contact_data_container">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("employee_type"), "employee_type"); ?>
                                            <select name="employee_type" id="employee_type" class="form-control not_select" required="required">
                                                <option value=""><?= lang("select"); ?></option>
                                                <?php foreach ($types_employees as $type_employee): ?>
                                                    <?php $employee_type = (!empty($_post)) ? $_post->employee_type : $contract->employee_type; ?>
                                                    <option value="<?= $type_employee->code; ?>"
                                                        data-afp="<?= $type_employee->afp; ?>"
                                                        data-eps="<?= $type_employee->eps; ?>"
                                                        data-arl="<?= $type_employee->arl; ?>"
                                                        data-caja="<?= $type_employee->caja; ?>"
                                                        data-icbf="<?= $type_employee->icbf; ?>"
                                                        data-sena="<?= $type_employee->sena; ?>"
                                                        data-cesantias="<?= $type_employee->cesantias; ?>" <?= ($type_employee->code == $employee_type) ? "selected":""; ?>><?= $type_employee->code ." - ". $type_employee->description; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("payroll_contracts_base_amount"), "base_amount"); ?>
                                            <div class="input-group">
                                                <?php $base_amount = (!empty($_post)) ? $_post->base_amount : $contract->base_amount; ?>

                                                <?php $readonly = ($existingContractPreparationPayroll == true) ? 'readonly' : ''; ?>
                                                <?php $disabled = ($existingContractPreparationPayroll == true) ? 'disabled' : ''; ?>
                                                <?= form_input(["name"=>"base_amount", "id"=>"base_amount", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], $base_amount, $readonly); ?>
                                                <span class="input-group-addon btn-default" id="get_minimum_salary" data-toggle="tooltip" title="Para obtener el valor del Salario míinimo haga clic aquí" <?= $disabled ?>>
                                                    <i class="fa fa-clipboard" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("payroll_contracts_integral_salary"), "integral_salary"); ?>
                                            <?php $sopts = [ NOT => lang("no"), YES => lang("yes")]; ?>
                                            <?php $integral_salary = (!empty($_post)) ? $_post->integral_salary : $contract->integral_salary; ?>
                                            <?= form_dropdown(["name"=>"integral_salary", "id"=>"integral_salary", "class"=>"form-control", "disabled"=>TRUE, "required"=>TRUE], $sopts, $integral_salary); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("trans_allowance"), "trans_allowance"); ?>
                                            <?php $aopts = [ NOT => lang("no"), YES => lang("yes")]; ?>
                                            <?php $trans_allowance = (!empty($_post)) ? $_post->trans_allowance : $contract->trans_allowance; ?>
                                            <?= form_dropdown(["name"=>"trans_allowance", "id"=>"trans_allowance", "class"=>"form-control"], $aopts, $trans_allowance); ?>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <?= form_label(lang("payroll_contracts_payment_frequency"), "payment_frequency"); ?>
                                            <?php $fopts = [""=>lang("select")]; ?>
                                            <?php if (!empty($payment_frequencies)): ?>
                                                <?php foreach ($payment_frequencies as $payment_frequency) {
                                                    $fopts[$payment_frequency->id] = $payment_frequency->period;
                                                } ?>
                                            <?php endif; ?>
                                            <?php
                                                if ($this->Payroll_settings->payment_frequency != DEFINED_IN_CONTRACT) {
                                                    $payment_frequency = $this->Payroll_settings->payment_frequency;
                                                    $disable_payment_frequency = 'disabled="disabled"';
                                                } else {
                                                    $payment_frequency = (!empty($contract->payment_frequency)) ? $contract->payment_frequency : '';
                                                    $disable_payment_frequency = '';
                                                }
                                            ?>
                                            <?= form_dropdown(["name"=>"payment_frequency", "id"=>"payment_frequency", "class"=>"form-control", "required"=>TRUE], $fopts, $payment_frequency, $disable_payment_frequency); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("withholding_method"), "withholding_method"); ?>
                                            <?php
                                                $mopts = [
                                                    0=>lang("n/a"),
                                                    WITHHOLDING_PROCESS_1=>lang("payroll_contracts_process_1"),
                                                    WITHHOLDING_PROCESS_2=>lang("payroll_contracts_process_2"),
                                                    HAND_CALCULATION=>"Cálculo manual"
                                                ];
                                            ?>
                                            <?php $withholding_method = (!empty($_post)) ? $_post->withholding_method : HAND_CALCULATION; ?>
                                            <?= form_dropdown(["name"=>"withholding_method", "id"=>"withholding_method", "class"=>"form-control not_select", "disabled"=>TRUE], $mopts, $withholding_method); ?>
                                        </div>
                                    </div>

                                    <!-- <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("withholding_percentage"), "withholding_percentage"); ?>
                                            <div class="input-group">
                                                <?php $withholding_percentage = (!empty($_post)) ? $_post->withholding_percentage : $contract->withholding_percentage; ?>
                                                <?= form_input(["name"=>"withholding_percentage", "id"=>"withholding_percentage", "type"=>"number", "class"=>"form-control", "min"=>0, "disabled"=>TRUE, "required"=>TRUE], $withholding_percentage); ?>
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("payment_method"), "payment_method"); ?>
                                            <?php
                                                $popts = [""=>lang("select")];
                                                foreach ($payment_means as $payment_mean) {
                                                    $popts[$payment_mean->code] = $payment_mean->name;
                                                }
                                            ?>
                                            <?php $payment_method = (!empty($_post)) ? $_post->payment_method : $contract->payment_method; ?>
                                            <?= form_dropdown(["name"=>"payment_method", "id"=>"payment_method", "class"=>"form-control not_select", "required"=>TRUE], $popts, $payment_method); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("bank"), "bank"); ?>
                                            <?php
                                                $bopts = [""=>lang("select")];
                                                foreach ($banks as $bank) {
                                                    $bopts[$bank->id] = ucwords(mb_strtolower($bank->company));
                                                }
                                            ?>
                                            <?php $bank = (!empty($_post)) ? $_post->bank : $contract->bank; ?>
                                            <?= form_dropdown(["name"=>"bank", "id"=>"bank", "class"=>"form-control not_select", "disabled"=>TRUE, "required"=>TRUE], $bopts, $bank); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("account_type"), "account_type"); ?>
                                            <?php $aopts = [""=>lang("select"), 1=>lang("payroll_contracts_savings_account"), 2=>lang("payroll_contracts_current_account")/*, 3=>lang("payroll_contracts_account_virtual_wallet")*/]; ?>
                                            <?php $account_type = (!empty($_post)) ? $_post->account_type : $contract->account_type; ?>
                                            <?= form_dropdown(["name"=>"account_type", "id"=>"account_type", "class"=>"form-control not_select", "disabled"=>TRUE, "required"=>TRUE], $aopts, $account_type); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("account_no"), "account_no"); ?>
                                            <?php $account_no = (!empty($_post)) ? $_post->account_no : $contract->account_no; ?>
                                            <?= form_input(["name"=>"account_no", "id"=>"account_no", "type"=>"number", "class"=>"form-control", "disabled"=>TRUE, "required"=>TRUE], $account_no); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($concepts)) : ?>
                                    <hr>
                                    <h3>Bonificaciones</h3>
                                    <br>

                                    <?php $grid_size = 0; ?>
                                    <?php foreach ($concepts as $key => $concept) : ?>
                                        <?php $disabled = ($existingContractPreparationPayroll == true) ? 'disabled' : ''; ?>
                                        <?php if ($grid_size % 12 == 0) : ?> <div class="row"> <?php endif; ?>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="form-label"><?= $concept->name; ?></label>
                                                <div class="checkbox" style="margin-left: 10px; margin-top: 6px;">
                                                    <?php $checked = (!empty($contract_concepts[$concept->id])) ? 'checked' : ''; ?>
                                                    <input class="concepts" type="checkbox" name="concept_check[<?= $concept->id ?>]" id="concept_check_<?= $concept->id ?>" data-concept="<?= $concept->id ?>" value="<?= $concept->id ?>" <?= $checked ?> <?= $disabled ?>/> Activar
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="form-label">Monto</label>
                                                <?php $value = (!empty($contract_concepts[$concept->id])) ? $contract_concepts[$concept->id]->amount : 0; ?>
                                                <?php
                                                    if (empty($contract_concepts[$concept->id])) {
                                                        $disabled = 'disabled';
                                                    } else {
                                                        $disabled = '';
                                                    }
                                                ?>
                                                <input class="form-control" type="number" name="concept[<?= $concept->id ?>]" id="concept_<?= $concept->id ?>" value="<?= $value ?>" data-concept_id="<?= $concept->id ?>" <?= $disabled ?>/>
                                                <input type="hidden" name="concept_type[<?= $concept->id ?>]" id="concept_type_<?= $concept->id ?>" value="<?= $concept->concept_type_id; ?>"/>
                                            </div>
                                        </div>

                                        <?php $grid_size += 6; ?>
                                        <?php if ($grid_size % 12 == 0) : ?> </div> <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </section>
                        <h1><?= lang("payroll_contracts_social_security") ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("afp"), "afp"); ?>
                                        <?php $afopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($afps)):
                                                foreach ($afps as $afp) :
                                                    $afopts[$afp->id] = ucwords(mb_strtolower($afp->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?php $afp = (!empty($_post)) ? $_post->afp :$contract->afp_id; ?>
                                        <?= form_dropdown(["name"=>"afp", "id"=>"afp", "class"=>"form-control not_select", "required"=>TRUE], $afopts, $afp); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("eps"), "eps"); ?>
                                        <?php $eopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($epss)):
                                                foreach ($epss as $eps) :
                                                    $eopts[$eps->id] = ucwords(mb_strtolower($eps->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?php $eps = (!empty($_post)) ? $_post->eps : $contract->eps_id; ?>
                                        <?= form_dropdown(["name"=>"eps", "id"=>"eps", "class"=>"form-control not_select", "required"=>TRUE], $eopts, $eps); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("cesantia"), "cesantia"); ?>
                                        <?php $csopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($cesantias)):
                                                foreach ($cesantias as $cesantia) :
                                                    $csopts[$cesantia->id] = ucwords(mb_strtolower($cesantia->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?php $cesantia = (!empty($_post)) ? $_post->cesantia : $contract->cesantia_id; ?>
                                        <?= form_dropdown(["name"=>"cesantia", "id"=>"cesantia", "class"=>"form-control not_select", "required"=>TRUE], $csopts, $cesantia); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("caja"), "caja"); ?>
                                        <?php $copts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($cajas)):
                                                foreach ($cajas as $caja) :
                                                    $copts[$caja->id] = ucwords(mb_strtolower($caja->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?php $caja = (!empty($_post)) ? $_post->caja : $contract->caja_id; ?>
                                        <?= form_dropdown(["name"=>"caja", "id"=>"caja", "class"=>"form-control not_select", "required"=>TRUE], $copts, $caja); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("arl"), "arl"); ?>
                                        <?php $rlopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($arls)):
                                                foreach ($arls as $arl) :
                                                    $rlopts[$arl->id] = ucwords(mb_strtolower($arl->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?php $arl = (!empty($_post)) ? $_post->arl : $contract->arl_id; ?>
                                        <?= form_dropdown(["name"=>"arl", "id"=>"arl", "class"=>"form-control not_select", "required"=>TRUE], $rlopts, $arl); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("arl_risk_classes"), "arl_risk_classes"); ?>
                                        <?php $arcopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($arl_risk_classes)):
                                                foreach ($arl_risk_classes as $arl_risk_class) :
                                                    $arcopts[$arl_risk_class->id] = $arl_risk_class->name;
                                                endforeach;
                                            endif;
                                        ?>
                                        <?php $arl_risk_classes = (!empty($_post)) ? $_post->arl_risk_classes : $contract->arl_risk_classes; ?>
                                        <?= form_dropdown(["name"=>"arl_risk_classes", "id"=>"arl_risk_classes", "class"=>"form-control not_select", "required"=>TRUE], $arcopts, $arl_risk_classes); ?>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("icbf"), "icbf"); ?>
                                        <?php $iopts = [0=>lang("no"), 1=>lang("yes")]; ?>
                                        <?php $icbf = (!empty($_post)) ? $_post->icbf : $contract->icbf_id; ?>
                                        <?= form_dropdown(["name"=>"icbf", "id"=>"icbf", "class"=>"form-control not_select", "required"=>TRUE], $iopts, $icbf); ?>
                                    </div>
                                </div> -->
                            </div>
                            <!-- <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("sena"), "sena"); ?>
                                        <?php $sopts = [0=>lang("no"), 1=>lang("yes")]; ?>
                                        <?php $sena = (!empty($_post)) ? $_post->sena : $contract->sena_id; ?>
                                        <?= form_dropdown(["name"=>"sena", "id"=>"sena", "class"=>"form-control not_select", "required"=>TRUE], $sopts, $sena); ?>
                                    </div>
                                </div>
                            </div> -->
                        </section>

                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade"  id="add_professional_position_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Cargo <small id="area_name"></small></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label("Cargo", "new_professional_position"); ?>
                            <?= form_input(["name"=>"new_professional_position", "id"=>"new_professional_position", "class"=>"form-control"]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?= form_button(["class"=>"btn btn-default", "data-dismiss"=>"modal"], lang("cancel")); ?>
                <?= form_button(["id"=>"save_new_professional_position", "class"=>"btn btn-primary"], lang("save")); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        load_wizard_configuration();

        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();

        $(document).on('change', '#internal_code', function(){ validate_existing_internal_code(); });
        $(document).on('change', '#contract_type', function(){ enable_disable_end_date_input(); });
        $(document).on('change', '#start_date', function() { set_minimum_date_contract_end_date() });
        $(document).on('change', '#end_date', function() { set_maximun_date_contract_end_date() });
        $(document).on('change', '#area', function(){ load_professional_position(); });
        $(document).on('click', '#add_professional_position', function(){ add_professional_position(); });
        $(document).on('click', '#save_new_professional_position', function(){ save_new_professional_position(); });
        $(document).on('change', '#employee_type', function() { enable_disable_social_security_fields(); });
        $(document).on('change', '#base_amount', function() { enable_disable_integral_salary_fields(); });
        $(document).on('click', '#get_minimum_salary', function(event) { get_minimum_salary() });
        $(document).on('keydown', '#withholding_percentage', function(e) { validate_number_comma(e); });
        $(document).on('change', '#payment_method', function() { enable_disable_bank_data_fields(); });
        $(document).on('click', '#add_settlement_date', function() { enable_disable_settlement_date($(this)); });

        $('.concepts').on('ifChecked', function(event) {
            var concept_id = $(this).data('concept');
            $('#concept_'+concept_id).attr({'disabled': false, 'required': true});
        });

        $('.concepts').on('ifUnchecked', function(event) {
            var concept_id = $(this).data('concept');
            $('#concept_'+concept_id).attr({'disabled': true, 'required': false, 'value': 0});
        });

        $('#start_date').trigger('change');
        $('#end_date').trigger('change');
        $('#internal_code').trigger('change');
        $('#contract_type').trigger('change');
        $('#workday').trigger('change');
        $('#area').trigger('change');
        $('#employee_type').trigger('change');
        $('#base_amount').trigger('change');
        $('#payment_method').trigger('change');
    });

    function load_wizard_configuration()
    {
        var form = $("#edit_contract_form");
        wizard = form.steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: '<i class="fa-solid fa-check" data-toggle="tooltip" title="Guardar" data-placement="top"></i>',
                next: '<i class="fa-solid fa-angles-right" data-toggle="tooltip" title="Siguiente" data-placement="top"></i>',
                previous: '<i class="fa-solid fa-angles-left"  data-toggle="tooltip" title="Anterior" data-placement="top"></i>',
                loading: "Loading ..."
            },
            onInit: function(event, currentIndex)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate({
                    highlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                        } else {
                            elem.addClass(errorClass);
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                        } else {
                            elem.removeClass(errorClass);
                        }
                    }
                }).settings.ignore = ":disabled,:hidden:not(.validate)";
                return form.valid();
            },
            onStepChanged: function(event)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                if(form.valid()){
                    $(form).submit();
                }
            }
        });

        $(document).on("select2-opening", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror")) {
                $(".select2-drop ul").addClass("mierror");
            } else {
                $(".select2-drop ul").removeClass("mierror");
            }
        });

        $(document).on("select2-close", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror") && $("#" + elem.attr("id")).val() != '') {
                $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');

                $("#" + elem.attr("id") + "-error").css('display', 'none');
            }
        });
    }

    function validate_existing_internal_code()
    {
        var internal_code = $('#internal_code').val();
        $.ajax({
            url: '<?= admin_url("payroll_contracts/validate_existing_internal_code"); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                'internal_code': internal_code,
                'employee_id': '<?= $contract->companies_id; ?>'
            },
        })
        .done(function(data) {
            if (data == true) {
                command:toastr.error('<?= lang("payroll_contracts_existing_internal_code"); ?>', '<?= lang("toast_error_title") ?>', {onHidden:function() { $("#internal_code").val(''); $("#internal_code").focus(); }});
            }
        })
        .fail(function(data) {
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function enable_disable_end_date_input()
    {
        var type_contract = $('#contract_type').val();

        if (type_contract == '<?= UNDEFINED_TERM ?>') {
            $('#end_date').prop('disabled', true);
        } else {
            $('#end_date').prop('disabled', false);
        }
    }

    function set_minimum_date_contract_end_date()
    {
        var start_date = $('#start_date').val();
        $('#end_date').prop('min', start_date);
        $('#settlement_date').prop('min', start_date);
    }

    function set_maximun_date_contract_end_date()
    {
        var end_date = $('#end_date').val();
        $('#start_date').prop('max', end_date);
        $('#settlement_date').prop('max', end_date);
    }

    function load_professional_position()
    {
        var area = $('#area').val();

        $.ajax({
            url: '<?= admin_url("payroll_contracts/get_professional_position") ?>',
            type: 'GET',
            dataType: 'html',
            data: {
                'area': area
            },
        })
        .done(function(data) {
            $('#professional_position').html(data);
            $('#professional_position').select2('val', "");
            <?php if (!empty($contract->professional_position)) : ?>
                $('#professional_position').select2('val', "<?= $contract->professional_position; ?>");
            <?php endif; ?>
        })
        .fail(function(data) {
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function add_professional_position()
    {
        if ($('#area').val() != '') {
            $('#add_professional_position_modal').modal({backdrop: 'static', keyboard: false});
            $('#area_name').html($('#area option:selected').text());
        } else {
            $('#area').select2('focus');
        }
    }

    function save_new_professional_position()
    {
        $.ajax({
            url: '<?= admin_url("professional_positions/save") ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                "area_id": $('#area').val(),
                "new_professional_position": $('#new_professional_position').val()
            },
        })
        .done(function(response) {
            if (response.status == true) {
                command:toastr.success(response.message, '<?= lang("toast_success_title"); ?>', {onHidden: function(){
                    $('#professional_position').html(response.data);
                    $('#professional_position').select2('val', response.option_selected);

                    $('#add_professional_position_modal').modal('hide');
                }});
            } else {
                command:toastr.error(response.message, '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
            }
        })
        .fail(function(data) {
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function validate_number_comma(event)
    {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 190 || event.keyCode == 110 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode == 67 && event.ctrlKey === true) || (event.keyCode == 86 && event.ctrlKey === true) || (event.keyCode == 88 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 40) ) {

            return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    }

    function enable_disable_social_security_fields()
    {
        var afp_id = $('#employee_type option:selected').data('afp_id');
        if (afp_id == '<?= YES ?>') { $('#afp_id').prop('disabled', false); } else { $('#afp_id').prop('disabled', true); }
        var eps_id = $('#employee_type option:selected').data('eps_id');
        if (eps_id == '<?= YES ?>') { $('#eps_id').prop('disabled', false); } else { $('#eps_id').prop('disabled', true); }
        var arl_id = $('#employee_type option:selected').data('arl_id');
        if (arl_id == '<?= YES ?>') { $('#arl_id').prop('disabled', false); } else { $('#arl_id').prop('disabled', true); }
        var caja_id = $('#employee_type option:selected').data('caja_id');
        if (caja_id == '<?= YES ?>') { $('#caja_id').prop('disabled', false); } else { $('#caja_id').prop('disabled', true); }
        var icbf_id = $('#employee_type option:selected').data('icbf_id');
        if (icbf_id == '<?= YES ?>') { $('#icbf_id').prop('disabled', false); } else { $('#icbf_id').prop('disabled', true); }
        var sena_id = $('#employee_type option:selected').data('sena_id');
        if (sena_id == '<?= YES ?>') { $('#sena_id').prop('disabled', false); } else { $('#sena_id').prop('disabled', true); }
        var cesantia_id = $('#employee_type option:selected').data('cesantias_id');
        if (cesantia_id == '<?= YES ?>') { $('#cesantia_id').prop('disabled', false); } else { $('#cesantia_id').prop('disabled', true); }
    }

    function enable_disable_integral_salary_fields()
    {
        var minimumSalaryValue = <?= $this->Payroll_settings->minimum_salary_value; ?>;
        var APPRENTICES_SENA_PRODUCTIVE = <?= APPRENTICES_SENA_PRODUCTIVE ?>;
        var APPRENTICES_SENA_LECTIVA = <?= APPRENTICES_SENA_LECTIVA ?>;
        var employeeType = $('#employee_type').val();
        var base_amount = $('#base_amount').val();
        var workday = $('#workday').val();
        var HALFTIME = <?= HALFTIME ?>;

        if (employeeType == APPRENTICES_SENA_LECTIVA || employeeType == APPRENTICES_SENA_PRODUCTIVE) {
            var percentageSalaryApprenticeTeaching = <?= $this->Payroll_settings->percentage_salary_apprentice_teaching; ?>;
            var percentageSalaryApprenticeProductive = <?= $this->Payroll_settings->percentage_salary_apprentice_productive; ?>;

            if (employeeType == APPRENTICES_SENA_LECTIVA) {
                minimumValueApprenticeSalary = minimumSalaryValue * (percentageSalaryApprenticeTeaching / 100);
            } else {
                minimumValueApprenticeSalary = minimumSalaryValue * (percentageSalaryApprenticeProductive / 100);
            }

            minimumSalaryValue = minimumValueApprenticeSalary;
        } else if (workday == HALFTIME) {
            minimumSalaryValue = minimumSalaryValue / 2;
        }

        if (base_amount > 0 && base_amount < minimumSalaryValue) {
            command: toastr.error('El valor del salario no puede ser menor al Salario mínimo estipulado por ley', '¡Error!', {onHidden: function() { $('#base_amount').val(''); }});
        }

        if (base_amount > <?= $this->Payroll_settings->integral_salary_value; ?>) {
            $('#integral_salary').prop('disabled', false);
        } else {
            $('#integral_salary').select2('val', '<?= NOT; ?>');
            $('#integral_salary').prop('disabled', true);
        }

        var minimum_salary_value_to_trans_allowance = <?= $this->Payroll_settings->minimum_salary_value * 2 ?>;
        if (base_amount > minimum_salary_value_to_trans_allowance) {
            $('#trans_allowance').select2('val', '<?= NOT; ?>');
            $('#trans_allowance').prop('disabled', true);
        } else {
            $('#trans_allowance').prop('disabled', false);
            $('#trans_allowance').select2('val', '<?= YES; ?>');
        }
    }

    function get_minimum_salary() {
        if ('<?= $existingContractPreparationPayroll ?>' != 1) {
            $('#base_amount').val(<?= $this->Payroll_settings->minimum_salary_value; ?>);
            $('#base_amount').trigger('change');
            $('#base_amount').trigger('blur');
        }
    }

    function enable_disable_bank_data_fields()
    {
        var payment_method = $('#payment_method').val();
        if (payment_method == '<?= WIRE_TRANSFER; ?>') {
            $('#bank').prop('disabled', false);
            $('#account_type').prop('disabled', false);
            $('#account_no').prop('disabled', false);
        } else {
            $('#bank').prop('disabled', true);
            $('#account_type').prop('disabled', true);
            $('#account_no').prop('disabled', true);
        }
    }

    function enable_disable_settlement_date(control)
    {
        var disabled = $('#settlement_date').attr('disabled');
        if (disabled == 'disabled') {
            $('#settlement_date').attr('disabled', false);
            $('#settlement_date').attr('max', '<?= date("Y-m-d") ?>');
            control.attr('data-original-title', 'Quitar fecha de liquidación');
        } else {
            $('#settlement_date').val('');
            $('#settlement_date').attr('disabled', true);
            control.attr('data-original-title', 'Agregar fecha de liquidación');
        }
    }
</script>
