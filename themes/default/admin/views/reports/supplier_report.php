<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row wrapper border-bottom white-bg page-heading no-printable">
    <div class="col-lg-12">
        <h2>
            <?php if ($supplier->type_person == NATURAL_PERSON) { ?>
                <?=lang('supplier')." : ".$supplier->name ?>
                <?php } else { ?>
                    <?=lang('supplier')." : ".$supplier->name ?>
                    <?=lang('social_reason')." : ".$supplier->company ?>
            <?php } ?>
        </h2>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="small-box padding1010 col-sm-4 bblue">
                    <h3><?= isset($purchases->total_amount) ? $this->sma->formatMoney($purchases->total_amount) : '0.00' ?></h3>

                    <p><?= lang('purchases_amount') ?></p>
                </div>
                <div class="small-box padding1010 col-sm-4 blightOrange">
                    <h3><?= isset($purchases->paid) ? $this->sma->formatMoney($purchases->paid) : '0.00' ?></h3>

                    <p><?= lang('total_paid') ?></p>
                </div>
                <div class="small-box padding1010 col-sm-4 borange">
                    <h3><?= (isset($purchases->total_amount) || isset($purchases->paid)) ? $this->sma->formatMoney($purchases->total_amount - $purchases->paid) : '0.00' ?></h3>

                    <p><?= lang('due_amount') ?></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="small-box padding1010 bblue">
                            <div class="inner clearfix">
                                <a>
                                    <h3><?= $total_purchases ?></h3>

                                    <p><?= lang('total_purchases') ?></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<ul id="myTab" class="nav nav-tabs no-print">
    <li class=""><a href="#purchases_summary-con" class="tab-grey"><?= lang('purchases_summary') ?></a></li>
    <li class=""><a href="#purcahses-con" class="tab-grey"><?= lang('purchases_detail') ?></a></li>
    <li class=""><a href="#payments-con" class="tab-grey"><?= lang('payments') ?></a></li>
    <li class=""><a href="#movements-con" class="tab-grey"><?= lang('supplier_movements') ?></a></li>
</ul>

<div class="tab-content">
    <div id="purchases_summary-con" class="tab-pane fade in">
        <?php
        $v2 = "&supplier_summary=" . $user_id;
        if ($this->input->post('submit_purchase_summary_report')) {
            if ($this->input->post('biller_summary')) {
                $v2 .= "&biller_summary=" . $this->input->post('biller_summary');
            }
            if ($this->input->post('warehouse_summary')) {
                $v2 .= "&warehouse_summary=" . $this->input->post('warehouse_summary');
            }
            if ($this->input->post('user_summary')) {
                $v2 .= "&user_summary=" . $this->input->post('user_summary');
            }
            if ($this->input->post('start_date_summary')) {
                $v2 .= "&start_date_summary=" . $this->input->post('start_date_summary');
            }
            if ($this->input->post('end_date_summary')) {
                $v2 .= "&end_date_summary=" . $this->input->post('end_date_summary');
            }
            if ($this->input->post('purchase_summary_portfolio')) {
                $v2 .= "&purchase_summary_portfolio=" . $this->input->post('purchase_summary_portfolio');
            }
        }
        ?>
        <script>
        $(document).ready(function () {
            oTable = $('#PoRDataSummary').dataTable({
                "aaSorting": [[0, "desc"]],
                "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('reports/getPurchasesSummaryReport/?v=1' . $v2) ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                    nRow.id = aData[8];
                    nRow.className = "purchase_link2";
                    return nRow;
                },
                "aoColumns": [
                                {"mRender": fld},
                                null,
                                null,
                                null,
                                {"mRender": currencyFormat},
                                {"mRender": currencyFormat},
                                {"mRender": currencyFormat},
                                {"mRender": row_status}
                            ],
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    var gtotal = 0, paid = 0, balance = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        gtotal += parseFloat(aaData[aiDisplay[i]][3]);
                        paid += parseFloat(aaData[aiDisplay[i]][4]);
                        balance += parseFloat(aaData[aiDisplay[i]][5]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[3].innerHTML = currencyFormat(parseFloat(gtotal));
                    nCells[4].innerHTML = currencyFormat(parseFloat(paid));
                    nCells[5].innerHTML = currencyFormat(parseFloat(balance));
                }
            }).fnSetFilteringDelay().dtFilter([
                {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                {column_number: 2, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
                {column_number: 3, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
                {column_number: 6, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
            ], "footer");
        });
        </script>
        <script type="text/javascript">
        $(document).ready(function () {
            $('#form').hide();
            $('.toggle_down_summary').click(function () {
                $("#form_summary").slideDown();
                return false;
            });
            $('.toggle_up_summary').click(function () {
                $("#form_summary").slideUp();
                return false;
            });
        });
        </script>

            <div class="box purchases-table">
                <div class="box-header">
                    <div class="box-icon">
                        <ul class="btn-tasks">
                            <li class="dropdown">
                                <a href="#" class="toggle_up_summary tip" title="<?= lang('hide_form') ?>">
                                    <i class="icon fa fa-toggle-up"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="toggle_down_summary tip" title="<?= lang('show_form') ?>">
                                    <i class="icon fa fa-toggle-down"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="box-icon">
                        <ul class="btn-tasks">
                            <li class="dropdown">
                                <a href="#" id="xls_2" class="tip" title="<?= lang('download_xls') ?>">
                                    <i class="icon fa fa-file-excel-o"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                    <i class="icon fa fa-file-picture-o"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="introtext"><?= lang('customize_report'); ?></p>

                            <div id="form_summary">

                                <?php echo admin_form_open("reports/supplier_report/" . $user_id."/#purchases_summary-con", 'id="form_filter"'); ?>
                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="user_summary"><?= lang("created_by"); ?></label>
                                            <?php
                                            $us[""] = lang('select').' '.lang('user');
                                            foreach ($users as $user) {
                                                $us[$user->id] = $user->first_name . " " . $user->last_name;
                                            }
                                            echo form_dropdown('user_summary', $us, (isset($_POST['user_summary']) ? $_POST['user_summary'] : ""), 'class="form-control" id="user_summary" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="warehouse_summary"><?= lang("warehouse"); ?></label>
                                            <?php
                                            $wh[""] = lang('select').' '.lang('warehouse');
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse_summary', $wh, (isset($_POST['warehouse_summary']) ? $_POST['warehouse_summary'] : ""), 'class="form-control" id="warehouse_summary" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="date_controls_summary">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("start_date", "start_date_summary"); ?>
                                                <?php echo form_input('start_date_summary', (isset($_POST['start_date_summary']) ? $_POST['start_date_summary'] : ""), 'class="form-control datetime" id="start_date_summary" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("end_date", "end_date_summary"); ?>
                                                <?php echo form_input('end_date_summary', (isset($_POST['end_date_summary']) ? $_POST['end_date_summary'] : ""), 'class="form-control datetime" id="end_date_summary" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('date_records_filter', 'date_records_filter_summary') ?>
                                        <select name="date_records_filter_summary" id="date_records_filter_summary" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="purchase_summary_portfolio" value="1" <?= isset($_POST['purchase_summary_portfolio']) && $_POST['purchase_summary_portfolio'] ? "checked='checked'" : "" ?>>
                                                <?= lang('purchase_summary_portfolio') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div
                                    class="controls"> <?php echo form_submit('submit_purchase_summary_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_purchase_summary_report"'); ?> </div>
                                </div>
                                <?php echo form_close(); ?>

                            </div>
                            <div class="clearfix"></div>


                            <div class="table-responsive">
                                <table id="PoRDataSummary"
                                class="table table-bordered table-hover table-condensed reports-table">
                                <thead>
                                    <tr>
                                        <th><?= lang("date"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("warehouse"); ?></th>
                                        <th><?= lang("supplier_name"); ?></th>
                                        <th><?= lang("TOTAL"); ?></th>
                                        <th><?= lang("paid"); ?></th>
                                        <th><?= lang("balance"); ?></th>
                                        <th><?= lang("status"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                    <tr class="active">
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("warehouse"); ?></th>
                                            <th><?= lang("supplier_name"); ?></th>
                                            <th><?= lang("TOTAL"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="purcahses-con" class="tab-pane fade in">
        <?php
        $v = "&supplier=" . $user_id;
        if ($this->input->post('submit_purchase_report')) {
            if ($this->input->post('biller')) {
                $v .= "&biller=" . $this->input->post('biller');
            }
            if ($this->input->post('warehouse')) {
                $v .= "&warehouse=" . $this->input->post('warehouse');
            }
            if ($this->input->post('user')) {
                $v .= "&user=" . $this->input->post('user');
            }
            if ($this->input->post('start_date')) {
                $v .= "&start_date=" . $this->input->post('start_date');
            } else {
                $v .= "&start_date=NULL";
            }
            if ($this->input->post('end_date')) {
                $v .= "&end_date=" . $this->input->post('end_date');
            }
        }
        ?>
        <script>
        $(document).ready(function () {


            $('#date_records_filter_summary').on('change', function(){
                filter = $(this).val();
                fecha_inicial = "";
                fecha_final = "<?= date('d/m/Y 23:59') ?>";
                hide_date_controls = true;
                if (filter == 5) { //RANGO DE FECHAS
                    fecha_final = "";
                    hide_date_controls = false;
                } else if (filter == 1) { // HOY
                    fecha_inicial = "<?= date('d/m/Y 00:00') ?>";
                } else if (filter == 2) { // MES
                    fecha_inicial = "<?= date('01/m/Y 00:00') ?>";
                } else if (filter == 3) { // TRIMESTRE
                    fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                } else if (filter == 4) { // AÑO
                    fecha_inicial = "<?= date('01/01/Y 00:00') ?>";
                }else if (filter == 6) { // MES PASADO
                    fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 1 month")) ?>";
                    fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                }else if (filter == 7) { // AÑO PASADO
                    fecha_inicial = "<?= date("01/01/Y 00:00",strtotime(date("Y-m-01")."- 1 year")) ?>";
                    fecha_final = "<?= date("31/12/Y 23:59",strtotime(date("Y-m-01")."- 1 year")) ?>";
                } else if (filter == 0) {
                    fecha_inicial = "";
                    fecha_final = "";
                    hide_date_controls = false;
                }
                <?php if (!isset($_POST['start_date_summary'])): ?>
                    $('#start_date_summary').val(fecha_inicial);
                    $('#end_date_summary').val(fecha_final);
                <?php endif ?>
                if (hide_date_controls) {
                    $('.date_controls_summary').css('display', 'none');
                } else {
                    $('.date_controls_summary').css('display', '');
                }
            });
            <?php if (!isset($_POST['start_date_summary']) && !isset($_POST['pay_start_date']) && !isset($_POST['start_date']) && !isset($_POST['movements_start_date'])) { ?>
                $('#date_records_filter_summary').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#submit_purchase_summary_report').click();
            <?php } else { ?>
                $('#date_records_filter_summary').val("<?= isset($_POST['date_records_filter_summary']) ? $_POST['date_records_filter_summary'] : $this->Settings->default_records_filter ?>").trigger('change');
            <?php } ?>



            oTable = $('#PoRData').dataTable({
                "aaSorting": [[0, "desc"]],
                "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('reports/getPurchasesReport/?v=1' . $v) ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                    nRow.id = aData[10];
                    nRow.className = "purchase_link2";
                    return nRow;
                },
                "aoColumns": [
                                {"mRender": fld},
                                null,
                                null,
                                null,
                                {"bSearchable": false, "mRender": pqFormat},
                                {"mRender": pqFormat},
                                {"mRender": currencyFormat},
                                {"mRender": currencyFormat},
                                {"mRender": currencyFormat},
                                {"mRender": row_status}
                            ],
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    var gtotal = 0, paid = 0, balance = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                        paid += parseFloat(aaData[aiDisplay[i]][6]);
                        balance += parseFloat(aaData[aiDisplay[i]][7]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[5].innerHTML = currencyFormat(parseFloat(gtotal));
                    nCells[6].innerHTML = currencyFormat(parseFloat(paid));
                    nCells[7].innerHTML = currencyFormat(parseFloat(balance));
                }
            }).fnSetFilteringDelay().dtFilter([
                {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                {column_number: 2, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
                {column_number: 3, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
                {column_number: 8, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
            ], "footer");
            
        <?php if ($this->session->userdata('warehouse_id')): ?>
            setTimeout(function() {
                $('#warehouse_summary').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
                $('#warehouse').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
                $('#warehouse_summary').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
        });
        </script>
        <script type="text/javascript">
        $(document).ready(function () {
            $('#form').hide();
            $('.toggle_down').click(function () {
                $("#form").slideDown();
                return false;
            });
            $('.toggle_up').click(function () {
                $("#form").slideUp();
                return false;
            });
        });
        </script>

            <div class="box purchases-table">
                <div class="box-header">
                    <div class="box-icon">
                        <ul class="btn-tasks">
                            <li class="dropdown">
                                <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                                    <i class="icon fa fa-toggle-up"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                                    <i class="icon fa fa-toggle-down"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="box-icon">
                        <ul class="btn-tasks">
                            <li class="dropdown">
                                <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                    <i class="icon fa fa-file-excel-o"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                    <i class="icon fa fa-file-picture-o"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="introtext"><?= lang('customize_report'); ?></p>

                            <div id="form">

                                <?php echo admin_form_open("reports/supplier_report/" . $user_id."/#purcahses-con"); ?>
                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                            <?php
                                            $us[""] = lang('select').' '.lang('user');
                                            foreach ($users as $user) {
                                                $us[$user->id] = $user->first_name . " " . $user->last_name;
                                            }
                                            echo form_dropdown('user', $us, (isset($_POST['user']) ? $_POST['user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                            <?php
                                            $wh[""] = lang('select').' '.lang('warehouse');
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('date_records_filter', 'date_records_filter') ?>
                                        <select name="date_records_filter" id="date_records_filter" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>
                                    <div class="date_controls">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("start_date", "start_date"); ?>
                                                <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("end_date", "end_date"); ?>
                                                <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div
                                    class="controls"> <?php echo form_submit('submit_purchase_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                                </div>
                                <?php echo form_close(); ?>

                            </div>
                            <div class="clearfix"></div>


                            <div class="table-responsive">
                                <table id="PoRData"
                                class="table table-bordered table-hover table-condensed reports-table">
                                <thead>
                                    <tr>
                                        <th><?= lang("date"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("warehouse"); ?></th>
                                        <th><?= lang("supplier_name"); ?></th>
                                        <th><?= lang("product"); ?></th>
                                        <th><?= lang("product_qty"); ?></th>
                                        <th><?= lang("TOTAL"); ?></th>
                                        <th><?= lang("paid"); ?></th>
                                        <th><?= lang("balance"); ?></th>
                                        <th><?= lang("status"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                    <tr class="active">
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("warehouse"); ?></th>
                                            <th><?= lang("supplier_name"); ?></th>
                                            <th><?= lang("product"); ?></th>
                                            <th><?= lang("product_qty"); ?></th>
                                            <th><?= lang("TOTAL"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="payments-con" class="tab-pane fade in">
        <?php
        $p = "&supplier=" . $user_id;
        if ($this->input->post('submit_payment_report')) {
            if ($this->input->post('pay_user')) {
                $p .= "&user=" . $this->input->post('pay_user');
            }
            if ($this->input->post('pay_start_date')) {
                $p .= "&start_date=" . $this->input->post('pay_start_date');
            } else {
                    $p .= "&start_date=NULL";
                }
            if ($this->input->post('pay_end_date')) {
                $p .= "&end_date=" . $this->input->post('pay_end_date');
            }
        }
        ?>
        <script>
        $(document).ready(function () {


            $('#date_records_filter').on('change', function(){
                filter = $(this).val();
                fecha_inicial = "";
                fecha_final = "<?= date('d/m/Y 23:59') ?>";
                hide_date_controls = true;
                if (filter == 5) { //RANGO DE FECHAS
                    fecha_final = "";
                    hide_date_controls = false;
                } else if (filter == 1) { // HOY
                    fecha_inicial = "<?= date('d/m/Y 00:00') ?>";
                } else if (filter == 2) { // MES
                    fecha_inicial = "<?= date('01/m/Y 00:00') ?>";
                } else if (filter == 3) { // TRIMESTRE
                    fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                } else if (filter == 4) { // AÑO
                    fecha_inicial = "<?= date('01/01/Y 00:00') ?>";
                }else if (filter == 6) { // MES PASADO
                    fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 1 month")) ?>";
                    fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                }else if (filter == 7) { // AÑO PASADO
                    fecha_inicial = "<?= date("01/01/Y 00:00",strtotime(date("Y-m-01")."- 1 year")) ?>";
                    fecha_final = "<?= date("31/12/Y 23:59",strtotime(date("Y-m-01")."- 1 year")) ?>";
                } else if (filter == 0) {
                    fecha_inicial = "";
                    fecha_final = "";
                    hide_date_controls = false;
                }
                <?php if (!isset($_POST['start_date'])): ?>
                    $('#start_date').val(fecha_inicial);
                    $('#end_date').val(fecha_final);
                <?php endif ?>
                if (hide_date_controls) {
                    $('.date_controls').css('display', 'none');
                } else {
                    $('.date_controls').css('display', '');
                }
            });
            <?php if (!isset($_POST['start_date']) ) { ?>
                $('#date_records_filter').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
            <?php } else { ?>
                $('#date_records_filter').val("<?= isset($_POST['date_records_filter']) ? $_POST['date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
            <?php } ?>

            var pb = <?= json_encode($pb); ?>;
            function paid_by(x) {
                return (x != null) ? (pb[x] ? pb[x] : x) : x;
            }

            oTable = $('#PayRData').dataTable({
                "aaSorting": [[0, "desc"]],
                "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('reports/getPaymentsReport/?v=1' . $p) ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                "aoColumns": [{"mRender": fld}, null, {"bVisible": false}, null, {"mRender": paid_by}, {"mRender": currencyFormat}, {"mRender": row_status}],
                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                    nRow.id = aData[7];
                    nRow.className = "payment_link2 warning";
                    return nRow;
                },
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    var total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        total += parseFloat(aaData[aiDisplay[i]][5]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[4].innerHTML = currencyFormat(parseFloat(total));
                }
            }).fnSetFilteringDelay().dtFilter([
                {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                {column_number: 1, filter_default_label: "[<?=lang('payment_ref');?>]", filter_type: "text", data: []},
                {column_number: 3, filter_default_label: "[<?=lang('purchase_ref');?>]", filter_type: "text", data: []},
                {column_number: 4, filter_default_label: "[<?=lang('paid_by');?>]", filter_type: "text", data: []},
                {column_number: 6, filter_default_label: "[<?=lang('type');?>]", filter_type: "text", data: []},
            ], "footer");

        });
        </script>
        <script type="text/javascript">
        $(document).ready(function () {
            $('#payform').hide();
            $('.paytoggle_down').click(function () {
                $("#payform").slideDown();
                return false;
            });
            $('.paytoggle_up').click(function () {
                $("#payform").slideUp();
                return false;
            });
        });
        </script>

        <div class="box payments-table">
            <div class="box-header">
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" class="paytoggle_up tip" title="<?= lang('hide_form') ?>">
                                <i class="icon fa fa-toggle-up"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="paytoggle_down tip" title="<?= lang('show_form') ?>">
                                <i class="icon fa fa-toggle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf1" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls1" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image1" class="tip" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">

                        <p class="introtext"><?= lang('customize_report'); ?></p>

                        <div id="payform">

                            <?php echo admin_form_open("reports/supplier_report/" . $user_id."/#payments-con"); ?>
                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                        <?php
                                        $us[""] = lang('select').' '.lang('user');
                                        foreach ($users as $user) {
                                            $us[$user->id] = $user->first_name . " " . $user->last_name;
                                        }
                                        echo form_dropdown('pay_user', $us, (isset($_POST['pay_user']) ? $_POST['pay_user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="pay_date_controls">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("start_date", "pay_start_date"); ?>
                                            <?php echo form_input('pay_start_date', (isset($_POST['pay_start_date']) ? $_POST['pay_start_date'] : ""), 'class="form-control datetime" id="pay_start_date" autocomplete="off"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("end_date", "pay_end_date"); ?>
                                            <?php echo form_input('pay_end_date', (isset($_POST['pay_end_date']) ? $_POST['pay_end_date'] : ""), 'class="form-control datetime" id="pay_end_date" autocomplete="off"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('date_records_filter', 'date_records_filter') ?>
                                    <select name="pay_date_records_filter" id="pay_date_records_filter" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div
                                class="controls"> <?php echo form_submit('submit_payment_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                            </div>
                            <?php echo form_close(); ?>

                        </div>
                        <div class="clearfix"></div>

                        <div class="table-responsive">
                            <table id="PayRData"
                            class="table table-bordered table-hover table-condensed reports-table">

                            <thead>
                                <tr>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("payment_ref"); ?></th>
                                    <th><?= lang("sale_ref"); ?></th>
                                    <th><?= lang("purchase_ref"); ?></th>
                                    <th><?= lang("paid_by"); ?></th>
                                    <th><?= lang("amount"); ?></th>
                                    <th><?= lang("type"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                </tr>
                            </tbody>
                            <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= lang("amount"); ?></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div id="movements-con" class="tab-pane fade in">
        <script type="text/javascript">
        $(document).ready(function () {

            $('#movementform').hide();
            $('.movementtoggle_down').click(function () {
                $("#movementform").slideDown();
                return false;
            });
            $('.movementtoggle_up').click(function () {
                $("#movementform").slideUp();
                return false;
            });

            $.ajax({
                url: '<?= admin_url('reports/get_supplier_movements/'.$user_id) ?>',
                type : 'POST',
                dataType : 'JSON',
                data : {
                            "<?= $this->security->get_csrf_token_name() ?>" :  "<?= $this->security->get_csrf_hash() ?>",
                            "start_date" : '<?= $this->input->post("movements_start_date") ?>',
                            "end_date" : '<?= $this->input->post("movements_end_date") ?>',
                            // "warehouse_id" : $('#kardex_warehouse_id').val(),
                        }
            }).done(function(data){

                response = data;
                // $('.kardex_balance').text('<?= lang('kardex_product_balance') ?>'+response.balance);
                $('#movementsData tbody').html(response.html);
                    kardexTable = $('#movementsData').dataTable({
                        "aaSorting": [],
                        "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                        "iDisplayLength": <?= $Settings->rows_per_page ?>,
                        'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                            // console.log(aData);
                        },
                        "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                            // var qty = 0;
                        }
                    }).fnSetFilteringDelay().dtFilter([
                        {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                        {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                        {column_number: 2, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
                        {column_number: 3, filter_default_label: "[<?=lang('created_by');?>]", filter_type: "text", data: []},
                    ], "footer");
                    $('#loading').fadeOut();
            });
        });
        </script>
        <div class="box">
            <div class="box-header">
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" class="movementtoggle_up tip" title="<?= lang('hide_form') ?>">
                                <i
                                class="icon fa fa-toggle-up"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="movementtoggle_down tip" title="<?= lang('show_form') ?>">
                                <i
                                class="icon fa fa-toggle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <!-- <li class="dropdown">
                            <a href="#" id="pdf1" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls1" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image1" class="tip image" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li> -->
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>

                        <div id="movementform">

                            <?php echo admin_form_open("reports/supplier_report/" . $user_id."/#movements-con"); ?>
                            <div class="row">

                                <div class="movements_date_controls">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("start_date", "movements_start_date"); ?>
                                            <?php echo form_input('movements_start_date', (isset($_POST['movements_start_date']) ? $_POST['movements_start_date'] : ""), 'class="form-control datetime" id="movements_start_date" autocomplete="off"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("end_date", "movements_end_date"); ?>
                                            <?php echo form_input('movements_end_date', (isset($_POST['movements_end_date']) ? $_POST['movements_end_date'] : ""), 'class="form-control datetime" id="movements_end_date" autocomplete="off"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('date_records_filter', 'movements_date_records_filter') ?>
                                    <select name="movements_date_records_filter" id="movements_date_records_filter" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div
                                class="controls"> <?php echo form_submit('submit_movements_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                            </div>
                            <?php echo form_close(); ?>

                        </div>

                        <div class="table-responsive">
                            <table id="movementsData" class="table table-bordered table-condensed table-hover reports-table">
                                <thead>
                                <tr class="primary">
                                    <th class="col-xs-2"><?= lang("date"); ?></th>
                                    <th class="col-xs-2"><?= lang("reference_no"); ?></th>
                                    <th class="col-xs-3"><?= lang("addition"); ?></th>
                                    <th class="col-xs-3"><?= lang("substraction"); ?></th>
                                    <th class="col-xs-2"><?= lang("balance"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr class="primary">
                                    <th class="col-xs-2"></th>
                                    <th class="col-xs-2"></th>
                                    <th class="col-xs-3"></th>
                                    <th class="col-xs-3"></th>
                                    <th class="col-xs-2"></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">

                $('#movements_date_records_filter').on('change', function(){
                    filter = $(this).val();
                    fecha_inicial = "";
                    fecha_final = "<?= date('d/m/Y 23:59') ?>";
                    hide_date_controls = true;
                    if (filter == 5) { //RANGO DE FECHAS
                        fecha_final = "";
                        hide_date_controls = false;
                    } else if (filter == 1) { // HOY
                        fecha_inicial = "<?= date('d/m/Y 00:00') ?>";
                    } else if (filter == 2) { // MES
                        fecha_inicial = "<?= date('01/m/Y 00:00') ?>";
                    } else if (filter == 3) { // TRIMESTRE
                        fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                    } else if (filter == 4) { // AÑO
                        fecha_inicial = "<?= date('01/01/Y 00:00') ?>";
                    }else if (filter == 6) { // MES PASADO
                        fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 1 month")) ?>";
                        fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                    }else if (filter == 7) { // AÑO PASADO
                        fecha_inicial = "<?= date("01/01/Y 00:00",strtotime(date("Y-m-01")."- 1 year")) ?>";
                        fecha_final = "<?= date("31/12/Y 23:59",strtotime(date("Y-m-01")."- 1 year")) ?>";
                    } else if (filter == 0) {
                        fecha_inicial = "";
                        fecha_final = "";
                        hide_date_controls = false;
                    }
                    <?php if (!isset($_POST['movements_start_date'])): ?>
                        $('#movements_start_date').val(fecha_inicial);
                        $('#movements_end_date').val(fecha_final);
                    <?php endif ?>
                    if (hide_date_controls) {
                        $('.movements_date_controls').css('display', 'none');
                    } else {
                        $('.movements_date_controls').css('display', '');
                    }
                });
                <?php if (!isset($_POST['movements_start_date'])) { ?>
                    $('#movements_date_records_filter').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
                <?php } else { ?>
                    $('#movements_date_records_filter').val("<?= isset($_POST['movements_date_records_filter']) ? $_POST['movements_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
                <?php } ?>
        </script>
    </div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#pdf').click(function (event) {
        event.preventDefault();
        window.location.href = "<?=admin_url('reports/getPurchasesReport/pdf/?v=1'.$v)?>";
        return false;
    });
    $('#xls').click(function (event) {
        event.preventDefault();
        window.location.href = "<?=admin_url('reports/getPurchasesReport/0/xls/?v=1'.$v)?>";
        return false;
    });
    $('#image').click(function (event) {
        event.preventDefault();
        html2canvas($('.purchases-table'), {
            onrendered: function (canvas) {
                openImg(canvas.toDataURL());
            }
        });
        return false;
    });
    $('#pdf1').click(function (event) {
        event.preventDefault();
        window.location.href = "<?=admin_url('reports/getPaymentsReport/pdf/?v=1'.$p)?>";
        return false;
    });
    $('#xls1').click(function (event) {
        event.preventDefault();
        window.location.href = "<?=admin_url('reports/getPaymentsReport/0/xls/?v=1'.$p)?>";
        return false;
    });
    $('#image1').click(function (event) {
        event.preventDefault();
        html2canvas($('.payments-table'), {
            onrendered: function (canvas) {
                openImg(canvas.toDataURL());
            }
        });
        return false;
    });


    $('#xls_2').click(function (event) {
        event.preventDefault();
        window.location.href = "<?=admin_url('reports/getPurchasesSummaryReport/0/xls/?v=1'.$v2)?>";
        return false;
    });


    $('#pay_date_records_filter').on('change', function(){
        filter = $(this).val();
        fecha_inicial = "";
        fecha_final = "<?= date('d/m/Y 23:59') ?>";
        hide_date_controls = true;
        if (filter == 5) { //RANGO DE FECHAS
            fecha_final = "";
            hide_date_controls = false;
        } else if (filter == 1) { // HOY
            fecha_inicial = "<?= date('d/m/Y 00:00') ?>";
        } else if (filter == 2) { // MES
            fecha_inicial = "<?= date('01/m/Y 00:00') ?>";
        } else if (filter == 3) { // TRIMESTRE
            fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
        } else if (filter == 4) { // AÑO
            fecha_inicial = "<?= date('01/01/Y 00:00') ?>";
        } else if (filter == 0) {
            fecha_inicial = "";
            fecha_final = "";
            hide_date_controls = false;
        }
        <?php if (!isset($_POST['pay_start_date'])): ?>
            $('#pay_start_date').val(fecha_inicial);
            $('#pay_end_date').val(fecha_final);
        <?php endif ?>
        if (hide_date_controls) {
            $('.pay_date_controls').css('display', 'none');
        } else {
            $('.pay_date_controls').css('display', '');
        }
    });
    <?php if (!isset($_POST['pay_start_date'])) { ?>
        $('#pay_date_records_filter').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
    <?php } else { ?>
        $('#pay_date_records_filter').val("<?= isset($_POST['pay_date_records_filter']) ? $_POST['pay_date_records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
    <?php } ?>
});
</script>
