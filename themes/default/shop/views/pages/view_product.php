<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    
    .product_category {
        font-size: 130%;
        color: #969696 !important;
    }

    .product_name, .product_price {
        font-weight: 600;
        margin: 1% 0% 0% 0%;
    }

    .product_description {
        margin: 3% 0% 1% 0%;
        color: #717171 !important;
    }

    .scrollleft {
        position: absolute;
        bottom: 8%;
        z-index: 999999;
        font-size: 140%;
    } 

    .scrollright {
        position: absolute;
        bottom: 8%;
        z-index: 999999;
        font-size: 140%;
        right: 0;
    }

</style>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="<?= $this->shop_settings->show_featured_products == 1 ? "col-sm-9 col-md-10" : "col-sm-12" ?> ">

                        <div class="product">
                            <div class="mprint">
                                <div class="row">
                                    <div class="col-sm-1 carousel_main_div">
                                        <div class="slider_over" style="overflow-x: hidden;">
                                            <ol class="carousel-indicators">
                                                <li class="active" data-slide-to="0" data-target="#photo-carousel">
                                                    <img class="img-thumbnail" alt="" src="<?= base_url() ?>assets/uploads/<?= $product->image ?>">
                                                </li>
                                                <?php
                                                $r = 1;
                                                if (!empty($images)) {
                                                    foreach ($images as $ph) {
                                                        echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/' . $ph->photo).'"></li>';
                                                        $r++;
                                                    }
                                                    // foreach ($images as $ph) {
                                                    //     echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/' . $ph->photo).'"></li>';
                                                    //     $r++;
                                                    // }
                                                    // foreach ($images as $ph) {
                                                    //     echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/' . $ph->photo).'"></li>';
                                                    //     $r++;
                                                    // }
                                                    // foreach ($images as $ph) {
                                                    //     echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/' . $ph->photo).'"></li>';
                                                    //     $r++;
                                                    // }
                                                    // foreach ($images as $ph) {
                                                    //     echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/' . $ph->photo).'"></li>';
                                                    //     $r++;
                                                    // }
                                                    // foreach ($images as $ph) {
                                                    //     echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/' . $ph->photo).'"></li>';
                                                    //     $r++;
                                                    // }
                                                    // foreach ($images as $ph) {
                                                    //     echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/' . $ph->photo).'"></li>';
                                                    //     $r++;
                                                    // }
                                                    // foreach ($images as $ph) {
                                                    //     echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/' . $ph->photo).'"></li>';
                                                    //     $r++;
                                                    // }
                                                }
                                                ?>

                                            </ol>
                                            <!-- <i class="fa fa-chevron-left scrollleft"></i>
                                            <i class="fa fa-chevron-right scrollright"></i> -->

                                            <script type="text/javascript">
                                                $('.scrollleft').on({
                                                    'mousedown touchstart': function () {
                                                        $(".slider_over").animate({scrollLeft: 0}, 2000);
                                                    },
                                                    'mouseup touchend': function () {
                                                        $(".slider_over").stop(true);
                                                    }
                                                });

                                                $('.scrollright').on({
                                                    'mousedown touchstart': function () {
                                                        $(".slider_over").animate({scrollLeft: 2000}, 2000);
                                                    },
                                                    'mouseup touchend': function () {
                                                        $(".slider_over").stop(true);
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">

                                        <div class="photo-slider">
                                            <div class="carousel slide article-slide" id="photo-carousel">

                                                <div class="carousel-inner cont-slider">
                                                    <div class="item active">
                                                        <a href="#" data-toggle="modal" data-target="#lightbox">
                                                            <img src="<?= base_url() ?>assets/uploads/<?= $product->image ?>" alt="<?= $product->name ?>" class="img-responsive img-thumbnail"/>
                                                        </a>
                                                    </div>
                                                    <?php
                                                    if (!empty($images)) {
                                                        foreach ($images as $ph) {
                                                            echo '<div class="item"><a href="#" data-toggle="modal" data-target="#lightbox"><img class="img-responsive img-thumbnail" src="' . base_url('assets/uploads/' . $ph->photo) . '" alt="' . $ph->photo . '" /></a></div>';
                                                        }
                                                    }
                                                    ?>
                                                    
                                                    <button class="btn_wshlst btn btn-default add-to-wishlist wishlist_inactive" data-id="<?= $product->id; ?>"><i class="view_span_wshlst fa fa-heart-o"></i></button>
                                                </div>

                                                <a class="left product-carousel-control" href="#photo-carousel" role="button" data-slide="prev" style="height: 80%;">
                                                    <span class="fa fa-chevron-left" aria-hidden="true" style="left:8px;"></span>
                                                    <span class="sr-only"><?= lang('prev'); ?></span>
                                                </a>
                                                <a class="right product-carousel-control" href="#photo-carousel" role="button" data-slide="next" style="height: 80%;">
                                                    <span class="fa fa-chevron-right" aria-hidden="true" style="right:8px;"></span>
                                                    <span class="sr-only"><?= lang('next'); ?></span>
                                                </a>  
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 row product_detail_data">
                                        <div class="col-sm-12">
                                            <span class="product_category"><?= $category->name ?></span>
                                            <h3 class="product_name"><?= $product->name; ?></h3>
                                            <hr>
                                            <h4 class="product_price" style="font-size: 160% !important; "><?= $this->sma->convertMoney($product->price); ?></h4>
                                            <p class="product_description"><?= $this->sma->decode_html($product->product_details) ?></p>
                                        </div>
                                        <!-- <div class="table-responsive">
                                            <table class="">
                                                <tbody>
                                                    <tr>
                                                        <td width="50%"><?= lang("name"); ?></td>
                                                        <td width="50%"><?= $product->name; ?></td>
                                                    </tr>
                                                    <?php if (!empty($product->second_name)) { ?>
                                                    <tr>
                                                        <td width="50%"><?= lang("secondary_name"); ?></td>
                                                        <td width="50%"><?= $product->second_name; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td><?= lang("code"); ?></td>
                                                        <td><?= $product->code; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= lang("type"); ?></td>
                                                        <td><?= lang($product->type); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= lang("brand"); ?></td>
                                                        <td><?= $brand ? '<a href="'.site_url('brand/'.$brand->slug).'" class="line-height-lg">'.$brand->name.'</a>' : ''; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= lang("category"); ?></td>
                                                        <td><?= '<a href="'.site_url('category/'.$category->slug).'" class="line-height-lg">'.$category->name.'</a>'; ?></td>
                                                    </tr>
                                                    <?php if ($product->subcategory_id) { ?>
                                                    <tr>
                                                        <td><?= lang("subcategory"); ?></td>
                                                        <td><?= '<a href="'.site_url('category/'.$category->slug.'/'.$subcategory->slug).'" class="line-height-lg">'.$subcategory->name.'</a>'; ?></td>
                                                    </tr>
                                                    <?php } ?>

                                                    <tr>
                                                        <td><?= lang("price"); ?></td>
                                                        <td><?= $this->sma->convertMoney($product->price); ?></td>
                                                    </tr>

                                                    <?php
                                                    if ($product->promotion) {
                                                        echo '<tr><td>' . lang("promotion") . '</td><td>' . $this->sma->convertMoney($product->promo_price) . ' ('.$this->sma->hrsd($product->start_date).' - '.$this->sma->hrsd($product->start_date).')</td></tr>';
                                                    }
                                                    ?>

                                                    <?php if ($product->tax_rate) { ?>
                                                    <tr>
                                                        <td><?= lang("tax_rate"); ?></td>
                                                        <td><?= $tax_rate->name; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= lang("tax_method"); ?></td>
                                                        <td><?= $product->tax_method == 0 ? lang('inclusive') : lang('exclusive'); ?></td>
                                                    </tr>
                                                    <?php } ?>

                                                    <tr>
                                                        <td><?= lang("unit"); ?></td>
                                                        <td><?= $unit ? $unit->name.' ('.$unit->code.')' : ''; ?></td>
                                                    </tr>
                                                    <?php if ($warehouse && $product->type == 'standard') { ?>
                                                    <tr>
                                                        <td><?= lang("in_stock"); ?></td>
                                                        <td><?= $this->sma->formatQuantity($warehouse->quantity); ?></td>
                                                    </tr>
                                                    <?php } ?>

                                                    <?php if ($variants) { ?>
                                                    <tr>
                                                        <td><?= lang("product_variants"); ?></td>
                                                        <td><?php foreach ($variants as $variant) {
                                                            echo '<span class="label label-primary">' . $variant->name . '</span> ';
                                                        } ?></td>
                                                    </tr>
                                                    <?php } ?>

                                                    <?php if (!empty($options) && $warehouse) {
                                                        foreach ($options as $option) {
                                                            if (isset($option->wh_qty) && $option->wh_qty != 0) {
                                                                echo '<tr><td colspan="2" class="bg-primary">' . $option->name . '</td></tr>';
                                                                echo '<td>' .lang("in_stock").': '. $this->sma->formatQuantity($option->wh_qty) . '</td>';
                                                                echo '<td>' .lang("price").': '.$this->sma->convertMoney(($product->special_price ? $product->special_price : $product->price)+$option->price). '</td>';
                                                                echo '</tr>';
                                                            }

                                                        }
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div> -->
                                        <?php if (($product->type != 'standard' || ($warehouse && $warehouse->quantity > 0)) || $Settings->overselling) { ?>
                                            <?= form_open('cart/add/'.$product->id, 'class="validate"'); ?>
                                            <?php
                                            if ($variants && $shop_settings->use_product_variants == 1) { 
                                            ?>
                                            <div class="form-group col-sm-12">
                                                <?php                                                    
                                                foreach ($variants as $variant) {
                                                        $opts[$variant->id] = $variant->name.($variant->price > 0 ? ' (+'.$this->sma->convertMoney($variant->price, TRUE, FALSE).')' : ($variant->price == 0 ? '' : ' (+'.$this->sma->convertMoney($variant->price, TRUE, FALSE).')'));
                                                    }
                                                    echo form_dropdown('option', $opts, '', 'class="form-control mobile-device" required="required"');
                                                ?>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                            if ($preferences && $shop_settings->use_product_preferences == 1) { 
                                            ?>
                                            <div class="form-group col-sm-12">
                                                <div class="div_preferences" data-pid="<?= $product->id ?>">Seleccione preferencias</div>
                                                <?php                                                    
                                                foreach ($preferences as $preference) {
                                                        $opts[$preference->name] = $preference->name;
                                                    }
                                                    echo form_dropdown('preferences[]', $opts, '', 'class="preference-select form-control select skip" required="required" multiple placeholder="'.lang('select_preferences').'" style="display:none;" data-pid="'.$product->id.'"');
                                                ?>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                            <div class="form-group col-sm-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
                                                    <input type="text" name="qty" class="form-control text-center quantity-input" value="1" required="required" data-id="<?= $product->id ?>">
                                                    <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
                                                </div>
                                            </div>
                                            <!-- <input type="hidden" name="quantity" class="form-control text-center" value="1"> -->

                                            <?php
                                            if (isset($units)) { 
                                                unset($opts);
                                            ?>
                                            <div class="form-group col-sm-6">
                                                <?php                                                    
                                                foreach ($units as $unit) {
                                                        $opts[$unit->product_unit_id] = $unit->name;
                                                    }
                                                    echo form_dropdown('unit', $opts, '', 'class="form-control mobile-device unit-select" required="required" data-id="'.$product->id.'"');
                                                ?>
                                            </div>
                                            <?php
                                            }
                                            ?>

                                            <div class="form-group <?= isset($units) ? "col-sm-12" : "col-sm-6" ?>">
                                                <button type="submit" class="btn btn-theme btn-lg" style="width: 100%;"><i class="fa fa-shopping-cart padding-right-md"></i> <?= lang('add_to_cart'); ?></button>
                                            </div>
                                            <?= form_close(); ?>
                                        <?php } else {
                                            echo '<div class="text-danger"><strong>'.lang('item_out_of_stock').'</strong></div>';
                                        } ?>
                                </div>
                            </div>

                            <?php include('share.php'); ?>
                        </div>
                    </div>
                </div>
                <?php if ($this->shop_settings->show_featured_products == 1): ?>
                    <div class="col-sm-3 col-md-2">
                        <?php include('sidebar2.php'); ?>
                    </div>
                <?php endif ?>
                    
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="featured-products">
    <div class="row">
        <?php
        if ($this->shop_settings->show_other_products == 1 && !empty($other_products)) {
            ?>
            <div class="col-xs-12">
                <h3 class="margin-top-no text-size-lg">
                    <?= lang('other_products'); ?>
                </h3>
            </div>
            <div class="row">
            <div class="col-xs-12">
                <?php
                foreach ($other_products as $fp) {
                    ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="product" style="z-index: 1;">
                            <div class="details" style="transition: all 100ms ease-out 0s;">
                                <?php
                                if ($fp->promotion) {
                                    ?>
                                    <span class="badge badge-right theme"><?= lang('promo'); ?></span>
                                    <?php
                                }
                                ?>
                                <img src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                <div class="image_overlay"></div>
                                <div class="btn add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart"></i> <?= lang('add_to_cart'); ?></div>
                                <div class="stats-container">
                                    <span class="product_price">
                                        <?php
                                        if ($fp->promotion) {
                                            echo '<del class="text-red">'.$this->sma->convertMoney(isset($fp->special_price) && !empty($fp->special_price) ? $fp->special_price : $fp->price).'</del><br>';
                                            echo $this->sma->convertMoney($fp->promo_price);
                                        } else {
                                            echo $this->sma->convertMoney(isset($fp->special_price) && !empty($fp->special_price) ? $fp->special_price : $fp->price);
                                        }
                                        ?>
                                    </span>
                                    <span class="product_name">
                                        <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                                    </span>
                                    <a href="<?= site_url('category/'.$fp->category_slug); ?>" class="link"><?= $fp->category_name; ?></a>
                                    <?php
                                    if ($fp->brand_name) {
                                        ?>
                                        <span class="link">-</span>
                                        <a href="<?= site_url('brand/'.$fp->brand_slug); ?>" class="link"><?= $fp->brand_name; ?></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
            <?php
        }
        ?>
    </div>
    </div>
</div>
</section>

<div id="lightbox" class="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-middle">
        <div class="modal-content">
            <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="modal-body">
                <img src="" alt="" />
            </div>
        </div>
    </div>
</div>