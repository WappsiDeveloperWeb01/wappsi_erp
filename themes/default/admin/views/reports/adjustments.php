<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$v = "";
/* if($this->input->post('name')){
  $v .= "&product=".$this->input->post('product');
  } */
if ($this->input->post('product')) {
    $v .= "&product=" . $this->input->post('product');
}
if ($this->input->post('biller')) {
    $v .= "&biller=" . $this->input->post('biller');
}
if ($this->input->post('document_type_id')) {
    $v .= "&document_type_id=" . $this->input->post('document_type_id');
}
if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}
if ($this->input->post('user')) {
    $v .= "&user=" . $this->input->post('user');
}
if ($this->input->post('serial')) {
    $v .= "&serial=" . $this->input->post('serial');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}

?>
<script>
    $(document).ready(function () {
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        oTable = $('#dmpData').dataTable({
            "aaSorting": [[0, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('reports/getAdjustmentReport/?v=1' . $v); ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"mRender": fld}, null, null, null, {"mRender": decode_html}, {"bSortable": false, "mRender": pqFormat}, {"mRender": pqFormat}],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[7];
                nRow.className = "adjustment_link2";
                return nRow;
            },
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('created_by');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang(' note');?>]", filter_type: "text", data: []},
        ], "footer");
        <?php endif ?>
    });

    $(document).ready(function () {
        $('#form').hide();
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php echo admin_form_open("reports/adjustments", 'id="form_filter"'); ?>
                    <input type="hidden" name="filter_action" id="filter_action" value="0">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                    <?php
                                    $wh[""] = lang('select').' '.lang('biller');
                                    foreach ($billers as $biller) {
                                        $wh[$biller->id] = $biller->name;
                                    }
                                    echo form_dropdown('biller', $wh, (isset($_POST['biller']) ? $_POST['biller'] : ""), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="reference_no"><?= lang("reference_no"); ?></label>
                                    <select name="document_type_id" class="form-control" id="document_type_id"></select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                    <?php
                                    $wh[""] = lang('select').' '.lang('warehouse');
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= lang("product", "suggest_product"); ?>
                                    <?php echo form_input('sproduct', (isset($_POST['sproduct']) ? $_POST['sproduct'] : ""), 'class="form-control" id="suggest_product"'); ?>
                                    <input type="hidden" name="product" value="<?= isset($_POST['product']) ? $_POST['product'] : "" ?>" id="report_product_id"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                    <?php
                                    $us[""] = lang('select').' '.lang('user');
                                    foreach ($users as $user) {
                                        $us[$user->id] = $user->first_name . " " . $user->last_name;
                                    }
                                    echo form_dropdown('user', $us, (isset($_POST['user']) ? $_POST['user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                    ?>
                                </div>
                            </div>
                            <?php if($Settings->product_serial) { ?>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang('serial_no', 'serial'); ?>
                                        <?= form_input('serial', '', 'class="form-control tip" id="serial"'); ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                    <?= $this->sma->get_filter_options(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="date_controls_dh">
                                <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                    <div class="col-sm-4 form-group">
                                        <?= lang('filter_year', 'filter_year_dh') ?>
                                        <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                            <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                <option value="<?= $key ?>"><?= $key ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                <?php endif ?>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("start_date", "start_date_dh"); ?>
                                        <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh" autocomplete="off"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("end_date", "end_date_dh"); ?>
                                        <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh" autocomplete="off"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div
                                class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_filter"'); ?> </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-right dropdown">
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                        <li class="dropdown">
                                            <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                                <i class="icon fa fa-file-excel-o"></i>
                                                <?= lang('download_xls') ?>
                                            </a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                                <i class="icon fa fa-file-picture-o"></i>
                                                <?= lang('save_image') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="dmpData" class="table table-bordered table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-2"><?= lang("date"); ?></th>
                                            <th class="col-xs-2"><?= lang("reference_no"); ?></th>
                                            <th class="col-xs-2"><?= lang("warehouse"); ?></th>
                                            <th class="col-xs-1"><?= lang("created_by"); ?></th>
                                            <th><?= lang("note"); ?></th>
                                            <th class="col-xs-2"><?= lang('product'); ?></th>
                                            <th class="col-xs-2"><?= lang('product_qty'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th><th></th><th></th><th></th><th></th>
                                            <th><?= lang('product'); ?></th>
                                            <th><?= lang('product_qty'); ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
<!-- /Body -->
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        
        
        $('#biller').change(function(){
            $.ajax({
                url: '<?= admin_url("billers/getBillersDocumentTypes/11/") ?>' + $('#biller').val(),
                type: 'get',
                dataType: 'JSON'
            }).done(function(data) {
                response = data;
                $('#document_type_id').html(response.options).select2();
            });
        });
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getAdjustmentReport/pdf/?v=1'.$v)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getAdjustmentReport/0/xls/?v=1'.$v)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });

        $('#submit_filter').on('click', function(e){
            // e.preventDefault();
            $('#filter_action').val(1);
            $('#sales_filter').submit();
        });
    });



    $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
            if ($('#biller').val()) {
                // $('#biller').trigger('change');
            }
        }, 150);
        
        <?php if ($this->session->userdata('biller_id')): ?>
            setTimeout(function() {
                $('#biller').select2('val', '<?= $this->session->userdata('biller_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
        <?php if ($this->session->userdata('warehouse_id')): ?>
            setTimeout(function() {
                $('#warehouse').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
    });
</script>
