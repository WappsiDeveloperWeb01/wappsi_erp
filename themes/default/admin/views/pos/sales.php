<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('pos/sales', ['id'=>'posFilterForm']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('date_records_filter') ?></label>
                                            <select class="form-control" name="date_records_filter" id="date_records_filter_dh">
                                                <?= $this->sma->get_filter_options($_POST["date_records_filter"]); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('biller') ?></label>
                                            <?php $billerId = (empty($billerId)) ? $this->session->userdata('biller_id') : $billerId; ?>
                                            <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                            <select class="form-control" id="biller_id" name="biller_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('document_type') ?></label>
                                            <?php $documentTypeId = (isset($_POST["document_type"])) ? $_POST["document_type"] : ''; ?>
                                            <select class="form-control" id="document_type" name="document_type">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($documentsTypes as $documentType) : ?>
                                                    <option value="<?= $documentType->id ?>" <?= ($documentType->id == $documentTypeId) ? 'selected' : 'no' ?>><?= $documentType->sales_prefix." - ".$documentType->nombre ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('origin') ?></label>
                                            <?php $saleOrigin = (isset($_POST["sale_origin"])) ? $_POST["sale_origin"] : ''; ?>
                                            <select class="form-control" id="sale_origin" name="sale_origin">
                                                <option value=""><?= lang('allsf') ?></option>
                                                <?php foreach ($salesOrigins as $origin) : ?>
                                                    <?php if ($origin->id != 5) : ?>
                                                        <option value="<?= $origin->id ?>" <?= ($origin->id == $saleOrigin) ? 'selected' : '' ?>><?= $origin->name ?></option>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="client"><?= $this->lang->line('customer') ?></label>
                                            <?php $client = (isset($_POST["client"])) ? $_POST["client"] : ''; ?>
                                            <input type="text" class="form-control input-tip" name="client" id="client" value="<?= $client ?>" placeholder="<?= lang('alls') ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label><?= $this->lang->line('seller') ?></label>
                                        <?php $sellerId = (isset($_POST["seller"])) ? $_POST["seller"] : ''; ?>
                                        <select class="form-control" name="seller" id="seller">
                                            <option value=""><?= lang('alls') ?></option>
                                            <?php foreach ($sellers as $seller): ?>
                                                <option value="<?= $seller->id ?>" <?= ($seller->id == $sellerId) ? 'selected' : '' ?>><?= $seller->company ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="left" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="warehouse_id"><?= $this->lang->line('warehouse') ?></label>
                                            <?php $warehouseId = (isset($_POST["warehouse_id"])) ? $_POST["warehouse_id"] : ''; ?>
                                            <select class="form-control" id="warehouse_id" name="warehouse_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php if (!empty($warehouses)) { ?>
                                                    <?php foreach ($warehouses as $warehouse) : ?>
                                                        <option value="<?= $warehouse->id ?>" <?= ($warehouse->id == $warehouseId) ? 'selected' : '' ?>><?= $warehouse->name ?></option>
                                                    <?php endforeach ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="payment_status"><?= lang('payment_status') ?></label>
                                            <?php $paymentStatus = (isset($_POST["payment_status"])) ? $_POST["payment_status"] : ''; ?>
                                            <select name="payment_status" id="payment_status" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($paymentsStatus as $status) : ?>
                                                    <option value="<?= $status->id ?>" <?= ($status->id == $paymentStatus) ? 'selected' : '' ?>><?= $status->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('originalPaymentMethod') ?></label>
                                            <?php $paymentMethod = (isset($_POST["payment_method"])) ? $_POST["payment_method"] : ''; ?>
                                            <select class="form-control" id="payment_method" name="payment_method">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($paymentsMethods as $method) : ?>
                                                    <option value="<?= $method->code ?>" <?= ($method->code == $paymentMethod) ? 'selected' : '' ?>><?= $method->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php if ($this->Owner || $this->Admin): ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="user"><?= $this->lang->line('user') ?></label>
                                                <?php $userId = (isset($_POST["user"])) ? $_POST["user"] : ''; ?>
                                                <select class="form-control" name="user" id="user">
                                                    <option value=""><?= lang('alls') ?></option>
                                                    <?php foreach ($users as $user): ?>
                                                        <option value="<?= $user->id ?>" <?= ($user->id == $userId) ? 'selected' : '' ?>><?= $user->first_name." ".$user->last_name ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                </div>

                                <div class="row date_controls_dh">
                                    <!-- <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                        <div class="col-sm-2 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?> -->
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for="start_date_dh"><?= $this->lang->line('start_date') ?></label>
                                            <?php $startDate = isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>
                                            <input class="form-control datetime" type="text" name="start_date" id="start_date_dh" value="<?= $startDate ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for="end_date_dh"><?= $this->lang->line('end_date') ?></label>
                                            <?php $endDate = isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>
                                            <input class="form-control datetime" type="text" name="end_date" id="end_date_dh" value="<?= $endDate ?>" class="form-control datetime">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-content">
                    <div class="box-content">
                        <div class="row">
                            <?php if ($Owner || $Admin ||  $GP['bulk_actions']) {
                                echo admin_form_open('pos/pos_actions', 'id="action-form"');
                            } ?>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="POSData" class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("payment_term"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("affects_to"); ?></th>
                                            <th><?= lang("biller"); ?></th>
                                            <th><?= lang("customer_name"); ?></th>
                                            <th><?= lang("grand_total"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("sale_status"); ?></th>
                                            <th><?= lang("payment_status"); ?></th>
                                            <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <input type="hidden" name="electronic" value="" id="electronic"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function () {
        var _tabFilterFill = false;
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }
        oTable = $('#POSData').dataTable({
            aaSorting: [[1, "desc"], [3, "desc"]],
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true, 'bServerSide': true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('pos/getSales?warehouse_id='.($warehouse_id ? $warehouse_id : '').'&dias_vencimiento='.(isset($dias_vencimiento) ? $dias_vencimiento : '')) ?>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "date_records_filter",
                    "value": $('#date_records_filter').val()
                }, {
                    "name": "biller_id",
                    "value": $('#biller_id').val()
                }, {
                    "name": "document_type",
                    "value": $('#document_type').val()
                }, {
                    "name": "sale_origin",
                    "value": $('#sale_origin').val()
                }, {
                    "name": "campaign",
                    "value": $('#campaign').val()
                }, {
                    "name": "seller",
                    "value": $('#seller').val()
                }, {
                    "name": "client",
                    "value": $('#client').val()
                }, {
                    "name": "warehouse_id",
                    "value": $('#warehouse_id').val()
                }, {
                    "name": "payment_status",
                    "value": $('#payment_status').val()
                }, {
                    "name": "payment_method",
                    "value": $('#payment_method').val()
                }/* , {
                    "name" : "fe_aceptado",
                    "value" : $('#fe_aceptado').val()
                } , {
                    "name" : "fe_correo_enviado",
                    "value" : $('#fe_correo_enviado').val()
                }*/, {
                    "name" : "user",
                    "value" : $('#user').val()
                }, {
                    "name": "start_date",
                    "value": $('#start_date_dh').val()
                }, {
                    "name": "end_date",
                    "value": $('#end_date_dh').val()
                }, {
                    "name": "optionFilter",
                    "value": option_filter
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "receipt_link";
                return nRow;
            },
            aoColumns: [
                {
                    "bSortable": false,
                    "mRender": checkbox
                },
                {"mRender": fld},
                null,
                null,
                null,
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": row_status},
                {"mRender": pay_status},
                {"bSortable": false}
            ],
            fnDrawCallback: function (oSettings) {
                var addSaleButton = postSaleButton = '';
                if (!_tabFilterFill) {
                    $('.additionalControlsContainer').html('<div class="wizard">' +
                        '<div class="steps clearfix">' +
                            '<ul role="tablist">' +
                                '<li role="tab" class="first current index_list" aria-disabled="false"  aria-selected="true" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="all" href="#">'+
                                        '<span class="all_span">0</span><br><?= lang('allsf') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done  index_list" aria-disabled="false" style="width:16.6%;">' +
                                    '<a class="wizard_index_step" data-optionfilter="returns" href="#" aria-controls="edit_biller_form-p-0">' +
                                        '<span class="returns_span">0</span><br><?= lang('returns') ?>' +
                                    '</a>' +
                                '</li>' +
                                /* '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="notApproved" href="#">' +
                                        '<span class="notApproved_span">0</span><br><?= lang('notApproved') ?>' +
                                    '</a>' +
                                '</li>' + */
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingPayment" href="#">'+
                                        '<span class="pendingPayment_span">0</span><br><?= lang('pendingPayment') ?>' +
                                    '</a>' +
                                '</li>' +
                                /* '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingDian" href="#">'+
                                        '<span class="pendingDian_span">0</span><br><?= lang('pendingDian') ?>' +
                                    '</a>' +
                                '</li>' + */
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingDispatch" href="#">'+
                                        '<span class="pendingDispatch_span">0</span><br><?= lang('pendingDispatch') ?>' +
                                    '</a>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>').addClass('col-without-padding');

                    _tabFilterFill = true;
                }

                <?php if ($this->Owner || $this->Admin || $this->GP['sales-add']): ?>
                    addSaleButton = '<a href="<?= admin_url('pos') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>';
                <?php endif ?>
                <?php if ($this->Owner || $this->Admin || $this->GP['sales-add']): ?>
                    postSaleButton = `<li>
                            <a href="#" id="post_sale" data-action="post_sale">
                                <i class="fa fa-file-pdf-o"></i> <?= lang('post_sale') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="mark_printed_sale" data-action="mark_printed_sale">
                                <i class="fa-solid fa-check-to-slot"></i> <?= lang('mark_printed_sale') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="mark_all_printed_sale" data-action="mark_all_printed_sale" data-electronic="0">
                                <i class="fa-solid fa-check-to-slot"></i> <?= lang('mark_all_printed_sale') ?>
                            </a>
                        </li>`;
                <?php endif ?>

                $('.actionsButtonContainer').html(addSaleButton +
                '<div class="pull-right dropdown">'+
                    '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="left" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                    '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                        '<li>'+
                            '<a href="#" id="excel" data-action="export_excel">'+
                                '<i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>'+
                            '</a>'+
                        '</li>'+
                        postSaleButton +
                    '</ul>'+
                '</div>');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });

                loadDataTabFilters();
            }
        });

        $('#client').val('<?= $client ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $(document).on('click', '.duplicate_pos', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            if (localStorage.getItem('positems')) {
                bootbox.confirm("<?= $this->lang->line('leave_alert') ?>", function (gotit) {
                    if (gotit == false) {
                        return true;
                    } else {
                        window.location.href = link;
                    }
                });
            } else {
                window.location.href = link;
            }
        });
        $(document).on('click', '.email_receipt', function () {
            var sid = $(this).attr('data-id');
            var ea = $(this).attr('data-email-address');
            var email = prompt("<?= lang("email_address"); ?>", ea);
            if (email != null) {
                $.ajax({
                    type: "post",
                    url: "<?= admin_url('pos/email_receipt') ?>/" + sid,
                    data: { <?= $this->security->get_csrf_token_name(); ?>: "<?= $this->security->get_csrf_hash(); ?>", email: email, id: sid },
                    dataType: "json",
                        success: function (data) {
                        bootbox.alert(data.msg);
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_request_failed'); ?>');
                        return false;
                    }
                });
            }
        });

        $(document).on('change', '#biller_id', function() { loadDocumentsType($(this)); loadWarehouses($(this)); });

        $('[data-toggle="tooltip"]').tooltip();
    });
    /* $(document).on('change', '#biller', function(){
        $.ajax({
            url:'<?= admin_url("billers/getMultipleBillersDocumentTypes/") ?>',
            type:'post',
            data : {
                "<?= $this->security->get_csrf_token_name() ?>" : "<?= $this->security->get_csrf_hash() ?>",
                "biller_id" : $('#biller').val(),
                "modules" : "<?= json_encode([1, 3]) ?>",
            },
            dataType:'JSON'
            }).done(function(data){
            response = data;
            $('#pos_reference_no').html(response.options).select2();
        });
    }); */


    function calcularMinutos(start_date, end_date)
    {
        var fecha1 = new Date(start_date);
        var fecha2 = new Date(end_date);
        var diff = fecha2.getTime() - fecha1.getTime();
        var minutos = diff/(1000 * 60);
        return minutos;
    }

    function setFilterText(){

        var reference_text = $('#pos_reference_no option:selected').data('dtprefix');
        var biller_text = $('#biller option:selected').text();
        var customer_text = $('#filter_customer').select2('data') !== null ? $('#filter_customer').select2('data').text : '';
        var sale_status_text = $('#sale_status option:selected').text();
        var payment_status_text = $('#payment_status option:selected').text();
        var start_date_text = $('#start_date_dh').val();
        var end_date_text = $('#end_date_dh').val();

        var text = "Filtros configurados : ";

        coma = false;
        if (pos_reference_no != '' && biller !== pos_reference_no) {
            text+=" Sucursal ("+reference_text+")";
            coma = true;
        }
        if (biller != '' && biller !== undefined) {
            text+=" Sucursal ("+biller_text+")";
            coma = true;
        }
        if (customer != '' && customer !== undefined) {
            text+= coma ? "," : "";
            text+=" Cliente ("+customer_text+")";
            coma = true;
        }
        if (sale_status != '' && sale_status !== undefined) {
            text+= coma ? "," : "";
            text+=" Estado de venta ("+sale_status_text+")";
            coma = true;
        }
        if (payment_status != '' && payment_status !== undefined) {
            text+= coma ? "," : "";
            text+=" Estado de pago ("+payment_status_text+")";
            coma = true;
        }
        if (start_date != '' && start_date !== undefined) {
            text+= coma ? "," : "";
            text+=" Fecha de inicio ("+start_date_text+")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text+= coma ? "," : "";
            text+=" Fecha final ("+end_date_text+")";
            coma = true;
        }

        $('.text_filter').html(text);

    }

    function loadDocumentsType(element)
    {
        var billerId = element.val();
        var options = '<option value=""><?= $this->lang->line('allsf') ?></option>';

        $.ajax({
            type: "post",
            url: "<?= admin_url("sales/getDocumentTypes") ?>",
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'billerId': element.val(),
                'asynchronous': 1,
                'modules'       : [1, 3, 26]
            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(documentType => {
                        options += '<option value="'+documentType.id+'">'+documentType.sales_prefix+' - '+documentType.nombre+'</option>';
                    });
                }

                $('#document_type').html(options);
            }
        });
    }

    function loadWarehouses(element) {
        if (element.val()) {
            var defaultwh = element.children("option:selected").data('defaultwh');
            $('#warehouse_id').select2('val', defaultwh).trigger('change');
        } else {
            $('#warehouse_id option').each(function(index, option) {
                $(option).attr('disabled', false);
            });
            $('#warehouse_id').select2('val', '').trigger('change');
        }
    };

    function loadDataTabFilters()
    {
        $.ajax({
            type: 'post',
            url: '<?= admin_url('pos/getSales'); ?>',
            data: {
                tabFilterData: 1,
                user: $('#user').val(),
                seller: $('#seller').val(),
                optionFilter: option_filter,
                biller_id: $('#biller_id').val(),
                campaign: $('#campaign').val(),
                sale_origin: $('#sale_origin').val(),
                // fe_aceptado: $('#fe_aceptado').val(),
                warehouse_id: $('#warehouse_id').val(),
                document_type: $('#document_type').val(),
                payment_status: $('#payment_status').val(),
                payment_method: $('#payment_method').val(),
                // fe_correo_enviado: $('#fe_correo_enviado').val(),
                date_records_filter: $('#date_records_filter').val(),
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                end_date: "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>",
                start_date: "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
            },
            dataType: 'json'
        }).done(function(data) {
            $('.all_span').text(data.all);
            // $('.notApproved_span').text(data.notApproved);
            $('.pendingDian_span').text(data.pendingDian);
            $('.pendingDispatch_span').text(data.pendingDispatch);
            // $('.pendingPayment_span').text(data.pendingPayment);
            $('.returns_span').text(data.returns);
        });
    }


$(document).ready(function(){
    setTimeout(function() {
        <?php if (!isset($_POST['date_records_filter'])): ?>
            $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            $('#posFilterForm').submit();
        <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
            $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
        <?php endif ?>
    }, 150);
});
</script>