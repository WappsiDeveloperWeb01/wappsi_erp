<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var potype = 1;
    var id_purchase = null;
    var purchase_has_payments = false;
    var is_csv = false;
    var is_quote = false;
    var support_document = 0;
    var edit_paid_by = false;
    var edit_paid_amount = false;
    var product_has_movements_after_date = false;
    var count = 1,
        an = 1,
        po_edit = false,
        product_variant = 0,
        DT = <?= $Settings->purchase_tax_rate ?>,
        order_discount = 0,
        DC = '<?= $default_currency->code ?>',
        shipping = 0,
        product_tax = 0,
        invoice_tax = 0,
        total_discount = 0,
        total = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>,
        poitems = {},
        audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3'),
        audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function() {
        <?php if ($this->input->get('supplier')) { ?>
            if (!localStorage.getItem('poitems')) {
                localStorage.setItem('posupplier', <?= $this->input->get('supplier'); ?>);
            }
        <?php } ?>
        <?php if ($Owner || $Admin) { ?>
            if (!localStorage.getItem('podate')) {
                $("#podate").datetimepicker({
                    format: site.dateFormats.js_ldate,
                    fontAwesome: true,
                    language: 'sma',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0
                }).datetimepicker('update', new Date());
            }
            $(document).on('change', '#podate', function(e) {
                localStorage.setItem('podate', $(this).val());
            });
            if (podate = localStorage.getItem('podate')) {
                $('#podate').val(podate);
            }
        <?php } ?>
        $('#extras').on('ifChecked', function() {
            $('#extras-con').slideDown();
        });
        $('#extras').on('ifUnchecked', function() {
            $('#extras-con').slideUp();
        });
        $(document).on('change', '#pobiller', function(e) {
            var document_type = 5;
            var payment_document_type = 20;
            $.ajax({
                url: '<?= admin_url("billers/getBillersDocumentTypes/") ?>' + document_type + '/' + $('#pobiller').val(),
                type: 'get',
                dataType: 'JSON'
            }).done(function(data) {
                response = data;
                $('#document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> (' + response.not_parametrized + ') no están parametrizados </b> en contabilidad', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                }
                if (response.status == 0) {
                    $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                }
                $('#document_type_id').trigger('change');
            });
            $.ajax({
                url: '<?= admin_url("billers/getBillersDocumentTypes/") ?>' + payment_document_type + '/' + $('#pobiller').val(),
                type: 'get',
                dataType: 'JSON'
            }).done(function(data) {
                response = data;
                $('#payment_reference_no').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> (' + response.not_parametrized + ') no están parametrizados </b> en contabilidad', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                }
                if (response.status == 0) {
                    $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                }
                $('#payment_reference_no').trigger('change');
            });
            warehousedefault = $('#pobiller option:selected').data('warehousedefault');
            userwarehousedefault = "<?= $this->session->userdata('warehouse_id') ?>";
            if (userwarehousedefault > 0) {
                $('#powarehouse').select2('val', userwarehousedefault);
            } else {
                $('#powarehouse').select2('val', warehousedefault);
            }
            localStorage.setItem('pobiller', $('#pobiller').val());
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php
                    $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-po-form');
                    echo admin_form_open_multipart("purchases/purchase_by_csv", $attrib)
                    ?>
                    <div class="row">
                        <div class="col-lg-12">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("date", "podate"); ?>
                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : date('d/m/Y H:i')), 'class="form-control input-tip datetime" id="podate" required="required"'); ?>
                            </div>
                        </div>
                        <?php
                        $bl[""] = "";
                        $bldata = [];
                        foreach ($billers as $biller) {
                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                            $bldata[$biller->id] = $biller;
                        }
                        ?>
                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="col-md-4  form-group">
                                <?= lang("biller", "pobiller"); ?>
                                <select name="biller" class="form-control" id="pobiller" required="required">
                                    <?php foreach ($billers as $biller) : ?>
                                        <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" selected><?= $biller->company ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("biller", "pobiller"); ?>
                                    <select name="biller" class="form-control" id="pobiller">
                                        <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                            $biller = $bldata[$this->session->userdata('biller_id')];
                                        ?>
                                            <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <?php if ($this->Settings->management_consecutive_suppliers) : ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("reference_no", "poref"); ?><br>
                                    <div style="display: flex;">
                                        <select name="document_type_id" class="form-control" id="document_type_id" required="required">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("reference_no", "poref"); ?>
                                    <label style="margin-left: 9%;">Número</label>
                                    <br>
                                    <div style="display: flex;">
                                        <select name="document_type_id" class="form-control" id="document_type_id" required="required" style="width: 30%;">
                                        </select>
                                        <?php echo form_input('reference_no', '', 'class="form-control input-tip" id="poref" style="width: 70%;"'); ?>
                                    </div>
                                    <em class="text-danger referencia_duplicada text-bold" style="display: none;"> * Referencia duplicada</em>
                                </div>
                            </div>
                        <?php endif ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("warehouse", "powarehouse"); ?>
                                <?php
                                $wh[''] = '';
                                foreach ($warehouses as $warehouse) {
                                    $wh[$warehouse->id] = $warehouse->name;
                                }
                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="powarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("supplier_creditor", "posupplier"); ?>
                                <?php if ($Owner || $Admin || $GP['suppliers-add'] || $GP['suppliers-index']) { ?>
                                    <div class="input-group">
                                    <?php } ?>
                                    <input type="hidden" name="supplier" value="" id="posupplier" class="form-control" style="max-width:370px;" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" required>
                                    <input type="hidden" name="supplier_id" value="" id="supplier_id" class="form-control">
                                    <?php if ($Owner || $Admin || $GP['suppliers-index']) { ?>
                                        <div class="input-group-addon no-print" style="padding: 2px 5px; border-left: 0;">
                                            <a id="view-supplier">
                                                <i class="fa fa-2x fa-user" id="addIcon"></i>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    <?php if ($Owner || $Admin || $GP['suppliers-add']) { ?>
                                        <div class="input-group-addon no-print" style="padding: 2px 5px;">
                                            <a href="<?= admin_url('suppliers/add'); ?>" id="add-supplier" class="external" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    <?php if ($Owner || $Admin || $GP['suppliers-add'] || $GP['suppliers-index']) { ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        $bl[""] = "";
                        $bldata = [];
                        foreach ($billers as $biller) {
                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                            $bldata[$biller->id] = $biller;
                        }
                        ?>
                    </div>
                    <div class="row">
                        <?php if ($this->Settings->management_consecutive_suppliers) : ?>
                            <div class="col-md-4 ">
                                <?= lang('consecutive_supplier', 'consecutive_supplier') ?>
                                <?= form_input('consecutive_supplier', '', 'class="form-control" id="consecutive_supplier" required') ?>
                            </div>
                        <?php endif ?>
                        <?php if ($this->Settings->descuento_orden == 3) : ?>
                            <?php
                            $dopts = array(
                                '' => lang('select'),
                                '1' => lang('descuento_orden_afecta_iva'),
                                '2' => lang('descuento_orden_no_afecta_iva'),
                            );
                            ?>
                            <div class="col-md-4  form-group">
                                <?= lang("descuento_orden", "order_discount_method") ?>
                                <?= form_dropdown('order_discount_method', $dopts, $this->Settings->descuento_orden, 'id="order_discount_method" class="form-control" required'); ?>
                            </div>
                        <?php else : ?>
                            <input type="hidden" name="order_discount_method" id="order_discount_method" value="<?= $this->Settings->descuento_orden ?>">
                        <?php endif ?>
                        <?php if (isset($cost_centers)) : ?>
                            <div class="col-md-4  form-group">
                                <?= lang('cost_center', 'cost_center_id') ?>
                                <?php
                                $ccopts[''] = lang('select');
                                if ($cost_centers) {
                                    foreach ($cost_centers as $cost_center) {
                                        $ccopts[$cost_center->id] = "(" . $cost_center->code . ") " . $cost_center->name;
                                    }
                                }
                                ?>
                                <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="row">
                        <?php if (count($currencies) > 1) : ?>
                            <div class="col-md-4 row">
                                <div class="col-md-6 form-group">
                                    <?= lang('currency', 'currency') ?>
                                    <select name="currency" class="form-control" id="currency">
                                        <?php foreach ($currencies as $currency) : ?>
                                            <option value="<?= $currency->code ?>" data-rate="<?= $currency->rate ?>" <?= $currency->code == $this->Settings->default_currency ? "selected='selected'" : "" ?>><?= $currency->code ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group trm-control" style="display: none;">
                                    <?= lang('trm', 'trm') ?>
                                    <input type="number" name="trm" id="trm" class="form-control" required="true" step="0.01">
                                </div>
                            </div>
                        <?php else : ?>
                            <input type="hidden" name="currency" value="<?= $this->Settings->default_currency ?>" id="currency">
                        <?php endif ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <span class="text-bold"><?= lang('xls_panel_header_info') ?></span>
                                    <a style="color: white;" href="<?php echo $this->config->base_url(); ?>assets/csv/sample_purchase_products.xlsx" class="pull-right"><i class="fa fa-download"></i> <?= lang('download_sample_file') ?></a>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>
                                                <?= lang('csv_column_orders_info') ?> :<br>
                                                <b><?=
                                                    lang("product_code") . ', ' .
                                                        lang("net_unit_cost") . ', ' .
                                                        lang("quantity") . ', ' .
                                                        lang("product_variant") . ', ' .
                                                        lang("tax_rate_name") . ', ' .
                                                        lang("discount") . ', ' .
                                                        lang("expiry");
                                                    ?>.
                                                </b>
                                            </p>
                                            <em><?= lang('csv1') ?></em>
                                        </div>
                                        <div class="col-md-6">
                                            <hr>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th><?= lang('tax') ?></th>
                                                        <th><?= lang('code') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($tax_rates as $tax_rate) : ?>
                                                        <tr>
                                                            <td><?= $tax_rate->name ?></td>
                                                            <td><?= $tax_rate->code ?></td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang("csv_file", "csv_file") ?>
                                                <input id="" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" required="required" data-show-upload="false" data-show-preview="false" class="form-control file">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                         <div class="col-md-12">
                            <div class="clearfix"></div>
                            <div class="well well-sm">
                                <a href="<?php echo $this->config->base_url(); ?>assets/csv/sample_purchase_products.csv"
                                class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample
                                    File</a>
                                <span class="text-warning"><?php echo $this->lang->line("csv1"); ?></span><br>
                                <?php echo $this->lang->line("csv2"); ?> <span
                                    class="text-info">(<?= lang("product_code") . ', ' . lang("net_unit_cost") . ', ' . lang("quantity") . ', ' . lang("product_variant") . ', ' . lang("tax_rate_name") . ', ' . lang("discount") . ', ' . lang("expiry"); ?>
                                    )</span> <?php echo $this->lang->line("csv3"); ?><br>
                                <strong><?= sprintf(lang('x_col_required'), 3); ?></strong>
                            </div>
                        </div> -->
                    </div>
                    <div class="row" id="extras-con" style="">
                        <?php if ($Settings->tax3) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <span class="fa fa-plus-square-o"></span> <?= lang('order_tax', 'potax2') ?>
                                    <?php
                                    $tr[""] = "";
                                    foreach ($tax_rates as $tax) {
                                        $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('order_tax', $tr, $Settings->purchase_tax_rate, 'id="potax2" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-4 podiscount_div">
                            <div class="form-group">
                                <span class="fa fa-minus-square-o"></span> <?= lang("discount_label", "podiscount"); ?>
                                <?php echo form_input('discount', '', 'class="form-control input-tip text-right" id="podiscount"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("status", "postatus"); ?>
                                <?php
                                $post = array('received' => lang('received'), 'pending' => lang('not_approved'), 'ordered' => lang('ordered'));
                                echo form_dropdown('status', $post, (isset($_POST['status']) ? $_POST['status'] : ''), 'id="postatus" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4" style="<?= $this->Settings->prorate_shipping_cost != 2 ? 'display: none;' : '' ?>">
                            <div class="form-group">
                                <?= lang('prorate_shipping_cost', 'prorate_shipping_cost') ?>
                                <select name="prorate_shipping_cost" id="prorate_shipping_cost" class="form-control">
                                    <option value=""><?= lang('select') ?></option>
                                    <option value="0" <?= $this->Settings->prorate_shipping_cost == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                    <option value="1" <?= $this->Settings->prorate_shipping_cost == 1 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 poshipping_div">
                            <div class="form-group">
                                <?= lang("purchase_shipping", "poshipping"); ?>
                                <?php echo form_input('shipping', '', 'class="form-control input-tip" id="poshipping" ' . ($this->Settings->prorate_shipping_cost == 2 ? 'readonly' : '')); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label onclick="$('#rtModal').modal('show');" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención </label>
                            <input type="text" name="retencion_show" id="retencion_show" class="form-control text-right" readonly>
                            <input type="hidden" name="retencion" id="rete">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false" data-show-preview="false" class="form-control file">
                            </div>
                        </div>

                        <div id="payments" style="display: none;">
                            <hr>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("payment_reference_no", "payment_reference_no"); ?><br>
                                    <div style="display: flex;">
                                        <select name="payment_reference_no" class="form-control" id="payment_reference_no">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="well well-sm well_1">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <?php if ($this->Settings->purchase_payment_affects_cash_register == 0) : ?>
                                                <?= form_hidden('payment_affects_register', '0') ?>
                                            <?php elseif ($this->Settings->purchase_payment_affects_cash_register == 1) : ?>
                                                <?= form_hidden('payment_affects_register', '1') ?>
                                            <?php else : ?>
                                                <div class="col-sm-4" id="div_affects_register">
                                                    <?= lang('payment_affects_register', 'payment_affects_register') ?><br>
                                                    <label>
                                                        <input type="radio" name="payment_affects_register" id="payment_affects_register" value="1">
                                                        <?= lang('affects_register') ?>
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="payment_affects_register" id="payment_affects_register" value="0">
                                                        <?= lang('no_affects_register') ?>
                                                    </label>
                                                    <label for="payment_affects_register" class="error">* Obligatorio</label>
                                                </div>
                                            <?php endif ?>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <?= lang("paying_by", "paid_by_1"); ?>
                                                    <select name="paid_by[]" id="paid_by_1" class="form-control paid_by" data-pbnum="1">
                                                        <option value="expense_causation" data-img-src="http://localhost/wappsi_erp/assets/payment_methods_icons/credit.png" class=""><?= lang('expense_causation') ?></option>
                                                        <?= $this->sma->paid_opts(null, true); ?>
                                                    </select>
                                                    <input type="hidden" name="due_payment[]" class="due_payment_1">
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-4">
                                                <div class="payment">
                                                    <div class="form-group ngc">
                                                        <?= lang("amount", "amount_1"); ?>
                                                        <input name="amount-paid[]" type="text" id="amount_1"
                                                            class="pa form-control kb-pad amount"/>
                                                    </div>
                                                    <div class="form-group gc" style="display: none;">
                                                        <?= lang("gift_card_no", "gift_card_no"); ?>
                                                        <input name="gift_card_no[]" type="text" id="gift_card_no"
                                                            class="pa form-control kb-pad"/>

                                                        <div id="gc_details"></div>
                                                    </div>
                                                </div>
                                            </div> -->

                                            <div class="col-sm-4 div_popayment_term_1" style="display:none;">
                                                <div class="form-group">
                                                    <?= lang("payment_term", "popayment_term_1"); ?>
                                                    <?php echo form_input('payment_term[]', '', 'class="form-control tip purchase_payment_term" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="popayment_term_1"'); ?>
                                                    <input type="hidden" name="supplierpaymentterm" id="supplierpaymentterm">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="pcc_1" style="display:none;">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_no[]" type="text" id="pcc_no_1" class="form-control" placeholder="<?= lang('cc_no') ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_holder[]" type="text" id="pcc_holder_1" class="form-control" placeholder="<?= lang('cc_holder') ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select name="pcc_type[]" id="pcc_type_1" class="form-control pcc_type" placeholder="<?= lang('card_type') ?>">
                                                            <option value="Visa"><?= lang("Visa"); ?></option>
                                                            <option value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                            <option value="Amex"><?= lang("Amex"); ?></option>
                                                            <option value="Discover"><?= lang("Discover"); ?></option>
                                                        </select>
                                                        <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_month[]" type="text" id="pcc_month_1" class="form-control" placeholder="<?= lang('month') ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <input name="pcc_year[]" type="text" id="pcc_year_1" class="form-control" placeholder="<?= lang('year') ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <input name="pcc_ccv[]" type="text" id="pcc_cvv2_1" class="form-control" placeholder="<?= lang('cvv2') ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pcheque_1" style="display:none;">
                                            <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                <input name="cheque_no[]" type="text" id="cheque_no_1" class="form-control cheque_no" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?= lang('payment_note', 'payment_note_1'); ?>
                                            <textarea name="payment_note[]" id="payment_note_1" class="pa form-control kb-text payment_note"></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("note", "ponote"); ?>
                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="ponote" style="margin-top: 10px; height: 100px;"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" name="total_items" value="" id="total_items" required="required" />
                        <div class="col-md-12">
                            <div class="from-group"><?php echo form_submit('add_pruchase', $this->lang->line("submit"), 'id="add_pruchase" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- retencion -->
<div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="rtModalLabel"><?= lang('apply_order_retention'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-2">
                        <label>Retención</label>
                    </div>
                    <div class="col-sm-5">
                        <label>Opción</label>
                    </div>
                    <div class="col-sm-5">
                        <label>Porcentaje</label>
                    </div>
                    <!-- <div class="col-sm-3">
                                  <label>Valor</label>
                                </div> -->
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label>
                            <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                        </label>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                            <option>Seleccione...</option>
                        </select>
                        <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                        <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                        <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                        <input type="hidden" name="rete_fuente_apply" id="rete_fuente_apply">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                    </div>
                    <!-- <div class="col-sm-3">
                                  <input type="text" name="rete_fuente_valor_show" id="rete_fuente_valor_show" class="form-control text-right" readonly>
                                  <input type="hidden" name="rete_fuente_valor" id="rete_fuente_valor">
                                </div> -->
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label>
                            <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                        </label>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                            <option>Seleccione...</option>
                        </select>
                        <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                        <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                        <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                        <input type="hidden" name="rete_iva_apply" id="rete_iva_apply">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                    </div>
                    <!-- <div class="col-sm-3">
                                  <input type="text" name="rete_iva_valor_show" id="rete_iva_valor_show" class="form-control text-right" readonly>
                                  <input type="hidden" name="rete_iva_valor" id="rete_iva_valor">
                                </div> -->
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label>
                            <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                        </label>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                            <option>Seleccione...</option>
                        </select>
                        <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                        <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                        <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                        <input type="hidden" name="rete_ica_apply" id="rete_ica_apply">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                    </div>
                    <!-- <div class="col-sm-3">
                                  <input type="text" name="rete_ica_valor_show" id="rete_ica_valor_show" class="form-control text-right" readonly>
                                  <input type="hidden" name="rete_ica_valor" id="rete_ica_valor">
                                </div> -->
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label>
                            <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                        </label>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                            <option>Seleccione...</option>
                        </select>
                        <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                        <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                        <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                        <input type="hidden" name="rete_otros_apply" id="rete_otros_apply">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                    </div>
                    <!-- <div class="col-sm-3">
                                  <input type="text" name="rete_otros_valor_show" id="rete_otros_valor_show" class="form-control text-right" readonly>
                                  <input type="hidden" name="rete_otros_valor" id="rete_otros_valor">
                                </div> -->
                </div>
                <!-- <div class="row">
                                <div class="col-md-8 text-right">
                                  <label>Total : </label>
                                </div>
                                <div class="col-md-4 text-right">
                                  <label id="total_rete_amount" style="display: none;"> 0.00 </label>
                                  <label id="total_rete_amount_show"> 0.00 </label>
                                </div> -->
                <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                <!-- </div> -->


                <!-- <div class="form-group">
                                <?= lang("order_tax", "order_tax_input"); ?>
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                              </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
                <button type="button" id="cancelOrderRete" class="btn btn-danger" data-dismiss="modal"><?= lang('reset') ?></button>
                <button type="button" id="updateOrderRete" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </div>
    </div>
</div>

<!-- /Body -->


<?php echo form_close(); ?>