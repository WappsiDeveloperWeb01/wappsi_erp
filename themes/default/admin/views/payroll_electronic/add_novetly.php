<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
    $max = ($contract_data->contract_status == ACTIVE) ? $payroll_data->end_date : $contract_data->settlement_date;
    $max_date = (date('j', strtotime($max)) == 30) ? date('Y-m-d', strtotime($max. '+ 1 days')) : $max;
?>

<style type="text/css">
    .label-help{
        position: relative;
        top: 2px;
    }
</style>

<div class="modal-dialog" id="add_concept_model">
    <?= form_input(["name"=>"max_date", "id"=>"max_date", "type"=>"hidden", 'value'=>$max_date]); ?>
    <div class="modal-content">
        <?= admin_form_open("payroll_management/save_concept_employee", ["id"=>"add_concept_employee_form"]); ?>
            <?= form_hidden("payroll_id", $payroll_id); ?>
            <?= form_hidden("employee_id", $employee_data->employee_id); ?>
            <?= form_hidden("concept_type", ""); ?>
            <?= form_hidden("earned_deduction", ""); ?>
            <?= form_hidden("contract_id", $contract_data->id); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?> <small><?= $employee_data->name; ?></small></h3>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("payroll_concepts"), "payroll_concept_id"); ?>
                            <select class="form-control select2" name="payroll_concept_id" id="payroll_concept_id">
                                <option value=""><?= lang("select") ?></option>
                                <?php foreach ($concepts as $concept) { ?>
                                    <?php if ($concept->id == COMMISSIONS
                                        || $concept->id == INCENTIVES
                                        || $concept->id == PER_DIEM_CONSTITUTES_SALARY
                                        || $concept->id == MAINTENANCE_ALLOWANCE
                                        || $concept->id == PER_DIEM_NOT_CONSTITUTES_SALARY
                                        || $concept->id == COMPANY_LOAN
                                        || $concept->id == FREE_INVESTMENT_LOAN) { ?>
                                    <?php } else { ?>
                                        <?php if ($concept->id == WITHHOLDING) { ?>
                                            <?php if ($employee_data->withholding_method == HAND_CALCULATION) { ?>
                                                <option value="<?= $concept->id ?>" data-concept_type="<?= $concept->concept_type_id ?>" data-earned_deduction="<?= $concept->type; ?>" data-concept_type_name="<?= $concept->concept_type_name ?>"><?= $concept->name?></option>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <option value="<?= $concept->id ?>" data-concept_type="<?= $concept->concept_type_id ?>" data-earned_deduction="<?= $concept->type; ?>" data-concept_type_name="<?= $concept->concept_type_name ?>"><?= $concept->name?></option>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="text-right">
                                <small class="label label-info label-help" id="concept_type_name"></small>
                                <small class="label label-info label-help" id="concept_earned_deduction"></small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="general_fields_container">
                    <div class="col-sm-12" id="start_date_container">
                        <div class="form-group">
                            <?= form_label(lang("start_date"), "start_date"); ?>
                            <?= form_input(["name"=>"start_date", "id"=>"start_date", "type"=>"date", "class"=>"form-control", "min"=>date('Y-m-d', strtotime($payroll_data->start_date)), "max"=>date('Y-m-d', strtotime($payroll_data->end_date)), "required"=>TRUE]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="frequency_container">
                        <div class="form-group">
                            <?= form_label(lang("parroll_management_frequency"), "frequency"); ?>
                            <?= form_dropdown(['name'=>'frequency', 'id'=>'frequency', 'class'=>'form-control select2'], [ONCE=>lang('parroll_management_once'), RECURRENT=>lang('parroll_management_recurrent'), PERMANENT=>lang('parroll_management_permanent')]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="number_of_times_container">
                        <div class="form-group">
                            <?= form_label(lang("payroll_management_number_time"), "number_of_time"); ?> <i class="fa fa-info-circle pointer" data-toggle="tooltip" data-placement="top" title="Las veces en que se repetirá dicho concepto. Se tendrá en cuenta el mes actual"></i>
                            <?= form_input(['name'=>'number_of_time', 'id'=>'number_of_time', 'type'=>'number', 'class'=>'form-control', 'min'=>2]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="working_days_container">
                        <div class="form-group">
                            <?= form_label(lang('working_days'), 'working_days'); ?>
                            <?= form_input(["name"=>"working_days", "id"=>"working_days", "type"=>"number", "class"=>"form-control", "min"=>1, "max"=>180, "required"=>TRUE]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="days_container">
                        <div class="form-group">
                            <?= form_label(lang('days'), 'days'); ?>
                            <?= form_input(["name"=>"days", "id"=>"days", "type"=>"number", "class"=>"form-control", "min"=>1, "max"=>30, "required"=>TRUE]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="amount_container">
                        <div class="form-group">
                            <?= form_label(lang('parroll_management_value'), 'amount'); ?>
                            <?= form_input(["name"=>"amount", "id"=>"amount", "type"=>"number", "class"=>"form-control", "min"=>1, "required"=>TRUE]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="amount_fees_container">
                        <div class="form-group">
                            <?= form_label(lang('parroll_management_amount_fees'), 'amount_fees'); ?>
                            <?= form_input(["name"=>"amount_fees", "id"=>"amount_fees", "type"=>"number", "class"=>"form-control", "min"=>1, "required"=>TRUE], ($contract_data->contract_status == INACTIVE) ? 1: ''); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="end_date_container">
                        <div class="form-group">
                            <?= form_label(lang('end_date'), 'end_date') ?>
                            <?= form_input(["name"=>"end_date", "id"=>"end_date", "type"=>"date", "class"=>"form-control", "max"=>date('Y-m-d', strtotime($payroll_data->end_date)), "required"=>TRUE]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="description_container">
                        <div class="form-group">
                            <?= form_label(lang('description'), 'description') ?>
                            <?= form_input(["name"=>"description", "id"=>"description", "type"=>"text", "class"=>"form-control"]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12" id="severancePayPaidEmployeeContainer">
                        <div class="form-group">
                            <label for="">
                                <input type="checkbox" class="icheck" name="paid_to_employee" id="paid_to_employee">
                                ¿Concepto pagado al empleado?
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-info text-right" id="text_info_number_days">
                            Se aplicarán <span class="badge badge-info">16</span> días para este novedad
                        </div>
                    </div>
                </div>

                <div class="row" id="overtime_fields_container">
                    <div class="col-sm-12">
                        <table class="table table-condensed" id="add_concept_table" style="margin-bottom: 5px;">
                            <thead>
                                <tr>
                                    <th class="col-sm-8" style="text-align: left">Fecha inicio</th>
                                    <th class="col-sm-2" style="text-align: left">Horas</th>
                                    <th class="col-sm-2" style="text-align: left">Minutos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <?= form_input(["name"=>"overtime_start_date[0]", "id"=>"overtime_start_date", "type"=>"text", "class"=>"form-control", "required"=>TRUE]); ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <?= form_input(["name"=>"hours[0]", "id"=>"hours", "type"=>"number", "class"=>"form-control", 'min'=>'0', 'value'=>'0', 'required'=>TRUE]); ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <?= form_input(["name"=>"minutes[0]", "id"=>"minutes", "type"=>"number", "class"=>"form-control", 'min'=>'0', 'max'=>'59', 'value'=>'0', "required"=>TRUE]); ?>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <?= form_button(['name'=>'add_overtime_field_button', 'id'=>'add_overtime_field_button', 'class'=>'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'Agregar hora extra'], '<i class="fa fa-plus"></i>'); ?>
                        <?= form_button(['name'=>'delete_overtime_field_button', 'id'=>'delete_overtime_field_button', 'class'=>'btn btn-primary', 'data-toggle'=>'tooltip', 'title'=>'Eliminar hora extra'], '<i class="fa fa-trash"></i>'); ?>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <?= form_button(["name"=>"save_button", "id"=>"save_button", "class"=>"btn btn-primary new-button", "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>lang('save')], '<i class="fas fa-check fa-lg"></i>'); ?>
            </div>
        <?= form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
        $('[data-toggle="tooltip"]').tooltip();

        $("#overtime_start_date").datetimepicker({
            autoclose: true,
            format: 'yyyy-mm-dd HH:ii',
            language: 'sma',
            startDate : '<?= $payroll_data->start_date; ?>',
            endDate: '<?= $max_date ?>',
            minuteStep: 5
        }).on('changeDate', function(e) {
            $("#overtime_end_date").datetimepicker('setStartDate', $("#overtime_start_date").val());
        });

        $('#days_container').hide();
        $('#amount_container').hide();
        $('#end_date_container').hide();
        $('#frequency_container').hide();
        $('#start_date_container').hide();
        $('#amount_fees_container').hide();
        $("#text_info_number_days").hide();
        $('#description_container').hide();
        $('#working_days_container').hide();
        $('#general_fields_container').hide();
        $('#overtime_fields_container').hide();
        $("#number_of_times_container").hide();
        $("#severancePayPaidEmployeeContainer").hide();

        $(document).on('change', '#payroll_concept_id', function() {
            get_concept_data();
            change_form_add_concepts($(this).val());
        });

        $(document).on('change', '#hours_quantity', function() {
            if ($(this).val() != '') {
                $('#overtime_end_date').attr('disabled', true);
            } else {
                $('#overtime_end_date').attr('disabled', false);
            }
        });

        $(document).on('change', '#start_date', function() { calculate_minimum_end_date(); calculate_number_of_days(); });
        $(document).on('change', '#end_date', function() { calculate_maximum_start_date(); calculate_number_of_days(); });
        $(document).on('change', '#frequency', function(){ show_hidden_number_of_times_input($(this).val()); });
        $(document).on('click', '#save_button', function() { save_type_concept(); });
    });

    function get_concept_data()
    {
        var concept_type = $('#payroll_concept_id option:selected').data('concept_type');
        var earned_deduction = $('#payroll_concept_id option:selected').data('earned_deduction');
        var concept_type_name = $('#payroll_concept_id option:selected').data('concept_type_name');

        earned_deduction_text = (earned_deduction == '<?= EARNED; ?>') ? '<?= lang("payroll_management_earning") ?>' : '<?= lang("payroll_management_deduction") ?>';

        $('#concept_type_name').html('<i class="fa fa-clock-o"></i> '+concept_type_name);
        $('#concept_earned_deduction').html('<i class="fa fa-clock-o"></i> '+earned_deduction_text);

        $('input[name="concept_type"]').val(concept_type);
        $('input[name="earned_deduction"]').val(earned_deduction);
    }

    function change_form_add_concepts(concept_id)
    {
        if (concept_id == <?= DAYTIME_OVERTIME ?>
            || concept_id == <?= NIGHT_OVERTIME ?>
            || concept_id == <?= NIGHT_SURCHARGE ?>
            || concept_id == <?= HOLIDAY_DAYTIME_OVERTIME ?>
            || concept_id == <?= HOLIDAY_DAYTIME_SURCHARGE ?>
            || concept_id == <?= HOLIDAY_NIGHT_OVERTIME ?>
            || concept_id == <?= HOLIDAY_NIGHT_SURCHARGE ?>) {
            $('#overtime_fields_container').fadeIn();
            $('#general_fields_container').fadeOut();
        } else {
            $('#overtime_fields_container').fadeOut();
            $('#overtime_start_date').attr('required', false);
            $('#hours').attr('required', false);
            $('#minutes').attr('required', false);
            $('#general_fields_container').fadeIn();
        }

        show_hidden_frequency(concept_id);
        show_hidden_value_input(concept_id);
        show_hidden_amount_fees(concept_id);
        show_hidden_end_date_days(concept_id);
        show_hidden_days_container(concept_id);
        show_hidden_start_date_input(concept_id);
        show_hidden_description_input(concept_id);
        show_hidden_working_days_container(concept_id);
        showHiddenseverancePayPaidEmployeeContainer(concept_id);
    }

    function calculate_minimum_end_date()
    {
        $('#end_date').attr('min', $('#start_date').val());
    }

    function calculate_maximum_start_date()
    {
        $('#start_date').attr('max', $('#end_date').val());
    }

    function calculate_number_of_days()
    {
        var payroll_concept_id =  $('#payroll_concept_id').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var payroll_id = $('input[name="payroll_id"]').val();

        if ($('#start_date').val() != '' && $('#end_date').val() != '') {
            $.ajax({
                type: "GET",
                url: site.base_url+'payroll_management/calculate_number_days_ajax/'+payroll_id+'/'+payroll_concept_id+'/'+start_date+'/'+end_date,
                dataType: "html",
                success: function (response) {
                    days_diff = response;

                    $('#text_info_number_days').show();
                    $('#text_info_number_days').html('Se aplicarán <span class="badge badge-info">'+ days_diff + ((days_diff > 1)? ' días':' dia') +'</span> para esta novedad.');
                },
                error:function(response) {
                    console.log(response.responseText);
                }
            });
        }
    }

    function show_hidden_number_of_times_input(frequency)
    {
        if (frequency == '<?= RECURRENT ?>') {
            $("#number_of_times_container").fadeIn();

            <?php if ($contract_data->contract_type == FIXED_TERM) { ?>
                <?php
                    $payroll_end_date = new DateTime($payroll_data->end_date);
                    $contract_end_date = new DateTime($contract_data->end_date);

                    $diff = $payroll_end_date->diff($contract_end_date);

                    $max_number_times = $diff->m;
                 ?>
                $("#number_of_time").prop("max", '<?= $max_number_times; ?>');
                $("#number_of_time").prop("required", true);
            <?php } ?>
        } else {
            $("#number_of_times_container").fadeOut();
            $("#number_of_time").prop("required", false);

            $("#number_of_times_container").fadeOut();
        }
    }

    function show_hidden_start_date_input(concept_id)
    {
        if (concept_id == <?= COMMISSIONS ?> ||
            concept_id == <?= INCENTIVES ?> ||
            concept_id == <?= BONUSES_CONSTITUTING_SALARY ?> ||
            concept_id == <?= PER_DIEM_CONSTITUTES_SALARY ?> ||
            concept_id == <?= COMPANY_LOAN ?> ||
            concept_id == <?= BONUSES_NOT_CONSTITUTING_SALARY ?> ||
            concept_id == <?= BEARING_ALLOWANCE ?> ||
            concept_id == <?= MAINTENANCE_ALLOWANCE ?> ||
            concept_id == <?= COMMON_DISABILITY ?> ||
            concept_id == <?= WORK_DISABILITY ?> ||
            concept_id == <?= PROFESSIONAL_DISABILITY ?> ||
            concept_id == <?= PER_DIEM_NOT_CONSTITUTES_SALARY ?> ||
            concept_id == <?= PAID_LICENSE ?> ||
            concept_id == <?= MATERNITY_LICENSE ?> ||
            concept_id == <?= PATERNITY_LICENSE ?> ||
            concept_id == <?= DUEL_LICENSE ?> ||
            concept_id == <?= NOT_PAID_LICENSE ?> ||
            concept_id == <?= DISCIPLINARY_SUSPENSION ?> ||
            concept_id == <?= FAMILY_ACTIVITY_PERMIT ?> ||
            concept_id == <?= NOT_PAID_PERMIT ?> ||
            concept_id == <?= UNEXCUSED_ABSENTEEISM ?> ||
            concept_id == <?= DRAFT_CREDIT ?> ||
            concept_id == <?= FREE_INVESTMENT_LOAN ?> ||
            concept_id == <?= TIME_VACATION ?> ||
            concept_id == <?= PAID_PERMIT ?> ||
            concept_id == <?= FOOD_BOND ?> ||
            concept_id == <?= SUNDAY_DISCOUNT ?> ) {
            $("#start_date_container").fadeIn();
            $("#start_date").prop("required", true);
        } else {
            $("#start_date_container").fadeOut();
            $("#start_date").prop("required", false);
        }
        $("#start_date").prop("max", '<?= date('Y-m-d', strtotime($payroll_data->end_date)) ?>');
    }

    function show_hidden_value_input(concept_id)
    {
        if (concept_id == <?= COMMISSIONS ?> ||
            concept_id == <?= INCENTIVES ?> ||
            concept_id == <?= BONUSES_CONSTITUTING_SALARY ?> ||
            concept_id == <?= PER_DIEM_CONSTITUTES_SALARY ?> ||
            concept_id == <?= SOLIDARITY_FUND_EMPLOYEES_1 ?> ||
            concept_id == <?= SOLIDARITY_FUND_EMPLOYEES_1_2 ?> ||
            concept_id == <?= SOLIDARITY_FUND_EMPLOYEES_1_4 ?> ||
            concept_id == <?= SOLIDARITY_FUND_EMPLOYEES_1_6 ?> ||
            concept_id == <?= SOLIDARITY_FUND_EMPLOYEES_1_8 ?> ||
            concept_id == <?= SOLIDARITY_FUND_EMPLOYEES_2 ?> ||
            concept_id == <?= WITHHOLDING ?> ||
            concept_id == <?= MONEY_VACATION ?> ||
            concept_id == <?= BEARING_ALLOWANCE ?> ||
            concept_id == <?= MAINTENANCE_ALLOWANCE ?> ||
            concept_id == <?= BONUSES_NOT_CONSTITUTING_SALARY ?> ||
            concept_id == <?= PER_DIEM_NOT_CONSTITUTES_SALARY ?> ||
            concept_id == <?= OTHER_ACCRUED_CONSTITUENTS_SALARY ?> ||
            concept_id == <?= SERVICE_BONUS ?> ||
            concept_id == <?= LAYOFFS ?> ||
            concept_id == <?= LAYOFFS_INTERESTS ?> ||
            concept_id == <?= FOOD_BOND ?> ||
            concept_id == <?= COMPANY_LOAN ?> ||
            concept_id == <?= DRAFT_CREDIT ?> ||
            concept_id == <?= FREE_INVESTMENT_LOAN ?> ||
            concept_id == <?= OTHER_DEDUCTIONS ?> ||
            concept_id == <?= OTHER_ACCRUED_NOT_CONSTITUENTS_SALARY ?> ||
            concept_id == <?= COMPENSATION ?> || concept_id == <?= FISCAL_SEIZURES ?>){
            if (concept_id == <?= BONUSES_NOT_CONSTITUTING_SALARY ?> ||
                concept_id == <?= BONUSES_CONSTITUTING_SALARY ?> ||
                concept_id == <?= BEARING_ALLOWANCE ?> ||
                concept_id == <?= MAINTENANCE_ALLOWANCE ?> ||
                concept_id == <?= PER_DIEM_NOT_CONSTITUTES_SALARY ?>) {
                $bonus_limit = <?= ($employee_data->base_amount * $this->Payroll_settings->bonus_limit_percentage)/100 ?>;
                $("#amount").prop("max", $bonus_limit);
            }

            if (concept_id == <?= LAYOFFS ?> || concept_id == <?= LAYOFFS_INTERESTS ?>) {
                $("#amount").siblings('label').html('Valor de las cesantías');
            } else {
                $("#amount").siblings('label').html('Valor');
            }

            $("#amount_container").fadeIn();
            $("#amount").prop("required", true);
        } else {
            $("#amount_container").fadeOut();
            $("#amount").prop("required", false);
            $("#amount").prop("max", 0);
        }
    }

    function show_hidden_amount_fees(payroll_concept_id)
    {
        if (payroll_concept_id == <?= COMPANY_LOAN ?> /*|| payroll_concept_id == <?= DRAFT_CREDIT ?>*/ || payroll_concept_id == <?= FREE_INVESTMENT_LOAN ?>) {
            $("#amount_fees_container").fadeIn();
            $("#amount_fees").prop("required", true);

            <?php if ($contract_data->contract_status == INACTIVE) { ?>
                $("#amount_fees").prop("readonly", true);
            <?php } ?>

            <?php if ($contract_data->contract_type == FIXED_TERM) { ?>
                <?php
                    $contract_end_date = new DateTime($contract_data->end_date);
                    $payroll_start_date = new DateTime($payroll_data->end_date);

                    $diff = $payroll_start_date->diff($contract_end_date);

                    $max_term = $diff->m;
                ?>
                $("#amount_fees").prop("max", <?= $max_term ?>);
            <?php } ?>
        } else {
            $("#amount_fees_container").fadeOut();
            $("#amount_fees").prop("required", false);
        }
    }

    function show_hidden_frequency(concept_id)
    {
        if (concept_id == <?= COMMISSIONS ?> ||
            concept_id == <?= PER_DIEM_CONSTITUTES_SALARY ?> ||
            concept_id == <?= INCENTIVES ?> ||
            concept_id == <?= BONUSES_CONSTITUTING_SALARY ?> ||
            concept_id == <?= BONUSES_NOT_CONSTITUTING_SALARY ?> ||
            concept_id == <?= BEARING_ALLOWANCE ?> ||
            concept_id == <?= MAINTENANCE_ALLOWANCE ?> ||
            concept_id == <?= PER_DIEM_NOT_CONSTITUTES_SALARY ?> ||
            concept_id == <?= FOOD_BOND ?>
            )
        {
            $("#frequency_container").fadeIn();
        } else {
            $("#frequency_container").fadeOut();
            $('#frequency').select2('val', '<?= ONCE; ?>');
        }

        $('#frequency').trigger('change');
    }

    function show_hidden_end_date_days(concept_id)
    {
        if (concept_id == <?= PAID_LICENSE ?> ||
            concept_id == <?= MATERNITY_LICENSE ?> ||
            concept_id == <?= PATERNITY_LICENSE ?> ||
            concept_id == <?= DUEL_LICENSE ?> ||
            concept_id == <?= NOT_PAID_LICENSE ?> ||
            concept_id == <?= DISCIPLINARY_SUSPENSION ?> ||
            concept_id == <?= FAMILY_ACTIVITY_PERMIT ?> ||
            concept_id == <?= NOT_PAID_PERMIT ?> ||
            concept_id == <?= UNEXCUSED_ABSENTEEISM ?> ||
            concept_id == <?= COMMON_DISABILITY ?> ||
            concept_id == <?= PROFESSIONAL_DISABILITY ?> ||
            concept_id == <?= WORK_DISABILITY ?> ||
            concept_id == <?= TIME_VACATION ?> ||
            concept_id == <?= PAID_PERMIT ?> ||
            concept_id == <?= SUNDAY_DISCOUNT ?>) {
            $('#end_date_container').fadeIn();
            $('#end_date').prop('required', true);
        } else {
            $('#end_date_container').fadeOut();
            $('#end_date').prop('required', false);
            $('#end_date').prop('val', '');
        }

        $('#end_date').prop("max", '<?= date('Y-m-d', strtotime($payroll_data->end_date)) ?>');
    }

    function show_hidden_description_input(concept_id) {
        if(concept_id == <?= COMPANY_LOAN ?> ||
            concept_id == <?= DRAFT_CREDIT ?> ||
            concept_id == <?= COMMISSIONS ?> ||
            concept_id == <?= BONUSES_CONSTITUTING_SALARY ?> ||
            concept_id == <?= INCENTIVES ?> ||
            concept_id == <?= PER_DIEM_CONSTITUTES_SALARY ?> ||
            concept_id == <?= BONUSES_NOT_CONSTITUTING_SALARY ?> ||
            concept_id == <?= BEARING_ALLOWANCE ?> ||
            concept_id == <?= MAINTENANCE_ALLOWANCE ?> ||
            concept_id == <?= PER_DIEM_NOT_CONSTITUTES_SALARY ?> ||
            concept_id == <?= OTHER_ACCRUED_CONSTITUENTS_SALARY ?> ||
            concept_id == <?= FREE_INVESTMENT_LOAN ?> ||
            concept_id == <?= FOOD_BOND ?> ||
            concept_id == <?= SERVICE_BONUS ?> ||
            concept_id == <?= MONEY_VACATION ?> ||
            concept_id == <?= OTHER_ACCRUED_NOT_CONSTITUENTS_SALARY ?>) {
            $('#description_container').fadeIn();
            $('#description').prop('required', true);
        } else {
            $('#description_container').fadeOut();
            $('#description').prop('required', false);
        }
    }

    function show_hidden_working_days_container(concept_id)
    {
        if (concept_id == <?= SERVICE_BONUS ?> || concept_id == <?= LAYOFFS ?> || concept_id == <?= LAYOFFS_INTERESTS ?>) {
            $('#working_days_container').fadeIn();
            $('#working_days').prop('required', true);


            if (concept_id == <?= LAYOFFS ?> || concept_id == <?= LAYOFFS_INTERESTS ?>) {
                $('#working_days').prop('max', 360);
            } else {
                $('#working_days').prop('max', 180);
            }
        } else {
            $('#working_days_container').fadeOut();
            $('#working_days').prop('required', false);
            $('#working_days').prop('max', 0);
        }
    }

    function show_hidden_days_container(concept_id)
    {
        if (concept_id == <?= MONEY_VACATION ?>) {
            $('#days_container').fadeIn();
            $('#days').prop('required', true);
        } else {
            $('#days_container').fadeOut();
            $('#days').prop('required', false);
        }
    }

    function confirmConceptSave()
    {
        event.preventDefault();
        swal({
            title: "¿Seguro de guardar la novedad?",
            text: "Se recalculará los montos en la nómina del empleado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Guardar!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                swal({
                    title: 'Guardando novedad...',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false,
                });

                $('#add_concept_employee_form').submit();
            }
        });
    }

    function showHiddenseverancePayPaidEmployeeContainer(concept_id)
    {
        if (concept_id == <?= LAYOFFS ?> || concept_id == <?= LAYOFFS_INTERESTS ?> || concept_id == <?= MONEY_VACATION ?> || concept_id == <?= SERVICE_BONUS ?>) {
            $('#severancePayPaidEmployeeContainer').fadeIn();
        } else {
            $('#severancePayPaidEmployeeContainer').fadeOut();
        }
    }

    function save_type_concept()
    {
        $("#add_concept_employee_form").validate({
            ignore: [],
            rules: {
                payroll_concept_id:"required"
            },
            messages: {
                payroll_concept_id:"Este campo es obligatorio"
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem[0].tagName == "SELECT") {
                    $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                    $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                } else {
                    elem.addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem[0].tagName == "SELECT") {
                    $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                    $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                } else {
                    elem.removeClass(errorClass);
                }
            }
        });

        if ($('#add_concept_employee_form').valid()) {
            confirmConceptSave();
        }
    }
</script>
