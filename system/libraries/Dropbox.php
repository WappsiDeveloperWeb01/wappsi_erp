<?php 

require_once FCPATH . "vendor/dropbox/vendor/autoload.php";

class CI_Dropbox
{

	public function upload($tempfile, $nombredropbox, $dropboxKey, $dropboxSecret, $dropboxToken){
		$app = new Kunnu\Dropbox\DropboxApp($dropboxKey,$dropboxSecret,$dropboxToken);
		$dropbox = new Kunnu\Dropbox\Dropbox($app);
		try{
			$file = $dropbox->upload($tempfile, $nombredropbox, ['autorename' => true]);
			return true;
		}catch(\exception $e){
			return $e;
		}
	}
	
}