$(document).ready(function (e) {
    localStorage.removeItem('re_retenciones');
    localStorage.removeItem('tax_diferent_msg_showed');
    if (!localStorage.getItem('slcustomerspecialdiscount')) {
        customer_special_discount();
    }
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }
    var $customer = $('#slcustomer');
    $customer.change(function (e) {
    });
    if (slcustomer = localStorage.getItem('slcustomer')) {
        $customer.val(slcustomer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        set_customer_data();
    } else {
        nsCustomer();
    }
    if (currencycode = localStorage.getItem('othercurrencycode')) {
        $('#currency').val(currencycode).trigger('change');
    }
    // Order level shipping and discount localStorage
    if (slseller = localStorage.getItem('slseller')) {
        setTimeout(function() {
            $('#slseller').select2("val", slseller);
        }, 2000);
    }

    if (sldiscount = localStorage.getItem('sldiscount')) {
        $('#sldiscount').val(sldiscount);
    }
    $('#sltax2').change(function (e) {
        localStorage.setItem('sltax2', $(this).val());
        $('#sltax2').val($(this).val());
    });
    $('#slseller').change(function (e) {
        localStorage.setItem('slseller', $(this).val());
    });
    $('#slcustomerbranch').change(function (e) {
        localStorage.setItem('slcustomerbranch', $(this).val());
    });
    if (sltax2 = localStorage.getItem('sltax2')) {
        $('#sltax2').select2("val", sltax2);
    }
    $('#slsale_status').change(function (e) {
        localStorage.setItem('slsale_status', $(this).val());
        if ($(this).val() == 'pending') {
            $('#payments').fadeOut();
        } else {
            $('#payments').fadeIn();
        }
        verify_locked_submit();
    });
    if (slsale_status = localStorage.getItem('slsale_status')) {
        setTimeout(function() {
            $('#slsale_status').select2("val", slsale_status).trigger('change');
        }, 850);
    }
    $('#slpayment_status').change(function (e) {
        var ps = $(this).val();
        if (rete_prev = JSON.parse(localStorage.getItem('slretenciones'))) {
            total_rete_amount = formatDecimal(rete_prev.total_rete_amount);
        } else {
            total_rete_amount = 0;
        }
        localStorage.setItem('slpayment_status', ps);
        $('#amount_1').prop('readonly', false);
        if (ps == 'partial' || ps == 'paid') {
            $('.div_slpayment_term').fadeOut();
            $('#amount_1').val(formatDecimals(parseFloat(gtotal)));
            $('#payments').slideDown();
            $('#pcc_no_1').focus();
        } else {
            $('#payments').slideUp();
            $('.div_slpayment_term').fadeIn();
            $('#slpayment_term').val(1);
            $('#customerpaymentterm').val(1);
        }
        if (localStorage.getItem('slcustomerdepositamount') && ps == 'partial') {

            var giftcard_balance = deposit_amount = 0;
            if (localStorage.getItem('slcustomerdepositamount') &&  deposit_payment_method.sale == true) {
                deposit_amount = parseFloat(localStorage.getItem('slcustomerdepositamount'));
                if (deposit_amount > 0) {
                    msg += formatMoney(deposit_amount)+" de saldo de anticipos ";
                }
            }
            if (localStorage.getItem('slcustomergiftcardbalance')) {
                giftcard_balance = parseFloat(localStorage.getItem('slcustomergiftcardbalance'));
                if (giftcard_balance > 0) {
                    msg += formatMoney(giftcard_balance)+" de saldo de "+lang.gift_card;
                }
            }
            if (deposit_amount > 0 && giftcard_balance > 0) {
                bootbox.confirm({
                    title: "Cliente con Saldo de "+lang.deposit+" o "+lang.gift_card,
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    buttons: {
                        deposit: {
                            label: lang.deposit,
                            className: 'btn-success send-submit-sale btn-full',
                            callback: function () {
                                gtotal = parseFloat(formatDecimal($('#gtotal').text()));
                                if (gtotal > deposit_amount) {
                                    $('#amount_1').val(deposit_amount);
                                    $('#add_more_payments').trigger('click');
                                    setTimeout(function() {
                                        $('#amount_2').val(gtotal - deposit_amount);
                                    }, 1200);
                                }
                                $('#paid_by_1').select2('val', 'deposit').trigger('change');
                                setTimeout(function() {
                                    if ($('#pricesChanged').hasClass('in')) {
                                        $('#pricesChanged').modal('show');
                                    }
                                }, 1200);
                            }
                        },
                        giftcard: {
                            label: lang.gift_card,
                            className: 'btn-success send-submit-sale btn-full',
                            callback: function(){
                                set_giftcard_payments(gtotal);
                            }
                        },
                        cancel: {
                            label: 'No, usar otro medio de pago',
                            className: 'btn-danger btn-full',
                            callback: function(){
                                setTimeout(function() {
                                    if ($('#pricesChanged').hasClass('in')) {
                                        $('#pricesChanged').modal('show');
                                    }
                                }, 1200);
                            }
                        }
                    }
                });
            } else if (deposit_amount > 0 ) {
                bootbox.confirm({
                    title: "Cliente con Saldo de "+lang.deposit,
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    buttons: {
                        deposit: {
                            label: lang.deposit,
                            className: 'btn-success send-submit-sale btn-full',
                            callback: function () {
                                gtotal = parseFloat(formatDecimal($('#gtotal').text()));
                                if (gtotal > deposit_amount) {
                                    $('#amount_1').val(deposit_amount);
                                    $('#add_more_payments').trigger('click');
                                    setTimeout(function() {
                                        $('#amount_2').val(gtotal - deposit_amount);
                                    }, 1200);
                                }
                                $('#paid_by_1').select2('val', 'deposit').trigger('change');
                                setTimeout(function() {
                                    if ($('#pricesChanged').hasClass('in')) {
                                        $('#pricesChanged').modal('show');
                                    }
                                }, 1200);
                            }
                        },
                        cancel: {
                            label: 'No, usar otro medio de pago',
                            className: 'btn-danger btn-full',
                            callback: function(){
                                setTimeout(function() {
                                    if ($('#pricesChanged').hasClass('in')) {
                                        $('#pricesChanged').modal('show');
                                    }
                                }, 1200);
                            }
                        }
                    }
                });
            } else if (giftcard_balance > 0) {
                bootbox.confirm({
                    title: "Cliente con Saldo de "+lang.gift_card,
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    buttons: {
                        giftcard: {
                            label: lang.gift_card,
                            className: 'btn-success send-submit-sale btn-full',
                            callback: function(){
                                set_giftcard_payments(gtotal);
                            }
                        },
                        cancel: {
                            label: 'No, usar otro medio de pago',
                            className: 'btn-danger btn-full',
                            callback: function(){
                                setTimeout(function() {
                                    if ($('#pricesChanged').hasClass('in')) {
                                        $('#pricesChanged').modal('show');
                                    }
                                }, 1200);
                            }
                        }
                    }
                });
            }
        }
    });
    $('#amount_1').on('change, keyup', function(){
        val = $(this).val();
        maxval = $(this).data('maxval');
        if (val > maxval || val < 0) {
            $(this).val(maxval);
        }
    });
    var slpayment_status = 'paid';
    $('#slpayment_status').select2("val", slpayment_status);
    var ps = slpayment_status;
    if (ps == 'partial' || ps == 'paid') {
        $('#payments').slideDown();
        $('#pcc_no_1').focus();
    } else {
        $('#payments').slideUp();
    }
    $(document).on('change', '.paid_by', function () {
        var p_val = $(this).val();
        var pbnum = $(this).data('pbnum');
        var due_payment = $(this).find('option:selected').data('duepayment');
        localStorage.setItem('paid_by', p_val);
        $('#rpaidby').val(p_val);
        if (p_val == 'cash' ||  p_val == 'other') {
            $('.pcheque_'+pbnum).hide();
            $('.pcc_'+pbnum).hide();
            $('.pcash_'+pbnum).show();
            $('#payment_note_'+pbnum).focus();
            $('.div_slpayment_term_'+pbnum).hide();
            $('#add_more_payments').prop('disabled', false);
        } else if (p_val == 'CC') {
            $('.pcheque_'+pbnum).hide();
            $('.pcash_'+pbnum).hide();
            $('.pcc_'+pbnum).show();
            $('#pcc_no_'+pbnum).focus();
            $('.div_slpayment_term_'+pbnum).hide();
            $('#add_more_payments').prop('disabled', false);
        } else if (p_val == 'Cheque') {
            $('.pcc_'+pbnum).hide();
            $('.pcash_'+pbnum).hide();
            $('.pcheque_'+pbnum).show();
            $('#cheque_no_'+pbnum).focus();
            $('.div_slpayment_term_'+pbnum).hide();
            $('#add_more_payments').prop('disabled', false);
        } else {
            $('.pcheque_'+pbnum).hide();
            $('.pcc_'+pbnum).hide();
            $('.pcash_'+pbnum).hide();
            $('.div_slpayment_term_'+pbnum).hide();
            $('#add_more_payments').prop('disabled', false);
        }
        if (p_val == 'gift_card') {
            $('.gc_'+pbnum).show();
            $('.ngc_'+pbnum).show();
            $('#gift_card_no_'+pbnum).focus();
        } else {
            // $('.ngc_'+pbnum).show();
            $('.gc_'+pbnum).hide();
            $('#gc_details_'+pbnum).html('');
        }
        if (due_payment == 1 || p_val == 'Credito') {
            $('.due_payment_'+pbnum).val(1);
            $('.pcc_'+pbnum).hide();
            $('.pcash_'+pbnum).hide();
            $('.pcheque_'+pbnum).hide();
            $('.div_slpayment_term_'+pbnum).show();
            input_slpayment_term = $('.div_slpayment_term_'+pbnum).find('input');
            if ($('#customerpaymentterm').val()) {
                $(input_slpayment_term).val($('#customerpaymentterm').val()).trigger('change');
            }
            $('#add_more_payments').prop('disabled', true);
            var index = $($(this)).index('select.paid_by');
            num_due = 0;
            $.each($('select.paid_by'), function(index, select){
                if ($(select).val() == 'Credito') {
                    num_due++;
                }
            });
            if (num_due > 1) {
                $(this).select2('val', 'cash').trigger('change');
                command: toastr.error('No puede escoger más de una forma de pago a crédito.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }


        } else {
            $('.due_payment_'+pbnum).val(0);
            if (total > 0 && $('#amount_'+pbnum).val() == 0) {
                $('#amount_'+pbnum).val(total);
            }
        }
        setTimeout(function() {
            cargar_codigo_forma_pago_fe();
        }, 850);
    });
    if (paid_by = localStorage.getItem('paid_by')) {
        var p_val = paid_by;
        $('.paid_by').select2("val", paid_by);
        $('#rpaidby').val(p_val);
        if (p_val == 'cash' ||  p_val == 'other') {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').show();
            $('#payment_note_1').focus();
        } else if (p_val == 'CC') {
            $('.pcheque_1').hide();
            $('.pcash_1').hide();
            $('.pcc_1').show();
            $('#pcc_no_1').focus();
        } else if (p_val == 'Cheque') {
            $('.pcc_1').hide();
            $('.pcash_1').hide();
            $('.pcheque_1').show();
            $('#cheque_no_1').focus();
        } else {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').hide();
        }
        if (p_val == 'gift_card') {
            $('.gc').show();
            $('.ngc').hide();
            $('#gift_card_no').focus();
        } else {
            $('.ngc').show();
            $('.gc').hide();
            $('#gc_details').html('');
        }
        setTimeout(function() {
            cargar_codigo_forma_pago_fe();
        }, 850);
    }
    if (gift_card_no = localStorage.getItem('gift_card_no')) {
        $('#gift_card_no').val(gift_card_no);
    }
    $('#gift_card_no').change(function (e) {
        localStorage.setItem('gift_card_no', $(this).val());
    });
    if (amount_1 = localStorage.getItem('amount_1')) {
        $('#amount_1').val(amount_1);
    }
    $('#amount_1').change(function (e) {
        localStorage.setItem('amount_1', $(this).val());
    });

    if (paid_by_1 = localStorage.getItem('paid_by_1')) {
        $('#paid_by_1').val( paid_by_1).trigger('change');
    }
    $('#paid_by_1').change(function (e) {
        localStorage.setItem('paid_by_1', $(this).val());
        if ($(this).val() != 'Credito') {
            // $('#amount_1').val(parseFloat(formatDecimal($('#gtotal').text())));
        }
    });
    if (pcc_holder_1 = localStorage.getItem('pcc_holder_1')) {
        $('#pcc_holder_1').val(pcc_holder_1);
    }
    $('#pcc_holder_1').change(function (e) {
        localStorage.setItem('pcc_holder_1', $(this).val());
    });
    if (pcc_type_1 = localStorage.getItem('pcc_type_1')) {
        $('#pcc_type_1').select2("val", pcc_type_1);
    }
    $('#pcc_type_1').change(function (e) {
        localStorage.setItem('pcc_type_1', $(this).val());
    });
    if (pcc_month_1 = localStorage.getItem('pcc_month_1')) {
        $('#pcc_month_1').val( pcc_month_1);
    }
    $('#pcc_month_1').change(function (e) {
        localStorage.setItem('pcc_month_1', $(this).val());
    });
    if (pcc_year_1 = localStorage.getItem('pcc_year_1')) {
        $('#pcc_year_1').val(pcc_year_1);
    }
    $('#pcc_year_1').change(function (e) {
        localStorage.setItem('pcc_year_1', $(this).val());
    });

    if (pcc_no_1 = localStorage.getItem('pcc_no_1')) {
        $('#pcc_no_1').val(pcc_no_1);
    }
    $('#pcc_no_1').change(function (e) {
        var pcc_no = $(this).val();
        localStorage.setItem('pcc_no_1', pcc_no);
        var CardType = null;
        var ccn1 = pcc_no.charAt(0);
        if(ccn1 == 4)
            CardType = 'Visa';
        else if(ccn1 == 5)
            CardType = 'MasterCard';
        else if(ccn1 == 3)
            CardType = 'Amex';
        else if(ccn1 == 6)
            CardType = 'Discover';
        else
            CardType = 'Visa';

        $('#pcc_type_1').select2("val", CardType);
    });
    if (cheque_no_1 = localStorage.getItem('cheque_no_1')) {
        $('#cheque_no_1').val(cheque_no_1);
    }
    $('#cheque_no_1').change(function (e) {
        localStorage.setItem('cheque_no_1', $(this).val());
    });
    if (payment_note_1 = localStorage.getItem('payment_note_1')) {
        $('#payment_note_1').redactor('set', payment_note_1);
    }
    $('#payment_note_1').redactor('destroy');
    $('#payment_note_1').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('payment_note_1', v);
        }
    });
    var old_payment_term;
    $('#slpayment_term').focus(function () {
        old_payment_term = $(this).val();
    }).change(function (e) {
        var new_payment_term = $(this).val() ? parseFloat($(this).val()) : 0;
        if (!is_numeric($(this).val())) {
            $(this).val(old_payment_term);
            bootbox.alert(lang.unexpected_value);
            return;
        } else {
            if (new_payment_term > 0) {
                localStorage.setItem('slpayment_term', new_payment_term);
                $('#slpayment_term').val(new_payment_term);
            } else {
                localStorage.setItem('slpayment_term', 1);
                $('#slpayment_term').val(1);
            }
        }
    });
    if (slpayment_term = localStorage.getItem('slpayment_term')) {
        $('#slpayment_term').val(slpayment_term);
    }
    var old_shipping;
    $('#slshipping').focus(function () {
        old_shipping = $(this).val();
    }).change(function () {
        var slsh = $(this).val() ? $(this).val() : 0;
        if (!is_numeric(slsh)) {
            $(this).val(old_shipping);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        shipping = parseFloat(slsh);
        localStorage.setItem('slshipping', shipping);
        var gtotal = ((total + invoice_tax) - order_discount) + shipping;

        $('#gtotal').text(formatMoney(gtotal));
        $('#tship').text(formatMoney(shipping));
    });
    if (slshipping = localStorage.getItem('slshipping')) {
        shipping = parseFloat(slshipping);
        $('#slshipping').val(shipping);
    } else {
        shipping = 0;
    }
    $('#add_sale, #edit_sale').attr('disabled', true);
    $(document).on('change', '.rserial', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        slitems[item_id].row.serial = $(this).val();
        localStorage.setItem('slitems', JSON.stringify(slitems));
    });
    // If there is any item in localStorage
    // if (localStorage.getItem('slitems')) {
    //     loadItems();
    // }
    // clear localStorage and reload
    $('#reset, #cancel, #cancel_to_pos').click(function (e) {
        btn_id = $(this).prop('id');
        if (protect_delete == 1 && (protect_delete_pin_request == 3 || protect_delete_pin_request == 2)) {
            biller_pin_code = billers_data[$('#slbiller').val()].pin_code_method == 2 ? md5(billers_data[$('#slbiller').val()].random_pin_code) : billers_data[$('#slbiller').val()].pin_code;
            var boxd = bootbox.dialog({
                title: "<i class='fa fa-key'></i> Código de autorización",
                message: '<input id="pos_pin" name="pos_pin" type="password" placeholder="Código de autorización" class="form-control"> ',
                buttons: {
                    success: {
                        label: "<i class='fa fa-tick'></i> OK",
                        className: "btn-success verify_pin",
                        callback: function () {
                            var pos_pin = md5($('#pos_pin').val());
                            if(pos_pin == biller_pin_code) {
                                clean_localstorage();
                            } else {
                                bootbox.alert('Código de autorización incorrecto');
                            }
                        }
                    }
                }
            });
        } else {
            bootbox.confirm(lang.r_u_sure, function (result) {
                if (result) {
                    clean_localstorage();
                }
            });
        }
        validate_key_log();
    });
    function clean_localstorage(){
        if (localStorage.getItem('slitems')) {
            localStorage.removeItem('slitems');
        }
        if (localStorage.getItem('sldiscount')) {
            localStorage.removeItem('sldiscount');
        }
        if (localStorage.getItem('sltax2')) {
            localStorage.removeItem('sltax2');
        }
        if (localStorage.getItem('slshipping')) {
            localStorage.removeItem('slshipping');
        }
        if (localStorage.getItem('slref')) {
            localStorage.removeItem('slref');
        }
        if (localStorage.getItem('slwarehouse')) {
            localStorage.removeItem('slwarehouse');
        }
        if (localStorage.getItem('slnote')) {
            localStorage.removeItem('slnote');
        }
        if (localStorage.getItem('slseller')) {
            localStorage.removeItem('slseller');
        }
        if (localStorage.getItem('slinnote')) {
            localStorage.removeItem('slinnote');
        }
        if (localStorage.getItem('slcustomer')) {
            localStorage.removeItem('slcustomer');
        }
        if (localStorage.getItem('slcurrency')) {
            localStorage.removeItem('slcurrency');
        }
        if (localStorage.getItem('sldate')) {
            localStorage.removeItem('sldate');
        }
        if (localStorage.getItem('slstatus')) {
            localStorage.removeItem('slstatus');
        }
        if (localStorage.getItem('slbiller')) {
            localStorage.removeItem('slbiller');
        }
        if (localStorage.getItem('gift_card_no')) {
            localStorage.removeItem('gift_card_no');
        }
        if (localStorage.getItem('slordersale')) {
            localStorage.removeItem('slordersale');
        }
        if (localStorage.getItem('slretenciones')) {
            localStorage.removeItem('slretenciones');
        }
        if (localStorage.getItem('othercurrencycode')) {
            localStorage.removeItem('othercurrencycode');
        }
        if (localStorage.getItem('othercurrencytrm')) {
            localStorage.removeItem('othercurrencytrm');
        }
        if (localStorage.getItem('popayment_status')) {
            localStorage.removeItem('popayment_status');
        }
        if (localStorage.getItem('slcustomerspecialdiscount')) {
            localStorage.removeItem('slcustomerspecialdiscount');
        }
        if (localStorage.getItem('customer_validate_min_base_retention')) {
            localStorage.removeItem('customer_validate_min_base_retention');
        }
        if (localStorage.getItem('keep_prices')) {
            localStorage.removeItem('keep_prices');
        }
        if (localStorage.getItem('keep_prices_quote_id')) {
            localStorage.removeItem('keep_prices_quote_id');
        }
        if (localStorage.getItem('price_updated')) {
            localStorage.removeItem('price_updated');
        }
        if (localStorage.getItem('locked_for_provider')) {
            localStorage.removeItem('locked_for_provider');
        }
        if (localStorage.getItem('locked_for_customer_fe_data')) {
            localStorage.removeItem('locked_for_customer_fe_data');
        }
        if (localStorage.getItem('locked_for_biller_resolution')) {
            localStorage.removeItem('locked_for_biller_resolution');
        }
        if (localStorage.getItem('locked_for_items_quantity')) {
            localStorage.removeItem('locked_for_items_quantity');
        }
        if (localStorage.getItem('lock_submit_by_margin')) {
            localStorage.removeItem('lock_submit_by_margin');
        }
        $('#modal-loading').show();
        if (btn_id == 'cancel_to_pos') {
            location.href = site.base_url+"pos";
        } else if (btn_id == 'cancel') {
            if (order_sale) {
                location.href = site.base_url+"sales/orders";
            } else {
                location.href = site.base_url+"sales/add";
            }
        } else {
            location.href = site.base_url+"sales/add";
        }
    }
    $('#add_sale').on('click', function(){
        if (is_sale_edit) {
            var form_name = 'edit_sale_form';
        } else {
            var form_name = 'add_sale_form';
        }
        lock_submit_by_margin = JSON.parse(localStorage.getItem('lock_submit_by_margin'));
        if (lock_submit_by_margin && protect_delete == 1 && (protect_delete_pin_request == 3 || protect_delete_pin_request == 1)) {

            biller_pin_code = billers_data[$('#slbiller').val()].pin_code_method == 2 ? md5(billers_data[$('#slbiller').val()].random_pin_code) : billers_data[$('#slbiller').val()].pin_code;
            setTimeout(function() {
                var boxd = bootbox.dialog({
                    title: "<i class='fa fa-key'></i> Código de autorización",
                    message: 'La venta se encuentra bloqueada por que existen márgenes inválidos en los precios de algunos productos, ingrese el código de autorización para desbloquear </br></br><input id="pos_pin" name="pos_pin" type="password" placeholder="Código de autorización" class="form-control"> ',
                    buttons: {
                        success: {
                            label: "<i class='fas fa-unlock-alt'></i> Desbloquear",
                            className: "btn-success verify_pin",
                            callback: function () {
                                var pos_pin = md5($('#pos_pin').val());
                                if(pos_pin == biller_pin_code) {
                                    $('#loading').fadeIn();
                                    if (localStorage.getItem('slretenciones')) {
                                        localStorage.removeItem('slretenciones');
                                    }
                                    $('#'+form_name).submit();
                                } else {
                                    bootbox.alert('Código de autorización incorrecto');
                                }
                            }
                        }
                    }
                });
            }, 1100);
        } else {
            // $('#loading').fadeIn();
            if (localStorage.getItem('slretenciones')) {
                localStorage.removeItem('slretenciones');
            }
            $('#'+form_name).submit();
            const wrapper = document.getElementById('wrapper'); 
            (function ( ) {
                wrapper.classList.add('wrapper-disabled');
                Swal.fire({
                    title: 'Enviando documento',
                    html: '<div class="dots-container"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>Por favor, espere un momento.',
                    icon: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    customClass: {
                        popup: 'swal2-popup'
                    },
                    backdrop: true
                });
            }) (  );
        }
    });
    // save and load the fields in and/or from localStorage
    $('#slref').change(function (e) {
        localStorage.setItem('slref', $(this).val());
    });
    if (slref = localStorage.getItem('slref')) {
        $('#slref').val(slref);
    }
    $('#slwarehouse').change(function (e) {
        localStorage.setItem('slwarehouse', $(this).val());

        updateQuantityItems($(this).val());
    });
    if (slwarehouse = localStorage.getItem('slwarehouse')) {
        $('#slwarehouse').select2("val", slwarehouse);
    }
    $('#slnote').redactor('destroy');
    $('#slnote').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('slnote', v);
        }
    });
    if (slnote = localStorage.getItem('slnote')) {
        $('#slnote').redactor('set', slnote);
    }
    $('#slinnote').redactor('destroy');
    $('#slinnote').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('slinnote', v);
        }
    });
    if (slinnote = localStorage.getItem('slinnote')) {
        $('#slinnote').redactor('set', slinnote);
    }
    // prevent default action usln enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });
    // Order tax calculation
    if (site.settings.tax2 != 0) {
        $('#sltax2').change(function () {
            localStorage.setItem('sltax2', $(this).val());
            loadItems();
            return;
        });
    }
    // Order discount calculation
    var old_sldiscount;
    $('#sldiscount').focus(function () {
        old_sldiscount = $(this).val();
    }).change(function () {
        var new_discount = $(this).val() ? $(this).val() : '0';
        if (is_valid_discount(new_discount)) {
            localStorage.removeItem('sldiscount');
            localStorage.setItem('sldiscount', new_discount);

            loadItems();
            return;
        } else {
            $(this).val(old_sldiscount);
            bootbox.alert(lang.unexpected_value);
            return;
        }
    });
    /* ----------------------
     * Delete Row Method
     * ---------------------- */
    $(document).on('click', '.sldel', function () {
        button_del = $(this);
        if (protect_delete == 1 && (protect_delete_pin_request == 3 || protect_delete_pin_request == 2)) {
            biller_pin_code = billers_data[$('#slbiller').val()].pin_code_method == 2 ? md5(billers_data[$('#slbiller').val()].random_pin_code) : billers_data[$('#slbiller').val()].pin_code;
            var boxd = bootbox.dialog({
                title: "<i class='fa fa-key'></i> Código de autorización",
                message: '<input id="pos_pin" name="pos_pin" type="password" placeholder="Código de autorización" class="form-control"> ',
                buttons: {
                    success: {
                        label: "<i class='fa fa-tick'></i> OK",
                        className: "btn-success verify_pin",
                        callback: function () {
                            var pos_pin = md5($('#pos_pin').val());
                            if(pos_pin == biller_pin_code) {
                                var row = button_del.closest('tr');
                                var item_id = row.attr('data-item-id');
                                delete slitems[item_id];
                                row.remove();
                                if(slitems.hasOwnProperty(item_id)) { } else {
                                    localStorage.setItem('slitems', JSON.stringify(slitems));
                                    loadItems();
                                    return;
                                }
                            } else {
                                bootbox.alert('Código de autorización incorrecto');
                            }
                        }
                    }
                }
            });
        } else {
            var row = button_del.closest('tr');
            var item_id = row.attr('data-item-id');
            delete slitems[item_id];
            row.remove();
            if(slitems.hasOwnProperty(item_id)) { } else {
                localStorage.setItem('slitems', JSON.stringify(slitems));
                loadItems();
                return;
            }
        }
    });
     $(document).on('change', '#slbiller', function(){
     });
    $(document).on('change', '#document_type_id', function(){
        validate_key_log();
        set_customer_data();
        loadItems();
    });
    /* -----------------------
     * Edit Row Modal Hanlder
     ----------------------- */
    $(document).on('click', '.edit_gc', function () {
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        item_id = row.attr('data-item-id');
        item = slitems[item_id];
        $('#product_id_gift_card').val(item_id);
        if (item.gift_card !== undefined) {
            $('#gc_value').val(item.gift_card.gc_value);
            $('#gc_expiry').val(item.gift_card.gc_expiry);
            $('#card_no').val(item.gift_card.gc_card_no);
        } else {
            $('#gc_value').val('');
            $('#gc_expiry').val($('#gc_expiry').data('originaldate'));
            $('#card_no').val('');
        }
        $('#gc_modal').modal('show');
    });
    $(document).on('click', '.edit', function () {
        var trmrate = 1;
        var key_log = localStorage.getItem('slkeylog');
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        $('#row_id').val(row_id);
        // console.log('TR '+row_id);
        item_id = row.attr('data-item-id');
        item = slitems[item_id];        var qty = row.children().children('.rquantity').val(),
        product_option = row.children().children('.roption').val(),
        unit_price = formatDecimal(row.children().children('.ruprice').val()),
        discount = row.children().children('.rdiscount').val();
        if(item.options !== 'undefined' && item.options !== false && site.settings.product_variant_per_serial == 0) {
            $.each(item.options, function () {
                if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                    unit_price = parseFloat(item.row.real_unit_price)+parseFloat(this.price);
                }
            });
        }
        var real_unit_price = item.row.real_unit_price;
        var net_price = unit_price;
        $('#prModalLabel').text(item.row.name + ' (' + item.row.code + ')');
        $('#pname').val(item.row.name);
        if (site.settings.tax1) {
            // $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax2').val(item.row.consumption_sale_tax);

            if(site.settings.allow_change_sale_iva == 0){
                $('#ptax').select2('readonly', true);
            }
            if (item.row.sale_tax_rate_2_percentage) {
                $('.ptax2_div').removeClass('col-sm-8').addClass('col-sm-4');
                $('#ptax2_percentage').val(item.row.sale_tax_rate_2_percentage);
                $('.ptax2_percentage_div').fadeIn();
            } else {
                $('.ptax2_div').removeClass('col-sm-4').addClass('col-sm-8');
                $('#ptax2_percentage').val('');
                $('.ptax2_percentage_div').fadeOut();
            }
            $('#old_tax').val(item.row.tax_rate);
            var item_discount = 0, ds = discount ? discount : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal(parseFloat(((unit_price) * parseFloat(pds[0])) / 100));
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }
            net_price -= item_discount;
            var pr_tax = item.row.tax_rate, pr_tax_val = 0;
            if (pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            if (slitems[item_id].row.tax_method == 0) {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                net_price -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / 100));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }
                        } else if (this.type == 2) {
                            pr_tax_val = parseFloat(this.rate);
                            pr_tax_rate = this.rate;
                        }
                    }
                });
            }
        } else {
            $('#ptax').select2('val', 1).trigger('change');
        }
        if (site.settings.product_serial !== 0) {
            $('#pserial').val(row.children().children('.rserial').val());
        }
        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.options !== false && site.settings.product_variant_per_serial == 0) {
            var o = 1;
            opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
            $.each(item.options, function () {
                if(o == 1) {
                    if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                }
                $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                o++;
            });
        } else {
            $('.poptions_div').fadeOut();
            product_variant = 0;
        }

        uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        var pref = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.preferences !== null && item.preferences !== undefined && item.preferences !== false) {
            pref = $("<select id=\"ppreferences\" name=\"ppreferences\" class=\"form-control select\" multiple />");
            $.each(item.preferences, function () {
                $("<option />", {value: this.id, text: "("+this.category_name+") "+this.name, "data-pfcatid" : this.preference_category_id}).appendTo(pref);
            });
        } else {
            $('.ppreferences_div').fadeOut();
        }
        if (item.units) {
            uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
            $.each(item.units, function () {
                if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                    if(this.product_unit_id == item.row.product_unit_id_selected) {
                        $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.product_unit_id, text: this.name}).appendTo(uopt);
                    }
                } else {
                    if(this.id == item.row.unit) {
                        $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                    }
                }
            });
            if (Object.keys(item.units).length == 1) {
                $('.punit_div').fadeOut();
            }
        }
        $('#poptions-div').html(opt);
        $('#ppreferences-div').html(pref);
        if (item.preferences_selected !== undefined && item.preferences_selected !== null) {
            $('#ppreferences').val(item.preferences_selected);
        }
        $('#punits-div').html(uopt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#pquantity').val(qty);
        $('#old_qty').val(qty);
        $('#pprice').val(trmrate != 1 ? formatDecimal(trmrate * unit_price, 6) : trmrate * unit_price);
        $('#pprice').data('prevprice', trmrate != 1 ? formatDecimal(trmrate * unit_price, 6) : trmrate * unit_price);
        $('#pprice_ipoconsumo').val('');
        $('#punit_price').val(formatDecimal(parseFloat(unit_price)+parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax)));
        $('#poption').select2('val', item.row.option);
        $('#old_price').val(unit_price);
        $('#item_id').val(item_id);
        $('#pserial').val(row.children().children('.rserial').val());
        $('#ptdiscount').val((formatDecimal(discount * qty) * trmrate));
        $('#pdiscount').val(getDiscountAssigned(discount, trmrate, 1));
        $('#net_price').text(trmrate != 1 ? formatDecimal(trmrate * net_price, 6) : formatMoney(trmrate * net_price));
        $('#pro_tax').text(formatMoney(trmrate * (parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax))));
        $('#under_cost_authorized').val(item.row.under_cost_authorized !== undefined ? item.row.under_cost_authorized : 0);

        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            units = item.units;
            unit_selected = units[item.row.product_unit_id_selected !== undefined && parseFloat(item.row.product_unit_id_selected) > 0 ? item.row.product_unit_id_selected : item.row.unit];
            unit_selected_real_quantity = 1;
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty / unit_selected.operation_value;
                unit_selected_real_quantity = 1 * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty * unit_selected.operation_value;
                unit_selected_real_quantity = 1 / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            }
            $('#pquantity').val(unit_selected_quantity);
            $('#punit').select2('val', item.row.product_unit_id_selected).trigger('change');
            $('#pproduct_unit_price').val((unit_price + parseFloat(item.row.consumption_sale_tax)) * unit_selected_real_quantity);
            $('#pproduct_unit_price').trigger('change');
            $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
        }

        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
        $('#prModal').appendTo("body").modal('show');
        $('#psubtotal').val('');

    });

    $('#prModal').on('shown.bs.modal', function (e) {
        if($('#poption').select2('val') != '') {
            $('#poption').select2('val', product_variant);
            product_variant = 0;
        }
    });

    $(document).on('change', '#pprice, #pprice_ipoconsumo, #ptax, #pdiscount, #ptdiscount, #paddprice', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        $('#under_cost_authorized').val(0);
        if (item_id) {
            var item = slitems[item_id];
        } else {
            return false;
        }
        tax_changed = false;
        if (item.row.tax_rate != $('#ptax').val()) {
            if (item.row.except_category_taxes == true) {
                $('#pprice').val(item.row.price);
            } else {
                new_tax_rate_rate = false;
                $.each(tax_rates, function (index, tr) {
                    if(this.id == $('#ptax').val()){
                        new_tax_rate_rate = tr.rate;
                        return;
                    }
                });
                if (new_tax_rate_rate == 0) {
                    $('#pprice').val(formatDecimal($('#net_price').text()));
                } else {
                    $('#pprice').val(item.row.price);
                }
            }
        } else {
            // $('#pprice').val(item.row.price);
        }

        var unit_price = parseFloat($('#pprice').val());
        var pprice_ipoconsumo = parseFloat($('#pprice_ipoconsumo').val());
        var additional_price = parseFloat($('#paddprice').val());
        var pquantity = $('#pquantity').val();
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
        var dst = parseFloat($('#ptdiscount').val() ? $('#ptdiscount').val() : '0');
        if (item.row.sale_tax_rate_2_percentage) {
            ipoconsumo_percentage_calculated =  calculate_second_tax(item.row.sale_tax_rate_2_percentage, unit_price, item.row.tax_rate, item.row.tax_method, true);
            unit_price = unit_price - ipoconsumo_percentage_calculated[1];
            $('#ptax2').val(ipoconsumo_percentage_calculated[1]);
        }
        if (additional_price > 0) {
            $('#paddprice').val(0);
            unit_price += (additional_price / pquantity);
            $('#pprice').val(unit_price);
        }
        if (parseFloat(dst) > 0) {
            item_discount = formatDecimal(dst / pquantity);
            $('#pdiscount').val(item_discount);
        } else {
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }
        }
        unit_price -= item_discount;
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / 100));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }
        if ((site.settings.disable_product_price_under_cost == 1 || parseFloat($('#pprice').val()) != parseFloat(item.row.price)) && protect_delete == 1 && (protect_delete_pin_request == 3 || protect_delete_pin_request == 1)) {
            if (parseFloat($('#pprice').val()) < parseFloat(item.row.cost)) {
                $('#editItem').prop('disabled', true);
                $('.error_under_cost').fadeIn();
                $('.div_pprice_authorization_code').fadeIn();
                $('#pprice_authorization_code').val('');
                $('#pprice_authorization_code').focus();
            } else if (parseFloat($('#pprice').val()) != parseFloat(item.row.price)) {
                $('#editItem').prop('disabled', true);
                $('.div_pprice_authorization_code').fadeIn();
                $('#pprice_authorization_code').val('');
                $('#pprice_authorization_code').focus();
                $('.error_under_cost').fadeOut();
            } else {
                $('#editItem').prop('disabled', false);
                $('.error_under_cost').fadeOut();
                $('.div_pprice_authorization_code').fadeOut();
            }
        } else {
            if (site.settings.disable_product_price_under_cost == 1 && parseFloat(item.row.cost) > parseFloat($('#pprice').val())) {
                $('#editItem').prop('disabled', true);
                $('.error_under_cost').fadeIn();
            } else {
                $('#editItem').prop('disabled', false);
                $('.error_under_cost').fadeOut();
                $('.div_pprice_authorization_code').fadeOut();
            }
        }

        if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 1  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(unit_price));
        } else if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 0  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(pprice_ipoconsumo));
        } else if (parseFloat($('#pprice_ipoconsumo').val()) <= parseFloat(item.row.consumption_sale_tax)) {
            Command: toastr.error('Por favor ingrese un precio válido', 'Valores incorrectos', {onHidden : function(){}})
            $('#pprice_ipoconsumo').val(0);
        }
        $('#net_price').text(formatMoney(unit_price));
        setTimeout(function() {
            $('#pprice_ipoconsumo').val('');
        }, 500);
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax)));
    });

    $(document).on('click', '#check_pprice_autoriz', function(){
        validation_under_cost();
    });
    $(document).on('keyup', '#pprice_authorization_code', function(e){
        if (e.keyCode == 13) {
            validation_under_cost();
        }
    });

    function validation_under_cost(){
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = slitems[item_id];
        pprice_pos_pin = md5($('#pprice_authorization_code').val());
        biller_pin_code = billers_data[$('#slbiller').val()].pin_code_method == 2 ? md5(billers_data[$('#slbiller').val()].random_pin_code) : billers_data[$('#slbiller').val()].pin_code;
        if (pprice_pos_pin == biller_pin_code) {
            $('#editItem').prop('disabled', false);
            $('.error_under_cost').fadeOut();
            $('.div_pprice_authorization_code').fadeOut();
            $('#under_cost_authorized').val(1);
        }
    }


   $(document).on('change', '#punit', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = slitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 0 && site.settings.precios_por_unidad_presentacion == 2) {
                valor = unit_selected.operation_value;
                var ipoconsumo_um = 0;
                if (unit_selected.operator == "*") {
                    valor =  parseFloat(unit_selected.valor_unitario) / parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) * parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  parseFloat(unit_selected.valor_unitario) * parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) / parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor);
                $('#pproduct_unit_price').val(parseFloat(unit_selected.valor_unitario) + parseFloat(ipoconsumo_um));
                $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
            } else {
                $('#pprice').val(unit_selected.valor_unitario);
                $('#pproduct_unit_price').val(unit_selected.valor_unitario);
            }
            $('#pprice').change();
        } else if(site.settings.prioridad_precios_producto == 11){

        } else {
            if(item.options !== false && site.settings.product_variant_per_serial == 0) {
                $.each(item.options, function () {
                    if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                        aprice = parseFloat(this.price);
                    }
                });
            }
            if(item.units && unit != slitems[item_id].row.base_unit) {
                $.each(item.units, function(){
                    if (this.id == unit) {
                        base_quantity = unitToBaseQty($('#pquantity').val(), this);
                        $('#pprice').val(formatDecimal(((parseFloat(item.row.base_unit_price+aprice))*unitToBaseQty(1, this)))).change();
                    }
                });
            } else {
                $('#pprice').val(formatDecimal(item.row.base_unit_price+aprice)).change();
            } //¡¡¡¡ NO BORRAR !!!!
        }
        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
    });
    $(document).on('change', '#pproduct_unit_price', function(){
        p_unit_price = $(this).val();
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = slitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 0) {
                valor = unit_selected.operation_value;
                ipoconsumo_um = 0;
                if (unit_selected.operator == "*") {
                    valor =  (parseFloat(p_unit_price)) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  (parseFloat(p_unit_price)) * parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor);
            } else {
                $('#pprice').val(p_unit_price);
            }
            $('#pprice').change();
        }

    });



    $(document).on('click', '#calculate_unit_price', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = slitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        tax_rate = formatDecimal($('#ptax option:selected').data('taxrate')) / 100;
        tax_rate = tax_rate != 0 ? tax_rate + 1 : 1;
        var subtotal = parseFloat($('#psubtotal').val()),
        qty = parseFloat($('#pquantity').val());
        if (item.row.tax_method == 1) { tax_rate = 1; }
        $('#pprice').val(formatDecimal(((subtotal) * tax_rate))).change();
        return false;
    });

    /* -----------------------
     * Edit Row Method
     ----------------------- */
    $(document).on('click', '#editItem', function () {
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_rate = false;
        if (new_pr_tax) {
            $.each(tax_rates, function () {
                if (this.id == new_pr_tax) {
                    new_pr_tax_rate = this;
                }
            });
        }
        var price = parseFloat($('#pprice').val());
        if(item.options !== false && site.settings.product_variant_per_serial == 0) {
            var opt = $('#poption').val();
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    price = price-parseFloat(this.price);
                }
            });
        }
        if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
            if(!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > price) {
                bootbox.alert(lang.unexpected_value);
                return false;
            }
        }
        if (!is_numeric($('#pquantity').val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var unit = $('#punit').val();
        var base_quantity = parseFloat($('#pquantity').val());
        if(unit != slitems[item_id].row.base_unit) {
            $.each(slitems[item_id].units, function(){
                if (this.id == unit) {
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }

        if (site.settings.product_variant_per_serial == 0) {
            slitems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '';
        }

        tpax2 = parseFloat($('#ptax2').val() ? $('#ptax2').val() : 0);
        slitems[item_id].row.fup = 1,
        slitems[item_id].row.qty = parseFloat($('#pquantity').val()),
        slitems[item_id].row.base_quantity = parseFloat(base_quantity),
        slitems[item_id].row.consumption_sale_tax = tpax2,
        slitems[item_id].row.real_unit_price = (price-parseFloat(tpax2)) / trmrate,
        slitems[item_id].row.unit_price = (price-parseFloat(tpax2)) / trmrate,
        slitems[item_id].row.unit = unit,
        slitems[item_id].row.under_cost_authorized = $('#under_cost_authorized').val(),
        slitems[item_id].row.price_before_promo = ($('#under_cost_authorized').val() == 1 ? $('#pprice').data('prevprice') : 0),
        slitems[item_id].row.tax_rate = new_pr_tax,
        slitems[item_id].tax_rate = new_pr_tax_rate,
        slitems[item_id].row.discount = $('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '',
        slitems[item_id].row.birthday_discount = 0,
        slitems[item_id].row.serial = $('#pserial').val();
        slitems[item_id].row.name = $('#pname').val();
        prev_product_unit_id = slitems[item_id].row.product_unit_id;
        slitems[item_id].row.product_unit_id = $('#punit').val();
        slitems[item_id].row.product_unit_id_selected = $('#punit').val();
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            var ditem = slitems[item_id];
            unit_selected = ditem.units[unit];
            qty = parseFloat($('#pquantity').val());
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            }
            slitems[item_id].row.qty = unit_selected_quantity;
        } else {
            slitems[item_id].row.unit_price_id = $('#punit').val();
        }
        if (site.settings.prioridad_precios_producto == 11 && prev_product_unit_id !== undefined && prev_product_unit_id != slitems[item_id].row.product_unit_id) {
            wappsiPreciosUnidad(slitems[item_id].row.id,item_id,$('#pquantity').val(), true);
        }
        var preferences_text = '';
        var preferences_selected = [];
        var parr = [];
        $('#ppreferences option:selected').each(function(index, option){
            if (preferences_selected[$(option).data('pfcatid')] === undefined) {
                preferences_selected[$(option).data('pfcatid')] = [];
            }
            preferences_selected[$(option).data('pfcatid')].push(parseInt($(option).val()));
            psplit = $(option).text().split(')');
            psplit_index = psplit[0].replace("(", "");
            if (parr[psplit_index] === undefined) {
                parr[psplit_index] = [];
            }
            parr[psplit_index].push(psplit[1]);
        });
        if ($('#ppreferences option:selected').length > 0) {
            phtml = "";
            for (let preference_category in parr) {
                phtml += "<b>"+preference_category+" : </b>";
                for (let index in parr[preference_category]) {
                    phtml += parr[preference_category][index]+", ";
                }
            }
            slitems[item_id].preferences_text = phtml;
            slitems[item_id].preferences_selected = preferences_selected;
        }
        localStorage.setItem('slitems', JSON.stringify(slitems));
        $('#prModal').modal('hide');
        loadItems();
        return;
    });

    /* -----------------------
     * Product option change
     ----------------------- */
    $(document).on('change', '#poption', function () {
        var row = $('#' + $('#row_id').val()), opt = $(this).val();
        var item_id = row.attr('data-item-id');
        var item = slitems[item_id];
        var unit = $('#punit').val(), base_quantity = parseFloat($('#pquantity').val()), base_unit_price = item.row.base_unit_price;
        if(unit != slitems[item_id].row.base_unit) {
            $.each(slitems[item_id].units, function(){
                if (this.id == unit) {
                    base_unit_price = formatDecimal((parseFloat(item.row.base_unit_price)*(unitToBaseQty(1, this))))
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        $('#pprice').val(parseFloat(base_unit_price)).trigger('change');
        if(item.options !== false && site.settings.product_variant_per_serial == 0) {
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    $('#pprice').val(parseFloat(base_unit_price)+(parseFloat(this.price))).trigger('change');
                }
            });
        }
    });

     /* ------------------------------
     * Sell Gift Card modal
     ------------------------------- */
     $(document).on('click', '#sellGiftCard', function (e) {
        if (count == 1) {
            slitems = {};
            if ($('#slwarehouse').val() && $('#slcustomer').val()) {
                $('#slcustomer').select2("readonly", true);
                $('#slwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#gcModal').appendTo("body").modal('show');
        return false;
    });

     $(document).on('click', '#addGiftCard', function (e) {
        var mid = (new Date).getTime(),
        gccode = $('#gccard_no').val(),
        gcname = $('#gcname').val(),
        gcvalue = $('#gcvalue').val(),
        gccustomer = $('#gccustomer').val(),
        gcexpiry = $('#gcexpiry').val() ? $('#gcexpiry').val() : '',
        gcprice = parseFloat($('#gcprice').val());
        if(gccode == '' || gcvalue == '' || gcprice == '' || gcvalue == 0 || gcprice == 0) {
            $('#gcerror').text('Please fill the required fields');
            $('.gcerror-con').show();
            return false;
        }

        var gc_data = new Array();
        gc_data[0] = gccode;
        gc_data[1] = gcvalue;
        gc_data[2] = gccustomer;
        gc_data[3] = gcexpiry;
        //if (typeof slitems === "undefined") {
        //    var slitems = {};
        //}

        $.ajax({
            type: 'get',
            url: site.base_url+'sales/sell_gift_card',
            dataType: "json",
            data: { gcdata: gc_data },
            success: function (data) {
                if(data.result === 'success') {
                    slitems[mid] = {"id": mid, "item_id": mid, "label": gcname + ' (' + gccode + ')', "row": {"id": mid, "code": gccode, "name": gcname, "quantity": 1, "base_quantity": 1, "price": gcprice, "real_unit_price": gcprice, "tax_rate": 0, "qty": 1, "type": "manual", "discount": "0", "serial": "", "option":""}, "tax_rate": false, "options":false, "units":false};
                    localStorage.setItem('slitems', JSON.stringify(slitems));
                    loadItems();
                    $('#gcModal').modal('hide');
                    $('#gccard_no').val('');
                    $('#gcvalue').val('');
                    $('#gcexpiry').val('');
                    $('#gcprice').val('');
                } else {
                    $('#gcerror').text(data.message);
                    $('.gcerror-con').show();
                }
            }
        });
        return false;
    });

    /* ------------------------------
     * Show manual item addition modal
     ------------------------------- */
     $(document).on('click', '#addManually', function (e) {
        if (count == 1) {
            slitems = {};
            if ($('#slwarehouse').val() && $('#slcustomer').val()) {
                $('#slcustomer').select2("readonly", true);
                $('#slwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#mnet_price').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal').appendTo("body").modal('show');
        return false;
    });

     $(document).on('click', '#addItemManually', function (e) {
        var mid = (new Date).getTime(),
        mcode = $('#mcode').val(),
        mname = $('#mname').val(),
        mtax = parseInt($('#mtax').val()),
        mqty = parseFloat($('#mquantity').val()),
        mdiscount = $('#mdiscount').val() ? $('#mdiscount').val() : '0',
        unit_price = parseFloat($('#mprice').val()),
        mtax_rate = {};
        if (mcode && mname && mqty && unit_price) {
            $.each(tax_rates, function () {
                if (this.id == mtax) {
                    mtax_rate = this;
                }
            });

            slitems[mid] = {"id": mid, "item_id": mid, "label": mname + ' (' + mcode + ')', "row": {"id": mid, "code": mcode, "name": mname, "quantity": mqty, "base_quantity": mqty, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": mtax, "tax_method": 0, "qty": mqty, "type": "manual", "discount": mdiscount, "serial": "", "option":""}, "tax_rate": mtax_rate, 'units': false, "options":false};
            localStorage.setItem('slitems', JSON.stringify(slitems));
            loadItems();
        }
        $('#mModal').modal('hide');
        $('#mcode').val('');
        $('#mname').val('');
        $('#mtax').val('');
        $('#mquantity').val('');
        $('#mdiscount').val('');
        $('#mprice').val('');
        return false;
    });

    $(document).on('change', '#mprice, #mtax, #mdiscount', function () {
        var unit_price = parseFloat($('#mprice').val());
        var ds = $('#mdiscount').val() ? $('#mdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#mtax').val(), item_tax_method = 0;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / 100));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        $('#mnet_price').text(formatMoney(unit_price));
        $('#mpro_tax').text(formatMoney(pr_tax_val));
    });

    /* --------------------------
     * Edit Row Quantity Method
    --------------------------- */
    var old_row_qty;
    $(document).on("focus", '.rquantity', function () {
        old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
        $('#add_sale').prop('disabled', true);
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = formatDecimal($(this).val()),
        item_id = row.attr('data-item-id');

        var expresion=/^\d*(\.\d{1})?\d{0,1}$/;
        if (expresion.test(new_qty) !== true) {
            new_qty = formatQuantity2(new_qty);
            $(this).val(new_qty);
        }
        slitems[item_id].row.base_quantity = new_qty;
        if(slitems[item_id].row.unit != slitems[item_id].row.base_unit) {
            $.each(slitems[item_id].units, function(){
                if (this.id == slitems[item_id].row.unit) {
                    slitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        wappsiPreciosUnidad(slitems[item_id].row.id,item_id,new_qty, true);
        $('.delete_payment_opt').trigger('click');
    }).on('keydown', '.rquantity', function(e){
        if (e.keyCode == 13) {
            $('#add_sale').prop('disabled', true);
            localStorage.setItem('enter_pressed_for_quantity', true);
            $(this).trigger('change');
            setTimeout(function() {
                localStorage.removeItem('enter_pressed_for_quantity');
            }, 2000);
            $('.delete_payment_opt').trigger('click');
        }
    });

    /* --------------------------
     * Edit Row Price Method
     -------------------------- */
     var old_price;
     $(document).on("focus", '.rprice', function () {
        old_price = $(this).val();
    }).on("change", '.rprice', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_price);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_price = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        slitems[item_id].row.price = new_price;
        localStorage.setItem('slitems', JSON.stringify(slitems));
        loadItems();
    });

    $(document).on("click", '#removeReadonly', function () {
        $('#slcustomer').select2('readonly', false);
        //$('#slwarehouse').select2('readonly', false);
        return false;
    });

    $('#aiu_tax').select2('val', site.settings.default_tax_rate);
    if (site.settings.aiu_management == 1) {
        if (localStorage.getItem('aiu_management')) {
            aiu_management = JSON.parse(localStorage.getItem('aiu_management'));
            $('#aiu_admin_percentage').val(aiu_management.aiu_admin_percentage);
            $('#aiu_imprev_percentage').val(aiu_management.aiu_imprev_percentage);
            $('#aiu_util_percentage').val(aiu_management.aiu_util_percentage);
            $('#aiu_tax_base_apply_to').select2('val', aiu_management.aiu_tax_base_apply_to);
            $('#aiu_tax').select2('val', aiu_management.aiu_tax);
            $('.afecta_aiu[value="'+aiu_management.afecta_aiu+'"]').iCheck('check');
            $('#aiu_management').iCheck('check');
        }
    }

    if (typeof invalid_csv_sale_products !== 'undefined') {
        setTimeout(function() {
            bootbox.alert(invalid_csv_sale_products);
            $(".bootbox").find(".modal-dialog").addClass("modal-lg");
        }, 1200);
    }

    $(document).on('ifChecked', '#recurring_sale', function(){
        $('.recurring_div').fadeIn();
        localStorage.setItem('recurring_sale', 1);
    });
    $(document).on('ifUnchecked', '#recurring_sale', function(){
        $('.recurring_div').fadeOut();
        localStorage.removeItem('recurring_sale');
        localStorage.removeItem('recurring_type');
        localStorage.removeItem('recurring_next_date');
    });
    $(document).on('change', '#recurring_type', function(){
        let valor = $(this).val();
        localStorage.setItem('recurring_type', valor);
        let fechaActual = new Date();
        let nuevoAnio = fechaActual.getFullYear();
        let nuevoMes = fechaActual.getMonth();
        let nuevoDia = fechaActual.getDate();
        if (valor == 1) {
            nuevoMes += 1;
        } else if (valor == 2) {
            nuevoAnio += 1;
        }
        if (nuevoMes > 11) {
            nuevoMes = 0;
            nuevoAnio += 1;
        }
        let nuevaFecha = new Date(nuevoAnio, nuevoMes, nuevoDia);
        if (nuevoMes == 1 && nuevoDia == 29 && nuevaFecha.getDate() != 29) {
            nuevaFecha.setDate(28);
        }
        let fechaFormateada = nuevaFecha.toISOString().split('T')[0];
        $('#recurring_next_date').val(fechaFormateada).trigger('change');
    });
    $(document).on('change', '#recurring_next_date', function(){
        let valor = $(this).val();
        localStorage.setItem('recurring_next_date', valor);
    });

    if (recurring_sale = localStorage.getItem('recurring_sale')) {
        $('#recurring_sale').iCheck('check');
    }
    if (recurring_type = localStorage.getItem('recurring_type')) {
        $('#recurring_type').select2('val', recurring_type);
    }
    if (recurring_next_date = localStorage.getItem('recurring_next_date')) {
        $('#recurring_next_date').val(recurring_next_date);
    }

});

var updated_items = 0;
var prices_checked = false;
var deposit_advertisement_showed = false;
/* -----------------------
 * Misc Actions
 ----------------------- */

// hellper function for customer if no localStorage value
function nsCustomer() {
    $('#slcustomer').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "customers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
//localStorage.clear();
/* aca loaditems */

var is_sale_gc = false;
var is_loading_items = false;
function check_prices(){
    slitems = JSON.parse(localStorage.getItem('slitems'));
    sortedItems =  slitems;
    var keep_prices = localStorage.getItem('keep_prices');
    var price_updated = localStorage.getItem('price_updated');
    $('#pricesChanged').modal('hide');
    $('.modal-backdrop').remove();
    if (!keep_prices && (is_quote || is_sale_edit) && price_updated) {
        var prices_changed = false;
        var prices_changed_html = '';
        $.each(sortedItems, function () {
            var item = this;
            var actual_price = item.row.real_unit_price;
            var item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var key_log = localStorage.getItem('slkeylog');
            var slcustomerspecialdiscount = localStorage.getItem('slcustomerspecialdiscount');
            if (key_log == 1 && slcustomerspecialdiscount == 1) {
                item_price = item.row.real_unit_price;
                if (item_tax_method == 1) {
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 0))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                } else {
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                }
                actual_price = item_price;
            }
            if (actual_price != parseFloat(item.row.prev_unit_price)) {
                prices_changed = true;
                prices_changed_html += '<tr>'+
                                            '<td>'+item_name+'</td>'+
                                            '<td class="text-right">'+formatMoney(item.row.prev_unit_price)+'</td>'+
                                            '<td class="text-right">'+formatMoney(actual_price)+'</td>'+
                                       '</tr>';
            }
        });
        if (prices_changed) {
            $('#prices_changed_table').html(prices_changed_html);
            $('#pricesChanged').modal('show');
            return false;
        }
    }
}
var first_id_new_items = false;
var different_recurrency = false;
function loadItems() {
    if (localStorage.getItem('slitems')) {
        $('#birthday_year_applied').val('');
        $('#currency').select2('readonly', true);
        subtotal = 0;
        total = 0;
        count = 1;
        an = 1;
        product_tax = 0;
        product_tax_2 = 0;
        ipoconsumo_total = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        total_discount = 0;
        var lock_submit = false;
        var except_category_taxes = false;
        var trmrate = 1;
        var key_log = localStorage.getItem('slkeylog');
        var slcustomerspecialdiscount = localStorage.getItem('slcustomerspecialdiscount');
        var tax_exempt_customer = false;
        var keep_prices = JSON.parse(localStorage.getItem('keep_prices'));
        var price_updated = JSON.parse(localStorage.getItem('price_updated'));
        var actual_price = false;
        if (keep_prices && price_updated) {
            actual_price = true;
        }
        $("#slTable tbody").empty();
        slitems = JSON.parse(localStorage.getItem('slitems'));
        sortedItems = _.sortBy(slitems, function(o) { return [parseInt(o.order)]; });
        if (site.settings.product_order == 2) {
            sortedItems.reverse();
        }
        if (is_sale_edit && !first_id_new_items) {
            first_id_new_items = (Object.keys(slitems)[0]) - 1;
        }
        slordersale = localStorage.getItem('slordersale');
        if (slordersale) {
            $('#order').val(true);
            $('#quote_id').val(slordersale);
        }
        // if (slordersale || is_fe_pos_sale || sale_has_payments || is_quote) {
        if (is_fe_pos_sale) {
            rquantity_readonly = 'readonly';
        } else {
            rquantity_readonly = '';
        }
 
        $('#edit_sale').attr('disabled', false);
        localStorage.removeItem('lock_submit_by_margin');
        $('.margin_alert').fadeOut();
        var diferent_tax_alert = false;
        var current_recurrency = false;
        $.each(sortedItems, function (index) {
            if (index == 0) {
                subtotal = 0;
                total = 0;
                count = 1;
                an = 1;
                product_tax = 0;
                product_tax_2 = 0;
                ipoconsumo_total = 0;
                invoice_tax = 0;
                product_discount = 0;
                order_discount = 0;
                total_discount = 0;
            }
            var item = this;
            var item_id = (site.settings.item_addition == 1 ? item.item_id : item.id) + (item.row.option && item.row.option > 0 ? item.row.option : '');
            item_num_order = item_num_order == 0 ? item_id - 1000 : item_num_order;
            var gc_incomplete_data = false;
            if (site.settings.gift_card_product_id == item.row.id) {
                is_sale_gc = true;
                if (item.gift_card === undefined || item.gift_card.gc_card_no == null) {
                    gc_incomplete_data = true;
                    lock_submit = true;
                }
            }
            item.order = item.order ? item.order : new Date().getTime();
            if (its_customer_birthday && item.row.birthday_discount) {
                item.row.discount = item.row.birthday_discount;
                $('#birthday_year_applied').val(birthday_year_applied);
            }
            var product_id = item.row.id, item_type = item.row.type, combo_items = item.combo_items, item_qty = formatDecimal(item.row.qty), item_aqty = formatDecimal(item.row.quantity), item_tax_method = item.row.tax_method, item_ds = item.row.discount, item_discount = 0, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var product_unit = item.row.product_unit_id_selected, base_quantity = item.row.base_quantity;
            var unit_price = actual_price ? item.row.unit_price : item.row.real_unit_price;
            var item_price = actual_price ? item.row.unit_price : item.row.real_unit_price;
            var pr_tax_rate = 0;

            if (current_recurrency == false || (item.row.wappsi_invoicing_frecuency > 0 && current_recurrency != item.row.wappsi_invoicing_frecuency)) {
                if ((item.row.wappsi_invoicing_frecuency > 0 && current_recurrency != item.row.wappsi_invoicing_frecuency)) {
                    different_recurrency = true;
                }
                current_recurrency = item.row.wappsi_invoicing_frecuency;
            }
            if (site.settings.prioridad_precios_producto == 7 && item.row.product_unit_id_selected !== undefined && item.units[item.row.product_unit_id_selected] !== undefined && site.settings.validate_um_min_factor_qty == 1) {
                if ($.inArray(site.settings.prioridad_precios_producto, [7, 10, 11])) {
                    unit_selected = item.units[item.row.product_unit_id_selected];
                    if (formatDecimal(item_qty) < formatDecimal(unit_selected.operation_value)) {
                        item_qty = formatDecimal(unit_selected.operation_value);
                        command: toastr.warning('El valor ingresado está por debajo del mínimo de la unidad escogida.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "50000",
                            "extendedTimeOut": "1000",
                        });
                    }
                }
            }

            if(item.options !== false && site.settings.product_variant_per_serial == 0) {
                $.each(item.options, function () {
                    if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                        item_price = unit_price+(parseFloat(this.price));
                        unit_price = item_price;
                    }
                });
            }
            if (item.row.except_category_taxes) {
                except_category_taxes = true;
            }
            if (item.row.tax_exempt_customer && site.settings.enable_customer_tax_exemption == 1) {
                tax_exempt_customer = true;
            }
            if (item.row.diferent_tax_alert) {
                diferent_tax_alert = true;
            }
            pr_t = {id : site.except_tax_rate_id, rate : 0};
            incorrect_tax_rate = false;
            if (item.tax_rate) {
                var pr_tax = item.tax_rate;
            } else {
                var pr_tax = pr_t;
                incorrect_tax_rate = true;
                if (!localStorage.getItem('tax_diferent_msg_showed')) {
                    localStorage.setItem('tax_diferent_msg_showed', 1);
                    command: toastr.warning('El producto '+item_name+' tiene el impuesto incorrecto.', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "50000",
                        "extendedTimeOut": "1000",
                    });
                }

            }
            var pr_tax_val = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_price, item_tax_method))) {
                pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
            }
            item_price = item_tax_method == 1 ? unit_price : unit_price - (pr_tax_val);
            if (pr_tax.type == 2 && item_price < 0) {
                item_price = 0;
            }
            var ds = item_ds ? item_ds : '0';
            item_discount = calculateDiscount(ds, item_price, pr_tax, item_tax_method);
            item_price -= item_discount; //precio sin IVA - Descuento
            if (item_discount > 0) {
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 1))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
            }
            if (key_log == 1 && slcustomerspecialdiscount == 1 && is_fe_pos_sale == false && !sale_has_payments) {
                item_price = item.row.real_unit_price;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, (item_tax_method == 1 ? 0 : item_tax_method)))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                item_price -= pr_tax_val;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                if (item_tax_method == 0) {
                    item_price -= pr_tax_val;
                }
            } else if (key_log == 2 && is_fe_pos_sale == false && !sale_has_payments) {
                item_price = item.row.real_unit_price;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (item_tax_method == 1) {
                    var pr_tax_val = 0;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 1))) {
                        pr_tax_val = ptax[0];
                    }
                    item_price = parseFloat(formatDecimal(item_price)) + parseFloat(formatDecimal(pr_tax_val));
                }
                pr_tax_rate = 0;
                pr_tax_val = 0;
                pr_tax_2_rate = 0;
                product_tax = 0;
                product_tax_2 = 0;
                unit_price = item.row.real_unit_price;
            } else if (key_log == 3 && is_fe_pos_sale == false && !sale_has_payments) {
                item_price = item.row.real_unit_price;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                if (item_tax_method != 1) {
                    item_price -= pr_tax_val;
                }
                pr_tax_rate = 0;
                pr_tax_val = 0;
                pr_tax_2_rate = 0;
                product_tax = 0;
                product_tax_2 = 0;
                unit_price = item.row.real_unit_price;
            }
            pr_tax_2_val = item.row.consumption_sale_tax > 0 ? item.row.consumption_sale_tax : 0;
            pr_tax_2_rate = item.row.sale_tax_rate_2_percentage ? item.row.sale_tax_rate_2_percentage : pr_tax_2_val;
            pr_tax_2_rate_id = item.row.sale_tax_rate_2_id;
            var sel_opt = '';
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.name;
                }
            });
            if (othercurrency = localStorage.getItem('othercurrency')) {
                rate = localStorage.getItem('othercurrencyrate');
                if (trm = localStorage.getItem('othercurrencytrm')) {
                    trmrate = rate / trm;
                }
            }
           var msg_error = false;
            /* profitability_margin */
            if (site.settings.alert_zero_cost_sale == 1 && formatDecimal(item.row.cost) == 0) {
                command: toastr.warning('El producto '+item_name+' tiene costo en 0.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "50000",
                    "extendedTimeOut": "1000",
                });
            }
            if (site.settings.profitability_margin_validation > 0) {
                var margin_cost = site.settings.which_cost_margin_validation == 1 ? item.row.cost : item.row.avg_cost;
                var margin_price = parseFloat(item_price) + parseFloat(pr_tax_val);
                var tax_method = item_tax_method;
                var margin_tax_rate = item.tax_rate.id;
                var margin = 0;
                if (margin_cost > 0 && margin_price > 0 && margin_tax_rate > 0 && tax_method == 0) {
                    var margin_pr_tax = tax_rates[margin_tax_rate];
                    margin_pr_tax_val = 0;
                    if (ptax = calculateTax(margin_pr_tax, margin_price, tax_method)) {
                        margin_pr_tax_val = ptax[0];
                    }
                    var margin_net_price = margin_price - margin_pr_tax_val;
                    if (ptax = calculateTax(margin_pr_tax, margin_cost, tax_method)) {
                        margin_pr_tax_val = ptax[0];
                    }
                    var margin_net_cost = margin_cost - margin_pr_tax_val;
                } else {
                    var margin_net_price = margin_price;
                    var margin_net_cost = margin_cost;
                }
                if (margin_net_price > 0 && margin_net_cost > 0) {

                    if (site.settings.profitability_margin_validation == 1 && item.category !== undefined) {
                        setted_margin = item.category.profitability_margin;
                    } else if(site.settings.profitability_margin_validation == 2 && item.subcategory !== undefined) {
                        setted_margin = item.subcategory.profitability_margin;
                    } else if ((site.settings.profitability_margin_validation == 1 && item.category === undefined) || (site.settings.profitability_margin_validation == 2 && item.subcategory === undefined)) {
                        setted_margin = 0;
                        // lock_submit = true;
                        msg_error = 'El producto no cuenta con la información completa para validación de margen';
                        $('.margin_alert').fadeIn();
                        localStorage.setItem('lock_submit_by_margin', 1);
                    }
                    if (setted_margin == 0) {
                        if (item.subcategory !== undefined && item.subcategory.profitability_margin > 0) {
                            setted_margin = item.subcategory.profitability_margin;
                        } else if (item.category !== undefined && item.category.profitability_margin > 0) {
                            setted_margin = item.category.profitability_margin;
                        }
                    }
                    if (setted_margin > 0) {
                        margin = formatDecimals((((margin_net_price - margin_net_cost) / margin_net_price) * 100), 2);
                        if (parseFloat(margin) <= parseFloat(setted_margin)) {
                            // lock_submit = true;
                            msg_error = 'El margen mínimo establecido para el producto es de '+formatDecimals(setted_margin)+'%, margen actual '+margin+'%;';
                            $('.margin_alert').fadeIn();
                            localStorage.setItem('lock_submit_by_margin', 1);
                        }
                    }
                }
            }
            if (item.row.ignore_hide_parameters == 0 && site.settings.invoice_values_greater_than_zero == 1 && formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty))) == 0) {
                lock_submit = true;
                if (msg_error !== false) {
                    msg_error += 'El total de producto no puede ser igual a 0';
                } else {
                    msg_error = 'El total de producto no puede ser igual a 0';
                }
            }
            if (site.settings.disable_product_price_under_cost == 1) {
                if (parseFloat((site.settings.cost_to_profit_and_validation == 1 ? item.row.cost : item.row.avg_cost)) > parseFloat(unit_price)) {
                    lock_submit = true;
                    if (msg_error !== false) {
                        msg_error += 'El precio del producto no puede estar por debajo del costo';
                    } else {
                        msg_error = 'El precio del producto no puede estar por debajo del costo';
                    }
                }
            }
            var row_no = item.id +  + (item.row.option && item.row.option > 0 ? item.row.option : '');
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                        (is_sale_edit ? '<input name="product_sale_item_id[]" type="hidden" value="' + (item.row.sale_item_id !== undefined ? item.row.sale_item_id : false) + '">' : '') +
                        '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                        '<input name="ignore_hide_parameters[]" type="hidden" class="ignore_hide_parameters" value="' + item.row.ignore_hide_parameters + '">'+
                        '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                        '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                        '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                        '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+

                        '<input name="product_seller_id[]" type="hidden" value="'+ (item.row.seller_id !== undefined && item.row.seller_id ? item.row.seller_id : $('#slseller').val()) +'">'+
                        '<input name="product_gift_card_no[]" type="hidden" value="'+ (item.gift_card !== undefined && item.gift_card.gc_card_no ? item.gift_card.gc_card_no : "" ) +'">'+
                        '<input name="product_gift_card_value[]" type="hidden" value="'+ (item.gift_card !== undefined && item.gift_card.gc_value ? item.gift_card.gc_value : "" ) +'">'+
                        '<input name="product_gift_card_expiry[]" type="hidden" value="'+ (item.gift_card !== undefined && item.gift_card.gc_expiry ? item.gift_card.gc_expiry : "" ) +'">'+

                        '<input name="under_cost_authorized[]" type="hidden" value="'+ (item.row.under_cost_authorized !== undefined ? item.row.under_cost_authorized : 0) +'">'+
                        '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name +(item.row.serialModal_serial === undefined && sel_opt != '' ? ' ('+sel_opt+')' : '')+(item.gift_card !== undefined && item.gift_card.gc_expiry ? " ("+item.gift_card.gc_card_no+")" : "" )+(its_customer_birthday && item.row.birthday_discount ? ' <i class="cambio_colores fa-solid fa-cake-candles"  data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+'Descuento aplicado por cumpleaños de cliente'+'"></i>' : '')+'</span> '+

                        (item.row.op_pending_quantity !== undefined && item.row.op_pending_quantity > 0 ? ' <i class="fa-solid fa-info-circle"  data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+
                                "En stock : "+formatDecimal((parseFloat(item.row.op_pending_quantity)+parseFloat(item.row.quantity)))+" | "+
                                "Pendientes orden de pedido : "+formatDecimal(item.row.op_pending_quantity)+" | "+
                                "Disponibles : "+formatDecimal(item.row.quantity)+
                            '"'+'"></i>': "")+
                        '</span>'+

                        (site.settings.gift_card_product_id == item.row.id ?
                            (gc_incomplete_data ?
                                " <i class='text-danger fa fa-credit-card edit_gc' style='cursor:pointer;' data-itemid='"+item_id+"' id='"+ row_no +"'></i>" : " <i class='text-success fa fa-credit-card edit_gc' style='cursor:pointer;' data-itemid='"+item_id+"' id='"+ row_no +"'></i>")
                            :
                                (rquantity_readonly == false && is_fe_pos_sale == false && !sale_has_payments ?
                                    ' <i class="pull-right fa fa-edit tip pointer edit" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i>'
                                    : '')
                        )+

                        (item.row.serialModal_serial ? "<input type='hidden' name='serialModal_serial[]' value='"+item.row.serialModal_serial+"'>" : "")+
                      '</td>';
            if (site.settings.product_serial == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'">'+
                            '</td>';
            }

            if (site.settings.product_variant_per_serial == 1) {
                tr_html += '<td class="text-right">'+
                                '<span> '+(item.row.serialModal_serial ? item.row.serialModal_serial : '')+'</span>'+
                            '</td>';
            }
            tr_html += '<td class="text-right">'+
                            '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(trmrate * (item_price + item_discount)) + '</span>'+
                        '</td>';
            if ((site.settings.product_discount == 1 && allow_discount == 1) || item_discount) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '">'+
                                '<input class="form-control input-sm" name="product_discount_val[]" type="hidden" value="' + formatDecimal(item_discount * item_qty) + '">'+
                                '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' +get_percentage_from_amount(item_ds, (item_price + item_discount))+ '</span>'+
                            '</td>';
            }
            tr_html += '<td class="text-right">'+
                            '<input class="form-control input-sm text-right rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '">'+
                            '<input class="ruprice" name="unit_price[]" type="hidden" value="' + unit_price + '">'+
                            '<input class="realuprice" name="real_unit_price[]" type="hidden" value="' + (parseFloat(item.row.real_unit_price)) + '">'+
                            '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(trmrate * item_price) + '</span>'+
                        '</td>';
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax.id : site.except_tax_rate_id) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_rate[]" type="hidden" value="' + (key_log == '1' ? pr_tax_rate : 1) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax_val : 0) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_val[]" type="hidden" value="' + (key_log == '1' ? pr_tax_rate : 0) + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (pr_tax_rate ? '(' + pr_tax_rate + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_val));
                if (incorrect_tax_rate) {
                    tr_html += "<span class='text-warning' style='font-weight:600;'>"+lang.incorrect_tax+' <i class="fa fa-exclamation-triangle"></i></span>';
                }
                tr_html += '</td>';
            }
            if (site.settings.ipoconsumo == 3 || site.settings.ipoconsumo == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_2[]" type="hidden" id="product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_rate_id : 1) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_rate_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_rate : 1) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_val : 0) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_val_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_rate : 0) + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '_2">' + (pr_tax_2_rate ? '(' + pr_tax_2_rate + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_2_val * item_qty)) +
                            '</td>';
            }
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">';
                if (msg_error == false) {
                    tr_html += '<span class="text-right">' + formatMoney(formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val))) * trmrate)) + '</span>';
                } else {
                    tr_html += '<span class="text-right text-danger danger bold" data-toggle="tooltip" data-placement="top" title="" style="cursor: pointer" data-original-title="'+msg_error+'"><i class="fa fa-exclamation-triangle"></i>  ' + formatMoney(formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val))) * trmrate)) + '</span>';
                }
                tr_html += '</td>';
            }
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatDecimal(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+rquantity_readonly+'>'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + item_qty + '">'+
                            '<input name="product_aqty[]" type="hidden" class="item_aqty" value="' + formatDecimal(item_aqty) + '">'+
                            (item.row.price_before_promo !== undefined ? '<input name="price_before_promo[]" type="hidden" value="' + item.row.price_before_promo + '">' : '' )+
                        '</td>';
            tr_html += '<td class="text-right"><span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(trmrate * ((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty))) + '</span>'+
                        '</td>';
            tr_html += ( rquantity_readonly == false && is_fe_pos_sale == false && !sale_has_payments ?
                        '<td class="text-center"><i class="fa fa-times tip pointer sldel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>' : '<td class="text-center"></td>');
            newTr.html(tr_html);
            if ((is_sale_edit || is_quote) && site.settings.product_order == 2) {
                newTr.prependTo("#slTable");
            } else{
                newTr.appendTo("#slTable");
            }
            if (item.preferences_text !== null && item.preferences_text !== undefined) {
                var newTr2 = $('<tr class="tr_preferences" style="font-size:112%;"></tr>');
                tr2_html = '<td colspan="5">'+
                            '<input type="hidden" name="product_preferences_text[]" value="'+JSON.stringify(item.preferences_selected)+'">' +
                            item.preferences_text +
                          '</td>';
                newTr2.html(tr2_html);
                newTr2.appendTo("#slTable");
            }
            subtotal += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
            total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
            count += parseFloat(item_qty);
            product_tax += parseFloat(pr_tax_val * item_qty);
            product_tax_2 += parseFloat(pr_tax_2_val * item_qty);
            ipoconsumo_total += parseFloat(pr_tax_2_val * item_qty);
            product_discount += (item_discount * item_qty);
            an++;
            if (item_type == 'standard' && item.options !== false && site.settings.product_variant_per_serial == 0 && is_fe_pos_sale == false) {
                $.each(item.options, function () {
                    if(this.id == item_option && item_qty > this.quantity) {
                        $('#row_' + row_no).addClass('danger');

                        if(site.settings.overselling != 1) {
                            $('#edit_sale').attr('disabled', true);
                            lock_submit = true;
                        }
                    }
                });
            } else if(item_type == 'standard' && base_quantity > item_aqty && is_fe_pos_sale == false) {
                $('#row_' + row_no).addClass('danger');
                if(site.settings.overselling != 1) {
                    $('#edit_sale').attr('disabled', true);
                    lock_submit = true;
                }
            } else if (item_type == 'combo' && is_fe_pos_sale == false) {
                if(combo_items === false) {
                    $('#row_' + row_no).addClass('danger');
                    if(site.settings.overselling != 1) {
                        $('#edit_sale').attr('disabled', true);
                        lock_submit = true;
                    }
                } else {
                    $.each(combo_items, function() {
                       if(parseFloat(this.quantity) < (parseFloat(this.qty)*base_quantity) && this.type == 'standard') {
                           $('#row_' + row_no).addClass('danger');
                            if(site.settings.overselling != 1) {
                                $('#edit_sale').attr('disabled', true);
                                lock_submit = true;
                            }
                       }
                   });
                }
            } 
            else if (item_type == 'standard' && base_quantity > item_aqty && is_sale_edit && (site.settings.highlight_products_without_quantities_in_edit_sales != 0)) {
                $('#row_' + row_no).addClass('danger');
            }
        });

        var col = 2;
        if (site.settings.product_serial == 1) { col++; }
        if (site.settings.product_variant_per_serial == 1) { col++; }
        var tfoot = '<tr id="tfoot" class="tfoot active">'+
                        '<th colspan="'+col+'">Total ('+(an - 1)+')</th>';

        if ((site.settings.product_discount == 1 && allow_discount == 1) || product_discount) {
            tfoot += '<th class="text-right">'+formatMoney(trmrate * product_discount)+'</th>';
        }
        tfoot +='<th class="text-right" id="subtotalS">'+formatMoney(trmrate * subtotal)+'</th>';
        if (site.settings.tax1 == 1) {
            // // console.log('PR TAX :'+product_tax);
            tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax)+'</th>';
        }
        if (site.settings.ipoconsumo == 3 || site.settings.ipoconsumo == 1) {
            tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax_2)+'</th>';
        }
        if (site.settings.tax1 == 1) {
            tfoot += '<th class="text-right"></th>';
        }
        tfoot +='<th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
        tfoot += '<th class="text-right" id="totalS">'+formatMoney(trmrate * total)+'</th>'+
                 '<th class="text-center">'+
                 '  <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>'+
                 '</th>'+
                '</tr>';
        $('#slTable tfoot').html(tfoot);

        // Order level discount calculations
        if (sldiscount = localStorage.getItem('sldiscount')) {
            var ds = sldiscount;
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    // order_discount = formatDecimal((((total) * parseFloat(pds[0])) / 100));
                    order_discount = formatDecimal((((subtotal) * parseFloat(pds[0])) / 100));
                } else {
                    order_discount = formatDecimal(ds);
                }
            } else {
                order_discount = formatDecimal(ds);
            }

            //total_discount += parseFloat(order_discount);
        }
        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (sltax2 = localStorage.getItem('sltax2')) {
                $.each(tax_rates, function () {
                    if (this.id == sltax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        } else if (this.type == 1) {
                            // invoice_tax = formatDecimal((((subtotal - order_discount) * this.rate) / 100));
                            invoice_tax = formatDecimal((((subtotal) * this.rate) / 100));
                        }
                    }
                });
            }
        }
        // slretenciones
        if (slretenciones = JSON.parse(localStorage.getItem('slretenciones'))) {
            total_rete_amount = formatDecimal(slretenciones.total_rete_amount);
        } else {
            total_rete_amount = 0;
        }
        //slretenciones
        shipping = formatDecimal($('#slshipping').val());
        total_discount = parseFloat(order_discount + product_discount);
        // Totals calculations after item addition
        var gtotal = parseFloat(((parseFloat(total) + parseFloat(invoice_tax)) - parseFloat(order_discount)) + parseFloat(shipping));
        $('#amount_1').data('maxval', gtotal).prop('readonly', false);
        total_rete_amount = 0;
        if (slretenciones = JSON.parse(localStorage.getItem('slretenciones'))) {
            total_rete_amount = formatDecimal(slretenciones.total_rete_amount);
        }
        $('#amount_1').val(formatDecimal((gtotal - total_rete_amount), 2));
        if ($('#slpayment_status').val() == 'paid') {
            $('#amount_1').prop('readonly', true);
        }
        $('#total').text(formatMoney(trmrate * total));
        $('#ipoconsumo_total').text(formatMoney(trmrate * ipoconsumo_total));
        $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
        $('#total_items').val((parseFloat(count) - 1));
        $('#tds').text(formatMoney(trmrate * order_discount));
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(trmrate * invoice_tax));
        }
        $('#tship').text(formatMoney(trmrate * shipping));
        $('#gtotal').text(formatMoney(trmrate * gtotal));
        lock_submit_by_min_sale_amount = false;
        if (gtotal > 0 && localStorage.getItem('slbiller') && billers_data[localStorage.getItem('slbiller')].min_sale_amount > 0) {
            $('.text_min_sale_amount').text(formatMoney(billers_data[localStorage.getItem('slbiller')].min_sale_amount));
            if (gtotal < billers_data[localStorage.getItem('slbiller')].min_sale_amount) {
                $('.error_min_sale_amount').fadeIn();
                $('#add_sale').prop('disabled', true);
                lock_submit_by_min_sale_amount = true;
            } else if (gtotal >= billers_data[localStorage.getItem('slbiller')].min_sale_amount) {
                $('.error_min_sale_amount').fadeOut();
                $('#add_sale').prop('disabled', false);
            }
        }
        if (count > 1) {
            if (is_fe_pos_sale == false && is_sale_edit == false) {
                $('#slcustomer').select2("readonly", true);
                $('#slcustomerbranch').select2("readonly", true);
            } else {
                $('#slseller').select2('readonly', true);
            }

            $('#recurring_sale').iCheck('disable');
            $('#recurring_type').select2('readonly', true);
            $('#recurring_next_date').prop('readonly', true);

        } else {
            $('#recurring_sale').iCheck('enable', false);
            $('#recurring_type').select2('readonly', false);
            $('#recurring_next_date').prop('readonly', false);
        }
        if (lock_submit) {
            localStorage.setItem('locked_for_items_quantity', 1);
        } else {
            if (localStorage.getItem('locked_for_items_quantity')) {
                localStorage.removeItem('locked_for_items_quantity');
            }
        }
        if (lock_submit_by_min_sale_amount) {
            localStorage.setItem('lock_submit_by_min_sale_amount', 1);
        } else {
            localStorage.removeItem('lock_submit_by_min_sale_amount');
        }
        if (slretenciones = JSON.parse(localStorage.getItem('slretenciones'))) {
            slretenciones.gtotal = $('#gtotal').text();
            localStorage.setItem('slretenciones', JSON.stringify(slretenciones));
        }
        verify_locked_submit();
        if (site.settings.product_order == 2) {
            an = 2;
        }
        set_page_focus(an);
        $('[data-toggle="tooltip"]').tooltip();
        if (except_category_taxes) {
            $('#except_category_taxes').val(1);
        }
        if (tax_exempt_customer) {
            $('#tax_exempt_customer').val(1);
        }
        $('.paid_by').trigger('change');

        if (diferent_tax_alert) {
            fix_different_taxes();
        }
        if (gtotal > 0 && deposit_advertisement_showed == false) {
            deposit_advertisement_showed = true;
            var msg = "";
            var giftcard_balance = deposit_amount = 0;
            if (localStorage.getItem('slcustomerdepositamount') &&  deposit_payment_method.sale == true) {
                deposit_amount = parseFloat(localStorage.getItem('slcustomerdepositamount'));
                if (deposit_amount > 0) {
                    msg += formatMoney(deposit_amount)+" de saldo de anticipos ";
                }
            }
            if (localStorage.getItem('slcustomergiftcardbalance')) {
                giftcard_balance = parseFloat(localStorage.getItem('slcustomergiftcardbalance'));
                if (giftcard_balance > 0) {
                    msg += formatMoney(giftcard_balance)+" de saldo de "+lang.gift_card+" ";
                }
            }
            if (deposit_amount > 0 || giftcard_balance > 0) {
                bootbox.dialog({
                    title: "Cliente con Saldo de "+lang.deposit+" o "+lang.gift_card,
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    size: 'x2',
                    onEscape: false,
                    backdrop: true,
                    buttons: {
                        deposit: {
                            label: lang.deposit,
                            className: 'btn-success btn-full',
                            callback: function(){
                                if (gtotal > deposit_amount) {
                                    $('#amount_1').val(deposit_amount).trigger('change');
                                    $('.addButton').trigger('click');
                                    $('#amount_2').val(gtotal - deposit_amount).trigger('change');
                               } else {
                                    $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
                               }
                               $('#paid_by_1').select2('val', 'deposit').trigger('change');
                               setTimeout(function() {
                                    $('#amount_1').trigger('change');
                                    // calculateTotals();
                                }, 850);
                            }
                        },
                        giftcard: {
                            label: lang.gift_card,
                            className: 'btn-success btn-full',
                            callback: function(){
                                set_giftcard_payments(gtotal);
                            }
                        },
                        cancel: {
                            label: lang.cancel,
                            className: 'btn-danger btn-full',
                            callback: function(){
                                $('#amount_1').trigger('change');
                                $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
                                setTimeout(function() {
                                    $('#amount_1').trigger('change');
                                    // calculateTotals();
                                }, 850);
                            }
                        }
                    }
                });
            } else if (deposit_amount > 0) {
                bootbox.dialog({
                    title: "Cliente con Saldo de "+lang.deposit,
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    size: 'x2',
                    onEscape: false,
                    backdrop: true,
                    buttons: {
                        deposit: {
                            label: lang.deposit,
                            className: 'btn-success btn-full',
                            callback: function(){
                                if (gtotal > deposit_amount) {
                                    $('#amount_1').val(deposit_amount).trigger('change');
                                    $('.addButton').trigger('click');
                                    $('#amount_2').val(gtotal - deposit_amount).trigger('change');
                               } else {
                                    $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
                               }
                               $('#paid_by_1').select2('val', 'deposit').trigger('change');
                               setTimeout(function() {
                                    $('#amount_1').trigger('change');
                                    // calculateTotals();
                                }, 850);
                            }
                        },
                        cancel: {
                            label: lang.cancel,
                            className: 'btn-danger btn-full',
                            callback: function(){
                                $('#amount_1').trigger('change');
                                $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
                                setTimeout(function() {
                                    $('#amount_1').trigger('change');
                                    // calculateTotals();
                                }, 850);
                            }
                        }
                    }
                });
            } else if (giftcard_balance > 0) {
                bootbox.dialog({
                    title: "Cliente con Saldo de "+lang.gift_card,
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    size: 'x2',
                    onEscape: false,
                    backdrop: true,
                    buttons: {
                        giftcard: {
                            label: lang.gift_card,
                            className: 'btn-success btn-full',
                            callback: function(){
                                set_giftcard_payments(gtotal);
                            }
                        },
                        cancel: {
                            label: lang.cancel,
                            className: 'btn-danger btn-full',
                            callback: function(){
                                $('#amount_1').trigger('change');
                                $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
                                setTimeout(function() {
                                    $('#amount_1').trigger('change');
                                    // calculateTotals();
                                }, 850);
                            }
                        }
                    }
                });
            }

        }

    }
    if (is_sale_gc) {
        $('select.paid_by option').each(function(index, option){
            if ($(option).val() == 'gift_card') {
                $(option).remove();
            }
        });
    }
    set_trm_input_payment(gtotal - total_rete_amount);
    aplicar_aiu();
    setTimeout(function() {
        recalcular_slretenciones();
    }, 1000);
}

function set_trm_input_payment(amount = null){
    if (localStorage.getItem('othercurrencytrm')) {
        $('.trm_amount').fadeIn();
        $('.amount').fadeOut();
        $('#trm_amount_1').val(formatDecimal(amount * getTrmRate())).trigger('keyup');
    } else {
        $('.trm_amount').fadeOut();
        $('.amount').fadeIn();
    }
}
function fix_different_taxes(){
    if (localStorage.getItem('slitems') && !localStorage.getItem('diferent_tax_alert')) {
        var key_log = localStorage.getItem('slkeylog');
        localStorage.setItem('diferent_tax_alert', 1);
        // bootbox.confirm({
        //     message: "<h3>¡Validación tarifa de impuestos!</h3></br>Se detectó que en algunos productos el registro de venta suspendida tiene el impuesto diferente al asignado orignalmente al producto, ¿Desea corregir el impuesto?",
        //     buttons: {
        //         confirm: {
        //             label: 'Asignar el del producto',
        //             className: 'btn-success btn-full'
        //         },
        //         cancel: {
        //             label: 'Conservar el del registro',
        //             className: 'btn-danger btn-full'
        //         }
        //     },
        //     callback: function (result) {
        //         if(result == true) {
                    slitems = JSON.parse(localStorage.getItem('slitems'));
                    $.each(slitems, function(index, item){
                        if (item.row.diferent_tax_alert) {
                            item.row.tax_rate = item.row.real_tax_rate;
                            item.tax_rate = item.real_tax_rate;
                            localStorage.setItem('diferent_tax_alert_fixed', 1);
                        }
                    });
                    localStorage.setItem('slitems', JSON.stringify(slitems));
                    loadItems();
            //     }
            // }

        // });
    }
}

/* -----------------------------
 * Add Sale Order Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
var item_num_order = 0;
 function add_invoice_item(item, modal = false, cont = false) {

    if (recurring_type = localStorage.getItem('recurring_type')) {
        if (recurring_type == 1 && item.row.wappsi_invoicing_frecuency != 30 || recurring_type == 2 && item.row.wappsi_invoicing_frecuency != 365) {
            frlabel = formatDecimal(item.row.wappsi_invoicing_frecuency) > 0 ? (item.row.wappsi_invoicing_frecuency == 30 ? 'Mensual' : 'Anual') : 'Sin frecuencia';
            header_alert('error', 'Producto '+item.row.name+' con frecuencia ('+frlabel+') diferente a la recurrencia configurada en la venta ('+(recurring_type == 1 ? 'Mensual' : 'Anual')+'); No se puede añadir a esta venta.');
            return true;
        }
    }

    if (count == 1) {
        slitems = {};
        if ($('#slwarehouse').val() && $('#slcustomer').val() && is_sale_edit == false) {
            $('#slcustomer').select2("readonly", true);
            $('#slwarehouse').select2("readonly", true);
        } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }
    if (item == null)
        return;

    if (is_sale_edit && site.settings.item_addition == 0) {
        item_num_order = first_id_new_items;
        item_id = first_id_new_items;
        item.id = first_id_new_items;
        item.item_id = first_id_new_items;
        item.order = first_id_new_items;
        first_id_new_items--;
    } else {
        if (order_sale == 1) {
            var item_id = item_num_order;
        } else {
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
        }
    }
    item_id = item_id + (item.row.option && item.row.option > 0 ? item.row.option : '');
    if (slitems[item_id]) {
        var new_qty = parseFloat(slitems[item_id].row.qty) + 1;
        slitems[item_id].row.base_quantity = new_qty;
        if(slitems[item_id].row.unit != slitems[item_id].row.base_unit) {
            $.each(slitems[item_id].units, function(){
                if (this.id == slitems[item_id].row.unit) {
                    slitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        slitems[item_id].row.qty = new_qty;
    } else {
        slitems[item_id] = item;
    }

    if (is_sale_edit && site.settings.item_addition == 0) {

    } else {
        if (order_sale == 1) {
            slitems[item_id].order = item_num_order;
            item_num_order++;
        } else {
            slitems[item_id].order = new Date().getTime();
        }
    }


    if (site.settings.product_variant_per_serial == 1 && modal === false && item.row.attributes == 1) {
        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(slitems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal_option_id').val('');
        $('#serialModal').modal('show');
        localStorage.setItem('modalSerial_item', JSON.stringify(slitems[item_id]));
        delete slitems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
        }, 850);
    } else {
        if (site.settings.product_variant_per_serial == 1) {
            localStorage.removeItem('modalSerial_item');
            if (site.settings.product_variant_per_serial == 1 && cont == false) {
                $('#serialModal').modal('hide');
                $('#add_item').val('').focus();
                localStorage.setItem('slitems', JSON.stringify(slitems));
                wappsiPreciosUnidad(slitems[item_id].row.id,item_id,slitems[item_id].row.qty, true);
                if (site.pos_settings.show_variants_and_preferences == 1 && slitems[item_id].preferences) {
                    setTimeout(function() {
                        $('#myModal').modal({remote: site.base_url + 'pos/product_preferences_selection/' + slitems[item_id].row.id + "/" + item_id+"/slitems",
                    backdrop: 'static',
                    keyboard: false});
                        $('#myModal').modal('show');
                    }, 850);
                }
            } else {
                item = slitems[item_id];
                $.ajax({
                    url : site.base_url + "products/get_random_id"
                }).done(function(data){
                    item.id = data;
                    item.item_id = data;
                    delete slitems[item_id];
                    item_id = data;
                    slitems[data] = item;
                    localStorage.setItem('modalSerial_item', JSON.stringify(item));
                    $('#serialModal_serial').val('');
                    $('#serialModal_meters').val('');
                    localStorage.setItem('slitems', JSON.stringify(slitems));
                    wappsiPreciosUnidad(slitems[item_id].row.id,item_id,slitems[item_id].row.qty, true);
                });
            }
        } else {
            localStorage.setItem('slitems', JSON.stringify(slitems));
            wappsiPreciosUnidad(slitems[item_id].row.id,item_id,slitems[item_id].row.qty, true);
            if (site.pos_settings.show_variants_and_preferences == 1 && slitems[item_id].preferences) {
                setTimeout(function() {
                    $('#myModal').modal({remote: site.base_url + 'pos/product_preferences_selection/' + slitems[item_id].row.id + "/" + item_id+"/slitems",
                    backdrop: 'static',
                    keyboard: false});
                    $('#myModal').modal('show');
                }, 850);
            }
        }
        // loadItems();
        return true;
    }

}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}

// JS slretenciones


    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_fuente_option').select2('val', '').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_iva_option').select2('val', '').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_ica_option').select2('val', '').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            reset_rete_bomberil();
            setReteTotalAmount(rete_ica_amount, '-');
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_otros_option').select2('val', '').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    $(document).on('click', '#rete_bomberil', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/BOMB"
            }).done(function(data){
                $('#rete_bomberil_option').html(data);
                $('#rete_bomberil_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_bomberil_option').select2('val', '').attr('disabled', true).select();
            $('#rete_bomberil_tax').val('');
            $('#rete_bomberil_valor').val('');
            setReteTotalAmount(rete_bomberil_amount, '-');
        }
    });

    $(document).on('click', '#rete_autoaviso', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/TABL"
            }).done(function(data){
                $('#rete_autoaviso_option').html(data);
                $('#rete_autoaviso_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_autoaviso_option').select2('val', '').attr('disabled', true).select();
            $('#rete_autoaviso_tax').val('');
            $('#rete_autoaviso_valor').val('');
            setReteTotalAmount(rete_autoaviso_amount, '-');
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;
    var rete_bomberil_amount = 0;
    var rete_autoaviso_amount = 0;

    $(document).on('change', '#rete_fuente_option, #rete_iva_option, #rete_ica_option, #rete_otros_option, #rete_bomberil_option, #rete_autoaviso_option', function(){
        rete_sale_option_changed($(this).prop('id'), getTrmRate());
    });

    function getReteAmount(apply){
        amount = 0;
        if (apply == "ST") {
            amount = $('#subtotalS').text();
        } else if (apply == "TX") {
            amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = $('#totalS').text();
        } else if (apply == "IC") {
            amount = $('#rete_ica_valor').val();
        }
        return amount;
    }

    function setReteTotalAmount(amount, action){
        // // console.log('Monto : '+amount+' acción : '+action);
        rtotal_rete =
                    formatDecimal($('#rete_fuente_valor').val()) +
                    formatDecimal($('#rete_iva_valor').val()) +
                    formatDecimal($('#rete_ica_valor').val()) +
                    formatDecimal($('#rete_otros_valor').val())+
                    formatDecimal($('#rete_bomberil_valor').val())+
                    formatDecimal($('#rete_autoaviso_valor').val())
                    ;
        trmrate = getTrmRate();
        // $('#total_rete_amount_show').text(formatMoney(trmrate * rtotal_rete));
        $('#total_rete_amount').text(formatMoney(rtotal_rete));
    }


// JS slretenciones

    $(document).on('click', '#updateOrderRete', function () {
        $('#add_sale').prop('disabled', true);
        $('#ajaxCall').fadeIn();
        rete_prev = JSON.parse(localStorage.getItem('slretenciones'));
        if ($('#aiu_management').is(':checked')) {
            localStorage.removeItem('aiu_management');
            var aiu_management =
                    {
                    'aiu_admin_percentage' : $('#aiu_admin_percentage').val(),
                    'aiu_imprev_percentage' : $('#aiu_imprev_percentage').val(),
                    'aiu_util_percentage' : $('#aiu_util_percentage').val(),
                    'aiu_tax' : $('#aiu_tax').val(),
                    'aiu_tax_base_apply_to' : $('#aiu_tax_base_apply_to').val(),
                    'afecta_aiu' : $('.afecta_aiu:checked').val(),
                    }
                ;
            localStorage.setItem('aiu_management', JSON.stringify(aiu_management));
        }
        var slretenciones = {
            'gtotal' : (rete_prev && rete_prev.gtotal > 0 ? rete_prev.gtotal : parseFloat(formatDecimal($('#gtotal').text()) - formatDecimal($('#slshipping').val()) + formatDecimal(order_discount))),
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            'bomberil_option' : $('#rete_bomberil_option option:selected').data('percentage'),
            'id_rete_bomberil' : $('#rete_bomberil_option').val(),
            'autoaviso_option' : $('#rete_autoaviso_option option:selected').data('percentage'),
            'id_rete_autoaviso' : $('#rete_autoaviso_option').val(),

            'fuente_base_apply' : $('#rete_fuente_base_apply').val(),
            'iva_base_apply' : $('#rete_iva_base_apply').val(),
            'ica_base_apply' : $('#rete_ica_base_apply').val(),
            'other_base_apply' : $('#rete_other_base_apply').val(),
            };
        if (rete_prev && rete_prev.prev_aiu !== undefined) {
            if (rete_prev.gtotal > total) {
                rete_prev.gtotal = rete_prev.gtotal - (rete_prev.prev_aiu + rete_prev.prev_aiu_tax);
            }
        }
        slretenciones.total_discounted = (parseFloat(formatDecimal((rete_prev && rete_prev.gtotal > 0 ? rete_prev.gtotal : total))) - parseFloat(formatDecimal($('#total_rete_amount').text())));
        if (site.settings.aiu_management == 1 && $('#aiu_management').is(':checked')) {
            slretenciones.total_discounted = formatDecimal(slretenciones.total_discounted) + formatDecimal($('#aiu_tax_total').val());
            total_aiu = 0;
            if ($('.afecta_aiu:checked').val() == 1) {
                total_aiu = formatDecimal(parseFloat($('#aiu_admin_total').val()) + parseFloat($('#aiu_imprev_total').val()) + parseFloat($('#aiu_util_total').val()));
                slretenciones.total_discounted = formatDecimal(slretenciones.total_discounted) + total_aiu;
            }
            slretenciones.prev_aiu = total_aiu;
            slretenciones.prev_aiu_tax = formatDecimal($('#aiu_tax_total').val());
        }

        setTimeout(function() {
            shipping = formatDecimal($('#slshipping').val());
            discount = formatDecimal(order_discount);
            $('#gtotal').text(formatMoney(slretenciones.total_discounted + shipping - discount));
            $('#amount_1').val(parseFloat(formatDecimal(slretenciones.total_discounted + shipping - discount)));
            $('#ajaxCall').fadeOut();
            verify_locked_submit();
            $('#paid_by_1').trigger('change');
        }, 1000);
        localStorage.setItem('slretenciones', JSON.stringify(slretenciones));
        if (parseFloat(formatDecimal(slretenciones.total_rete_amount)) > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(slretenciones.total_rete_amount)));
        } else {
            $('#rete_applied').val(0);
        }
        $('#rete').val(slretenciones.total_rete_amount);
        $('#rtModal').modal('hide');

    });

    $('#rtModal').on('hidden.bs.modal', function () {
        // loadItems();
    });

     $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('slretenciones');
        // localStorage.removeItem('aiu_management');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#rete_bomberil').prop('checked', true).trigger('click');
        $('#rete_autoaviso').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
     });

function recalcular_slretenciones(){
    $('#add_sale').prop('disabled', true);
    slretenciones = JSON.parse(localStorage.getItem('slretenciones'));
    if (slretenciones != null) {
        if (slretenciones.id_rete_fuente > 0) {
            if (!$('#rete_fuente').is(':checked')) {
                $('#rete_fuente').trigger('click');
            }
        }
        if (slretenciones.id_rete_iva > 0) {
            if (!$('#rete_iva').is(':checked')) {
                $('#rete_iva').trigger('click');
            }
        }
        if (slretenciones.id_rete_ica > 0) {
            if (!$('#rete_ica').is(':checked')) {
                $('#rete_ica').trigger('click');
            }
        }
        if (slretenciones.id_rete_otros > 0) {
            if (!$('#rete_otros').is(':checked')) {
                $('#rete_otros').trigger('click');
            }
        }
        if (slretenciones.id_rete_bomberil > 0) {
            if (!$('#rete_bomberil').is(':checked')) {
                $('#rete_bomberil').trigger('click');
            }
        }
        if (slretenciones.id_rete_autoaviso > 0) {
            if (!$('#rete_autoaviso').is(':checked')) {
                $('#rete_autoaviso').trigger('click');
            }
        }
        $('#add_sale').prop('disabled', true);
        setTimeout(function() {
            if (slretenciones) {
                $('#rete_fuente_base_apply').select2('val', slretenciones.fuente_base_apply);
                $('#rete_iva_base_apply').select2('val', slretenciones.iva_base_apply);
                $('#rete_ica_base_apply').select2('val', slretenciones.ica_base_apply);
                $('#rete_other_base_apply').select2('val', slretenciones.other_base_apply);

                $.each($('#rete_fuente_option option'), function(index, value){
                    if(slretenciones.id_rete_fuente != '' && value.value == slretenciones.id_rete_fuente){
                        $('#rete_fuente_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_iva_option option'), function(index, value){
                    if(slretenciones.id_rete_iva != '' && value.value == slretenciones.id_rete_iva){
                        $('#rete_iva_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_ica_option option'), function(index, value){
                    if(slretenciones.id_rete_ica != '' && value.value == slretenciones.id_rete_ica){
                        $('#rete_ica_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_otros_option option'), function(index, value){
                    if(slretenciones.id_rete_otros != '' && value.value == slretenciones.id_rete_otros){
                        $('#rete_otros_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_bomberil_option option'), function(index, value){
                    if(slretenciones.id_rete_bomberil != '' && value.value == slretenciones.id_rete_bomberil){
                        $('#rete_bomberil_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_autoaviso_option option'), function(index, value){
                    if(slretenciones.id_rete_autoaviso != '' && value.value == slretenciones.id_rete_autoaviso){
                        $('#rete_autoaviso_option').select2('val', value.value).trigger('change');
                    }
                });

                $('#updateOrderRete').trigger('click');
                $('#add_sale').prop('disabled', false);
            }

        }, 3000);
    }
}

if ($('#rete_applied').val()) {
  total_rete_amount = $('#total_rete_amount').text();
  total_rete_amount = parseFloat(formatDecimal(total_rete_amount));
} else {
  total_rete_amount = 0;
}

// JS slretenciones
var idsProcesados = {};
function wappsiPreciosUnidad(id,item_id,new_qty, load_items = false) {
    var keep_prices = localStorage.getItem('keep_prices');
    // if (site.settings.prioridad_precios_producto != 7 && site.settings.prioridad_precios_producto != 10) {
        if (keep_prices == 'true' || sale_has_payments || slitems[item_id].row.fup == true) {
            slitems[item_id].row.qty = new_qty;
            localStorage.setItem('price_updated', true);
            localStorage.setItem('slitems', JSON.stringify(slitems));
            if (load_items) {
                loadItems();
            }
            return false;
        } else {
            var biller_price_group = $('#slbiller option:selected').data('pricegroupdefault');
            var customer = $('#slcustomer').val();
            var biller = $('#slbiller').val();
            var address_id = $('#slcustomerbranch').val();
            var unit_price_id = null;
            if (site.settings.precios_por_unidad_presentacion == 2 || site.settings.prioridad_precios_producto == 11 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 7) {
                if (item_selected = slitems[item_id]) {
                    unit_price_id = item_selected.row.product_unit_id_selected;
                }
            }
            var datos = {
                            id : id,
                            new_qty : new_qty,
                            price_group : biller_price_group,
                            customer : customer,
                            address_id : address_id,
                            biller : biller,
                            prioridad : site.settings.prioridad_precios_producto,
                            unit_price_id : unit_price_id
                        };
            if (!idsProcesados[id]) {
                if (site.settings.item_addition == 1) {
                    idsProcesados[id] = true;
                }

                $.ajax({
                    type: 'get',
                    url: site.base_url+"sales/preciosUnidad",
                    dataType: "json",
                    data: datos,
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        if (data !== null && data[0] !== null && data[0] != 0) {
                            var valor = data[0];
                            if (slitems[item_id]) {
                                if (data[5] !== undefined && slitems[item_id].row.price_before_promo === undefined) {
                                    slitems[item_id].row.price_before_promo = data[5];
                                }
                                slitems[item_id].row.unit_price = slitems[item_id].row.base_unit_price = slitems[item_id].row.real_unit_price = valor;
                                if (site.settings.precios_por_unidad_presentacion == 2) {
                                    slitems[item_id].row.unit_price_quantity = data[1];
                                    if (data[1] !== undefined) {
                                        slitems[item_id].row.unit_price = slitems[item_id].row.base_unit_price = slitems[item_id].row.real_unit_price = valor / data[1];
                                    }
                                }
                                if (data[2] !== undefined && data[2] != '0%') {
                                    slitems[item_id].row.discount = data[2];
                                }
                                if (data[3] !== undefined) {
                                    slitems[item_id].row.tax_rate = data[3];
                                    slitems[item_id].tax_rate = data[4];
                                    slitems[item_id].row.tax_exempt_customer = true;
                                }
                            }
                        }
                        slitems[item_id].row.qty = new_qty;
                        localStorage.setItem('price_updated', true);
                        localStorage.setItem('slitems', JSON.stringify(slitems));
                        $('#payment').prop('disabled', true);
                        updated_items++;
                        if (updated_items == Object.keys(slitems).length) {
                            updated_items = 0;
                            loadItems();
                            if (prices_checked == false) {
                                check_prices();
                                prices_checked = true;
                            }
                        } else if (load_items == true) {
                            updated_items = 0;
                            loadItems();
                        }
                    }
                });
            } else {
                slitems[item_id].row.qty = new_qty;
                localStorage.setItem('slitems', JSON.stringify(slitems));
                updated_items = 0;
                loadItems();
            }
        }
    // }
}
// Termina wappsiPreciosUnidad

function validate_key_log(){
    var documnet_type_id = $('#document_type_id').val();
    if (documnet_type_id != '' && is_fe_pos_sale == false) {
        $.ajax({
            type: "get", async: false,
            url: site.base_url+"sales/validate_key_log/" + $('#document_type_id').val(),
            dataType: "json",
            success: function (data) {
                localStorage.setItem('slkeylog', data);
                if (data != 1 && data != null) {
                    $('#slbiller').select2('readonly', true);
                    $('#document_type_id').select2('readonly', true);
                    command: toastr.warning('Tipo de documento cambiado, refresque o reinicie si quiere volver a uno normal.', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                }
                loadItems();
            }
        });
    }
}

$('#currency').on('change', function(){
    default_currency = site.settings.default_currency;
    actual_currency = $(this).val();
    actual_currency_rate = $('#currency option:selected').data('rate');
    if (default_currency != actual_currency) {
        $('.trm-control').css('display', '');
        $('#trm').prop('required', true);
        if (othercurrencytrm = localStorage.getItem('othercurrencytrm')) {
            $('#trm').val(othercurrencytrm);
        }
        localStorage.setItem('othercurrency', true);
        localStorage.setItem('othercurrencycode', actual_currency);
        localStorage.setItem('othercurrencyrate', actual_currency_rate);
        $('.currency').text(actual_currency);
    } else {
        $('.trm-control').css('display', 'none');
        $('#trm').prop('required', false);
        localStorage.removeItem('othercurrency');
        localStorage.removeItem('othercurrencycode');
        localStorage.removeItem('othercurrencyrate');
        localStorage.removeItem('othercurrencytrm');
        $('#trm').val('');
        $('.currency').text(default_currency);
    }
    loadItems();
});

$('#trm').on('change', function(){
    trm = $(this).val();

    if(trm != '' && trm != 0) {
        localStorage.setItem('othercurrencytrm', trm);
    } else {
        localStorage.removeItem('othercurrencytrm');
    }

    loadItems();

});



function getTrmRate(){
    trmrate = 1;
    if (othercurrency = localStorage.getItem('othercurrency')) {
        othercurrencytrm = localStorage.getItem('othercurrencytrm');
        othercurrencyrate = localStorage.getItem('othercurrencyrate');
        othercurrencycode = localStorage.getItem('othercurrencycode');
        if (othercurrencytrm !== null && othercurrencytrm !== undefined) {
            trmrate = othercurrencyrate / othercurrencytrm;
        }
    }

    return trmrate;
}

$('#slcustomer').on('change', function(){
    var prev_customer = localStorage.getItem('slcustomer');
    var current_customer = $(this).val();
    slitems = JSON.parse(localStorage.getItem('slitems'));

    var keep_prices = localStorage.getItem('keep_prices');
    if (prev_customer != current_customer) {
        idsProcesados = {};
    }
    if (    (
                (order_sale && site.settings.loaded_suspended_sale_validate_prices == 1) ||
                (!order_sale)
            ) &&
            is_fe_pos_sale == false && !sale_has_payments && 
            slitems && prev_customer != current_customer && 
            site.settings.prioridad_precios_producto != 1 && 
            site.settings.prioridad_precios_producto != 2
        ) {
        bootbox.confirm({
            message: "Al cambiar el cliente, los precios se actualizarán de acuerdo con las condiciones del mismo. Que desea hacer ?",
            buttons: {
                confirm: {
                    label: 'Cambiar cliente',
                    className: 'btn-success send-submit-sale'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    localStorage.setItem('slcustomer', current_customer);
                } else {
                    $('#slcustomer').select2('val', prev_customer);
                }
                set_customer_data();
            }
        });
    } else {
        localStorage.setItem('slcustomer', current_customer);
        set_customer_data();
    }
});

function set_customer_data(){
    if (!is_sale_edit) {
        $('#cancelOrderRete').trigger('click');
    }
    customer = $('#slcustomer');
    validate_fe_customer_data();
    set_customer_payment();
    customer_special_discount();
    customer_retentions();
    $.ajax({
        url: site.base_url+"sales/getCustomerAddresses",
        type: "get",
        data: {"customer_id" : customer.val()}
    }).done(function(data){
        if (data != false) {
            $('#slcustomerbranch').html(data);
            if (slcustomerbranch = localStorage.getItem('slcustomerbranch')) {
                $('#slcustomerbranch option').each(function(index, option){
                    if ($(option).val() == slcustomerbranch) {
                        $(option).prop('selected', 'selected');
                    }
                });
            }
            slitems = JSON.parse(localStorage.getItem('slitems'));
            if (slitems !== null) {
                $.each(slitems, function(id, arr){
                    wappsiPreciosUnidad(slitems[id].row.id,id,slitems[id].row.qty);
                });
                setTimeout(function() {
                    loadItems();
                }, 800);
            }
        } else {
            $('#slcustomerbranch').select2('val', '');
        }
        $('#slcustomerbranch').trigger('change');
    });
}

function validate_fe_customer_data()
{
    var is_fe = $('#document_type_id option:selected').data('fe');
    var customer_id = $('#slcustomer').val();

    if (is_fe) {
        $.ajax({
            url : site.base_url+"sales/validate_fe_customer_data/"+customer_id
        }).done(function(data){
            if (data != '') {
                localStorage.setItem('locked_for_customer_fe_data', 1);
                $('.txt-error').html(data).css('display', '');
                command: toastr.error(data, 'Mensaje de validación', { onHidden : function(){
                    //code...
                } });
                verify_locked_submit();
                $('#add_item').attr('disabled', true);
            } else if (data == ''){
                if (localStorage.getItem('locked_for_customer_fe_data')) {
                    localStorage.removeItem('locked_for_customer_fe_data');
                }
                $('.txt-error').html('').css('display', 'none');
                verify_locked_submit();
                $('#add_item').attr('disabled', false);
            }
            // $("#sldate").datetimepicker({
            //         format: site.dateFormats.js_ldate,
            //         fontAwesome: true,
            //         language: 'sma',
            //         weekStart: 1,
            //         todayBtn: 1,
            //         autoclose: 1,
            //         todayHighlight: 1,
            //         startView: 2,
            //         forceParse: 0
            //     }).datetimepicker('update', new Date());
            $('#sldate').prop('readonly', true);
        }).fail(function (data){
            console.log(data.responseText);
        });
    } else {
        localStorage.removeItem('locked_for_customer_fe_data');
        $('#add_item').attr('disabled', false);
        $('.txt-error').html('').css('display', 'none');
        $('#sldate').prop('readonly', false);
    }
}

function verify_locked_submit(){
    if (((is_sale_edit && $('#slsale_status').val() == 'completed') || !is_sale_edit) && (localStorage.getItem('locked_for_provider') || localStorage.getItem('locked_for_customer_fe_data') || localStorage.getItem('locked_for_biller_resolution') || localStorage.getItem('locked_for_items_quantity') || localStorage.getItem('lock_submit_by_min_sale_amount'))) {
        $('#add_sale').attr('disabled', true);
    } else {
        $('#add_sale').attr('disabled', false);
    }
}

function set_customer_payment(){
    if ( is_sale_edit == true  || (is_quote == true && get_customer_payment == false)) {
        return false;
    }
    var customer_id = $('#slcustomer').val();

    $('#slpayment_status').select2('val', 'pending').trigger('change');
    $('#slpayment_term').val('');

    localStorage.removeItem('sl_customer_payment');
    $.ajax({
        url : site.base_url+"customers/get_payment_type/"+customer_id,
        dataType : "JSON"
    }).done(function(data){
        setTimeout(function() {
            if (data.payment_type == 0) {
                localStorage.setItem('sl_customer_payment', JSON.stringify(data));
                $('#slpayment_status').select2('val', 'pending').trigger('change');
                $('#slpayment_term').val(data.payment_term > 0 ? data.payment_term : 1);
                $('#sale_payment_term').val(data.payment_term > 0 ? data.payment_term : 1);
                $('#customerpaymentterm').val(data.payment_term > 0 ? data.payment_term : 1);
                $('#paid_by_1').val('Credito').trigger('change');
                if (site.settings.control_customer_credit == 1) {
                    if (data.term_expired == 1) {
                        header_alert('error', 'Cliente con plazo de pago expirado en facturas, no se puede facturar');
                        localStorage.setItem('locked_for_items_quantity', 1);
                    } else {
                        localStorage.removeItem('locked_for_items_quantity');
                    }
                    verify_locked_submit();
                }
            } else if (data.payment_type == 1) {
                if ($('.paid_by option[value="cash"]:not([disabled])').length == 0) {
                    $('.paid_by').select2('val', $('.paid_by option:not("disabled")').eq(1).val()).trigger('change');
                } else {
                    $('#paid_by_1').select2('val', 'cash').trigger('change');
                }
                localStorage.removeItem('sl_customer_payment');
            }
        }, 850);
    });
}

function customer_special_discount(){

    var customer_id = $('#slcustomer').val();

    $.ajax({
        url : site.base_url+"customers/get_customer_special_discount/"+customer_id,
        dataType : "JSON"
    }).done(function(data){

        localStorage.setItem('slcustomerspecialdiscount', data.special_discount);
        if (data.special_discount == 1) {
                loadItems();
                command: toastr.warning('Cliente con descuento especial asignado', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        }
        localStorage.removeItem('slcustomerdepositamount');
        if (data.deposit_amount > 0) {
            localStorage.setItem('slcustomerdepositamount', parseFloat(data.deposit_amount));
        }
        localStorage.removeItem('slcustomergiftcardbalance');
        localStorage.removeItem('slcustomergiftcards');
        if (data.giftcard_balance > 0) {
            localStorage.setItem('slcustomergiftcardbalance', parseFloat(data.giftcard_balance));
            localStorage.setItem('slcustomergiftcards', JSON.stringify(data.gift_cards));
        }
        if (data.tax_exempt_customer == 1) {
            $('#slinnote').redactor('set', lang.tax_exempt_customer_text);
        }

    });

}

var its_customer_birthday = false;
var birthday_year_applied = false;
function customer_retentions(){
    if (is_sale_edit) {
        return false;
    }
    var customer_validate_min_base_retention = false;
    key_log = localStorage.getItem('slkeylog');
    $.ajax({
        url : site.base_url+"customers/get_customer_by_id/"+$('#slcustomer').val(),
        dataType : "JSON"
    }).done(function(data){
        $('#birthday_year_applied').val('');
        its_customer_birthday = data.apply_birthday_discount;
        if (its_customer_birthday) {
            birthday_year_applied = data.birthday_year_applied;
            header_alert('success', '¡Es cumpleaños del cliente!, se aplicarán descuentos configurados a productos');
        }
        if (data.customer_validate_min_base_retention == 0) {
            customer_validate_min_base_retention = true;
        }
        localStorage.setItem('customer_validate_min_base_retention', customer_validate_min_base_retention);
        if((data.default_rete_fuente_id || data.default_iva_fuente_id || data.default_ica_fuente_id || data.default_other_fuente_id) && key_log == 1) {
            var slretenciones = {
                    'gtotal' : $('#gtotal').text(),
                    'id_rete_fuente' : data.default_rete_fuente_id,
                    'id_rete_iva' : data.default_rete_iva_id,
                    'id_rete_ica' : data.default_rete_ica_id,
                    'id_rete_otros' : data.default_rete_other_id,
                };
            localStorage.setItem('slretenciones', JSON.stringify(slretenciones));
            recalcular_slretenciones();
        }
    });
}

$(document).on('click', '#keep_prices', function(){
    if (!is_sale_edit) {
        var pr_id = $('#quote_id').val() ? $('#quote_id').val() : ($('#sid').val() ? $('#sid').val() : 1);
    } else {
        var pr_id = sale_edit_id;
    }
    console.log("pr_id >> "+pr_id);
    localStorage.setItem('keep_prices', true);
    localStorage.setItem('keep_prices_quote_id', pr_id);
    location.reload();
});

$(document).on('click', '#update_prices', function(){
    var pr_id = $('#quote_id').val() ? $('#quote_id').val() : $('#sid').val();
    localStorage.setItem('keep_prices', false);
    localStorage.setItem('keep_prices_quote_id', pr_id);
    location.reload();
});

$(document).on('click', '#add_more_payments', function(){
    var pa = ($('.pay_option').length)+1;
    $('#pcc_type_1').select2('destroy');
    $('#paid_by_1').select2('destroy');
    var phtml = $('.pay_option_div').html(),
    update_html = phtml.replace(/_1/g, '_' + pa);
    update_html = update_html.replace(/"1"/g, '"' + pa+'"');
    pi = 'amount_' + pa;
    $('.payments_div').append(update_html);
    $('#pcc_type_1, #pcc_type_' + pa).select2({minimumResultsForSearch: 7});
    $('.row_payment').eq(pa-1).append('<span class="fa fa-times delete_payment_opt" style="position: absolute;right: 1.6%;font-size: 130%;color: #ed5767;"></span>');
    $('.paid_by').last().trigger('change');
    set_payment_method_billers();
    set_trm_input_payment();
    $("select.paid_by").select2({
        formatResult: payment_method_icons,
        formatSelection: payment_method_icons,
        escapeMarkup: function (m) {
          return m;
        }
    });
});

$(document).on('change', '.paid_by', function(){
    payment = $(this).closest('.well');
    paid_by = $(this).val();
    amount_to_pay = payment.find('.amount').val();
    customer = $('#slcustomer').val();
    if (paid_by == "deposit") {
        $.ajax({
            url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    } else {
        payment.find('.deposit_message').fadeOut();
    }
    check_customer_credit_limit();
});

$(document).on('change', '.amount', function(){
    check_customer_credit_limit();
});
function check_customer_credit_limit(){
    if (site.settings.control_customer_credit == 1) {
        if (sl_customer_payment = JSON.parse(localStorage.getItem('sl_customer_payment'))) {
            if (sl_customer_payment.payment_type == 0) {
                pb_total_credit_to_pay = 0;
                $.each($('select.paid_by'), function(index, pb_paid_by){
                    pb_payment = $(pb_paid_by).closest('.well');
                    pb_amount_to_pay = pb_payment.find('.amount').val();
                    if ($(pb_paid_by).val() != 'Credito') {
                        pb_total_credit_to_pay += parseFloat(pb_amount_to_pay);
                    }
                });
                pb_total_credit_amount = formatDecimal($('#totalS').text()) - pb_total_credit_to_pay;
                if (parseFloat(pb_total_credit_amount) > parseFloat(sl_customer_payment.credit_limit)) {
                    header_alert('error', 'El valor crédito ('+formatMoney(pb_total_credit_amount)+') supera el límite de crédito definido al cliente ('+formatMoney(sl_customer_payment.credit_limit)+')');
                    localStorage.setItem('locked_for_items_quantity', 1);
                    verify_locked_submit();
                } else {
                    localStorage.removeItem('locked_for_items_quantity');
                    verify_locked_submit();
                }
            }
        }
    } 
}

$(document).on('keyup', '.amount', function(){
    payment = $(this).closest('.well');
    paid_by = payment.find('select.paid_by');
    amount_to_pay = payment.find('.amount').val();
    customer = $('#slcustomer').val();
    if (paid_by.val() == "deposit") {
        $.ajax({
            url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    } else {
        payment.find('.deposit_message').fadeOut();
    }
});


$(document).on('keyup', '.trm_amount', function(){
    num = $(this).data('num');
    input_payment = $('input[name="amount-paid[]"][data-num="'+num+'"]');
    othercurrencytrm = parseFloat(localStorage.getItem('othercurrencytrm'));
    input_payment.val(formatDecimal($(this).val() * othercurrencytrm));
});

$(document).on('click', '.delete_payment_opt', function(){
    pay_opt = $(this).parents('.pay_option');
    pay_opt.remove();
});

$(document).on('change', '.sale_payment_term', function(){
    $('#sale_payment_term').val($(this).val());
});

function cargar_codigo_forma_pago_fe() {

    $('select.paid_by').each(function(index, select){
        index++;
        element_index = $('select.paid_by').index(select);
        element_codigo_fe_forma_pago = $('#paid_by_'+index+' option:selected').data('code_fe');
        $('.mean_payment_code_fe').eq(element_index).val(element_codigo_fe_forma_pago);
    });

}

///SERIAL MODAL


$(document).on('click', '.send_serial_modal', function(){
    validar_modal_serial();
});

$(document).on('click', '.continue_serial_modal', function(){
    validar_modal_serial(true);
});


function validar_modal_serial(cont = false){
    if ($('#serialModal_form').valid()) {
        serial = $('#serialModal_serial').val();
        meters = $('#serialModal_meters').val();
        item_id = $('#serialModal_product_id').val();
        option_id = $('#serialModal_option_id').val();
        item = JSON.parse(localStorage.getItem('modalSerial_item'));
        item.row.serialModal_serial = serial;
        item.row.qty = meters;
        item.row.quantity = $('#serialModal_meters').prop('max');
        item.row.base_quantity = $('#serialModal_meters').prop('max');
        item.row.delivered_qty = 0;
        item.row.pending_qty = meters;
        item.row.bill_qty = 0;
        item.row.option = option_id;
        add_invoice_item(item, true, cont);
    }
}

$(document).on('keyup', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal, e);
});

$(document).on('change', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal);
});

$(document).on('keyup', '#serialModal_meters', function(e){
    if (e.keyCode == 13) {
        $('.send_serial_modal').trigger('click');
    }
});

function validate_serial(serialModal, e = null){
    if ( e == null || e != null && e.keyCode == 13) {
        serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
        slwarehouse = $('#slwarehouse').val();
        $.ajax({
            url : site.base_url + 'products/get_product_serial_variant_quantity/'+serial_item.row.id+'/'+serialModal+'/'+slwarehouse,
            dataType : 'JSON',
        }).done(function(data){
            if (data.id  !== undefined) {
                $('#serialModal_meters').val(data.quantity);
                $('#serialModal_meters').prop('max', data.quantity);
                $('#serialModal_option_id').val(data.option_id);
                $('#serialModal_meters').focus();
            } else {
                $('#serialModal_serial').val('');
                $('#serialModal_option_id').val('');
                 command: toastr.warning('El serial indicado no está registrado para este producto', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        });
    }
}

$(document).on('click', '#random_num', function(e){
    $('#serialModal_serial').select();
});

function reset_rete_bomberil(){
    $('#rete_bomberil').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_autoaviso').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_bomberil_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_autoaviso_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_bomberil_tax').val('');
    $('#rete_bomberil_valor').val('');
    $('#rete_autoaviso_tax').val('');
    $('#rete_autoaviso_valor').val('');
}

function set_biller_warehouses_related(){
    warehouses_related = billers_data[$('#slbiller').val()].warehouses_related;
    if (warehouses_related) {
        $('#slwarehouse option').each(function(index, option){
            $(option).prop('disabled', false);
            if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                $(option).prop('disabled', true);
            }
        });
        setTimeout(function() {
            $('#slwarehouse').select2();
        }, 850);
    }
}


$(document).on('click', '.send_product_gift_card_data', function(){
    if ($('.product_gift_card_form').valid()) {
        var product_id_gift_card = $('#product_id_gift_card').val();
        slitems = JSON.parse(localStorage.getItem('slitems'));
        slitems[product_id_gift_card].gift_card = {
                            gc_card_no : $('#card_no').val(),
                            gc_value : $('#gc_value').val(),
                            gc_expiry : $('#gc_expiry').val(),
                        };
        slitems[product_id_gift_card].row.price = formatDecimal($('#gc_value').val());
        slitems[product_id_gift_card].row.unit_price = formatDecimal($('#gc_value').val());
        slitems[product_id_gift_card].row.real_unit_price = formatDecimal($('#gc_value').val());
        localStorage.setItem('slitems', JSON.stringify(slitems));
        loadItems();
        $('#gc_modal').modal('hide');
    }
});

$(document).on('click', '.sname', function(e) {
    var row = $(this).closest('tr');
    var slitems_id = $(row).data('item-id');
    var itemid = row.find('.rid').val();
    if (slitems[slitems_id].preferences) {
        if (slitems[slitems_id].preferences) {
            $('#myModal').modal({remote: site.base_url + 'pos/product_preferences_selection/' + itemid + "/" + slitems_id,
            backdrop: 'static',
            keyboard: false});
            $('#myModal').modal('show');
        }
    } else {
        if (site.settings.product_clic_action == 1) {
            $('#myModal').modal({remote: site.base_url + 'products/modal_view/' + itemid});
            $('#myModal').modal('show');
        } else if (site.settings.product_clic_action == 2) {
            if (slitems[slitems_id].options) {
                $('#myModal').modal({remote: site.base_url + 'pos/product_variants_selection/' + itemid + "/" + slitems_id + "/"+$('#slwarehouse').val()});
                $('#myModal').modal('show');
            }
        } else if (site.settings.product_clic_action == 3) {
            row.find('.edit').trigger('click');
        }
    }

});

function set_giftcard_payments(gtotal){
    gc_balance = formatDecimal(localStorage.getItem('slcustomergiftcardbalance'));
    gc_data = JSON.parse(localStorage.getItem('slcustomergiftcards'));
    var gtotal_balance = gtotal;
    var pnum = 0;
    $('#paid_by_1').select2('val', 'cash').trigger('change');
    $.each(gc_data, function(index, gc){
        console.log(" >> gtotal_balance "+gtotal_balance);
        console.log(" >> gc.balance "+gc.balance);
        console.log(" >> pnum "+(pnum+1));
        if (gtotal_balance > 0 && (gc.balance > gtotal_balance || gc.balance <= gtotal_balance)) {
            pnum++;
            if (pnum >= 2) {
                $('#add_more_payments').trigger('click');
            }
            $('#paid_by_'+pnum).select2('val', 'gift_card').trigger('change');
            $('#gift_card_no_'+pnum).val(gc.card_no).trigger('change');
            $('#amount_'+pnum).val((gc.balance > gtotal_balance) ? gtotal_balance : gc.balance).trigger('change');
            gtotal_balance -= gc.balance;
        }
        if ((index+1) == Object.keys(gc_data).length) {
            if (gtotal_balance > 0) {
                setTimeout(function() {
                    $('#add_more_payments').trigger('click');
                    pnum++;
                    $('#amount_'+pnum).val(gtotal_balance).trigger('change');
                    $('#paid_by_'+pnum).val('cash').trigger('change');
                }, 1000);
            }
        }
    });
    // // calculateTotals();
}


$(document).on('ifChecked', '#aiu_management', function(){
    $('.div_retention_modal').fadeOut();
    $('#rtModalLabel').fadeOut();
    $('#rtModalLabel2').fadeIn();
    $('.btn_aiu').fadeIn();
    $('.div_aiu').fadeIn();
    $('.retention_tr_div').fadeOut();
    $('.retention_aiu_div').fadeIn();
    $('.rete_option_apply').fadeIn();
    $('.rete_option').removeClass('col-sm-5').addClass('col-sm-3');
});
$(document).on('ifUnchecked', '#aiu_management', function(){
    $('.div_retention_modal').fadeIn();
    $('#rtModalLabel').fadeIn();
    $('#rtModalLabel2').fadeOut();
    $('.btn_aiu').fadeOut();
    $('.div_aiu').fadeOut();
    $('.retention_tr_div').fadeIn();
    $('.retention_aiu_div').fadeOut();
    $('.rete_option_apply').fadeOut();
    $('.rete_option').removeClass('col-sm-3').addClass('col-sm-5');
    localStorage.removeItem('aiu_management');
    $('#updateOrderRete').trigger('click');
});

function aplicar_aiu(){
    if (site.settings.aiu_management == 0) {
        return false;
    }
    var aiu_admin_percentage = formatDecimal($('#aiu_admin_percentage').val());
    var aiu_imprev_percentage = formatDecimal($('#aiu_imprev_percentage').val());
    var aiu_util_percentage = formatDecimal($('#aiu_util_percentage').val());
    var aiu_total_base = total;
    var aiu_tax_rate;
    $.each(tax_rates, function () {
        if(this.id == $('#aiu_tax').val()){
            aiu_tax_rate = this;
        }
    });
    var aiu_tax_base_apply_to = $('#aiu_tax_base_apply_to').val();
    var aiu_tax_rate_percentage = formatDecimal(aiu_tax_rate.rate);

    aiu_admin_total = formatDecimal(total * (aiu_admin_percentage / 100));
    aiu_imprev_total = formatDecimal(total * (aiu_imprev_percentage / 100));
    aiu_util_total = formatDecimal(total * (aiu_util_percentage / 100));

    $('#aiu_admin_total').val(formatDecimals(aiu_admin_total));
    $('#aiu_imprev_total').val(formatDecimals(aiu_imprev_total));
    $('#aiu_util_total').val(formatDecimals(aiu_util_total));

    $('.total_aiu_calculated').text(formatMoney(aiu_admin_total + aiu_imprev_total + aiu_util_total));

    aiu_tax_base_calculated = total;
    if (aiu_tax_base_apply_to == 1) {
        aiu_tax_total = formatDecimal(total * (aiu_tax_rate_percentage / 100));
    } else if (aiu_tax_base_apply_to == 2) {
        aiu_tax_base_calculated = formatDecimal(aiu_admin_total + aiu_imprev_total + aiu_util_total);
        aiu_tax_total = formatDecimal(aiu_tax_base_calculated * (aiu_tax_rate_percentage / 100));
    } else if (aiu_tax_base_apply_to == 3) {
        aiu_tax_base_calculated = formatDecimal(aiu_util_total);
        aiu_tax_total = formatDecimal(aiu_tax_base_calculated * (aiu_tax_rate_percentage / 100));
    }
    $('#aiu_tax_base').val(aiu_tax_base_calculated);
    $('#aiu_tax_total').val(formatDecimals(aiu_tax_total));
    $('.option_aiu_base').text("Base AIU ("+formatDecimals(aiu_admin_percentage+aiu_imprev_percentage+aiu_util_percentage)+")");
    $('.option_utilidad_base').text("Utilidad ("+formatDecimals(aiu_util_percentage)+")");
}

$(document).on('change', '#aiu_tax, #aiu_tax_base_apply_to, #aiu_admin_percentage, #aiu_imprev_percentage, #aiu_util_percentage', function(){
    aplicar_aiu();
});

$(document).on('change', '#upload_xls', function(){
    input = $(this);
    if (input.is(':checked')) {
        $('.xls_file_div').fadeIn();
        $('#slsale_status').select2('val', 'pending').select2('readonly', true).trigger('change');
        header_alert('warning', lang.sale_uploading_xls);
        header_alert('info', lang.sale_uploading_xls_verify_products);
    } else {
        $('.xls_file_div').fadeOut();
        $('#slsale_status').select2('val', 'completed').select2('readonly', false).trigger('change');
    }
});


function updateQuantityItems(warehouseId)
{
    let items = localStorage.getItem('slitems')
    if (items) {
        items = JSON.parse(items)
        $.each(items, function () {
            productId = this.item_id
            options = this.options
            if (options) {
                $.each(options, function() {
                    this.quantity = getQuantitiesProductInWarehouse(warehouseId, productId, this.id)
                })
            } else {
                this.row.quantity = getQuantitiesProductInWarehouse(warehouseId, productId)
            }
        })
    }
    localStorage.setItem('slitems', JSON.stringify(items));
    loadItems()
}

function getQuantitiesProductInWarehouse(warehouseId, productId, optionId = null)
{
    let quantity = 0
    $.ajax({
        type: "get",
        async: false,
        url: site.base_url + 'products/getQuantitiesProductInWarehouseAjax',
        data: {
            'warehouseId' : warehouseId,
            'productId' : productId,
            'optionId' : optionId,
        },
        dataType: "json",
        success: function (respuesta) {
            quantity = respuesta.quantity;
        }
    });
    return quantity;
}