<?php defined('BASEPATH') OR exit('No direct script access allowed');

class warranty extends MY_Controller
{
    public $status = [];
    public $siteModel = '';
    public $salesModel = '';
    public $productsModel = '';
    public $warrantyModel = '';
    public $transfersModel = '';
    public $companiesModel = '';
    public $documentTypeModel = '';

    /*
    1: Ajuste Entrada;
    2: Ajuste Salida;
    3: Nota Crédito;
    4: Tranferencia producto a Bodega de Garantía;
    5: transferencia producto a Bodega Proveedor;
    */

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        $this->load->admin_model("Warranty_model");
        $this->load->admin_model("Products_model");
        $this->load->admin_model("Companies_model");
        $this->load->admin_model("transfers_model");
        $this->load->admin_model("DocumentsTypes_model");

        $this->siteModel = new Site();
        $this->salesModel = new Sales_model();
        $this->warrantyModel = new Warranty_model();
        $this->productsModel = new Products_model();
        $this->companiesModel = new Companies_model();
        $this->transfersModel = new Transfers_model();
        $this->documentTypeModel = new DocumentsTypes_model();

        $this->lang->admin_load('warranty', $this->Settings->user_language);

        $this->load->library('form_validation');

        $this->status = [
            1 => (object) ["color" => "warning", "name" => "Pendiente"],
            2 => (object) ["color" => "danger", "name" => "Rechazada"],
            3 => (object) ["color" => "info", "name" => "Revisión Proveedor"],
            4 => (object) ["color" => "danger", "name" => "Rechazada por Proveedor"],
            5 => (object) ["color" => "primary", "name" => "Aceptada por proveedor NC"],
            6 => (object) ["color" => "primary", "name" => "Aceptada Proveedor Cambio"],
            7 => (object) ["color" => "primary", "name" => "Aceptada"],
            8 => (object) ["color" => "success", "name" => "Completada"],
        ];
    }

    public function index()
    {
        $this->page_construct('sales/warranty/index', ["page_title" => $this->lang->line("warranty")], $this->data);
    }

    public function getDataTables()
    {
        $this->load->library('datatables');

        $this->datatables->select("
            product_warranty.id AS id,
            date,
            reference_no,
            companies.name AS customerName,
            products.name AS productName,
            product_warranty.status,
            wa.description");
        $this->datatables->from("product_warranty");
        $this->datatables->join("companies", "companies.id = product_warranty.customer_id");
        $this->datatables->join("products", "products.id = product_warranty.product_id");
        $this->datatables->join("warranty_activities AS wa", "wa.id = product_warranty.warranty_activity_id");

        $detail = anchor('admin/warranty/view/$1', '<i class="fas fa-eye"></i> ' . lang('detail'), 'data-toggle="modal" data-target="#myModal"');
        $addActivity = anchor('admin/warranty/addActivity/$1', '<i class="fa fa-plus"></i> ' . lang('add_activity'), 'id="addActivityLink" data-toggle="modal" data-target="#myModal"');
        $actions = '<div class="btn-group text-left">
            <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                <i class="fas fa-ellipsis-v fa-lg"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>' . $detail . '</li>
                <li>' . $addActivity . '</li>
            </ul>
        </div>';
        $this->datatables->add_column("actions", $actions, "id");

        echo $this->datatables->generate();
    }

    public function add()
    {
        $this->data['customerQualifications'] = $this->warrantyModel->getAllCustomerQualifications();
        $this->data['customerAttitude'] = $this->warrantyModel->getAllCustomerAttitude();
        $this->data['warranty_reasons'] = $this->warrantyModel->findWarrantyReason();
        $this->data['billers'] = $this->siteModel->getBillers('biller');
        $this->data['documentsTypes'] = [];

        $this->page_construct('sales/warranty/add', ["page_title" => $this->lang->line("add_warranty")], $this->data);
    }

    public function create()
    {
        $this->form_validation->set_rules('date', lang("date"), 'trim|required');
        $this->form_validation->set_rules('billerId', lang("biller"), 'trim|required');
        $this->form_validation->set_rules('document_type', lang("document_type"), 'trim|required');
        $this->form_validation->set_rules('customer_id', lang("customer"), 'trim');
        $this->form_validation->set_rules('customer_branch', lang("customer_branch"), 'trim');
        $this->form_validation->set_rules('seller_id', lang("seller"), 'trim|required');
        $this->form_validation->set_rules('seller_id', lang("seller"), 'trim|required');
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'trim');
        $this->form_validation->set_rules('sale_item_id', lang("product"), 'trim|required');
        $this->form_validation->set_rules('warranty_reason', lang("warranty_reason"), 'trim|required');
        $this->form_validation->set_rules('contact_means', lang("contact_means"), 'trim|required');
        $this->form_validation->set_rules('product_status', lang("product_status"), 'trim|required');
        $this->form_validation->set_rules('customer_note', lang("customer_note"), 'trim');
        $this->form_validation->set_rules('internal_note', lang("internal_note"), 'trim');
        $this->form_validation->set_rules('internal_note', lang("internal_note"), 'trim');

        if ($this->input->post('has_it_been_intervened')) {
            $this->form_validation->set_rules('description', lang("description"), 'trim|required');
        }

        if ($this->form_validation->run()) {
            $this->validateDocumentTypeConfiguration();

            $documentTypeId = $this->input->post('document_type');
            $referenceNo = $this->documentTypeModel->getReference($documentTypeId);
            $data = [
                "date"                      => date("Y-m-d H:i", strtotime(str_replace('/','-', $this->input->post('date')))),
                "document_type_id"          => $documentTypeId,
                "status"                    => 1,
                "warranty_activity_id"      => 1,
                "biller_id"                 => $this->input->post("billerId"),
                "sale_id"                   => $this->input->post("sale_id"),
                "customer_id"               => $this->input->post("customer_id"),
                "registration_date"         => date("Y-m-d h:i"),
                "sale_item_id"              => $this->input->post("sale_item_id"),
                "product_id"                => $this->input->post("product_id"),
                "quantity"                  => 1,
                "created_by"                => $this->session->userdata('user_id'),
                "warranty_reason"           => $this->input->post('warranty_reason'),
                "warranty_details"          => $this->input->post('warranty_details'),
                "product_status"            => $this->input->post('product_status'),
                "customer_note"             => $this->input->post('customer_note'),
                "has_it_been_intervened?"   => $this->input->post('has_it_been_intervened') ? 1 : 0,
                "description"               => $this->input->post('description'),
                "contact_means"             => $this->input->post('contact_means'),
                "internal_note"             => $this->input->post('internal_note'),
                "customer_since"            => $this->input->post('customer_since'),
                "customer_qualification"    => $this->input->post('customer_qualification'),
                "number_credits"            => $this->input->post('number_credits'),
                "customer_attitude"         => $this->input->post('customer_attitude'),
                "maximum_purchase"          => $this->input->post('maximum_purchase'),
                "reference_no"              => $referenceNo->referenceNo,
            ];

            $warrantyId = $this->warrantyModel->create($data);
            if ($warrantyId != false) {
                $this->documentTypeModel->updateConsecutiveDocumentType([
                    "sales_consecutive" => ($referenceNo->consecutive + 1)
                ], $documentTypeId);

                $this->triggerCollateralActivityAction($warrantyId, 1);

                $this->session->set_flashdata("message", lang("warranty_saved"));
            } else {
                $this->session->set_flashdata("error", lang("warranty_not_saved"));
            }
        }

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        admin_redirect("warranty");
    }

    private function validateDocumentTypeConfiguration()
    {
        $adjustmentDocument = $this->documentTypeModel->getAdjustmentDocumentType();
        $trasferDocument = $this->documentTypeModel->getTrasferDocumentType();

        if (empty($adjustmentDocument) && empty($trasferDocument)) {
            $this->session->set_flashdata("error", lang("adjustment_documents_not_exist"));
            admin_redirect("warranty");
        } else if (empty($this->Settings->adjustments_document) && empty($this->Settings->transfer_document)) {
            $this->session->set_flashdata("error", lang("adjustment_documents_not_configured"));
            admin_redirect("warranty");
        }
    }

    private function triggerCollateralActivityAction($warrantyId, $warrantyAction)
    {
        /*
            1: Ajuste Entrada;
            2: Ajuste Salida;
            3: Nota Crédito;
            4: Tranferencia producto a Bodega de Garantía;
            5: transferencia producto a Bodega Proveedor;
        */
        if ($warrantyAction == 1) {
            $this->transferTowarehouse($warrantyId, "addition");
        } else if ($warrantyAction == 2) {
            $this->transferTowarehouse($warrantyId, "subtraction");
        } else if ($warrantyAction == 3) {
            //TODO
        } else if ($warrantyAction == 4) {
            $fromWarehouse  = $this->siteModel->findWarehouse(["id" => $this->Settings->warranty_supplier_warehouse]);
            $toWarehouse  = $this->siteModel->findWarehouse(["id" => $this->Settings->warranty_warehouse]);
            $this->transferFromWarehouseToWarehouse($warrantyId, $fromWarehouse, $toWarehouse);
        } else if ($warrantyAction == 5) {
            $fromWarehouse  = $this->siteModel->findWarehouse(["id" => $this->Settings->warranty_warehouse]);
            $toWarehouse  = $this->siteModel->findWarehouse(["id" => $this->Settings->warranty_supplier_warehouse]);
            $this->transferFromWarehouseToWarehouse($warrantyId, $fromWarehouse, $toWarehouse);
        }
    }

    private function transferTowarehouse($warrantyId, $typeTransfer)
    {
        $documentType       = $this->documentTypeModel->getDocumentType(["id" => $this->Settings->adjustments_document]);
        $warrantyWarehouse  = $this->siteModel->findWarehouse(["id" => $this->Settings->warranty_warehouse]);
        $date               = date("Y-m-d H:i", strtotime(str_replace('/','-', $this->input->post('date'))));
        $warranty           = $this->warrantyModel->findWarranty(["product_warranty.id" => $warrantyId]);
        $customer           = $this->companiesModel->getCompanyByID($warranty->customer_id);
        $product            = $this->productsModel->getProductByID($warranty->product_id);
        $item               = $this->salesModel->getItemByID($warranty->sale_item_id);

        $productsNames[$warranty->product_id] = $product->name;

        $products[] = [
            'product_id'        => $warranty->product_id,
            'type'              => $typeTransfer,
            'quantity'          => 1,
            'warehouse_id'      => $warrantyWarehouse->id,
            'option_id'         => $item->option_id,
            'serial_no'         => $item->serial_no,
            'avg_cost'          => $product->avg_cost,
            'adjustment_cost'   => $product->cost,
        ];

        $data = [
            'date'              => $this->site->movement_setted_datetime($date),
            'warehouse_id'      => $warrantyWarehouse->id,
            'note'              => ($typeTransfer == "addition") ? sprintf(lang("note_addition_warranty"), $warrantyId, $customer->name) : sprintf(lang("note_subtraction_warranty"), $warrantyId, $customer->name),
            'created_by'        => $this->session->userdata('user_id'),
            'count_id'          => null,
            'companies_id'      => $warranty->customer_id,
            'biller_id'         => $warranty->biller_id,
            'document_type_id'  => $documentType->id,
        ];

        if ($this->productsModel->addAdjustment($data, $products, $productsNames)) {
            return true;
        }
        return false;
    }

    private function transferFromWarehouseToWarehouse($warrantyId, $fromWarehouse, $toWarehouse)
    {
        $documentType       = $this->documentTypeModel->getDocumentType(["id" => $this->Settings->transfer_document]);
        $warranty = $this->warrantyModel->findWarranty(["product_warranty.id"=>$warrantyId]);
        $item     = $this->salesModel->getItemByID($warranty->sale_item_id);
        $date               = date("Y-m-d H:i", strtotime(str_replace('/','-', $this->input->post('date'))));

        $products[] = [
            'product_id'        => $warranty->product_id,
            'product_code'      => $warranty->productCode,
            'product_name'      => $warranty->productName,
            'option_id'         => $item->option_id,
            'quantity'          => 1,
            'product_unit_id'   => $item->product_unit_id,
            'product_unit_code' => $item->product_unit_code,
            'unit_quantity'     => $item->unit_quantity,
            'quantity_balance'  => 1,
            'warehouse_id'      => $toWarehouse->id,
            'date'              => date('Y-m-d', strtotime($date)),
            'serial_no'         => $item->serial_no,
        ];

        $data = [
            'date'                  => $this->site->movement_setted_datetime($date),
            'from_warehouse_id'     => $fromWarehouse->id,
            'from_warehouse_code'   => $fromWarehouse->code,
            'from_warehouse_name'   => $fromWarehouse->name,
            'to_warehouse_id'       => $toWarehouse->id,
            'to_warehouse_code'     => $toWarehouse->code,
            'to_warehouse_name'     => $toWarehouse->name,
            'note'                  => lang("note_addition_warranty"),
            'created_by'            => $this->session->userdata('user_id'),
            'biller_id'             => $warranty->biller_id,
            'destination_biller_id' => $warranty->biller_id,
            'document_type_id'      => $documentType->id,
            'companies_id'          => $warranty->customer_id,
        ];

        if ($this->transfersModel->addTransfer($data, $products)) {
            return true;
        }

        return false;
    }

    public function getProduct()
    {
        $data = [
            "term"          => $this->input->get("term"),
            "customer_id"   => $this->input->get("customer_id"),
            "reference_no"  => $this->input->get("reference_no"),
        ];
        $result = $this->warrantyModel->findProducts($data);
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function getInvoiceSuggestions()
    {
        $data = [
            "term"          => $this->input->get("term"),
            "customer_id"   => $this->input->get("customer_id"),
        ];
        $result = $this->warrantyModel->findInvoices($data);
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function getProductSuggestions()
    {
        $data = [
            "term"          => $this->input->get("term"),
            "customer_id"   => $this->input->get("customer_id"),
            "reference_no"  => $this->input->get("reference_no"),
        ];
        $result = $this->warrantyModel->findProducts($data);
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function getItem()
    {
        $itemId = $this->input->post('itemId');
        $item = $this->salesModel->getItemByID($itemId);
        echo json_encode($item);
    }

    public function validate()
    {
        $messages = [];
        $productId = $this->input->post("productId");
        $date = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date'))));
        $product = $this->warrantyModel->getDateSale($productId);

        $continueProcess = true;
        $startDate = new DateTime($date);
        $finalDate = new DateTime($product->date);
        $diff = $startDate->diff($finalDate);
        $diffDays = $diff->days;
        $warrantyTime = intval($product->warranty_time);

        if (!empty($this->Settings->block_warranty_by_warranty_days)) {
            $continueProcess = false;
            $type = "danger";
        } else {
            $type = "warning";
        }

        if (empty($warrantyTime)) {
            $message = "El producto seleccionado no tiene asignado tiempo de garantía";
        } else {
            $timeExceeded = $diffDays - $warrantyTime;
            $message = "El tiempo para realizar la garantía ha sido excedida. Tiempo de Garantía del producto: <b>$warrantyTime</b> días. Tiempo excedido: <b>$timeExceeded</b> días";
        }

        $messages[] = [
            "continue"  => $continueProcess,
            "type"      => $type,
            "message"   => $message,
        ];

        echo json_encode($messages);
    }

    public function findCustomerAjax()
    {
        $customerId = $this->input->post("customer_id");
        $customer = $this->companiesModel->getCompanyByID($customerId);
        echo json_encode($customer);
    }

    public function addActivity($warrantyId)
    {
        $warranty = $this->warrantyModel->findWarranty(["product_warranty.id"=>$warrantyId]);
        $this->data['warranty']   = $warranty;
        $this->data['activities'] = $this->warrantyModel->getWarrantyActivities(["depends_on"=>$warranty->status]);
        $this->data['modal_js']   = $this->siteModel->modal_js();

        $this->load_view($this->theme . 'sales/warranty/add_activity', $this->data);
    }

    public function createActivity($warrantyId)
    {
        $this->form_validation->set_rules('date', lang("date"), 'trim|required');
        $this->form_validation->set_rules('warranty_activity_id', lang("warranty_activity"), 'trim|required');
        $this->form_validation->set_rules('description', lang("description"), 'trim|required');

        if ($this->form_validation->run()) {
            $data = [
                "date"                  => date("Y-m-d H:i", strtotime(str_replace('/','-', $this->input->post('date')))),
                "warranty_id"           => $warrantyId,
                "warranty_activity_id"  => $this->input->post("warranty_activity_id"),
                "description"           => $this->input->post("description"),
                "status"                => $this->input->post("warranty_activity_status"),
                "created_by"            => $this->session->userdata('user_id'),
                "registration_date"     => date("Y-m-d H:i"),
            ];
            if ($this->warrantyModel->createActivity($data)) {
                $this->warrantyModel->update(["status"=>$data["status"]], $warrantyId);
                $this->triggerCollateralActivityAction($warrantyId, $this->input->post("warranty_activity_action"));

                $this->session->set_flashdata("message", lang("saved_warranty_activity"));
            } else {
                $this->session->set_flashdata("error", lang("unsaved_warranty_activity"));
            }
        }

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        admin_redirect("warranty");
    }

    public function view($warrantyId)
    {
        $warranty = $this->warrantyModel->findWarranty(["product_warranty.id"=>$warrantyId]);
        $this->data['warranty']   = $warranty;
        $this->data['status']     = $this->status;
        $this->data['activities'] = $this->warrantyModel->findActivities(["warranty_id" => $warrantyId]);

        $this->data['modal_js']   = $this->siteModel->modal_js();
        $this->load_view($this->theme . 'sales/warranty/view', $this->data);
    }
}