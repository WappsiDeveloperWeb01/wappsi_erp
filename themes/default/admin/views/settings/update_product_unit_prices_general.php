<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?= admin_form_open('system_settings/get_products_units_xls', 'id="action-form"') ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php if ($this->Admin || $this->Owner || $this->GP['system_settings-update_products_group_prices']): ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive"></div>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?php if ($this->Admin || $this->Owner || $this->GP['system_settings-update_products_group_prices']): ?>
    <script type="text/javascript">

        $(document).ready(function(){
            $("#loading").fadeIn("slow");
            get_products_units();
        });
        var table;
        function get_products_units() {
          $.ajax({
            url: site.base_url+'system_settings/get_products_units_html',
            success: function(data) {
                $('.table-responsive').html(data);
                table = $('#units_data').DataTable({
                    "processing": false,
                    pageLength: 100,
                    responsive: true,
                    autoWidth: false,
                    oLanguage: <?php echo $dt_lang; ?>,
                    dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
                    fnDrawCallback: function (oSettings) {
                        $('.actionsButtonContainer').html('<div class="pull-right dropdown">'+
                            '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>">'+
                                '<i class="fas fa-ellipsis-v fa-lg"></i>'+
                            '</button>'+
                            '<?php if ($this->Admin || $this->Owner || $GP['system_settings-get_products_units_xls']): ?>'+
                                    '<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">'+
                                        '<li>'+
                                            '<a href="#" id="excel" data-action="export_excel">'+
                                                '<i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>'+
                                            '</a>'+
                                        '</li>'+
                                    '</ul>'+
                                '<?php endif ?>'+
                        '</div>');
                    }
                });
                $("#loading").fadeOut("slow");
            },
            error: function(xhr, status, error) {
              console.log('Error al obtener la tabla HTML: ' + error);
            }
          });
        }
        var prev_unit_price = 0;
        $(document).on('focus', '.unit_price', function(){
            prev_unit_price = $(this).val();
        });
        $(document).on('change', '.unit_price', function(){
            idunit = $(this).data('idunit');
            idproduct = $(this).data('idproduct');
            unitbase = $(this).data('unitbase');
            cost = $(this).data('cost');
            oldprice = $(this).data('oldprice');
            pgid = $(this).data('pgid');
            price = $(this).val();
            input = $(this);
            margin = formatDecimals((((price - cost) / price) * 100), 2);
            if (site.settings.disable_product_price_under_cost == 1 && parseFloat(price) < parseFloat(cost)) {
                command: toastr.error('El precio no puede estar por debajo del costo', '¡Error!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                $(this).val(prev_unit_price);
            } else {
                prev_unit_price = $(this).val();
            }
            $.ajax({
                url : site.base_url+"system_settings/update_product_unit_price_general",
                method : "POST",
                dataType : "JSON",
                data : {
                    "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
                    product_id : idproduct,
                    unit_id : idunit,
                    price : price,
                    unit_base : unitbase,
                    price_group_id : pgid,
                    old_price : oldprice,
                }
            }).done(function(data){
                if (data) {
                    input.data('oldprice', price);
                    input.parent('td').find('.text_margin').html('('+formatDecimals(margin)+' %)');
                     command: toastr.success('El precio se actualizó correctamente.', '¡Actualizado!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                } else {
                     command: toastr.error('Hubo un error al actualizar.', '¡Error al actualizar!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                }
            });
        });
    </script>
<?php endif ?>
<?= form_close() ?>
