<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $payroll_status = isset($payroll) && !empty($payroll) ? $payroll->status : NULL; ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="contracts_table" class="table table-hover" style="width: 100%">
                                    <thead>
                                        <tr class="primary">
                                            <th></th>
                                            <th><?= lang("creation_date"); ?></th>
                                            <th><?= lang("month"); ?></th>
                                            <th><?= lang("payroll_management_number_employees"); ?></th>
                                            <th><?= lang("payroll_management_earnings"); ?></th>
                                            <th><?= lang("payroll_management_deductions"); ?></th>
                                            <th><?= lang("payroll_management_total_payment"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody class="pointer">
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        load_datatable();
        $(document).on('click', '.see_electronic_payroll_employees td:not(:last-child)', function() {
            var electronic_payroll_employee_id = $(this).parent('.see_electronic_payroll_employees').attr('id');
            var month = $(this).parent('.see_electronic_payroll_employees').data('month');

            window.location.href = site.base_url + 'payroll_electronic/see/'+ electronic_payroll_employee_id +'/'+ month;
        });

        $(document).on('click', '.delete_electronic_payroll a', function(e) { confirm_delete_electronic_payroll(e, $(this)); });
        $(document).on('click', '#automatic_enable', function (e) { automatic_enable(e, $(this)); });
    });

    function load_datatable()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#contracts_table').dataTable({
            "aaSorting": [[0, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('payroll_electronic/get_datatables') ?>',
            dom: '<"col-sm-7 containerBtn"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.dataset.month = aData[2];
                nRow.className = "see_electronic_payroll_employees";
                return nRow;
            },
            "aoColumns": [
                { visible: false},
                { className: 'text-center' },
                {
                    className: 'text-left',
                    render: function(data) {
                        _months = { 1: "Enero", 2: "Febrero", 3: "Marzo", 4: "Abril", 5: "Mayo", 6: "Junio", 7: "Julio", 8: "Agosto", 9: "Septiembre", 10: "Octubre", 11: "Noviembre", 12: "Diciembre"};
                        return _months[data];
                    }
                },
                { className: 'text-center' },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        return (data == <?= NOT_SENT ?>) ? '<span class="label label-danger"><?= lang("not_sent") ?></span>' : (data == <?= PENDING ?>) ? '<span class="label label-warning"><?= lang("pending") ?></span>' : '<span class="label label-success"><?= lang("accepted") ?></span>';
                    }
                },
                {
                    bSortable: false,
                    render: function(data, type, row) {
                        var actions = $(data);

                        <?php if ($this->Owner || $this->Admin) { ?>
                            if (row[7] == <?= PENDING ?>) { actions.find('.dropdown-menu').find('.delete_electronic_payroll').remove(); }
                        <?php } ?>

                        var data = actions.prop('outerHTML');
                        return data;
                    }
                }
            ]
        });
    }

    function confirm_delete_electronic_payroll(event, element)
    {
        event.preventDefault();
        swal({
            title: '¡Advertencia!',
            text: 'Este proceso eliminará los datos de la nómina electrónica y desaprobará la última nómina generada.',
            type: 'warning',
            confirmButtonText: "¡Entendido!",
            closeOnConfirm: false,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Cancelar",
            closeOnClickOutside: false,
        }, function () {
            swal({
                title: 'Eliminando Nóminas electrónicas',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });
            window.location.href = element.attr('href');
        });
    }

    function automatic_enable(event, element)
    {
        event.preventDefault();
        swal({
            title: 'Habilitación automática',
            text: 'Este proceso activará al empleador al entorno de producción ante la DIAN',
            type: 'warning',
            confirmButtonText: "¡Entendido!",
            closeOnConfirm: false,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Cancelar",
            closeOnClickOutside: false,
        }, function () {
            swal({
                title: 'Habilitación automática en proceso',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });
            window.location.href = element.attr('href');
        });

    }
</script>