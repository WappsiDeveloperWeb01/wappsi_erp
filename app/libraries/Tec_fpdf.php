<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 *  ==============================================================================
 *  Author  : Mian Saleem
 *  Email   : saleem@tecdiary.com
 *  For     : DOMPDF
 *  Web     : https://github.com/dompdf/dompdf
 *  License : LGPL-2.1
 *      : https://github.com/dompdf/dompdf/blob/master/LICENSE.LGPL
 *  ==============================================================================
 */

use FPDF\FPDF;

class Tec_fpdf extends FPDF
{
    public function __construct() {
        parent::__construct();
    }

    public function generate($content, $name = 'download.pdf', $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, $orientation = 'P') {
        $this->AddPage();
        $this->SetFont('Arial','B',16);
        $this->Output();
    }

}
