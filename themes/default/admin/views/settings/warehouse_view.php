<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    p {
        word-break: break-all;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= $warehouse->name ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("code"); ?></h4>
                    <p><?= $warehouse->code; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("price_group"); ?></h4>
                    <p><?= $warehouse->price_group_id; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("phone"); ?></h4>
                    <p><?= $warehouse->phone; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("email"); ?></h4>
                    <p><?= $warehouse->email; ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("address"); ?></h4>
                    <p><?= $warehouse->address; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("status"); ?></h4>
                    <p><?= $warehouse->status == 1 ? lang('active') : lang('inactive'); ?></p>
                </div>
            </div>

            <div class="modal-footer no-print">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= lang('close'); ?></button>
                <?php if ($Owner || $Admin || $GP['customers-edit']) { ?>
                    <a href="<?=admin_url('system_settings/edit_warehouse/'.$warehouse->id);?>" data-toggle="modal" data-target="#myModal2" class="btn btn-primary"><i class="fa fa-edit"></i> <?= lang('edit_warehouse'); ?></a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>