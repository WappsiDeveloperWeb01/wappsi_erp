<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";
if ($this->input->post('product')) {
    $v .= "&product=" . $this->input->post('product');
}
if ($this->input->post('reference_no')) {
    $v .= "&reference_no=" . $this->input->post('reference_no');
}
if ($this->input->post('biller')) {
    $v .= "&biller=" . $this->input->post('biller');
}
if ($this->input->post('user')) {
    $v .= "&user=" . $this->input->post('user');
}
if ($this->input->post('serial')) {
    $v .= "&serial=" . $this->input->post('serial');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <!-- <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div> -->
                <div class="ibox-content">
                    <?= admin_form_open("reports/order_sales", 'id="form_filter" target="_blank"'); ?>
                        <input type="hidden" name="generate_report" value="1">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= lang('reference_no', 'reference_no') ?>
                                    <input type="text" name="reference_no" id="reference_no" class="form-control" placeholder="Número de orden">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                    <?php
                                    $bl[""] = lang('select');
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ""), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . '"');
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                    <?php
                                    $wh[""] = lang('select');
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . '"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= lang('customer', 'customer') ?>
                                    <?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="customer" data-placeholder="' . lang("select") . '" class="form-control input-tip" style="width:100%;"'); ?>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="seller"><?= lang("seller"); ?></label>
                                    <?php
                                    $sl[""] = lang('select');
                                    foreach ($sellers as $seller) {
                                        $sl[$seller->id] = $seller->name;
                                    }
                                    echo form_dropdown('seller', $sl, (isset($_POST['seller']) ? $_POST['seller'] : ""), 'class="form-control" id="seller" data-placeholder="' . $this->lang->line("select") . '"');
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="status"><?= lang("status"); ?></label>
                                    <?php
                                    $st[""] = lang('select');
                                    $st['pending'] = lang('pending');
                                    $st['partial'] = lang('partial');
                                    $st['completed'] = lang('completed');
                                    echo form_dropdown('status', $st, (isset($_POST['status']) ? $_POST['status'] : ""), 'class="form-control" id="status" data-placeholder="' . $this->lang->line("select") . '"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= lang("country", "country"); ?>
                                    <?php $countryId = (isset($_POST['country'])) ? $_POST['country']: '' ?>
                                    <select class="form-control select" name="country" id="country">
                                        <option value=""><?= $this->lang->line("alls") ?></option>
                                        <?php if (!empty($countries)) : ?>
                                            <?php foreach ($countries as $country): ?>
                                                <option value="<?= $country->NOMBRE ?>" code="<?= $country->CODIGO ?>" <?= ($country->NOMBRE == $countryId) ? 'selected' : '' ?>><?= $country->NOMBRE ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                    <input type="hidden" name="countryCode" id="countryCode" value="<?= $_POST['countryCode'] ?? '' ?>">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="state"><?= $this->lang->line("state") ?></label>
                                    <?php $stateId = (isset($_POST['state'])) ? $_POST['state']: '' ?>
                                    <select class="form-control" name="state" id="state">
                                        <option value=""><?= $this->lang->line("alls") ?></option>
                                        <?php if (!empty($states)) : ?>
                                            <?php foreach ($states as $state): ?>
                                                <option value="<?= $state->DEPARTAMENTO ?>" data-code="<?= $state->CODDEPARTAMENTO ?>" <?= ($state->DEPARTAMENTO == $stateId) ? 'selected' : '' ?>><?= $state->DEPARTAMENTO ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                    <input type="hidden" name="stateCode" id="stateCode" value="<?= $_POST['stateCode'] ?? '' ?>">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="city"><?= $this->lang->line("city"); ?></label>
                                    <?php $cityId = (isset($_POST['city'])) ? $_POST['city']: '' ?>
                                    <select class="form-control" name="city" id="city">
                                        <option value=""><?= $this->lang->line("allsf") ?></option>
                                        <?php if (!empty($cities)) : ?>
                                            <?php foreach ($cities as $city): ?>
                                                <option value="<?= $city->DESCRIPCION ?>" data-code="<?= $city->CODIGO ?>" <?= ($city->DESCRIPCION == $cityId) ? 'selected' : '' ?>><?= $city->DESCRIPCION ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                    <input type="hidden" name="cityCode" id="cityCode" value="<?= $_POST['cityCode'] ?? '' ?>">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="zone"><?= $this->lang->line("zone"); ?></label>
                                    <?php $zoneId = (isset($_POST['zone'])) ? $_POST['zone']: '' ?>
                                    <select class="form-control select" name="zone" id="zone">
                                        <option value=""><?= $this->lang->line("allsf") ?></option>
                                        <?php if (!empty($zones)) : ?>
                                            <?php foreach ($zones as $zone): ?>
                                                <option value="<?= $zone->id ?>" data-code="<?= $zone->zone_code ?>" <?= ($zone->id == $zoneId) ? 'selected' : '' ?>><?= $zone->zone_name ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                    <input type="hidden" name="zoneCode" id="zoneCode" value="<?= $_POST['zoneCode'] ?? '' ?>">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="subzone"><?= $this->lang->line("subzone"); ?></label>
                                    <?php $subzoneId = (isset($_POST['subzone'])) ? $_POST['subzone']: '' ?>
                                    <select class="form-control select" name="subzone" id="subzone">
                                        <option value=""><?= $this->lang->line("allsf") ?></option>
                                        <?php if (!empty($subzones)) : ?>
                                            <?php foreach ($subzones as $subzone): ?>
                                                <option value="<?= $subzone->id ?>" data-code="<?= $subzone->subzone_code ?>" <?= ($subzone->id == $subzoneId) ? 'selected' : '' ?>><?= $subzone->subzone_name ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                    <input type="hidden" name="subzoneCode" id="subzoneCode" value="<?= $_POST['subzoneCode'] ?? '' ?>">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="filter_date"><?= lang("filter_date"); ?></label>
                                    <?php
                                    $date_opt[""] = lang('select');
                                    $date_opt[1] = lang('document_date');
                                    $date_opt[2] = lang('delivery_date');
                                    echo form_dropdown('filter_date', $date_opt, (isset($_POST['filter_date']) ? $_POST['filter_date'] : ""), 'class="form-control" id="filter_date" data-placeholder="' . $this->lang->line("select") . '"');
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">                                                    
                            <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                <div class="form-group">
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>
                            </div>

                            <div class="date_controls_dh">
                                <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endif ?>
                                <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <div class="form-group">
                                        <?= lang('start_date', 'start_date_dh') ?>
                                        <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                    </div>
                                </div>
                                <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <div class="form-group">
                                        <?= lang('end_date', 'end_date_dh') ?>
                                        <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $this->sma->fld($_POST['end_date']) : $this->filtros_fecha_final ?>" class="form-control datetime">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <br>
                                    <label for="group_by_order">
                                        <input type="checkbox" name="group_by_order" id="group_by_order">
                                        Agrupar por Orden
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <br>
                                    <label for="group_by_zone">
                                        <input type="checkbox" name="group_by_zone" id="group_by_zone">
                                        Agrupar por Zonas
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <b>Generar en :</b>
                                    <br>
                                    <label>
                                        <input type="radio" name="print_to" value="pdf" checked>
                                        <?= lang('PDF') ?>
                                    </label>
                                    <label>
                                        <input type="radio" name="print_to" value="xls">
                                        <?= lang('Excel') ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary new-button pull-right" data-toggle="tooltip" title="<?= lang('generate') ?>">
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });

        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    
        $('[data-toggle="tooltip"]').tooltip();

        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%'
        });

        nscustomer();

        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            startDate: min_input_date,
            endDate: max_input_date,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());

        $(document).on('change', '#country', function(){ getStates(); });
        $(document).on('change', '#state', function(){ getCities(); });
        $(document).on('change', '#city', function() { getZones(); });
        $(document).on('change', '#zone', function() { getSubzones() });
        $(document).on('change', '#subzone', function() { getSubzoneCode() });
    });

</script>

