<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_contracts_model extends CI_Model {

	public function get()
	{
		$q = $this->db->get("payroll_contracts");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_by_biller_id($endDatePayroll = NULL, $branch_id = NULL)
	{
		if (!empty($branch_id)) {
			$this->db->where("biller_id", $branch_id);
		}

        if (!empty($endDatePayroll)) {
            $this->db->where("start_date <=", $endDatePayroll);
        }
		$this->db->order_by("start_date", "ASC");
		$q = $this->db->get("payroll_contracts");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_by_id($id)
	{
		$this->db->where("id", $id);
		$q = $this->db->get("payroll_contracts");
		if ($q->num_rows() > 0) {
			return $q->row();
		}
		return FALSE;
	}

	public function get_by_status($status)
	{
		$this->db->where("contract_status", $status);
		$q = $this->db->get("payroll_contracts");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_by_employee_id($id, $contract_id)
	{
		$this->db->where(["id"=>$contract_id]);
		$this->db->where(["companies_id"=>$id]);
		$q = $this->db->get("payroll_contracts");
		if ($q->num_rows() > 0) {
			return $q->row();
		}
		return FALSE;
	}

	public function get_employee_by_id($id)
	{
		$q = $this->db->where("id", $id)->get("companies");
		if ($q->num_rows() > 0) {
			return $q->row();
		}
		return FALSE;
	}

	public function get_types_contracts()
	{
		$q = $this->db->get("payroll_types_contracts");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_types_employees()
	{
		$q = $this->db->get("payroll_types_employees");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_types_employees_by_id($id)
	{
		$q = $this->db->where("id", $id)->get("payroll_types_employees");
		if ($q->num_rows() > 0) {
			return $q->row();
		}
		return FALSE;
	}

	public function get_billers()
	{
		$q = $this->db->where("group_name", "biller")->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_areas()
	{
		$q = $this->db->get("payroll_areas");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_workdays()
	{
		$q = $this->db->get("payroll_workday");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_professional_positions()
	{
		$q = $this->db->get("payroll_professional_position");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_professional_position_by_area_id($area_id)
	{
		$this->db->where("area_id", $area_id);
        $this->db->order_by("name");
		$q = $this->db->get("payroll_professional_position");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_internal_code($internal_code, $employee_id = NULL)
	{
		$this->db->where("companies_id != ", $employee_id);
		$q = $this->db->where("internal_code", $internal_code)->get("payroll_contracts");
		if ($q->num_rows() > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function get_payment_means()
	{
		$q = $this->db->where_in("code", [MCASH, CHECK, WIRE_TRANSFER])->get("payment_mean_code_fe");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_banks()
	{
		$this->db->where("supplier_type", FINANCIAL_ENTITY);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_afps()
	{
		// $this->db->select();
		$this->db->where("supplier_type", AFP_LAYOFFS);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_epss()
	{
		$this->db->where("supplier_type", EPS);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_arls()
	{
		$this->db->where("supplier_type", ARL);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_cajas()
	{
		$this->db->where("supplier_type", COMPENSATION_BOX);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_cesantias()
	{
		$this->db->where("supplier_type", AFP_LAYOFFS);
		$q = $this->db->get("companies");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_payment_frequency()
	{
		$this->db->where('status', YES);
		$this->db->where('id !=', DEFINED_IN_CONTRACT);
		$q = $this->db->get("payroll_period");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_arl_risk_classes()
	{
		$q = $this->db->get("payroll_arl_risk_classes");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_payroll_contract_concepts($employee_id, $contract_id)
	{
		$this->db->select("payroll_contract_concepts.concept_id, payroll_concepts.name, payroll_contract_concepts.amount");
		$this->db->join("payroll_concepts", "payroll_concepts.id = payroll_contract_concepts.concept_id", "left");
		$this->db->where("employee_id", $employee_id);
		$this->db->where("contract_id", $contract_id);
		$this->db->where("deleted", NOT);
		$q = $this->db->get("payroll_contract_concepts");
		return $q->result();
	}

	public function insert($data)
	{
		if ($this->db->insert("payroll_contracts", $data)) {
			return $this->db->insert_id();
		}
		return FALSE;
	}

	public function update($data, $employee_id, $contract_id)
	{
		$this->db->where("id", $contract_id);
		$this->db->where("companies_id", $employee_id);
		if ($this->db->update("payroll_contracts", $data)) {
			return TRUE;
		}
		return FALSE;
	}

	public function delete(int $employee_id)
	{
		$this->db->where('companies_id', $employee_id);
		if ($this->db->delete('payroll_contracts')) {
			return TRUE;
		}
		return FALSE;
	}

	public function getPayrollContracts()
	{
		$this->db->select("
			c.name,
			tc.description contract_type,
			internal_code,
			creation_date,
			start_date,
			end_date,
			settlement_date,
			b.name biller,
			a.name area,
			pp.name professional_position,
			base_amount,
			CASE
				WHEN withholding_method = 0 THEN 'No aplica'
				WHEN withholding_method = 1 THEN 'Método 1'
				WHEN withholding_method = 2 THEN 'Método 2'
				WHEN withholding_method = 3 THEN 'Método Manual'
			END withholding_method,
			withholding_percentage,
			pmfe.name payment_method,
			te.description employee_type,
			fp.name afp,
			eps.name eps,
			ce.name cesantia,
			arl.name arl,
			CONCAT(arlc.name, ' ', arlc.description) AS arl_risk_classes,
			caj.name caja,
			(SELECT
					GROUP_CONCAT(name)
				FROM
					sma_payroll_contract_concepts pcc
						JOIN
					sma_payroll_concepts pc ON id = pcc.concept_id
				WHERE
					contract_id = 1) AS contract_concepts");
		$this->db->join("companies c", "c.id = companies_id", "inner");
		$this->db->join("payroll_types_contracts tc", "tc.id = contract_type", "inner");
		$this->db->join("payroll_types_employees te", "te.code = employee_type", "inner");
		$this->db->join("companies b", "b.id = biller_id", "inner");
		$this->db->join("payroll_areas a", "a.id = area", "inner");
		$this->db->join("payroll_professional_position pp", "pp.id = professional_position", "inner");
		$this->db->join("payment_mean_code_fe pmfe", "pmfe.code = payment_method", "inner");
		$this->db->join("companies fp", "fp.id = afp_id", "inner");
		$this->db->join("companies eps", "eps.id = eps_id", "inner");
		$this->db->join("companies ce", "ce.id = cesantia_id", "inner");
		$this->db->join("companies arl", "arl.id = arl_id", "inner");
		$this->db->join("payroll_arl_risk_classes arlc", "arlc.id = arl_risk_classes", "inner");
		$this->db->join("companies caj", "caj.id = caja_id", "inner");
		$q = $this->db->get("payroll_contracts");
		return $q->result();
	}

}

/* End of file payroll_contracts_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/payroll_contracts_model.php */