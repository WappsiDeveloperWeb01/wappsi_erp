<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'stForm');
                            echo admin_form_open_multipart("products/sequentialCount", $attrib);
                            ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "biller"); ?>
                                                <?php
                                                $wh[''] = '';
                                                foreach ($billers as $biller) {
                                                    $wh[$biller->id] = $biller->name;
                                                }
                                                echo form_dropdown('biller', $wh, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="biller" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } else {
                                        $biller_input = array(
                                            'type' => 'hidden',
                                            'name' => 'biller',
                                            'id' => 'biller',
                                            'value' => $this->session->userdata('biller_id'),
                                            );

                                        echo form_input($biller_input);
                                    } ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("reference_no", "document_type_id"); ?>
                                            <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                        </div>
                                    </div>

                                    <?php if ($Owner || $Admin) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("date", "qadate"); ?>
                                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="qadate" required="required"'); ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?= form_hidden('count_id', $count_id); ?>

                                    <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("warehouse", "qawarehouse"); ?>
                                                <?php
                                                $wh[''] = '';
                                                unset($wh);
                                                foreach ($warehouses as $warehouse) {
                                                    $wh[$warehouse->id] = $warehouse->name;
                                                }
                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ($warehouse_id ? $warehouse_id :$Settings->default_warehouse)), 'id="qawarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" '.($warehouse_id ? 'readonly' : '').' style="width:100%;"');
                                                ?>
                                            </div>
                                        </div>
                                        <?php } else {
                                            $warehouse_input = array(
                                                'type' => 'hidden',
                                                'name' => 'warehouse',
                                                'id' => 'qawarehouse',
                                                'value' => $this->session->userdata('warehouse_id'),
                                                );

                                            echo form_input($warehouse_input);
                                        } ?>



                                    <div class="col-md-4 form-group">
                                        <?= lang('third', 'company_id') ?>
                                        <?php
                                        $cOptions[''] = lang('select_third');
                                        foreach ($companies as $row => $company) {
                                            $cOptions[$company['id']] = $company['name'];
                                        }

                                         ?>
                                        <?= form_dropdown('company_id', $cOptions, '',' id="company_id" class="form-control select" required style="width:100%;"'); ?>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <label><?= lang("type"); ?> *</label>
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-2">
                                                            <input type="radio" class="checkbox type" value="full" name="type" id="full" <?= $this->input->post('type') ? 'checked="checked"' : 'checked'; ?> required="required">
                                                            <label for="full" class="padding05">
                                                                <?= lang('full'); ?>
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-2">
                                                            <input type="radio" class="checkbox type" value="partial" name="type" id="partial" <?= $this->input->post('type') ? 'checked="checked"' : ''; ?>>
                                                            <label for="partial" class="padding05">
                                                                <?= lang('partial'); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="clearfix"></div>
                                    <div class="col-md-12 partials" style="display:none;">
                                    <div class="well well-sm">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("brands", "brand"); ?>
                                                    <?php
                                                    unset($wh);
                                                    foreach ($brands as $brand) {
                                                        $wh[$brand->id] = $brand->name;
                                                    }
                                                    echo form_dropdown('brand[]', $wh, (isset($_POST['brand']) ? $_POST['brand'] : 0), 'id="brand" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("brand") . '" style="width:100%;" multiple');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("categories", "category"); ?>
                                                    <?php
                                                    unset($wh);
                                                    foreach ($categories as $category) {
                                                        $wh[$category->id] = $category->name;
                                                    }
                                                    echo form_dropdown('category[]', $wh, (isset($_POST['category']) ? $_POST['category'] : 0), 'id="category" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("category") . '" style="width:100%;" multiple');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="col-md-12" id="sticker">
                                        <div class="well well-sm">
                                            <div class="form-group" style="margin-bottom:0;">
                                                <div class="input-group wide-tip">
                                                    <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                        <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                                    <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("products"); ?> *</label>

                                            <div class="controls table-controls">
                                                <table id="qaTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <!-- <th class="col-md-2"><?= lang("variant"); ?></th> -->
                                                        <!-- <th class="col-md-1"><?= lang("type"); ?></th> -->
                                                        <th class="col-md-1"><?= lang("quantity"); ?></th>
                                                        <?php
                                                        if ($Settings->product_serial) {
                                                            echo '<th class="col-md-4">' . lang("serial_no") . '</th>';
                                                        }
                                                        ?>
                                                        <th style="max-width: 30px !important; text-align: center;">
                                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= lang("note", "qanote"); ?>
                                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qanote" style="margin-top: 10px; height: 100px;"'); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <div
                                            class="fprom-group"><?php echo form_submit('add_sequential_count', lang("submit"), 'id="add_sequential_count" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                            <button type="button" class="btn btn-danger" id="resetSC"><?= lang('reset') ?></div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    $(document).ready(function () {
        $('input').keydown(function (e) {
            if (e.which == 13) {
                var s = $(this).val();
                $(this).val(s+'\n').focus();
                e.preventDefault();
                console.log('Se presiono enter.');
                return false;
            }
        });

        if (localStorage.getItem('remove_qals')) {
            if (localStorage.getItem('inventoryItems')) {
                localStorage.removeItem('inventoryItems');
            }
            if (localStorage.getItem('qaref')) {
                localStorage.removeItem('qaref');
            }
            if (localStorage.getItem('qawarehouse')) {
                localStorage.removeItem('qawarehouse');
            }
            if (localStorage.getItem('qanote')) {
                localStorage.removeItem('qanote');
            }
            if (localStorage.getItem('qadate')) {
                localStorage.removeItem('qadate');
            }
            localStorage.removeItem('remove_qals');
        }

        <?php if ($adjustment_items) { ?>
            localStorage.setItem('inventoryItems', JSON.stringify(<?= $adjustment_items; ?>));
        <?php } ?>
        <?php if ($warehouse_id) { ?>
            localStorage.setItem('qawarehouse', '<?= $warehouse_id; ?>'); $('#qawarehouse').select2('readonly', true);
        <?php } ?>

        <?php if ($Owner || $Admin) { ?>
            if (!localStorage.getItem('qadate')) {
                $("#qadate").datetimepicker({
                    format: site.dateFormats.js_ldate,
                    fontAwesome: true,
                    language: 'sma',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0
                }).datetimepicker('update', new Date());
            }
            $(document).on('change', '#qadate', function (e) {
                localStorage.setItem('qadate', $(this).val());
            });
            if (qadate = localStorage.getItem('qadate')) {
                $('#qadate').val(qadate);
            }
        <?php } ?>

        //if (localStorage.getItem('inventoryItems')) {
            //loadItems();
        //}

        $("#add_item").autocomplete({
            source: function(request, response) {

                if (!$('#document_type_id').val() || !$('#company_id').val()) {
                    var msg = "";
                    if (!$('#document_type_id').val()) {
                        msg += "</br><?= lang('reference_no') ?>";
                    }
                    if (!$('#company_id').val()) {
                        msg += "</br><?= lang('third') ?>";
                    }
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    command: toastr.warning('<?=lang('select_above');?> : '+msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#add_item').focus();
                    return false;
                }

                var src = '<?= admin_url('products/sc_suggestions'); ?>';
                //Necesitamos armar array con todas las categorias y todas alas marcas
                categories = [];
                categories = $('#category').val();
                brands = [];
                brands =  $('#brand').val();
                $.ajax({
                    url: src,
                    dataType: "json",
                    data: {
                        term : request.term,
                        brands:brands,
                        categories:categories
                    },
                    success: function(ui) {
                        console.log('autocomplete');
                        response(ui);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                console.log('Termina la busqueda');
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                    $('#add_item').focus();
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                    console.log('Resultados = 1');
                    $('#add_item').focus();
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                    $('#add_item').focus();
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                console.log(ui);
                if (ui.item.id !== 0) {
                    var row = add_inventory_item(ui.item);
                    if (row)
                    $(this).val('');
                    $('#add_item').focus();
                }
                else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                    $('#add_item').focus();
                }
             }
        });
    });
    //Termina el document Ready

	//Aceite 33320729
    function add_inventory_item(item) {
        if (count == 1) {
            inventoryItems = {};
        }
        if (item == null)
            return;
        //var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
        var item_id = item.item_id;
        console.log('Agregando Item '+item_id);
        console.log(item);
        console.log(inventoryItems);
        if (inventoryItems[item_id]) {
            console.log('Encontrado');
            var new_qty = parseFloat(inventoryItems[item_id].row.qty) + 1;
            inventoryItems[item_id].row.base_quantity = new_qty;
            if(inventoryItems[item_id].row.unit != inventoryItems[item_id].row.base_unit) {
                $.each(inventoryItems[item_id].units, function(){
                    if (this.id == inventoryItems[item_id].row.unit) {
                        inventoryItems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                    }
                });
            }
            inventoryItems[item_id].row.qty = new_qty;
        } else {
            console.log('No Encontrado');
            inventoryItems[item_id] = item;
        }
        inventoryItems[item_id].order = new Date().getTime();
        localStorage.setItem('inventoryItems', JSON.stringify(inventoryItems));
        loadItems();
        return true;
    }

    function loadItems() {
        if (localStorage.getItem('inventoryItems')) {
            count = 1;
            an = 1;
            $("#qaTable tbody").empty();
            inventoryItems = JSON.parse(localStorage.getItem('inventoryItems'));
            sortedItems = (site.settings.item_addition == 1) ? _.sortBy(inventoryItems, function(o){return [parseInt(o.order)];}) : inventoryItems;
            $.each(sortedItems, function () {
                var item = this;
                var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
                item.order = item.order ? item.order : new Date().getTime();
                var product_id = item.row.id, item_qty = item.row.qty, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");


                var type = item.row.type ? item.row.type : '';
                var opt = $("<select id=\"poption\" name=\"variant\[\]\" class=\"form-control select rvariant\" />");

                if(item.options !== false) {
                    $.each(item.options, function () {
                        if (item.row.option == this.id)
                            $("<option />", {value: this.id, text: this.name, selected: 'selected'}).appendTo(opt);
                        else
                            $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                    });
                } else {
                    $("<option />", {value: 0, text: 'n/a'}).appendTo(opt);
                    opt = opt.hide();
                }

                var row_no = (new Date).getTime();
                var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
                tr_html = '<td><input name="product_id[]" type="hidden" class="rid" value="' + product_id + '"><span class="sname" id="name_' + row_no + '">' + item_code +' - ' + item_name +'</span></td>';


                // tr_html += '<td>'+(opt.get(0).outerHTML)+'</td>';

                //tr_html += '<td><select name="type[]" class="form-contol select rtype" style="width:100%;"><option value="subtraction"'+(type == 'subtraction' ? ' selected' : '')+'>'+type_opt.subtraction+'</option><option value="addition"'+(type == 'addition' ? ' selected' : '')+'>'+type_opt.addition+'</option></select></td>';



                tr_html += '<td><input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" readonly value="' + formatQuantity2(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
                if (site.settings.product_serial == 1) {
                    tr_html += '<td class="text-right"><input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'"></td>';
                }


                tr_html += '<td class="text-center"><i class="fa fa-times tip qadel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                newTr.html(tr_html);
                newTr.prependTo("#qaTable");
                count += parseFloat(item_qty);
                an++;

            });

            //var col = 3;
            var col = 1;
            var tfoot = '<tr id="tfoot" class="tfoot active"><th colspan="'+col+'">Total</th><th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
            if (site.settings.product_serial == 1) { tfoot += '<th></th>'; }
            tfoot += '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th></tr>';
            $('#qaTable tfoot').html(tfoot);
            //$('select.select').select2({minimumResultsForSearch: 7});
            if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
                $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
                $(window).scrollTop($(window).scrollTop() + 1);
            }
            set_page_focus();
        }
    }

    /* ----------------------
     * Delete Row Method
     * ---------------------- */
    $(document).on('click', '.qadel', function () {
        var row = $(this).closest('tr');
        //var item_id = row.attr('data-item-id');
        var item_id =  $(row).find(".rid").val();
        console.log(item_id);
        console.log(inventoryItems);
        delete inventoryItems[item_id];
        row.remove();
        if(inventoryItems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('inventoryItems', JSON.stringify(inventoryItems));
            loadItems();
            return;
        }
    });

    $(document).ready(function() {
        $("#brand option[value=''], #category option[value='']").remove();
        $('.type').on('ifChecked', function(e){
            var type_opt = $(this).val();
            if (type_opt == 'partial')
                $('.partials').slideDown();
            else
                $('.partials').slideUp();
            $('#stForm').bootstrapValidator('revalidateField', $(this));
        });
        //$("#date").datetimepicker({format: site.dateFormats.js_ldate, fontAwesome: true, language: 'sma', weekStart: 1, todayBtn: 1, autoclose: 1, todayHighlight: 1, startView: 2, forceParse: 0, startDate: "<?= $this->sma->hrld(date('Y-m-d H:i:s')); ?>"});

        $("#resetSC").click(function() {
            console.log('Click en reset.');
            localStorage.removeItem('inventoryItems');
            location.reload();
        });
        $('#biller').trigger('change');

    });

    $(document).on('change', '#biller', function (e) {
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/38/") ?>'+$('#biller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;

          $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }

          if (response.status == 0) {
            $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            localStorage.setItem('locked_for_biller_resolution', 1);
          }

          $('#document_type_id').trigger('change');
        });
    });
</script>