<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('deposits') . " (" . $company->name . ")"; ?></h4>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <p class="no-print"><?= lang('deposits_subheading'); ?></p>
                </div>
                <div class="col-md-6">
                    <a href="<?= admin_url('suppliers/add_deposit/').$company->id ?>" data-toggle="modal" data-target="#myModal2" class="btn btn-primary pull-right"><?= lang("add_deposit"); ?></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="alerts-con"></div>

            <div class="table-responsive">
                <table id="DepData" cellpadding="0" cellspacing="0" border="0"
                       class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr class="primary">
                        <th></th>
                        <th><?= lang("reference"); ?></th>
                        <th><?= lang("date"); ?></th>
                        <th><?= lang("value"); ?></th>
                        <th><?= lang("applied_valued"); ?></th>
                        <th><?= lang("balance"); ?></th>
                        <th><?= lang("paid_by"); ?></th>
                        <th><?= lang("origin"); ?></th>
                        <th><?= lang("origin_reference_no"); ?></th>
                        <th><?= lang("created_by"); ?></th>
                        <th><?= lang("suppliers"); ?></th>
                        <th><?= lang("actions"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?= $modal_js ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tip').tooltip();
            oTable = $('#DepData').dataTable({
                "aaSorting": [[1, "asc"]],
                "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('suppliers/get_deposits/'.$company->id) ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                "aoColumns": [{"bVisible": false}, null,{"mRender": fld},{"mRender": currencyFormat}, {"mRender": currencyFormat}, {"mRender": currencyFormat}, null, null, null, null, null, {"bSortable": false}]
            });
            $('div.dataTables_length select').addClass('form-control');
            $('div.dataTables_length select').addClass('select2');
            $('div.dataTables_filter input').attr('placeholder', 'Buscar...');
            $('select.select2').select2({minimumResultsForSearch: 7});
        });
    </script>

