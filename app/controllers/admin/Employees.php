<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->enableElectronicPayroll) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('payroll', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('Employees_model');
        $this->load->admin_model('UserActivities_model');
        $this->load->admin_model('Payroll_management_model');
        $this->load->admin_model('Payroll_contracts_model');
    }

	public function index()
	{
        $this->sma->checkPermissions('index', NULL, 'payroll_management');
        $this->data['action'] = "";
        $this->page_construct("employees/index", array("page_title" => lang("employees")), $this->data);
	}

	public function get_employees_datatables()
    {
        // $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->load->library('datatables');
        $this->datatables->select("companies.id AS id,
                LOWER(".$this->db->dbprefix('companies').".name) AS name,
                companies.vat_no,
                companies.email,
                companies.status,
                companies.phone,
                companies.birth_month AS birth_month,
                companies.birth_day AS birth_day");
        $this->datatables->from("companies");
        $this->datatables->join("employee_data", "employee_data.companies_id = companies.id", "left");
        $this->datatables->join('groups', 'groups.id = companies.group_id', 'inner');
        $this->datatables->where("(groups.name = 'employee' OR (groups.name='seller' AND ".$this->db->dbprefix('companies').".mark_as_seller='1'))");
        $this->datatables->add_column("Actions", '<div class="text-center">
                                        <div class="btn-group text-left">
                                            <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                                            <i class="fas fa-ellipsis-v fa-lg"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li>
                                                    <a href="'. admin_url('employees/edit/$1') .'"><i class="fa fa-edit"></i>'. lang("employees_edit") . '</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>', "id");
        echo $this->datatables->generate();
    }

    public function see($id = NULL, $contract_id = NULL)
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $employee = $this->Employees_model->get_by_id($id, $contract_id);
        $this->data["employee"] = $employee;
        $this->data["types_contracts"] = $this->Employees_model->get_types_contracts();
        $this->data["payment_means"] = $this->Payroll_contracts_model->get_payment_means();
        $this->data["employee_contacts"] = $this->Employees_model->get_employee_contacts($id);
        $this->data["type_regime"] = $this->Employees_model->get_types_regime_by_id($employee->tipo_regimen);
        $this->data["document_type"] = $this->Employees_model->get_document_types_by_id($employee->tipo_documento);
        $this->data["concepts_contract"] = $this->Payroll_contracts_model->get_payroll_contract_concepts($id, $employee->contract_id);
        $this->load_view($this->theme.'employees/see', $this->data);
    }

    public function add($id = NULL)
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->data['jasny_js'] = $this->site->jasny_js();
        $this->data["countries"] = $this->Employees_model->get_countries();
        $this->data["document_types"] = $this->Employees_model->get_documenttypes();
        $this->data["types_regime"] = $this->Employees_model->get_types_regime();

        $this->page_construct('employees/add', array('page_title' => lang('employees_add')), $this->data);
    }

    public function validate_existing_document_number()
    {
        $id = $this->input->get("id");
    	$document_number = $this->input->get('document_number');

    	$existing_document_number = $this->Employees_model->get_document_number($document_number, $id);
    	if ($existing_document_number === TRUE) {
    		echo json_encode(TRUE);
    	} else {
    	   echo json_encode(FALSE);
        }
    }

    public function validate_existing_email() {
    	$email = $this->input->get('email');
    	$existing_email = $this->Employees_model->get_email($email);
    	if ($existing_email === TRUE) {
    		echo json_encode(TRUE);
    	} else {
    	   echo json_encode(FALSE);
        }
    }

    public function save()
    {
    	$this->sma->checkPermissions('index', NULL, 'payroll_management');

    	$this->form_validation->set_rules("first_name", lang("first_name"), "trim|required");
    	$this->form_validation->set_rules("first_lastname", lang("first_lastname"), "trim|required");
    	$this->form_validation->set_rules("tipo_documento", lang("employees_document_type"), "trim|required");
    	$this->form_validation->set_rules("vat_no", lang("employees_document_number"), "trim|required");
    	$this->form_validation->set_rules("educational_level", lang("employees_educational_level"), "trim|required");
    	$this->form_validation->set_rules("profession", lang("employees_profession"), "trim|required");
    	$this->form_validation->set_rules("marital_status", lang("marital_status"), "trim|required");
    	$this->form_validation->set_rules("blood_type", lang("employees_blood_type"), "trim|required");
    	$this->form_validation->set_rules("birthdate", lang("birthdate"), "trim|required");
    	$this->form_validation->set_rules("gender", lang("employees_gender"), "trim|required");
    	$this->form_validation->set_rules("gender", lang("employees_gender"), "trim|required");
    	if ($this->input->post('gender') == MALE) { $this->form_validation->set_rules("military_card", lang("employees_military_card"), "trim"); }
    	$this->form_validation->set_rules("email", lang("employees_email"), "trim|required");
    	$this->form_validation->set_rules("country", lang("country"), "trim|required");
    	$this->form_validation->set_rules("state", lang("state"), "trim|required");
    	$this->form_validation->set_rules("city", lang("city"), "trim|required");
    	$this->form_validation->set_rules("address", lang("address"), "trim|required");
    	$this->form_validation->set_rules("phone", lang("employees_phone"), "trim|required");
    	$this->form_validation->set_rules("contact_name[]", lang("contact_name"), "trim|required");
    	$this->form_validation->set_rules("relationship[]", lang("employees_relationship"), "trim|required");
    	$this->form_validation->set_rules("contact_address[]", lang("address"), "trim|required");
    	$this->form_validation->set_rules("contact_phone[]", lang("phone"), "trim|required");

    	if($this->form_validation->run()){
            $group_data_for_employees = $this->Employees_model->get_group_data_for_employees();

    		$companies_data = [
    			"group_id" => $group_data_for_employees->id,
    			"group_name"=> $group_data_for_employees->name,
    			"type_person"=>2,
    			"name"=>$this->input->post("first_name") . ($this->input->post("second_name")?" ".$this->input->post("second_name"):"") ." ". $this->input->post("first_lastname") . ($this->input->post("second_lastname")?" ".$this->input->post("second_lastname"):""),
    			"first_name"=>$this->input->post("first_name"),
    			"second_name"=>$this->input->post("second_name"),
    			"first_lastname"=>$this->input->post("first_lastname"),
    			"second_lastname"=>$this->input->post("second_lastname"),
    			"type_person"=>NATURAL_PERSON,
    			"tipo_documento"=>$this->input->post("tipo_documento"),
    			"document_code"=>$this->input->post("document_code"),
    			"vat_no"=>str_replace([".", ","], "", $this->input->post("vat_no")),
    			"digito_verificacion"=>$this->input->post("digito_verificacion"),
    			"address"=>$this->input->post("address"),
    			"city"=>$this->input->post("city"),
    			"state"=>$this->input->post("state"),
    			"country"=>$this->input->post("country"),
    			"postal_code"=>$this->input->post("postal_code"),
    			"phone"=>$this->input->post("phone"),
    			"email"=>$this->input->post("email"),
    			"tipo_regimen"=>2,
    			"status"=>ACTIVE,
    			"city_code"=>$this->input->post("city_code"),
    			"birth_month"=>date("n", strtotime($this->input->post("birthdate"))),
    			"birth_day"=>date("j", strtotime($this->input->post("birthdate"))),
    			"gender"=>$this->input->post("gender"),
                "created_by" => $this->session->userdata('user_id')
    		];

    		if($_FILES['employee_photo']['size'] > 0){
                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/avatars';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2048';
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if(!$this->upload->do_upload('employee_photo')){
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    $this->session->set_flashdata('post', $_POST);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $companies_data['customer_profile_photo'] = $photo;
                $config = NULL;
            }

            $companies_id = $this->Employees_model->insert_companies($companies_data);
            if ($companies_id !== FALSE) {
            	$employee_data = [
            		"companies_id"=>$companies_id,
            		"home_phone"=>$this->input->post("home_phone"),
            		"marital_status"=>$this->input->post("marital_status"),
            		"blood_type"=>$this->input->post("blood_type"),
            		"military_card"=>$this->input->post("military_card"),
            		"birthdate"=>$this->input->post("birthdate"),
            		"educational_level"=>$this->input->post("educational_level"),
            		"profession"=>$this->input->post("profession"),
                    'created_by'=>$this->session->userdata('user_id')
            	];
            	$employee_data_saved = $this->Employees_model->insert_employees_data($employee_data);

                foreach ($this->input->post('contact_name') as $key => $contact) {
                    $employee_contacts_data[] = [
                        "companies_id"=>$companies_id,
                        "name"=>$this->input->post('contact_name')[$key],
                        "relationship"=>$this->input->post('relationship')[$key],
                        "address"=>$this->input->post('contact_address')[$key],
                        "phone"=>$this->input->post('contact_phone')[$key],
                        "email"=>$this->input->post('contact_email')[$key],
                        'main_contact'=>($key == $this->input->post('main_contact') ? YES : NOT)
                    ];
                }

            	$employee_contacts_saved = $this->Employees_model->insert_employees_contact($employee_contacts_data);

            	if ($employee_data_saved == TRUE) {
    				$this->session->set_flashdata('message', lang("employees_saved"));
                    if ($this->input->post("add_contract") == 1) {
            		    admin_redirect('payroll_contracts/add/'.$companies_id);
                    } else {
                        admin_redirect("employees");
                    }
            	} else {
            		$this->session->set_flashdata('error', lang("employees_not_saved"));
            		admin_redirect('employees/add');
            	}
            } else {
                $this->session->set_flashdata("post", (object) $this->input->post());
            	$this->session->set_flashdata('error', lang("employees_not_saved"));
        		admin_redirect('employees/add');
            }
    	}else{
            $this->session->set_flashdata("post", (object) $this->input->post());
    		$this->session->set_flashdata('error', validation_errors());
            admin_redirect('employees/add');
    	}
    }

    public function edit($id, $contract_id = NULL)
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->data['jasny_js'] = $this->site->jasny_js();
        $this->data["countries"] = $this->Employees_model->get_countries();
        $this->data["types_regime"] = $this->Employees_model->get_types_regime();
        $this->data["document_types"] = $this->Employees_model->get_documenttypes();
        $this->data["employee_data"] = $this->Employees_model->get_by_id($id, $contract_id);
        $this->data["employee_contacts"] = $this->Employees_model->get_employee_contacts($id);

        $this->page_construct('employees/edit', array('page_title' => lang('employees_edit')), $this->data);
    }

    public function update()
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->form_validation->set_rules("first_name", lang("first_name"), "trim|required");
        $this->form_validation->set_rules("first_lastname", lang("first_lastname"), "trim|required");
        $this->form_validation->set_rules("tipo_documento", lang("employees_document_type"), "trim|required");
        $this->form_validation->set_rules("vat_no", lang("employees_document_number"), "trim|required");
        $this->form_validation->set_rules("educational_level", lang("employees_educational_level"), "trim|required");
        $this->form_validation->set_rules("profession", lang("employees_profession"), "trim|required");
        $this->form_validation->set_rules("marital_status", lang("marital_status"), "trim|required");
        $this->form_validation->set_rules("blood_type", lang("employees_blood_type"), "trim|required");
        $this->form_validation->set_rules("birthdate", lang("birthdate"), "trim|required");
        $this->form_validation->set_rules("gender", lang("employees_gender"), "trim|required");
        $this->form_validation->set_rules("gender", lang("employees_gender"), "trim|required");
        if ($this->input->post('gender') == MALE) { $this->form_validation->set_rules("military_card", lang("employees_military_card"), "trim"); }
        $this->form_validation->set_rules("email", lang("employees_email"), "trim|required");
        $this->form_validation->set_rules("country", lang("country"), "trim|required");
        $this->form_validation->set_rules("state", lang("state"), "trim|required");
        $this->form_validation->set_rules("city", lang("city"), "trim|required");
        $this->form_validation->set_rules("address", lang("address"), "trim|required");
        $this->form_validation->set_rules("phone", lang("employees_phone"), "trim|required");
        $this->form_validation->set_rules("contact_name[]", lang("contact_name"), "trim|required");
        $this->form_validation->set_rules("relationship[]", lang("employees_relationship"), "trim|required");
        $this->form_validation->set_rules("contact_address[]", lang("address"), "trim|required");
        $this->form_validation->set_rules("contact_phone[]", lang("phone"), "trim|required");

        $employee_id = $this->input->post("employee_id");

        if($this->form_validation->run()){
            $companies_data = [
                "name"=>$this->input->post("first_name") . ($this->input->post("second_name")?" ".$this->input->post("second_name"):"") ." ". $this->input->post("first_lastname") . ($this->input->post("second_lastname")?" ".$this->input->post("second_lastname"):""),
                "first_name"=>$this->input->post("first_name"),
                "second_name"=>$this->input->post("second_name"),
                "first_lastname"=>$this->input->post("first_lastname"),
                "second_lastname"=>$this->input->post("second_lastname"),
                "tipo_documento"=>$this->input->post("tipo_documento"),
                "document_code"=>$this->input->post("document_code"),
                "vat_no"=>str_replace([".", ","], "", $this->input->post("vat_no")),
                "digito_verificacion"=>$this->input->post("digito_verificacion"),
                "address"=>$this->input->post("address"),
                "city"=>$this->input->post("city"),
                "state"=>$this->input->post("state"),
                "country"=>$this->input->post("country"),
                "postal_code"=>$this->input->post("postal_code"),
                "phone"=>$this->input->post("phone"),
                "email"=>$this->input->post("email"),
                "tipo_regimen"=>2,
                'status'=>$this->input->post('status'),
                "city_code"=>$this->input->post("city_code"),
                "birth_month"=>date("n", strtotime($this->input->post("birthdate"))),
                "birth_day"=>date("j", strtotime($this->input->post("birthdate"))),
                "gender"=>$this->input->post("gender")
            ];

            if($_FILES['employee_photo']['size'] > 0){
                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/avatars';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2048';
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if(!$this->upload->do_upload('employee_photo')){
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    $this->session->set_flashdata('post', $_POST);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $companies_data['customer_profile_photo'] = $photo;
                $config = NULL;
            }

            // $this->sma->print_arrays($companies_data);

            $employee_data = $this->Employees_model->get_by_id($employee_id);
            $companies_updated = $this->Employees_model->update_companies($companies_data, $employee_id);
            if ($companies_updated == TRUE) {
                $additional_employee_data = [
                    "home_phone"=>$this->input->post("home_phone"),
                    "marital_status"=>$this->input->post("marital_status"),
                    "blood_type"=>$this->input->post("blood_type"),
                    "military_card"=>$this->input->post("military_card"),
                    "birthdate"=>$this->input->post("birthdate"),
                    "educational_level"=>$this->input->post("educational_level"),
                    "profession"=>$this->input->post("profession")
                ];

                $existing_employee_data = $this->Employees_model->get_employee_data($employee_id);
                if ($existing_employee_data == FALSE) {
                    $additional_employee_data["companies_id"] = $employee_id;
                    $this->Employees_model->insert_employees_data($additional_employee_data);
                } else {
                    $this->Employees_model->update_employee_data($additional_employee_data, $employee_id);

                    $update_array_data = $this->Employees_model->get_by_id($employee_id);
                    $this->set_user_log($employee_data, $update_array_data);
                }

                foreach ($this->input->post('contact_name') as $key => $contact) {
                    $employee_contacts_data[] = [
                        "companies_id"=>$employee_id,
                        "name"=>$this->input->post('contact_name')[$key],
                        "relationship"=>$this->input->post('relationship')[$key],
                        "address"=>$this->input->post('contact_address')[$key],
                        "phone"=>$this->input->post('contact_phone')[$key],
                        "email"=>$this->input->post('contact_email')[$key],
                        'main_contact'=>($key == $this->input->post('main_contact') ? YES : NOT)
                    ];
                }

                $employee_contacts_saved = $this->Employees_model->update_employee_contacts($employee_contacts_data, $employee_id);
                if ($employee_contacts_saved == TRUE) {
                    $this->session->set_flashdata('message', lang("employees_updated"));
                    if ($this->input->post("add_contract") == 1) {
                        admin_redirect('payroll_contracts/add/'.$employee_id);
                    } else {
                        admin_redirect("employees");
                    }
                } else {
                    $this->session->set_flashdata('error', lang("employees_not_updated"));
                    admin_redirect('employees/add');
                }
            } else {
                $this->session->set_flashdata("post", (object) $this->input->post());
                $this->session->set_flashdata('error', validation_errors());
                admin_redirect('employees/edit/'.$employee_id);
            }
        } else {
            $this->session->set_flashdata("post", (object) $this->input->post());
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('employees/edit/'.$employee_id);
        }
    }

    public function get_states($country, $cur_state = null, $response = null)
    {
        $cur_state = urldecode($cur_state);
        $states = $this->Employees_model->get_states_by_country($country);
        if ($response === null) {
            $options = '<option value="">'. lang("select") .'</option>';
            foreach ($states as $row => $state) {

                if ($cur_state !== null && $cur_state == $state->DEPARTAMENTO) {
                    $selected =' selected="selected"';
                } else {
                    $selected = "";
                }

                $options.='<option value="'.$state->DEPARTAMENTO.'" data-code="'.$state->CODDEPARTAMENTO.'"'.$selected.'>'.$state->DEPARTAMENTO.'</option>';
            }
            echo $options;
        } else {
            foreach ($states as $row => $state) {
                $options[$state->DEPARTAMENTO] = $state->DEPARTAMENTO;
            }
            return $options;
        }
    }

    public function get_cities($state, $cur_city = null, $response = null)
    {
        $cur_city = urldecode($cur_city);
        $cities = $this->Employees_model->get_cities_by_state_id($state);
        if ($response === null) {
            $options = '<option value="">'. lang("select") .'</option>';
            foreach ($cities as $row => $city) {

                if ($cur_city !== null && $cur_city == $city->DESCRIPCION) {
                    $selected =' selected="selected"';
                } else {
                    $selected = "";
                }

                $options.='<option value="'.$city->DESCRIPCION.'" data-code="'.$city->CODIGO.'"'.$selected.'>'.$city->DESCRIPCION.'</option>';
            }
            echo $options;
        } else {
            foreach ($cities as $row => $city) {
                $options[$city->DESCRIPCION] = $city->DESCRIPCION;
            }
            return $options;
        }
    }

    public function get_zones($city, $cur_zone = null, $response = null)
    {
        $cur_zone = urldecode($cur_zone);
        $zones = $this->Employees_model->get_zones_by_city_id($city);
        if ($response === null) {
            $options = '<option value="">'. lang("select") .'</option>';
            if ($zones) {
                foreach ($zones as $row => $zone) {
                    if ($cur_zone !== null && $cur_zone == $zone->zone_name) {
                        $selected =' selected="selected"';
                    } else {
                        $selected = "";
                    }

                    $options.='<option value="'.$zone->zone_name.'" data-code="'.$zone->zone_code.'"'.$selected.'>'.$zone->zone_name.'</option>';
                }
            }
            echo $options;
        } else {
            foreach ($zones as $row => $zone) {
                $options[$zone->zone_name] = $zone->zone_name;
            }
            return $options;
        }
    }

    public function get_contact_fields($number_contacts)
    {
	    $ropts = [""=>lang("select"), 1=>"Conyuge", 2=>"Padre", 3=>"Madre", 4=>"Hijo", 5=>"Hija", 6=>"Abuelo", 7=>"Abuela", 8=>"Otros"];
    	$contact_data_string = '<div class="row">'.
                                    '<div class="col-sm-3">'.
                                        '<div class="form-group">'.
                                            form_label(lang("name"), "contact_name").
                                            trim(form_input(["name"=>"contact_name[".$number_contacts."]", "id"=>"contact_name".$number_contacts, "class"=>"form-control", "required"=>TRUE], ""), "\n").
                                        '</div>'.
                                    '</div>'.
                                    '<div class="col-md-2">'.
                                        '<div class="form-group">'.
                                           	form_label(lang("relationship"), "relationship").
                                           	trim(form_dropdown(["name"=>"relationship[".$number_contacts."]", "id"=>"relationship".$number_contacts, "class"=>"form-control validate", "required"=>TRUE], $ropts), "\n").
                                        '</div>'.
                                    '</div>'.
                                    '<div class="col-sm-2">'.
                                        '<div class="form-group">'.
                                            form_label(lang("address"), "contact_address").
                                            trim(form_input(["name"=>"contact_address[".$number_contacts."]", "id"=>"contact_address".$number_contacts, "class"=>"form-control", "required"=>TRUE], ""), "\n").
                                        '</div>'.
                                    '</div>'.
                                    '<div class="col-sm-1">'.
                                        '<div class="form-group">'.
                                            form_label(lang("phone"), "contact_phone").
                                            trim(form_input(["name"=>"contact_phone[".$number_contacts."]", "id"=>"contact_phone".$number_contacts, "type"=>"tel", "class"=>"form-control", "required"=>TRUE], ""), "\n").
                                        '</div>'.
                                    '</div>'.
                                    '<div class="col-sm-2">'.
                                        '<div class="form-group">'.
                                            form_label(lang("email"), "contact_email").
                                            trim(form_input(["name"=>"contact_email[".$number_contacts."]", "id"=>"contact_email".$number_contacts, "type"=>"email", "class"=>"form-control"], ""), "\n").
                                        '</div>'.
                                    '</div>'.
                                    '<div class="col-sm-1">'.
                                        '<div class="form-group text-center">'.
                                            form_label(lang('employees_main_contact'), 'main_contact').
                                            '<br>'.
                                            form_radio(['name'=>'main_contact', 'id'=>'main_contact_'.$number_contacts, 'required'=>TRUE], $number_contacts, FALSE).
                                        '</div>'.
                                    '</div>'.
                                    '<div class="col-sm-1 text-center">'.
                                        trim(form_button(["type"=>"button", "class"=>"btn btn-default new-button remove_contact_fields tip", "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>lang("delete"), "style"=>"margin-top: 29px;"], '<i class="fa fa-trash"></i>'), "\n").
                                    '</div>'.
                                '</div>';
        echo $contact_data_string;
    }

    public function remove()
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $id = $this->input->get("employee_id");
        $contract_id = $this->input->get("contract_id");
        $employee_data = $this->Employees_model->get_by_id($id, $contract_id);

        if ($this->Payroll_management_model->get_existing_employee_payroll($id) == TRUE) {
            echo json_encode([
                'status' => 0,
                'message' => lang("eliminated_not_employee") .'. '. lang('existing_employee_on_payroll')
            ]);
            exit();
        }

        if ($employee_data->group_name == 'employee') {
            $this->delete_employee($id, $contract_id);

            $response_ajax = [
                'status' => 1,
                'message' => lang('eliminated_employee')
            ];
        } else {
            if ($this->Employees_model->get_existing_user_transactions($id)) {
                $response_ajax = [
                    'status' => 0,
                    'message' => lang("eliminated_not_employee") .'. '. lang("existing_user_transactions")
                ];
            } else {
                $this->delete_employee($id, $contract_id);

                $response_ajax = [
                    'status' => 1,
                    'message' => lang('eliminated_employee')
                ];
            }
        }

        echo json_encode($response_ajax);
    }

    private function delete_employee($id, $contract_id)
    {
        $employee_data = $this->Employees_model->get_by_id($id, $contract_id);
        $employee_additional_data = $this->Employees_model->get_employee_data($id);
        $employee_contacts_data = $this->Employees_model->get_employee_contacts($id);
        $employee_contract_data = $this->Payroll_contracts_model->get_by_employee_id($id, $contract_id);

        if ($this->Employees_model->delete_employee_contacts($id)) {
            if ($this->Employees_model->delete_employee_data($id)) {
                if ($this->Payroll_contracts_model->delete($id)) {
                    if ($this->Employees_model->delete($id)) {
                        $this->set_remove_user_log($id, $employee_data);
                        return [
                            'status' => 1,
                            'message' => lang('eliminated_employee')
                        ];
                    } else {
                        if (!empty($employee_additional_data)) {
                            $data = [
                                'companie_id'=>$employee_additional_data->companies_id,
                                'home_phone'=>$employee_additional_data->home_phone,
                                'marital_status'=>$employee_additional_data->marital_status,
                                'blood_type'=>$employee_additional_data->blood_type,
                                'military_card'=>$employee_additional_data->military_card,
                                'birthdate'=>$employee_additional_data->birthdate,
                                'educational_level'=>$employee_additional_data->educational_level,
                                'profession'=>$employee_additional_data->profession,
                                'created_by'=>$employee_additional_data->created_by
                            ];
                            $this->Employees_model->insert_employees_data($data);
                        }

                        if (!empty($employee_contacts_data)) {
                            foreach ($employee_contacts_data as $key => $contact) {
                                $employee_contacts_array[] = [
                                    'companies_id'=>$contact->companies_id,
                                    'name'=>$contact->contact_name,
                                    'relationship'=>$contact->relationship,
                                    'address'=>$contact->contact_address,
                                    'phone'=>$contact->contact_phone,
                                    'email'=>$contact->contact_email,
                                    'main_contact'=>$contact->main_contact
                                ];
                            }
                            $this->Employees_model->insert_employees_contact($employee_contacts_array);
                        }

                        $employee_contract_array = [
                            "companies_id"=>$this->employee_contract_data->employee_id,
                            "internal_code"=>$this->employee_contract_data->internal_code,
                            "contract_type"=>$this->employee_contract_data->contract_type,
                            "employee_type"=>$this->employee_contract_data->employee_type,
                            "creation_date"=>$this->employee_contract_data->creation_date,
                            "start_date"=>$this->employee_contract_data->start_date,
                            "settlement_date"=>$this->employee_contract_data->settlement_date,
                            "workday"=>$this->employee_contract_data->workday,
                            "biller_id"=>$this->employee_contract_data->biller_id,
                            "area"=>$this->employee_contract_data->area,
                            "professional_position"=>$this->employee_contract_data->professional_position,
                            "retired_risk"=>$this->employee_contract_data->retired_risk,
                            "contract_status"=>$this->employee_contract_data->contract_status,
                            "base_amount"=>$this->employee_contract_data->base_amount,
                            "integral_salary"=>$this->employee_contract_data->integral_salary,
                            "trans_allowance"=>$this->employee_contract_data->trans_allowance,
                            "payment_frequency"=>$this->employee_contract_data->payment_frequency,
                            "withholding_method"=>$this->employee_contract_data->withholding_method,
                            "payment_method"=>$this->employee_contract_data->payment_method,
                            "afp_id"=>$this->employee_contract_data->afp,
                            "eps_id"=>$this->employee_contract_data->eps,
                            "arl_id"=>$this->employee_contract_data->arl,
                            "caja_id"=>$this->employee_contract_data->caja,
                            "icbf_id"=>$this->employee_contract_data->icbf,
                            "sena_id"=>$this->employee_contract_data->sena,
                            "cesantia_id"=>$this->employee_contract_data->cesantia,
                            'created_by'=>$this->employee_contract_data->created_by
                        ];
                        $this->Payroll_contracts_model->insert($employee_contract_array);

                        return [
                            'status' => 0,
                            'message' => lang('eliminated_not_employee')
                        ];
                    }
                } else {
                    if (!empty($employee_additional_data)) {
                        $data = [
                            'companie_id'=>$employee_additional_data->companies_id,
                            'home_phone'=>$employee_additional_data->home_phone,
                            'marital_status'=>$employee_additional_data->marital_status,
                            'blood_type'=>$employee_additional_data->blood_type,
                            'military_card'=>$employee_additional_data->military_card,
                            'birthdate'=>$employee_additional_data->birthdate,
                            'educational_level'=>$employee_additional_data->educational_level,
                            'profession'=>$employee_additional_data->profession,
                            'created_by'=>$employee_additional_data->created_by
                        ];
                        $this->Employees_model->insert_employees_data($data);
                    }

                    if (!empty($employee_contacts_data)) {
                        foreach ($employee_contacts_data as $key => $contact) {
                            $employee_contacts_array[] = [
                                'companies_id'=>$contact->companies_id,
                                'name'=>$contact->contact_name,
                                'relationship'=>$contact->relationship,
                                'address'=>$contact->contact_address,
                                'phone'=>$contact->contact_phone,
                                'email'=>$contact->contact_email,
                                'main_contact'=>$contact->main_contact
                            ];
                        }
                        $this->Employees_model->insert_employees_contact($employee_contacts_array);
                    }

                    return [
                        'status' => 0,
                        'message' => lang('eliminated_not_employee')
                    ];
                }
            } else {
                if (!empty($employee_contacts_data)) {
                    foreach ($employee_contacts_data as $key => $contact) {
                        $employee_contacts_array[] = [
                            'companies_id'=>$contact->companies_id,
                            'name'=>$contact->contact_name,
                            'relationship'=>$contact->relationship,
                            'address'=>$contact->contact_address,
                            'phone'=>$contact->contact_phone,
                            'email'=>$contact->contact_email,
                            'main_contact'=>$contact->main_contact
                        ];
                    }
                    $this->Employees_model->insert_employees_contact($employee_contacts_array);
                }

                return [
                    'status' => 0,
                    'message' => lang('eliminated_not_employee')
                ];
            }
        } else {
            return [
                'status' => 0,
                'message' => lang('eliminated_not_employee')
            ];
        }
    }

    private function set_user_log($array_data, $update_array_data)
    {
        $diff = array_diff_assoc((array) $update_array_data, (array) $array_data);

        if (!empty($diff)) {
            $description_changes_detected = '';
            $documentsTypes = $this->Employees_model->get_documenttypes();
            $status = [ACTIVE=>lang('active'), INACTIVE=>lang('inactive')];
            $description = 'El usuario '. $this->session->userdata('first_name') .' '. $this->session->userdata('last_name') .' hizo lo siguiente cambios en la edición de empleados: ';

            foreach ($diff as $field_name => $field_data) {
                $data_from = '';
                $data_to = '';

                if ($field_name == 'status') {
                    $data_from = $status[$array_data->$field_name];
                    $data_to = $status[$field_data];
                } else if ($field_name == 'tipo_documento') {
                    foreach ($documentsTypes as $documentType) {
                        if ($documentType->id == $update_array_data->$field_name) {
                            $data_from = $documentType->nombre;
                        }
                        if ($documentType->id == $field_data) {
                            $data_to = $documentType->nombre;
                        }
                    }
                } else if ($field_name == 'gender') {
                    $data_from = lang('genders')[$array_data->$field_name];
                    $data_to = lang('genders')[$field_data];
                } else  if ($field_name == 'blood_type') {
                    $data_from = lang('blood_types')[$array_data->$field_name];
                    $data_to = lang('blood_types')[$field_data];
                } else if ($field_name == 'educational_level') {
                    $data_from = lang('educational_levels')[$array_data->$field_name];
                    $data_to = lang('educational_levels')[$field_data];
                } else if ($field_name == 'marital_status') {
                    $data_from = lang('marital_statuses')[$array_data->$field_name];
                    $data_to = lang('marital_statuses')[$field_data];
                } else {
                    $data_from = $array_data->$field_name;
                    $data_to = $field_data;
                }

                $description_changes_detected .= 'Campo: '. lang($field_name). ' de "'. $data_from .'" a "'. $data_to. '", ';
            }

            $log_user_data = [
                'date'=>date('Y-m-d H:m:i'),
                'type_id'=>EDITION,
                'user_id'=>$this->session->userdata('user_id'),
                'module_name'=>'employee',
                'table_name'=>'companies, employee_data',
                'record_id'=>$array_data->id,
                'description'=>trim($description . $description_changes_detected, ', ')
            ];
            $this->UserActivities_model->insert($log_user_data);
        }
        /*if ($table_name == "employee_contacts") {
            foreach ($diff as $field_name => $field_data) {
                if ($field_name == 'name') {
                    foreach ($field_data as $contact_field_name => $contact_field_data) {
                        $employee_contact_data = (array) $employee_contacts_data[$contact_field_name];

                        if ($employee_contact_data['name'] != $contact_field_data) {
                            $data_from = $employee_contact_data['name'];
                            $data_to = $contact_field_data;
                        }
                    }
                }

                if ($field_name == 'relationship') {
                    foreach ($field_data as $contact_field_name => $contact_field_data) {
                        $employee_contact_data = (array) $employee_contacts_data[$contact_field_name];

                        if ($employee_contact_data['relationship'] != $contact_field_data) {
                            $data_from = lang('relations')[$employee_contact_data['relationship']];
                            $data_to = lang('relations')[$contact_field_data];
                        }
                    }
                }

                if ($field_name == 'phone') {
                    foreach ($field_data as $contact_field_name => $contact_field_data) {
                        $employee_contact_data = (array) $employee_contacts_data[$contact_field_name];

                        if ($employee_contact_data['phone'] != $contact_field_data) {
                            $data_from = $employee_contact_data['phone'];
                            $data_to = $contact_field_data;
                        }
                    }
                }

                if ($field_name == 'address') {
                    foreach ($field_data as $contact_field_name => $contact_field_data) {
                        $employee_contact_data = (array) $employee_contacts_data[$contact_field_name];

                        if ($employee_contact_data['address'] != $contact_field_data) {
                            $data_from = $employee_contact_data['address'];
                            $data_to = $contact_field_data;
                        }
                    }
                }

                if ($field_name == 'email') {
                    foreach ($field_data as $contact_field_name => $contact_field_data) {
                        $employee_contact_data = (array) $employee_contacts_data[$contact_field_name];

                        if ($employee_contact_data['email'] != $contact_field_data) {
                            $data_from = $employee_contact_data['email'];
                            $data_to = $contact_field_data;
                        }
                    }
                }

                if ($field_name == 'main_contact') {
                    $contact_data = (array) $employee_contacts_data[$field_data];
                    if ($contact_data['main_contact'] == INACTIVE) {
                        $description .= 'Se cambió el contacto principal';
                    }
                }
            }
        }*/
    }

    private function set_remove_user_log($employee_id, $employee_data)
    {
        $log_user_data = [
            'date'=>date('Y-m-d H:m:i'),
            'type_id'=>ELIMINATION,
            'user_id'=>$employee_id,
            'module_name'=>'employee',
            'description'=>'Se eliminó el usuario '. $employee_data->name
        ];
        $this->UserActivities_model->insert($log_user_data);
    }

    public function getCustomerSelect2($id = NULL)
    {
        $employee = $this->Employees_model->get_by_id($id);
        $name = $employee->vat_no." - ".$employee->company;
        $this->sma->send_json([['id' => $employee->id, 'text' => $name, 'value' => $name]]);
    }

    public function suggestions($term = NULL, $limit = NULL, $a = NULL)
    {
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }

        if (strlen($term) < 1) {
            return FALSE;
        }

        $limit = $this->input->get('limit', TRUE);
        $result = $this->Employees_model->getEmployeeSuggestions($term, $limit);

        if ($a) {
            $this->sma->send_json($result);
        }

        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }
}

/* End of file Employees.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/controllers/admin/Employees.php */