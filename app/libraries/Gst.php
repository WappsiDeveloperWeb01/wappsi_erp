<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
 *  ==============================================================================
 *  Author    : Mian Saleem
 *  Email     : saleem@tecdiary.com
 *  For       : Stock Manager Advance
 *  Web       : http://tecdiary.com
 *  ==============================================================================
 */

class Gst
{

    public function __construct() {
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    function summary($rows = [], $return_rows = [], $product_tax = 0, $onCost = false, $inv_currency = null, $tax_indicator = 0, $plain_summary = false, $value_decimals = null, $qty_decimals = null) {
        if ($value_decimals === null) {
            $value_decimals = $this->Settings->decimals;
        }
        if ($qty_decimals === null) {
            $qty_decimals = $this->Settings->decimals;
        }
        $code = '';

        if ($inv_currency == null) {
            $currency = $this->Settings->default_currency;
            $currency_rate = 1;
        } else {
            $currency = $inv_currency->currency;
            $currency_rate = $inv_currency->rate;
        }

        if ($this->Settings->invoice_view > 0 && !empty($rows)) {
            $tax_summary = $this->taxSummary($rows, $onCost);
            if (!empty($return_rows)) {
                $return_tax_summary = $this->taxSummary($return_rows, $onCost);
                $tax_summary = $tax_summary + $return_tax_summary;
            }
            $code = $this->genHTML($tax_summary, $product_tax, $currency, $currency_rate, $tax_indicator, $plain_summary, $value_decimals, $qty_decimals);
        }
        return $code;
    }

    function taxSummary($rows = [], $onCost = false) {
        $tax_summary = [];
        if (!empty($rows)) {
            foreach ($rows as $row) {
                if (isset($tax_summary[$row->tax_code])) {
                    $tax_summary[$row->tax_code]['items'] += $row->quantity;
                    $tax_summary[$row->tax_code]['tax'] += $row->item_tax;
                    $tax_summary[$row->tax_code]['amt'] += ($row->quantity * $this->sma->formatDecimal($onCost ? $row->net_unit_cost : $row->net_unit_price));
                } else {
                    $tax_summary[$row->tax_code]['items'] = $row->quantity;
                    $tax_summary[$row->tax_code]['tax'] = $row->item_tax;
                    $tax_summary[$row->tax_code]['amt'] = ($row->quantity * $this->sma->formatDecimal($onCost ? $row->net_unit_cost : $row->net_unit_price));
                    $tax_summary[$row->tax_code]['name'] = $row->tax_name;
                    $tax_summary[$row->tax_code]['tax_indicator'] = isset($row->tax_indicator) ? $row->tax_indicator : '';
                    $tax_summary[$row->tax_code]['code'] = $row->tax_code;
                    $tax_summary[$row->tax_code]['rate'] = $row->tax_rate;
                    $tax_summary[$row->tax_code]['type'] = 1;
                }

                if ($row->item_tax_2 != 0) {
                    if (isset($tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')])) {
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['items'] += $row->quantity;
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['tax'] += $row->item_tax_2;
                    } else {
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['items'] = $row->quantity;
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['tax'] = $row->item_tax_2;
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['amt'] = 0;
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['name'] = (isset($row->tax_indicator2ic) ? $row->tax_indicator2ic : 'ICO');
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['code'] = $row->tax_rate_2_id;
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['rate'] = $row->tax_rate_2_id;
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['tax_indicator'] = '';
                        $tax_summary[(isset($row->tax_code2ic) ? $row->tax_code2ic : 'ICO')]['type'] = 1;
                    }
                }
            }
        }
        return $tax_summary;
    }

    function genHTML($tax_summary = [], $product_tax = 0, $currency = null, $currency_rate = null, $tax_indicator = null, $plain_summary = null, $value_decimals = null, $qty_decimals = null) {
        $tax_summary_2 = $tax_summary;
        $html = '';
        $total_base = 0;
        $product_tax = 0;
        if (!empty($tax_summary)) {
            $html .= '<div><p style="font-weight:bold;" class="text-center">' . lang('tax_summary') . '</p></div>';
            $html .= '<table class="'.($plain_summary ? '' : 'table print-table order-table table-condensed' ).'" '.($plain_summary ? 'style="width:100%;"' : '' ).'>
            <thead>
                <tr>
                    <th class="text-center">' . lang('summary_tax_rate') . '</th>
                    '.($plain_summary === true ? '<th class="text-center">' . lang('quantity') . '</th>' : '').'
                    <th class="text-center">' . lang('summary_tax_base') . '</th>
                    <th class="text-center">' . lang('summary_tax_amount') . '</th>
                </tr>
            </thead>
            <tbody>';
            $exists_second_tax = false;
            foreach ($tax_summary as $summary) {

                if ($summary['type'] == 1) {
                    $total_base+=$summary['amt'];
                    $style_base = '';
                } else {
                    if ($summary['tax'] > 0) {
                        $exists_second_tax = true;
                    }
                    continue;
                }
                $product_tax +=  $summary['tax'];
                $html .= '<tr>
                            <td '.($plain_summary === 2 ? 'class="text-center"' : '').'>' . $summary['name'] . ($tax_indicator ? ' ('.$summary['tax_indicator'].')' : '') . '</td>
                            '.($plain_summary === true ? '<td style="text-align:right;">' . $this->sma->formatValue($qty_decimals, $summary['items']). '</td>' : '').'
                            <td  '.($plain_summary === 2 ? 'class="text-center"' : 'style="text-align:right;"').'>' . ($currency_rate != 1 ? $this->sma->formatValue($value_decimals, $currency_rate * $summary['amt']) :  $this->sma->formatValue($value_decimals, $summary['amt'])) . '</td>
                            <td  '.($plain_summary === 2 ? 'class="text-center"' : 'style="text-align:right;"').'>' . $this->sma->formatValue($value_decimals,  $currency_rate * $summary['tax']) . '</td>
                        </tr>';

            }
            $html .= '</tbody><tfoot>';
                $html .= '<tr class="active">
                            <th '.($plain_summary === true ? 'colspan="2"' : '').'>' . lang('summary_total_tax_amount') . '</th>
                            <th  '.($plain_summary === 2 ? 'class="text-center"' : 'style="text-align:right;"').'>' . ($currency_rate != 1 ? $this->sma->formatValue($value_decimals, $currency_rate * $total_base) : $this->sma->formatValue($value_decimals, $total_base)). '</th>
                            <th  '.($plain_summary === 2 ? 'class="text-center"' : 'style="text-align:right;"').'>' . ($currency_rate != 1 ? $this->sma->formatValue($value_decimals, $currency_rate * $product_tax) : $this->sma->formatValue($value_decimals, $product_tax)) . '</th>
                          </tr>';
            $html .= '</tfoot></table>';
            $html .= '</table>';


            if ($exists_second_tax) {
                $html .= '<table class="table print-table order-table table-condensed">
                <thead>
                    <tr>
                        <th class="text-center">' . lang('summary_tax_rate') . '</th>
                        <th class="text-center">' . lang('summary_tax_base') . '</th>
                        <th class="text-center">' . lang('summary_tax_amount') . '</th>
                    </tr>
                </thead>
                <tbody>';


                $total_base = 0;
                $product_tax = 0;

                foreach ($tax_summary_2 as $summary) {
                    if ($summary['type'] == 2) {
                        $total_base+=$summary['amt'];
                        $style_base = '';
                        // echo var_dump($summary);
                    } else {
                        continue;
                    }
                    $product_tax += $currency_rate * $summary['tax'];
                        $html .= '<tr>
                                    <td>' . $summary['name'] . '</td>
                                    <td style="text-align:right;">' . $this->sma->formatValue($value_decimals, $currency_rate * $summary['amt']) . '</td>
                                    <td class="text-right">' . $this->sma->formatValue($value_decimals,  $currency_rate * $summary['tax']) . '</td>
                                </tr>';

                }
                $html .= '</tbody><tfoot>';
                $html .= '<tr class="active">
                            <th style="text-align:right;">' . lang('summary_total_tax_amount') . '</th>
                            <th class="text-right">' . ($currency_rate != 1 ? $this->sma->formatValue($value_decimals, $currency_rate * $total_base) : $this->sma->formatValue($value_decimals, $total_base)). '</th>
                            <th class="text-right">' . ($currency_rate != 1 ? $this->sma->formatValue($value_decimals, $currency_rate * $product_tax) : $this->sma->formatValue($value_decimals, $product_tax)) . '</th>
                          </tr>';
                $html .= '</tfoot></table>';
                $html .= '</table>';
            }

        }
        return $html;
    }

    function calculteIndianGST($item_tax, $state, $tax_details) {
        if ($this->Settings->indian_gst) {
            $cgst = $sgst = $igst = 0;
            if ($state) {
                $gst = $tax_details->type == 1 ? $this->sma->formatDecimal(($tax_details->rate/2), 0).'%' : $this->sma->formatDecimal(($tax_details->rate/2), 0);
                $cgst = $this->sma->formatDecimal(($item_tax / 2), 4);
                $sgst = $this->sma->formatDecimal(($item_tax / 2), 4);
            } else {
                $gst = $tax_details->type == 1 ? $this->sma->formatDecimal(($tax_details->rate), 0).'%' : $this->sma->formatDecimal(($tax_details->rate), 0);
                $igst = $item_tax;
            }
            return ['gst' => $gst, 'cgst' => $cgst, 'sgst' => $sgst, 'igst' => $igst];
        }
        return [];
    }

    function getIndianStates($blank = false) {
        $istates  = [
            'AN' => 'Andaman & Nicobar',
            'AP' => 'Andhra Pradesh',
            'AR' => 'Arunachal Pradesh',
            'AS' => 'Assam',
            'BR' => 'Bihar',
            'CH' => 'Chandigarh',
            'CT' => 'Chhattisgarh',
            'DN' => 'Dadra and Nagar Haveli',
            'DD' => 'Daman & Diu',
            'DL' => 'Delhi',
            'GA' => 'Goa',
            'GJ' => 'Gujarat',
            'HR' => 'Haryana',
            'HP' => 'Himachal Pradesh',
            'JK' => 'Jammu & Kashmir',
            'JH' => 'Jharkhand',
            'KA' => 'Karnataka',
            'KL' => 'Kerala',
            'LD' => 'Lakshadweep',
            'MP' => 'Madhya Pradesh',
            'MH' => 'Maharashtra',
            'MN' => 'Manipur',
            'ML' => 'Meghalaya',
            'MZ' => 'Mizoram',
            'NL' => 'Nagaland',
            'OR' => 'Odisha',
            'PY' => 'Puducherry',
            'PB' => 'Punjab',
            'RJ' => 'Rajasthan',
            'SK' => 'Sikkim',
            'TN' => 'Tamil Nadu',
            'TS' => 'Telangana',
            'TR' => 'Tripura',
            'UK' => 'Uttarakhand',
            'UP' => 'Uttar Pradesh',
            'WB' => 'West Bengal',
        ];
        if ($blank) {
            array_unshift($istates, lang('select'));
        }
        return $istates;
    }



}
