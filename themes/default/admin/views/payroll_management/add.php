<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
    $months = [1=>"Enero", 2=>"Febrero", 3=>"Marzo", 4=>"Abril", 5=>"Mayo", 6=>"Junio", 7=>"Julio", 8=>"Agosto", 9=>"Septiembre", 10=>"Octubre", 11=>"Noviembre", 12=>"Diciembre"];
    $biweeklys = [FIRST_FORTNIGHT=>lang("biweekly_one"), SECOND_FORTNIGHT=>lang("biweekly_two")];

    if (!empty($payroll)) {
        $payrollYear = $payroll->year;
        $payrollMonth = $payroll->month;
        $payrollNumber = $payroll->number;

        if ($payrollMonth == 12 && $payrollNumber == SECOND_FORTNIGHT) {
            $payrollYear = date ('Y', strtotime ('+1 year' , strtotime($payrollYear."-".$payrollMonth)));
            $payrollMonth = date ('m', strtotime ('+1 month' , strtotime($payrollYear."-".$payrollMonth)));
        }
    } else {
        $payrollMonth = date("n");
        $payrollYear = date("Y");
    }
?>
<div class="modal-dialog" id="add_payroll_modal">
    <div class="modal-content">
        <?= admin_form_open("payroll_management/save", ["id"=>"add_type_concept_form"]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-lg">&times;</i></button>
                <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?></h3>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("year"), "year"); ?>
                            <?php
                                $anioActual = date('Y');
                                $anioAnterior = date ('Y', strtotime ('-1 year' , strtotime($anioActual)));
                                $anioSiguiente = date ('Y', strtotime ('+1 year' , strtotime($anioActual)));
                                $yopts = [
                                    $anioAnterior => $anioAnterior,
                                    $anioActual => $anioActual,
                                    $anioSiguiente => $anioSiguiente
                                ];
                            ?>
                            <?= form_dropdown(["name"=>"year", "id"=>"year", "class"=>"form-control select2", "required"=>TRUE], $yopts, $payrollYear); ?>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("payroll_management_month"), "month"); ?>
                            <?= form_dropdown(["name"=>"month", "id"=>"month", "class"=>"form-control select2", "required"=>TRUE], $months, $payrollMonth); ?>
                        </div>
                    </div>

                    <?php if ($this->Payroll_settings->payment_frequency == MONTHLY) { ?>
                        <?= form_hidden('number', 1); ?>
                    <?php } else if ($this->Payroll_settings->payment_frequency == BIWEEKLY) { ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?= form_label(lang("payroll_management_biweekly", "number")); ?>
                                <?php
                                    $bopts = [];
                                    foreach ($biweeklys as $key => $biweekly) {
                                        $bopts[$key] = $biweekly;
                                    }
                                ?>
                                <?php $number = isset($payroll) && !empty($payroll) ? (($payroll->number + 1) == SECOND_FORTNIGHT ? SECOND_FORTNIGHT : FIRST_FORTNIGHT) : ''; ?>
                                <?= form_dropdown(["name"=>"number", "id"=>"number", "class"=>"form-control select2", "required"=>TRUE], $bopts, $number); ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <?= form_button(["name"=>"save_button", "id"=>"save_button", "class"=>"btn btn-primary new-button", "type"=>"submit", "title"=>lang("create")], "<i class='fa fa-check'></i>"); ?>
                <?= form_button(["class"=>"btn btn-default new-button", "data-dismiss"=>"modal", "data-toggle"=>"tooltip", "title"=>lang("cancel")], "<i class='fas fa-times'></i>"); ?>
            </div>
        <?= form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        <?php if (!empty($payroll) == TRUE) { ?>
            if (<?= $payroll->status ?> == <?= IN_PREPARATION ?>) {
                $('#myModal').modal('hide');
                Command:toastr.error('No es posible agregar una nueva nómina. Es necesario <strong>Aprobar</strong> la actual nómina en <strong>Proceso</strong>', '<?= lang('toast_error_title'); ?>')
            }
        <?php } ?>

        $('.select2').select2();

        <?php if ($this->Payroll_settings->payment_frequency == BIWEEKLY) { ?>
            $('#month').trigger('change');
        <?php } else if ($this->Payroll_settings->payment_frequency == WEEKLY) { ?>
            $(document).on('change', '#month', function() { get_weeks_month(); });
            $(document).on('change', '#number', function() { get_dates_week(); });

            $('#month').trigger('change');
            $('#number').trigger('change');
        <?php } ?>

        $(document).on('click', "#save_button", function() { $('#add_payroll_modal').modal('hide'); creating_payroll(); });

        $('[data-toggle="tooltip"]').tooltip();
    });

    function get_weeks_month()
    {
        $.ajax({
            url: '<?= admin_url("payroll_management/get_weeks_month"); ?>',
            type: 'GET',
            dataType: 'HTML',
            data: {
                month: $('#month').val(),
                number: '<?= (isset($payroll) && !empty($payroll)) ? $payroll->number : 0; ?>'
            },
        })
        .done(function(data) {
            $("#number").html(data);

            $('#number').trigger('change');

            validate_current_week();
        })
        .fail(function(data) {
            command: toastr.error('<?= lang("ajax_error") ?>', '<?= lang("toast_error_title") ?>', { onHidden: function() { console.log(data.responseText); } });
        });
    }

    function validate_current_week() {
        var month = $('#month').val();
        var current_day = '<?= date("Y-m-d"); ?>';

        if (month == '<?= date("n") ?>') {
            $('#number option').each(function(index, el) {
                var end_date = $(this).attr('end_date');

                if (Date.parse(current_day) <= Date.parse(end_date)) {
                    $(this).attr('disabled', true);
                }
            });
        }
    }

    function get_dates_week()
    {
        $('input[name="start_date"]').val($('#number option:selected').attr('start_date'));
        $('input[name="end_date"]').val($('#number option:selected').attr('end_date'));
    }

    function creating_payroll()
    {
        swal({
            title: 'Creando nómina.',
            text: 'Por favor, espere un momento',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });
    }
</script>
