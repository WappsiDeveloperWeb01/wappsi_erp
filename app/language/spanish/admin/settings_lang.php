<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */

    /* Site configuration */
$lang['site_config']                            = "Parámetros generales";
$lang['site_name']                              = "Nombre empresa";
$lang['default_currency']                       = "Moneda por defecto";
$lang['accounting_method']                      = "Método de Costeo";
$lang['default_email']                          = "Correo por defecto";
$lang['default_customer_group']                 = "Grupo de clientes por defecto";
$lang['maintenance_mode']                       = "Modo Mantenimiento";
$lang['theme']                                  = "Tema";
$lang['login_captcha']                          = "Captcha en Login";
$lang['rows_per_page']                          = "Filas por Página";
$lang['dateformat']                             = "Formato de Fecha";
$lang['timezone']                               = "Zona horaria";
$lang['reg_ver']                                = "Verificación del registro";
$lang['allow_reg']                              = "Permitir registro";
$lang['reg_notification']                       = "Notificación del registro";
$lang['calendar']                               = "Calendario";
$lang['private']                                = "Privado";
$lang['shared']                                 = "Compartido";
$lang['default_warehouse']                      = "Bodega por defecto";
$lang['restrict_user']                          = "Restringir Usuario";

    /* Products */
$lang['product_tax']                            = "Impuesto del producto";
$lang['racks']                                  = "Manejo de estantes";
// $lang['product_variants']                       = "Manejo de variantes";
$lang['product_expiry']                         = "Vencimiento de producto";
$lang['image_size']                             = "Tamaño de la imagen";
$lang['thumbnail_size']                         = "Tamaño de imagenes pequeñas";
$lang['watermark']                              = "Marca de agua";

    /* Ventas */
$lang['over_selling']                           = "Manejo de sobreventas";
$lang['reference_format']                       = "Formato de numeración";
$lang['prefix_month_year_no']                   = "Prefijo/Año/Mes/Secuencia (SL/2015/08/001)";
$lang['prefix_year_no']                         = "Prefijo/Año/Secuencia (SL/2015/001)";
$lang['product_level_discount']                 = "Manejo de descuento en producto";
$lang['bc_fix']                                 = "Digitos para validar código de barras";
$lang['item_addition']                          = "Método para agregar productos";
$lang['add_new_item']                           = "Agregar nuevo producto en la venta";
$lang['increase_quantity_if_item_exist']        = "Incrementar la cantidad del artículo, si ya existe en la venta";
$lang['invoice_view']                           = "Ver factura";

    /* Prefijos */
$lang['prefix']                                 = "Prefijos de documentos";
$lang['sales_prefix']                           = "Ventas";
$lang['return_prefix']                          = "Nota crédito de ventas";
$lang['payment_prefix']                         = "Pagos";
$lang['delivery_prefix']                        = "Orden de despacho";
$lang['quote_prefix']                           = "Cotización";
$lang['quote_purchase_prefix']                  = "Orden de compra";
$lang['order_sale_prefix']                      = "Orden de pedido";
$lang['purchase_prefix']                        = "Compras";
$lang['transfer_prefix']                        = "Traslados";

    /* Money and Number Format */
$lang['money_number_format']                    = "Formato de Decimales y Moneda";
$lang['decimals']                               = "Decimales";
$lang['decimals_sep']                           = "Separador de Decimales";
$lang['thousands_sep']                          = "Separador de Miles";
$lang['dot']                                    = "Punto";
$lang['comma']                                  = "Coma";
$lang['space']                                  = "Espacio";

    /* Email */
$lang['email_protocol']                         = "Protocolo de Correo";
$lang['smtp_host']                              = "Host SMTP";
$lang['smtp_user']                              = "Usuario SMTP";
$lang['smtp_pass']                              = "Contraseña SMTP";
$lang['smtp_port']                              = "Puerto SMTP";

$lang['update_settings']                        = "Actualizar parámetros";
$lang['setting_updated']                        = "Parámetros actualizados correctamente";

    /* Others */
$lang['image_width']                            = "Ancho de la imagen";
$lang['image_height']                           = "Alto de la imagen";
$lang['thumbnail_width']                        = "Ancho de la miniatura";
$lang['thumbnail_height']                       = "Alto de la miniatura";
$lang['invoice_tax']                            = "Impuesto de la orden";
$lang['detect_barcode']                         = "Detectar código de barras";
$lang['product_serial']                         = "Seriales de producto";
$lang['product_discount']                       = "Descuento de Producto";
$lang['overselling_will_only_work_with_AVCO_accounting_method_only']= "El método de sobreventas solo funciona con Costo promedio, y se ha ajustado";
$lang['accounting_method_change_alert']         = "Está a punto de cambier el método de costeo y esto complicará sus cuentas antiguas. Le sujerimos hacer el cambio solo el primer día del mes.";
$lang['logo_uploaded']                          = "Logo cargado correctamente";
$lang['auto_detect_barcode']                    = "Detectar automáticamente el código de barras";

    /* Change Logo */
$lang['site_logo']                              = "Logo de empresa";
$lang['biller_logo']                            = "Logo de sucursal";
$lang['upload_logo']                            = "Actualizar logo";

    /* Currencies */
$lang['add_currency']                           = "Agregar Moneda";
$lang['edit_currency']                          = "Agregar Moneda";
$lang['delete_currency']                        = "Eliminar Moneda";
$lang['delete_currencies']                      = "Eliminar Monedas";
$lang['currency_code']                          = "Código de la moneda";
$lang['currency_name']                          = "Nombre de la moneda";
$lang['exchange_rate']                          = "Tasa de cambio";
$lang['new_currency']                           = "Agregar moneda";
$lang['currency_added']                         = "Moneda agregada correctamente";
$lang['currency_updated']                       = "Moneda actualizada correctamente";
$lang['currency_deleted']                       = "Moneda eliminada correctamente";
$lang['currencies_deleted']                     = "Monedas eliminadas correctamente";

    /* Customer Groups */
$lang['add_customer_group']                     = "Agregar Grupo de clientes";
$lang['edit_customer_group']                    = "Editar Grupo de clientes";
$lang['delete_customer_group']                  = "Eliminar Grupo de clientes";
$lang['delete_customer_groups']                 = "Eliminar Grupos de clientes";
$lang['percentage']                             = "Porcentaje";
$lang['group_name']                             = "Nombre del grupo";
$lang['group_percentage']                       = "Porcentaje para el Grupo (sin el signo %)";
$lang['customer_group_added']                   = "Grupo de clientes agregado correctamente";
$lang['customer_group_updated']                 = "Grupo de clientes actualizado  correctamente";
$lang['customer_group_deleted']                 = "Grupo de clientes eliminado correctamente";
$lang['customer_groups_deleted']                = "Grupos de clientes eliminados  correctamente";

    /* Categories */
$lang['add_category']                           = "Agregar %s";
$lang['edit_category']                          = "Editar %s";
$lang['delete_category']                        = "Eliminar %s";
$lang['delete_categories']                      = "Eliminar %s";
$lang['category_code']                          = "Código de la categoría";
$lang['category_name']                          = "Nombre de la categoría";
$lang['category_added']                         = "Categoría agregada correctamente";
$lang['category_updated']                       = "Categoría actualizada correctamente";
$lang['category_deleted']                       = "Categoría eliminada correctamente";
$lang['categories_deleted']                     = "Categorías eliminadas correctamente";
$lang['category_image']                         = "Imagen de la categoría";
$lang['list_subcategories']                     = "Lista de subcategorías";
$lang['add_subcategory']                        = "Agregar subcategoría";
$lang['edit_subcategory']                       = "Editar subcategoría";
$lang['delete_subcategory']                     = "Eliminar subcategoría";
$lang['delete_subcategories']                   = "Eliminar subcategorías";
$lang['main_category']                          = "Categoría principal";
$lang['subcategory_code']                       = "Código de la subcategoría";
$lang['subcategory_name']                       = "Nombre de la subcategoría";
$lang['subcategory_added']                      = "Subcategoría agregada correctamente";
$lang['subcategory_updated']                    = "Subcategoría actualizada correctamente";
$lang['subcategory_deleted']                    = "Subcategoría eliminada correctamente";
$lang['subcategories_deleted']                  = "Subcategorías eliminadas correctamente";

    /* Tax Rates */
$lang['tax_rate']                               = "Tasa de Impuesto";
$lang['add_tax_rate']                           = "Agregar Tasa de impuesto";
$lang['edit_tax_rate']                          = "Editar Tasa de impuesto";
$lang['delete_tax_rate']                        = "Eliminar Tasa de impuesto";
$lang['delete_tax_rates']                       = "Eliminar Tasa de impuestos";
$lang['fixed']                                  = "Fijo";
$lang['type']                                   = "Tipo";
$lang['rate']                                   = "Tasa";
$lang['tax_rate_added']                         = "Tasa de impuesto agregada  correctamente";
$lang['tax_rate_updated']                       = "Tasa de impuesto actualizada correctamente";
$lang['tax_rate_deleted']                       = "Tasa de Impuesto eliminada  correctamente";
$lang['tax_rates_deleted']                      = "Tasas de Impuesto eliminadas correctamente";
$lang['tax_rate_code_fe']                       = "Código facturación electrónica";

    /* Warehouse */
$lang['warehouse']                              = "Bodega";
$lang['add_warehouse']                          = "Agregar bodega";
$lang['edit_warehouse']                         = "Editar bodega";
$lang['deactivate_warehouse']                       = "Desactivar bodega";
$lang['deactivate_warehouses']                      = "Desactivar bodegas";
$lang['activate_warehouse']                       = "Activar bodega";
$lang['activate_warehouses']                      = "Activar bodegas";
$lang['code']                                   = "Código";
$lang['name']                                   = "Nombre";
$lang['map']                                    = "Mapa";
$lang['map_image']                              = "Imagen del mapa";
$lang['warehouse_map']                          = "Mapa de bodega";
$lang['warehouse_added']                        = "Bodega agregada correctamente";
$lang['warehouse_updated']                      = "Bodega actualizada correctamente";
$lang['warehouse_deleted']                      = "Bodega eliminada correctamente";
$lang['warehouses_deleted']                     = "Bodegas eliminadas correctamente";

    /* Email Templates */
$lang['mail_message']                           = "Mensaje de correo";
$lang['activate_email']                         = "Correo de activación";
$lang['forgot_password']                        = "Olvidó la contraseña";
$lang['short_tags']                             = "Etiquetas cortas";
$lang['message_successfully_saved']             = "Mensaje correctamente guardado";

$lang['group']                                  = "Perfil de usuarios";
$lang['groups']                                 = "Perfiles de usuarios";
$lang['add_group']                              = "Agregar perfil de usuarios";
$lang['create_group']                           = "Agregar perfil";
$lang['edit_group']                             = "Perfil de usuarios";
$lang['delete_group']                           = "Eliminar perfil de usuarios";
$lang['delete_groups']                          = "Eliminar perfil de usuarios";
$lang['group_id']                               = "ID de perfil";
$lang['description']                            = "Descripción";
$lang['group_description']                      = "Descripción del perfil";
$lang['change_permissions']                     = "Cambiar permisos";
$lang['group_added']                            = "Perfil de usuarios agregado  correctamente";
$lang['group_updated']                          = "Perfil de usuarios actualizado correctamente";
$lang['group_deleted']                          = "Perfil de usuarios eliminado correctamente";
$lang['groups_deleted']                         = "Perfil de usuarios eliminados correctamente";
$lang['permissions']                            = "Permisos";
$lang['set_permissions']                        = "Lista de permisos";
$lang['module_name']                            = "Nombre del Módulo";
$lang['misc']                                   = "Opciones";
$lang['pos_print_server']                       = "Servidor de impresión";

$lang['paypal']                                 = "Paypal";
$lang['paypal_settings']                        = "Ajustes de Paypal";
$lang['activate']                               = "Activar";
$lang['paypal_account_email']                   = "Correo de la cuenta de Paypal";
$lang['fixed_charges']                          = "Cargos fijos";
$lang['account_email_tip']                      = "Por favor escriba la dirección de correo de su cuenta de Paypal";
$lang['fixed_charges_tip']                      = "Cualquier cargo fijo extra para todos sus pagos a través de ésta pasarela";
$lang['extra_charges_my']                       = "Porcentaje de cargos extra para su país";
$lang['extra_charges_my_tip']                   = "Porcentaje de cargos extra para todos los pagos desde su país";
$lang['extra_charges_others']                   = "Porcentaje de cargos extra para otros países";
$lang['extra_charges_others_tip']               = "Porcentaje de cargos extra para todos los pagos desde otros países";
$lang['ipn_link']                               = "Enlace IPN";
$lang['ipn_link_tip']                           = "Añadir este enlace a su cuenta de paypal para activar este IPN";
$lang['paypal_setting_updated']                 = "Ajustes de Paypal actualizados correctamente";
$lang['skrill']                                 = "Skrill";
$lang['skrill_account_email']                   = "Correo de la cuenta de Skrill";
$lang['skrill_email_tip']                       = "Por favor escriba la cuenta de correo de su cuenta de skrill";
$lang['secret_word']                            = "Palabra Secreta";
$lang['paypal_setting_updated']                 = "Ajustes de Paypal correctamente actualizados";
$lang['skrill_settings']                        = "Ajustes de Skrill";
$lang['secret_word_tip']                        = "Por favor escriba su palabra secreta de skrill";

$lang['auto_update_rate']                       = "Auto Actualizar Tasa";
$lang['product_variants_feature_x']             = "La característica de variaciones en Producto no trabajará con ésta opción";
$lang['group_permissions_updated']              = "Perfil de usuarios correctamente actualizado";
$lang['tax_invoice']                            = "Factura de impuesto";
$lang['standard']                               = "Estandar";
$lang['logo_image_tip']                         = "El tamaño máximo del archivo es de 1024KB y (Ancho máx 300px y Altura máx 80px).";
$lang['biller_logo_tip']                        = "Por favor edite la sucursal despues de cargar el nuevo logo y seleccionar el logo actualizado.";


$lang['default_biller']                         = "Sucursal por defecto";
$lang['group_x_b_deleted']                      = "No se puede completar la acción, hay usuarios asignados a éste perfil";
$lang['profit_loss']                            = "Ganancias y/o pérdidas";
$lang['staff']                                  = "Personal";
$lang['stock_chartr']                           = "Gráfica de inventario";
$lang['rtl_support']                            = "Soporte RTL (Right to left)";
$lang['backup_on']                              = "Copia de seguridad";
$lang['restore']                                = "Restaurar copia";
$lang['download']                               = "Descargar";
$lang['file_backups']                           = "Copia de archivos";
$lang['backup_files']                           = "Hacer copia de archivos";
$lang['database_backups']                       = "Copias de seguridad";
$lang['db_saved']                               = "Copia de seguridad realizada correctamente";
$lang['db_deleted']                             = "Copia de seguridad eliminada correctamente";
$lang['backup_deleted']                         = "Copia de seguridad eliminada correctamente";
$lang['backup_saved']                           = "Copia de seguridad realizada correctamente";
$lang['backup_modal_heading']                   = "Haciendo copia de seguridad";
$lang['backup_modal_msg']                       = "Por favor espere, éste proceso podría tomar algunos minutos";
$lang['restore_modal_heading']                  = "Restaurando copia de seguridad";
$lang['restore_confirm']                        = "Esta acción no se puede deshacer, está seguro que desea restaurar la copia de seguridad?";
$lang['delete_confirm']                         = "Esta acción no se puede deshacer, está seguro que desea eliminar?";
$lang['restore_heading']                        = "Por favor realice una copia antes de actualizar";
$lang['full_backup']                            = 'Copia de seguridad completa';
$lang['database']                               = 'Base de datos';
$lang['files_restored']                         = 'Archivos restaurados correctamente';
$lang['variants']                               = "Variantes";
$lang['add_variant']                            = "Agregar %s";
$lang['edit_variant']                           = "Editar %s";
$lang['delete_variant']                         = "Eliminar variante";
$lang['delete_variants']                        = "Eliminar variantes";
$lang['variant_added']                          = "Acción de variante Completada Agregado";
$lang['variant_updated']                        = "Variante actualizada correctamente";
$lang['variant_deleted']                        = "%s eliminada correctamente";
$lang['variants_deleted']                       = "Variantes eliminadas correctamente";
$lang['customer_award_points']                  = 'Puntos premio de clientes';
$lang['staff_award_points']                     = 'Puntos premio de empleados';
$lang['each_spent']                             = 'Por cada monto de este valor';
$lang['each_in_sale']                           = 'Cada venta realizada <i class="fa fa-arrow-down"></i> in sale is equal to';
$lang['mailpath']                               = "Mail Path";
$lang['smtp_crypto']                            = "SMTP Crypto";
$lang['random_number']                          = "Random Number";
$lang['sequence_number']                        = "Número";
$lang['update_heading']                         = "";
$lang['update_successful']                      = "Articulo Actualizado";
$lang['using_latest_update']                    = "Está usando la última versión";
$lang['version']                                = "Versión";
$lang['install']                                = "Instalar";
$lang['changelog']                              = "Control de versiones";
$lang['expense_prefix']                         = "Prefijo de Gasto";
$lang['purchase_code']                          = "Código de compra";
$lang['envato_username']                        = "Usuario";

$lang['adjust_quantity']                        = "Ajustar Cantidad";
$lang['deposit_prefix']                         = "Prefijo de anticipos";
$lang['prioridad_precios_producto']             = "Política de precios de productos";

$lang['descuento_orden']                        = '¿Descuentos afectan IVA?';
$lang['descuento_orden_decidir']                = 'Escoger durante registro de compra';

/* Compatibilities */
$lang["compatibilities"] = "Compatibilidades";
$lang["compatibilities_name"] = "Nombre";
$lang["compatibilities_brand"] = "Marca";
$lang["compatibilities_full_name"] = "Nombre completo";
$lang["compatibilities_duplicate"] = "Nombre duplicado";
$lang["compatibilities_add_compatibility"] = "Agregar compatibilidad";
$lang["compatibilities_edit_compatibility"] = "Editar compatibilidad";
$lang["compatibilities_delete_compatibility"] = "Eliminar compatibilidad";
$lang["compatibilities_compatibility_added"] = "Compatibilidad agregada";
$lang["compatibilities_compatibility_update"] = "Compatibilidad actualizada";
$lang["compatibilities_compatibility_deleted"] = "Compatibilidad eliminada";

//NO TRADUCIDO

$lang['parent_category']                        = 'Categoría padre';
$lang['sac']                                    = 'Formato de moneda Asia';
$lang['qty_decimals']                           = 'Decimales en cantidad';
$lang['display_all_products']                   = 'Mostrar todos los productos';
$lang['hide_with_0_qty']                        = 'Ocultar productos con cantidad cero (0)';
$lang['show_with_0_qty']                        = 'Mostrar productos con cantidad cero (0)';
$lang['display_currency_symbol']                = 'Mostrar símbolo de moneda';
$lang['currency_symbol']                        = 'Símbolo de moneda';
$lang['after']                                  = 'Después';
$lang['before']                                 = 'Antes';
$lang['remove_expired']                         = 'Eliminar productos vencidos del inventario';
$lang['remove_automatically']                   = 'eliminar automáticamente productos vencidos';
$lang['i_ll_remove']                            = 'los eliminaré manualmente';
$lang['db_restored']                            = 'Base de datos restaurada correctamente';
$lang['bulk_actions']                           = 'Acciones globales';
$lang['barcode_separator']                      = 'Separador de código de barras';
$lang['dash']                                   = 'Guión ( - )';
$lang['tilde']                                  = 'Tilde ( ~ )';
$lang['underscore']                             = 'Guión bajo ( _ )';
$lang['deposits']                               = 'Anticipos (Ver, agregar y editar)';
$lang['delete_deposit']                         = 'Eliminar anticipo';
$lang['add_expense_category']                   = 'Agregar categoría de gastos';
$lang['edit_expense_category']                  = 'Editar categoría de gastos';
$lang['delete_expense_category']                = 'Eliminar categoría de gastos';
$lang['delete_expense_categories']              = 'Eliminar categorías de gastos';
$lang['expense_category_added']                 = 'Categoría de gastos agregada correctamente';
$lang['expense_category_updated']               = 'Categoría de gastos actualizada correctamente';
$lang['expense_category_deleted']               = 'Categoría de gastos eliminada correctamente';
$lang['category_has_expenses']                  = 'La categoría de gastos no puede ser eliminada';
$lang['returnp_prefix']                         = 'Nota débito de compra';
$lang['set_focus']                              = 'Foco por defecto';
$lang['add_item_input']                         = 'Campo agregar producto';
$lang['last_order_item']                        = 'Campo de cantidad para último producto';
$lang['import_categories']                      = 'Importar categorías';
$lang['categories_added']                       = 'Categorías importadas correctamente';
$lang['parent_category_code']                   = 'Código categoría padre';
$lang['import_subcategories']                   = 'Importar subcategorías';
$lang['subcategories_added']                    = 'Subcategorías importadas correctamente';
$lang['import_expense_categories']              = 'Importar categorías de gastos';
$lang['unit']                                   = 'Unidad';
$lang['list_subunits']                          = 'Lista de subunidades';
$lang['unit_code']                              = 'Código unidad';
$lang['unit_name']                              = 'Nombre unidad';
$lang['base_unit']                              = 'Unidad base';
$lang['operator']                               = 'Operador';
$lang['+']                                      = 'Sumar (+)';
$lang['-']                                      = 'Restar (-)';
$lang['*']                                      = 'Multiplicar (*)';
$lang['/']                                      = 'Dividir (/)';
$lang['operation_value']                        = 'Factor';
$lang['add_unit']                               = 'Agregar unidad';
$lang['edit_unit']                              = 'Editar unidad';
$lang['delete_unit']                            = 'Eliminar unidad';
$lang['delete_units']                           = 'Eliminar unidades';
$lang['unit_added']                             = 'Unidad agregada correctamente';
$lang['unit_updated']                           = 'Unidad actualizada correctamente';
$lang['unit_deleted']                           = 'Unidad eliminada correctamente';
$lang['lang_englishuage_x_found']               = 'Nose encuentran los archivos lang_englishuage, por favor revisar que se encuentre la carpeta';
$lang['default_price_group']                    = 'Lista de precios por defecto';
$lang['barcode_renderer']                       = 'Generador de código de barras';
$lang['svg'] = 'SVG';
$lang['disable_editing']                        = 'Número de días para deshabilitar la edición';
$lang['add_price_group']                        = 'Agregar lista de precios';
$lang['edit_price_group']                       = 'Editar lista de precios';
$lang['delete_price_group']                     = 'Eliminar lista de precios';
$lang['delete_price_groups']                    = 'Eliminar listas de precios';
$lang['price_group_added']                      = 'Lista de precios agregada correctamente';
$lang['price_group_updated']                    = 'Lista de precios actualizada correctamente';
$lang['price_group_deleted']                    = 'Lista de precios eliminada correctamente';
$lang['price_groups_deleted']                   = 'Listas de precios eliminadas correctamente';
$lang['no_price_group_selected']                = 'No se ha seleccionado una lista de precios';
$lang['no_customer_group_selected']             = 'No se ha seleccionado grupo de clientes';
$lang['group_product_prices']                   = 'Lista de precios';
$lang['update_prices_csv']                      = 'Actualizar precios por archivo CSV';
$lang['delete_product_group_prices']            = 'Borrar producto de la lista de precios';
$lang['products_group_price_updated']           = 'Productos en lista de precios actualizados correctamente';
$lang['products_group_price_deleted']           = 'Productos en lista de precios eliminados correctamente';
$lang['ppayment_prefix']                        = 'Pagos en compras';
$lang['add_brand']                              = 'Agregar %s';
$lang['edit_brand']                             = 'Editar %s';
$lang['delete_brand']                           = 'Eliminar %s';
$lang['delete_brands']                          = 'Eliminar %s';
$lang['import_brands']                          = 'Importar marcas';
$lang['brand_added']                            = 'Marca agregada correctamente';
$lang['brands_added']                           = 'Marcas agregadas correctamente';
$lang['brand_updated']                          = 'Marca actualizada correctamente';
$lang['brand_deleted']                          = 'Marca eliminada correctamente';
$lang['brands_deleted']                         = 'Marcas eliminadas correctamente';
$lang['code_x_exist']                           = 'Código de producto no existe';
$lang['price_updated']                          = 'Precio de producto actualizado correctamente';
$lang['qa_prefix']                              = 'Ajuste de cantidad';
$lang['categories_import_tip']                  = 'Si el archivo CSV tiene subcategorías (Categorías hijo - Categorías con código de categoría padre), las categorías padre serán importadas primero. Luego deberá importar el mismo archivo de nuevo para cargar las subcategorías. (Categorías hijo)';
$lang['update_cost_with_purchase']              = 'Actualizar el costo con la compra';
$lang['edit_price_on_sale']                     = 'Editar el precio en la venta';
$lang['login_logo']                             = 'Logo del login';
$lang['unit_has_subunit']                       = 'La unidad no puede ser eliminada porque tiene subunidades';
$lang['brand_has_products']                     = 'La marca no puede ser eliminada pues tiene productos con esta marca asignada. Si aún desea eliminar la marca puede reasignar otra marca en los productos y luego intentar nuevamente';
$lang['apis_feature']                           = 'Características APIs';
$lang['pdf_lib']                                = 'Librería PDF';
$lang['indian_gst']                             = 'GST para India';
$lang['biz_state']                              = 'Estado para negocios';
$lang['not_recommended']                        = 'No recomendado';

/* Electronic billing*/
$lang['seccion_title_company_data'] = 'Datos de empresa';

// Label
  $lang['label_city'] = 'Ciudad';
  $lang['label_country'] = 'País';
  $lang['label_address'] = 'Dirección';
  $lang['label_postal_code'] = "Código postal";
  $lang['label_state'] = 'Departamento';
  $lang['label_document_number'] = 'NIT/CC';
  $lang['label_first_name'] = 'Primer nombre';
  $lang['label_type_person'] = 'Tipo persona';
  $lang['label_type_vat_regime'] = 'Tipo régimen';
  $lang['label_locality'] = 'Barrio/Localidad';
  $lang['label_second_name'] = 'Segundo nombre';
  $lang['label_business_name'] = 'Razón social';
  $lang['label_trade_name'] = 'Nombre Comercial';
  $lang['label_document_type'] = 'Tipo documento';
  $lang['label_first_lastname'] = 'Primer apellido';
  $lang['label_second_lastname'] = 'Segundo apellido';
  $lang['label_commercial_register'] = 'Matrícula mercantíl';
  $lang['label_types_obligations'] = 'Tipos de obligaciones - responsabilidades';

// Text
  $lang['legal_person'] = 'Jurídica';
  $lang['natural person'] = 'Natural';
  $lang['great_contributor'] = 'Gran contribuyente';

/*********************/


$lang['edit_payment_method']="Editar medio de pago";
$lang['delete_payment_method']="Eliminar medio de pago";
$lang['delete_payment_methods']="Eliminar medios de pago";
$lang['add_payment_method']="Agregar medio de pago";

$lang['state_sale']="Estado en ventas";
$lang['state_purchase']="Estado en compras";

$lang['payment_method_added'] = "El medio de pago se creó correctamente";
$lang['payment_method_updated'] = "El medio de pago se actualizó correctamente";

$lang['cashier_close_register'] = "Cajero puede cerrar la caja";
$lang['allow_change_sale_iva']="Permitir cambiar el impuesto de productos";
$lang['alert_sale_expired']="Días para alerta de vencimiento de Plazo";
$lang['resolucion_porc_aviso']="Porcentaje restante de aviso para vencimiento de resolución por numeración.";
$lang['resolucion_dias_aviso']="Días de aviso restantes para vencimiento de resolución por fecha de vencimiento.";

$lang['payment_method_has_payments'] = 'No se puede eliminar la forma de pago por que ya hay pagos registrados con este medio';
$lang['payment_method_deleted'] = 'El medio de pago se eliminó correctamente';
$lang['delete_payment_method'] = 'Eliminar medio de pago';

$lang['currency_has_movements'] = 'No se puede eliminar la moneda por que tiene movimientos en compras y/o en órdenes de compra/gasto';

$lang['get_companies_check_digit'] = 'Calcular el dígito de verificación para NIT de terceros';

$lang['FIFO'] = 'PEPS (Primeros en entrar, primeros en salir)';
$lang['AVCO'] = 'PROMEDIO (Método de costo promedio)';

$lang['rounding'] = 'Redondear cifras';

$lang['purchase_payment_affects_cash_register'] = "¿Los pagos a las compras afectan la caja?";
$lang['purchase_payments_never_affects_register'] = "Nunca afectan caja";
$lang['purchase_payments_always_affects_register'] = "Siempre afectan caja";
$lang['always_ask_if_purchase_payments_affects_register'] = "Decidir al momento de registrar el pago";

$lang['use_cost_center'] = 'Manejo de centros de costo';

$lang['cost_center_selection'] = 'Método para definir centro de costo';
$lang['default_biller_cost_center'] = 'Centro de costo definido en sucursal';
$lang['select_cost_center'] = 'Definir centro de costo en cada registro';
$lang["advanced_search"] = "Búsqueda avanzada";
$lang['default_cost_center'] = 'Centro de costo por defecto';
$lang["enlist_customer"] = "Alta cliente";
$lang["electronic_billing"] = "Facturación electrónica";
$lang["check_digit"] = "Dígito de verificación";

$lang['technology_provider'] = 'Proveedor tecnológico';
$lang['electronic_billing_user'] = 'Usuario';
$lang['electronic_billing_password'] = 'Contraseña';
$lang['wappsi_print_format'] = 'Formato de impresión Wappsi';
$lang["webservices_url"] = "URL del servicio web";
$lang['mean_payment_code_fe'] = 'Código medio de pago (DIAN)';

$lang['hide_products_in_zero_price'] = 'Ocultar productos con precio en 0 en la lista';

$lang['hide'] = 'Ocultar';
$lang['show'] = 'Mostrar';

$lang['price_group_base_with_promotions'] = 'Lista de precios base más promociones';
$lang['biller_price_group'] = 'Lista de precios asignada a sucursal';
$lang['customer_price_group'] = 'Lista de precios asignada a cliente';
$lang['customer_address_price_group'] = 'Lista de precios asignada a sucursal del cliente';
$lang['biller_price_group_with_discounts'] = 'Lista asignada a sucursal / cliente, más promociones y descuentos de cliente.';
$lang['customer_price_group_with_discounts'] = 'Lista asignada a cliente / sucursal, más promociones y descuentos de cliente.';
$lang['customer_address_price_group_with_discounts'] = 'Lista asignada a sucursal del cliente / sucursal, más promociones y descuentos de cliente.';

$lang['select_each_product'] = 'Escoger en cada producto';

$lang['products_with_different_tax_method'] = 'Existen productos con diferente método de cálculo de IVA al seleccionado, si continúa, se actualizarán todos los productos a dicho método.';

$lang['dont_show'] = 'No mostrar';
$lang['show_alert_sale_expired'] = '¿Mostrar modal de ventas con Plazo a vencerse?';

$lang['customer_default_country'] = 'País por defecto para Clientes';
$lang['customer_default_state'] = 'Departamento por defecto para Clientes';
$lang['customer_default_city'] = 'Ciudad por defecto para Clientes';

$lang['set_adjustment_cost'] = 'Ingresar costo en ajustes';

$lang['cannot_activate_tax_traslate'] = '<h4> No se puede activar el traslado de impuestos por que la parametrización de cuentas en contabilidad está definida por categorías. </h4>';
$lang['lock_cost_field_in_product'] = 'Bloquear campo "Costo" en edición y creación del producto';
$lang["work_environment"] = "Ambiente de trabajo";
$lang["contingency_invoice"] = "Facturación de contingencia";
$lang["test_resolution"] = "Ambiente de pruebas";
$lang["test"] = "Pruebas";
$lang["production"] = "Produccción";
$lang["contingency"] = "Contingencia";
$lang["setTestId"] = "TestId";
$lang["tax_rate_not_deleted"] = "La tarifa no se pudo borrar, por que ya existen movimientos con ésta";

/**
 * Lang Restobar
 */

$lang["restobar"] = "Mesas";

$lang["restobar_title_add"] = "Agregar mesa";

$lang["restobar_label_new"] = "Nuevo";
$lang["restobar_label_table_number"] = "Número";
$lang["restobar_label_table_shape"] = "Forma";
$lang["restobar_label_number_table_seats"] = "Cantidad de asientos";
$lang["restobar_label_table_state"] = "Estado";
$lang["restobar_label_table_width"] = "Ancho";
$lang["restobar_label_table_height"] = "Altura";
$lang["restobar_label_table_size"] = "Tamaño";
$lang["restobar_label_table_radius"] = "Radio";
$lang["restobar_label_x_position_table"] = "Posición X";
$lang["restobar_label_y_position_table"] = "Posición Y";
$lang["restobar_label_available_status"] = "Disponible";
$lang["restobar_label_not_available_status"] = "No disponible";
$lang["restobar_label_occuped_status"] = "Ocupado";
$lang["restobar_label_reserved_status"] = "Reservado";
$lang["restobar_label_square_shape"] = "Cuadrada";
$lang["restobar_label_circle_shape"] = "Circular";
$lang["restobar_label_rectangular_shape"] = "Rectangular";
$lang["restobar_label_ellipse_shape"] = "Elipse";
$lang["restobar_label_bar_shape"] = "Barra";

/*
|
|---------------------------------
| Document Types
|---------------------------------
|
*/
$lang["document_type_add_automatic"] = "Añadir Número automático";
$lang["document_types_added_successfully"] = "Tipos de documentos agregados exitosamente";
$lang["document_types_already_updated"] = "Los tipos de documentos ya se encuentran actualizados";
$lang['select_during_sale_registration'] = 'Escoger durante registro de venta';


$lang["period_days_allowed_after_current_date"] = "Plazo de días permitidos después de la fecha actual";
$lang["period_days_allowed_before_current_date"] = "Plazo de días permitidos antes de la fecha actual";

$lang["technical_annex_version"] = "Anexo técnico";
$lang["technical_annex_version_1_7"] = "Versión 1.7";
$lang["technical_annex_version_1_8"] = "Versión 1.8";
$lang["add_individual_attachments"] = "Agregar archivos adjuntos";

$lang["payroll"] = "Nómina";


/*
|-------------------------------
| SETTINGS UPDATE
|-------------------------------
*/
$lang['user_fields_changed_settings']                   = '%s hizo los siguientes cambios en %s ';
$lang['from']                                           = 'DE';
$lang['to']                                             = 'A';
$lang['las_update']                                     = 'Última actualización';
$lang['customer_branches_language']                     = 'Cambiar texto de "sucursales" de clientes';
$lang['city_code']                                      = 'Código ciudad';
$lang['fuente_retainer']                                = 'Autorretenedor fuente';
$lang['iva_retainer']                                   = 'Autorretenedor iva';
$lang['ica_retainer']                                   = 'Autorretenedor ica';
$lang['mmode']                                          = 'Modo antenimiento';
$lang['rtl']                                            = 'Soporte RTL (Right to left)';
$lang['restrict_calendar']                              = 'Calendario';
$lang['default_tax_rate']                               = 'Impuesto del producto';
$lang['iheight']                                        = 'Altura imagen';
$lang['iwidth']                                         = 'Anchura imagen';
$lang['twidth']                                         = 'Anchura imagen pequeña';
$lang['theight']                                        = 'Altura imagen pequeña';
$lang['barcode_img']                                    = 'Generador de código de barras';
$lang['update_cost']                                    = 'Actualizar el costo con la compra';
$lang['purchase_tax_rate']                              = 'Impuesto de la orden compras';
$lang['tax3']                                           = 'Descuentos afectan IVA';
$lang['default_tax_rate2']                              = 'Impuesto de la orden ventas';
$lang['cashier_close']                                  = "Cajero puede cerrar la caja";
$lang["allow_advanced_search"]                          = "Búsqueda avanzada";
$lang["days_before_current_date"]                       = "Plazo de días permitidos antes de la fecha actual";
$lang["days_after_current_date"]                        = "Plazo de días permitidos después de la fecha actual";
$lang['fe_technology_provider']                         = 'Proveedor tecnológico';
$lang["fe_work_environment"]                            = "Ambiente de trabajo";
$lang['display_symbol']                                 = 'Mostrar símbolo de moneda';
$lang['protocol']                                       = "Protocolo de Correo";
$lang['ca_point']                                       = "Puntos premio clientes";
$lang['each_sale']                                      = 'Cada venta realizada es igual a';
$lang['sa_point']                                       = 'Puntos premio empleados';
$lang['data_synchronization_to_store']                  = 'Sincronización de datos a Tienda';
$lang['add_product_color']                              = 'Agregar Color';
$lang['edit_product_color']                             = 'Editar Color';
$lang['saved_color']                                    = 'Color guardado';
$lang['unsaved_color']                                  = 'Color no guardado';
$lang['updated_color']                                  = 'Color actualizado';
$lang['color_not_updated']                              = 'Color no actualizado';
$lang['add_material']                                   = 'Agregar Material';
$lang['saved_material']                                 = 'Material guardado';
$lang['unsaved_material']                               = 'Material no guardado';
$lang['edit_material']                                  = 'Editar Material';
$lang['updated_material']                               = 'Material actualizado';
$lang['material_not_updated']                           = 'Material no actualizado';
$lang['edit_tag']                                       = 'Editar Etiqueta';
$lang['add_tag']                                        = 'Agregar Etiqueta';
$lang['saved_tag']                                      = 'Tag guardado';
$lang['unsaved_tag']                                    = 'Tag no guardado';
$lang['updated_tag']                                    = 'Etiqueta actualizada';
$lang['tag_not_updated']                                = 'Etiqueta no actualizada';
$lang['logo_symbol']                                    = 'Logo símbolo';
$lang['logo_app']                                       = 'Logo aplicación';
$lang['entry_image_1']                                  = 'Imágen de registro 1';
$lang['entry_image_2']                                  = 'Imágen de registro 2';
$lang['entry_image_3']                                  = 'Imágen de registro 3';
$lang['warranty']                                       = 'Garantías';
$lang['block_warranty_by_warranty_days']                = 'Bloqueo de Garantías por cumplimiento de días';
$lang['warranty_supplier_warehouse']                    = 'Bodega de Garantías del proveedor';
$lang['warranty_warehouse']                             = 'Bodega de Garantías';
$lang['adjustments_document']                           = 'Documento de ajustes';
$lang['transfer_document']                              = 'Documento de traslados';
$lang['synchronized']                                   = 'Sincronizado';
$lang['sync_store']                                     = 'Sincronizar en Tienda';

$lang['colletionsConfiguration']                        = 'Configuración recuados Kiowa';
$lang['aIdMarca']                                       = 'Marca';
$lang['aSchemacia']                                     = 'Esquema';
$lang['cidToken']                                       = 'Token';
$lang['urlComercio']                                    = 'Url retorno';
$lang['aLogin']                                         = 'Usuario';
$lang['aPassword']                                      = 'Contraseña';
$lang['cSucursal']                                      = 'Código de sucursal';

$lang['payment_method_for_credit_financing']            = 'Medio de pago para financiación';
$lang['current_interest_percentage_credit_financing']   = 'Porcentaje Interés Corriente';
$lang['default_interest_percentage_credit_financing']   = 'Porcentaje Interés Moratorio';
$lang['insurance_percentage_credit_financing']          = 'Porcentaje Interés de Seguro';
$lang['vat_default_interest']                           = 'IVA Interés Moratorio';
$lang['quota_calculation_method_credit_financing']      = 'Método de Cálculo de Cuotas';
$lang['fixed_fee']                                      = 'Cuota fija';
$lang['variable_fee']                                   = 'Cuota variable';
$lang['module_name_customization_credit_financing']     = 'Personalización nombre del módulo';
$lang['make_collections']                               = 'Realizar Recaudos Cuotas';
$lang['make_collections_reported']                      = 'Realizar Recaudos Cuotas Reportadas';
$lang['enable_for']                                     = 'Habilitar para';
$lang['all_billing']                                    = 'Toda la facturación';
$lang['sale_invoice']                                   = 'Factura de Venta';
$lang['pos_invoice']                                    = 'Factura POS';
$lang['generate_automatic_invoice']                     = 'Generar factura automática';
$lang['concepts_for_current_interest']                  = 'Concepto Interés Corriente';
$lang['concepts_for_default_interest']                  = 'Concepto Interés Mora';
$lang['management_weight_in_variants']                  = 'Manejar peso en variantes';
$lang['weight']                                         = 'Peso';
$lang['installment_edit']                               = 'Edición de cuotas';
$lang['concept_for_procredito_commission']              = 'Concepto Comisión Procrédito';
$lang['concept_for_procredito_interest']                = 'Concepto Interés Procrédito';
$lang['days_for_notification']                          = "Días para notificación";
$lang['days_for_report']                                = "Días para Reporte";
$lang['delete_table_order']                             = "Eliminar pedido de Mesa";
$lang['handle_jewerly_products']                        = 'Manejar productos de joyería';
$lang["sync_ubications"]                                = "Sincronizar Ubicaciones";
$lang["countries"]                                      = "Países";
$lang["states"]                                         = "Estados";
$lang["cities"]                                         = "Ciudades";
$lang["active/inactive"]                                = "Activar / Inactivar";
$lang["version_date"]                                   = "Fecha de la versión";
$lang["visible"]                                        = "Visible";
