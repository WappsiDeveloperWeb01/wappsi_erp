<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<?php if ($Owner || $GP['bulk_actions']) {
    echo admin_form_open('system_settings/cost_center_actions', 'id="action-form"');
} ?>

<div class="wrapper wrapper-content  animated fadeInRight  no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="QUData" class="table table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th><?= lang('type') ?></th>
                                            <th><?= lang('movement_type') ?></th>
                                            <th><?= lang('tax') ?></th>
                                            <th><?= lang('code') ?></th>
                                            <th><?= lang('ledger') ?></th>
                                            <th></th>
                                            <th><?= lang('actions') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="14" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script>
    if ($(window).width() < 1000) {
        var nums = [[10, 25], [10, 25]];
    } else {
        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
    }

    $(document).ready(function () {
        oTable = $('#QUData').dataTable({
            aaSorting: [[1, "desc"], [2, "desc"]],
            aLengthMenu: nums,
            iDisplayLength:  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true, 'bServerSide': true,
            sAjaxSource: '<?=admin_url('system_settings/getPAccounts')?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                {
                    "mRender" : function (type){
                        if (type == 1) {
                            return "<?= lang('sales') ?>";
                        } else {
                            return "<?= lang('purchases') ?>";
                        }
                    }
                },
                null,
                null,
                null,
                null,
                {"bVisible":false},
                {
                    bSortable: false,
                    className: 'text-center',
                    sWidth: '65px'
                },
            ],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html('<div class="pull-right dropdown">'+
                '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                    '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                        '<li>'+
                            '<a href="<?= admin_url('system_settings/add_paccount') ?>" data-toggle="modal" data-target="#myModal" class="tip" title="" data-original-title="<?= lang('add_paccount') ?>">'+
                                '<i class="fa fa-plus-circle"></i> <?= lang('add_paccount') ?>'+
                            '</a>'+
                        '</li>'+
                    '</ul>'+
                '</div>');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });
    });
</script>