<?php
$txt_home = "";
if ($this->session->userdata('account_locked') && $this->session->userdata('account_locked') == 2) {
    $txt_home = "<b> " . lang('account_for_reading_only') . " </b>";
}
?>

<li class="mm_welcome">
    <a href="<?= admin_url('dashboard') ?>">
        <i class="fa fa-home"></i>
        <span class="text"><?= lang('home') ?> <?= $txt_home ?></span>
    </a>
</li>

<li class="mm_calendar">
    <a href="<?= admin_url('calendar'); ?>">
        <i class="fa fa-calendar"></i>
        <span class="nav-label"> <?= lang('calendar'); ?></span>
    </a>
</li>

<?php if ($this->session->userdata('account_locked') && $this->session->userdata('account_locked') == 1) : ?>
<?php else : ?>
    <?php if ($Owner || $Admin) { ?>
        <li class="mm_products">
            <a href="#">
                <i class="fas fa-box"></i>
                <span class="nav-label"> <?= lang('products'); ?> </span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <li id="products_index">
                    <a href="<?= admin_url('products'); ?>"><?= lang('list_products'); ?></a>
                </li>
                <li id="products_add">
                    <a href="<?= admin_url('products/add'); ?>"><?= lang('add_product'); ?></a>
                </li>
                <li id="products_import_csv">
                    <a href="<?= admin_url('products/import_csv'); ?>"><?= lang('import_products'); ?></a>
                </li>
                <li id="products_print_barcodes">
                    <a href="<?= admin_url('products/print_barcodes'); ?>"><?= lang('print_barcode_label'); ?></a>
                </li>
                <li id="products_quantity_adjustments">
                    <a href="<?= admin_url('products/quantity_adjustments'); ?>"><?= lang('quantity_adjustments'); ?></a>
                </li>
                <li id="products_add_adjustment">
                    <a href="<?= admin_url('products/add_adjustment'); ?>"><?= lang('add_adjustment'); ?></a>
                </li>
                <li id="products_production_orders">
                    <a href="<?= admin_url('products/production_orders'); ?>"><?= lang('production_orders'); ?></a>
                </li>
                <li id="products_add_production_order">
                    <a href="<?= admin_url('products/add_production_order'); ?>"><?= lang('add_production_order'); ?></a>
                </li>
                <li class="mm_production_order">
                    <a href="#">
                        <?= lang('making_order'); ?>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-third-level">
                        <li id="production_order">
                            <a href="<?= admin_url('production_order'); ?>"><?= lang('list_making_orders'); ?></a>
                        </li>
                        <li id="production_order_add">
                            <a href="<?= admin_url('production_order/add'); ?>"><?= lang('add_making_order'); ?></a>
                        </li>
                        <li id="production_order_cutting_orders">
                            <a href="<?= admin_url('production_order/cutting_orders'); ?>"><?= lang('cutting_orders'); ?></a>
                        </li>
                        <li id="production_order_assemble_orders">
                            <a href="<?= admin_url('production_order/assemble_orders'); ?>"><?= lang('assemble_orders'); ?></a>
                        </li>
                        <li id="production_order_packing_orders">
                            <a href="<?= admin_url('production_order/packing_orders'); ?>"><?= lang('packing_orders'); ?></a>
                        </li>
                    </ul>
                </li>
                <li id="products_sequential_counts">
                    <a href="<?= admin_url('products/sequential_counts'); ?>"> Conteos Secuenciales </a>
                </li>
                <li id="products_sequentialcount">
                    <a href="<?= admin_url('products/sequentialCount'); ?>"> Conteo Secuencial </a>
                </li>
                <li id="products_stock_counts">
                    <a href="<?= admin_url('products/stock_counts'); ?>"><?= lang('list_stock_counts'); ?></a>
                </li>
                <li id="products_count_stock">
                    <a href="<?= admin_url('products/count_stock'); ?>"><?= lang('add_count_stock'); ?></a>
                </li>
                <li id="products_count_stock_variants">
                    <a href="<?= admin_url('products/count_stock_variants'); ?>"><?= lang('add_count_stock_variants'); ?></a>
                </li>
                <li id="products_count_stock_variants">
                    <a href="<?= admin_url('products/add_express_count'); ?>"><?= lang('add_express_count'); ?></a>
                </li>
                <li id="system_settings_update_price_margin">
                    <a href="<?= admin_url('system_settings/update_price_margin'); ?>"><?= lang('update_price_margin'); ?></a>
                </li>
                <li class="mm_transfers">
                    <a href="#">
                        <?= lang('transfers'); ?>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-third-level">
                        <li id="transfers_index">
                            <a href="<?= admin_url('transfers'); ?>"><?= lang('list_transfers'); ?></a>
                        </li>
                        <li id="transfers_add">
                            <a href="<?= admin_url('transfers/add'); ?>"><?= lang('add_transfer'); ?></a>
                        </li>
                        <li id="transfers_transfer_by_csv">
                            <a href="<?= admin_url('transfers/transfer_by_csv'); ?>"><?= lang('add_transfer_by_csv'); ?></a>
                        </li>
                        <li id="transfers_orders">
                            <a href="<?= admin_url('transfers/orders'); ?>"><?= lang('transfers_orders'); ?></a>
                        </li>
                        <li id="transfers_add_order">
                            <a href="<?= admin_url('transfers/add_order'); ?>"><?= lang('add_transfer_order'); ?></a>
                        </li>
                    </ul>
                </li>

                <?php if ($this->Owner) { ?>
                    <li id="products_update_monthly_cost">
                        <a href="<?= admin_url('products/update_monthly_cost'); ?>"><?= lang('update_monthly_cost'); ?></a>
                    </li>
                <?php } ?>

                <li id="products_product_transformations">
                    <a href="<?= admin_url('products/product_transformations'); ?>"><?= lang('product_transformations'); ?></a>
                </li>

                <li id="products_add_product_transformation">
                    <a href="<?= admin_url('products/add_product_transformation'); ?>"><?= lang('add_product_transformation'); ?></a>
                </li>

                <?php if ($this->Owner) : ?>
                    <li id="products_update_csv_fix_products_cost">
                        <a href="<?= admin_url('products/update_csv_fix_products_cost'); ?>"><?= lang('update_csv_fix_products_cost'); ?></a>
                    </li>
                    <li id="products_update_csv_recosting_registers">
                        <a href="<?= admin_url('products/update_csv_recosting_registers'); ?>"><?= lang('update_csv_recosting_registers'); ?></a>
                    </li>
                    <li id="products_recosting_selection">
                        <a href="<?= admin_url('products/recosting_selection'); ?>"><?= lang('recosting_selection'); ?></a>
                    </li>
                <?php endif ?>
            </ul>
        </li>

        <li class="mm_quotes">
            <a href="#">
                <i class="fas fa-file-invoice-dollar"></i>
                <span class="nav-label"> <?= lang('quotes'); ?> </span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <li id="quotes_index">
                    <a href="<?= admin_url('quotes'); ?>"><?= lang('list_quotes'); ?></a>
                </li>
                <li id="quotes_add">
                    <a href="<?= admin_url('quotes/add'); ?>"><?= lang('add_quote'); ?></a>
                </li>
            </ul>
        </li>

        <li class="mm_orders">
            <a href="#">
                <i class="fas fa-file-signature"></i>
                <span class="nav-label"> <?= lang('sale_orders'); ?> </span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <li id="sales_orders">
                    <a href="<?= admin_url('sales/orders'); ?>"><?= lang('list_orders'); ?></a>
                </li>
                <li id="order_add">
                    <a href="<?= admin_url('sales/add_order'); ?>"><?= lang('add_order'); ?></a>
                </li>
            </ul>
        </li>

        <li class="mm_sales <?= strtolower($this->router->fetch_class()) == 'sales' ? 'mm_pos' : '' ?>  mm_budget mm_warranty">
            <a href="#">
                <i class="fas fa-store"></i>
                <span class="nav-label"> <?= lang('sales'); ?>
                </span> <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <li id="sales_index">
                    <a href="<?= admin_url('sales'); ?>"> <?= lang('list_sales'); ?></a>
                </li>
                <li id="sales_fe_index">
                    <a href="<?= admin_url('sales/fe_index'); ?>"> <?= lang('list_sales_fe'); ?></a>
                </li>
                <li id="sales_add">
                    <a href="<?= admin_url('sales/add'); ?>"> <?= lang('add_sale'); ?></a>
                </li>
                <li id="sales_sale_by_csv">
                    <a href="<?= admin_url('sales/sale_by_csv'); ?>"> <?= lang('add_sale_by_csv'); ?></a>
                </li>
                <li id="sales_deliveries">
                    <a href="<?= admin_url('sales/deliveries'); ?>"> <?= lang('deliveries'); ?></a>
                </li>
                <li id="sales_gift_cards">
                    <a href="<?= admin_url('sales/gift_cards'); ?>"> <?= lang('gift_cards'); ?></a>
                </li>
                <?php if ($this->deposit_payment_method['sale'] == true) : ?>
                    <li id="customers_list_deposits">
                        <a href="<?= admin_url('customers/list_deposits'); ?>"> <?= lang('list_deposits'); ?></a>
                    </li>
                <?php endif ?>
                <li class="dropdown">
                    <a href="<?= admin_url('payments') ?>">
                        <?= lang('multi_payments') ?>
                    </a>
                </li>
                <li id="returns_credit_note_other_concepts">
                    <a href="<?= admin_url('returns/credit_note_other_concepts'); ?>"> <?= lang('credit_note_other_concepts_label_menu'); ?> </a>
                </li>
                <li class="debit_notes_add">
                    <a href="<?= admin_url('debit_notes/add'); ?>">
                        <span class="nav-label"> <?= lang('debit_note_label_menu'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= admin_url('billers/random_pin_code'); ?>">
                        <span class="nav-label"> <?= lang('random_pin_code'); ?></span>
                    </a>
                </li>
                <li id="returns_add">
                    <a href="<?= admin_url('returns/add_past_years_sale_return'); ?>"><?= lang('add_past_years_sale_return'); ?></a>
                </li>
                <li id="budget_index">
                    <a href="<?= admin_url('budget') ?>"><?= $this->lang->line('budget') ?></a>
                </li>
                <li class="mm_sales_reports">
                    <a href="#">
                        <?= lang('reports'); ?>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-third-level">
                        <li id="sales_reports_budget_execution">
                            <a href="<?= admin_url('sales_reports/budget_execution'); ?>"><?= lang('sales_budget_execution'); ?></a>
                        </li>
                    </ul>
                </li>
                <li id="warranty_index">
                    <a href="<?= admin_url('warranty') ?>"><?= $this->lang->line('list_warranty') ?></a>
                </li>
                <li id="warranty_add">
                    <a href="<?= admin_url('warranty/add') ?>"><?= $this->lang->line('add_warranty') ?></a>
                </li>
                <li id="print_server">
                    <a href="<?= admin_url('sales/printServer') ?>"><?= $this->lang->line('print_server') ?></a>
                </li>
            </ul>
        </li>

        <?php if (POS) { ?>
            <li class="mm_pos <?= strtolower($this->router->fetch_method()) == 'sales' ? 'mm_pos' : '' ?>">
                <a href="#">
                    <i class="fas fa-store"></i>
                    <span class="nav-label"> <?= lang('pos_sales'); ?> </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="pos_sales">
                        <a href="<?= admin_url('pos/sales'); ?>"><?= lang('pos_sales_list'); ?></a>
                    </li>
                    <li id="pos_fe_index">
                        <a href="<?= admin_url("pos/fe_index") ?>"><?= $this->lang->line("pos_fe_index") ?></a>
                    </li>

                    <?php if ($this->Settings->cashier_close == 2) { ?>
                        <li class="disabled tip" title="<?= lang('cash_register_not_used') ?>">
                            <a><?= lang('pos_sales_add') ?></a>
                        </li>
                    <?php } else { ?>
                        <li id="pos_index">
                            <a href="<?= admin_url('pos') ?>"> <?= lang('pos_sales_add') ?> </a>
                        </li>

                        <li id="pos_add_wholesale">
                            <a href="<?= admin_url('pos/add_wholesale') ?>"> <?= lang('pos_wholesale') ?> </a>
                        </li>
                    <?php } ?>

                    <?php if ($Owner) { ?>
                        <li>
                            <a id="today_profit" title="<span><?= lang('today_profit') ?></span>" data-placement="bottom" data-html="true" href="<?= admin_url('reports/profit') ?>" data-toggle="modal" data-target="#myModal"> <?= lang('today_profit') ?></a>
                        </li>
                    <?php } ?>

                    <li id="pos_registers">
                        <a href="<?= admin_url('pos/registers') ?>"><?= lang('list_open_registers') ?></a>
                    </li>

                    <li>
                        <a id="clearLS" href="#"> <?= lang('clear_ls') ?></a>
                    </li>

                    <?php if ($this->session->userdata('register_open_time') != null) { ?>
                        <li id="pos_close_register">
                            <a id="close_register" href="<?= admin_url('pos/close_register/?v=' . $this->v . '&m=' . $this->m) ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><?= lang('close_register') ?></a>
                        </li>
                    <?php } elseif ($this->Settings->cashier_close == 2) { ?>
                        <li class="disabled tip" title="<?= lang('cash_register_not_used') ?>">
                            <a id="close_register"> <?= lang('close_register') ?> </a>
                        </li>
                    <?php } else { ?>
                        <li class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                            <a id="close_register"> <?= lang('close_register') ?> </a>
                        </li>
                    <?php } ?>

                    <?php if ($this->session->userdata('register_open_time') != null) { ?>
                        <li id="purchases_expenses">
                            <a href="<?= admin_url('purchases/expenses'); ?>"><?= lang('list_expenses'); ?></a>
                        </li>
                        <li id="purchases_add_expense">
                            <a href="<?= admin_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_expense'); ?></a>
                        </li>
                    <?php } elseif ($this->Settings->cashier_close == 2) { ?>
                        <li id="purchases_expenses">
                            <a href="<?= admin_url('purchases/expenses'); ?>"><?= lang('list_expenses'); ?></a>
                        </li>
                        <li id="purchases_add_expense">
                            <a href="<?= admin_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_expense'); ?></a>
                        </li>
                    <?php } else { ?>
                        <li id="purchases_list_expenses" class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                            <a><?= lang('list_expenses'); ?></a>
                        </li>
                        <li id="purchases_add_expense" class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                            <a><?= lang('add_expense'); ?></a>
                        </li>
                    <?php } ?>

                    <li id="pos_pos_register_movements">
                        <a href="<?= admin_url('pos/pos_register_movements'); ?>"><?= lang('pos_register_movements'); ?></a>
                    </li>

                    <li id="pos_register_add_movement">
                        <a href="<?= admin_url('pos/pos_register_add_movement'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('pos_register_add_movement'); ?></a>
                    </li>

                    <?php if ($this->Settings->payment_collection_webservice) : ?>
                        <li id="payments_collections_index">
                            <a href="<?= admin_url('payments_collections/index'); ?>"><?= lang('payments_collections'); ?></a>
                        </li>
                        <?php if ($this->session->userdata('register_open_time')): ?>
                            <li id="payments_collections_add">
                                <a href="<?= admin_url('payments_collections/add'); ?>" data-toggle="modal" data-target="#myModal"><?= sprintf(lang('payments_collections_add'), lang('payment_collection')); ?></a>
                            </li>
                        <?php else: ?>
                            <li class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                                <a id="close_register"><?= sprintf(lang('payments_collections_add'), lang('payment_collection')) ?></a>
                            </li>
                        <?php endif ?>
                    <?php endif ?>

                    <li>
                        <a id="pos_opened_bills" data-toggle="modal" data-target="#myModal" href="<?= admin_url('pos/opened_bills') ?>" data-toggle="ajax" data-backdrop="static" data-keyboard="false"><?= lang('suspended_sales') ?></a>
                    </li>

                    <li id="pos_today_sale">
                        <a id="today_sale" href="<?= admin_url('pos/today_sale') ?>" data-toggle="modal" data-target="#myModal"><?= lang('today_sale') ?></a>
                    </li>

                    <?php if ($this->pos_settings->restobar_mode == '1') { ?>
                        <li id="pos_see_order_preparation">
                            <a id="see_order_preparation" href="<?= admin_url('pos/see_order_preparation') ?>"><?= lang('see_order_preparation') ?></a>
                        </li>
                    <?php } ?>

                    <li id="pos_pos_print_server">
                        <a href="<?= admin_url('pos/pos_print_server'); ?>" target="_blank"><?= lang('pos_print_server'); ?></a>
                    </li>
                </ul>
            </li>
        <?php } ?>

        <li class="mm_purchases">
            <a href="#">
                <i class="fas fa-truck"></i>
                <span class="nav-label"> <?= lang('purchases'); ?></span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <li id="purchases_index">
                    <a href="<?= admin_url('purchases'); ?>"><?= lang('list_purchases'); ?></a>
                </li>
                <li id="purchases_add">
                    <a href="<?= admin_url('purchases/add'); ?>"><?= lang('add_purchase'); ?></a>
                </li>
                <?php if ($this->enableSupportingDocument) : ?>
                    <li id="purchases_supporting_document_index">
                        <a href="<?= admin_url('purchases/supporting_document_index'); ?>"><?= lang('purchases_list_supporting_document'); ?></a>
                    </li>
                    <li id="purchases_add_support_document">
                        <a href="<?= admin_url('purchases/add_support_document'); ?>"><?= lang('add_support_document'); ?></a>
                    </li>
                <?php endif ?>

                <li id="purchases_purchase_by_csv">
                    <a href="<?= admin_url('purchases/purchase_by_csv'); ?>"><?= lang('add_purchase_by_csv'); ?></a>
                </li>
                <li id="purchases_p_index">
                    <a href="<?= admin_url('quotes/p_index'); ?>"><?= lang('quotes_purchases'); ?></a>
                </li>
                <li id="purchases_addqpurchase">
                    <a href="<?= admin_url('quotes/addqpurchase'); ?>"><?= lang('add_quote_purchase'); ?></a>
                </li>
                <li id="purchases_addqexpense">
                    <a href="<?= admin_url('quotes/addqexpense'); ?>"><?= lang('add_quote_expense'); ?></a>
                </li>
                <?php if ($this->deposit_payment_method['purchase'] == true) : ?>
                    <li id="suppliers_list_deposits">
                        <a href="<?= admin_url('suppliers/list_deposits'); ?>"> <?= lang('list_deposits_suppliers'); ?></a>
                    </li>
                <?php endif ?>
                <li id="payments_pindex" class="dropdown">
                    <a href="<?= admin_url('payments/pindex') ?>"><?= lang('multi_payments_purchases') ?></a>
                </li>

                <li id="payments_exindex" class="dropdown">
                    <a href="<?= admin_url('payments/exindex') ?>"><?= lang('multi_payment_expenses') ?></a>
                </li>

                <li id="returns_add">
                    <a href="<?= admin_url('returns/add_past_years_purchase_return'); ?>"><?= lang('add_past_years_purchase_return'); ?></a>
                </li>
                <li id="purchases_add">
                    <a href="<?= admin_url('purchases/imports'); ?>"><?= lang('imports'); ?></a>
                </li>
                <li id="purchases_add">
                    <a href="<?= admin_url('purchases/add_import'); ?>"><?= lang('add_import'); ?></a>
                </li>
            </ul>
        </li>

        <li class="mm_financing">
            <a href="#">
                <i class="fas fa-store"></i>
                <span class="nav-label"> <?= lang('credit_financing_language'); ?> </span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <li id="financing_list">
                    <a href="<?= admin_url('financing') ?>"><?= $this->lang->line('financing_list') ?></a>
                </li>
                <li id="financing_installmentList">
                    <a href="<?= admin_url('financing/installmentList') ?>"><?= $this->lang->line('installment_list') ?></a>
                </li>
                <li id="financing_paymentlist">
                    <a href="<?= admin_url('financing/paymentList') ?>"><?= $this->lang->line('payment_list') ?></a>
                </li>
                <li id="financing_paymentfinancingfee">
                    <a href="<?= admin_url('financing/paymentFinancingFee') ?>"><?= $this->lang->line('payment_financing_fee') ?></a>
                </li>
            </ul>
        </li>

        <?php if ($this->enableElectronicPayroll) { ?>
            <li class="mm_payroll
                <?= ($this->router->fetch_class() == 'payroll_management' || $this->router->fetch_class() == 'employees'
                || $this->router->fetch_class() == 'payroll_contracts' || $this->router->fetch_class() == 'payroll_concepts'
                || $this->router->fetch_class() == 'payroll_settings' || $this->router->fetch_class() == 'payroll_electronic'
                || $this->router->fetch_class() == 'payroll_exports')
                    ? 'active' : ''; ?>">
                <a href="#">
                    <i class="fa fa-id-card"></i>
                    <span class="nav-label"><?= lang("payroll"); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="employees_index">
                        <a href="<?= admin_url("employees"); ?>"><?= lang("employees_list"); ?></a>
                    </li>
                    <li id="employees_index">
                        <a href="<?= admin_url("employees/add"); ?>"><?= lang("employees_add"); ?></a>
                    </li>
                    <li id="payroll_contracts">
                        <a href="<?= admin_url("payroll_contracts"); ?>"> <?= lang("contract_list"); ?> </a>
                    </li>
                    <li id="payroll_contracts">
                        <a href="<?= admin_url("payroll_contracts/add"); ?>"> <?= lang("contracts_add"); ?> </a>
                    </li>
                    <li id="payroll_management">
                        <a href="<?= admin_url("payroll_management"); ?>"><?= lang("payroll_management"); ?></a>
                    </li>
                    <li id="payroll_electronic">
                        <a href="<?= admin_url("payroll_electronic"); ?>"><?= lang("payroll_electronic"); ?></a>
                    </li>
                    <li id="payroll_concepts">
                        <a href="<?= admin_url("payroll_concepts"); ?>"> <?= lang("payroll_concepts"); ?> </a>
                    </li>
                    <li id="payroll_exports">
                        <a href="<?= admin_url("payroll_exports"); ?>"> <?= lang("payroll_exports"); ?> </a>
                    </li>
                    <li id="payroll_settings">
                        <a href="<?= admin_url("payroll_settings"); ?>"> <?= lang("settings"); ?> </a>
                    </li>
                </ul>
            </li>
        <?php } ?>

        <?php if ($this->enableDocumentsReception) { ?>
            <li class="mm_documentsReception <?= ($this->router->fetch_class() == 'documentsReception') ? 'active' : ''; ?>">
                <a href="#">
                    <i class="fa fa-cloud-download"></i>
                    <span class="nav-label"><?= $this->lang->line('documents_reception'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="documentsReception_index">
                        <a href="<?= admin_url("documentsReception"); ?>"><?= $this->lang->line("document_reception_list"); ?></a>
                    </li>
                    <li id="documentsReception_settings">
                        <a href="<?= admin_url("documentsReception/settings") ?>"><?= $this->lang->line('settings') ?></a>
                    </li>
                </ul>
            </li>
        <?php } ?>

        <li class="mm_auth mm_customers mm_suppliers mm_billers mm_seller mm_creditors">
            <a href="#">
                <i class="fa fa-users"></i>
                <span class="nav-label"> <?= lang('people'); ?> </span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <?php if ($Owner || $Admin) { ?>
                    <li id="auth_users">
                        <a href="<?= admin_url('users'); ?>"><?= lang('list_users'); ?></a>
                    </li>
                    <?php if ($Owner) { ?>
                        <li id="auth_create_user">
                            <a href="<?= admin_url('users/create_user'); ?>"><?= lang('new_user'); ?></a>
                        </li>
                    <?php } ?>
                <?php } ?>

                <li id="billers_index">
                    <a href="<?= admin_url('billers'); ?>"><?= lang('list_billers'); ?></a>
                </li>

                <?php if ($Owner) { ?>
                    <li id="billers_index">
                        <a href="<?= admin_url('billers/add'); ?>"><?= lang('add_biller'); ?></a>
                    </li>
                <?php } ?>

                <li id="customers_index">
                    <a href="<?= admin_url('customers'); ?>"><?= lang('list_customers'); ?></a>
                </li>

                <li id="customers_index">
                    <a href="<?= admin_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_customer'); ?></a>
                </li>

                <li id="suppliers_index">
                    <a href="<?= admin_url('suppliers'); ?>"><?= lang('list_suppliers'); ?></a>
                </li>

                <li id="suppliers_index">
                    <a href="<?= admin_url('suppliers/add'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_supplier'); ?></a>
                </li>

                <li id="suppliers_index">
                    <a href="<?= admin_url('creditors'); ?>"><?= lang('list_creditors'); ?></a>
                </li>

                <li id="suppliers_index">
                    <a href="<?= admin_url('creditors/add'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_creditor'); ?></a>
                </li>

                <li id="seller_index">
                    <a href="<?= admin_url('seller'); ?>"><?= lang('seller_list'); ?></a>
                </li>
                <li id="seller_index">
                    <a href="<?= admin_url('seller/add'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_seller'); ?></a>
                </li>

                <li id="affiliate_index">
                    <a href="<?= admin_url('affiliates'); ?>"><?= lang('affiliate_list'); ?></a>
                </li>
                <li id="affiliate_index">
                    <a href="<?= admin_url('affiliates/add'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_affiliate'); ?></a>
                </li>
            </ul>
        </li>

        <?php if ($Owner && file_exists(APPPATH . 'controllers' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'Shop.php')) { ?>
            <li class="mm_shop_settings mm_api_settings">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="nav-label"> <?= lang('front_end'); ?> </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <?php if (SHOP) { ?>
                        <li>
                            <a href="<?= base_url() ?>"><?= lang('shop') ?></a>
                        </li>
                    <?php } ?>

                    <li id="shop_settings_index">
                        <a href="<?= admin_url('shop_settings') ?>"><?= lang('shop_settings'); ?></a>
                    </li>

                    <li id="shop_settings_slider">
                        <a href="<?= admin_url('shop_settings/slider') ?>"><?= lang('slider_settings'); ?></a>
                    </li>

                    <?php if ($Settings->apis) { ?>
                        <li id="api_settings_index">
                            <a href="<?= admin_url('api_settings') ?>"><?= lang('api_keys'); ?></a>
                        </li>
                    <?php } ?>

                    <li id="shop_settings_pages">
                        <a href="<?= admin_url('shop_settings/pages') ?>"><?= lang('list_pages'); ?></a>
                    </li>

                    <li id="shop_settings_pages">
                        <a href="<?= admin_url('shop_settings/add_page') ?>"><?= lang('add_page'); ?></a>
                    </li>

                    <li id="shop_settings_sms_settings">
                        <a href="<?= admin_url('shop_settings/sms_settings') ?>"><?= lang('sms_settings'); ?></a>
                    </li>

                    <li id="shop_settings_send_sms">
                        <a href="<?= admin_url('shop_settings/send_sms') ?>"><?= lang('send_sms'); ?></a>
                    </li>

                    <li id="shop_settings_sms_log">
                        <a href="<?= admin_url('shop_settings/sms_log') ?>"><?= lang('sms_log'); ?></a>
                    </li>
                </ul>
            </li>
        <?php } ?>

        <li class="mm_reports">
            <a href="#">
                <i class="fa fa-bar-chart-o"></i>
                <span class="nav-label"> <?= lang('reports'); ?> </span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <li id="reports_index">
                    <a href="<?= admin_url('reports') ?>"><?= lang('overview_chart'); ?></a>
                </li>

                <li id="reports_warehouse_stock">
                    <a href="<?= admin_url('reports/warehouse_stock') ?>"><?= lang('warehouse_stock'); ?></a>
                </li>

                <li id="reports_best_sellers">
                    <a href="<?= admin_url('reports/best_sellers') ?>"><?= lang('best_sellers'); ?></a>
                </li>

                <?php if (POS) { ?>
                    <li id="reports_register">
                        <a href="<?= admin_url('reports/register') ?>"><?= lang('register_report'); ?></a>
                    </li>
                    <li id="reports_biller_register">
                        <a href="<?= admin_url('reports/biller_register') ?>"><?= lang('biller_register_report'); ?></a>
                    </li>
                    <li id="reports_biller_register">
                        <a href="<?= admin_url('reports/serial_register') ?>"><?= lang('serial_register_report'); ?></a>
                    </li>
                <?php } ?>

                <li id="reports_zeta_report">
                    <a href="<?= admin_url('reports/load_zeta') ?>"><?= lang('zeta_report'); ?></a>
                </li>

                <?php if (POS) { ?>
                    <li>
                        <a id="close_register_detailed" href="<?= admin_url('reports/closed_register_details') ?>" target="_blank"><?= lang('close_register_detailed') ?></a>
                    </li>
                <?php } ?>


                <li id="reports_quantity_alerts">
                    <a href="<?= admin_url('reports/quantity_alerts') ?>"><?= lang('product_quantity_alerts'); ?></a>
                </li>

                <?php if ($Settings->product_expiry) { ?>
                    <li id="reports_expiry_alerts">
                        <a href="<?= admin_url('reports/expiry_alerts') ?>"><?= lang('product_expiry_alerts'); ?></a>
                    </li>
                <?php } ?>

                <li id="reports_products">
                    <a href="<?= admin_url('reports/products') ?>"><?= lang('products_report'); ?></a>
                </li>

                <li id="reports_products">
                    <a href="<?= admin_url('reports/valued_products') ?>"><?= lang('valued_products_report'); ?></a>
                </li>

                <li id="reports_adjustments">
                    <a href="<?= admin_url('reports/adjustments') ?>"><?= lang('adjustments_report'); ?></a>
                </li>

                <li id="reports_categories">
                    <a href="<?= admin_url('reports/categories') ?>"><?= sprintf(lang('sale_purchase_categories_report'), lang('category')); ?></a>
                </li>

                <li id="reports_categories">
                    <a href="<?= admin_url('reports/categories2') ?>"><?= sprintf(sprintf(lang('categories_report'), lang('categories')), lang('categories')); ?></a>
                </li>

                <li id="reports_brands">
                    <a href="<?= admin_url('reports/brands') ?>"><?= sprintf(lang('brands_report'), lang('brands')); ?></a>
                </li>

                <li id="reports_daily_sales">
                    <a href="<?= admin_url('reports/daily_sales') ?>"><?= lang('daily_sales'); ?></a>
                </li>

                <li id="reports_monthly_sales">
                    <a href="<?= admin_url('reports/monthly_sales') ?>"><?= lang('monthly_sales'); ?></a>
                </li>

                <li id="reports_sales">
                    <a href="<?= admin_url('reports/sales') ?>"><?= lang('sales_report'); ?></a>
                </li>

                <li id="reports_daily_incomes">
                    <a href="<?= admin_url('reports/daily_incomes') ?>"><?= lang('daily_incomes_report'); ?></a>
                </li>

                <li id="reports_billers_monthly_sales">
                    <a href="<?= admin_url('reports/billers_monthly_sales') ?>"><?= lang('billers_monthly_sales'); ?></a>
                </li>

                <li id="reports_payments">
                    <a href="<?= admin_url('reports/payments') ?>"><?= lang('payments_report'); ?></a>
                </li>

                <li id="reports_tax">
                    <a href="<?= admin_url('reports/tax') ?>"><?= lang('tax_report'); ?></a>
                </li>

                <li id="reports_profit_loss">
                    <a href="<?= admin_url('reports/profit_loss') ?>"><?= lang('profit_and_loss'); ?></a>
                </li>

                <li id="reports_daily_purchases">
                    <a href="<?= admin_url('reports/daily_purchases') ?>"><?= lang('daily_purchases'); ?></a>
                </li>

                <li id="reports_monthly_purchases">
                    <a href="<?= admin_url('reports/monthly_purchases') ?>"><?= lang('monthly_purchases'); ?></a>
                </li>

                <li id="reports_purchases">
                    <a href="<?= admin_url('reports/purchases') ?>"><?= lang('purchases_report'); ?></a>
                </li>

                <li id="reports_expenses">
                    <a href="<?= admin_url('reports/expenses') ?>"><?= lang('expenses_report'); ?></a>
                </li>

                <li id="reports_customer_report">
                    <a href="<?= admin_url('reports/customers') ?>"><?= lang('customers_report'); ?></a>
                </li>

                <li id="reports_supplier_report">
                    <a href="<?= admin_url('reports/suppliers') ?>"><?= lang('creditors_suppliers_report'); ?></a>
                </li>

                <li id="reports_staff_report">
                    <a href="<?= admin_url('reports/users') ?>"><?= lang('staff_report'); ?></a>
                </li>

                <li>
                    <a href="<?= admin_url() ?>pos/payment_term_expire" data-toggle="modal" data-target="#myModal" id="payment_term_expire"><?= lang('payment_term_expired') ?></a>
                </li>

                <?php if ($this->Owner || $this->Admin) { ?>
                    <?php if ($this->Settings->big_data_limit_reports == 0) : ?>
                        <li id="reports_staff_report">
                            <a href="<?= admin_url('reports/portfolio') ?>"><?= lang('portfolio_report'); ?></a>
                        </li>
                    <?php endif ?>
                    <li id="reports_staff_report">
                        <a href="<?= admin_url('reports/collection_commissions') ?>"><?= lang('collection_commissions_report'); ?></a>
                    </li>
                <?php } ?>

                <?php if ($this->Owner || $this->Admin) { ?>
                    <li id="reports_portfolio_report">
                        <a href="<?= admin_url('reports/portfolio_report') ?>"><?= lang('portfolio_by_ages_seller'); ?></a>
                    </li>

                    <li id="reports_portfolio_report">
                        <a href="<?= admin_url('reports/portfolio_report_2') ?>"><?= lang('portfolio_by_ages_customer'); ?></a>
                    </li>

                    <li id="reports_debts_to_pay_report">
                        <a href="<?= admin_url('reports/debts_to_pay_report') ?>"><?= lang('debts_to_pay_report'); ?></a>
                    </li>
                    <li id="reports_load_bills">
                        <a href="<?= admin_url('reports/load_bills') ?>"><?= lang('seller_report'); ?></a>
                    </li>

                    <li id="repots_load_rentabilidad_doc">
                        <a href="<?= admin_url('reports/load_rentabilidad_doc') ?>"><?= lang('profitability_document'); ?></a>
                    </li>

                    <li id="reports_load_rentabilidad_customer">
                        <a href="<?= admin_url('reports/load_rentabilidad_customer') ?>"><?= lang('profitability_customer'); ?></a>
                    </li>

                    <li id="reports_load_rentabilidad_producto">
                        <a href="<?= admin_url('reports/load_rentabilidad_producto') ?>"><?= lang('profitability_product'); ?></a>
                    </li>

                    <li id="reports_load_rentabilidad_producto">
                        <a href="<?= admin_url('reports/product_profitability') ?>"><?= lang('profitability_product_purchase_cost'); ?></a>
                    </li>

                    <li id="reports_warehouse_inventory_variants">
                        <a href="<?= admin_url('reports/warehouse_inventory_variants') ?>"><?= lang('warehouse_inventory_variants'); ?></a>
                    </li>
                <?php } ?>

                <li id="reports_order_sales">
                    <a href="<?= admin_url('reports/purchases_request') ?>"><?= lang('purchases_request_report'); ?></a>
                </li>
                <li id="reports_order_sales">
                    <a href="<?= admin_url('reports/order_sales') ?>"><?= lang('order_sales_report'); ?></a>
                </li>
                <li id="reports_price_groups">
                    <a href="<?= admin_url('reports/price_groups') ?>"><?= lang('price_groups_report'); ?></a>
                </li>
                <li id="products_recosting_selection">
                    <a href="<?= admin_url('sales/export_accountant_notes'); ?>"><?= lang('export_accountant_notes'); ?></a>
                </li>
                <li id="reports_user_activities">
                    <a href="<?= admin_url('reports/user_activities') ?>"> <?= lang('user_activities'); ?> </a>
                </li>
                <li id="reports_award_points">
                    <a href="<?= admin_url('reports/award_points') ?>"> <?= lang('award_points'); ?> </a>
                </li>
                <li id="reports_transfers">
                    <a href="<?= admin_url('reports/transfers') ?>"> <?= lang('transfers_report'); ?> </a>
                </li>
                <li id="reports_production_order_report">
                    <a href="<?= admin_url('reports/production_order_report') ?>"><?= lang('production_order_report'); ?></a>
                </li>
            </ul>
        </li>

        <!-- <li class="mm_returns">
            <a href="#">
                <i class="fa fa-random"></i>
                <span class="nav-label"><?= lang('returns'); ?></span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <li id="returns_index">
					<a href="<?= admin_url('returns'); ?>"><?= lang('list_returns'); ?></a>
				</li>
                <li id="returns_add">
                    <a href="<?= admin_url('returns/add_past_years_sale_return'); ?>"><?= lang('add_past_years_sale_return'); ?></a>
                </li>
                <li id="returns_add">
                    <a href="<?= admin_url('returns/add_past_years_purchase_return'); ?>"><?= lang('add_past_years_purchase_return'); ?></a>
                </li>
            </ul>
        </li> -->

        <li class="mm_system_settings <?= strtolower($this->router->fetch_method()) == 'sales' ? '' : 'mm_pos' ?>">
            <a href="#">
                <i class="fa fa-cog"></i>
                <span class="nav-label"> <?= lang('settings'); ?> </span>
                <span class="fa arrow"></span>
            </a>

            <ul class="nav nav-second-level collapse">

                <?php if ($this->Owner) : ?>
                    <li id="system_settings_auditor">
                        <a href="<?= admin_url('system_settings/auditor') ?>"> <?= lang('auditor'); ?></a>
                    </li>
                <?php endif ?>
                <li id="system_settings_index">
                    <a href="<?= admin_url('system_settings') ?>"> <?= lang('system_settings'); ?></a>
                </li>
                <?php if (POS) { ?>
                    <li id="pos_settings">
                        <a href="<?= admin_url('pos/settings') ?>"><?= lang('pos_settings'); ?></a>
                    </li>

                    <li id="pos_printers">
                        <a href="<?= admin_url('pos/printers') ?>"><?= lang('list_printers'); ?></a>
                    </li>

                    <li id="pos_add_printer">
                        <a href="<?= admin_url('pos/add_printer') ?>"><?= lang('add_printer'); ?></a>
                    </li>
                <?php } ?>

                <li id="system_settings_index">
                    <a href="<?= admin_url('system_settings/document_types') ?>"> <?= lang('document_types'); ?> </a>
                </li>

                <li id="system_settings_change_logo">
                    <a href="<?= admin_url('system_settings/change_logo') ?>" data-toggle="modal" data-target="#myModal"> <?= lang('change_logo'); ?> </a>
                </li>

                <li id="system_settings_currencies">
                    <a href="<?= admin_url('system_settings/currencies') ?>"> <?= lang('currencies'); ?> </a>
                </li>

                <li id="system_settings_customer_groups">
                    <a href="<?= admin_url('system_settings/customer_groups') ?>"> <?= lang('customer_groups'); ?> </a>
                </li>

                <li id="system_settings_price_groups">
                    <a href="<?= admin_url('system_settings/price_groups') ?>"> <?= lang('price_groups'); ?> </a>
                </li>

                <?php if (
                    $this->Settings->prioridad_precios_producto == 2 ||
                    $this->Settings->prioridad_precios_producto == 3 ||
                    $this->Settings->prioridad_precios_producto == 8 ||
                    $this->Settings->prioridad_precios_producto == 4 ||
                    $this->Settings->prioridad_precios_producto == 6 ||
                    $this->Settings->prioridad_precios_producto == 9 ||
                    $this->Settings->prioridad_precios_producto == 10
                ) : ?>
                    <li id="system_settings_price_groups">
                        <a href="<?= admin_url('system_settings/update_products_group_prices') ?>"><?= lang('update_products_group_prices'); ?> </a>
                    </li>
                <?php endif ?>

                <?php if (
                    $this->Settings->prioridad_precios_producto == 5 ||
                    $this->Settings->prioridad_precios_producto == 7 ||
                    $this->Settings->prioridad_precios_producto == 11 ||
                    $this->Settings->prioridad_precios_producto == 10
                ) : ?>

                    <li id="system_settings_price_groups">
                        <a href="<?= admin_url('system_settings/update_product_unit_prices_general') ?>"><?= lang('update_product_unit_prices_general'); ?> </a>
                    </li>
                <?php endif ?>


                <?php if (
                    $this->Settings->prioridad_precios_producto == 5 ||
                    $this->Settings->prioridad_precios_producto == 7
                    // ||
                    // $this->Settings->prioridad_precios_producto == 10
                ) : ?>

                    <!-- <li id="system_settings_price_groups">
                        <a href="<?= admin_url('system_settings/update_products_unit_prices') ?>"><?= lang('update_products_unit_prices'); ?> </a>
                    </li> -->
                <?php endif ?>

                <?php if ($this->Settings->prioridad_precios_producto == 11) : ?>
                    <li id="system_settings_price_groups">
                        <a href="<?= admin_url('system_settings/update_products_hybrid_prices') ?>"><?= lang('update_products_hybrid_prices'); ?> </a>
                    </li>
                <?php endif ?>

                <li id="system_settings_update_product_prices_per_margin">
                    <a href="<?= admin_url('system_settings/update_product_prices_per_margin') ?>"> <?= lang('update_product_prices_per_margin') ?></a>
                </li>

                <li id="system_settings_w_units">
                    <a href="<?= admin_url('system_settings/w_units') ?>"> Precios por Unidad</a>
                </li>
                <li id="system_settings_categories">
                    <a href="<?= admin_url('system_settings/categories') ?>"><?= lang('categories'); ?></a>
                </li>

                <li id="system_settings_expense_categories">
                    <a href="<?= admin_url('system_settings/expense_categories') ?>"><?= lang('expense_categories'); ?></a>
                </li>

                <li id="system_settings_units">
                    <a href="<?= admin_url('system_settings/units') ?>"><?= lang('units'); ?></a>
                </li>

                <li id="system_settings_brands">
                    <a href="<?= admin_url('system_settings/brands') ?>"><?= lang('brands'); ?></a>
                </li>

                <li id="system_settings_variants">
                    <a href="<?= admin_url('system_settings/variants') ?>"><?= lang('product_variants'); ?></a>
                </li>

                <li id="system_settings_colors">
                    <a href="<?= admin_url('system_settings/colors') ?>"><?= lang('product_colors'); ?></a>
                </li>

                <li id="system_settings_materials">
                    <a href="<?= admin_url('system_settings/materials') ?>"><?= lang('product_materials'); ?></a>
                </li>

                <li id="system_settings_tax_rates">
                    <a href="<?= admin_url('system_settings/tax_rates') ?>"><?= lang('tax_rates'); ?></a>
                </li>

                <li id="system_settings_warehouses">
                    <a href="<?= admin_url('system_settings/warehouses') ?>"><?= lang('warehouses'); ?></a>
                </li>

                <li id="system_settings_email_templates">
                    <a href="<?= admin_url('system_settings/email_templates') ?>"><?= lang('email_templates'); ?></a>
                </li>

                <li id="system_settings_user_groups">
                    <a href="<?= admin_url('system_settings/user_groups') ?>"><?= lang('group_permissions'); ?></a>
                </li>

                <li id="system_settings_withholding_tax">
                    <a href="<?= admin_url("system_settings/withholding_tax"); ?>"><?= lang("withholding_tax"); ?></a>
                </li>

                <?php if ($this->Settings->modulary  == YES) : ?>
                    <li id="system_settings_types_movement">
                        <a href="<?= admin_url("system_settings/types_movement"); ?>"><?= lang("types_movement"); ?></a>
                    </li>

                    <li id="system_settings_commercial_discounts">
                        <a href="<?= admin_url("system_settings/commercial_discounts"); ?>"><?= lang("commercial_discounts"); ?></a>
                    </li>
                <?php endif ?>

                <li id="system_settings_invoice_notes">
                    <a href="<?= admin_url("system_settings/invoice_notes"); ?>"><?= lang("invoice_notes"); ?></a>
                </li>

                <li id="system_settings_backups">
                    <a href="<?= admin_url('system_settings/backups') ?>"><?= lang('backups'); ?></a>
                </li>

                <li id="system_settings_payment_methods">
                    <a href="<?= admin_url('system_settings/payment_methods') ?>"><?= lang('payment_methods'); ?></a>
                </li>

                <?php if ($this->pos_settings->restobar_mode == YES) : ?>
                    <li id="system_settings_restobar">
                        <a href="<?= admin_url("system_settings/restobar"); ?>"><?= lang("restobar"); ?></a>
                    </li>
                <?php endif ?>

                <?php if ($this->Owner && $this->Settings->years_database_management == 2) : ?>
                    <li id="system_settings_close_year">
                        <a href="<?= admin_url('system_settings/close_year') ?>"> <?= lang('close_year'); ?> </a>
                    </li>
                <?php endif ?>
                <li id="system_settings_product_preferences">
                    <a href="<?= admin_url('system_settings/product_preferences') ?>"> <?= lang('product_preferences'); ?> </a>
                </li>
                <li id="system_settings_ubication">
                    <a href="<?= admin_url('system_settings/ubication') ?>"> <?= lang('ubication'); ?> </a>
                </li>
                <li id="system_settings_update_ubication_shipping_cost">
                    <a href="<?= admin_url('system_settings/update_ubication_shipping_cost') ?>"> <?= lang('update_ubication_shipping_cost'); ?> </a>
                </li>
                <li id="system_settings_notes_concept_set">
                    <a href="<?= admin_url('system_settings/configuration_concept_notes') ?>"> <?= lang('configuration_concept_notes'); ?> </a>
                </li>
                <?php if ($this->Settings->modulary) : ?>
                    <li id="system_settings_cost_center">
                        <a href="<?= admin_url('system_settings/cost_centers') ?>"> <?= lang('cost_centers'); ?> </a>
                    </li>
                    <li id="system_settings_paccounts">
                        <a href="<?= admin_url('system_settings/paccount_settings') ?>"> <?= lang('paccount_settings'); ?> </a>
                    </li>
                <?php endif ?>

                <?php if ($this->Owner || $this->Admin) { ?>
                    <li id="system_settings_instances">
                        <a href="<?= admin_url("system_settings/instances") ?>"> <?= $this->lang->line("instances") ?></a>
                    </li>

                    <li id="system_settings_tags">
                        <a href="<?= admin_url("system_settings/tags") ?>"> <?= $this->lang->line("tags") ?></a>
                    </li>
                <?php } ?>
            </ul>
        </li>

        <?php if ($this->Settings->hotel_module_management == 1 && ($this->Owner || $this->Admin)) : ?>
            <li class="mm_hotel">
                <a href="#">
                    <i class="fa fa-building"></i>
                    <span class="nav-label"> <?= lang('hotel'); ?> </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="hotel_guest_registers">
                        <a href="<?= admin_url('hotel/guest_registers') ?>"><?= lang('guest_registers'); ?></a>
                    </li>
                    <li id="hotel_add_guest_register">
                        <a href="<?= admin_url('hotel/add_guest_register') ?>"><?= lang('add_guest_register'); ?></a>
                    </li>
                    <li id="hotel_guests">
                        <a href="<?= admin_url('hotel/guests') ?>"><?= lang('guests'); ?></a>
                    </li>
                    <li id="hotel_add_guest">
                        <a href="<?= admin_url('hotel/add_guest') ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_guest'); ?></a>
                    </li>
                </ul>
            </li>
        <?php endif ?>

        <?php if ($this->Owner || $this->Admin) : ?>
            <li class="mm_wappsi_invoicing">
                <a href="#">
                    <i class="fas fa-file-invoice-dollar"></i>
                    <span class="nav-label"> <?= lang('wappsi_invoicing'); ?> </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="hotel_guest_registers">
                        <a href="<?= admin_url('wappsi_invoicing') ?>"><?= lang('wappsi_invoicing'); ?></a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li id="hotel_guest_registers">
                        <a href="<?= admin_url('wappsi_invoicing/electronic_hits') ?>"><?= lang('electronic_hits'); ?></a>
                    </li>
                </ul>
            </li>
        <?php endif ?>
    <?php } ?>

    <?php if (
        $this->Settings->hotel_module_management == 1 &&
        ($this->Admin || $this->Owner || ($GP['hotel-guests'] || $GP['hotel-add_guest'] || $GP['hotel-edit_guest'] ||
                    $GP['hotel-guest_registers'] || $GP['hotel-add_guest_register'] || $GP['hotel-edit_guest_register']))) : ?>
        <li class="mm_hotel">
            <a href="#">
                <i class="fa fa-building"></i>
                <span class="nav-label"> <?= lang('hotel'); ?> </span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse">
                <?php if ($GP['hotel-guest_registers']) : ?>
                    <li id="hotel_guest_registers">
                        <a href="<?= admin_url('hotel/guest_registers') ?>"><?= lang('guest_registers'); ?></a>
                    </li>
                <?php endif ?>
                <?php if ($GP['hotel-add_guest_register']) : ?>
                    <li id="hotel_add_guest_register">
                        <a href="<?= admin_url('hotel/add_guest_register') ?>"><?= lang('add_guest_register'); ?></a>
                    </li>
                <?php endif ?>
                <?php if ($GP['hotel-guests']) : ?>
                    <li id="hotel_guests">
                        <a href="<?= admin_url('hotel/guests') ?>"><?= lang('guests'); ?></a>
                    </li>
                <?php endif ?>
                <?php if ($GP['hotel-add_guest']) : ?>
                    <li id="hotel_add_guest">
                        <a href="<?= admin_url('hotel/add_guest') ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_guest'); ?></a>
                    </li>
                <?php endif ?>
            </ul>
        </li>
    <?php endif ?>

    <?php if ($Owner || $Admin) { ?>
        <li class="mm_notifications">
            <a href="<?= admin_url('notifications'); ?>">
                <i class="fa fa-info-circle"></i>
                <span class="nav-label"> <?= lang('notifications'); ?></span>
            </a>
        </li>
    <?php } else { ?>
        <?php if ($GP['products-index'] || $GP['products-add'] || $GP['products-barcode'] || $GP['products-adjustments'] || $GP['products-stock_count'] || $GP['products-import_csv']) { ?>
            <li class="mm_products">
                <a href="#">
                    <i class="fas fa-box"></i>
                    <span class="nav-label"><?= lang('products'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="products_index">
                        <a href="<?= admin_url('products'); ?>"><?= lang('list_products'); ?></a>
                    </li>

                    <?php if ($GP['products-add']) { ?>
                        <li id="products_add">
                            <a href="<?= admin_url('products/add'); ?>"><?= lang('add_product'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['products-import_csv']) { ?>
                        <li id="products_import_csv">
                            <a href="<?= admin_url('products/import_csv'); ?>"><?= lang('import_products'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['products-barcode']) { ?>
                        <li id="products_sheet">
                            <a href="<?= admin_url('products/print_barcodes'); ?>"><?= lang('print_barcode_label'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['products-adjustments']) { ?>
                        <li id="products_quantity_adjustments">
                            <a href="<?= admin_url('products/quantity_adjustments'); ?>"><?= lang('quantity_adjustments'); ?></a>
                        </li>
                        <li id="products_add_adjustment">
                            <a href="<?= admin_url('products/add_adjustment'); ?>"><?= lang('add_adjustment'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['products-production_orders']) { ?>
                        <li id="products_production_orders">
                            <a href="<?= admin_url('products/production_orders'); ?>"><?= lang('production_orders'); ?></a>
                        </li>
                    <?php } ?>
                    <?php if ($GP['products-add_production_order']) { ?>
                        <li id="products_add_production_order">
                            <a href="<?= admin_url('products/add_production_order'); ?>"><?= lang('add_production_order'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['products-stock_count']) { ?>
                        <li id="products_stock_counts">
                            <a href="<?= admin_url('products/stock_counts'); ?>"><?= lang('list_stock_counts'); ?></a>
                        </li>
                        <li id="products_count_stock">
                            <a href="<?= admin_url('products/count_stock'); ?>"><?= lang('add_count_stock'); ?></a>
                        </li>
                        <li id="products_count_stock_variants">
                            <a href="<?= admin_url('products/count_stock_variants'); ?>"><?= lang('add_count_stock_variants'); ?></a>
                        </li>
                        <li id="products_count_stock_variants">
                            <a href="<?= admin_url('products/add_express_count'); ?>"><?= lang('add_express_count'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['products-product_transformations']) { ?>
                        <li id="products_production_orders">
                            <a href="<?= admin_url('products/product_transformations'); ?>"><?= lang('product_transformations'); ?></a>
                        </li>
                        <?php if ($GP['products-add_product_transformation']) { ?>
                            <li id="products_production_orders">
                                <a href="<?= admin_url('products/add_product_transformation'); ?>"><?= lang('add_product_transformation'); ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>


                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['production_order-index'] || $GP['production_order-cutting_orders'] || $GP['production_order-assemble_orders'] || $GP['production_order-packing_orders'] || $GP['production_order-production_order_report']) : ?>
            <li class="mm_production_order">
				<a href="#">
					<i class="fas fa-file-signature"></i>
					<span class="nav-label"> <?= lang('making_orders'); ?> </span>
					<span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level collapse">
					<?php if ($GP['production_order-index']) : ?>
						<li id="production_order_add">
							<a href="<?= admin_url('production_order'); ?>"><?= lang('list_making_orders'); ?></a>
						</li>
						<?php if ($GP['production_order-add']) : ?>
							<li id="production_order_add">
								<a href="<?= admin_url('production_order/add'); ?>"><?= lang('add_making_order'); ?></a>
							</li>
						<?php endif ?>
					<?php endif ?>
					<?php if ($GP['production_order-cutting_orders']) : ?>
						<li id="production_order_cutting_orders">
							<a href="<?= admin_url('production_order/cutting_orders'); ?>"><?= lang('list_cutting_orders'); ?></a>
						</li>
						<?php if ($GP['production_order-add_cutting_order']) : ?>
							<li id="production_order_add_cutting_order">
								<a href="<?= admin_url('production_order/add_cutting_order'); ?>"><?= lang('add_cutting_order'); ?></a>
							</li>
						<?php endif ?>
					<?php endif ?>
					<?php if ($GP['production_order-assemble_orders']) : ?>
						<li id="production_order_assemble_orders">
							<a href="<?= admin_url('production_order/assemble_orders'); ?>"><?= lang('list_assemble_orders'); ?></a>
						</li>
						<?php if ($GP['production_order-add_assemble_order']) : ?>
							<li id="production_order_add_assemble_order">
								<a href="<?= admin_url('production_order/add_assemble_order'); ?>"><?= lang('add_assemble_order'); ?></a>
							</li>
						<?php endif ?>
					<?php endif ?>
					<?php if ($GP['production_order-packing_orders']) : ?>
						<li id="production_order_packing_orders">
							<a href="<?= admin_url('production_order/packing_orders'); ?>"><?= lang('list_packing_orders'); ?></a>
						</li>
						<?php if ($GP['production_order-add_packing_order']) : ?>
							<li id="production_order_add_packing_order">
								<a href="<?= admin_url('production_order/add_packing_order'); ?>"><?= lang('add_packing_order'); ?></a>
							</li>
						<?php endif ?>
					<?php endif ?>
					<?php if ($GP['production_order-production_order_report']) : ?>
						<li id="reports_production_order_report">
							<a href="<?= admin_url('reports/production_order_report') ?>"><?= lang('production_order_report'); ?></a>
						</li>
					<?php endif ?>
				</ul>
			</li>
        <?php endif ?>

        <?php if ($GP['pos-sales'] || $GP['pos-index'] || $GP['purchases-expenses'] || $GP['pos_print_server'] || $GP['pos-order_preparation'] || $GP['pos-see_order_preparation']) { ?>
            <li class="mm_sales <?= strtolower($this->router->fetch_method()) == 'sales' ? 'mm_pos' : '' ?>">
                <a href="#">
                    <i class="fas fa-store"></i>
                    <span class="nav-label"><?= lang('pos_sales'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <?php if ($GP['pos-sales']) : ?>
                        <li id="pos_sales">
                            <a href="<?= admin_url('pos/sales'); ?>"><?= lang('pos_sales_list'); ?></a>
                        </li>
                        <li id="pos_fe_index">
                            <a href="<?= admin_url("pos/fe_index") ?>"><?= $this->lang->line("pos_fe_index") ?></a>
                        </li>
                    <?php endif ?>
                    <?php if ($this->pos_settings->restobar_mode == '1') { ?>
                        <?php if ($this->pos_settings->order_print_mode == '3' && $GP['pos-order_preparation']) { ?>
                            <li>
                                <a href="<?= admin_url('pos/pa_print_server') ?>"><?= lang('pa_print_server'); ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($GP['pos-index']) : ?>
                        <?php if ($this->Settings->cashier_close == 2) { ?>
                            <li class="disabled tip" title="<?= lang('cash_register_not_used') ?>">
                                <a><?= lang('pos_sales_add'); ?></a>
                            </li>
                        <?php } else { ?>
                            <li>
                                <a href="<?= admin_url('pos') ?>"><?= lang('pos_sales_add'); ?></a>
                            </li>
                        <?php } ?>
                        <li>
                            <a id="opened_bills" data-toggle="modal" data-target="#myModal" href="<?= admin_url('pos/opened_bills') ?>" data-toggle="ajax"><?= lang('suspended_sales') ?></a>
                        </li>

                        <?php if ($GP['pos-close_register']) : ?>
                            <?php if ($this->Settings->cashier_close == 1 && !$this->session->userdata('register_cash_movements_with_another_user')) { ?>
                                <?php if ($this->session->userdata('register_open_time') != null) { ?>
                                    <li>
                                        <a id="close_register" href="<?= admin_url('pos/close_register/?v=' . $this->v . '&m=' . $this->m) ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><?= lang('close_register') ?></a>
                                    </li>
                                <?php } else if ($this->Settings->cashier_close == 2) { ?>
                                    <li class="disabled tip" title="<?= lang('cash_register_not_used') ?>">
                                        <a id="close_register"><?= lang('close_register') ?></a>
                                    </li>
                                <?php } else { ?>
                                    <li class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                                        <a id="close_register"><?= lang('close_register') ?></a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        <?php endif ?>
                    <?php endif ?>

                    <?php if ($this->pos_settings->restobar_mode == '1') { ?>
                        <?php if ($this->pos_settings->order_print_mode == '0' && $GP['pos-order_preparation']) { ?>
                            <li>
                                <a id="order_preparation" href="<?= admin_url('pos/order_preparation') ?>"><?= lang('order_preparation') ?></a>
                            </li>
                        <?php } ?>
                        <?php if ($GP['pos-see_order_preparation']) { ?>
                            <li id="pos_see_order_preparation">
                                <a id="see_order_preparation" href="<?= admin_url('pos/see_order_preparation') ?>"><?= lang('see_order_preparation') ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($GP['pos-add_wholesale']) : ?>
                        <li id="pos_add_wholesale">
                            <a href="<?= admin_url('pos/add_wholesale'); ?>"><?= lang('pos_wholesale'); ?></a>
                        </li>
                    <?php endif ?>

                    <?php if ($GP['purchases-expenses']) { ?>
                        <?php if ($this->session->userdata('register_open_time') != null) { ?>
                            <li id="purchases_expenses">
                                <a href="<?= admin_url('purchases/expenses'); ?>"><?= lang('list_expenses'); ?></a>
                            </li>
                            <li id="purchases_add_expense">
                                <a href="<?= admin_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_expense'); ?></a>
                            </li>
                        <?php } else if ($this->Settings->cashier_close == 2) { ?>
                            <li id="purchases_expenses">
                                <a href="<?= admin_url('purchases/expenses'); ?>"><?= lang('list_expenses'); ?></a>
                            </li>
                            <li id="purchases_add_expense">
                                <a href="<?= admin_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_expense'); ?></a>
                            </li>
                        <?php } else { ?>
                            <li id="purchases_expenses" class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                                <a><?= lang('list_expenses'); ?></a>
                            </li>
                            <li id="purchases_add_expense" class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                                <a><?= lang('add_expense'); ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($GP['pos-pos_register_movements']) { ?>
                        <li id="pos_pos_register_movements">
                            <a href="<?= admin_url('pos/pos_register_movements'); ?>"><?= lang('pos_register_movements'); ?></a>
                        </li>
                        <?php if ($GP['pos-pos_register_add_movement']) { ?>
                            <?php if ($this->session->userdata('register_open_time') != null) { ?>
                                <li id="pos_register_add_movement">
                                    <a href="<?= admin_url('pos/pos_register_add_movement'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('pos_register_add_movement'); ?></a>
                                </li>
                            <?php } else if ($this->Settings->cashier_close == 2) { ?>
                                <li id="pos_register_add_movement">
                                    <a href="<?= admin_url('pos/pos_register_add_movement'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('pos_register_add_movement'); ?></a>
                                </li>
                            <?php } else { ?>
                                <li id="pos_register_add_movement" class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                                    <a><?= lang('pos_register_add_movement'); ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($GP['payments_collections-index']) { ?>
                        <li id="payments_collections_index">
                            <a href="<?= admin_url('payments_collections/index'); ?>"><?= lang('payments_collections'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['payments_collections-add']) { ?>
                        <?php if ($this->session->userdata('register_open_time')): ?>
                            <li id="payments_collections_add">
                                <a href="<?= admin_url('payments_collections/add'); ?>" data-toggle="modal" data-target="#myModal"><?= sprintf(lang('payments_collections_add'), lang('payment_collection')); ?></a>
                            </li>
                        <?php else: ?>
                            <li class="disabled tip" title="<?= lang('no_data_pos_register') ?>">
                                <a id="payments_collections_add"><?= sprintf(lang('payments_collections_add'), lang('payment_collection')) ?></a>
                            </li>
                        <?php endif ?>
                    <?php } ?>

                    <?php if ($GP['pos_print_server']) { ?>
                        <li id="pos_print_server">
                            <a href="<?= admin_url('pos/pos_print_server'); ?>" target="_blank"><?= lang('pos_print_server'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['sales-index'] || $GP['sales-add'] || $GP['sales-deliveries'] || $GP['sales-gift_cards'] || $GP['customers-list_deposits'] || $GP["warranty-index"] || $GP['debit_notes-add']) { ?>
            <li class="mm_sales <?= strtolower($this->router->fetch_method()) == 'sales' ? 'mm_pos' : '' ?>">
                <a href="#">
                    <i class="fas fa-store"></i>
                    <span class="nav-label"> <?= lang('sales'); ?> </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <?php if ($GP['sales-index']) : ?>
                        <li id="sales_index">
                            <a href="<?= admin_url('sales'); ?>"><?= lang('list_sales'); ?></a>
                        </li>
                    <?php endif ?>

                    <?php if ($GP['sales-fe_index']) { ?>
                        <li id="sales_fe_index">
                            <a href="<?= admin_url('sales/fe_index'); ?>"><?= lang('list_sales_fe'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['sales-add']) { ?>
                        <li id="sales_add">
                            <a href="<?= admin_url('sales/add'); ?>"><?= lang('add_sale'); ?></a>
                        </li>
                    <?php }

                    if ($GP['sales-deliveries']) { ?>
                        <li id="sales_deliveries">
                            <a href="<?= admin_url('sales/deliveries'); ?>"><?= lang('deliveries'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['sales-gift_cards']) { ?>
                        <li id="sales_gift_cards">
                            <a href="<?= admin_url('sales/gift_cards'); ?>"><?= lang('gift_cards'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['payments-index']) { ?>
                        <li class="dropdown">
                            <a href="<?= admin_url('payments') ?>"><?= lang('multi_payments') ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['sales-return_sales']) { ?>
                        <!-- <li id="sales_return_sale">
                            <a href="<?= admin_url('sales/return_sale'); ?>"> <?= lang('return_sale'); ?> </a>
                        </li> -->
                    <?php } ?>

                    <?php if ($GP['returns-credit_note_other_concepts']) { ?>
                        <li id="returns_credit_note_other_concepts">
                            <a href="<?= admin_url('returns/credit_note_other_concepts'); ?>"> <?= lang('credit_note_other_concepts_label_menu'); ?> </a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['debit_notes-add']) { ?>
                        <li class="debit_notes_add">
                            <a href="<?= admin_url('debit_notes/add'); ?>">
                                <span class="nav-label"> <?= lang('debit_note_label_menu'); ?></span>
                            </a>
                        </li>
                    <?php } ?>

                    <?php if ($this->deposit_payment_method['sale'] == true) : ?>
                        <?php if ($GP['customers-list_deposits']) : ?>
                            <li id="customers_list_deposits">
                                <a href="<?= admin_url('customers/list_deposits'); ?>"> <?= lang('list_deposits'); ?></a>
                            </li>
                        <?php endif ?>
                    <?php endif ?>

                    <?php if ($GP['returns-add_past_years_sale_return']) { ?>
                        <li id="returns_add_past_years_sale_return">
                            <a href="<?= admin_url('returns/add_past_years_sale_return'); ?>"><?= lang('add_past_years_sale_return'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP["warranty-index"]) { ?>
                        <li id="warranty_index">
                            <a href="<?= admin_url('warranty') ?>"><?= $this->lang->line('list_warranty') ?></a>
                        </li>
                    <?php } ?>
                    <?php if ($GP["warranty-add"]) { ?>
                        <li id="warranty_add">
                            <a href="<?= admin_url('warranty/add') ?>"><?= $this->lang->line('add_warranty') ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP["budget-index"]) { ?>
                    <li id="budget_index">
                        <a href="<?= admin_url('budget') ?>"><?= $this->lang->line('budget') ?></a>
                    </li>
                    <?php } ?>

                    <?php if ($GP["sales_reports-budget_execution"]) { ?>
                    <li class="mm_sales_reports">
                        <a href="#">
                            <?= lang('reports'); ?>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level">
                            <li id="sales_reports_budget_execution">
                                <a href="<?= admin_url('sales_reports/budget_execution'); ?>"><?= lang('sales_budget_execution'); ?></a>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if ($GP["sales-print_server"]) { ?>
                        <li id="print_server">
                            <a href="<?= admin_url('sales/printServer') ?>"><?= $this->lang->line('print_server') ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['sales-orders']) : ?>
            <li class="mm_orders">
                <a href="#">
                    <i class="fas fa-file-signature"></i>
                    <span class="nav-label"> <?= lang('orders'); ?> </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="order_add">
                        <a href="<?= admin_url('sales/orders'); ?>"><?= lang('list_orders'); ?></a>
                    </li>
                    <?php if ($GP['sales-add_order']) : ?>
                        <li id="order_add">
                            <a href="<?= admin_url('sales/add_order'); ?>"><?= lang('add_order'); ?></a>
                        </li>
                    <?php endif ?>
                </ul>
            </li>
        <?php endif ?>

        <?php if ($GP['quotes-index'] || $GP['quotes-add']) { ?>
            <li class="mm_quotes">
                <a href="#">
                    <i class="fa fa-heart-o"></i>
                    <span class="nav-label"><?= lang('quotes'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="sales_index">
                        <a href="<?= admin_url('quotes'); ?>"><?= lang('list_quotes'); ?></a>
                    </li>

                    <?php if ($GP['quotes-add']) { ?>
                        <li id="sales_add">
                            <a href="<?= admin_url('quotes/add'); ?>"><?= lang('add_quote'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['purchases-index'] || $GP['purchases-add'] || $GP['purchases-expenses'] || $GP["payments-pindex"] || $GP["suppliers-list_deposits"] || $GP['returns-add_past_years_purchase_return']) { ?>
            <li class="mm_purchases">
                <a href="#">
                    <i class="fas fa-truck"></i>
                    <span class="nav-label"><?= lang('purchases'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="purchases_index">
                        <a class="submenu" href="<?= admin_url('purchases'); ?>"><?= lang('list_purchases'); ?></a>
                    </li>

                    <?php if ($GP['purchases-add']) { ?>
                        <li id="purchases_add">
                            <a href="<?= admin_url('purchases/add'); ?>"><?= lang('add_purchase'); ?></a>
                        </li>
                        <li id="purchases_add">
                            <a href="<?= admin_url('purchases/add_support_document'); ?>"><?= lang('add_support_document'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($this->Settings->supportingDocument == YES) : ?>
                        <?php if ($GP['supportingDocument-index']) : ?>
                            <li id="purchases_supporting_document_index">
                                <a href="<?= admin_url('purchases/supporting_document_index'); ?>"><?= lang('purchases_list_supporting_document'); ?></a>
                            </li>
                        <?php endif ?>
                        <?php if ($GP['supportingDocument-add']) : ?>
                            <li id="purchases_add_support_document">
                                <a href="<?= admin_url('purchases/add_support_document'); ?>"><?= lang('add_support_document'); ?></a>
                            </li>
                        <?php endif ?>
                    <?php endif ?>

                    <?php if ($GP['quotes-add']) { ?>
                        <li id="purchases_quotes">
                            <a href="<?= admin_url('quotes/p_index'); ?>"> <?= lang('quotes_purchases'); ?></a>
                        </li>
                        <li id="purchases_add_quote">
                            <a href="<?= admin_url('quotes/addqpurchase'); ?>"> <?= lang('add_quote_purchase'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['payments-pindex']) : ?>
                        <li class="dropdown">
                            <a href="<?= admin_url('payments/pindex') ?>">
                                <?= lang('multi_payments_purchases') ?>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php if ($GP['suppliers-list_deposits']) : ?>
                        <li class="dropdown">
                            <a href="<?= admin_url('suppliers/list_deposits') ?>">
                                <?= lang('list_deposits_suppliers') ?>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php if ($GP['returns-add_past_years_purchase_return']) : ?>
                        <li class="dropdown">
                            <a href="<?= admin_url('returns/add_past_years_purchase_return') ?>">
                                <?= lang('add_past_years_purchase_return') ?>
                            </a>
                        </li>
                    <?php endif ?>


                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['financing-index'] || $GP['financing-add'] || $GP['financing-edit'] || $GP['financing-delete'] || $GP['financing-make_collections']) { ?>
            <?php if ($GP['financing-make_collections']) { ?>
                <li class="mm_financing">
                    <a href="#">
                        <i class="fas fa-store"></i>
                        <span class="nav-label"> <?= lang('credit_financing_language'); ?> </span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li id="financing_list">
                            <a href="<?= admin_url('financing') ?>"><?= $this->lang->line('financing_list') ?></a>
                        </li>
                        <li id="financing_installmentList">
                            <a href="<?= admin_url('financing/installmentList') ?>"><?= $this->lang->line('installment_list') ?></a>
                        </li>
                        <li id="financing_paymentlist">
                            <a href="<?= admin_url('financing/paymentList') ?>"><?= $this->lang->line('payment_list') ?></a>
                        </li>
                        <?php if ($GP['financing-make_collections']) { ?>
                            <li id="financing_payment_financing_fee">
                                <a href="<?= admin_url('financing/paymentFinancingFee') ?>"><?= $this->lang->line('payment_financing_fee') ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        <?php } ?>

        <?php if ($GP['transfers-index'] || $GP['transfers-add'] || $GP['transfers-orders'] || $GP['transfers-add_order']) { ?>
            <li class="mm_transfers">
                <a href="#">
                    <i class="fa fa-star-o"></i>
                    <span class="nav-label"> <?= lang('transfers'); ?> </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="transfers_index">
                        <a href="<?= admin_url('transfers'); ?>"><?= lang('list_transfers'); ?></a>
                    </li>
                    <?php if ($GP['transfers-add']) { ?>
                        <li id="transfers_add">
                            <a href="<?= admin_url('transfers/add'); ?>"><?= lang('add_transfer'); ?></a>
                        </li>
                    <?php } ?>
                    <?php if ($GP['transfers-orders']) { ?>
                        <li id="transfers_orders">
                            <a href="<?= admin_url('transfers/orders'); ?>"><?= lang('transfers_orders'); ?></a>
                        </li>
                    <?php } ?>
                    <?php if ($GP['transfers-add_order']) { ?>
                        <li id="transfers_add_order">
                            <a href="<?= admin_url('transfers/add_order'); ?>"><?= lang('add_transfer_order'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <!-- <?php if ($GP['returns-index'] || $GP['returns-add_past_years_sale_return']) { ?>
            <li class="mm_returns">
                <a href="#">
                    <i class="fa fa-random"></i>
                    <span class="nav-label"><?= lang('returns'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="returns_index">
						<a href="<?= admin_url('returns'); ?>"><?= lang('list_returns'); ?></a>
					</li>

                    <?php if ($GP['returns-add']) { ?>
						<li id="returns_add">
							<a href="<?= admin_url('returns/add'); ?>"><?= lang('add_return'); ?></a>
						</li>
					<?php } ?>
                    <?php if ($GP['returns-add_past_years_sale_return']) { ?>
                        <li id="returns_add_past_years_sale_return">
                            <a href="<?= admin_url('returns/add_past_years_sale_return'); ?>"><?= lang('add_past_years_sale_return'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?> -->

        <?php if ($GP['customers-index'] || $GP['customers-add'] || $GP['suppliers-index'] || $GP['suppliers-add'] || $GP['auth-users'] || $GP['auth-create_user']) { ?>
            <li class="mm_auth mm_customers mm_suppliers mm_billers">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span class="nav-label"><?= lang('people'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <?php if ($GP['auth-users']) { ?>
                        <li id="auth_users">
                            <a href="<?= admin_url('users'); ?>"><?= lang('list_users'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['auth-create_user']) { ?>
                        <li id="auth_create_user">
                            <a href="<?= admin_url('admin/create_user'); ?>"><?= lang('create_user'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['customers-index']) { ?>
                        <li id="customers_index">
                            <a href="<?= admin_url('customers'); ?>"><?= lang('list_customers'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['customers-add']) { ?>
                        <li id="customers_index">
                            <a href="<?= admin_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_customer'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['suppliers-index']) { ?>
                        <li id="suppliers_index">
                            <a href="<?= admin_url('suppliers'); ?>"><?= lang('list_suppliers'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['suppliers-add']) { ?>
                        <li id="suppliers_index">
                            <a href="<?= admin_url('suppliers/add'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_supplier'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['suppliers-index']) { ?>
                        <li id="suppliers_index">
                            <a href="<?= admin_url('creditors'); ?>"><?= lang('list_creditors'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['suppliers-add']) { ?>
                        <li id="suppliers_index">
                            <a href="<?= admin_url('creditors/add'); ?>" data-toggle="modal" data-target="#myModal"><?= lang('add_creditor'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['shop_settings-index'] || $GP['shop_settings-slider'] || $GP['shop_settings-pages'] || $GP['shop_settings-add_page'] || $GP['shop_settings-sms_settings'] || $GP['shop_settings-send_sms'] || $GP['shop_settings-sms_log']) { ?>
            <li class="mm_shop_settings mm_api_settings">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="nav-label"><?= lang('front_end'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <?php if ($GP['shop_settings-index']) { ?>
                        <li id="shop_settings_index">
                            <a href="<?= admin_url('shop_settings') ?>"><?= lang('shop_settings'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['shop_settings-slider']) { ?>
                        <li id="shop_settings_slider">
                            <a href="<?= admin_url('shop_settings/slider') ?>"><?= lang('slider_settings'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['shop_settings-pages']) { ?>
                        <li id="shop_settings_pages">
                            <a href="<?= admin_url('shop_settings/pages') ?>"><?= lang('list_pages'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['shop_settings-add_page']) : ?>
                        <li id="shop_settings_pages">
                            <a href="<?= admin_url('shop_settings/add_page') ?>"><?= lang('add_page'); ?></a>
                        </li>
                    <?php endif ?>

                    <?php if ($GP['shop_settings-sms_settings']) { ?>
                        <li id="shop_settings_sms_settings">
                            <a href="<?= admin_url('shop_settings/sms_settings') ?>"><?= lang('sms_settings'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['shop_settings-send_sms']) { ?>
                        <li id="shop_settings_send_sms">
                            <a href="<?= admin_url('shop_settings/send_sms') ?>"><?= lang('send_sms'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['shop_settings-sms_log']) { ?>
                        <li id="shop_settings_sms_log">
                            <a href="<?= admin_url('shop_settings/sms_log') ?>"><?= lang('sms_log'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['reports-warehouse_stock'] || $GP['reports-best_sellers'] || $GP['reports-register'] || $GP['reports-quantity_alerts'] || $GP['reports-products'] || $GP['reports-valued_products'] || $GP['reports-adjustments'] || $GP['reports-categories'] || $GP['reports-brands'] || $GP['reports-daily_sales'] || $GP['reports-monthly_sales'] || $GP['reports-sales'] || $GP['reports-payments'] || $GP['reports-tax'] || $GP['reports-profit_loss'] || $GP['reports-daily_purchases'] || $GP['reports-monthly_purchases'] || $GP['reports-purchases'] || $GP['reports-expenses'] || $GP['reports-customers'] || $GP['reports-suppliers'] || $GP['reports-users'] || $GP['reports-portfolio'] || $GP['reports-portfolio_report'] || $GP['reports-debts_to_pay_report'] || $GP['reports-load_zeta'] || $GP['reports-load_bills'] || $GP['reports-load_rentabilidad_doc'] || $GP['reports-load_rentabilidad_customer'] || $GP['reports-quantity_alerts'] || $GP['reports-collection_commissions'] || $GP['system_settings-user_activities'] || $GP['reports-biller_register'] || $GP['reports-serial_register'] || $GP['reports-load_award_points'] || $GP['reports-daily_incomes']) { ?>
            <li class="mm_reports">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span class="nav-label"><?= lang('reports'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <?php if ($GP['reports-quantity_alerts']) { ?>
                        <li id="reports_quantity_alerts">
                            <a href="<?= admin_url('reports/quantity_alerts') ?>"><?= lang('product_quantity_alerts'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-warehouse_stock']) { ?>
                        <li id="reports_quantity_alerts">
                            <a href="<?= admin_url('reports/warehouse_stock') ?>"><?= lang('warehouse_stock'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-best_sellers']) { ?>
                        <li id="reports_quantity_alerts">
                            <a href="<?= admin_url('reports/best_sellers') ?>"><?= lang('best_sellers'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-register']) { ?>
                        <li id="reports_quantity_alerts">
                            <a href="<?= admin_url('reports/register') ?>"><?= lang('register_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-biller_register']) { ?>
                        <li id="reports_quantity_alerts">
                            <a href="<?= admin_url('reports/biller_register') ?>"><?= lang('biller_register_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-serial_register']) { ?>
                        <li id="reports_quantity_alerts">
                            <a href="<?= admin_url('reports/serial_register') ?>"><?= lang('serial_register_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-load_zeta']) { ?>
                        <li id="reports_load_zeta">
                            <a href="<?= admin_url('reports/load_zeta') ?>"><?= lang('zeta_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-quantity_alerts']) { ?>
                        <li id="reports_quantity_alerts">
                            <a href="<?= admin_url('reports/quantity_alerts') ?>"><?= lang('quantity_alerts'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-products']) { ?>
                        <li id="reports_products">
                            <a href="<?= admin_url('reports/products') ?>"><?= lang('products_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-valued_products']) { ?>
                        <li id="reports_valued_products">
                            <a href="<?= admin_url('reports/valued_products') ?>"><?= lang('valued_products_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-adjustments']) { ?>
                        <li id="reports_adjustments">
                            <a href="<?= admin_url('reports/adjustments') ?>"><?= lang('adjustments_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-categories']) { ?>
                        <li id="reports_categories">
                            <a href="<?= admin_url('reports/categories') ?>"><?= sprintf(sprintf(lang('categories_report'), lang('categories')), lang('categories')); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-brands']) { ?>
                        <li id="reports_brands">
                            <a href="<?= admin_url('reports/brands') ?>"><?= sprintf(lang('brands_report'), lang('brands')); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-daily_sales']) { ?>
                        <li id="reports_daily_sales">
                            <a href="<?= admin_url('reports/daily_sales') ?>"><?= lang('daily_sales'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-monthly_sales']) { ?>
                        <li id="reports_monthly_sales">
                            <a href="<?= admin_url('reports/monthly_sales') ?>"><?= lang('monthly_sales'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-sales']) { ?>
                        <li id="reports_sales">
                            <a href="<?= admin_url('reports/sales') ?>"><?= lang('sales_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-daily_incomes']) { ?>
                        <li id="reports_daily_incomes">
                            <a href="<?= admin_url('reports/daily_incomes') ?>"><?= lang('daily_incomes_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-payments']) { ?>
                        <li id="reports_payments">
                            <a href="<?= admin_url('reports/payments') ?>"><?= lang('payments_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-tax']) { ?>
                        <li id="reports_tax">
                            <a href="<?= admin_url('reports/tax') ?>"><?= lang('tax_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-profit_loss']) { ?>
                        <li id="reports_profit_loss">
                            <a href="<?= admin_url('reports/profit_loss') ?>"><?= lang('profit_and_loss'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-daily_purchases']) { ?>
                        <li id="reports_daily_purchases">
                            <a href="<?= admin_url('reports/daily_purchases') ?>"><?= lang('daily_purchases'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-monthly_purchases']) { ?>
                        <li id="reports_monthly_purchases">
                            <a href="<?= admin_url('reports/monthly_purchases') ?>"><?= lang('monthly_purchases'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-purchases']) { ?>
                        <li id="reports_purchases">
                            <a href="<?= admin_url('reports/purchases') ?>"><?= lang('purchases_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-expenses']) { ?>
                        <li id="reports_expenses">
                            <a href="<?= admin_url('reports/expenses') ?>"><?= lang('expenses_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-customers']) { ?>
                        <li id="reports_customers">
                            <a href="<?= admin_url('reports/customers') ?>"><?= lang('customers_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-suppliers']) { ?>
                        <li id="reports_suppliers">
                            <a href="<?= admin_url('reports/suppliers') ?>"><?= lang('creditors_suppliers_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-users']) { ?>
                        <li id="reports_users">
                            <a href="<?= admin_url('reports/users') ?>"><?= lang('staff_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-portfolio']) { ?>
                        <?php if ($this->Settings->big_data_limit_reports == 0) : ?>
                            <li id="reports_portfolio">
                                <a href="<?= admin_url('reports/portfolio') ?>"><?= lang('portfolio_report'); ?></a>
                            </li>
                        <?php endif ?>
                    <?php } ?>

                    <?php if ($GP['reports-portfolio_report']) { ?>
                        <li id="reports_portfolio_report">
                            <a href="<?= admin_url('reports/portfolio_report') ?>"><?= lang('portfolio_by_ages_seller'); ?></a>
                        </li>

                        <li id="reports_portfolio_report">
                            <a href="<?= admin_url('reports/portfolio_report_2') ?>"><?= lang('portfolio_by_ages_customer'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-collection_commissions']) { ?>
                        <li id="reports_staff_report">
                            <a href="<?= admin_url('reports/collection_commissions') ?>"><?= lang('collection_commissions_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-debts_to_pay_report']) { ?>
                        <li id="reports_debts_to_pay_report">
                            <a href="<?= admin_url('reports/debts_to_pay_report') ?>"><?= lang('debts_to_pay_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-load_bills']) { ?>
                        <li id="reports_load_bills">
                            <a href="<?= admin_url('reports/load_bills') ?>"><?= lang('seller_report'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-load_rentabilidad_doc']) { ?>
                        <li id="reports_load_rentabilidad_doc">
                            <a href="<?= admin_url('reports/load_rentabilidad_doc') ?>"><?= lang('profitability_document'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-load_rentabilidad_customer']) { ?>
                        <li id="reports_load_rentabilidad_customer">
                            <a href="<?= admin_url('reports/load_rentabilidad_customer') ?>"><?= lang('profitability_customer'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-load_rentabilidad_producto']) { ?>
                        <li id="reports_load_rentabilidad_producto">
                            <a href="<?= admin_url('reports/load_rentabilidad_producto') ?>"><?= lang('profitability_product'); ?></a>
                        </li>
                    <?php } ?>

                    <!-- <li id="reports_load_inventario_bodega_variantes">
						<a href="<?= admin_url('reports/warehouse_inventory_variants') ?>"><?= lang('warehouse_inventory_variants'); ?></a>
					</li> -->

                    <?php if ($GP['system_settings-user_activities']) { ?>
                        <li id="reports_user_activities">
                            <a href="<?= admin_url('reports/user_activities') ?>"><?= lang('user_activities'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-load_award_points']) { ?>
                        <li id="reports_award_points">
                            <a href="<?= admin_url('reports/award_points') ?>"><?= lang('award_points'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['reports-transfers']) { ?>
                        <li id="reports_transfers">
                            <a href="<?= admin_url('reports/transfers') ?>"><?= lang('transfers_report'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['system_settings-categories'] || $GP['system_settings-units'] || $GP['system_settings-brands'] || $GP['system_settings-tax_rates'] || $GP['system_settings-payment_methods'] || $GP['system_settings-update_products_group_prices'] || $GP['expense_categories-index'] || $GP['system_settings-colors'] || $GP['system_settings-materials'] || $GP['system_settings-variants']) { ?>
            <li class="mm_system_settings <?= strtolower($this->router->fetch_method()) == 'sales' ? '' : 'mm_pos' ?>">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span class="nav-label"> <?= lang('settings'); ?> </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <?php if ($GP['system_settings-categories']) { ?>
                        <li id="system_settings_categories">
                            <a href="<?= admin_url('system_settings/categories') ?>"><?= lang('categories'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['system_settings-units']) { ?>
                        <li id="system_settings_units">
                            <a href="<?= admin_url('system_settings/units') ?>"><?= lang('units'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['system_settings-brands']) { ?>
                        <li id="system_settings_brands">
                            <a href="<?= admin_url('system_settings/brands') ?>"><?= lang('brands'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['system_settings-tax_rates']) { ?>
                        <li id="system_settings_tax_rates">
                            <a href="<?= admin_url('system_settings/tax_rates') ?>"><?= lang('tax_rates'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['system_settings-payment_methods']) { ?>
                        <li id="system_settings_payment_methods">
                            <a href="<?= admin_url('system_settings/payment_methods') ?>"><?= lang('payment_methods'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if (
                        $GP['system_settings-update_products_group_prices'] &&
                        ($this->Settings->prioridad_precios_producto == 2 ||
                            $this->Settings->prioridad_precios_producto == 3 ||
                            $this->Settings->prioridad_precios_producto == 8 ||
                            $this->Settings->prioridad_precios_producto == 4 ||
                            $this->Settings->prioridad_precios_producto == 6 ||
                            $this->Settings->prioridad_precios_producto == 9 ||
                            $this->Settings->prioridad_precios_producto == 10
                        )
                    ) : ?>
                        <li id="system_settings_price_groups">
                            <a href="<?= admin_url('system_settings/update_products_group_prices') ?>"><?= lang('update_products_group_prices'); ?> </a>
                        </li>
                    <?php endif ?>

                    <?php if (
                        $GP['system_settings-update_products_group_prices'] &&
                        ($this->Settings->prioridad_precios_producto == 5 ||
                            $this->Settings->prioridad_precios_producto == 7)
                        // ||
                        // $this->Settings->prioridad_precios_producto == 10
                    ) : ?>

                        <li id="system_settings_price_groups">
                            <a href="<?= admin_url('system_settings/update_products_unit_prices') ?>"><?= lang('update_products_unit_prices'); ?> </a>
                        </li>
                    <?php endif ?>

                    <?php if (($GP['system_settings-update_products_group_prices'] || $GP['system_settings-get_products_units_xls']) &&
                        $this->Settings->prioridad_precios_producto == 5 ||
                        $this->Settings->prioridad_precios_producto == 7 ||
                        $this->Settings->prioridad_precios_producto == 11 ||
                        $this->Settings->prioridad_precios_producto == 10
                    ) : ?>

                        <li id="system_settings_price_groups">
                            <a href="<?= admin_url('system_settings/update_product_unit_prices_general') ?>"><?= lang('update_product_unit_prices_general'); ?> </a>
                        </li>
                    <?php endif ?>

                    <?php if ($GP['expense_categories-index']) { ?>
                        <li id="system_settings_expense_categories">
                            <a href="<?= admin_url('system_settings/expense_categories') ?>"><?= lang('expense_categories'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['system_settings-variants']) { ?>
                        <li id="system_settings_variants">
                            <a href="<?= admin_url('system_settings/variants') ?>"><?= lang('variants'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['system_settings-materials']) { ?>
                        <li id="system_settings_materials">
                            <a href="<?= admin_url('system_settings/materials') ?>"><?= lang('product_materials'); ?></a>
                        </li>
                    <?php } ?>

                    <?php if ($GP['system_settings-colors']) { ?>
                        <li id="system_settings_colors">
                            <a href="<?= admin_url('system_settings/colors') ?>"><?= lang('product_colors'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($GP['payroll_management-index']) : ?>
            <li class="mm_payroll
                <?= ($this->router->fetch_class() == 'payroll_management' || $this->router->fetch_class() == 'employees'
                || $this->router->fetch_class() == 'payroll_contracts' || $this->router->fetch_class() == 'payroll_concepts'
                || $this->router->fetch_class() == 'payroll_settings' || $this->router->fetch_class() == 'payroll_electronic'
                || $this->router->fetch_class() == 'payroll_exports')
                    ? 'active' : ''; ?>">
                <a href="#">
                    <i class="fa fa-id-card"></i>
                    <span class="nav-label"><?= lang("payroll"); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="employees_index">
                        <a href="<?= admin_url("employees"); ?>"><?= lang("employees"); ?></a>
                    </li>
                    <li id="payroll_contracts">
                        <a href="<?= admin_url("payroll_contracts"); ?>"> <?= lang("payroll_contracts"); ?> </a>
                    </li>
                    <li id="payrrol_management">
                        <a href="<?= admin_url("payroll_management"); ?>"><?= lang("payroll_management"); ?></a>
                    </li>
                    <li id="payroll_electronic">
                        <a href="<?= admin_url("payroll_electronic"); ?>"> <?= lang("payroll_electronic"); ?> </a>
                    </li>
                    <li id="payroll_concepts">
                        <a href="<?= admin_url("payroll_concepts"); ?>"> <?= lang("payroll_concepts"); ?> </a>
                    </li>
                    <li id="payroll_exports">
                        <a href="<?= admin_url("payroll_exports"); ?>"> <?= lang("payroll_exports"); ?> </a>
                    </li>
                    <li id="payroll_settings">
                        <a href="<?= admin_url("payroll_settings"); ?>"> <?= lang("settings"); ?> </a>
                    </li>
                </ul>
            </li>
        <?php endif; ?>

        <?php if ($GP['documentsReception-index'] && $this->Settings->documents_reception == YES) : ?>
            <li class="mm_documentsReception <?= ($this->router->fetch_class() == 'documentsReception-index') ? 'active' : ''; ?>">
                <a href="#">
                    <i class="fa fa-cloud-download"></i>
                    <span class="nav-label"><?= $this->lang->line('documents_reception'); ?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="documentsReception_index">
                        <a href="<?= admin_url("documentsReception"); ?>"><?= $this->lang->line("list"); ?></a>
                    </li>
                </ul>
            </li>
        <?php endif; ?>
    <?php } ?>

    <li class="mm_welcome">
        <a href="<?= admin_url('logout'); ?>">
            <i class="fa fa-sign-out"></i>
            <span class="text"><?= lang('logout') ?></span>
        </a>
    </li>

    <?php if ($this->session->userdata() != null && $this->session->userdata('payment_term_first_view') != 1 && $this->sma->actionPermissions('payment_term_expired', 'reports') && $this->Settings->show_alert_sale_expired == 1) : ?>
        <script type="text/javascript">
            setTimeout(function() {
                document.getElementById("payment_term_expire").click();
            }, 5000);
        </script>
    <?php endif ?>
<?php endif ?>