<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */

$lang['add_quote']                      = "Agregar cotización";
$lang['edit_quote']                     = "Editar cotización";
$lang['delete_quote']                   = "Eliminar cotización";
$lang['delete_quotes']                  = "Eliminar cotizaciones";
$lang['quote_added']                    = "La cotización se ha agregado correctamente";
$lang['quote_updated']                  = "La cotización se ha actualizado correctamente";
$lang['quote_deleted']                  = "La cotización se ha eliminado correctamente";
$lang['quotes_deleted']                 = "Las cotizaciones se han eliminado correctamente";
$lang['quote_details']                  = "Detalles de la cotización";
$lang['email_quote']                    = "Email de la cotización";
$lang['view_quote_details']             = "Ver detalles cotización";
//quote purchase/expenses
$lang['edit_quote_purchase_expense']                     = "Editar orden de compra/gasto";
$lang['delete_quote_purchase_expense']                   = "Eliminar orden de compra/gasto";
$lang['delete_quotes_purchase_expense']                  = "Eliminar ordenes de compra/gasto";
$lang['quote_purchase_expense_added']                    = "La orden de compra/gasto se ha agregado correctamente";
$lang['quote_purchase_expense_updated']                  = "La orden de compra/gasto se ha actualizado correctamente";
$lang['quote_purchase_expense_deleted']                  = "La orden de compra/gasto se ha eliminado correctamente";
$lang['quotes_purchase_expense_deleted']                 = "Las ordenes de compra/gasto se han eliminado correctamente";
$lang['quote_purchase_expense_details']                  = "Detalles de la orden de compra/gasto";
$lang['email_quote_purchase_expense']                    = "Email de la orden de compra/gasto";
$lang['view_quote_purchase_expense_details']             = "Ver detalles orden de compra/gasto";
//quote purchase/expenses
$lang['quote_no']                       = "Número ";
$lang['send_email']                     = "Enviar correo electrónico";
$lang['quote_items']                    = "Elementos de la cotización";
$lang['no_quote_selected']              = "No hay cotización seleccionada. Por favor seleccione al menos una";
$lang['stamp_sign']                     = "Firma";
$lang['create_invoice']                 = "Convertir en Factura";
$lang['create_purchase']                = "Convertir en Compra";
$lang['cant_create_to']					= "No se puede crear la %s , revise la cotización";

$lang['create_sale']                    ="Convertir en venta";
$lang['create_purchase']                ="Convertir en compra";

$lang['type_quote_purchase']			="Tipo de óden de compra";
$lang['type_quote_purchase_products']			="Productos";
$lang['type_quote_purchase_expenses']			="Gastos";

// Quotes Purchases

$lang['quote_purchase']					="Órden de Compra";
$lang['quote_expense']					="Órden de Gasto";
$lang['choose_quote_currency_view']		="Escoja la moneda para visualizar";
$lang['requisition']		            ="Requisición";


