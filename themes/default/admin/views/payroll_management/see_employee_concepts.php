<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?> <?= $employee_data->name; ?></h3>
        </div>
        <div class="modal-body">
            <form id="see_employee_concepts_form">
                <h1>Conceptos</h1>
                <section>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="employee_concepts_table" class="table table-hover" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Concepto</th>
                                        <th>Dias/horas</th>
                                        <th class="col-sm-2">Devengados</th>
                                        <th class="col-sm-2">Deducciones</th>
                                        <th></th>
                                        <th></th>
                                        <?php if ($payroll->status == IN_PREPARATION) {  ?>
                                            <th><i class="fa fa-trash fa-lg"></i></th>
                                        <?php } ?>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="dataTables_empty text-center"><?= lang('loading_data_from_server') ?></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th colspan="2"></th>
                                        <th id="total_employee_earned"></th>
                                        <th id="total_employee_deductions"></th>
                                        <th></th>
                                        <th></th>
                                        <?php if ($payroll->status == IN_PREPARATION) {  ?>
                                            <th></th>
                                        <?php } ?>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <?php $total_number_disabilities = 0; ?>
                    <?php $total_earned_disabilities = 0; ?>
                    <?php $total_deductions_disabilities = 0; ?>
                    <?php if (!empty($disabilities)) { ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Incapacidades, vacaciones y licencias</h3>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Concepto</th>
                                            <th>Días/horas</th>
                                            <th style="width: 16.66%">Devengado</th>
                                            <th style="width: 16.66%">Deducciones</th>
                                            <th><i class="fa fa-trash fa-lg"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($disabilities as $concept_id => $inhabilities) { ?>
                                            <?php foreach ($inhabilities as $id => $inhability) { ?>
                                                <?php $inhability_id = $inhability['id']; ?>
                                                <?php $total_number_disabilities += $inhability['cantidad']; ?>
                                                <?php $total_earned_disabilities += $inhability['devengado']; ?>
                                                <?php $total_deductions_disabilities += $inhability['deducido']; ?>
                                                <tr>
                                                    <td><?= lang("conceptNames")[$concept_id]; ?></td>
                                                    <td class="text-center"><?= $inhability['cantidad'] ?></td>
                                                    <td class="text-right"><?= number_format($inhability['devengado'], 2, '.', ','); ?></td>
                                                    <td class="text-right"><?= number_format($inhability['deducido'], 2, '.', ','); ?></td>
                                                    <td class="text-center">
                                                        <?php if (
                                                            $concept_id == PAID_LICENSE ||
                                                            $concept_id == FAMILY_ACTIVITY_PERMIT ||
                                                            $concept_id == DUEL_LICENSE ||
                                                            $concept_id == PAID_PERMIT ||
                                                            $concept_id == MATERNITY_LICENSE ||
                                                            $concept_id == PATERNITY_LICENSE ||
                                                            $concept_id == COMMON_DISABILITY ||
                                                            $concept_id == PROFESSIONAL_DISABILITY ||
                                                            $concept_id == WORK_DISABILITY ||
                                                            $concept_id == DISCIPLINARY_SUSPENSION ||
                                                            $concept_id == UNEXCUSED_ABSENTEEISM ||
                                                            $concept_id == NOT_PAID_LICENSE ||
                                                            $concept_id == NOT_PAID_PERMIT ||
                                                            $concept_id == TIME_VACATION ||
                                                            $concept_id == SUNDAY_DISCOUNT
                                                        ) { ?>
                                                            <i class="fa fa-trash fa-lg pointer remove_novelty" novetly_id="<?= $inhability_id ?>"></i>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total Novedades</th>
                                            <th class="text-center"><?= $total_number_disabilities ?></th>
                                            <th class="text-right" id="total_earned_disabilities"><?= number_format($total_earned_disabilities, 2, ".", ",") ?></th>
                                            <th class="text-right" id="total_deduction_disabilities"><?= number_format($total_deductions_disabilities, 2, ".", ",") ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    <?php } ?>

                    <div id="full_payment"></div>

                    <?php if (!empty($conceptsPaidEmployee)) { ?>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="text-success">Conceptos de provisión</h3>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Concepto</th>
                                            <th class="text-right">Devengado</th>
                                            <th class="text-right">Deducciones</th>
                                            <th class="text-center"><i class="fa fa-trash fa-lg"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $totalEarneds = $totalDeductions = 0; ?>
                                        <?php foreach ($conceptsPaidEmployee as $concept_id => $conceptPaidEmployee) { ?>
                                            <?php foreach ($conceptPaidEmployee as $concept) { ?>
                                                <?php $item_id = $concept['id']; ?>
                                                <?php
                                                    $totalEarneds += $concept['devengado'];
                                                    $totalDeductions += $concept['deducido'];
                                                ?>
                                                <tr>
                                                    <td><?= lang("conceptNames")[$concept_id]; ?></td>
                                                    <td class="text-right"><?= number_format($concept['devengado'], 2, '.', ','); ?></td>
                                                    <td class="text-right"><?= number_format($concept['deducido'], 2, '.', ','); ?></td>
                                                    <td class="text-center">
                                                        <i class="fa fa-trash fa-lg pointer remove_novelty" novetly_id="<?= $item_id ?>"></i>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfooter>
                                        <th colspan="1"></th>
                                        <th class="text-right"><?= number_format($totalEarneds, 2, '.', ','); ?></th>
                                        <th class="text-right"><?= number_format($totalDeductions, 2, '.', ','); ?></th>
                                        <th></th>
                                    </tfooter>
                                </table>
                            </div>
                        </div>
                    <?php } ?>
                </section>
                <?php if ($hideShowParafiscalProvisions == TRUE) { ?>
                    <h1>Seguridad social</h1>
                    <section>
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Cálculos seguridad social</h3>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Concepto</th>
                                            <th>Base</th>
                                            <th style="width: 8.33%">Porcentaje</th>
                                            <th style="width: 16.66%">Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($social_security_parafiscal as $key => $ssp) { ?>
                                            <?php if (strpos($ssp->description, 'Pensión') !== FALSE) { ?>
                                                <tr>
                                                    <td><?= $ssp->description; ?></td>
                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php foreach ($social_security_parafiscal as $key => $ssp) { ?>
                                            <?php if (strpos($ssp->description, 'CLASE') !== FALSE) { ?>
                                                <tr>
                                                    <td><?= $ssp->description; ?></td>
                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-sm-12">
                                <h3>Cálculos parafiscales</h3>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Concepto</th>
                                            <th>Base</th>
                                            <th style="width: 8.33%">Porcentaje</th>
                                            <th style="width: 16.66%">Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($social_security_parafiscal as $key => $ssp) { ?>
                                            <?php if (strpos($ssp->description, 'Caja de compensación') !== FALSE) { ?>
                                                <tr>
                                                    <td><?= $ssp->description; ?></td>
                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                </tr>
                                            <?php } ?>
                                            <?php if (strpos($ssp->description, 'SENA') !== FALSE) { ?>
                                                <tr>
                                                    <td><?= $ssp->description; ?></td>
                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                </tr>
                                            <?php } ?>
                                            <?php if (strpos($ssp->description, 'ICBF') !== FALSE) { ?>
                                                <tr>
                                                    <td><?= $ssp->description; ?></td>
                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                    <td class="text-right"><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <h1>Provisiones</h1>
                    <section>
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Provisiones</h3>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Concepto</th>
                                            <th>Base</th>
                                            <th style="width: 8.33%">Porcentaje</th>
                                            <th style="width: 16.66%">Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total_provisions = 0; ?>
                                        <?php foreach ($provisions as $key => $provision) { ?>
                                            <?php $total_provisions += $provision->amount; ?>
                                            <tr>
                                                <td><?= $provision->name; ?></td>
                                                <td class="text-right"><?= number_format($provision->base_salary, 2, ",", "."); ?></td>
                                                <td class="text-right"><?= number_format($provision->percentage, 2, ",", "."); ?></td>
                                                <td class="text-right"><?= number_format($provision->amount, 2, ",", "."); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3">Total de provisiones</th>
                                            <th class="text-right"><?= number_format($total_provisions, 2, ",", "."); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </section>
                <?php } ?>
            </form>
        </div>
        <div class="modal-footer">
            <?= form_button(["class" => "btn btn-default", "data-dismiss" => "modal"], lang("close")); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        _total_earned = 0;
        _total_deductions = 0;
        _total_earned_disabilities = <?= $total_earned_disabilities ?>;
        _total_deductions_disabilities = <?= $total_deductions_disabilities ?>;

        $("#see_employee_concepts_form").show().steps({
            headerTag: "h1",
            bodyTag: "section",
            titleTemplate: "#title#",
            transitionEffect: "fade",
            enableAllSteps: true,
            enablePagination: false
        });

        $(document).on('click', '.remove_novelty', function() {
            confirm_remove_novetly($(this));
        });

        load_datatable();
    });

    function load_datatable() {
        if ($(window).width() < 1000) {
            var nums = [
                [10, 25],
                [10, 25]
            ];
        } else {
            var nums = [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ];
        }

        $('#employee_concepts_table').dataTable({
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url("payroll_management/get_employee_concepts_datatables/" . $payroll_id . "/" . $employee_id . '/' . $contract_id) ?>',
            dom: 't',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            fnRowCallback: function( nRow, aData, iDisplayIndex ) {
                var actions = $(nRow);
                if ((aData[8] == <?= LAYOFFS ?> || aData[8] == <?= LAYOFFS_INTERESTS ?> || aData[8] == <?= MONEY_VACATION ?> || aData[8] == <?= SERVICE_BONUS ?>) && aData[10] == 0) {
                    actions.css('display', 'none');
                }
            },
            fnFooterCallback: function(row, aData) {
                var total_earned = total_deductions = 0;

                $.each(aData, function(index, data) {
                    paidToEemployee = data[10];
                    conceptId = data[8];
                    earned = data[3];
                    deduction = data[4];
                    if ((conceptId == <?= LAYOFFS ?> || conceptId == <?= LAYOFFS_INTERESTS ?> || conceptId == <?= MONEY_VACATION ?> || conceptId == <?= SERVICE_BONUS ?>)) {
                        if (paidToEemployee == 1) {
                            total_earned += parseFloat((earned == '') ? 0 : earned);
                        }
                    } else {
                        total_earned += parseFloat((earned == '') ? 0 : earned);
                    }
                    total_deductions += parseFloat((deduction == '') ? 0 : deduction);
                });

                $('#total_employee_earned').html(currencyFormat(total_earned));
                $('#total_employee_deductions').html(currencyFormat(total_deductions));

                _total_earned = total_earned;
                _total_deductions = total_deductions;
            },
            aoColumns: [{
                    visible: false
                },
                {
                    render: function(data, b, row) {
                        description = row[9];
                        if (description == '' || description == null) {
                            return data;
                        } else {
                            return data + '<br><small>' + description + '</small>';
                        }

                    }
                },
                {
                    className: 'text-center',
                    render: function(data, b, row) {
                        time_unit = row[6];
                        amount = (time_unit == 'horas') ? data : parseInt(data);

                        if (amount == 0) {
                            return '';
                        } else {
                            return amount + ' ' + time_unit;
                        }
                    }
                },
                {
                    className: 'text-right',
                    render: currencyFormat
                },
                {
                    className: 'text-right',
                    render: currencyFormat
                },
                {
                    visible: false
                },
                {
                    visible: false
                },
                <?php if ($payroll->status == IN_PREPARATION) {  ?> {
                        className: 'text-center',
                        render: function(data, b, row) {
                            id = row[8];
                            if (data == 0) {
                                if (id == "<?= COMPANY_LOAN ?>" || id == "<?= FREE_INVESTMENT_LOAN ?>") {
                                    return '';
                                } else {
                                    return '<i class="fa fa-trash fa-lg pointer remove_novelty" novetly_id="' + row[0] + '"></i>';
                                }
                            }
                            return '';
                        }
                    },
                <?php } ?>
                {
                    visible: false
                },
                {
                    visible: false
                },
                {
                    visible: false
                },
            ]
        });

        $('#employee_concepts_table').DataTable().on("draw", function() {
            calculate_total_payment();
        })

    }

    function calculate_total_payment() {
        var full_payment = (_total_earned + _total_earned_disabilities) - (_total_deductions + _total_deductions_disabilities);
        $('#full_payment').html('<div class="alert alert-success text-right">Total a pagar: <label>' + currencyFormat(full_payment) + ' </label></div>');
    }

    function confirm_remove_novetly(element) {
        swal({
            title: '¿Está seguro de eliminar la novedad?',
            text: 'Se realizará el recálculo para los Parafiscales y las provisiones',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Sí, Borrar!",
            closeOnConfirm: false,
            closeOnClickOutside: false,
        }, function() {
            swal({
                title: 'Eliminando novedad',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            var id = element.attr('novetly_id');
            window.location.href = "<?= admin_url("payroll_management/remove_novetly") ?>/" + id;
        })
    }
</script>