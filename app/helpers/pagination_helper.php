<?php defined('BASEPATH') OR exit('No direct script access allowed');

if(! function_exists('pagination')) {
    function pagination($uri, $total, $per_page, $lang = NULL)
    {
        $ci = & get_instance();
        $ci->load->library('pagination');
        $config = array();
        $config['base_url']             = site_url($uri);
        $config['total_rows']           = $total;
        $config["per_page"]             = $per_page;
        $config['full_tag_open']        = '<ul class="pagination" style="width:100%;">';
        $config['full_tag_close']       = '</ul>';
        $config['first_tag_open']       = '<li class="first text-bold">';
        $config['first_tag_close']      = '</li>';
        $config['last_tag_open']        = '<li class="last text-bold">';
        $config['last_tag_close']       = '</li>';
        $config['next_tag_open']        = '<li class="next text-bold">';
        $config['next_tag_close']       = '</li>';
        $config['prev_tag_open']        = '<li class="prev text-bold">';
        $config['prev_tag_close']       = '</li>';
        $config['cur_tag_open']         = '<li class="active"><a>';
        $config['cur_tag_close']        = '</a></li>';
        $config['num_tag_open']         = '<li class="page">';
        $config['num_tag_close']        = '</li>';
        $config['page_query_string']    = TRUE;
        $config['use_page_numbers']     = TRUE;
        $config['display_pages']     = FALSE;
        $config['query_string_segment'] = 'page';
        $config['first_link']           = '<i class="fa fa-long-arrow-left"></i> '.(isset($lang['first-page']) ? $lang['first-page'] : 'Primera página');
        $config['last_link']            = (isset($lang['last-page']) ? $lang['last-page'] : 'Última página').' <i class="fa fa-long-arrow-right"></i>';
        $config['prev_link']            = '<i class="fa fa-arrow-left"></i> '.(isset($lang['previous-page']) ? $lang['previous-page'] : 'Página anterior');
        $config['next_link']            = (isset($lang['next-page']) ? $lang['next-page'] : 'Siguiente página').' <i class="fa fa-arrow-right"></i>';
        $ci->pagination->initialize($config);
        return $ci->pagination->create_links();
    }
}
