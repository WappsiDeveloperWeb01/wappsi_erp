<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
	.ibox-content {
		height: 100%;
		overflow: auto;
		padding: 10px 0;
	}
	.contenedor-canvas {
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.225);
		height: 700px;
		margin: auto;
		width:1200px;
	}
	.nav-link label {
		cursor: inherit;
	}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="row">
	    <div class="col-md-10">
	        <h2>
	        	<?= lang('restobar'); ?>
	        </h2>
	    </div>
	    <div class="col-md-2" style="padding-top: 12px;">
	    	<div class="form-group">
	    		<label for="sucursal" class="form-label col-md-3">sucursal</label>
	    		<div class="col-md-9">
		        	<select class="form-control" name="sucursal" id="sucursal">
		        		<?php foreach ($branches as $key => $branch) { ?>
		        			<option value="<?= $branch->id; ?>" <?= ($branch_id == $branch->id) ? 'selected' : ''; ?>><?= $branch->nombre; ?></option>
		        		<?php } ?>
		        	</select>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
            	<div class="tabs-container">
                    <ul class="nav nav-tabs" role="tablist">
            			<?php foreach ($branch_areas as $key => $area) { ?>
            				<?php $active = ($area_id == $area->id) ? 'active' : ''; ?>
                        	<li class="<?= $active; ?>"><a class="nav-link area_sucursal <?= $active; ?>" id="<?= $area->id ?>" data-toggle="tab" href="#area_<?= $area->id ?>"> <label><?= $area->nombre ?></label></a></li>
        				<?php } ?>
                    </ul>
                    <div class="tab-content">
            			<?php foreach ($branch_areas as $key => $area) { ?>
							<?php $active = ($area_id == $area->id) ? 'active' : ''; ?>
	                        <div role="tabpanel" class="tab-pane <?= $active ?>" id="area_<?= $area->id ?>">
	                            <div class="panel-body">
	                            	<div class="text-right">
	                            		<div class="btn-group">
				                            <button data-toggle="dropdown" class="btn btn-success" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				                            	<i class="fa fa-plus" aria-hidden="true"></i> <?= lang("restobar_label_new"); ?>
				                            </button>
				                            <ul class="dropdown-menu" style="right: 0; left: auto;">
				                                <li><a class="dropdown-item" id="agregar_mesa" data-area_id="<?= $area->id ?>">Mesa</a></li>
				                                <li><a class="dropdown-item" id="agregar_multiple_mesas" data-area_id="<?= $area->id ?>">Multiples mesas</a></li>
				                            </ul>
				                        </div>
	                            	</div>
	                            	<hr>
	                            	<div style="overflow: auto;">
										<div class="contenedor-canvas" id="container_<?= $area->id ?>"></div>
	                            	</div>
	                            </div>
	                        </div>
        				<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_agregar_mesa" tabindex="-1" role="dialog">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
			<?= form_open(admin_url("system_settings/save_table"), ["id"=>"formulario_guardar_mesa"]) ?>
	      		<div class="modal-header">
	        		<h4 class="modal-title"><?= lang("restobar_title_add"); ?></h4>
	      		</div>
	      		<div class="modal-body">
	        		<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="numero_mesa"><?= lang("restobar_label_table_number"); ?></label>
								<input class="form-control" type="text" name="numero_mesa" id="numero_mesa" min="1">
							</div>
						</div>

	        			<div class="col-md-6">
	        				<div class="form-group">
	        					<label for="estados_mesa"><?= lang("restobar_label_table_state"); ?></label>
	        					<select class="form-control" name="estados_mesa" id="estados_mesa">
									<?php
										$states_table = [
											"" => lang("select"),
											AVAILABLE => lang("restobar_label_available_status"),
											NOT_AVAILABLE => lang("restobar_label_not_available_status"),
											OCCUPIED => lang("restobar_label_occuped_status"),
											RESERVED => lang("restobar_label_reserved_status"),
										];
									?>
									<?php foreach ($states_table as $key => $status) { ?>
										<option value="<?= $key; ?>"><?= $status; ?></option>
									<?php } ?>
	        					</select>
	        				</div>
	        			</div>
	        		</div>

	        		<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="forma_mesa"><?= lang("restobar_label_table_shape"); ?></label>
								<select class="form-control" name="forma_mesa" id="forma_mesa">
									<?php
										$table_shape = [
											"" => lang("select"),
											SQUARE => lang("restobar_label_square_shape"),
											RECTANGULAR => lang("restobar_label_rectangular_shape"),
											CIRCLE => lang("restobar_label_circle_shape"),
											ELLIPSE => lang("restobar_label_ellipse_shape"),
											BAR => lang("restobar_label_bar_shape")
										];
									?>
									<?php foreach ($table_shape as $key => $shape) { ?>
										<option value="<?= $key; ?>"><?= $shape; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

	        			<div class="col-md-6">
	        				<div class="form-group">
	        					<label for="numero_asientos_mesa"><?= lang("restobar_label_number_table_seats"); ?></label>
	        					<select class="form-control" type="number" name="numero_asientos_mesa" id="numero_asientos_mesa" min="0">
									<option value=""><?= lang('select'); ?></option>
	        					</select>
	        				</div>
	        			</div>
	        		</div>

	        		<div class="row">
	        			<div class="col-md-6" id="contenedor_ancho_mesa">
	        				<div class="form-group">
	        					<label for="ancho_mesa"><?= lang("restobar_label_table_width"); ?></label>
	        					<input class="form-control" type="number" name="ancho_mesa" id="ancho_mesa" min="1">
	        				</div>
	        			</div>

	        			<div class="col-md-6" id="contenedor_alto_mesa">
	        				<div class="form-group">
	        					<label for="alto_mesa"><?= lang("restobar_label_table_height"); ?></label>
	        					<input class="form-control" type="number" name="alto_mesa" id="alto_mesa" min="1">
	        				</div>
	        			</div>

	        			<div class="col-md-6" id="contenedor_tamano_mesa">
	        				<div class="form-group">
	        					<label for="tamano_mesa"><?= lang("restobar_label_table_size"); ?></label>
	        					<input class="form-control" type="number" name="tamano_mesa" id="tamano_mesa" min="1">
	        				</div>
	        			</div>

	        			<div class="col-md-6" id="contenedor_radio_mesa">
	        				<div class="form-group">
	        					<label for="radio_mesa"><?= lang("restobar_label_table_radius"); ?></label>
	        					<input class="form-control" type="number" name="radio_mesa" id="radio_mesa" min="1">
	        				</div>
	        			</div>
	        		</div>

	        		<div class="row">
	        			<div class="col-md-6">
	        				<div class="form-group">
	        					<label for="posicion_x_mesa"><?= lang("restobar_label_x_position_table"); ?></label>
	        					<input class="form-control" type="number" name="posicion_x_mesa" id="posicion_x_mesa" min="0" value="100">
	        				</div>
	        			</div>

	        			<div class="col-md-6">
	        				<div class="form-group">
	        					<label for="posicion_y_mesa"><?= lang("restobar_label_y_position_table"); ?></label>
	        					<input class="form-control" type="number" name="posicion_y_mesa" id="posicion_y_mesa" min="0" value="100">
	        				</div>
	        			</div>
	        		</div>
	      		</div>
	      		<div class="modal-footer">
	      			<input type="hidden" name="area_name" id="area_name" value="<?= $area_name ?>">
	        		<button class="btn btn-default" type="button" data-dismiss="modal"><i class="fa fa-times"></i> <?= lang("close"); ?></button>
	    			<button class="btn btn-primary" type="button" id="guardar_mesa"><i class="fa fa-check"></i> <?= lang("save") ?></button>
	      		</div>
	      		<input type="hidden" name="id_area" id="id_area">
      		<?= form_close(); ?>
    	</div>
  	</div>
</div>

<div class="modal fade" id="modal_editar_mesa" tabindex="-1" role="dialog">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
			<?= form_open(admin_url("system_settings/edit_table"), ["id"=>"formulario_editar_mesa"]) ?>
	      		<div class="modal-header">
	        		<h4 class="modal-title"><?= lang("restobar_title_add"); ?></h4>
	      		</div>
	      		<div class="modal-body">
	        		<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="numero_mesa"><?= lang("restobar_label_table_number"); ?></label>
								<input class="form-control" type="text" name="numero_mesa" id="numero_mesa" min="1">
							</div>
						</div>

	        			<div class="col-md-6">
	        				<div class="form-group">
	        					<label for="estados_mesa"><?= lang("restobar_label_table_state"); ?></label>
	        					<select class="form-control" name="estados_mesa" id="estados_mesa">
									<?php
										$states_table = [
											"" => lang("select"),
											AVAILABLE => lang("restobar_label_available_status"),
											NOT_AVAILABLE => lang("restobar_label_not_available_status"),
											OCCUPIED => lang("restobar_label_occuped_status"),
											RESERVED => lang("restobar_label_reserved_status"),
										];
									?>
									<?php foreach ($states_table as $key => $status) { ?>
										<option value="<?= $key; ?>"><?= $status; ?></option>
									<?php } ?>
	        					</select>
	        				</div>
	        			</div>
	        		</div>

	        		<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="forma_mesa"><?= lang("restobar_label_table_shape"); ?></label>
								<select class="form-control" name="forma_mesa" id="forma_mesa">
									<?php
										$table_shape = [
											"" => lang("select"),
											SQUARE => lang("restobar_label_square_shape"),
											RECTANGULAR => lang("restobar_label_rectangular_shape"),
											CIRCLE => lang("restobar_label_circle_shape"),
											ELLIPSE => lang("restobar_label_ellipse_shape"),
											BAR => lang("restobar_label_bar_shape")
										];
									?>
									<?php foreach ($table_shape as $key => $shape) { ?>
										<option value="<?= $key; ?>"><?= $shape; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

	        			<div class="col-md-6">
	        				<div class="form-group">
	        					<label for="numero_asientos_mesa"><?= lang("restobar_label_number_table_seats"); ?></label>
	        					<select class="form-control" type="number" name="numero_asientos_mesa" id="numero_asientos_mesa">
									<option value=""><?= lang("select"); ?></option>
	        					</select>
	        				</div>
	        			</div>
	        		</div>

	        		<div class="row">
	        			<div class="col-md-6" id="contenedor_ancho_mesa">
	        				<div class="form-group">
	        					<label for="ancho_mesa"><?= lang("restobar_label_table_width"); ?></label>
	        					<input class="form-control" type="number" name="ancho_mesa" id="ancho_mesa" min="1">
	        				</div>
	        			</div>

	        			<div class="col-md-6" id="contenedor_alto_mesa">
	        				<div class="form-group">
	        					<label for="alto_mesa"><?= lang("restobar_label_table_height"); ?></label>
	        					<input class="form-control" type="number" name="alto_mesa" id="alto_mesa" min="1">
	        				</div>
	        			</div>

	        			<div class="col-md-6" id="contenedor_tamano_mesa">
	        				<div class="form-group">
	        					<label for="tamano_mesa"><?= lang("restobar_label_table_size"); ?></label>
	        					<input class="form-control" type="number" name="tamano_mesa" id="tamano_mesa" min="1">
	        				</div>
	        			</div>

	        			<div class="col-md-6" id="contenedor_radio_mesa">
	        				<div class="form-group">
	        					<label for="radio_mesa"><?= lang("restobar_label_table_radius"); ?></label>
	        					<input class="form-control" type="number" name="radio_mesa" id="radio_mesa" min="1">
	        				</div>
	        			</div>
	        		</div>

	        		<div class="row">
	        			<div class="col-md-6">
	        				<div class="form-group">
	        					<label for="posicion_x_mesa"><?= lang("restobar_label_x_position_table"); ?></label>
	        					<input class="form-control" type="number" name="posicion_x_mesa" id="posicion_x_mesa" min="0">
	        				</div>
	        			</div>

	        			<div class="col-md-6">
	        				<div class="form-group">
	        					<label for="posicion_y_mesa"><?= lang("restobar_label_y_position_table"); ?></label>
	        					<input class="form-control" type="number" name="posicion_y_mesa" id="posicion_y_mesa" min="0">
	        				</div>
	        			</div>
	        		</div>
	      		</div>
	      		<div class="modal-footer">
	      			<input type="hidden" name="id_mesa" id="id_mesa">
	        		<button class="btn btn-default" type="button" data-dismiss="modal"><i class="fa fa-times"></i> <?= lang("close"); ?></button>
	    			<button class="btn btn-primary" type="button" id="editar_mesa"><i class="fa fa-check"></i> <?= lang("save") ?></button>
	      		</div>
	      		<input type="hidden" name="id_area" id="id_area">
      		<?= form_close(); ?>
    	</div>
  	</div>
</div>

<div class="modal fade in" id="add_multiple_tables_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<?= form_open(admin_url('system_settings/restobar'), 'id="formulario_sucursal"'); ?>
	<input type="hidden" name="id_sucursal" id="id_sucursal" value="<?= $branch_id ?>">
	<input type="hidden" name="id_area" id="id_area">
<?= form_close(); ?>

<script type="text/javascript" src="<?= $assets ?>js/konva.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		cargar_mesas(<?= $area_id ?>);

		$('body').addClass('mini-navbar');
		$('[data-toggle="tooltip"]').tooltip();

		$('#formulario_guardar_mesa #contenedor_tamano_mesa').fadeOut();
		$('#formulario_guardar_mesa #contenedor_radio_mesa').fadeOut();

		$(document).on('change', '#sucursal', cargar_areas_sucursal);
		$(document).on('click', '.area_sucursal', function() { cargar_mesas_areas($(this).attr('id')); });
		$(document).on('change', '#formulario_guardar_mesa #forma_mesa', function() { cargar_numero_asientos(); mostrar_campos_tamaño_mesas(); });
		$(document).on('change', '#formulario_editar_mesa #forma_mesa', function() { cargar_numero_asientos(true); mostrar_campos_tamaño_mesas(true); });
		$(document).on('change', '#formulario_guardar_mesa #numero_mesa', function() { validar_mesa_existente(); });
		$(document).on('change', '#formulario_editar_mesa #numero_mesa', function() { validar_mesa_existente(true); });
		$(document).on('click', '#agregar_mesa', function() { agregar_mesa($(this).data('area_id')); });
		$(document).on('click', '#agregar_multiple_mesas', function() { agregar_multiple_mesas($(this).data('area_id')); });
		$(document).on('click', '#guardar_mesa', guardar_mesa);
		$(document).on('click', '#editar_mesa', editar_mesa);

	});

	function cargar_areas_sucursal()
	{
		var branch_id = $('#sucursal').val();
		$('#formulario_sucursal #id_sucursal').val(branch_id);

		$('#formulario_sucursal').submit();
	}

	function cargar_mesas_areas(area_id)
	{
		$('#formulario_sucursal #id_area').val(area_id);

		$('#formulario_sucursal').submit();
	}

	function validar_mesa_existente(to_edit = false)
	{
		if (to_edit == true) {
			numero_mesa = $('#formulario_editar_mesa #numero_mesa').val();
			id_area = $('#formulario_editar_mesa #id_area').val();
			id_mesa = $('#formulario_editar_mesa #id_mesa').val();
		} else {
			numero_mesa = $('#formulario_guardar_mesa #numero_mesa').val();
			id_area = $('#formulario_guardar_mesa #id_area').val();
			id_mesa = '';
		}

		$.ajax({
			url: '<?= admin_url("system_settings/get_existing_table_number"); ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
				'numero_mesa': numero_mesa,
				'id_area': id_area,
				'id_mesa': id_mesa
			}
		})
		.done(function(response) {
			if (response == true) {
				Command: toastr.error(
					'El número de mesa ingresado ya se encuentra registrado para esta area.',
					'Validación de formulario',
					{
						onHidden: function() {
							if (to_edit == true) {
								$('#formulario_editar_mesa #numero_mesa').val('');
							} else {
								$('#formulario_guardar_mesa #numero_mesa').val('');
							}
						}
					}
				);
			}
		})
		.fail(function(response) {
			console.log(response.responseText);
		});
	}

	function mostrar_campos_tamaño_mesas(to_edit = false)
	{
		if (to_edit == true) {
			if ($('#formulario_editar_mesa #forma_mesa').val() == <?= SQUARE ?>) {
				$('#formulario_editar_mesa #contenedor_radio_mesa').fadeOut();
				$('#formulario_editar_mesa #contenedor_ancho_mesa').fadeOut();
				$('#formulario_editar_mesa #contenedor_alto_mesa').fadeOut();

				$('#formulario_editar_mesa #contenedor_tamano_mesa').fadeIn();
			} else if ($('#formulario_editar_mesa #forma_mesa').val() == <?= CIRCLE ?> || $('#formulario_editar_mesa #forma_mesa').val() == <?= ELLIPSE ?>) {
				$('#formulario_editar_mesa #contenedor_tamano_mesa').fadeOut();
				$('#formulario_editar_mesa #contenedor_ancho_mesa').fadeOut();
				$('#formulario_editar_mesa #contenedor_alto_mesa').fadeOut();

				$('#formulario_editar_mesa #contenedor_radio_mesa').fadeIn();
			} else {
				$('#formulario_editar_mesa #contenedor_tamano_mesa').fadeOut();
				$('#formulario_editar_mesa #contenedor_radio_mesa').fadeOut();

				$('#formulario_editar_mesa #contenedor_ancho_mesa').fadeIn();
				$('#formulario_editar_mesa #contenedor_alto_mesa').fadeIn();
			}
		} else {
			if ($('#formulario_guardar_mesa #forma_mesa').val() == <?= SQUARE ?>) {
				$('#formulario_guardar_mesa #contenedor_radio_mesa').fadeOut();
				$('#formulario_guardar_mesa #contenedor_ancho_mesa').fadeOut();
				$('#formulario_guardar_mesa #contenedor_alto_mesa').fadeOut();

				$('#formulario_guardar_mesa #contenedor_tamano_mesa').fadeIn();
			} else if ($('#formulario_guardar_mesa #forma_mesa').val() == <?= CIRCLE ?> || $('#formulario_guardar_mesa #forma_mesa').val() == <?= ELLIPSE ?>) {
				$('#formulario_guardar_mesa #contenedor_tamano_mesa').fadeOut();
				$('#formulario_guardar_mesa #contenedor_ancho_mesa').fadeOut();
				$('#formulario_guardar_mesa #contenedor_alto_mesa').fadeOut();

				$('#formulario_guardar_mesa #contenedor_radio_mesa').fadeIn();
			} else {
				$('#formulario_guardar_mesa #contenedor_tamano_mesa').fadeOut();
				$('#formulario_guardar_mesa #contenedor_radio_mesa').fadeOut();

				$('#formulario_guardar_mesa #contenedor_ancho_mesa').fadeIn();
				$('#formulario_guardar_mesa #contenedor_alto_mesa').fadeIn();
			}
		}
	}

	function cargar_numero_asientos(to_edit = false)
	{
		var table_seats = <?= json_encode(TABLE_SEATS); ?>;
		var	table_shape_seating_options = '<option value=""><?= lang('select'); ?></option>';

		if (to_edit == true) {
			var table_shape = $('#formulario_editar_mesa #forma_mesa').val();
		} else {
			var table_shape = $('#formulario_guardar_mesa #forma_mesa').val();
		}

		var seats_according_table_shape = table_seats[table_shape];

		if (table_shape != '') {
			for (var i = 0; i < seats_according_table_shape.length; i++) {
				table_shape_seating_options += '<option value="'+seats_according_table_shape[i]+'">'+seats_according_table_shape[i]+'</option>'
			}
		}

		if (to_edit == true) {
			$('#formulario_editar_mesa #numero_asientos_mesa').html(table_shape_seating_options);
		} else {
			$('#formulario_guardar_mesa #numero_asientos_mesa').html(table_shape_seating_options);
		}
	}

	function cargar_mesas(table_id)
	{
		var blockSnapSize = 20;
		var width = $('#container_'+table_id).width();
      	var height = $('#container_'+table_id).height();

      	var stage = new Konva.Stage({
        	container: 'container_'+table_id,
        	width: width,
        	height: height
      	});

		var padding = blockSnapSize;
		var gridLayer = new Konva.Layer();
		var newLayer = new Konva.Layer();

		for (var i = 0; i < width / padding; i++) {
		  gridLayer.add(new Konva.Line({
		    points: [Math.round(i * padding) + 0.5, 0, Math.round(i * padding) + 0.5, height],
		    stroke: '#ddd',
		    strokeWidth: 1,
		  }));
		}

		gridLayer.add(new Konva.Line({points: [0,0,10,10]}));
		for (var j = 0; j < height / padding; j++) {
		  gridLayer.add(new Konva.Line({
		    points: [0, Math.round(j * padding), width, Math.round(j * padding)],
		    stroke: '#ddd',
		    strokeWidth: 0.5,
		  }));
		}

      	stage.add(gridLayer);

      	<?php if (! empty($tables)) { ?>
      		<?php foreach ($tables as $i => $table) { ?>
        		/*********************************************************/
        		<?php
        			if ($table->forma_mesa == SQUARE) {
    					$ancho = $table->tamano;
    					$alto = $table->tamano;
    				} else if ($table->forma_mesa == CIRCLE) {
    					$ancho = $table->radio;
    					$alto = $table->radio;
        			} else if ($table->forma_mesa == ELLIPSE) {
        				$ancho = $table->radio;
    					$alto = $table->radio / 1.5;
        			} else {
        				$ancho = $table->ancho;
    					$alto = $table->alto;
        			}

        			if ($table->estado == AVAILABLE) {
        				$color = '';
        				$text_color = '#676a6c';
        			} else if ($table->estado == NOT_AVAILABLE) {
        				$color = '_amarillo';
        				$text_color = '#676a6c';
        			} else if ($table->estado == OCCUPIED) {
        				$color = '_verde';
        				$text_color = '#fefefe';
        			} else if ($table->estado == RESERVED) {
        				$color = '_rojo';
        				$text_color = '#fefefe';
        			}
        		?>

        		var group<?= $i; ?> = new Konva.Group({
			        x: <?= $table->posicion_X; ?>,
          			y: <?= $table->posicion_Y; ?>,
			        draggable: true,
			        rotation: <?= $table->rotacion; ?>
		    	});

		    	var text<?= $i; ?> = new Konva.Text({
			        width: <?= $ancho; ?>,
          			height: <?= $alto; ?>,
			        fontSize: 20,
			        fill: '<?= $text_color; ?>',
			        stroke: '<?= $text_color; ?>',
			        strokeWidth: 2,
			        fontFamily: 'Arial',
			        text: '<?= $table->tipo ." ". $table->numero; ?>',
			        fill: 'gray',
			        align: 'center',
			        verticalAlign: 'middle'
                });

        		var mesa<?= $i; ?> = new Konva.Image({
          			width: <?= $ancho; ?>,
          			height: <?= $alto; ?>
        		});

        		var imageObj<?= $i; ?> = new Image();
        		imageObj<?= $i; ?>.onload = function() {
          			mesa<?= $i; ?>.image(imageObj<?= $i; ?>);
          			gridLayer.draw();
        		};
        		imageObj<?= $i; ?>.src = '<?= base_url("themes/default/admin/assets/images/tables/".$table->imagen.$color.".png"); ?>';

        		group<?= $i; ?>.add(mesa<?= $i; ?>).add(text<?= $i; ?>);
        		gridLayer.add(group<?= $i; ?>);

        		var group_rotation<?= $i; ?> = new Konva.Transformer({
        			id: <?= $table->id; ?>,
			        node: group<?= $i; ?>,
			        centeredScaling: true,
			        rotationSnaps: [0, 90, 180, 270],
			        resizeEnabled: false
                });
			    gridLayer.add(group_rotation<?= $i; ?>);


			    group<?= $i; ?>.on('transformend', function() {
			        actualizar_datos_posicion_mesa(group_rotation<?= $i; ?>);
			    });

			    group<?= $i; ?>.on('click', function() {
			        abrir_modal_editar_mesa(group_rotation<?= $i; ?>.attrs.id);
			    });

        		group<?= $i; ?>.on('dragend', function() {
			        actualizar_datos_posicion_mesa(group_rotation<?= $i; ?>);

					group<?= $i; ?>.position({
					  x: Math.round(group<?= $i; ?>.x() / blockSnapSize) * blockSnapSize,
					  y: Math.round(group<?= $i; ?>.y() / blockSnapSize) * blockSnapSize
					});
					newLayer.batchDraw();
				});

        		/*********************************************************/
      		<?php } ?>

			gridLayer.on('mouseover', function(evt) {
				document.body.style.cursor = 'pointer';
				gridLayer.draw();
			});

			gridLayer.on('mouseout', function(evt) {
				document.body.style.cursor = 'default';
				gridLayer.draw();
			});
  		<?php } ?>
	}

	function agregar_mesa(area_id)
	{
		$('#modal_agregar_mesa #id_area').val(area_id);

		$('#modal_agregar_mesa').modal('show');
	}

	function agregar_multiple_mesas()
	{
		var branch_id = $('#sucursal').val();
		$('#add_multiple_tables_modal').modal({remote: site.base_url + 'system_settings/add_multiple_tables_restobar/'+ branch_id});
    	$('#add_multiple_tables_modal').modal('show');
	}

	function guardar_mesa()
	{
		if (validar_campos_formulario_guardar_mesa()) {
			var formulario_guardar_mesa = $('#formulario_guardar_mesa');

			$.ajax({
				url: formulario_guardar_mesa.attr('action'),
				type: 'POST',
				dataType: 'JSON',
				data: formulario_guardar_mesa.serialize(),
			})
			.done(function(response) {
				$('#modal_agregar_mesa').modal('hide');

				if (response.response == 1) {
					$('#loading').fadeIn();

					Command: toastr.success(response.message, "Mensaje del sistema", { onHidden: function () { location.reload(); }, timeOut: 1500 });
				} else {
					Command: toastr.error(
						response.message,
						"Mensaje del sistema"
					);
				}
			})
			.fail(function(response) {
				console.log(response.responseText);
				Command: toastr.error(
					"Al parecer existe un problema. Por favor comuníquese con el administrador del sistema.",
					"Mensaje del sistema"
				);
			});
		}
	}

	function validar_campos_formulario_guardar_mesa()
	{
		if ($("#formulario_guardar_mesa #numero_mesa").val() == '') {
			mostra_mensaje('<?= lang("restobar_label_table_number"); ?>', $("#formulario_guardar_mesa #numero_mesa"));

			return false;
		} else if ($('#formulario_guardar_mesa #estados_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_table_state"); ?>', $("#formulario_guardar_mesa #estados_mesa"));

			return false;
		} else if ($('#formulario_guardar_mesa #forma_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_table_shape"); ?>', $("#formulario_guardar_mesa #forma_mesa"));

			return false;
		} else if ($('#formulario_guardar_mesa #numero_asientos_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_number_table_seats"); ?>', $("#formulario_guardar_mesa #numero_asientos_mesa"));

			return false;
		} else if ($('#formulario_guardar_mesa #forma_mesa').val() == <?= SQUARE ?>) {
			if ($('#formulario_guardar_mesa #tamano_mesa').val() == '' || $('#formulario_guardar_mesa #tamano_mesa').val() == 0) {
				mostra_mensaje('<?= lang("restobar_label_table_size"); ?>', $("#formulario_guardar_mesa #tamano_mesa"));

				return false;
			}
		} else if ($('#formulario_guardar_mesa #forma_mesa').val() == <?= CIRCLE ?> || $('#formulario_guardar_mesa #forma_mesa').val() == <?= ELLIPSE ?>) {
			if ($('#formulario_guardar_mesa #radio_mesa').val() == '' || $('#formulario_guardar_mesa #radio_mesa').val() == 0) {
				mostra_mensaje('<?= lang("restobar_label_table_radius"); ?>', $("#formulario_guardar_mesa #radio_mesa"));

				return false;
			}
		} else if ($('#formulario_guardar_mesa #forma_mesa').val() == <?= RECTANGULAR ?> || $('#formulario_guardar_mesa #forma_mesa').val() == <?= BAR ?>) {
			if ($('#formulario_guardar_mesa #ancho_mesa').val() == '' || $('#formulario_guardar_mesa #ancho_mesa').val() == 0) {
				mostra_mensaje('<?= lang("restobar_label_table_width"); ?>', $("#formulario_guardar_mesa #ancho_mesa"));

				return false;
			} else if ($('#formulario_guardar_mesa #alto_mesa').val() == '' || $('#formulario_guardar_mesa #ancho_mesa').val() == 0) {
				mostra_mensaje('<?= lang("restobar_label_table_height"); ?>', $("#formulario_guardar_mesa #alto_mesa"));

				return false;
			}
		} else if ($('#formulario_guardar_mesa #posicion_x_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_x_position_table"); ?>', $("#formulario_guardar_mesa #posicion_x_mesa"));

			return false;
		} else if ($('#formulario_guardar_mesa #posicion_y_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_y_position_table"); ?>', $("#formulario_guardar_mesa #posicion_y_mesa"));

			return false;
		}

		return true;
	}

	function abrir_modal_editar_mesa(table_id)
	{
		$.ajax({
			url: '<?= admin_url("system_settings/get_table_by_id") ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash(); ?>',
				'id_mesa': table_id
			},
		})
		.done(function(response) {
			if (response != '') {
				$("#formulario_editar_mesa #numero_mesa").val(response.numero);
				$("#formulario_editar_mesa #estados_mesa").select2('val', response.estado);
				$("#formulario_editar_mesa #forma_mesa").select2('val', response.forma_mesa);
				$("#formulario_editar_mesa #forma_mesa").trigger('change');
				$("#formulario_editar_mesa #numero_asientos_mesa").select2('val', response.numero_asientos);
				$("#formulario_editar_mesa #posicion_x_mesa").val(response.posicion_X);
				$("#formulario_editar_mesa #posicion_y_mesa").val(response.posicion_Y);
				$("#formulario_editar_mesa #id_mesa").val(response.id);
				$("#formulario_editar_mesa #id_area").val(response.area_id);

				if (response.forma_mesa == <?= SQUARE ?>) {
					$("#formulario_editar_mesa #tamano_mesa").val(response.tamano);
				} else if(response.forma_mesa == <?= CIRCLE ?> || response.forma_mesa == <?= ELLIPSE ?>) {
					$("#formulario_editar_mesa #radio_mesa").val(response.radio);
				} else {
					$("#formulario_editar_mesa #ancho_mesa").val(response.ancho);
					$("#formulario_editar_mesa #alto_mesa").val(response.alto);
				}

				$('#modal_editar_mesa').modal('show');
			} else {
				Command: toastr.error(
					"No existen datos para la mesa seleccionada",
					"Mensaje del sistema"
				);
			}
		})
		.fail(function(response) {
			console.log(response.responseText);
		});
	}

	function editar_mesa()
	{
		if (validar_campos_formulario_editar_mesa()) {
			var formulario_editar_mesa = $('#formulario_editar_mesa');

			$.ajax({
				url: formulario_editar_mesa.attr('action'),
				type: 'POST',
				dataType: 'JSON',
				data: formulario_editar_mesa.serialize(),
			})
			.done(function(response) {
				$('#modal_editar_mesa').modal('hide');

				if (response.response == 1) {
					location.reload();
				} else {
					Command: toastr.error(
						response.message,
						"Mensaje del sistema"
					);
				}
			})
			.fail(function(response) {
				console.log(response.responseText);
			});
		}
	}

	function validar_campos_formulario_editar_mesa()
	{
		if ($("#formulario_editar_mesa #numero_mesa").val() == '') {
			mostra_mensaje('<?= lang("restobar_label_table_number"); ?>', $("#formulario_editar_mesa #numero_mesa"));

			return false;
		} else if ($('#formulario_editar_mesa #estados_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_table_state"); ?>', $("#formulario_editar_mesa #estados_mesa"));

			return false;
		} else if ($('#formulario_editar_mesa #forma_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_table_shape"); ?>', $("#formulario_editar_mesa #forma_mesa"));

			return false;
		} else if ($('#formulario_editar_mesa #numero_asientos_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_number_table_seats"); ?>', $("#formulario_editar_mesa #numero_asientos_mesa"));

			return false;
		} else if ($('#formulario_editar_mesa #forma_mesa').val() == <?= SQUARE ?>) {
			if ($('#formulario_editar_mesa #tamano_mesa').val() == '' || $('#formulario_editar_mesa #tamano_mesa').val() == 0) {
				mostra_mensaje('<?= lang("restobar_label_table_size"); ?>', $("#formulario_editar_mesa #tamano_mesa"));

				return false;
			}
		} else if ($('#formulario_editar_mesa #forma_mesa').val() == <?= CIRCLE ?> || $('#formulario_editar_mesa #forma_mesa').val() == <?= ELLIPSE ?>) {
			if ($('#formulario_editar_mesa #radio_mesa').val() == '' || $('#formulario_editar_mesa #radio_mesa').val() == 0) {
				mostra_mensaje('<?= lang("restobar_label_table_radius"); ?>', $("#formulario_editar_mesa #radio_mesa"));

				return false;
			}
		} else if ($('#formulario_editar_mesa #forma_mesa').val() == <?= RECTANGULAR ?> || $('#formulario_editar_mesa #forma_mesa').val() == <?= BAR ?>) {
			if ($('#formulario_editar_mesa #ancho_mesa').val() == '' || $('#formulario_editar_mesa #ancho_mesa').val() == 0) {
				mostra_mensaje('<?= lang("restobar_label_table_width"); ?>', $("#formulario_editar_mesa #ancho_mesa"));

				return false;
			} else if ($('#formulario_editar_mesa #alto_mesa').val() == '' || $('#formulario_editar_mesa #ancho_mesa').val() == 0) {
				mostra_mensaje('<?= lang("restobar_label_table_height"); ?>', $("#formulario_editar_mesa #alto_mesa"));

				return false;
			}
		} else if ($('#formulario_editar_mesa #posicion_x_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_x_position_table"); ?>', $("#formulario_editar_mesa #posicion_x_mesa"));

			return false;
		} else if ($('#formulario_editar_mesa #posicion_y_mesa').val() == '') {
			mostra_mensaje('<?= lang("restobar_label_y_position_table"); ?>', $("#formulario_editar_mesa #posicion_y_mesa"));

			return false;
		}

		return true;
	}

	function actualizar_datos_posicion_mesa(table_position_data)
	{
		$.ajax({
			url: '<?= admin_url("system_settings/edit_table_position_data")  ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
				id_mesa: table_position_data.attrs.id,
				posicion_x_mesa: parseInt(table_position_data.x()),
				posicion_y_mesa: parseInt(table_position_data.y()),
				rotacion_mesa: table_position_data.rotation(),
			},
		})
		.done(function(response) {
			console.log(response);
		})
		.fail(function(response) {
			console.log(response.responseText);
		});
	}

	function mostra_mensaje(etiqueta, elemento)
	{
		Command: toastr.error(
			"El campo <strong>"+ etiqueta +"</strong> es obligatorio.",
			"Validación de formulario",
			{
				onHidden: function () {
					elemento.focus();
				}
			}
		);
	}
</script>