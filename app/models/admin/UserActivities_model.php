<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserActivities_model extends CI_Model {

	public function insert($data)
	{
		if ($this->db->insert('user_activities', $data)) {
			return TRUE;
		}
		return FALSE;
	}
}

/* End of file userActivities_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/userActivities_model.php */