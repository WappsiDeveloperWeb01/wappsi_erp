<?php defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/RestController.php';
require APPPATH . '/libraries/Format.php';

use chriskacerguis\RestServer\RestController;

class SalesOrder extends RestController
{
    private $shopSettings;
    private $settings;
    
    private const ORDER_SALES = 8;

    function __construct() 
    {
        parent::__construct();

        $this->load->api_model('ProductVariantsModel');
        $this->load->api_model('DocumentsTypesModel');
        $this->load->api_model('OrderSaleItemsModel');
        $this->load->api_model('PaymentMethodsModel');
        $this->load->api_model('DocumentTypesModel');
        $this->load->api_model('OrderSalesModel');
        $this->load->api_model('TaxesRatesModel');
        $this->load->api_model('CompaniesModel');
        $this->load->api_model('ProductsModel');
        $this->load->api_model('SettingsModel');
        $this->load->api_model('AddressModel');
        $this->load->api_model('GroupsModel');

        $settings = new SettingsModel();
        $this->shopSettings = $settings->getShopSettings();
        $this->settings = $settings->getSettings();
    }

    private function validate_api_key()  
    {
        $api_key = $this->input->get_request_header('API-Key');
        $api_key_hash = $this->shopSettings->api_key_hash;

        if (!password_verify($api_key, $api_key_hash)) {
            $this->response([
                'error' => 'Unauthorized'
            ], RestController::HTTP_UNAUTHORIZED);
            return false;
        }
        return true;
    }

    public function create_post() 
    {
        // $this->validate_api_key();

        $post = json_encode($this->post());
        $post = json_decode($post);

        $addresses = $post->address;
        $order = $post->order;
        $user = $post->user;
        
        $userCreated = $this->createCustomer($user, $addresses);
        $addressesCreated = $this->createAddresses($userCreated->id_wappsi, $addresses);
        $orderCreated = $this->createOrder($userCreated->id_wappsi, $order);

        if ($orderCreated) {
            $this->response([
                'message' => 'Orden almacenado con éxito.',
                "response"=> [
                    "user" => $userCreated,
                    "addresses" => $addressesCreated,
                    "order" => $orderCreated,
                ]
            ], RestController::HTTP_OK);
        } else {
            $this->response([
                'error' => 'No se pudo almacenar La Orden.'
            ], RestController::HTTP_INTERNAL_ERROR);
        }
    }

    private function createCustomer($user, $addresses) 
    {
        $customer = $this->checkIfCustomerExists($user);
        if (!$customer) {
            $DocumentTypesModel = new DocumentTypesModel();
            $CompaniesModel = new CompaniesModel();
            $GroupsModel = new GroupsModel();
    
            $documentType = $DocumentTypesModel->find(["abreviacion" => $user->doc_type]);
            $group = $GroupsModel->find(["name" => "customer"]);
            $address = $addresses->bill_address;

            $data = [
                "group_id"      => $group->id,
                "group_name"    => $group->name,
                "vat_no"        => $user->no_identification,
                "type_person"   => $user->type_of_person,
                "first_name"    => $user->name,
                "first_lastname"=> $user->last_name,
                "name"          => "$user->name $user->last_name",
                "company"       => "$user->name $user->last_name",
                "commercial_register"=> "$user->name $user->last_name",
                "tipo_documento"=> $documentType->id,
                "document_code" => $documentType->codigo_doc,
                "address"       => $address->address,
                "location"      => mb_strtoupper($address->city),
                "email"         => $user->email,
                "city_code"     => $address->city_id, /* Cambiar al campo id_wappsi 1321*/
                "city"          => mb_strtoupper($address->city),
                "state"         => mb_strtoupper($address->state),
                "country"       => mb_strtoupper($address->country)
            ];
            $customerCreated = $CompaniesModel->create($data);
            if ($customerCreated) {
                return (object) [
                    "id_store" => $user->id,
                    "id_wappsi"=> $customerCreated,
                ];
            }
        }

        return (object) [
            "id_store" => $user->id,
            "id_wappsi"=> $customer,
        ];
    }

    private function checkIfCustomerExists($user) 
    {
        $Companies = new CompaniesModel();

        $filters = [
            "email"     => $user->email,
            "group_name"=> "customer"
        ];
        $customer = $Companies->find($filters);
        if (!empty($customer)) {
            return $customer->id;
        }
        return false;
    }

    private function createAddresses($userId, $addresses) 
    {
        $billAddress = $addresses->bill_address;
        if (!$this->checkIfAddressExists($billAddress)) {
            $CompaniesModel = new CompaniesModel();
            $AddressModel = new AddressModel();

            $customer = $CompaniesModel->find(["id" => $userId]);

            $data = [
                "company_id"=> $userId,
                "sucursal"  => $customer->name,
                "direccion" => $billAddress->address,
                "city"      => mb_strtoupper($billAddress->city),
                "state"     => mb_strtoupper($billAddress->state),
                "country"   => mb_strtoupper($billAddress->country),
                "postal_code"=> $billAddress->postal_code,
                "phone"     => $billAddress->phone,
                "code"      => $customer->vat_no,
                "email"     => $customer->email,
                "city_code" => $billAddress->id_wappsi
            ];
            $billAddressCreated = $AddressModel->create($data);
        }

        $shippingAddress = $addresses->shipping_address;
        if (!$this->checkIfAddressExists($shippingAddress)) {
            $data = [
                "company_id"=> $userId,
                "sucursal"  => $customer->name,
                "direccion" => $shippingAddress->address,
                "city"      => mb_strtoupper($shippingAddress->city),
                "state"     => mb_strtoupper($shippingAddress->state),
                "country"   => mb_strtoupper($shippingAddress->country),
                "postal_code"=> $shippingAddress->postal_code,
                "phone"     => $shippingAddress->phone,
                "code"      => $customer->vat_no,
                "email"     => $customer->email,
                "city_code" => $shippingAddress->id_wappsi
            ];
            $shippingAddressCreated = $AddressModel->create($data);
        }

        return (object) [
            "shippingAddress"=> [
               "id_store"  => $shippingAddress->id, 
               "id_wappsi" => $shippingAddressCreated, 
            ],
            "billAddress"    => [
                "id_store"  => $billAddress->id, 
                "id_wappsi" => $billAddressCreated, 
             ],
        ];
    }

    private function checkIfAddressExists($address) 
    {
        if (!empty($address->id_wappsi)) {
            return true;
        }
        return false;
    }

    public function createOrder($userId, $order) 
    {
        $OrderSalesModel = new OrderSalesModel();
        $CompaniesModel = new CompaniesModel();
        
        $billerId = $this->shopSettings->biller;
        $biller = $CompaniesModel->find(["id" => $billerId]);
        $customer = $CompaniesModel->find(["id" => $userId]);
        $billerData = $CompaniesModel->billerData(["biller_id" => $billerId]);
        $totals = $this->getTotal($order->details);
        $response = [];

        $reference = $this->getReferenceNo();
        $data = [
            "date"        => date("Y-m-d H:i:s"),
            "reference_no"=> $reference->referenceNo,
            "customer_id" => $customer->id,
            "customer"    => $customer->name,
            "biller_id"   => $biller->id,
            "biller"      => $biller->name,
            "total"       => $totals->total,
            "grand_total" => $totals->grandTotal,
            "seller_id"   => $billerData->default_seller_id           
        ];
        $data = $this->getPaymentMethods($data, $order);
        if ($orderCreated = $OrderSalesModel->create($data)) {
            $this->updateConsecutive($reference->id, $reference->consecutive);
            $this->createOrderDetails($orderCreated, $order->details, $billerData);
            $response = [
                "id_store" => $order->id,
                "id_wappsi" => $orderCreated
            ];
        }

        return (object) $response;
    }

    public function getTotal($details) 
    {
        $total = 0;
        $grandTotal = 0;
        foreach ($details as $detail) {
            $total += $detail->total;
            $grandTotal += $detail->total + $detail->tax;
        }

        return (object) [
            "total" => $total,
            "grandTotal" => $grandTotal
        ];
    }
    
    public function getReferenceNo() 
    {
        $DocumentsTypesModel = new DocumentsTypesModel();

        $referenceNo = "";
        $billerId = $this->shopSettings->biller;
        $documentType = $DocumentsTypesModel->getBybiller(["biller_id" => $billerId, "module" => self::ORDER_SALES]);

        if (!empty($documentType)) {
            $id = $documentType->id;
            $prefix = $documentType->sales_prefix;
            $consecutive = $documentType->sales_consecutive;
            $hyphen = !empty($prefix) ? "-" : "";
            $referenceNo = "$prefix$hyphen$consecutive";
        }        
        
        return (object) [
            "id"         => $id,
            "prefix"     => $prefix,
            "consecutive"=> $consecutive,
            "referenceNo"=> $referenceNo
        ];
    }

    public function getPaymentMethods($data, $order) 
    {
        $PaymentMethodsModel = new PaymentMethodsModel();

        $paymentStatus = $order->paymentMethods->payment_status;
        $paymentName = $order->paymentMethods->payment_name;

        $paymentMethod = $PaymentMethodsModel->find(["code" => $paymentName]);
        if (!empty($paymentMethod)) {
            $data["payment_method"] = $paymentMethod->code;
            $data["sale_status"] = ($paymentStatus == "paid" ? "completed" : "pending");
        }

        return $data;
    }

    public function updateConsecutive($documentId, $consecutive)
    {
        $DocumentsTypesModel = new DocumentsTypesModel();

        $documentUpdated = $DocumentsTypesModel->update([
            "sales_consecutive" => ($consecutive + 1)], 
            ["id" => $documentId]);
        return $documentUpdated;
    }

    public function createOrderDetails($orderCreated, $details, $billerData) {
        $ProductVariantsModel = new ProductVariantsModel();
        $OrderSaleItemsModel = new OrderSaleItemsModel();
        $TaxesRatesModel = new TaxesRatesModel();
        $CompaniesModel = new CompaniesModel();
        $ProductsModel = new ProductsModel();
        
        foreach ($details as $detail) {
            $billerId = $this->shopSettings->biller;
            $product = $ProductsModel->find(["id" => $detail->product->id_wappsi]);
            $variant = $ProductVariantsModel->find(["id" => $detail->product->product_variation->id_wappsi]);
            $tax = $TaxesRatesModel->find(["id" => $product->tax_rate]);
            $billerData = $CompaniesModel->billerData(["biller_id" => $billerId]);

            $data = [
                "sale_id" => $orderCreated,
                "product_id" => $product->id,
                "product_code" => $product->code,
                "product_name" => $product->name,
                "product_type" => "standard",
                "option_id" => $variant->id,
                "net_unit_price" => $detail->price,
                "unit_price" => ($detail->total + $detail->tax),
                "quantity" => $detail->quantity,
                "warehouse_id" => $billerData->default_warehouse_id,
                "item_tax" => $detail->tax,
                "quantity_delivered" => 0,
                "subtotal" => $detail->total,
                "unit_price" => ($detail->total + $detail->tax),
                "unit_quantity" => $detail->quantity,
                "price_before_tax" => $detail->price,
                "tax_rate_id" => $tax->id,
                "tax" => ($tax->type == 1) ? (int) $tax->rate . "%" : $tax->rate,
                "seller_id"   => $billerData->default_seller_id,

            ];
            $itemsCreated = $OrderSaleItemsModel->create($data);
            if ($itemsCreated) {
                return true;
            }
            return false;
        }

    }
}
