<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo sprintf(lang('edit_brand'), lang('brand')); ?></h4>
        </div>
        <?php $attrib = ['id' => 'edit_brand_form'];
        echo admin_form_open_multipart("system_settings/edit_brand/" . $brand->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>

            <div class="form-group">
                <?= lang('code', 'code'); ?>
                <div class="input-group">
                    <?= form_input('code', $brand->code, 'class="form-control validate_code" id="code" data-module="brand"  data-id="'.$brand->id.'" required'.($brand->code_consecutive == 1 ? " readonly" : "")); ?>
                    <?php if ($brand->code_consecutive == 0): ?>
                        <span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('random_code_text') ?>">
                            <i class="fa fa-random"></i>
                        </span>
                        <span class="input-group-addon pointer" id="consecutive_code" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('consecutive_code_text') ?>">
                            <b>123</b>
                        </span>
                    <?php else: ?>
                        <span class="input-group-addon pointer" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('code_blocked_by_consecutive') ?>">
                            <i class="fa-solid fa-ban"></i>
                        </span>
                    <?php endif ?>
                </div>
                <input type="hidden" name="code_consecutive" class="code_consecutive_setted">
            </div>

            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', $brand->name, 'class="form-control gen_slug" id="name" required="required"'); ?>
            </div>

            <div class="form-group all">
                <?= lang('slug', 'slug'); ?>
                <?= form_input('slug', set_value('slug', $brand->slug), 'class="form-control tip" id="slug" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang("image", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>

            <div class="form-group">
                <label>
                    <input type="checkbox" name="hide_ecommerce" id="hide_ecommerce" value="1" <?= $brand->hide_ecommerce == 1 ? 'checked' : '' ?>>
                    <?= lang('hide_ecommerce', 'hide_ecommerce') ?>
                </label>
            </div>

            <?php echo form_hidden('id', $brand->id); ?>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="edit_brand" value="1">
            <button class="btn btn-primary submit" type="button"><?= lang('submit') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
    $(document).ready(function() {
        $(document).on('click', '.submit', function(){
            if ($('#edit_brand_form').valid()) {
                $('#edit_brand_form').submit();
            }
        });
        $('.gen_slug').change(function(e) {
            getSlug($(this).val(), 'brand');
        });
    });
</script>