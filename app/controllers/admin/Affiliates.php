<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Affiliates extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('affiliate', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
    }

    public function index()
    {
        if (!$this->Owner && !$this->Admin) {
            $this->sma->checkPermissions();
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('affiliates/index', ['page_title' => lang('affiliates')], $this->data);
    }

    public function get_affiliates()
    {
        if (!$this->Owner && !$this->Admin) {
            $this->sma->checkPermissions('index');
        }

        $this->load->library('datatables');
        $this->datatables->select("sma_companies.id AS id_cliente,
    													companies.name AS nombre_cliente,
  														companies.vat_no AS numero_documento,
  														companies.phone AS telefono,
  														companies.email AS correo,
  														companies.city AS ciudad,
  														companies.country AS pais,
  														IF (status = 1, 'fas fa-user-slash', 'fas fa-user-check') AS icono_activar_inactivar,
															IF (status = 1, '" . lang('disable_affiliate') . "', '" . lang('enable_affiliate') . "') AS texto_activar_inactivar,
															IF (status = 1, '" . admin_url('affiliates/disable') . "', '" . admin_url('affiliates/enable') . "') AS enlace_activar_inactivar");
        $this->datatables->from("companies");
        $this->datatables->where("group_name", "affiliate");
        $this->datatables->join("documentypes", "documentypes.id = companies.tipo_documento");

        if ($this->Owner || $this->Admin) {

            $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                                <div class=\"btn-group text-left\">
                                                    <button type=\"button\" class=\"btn btn-default new-button new-button-sm btn-xs dropdown-toggle\" data-toggle=\"dropdown\"  data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                                                      <i class='fas fa-ellipsis-v fa-lg'></i>
                                                    </button>
                                                    <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                        <li>
                                                            <a class=\"tip\" href=\"" . admin_url('affiliates/edit/$1') . "\" data-toggle=\"modal\" data-target=\"#myModal\">
                                                              <i class=\"fa fa-edit\"></i>
                                                              " . $this->lang->line("edit_affiliate") . "
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href=\"#\" class=\"tip po\" title=\"<b>$3</b>\" data-content=\"<p>" . lang('r_u_sure') . "</p>
                                                              <a class='btn btn-danger po-delete' href='$4/$1'>" . lang('i_m_sure') . "</a>
                                                              <button class='btn po-close'>" . lang('no') . "</button>\"  rel=\"popover\"><i class=\"$2\" style=\"font-size: 12px;\"></i>
                                                              $3
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href=\"#\" class=\"tip po\" title=\"<b>" . lang('delete_affiliate') . "</b>\" data-content=\"<p>" . lang('r_u_sure') . "</p>
                                                              <a class='btn btn-danger po-delete' href='" . admin_url('affiliates/delete') . "/$1'>" . lang('i_m_sure') . "</a>
                                                              <button class='btn po-close'>" . lang('no') . "</button>\"  rel=\"popover\"><i class=\"fa fa-trash\" style=\"font-size: 12px;\"></i>
                                                              " . lang('delete_affiliate') . "
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>", "id_cliente, icono_activar_inactivar, texto_activar_inactivar, enlace_activar_inactivar");
        } else {
            $this->datatables->add_column("Actions", '<div class="text-center"><a class="tip" title="' . $this->lang->line("edit_affiliate") . '" href="' . admin_url('affiliates/edit/$1') . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></a></div>', "id_cliente");
        }

        $this->datatables->unset_column('icono_activar_inactivar');
        $this->datatables->unset_column('texto_activar_inactivar');
        $this->datatables->unset_column('enlace_activar_inactivar');

        echo $this->datatables->generate();
    }

    public function add()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('document_type', lang("id_document_type"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('document_number', lang("vat_no"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('first_name', lang("first_name_affiliate"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('second_name', lang("second_name_affiliate"), 'trim');
        $this->form_validation->set_rules('first_lastname', lang("first_lastname_affiliate"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('second_lastname', lang("second_lastname_affiliate"), 'trim');
        $this->form_validation->set_rules('email', lang("email_address"), 'trim|required|valid_email', ['required' => lang('validation_message_required'), 'valid_email' => lang('validation_message_valid_email')]);
        $this->form_validation->set_rules('phone', lang("phone"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('address', lang("address"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('country', lang("country"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('state', lang("state"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('city', lang("city"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('billers[]', lang("biller"), 'trim|required', ['required' => lang('validation_message_required')]);
        if ($this->form_validation->run() === TRUE) {
            if ($this->companies_model->validate_existing_affiliate(['vat_no'=>$this->input->post('document_number')])) {
                $this->session->set_flashdata('error', 'Ya existe un afiliado con el número de documento ingresado.');
                admin_redirect('affiliates');
            }

            $nombre_completo = $this->input->post('first_name') . (!is_null($this->input->post('second_name')) ? " " . $this->input->post('second_name') : " ") . $this->input->post('first_lastname') . (!is_null($this->input->post('second_lastname')) ? " " . $this->input->post('second_lastname') : '');

            $addresses = $this->input->post('addresses[]');

            $affiliate_data = [
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => 11,
                'group_name' => 'affiliate',
                'company' => $nombre_completo,
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('document_number'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $nombre_completo,
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'type_person' => 1,
                'tipo_regimen' => 1,
                'tipo_documento' => $this->input->post('document_type'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code'),
                "created_by" => $this->session->userdata('user_id')
            ];
            $addresses = explode(",", $addresses[0]);
        }

        if ($this->form_validation->run() === TRUE && $affiliate_id = $this->companies_model->insert_seller($affiliate_data, $addresses)) {
            $biller_data = [];
            $billers = $this->input->post('billers');
            foreach ($billers as $biller) {
                $biller_data[] = [
                    'biller_id' => $biller,
                    'companies_id' => $affiliate_id
                ];
            }

            $this->companies_model->insert_billers_seller($biller_data);

            $this->session->set_flashdata('message', lang('added_affiliate'));
            admin_redirect('affiliates');
        } else {
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['billers'] = $this->companies_model->get_billers();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->load_view($this->theme . 'affiliates/add', $this->data);
        }
    }

    public function edit($id)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('document_type', lang("id_document_type"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('document_number', lang("vat_no"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('first_name', lang("first_name_affiliate"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('second_name', lang("second_name_affiliate"), 'trim');
        $this->form_validation->set_rules('first_lastname', lang("first_lastname_affiliate"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('second_lastname', lang("second_lastname_affiliate"), 'trim');
        $this->form_validation->set_rules('email', lang("email_address"), 'trim|required|valid_email', ['required' => lang('validation_message_required'), 'valid_email' => lang('validation_message_valid_email')]);
        $this->form_validation->set_rules('phone', lang("phone"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('address', lang("address"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('country', lang("country"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('state', lang("state"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('city', lang("city"), 'trim|required', ['required' => lang('validation_message_required')]);
        $this->form_validation->set_rules('billers[]', lang("biller"), 'trim|required', ['required' => lang('validation_message_required')]);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->form_validation->run() === TRUE) {
            $fields = [
                'vat_no' => $this->input->post('document_number'),
                'email' => $this->input->post('email'),
            ];

            if ($this->companies_model->validate_existing_affiliate($fields, $id)) {
                $this->session->set_flashdata('error', 'Ya existe un afiliado con el número de documento y/o email ingresado.');
                admin_redirect('affiliates');
            }
            $nombre_completo = $this->input->post('first_name') . (!is_null($this->input->post('second_name')) ? " " . $this->input->post('second_name') . " " : " ") . $this->input->post('first_lastname') . (!is_null($this->input->post('second_lastname')) ? " " . $this->input->post('second_lastname') : '');
            $addresses = $this->input->post('addresses');
            $affiliate_data = [
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'company' => $nombre_completo,
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('document_number'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $nombre_completo,
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'tipo_documento' => $this->input->post('document_type'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code')
            ];
        }

        if ($this->form_validation->run() === TRUE && $this->companies_model->update_seller($affiliate_data, $id, $addresses)) {
            $this->companies_model->delete_billers_seller_by_seller_id($id);
            $biller_data = [];
            $billers = $this->input->post('billers');
            foreach ($billers as $biller) {
                $biller_data[] = [
                    'biller_id' => $biller,
                    'companies_id' => $id
                ];
            }
            $this->companies_model->insert_billers_seller($biller_data);
            $this->session->set_flashdata('message', lang('updated_affiliate'));
            admin_redirect('affiliates');
        } else {
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['affiliate'] = $this->companies_model->getCompanyByID($id);
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['billers_affiliate'] = $this->companies_model->get_billers_seller_by_seller_id($id);
            $this->data['addresses'] = $this->site->get_all_customer_addresses($id);

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->load_view($this->theme . 'affiliates/edit', $this->data);
        }
    }

    public function delete($id)
    {
        $this->sma->checkPermissions(NULL, TRUE);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($this->companies_model->existing_sales_by_affiliate_id($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("existing_affiliate_sales")));
            exit();
        }
        if ($this->companies_model->delete_affiliate($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang('affiliate_removed')));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("affiliate_not_removed")));
        }
    }

    public function disable($id)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->disable_seller($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("disabled_affiliate")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("disabled_not_affiliate")));
        }
    }

    public function enable($id)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->enable_seller($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang('enabled_affiliate')));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang('enabled_not_affiliate')));
        }
    }

    public function affiliate_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() === TRUE) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $error = FALSE;
                    foreach ($_POST['val'] as $id) {
                        if ($this->companies_model->existing_sales_by_affiliate_id($id) === FALSE) {
                            if (!$this->companies_model->delete_affiliate($id)) {
                                $error = TRUE;
                            }
                        } else {
                            $error = TRUE;
                        }
                    }

                    if ($error) {
                        $this->session->set_flashdata('warning', lang('existing_affiliate_sales'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("affiliates_removed"));
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('affiliates'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('vat_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('city'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->companies_model->get_affiliate($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->vat_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->city);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'affiliates_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_affiliate_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function validate_vat_no($vat_no)
    {
        $validate = $this->companies_model->validate_vat_no($vat_no, 'affiliate');
        if ($validate) {
            echo "true";
        } else {
            echo "false";
        }
    }


}

/* End of file affiliate.php */
/* Location: .//C/wamp64/www/wappsi_inspinia_implementation/app/controllers/admin/affiliate.php */