<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        if ($this->biller_logo == 2) {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,4,5,26);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,4,15,28);
        }

        if ($this->show_document_type_header == 1) {
            $this->SetFont('Arial','',$this->fuente);
            $this->setXY(13, 36);
            $this->MultiCell(123, 3,  $this->sma->utf8Decode(strip_tags($this->sma->decode_html($this->invoice_header))), 0, 'L');
        }

        $cx = 33;
        $cy = 5;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->MultiCell(105, 3.5,  $this->sma->utf8Decode($this->Settings->nombre_comercial), '', 'L');
        $cy  = $this->getY();
        $cy+=1.3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(102, 3 , $this->sma->utf8Decode('Nit : '.$this->Settings->numero_documento.($this->Settings->digito_verificacion != "" ? '-'.$this->Settings->digito_verificacion : '')."  ".$this->tipo_regimen),'',1,'L');
        if ($this->ciiu_code) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(102, 3 , $this->sma->utf8Decode("Códigos CIIU : ".$this->ciiu_code),'',1,'L');
        }
        if ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(102, 3 , $this->sma->utf8Decode(
                                                ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? 'Auto retenedor de ' : '').
                                                ($this->Settings->fuente_retainer ? 'Fuente' : '').
                                                ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '').'IVA ' : '').
                                                ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '').'ICA ' : '')
                                            ),'',1,'L');
        }
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(102, 3 , $this->sma->utf8Decode($this->biller->address),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(102, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(102, 3 , $this->sma->utf8Decode($this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(102, 3 , $this->sma->utf8Decode($this->biller->email),'',1,'L');

        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(102, 3 , $this->sma->utf8Decode(isset($this->Settings->url_web) ? $this->Settings->url_web : ''),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+(6));
        $this->RoundedRect(116, 4, 50, 28, 3, '1234', '');
        $cx = 116;
        $cy = 7;
        $this->setXY($cx, $cy);
        $this->MultiCell(26, 5,  $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'FACTURA DE VENTA'), '', 'L');
        $cy +=15;
        $this->setXY($cx, $cy);
        $this->Cell(107, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'L');
        if ($this->factura->fe_aceptado == ACCEPTED) {
            $this->Image(base_url().'themes/default/admin/assets/images/qr_code/'.$this->factura->reference_no.'.png', 180, 12.5, 26);
        }

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(4, 36, 161.7, 5, 3, '12', 'DF');
        $this->RoundedRect(4, 41, 161.7, 24, 3, '34', '');
        $cx = 4;
        $cy = 36;
        $this->setXY($cx, $cy);
        $this->Cell(0, 5 , $this->sma->utf8Decode('Información del cliente'),'B',1,'C');
        $cx += 1;
        $cy += 7;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->phone_negocio),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $altura = 5;
        $adicional_altura = 1;
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);

        $cx = 4;
        $cy = 66.5;
        $this->setXY($cx, $cy);
        $this->Cell(36.425, $altura , $this->sma->utf8Decode('Fecha Factura'),'TLB',1,'C',1);
        $cx += 36.425;

        $this->setXY($cx, $cy);
        $this->Cell(36.425, $altura , $this->sma->utf8Decode('Fecha Vencimiento'),'TLB',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cx -= 36.425;
        $cy += $altura;

        $this->setXY($cx, $cy);
        $this->Cell(36.425, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'LB',1,'C');
        $cx += 36.425;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".($this->factura->payment_term > 0 ? $this->factura->payment_term : 0)." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(36.425, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'LB',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);

        $cx = 77;
        $cy = 66.5;
        $this->setXY($cx, $cy);
        $this->Cell(48.425, $altura , $this->sma->utf8Decode('Vendedor'),'TLB',1,'C',1);
        $cx += 48.425;
        $this->setXY($cx, $cy);
        $this->Cell(40.425, $altura , $this->sma->utf8Decode('Forma de Pago'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cx -= 48.425;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(48.425, $altura+$adicional_altura , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'LB',1,'C');
        $cx += 48.425;
        $this->setXY($cx, $cy);
        $this->Cell(40.425, $altura+$adicional_altura , $this->sma->utf8Decode($this->sale_payment_method),'LBR',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 32.925;
        $this->setXY($cx, $cy);

        $this->ln(1.7);

        $this->SetFont('Arial','B',$this->fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $current_X = $this->getX();
        $current_Y = $this->getY();
        $pr_name_wdth = 60.3;

        if (!$this->document_type_invoice_format || $this->document_type_invoice_format->show_product_numeration == 1) {
            $this->Cell(9.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        } else {
            $pr_name_wdth += 9.6;
        }
        if ($this->document_type_invoice_format && $this->document_type_invoice_format->product_detail_discount == 0) {
            $pr_name_wdth += 10.6;
        }

        $this->Cell($pr_name_wdth, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);

        if ($this->document_type_invoice_format && $this->document_type_invoice_format->product_detail_discount == 1) {
            $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        }
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(24.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(11.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10  , 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(24.6, 8, $this->sma->utf8Decode(''),'TBLR',1,'C',1);

        $this->setXY($current_X, $current_Y);

        if (!$this->document_type_invoice_format || $this->document_type_invoice_format->show_product_numeration == 1) {
            $this->Cell(9.6 , 8, $this->sma->utf8Decode('Ítem'),'R',0,'C',0);
        }

        $this->Cell($pr_name_wdth, 8, $this->sma->utf8Decode('Nombre producto'),'',0,'C',0);

        if (!$this->document_type_invoice_format || ($this->document_type_invoice_format && $this->document_type_invoice_format->product_detail_discount == 1)) {
            $this->Cell(10.6, 8, $this->sma->utf8Decode('Dcto'),'',0,'C',0);
        }
        $this->Cell(10.6, 8, $this->sma->utf8Decode(lang('iva')),'',0,'C',0);
        $this->Cell(24.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(11.6, 8, $this->sma->utf8Decode('Cantidad'),'',0,'C',0);
        $this->Cell(10  , 8, $this->sma->utf8Decode('UM'),'',0,'C',0);
        $this->Cell(24.6, 8, $this->sma->utf8Decode('Subtotal'),'',1,'C',0);

        $this->setXY($current_X, $current_Y+4);

        if (!$this->document_type_invoice_format || $this->document_type_invoice_format->show_product_numeration == 1) {
            $this->Cell(9.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        }

        $this->Cell($pr_name_wdth, 4, $this->sma->utf8Decode(''),'',0,'C',0);

        if (!$this->document_type_invoice_format || ($this->document_type_invoice_format && $this->document_type_invoice_format->product_detail_discount == 1)) {
            $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        }
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(24.6, 4, $this->sma->utf8Decode(lang('iva_included')),'',0,'C',0);
        $this->Cell(11.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(10  , 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(24.6, 4, $this->sma->utf8Decode(''),'',1,'C',0);
    }

    function Footer()
    {
        // Print centered page number
        if ($this->description2 != '') {
            // $this->SetXY(15, -70);
            $this->SetFont('Arial','',$this->fuente);
            $this->SetXY(4, -24);
            $this->RoundedRect(4, 254, 161.7, 10, 0.8, '1234', '');
            $this->MultiCell(161.7, 3,  $this->sma->utf8Decode($this->factura->resolucion), 0, 'C');
            $this->MultiCell(161.7, 3,  $this->sma->utf8Decode('CUFE: '. $this->factura->cufe), 0, 'C');
        } else {
            $this->SetXY(4, -25);
            $this->RoundedRect(4, 255, 161.7, 10, 1, '1234', '');
            $this->MultiCell(161.7, 3,  $this->sma->utf8Decode($this->factura->resolucion), 0, 'C');
            $this->MultiCell(161.7, 3,  $this->sma->utf8Decode('CUFE: '. $this->factura->cufe), 0, 'C');
        }

        $this->SetXY(4, -14);
        $img_x = $this->getX()+110;
        $img_y = $this->getY()+5;
        $this->Cell(155.7, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS NIT 901.090.070-9     www.wappsi.com'),'',1,'L');
        $this->Image(base_url($this->technologyProviderLogo), $img_x, $img_y, 45);
        $this->SetXY(156, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Página N° '.$this->PageNo()),'',1,'C');
    }

    function reduceTextToDescription1($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        $max_size = 520;
        if (strlen($text) > $max_size) {
            $text = substr($text, 0, ($max_size - 5));
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
}
$pdf = new PDF('P', 'mm', array(216, 170));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(4, 4);

$fuente = 6.5;
$adicional_fuente = 1.5;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;

$pdf->technologyProviderLogo = $technologyProviderLogo;
$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sale_payment_method = $sale_payment_method;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->value_decimals = $value_decimals;
$pdf->ciiu_code = $ciiu_code;
$pdf->invoice_header = $invoice_header ? $invoice_header : "";
$pdf->show_document_type_header = $show_document_type_header;
$pdf->document_type_invoice_format = $document_type_invoice_format;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();

$maximo_footer = 175;

$pdf->SetFont('Arial','',$fuente);

$taxes = [];
$total_bruto = 0;
$references = [];
$inv_total_before_promo = 0;

    foreach ($rows as $item) {

        if ($product_detail_promo == 1 && $item->price_before_promo > 0) {
            $inv_total_before_promo = ($item->price_before_promo - ($item->tax_method == 1 ? $item->net_unit_price : $item->unit_price)) * $item->quantity;
        }

        $total_bruto += (($item->net_unit_price + ($item->item_discount / $item->quantity)) * $item->quantity) * $trmrate;

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
        }

        if (isset($references[$item->id])) {
            $references[$item->id]['pr_subtotal'] += $item->subtotal * $trmrate;
            $references[$item->id]['pr_quantity'] += $item->quantity;
        } else {
            $references[$item->id]['pr_code'] = ($show_code == 1 ? $item->product_code : $item->reference);
            $pr_name = $pdf->sma->reduce_text_length(($item->under_cost_authorized == 1 ? '     ' : '').
                                                $item->product_name.
                                                (!is_null($item->variant) && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? "( ".$item->variant." )" : '').
                                                ($pdf->Settings->show_brand_in_product_search && !is_null($item->brand_name) ? " - ".$item->brand_name : '').
                                                (!is_null($item->serial_no) ? " ".$item->serial_no : '').
                                                ($product_detail_promo == 1 && $item->price_before_promo > 0 ? " (".lang('discount_by_promo')." -".$this->sma->formatDecimal((($item->price_before_promo - ($item->tax_method == 1 ? $item->net_unit_price : $item->unit_price)) / $item->price_before_promo) * 100, 2)." %)" : '' )
                                            , 500);
            // $pr_name .=$pr_name;
            $references[$item->id]['pr_name'] = $pr_name;
            $references[$item->id]['pr_discount'] = $item->discount;
            $references[$item->id]['pr_individual_discount'] = $item->item_discount / $item->quantity;
            $perc = $item->discount ? $item->discount : ((($item->item_discount / $item->quantity) * 100) / ($item->unit_price + ($item->item_discount / $item->quantity)));

            $references[$item->id]['perc_discount'] =  $item->discount ? $item->discount : $this->sma->formatDecimal($perc, 2)."%";
            $references[$item->id]['pr_tax'] = $item->tax != '' ? $item->tax : 0;
            $references[$item->id]['pr_individual_tax'] = $item->item_tax / $item->quantity;
            $references[$item->id]['unit_price_1'] = ($item->net_unit_price + ($item->item_discount / $item->quantity)) * $trmrate;
            $references[$item->id]['unit_price_2'] = $item->net_unit_price * $trmrate;
            $references[$item->id]['unit_price_3'] = $item->unit_price * $trmrate;
            $references[$item->id]['pr_subtotal'] = $item->subtotal * $trmrate;
            $references[$item->id]['pr_quantity'] = $item->quantity;
            $references[$item->id]['operation_value'] = $item->operation_value;
            $references[$item->id]['under_cost_authorized'] = $item->under_cost_authorized;
            $references[$item->id]['operator'] = $item->operator;
            $references[$item->id]['product_unit_code'] = $item->product_unit_code;
        }
    }
    $cnt = 1;
    $total_items_qty = 0;
    foreach ($references as $reference => $data) {
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }
        $pr_name_wdth = 60.3;
        $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        if (!$document_type_invoice_format || $document_type_invoice_format->show_product_numeration == 1) {
            $columns[0]['inicio_x'] = $pdf->getX();
            $pdf->Cell( 9.6, 5, $this->sma->utf8Decode($cnt),'',0,'C',0);
            $columns[0]['fin_x'] = $pdf->getX();
        } else {
            $pr_name_wdth += 9.6;
        }
        if ($document_type_invoice_format && $document_type_invoice_format->product_detail_discount == 0) {
            $pr_name_wdth += 10.6;
        }
        $columns[1]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $pdf->setXY($cX, $cY+0.5);
        if ($data['under_cost_authorized'] == 1) {
            $unlock_icon_X = $pdf->getX();
            $unlock_icon_Y = $pdf->getY();
            $pdf->Image(base_url().'assets/images/unlock-icon.png',$unlock_icon_X+1, $unlock_icon_Y+0.6,2.6);
        }
        $pdf->MultiCell($pr_name_wdth,3, $this->sma->utf8Decode($data['pr_name']),'','L');
        $columns[1]['fin_x'] = $cX+$pr_name_wdth;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+$pr_name_wdth, $cY);
        if (!$document_type_invoice_format || ($document_type_invoice_format && $document_type_invoice_format->product_detail_discount == 1)) {
            $columns[2]['inicio_x'] = $pdf->getX();
            $pdf->Cell(10.6, 4, $this->sma->utf8Decode($data['perc_discount']),'',0,'C',0);
            $columns[2]['fin_x'] = $pdf->getX();
        }
        $columns[3]['inicio_x'] = $pdf->getX();
        $pdf->Cell(10.6, 4, $this->sma->utf8Decode($data['pr_tax']),'',0,'C',0);
        $columns[3]['fin_x'] = $pdf->getX();
        $columns[4]['inicio_x'] = $pdf->getX();
        $pdf->Cell(24.6, 4, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_3'])),'',0,'C',0);
        $columns[4]['fin_x'] = $pdf->getX();
        // INICIO
        $pr_quantity = $data['pr_quantity'];
        $pr_unit_code = NULL;
        if (isset($data['operator'])) {
            if ($data['operator'] == "*") {
                $pr_quantity = $data['pr_quantity'] / $data['operation_value'];
            } else if ($data['operator'] == "/") {
                $pr_quantity = $data['pr_quantity'] * $data['operation_value'];
            } else if ($data['operator'] == "+") {
                $pr_quantity = $data['pr_quantity'] - $data['operation_value'];
            } else if ($data['operator'] == "-") {
                $pr_quantity = $data['pr_quantity'] + $data['operation_value'];
            }
        }
        $pr_unit_code = $data['product_unit_code'];
        $columns[5]['inicio_x'] = $pdf->getX();
        $pdf->Cell(11.6, 4, $this->sma->utf8Decode($this->sma->formatQuantity($pr_quantity, $qty_decimals)),'',0,'C',0);
        $total_items_qty += $data['pr_quantity'];
        $columns[5]['fin_x'] = $pdf->getX();
        $columns[6]['inicio_x'] = $pdf->getX();
        $pdf->Cell(10, 4, $this->sma->utf8Decode($pr_unit_code),'',0,'C',0);
        $total_items_qty += $data['pr_quantity'];
        $columns[6]['fin_x'] = $pdf->getX();
        // FIN

        $columns[7]['inicio_x'] = $pdf->getX();
        $pdf->Cell(24.6, 4, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['pr_subtotal'])),'',0,'C',0);
        $columns[7]['fin_x'] = $pdf->getX();
            $fin_fila_X = $pdf->getX();
            $fin_fila_Y = $pdf->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
            foreach ($columns as $key => $data) {
                $ancho_column = $data['fin_x'] - $data['inicio_x'];
                if ($altura_fila < 3) {
                    $altura_fila = 3;
                }
                $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
            }
            $pdf->ln($altura_fila);
        $cnt++;
    }

    if ($document_type_invoice_format && $document_type_invoice_format->totalize_quantity == 1) {
        $pdf->Cell(162.8, 4, $this->sma->utf8Decode('Total cantidades'),'LB',0,'R');
        $pdf->Cell(11.6, 4, $this->sma->utf8Decode($this->sma->formatQuantity($total_items_qty)),'LB',0,'C');
        $pdf->Cell(21.6, 4, $this->sma->utf8Decode(''),'LBR',1,'R');
    }

if ($pdf->getY() > 160) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

// $pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(1,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(80,5, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->SetFont('Arial','',$fuente);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(5);
$pdf->SetFont('Arial','',$fuente);
$pdf->MultiCell(80, 3, $this->sma->utf8Decode($number_convert->convertir((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate), (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency))), 0, 'L');
$pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
$pdf->setX(13);
$pdf->setXY($current_x, $current_y+8);


$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(80,0, $this->sma->utf8Decode(''),'B',1,'C');
$pdf->Cell(20,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
$pdf->Cell(20,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
$pdf->Cell(20,5, $this->sma->utf8Decode('Valor Impuesto'),'',0,'C');
$pdf->Cell(20,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');

$pdf->SetFont('Arial','',$fuente);
$total_tax_base = 0;
$total_tax_amount = 0;

foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $total =$arr['base'] + $arr['tax'];
    $pdf->Cell(20,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"),'',1,'C');
    $pdf->setXY($current_x + 20, $current_y);
    $pdf->setX($current_x + 20);
    $pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['base'] * $trmrate)),'',1,'C');
    $total_tax_base += $arr['base'] * $trmrate;
    $pdf->setXY($current_x + (20 * 2), $current_y);
    $pdf->setX($current_x + (20 * 2));
    $pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['tax'] * $trmrate)),'',1,'C');
    $total_tax_amount += $arr['tax'] * $trmrate;
    $pdf->setXY($current_x + (20 * 3), $current_y);
    $pdf->setX($current_x + 20 * 3);
    $pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total)),'',1,'C');
}

if ($inv->order_tax > 0) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->Cell(20,3, $this->sma->utf8Decode("(SP) ".$this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id]." %" : "0%"))),'',1,'C');
    $pdf->setXY($current_x + 20, $current_y);
    $pdf->setX($current_x + 20);
    $pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)),'',1,'C');
    $total_tax_base +=  $inv->total * $trmrate;
    $pdf->setXY($current_x + (20 * 2), $current_y);
    $pdf->setX($current_x + (20 * 2));
    $pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax * $trmrate)),'',1,'C');
    $total_tax_amount += $inv->order_tax * $trmrate;
    $pdf->setXY($current_x + (20 * 3), $current_y);
    $pdf->setX($current_x + 20 * 3);
    $pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total * $trmrate)),'',1,'C');
}

if ($inv->consumption_sales > 0) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->Cell(20,3, $this->sma->utf8Decode('Impoconsumo (L&C)'),'',1,'C');
    $pdf->setXY($current_x + 20, $current_y);
    $pdf->setX($current_x + 20);
    $pdf->Cell(20,3, $this->sma->utf8Decode(''),'',1,'C');
    $pdf->setXY($current_x + (20 * 2), $current_y);
    $pdf->setX($current_x + (20 * 2));
    $pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->consumption_sales * $trmrate)),'',1,'C');
    $total_tax_amount += $inv->consumption_sales * $trmrate;
    $pdf->setXY($current_x + (20 * 3), $current_y);
    $pdf->setX($current_x + 20 * 3);
    $pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->consumption_sales * $trmrate)),'',1,'C');
}


$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(20,3, $this->sma->utf8Decode("Total"),'',1,'C');
$pdf->setXY($current_x + 20, $current_y);
$pdf->setX($current_x + 20);
$pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base)),'',1,'C');

$pdf->setXY($current_x + (20 * 2), $current_y);
$pdf->setX($current_x + (20 * 2));
$pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_amount)),'',1,'C');

$pdf->setXY($current_x + (20 * 3), $current_y);
$pdf->setX($current_x + 20 * 3);
$pdf->Cell(20,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base + $total_tax_amount)),'',1,'C');
$pdf->SetFont('Arial','',$fuente);

$tax_summary_end = $pdf->getY() + 3;

//derecha

$pdf->setXY($cX_items_finished+80, $cY_items_finished);

$current_x = $pdf->getX()+30;
$current_y = $pdf->getY();

$left_width = 25.85;
$right_width = 25.85;

if ($inv->product_discount > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode('Total bruto'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->total+ $inv->product_discount ) * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

if ($inv->product_discount > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode('Descuento a productos'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode(($inv->product_discount > 0 ? "- " : "").$this->sma->formatValue($value_decimals, ($inv->product_discount) * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

$pdf->setXY($current_x, $current_y);
$pdf->cell($left_width, 5, $this->sma->utf8Decode('Subtotal'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$left_width, $current_y);
$pdf->cell($right_width, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($left_width, 5, $this->sma->utf8Decode('Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$left_width, $current_y);
$pdf->cell($right_width, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total_tax * $trmrate)), 'TBR', 1, 'R');

if ($inv->order_discount > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode('Descuento general'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode(($inv->order_discount > 0 ? "- " : "").$this->sma->formatValue($value_decimals, ($inv->order_discount) * $trmrate)), 'TBR', 1, 'R');
}


if ($inv->rete_fuente_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode('Retención en la fuente'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode(($inv->rete_fuente_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');
}


if ($inv->rete_iva_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode('Retención al IVA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode(($inv->rete_iva_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode('Retención al ICA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode(($inv->rete_ica_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_other_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode(' '.lang('rete_other')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode(($inv->rete_other_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_bomberil_id && $inv->rete_bomberil_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode(' '.lang('rete_bomberil')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode(($inv->rete_bomberil_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_bomberil_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode(' '.lang('rete_autoaviso')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode(($inv->rete_autoaviso_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_autoaviso_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->shipping > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($left_width, 5, $this->sma->utf8Decode('Serv. Transporte'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$left_width, $current_y);
    $pdf->cell($right_width, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell($left_width, 5, $this->sma->utf8Decode('TOTAL A PAGAR'), 'TBLR', 1, 'L', 1);
$pdf->setXY($current_x+$left_width, $current_y);
$pdf->cell($right_width, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate)), 'TBR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente+1.5);

$inv_total_summary_end = $pdf->getY() + 3;

if ($tax_summary_end > $inv_total_summary_end) {
    $pdf->setXY(13, $tax_summary_end);
} else {
    $pdf->setXY(13, $inv_total_summary_end);
}

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->cell(1,3,"",0,'L');
$pdf->SetFont('Arial','B',$fuente+1.5);
if (!empty($description1)) {
    $pdf->ln(1);
    $pdf->cell(10, 3, $this->sma->utf8Decode('Observaciones'), '', 1, 'L');
    $pdf->setX($current_x+1);
}
$pdf->SetFont('Arial','',$fuente+1.5);
$tp_text = '';
if ($inv_total_before_promo > 0) {
    // $tp = $this->sma->formatDecimal((($inv_total_before_promo - $inv->grand_total) / $inv_total_before_promo) * 100, 2)." %";
    $tp = " $ ".$this->sma->formatMoney($inv_total_before_promo + $inv->order_discount);
    $tp_text = "<p class='text-center'>".sprintf(lang('invoice_total_promo_discount'), $tp)."</p>";
}
if (!empty($description1.$tp_text)) {
    $pdf->MultiCell(226, 3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1.$tp_text)),0,'L');
}
$footer_note_x = 3;
$current_x = 3;
$current_y = $pdf->getY()-1;
$square_initial_y = $pdf->getY()-1;
$pdf->SetFont('Arial','',$pdf->fuente+0.5);
$pdf->setXY($current_x, $current_y);
if (isset($pdf->cost_center) && $pdf->cost_center) {
    $pdf->setXY($current_x+3, $current_y+10);
    $pdf->MultiCell(161.7,3,$this->sma->utf8Decode($pdf->reduceTextToDescription2( "Centro de costo : ".$pdf->cost_center->name." (".$pdf->cost_center->code.") \n".$pdf->description2)), 0, 'L');
} else {
    $pdf->MultiCell(161.7,3,$this->sma->utf8Decode($pdf->reduceTextToDescription2($pdf->description2)), 0, 'L');
}
$square_end_y = $pdf->getY();
$square_height = $square_end_y - $square_initial_y;
if (!empty($pdf->description2) || isset($pdf->cost_center) && $pdf->cost_center) {
    // $pdf->RoundedRect($footer_note_x, $square_initial_y, 131.7, $square_height+2, 3, '1234', '');
}
if ($inv->sale_status == 'pending') {
    $pdf->setXY(90, 90);
    $pdf->SetFont('Arial','B',50);
    $pdf->SetTextColor(125,125,125);
    $pdf->RotatedImage(base_url().'assets/images/not_approved.png',20,220,240,18,45);
    $pdf->SetFont('Arial','',$fuente);
    $pdf->SetTextColor(0,0,0);
}

if ($for_email === true) {
    $pdf->Output(FCPATH."files/". $filename .".pdf", "F");
} else {
    if ($download) {
        $pdf->Output($filename.".pdf", "D");
    } else if ($internal_download == TRUE) {
        $pdf->Output("files/electronic_billing/". $filename .".pdf", "F");
    } else {
        $pdf->Output($filename.".pdf", "I");
    }
}