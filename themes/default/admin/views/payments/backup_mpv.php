<?php

/***********

FORMATO PARA VISUALIZACIÓN DE RECIBOS DE CAJA MÚLTIPLES

************/

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        //izquierda
        $this->RoundedRect(13, 7, 117, 26, 3, '1234', '');

        if (file_exists('assets/uploads/logos/'.$this->biller->logo)) {
            $imagen = getimagesize(base_url().'assets/uploads/logos/'.$this->biller->logo);
            $ancho = $imagen[0] / 3.77952;
            $alto = $imagen[1] / 3.77952;
            $altura = 14.5;
        }

        $this->Image((isset($this->biller->logo) ? base_url().'assets/uploads/logos/'.$this->biller->logo : ''),16,9,21.5);
        $cx = 45;
        $cy = 11;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(mb_strtoupper($this->Settings->nombre_comercial)),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address." - ".ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 26, 3, '1234', '');

        $cx = 132;

        //derecha
        $cy = 9;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Recibo de caja'),'',1,'C');
        $cy +=6;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode('N°. '.$this->encabezado->reference_no),'',1,'C');
        $cy +=10;
        $this->setXY($cx, $cy);
        $this->Cell(77, 3 , $this->sma->utf8Decode('Valor '.$this->sma->formatMoney($this->enc_total_amount)),'',1,'C');

        //izquierda

        $cx = 13;
        $cy = 35;

        $this->setXY($cx, $cy);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Cliente : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->encabezado->customer),'',1,'L');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Nit / CC :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->encabezado->vat_no),'',1,'L');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Fecha real de pago :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->encabezado->payment_date),'',1,'L');

        //derecha

        $cx = 132;
        $cy = 35;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(28.1, 4, $this->sma->utf8Decode('Fecha : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(48.9, 4, $this->sma->utf8Decode($this->encabezado->date),'',1,'L');
        $cx = 132;
        $cy += 4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(28.1, 4, $this->sma->utf8Decode('Forma de pago :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(48.9, 4, $this->sma->utf8Decode($this->encabezado->paid_by),'',1,'L');
        $cx = 132;
        $cy += 4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(28.1, 4, $this->sma->utf8Decode('N° comprobante :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(48.9, 4, $this->sma->utf8Decode($this->encabezado->consecutive_payment),'',1,'L');

        $this->ln(4);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(24.5, 5, $this->sma->utf8Decode('Afecta a'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('Valor'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('ReteFuente'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('ReteIVA'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('ReteICA'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode(lang('rete_other')),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('Descuentos'),'B',0,'C');
        $this->Cell(24.5, 5, $this->sma->utf8Decode('Valor pagado'),'B',1,'C');

    }

    function Footer()
    {
        // Print centered page number
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 805) {
            $text = substr($text, 0, 800);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

if ($encabezado->supplier_id) {
    $pdf->setTitle($this->sma->utf8Encode('Comprobante egreso'));
} else if ($encabezado->customer_id) {
    $pdf->setTitle($this->sma->utf8Encode('Recibo de caja'));
}
$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->customer = $customer;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->document_type = $document_type;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->Settings = $this->Settings;
$pdf->encabezado = $encabezado;
$pdf->sma = $this->sma;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}
$enc_total_amount = 0;
foreach ($pmnts as $pmnt) {
    if ($pmnt->type == 'discount') {
        continue;
    }
    $total_retentions = $pmnt->rete_fuente_total + $pmnt->rete_iva_total + $pmnt->rete_ica_total + $pmnt->rete_other_total;
    $enc_total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
}
$pdf->enc_total_amount = $enc_total_amount;
$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();

$maximo_footer = 235;

$pdf->SetFont('Arial','',$fuente+1);

$total_amount = 0;
$total_rete_fuente = 0;
$total_rete_iva = 0;
$total_rete_ica = 0;
$total_rete_other = 0;
$total_received = 0;
$total_discount = 0;
$sales_discount = [];
// $this->sma->print_arrays($pmnts);
foreach ($pmnts as $pmnt) {
    if ($pmnt->type == 'discount') {
        if (isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id])) {
            $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] += $pmnt->amount;
        } else {
            $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]= $pmnt->amount;
        }
    } else {
        continue;
    }
    
}

foreach ($pmnts as $pmnt) {
    if ($pmnt->type == 'discount') {
        continue;
    }
    $sale_discount = isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] : 0;
    $total_discount += $sale_discount;
    $total_rete_fuente += $pmnt->rete_fuente_total;
    $total_rete_iva += $pmnt->rete_iva_total;
    $total_rete_ica += $pmnt->rete_ica_total;
    $total_rete_other += $pmnt->rete_other_total;
    $total_received += ($pmnt->reference_no != NULL ? $pmnt->amount : 0 ) + $sale_discount;
    $total_retentions = $pmnt->rete_fuente_total + $pmnt->rete_iva_total + $pmnt->rete_ica_total + $pmnt->rete_other_total;
    $total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
    $text = $pmnt->reference_no;
    // if ($pmnt->reference_no && $pmnt->expense_id) {
    //     $text = $pmnt->note;
    // }
    $pdf->Cell(24.5, 5, $this->sma->utf8Decode($text),'B',0,'C');
    $pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->reference_no != NULL ? $pmnt->amount + $sale_discount : 0)),'B',0,'R');
    $pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->rete_fuente_total)),'B',0,'R');
    $pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->rete_iva_total)),'B',0,'R');
    $pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->rete_ica_total)),'B',0,'R');
    $pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->rete_other_total)),'B',0,'R');
    $pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($sale_discount)),'B',0,'R');
    $pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->amount - $total_retentions)),'B',1,'R');
}


$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(24.5, 5, $this->sma->utf8Decode('TOTAL '),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_received)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_rete_fuente)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_rete_iva)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_rete_ica)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_rete_other)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_discount)),'B',0,'R');
$pdf->Cell(24.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_amount)),'B',1,'R');

$pdf->ln(5);

$pdf->MultiCell(196, 5 , $this->sma->utf8Decode('Nota : '.$encabezado->note),'','L');

$pdf->ln(5);

$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Elaborado'),'T',0,'C');

$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Aprobado'),'T',0,'C');

$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Contabilizado'),'T',0,'C');

$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx, $cy);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Firma y sello recibido:'),'',0,'L');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Cc o Nit :'),'T',1,'L');

$pdf->ln(5);

$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
$pdf->setXY($cx+185, $cy);
$pdf->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$pdf->PageNo()),'',1,'C');


// $pdf->Cell(28.9,3, $this->sma->utf8Decode($sma->formatMoney($total)),'',1,'C');
$download = false;
if ($download || $for_email) {
    if ($download) {
        $pdf->Output("factura_venta.pdf", "D");
    } else if ($for_email) {
        $pdf->Output("assets/uploads/". $inv->reference_no .".pdf", "F");
    }
} else {
  $pdf->Output("factura_venta.pdf", "I");
}