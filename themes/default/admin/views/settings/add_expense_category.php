<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_expense_category'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/add_expense_category", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= lang('category_code', 'code'); ?>
                <?= form_input('code', '', 'class="form-control" id="code" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang('category_name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>

            <?php if (isset($ledgers) && $ledgers): ?>
            <!-- LEDGER DE GASTO -->
            <div class="form-group">
                <?= lang('ledger', 'ledger_id') ?>
                <?php
                    $lopts[''] = lang('select');

                    foreach ($ledgers as $ledger) {
                        $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                    }

                 ?>

                <?= form_dropdown('ledger_id', $lopts, '', 'class="form-control" id="ledger_id" style="width:100%;" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('creditor_ledger', 'creditor_ledger_id') ?>
                <?php
                    $lopts[''] = lang('select');

                    foreach ($ledgers as $ledger) {
                        $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                    }

                 ?>

                <?= form_dropdown('creditor_ledger_id', $lopts, '', 'class="form-control" id="creditor_ledger_id" style="width:100%;" required="required"'); ?>
            </div>
            <?php endif ?>

            <?php
            $tropts[''] = lang('select');
            $troptstxt = "<option value=''>".lang('select')."</option>";
            foreach ($tax_rates as $tax) {
                $tropts[$tax->id] = $tax->name;
                $troptstxt .="<option value='".$tax->id."' data-rate='".$tax->rate."'>".$tax->name."</option>";
            }
             ?>
            <!-- IMPUESTO 1 -->
            <div class="form-group">
                <?= lang('tax_1', 'tax_rate_id') ?>

                <select name="tax_rate_id" id="tax_rate_id" class="form-control select" style="width: 100%;" required>
                    <?= $troptstxt ?>
                </select>
            </div>

            <?php if (isset($ledgers) && $ledgers): ?>
            <!-- LEDGER DE IMPUESTO 1 -->
            <div class="form-group">
                <?= lang('tax_1_ledger', 'tax_ledger_id') ?>
                <?php
                    $lopts[''] = lang('select');

                    foreach ($ledgers as $ledger) {
                        $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                    }

                 ?>

                <?=  form_dropdown('tax_ledger_id', $lopts, '', 'class="form-control" id="tax_ledger_id" style="width:100%;"'); ?>
            </div>
            <?php endif ?>

            <!-- IMPUESTO 2 -->
            <div class="form-group">
                <?= lang('tax_2', 'tax_rate_2_id') ?>
                <select name="tax_rate_2_id" id="tax_rate_2_id" class="form-control select" style="width: 100%;">
                    <?= $troptstxt ?>
                </select>
            </div>

            <?php if (isset($ledgers) && $ledgers): ?>
            <!-- LEDGER DE IMPUESTO 2 -->
            <div class="form-group">
                <?= lang('tax_2_ledger', 'tax_2_ledger_id') ?>
                <?php
                    $lopts[''] = lang('select');

                    foreach ($ledgers as $ledger) {
                        $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                    }

                 ?>

                <?= form_dropdown('tax_2_ledger_id', $lopts, '', 'class="form-control" id="tax_2_ledger_id" style="width:100%;"'); ?>
            </div>
            <?php endif ?>

            <div class="form-group">
                <label>
                    <input type="checkbox" name="expense_import">
                    <?= lang('expense_import') ?>
                </label>
            </div>


        </div>
        <div class="modal-footer">
            <?= form_submit('add_expense_category', lang('add_expense_category'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>

<script type="text/javascript">
    $('#tax_rate_id').on('change', function(){
        tax1_id = $(this).val();
        tax1_rate = formatDecimal($('#tax_rate_id option:selected').data('rate'));
        if (tax1_rate != 0) {
            // $('#tax_ledger_id').prop('required', true);
            $('#tax_ledger_id').select2('readonly', false);
            $('#tax_ledger_id').select2('open');
        } else {
            // $('#tax_ledger_id').prop('required', false);
            $('#tax_ledger_id').select2('readonly', true).select2('val', '');
        }
    });

    $('#tax_rate_2_id').on('change', function(){
        tax2_id = $(this).val();
        tax2_rate = formatDecimal($('#tax_rate_2_id option:selected').data('rate'));
        if (tax2_rate != 0) {
            // $('#tax_2_ledger_id').prop('required', true);
            $('#tax_2_ledger_id').select2('readonly', false);
            $('#tax_2_ledger_id').select2('open');
        } else {
            // $('#tax_2_ledger_id').prop('required', false);
            $('#tax_2_ledger_id').select2('readonly', true).select2('val', '');
        }
    });
</script>

<?= $modal_js ?>