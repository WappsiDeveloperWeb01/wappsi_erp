<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_warehouse'); ?></h4>
        </div>
        <?php $attrib = array("id"=>"form_edit_warehouse");
        echo admin_form_open_multipart("system_settings/edit_warehouse/" . $id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label>
                    <input type="radio" name="type" value="1" class="wh_type" <?= $warehouse->type == 1 ? 'checked' : '' ?>>
                    <?= lang('warehouse_type_commercial') ?>
                </label>
                <label>
                    <input type="radio" name="type" value="2" class="wh_type" <?= $warehouse->type == 2 ? 'checked' : '' ?>>
                    <?= lang('warehouse_type_picking') ?>
                </label>
                <label>
                    <input type="radio" name="type" value="3" class="wh_type" <?= $warehouse->type == 3 ? 'checked' : '' ?>>
                    <?= lang('warehouse_type_packing') ?>
                </label>
            </div>

            <div class="form-group picking_type_section row" <?= $warehouse->type == 2 ? '' : 'style="display:none;"' ?> >
                <div class="col-sm-4">                    <?= lang('preffix', 'preffix') ?>
                    <input type="text" name="preffix" id="wh_preffix" class="form-control wh_forming_code" value="<?= $warehouse->preffix ?>">
                </div>  
                <div class="col-sm-4">
                    <?= lang('area', 'area') ?>
                    <input type="text" name="area" id="wh_area" class="form-control wh_forming_code" value="<?= $warehouse->area ?>">
                </div>  
                <div class="col-sm-4">
                    <?= lang('street', 'street') ?>
                    <input type="text" name="street" id="wh_street" class="form-control wh_forming_code" value="<?= $warehouse->street ?>">
                </div>  
            </div>
            <div class="form-group picking_type_section row" <?= $warehouse->type == 2 ? '' : 'style="display:none;"' ?> >
                <div class="col-sm-4">
                    <?= lang('module', 'module') ?>
                    <input type="text" name="module" id="wh_module" class="form-control wh_forming_code" value="<?= $warehouse->module ?>">
                </div>
                <div class="col-sm-4">
                    <?= lang('level', 'level') ?>
                    <input type="text" name="level" id="wh_level" class="form-control wh_forming_code" value="<?= $warehouse->level ?>">
                </div>
                <div class="col-sm-4">
                    <?= lang('suffix', 'suffix') ?>
                    <input type="text" name="suffix" id="wh_suffix" class="form-control wh_forming_code" value="<?= $warehouse->suffix ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="code"><?php echo $this->lang->line("code"); ?></label>
                <?php echo form_input('code', $warehouse->code, 'class="form-control" id="code" required="required" '.($warehouse->type == 2 ? 'readonly' : '')); ?>
            </div>
            <div class="form-group">
                <label class="control-label" for="name"><?php echo $this->lang->line("name"); ?></label>
                <?php echo form_input('name', $warehouse->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <label class="control-label" for="price_group"><?php echo $this->lang->line("price_group"); ?></label>
                <?php
                $pgs[''] = lang('select').' '.lang('price_group');
                foreach ($price_groups as $price_group) {
                    $pgs[$price_group->id] = $price_group->name;
                }
                echo form_dropdown('price_group', $pgs, $warehouse->price_group_id, 'class="form-control tip select" id="price_group" style="width:100%;"');
                ?>
            </div>
            <div class="form-group">
                <label class="control-label" for="phone"><?php echo $this->lang->line("phone"); ?></label>
                <?php echo form_input('phone', $warehouse->phone, 'class="form-control" id="phone"'); ?>
            </div>
            <div class="form-group">
                <label class="control-label" for="email"><?php echo $this->lang->line("email"); ?></label>
                <?php echo form_input('email', $warehouse->email, 'class="form-control" id="email"'); ?>
            </div>
            <div class="form-group">
                <label class="control-label" for="address"><?php echo $this->lang->line("address"); ?></label>
                <?php echo form_textarea('address', $warehouse->address, 'class="form-control" id="address" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("warehouse_map", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>
            <div class="form-group">
                <?= lang('status', 'status'); ?>
                <?php
                $opts = array('1' => lang('active'), '0' => lang('inactive'));
                ?>
                <?= form_dropdown('status', $opts, $warehouse->status, 'class="form-control" id="status" required="required" style="width:100%;"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="form-control btn btn-primary" id="edit_warehouse"><?= lang('edit_warehouse') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#form_edit_warehouse").validate({
              ignore: []
        });
    });
    $(document).on('ifChecked', '.wh_type', function(){
        if ($('.wh_type:checked').val() == 2) {
            $('.picking_type_section').fadeIn();
            $('#code').prop('readonly', true);
            $('#wh_street').trigger('keyup');
        } else {
            $('.picking_type_section').fadeOut();
            $('#code').prop('readonly', false).val('');
        }
    });
    $(document).on('keyup', '.wh_forming_code', function(){
        var wh_preffix = $('#wh_preffix').val();
        var wh_area = $('#wh_area').val();
        var wh_street = $('#wh_street').val();
        var wh_module = $('#wh_module').val();
        var wh_level = $('#wh_level').val();
        var wh_suffix = $('#wh_suffix').val();
        var wh_code = wh_preffix+wh_area+wh_street+wh_module+wh_level+wh_suffix; 
        var wh_name = (wh_preffix != "" ? "Prefijo : "+wh_preffix : "")+(wh_area != "" ? ", Área : "+wh_area : "")+(wh_street != "" ? ", Calle: "+wh_street : "")+(wh_module != "" ? ", Módulo: "+wh_module : "")+(wh_level != "" ? ", Nivel: "+wh_level : "")+(wh_suffix != "" ? ", Sufijo: "+wh_suffix : ""); 
        $('#code').val(wh_code);
        $('#name').val(wh_name);
    });
    $(document).on('click', '#edit_warehouse', function(){
        if ($('#form_edit_warehouse').valid()) {
            $('#form_edit_warehouse').submit();
        }
    });
</script>
<?= $modal_js ?>