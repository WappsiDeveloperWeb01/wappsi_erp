$(document).ready(function(){
	if (cuproduction_order = localStorage.getItem('cuproduction_order')) {
		$('#cuproduction_order').val(cuproduction_order).select2({
			minimumInputLength: 1,
			data: [],
			initSelection: function (element, callback) {
			    $.ajax({
			        type: "get", async: false,
			        url: site.base_url+"production_order/get_production_order/" + $(element).val(),
			        dataType: "json",
			        success: function (data) {
			            callback(data[0]);
			        }
			    });
			},
			ajax: {
			    url: site.base_url + "production_order/production_order_suggestions",
			    dataType: 'json',
			    quietMillis: 15,
			    data: function (term, page) {
			        return {
			            term: term,
			            limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
			        };
			    },
			    results: function (data, page) {
			        if (data.results != null) {
			            return {results: data.results};
			        } else {
			            return {results: [{id: '', text: lang.no_match_found}]};
			        }
			    }
			}
    	});
        $('#cuproduction_order').trigger('change');
	} else {
		nsProductionOrder();
	}

	if (cuemployee = localStorage.getItem('cuemployee')) {
        $('#cuemployee').val(cuemployee).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/employee_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        ptype: 4,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#cuemployee').trigger('change');
    } else {
        nsEmployee();
    }

    if (cuemployee2 = localStorage.getItem('cuemployee2')) {
        $('#cuemployee2').val(cuemployee2).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/employee_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        ptype: 4,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#cuemployee2').trigger('change');
    } else {
        nsEmployee2();
    }

    if (cutender = localStorage.getItem('cutender')) {
        $('#cutender').val(cutender).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/employee_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        ptype: 4,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#cutender').trigger('change');
    } else {
        nsTender();
    }

    if (cutender2 = localStorage.getItem('cutender2')) {
        $('#cutender2').val(cutender2).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/employee_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        ptype: 4,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#cutender2').trigger('change');
    } else {
        nsTender2();
    }

    if (cupacker = localStorage.getItem('cupacker')) {
        $('#cupacker').val(cupacker).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/employee_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        ptype: 4,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#cupacker').trigger('change');
    } else {
        nsPacker();
    }

    if (cupacker2 = localStorage.getItem('cupacker2')) {
        $('#cupacker2').val(cupacker2).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/employee_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        ptype: 4,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#cupacker2').trigger('change');
    } else {
        nsPacker2();
    }

	if (cudate = localStorage.getItem('cudate')) {
		$('#cudate').val(cudate);
	}
	if (cuest_date = localStorage.getItem('cuest_date')) {
		$('#cuest_date').val(cuest_date);
	}
	if (cureference_no = localStorage.getItem('cureference_no')) {
		$('#cureference_no').val(cureference_no);
	}

    nsEmbroiderer();
    nsStamper();
	
});


var pfinished_available_qty;
var ciq_options_cutted_qty; //valores base, si el componente está configurado cómo de a 2 por producto terminado, y se cortaron 6, el valor guardado sería 3, (6 / 2 = 3)

$(document).on('change', '#cuproduction_order', function(){
    $.ajax({
        url : site.base_url+'production_order/get_production_order_items/'+$(this).val(),
        dataType : 'JSON'
    }).done(function(data){
       setTimeout(function() {
            $("#cudate").datetimepicker('remove').datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                enableOnReadonly : false,
                startDate : data.por.date,
                endDate : data.por.est_date
            });
            // $("#cudate").val(data.por.date);
            $("#cuest_date").datetimepicker('remove').datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                enableOnReadonly : false,
                startDate : data.por.date,
                endDate : data.por.est_date
            });
            $("#cuest_date").val(data.por.est_date);
       }, 850);

       var new_cuitems = JSON.parse('{}');
       var pos = 0;
       var poptions_qty = [];
        $.each(data.rows, function(dr_id, drow){
            if (!drow.production_order_pfinished_item_id) {
                // drow.quantity -= drow.pfinished_cutting_quantity;
            } else {
                return;
            }
            var original_qty = drow.quantity;
            var option_qty = drow.quantity / Object.keys(drow.options).length;
            var option_qty_floor = Math.floor(option_qty);
            var option_qty_remaining = option_qty - option_qty_floor;
            var last_option_plus_qty = 0;
            if (option_qty_remaining > 0) {
                last_option_plus_qty = drow.quantity - (option_qty_floor * Object.keys(drow.options).length)
            }
            var item_repeat_pos = 1;
            if (drow.options) {
                $.each(drow.options, function(dr_opt_id, dr_opt){
                    drow.option_id = dr_opt.id;
                    drow.quantity = option_qty_floor;
                    drow.variant = drow.options[dr_opt_id].name;
                    drow.original_qty = original_qty;
                    drow.finished_quantity = dr_opt.finished_quantity;
                    drow.cutting_quantity = dr_opt.cutting_quantity;
                    drow.fault_quantity = dr_opt.fault_quantity;
                    drow.in_process_quantity = dr_opt.cutting_quantity - dr_opt.fault_quantity;
                    if (item_repeat_pos == Object.keys(drow.options).length) {
                        drow.quantity = drow.quantity + last_option_plus_qty;
                    }
                    new_cuitems[pos] = JSON.stringify(drow);
                    pos++;
                    item_repeat_pos++;
                    if (poptions_qty[drow.product_id] == undefined) {
                        poptions_qty[drow.product_id] = [];
                    }
                    poptions_qty[drow.product_id][dr_opt.id] = drow.quantity;
                });
            }
                
        });
        pfinished_available_qty = [];
        ciq_options_cutted_qty = [];
        var other_order_cutting_exists = false;
        $.each(data.rows, function(dr_id, drow){
            if (drow.options) {
                $.each(drow.options, function(dr_opt_id, dr_opt){
                    if (!drow.production_order_pfinished_item_id) {
                        return;
                    }
                    if (dr_opt.cutting_quantity > 0) {
                        other_order_cutting_exists = true;
                    }
                    if (pfinished_available_qty[drow.pfinished_id] === undefined) {
                        pfinished_available_qty[drow.pfinished_id] = [];
                    }
                    if (pfinished_available_qty[drow.pfinished_id][drow.product_id] === undefined) {
                        pfinished_available_qty[drow.pfinished_id][drow.product_id] = formatDecimal(drow.quantity) - formatDecimal(dr_opt.cutting_quantity);
                    } else {
                        pfinished_available_qty[drow.pfinished_id][drow.product_id] -= formatDecimal(dr_opt.cutting_quantity);
                    }
                    drow_ciq_quantity = formatDecimal(dr_opt.cutting_quantity) / formatDecimal(drow.combo_item_qty);
                    if (ciq_options_cutted_qty[drow.pfinished_id] == undefined) {
                        ciq_options_cutted_qty[drow.pfinished_id] = [];
                    }
                    if (
                        ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id] == undefined || 
                        drow_ciq_quantity > ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id]
                        ) {
                            ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id] = drow_ciq_quantity;
                    }
                });
            }
        });
        $.each(data.rows, function(dr_id, drow){
            if (!drow.production_order_pfinished_item_id) {
                return;
            }
            var original_qty = 0;
            if (drow.options) {
                $.each(drow.options, function(dr_opt_id, dr_opt){
                    drow.option_id = dr_opt.id;
                    option_combo_qty = parseFloat(drow.combo_item_qty) * parseFloat(poptions_qty[drow.pfinished_id][dr_opt.id]);
                    if (original_qty == 0) {
                        original_qty = drow.quantity;
                    }
                    if (other_order_cutting_exists || parseFloat(pfinished_available_qty[drow.pfinished_id][drow.product_id]) != parseFloat(original_qty)) {
                        $('.div_cutable').fadeOut();
                        $('.div_porc').fadeIn();
                        if (pfinished_available_qty[drow.pfinished_id][drow.product_id] > 0) {
                            if (ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id] > 0 && pfinished_available_qty[drow.pfinished_id][drow.product_id] > (ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id] * formatDecimal(drow.combo_item_qty))) {

                                ciq_quantity_available = (formatDecimal(ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id]) * formatDecimal(drow.combo_item_qty));

                            } else if ((ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id] > 0 && pfinished_available_qty[drow.pfinished_id][drow.product_id] <= (ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id] * formatDecimal(drow.combo_item_qty))) || ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id] == 0) {

                                ciq_quantity_available = pfinished_available_qty[drow.pfinished_id][drow.product_id];
                                if (ciq_options_cutted_qty[drow.pfinished_id][dr_opt.id] > 0) {
                                    // pfinished_available_qty[drow.pfinished_id][drow.product_id] = 0;
                                }

                            }
                            drow.quantity = formatDecimal(ciq_quantity_available);
                        } else {
                            drow.quantity = formatDecimal(dr_opt.cutting_quantity);
                        }
                        // drow.manual_qty = drow.quantity - dr_opt.cutting_quantity;
                        drow.manual_qty = 0;
                    } else {
                        if (original_qty == 0) {
                            original_qty = option_combo_qty;
                        }
                        drow.quantity = option_combo_qty;
                    }
                    // pfinished_available_qty[drow.pfinished_id][drow.product_id];
                    // drow.quantity = option_combo_qty;
                    drow.max_qty = 9999999;
                    drow.original_qty = original_qty;
                    drow.variant = drow.options[dr_opt_id].name;
                    drow.finished_quantity = dr_opt.finished_quantity;
                    drow.cutting_quantity = dr_opt.cutting_quantity;
                    drow.fault_quantity = dr_opt.fault_quantity;
                    drow.in_process_quantity = dr_opt.cutting_quantity - dr_opt.fault_quantity;
                    new_cuitems[pos] = JSON.stringify(drow);
                    pos++;
                });
            }
        });
        set_cuitems = JSON.parse('{}');
        $.each(new_cuitems, function(id, cuitem){
            set_cuitems[id] = JSON.parse(cuitem);
        });
        console.log(new_cuitems);
        localStorage.setItem('cuitems', JSON.stringify(set_cuitems));
        loadItems();
    });
    localStorage.setItem('cuproduction_order', $(this).val());
    $('#cuproduction_order_reference_no').val($('#cuproduction_order').select2('data').text);
});
$(document).on('change', '#cuemployee', function(){
    localStorage.setItem('cuemployee', $(this).val());
});
$(document).on('change', '#cuemployee2', function(){
    localStorage.setItem('cuemployee2', $(this).val());
});
$(document).on('change', '#cutender', function(){
    localStorage.setItem('cutender', $(this).val());
});
$(document).on('change', '#cutender2', function(){
    localStorage.setItem('cutender2', $(this).val());
});
$(document).on('change', '#cupacker', function(){
    localStorage.setItem('cupacker', $(this).val());
});
$(document).on('change', '#cupacker2', function(){
    localStorage.setItem('cupacker2', $(this).val());
});
$(document).on('change', '#cuest_date', function(){
	localStorage.setItem('cuest_date', $(this).val());
});
$(document).on('change', '#cudate', function(){
	localStorage.setItem('cudate', $(this).val());
});
$(document).on('change', '#cureference_no', function(){
	localStorage.setItem('cureference_no', $(this).val());
});

function nsProductionOrder(){
	$('#cuproduction_order').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "production_order/production_order_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function nsEmployee(){
    $('#cuemployee').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/employee_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 4,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
function nsEmployee2(){
    $('#cuemployee2').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/employee_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 4,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
function nsTender(){
    $('#cutender').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/employee_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 4,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
function nsTender2(){
    $('#cutender2').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/employee_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 4,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function nsPacker(){
    $('#cupacker').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/employee_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 4,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
function nsPacker2(){
    $('#cupacker2').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/employee_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 4,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function nsEmbroiderer(){
    $('#cuembroiderer').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 1,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function nsStamper(){
    $('#custamper').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 1,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
//aca laod
function loadItems(tabindex = null) {
    var decimals = 3;
    if (site.settings.rounding == 1) {
        decimals = 1;
    }

    if (localStorage.getItem('cuitems')) {
        count = 1;
        an = 1;
        Ttotal = 0;
        $("#cuTable tbody").empty();
        cuitems = JSON.parse(localStorage.getItem('cuitems'));
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(cuitems, function(o){return [parseInt(o.order)];}) : cuitems;
        if (comproducts !== undefined) { delete comproducts; }
        var comproducts = [];
        $.each(sortedItems, function (index) {
            var item = this;
            if (item.production_order_pfinished_item_id != null) {
            	return;
            }
            var item_id = item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.product_id, item_qty = item.quantity, item_code = item.product_code, item_name = item.product_name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '" data-option_id="' + item.option_id + '" data-index="' + index + '" data-original_qty="' + item.original_qty + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '"><input name="product_name[]" type="hidden" value="' + item_name + '"><span class="sname" id="name_' + row_no + '">' + item_code +' - ' + item_name +'</span>'+
                        '</td>';
            tr_html += '<td>'+lang[item.type]+'</td>';
            tr_html += '<td>'+(item.variant != null ? item.variant : '')+'</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" name="quantity[]" type="text" value="' + formatDecimal(item_qty, decimals) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                        '</td>';
            tr_html += '<td class="text-center"><i class="fa fa-times tip cudel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.appendTo("#cuTable");
            count += parseFloat(item_qty);
            an++;
            // $.each(item.composition_products, function(index, item){
            //     item.qty = item.qty * item_qty;
            //     item.production_order_pfinished_item_id = product_id;
            // });
        });
        var col = 4;
        var tfoot = '<tr id="tfoot" class="tfoot active">'+(site.settings.set_adjustment_cost ? '<th></th>' : '')+
                        '<th colspan="2">Total</th>'+
                        '<th class="text-center">' + formatQuantity2(parseFloat(count) - 1) + '</th>'+
                        '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                    '</tr>';
        $('#cuTable tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        set_page_focus();
        localStorage.setItem('cucomproducts', JSON.stringify(comproducts));
        loadCompositionItems(tabindex);
    }
}

function loadCompositionItems(focus_tabindex = null){

	comproducts = JSON.parse(localStorage.getItem('cuitems'));

    var decimals = 3;
    if (site.settings.rounding == 1) {
        decimals = 1;
    }
    $("#cuCTable tbody").empty();
    ttorder = 0;
    ttfinished = 0;
    ttprocess = 0;
    ttcut = 0;
    tabindex = 1;
    $.each(comproducts, function (index, item) {
        if (item.production_order_pfinished_item_id == null) {
            return;
        }
        if (item === undefined) { return; }
        var trclass = 'class="cproduct_'+item.id+'"';
        var newTr = $('<tr data-item-id="' + index + '" data-pfinished_item_id="' + item.production_order_pfinished_item_id + '" data-pfinished_option_id="' + item.option_id + '" data-combo_item_qty="' + item.combo_item_qty + '"></tr>');
        tr_html = '<td '+trclass+'>'+
                        '<input name="cp_product_id[]" type="hidden" class="rid" value="' + item.product_id + '"><input name="cp_product_name[]" type="hidden" value="' + item.product_name + '"><span class="sname">' + item.product_code +' - ' + item.product_name +'</span>'+
                        '<input name="cp_production_order_pfinished_item_id[]" type="hidden" class="rid" value="' + item.production_order_pfinished_item_id + '">'+
                        '<input name="cp_production_order_pid[]" type="hidden" class="rid" value="' + item.id + '">'+
                        '<input name="cp_production_order_pfinished_option_id[]" type="hidden" class="rid" value="' + item.option_id + '">'+
                    '</td>';
        tr_html += '<td>'+
                            '<span>' + item.pfinished_name +' - ' + item.pfinished_code  + (item.variant != null ? " - "+item.variant : "") + '</span>'+
                        '</td>';
        tr_html += '<td '+trclass+'>'+lang[item.type]+'</td>';
        // tr_html += '<td '+trclass+'>'+
        //                 '<input class="form-control text-center order_quantity" name="order_quantity[]" type="text" value="' + formatDecimal(item.quantity, decimals) + '" readonly>'+
        //             '</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center finished_quantity" name="finished_quantity[]" type="text" value="' + formatDecimal((item.finished_quantity ? item.finished_quantity : 0), decimals) + '" data-pfinishedid="'+item.pfinished_id+'" data-productid="'+item.product_id+'" data-optionid="'+item.option_id+'" readonly>'+
                    '</td>';
        // tr_html += '<td '+trclass+'>'+
        //                 '<input class="form-control text-center in_process_quantity" name="in_process_quantity[]" type="text" value="' + formatDecimal((item.in_process_quantity ? item.in_process_quantity : 0) - (item.finished_quantity ? item.finished_quantity : 0), decimals) + '" readonly>'+
        //             '</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center to_cut_quantity" name="to_cut_quantity[]" type="text" value="' + ((formatDecimal(item.manual_qty !== undefined ? item.manual_qty : (item.quantity - (item.in_process_quantity ? item.in_process_quantity : 0)), decimals))) + '" data-original_qty="'+item.original_qty+'" id="cqty_'+item.id+'" tabindex="'+tabindex+'"  data-pfinishedid="'+item.pfinished_id+'" data-productid="'+item.product_id+'" data-optionid="'+item.option_id+'">'+
                        '<label class="error" for="cqty_'+item.id+'" style="display:none;"></label>'+
                    '</td>';
                    // max="'+(item.max_qty > 0 ? item.max_qty : 0)+'"
        newTr.html(tr_html);
        newTr.appendTo("#cuCTable");
        count += parseFloat(item.qty);
        ttorder += parseFloat(item.quantity);
        ttfinished += parseFloat(item.finished_quantity ? item.finished_quantity : 0);
        ttprocess += parseFloat((item.in_process_quantity ? item.in_process_quantity : 0) - (item.finished_quantity ? item.finished_quantity : 0));
        ttcut += parseFloat(item.manual_qty !== undefined ? item.manual_qty : (item.quantity - (item.in_process_quantity ? item.in_process_quantity : 0)));
        tabindex++;
    });

    var tfoot = '<tr id="tfoot" class="tfoot active">'+
                    '<th colspan="3">Total</th>'+
                    // '<th class="text-center">' + formatQuantity2(parseFloat(ttorder)) + '</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(ttfinished)) + '</th>'+
                    // '<th class="text-center">' + formatQuantity2(parseFloat(ttprocess)) + '</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(ttcut)) + '</th>'+
                '</tr>';
    $('#cuCTable tfoot').html(tfoot);

    if (focus_tabindex) {
        $('[tabindex='+(focus_tabindex+1)+']').focus().select();
    } else {

        $('[tabindex='+1+']').focus().select();
    }

}

$('#reset').click(function (e) {
    bootbox.confirm(lang.r_u_sure, function (result) {
        if (result) {
            if (localStorage.getItem('cuitems')) {
                localStorage.removeItem('cuitems');
            }
            if (localStorage.getItem('cucomproducts')) {
                localStorage.removeItem('cucomproducts');
            }
            if (localStorage.getItem('cudate')) {
                localStorage.removeItem('cudate');
            }
            if (localStorage.getItem('cuproduction_order')) {
                localStorage.removeItem('cuproduction_order');
            }
            if (localStorage.getItem('cureference_no')) {
                localStorage.removeItem('cureference_no');
            }
            if (localStorage.getItem('cuemployee')) {
                localStorage.removeItem('cuemployee');
            }
            if (localStorage.getItem('cuemployee2')) {
                localStorage.removeItem('cuemployee2');
            }
            if (localStorage.getItem('cutender')) {
                localStorage.removeItem('cutender');
            }
            if (localStorage.getItem('cutender2')) {
                localStorage.removeItem('cutender2');
            }
            if (localStorage.getItem('cupacker')) {
                localStorage.removeItem('cupacker');
            }
            if (localStorage.getItem('cupacker2')) {
                localStorage.removeItem('cupacker2');
            }
            $('#modal-loading').show();
            location.reload();
        }
    });
});

$(document).on("change", '.to_cut_quantity', function (e) {
    if (e.keyCode != 13) {
        setTimeout(function() {
            $('.rquantity').prop('readonly', true);
        }, 1000);
        cuitems = JSON.parse(localStorage.getItem('cuitems'));
        var row = $(this).closest('tr');
        item_id = row.attr('data-item-id');
        var max = parseFloat($(this).prop('max'));
        var new_qty = parseFloat($(this).val());
        var max_manual_qty = 0;
        var actual_manual_qty = 0;
        var old_item_qty = cuitems[item_id].manual_qty;
        pfinishedid = cuitems[item_id].pfinished_id;
        productid = cuitems[item_id].product_id;
        optionid = cuitems[item_id].option_id;
        // pfinished_available_qty[pfinishedid][productid] //CANTIDAD DISPONIBLE PARA CORTE DEL COMPONENTE
        // ciq_options_cutted_qty[pfinishedid][optionid] //CANTIDAD BASE MÍNIMA CORTADA PARA EL PRODUCTO TERMINADO EN LAS VARIANTES
        option_base_qty_cutted = (formatDecimal(ciq_options_cutted_qty[pfinishedid][optionid]) * formatDecimal(cuitems[item_id].combo_item_qty));
        component_max_qty = formatDecimal(pfinished_available_qty[pfinishedid][productid]);
        console.log(">> 1 "+component_max_qty);
        cu_items_options_pending_to_cut = 0;
        // $.each(cuitems, function(cuitem_id, cuitem){
        //     if (
        //             cuitem.production_order_pfinished_item_id != null && 
        //             cuitem.product_id == productid && 
        //             cuitem.option_id != optionid 
        //         ) {
        //         cuitem_option_base_qty_cutted = formatDecimal(ciq_options_cutted_qty[cuitem.pfinished_id][cuitem.option_id]) * formatDecimal(cuitem.combo_item_qty);
        //         if (cuitem.cutting_quantity < cuitem_option_base_qty_cutted) {
        //             // component_max_qty -= cuitem_option_base_qty_cutted;
        //         }
        //     }
        // });
        inputted_qty = 0;
        $('input[name="to_cut_quantity[]"][data-pfinishedid="'+pfinishedid+'"][data-productid="'+productid+'"]').each(function(tcut_input_id, tcut_input){
            if ($(tcut_input).data('optionid') != optionid) {
                component_max_qty -= formatDecimal($(tcut_input).val());
            }
        });
        if (cuitems[item_id].max_qty != 9999999) {
            // component_max_qty = cuitems[item_id].max_qty;
        }
        console.log(">> 2 "+component_max_qty);
        if ((component_max_qty > 0 && (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0|| parseFloat($(this).val()) > component_max_qty)) || component_max_qty <= 0) {
            if (inputted_qty == component_max_qty || component_max_qty < 0) {
                $(this).val(0);
            } else {
                $(this).val(component_max_qty);
            }
            command: toastr.error('El valor digitado está fuera del margen permitido A.', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
            return;
        }
        $.each(cuitems, function(item_index, item){
            if (
                item_index != item_id &&
                cuitems[item_index].production_order_pfinished_item_id != null && 
                cuitems[item_index].production_order_pfinished_item_id == cuitems[item_id].production_order_pfinished_item_id && 
                cuitems[item_index].product_id == cuitems[item_id].product_id
            ) {
                actual_manual_qty += formatDecimal(cuitems[item_index].manual_qty !== undefined ? cuitems[item_index].manual_qty : 0) + formatDecimal(cuitems[item_index].finished_quantity !== undefined ? cuitems[item_index].finished_quantity : 0);
            }
        });
        max_manual_qty = formatDecimal(cuitems[item_id].original_qty) - actual_manual_qty - formatDecimal(cuitems[item_id].finished_quantity);
        if (new_qty > max_manual_qty) {
            $(this).val(0);
            command: toastr.error('El valor digitado está fuera del margen permitido B.', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
            return;
        }
        $.each(cuitems, function(item_index, item){
            if (cuitems[item_index] != item_id && cuitems[item_index].manual_qty === undefined) {
                cuitems[item_index].manual_qty = 0;
            }
            if (
                item_index != item_id && 
                cuitems[item_index].production_order_pfinished_item_id == cuitems[item_id].production_order_pfinished_item_id && 
                cuitems[item_index].option_id == cuitems[item_id].option_id
                ) {
                var old_qty_cq = cuitems[item_id].combo_item_qty;
                var new_qty_cq = cuitems[item_index].combo_item_qty;
                var old_qty_cq_val = new_qty / cuitems[item_id].combo_item_qty;
                var new_qty_cq_val = old_qty_cq_val * new_qty_cq;
                cuitems[item_index].max_qty = new_qty_cq_val > 0 ? new_qty_cq_val+(old_item_qty !== undefined && old_item_qty > new_qty ? formatDecimal((max_manual_qty-new_qty_cq_val)) : 0 ) : 9999999;
                cuitems[item_index].max_qty = new_qty_cq_val;
            }
        });
        cuitems[item_id].manual_qty = new_qty;
        localStorage.setItem('cuitems', JSON.stringify(cuitems));
        loadItems($(this).prop('tabindex'));
    }
}).on('keydown', '.to_cut_quantity', function(e){
    if (e.keyCode == 13) {
        setTimeout(function() {
            $(this).trigger('change');
        }, 800);
    }
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#previewImage').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#document").change(function(){
    input = this;
    var file, img;
    var _URL = window.URL || window.webkitURL;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL.createObjectURL(file);
        img.onload = function () {
            _URL.revokeObjectURL(objectUrl);
            if (this.height > site.settings.iheight) {
                command: toastr.warning('La imagen no puede tener más de '+site.settings.iheight+'px de altura.', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                return false;
            } else {
                readURL(input);
            }
        };
        img.src = objectUrl;
    }
});

$(document).on('click', '.fileinput-remove', function(){

      $('#previewImage').attr('src', '');
});

$(document).on("change", '.rquantity', function () {
    var row = $(this).closest('tr');
    var item_id = row.attr('data-item-id'); //main
    var option_id = row.attr('data-option_id'); //main
    var index = row.attr('data-index');
    var original_qty = parseFloat(row.attr('data-original_qty'));
    var total_item_qty = 0;
    $('input.rquantity[data-item="'+item_id+'"]').each(function(index, input){
        total_item_qty = parseFloat(total_item_qty) + parseFloat($(input).val());
    });
    var new_rquantity_val = $(this).val();
    cuitems = JSON.parse(localStorage.getItem('cuitems'));
    if (total_item_qty > original_qty) {
        new_rquantity_val = original_qty - (total_item_qty - new_rquantity_val);
        cuitems[index].quantity = new_rquantity_val;
        command: toastr.error('El valor digitado está fuera del margen permitido.', '¡Atención!', {
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "4000",
            "extendedTimeOut": "1000",
        });
    }
    cuitems[index].quantity = new_rquantity_val;
    $.each(cuitems, function(cuindex, cuitem){
        if (cuitem.production_order_pfinished_item_id == item_id && cuitem.option_id == option_id) {
            cuitems[cuindex].quantity = (parseFloat(new_rquantity_val) * parseFloat(cuitems[cuindex].combo_item_qty)) + parseFloat(cuitem.cutting_quantity > 0 ? cuitem.cutting_quantity : 0);
        }
    });
    localStorage.setItem('cuitems', JSON.stringify(cuitems));
    loadItems();
});

$(document).on('click', '.cudel', function(){
    var row = $(this).closest('tr');
    var item_id = row.attr('data-item-id'); //main
    var option_id = row.attr('data-option_id'); //main
    var index = row.attr('data-index');
    $.each(cuitems, function(cuindex, cuitem){
        if (cuitem.production_order_pfinished_item_id == item_id && cuitem.option_id == option_id) {
            delete cuitems[cuindex];
        }
    });
    delete cuitems[index];
    localStorage.setItem('cuitems', JSON.stringify(cuitems));
    loadItems();
});