<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .ob {
        list-style: none;
        padding: 0;
        margin: 0;
        margin-top: 10px;
    }

    .ob li {
        width: 49%;
        margin: 0 10px 10px 0;
        float: left;
    }

    @media only screen and (max-width: 799px) {
        .ob li {
            width: 100%;
        }
    }

    .ob li .btn {
        width: 100%;
    }

    .ob li:nth-child(2n+2) {
        margin-right: 0;
    }
    .btn_filters {
        min-width: 32%;
        max-width: 32%;
        background-color: #ffffff;
        border-width: 2px;
        border-radius: 8px;
        padding: 4px;
        border-color: #1484c6;
        border-style: solid;
    }
    .btn_filters:first-child {
        margin-left: 1%;
    }
    .btn_filters:hover, .btn_filters_active {
        background-color: #1484c6 !important;
        color:#ffffff !important;
    }
</style>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header modal-primary">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <span class="modal-title" id="susModalLabel" style="font-size: 150%;"><?=lang('suspended_sales');?></span>
        </div>
        <div class="modal-body" style="padding-bottom:0;">
            <div class="form-group" id="ui3">
                <div class="input-group">
                    <?php
                        if(!isset($bill_reference)){
                            $bill_reference = "";
                        }
                    ?>
                    <?php echo form_input('search_reference', $bill_reference, 'class="form-control pos-tip btn-pos-product" id="search_reference" data-placement="top" data-trigger="focus" placeholder="'.lang('key_word').'" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
                    <div class="input-group-addon" style="padding: 2px 8px;border:none !important;">
                        <a id="billSearch" title="<span>Ventas Suspendidas</span>" data-placement="bottom" data-html="true" href="http://localhost/kiloencajes/admin/pos/opened_bills/18" data-toggle="ajax" tabindex="-1"> <i class="fa fa-search" id="xxd" style="font-size: 1.5em;"></i> </a>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
            <div>
                <?php 
                $sort_order = isset($sort_order) ? $sort_order : $this->pos_settings->suspended_sales_default_sorting;
                 ?>
                <button href="http://localhost/kiloencajes/admin/pos/opened_bills/18" data-toggle="ajax" data-sort="1" type="button" class="btn_filters <?= $sort_order == 1 ? 'btn_filters_active' : '' ?>" data-toggle="tooltip" data-placement="top" title="<?= lang('date_ascent_sorting_today') ?>">Hoy <i class="fa-solid fa-arrow-down-short-wide"></i></button>
                <button href="http://localhost/kiloencajes/admin/pos/opened_bills/18" data-toggle="ajax" data-sort="2" type="button" class="btn_filters <?= $sort_order == 2 ? 'btn_filters_active' : '' ?>" data-toggle="tooltip" data-placement="top" title="<?= lang('date_descent_sorting_today') ?>">Hoy <i class="fa-solid fa-arrow-down-wide-short"></i></button>
                <button href="http://localhost/kiloencajes/admin/pos/opened_bills/18" data-toggle="ajax" data-sort="3" type="button" class="btn_filters <?= $sort_order == 3 ? 'btn_filters_active' : '' ?>" data-toggle="tooltip" data-placement="top" title="<?= lang('date_descent_sorting_all') ?>">Todos <i class="fa-solid fa-arrow-down-wide-short"></i></button>
            </div>
            <div class="html_con"><?= $html ?></div>
            <div class="clearfix"></div>
        </div>
        <?php if ($page) { ?>
            <div class="modal-footer" style="padding:0;">
                <div class="text-center">
                    <div class="page_con"><?= $page ?></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('.pagination a').attr('data-toggle', 'ajax');

        $('.sus_sale').on('click', function (e) {
            var sid = $(this).attr("id");
            <?php if ($electronic_billing_environment && $this->Settings->suspended_sales_to_retail == 1) { ?>
                var dialog = bootbox.dialog({ 
                        title: '<?= lang('suspended_bill_title') ?>',
                        message: '<?= lang('suspended_bill_text') ?>',
                        size: 'x1',
                        onEscape: false,
                        backdrop: true,
                        buttons: {
                            pos: {
                                label: '<?= lang('suspended_bill_to_pos') ?>',
                                className: 'btn-success btn-full',
                                callback: function(){
                                    window.location.href = (type_pos == 1 ? "<?= admin_url('pos/index') ?>/" : "<?= admin_url('pos/add_wholesale') ?>/") + sid;
                                }
                            },
                            detal_fe: {
                                label: '<?= lang('suspended_bill_to_detal_fe') ?>',
                                className: 'btn-success btn-full',
                                callback: function(){
                                    window.location.href = "<?= admin_url('sales/add?suspend_to_fe=1&suspended_sale=') ?>" + sid;
                                }
                            },
                            detal: {
                                label: '<?= lang('suspended_bill_to_detal') ?>',
                                className: 'btn-success btn-full',
                                callback: function(){
                                    window.location.href = "<?= admin_url('sales/add?suspend_to_fe=0&suspended_sale=') ?>" + sid;
                                }
                            },
                            cancel: {
                                label: '<?= lang('cancel') ?>',
                                className: 'btn-danger btn-full',
                                callback: function(){
                                    $('#myModal').modal('hide');
                                }
                            }
                        }
                    });
                setTimeout(function() {
                    $('.bootbox-close-button').css('display', 'none');
                }, 200);
            <?php } else { ?>
                    window.location.href = (type_pos == 1 ? "<?= admin_url('pos/index') ?>/" : "<?= admin_url('pos/add_wholesale') ?>/") + sid;
            <?php } ?>
        });

        $('#billSearch').on('click', function (e) {
            var reference = $('#search_reference').val();
            var destino = "<?= admin_url('pos/opened_bills_by_reference') ?>/"+reference+"/0";
            $("#billSearch").attr("href", destino);
        });

        $('.btn_filters').on('click', function (e) {
            $('.btn_filters').removeClass('btn_filters_active');
            $(this).addClass('btn_filters_active');
            var sort = $(this).data('sort');
            var destino = "<?= admin_url('pos/opened_bills_by_reference') ?>?sort_order="+sort;
            $(this).attr("href", destino);
        });

        setTimeout(function() {
            $('#search_reference').focus();
        }, 800);
        $('[data-toggle="tooltip"]').tooltip();
    });

    $('.bootbox-close-button').on('click', function(e){
        $('#myModal').modal('hide');
        e.preventDefault();
    });

    $('#search_reference').on('keypress', function(e){
        if (e.keyCode == 13) {
            $('#billSearch').click();
        }
    });
</script>
