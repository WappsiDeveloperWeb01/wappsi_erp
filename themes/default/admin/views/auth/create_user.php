<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open("auth/create_user", array('id' => 'create_user')); ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('first_name', 'first_name'); ?>
                                                    <div class="controls">
                                                        <?= form_input('first_name', '', 'class="form-control" id="first_name" required="required" pattern=".{3,10}"'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('last_name', 'last_name'); ?>
                                                    <div class="controls">
                                                        <?= form_input('last_name', '', 'class="form-control" id="last_name" required="required"'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('gender', 'gender'); ?>
                                                    <?php
                                                    $ge[''] = array('male' => lang('male'), 'female' => lang('female'));
                                                    echo form_dropdown('gender', $ge, (isset($_POST['gender']) ? $_POST['gender'] : ''), 'class="tip form-control" id="gender" data-placeholder="' . lang("select") . ' ' . lang("gender") . '" required="required"');
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('company', 'company'); ?>
                                                    <div class="controls">
                                                        <?= form_input('company', '', 'class="form-control" id="company" required="required"'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('phone', 'phone'); ?>
                                                    <div class="controls">
                                                        <?= form_input('phone', '', 'class="form-control" id="phone" required="required"'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('email', 'email'); ?>
                                                    <div class="controls">
                                                        <input type="email" id="email" name="email" class="form-control" required="required" />
                                                        <?php /* echo form_input('email', '', 'class="form-control" id="email" required="required"'); */ ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('user_pc_serial', 'user_pc_serial'); ?>
                                                    <div class="controls">
                                                        <input type="user_pc_serial" id="user_pc_serial" name="user_pc_serial" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('username', 'username'); ?>
                                                    <div class="controls">
                                                        <input type="text" id="username" name="username" class="form-control" required="required" pattern=".{4,20}" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('password', 'password'); ?>
                                                    <div class="controls">
                                                        <?= form_password('password', '', 'class="form-control tip" id="password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-regexp-message="' . lang('pasword_hint') . '"'); ?>
                                                        <span class="help-block"><?= lang('pasword_hint') ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('confirm_password', 'confirm_password'); ?>
                                                    <div class="controls">
                                                        <?= form_password('confirm_password', '', 'class="form-control" id="confirm_password" required="required" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                <button class="btn btn-primary submit_create" type="button"><?= lang('add_user') ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang("language", "language"); ?>
                                                    <?php
                                                        $lang = array(
                                                        'arabic'                    => 'Arabic',
                                                        'english'                   => 'English',
                                                        'german'                    => 'German',
                                                        'portuguese-brazilian'      => 'Portuguese (Brazil)',
                                                        'simplified-chinese'        => 'Simplified Chinese',
                                                        'spanish'                   => 'Spanish',
                                                        'thai'                      => 'Thai',
                                                        'traditional-chinese'       => 'Traditional Chinese',
                                                        'turkish'                   => 'Turkish',
                                                        'vietnamese'                => 'Vietnamese',
                                                        );
                                                    ?>
                                                    <?= form_dropdown('language', $lang, $Settings->language, 
                                                        'class="form-control tip" id="language" required="required" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('status', 'status'); ?>
                                                    <?= form_dropdown('status', 
                                                            [1 => lang('active'), 0 => lang('inactive')], 
                                                            (isset($_POST['status']) ? $_POST['status'] : ''), 
                                                            'id="status" required="required" class="form-control select" style="width:100%;"') ?>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang("group", "group"); ?>
                                                    <select name="group" id="group" required="required" class="form-control select">
                                                        <?php foreach ($groups as $group) : ?>
                                                            <option value="<?= $group['id'] ?>" data-relationedcompanies="<?= $group['relationed_companies'] ?>" data-groupname="<?= $group['name'] ?>"><?= ucwords(mb_strtolower(lang($group['name']))) ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <input type="hidden" name="group_name" id="group_name">
                                                </div>
                                            </div>
        
                                            <div class="col-md-12">
                                                <div class="companies_relation" style="display: none;">
                                                    <div class="from-group " style="width: 100%;">
                                                        <?= lang('company', 'company_id') ?>
                                                        <select name="company_id" id="company_id" class="form-control">
        
                                                        </select>
                                                        <em class="text-danger"><?= lang('input_required') ?></em>
                                                        <input type="hidden" name="relationedcompanies" id="relationedcompanies" value="0">
                                                    </div>
                                                    <div class="form-group" style="width: 100%; margin: 0 !important">
                                                        <?= lang('register_cash_movements_with_another_user', 'register_cash_movements_with_another_user') ?>
                                                        <select name="register_cash_movements_with_another_user" id="register_cash_movements_with_another_user" class="form-control">
                                                            <option value=""><?= lang('use_own_cash_register') ?></option>
                                                            <?php foreach ($users as $euser) : ?>
                                                                <option value="<?= $euser->id ?>"><?= $euser->first_name . " " . $euser->last_name . " (" . $euser->username . ")" ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
        
                                            <div class="clearfix"></div>

                                            <div class="col-md-12">
                                                <div class="administrator" style="display:none;">
                                                    <div class="form-group">
                                                        <?= lang("main_administrator", "main_administrator"); ?>
                                                        <select name="main_administrator" id="main_administrator" class="form-control">
                                                            <option value="0"><?= lang('no') ?></option>
                                                            <option value="1"><?= lang('yes') ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="no">
                                                    <div class="form-group">
                                                        <?= lang("biller", "biller"); ?>
                                                        <?php
                                                        $bl[""] = lang('select') . ' ' . lang('biller');
                                                        foreach ($billers as $biller) {
                                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                        }
                                                        echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ''), 'id="biller" class="form-control select" style="width:100%;"');
                                                        ?>
                                                    </div>
        
                                                    <div class="form-group">
                                                        <?= lang('document_type', 'sale_document_type_id') ?>
                                                        <select name="sale_document_type_id" class="form-control" id="sale_document_type_id"></select>
                                                    </div>

                                                    <div class="form-group">
                                                            <?= lang('fe_document_type', 'doc_type_id') ?>
                                                        <select name="doc_type_id" class="form-control" id="doc_type_id"></select>
                                                    </div>

                                                    <div class="form-group">
                                                            <?= lang('pos_document_type', 'pos_doc_type_id') ?>
                                                        <select name="pos_doc_type_id" class="form-control" id="pos_doc_type_id"></select>
                                                    </div>

                                                    <div class="form-group">
                                                            <?= lang('fe_pos_document_type', 'fe_pos_doc_type_id') ?>
                                                        <select name="fe_pos_doc_type_id" class="form-control" id="fe_pos_doc_type_id"></select>
                                                    </div>
        
                                                    <div class="form-group">
                                                        <?= lang("warehouse", "warehouse"); ?>
                                                        <?php
                                                        $wh[''] = lang('select') . ' ' . lang('warehouse');
                                                        foreach ($warehouses as $warehouse) {
                                                            $wh[$warehouse->id] = $warehouse->name;
                                                        }
                                                        echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ''), 'id="warehouse" class="form-control select" style="width:100%;" ');
                                                        ?>
                                                    </div>
        
                                                    <div class="form-group seller_div">
                                                        <?= lang("seller", "seller"); ?>
                                                        <?php
                                                        $sl[""] = lang('select') . ' ' . lang('seller');
                                                        foreach ($sellers as $seller) {
                                                            $sl[$seller->id] = $seller->company != '-' ? $seller->company : $seller->name;
                                                        }
                                                        echo form_dropdown('seller', $sl, (isset($_POST['seller']) ? $_POST['seller'] : ''), 'id="seller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("seller") . '" class="form-control select" style="width:100%;"');
                                                        ?>
                                                    </div>
        
                                                    <div class="form-group">
                                                        <?= lang("view_right", "view_right"); ?>
                                                        <?php
                                                        $vropts = array(1 => lang('all_records'), 0 => lang('own_records'));
                                                        echo form_dropdown('view_right', $vropts, (isset($_POST['view_right']) ? $_POST['view_right'] : 1), 'id="view_right" class="form-control select" style="width:100%;"');
                                                        ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang("edit_right", "edit_right"); ?>
                                                        <?php
                                                        $opts = array(1 => lang('yes'), 0 => lang('no'));
                                                        echo form_dropdown('edit_right', $opts, (isset($_POST['edit_right']) ? $_POST['edit_right'] : 0), 'id="edit_right" class="form-control select" style="width:100%;"');
                                                        ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang("allow_discount", "allow_discount"); ?>
                                                        <?= form_dropdown('allow_discount', $opts, (isset($_POST['allow_discount']) ? $_POST['allow_discount'] : 0), 'id="allow_discount" class="form-control select" style="width:100%;"'); ?>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <label class="checkbox" for="cant_finish_pos_invoice">
                                                                <input type="checkbox" name="cant_finish_pos_invoice" value="1" id="cant_finish_pos_invoice" />
                                                                <?= lang('cant_finish_pos_invoice') ?>
                                                            </label>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
        
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?= lang('user_dashboard', 'dashboard_id') ?>
                                                    <select name="dashboard_id" id="dashboard_id" class="form-control">
                                                        <option value=""><?= lang('select') ?></option>
                                                        <?php if ($dashboards) : ?>
                                                            <?php foreach ($dashboards as $dashboard) : ?>
                                                                <option value="<?= $dashboard->id ?>"><?= $dashboard->name ?></option>
                                                            <?php endforeach ?>
                                                        <?php endif ?>
                                                    </select>
                                                </div>
                                            </div>
            
                                            <div class="col-md-8">
                                                <label class="checkbox" for="notify">
                                                    <input type="checkbox" name="notify" value="1" id="notify" checked="checked" />
                                                    <?= lang('notify_user_by_email') ?>
                                                </label>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#create_user').validate({
            ignore: []
        });

        $('.no').slideUp();
        
        $('#group').change(function(event) {
            var group = $(this).val();
            var groupname = $('#group option:selected').data('groupname');
            if (group == 1 || group == 2 || groupname == 'preparation') {
                $('.no').slideUp();
            } else {
                $('.no').slideDown();
            }
            if (group == 2) {
                $('.administrator').slideDown();
            } else {
                $('.administrator').slideUp();
            }
        });

        $('#biller').on('change', function() {
            getDocumentTypeToSalesAjax()
            getDocumentTypeToElectronicBillingAjax()
            getDocumentTypePosAjax()
            getDocumentTypePosToElectronicBillingAjax()
        })
    });

    $(document).on('change', '#group', function() {
        var selected = $('#group option:selected');

        $('#group_name').val(selected.data('groupname'));

        if (selected.data('relationedcompanies')) {

            $.ajax({
                url: "<?= admin_url('auth/get_companies') ?>",
                type: "get",
                data: {
                    group_name: selected.data('groupname')
                }
            }).done(function(data) {

                if (data != false) {
                    $('.companies_relation').css('display', '');
                    $('#company_id').html(data);
                    $('#company_id').select2({
                        width: '100%',
                        required: true
                    });
                    $('#relationedcompanies').val(1);
                } else {
                    $('.companies_relation').css('display', 'none');
                    $('#relationedcompanies').val(0);
                }

                console.log(data);
            });

            $('.seller_div').fadeOut();
            $('#seller').select2('val', '');

            $('#biller').prop('required', true);
            $('#doc_type_id').prop('required', true);

        } else {
            $('.companies_relation').css('display', 'none');
            $('#company_id').empty().select2({
                required: false
            });
            $('#relationedcompanies').val(0);
            $('.seller_div').fadeIn();

            $('#biller').prop('required', false);
            $('#doc_type_id').prop('required', false);
        }

    });

    $(document).on('click', '.submit_create', function() {
        if ($('#create_user').valid()) {
            $('#create_user').submit();
        }
    });

    // $('#biller').on('change', function() {
    //     $.ajax({
    //         url: '<?= admin_url("billers/getBillersDocumentTypes/1/") ?>' + $('#biller').val(),
    //         type: 'get',
    //         dataType: 'JSON'
    //     }).done(function(data) {
    //         response = data;
    //         $('#doc_type_id').html(response.options).select2();
    //         if (response.not_parametrized != "") {
    //             command: toastr.warning('Los documentos <b> (' + response.not_parametrized + ') no están parametrizados </b> en contabilidad', '¡Atención!', {
    //                 "showDuration": "500",
    //                 "hideDuration": "1000",
    //                 "timeOut": "6000",
    //                 "extendedTimeOut": "1000",
    //             });
    //         }
    //         if (response.status == 0) {
    //             $('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>");
    //         } else {
    //             $('.resAlert').css('display', 'none');
    //         }
    //     });
    // });

    $(document).on('change', '#email', function() {

        var email = $('#email').val().replace("@", '999666');
        $.ajax({
            url: '<?= admin_url("auth/validate_user_email/") ?>' + email,
            type: 'get',
            dataType: 'JSON'
        }).done(function(data) {
            if (data.exists == 1) {
                $('.submit_create').prop('disabled', true);
                Command: toastr.error('El correo que ingresó ya existe para otro usuario, por favor verifíquelo', 'Correo existente', {
                    onHidden: function() {}
                })
                $('#email').select().css('background-color', '#fbc2c4');
            } else {
                $('.submit_create').prop('disabled', false);
                $('#email').css('background-color', 'white');
            }
        });

    });

    $(document).on('keyup', '#username', function() {

        var username = $('#username').val();
        $.ajax({
            url: '<?= admin_url("auth/validate_username/") ?>' + username,
            type: 'get',
            dataType: 'JSON'
        }).done(function(data) {
            if (data.exists == 1) {
                $('.submit_create').prop('disabled', true);
                Command: toastr.error('El nombre de usuario que ingresó ya existe para otro usuario, por favor verifíquelo', 'Nombre de usuario existente', {
                    onHidden: function() {}
                })
            } else {
                $('.submit_create').prop('disabled', false);
            }
        });

    });

    function getDocumentTypeToSalesAjax()
    {
        $.ajax({
            url     : site.base_url + 'billers/getBillersDocumentTypesAjax',
            type    : 'POST',
            dataType: 'JSON',
            data    : {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                billerId        : $('#biller').val(),
                isItElectronic  : 0,
                module          : 2
            },
        }).done(function(data) {
            response = data;
            $('#sale_document_type_id').html(response.options).select2()

            showAlerts(response)
        });
    }

    function getDocumentTypeToElectronicBillingAjax()
    {
        $.ajax({
            url     : site.base_url + 'billers/getBillersDocumentTypesAjax',
            type    : 'POST',
            dataType: 'JSON',
            data    : {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                billerId        : $('#biller').val(),
                isItElectronic  : 1,
                module          : 2
            },
        }).done(function(data) {
            response = data;
            $('#doc_type_id').html(response.options).select2()

            showAlerts(response)
        });
    }

    function getDocumentTypePosAjax()
    {
        $.ajax({
            url     : site.base_url + 'billers/getBillersDocumentTypesAjax',
            type    : 'POST',
            dataType: 'JSON',
            data    : {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                billerId        : $('#biller').val(),
                isItElectronic  : 0,
                module          : 1,
            },
        }).done(function(data) {
            response = data;
            $('#pos_doc_type_id').html(response.options).select2()
            
            showAlerts(response)
        });
    }

    function getDocumentTypePosToElectronicBillingAjax()
    {
        $.ajax({
            url     : site.base_url + 'billers/getBillersDocumentTypesAjax',
            type    : 'POST',
            dataType: 'JSON',
            data    : {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                billerId        : $('#biller').val(),
                isItElectronic  : 1,
                module          : 1,
            },
        }).done(function(data) {
            response = data;
            $('#fe_pos_doc_type_id').html(response.options).select2();

            showAlerts(response)
        });
    }

    function showAlerts(response)
    {
        if (response.not_parametrized != "") {
            command: toastr.warning('Los documentos <b> (' + response.not_parametrized + ') no están parametrizados </b> en contabilidad', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "6000",
                "extendedTimeOut": "1000",
            });
        }

        if (response.status == 0) {
            $('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>");
        } else {
            $('.resAlert').css('display', 'none');
        }
    }
</script>