<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $page_title; ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body style="color: #4e4e4e; font-family: Roboto, sans-serif;">
    <table style="width: 800px" align="center">
        <tbody style="width: 100%">
            <tr style="background-color: gray; text-align: left; width: 100%;">
                <th style="background-color: white; width: 40%;">
                    <img src="<?= base_url("assets/uploads/logos/".$issuer->logo); ?>">
                </th>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid #979a9b"></td>
            </tr>
            <tr style="width: 100%;">
                <td style="font-weight:lighter; width: 100%;" colspan="2">
                    <p style="font-size: 70px; padding: 0 10px; text-align: center;">
                    	¡Gracias!
                    	<br>
                    	<i class="fa fa-check" style="color: green"></i>
                	</p>
                </td>
            </tr>
            <tr><td colspan="2" style="border-bottom:1px solid #979a9b"></td></tr>
            <tr style="width: 100%;">
                <td style="width: 30%;">
                    <p style=" font-size: 20px; padding: 0 10px; text-align: center;">Su respuesta fue recibida exitosamente.</p>
                </td>
            </tr>
            <tr><td colspan="2" style="border-bottom:1px solid #979a9b"></td></tr>
            <tr>
                <td colspan="2"><small style="font-weight: lighter;">Comprobante generado por: <a href="https://www.wappsi.com.co/">Web Apps Innovation SAS</a> | Copyright © 2019  | wappsi te lo simplifica | Telefono: 301 794 6302</small></td>
            </tr>
        </tbody>
    </table>
</body>
</html>