<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FutureItems_model extends CI_Model {
    public function get_by_data($payroll_id, $contract_id)
    {
        $this->db->where('payroll_id', $payroll_id);
        $this->db->where('contract_id', $contract_id);
        $q = $this->db->get('payroll_futures_items');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

	public function insert($data)
	{
		if ($this->db->insert('payroll_futures_items', $data)) {
			return TRUE;
		}
		return FALSE;
	}

    public function delete($payroll_id, $employee_id, $contract_id, $concept_id = NULL)
    {
        if (!empty($concept_id)) {
            $this->db->where('concept_id', $concept_id);
        }
        $this->db->where('payroll_id', $payroll_id);
        $this->db->where('employee_id', $employee_id);
        $this->db->where('contract_id', $contract_id);
        if ($this->db->delete('payroll_futures_items')) {
            return TRUE;
        }
        return FALSE;
    }

}

/* End of file FutureItems_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/FutureItems_model.php */