<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tax_rates extends My_Controller {

	public function index()
	{
		parent::__construct();

		if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if ($this->Supplier || $this->Customer) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('form_validation');
        $this->load->admin_model('returns_model');
	}


}

/* End of file Tax_rates.php */
/* Location: .//opt/lampp/htdocs/wappsi/app/controllers/admin/Tax_rates.php */