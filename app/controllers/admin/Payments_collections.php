<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_collections extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (empty($this->Settings->payment_collection_webservice)) {
            $this->session->set_flashdata('error', lang("No tiene acceso a este módulo"));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('form_validation');
        $this->load->admin_model('payments_collections_model');
        $this->load->admin_model('UserActivities_model');
    }

    public function add()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('pc_amount[]', lang("amount"), 'required');
        $this->form_validation->set_rules('pc_biller', lang("biller"), 'required');
        $this->form_validation->set_rules('pc_document_type_id', lang("reference"), 'required');
        if ($this->form_validation->run() == true) {
            $amount = $this->input->post('pc_amount[]');
            $note = $this->input->post('pc_note[]');
            $date = date('Y-m-d H:i:s');
            $biller_id = $this->input->post('pc_biller');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            $customer_id = $this->input->post('customer_id');
            $document_type_id = $this->input->post('pc_document_type_id');
            $paid_by = $this->input->post('paid_by');
            $kaiowa_payment_collection_reference = $this->input->post('kaiowa_payment_collection_reference');
            $kaiowa_payment_collection_amount = $this->input->post('kaiowa_payment_collection_amount');
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect($_SERVER["HTTP_REFERER"]);
            }
            $data = [];
            foreach ($amount as $key => $value) {
                $data[$key] = array(
                            'date' => $date,
                            'reference_no' => '',
                            'amount' => $value,
                            'note' => $note,
                            'paid_by' => $paid_by[$key],
                            'biller_id' => $biller_id,
                            'customer_id' => $customer_id,
                            'document_type_id' => $document_type_id,
                            'kaiowa_payment_collection_reference' => $kaiowa_payment_collection_reference,
                            'kaiowa_payment_collection_amount' => $kaiowa_payment_collection_amount,
                            'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                            'registration_date' => date('Y-m-d H:i:s')
                        );
                if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                    $data[$key]['cost_center_id'] = $biller_cost_center->id;
                } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('pc_cost_center_id')) {
                    $data[$key]['cost_center_id'] = $this->input->post('pc_cost_center_id');
                }
            }

            if (!$this->session->userdata('payment_collection_processing')) {
                $this->session->set_userdata('payment_collection_processing', 1);
            } else {
                $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
                admin_redirect("payments_collections");
            }

            $ref_payment = $this->payments_collections_model->add($data);
        }
        if ($this->form_validation->run() == true && $ref_payment) {
            if ($this->session->userdata('payment_collection_processing')) {
                $this->session->unset_userdata('payment_collection_processing');
            }
            $this->session->set_flashdata('message', sprintf(lang("payment_collection_registered"), lang('payment_collection')));
            admin_redirect("payments_collections/view/{$ref_payment}");
        } else {
            $this->session->set_flashdata('error', validation_errors());
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['users'] = $this->site->get_all_users();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->load_view($this->theme . 'payments_collections/add', $this->data);
        }
    }

    public function index($id = null)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('payments_collections')));
        $meta = array('page_title' => lang('payments_collections'), 'bc' => $bc);

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([57]);
        $this->data['users'] = $this->site->get_all_users();
        $this->data["advancedFiltersContainer"] = false;

        $this->page_construct('payments_collections/index', $meta, $this->data);
    }

    public function get_payments_collections()
    {
        $change_payment_method = anchor('admin/payments_collections/change_payment_method/$1', '<i class="fa fa-file-text-o"></i> ' . lang('change_payment_method'), 'data-toggle="modal" data-target="#myModal"');
        $void = anchor('admin/payments_collections/void/$1', '<i class="fas fa-ban"></i> ' . lang('void'), 'class="void"');
        if (1 == 1) {
            $changePaymentMethodLinkItem =  '<li>' .$change_payment_method. '</li>';
            $action = '<div class="text-center">'.
                '<div class="btn-group text-left">'.
                    '<button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                        <i class="fas fa-ellipsis-v fa-lg"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">'
                        .$changePaymentMethodLinkItem.
                        '<li>'.$void.'</li>'.
                    '</ul>
                </div>'.
            '</div>';
        }

        if ($this->input->post('start_date')) {
             $start_date = $this->input->post('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        $document_type_id = NULL;
        if ($this->input->post('posrmpayment_reference_no')) {
            $document_type_id = $this->input->post('posrmpayment_reference_no');
        }
        if ($this->session->userdata('biller_id')) {
            $biller_id = $this->session->userdata('biller_id');
        } else {
            $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        }
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $this->load->library('datatables');
        $this->datatables
            ->select(
                        $this->db->dbprefix('payment_collection') . ".id as id,
                        date,
                        reference_no,
                        companies.name,
                        companies.vat_no,
                        SUM(".$this->db->dbprefix('payment_collection') . ".amount),
                        ".$this->db->dbprefix('payment_collection') . ".note,
                        CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user"
                        , false)
            ->from('payment_collection')
            ->join('users', 'users.id=payment_collection.created_by', 'left')
            ->join('companies', 'companies.id=payment_collection.customer_id', 'left')
            ->group_by('payment_collection.reference_no')
            ;
        /* FILTROS */
        if ($start_date) {
            $this->datatables->where('payment_collection.date >= ', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('payment_collection.date <= ', $end_date);
        }
        if ($document_type_id) {
            $this->datatables->where('payment_collection.document_type_id', $document_type_id);
        }
        if ($user_id) {
            $this->datatables->where('payment_collection.created_by', $user_id);
        }
        if ($biller_id) {
            $this->datatables->where('payment_collection.biller_id', $biller_id);
        }
        /* FILTROS */
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function modal_view($reference){
        $payment_collection = $this->payments_collections_model->get_payment_collection($reference);
        $this->data['user'] = $this->site->getUser($payment_collection[0]->created_by);
        $this->data['biller'] = $payment_collection[0]->biller_id ? $this->site->getCompanyByID($payment_collection[0]->biller_id) : NULL;
        $this->data['payment_collection'] = $payment_collection;
        $this->data['page_title'] = $this->lang->line("payment_collection_view");
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($payment_collection[0]->cost_center_id);
        }
        $this->load_view($this->theme . 'payments_collections/modal_view', $this->data);
    }

    public function view($reference){
        $payment_collection = $this->payments_collections_model->get_payment_collection($reference);
        $this->data['user'] = $this->site->getUser($payment_collection[0]->created_by);
        $this->data['biller'] = $payment_collection[0]->biller_id ? $this->site->getCompanyByID($payment_collection[0]->biller_id) : NULL;
        $this->data['payment_collection'] = $payment_collection;
        $this->data['page_title'] = $this->lang->line("payment_collection_view");
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($payment_collection[0]->cost_center_id);
        }
        $this->load_view($this->theme . 'payments_collections/view', $this->data);
    }

    public function checkAmountPaidKiowa()
    {
        $customerId = $this->input->post('customerId');
        $kaiowaReference = $this->input->post('kaiowaReference');
        $aSchemacia = $this->Settings->a_schemacia;
        $aIdMarca = $this->Settings->a_id_marca;
        $aLogin = $this->Settings->a_login;
        $aPassword = $this->Settings->a_password;
        $customer = $this->site->getCompanyByID($customerId);
        $vatNo = $customer->vat_no;

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://kaiowa.app/APIS/API_REST_SERVICE_KAIOWA/services/transactions/get_rep_recibo_abono",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"a_schemacia\":\"$aSchemacia\", \"a_id_marca\":\"$aIdMarca\", \"a_login\":\"$aLogin\", \"a_password\": \"$aPassword\", \"a_recibo\":\"$kaiowaReference\", \"a_cedula_deudor\":\"$vatNo\"}",
            CURLOPT_HTTPHEADER => [
                "Accept: */*",
                "Content-Type: application/json",
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        echo $this->manageResponseKaiowa($response, $err);
        // el siguiente codigo es la estructura de la respuesta sirve para realizar pruebas no borrar
        // echo $this->manageResponseKaiowa(json_encode(["datos" => [["neto_apagar" => 1000]], "err" => 0 ]), false);
    }

    private function manageResponseKaiowa($response, $err)
    {
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response);

            if ($response->err == 0) {
                $data = $response->datos[0];

                return json_encode([
                    'status' => true,
                    'message' => '',
                    'data' => $data->neto_apagar,
                ]);
            } else {
                return json_encode([
                    'status' => false,
                    'message' => $response->msg
                ]);
            }
        }
    }

    public function change_payment_method($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('paid_by[]', lang('paid_by'), 'required');
        $payment_collection = $this->payments_collections_model->getPaymentCollectionByID($id);
        $pos_register_status = $this->pos_model->get_invoice_pos_register_status($payment_collection);
        if ($this->form_validation->run() == true) {
            if (!$pos_register_status) {
                $this->session->set_flashdata('error', lang('collection_from_pos_register_closed'));
                admin_redirect("payments_collections/index");
            }
            $paid_by = $this->input->post('paid_by');
            $payment_id = $this->input->post('payment_id');
            $customer_id = $this->input->post('customer_id');
            $payment_amount = $this->input->post('payment_amount');
            foreach ($paid_by as $key => $value) {
                $data = array(
                    'id'            => $payment_id[$key],
                    'paid_by'       => $paid_by[$key],
                    'customer_id'   => $customer_id[$key],
                    'amount'        => $payment_amount[$key]
                );
                $ref_payment = $this->payments_collections_model->update($data);
            }
        }
        if ($this->form_validation->run() == true && $ref_payment) {
            $this->session->set_flashdata('message', sprintf(lang("payment_method_updated"), lang('payment_method_updated')));
            admin_redirect("payments_collections/index");
        }
        else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['inv'] = $payment_collection;
            $this->data['payments'] = $this->payments_collections_model->getPaymentCollectionByRef($payment_collection->reference_no);
            $this->data['pos_register_status'] = $pos_register_status;
            // exit(var_dump($pos_register_status));
           if (!$pos_register_status) {
                $this->session->set_flashdata('error', lang('collection_from_pos_register_closed'));
            }
            $this->load_view($this->theme . 'payments_collections/change_payment_method', $this->data);
        }
    }

    public function void($id)
    {
        $paymentColletions = $this->payments_collections_model->find(["id" => $id]);
        if (!empty($paymentColletions)) {
            $this->db->where("id", $id);
            if ($this->db->update('payment_collection', ['amount' => 0, "note" => "anulado", "kaiowa_payment_collection_amount" => 0])) {
                $this->userActivities($paymentColletions);

                $this->session->set_flashdata('message', lang("canceled_collection"));
                admin_redirect($_SERVER["HTTP_REFERER"]);
            }

            $this->session->set_flashdata('error', lang("uncanceled_collection"));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function userActivities($paymentColletions)
    {
        $amountChangeMade = $this->changesMade($paymentColletions->amount, 0, "Valor monto de");

        if ($amountChangeMade != "") {
            $description = "{$this->session->userdata("username")} editó los siguiente datos: $amountChangeMade";

            $data = [
                "date" => date("Y-m-d H:i:s"),
                "type_id" => 1,
                "user_id" => $this->session->userdata('user_id'),
                "module_name" => "Anular Recaudo Kaiowa",
                "table_name" => "payment_collection",
                "record_id" => $paymentColletions->id,
                "description" => $description
            ];
            $this->UserActivities_model->insert($data);
        }
    }

    private function changesMade($data1, $data2, $string)
    {
        if ($this->sma->formatDecimal($data1) != $this->sma->formatDecimal($data2)) {
            return "{$string} {$data1} a {$data2}; ";
        }
    }
}