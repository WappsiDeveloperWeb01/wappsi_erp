<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
  $(document).ready(function(){
    if ((site.m_module == 'sales' && site.v_action == 'add') || (site.m_module == 'pos' && site.v_action == 'index')) {
      if ("<?= $customer->vat_no ?>" == "222222222222") {
        setTimeout(function() {
          $('#myModal2').modal('hide');
          Command: toastr.error('No se puede editar este cliente', '¡Error!', {onHidden : function(){}})
        }, 1200);
      }
    }
  });
</script>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <?= admin_form_open_multipart("customers/edit/".$customer->id, ['id' => 'edit-customer-form']); ?>
        <?= form_hidden('document_code'); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_customer'); ?></h4>
            </div>
            <div class="modal-body">
              <p><?= lang('enter_info'); ?></p>
              <div class="row">
                <div class="col-sm-3 form-group">
                  <label>
                    <input type="checkbox" name="status" id="status" <?= $customer->status == 1 ? 'checked="checked"' : '' ?>> <?= lang('customer')." ".lang('active') ?>
                  </label>
                </div>
                <div class="col-sm-3 form-group">
                  <label>
                    <input type="checkbox" name="customer_only_for_pos" id="customer_only_for_pos" <?= $customer->customer_only_for_pos == 1 ? 'checked="checked"' : '' ?>> <?= lang('customer_only_for_pos') ?>
                  </label>
                </div>
                <div class="col-sm-3 form-group">
                  <label>
                    <input type="checkbox" name="award_points_no_management" id="award_points_no_management" <?= $customer->award_points_no_management == 1 ? 'checked="checked"' : '' ?>> <?= lang('award_points_no_management') ?>
                  </label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <?= lang('customer_type', 'customer_type') ?>
                  <select name="customer_type" id="customer_type" class="form-control">
                    <option value="customer" <?= $customer->group_name == 'customer' ? 'selected' : '' ?>><?= lang('customer') ?></option>
                    <option value="lead" <?= $customer->group_name == 'lead' ? 'selected' : '' ?>><?= lang('lead') ?></option>
                  </select>
                </div>
                <?php if ($this->Settings->enable_customer_tax_exemption == 1): ?>
                  <div class="col-sm-6 form-group">
                    <label>
                      <input type="checkbox" name="tax_exempt_customer" id="tax_exempt_customer" <?= $customer->tax_exempt_customer == 1 ? 'checked="checked"' : '' ?>> <?= lang('tax_exempt_customer') ?>
                    </label>
                  </div>
                <?php endif ?>
              </div>
              <div class="row">
                <div class="form-group col-sm-6">
                  <?= form_label(lang('label_type_person'), 'type_person'); ?>
                  <?php
                    $types_person_options[""] = lang('select');
                    foreach ($types_person as $type_person)
                    {
                      $types_person_options[$type_person->id] = lang($type_person->description);
                    }
                  ?>
                  <?= form_dropdown(['name'=>'type_person', 'id'=>'type_person', 'class'=>'form-control select', 'required'=>TRUE], $types_person_options, $customer->type_person); ?>
                </div>
                <div class="form-group col-sm-6">
                  <?= form_label(lang("label_type_vat_regime"), "type_vat_regime"); ?>
                  <?php
                    $types_vat_regime_options[""] = lang('select');
                    foreach ($types_vat_regime as $type_vat_regime)
                    {
                      $types_vat_regime_options[$type_vat_regime->id] = lang($type_vat_regime->description);
                    }
                  ?>
                  <?= form_dropdown(["name"=>"type_vat_regime", "id"=>"type_vat_regime", "class"=>"form-control select", "required"=>TRUE], $types_vat_regime_options, $customer->tipo_regimen); ?>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <?= lang("id_document_type", "id_document_type"); ?>
                  <select class="form-control" name="tipo_documento" id="tipo_documento" style="width: 100%;" required>
                    <option value=""><?= lang("select"); ?></option>
                    <?php foreach ($id_document_types as $idt) : ?>
                      <option value="<?= $idt->id; ?>" data-code="<?= $idt->codigo_doc; ?>" <?= (isset($customer) && $customer->tipo_documento == $idt->id) ? "selected" : ""; ?>><?= lang($idt->nombre); ?></option>
                    <?php endforeach ?>
                  </select>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("vat_no", "vat_no"); ?>
                        <?php echo form_input('vat_no', $customer->vat_no, 'class="form-control" id="vat_no" required'); ?>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 digito-verificacion" style="display: none;">
                  <div class="form-group">
                    <?= lang("check_digit", "check_digit"); ?>
                    <?php echo form_input('digito_verificacion', $customer->digito_verificacion, 'class="form-control" id="digito_verificacion" readonly'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6 person juridical_person">
                  <?= lang("label_business_name", "name"); ?>
                  <?php echo form_input('name', $customer->name, 'class="form-control tip input_required" id="name" data-bv-notempty="true"'); ?>
                </div>
              </div>
              <div class="form-group person natural_person" style="display: none;">
                <div class="row">
                  <div class="col-md-6">
                    <label>Primer Nombre</label><span class='input_required'> *</span>
                    <input type="text" name="first_name" id="first_name" value="<?= $customer->first_name ?>" class="form-control input_required">
                  </div>
                  <div class="col-md-6">
                    <label>Segundo Nombre</label>
                    <input type="text" name="second_name" id="second_name" value="<?= $customer->second_name ?>" class="form-control">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <label>Primer Apellido</label><span class='input_required'> *</span>
                    <input type="text" name="first_lastname" id="first_lastname" value="<?= $customer->first_lastname ?>" class="form-control input_required">
                  </div>
                  <div class="col-md-6">
                    <label>Segundo Apellido</label>
                    <input type="text" name="second_lastname" id="second_lastname" value="<?= $customer->second_lastname ?>" class="form-control">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group company hide_pos_customer" <?= $customer->customer_only_for_pos == 1 ? 'style="display:none;"' : '' ?>>
                  <?= lang("label_tradename", "company"); ?>
                  <?= form_input(["name"=>"company", "class"=>"form-control tip", "id"=>"company", "required"=> TRUE], (!empty($customer->company) ? $customer->company : $customer->name)); ?>
                </div>
                <div class="col-md-6 form-group juridical_person hide_pos_customer" <?= $customer->customer_only_for_pos == 1 ? 'style="display:none;"' : '' ?>>
                  <?= form_label(lang('label_commercial_register'), 'commercial_register'); ?>
                  <?= form_input(['name'=>'commercial_register', 'id'=>'commercial_register', 'class'=>'form-control'], $customer->commercial_register); ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group ">
                    <?= lang("email_address", "email_address"); ?>
                    <input type="email" name="email" class="form-control" <?= $customer->customer_only_for_pos == 1 ? '' : 'required="required"' ?> value="<?= $customer->email ?>" id="email_address"/>
                </div>
                <div class="col-md-6 form-group">
                  <?= lang("phone", "phone"); ?>
                  <?= form_input(["type"=>"tel", "name"=>"phone", "id"=>"phone", "class"=>"form-control", "required"=>TRUE], $customer->phone); ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <?= lang("country", "country"); ?>
                  <select class="form-control select" name="country" id="country" required>
                    <option value=""><?= lang("select"); ?></option>
                    <?php foreach ($countries as $row => $country): ?>
                      <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= ($country->NOMBRE == $customer->country) ? "selected='selected'" : "" ?> ><?= $country->NOMBRE ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-sm-6 form-group">
                  <?= lang("state", "state"); ?>
                  <select class="form-control select" name="state" id="state" required>
                    <option value=""><?= lang("select"); ?></option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <?= lang("city", "city"); ?>
                  <select class="form-control select" name="city" id="city" required>
                    <option value=""><?= lang("select"); ?></option>
                  </select>
                  <?php $cityCode = (isset($_POST['city_code'])) ? $_POST['city_code'] : (!empty($customer->city_code) ? $customer->city_code : ''); ?>
                  <input type="hidden" name="city_code" id="city_code" value="<?= $cityCode ?>">
                </div>
                <div class="form-group col-md-6">
                  <?= lang("zone", "zone"); ?>
                  <select class="form-control select" name="zone" id="zone">
                    <option value=""><?= lang("select") ?></option>
                  </select>
                  <input type="hidden" name="zone_code" id="zone_code" <?= (isset($_POST['zone_code'])) ? "value='".$_POST['zone_code']."'" : "" ?>>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <?= lang("subzone", "subzone"); ?>
                  <select class="form-control select" name="subzone" id="subzone">
                    <option value=""><?= lang("select") ?></option>
                  </select>
                  <input type="hidden" name="subzone_code" id="subzone_code" <?= (isset($_POST['subzone_code'])) ? "value='".$_POST['subzone_code']."'" : "" ?>>
                </div>
                <div class="col-sm-6 form-group">
                  <?= lang("address", "address"); ?>
                  <?= form_input('address', $customer->address, 'class="form-control input_required" id="address"'); ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 form-group">
                  <?= lang("postal_code", "postal_code"); ?>
                  <small><a href="https://visor.codigopostal.gov.co/472/visor/" target="_blank"> Buscar códigos postales</a></small>
                  <?= form_input('postal_code', $customer->postal_code, 'class="form-control postal_code" id="postal_code"'); ?>
                </div>
                <div class="form-group col-md-6 row">
                  <div class="col-md-6">
                    <?= lang('birth_month', 'birth_month') ?>
                    <?php

                    for ($i=1; $i <=12 ; $i++) {
                      $bmopts[$i] = lang('months')[$i];
                    }

                     ?>
                     <?= form_dropdown('birth_month', $bmopts, $customer->birth_month, 'class="form-control" id="birth_month"') ?>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <?= lang('birth_day', 'birth_day') ?>
                    <?php

                    $bdopts[''] = lang('select');

                     ?>
                    <?= form_dropdown('birth_day', $bdopts, '', 'class="form-control" id="birth_day"') ?>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <?= lang("award_points", "award_points"); ?>
                  <?= form_input('award_points', set_value('award_points', $customer->award_points), 'class="form-control input_required tip" id="award_points"');  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group hide_pos_customer" <?= $customer->customer_only_for_pos == 1 ? 'style="display:none;"' : '' ?>>
                  <?= form_label(lang("label_types_obligations"), "types_obligations"); ?>
                  <br>
                    <?php
                        $types_obligations_options = [];
                        foreach ($types_obligations as $types_obligation)
                        {
                            $types_obligations_options[$types_obligation->code] = $types_obligation->code ." - ". $types_obligation->description;
                        }
                    ?>
                    <?php
                      $ob_attr = [
                                  'name'=>'types_obligations[]',
                                  'id'=>'types_obligations',
                                  'class'=>'form-control select',
                                  'multiple'=>'multiple',
                                  'style' => 'height:auto;'
                                ];
                      if ($customer->customer_only_for_pos != 1) {
                        $ob_attr['required'] = false;
                      }

                     ?>
                    <?= form_dropdown($ob_attr, $types_obligations_options, ($types_customer_obligations ? $types_customer_obligations : ["O-99"])); ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <label class="control-label" for="customer_group"><?php echo $this->lang->line("customer_group"); ?>  <input type="checkbox" name="customer_special_discount" id="customer_special_discount" <?= $customer->customer_special_discount ? 'checked="checked"' : '' ?>> <?= lang('customer_special_discount', 'customer_special_discount') ?></label>
                  <?php
                    foreach ($customer_groups as $customer_group) {
                        $cgs[$customer_group->id] = $customer_group->name;
                    }
                    echo form_dropdown('customer_group', $cgs, $customer->customer_group_id, 'class="form-control select input_required" id="customer_group" style="width:100%;" ');
                  ?>
                </div>
                <div class="col-md-6 form-group">
                  <label class="control-label" for="price_group"><?php echo $this->lang->line("price_group"); ?></label>
                  <?php
                    $pgs[''] = lang('select').' '.lang('price_group');
                    foreach ($price_groups as $price_group) {
                      $pgs[$price_group->id] = $price_group->name;
                    }
                    echo form_dropdown('price_group', $pgs, $customer->price_group_id, 'class="form-control select" id="price_group" style="width:100%;"');
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <?= lang('customer_seller_id_assigned', 'customer_seller_id_assigned') ?>
                  <?php
                    $sellers_opt[''] = lang('select');
                    foreach ($sellers as $seller) {
                        $sellers_opt[$seller->id] = $seller->name;
                    }
                   ?>
                  <?=  form_dropdown('customer_seller_id_assigned', $sellers_opt, $customer->customer_seller_id_assigned, 'class="form-control select" id="customer_seller_id_assigned"'); ?>
                </div>
                <div class="form-group col-md-6">
                    <?= lang('customer_payment_type', 'customer_payment_type') ?>
                    <?php
                      $cptypes = [
                        '0' => lang('credit'),
                        '1' => lang('payment_type_cash'),
                      ];
                    ?>
                    <?= form_dropdown('customer_payment_type', $cptypes, $customer->customer_payment_type, 'class="form-control select input_required" id="customer_payment_type" style="width:100%;"'); ?>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6 credit_customer">
                    <?= lang('customer_credit_limit', 'customer_credit_limit') ?>
                    <input type="text" name="customer_credit_limit" id="customer_credit_limit" class="form-control only_number" value="<?= $customer->customer_credit_limit ?>">
                </div>
                <div class="form-group col-md-6 credit_customer">
                    <?= lang('customer_payment_term', 'customer_payment_term') ?>
                    <input type="text" name="customer_payment_term" id="customer_payment_term" class="form-control only_number" value="<?= $customer->customer_payment_term ?>">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                    <?= lang('latitude', 'latitude') ?>
                    <input type="text" name="latitude" id="latitude" class="form-control" value="<?= $main_address ? $main_address->latitude : '' ?>">
                </div>
                <div class="form-group col-md-6">
                    <?= lang('longitude', 'longitude') ?>
                    <input type="text" name="longitude" id="longitude" class="form-control" value="<?= $main_address ? $main_address->longitude : '' ?>">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-sm-6">
                    <?= lang('customer_employer', 'employer') ?>
                    <?php
                    echo form_input('employer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="customer" data-placeholder="' . lang("select") . ' ' . lang("employer") . '" class="form-control input-tip" style="width:100%;"');
                    ?>
                </div>
                <div class="form-group col-sm-6">
                      <?= lang("customer_employer_branch", "employer_branch"); ?>
                      <?php
                      echo form_dropdown('employer_branch', array('0' => lang("select") . ' ' . lang("customer_employer")), (isset($_POST['employer_branch']) ? $_POST['employer_branch'] : ''), 'id="address_id" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip select" style="width:100%;"');
                      ?>
                  </div>
              </div>

              <?php if ($custom_fields): ?>
                <div class="row">
                  <hr class="col-md-11">
                  <?php foreach ($custom_fields as $id => $arr): ?>
                    <?php
                    $cf = $arr['data'];
                    $cf_values = isset($arr['values']) ? $arr['values'] : NULL;
                    ?>
                    <div class="col-md-6">
                      <label><?= $cf->cf_name ?></label>
                      <?php if ($cf->cf_type == 'select' || $cf->cf_type == 'multiple'): ?>
                        <select name="<?= $cf->cf_code ?>[]" class="form-control" <?= $cf->cf_type == 'multiple' ? "multiple" : "" ?>>
                          <?php if ($cf_values): ?>
                            <option value="">Seleccione...</option>
                            <?php foreach ($cf_values as $cfv): ?>
                              <?php
                                  $selected = "";
                                  if ($cf->cf_type == 'multiple') {
                                      $selected = !empty($customer->{$cf->cf_code}) && strpos($customer->{$cf->cf_code}, $cfv->cf_value) !== false ? "selected" : "";
                                  } else {
                                      $selected = $customer->{$cf->cf_code} == $cfv->cf_value ? "selected" : "";
                                  }
                              ?>
                              <option value="<?= $cfv->cf_value ?>" <?= $selected ?>><?= $cfv->cf_value ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                            <option value="">Sin valores para el campo</option>
                          <?php endif ?>
                        </select>
                      <?php endif ?>
                      <?php if ($cf->cf_type == 'date' || $cf->cf_type == 'text' || $cf->cf_type == 'input'): ?>
                        <input type="<?= $cf->cf_type ?>" name="<?= $cf->cf_code ?>" class="form-control" <?= $customer->{$cf->cf_code} ? "value='".$customer->{$cf->cf_code}."'" : "" ?>>
                      <?php endif ?>
                    </div>
                  <?php endforeach ?>
                </div>
              <?php endif ?>

              <div class="row">
                <hr class="col-md-11">

                <!-- <div class="form-group col-md-6">
                  <label><?= lang('auto_retainer') ?></label><br>
                  <label>
                      <input type="checkbox" name="fuente_retainer" id="fuente_retainer" <?= $customer->fuente_retainer ? 'checked="checked"' : '' ?>>
                      <?= lang('fuente') ?>
                  </label>
                  <label>
                      <input type="checkbox" name="iva_retainer" id="iva_retainer" <?= $customer->iva_retainer ? 'checked="checked"' : '' ?>>
                      <?= lang('iva') ?>
                  </label>
                  <label>
                      <input type="checkbox" name="ica_retainer" id="ica_retainer" <?= $customer->ica_retainer ? 'checked="checked"' : '' ?>>
                      <?= lang('ica') ?>
                  </label>
                </div> -->
                <div class="form-group col-md-6">
                    <label>
                        <input type="checkbox" name="customer_validate_min_base_retention" id="customer_validate_min_base_retention"  <?= $customer->customer_validate_min_base_retention ? 'checked="checked"' : '' ?>>
                        <?= lang('customer_validate_min_base_retention') ?>
                    </label>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12">
                  <table class="table">
                    <thead>
                      <tr>
                        <th><?= lang('retention') ?></th>
                        <th><?= lang('percentage') ?></th>
                        <th><?= lang('min_base') ?></th>
                        <th><?= lang('ledger') ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (count($retentions) > 0): ?>
                        <?php if (isset($retentions['FUENTE'])): ?>
                          <tr class="row_rete_fuente" <?= $customer->fuente_retainer ? 'style="display:none;"' : '' ?>>
                            <td>
                              <select name="default_rete_fuente_id" id="default_rete_fuente_id" class="form-control">
                                <option value=""><?= lang('no_rete_fuente_default') ?></option>
                                <?php foreach ($retentions['FUENTE'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>" <?= $customer->default_rete_fuente_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_fuente_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['IVA'])): ?>
                          <tr class="row_rete_iva" <?= $customer->iva_retainer ? 'style="display:none;"' : '' ?>>
                            <td>
                              <select name="default_rete_iva_id" id="default_rete_iva_id" class="form-control">
                                <option value=""><?= lang('no_rete_iva_default') ?></option>
                                <?php foreach ($retentions['IVA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>" <?= $customer->default_rete_iva_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_iva_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['ICA'])): ?>
                          <tr class="row_rete_ica" <?= $customer->ica_retainer ? 'style="display:none;"' : '' ?>>
                            <td>
                              <select name="default_rete_ica_id" id="default_rete_ica_id" class="form-control">
                                <option value=""><?= lang('no_rete_ica_default') ?></option>
                                <?php foreach ($retentions['ICA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>" <?= $customer->default_rete_ica_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_ica_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                        <?php if (isset($retentions['OTRA'])): ?>
                          <tr class="row_rete_other">
                            <td>
                              <select name="default_rete_other_id" id="default_rete_other_id" class="form-control">
                                <option value=""><?= lang('no_rete_other_default') ?></option>
                                <?php foreach ($retentions['OTRA'] as $row): ?>
                                  <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>" <?= $customer->default_rete_other_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                <?php endforeach ?>
                              </select>
                            </td>
                            <td>
                              <input type="text" class="rete_other_percentage form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_other_minbase form-control" readonly>
                            </td>
                            <td>
                              <input type="text" class="rete_other_ledger form-control" readonly>
                            </td>
                          </tr>
                        <?php endif ?>
                      <?php endif ?>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="row">
                  <div class="form-group col-sm-3 text-center">
                    <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 200px;">
                      <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                        <img alt="User Image" src="<?= base_url() . 'assets/uploads/avatars/' . $customer->customer_profile_photo ?>" style="width: 100% !important;">
                      </div>
                      <div>
                        <span class="btn btn-success btn-file btn-profile-img btn-profile-img-edit">
                          <span class="fileinput-new">
                            <i class="fa fa-pencil"></i>
                          </span>
                        <span class="fileinput-exists">
                          <i class="fa fa-pencil"></i>
                        </span>
                        <input type="file" name="customer_profile_photo" id="userimageupdate" /></span>.
                        <span>Tamaño <?= $this->Settings->iwidth ?> x <?= $this->Settings->iheight ?></span>
                      </div>
                    </div>
                    <label><?= lang('customer_profile_photo') ?></label>
                  </div>
              </div>
              <div class="row">
                  <div class="form-group col-sm-12">
                    <?= lang('note', 'note') ?>
                    <textarea name="note" id="note" class="form-control">
                      <?= $customer->note ?>
                    </textarea>
                  </div>
              </div>
            <div class="modal-footer">
                <input type="hidden" name="edit_customer" value="1">
                <button class="btn btn-success" id="submit_edit_customer" type="button"><?= lang('edit_customer') ?></button>
            </div>
        <?= form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
      $("#edit-customer-form").validate({
          ignore: []
      });
      setTimeout(function() {
        // $('#vat_no').val($('#vat_no').val().replace(/[^0-9]/g,''));
        $('#phone').val($('#phone').val().replace(/[^0-9]/g,''));
      }, 1200);
      $(document).on('click', '#submit_edit_customer', function(){
        form = $('#edit-customer-form');
        if (form.valid()) {
          form.submit();
        }
      });
      set_ubication();
      $('#tipo_documento').on('change', function() { show_hide_verification_digit(); });
      $('#type_person').on('change', function() { show_hide_type_person(); });
      $('#state').on('change', function(){ get_cities(); });
      $('#country').on('change', function(){ get_states(); });
      $('#city').on('change', function() { get_postal_code(); get_zones(); });
      /*NUEVO*/
      $('#zone').on('change', function(){get_subzones()});
      $('#subzone').on('change', function(){   });
      $('#vat_no').on('change', function() { calculate_digit_verification(); });
      <?php if ($types_customer_obligations !== FALSE && ! empty($types_customer_obligations)) { ?> load_types_customer_obligations(); <?php } ?>
      $('#type_person').trigger('change');
      $('#tipo_documento').trigger('change');
      $('#vat_no').trigger('change');
      $('#customer_payment_type').on('change', function(){
        val = $(this).val();
        if (val == 0) {
          $('.credit_customer').css('display', '');
          $('#customer_credit_limit').prop('required', true).prop('min', 1);
          $('#customer_payment_term').prop('required', true).prop('min', 1);
        } else if (val == 1) {
          $('.credit_customer').css('display', 'none');
          $('#customer_credit_limit').prop('required', false).prop('min', false);
          $('#customer_payment_term').prop('required', false).prop('min', false);
        }
      });
      $('#customer_payment_type').trigger('change');
      setTimeout(function() {
        $('#customer_type').trigger('change');
      }, 950);
    });
    function load_types_customer_obligations() {
        var types_customer_obligations_array = [];

        <?php foreach ($types_customer_obligations as $type_customer_obligation) { ?>
            types_customer_obligations_array.push('<?= $type_customer_obligation->types_obligations_id; ?>');
        <?php } ?>

        $('#types_obligations').select2({}).select2('val', types_customer_obligations_array);
    }
    function get_states() {
        dpto = $('#country option:selected').data('code');

        $.ajax({
          url:"<?= admin_url() ?>customers/get_states/"+dpto,
        }).done(function(data) {
          $('#state').html(data);
        }).fail(function(data) {
          console.log(data.responseText);
        });
    }
    function get_cities() {
        dpto = $('#state option:selected').data('code');
        $.ajax({
          url:"<?= admin_url() ?>customers/get_cities/"+dpto,
        }).done(function(data) {
          $('#city').html(data);
        }).fail(function(data) {
          console.log(data.responseText);
        });
    }
    function get_zones(){
      code = $('#city option:selected').data('code');
      $.ajax({
        url:"<?= admin_url().'customers/get_zones/' ?>"+code
      }).done(function(data){
        $('#zone').html(data);
      });
    }
    function get_subzones(){
      code = $('#zone option:selected').data('code');
      $('.postal_code').val(code);

      $.ajax({
        url:"<?= admin_url().'customers/get_subzones/' ?>"+code
      }).done(function(data){
        $('#subzone').html(data);
      });
    }
    $('#subzone').on('change', function(){
      code = $('#subzone option:selected').data('code');
      $('.postal_code').val(code);
    });
    function show_hide_verification_digit() {
      var document_type = $('#tipo_documento').val();

      if (document_type == 6 && site.settings.get_companies_check_digit == 1) {
        $('.digito-verificacion').css('display', 'block');
        $('.digito-verificacion input').attr('required', true);
      } else {
        $('.digito-verificacion').css('display', 'none');
        $('.digito-verificacion input').attr('required', false);
      }

      $('input[name="name"]').val('<?= $customer->name ?>');
      $('input[name="commercial_register"]').val('<?= $customer->commercial_register ?>');
      $('input[name="document_code"]').val($('#tipo_documento option:selected').data('code'));
    }
    function show_hide_type_person() {
      type_person = $('#type_person').val();
      if (type_person == '<?= NATURAL_PERSON; ?>') {
        $('input[name="first_name"]').attr('required', true);
        $('input[name="first_lastname"]').attr('required', true);
        $('.natural_person').css('display', 'block');
        $.each($('.natural_person').find('.input_required'), function(){
          $(this).attr('required', true);
        });
        $('.juridical_person').css('display', 'none');
        $('#name').val('-');
        $.each($('.juridical_person').find('.input_required'), function(){
          $(this).attr('required', false);
        });
        // Elimina los atributos requeridos.
        $('input[name="name"]').removeAttr('required');
        // $('input[name="commercial_register"]').removeAttr('required');
        // Iteración para remover los atributos "disabled" de todas las opciones de tipos de documento.
        $('#tipo_documento option').each(function()
        {
          $(this).removeAttr('disabled');
        });
        $('#tipo_documento').select2('val', '');
        $('#tipo_documento').trigger('change');
        $('input[name="document_code"]').val('');
        // Iteración para remover los atributos "disabled" de todas las opciones de tipo de régimen.
        // $('#type_vat_regime option').each(function(){
        //   $(this).removeAttr('disabled');
        // });
        $('#type_vat_regime').select2('val', '');
      } else if (type_person == '<?= LEGAL_PERSON; ?>') {
        $('.natural_person').css('display', 'none');
        $('.juridical_person').css('display', 'block');
        $('#name').val('<?= $customer->name ?>');
        $.each($('.natural_person').find('.input_required'), function(){
          $(this).attr('required', false);
        });
        $.each($('.juridical_person').find('.input_required'), function(){
          $(this).attr('required', true);
        });
        // Iteración para seleccionar el Nit extrictamente y desabilitar las demás opciones.
        $('#tipo_documento option').each(function() {
          if ($(this).data('code') == 31) {
            document_type_id = $(this).val();
            document_code = $(this).data('code');
          }
        });
        $('#tipo_documento').select2('val', document_type_id);
        $('#tipo_documento').trigger('change');
        $('input[name="document_code"]').val(document_code);
        // Iteración para seleccionar el régimen común y deshabilitar las demás opciones.
        // $('#type_vat_regime option').each(function(){
        //   if ($(this).val() != '<?= COMMON ?>') {
        //     $(this).prop('disabled', 'disabled');
        //   }
        // });
        $('#type_vat_regime').select2('val', <?= COMMON; ?>);
      }
      $("#edit-customer-form").validate({
        ignore: []
      });
    }

    function get_postal_code() {
        code_country = $('#country option:selected').data('code');
        code = $('#city option:selected').data('code');
        postal_code = '';
        if (code) {
          postal_code = code.toString().replace(code_country, '');
        }
        $('.postal_code').val(postal_code);
        $('#city_code').val(code);
    }

    function calculate_digit_verification() {
        nit = $('#vat_no').val();
        if (site.settings.get_companies_check_digit == 1) {
            dvf = calcularDigitoVerificacion(nit);
            $('#digito_verificacion').val(dvf);
        }
    }


    function set_ubication(){
      country = $('#country option:selected').data('code');
      state = "<?= $customer->state ?>";
      set_city = "<?= $customer->city ?>";
      $.ajax({
        url:"<?= admin_url() ?>customers/get_states",
        method : "POST",
        data : {
          "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
          country : country,
          state : state
        }
      })
      .done(function(data) {
        $('#state').html(data);
        $('#state').select2();
        state = $('#state option:selected').data('code');

        $.ajax({
          url:"<?= admin_url() ?>customers/get_cities",
          method : "POST",
          data : {
            "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
            state : state,
            city : set_city
          }
        }).done(function(data){
          $('#city').html(data);
          $('#city').select2('val', set_city);

          <?php if (!empty($customer->location)) { ?>

            city = $('#city option:selected').data('code');
            zone = $('#zone option:selected').data('code');

            $.ajax({
              url:"<?= admin_url() ?>customers/get_zones",
              method : "POST",
              data : {
                "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
                state : state,
                city : city
              }
            }).done(function(data){
              $('#zone').html(data).select2().trigger('change');
              <?php if (!empty($customer->subzone)) { ?>
                zone = $('#zone option:selected').data('code');
                $.ajax({
                  url:"<?= admin_url() ?>customers/get_subzones",
                  method : "POST",
                  data : {
                    "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
                    zone : zone,
                    subzone : subzone
                  }
                }).done(function(data){
                  $('#subzone').html(data).select2();
                });
              <?php } ?>
            });
          <?php } ?>

        })
        .fail(function(data){ console.log(data.responseText); });

      }).fail(function(data){ console.log(data.respnoseText); });
    }
    $('#birth_month').on('change', function(){
      var month = $(this).val();
      var days_of_the_month = new Date("<?= date('Y') ?>", month, 0).getDate();
      var options_html = "";
      for (var i = 1; i <= days_of_the_month; i++) {
          options_html += "<option value='"+i+"' "+("<?= $customer->birth_day ?>" == i ? 'selected="selected"' : '')+"  >"+i+"</option>";
      }
      $('#birth_day').html(options_html);
    });
    $('#birth_month').trigger('change');
    $(document).on('ifChecked', '#customer_only_for_pos', function(){
      set_only_pos_required();
    });
    $(document).on('ifUnchecked', '#customer_only_for_pos', function(){
      unset_only_pos_required();
    });
    $(document).on('change', '#customer_type', function(){
      if ($(this).val() == 'lead') {
        set_only_pos_required();
      } else {
        unset_only_pos_required();
      }
    });
    function set_only_pos_required(){
      $('#types_obligations').attr('required', false);
      $('.hide_pos_customer').css('display', 'none');
      $('#email_address').removeAttr('required');
      $('#address').removeAttr('required');
      $('#phone').removeAttr('required');
      $('#type_person').val(2).trigger('change');
      $('#type_vat_regime').val(1).trigger('change');
      $('#tipo_documento').val(3).trigger('change');
      $('#vat_no').focus();
    }
    function unset_only_pos_required(){
      $('#types_obligations').attr('required', true);
      $('.hide_pos_customer').css('display', '');
      $('#email_address').prop('required', 'required');
      $('#address').prop('required', 'required');
      $('#phone').prop('required', 'required');
    }
    $(document).on('change', '#default_rete_fuente_id', function(){
      var rete_fuente = $('#default_rete_fuente_id option:selected');
      var percentage = rete_fuente.data('percentage');
      var min_base = rete_fuente.data('minbase');
      var ledger = rete_fuente.data('account');
      $('.rete_fuente_percentage').val(percentage);
      $('.rete_fuente_minbase').val(min_base);
      $('.rete_fuente_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_iva_id', function(){
      var rete_iva = $('#default_rete_iva_id option:selected');
      var percentage = rete_iva.data('percentage');
      var min_base = rete_iva.data('minbase');
      var ledger = rete_iva.data('account');
      $('.rete_iva_percentage').val(percentage);
      $('.rete_iva_minbase').val(min_base);
      $('.rete_iva_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_ica_id', function(){
      var rete_ica = $('#default_rete_ica_id option:selected');
      var percentage = rete_ica.data('percentage');
      var min_base = rete_ica.data('minbase');
      var ledger = rete_ica.data('account');
      $('.rete_ica_percentage').val(percentage);
      $('.rete_ica_minbase').val(min_base);
      $('.rete_ica_ledger').val(ledger);
    });
    $(document).on('change', '#default_rete_other_id', function(){
      var rete_other = $('#default_rete_other_id option:selected');
      var percentage = rete_other.data('percentage');
      var min_base = rete_other.data('minbase');
      var ledger = rete_other.data('account');
      $('.rete_other_percentage').val(percentage);
      $('.rete_other_minbase').val(min_base);
      $('.rete_other_ledger').val(ledger);
    });
    $(document).on('ifChecked', '#fuente_retainer', function(){
      $('.row_rete_fuente').fadeOut();
      $('#default_rete_fuente_id').select2('val', '').trigger('change');
    });
    $(document).on('ifUnchecked', '#fuente_retainer', function(){
      $('.row_rete_fuente').fadeIn();
    });
    $(document).on('ifChecked', '#iva_retainer', function(){
      $('.row_rete_iva').fadeOut();
      $('#default_rete_iva_id').select2('val', '').trigger('change');
    });
    $(document).on('ifUnchecked', '#iva_retainer', function(){
      $('.row_rete_iva').fadeIn();
    });
    $(document).on('ifChecked', '#ica_retainer', function(){
      $('.row_rete_ica').fadeOut();
      $('#default_rete_ica_id').select2('val', '').trigger('change');
    });
    $(document).on('ifUnchecked', '#ica_retainer', function(){
      $('.row_rete_ica').fadeIn();
    });
    $(document).on('keydown', '#longitude', function(){
      val = $(this).val();
      if (val > 999) {
        Command: toastr.error('Valor ingresado incorrecto', '¡Error!', {onHidden : function(){}})
        $(this).val('');
      }
    });
    $(document).on('keydown', '#latitude', function(){
      val = $(this).val();
      if (val > 999) {
        Command: toastr.error('Valor ingresado incorrecto', '¡Error!', {onHidden : function(){}})
        $(this).val('');
      }
    });
    $('#default_rete_fuente_id').trigger('change');
    $('#default_rete_iva_id').trigger('change');
    $('#default_rete_ica_id').trigger('change');
    $('#default_rete_other_id').trigger('change');
    nscustomer("<?= $customer->customer_employer_id ?>" ? "<?= $customer->customer_employer_id ?>" : null);
    function nscustomer(customer_id = null) {
      if (customer_id) {
        $('#customer').val(customer_id).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#customer').trigger('change');
      } else {
        $('#customer').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
              $.ajax({
                type: "get", async: false,
                url: site.base_url+"customers/getcustomer/" + $(element).val(),
                dataType: "json",
                success: function (data) {
                  callback(data[0]);
                }
              });
            },
            ajax: {
              url: site.base_url+"customers/suggestions",
              dataType: 'json',
              quietMillis: 15,
              data: function (term, page) {
                return {
                  term: term,
                  limit: 10
                };
              },
              results: function (data, page) {
                if (data.results != null) {
                  return {results: data.results};
                } else {
                  return {results: [{id: '', text: lang.no_match_found}]};
                }
              }
            }
        });
      }
    }
$(document).on('change', '#customer', function(){
  $.ajax({
        url: site.base_url+"sales/getCustomerAddresses",
        type: "get",
        data: {"customer_id" : $(this).val()}
    }).done(function(data){
        if (data != false) {
            $('#address_id').html(data);
            if (address_id = localStorage.getItem('address_id')) {
                $('#address_id option').each(function(index, option){
                    if ($(option).val() == address_id) {
                        $(option).prop('selected', 'selected');
                    }
                });
            }
            $('#address_id').select2();
        } else {
            $('#address_id').select2('val', '');
        }
        <?php if ($customer->customer_employer_branch_id): ?>
          $('#address_id').select2('val', '<?= $customer->customer_employer_branch_id ?>');
        <?php endif ?>
    }); //FIN AJAX SUCURSALES
  });
</script>
<?= $modal_js ?>