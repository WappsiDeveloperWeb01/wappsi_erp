<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
if ($this->session->userdata('detal_post_processing')) {
    $this->session->unset_userdata('detal_post_processing');
}
?>
<script type="text/javascript">
    var count = 1, an = 1, DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, surcharge = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>;
    var deposit_advertisement_showed = false;
    var total_rc_rete_amount = parseFloat("<?= $total_rc_rete_amount ?>");
    var start_date = "<?= date('d/m/Y H:i', strtotime($register->date)) ?>";
    <?php if($this->Settings->allow_returns_date_before_current_date == 0) { ?>
        var end_date = "<?= date('d/m/Y H:i') ?>";  // esta opcion es si la fecha va a ser como minimo el día que se esta devolviendo la venta
    <?php } ?>
    <?php if($this->Settings->allow_returns_date_before_current_date == 1) { ?>
        var end_date = "<?= date('d/m/Y H:i', strtotime($inv->date)) ?>"; // esta opcion es si la fecha va a ser como minimo el día que se creo la venta
    <?php } ?>
    var return_type = <?= $return_type ?>;
    var inv_grand_total = parseFloat(<?= $inv->grand_total ?>);
    var inv_order_discount = parseFloat(<?= $inv->order_discount ?>);
    var inv_tip_amount = parseFloat(<?= $inv->tip_amount ?>);
    var amount_discounted = parseFloat(<?= $amount_discounted ?>);
    var is_pos = "<?= $inv->pos ?>";
    var biller_id = "<?= $inv->biller_id ?>";
    var customer_id = "<?= $inv->customer_id ?>";
    var return_sale = true;
    var total_payment_discount = 0;
    var sale_payments = JSON.parse('<?= json_encode(isset($sale_payments) ? $sale_payments : NULL) ?>');
    var paid_by_giftcard = JSON.parse('<?= ($paid_by_giftcard) ? 'true' : 'false' ?>');
    var is_document_electronic = "<?= $invoice_resolution_data->factura_electronica ?>";
    var rete_applied_total = {
            'rete_fuente_total' : "<?= $inv->rete_fuente_total ?>",
            'rete_iva_total' : "<?= $inv->rete_iva_total ?>",
            'rete_ica_total' : "<?= $inv->rete_ica_total ?>",
            'rete_other_total' : "<?= $inv->rete_other_total ?>",
        };
    $(document).ready(function () {
        localStorage.removeItem('reitems');
        localStorage.removeItem('rediscount');
        localStorage.removeItem('reshipping');
        localStorage.removeItem('retax2');
        localStorage.removeItem('reinvoice_paid');
        localStorage.removeItem('repayment_status');
        localStorage.removeItem('resale_balance');
        localStorage.removeItem('re_retenciones');
        <?php if (isset($inv) && $inv) { ?>
            $('#add_return').css('display', 'none');
            command: toastr.warning('Por favor espere unos segundos...', 'Espere...');
            setTimeout(function() {
                $('#add_return').css('display', '');
                command: toastr.success('Ya se completó la carga', 'Completo');
            }, 3000);
            localStorage.setItem('reref', '<?= $reference ?>');
            localStorage.setItem('reitems', JSON.stringify(<?= $inv_items; ?>));
            localStorage.setItem('rediscount', '<?= $inv->order_discount_id ?>');
            localStorage.setItem('reshipping', '<?= $inv->shipping ?>');
            localStorage.setItem('retip_amount', '<?= $inv->tip_amount ?>');
            localStorage.setItem('retax2', '<?= $inv->order_tax_id ?>');
            localStorage.setItem('reinvoice_paid', '<?= $inv->paid ?>');
            localStorage.setItem('repayment_status', '<?= $inv->payment_status ?>');
            localStorage.setItem('resale_balance', '<?= ($inv->grand_total - ($inv->paid - ($inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_bomberil_total + $inv->rete_autoaviso_total + $inv->rete_other_total + $total_rc_rete_amount))) ?>');
            localStorage.setItem('return_surcharge', '0');
            localStorage.removeItem('re_retenciones');
                <?php if ($inv->rete_fuente_id || $inv->rete_iva_id || $inv->rete_ica_id || $inv->rete_other_id) { ?>
                    re_retenciones = {
                                'gtotal' : $('#gtotal').text(),
                                'id_rete_fuente' : '<?= $inv->rete_fuente_id ?>',
                                'id_rete_iva' : '<?= $inv->rete_iva_id ?>',
                                'id_rete_ica' : '<?= $inv->rete_ica_id ?>',
                                'id_rete_otros' : '<?= $inv->rete_other_id ?>',
                                'id_rete_bomberil' : '<?= $inv->rete_bomberil_id ?>',
                                'id_rete_autoaviso' : '<?= $inv->rete_autoaviso_id ?>',
                            };
                    localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));
                <?php } ?>
        <?php } ?>
        <?php if (isset($inv) && $inv): ?>
            $('#gccustomer').val(<?=$inv->customer_id?>).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: "<?= admin_url('customers/getCustomer') ?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        <?php endif ?>

        if (amount_discounted > 0 || total_rc_rete_amount > 0) {
            msg = "Esta factura cuenta con ";
            if (amount_discounted > 0) {
                msg += formatMoney(amount_discounted)+' de descuento, ';
            }
            if (total_rc_rete_amount > 0) {
                msg += formatMoney(total_rc_rete_amount)+' de retenciones posteriores, ';
            }
            msg += ' en los pagos, dicho valor se tiene en cuenta en el pago por la devolución';
            bootbox.alert(msg);
        }

    });
    //DESDE AQUI TRASLADAR
function obtener_documents_types(odt_is_pos = null, odt_biller_id = null, odt_is_document_electronic = null)
{
    document_type = null;
    if (!odt_is_pos) {
        odt_is_pos = is_pos;
    }
    if (!odt_biller_id) {
        odt_biller_id = biller_id;
    }
    if (!odt_is_document_electronic) {
        odt_is_document_electronic = is_document_electronic;
    }
    if (odt_is_pos == 1) {
        document_type = 3;
    } else if (odt_is_pos == 0) {
        document_type = 4;
    }
    if (document_type) {
        $.ajax({
          url:site.base_url+'billers/getBillersDocumentTypes/',
          type:'POST',
          dataType:'JSON',
          data: {
            '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash(); ?>',
            'module': document_type,
            'biller_id': odt_biller_id,
            'is_document_electronic': odt_is_document_electronic
          }
        }).done(function(data){
            if (data) {
                response = data;
                $('#document_type_id').html(response.options).select2();

                $('#document_type_id option').each(function (index, element) {
                    if ($(this).data('fe') != odt_is_document_electronic) {
                        $(this).remove();
                    }
                });
            }

            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        }).fail(function (data) {
            console.log(data.responseText);
        });
    }
    $.ajax({
        url:'<?= admin_url("billers/getBillersDocumentTypes/31/") ?>'+odt_biller_id,
        type:'get',
        dataType:'JSON'
    }).done(function(data){
        response = data;
        $('#payment_reference_no').html(response.options).select2();
        if (response.not_parametrized != "") {
            command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
        }
        if (response.status == 0) {

        }
      $('#payment_reference_no').trigger('change');
    });
}
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-12">
                    <?=admin_form_open_multipart("sales/return_sale/" . (isset($inv) ? $inv->id : NULL), array('role' => 'form', 'class' => 'return_sale_form')); ?>
                    <input type="hidden" name="id" id="sale_id">
                    <input type="hidden" name="tip_amount" id="tip_amount">
                    <input type="hidden" name="paid_by_giftcard" id="paid_by_giftcard" value="<?= $paid_by_giftcard ? 1 : 0 ?>">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php if ($Owner || $Admin) { ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("date", "redate"); ?>
                                        <input type="datetime" id="redate" name="date" class="form-control">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                                $bl[""] = "";
                                $bldata = [];
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    $bldata[$biller->id] = $biller;
                                }
                            ?>
                            <?php if (!isset($inv)): ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "rebiller"); ?>
                                            <select name="biller" class="form-control" id="rebiller" required="required">
                                                <?php foreach ($billers as $biller): ?>
                                                  <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4" style="display: none;">
                                        <div class="form-group">
                                            <?= lang("biller", "rebiller"); ?>
                                            <select name="biller" class="form-control" id="rebiller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("reference_invoice", "reference_invoice"); ?>
                                        <input type="text" name="reference_invoice" id="reference_invoice" class="form-control" required>
                                    </div>
                                </div>
                            <?php else: ?>
                                <input type="hidden" name="biller_id" id="rebiller" value="<?= $inv->biller_id ?>">
                                <div class="col-md-4" style="display: none;">
                                    <div class="form-group">
                                        <?= lang("biller", "rebiller"); ?>
                                        <select name="biller" class="form-control" id="rebiller">
                                            <?php $biller = $bldata[$inv->biller_id]; ?>
                                                <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        </select>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("reference_no", "document_type_id"); ?>
                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                </div>
                            </div>
                            <!-- <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("return_surcharge", "return_surcharge"); ?>
                                    <?php echo form_input('return_surcharge', (isset($_POST['return_surcharge']) ? $_POST['return_surcharge'] : ''), 'class="form-control input-tip" id="return_surcharge" required="required"'); ?>
                                </div>
                            </div> -->
                            <?php if (isset($cost_centers)): ?>
                                <div class="col-md-4 form-group">
                                    <?= lang('cost_center', 'cost_center_id') ?>
                                    <?php
                                    $ccopts[''] = lang('select');
                                    if ($cost_centers) {
                                        foreach ($cost_centers as $cost_center) {
                                            $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                        }
                                    }
                                     ?>
                                     <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                </div>
                            <?php endif ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("document", "document") ?>
                                    <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                           data-show-preview="false" class="form-control file">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("affects", "affects") ?>
                                    <input id="affects" type="text"name="affects" class="form-control" value="<?= isset($inv) ? $inv->reference_no : '' ?>" readonly>
                                </div>
                            </div>
                            <?php if (count($currencies) > 1) : ?>
                                <div class="col-md-4 row">
                                    <div class="col-md-6 form-group">
                                        <?= lang('currency', 'currency') ?>
                                        <select name="currency" class="form-control" id="currency">
                                            <?php foreach ($currencies as $currency) : ?>
                                                <option value="<?= $currency->code ?>" data-rate="<?= $currency->rate ?>" <?= $currency->code == $inv->sale_currency ? "selected='selected'" : "" ?>><?= $currency->code ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group trm-control" <?= $inv->sale_currency != $this->Settings->default_currency ? "selected='selected'" : 'style="display: none;"' ?> >
                                        <?= lang('trm', 'trm') ?>
                                        <input type="number" name="trm" id="trm" class="form-control"  value="<?= $inv->sale_currency_trm ?>" readonly>
                                    </div>
                                </div>
                            <?php else : ?>
                                <input type="hidden" name="currency" value="<?= $this->Settings->default_currency; ?>" id="currency">
                            <?php endif ?>
                            <div class="col-sm-4">
                                <!-- <label onclick="$('#rtModal').modal('show');" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención</label> -->
                                <label>Retención</label>
                                <input type="text" name="retencion" id="rete" class="form-control text-right" readonly>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="col-sm-4">
                                <label>Cliente</label>
                                <input type="text" name="" value="<?= $customer_details->company_text ?>" class="form-control" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Sucursal cliente</label>
                                <input type="text" name="" value="<?= $address_details->name ?>" class="form-control" readonly>
                            </div>
                            <div class="col-sm-4 div_order_discount" style="display:none;">
                                <label>Aplicar descuento de orden Original a devolución </label>
                                <select class="form-control" name="apply_return_order_discount" id="apply_return_order_discount">
                                    <option value="0"><?= lang('no') ?></option>
                                    <option value="1" selected><?= lang('yes') ?></option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group table-group">
                                    <label class="table-label"><?= lang("order_items"); ?></label> (<?= lang('return_tip'); ?>)
                                    <div class="controls table-controls">
                                        <table id="reTable"
                                               class="table items  table-bordered table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-4"><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                                                    <?php
                                                    if ($Settings->product_serial || $Settings->product_variant_per_serial == 1) {
                                                        echo '<th class="col-md-2">' . lang("serial_no") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang('gross_net_unit_price') ?></th>
                                                    <?php
                                                    if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {
                                                        echo '<th class="col-md-1">' . lang("discount") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang("price_x_discount"); ?></th>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        echo '<th class="col-md-1">' . lang("product_tax") . '</th>';
                                                    }
                                                        if ($Settings->ipoconsumo == 3 || $Settings->ipoconsumo == 1) {
                                                            echo '<th class="col-md-1">' . lang("second_product_tax") . '</th>';
                                                        }
                                                    ?>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        echo '<th class="col-md-1">' . lang("price_x_tax") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang("available"); ?></th>
                                                    <th class="col-md-1"><?= lang("to_return"); ?></th>
                                                    <!-- <th>
                                                        <?= lang("subtotal"); ?>
                                                        (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th> -->
                                                    <th>
                                                        <?= lang("total"); ?>
                                                        (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th>
                                                    <th style="width: 30px !important; text-align: center;">
                                                        <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                        <tr class="warning">
                                            <td>
                                                <?= lang('items') ?>
                                                <span class="totals_val pull-right" id="titems">0</span>
                                            </td>
                                            <td>
                                                <?= lang('total') ?>
                                                <span class="totals_val pull-right" id="total">0.00</span>
                                            </td>
                                            <?php if ($Settings->tax1) { ?>
                                            <td>
                                                <?= lang('product_tax') ?>
                                                <span class="totals_val pull-right" id="ttax1">0.00</span>
                                            </td>
                                            <?php } ?>
                                            <td>
                                                <?= lang('surcharges') ?>
                                                <span class="totals_val pull-right" id="trs">0.00</span>
                                            </td>
                                            <?php if ($this->Settings->ipoconsumo): ?>
                                                <td>
                                                    <?= lang('second_product_tax') ?>
                                                    <span class="totals_val pull-right" id="ipoconsumo_total">0.00</span>
                                                </td>
                                            <?php endif ?>
                                            <?php if ($Settings->tax2) { ?>
                                            <td>
                                                <?= lang('order_tax') ?>
                                                <span class="totals_val pull-right" id="ttax2">0.00</span>
                                            </td>
                                            <?php } ?>
                                            <td>
                                                <?= lang('return_amount') ?>
                                                <span class="totals_val pull-right" id="gtotal">0.00</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="height:15px; clear: both;"></div>
                            <!-- <div class="col-md-12">
                                <?php
                                if (isset($inv)) {
                                    if ($inv->payment_status == 'paid') {
                                        echo '<div class="alert alert-success">' . lang('payment_status') . ': <strong>' . $inv->payment_status . '</strong> & ' . lang('paid_amount') . ' <strong>' . $this->sma->formatMoney($inv->paid) . '</strong></div>';
                                    } else {
                                        echo '<div class="alert alert-warning">' . lang('payment_status_not_paid') . ' ' . lang('payment_status') . ': <strong>' . $inv->payment_status . '</strong> & ' . lang('paid_amount') . ' <strong>' . $this->sma->formatMoney($inv->paid) . '</strong></div>';
                                    }
                                }
                                ?>
                            </div> -->
                            <?php if ( ( $Owner || $Admin || $GP['sales-payments'] ) && $allow_payment) { ?>
                                <div id="payments">
                                    <hr>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                            <select name="payment_reference_no" class="form-control" id="payment_reference_no" required="required"></select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="payments_div">
                                        <div class="col-md-12 pay_option">
                                            <div class="well well-sm well_1">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-4 deposit_div_1" style="display:none;">
                                                            <?= lang('deposit_document_type_id', 'deposit_document_type_id') ?>
                                                            <select name="deposit_document_type_id[]" class="form-control deposit_document_type_id" data-pbnum="1">
                                                                <option value="">Seleccione</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <?= lang("paying_by", "paid_by_1"); ?>
                                                                <select name="paid_by[]" id="paid_by_1" data-pbnum="1" class="form-control paid_by">
                                                                    <?= $this->sma->paid_opts(null, false, true); ?>
                                                                </select>
                                                                <input type="hidden" name="due_payment[]" class="due_payment_1">
                                                            </div>
                                                            <input type="hidden" name="mean_payment_code_fe[]" class="mean_payment_code_fe">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="payment">
                                                                <input type="hidden" name="amount-discounted" id="discounted"/>
                                                                <div class="form-group ngc_1">
                                                                    <?= lang("value", "amount_1"); ?>
                                                                    <input type="text" id="trm_amount_1" data-num="1" class="pa form-control kb-pad trm_amount only_number" style="display:none;" />
                                                                    <input name="amount-paid[]" type="text" id="amount_1" class="pa form-control kb-pad amount only_number" data-num="1"/>
                                                                </div>
                                                                <div class="form-group gc_1" style="display: none;">
                                                                    <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                    <input name="gift_card_no[]" type="text" id="gift_card_no_1"
                                                                        class="pa form-control gift_card_no kb-pad"/>
                                                                    <div id="gc_details_1"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 div_slpayment_term_1" style="display:none;">
                                                            <div class="form-group">
                                                                <?= lang("payment_term", "slpayment_term_1"); ?>
                                                                <?php echo form_input('payment_term[]', '', 'class="form-control tip sale_payment_term" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="slpayment_term_1"'); ?>
                                                                <input type="hidden" name="customerpaymentterm" id="customerpaymentterm">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="pcc_1" style="display:none;">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_no[]" type="text" id="pcc_no_1"
                                                                        class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_holder[]" type="text" id="pcc_holder_1"
                                                                        class="form-control"
                                                                        placeholder="<?= lang('cc_holder') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <select name="pcc_type[]" id="pcc_type_1"
                                                                            class="form-control pcc_type"
                                                                            placeholder="<?= lang('card_type') ?>">
                                                                        <option value="Visa"><?= lang("Visa"); ?></option>
                                                                        <option
                                                                            value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                        <option value="Amex"><?= lang("Amex"); ?></option>
                                                                        <option value="Discover"><?= lang("Discover"); ?></option>
                                                                    </select>
                                                                    <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_month[]" type="text" id="pcc_month_1"
                                                                        class="form-control" placeholder="<?= lang('month') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <input name="pcc_year[]" type="text" id="pcc_year_1"
                                                                        class="form-control" placeholder="<?= lang('year') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <input name="pcc_ccv[]" type="text" id="pcc_cvv2_1"
                                                                        class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pcheque_1" style="display:none;">
                                                        <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                            <input name="cheque_no[]" type="text" id="cheque_no_1"
                                                                class="form-control cheque_no"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-bottom: 4%">
                                        <button type="button" class="btn btn-primary" id="add_more_payments"><?= lang('add_more_payments') ?></button>
                                    </div>
                                </div>
                            <?php } ?>
                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                        <input type="hidden" name="order_tax" value="" id="retax2" required="required"/>
                        <input type="hidden" name="order_discount" value="" id="rediscount" required="required"/>
                        <input type="hidden" name="order_discount_not_applied" value="" id="rediscount_not_applied" required="required"/>
                            <?php if ($discounts): ?>
                                <?php foreach ($discounts as $discount): ?>
                                    <input type="hidden" name="discount_amount[]" value="<?= $discount['amount'] ?>" data-originalamount="<?= $discount['amount'] ?>">
                                    <input type="hidden" name="discount_ledger_id[]" value="<?= $discount['ledger_id'] ?>">
                                    <input type="hidden" name="discount_description[]" value="<?= $discount['description'] ?>">
                                <?php endforeach ?>
                            <?php endif ?>
                            <?php if ($rc_retentions): ?>
                                <?php foreach ($rc_retentions as $rc_reference => $rc_rete_type): ?>
                                    <?php foreach ($rc_rete_type as $rc_type => $rc_retention): ?>
                                        <input type="hidden" name="rc_retention_type[<?= $rc_reference ?>][]" value="<?= $rc_type ?>">
                                        <input type="hidden" name="rc_retention_total[<?= $rc_reference ?>][]" value="<?= $rc_retention['total'] ?>">
                                        <input type="hidden" name="rc_retention_base[<?= $rc_reference ?>][]" value="<?= $rc_retention['base'] ?>">
                                        <input type="hidden" name="rc_retention_account[<?= $rc_reference ?>][]" value="<?= $rc_retention['account'] ?>">
                                        <input type="hidden" name="rc_retention_percentage[<?= $rc_reference ?>][]" value="<?= $rc_retention['percentage'] ?>">
                                    <?php endforeach ?>
                                <?php endforeach ?>
                            <?php endif ?>
                            <div class="row" id="bt">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("return_note", "renote"); ?>
                                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="renote" style="margin-top: 10px; height: 100px;"'); ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="fprom-group">
                                    <button type="button" class="btn btn-primary" id="add_return"  style="padding: 6px 15px; margin:15px 0;"><?= $this->lang->line("submit") ?></button>
                                    <button  type="button"  class="btn btn-danger" onclick="location.href='<?= admin_url('sales') ?>';"><?= lang('cancel') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- retencion -->
                    <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                  class="fa fa-2x">&times;</i></button>
                                  <h4 class="modal-title" id="rtModalLabel"><?=lang('apply_order_retention');?></h4>
                                </div>
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-sm-2">
                                      <label>Retención</label>
                                    </div>
                                    <div class="col-sm-5">
                                      <label>Opción</label>
                                    </div>
                                    <div class="col-sm-2">
                                      <label>Porcentaje</label>
                                    </div>
                                    <div class="col-sm-3">
                                      <label>Valor</label>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-2">
                                      <label>
                                        <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                                      </label>
                                    </div>
                                    <div class="col-sm-5">
                                      <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                                        <option>Seleccione...</option>
                                      </select>
                                      <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                      <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                      <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                      <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right" readonly>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-2">
                                      <label>
                                        <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                                      </label>
                                    </div>
                                    <div class="col-sm-5">
                                      <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                                        <option>Seleccione...</option>
                                      </select>
                                      <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                      <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                      <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                      <input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right" readonly>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-2">
                                      <label>
                                        <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                                      </label>
                                    </div>
                                    <div class="col-sm-5">
                                      <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                                        <option>Seleccione...</option>
                                      </select>
                                      <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                      <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                      <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                      <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right" readonly>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-2">
                                      <label>
                                        <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                                      </label>
                                    </div>
                                    <div class="col-sm-5">
                                      <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                                        <option>Seleccione...</option>
                                      </select>
                                      <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                      <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                      <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                      <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right" readonly>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-2">
                                      <label>
                                        <input type="checkbox" name="rete_bomberil" class="skip" id="rete_bomberil" disabled> <?= lang('rete_bomberil') ?>
                                      </label>
                                    </div>
                                    <div class="col-sm-5">
                                      <select class="form-control" name="rete_bomberil_option" id="rete_bomberil_option" disabled='true'>
                                        <option>Seleccione...</option>
                                      </select>
                                      <input type="hidden" name="rete_bomberil_account" id="rete_bomberil_account">
                                      <input type="hidden" name="rete_bomberil_base" id="rete_bomberil_base">
                                      <input type="hidden" name="rete_bomberil_id" id="rete_bomberil_id">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" name="rete_bomberil_tax" id="rete_bomberil_tax" class="form-control" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                      <input type="text" name="rete_bomberil_valor" id="rete_bomberil_valor" class="form-control text-right" readonly>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-2">
                                      <label>
                                        <input type="checkbox" name="rete_autoaviso" class="skip" id="rete_autoaviso" disabled> <?= lang('rete_autoaviso') ?>
                                      </label>
                                    </div>
                                    <div class="col-sm-5">
                                      <select class="form-control" name="rete_autoaviso_option" id="rete_autoaviso_option" disabled='true'>
                                        <option>Seleccione...</option>
                                      </select>
                                      <input type="hidden" name="rete_autoaviso_account" id="rete_autoaviso_account">
                                      <input type="hidden" name="rete_autoaviso_base" id="rete_autoaviso_base">
                                      <input type="hidden" name="rete_autoaviso_id" id="rete_autoaviso_id">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" name="rete_autoaviso_tax" id="rete_autoaviso_tax" class="form-control" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                      <input type="text" name="rete_autoaviso_valor" id="rete_autoaviso_valor" class="form-control text-right" readonly>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-8 text-right">
                                      <label>Total : </label>
                                    </div>
                                    <div class="col-md-4 text-right">
                                      <label id="total_rete_amount"> 0.00 </label>
                                    </div>
                                    <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                                  </div>
                                  <!-- <div class="form-group">
                                    <?=lang("order_tax", "order_tax_input");?>
                                    <?php
                                    $tr[""] = "";
                                    foreach ($tax_rates as $tax) {
                                      $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                                    ?>
                                  </div> -->
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
                                  <button type="button" id="cancelOrderRete" class="btn btn-danger" data-dismiss="modal"><?= lang('reset') ?></button>
                                  <button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- retencion -->
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '#add_return', function(e){
        e.preventDefault(); // Previene comportamiento por defecto si es necesario
        const button = $(this);
        button.prop('disabled', true);
        button.fadeOut();
        $('#add_return').fadeOut();
        if ($('.return_sale_form').valid()) {
            $('.return_sale_form').submit();
            if ($('#document_type_id option:selected').data('module') == 3) {
                if ($('#document_type_id option:selected').data('fe') == 1) {
                    setTimeout(function() {
                        location.href = site.base_url+'pos/fe_index';
                    }, 850); 
                }else{
                    setTimeout(function() {
                        location.href = site.base_url+'pos/sales';
                    }, 850); 
                }
            }
        } else {
            button.fadeIn();
            $('#add_return').fadeIn();
        }
    })
    $(document).on('change', '#document_type_id', function(){
        if ($('#document_type_id option:selected').data('module') == 3) {
            $('.return_sale_form').prop('target', '_blank');
        } else {
            $('.return_sale_form').removeAttr('target');
        }
    });
</script>