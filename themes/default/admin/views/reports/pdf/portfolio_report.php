<?php
/*
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'vendor/fpdf/fpdf.php';
*/
require_once APPPATH.'../vendor/fpdf/fpdf.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {     
        $this->SetFont('Arial','B',12);
        $this->Image($this->Logo,18,8,30,0);
        $this->Cell(100);        
        $this->Cell(30,4,'ANALISIS DE CARTERA POR VENCIMIENTOS',0,0,'C');
        $this->Ln();
        $this->Cell(37); 
        $this->SetFont('Arial','',6);
        $this->Cell(0,4,'Fecha : '.date('d-m-Y h:i A'),0,0,'C');
        $this->Ln(8);
    }

    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica '.'            Usuario: '.$this->User),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }

}

$fuente = 6;
$adicional_fuente = 2;

$height = 5;

$pdf = new PDF('P', 'mm', array(216, 279));
//$pdf->Logo = base_url('assets/uploads/logos/'.$Settings->logo);
$pdf->Logo = 'https://static.wixstatic.com/media/a560e4_0525ed9cae7e4d42bd1ac3c8a7851bca~mv2.png';

$pdf->AliasNbPages();

$pdf->User = $this->session->userdata('username');

$pdf->AddPage();
$pdf->SetFont('Arial','B',$fuente);

$pdf->Cell(40,8, 'DOCUMENTO','TB',0,'C');
$pdf->Cell(20,8, 'FECHA','TB',0,'C');
$pdf->Cell(10,8, 'DIAS','TB',0,'C');
$pdf->SetXY($pdf->GetX(),$pdf->GetY()); 
$pdf->MultiCell(20,4, 'SALDO '.chr(10).' 0 - 30','TB','C');
$pdf->SetXY($pdf->GetX()+90,$pdf->GetY()-8); 
$pdf->MultiCell(20,4, 'SALDO '.chr(10).' 31 - 60','TB','C');
$pdf->SetXY($pdf->GetX()+110,$pdf->GetY()-8); 
$pdf->MultiCell(20,4, 'SALDO '.chr(10).' 61 - 90','TB','C');
$pdf->SetXY($pdf->GetX()+130,$pdf->GetY()-8); 
$pdf->MultiCell(20,4, 'SALDO '.chr(10).' 91 - 120','TB','C');
$pdf->SetXY($pdf->GetX()+150,$pdf->GetY()-8); 
$pdf->MultiCell(20,4, 'SALDO '.chr(10).' 121 o mas','TB','C');
$pdf->SetXY($pdf->GetX()+170,$pdf->GetY()-8); 
$pdf->Cell(26,8, 'SALDO TOTAL','TB',0,'C');
$pdf->Ln();

$nit_old = $sucursal_old = $vendedor_old =  '' ; 
$total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;
$total_vendedor = $total_30_vendedor = $total_60_vendedor = $total_90_vendedor = $total_120_vendedor =  $total_mas_vendedor = 0;
$saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';
$total_sucursal = $total_30_sucursal = $total_60_sucursal = $total_90_sucursal = $total_120_sucursal =  $total_mas_sucursal = 0;         

foreach ($sales as $sale) 
{
    $Referencia = $sale['Referencia'];
    $Fecha_Creacion = substr($sale['Fecha_Creacion'],0,10);
    $val_pago = number_format($sale['val_pago'],0,'','.'); 
    $Saldo = number_format($sale['Saldo'],0,'','.'); 

    if ($sucursal_old == '' || $sale['cod_Sucursal'] != $sucursal_old)
    {   
        if ($total > 0) 
        {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(70,$height+1, 'SubTotal Cliente' ,0,0,'R');
            $pdf->SetFont('Arial','',$fuente);
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_30,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_60,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_90,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_120,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_mas,0,'','.'),'T',0,'R');
            $pdf->Cell(26,$height+1, $this->sma->formatMoney($total,0,'','.') ,'T',0,'R');
            $pdf->Ln();
            $total = $total_30 = $total_60 = $total_90 = $total_120 = $total_mas = 0;
        }
        if ($total_vendedor > 0)
        {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(70,$height+1, 'SubTotal Vendedor' ,0,0,'R');
            $pdf->SetFont('Arial','',$fuente);
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_30_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_60_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_90_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_120_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_mas_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(26,$height+1, $this->sma->formatMoney($total_vendedor,0,'','.') ,'T',0,'R');
            $pdf->Ln(); 
            $total_vendedor = $total_30_vendedor = $total_60_vendedor = $total_90_vendedor = $total_120_vendedor =  $total_mas_vendedor = 0;
        }

        if ($total_sucursal > 0) {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(70,$height+1, 'SubTotal Sucursal' ,'TB',0,'R');
            $pdf->SetFont('Arial','',$fuente);
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_30_sucursal,0,'','.'),'TBT',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_60_sucursal,0,'','.'),'TB',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_90_sucursal,0,'','.'),'TB',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_120_sucursal,0,'','.'),'TB',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_mas_sucursal,0,'','.'),'TB',0,'R');
            $pdf->Cell(26,$height+1, $this->sma->formatMoney($total_sucursal,0,'','.') ,'TB',0,'R');
            $pdf->Ln();
            $total_sucursal = $total_30_sucursal = $total_60_sucursal = $total_90_sucursal = $total_120_sucursal =  $total_mas_sucursal = 0;
        }
        
        $pdf->SetFillColor(245,245,245);
        $pdf->SetFont('Arial','B',$fuente+2);
        $pdf->Cell(0,$height+2, 'Sucursal: '.$sale['Sucursal'] ,'T',0,'L');
        $pdf->Ln();
        $sucursal_old = $sale['cod_Sucursal']; 
        $pdf->SetFont('Arial','',$fuente);
        $nit_old = '';
        $vendedor_old = '';
    }

    if ($vendedor_old == '' || $sale['cod_seller'] != $vendedor_old)
    {
        if ($total > 0)
        {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(70,$height+1, 'SubTotal Cliente' ,0,0,'R');
            $pdf->SetFont('Arial','',$fuente);
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_30,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_60,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_90,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_120,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_mas,0,'','.'),'T',0,'R');
            $pdf->Cell(26,$height+1, $this->sma->formatMoney($total,0,'','.') ,'T',0,'R');
            $pdf->Ln();
            $total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;  
        }
        if ($total_vendedor > 0) {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(70,$height+1, 'SubTotal Vendedor' ,0,0,'R');
            $pdf->SetFont('Arial','',$fuente);
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_30_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_60_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_90_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_120_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height+1, $this->sma->formatMoney($total_mas_vendedor,0,'','.'),'T',0,'R');
            $pdf->Cell(26,$height+1, $this->sma->formatMoney($total_vendedor,0,'','.') ,'T',0,'R');
            $pdf->Ln(); 
            $total_vendedor = $total_30_vendedor = $total_60_vendedor = $total_90_vendedor = $total_120_vendedor =  $total_mas_vendedor = 0;
        }

        $pdf->SetFont('Arial','B',$fuente+2);
        $pdf->Cell(0,$height+2, 'Vendedor: '.$this->sma->utf8Decode($sale['nom_seller']) ,'T',0,'L');
        $pdf->Ln();
        $vendedor_old = $sale['cod_seller']; 
        $nit_old = '';
    }
    if ($nit_old == '' || $sale['documento'] != $nit_old)
    {
        if($total > 0)
        {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(70,$height, 'SubTotal Cliente' ,0,0,'R');
            $pdf->SetFont('Arial','',$fuente);
            $pdf->Cell(20,$height, $this->sma->formatMoney($total_30,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height, $this->sma->formatMoney($total_60,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height, $this->sma->formatMoney($total_90,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height, $this->sma->formatMoney($total_120,0,'','.'),'T',0,'R');
            $pdf->Cell(20,$height, $this->sma->formatMoney($total_mas,0,'','.'),'T',0,'R');
            $pdf->Cell(26,$height, $this->sma->formatMoney($total,0,'','.') ,'T',0,'R');
            $pdf->Ln();   
            $total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0; 
        }   
        $pdf->SetFont('Arial','B',$fuente+2);
        $pdf->Cell(100,$height+1,'** CLIENTE: '.$this->sma->utf8Decode($sale['nom_companie']),0,0,'L');
        $pdf->Cell(30,$height+1, 'NIT: '.$sale['documento'] ,0,0,'L');
        $pdf->Cell(42,$height+1, 'TEL.: '.$sale['phone'] ,0,0,'L');
        $pdf->Cell(24,$height+1, $this->sma->utf8Decode($sale['city']),0,0,'L');
        $pdf->Ln();
        $nit_old = $sale['documento'];  
    }

    $pdf->SetFont('Arial','',$fuente);
    $fecha = substr($sale['due_date'],0,10);
    $fecha_now = date('Y-m-d');
    $dias   = (strtotime($fecha)-strtotime($fecha_now))/86400;
    $dias   = abs($dias); 
    $dias   = floor($dias);
    $Saldo = $this->sma->formatMoney($sale['Saldo'],0,'','.');
    $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 = $saldo_mas = 0;
    if ($dias <=30) 
    {
        $saldo_30 = $Saldo;
        $total_30 += $sale['Saldo']; 
        $total_30_vendedor += $sale['Saldo']; 
        $total_30_sucursal += $sale['Saldo'];
    }
    else if ($dias <=60)
    {
        $saldo_60 = $Saldo;
        $total_60 += $sale['Saldo']; 
        $total_60_vendedor += $sale['Saldo']; 
        $total_60_sucursal += $sale['Saldo'];
    }
    else if ($dias <=90)
    {
        $saldo_90 = $Saldo;
        $total_90 += $sale['Saldo']; 
        $total_90_vendedor += $sale['Saldo']; 
        $total_90_sucursal += $sale['Saldo'];
    }
    elseif ($dias <=120) 
    { 
        $saldo_120 = $Saldo;
        $total_120 += $sale['Saldo']; 
        $total_120_vendedor += $sale['Saldo']; 
        $total_120_sucursal += $sale['Saldo'];
    }
    else
    {
        $saldo_mas = $Saldo;
        $total_mas += $sale['Saldo']; 
        $total_mas_vendedor += $sale['Saldo']; 
        $total_mas_sucursal += $sale['Saldo'];
    }
    $total += $sale['Saldo'];
    $total_vendedor += $sale['Saldo'];
    $total_sucursal += $sale['Saldo'];
    $Saldo = $this->sma->formatMoney($total,0,'','.');

    $pdf->Cell(40,$height, $sale['Referencia'] ,0,0,'L');
    $pdf->Cell(20,$height, $fecha ,0,0,'C');
    $pdf->Cell(10,$height, $dias ,0,0,'C');
    $pdf->Cell(20,$height, $saldo_30 ,0,0,'R');
    $pdf->Cell(20,$height, $saldo_60 ,0,0,'R'); 
    $pdf->Cell(20,$height, $saldo_90 ,0,0,'R'); 
    $pdf->Cell(20,$height, $saldo_120 ,0,0,'R');
    $pdf->Cell(20,$height, $saldo_mas ,0,0,'R');
    $pdf->Cell(26,$height, $Saldo ,0,0,'R');
    $pdf->Ln();
}

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(70,$height+1, 'SubTotal Cliente' ,0,0,'R');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_30,0,'','.'),'T',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_60,0,'','.'),'T',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_90,0,'','.'),'T',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_120,0,'','.'),'T',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_mas,0,'','.'),'T',0,'R');
$pdf->Cell(26,$height+1, $this->sma->formatMoney($total,0,'','.') ,'T',0,'R');
$pdf->Ln();

$total_vendedor += $total;
$total_30_vendedor += $total_30;
$total_60_vendedor += $total_60;
$total_90_vendedor += $total_90;
$total_120_vendedor += $total_120; 
$total_mas_vendedor += $total_mas;

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(70,$height+1, 'SubTotal Vendedor' ,0,0,'R');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_30_vendedor,0,'','.'),'T',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_60_vendedor,0,'','.'),'T',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_90_vendedor,0,'','.'),'T',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_120_vendedor,0,'','.'),'T',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_mas_vendedor,0,'','.'),'T',0,'R');
$pdf->Cell(26,$height+1, $this->sma->formatMoney($total_vendedor,0,'','.') ,'T',0,'R');
$pdf->Ln();

$total_sucursal += $total_vendedor;
$total_30_sucursal += $total_30_vendedor;
$total_60_sucursal += $total_60_vendedor;
$total_90_sucursal += $total_90_vendedor;
$total_120_sucursal += $total_120_vendedor; 
$total_mas_sucursal += $total_mas_vendedor;

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(70,$height+1, 'SubTotal Sucursal' ,'TB',0,'R');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_30_sucursal,0,'','.'),'TB',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_60_sucursal,0,'','.'),'TB',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_90_sucursal,0,'','.'),'TB',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_120_sucursal,0,'','.'),'TB',0,'R');
$pdf->Cell(20,$height+1, $this->sma->formatMoney($total_mas_sucursal,0,'','.'),'TB',0,'R');
$pdf->Cell(26,$height+1, $this->sma->formatMoney($total_sucursal,0,'','.') ,'TB',0,'R');
$pdf->Ln();


$pdf->Output();

?>