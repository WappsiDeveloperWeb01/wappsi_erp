<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$v = "";
if ($this->input->post('product')) {
    $v .= "&product=" . $this->input->post('product');
}
if ($this->input->post('reference_no')) {
    $v .= "&reference_no=" . $this->input->post('reference_no');
}
if ($this->input->post('customer')) {
    $v .= "&customer=" . $this->input->post('customer');
}
if ($this->input->post('biller')) {
    $v .= "&biller=" . $this->input->post('biller');
}
if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}
if ($this->input->post('category')) {
    $v .= "&category=" . $this->input->post('category');
}
if ($this->input->post('brand')) {
    $v .= "&brand=" . $this->input->post('brand');
}
if ($this->input->post('user')) {
    $v .= "&user=" . $this->input->post('user');
}
if ($this->input->post('serial')) {
    $v .= "&serial=" . $this->input->post('serial');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
if ($this->input->post('state')) {
    $v .= "&state=" . $this->input->post('state');
}
if ($this->input->post('city')) {
    $v .= "&city=" . $this->input->post('city');
}
if ($this->input->post('zone')) {
    $v .= "&zone=" . $this->input->post('zone');
}
if ($this->input->post('subzone')) {
    $v .= "&subzone=" . $this->input->post('subzone');
}
if ($this->input->post('detailed_sales')) {
    $v .= "&detailed_sales=" . 1;
}
if ($this->input->post('detailed_seller')) {
    $v .= "&detailed_seller=1&detailed_sales=1";
    $_POST['detailed_sales'] = 1;
}

if ($this->input->post('per_variant')) {
    $v .= "&per_variant=1&detailed_sales=1";
    $_POST['detailed_sales'] = 1;
}
if ($this->input->post('sale_document_type')) {
    $v .= "&sale_document_type=" . $this->input->post('sale_document_type');
}
if ($this->input->post('seller')) {
    $v .= "&seller=" . $this->input->post('seller');
}
if ($this->input->post('option_id')) {
    $v .= "&option_id=" . $this->input->post('option_id');
}
if ($this->input->post('detail_profitability')) {
    $v .= "&detail_profitability=" . $this->input->post('detail_profitability');
}

?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open("reports/sales", 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("sale_document_type", "sale_document_type"); ?>
                                        <select name="sale_document_type" class="form-control" id="sale_document_type">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php if ($sale_document_types): ?>
                                                <?php foreach ($sale_document_types as $sdt): ?>
                                                    <option value="<?= $sdt->id ?>" <?= isset($_POST['sale_document_type']) && $_POST['sale_document_type'] == $sdt->id ? 'selected="selected"' : '' ?>><?= $sdt->sales_prefix ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("product", "suggest_product"); ?>
                                        <?php echo form_input('sproduct', (isset($_POST['sproduct']) ? $_POST['sproduct'] : ""), 'class="form-control" id="suggest_product"'); ?>
                                        <input type="hidden" name="product" value="<?= isset($_POST['product']) ? $_POST['product'] : "" ?>" id="report_product_id"/>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="category"><?= lang("category"); ?></label>
                                        <?php
                                        $wh[""] = lang('select');
                                        foreach ($categories as $category) {
                                            $wh[$category->id] = $category->name;
                                        }
                                        echo form_dropdown('category', $wh, (isset($_POST['category']) ? $_POST['category'] : ""), 'class="form-control" id="category" data-placeholder="' . $this->lang->line("select"). '"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="brand"><?= lang("brand"); ?></label>
                                        <?php
                                        $wh = [];
                                        $wh[""] = lang('select');
                                        foreach ($brands as $brand) {
                                            $wh[$brand->id] = $brand->name;
                                        }
                                        echo form_dropdown('brand', $wh, (isset($_POST['brand']) ? $_POST['brand'] : ""), 'class="form-control" id="brand" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("brand") . '"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="reference_no"><?= lang("reference_no"); ?></label>
                                        <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ""), 'class="form-control tip" id="reference_no"'); ?>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                        <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'class="form-control" id="customer" data-placeholder="' . $this->lang->line("select"). '"'); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                        <?php
                                        $wh = [];
                                        $wh[""] = lang('select');
                                        foreach ($warehouses as $warehouse) {
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                        echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                        ?>
                                    </div>
                                </div>

                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <?= lang("biller", "slbiller"); ?>
                                            <select name="biller" class="form-control" id="slbiller" required="required">
                                                <?php if ($billers && count($billers) > 1): ?>
                                                    <option value=""><?= lang('select') ?></option>
                                                <?php endif ?>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" <?= (isset($_POST['biller']) && $_POST['biller'] == $biller->id ? "selected" : "") ?>><?= $biller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <?php
                                        $bl[""] = "";
                                        $bldata = [];
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                            $bldata[$biller->id] = $biller;
                                        }
                                    ?>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <?= lang("biller", "slbiller"); ?>
                                            <select name="biller" class="form-control" id="slbiller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                ?>
                                                    <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                        <?php
                                        $us[""] = lang('select');
                                        foreach ($users as $user) {
                                            $us[$user->id] = $user->first_name . " " . $user->last_name;
                                        }
                                        echo form_dropdown('user', $us, (isset($_POST['user']) ? $_POST['user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label class="control-label" for="seller"><?= lang("seller"); ?></label>
                                    <?php
                                    $user_seller = false;
                                    // exit(var_dump($this->sma->seller_restricted()));
                                    if ($this->sma->seller_restricted()) {
                                        $sl = [];
                                        $user_seller = $this->sma->seller_restricted();
                                    } else {
                                        $sl[""] = lang('select');
                                    }
                                    foreach ($sellers as $seller) {
                                        if ($user_seller && $user_seller == $seller->id || !$user_seller) {
                                            $sl[$seller->id] = $seller->company != '-' ? $seller->company : $seller->name;
                                        }
                                    }
                                    echo form_dropdown('seller', $sl, (isset($_POST['seller']) ? $_POST['seller'] : $user_seller), 'class="form-control" id="seller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("seller") . '" '.($user_seller ? 'readonly' : ''));
                                    ?>
                                </div>

                                    

                                <?php if ($this->Settings->big_data_limit_reports == 0) { ?>
                                    <?php if($Settings->product_serial) { ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang('serial_no', 'serial'); ?>
                                                <?= form_input('serial', '', 'class="form-control tip" id="serial"'); ?>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="form-group col-md-2">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label" for="seller"><?= sprintf(lang('per_variant'), lang('variant')) ?></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>
                                                        <input type="checkbox" name="per_variant" id="per_variant" class="form-control">
                                                        <?php //lang('filter') ?>
                                                    </label>
                                                </div>
                                                <div class="col-md-9 option_id_div" style="display:none;">
                                                    <input type="text" name="option_id" id="option_id" class="form-control" placeholder="<?= lang("select") ?>">
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang('detailed_sales', 'detailed_sales'); ?>
                                                <br>
                                                <label>
                                                    <input type="checkbox" name="detailed_sales" id="detailed_sales" class="form-control" <?= isset($_POST['detailed_sales']) ? 'checked="checked"' : '' ?>>
                                                    <?= lang('detail_by_products') ?>
                                                </label>
                                                <?php if ($this->Settings->product_seller_management == 1) { ?>
                                                    <label>
                                                        <input type="checkbox" name="detailed_seller" id="detailed_seller" class="form-control" <?= isset($_POST['detailed_seller']) ? 'checked="checked"' : '' ?>>
                                                        <?= lang('detail_by_seller_product') ?>
                                                    </label>
                                                <?php } ?>
                                            </div>
                                        </div>
                                <?php } ?>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("state"); ?></label>
                                        <select name="state" id="state" class="form-control">
                                            <option value=""><?= lang("select") ?></option>
                                            <?php foreach ($states as $state): ?>
                                                <option value="<?= $state->DEPARTAMENTO ?>" data-code="<?= $state->CODDEPARTAMENTO ?>"><?= $state->DEPARTAMENTO ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("city"); ?></label>
                                        <select name="city" id="city" class="form-control">
                                            <option value=""><?= lang("select") ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("zone"); ?></label>
                                        <select name="zone" id="zone" class="form-control">
                                            <option value=""><?= lang("select") ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("subzone", "subzone"); ?>
                                        <select class="form-control select" name="subzone" id="subzone">
                                        <option value=""><?= lang("select") ?></option>
                                        </select>
                                        <input type="hidden" name="subzone_code" id="subzone_code" <?= (isset($_POST['subzone_code'])) ? "value='".$_POST['subzone_code']."'" : "" ?>>
                                    </div>
                                </div>
                                <?php if ($this->Settings->big_data_limit_reports == 0 && ($this->Admin || $this->Owner || $this->GP['products-cost'])): ?>
                                    <div class="row">
                                        <div class="col-md-4 detail_profitability" style="display:none;">
                                            <label>
                                                <input type="checkbox" name="detail_profitability" id="detail_profitability" class="form-control" <?= (isset($_POST['detail_profitability']) && $_POST['detail_profitability']) || !isset($_POST['detail_profitability']) ? 'checked="checked"' : '' ?>>
                                                <?= lang('detail_profitability') ?>
                                            </label>
                                        </div>
                                    </div>
                                <?php endif ?>
                            </div>

                            <div class="row">
                                <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>
                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                        <div class="col-sm-4 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang("start_date", "start_date_dh"); ?>
                                            <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang("end_date", "end_date_dh"); ?>
                                            <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <button class="btn btn-primary new-button" id="submit_filter" type="button" title="<?= lang('search') ?>">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="SlRData"
                                        class="table table-hover table-condensed reports-table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("affects_to"); ?></th>
                                            <th><?= lang("biller"); ?></th>
                                            <th><?= lang("customer_name"); ?></th>
                                            <?php if ($this->input->post('detailed_sales')): ?>
                                                <th><?= lang("product"); ?></th>
                                                <th><?= lang("reference"); ?></th>
                                                <?php if ($this->input->post('detailed_seller')): ?>
                                                    <th><?= lang("product_seller"); ?></th>
                                                <?php endif ?>
                                                <th><?= lang("product_qty"); ?></th>
                                            <?php endif ?>
                                            <th>Subtotal</th>
                                            <th>Impuestos</th>
                                            <th>Dcto General</th>
                                            <th>Total</th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("sale_status"); ?></th>
                                            <th><?= lang("last_payment"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <?php if ($this->input->post('detailed_sales')): ?>
                                                <th><?= lang("product"); ?></th>
                                                <?php if ($this->input->post('detailed_seller')): ?>
                                                    <th><?= lang("product_seller"); ?></th>
                                                <?php endif ?>
                                                <th><?= lang("product_qty"); ?></th>
                                            <?php endif ?>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>Total</th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th></th>
                                            <th><?= lang("last_payment"); ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>

</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        oTable = $('#SlRData').dataTable({
            aaSorting: [[0, "desc"]],
            aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('reports/getSalesReport/?v=1' . $v) ?>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "invoice_link2";
                return nRow;
            },
            aoColumns: [
                {"bVisible": false},
                {"mRender": fld},
                null,
                null,
                null,
                null,
                <?php if ($this->input->post('detailed_sales')): ?>
                    {"bSearchable": false, "mRender": pqFormat},
                    null,
                    <?php if ($this->input->post('detailed_seller')): ?>
                        null,
                    <?php endif ?>
                    {"mRender": pqFormat},
                <?php endif ?>
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                <?php if ($this->input->post('detailed_sales')): ?>
                    {"bVisible": false},
                    {"bVisible": false},
                    {"bVisible": false},
                <?php else: ?>
                    {"mRender": currencyFormat},
                    {"mRender": currencyFormat},
                    {"mRender": row_status},
                <?php endif ?>
                null,
            ],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html(`<div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>">
                        <i class="fas fa-ellipsis-v fa-lg"></i>
                    </button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?>
                            </a>
                        </li>
                    </ul>
                </div>`);

                $('[data-toggle-second="tooltip"]').tooltip();
            },
            fnFooterCallback: function (nRow, aaData, iStart, iEnd, aiDisplay) {
                <?php if ($this->input->post('detailed_sales')): ?>
                    var tt1 = 0, tt2 = 0, tt3 = 0, tt4 = 0, tt5 = 0, tt6 = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        <?php if ($this->input->post('detailed_seller')): ?>
                            num_row = 8;
                        <?php else: ?>
                            num_row = 7;
                        <?php endif ?>
                        tt1 += sumListQuantitys(aaData[aiDisplay[i]][num_row]);num_row++;
                        tt2 += parseFloat(aaData[aiDisplay[i]][num_row]);num_row++;
                        tt3 += parseFloat(aaData[aiDisplay[i]][num_row]);num_row++;
                        tt4 += parseFloat(aaData[aiDisplay[i]][num_row]);num_row++;
                        tt5 += parseFloat(aaData[aiDisplay[i]][num_row]);num_row++;
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    <?php if ($this->input->post('detailed_seller')): ?>
                        cell = 7;
                    <?php else: ?>
                        cell = 6;
                    <?php endif ?>
                    nCells[cell].innerHTML = currencyFormat(parseFloat(tt1));cell++;
                    nCells[cell].innerHTML = currencyFormat(parseFloat(tt2));cell++;
                    nCells[cell].innerHTML = currencyFormat(parseFloat(tt3));cell++;
                    nCells[cell].innerHTML = currencyFormat(parseFloat(tt4));cell++;
                    nCells[cell].innerHTML = currencyFormat(parseFloat(tt5));cell++;
                <?php else: ?>
                    var tt1 = 0, tt2 = 0, tt3 = 0, tt4 = 0, tt5 = 0, tt6 = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        tt1 += parseFloat(aaData[aiDisplay[i]][6]);
                        tt2 += parseFloat(aaData[aiDisplay[i]][7]);
                        tt3 += parseFloat(aaData[aiDisplay[i]][8]);
                        tt4 += parseFloat(aaData[aiDisplay[i]][9]);
                        tt5 += parseFloat(aaData[aiDisplay[i]][10]);
                        tt6 += parseFloat(aaData[aiDisplay[i]][11]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[5].innerHTML = currencyFormat(parseFloat(tt1));
                    nCells[6].innerHTML = currencyFormat(parseFloat(tt2));
                    nCells[7].innerHTML = currencyFormat(parseFloat(tt3));
                    nCells[8].innerHTML = currencyFormat(parseFloat(tt4));
                    nCells[9].innerHTML = currencyFormat(parseFloat(tt5));
                    nCells[10].innerHTML = currencyFormat(parseFloat(tt6));
                <?php endif ?>

            }
        }).fnSetFilteringDelay().dtFilter([

        ], "footer");
        <?php endif ?>


        <?php if ($this->session->userdata('biller_id')): ?>
            setTimeout(function() {
                $('#slbiller').select2('val', '<?= $this->session->userdata('biller_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
        <?php if ($this->session->userdata('warehouse_id')): ?>
            setTimeout(function() {
                $('#warehouse').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>


        $('#form').hide();
        <?php if ($this->input->post('customer')) { ?>
        $('#customer').val(<?= $this->input->post('customer') ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/suggestions/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#customer').val(<?= $this->input->post('customer') ?>);
        <?php } ?>
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });

        $(document).on('click', '#pdf', function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getSalesReport/pdf/?v=1'.$v)?>";
            return false;
        });
        $(document).on('click', '#xls', function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getSalesReport/0/xls/?v=1'.$v)?>";
            return false;
        });
        $(document).on('click', '#image', function (event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
        $(document).on('click', '#submit_filter', function (event) {
            if ($('#form_filter').valid()) {
                $('#filter_action').val(1);
                $('#form_filter').submit();
            }
        });
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '20%'
        });

        $("#form_filter").validate({
            ignore: []
        });
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
        <?php if ($this->Settings->big_data_limit_reports == 1): ?>
            $('#biller').prop('required', true);
        <?php endif ?>

        <?php if (isset($_POST['state'])): ?>
            setTimeout(function() {
                $('#state').select2('val', '<?= $_POST['state'] ?>').trigger('change');
            }, 800);
        <?php endif ?>

        $('#state').on('change', function(){ get_cities(); });
        $('#country').on('change', function(){ get_states(); });
        $('#city').on('change', function() { get_postal_code(); get_zones(); });
        $('#zone').on('change', function(){get_subzones()});


        <?php if ($this->input->post('per_variant')): ?>
            $('#per_variant').iCheck('check');
        <?php endif ?>
        <?php if ($this->input->post('option_id')): ?>
            $('#option_id').val("<?= $this->input->post('option_id') ?>").select2({
                minimumInputLength: 1,
                data: [],
                allowClear: true,
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"products/getVariant/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "products/variants_suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        <?php else: ?>
            $('#option_id').select2({
                minimumInputLength: 1,
                data: [],
                allowClear: true,
                ajax: {
                url: site.base_url+"products/variants_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                    term: term,
                    limit: 10
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                    return {results: data.results};
                    } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
                }
            });
        <?php endif ?>

        $(document).on('ifChecked', '#detailed_seller', function(){
            $('#detailed_sales').iCheck('check').iCheck('disable');
        });
        $(document).on('ifUnchecked', '#detailed_seller', function(){
            $('#detailed_sales').iCheck('uncheck').iCheck('enable');
        });
        $(document).on('ifChecked', '#per_variant', function(){
            $('#detailed_sales').iCheck('check').iCheck('disable');
            $('.option_id_div').fadeIn();
        });
        $(document).on('ifUnchecked', '#per_variant', function(){
            $('#detailed_sales').iCheck('uncheck').iCheck('enable');
            $('.option_id_div').fadeOut();
        });

        $(document).on('ifChecked', '#detailed_sales', function(){
            $('.detail_profitability').fadeIn();
        });
        $(document).on('ifUnchecked', '#detailed_sales', function(){
            $('.detail_profitability').fadeOut();
        });

        <?php if (isset($_POST['detailed_sales']) && $_POST['detailed_sales']): ?>
            $('#detailed_sales').iCheck('uncheck').iCheck('check');
        <?php endif ?>

        $(document).on('change', '#start_date_dh, #end_date_dh, #date_records_filter_dh', function(){
            change_biller_required();
        });
    });

    function get_cities() {
        dpto = $('#state option:selected').data('code');
        $.ajax({
        url:"<?= admin_url() ?>customers/get_cities/"+dpto+"<?= !isset($_POST['city']) ? '' : '/'.$_POST['city'] ?>",
        }).done(function(data) {
        $('#city').html(data).select2();
        }).fail(function(data) {
        console.log(data.responseText);
        });
    }

    function get_zones(){
        code = $('#city option:selected').data('code');
        $.ajax({
            url:"<?= admin_url().'customers/get_zones/' ?>"+code+"<?= !isset($_POST['zone']) ? '' : '/'.$_POST['zone'] ?>"
        }).done(function(data){
            $('#zone').html(data).select2();
        });
    }

    function get_subzones(){
        code = $('#zone option:selected').data('code');
        $('.postal_code').val(code);
        $.ajax({
            url:"<?= admin_url().'customers/get_subzones/' ?>"+code+"<?= !isset($_POST['subzone']) ? '' : '/'.$_POST['subzone'] ?>"
        }).done(function(data){
            $('#subzone').html(data).select2();
        });
    }

    function change_biller_required()
    {
        if (site.settings.big_data_limit_reports == 0) { //NO LIMITAR
            $('#slbiller').prop('required', false); //SUCURSAL NO REQUERIDA
        } else {
            var fechaInicioStr = $('#start_date_dh').val();
            var fechaFinStr = $('#end_date_dh').val();
            var fechaInicio = parsearFecha(fechaInicioStr);
            var fechaFin = parsearFecha(fechaFinStr);
            if (fechaInicio && fechaFin) { 

                var diferencia = fechaFin - fechaInicio;
                var diasDiferencia = Math.floor(diferencia / (1000 * 60 * 60 * 24));
                if (diasDiferencia > 31) {
                    $('#slbiller').prop('required', true);
                } else {
                    $('#slbiller').prop('required', false);
                }
            }
        }
            
    }

    function parsearFecha(fechaStr)
    {
        var partes = fechaStr.split(' ');
        var fechaPartes = partes[0].split('/');
        var horaPartes = partes[1].split(':');
        if (fechaPartes.length === 3 && horaPartes.length === 2) {
            var dia = parseInt(fechaPartes[0]);
            var mes = parseInt(fechaPartes[1]) - 1; // Meses en JavaScript comienzan desde 0
            var anio = parseInt(fechaPartes[2]);
            var hora = parseInt(horaPartes[0]);
            var minutos = parseInt(horaPartes[1]);
            return new Date(anio, mes, dia, hora, minutos);
        } else {
            return null;
        }
    }
</script>