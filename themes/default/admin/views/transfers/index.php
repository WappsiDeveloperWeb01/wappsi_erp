<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>

    if (localStorage.getItem('remove_tols')) {
        if (localStorage.getItem('toitems')) {
            localStorage.removeItem('toitems');
        }
        if (localStorage.getItem('toshipping')) {
            localStorage.removeItem('toshipping');
        }
        if (localStorage.getItem('toref')) {
            localStorage.removeItem('toref');
        }
        if (localStorage.getItem('to_warehouse')) {
            localStorage.removeItem('to_warehouse');
        }
        if (localStorage.getItem('tonote')) {
            localStorage.removeItem('tonote');
        }
        if (localStorage.getItem('from_warehouse')) {
            localStorage.removeItem('from_warehouse');
        }
        if (localStorage.getItem('todate')) {
            localStorage.removeItem('todate');
        }
        if (localStorage.getItem('tostatus')) {
            localStorage.removeItem('tostatus');
        }
        if (localStorage.getItem('tobiller_origin')) {
            localStorage.removeItem('tobiller_origin');
        }
        if (localStorage.getItem('tobiller_destination')) {
            localStorage.removeItem('tobiller_destination');
        }
        localStorage.removeItem('remove_tols');
    }

    <?php if ($this->session->userdata('remove_tols')) {?>
        if (localStorage.getItem('toitems')) {
            localStorage.removeItem('toitems');
        }
        if (localStorage.getItem('toshipping')) {
            localStorage.removeItem('toshipping');
        }
        if (localStorage.getItem('toref')) {
            localStorage.removeItem('toref');
        }
        if (localStorage.getItem('to_warehouse')) {
            localStorage.removeItem('to_warehouse');
        }
        if (localStorage.getItem('tonote')) {
            localStorage.removeItem('tonote');
        }
        if (localStorage.getItem('from_warehouse')) {
            localStorage.removeItem('from_warehouse');
        }
        if (localStorage.getItem('todate')) {
            localStorage.removeItem('todate');
        }
        if (localStorage.getItem('tostatus')) {
            localStorage.removeItem('tostatus');
        }
        if (localStorage.getItem('tobiller_origin')) {
            localStorage.removeItem('tobiller_origin');
        }
        if (localStorage.getItem('tobiller_destination')) {
            localStorage.removeItem('tobiller_destination');
        }
    <?php
        $this->sma->unset_data('remove_tols');
    }
    ?>

    if ($(window).width() < 1000) {
        var nums = [[10, 25], [10, 25]];
    } else {
        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
    }
    $(document).ready(function () {
        _tabFilterFill = false;
        oTable = $('#TOData').dataTable({
            "aaSorting": [[1, "desc"], [2, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('transfers/getTransfers') ?>',
            'dom': '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push(
                    {
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    },                    
                    {
                        "name": "warehouse_origin_id",
                        "value": "<?= isset($_POST['warehouse_origin_id']) ? $_POST['warehouse_origin_id'] : '' ?>"
                    }, 
                    {
                        "name": "warehouse_destination_id",
                        "value": "<?= isset($_POST['warehouse_destination_id']) ? $_POST['warehouse_destination_id'] : '' ?>"
                    },                    
                    {
                        "name": "status",
                        "value": "<?= isset($_POST['status']) ? $_POST['status'] : '' ?>"
                    },                    
                    {
                        "name": "accepted",
                        "value": "<?= isset($_POST['accepted']) ? $_POST['accepted'] : '' ?>"
                    }, 
                    {
                        "name": "start_date",
                        "value": "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
                    }, 
                    {
                        "name": "end_date",
                        "value": "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>"
                    }
                );
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false,"mRender": checkbox}, 
                {"mRender": fld}, 
                null, 
                null, 
                null, 
                {"mRender": currencyFormat}, 
                {"mRender": currencyFormat}, 
                {"mRender": currencyFormat}, 
                {"mRender": row_status}, 
                {"mRender": receivedStatus},
                {"bSortable": false,"mRender": attachment}, 
                {"bSortable": false}],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "transfer_link";
                return nRow;
            },
            'fnDrawCallback' : function(){
                $('.actionsButtonContainer').html(`<a href="<?= admin_url('transfers/add') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>
                <div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a>
                        </li>
                        <li>
                            <a href="#" id="combine" data-action="combine"><i class="fa fa-file-pdf-o"></i> <?= lang('combine_to_pdf') ?></a>
                        </li>
                        <li>
                            <a href="#" id="post_transfer" data-action="post_transfer"><i class="fa fa-file-pdf-o"></i> <?= lang('post_purchase') ?></a>
                        </li>
                    </ul>
                </div>`);
                
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var row_total = 0, tax = 0, gtotal = 0;
                for (var i = 0; i < aaData.length; i++) {
                    row_total += parseFloat(aaData[aiDisplay[i]][5]);
                    tax += parseFloat(aaData[aiDisplay[i]][6]);
                    gtotal += parseFloat(aaData[aiDisplay[i]][7]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[5].innerHTML = currencyFormat(formatMoney(row_total));
                nCells[6].innerHTML = currencyFormat(formatMoney(tax));
                nCells[7].innerHTML = currencyFormat(formatMoney(gtotal));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('ref_no');?>]", filter_type: "text", data: []},
            {
                column_number: 3,
                filter_default_label: "[<?=lang("warehouse").' ('.lang('from').')';?>]",
                filter_type: "text", data: []
            },
            {
                column_number: 4,
                filter_default_label: "[<?=lang("warehouse").' ('.lang('to').')';?>]",
                filter_type: "text", data: []
            },
            {column_number: 8, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
        ], "footer");
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '20%'
        });
    });
</script>


<div class="wrapper wrapper-content  animated fadeInRight  no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="no-print notifications_container"></div>
        </div> 
    </div>    
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('transfers', ['id' => 'transfers_filter']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <?php if($this->Owner || $this->Admin) { ?>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="warehouse_origin"><?= $this->lang->line('warehouse_origin') ?></label>
                                                <?php $warehouse_origin_id = (isset($_POST["warehouse_origin_id"])) ? $_POST["warehouse_origin_id"] : ''; ?>
                                                <select class="form-control" id="warehouse_origin" name="warehouse_origin_id">
                                                    <option value=""><?= $this->lang->line('allsf') ?></option>
                                                    <?php foreach ($warehouses as $warehouse) : ?>
                                                        <option value="<?= $warehouse->id ?>" <?= ($warehouse->id == $warehouse_origin_id) ? 'selected' : '' ?>><?= $warehouse->name ?></option>
                                                    <?php endforeach ?> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="warehouse_destination"><?= $this->lang->line('warehouse_destination') ?></label>
                                                <?php $warehouse_destination_id = (isset($_POST["warehouse_destination_id"])) ? $_POST["warehouse_destination_id"] : ''; ?>
                                                <select class="form-control" id="warehouse_destination" name="warehouse_destination_id">
                                                    <option value=""><?= $this->lang->line('allsf') ?></option>
                                                    <?php foreach ($warehouses as $warehouse) : ?>
                                                        <option value="<?= $warehouse->id ?>" <?= ($warehouse->id == $warehouse_destination_id) ? 'selected' : '' ?>><?= $warehouse->name ?></option>
                                                    <?php endforeach ?> 
                                                </select>
                                            </div>
                                        </div>
                                    <?php } ?>    
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="status"><?= $this->lang->line('status') ?></label>
                                            <?php $status = (isset($_POST["status"])) ? $_POST["status"] : ''; ?>
                                            <?php $arrayStatus = array('completed' => lang('completed'), 'pending' => lang('pending'), 'sent' => lang('sent')); ?>
                                            <select class="form-control" id="status" name="status">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($arrayStatus as $key => $value) : ?>
                                                    <option value="<?= $key ?>" <?= ($key == $status) ? 'selected' : '' ?>><?= $value ?></option>
                                                <?php endforeach ?> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="accepted"><?= $this->lang->line('accepted') ?></label>
                                            <?php $accepted = (isset($_POST["accepted"])) ? $_POST["accepted"] : ''; ?>
                                            <?php $arrayAccepted = array('1' => lang('accepted'), '2' => lang('No_accepted')); ?>
                                            <select class="form-control" id="accepted" name="accepted">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($arrayAccepted as $key => $value) : ?>
                                                    <option value="<?= $key ?>" <?= ($key == $accepted) ? 'selected' : '' ?>><?= $value ?></option>
                                                <?php endforeach ?> 
                                            </select>
                                        </div>
                                    </div>
                                </div> <!-- row -->
                            </div> <!-- col-sm-11 -->
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div> <!-- ibox-title -->
                    <div class="ibox-content" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Filtros avanzados</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-3" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <?= lang('date_records_filter', 'date_records_filter_h') ?>
                                            <select name="date_records_filter" id="date_records_filter_h" class="form-control">
                                                <?= $this->sma->get_filter_options(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="date_controls_h">
                                        <div class="col-sm-3" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <?= lang('start_date', 'start_date_h') ?>
                                            <input type="date" name="start_date" id="start_date_h" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                        </div>
                                        <div class="col-sm-3" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <?= lang('end_date', 'end_date_h') ?>
                                            <input type="date" name="end_date" id="end_date_h" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" id="submit-sales-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div> <!-- ibox-content -->
                </div> <!-- ibox -->
            <?= form_close() ?>    
        </div> <!-- col-sm-12 -->
    </div> <!-- row -->
           

<?php if ($Owner || $Admin || $GP['bulk_actions']) {
    echo admin_form_open('transfers/transfer_actions', 'id="action-form"');
} ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="text_filter"></h4>
                            <div class="table-responsive">
                                <table id="TOData" class="table table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("ref_no"); ?></th>
                                            <th><?= lang("warehouse") . ' (' . lang('from') . ')'; ?></th>
                                            <th><?= lang("warehouse") . ' (' . lang('to') . ')'; ?></th>
                                            <th><?= lang("total"); ?></th>
                                            <th><?= lang("product_tax"); ?></th>
                                            <th><?= lang("grand_total"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th><?= lang("acepted"); ?></th>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                            <th style="width:100px;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
                                            <th style="width:100px; text-align: center;"><?= lang("acepted"); ?></th>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script type="text/javascript">


$(document).ready(function(){
    setTimeout(function() {
        <?php if (!isset($_POST['date_records_filter'])): ?>
            $('#date_records_filter_h').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            $('#transfers_filter').submit();
        <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
            $('#date_records_filter_h').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
        <?php endif ?>
        setFilterText();
    }, 150);

    $(document).on('change', '#warehouse_origin', function(){
        $('#warehouse_destination').find('option').prop('disabled', false);
        if ($(this).val() != '') {
            $('#warehouse_destination').find('option[value="' + $(this).val() + '"]').prop('disabled', true);
        }   
    });

    $(document).on('change', '#warehouse_destination', function(){
        $('#warehouse_origin').find('option').prop('disabled', false);
        if ($(this).val() != '') {
            $('#warehouse_origin').find('option[value="' + $(this).val() + '"]').prop('disabled', true);
        }
    });

    if ($('#warehouse_origin').val() != '') {
        let warehouse_origin = $('#warehouse_origin').val();
        $('#warehouse_destination').find('option[value="' + warehouse_origin + '"]').prop('disabled', true);
    }

    if ($('#warehouse_destination').val() != '') {
        let warehouse_destination = $('#warehouse_destination').val();
        $('#warehouse_origin').find('option[value="' + warehouse_destination + '"]').prop('disabled', true);
    }
});

function setFilterText(){
    var start_date_text = $('#start_date_h').val();
    var end_date_text = $('#end_date_h').val();
    var text = "Filtros configurados : ";
    coma = false;
    if (start_date_text != '' && start_date_text !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha de inicio ("+start_date_text+")";
        coma = true;
    }
    if (end_date_text != '' && end_date_text !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha final ("+end_date_text+")";
        coma = true;
    }
    $('.text_filter').html(text);
}

function receivedStatus(x)
{ 
    y = x.split('-');
    if (y[1] == '0') {
        return '<div class="receivedClass text-center text-default"><a href="'+site.base_url+'/transfers/received/'+y[0]+'" data-toggle="modal" data-target="#myModal"><i style="cursor:pointer;" class="fa fa-times text-default" data-toggle="tooltip" data-placement="top" title="'+lang["no"]+'"> </i></a></div>';
    } else if(y[1] == '1') {
        return '<div class="receivedClass text-center text-info"><i style="cursor:pointer;" class="fa fa-check" data-toggle="tooltip" data-placement="top" title="'+lang["yes"]+'"></i></div>';
    }
}
</script>