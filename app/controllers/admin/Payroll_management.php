<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payroll_management extends MY_Controller
{
    public $months;

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->enableElectronicPayroll) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->Payroll_settings = $this->site->get_payroll_setting();

        $this->lang->admin_load('payroll', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->load->admin_model('Calendar_model');
        $this->load->admin_model("Employees_model");
        $this->load->admin_model("FutureItems_model");
        $this->load->admin_model('UserActivities_model');
        $this->load->admin_model("Payroll_concepts_model");
        $this->load->admin_model('Payroll_contracts_model');
        $this->load->admin_model('Payroll_management_model');
        $this->load->admin_model("Payroll_provisions_model");
        $this->load->admin_model("Payroll_electronic_model");
        $this->load->admin_model('PayrollContractConcepts_model');
        $this->load->admin_model("Payroll_social_security_parafiscal_model");

        $this->months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    }

    public function index()
    {
        $this->sma->checkPermissions("index", NULL, "payroll_management");

        $this->data["payroll"] = $this->Payroll_management_model->get_last_payroll();
        $this->data["in_preparation_payroll"] = $this->Payroll_management_model->get_by_status(IN_PREPARATION);
        $this->page_construct("payroll_management/index", ["page_title" => lang("payroll_management_list")], $this->data);
    }

    public function get_datatables()
    {
        $this->load->library('datatables');
        $this->datatables->select("payroll.id AS id, code, creation_date, start_date, end_date, employee_no, earnings, deductions, total_payment, provisions, payroll.status");
        $this->datatables->from("payroll");
        $this->datatables->add_column("Actions", '<div class="text-center"><div class="btn-group text-left">
            <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones"><i class="fas fa-ellipsis-v fa-lg"></i></button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="' . admin_url('payroll_management/see/$1') . '"><i class="fa fa-eye" aria-hidden="true"></i> ' . lang('payroll_management_see_details') . '</a></li>
                <li class="option_update_payroll"><a href="' . admin_url('payroll_management/update/$1') . '"><i class="fa fa-refresh" aria-hidden="true"></i> ' . lang('update') . '</a></li>
                <li class="option_approve_payroll" data-payroll_id="$1"><a><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' . lang('approve') . '</a></li>
                <li class="option_delete_payroll" data-payroll_id="$1"><a><i class="fa fa-trash" aria-hidden="true"></i> ' . lang('delete') . '</a></li>
                <li class="optionToPayPayroll"><a href="'. admin_url('payroll_management/to_pay/$1') .'"><i class="fa fa-money"></i> '. lang('to_pay') .'</a></li>
                <li class="optionSpreadsheet"><a href="'. admin_url('payroll_management/generatePayrollPDF/$1') .'" target="_blank"><i class="fa fa-file-text-o"></i> '. lang('spreadsheet') .'</a></li>
            </ul>
        </div>', "id");
        echo $this->datatables->generate();
    }

    public function approve($payroll_id)
    {
        $this->sma->checkPermissions("approve", NULL, "payroll_management");

        if ($this->validate_contract_data_changes() == FALSE) { redirect($_SERVER["HTTP_REFERER"]); }

        $payroll_data = $this->Payroll_management_model->get_by_id($payroll_id);
        if ($this->validate_payroll_exists($payroll_data)) {
            $this->session->set_flashdata('error', lang("existingPayroll"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $document_type = $this->Payroll_electronic_model->get_document_types_by_biller_id(43, $payroll_data->biller_id);
        if ($document_type == FALSE) {
            $this->session->set_flashdata('error', lang("documentTypesNotExist"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $electronic_payroll_created = $this->generates_electronic_payroll_header_data($payroll_id);
        if ($electronic_payroll_created == TRUE) {
            $approved_payroll = $this->Payroll_management_model->update(['status' => APPROVED], $payroll_id);

            if ($approved_payroll == TRUE) {
                $payroll_items = $this->Payroll_management_model->get_payroll_items($payroll_id);
                $this->mark_future_item_applied($payroll_items);

                $this->session->set_flashdata('message', lang("approved_payroll"));
            } else {
                $this->session->set_flashdata('error', lang("not_approved_payroll"));
            }
        } else {
            $this->session->set_flashdata('error', lang("not_approved_payroll"));
        }

        admin_redirect('payroll_management');
    }

    private function validate_payroll_exists($payroll)
    {
        if (!empty($payroll)) {
            $data = [
                "year" => $payroll->year,
                "month" => $payroll->month,
            ];
            $existingPayroll = $this->Payroll_electronic_model->find($data);
            if (!empty($existingPayroll)) {
                return TRUE;
            }

            return FALSE;
        }
    }

    private function validate_contract_data_changes()
    {
        if ($this->session->userdata('existing_configuration_data_changes') == TRUE || $this->session->userdata('employee_settlement_date_change') == TRUE) {
            return FALSE;
        }
        return TRUE;
    }

    private function generates_electronic_payroll_header_data($payroll_id)
    {
        $payroll_data = $this->Payroll_management_model->get_by_id($payroll_id);

        if ($this->Payroll_settings->payment_frequency == MONTHLY) {
            $biller_id = $payroll_data->biller_id;
            $payroll_items_data = $this->Payroll_management_model->get_payroll_items_by_payroll_id($payroll_data->id);

            if (!empty($payroll_items_data)) {
                $amount_earneds = 0;
                $amount_deductions = 0;
                $employees = [];
                foreach ($payroll_items_data as $item) {
                    if (!in_array($item->payroll_concept_id, [LAYOFFS, LAYOFFS_INTERESTS, SERVICE_BONUS, MONEY_VACATION])) {
                        if ($item->earned_deduction == EARNED) {
                            $amount_earneds += $item->amount;
                        } else {
                            $amount_deductions += $item->amount;
                        }
                    } else {
                        if ($this->Payroll_settings->provision_options == 3) {
                            if ($item->earned_deduction == EARNED) {
                                $amount_earneds += $item->amount;
                            } else {
                                $amount_deductions += $item->amount;
                            }
                        } else {
                            if ($item->paid_to_employee == YES) {
                                if ($item->earned_deduction == EARNED) {
                                    $amount_earneds += $item->amount;
                                } else {
                                    $amount_deductions += $item->amount;
                                }
                            }
                        }
                    }
                    
                    if (!in_array($item->employee_id, $employees)) {
                        $employees[] = $item->employee_id;
                    }
                }

                $total_payment = $amount_earneds - $amount_deductions;
            }

            $data = [
                "creation_date" => date("Y-m-d"),
                "year"=>$payroll_data->year,
                "month"=>$payroll_data->month,
                "biller_id"=>$payroll_data->biller_id,
                "employee_no"=>count($employees),
                "earnings"=>$amount_earneds,
                "deductions"=>$amount_deductions,
                "total_payment"=>$total_payment,
                "status" => NOT_SENT,
                "created_by" => $this->session->userdata('user_id')
            ];

            $electronic_payroll_id = $this->Payroll_electronic_model->insert_header($data);
            if ($electronic_payroll_id != FALSE) {
                return $this->generate_electronic_payroll_employee_data($electronic_payroll_id, $payroll_items_data, $biller_id);
            } else {
                return FALSE;
            }
        } else if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
            if ($payroll_data->number == SECOND_FORTNIGHT) {
                $biller_id = $payroll_data->biller_id;
                $payroll_ids_data = $this->Payroll_management_model->get_payroll_grouped_by_month($payroll_data->year, $payroll_data->month);
                $payroll_items_data = $this->Payroll_management_model->get_payroll_items_by_payroll_id($payroll_ids_data->ids);

                if (!empty($payroll_items_data)) {
                    $amount_earneds = 0;
                    $amount_deductions = 0;
                    $employees = [];
                    foreach ($payroll_items_data as $item) {
                        if (!in_array($item->payroll_concept_id, [LAYOFFS, LAYOFFS_INTERESTS, SERVICE_BONUS, MONEY_VACATION])) {
                            if ($item->earned_deduction == EARNED) {
                                $amount_earneds += $item->amount;
                            } else {
                                $amount_deductions += $item->amount;
                            }
        
                        } else {
                            if ($this->Payroll_settings->provision_options == 3) {
                                if ($item->earned_deduction == EARNED) {
                                    $amount_earneds += $item->amount;
                                } else {
                                    $amount_deductions += $item->amount;
                                }
                            } else {
                                if ($item->paid_to_employee == YES) {
                                    if ($item->earned_deduction == EARNED) {
                                        $amount_earneds += $item->amount;
                                    } else {
                                        $amount_deductions += $item->amount;
                                    }
                                }
                            }
                        }

                        if (!in_array($item->employee_id, $employees)) {
                            $employees[] = $item->employee_id;
                        }
                    }
                    $total_payment = $amount_earneds - $amount_deductions;
                }

                $data = [
                    "creation_date" => date("Y-m-d"),
                    "year"=>$payroll_data->year,
                    "month"=>$payroll_data->month,
                    "biller_id"=>$payroll_data->biller_id,
                    "employee_no"=>count($employees),
                    "earnings"=>$amount_earneds,
                    "deductions"=>$amount_deductions,
                    "total_payment"=>$total_payment,
                    "status" => NOT_SENT,
                    "created_by" => $this->session->userdata('user_id')
                ];

                $electronic_payroll_id = $this->Payroll_electronic_model->insert_header($data);
                if ($electronic_payroll_id != FALSE) {
                    return $this->generate_electronic_payroll_employee_data($electronic_payroll_id, $payroll_items_data, $biller_id);
                } else {
                    return FALSE;
                }
            }
            return TRUE;
        }
    }

    private function generate_electronic_payroll_employee_data($electronic_payroll_id, $payroll_items_data, $biller_id)
    {
        $errors = 0;
        $employees = [];
        foreach ($payroll_items_data as $item) { $employees[$item->contract_id][] = $item; }

        foreach ($employees as $contract_id => $employee_items) {
            $contract = $this->Payroll_contracts_model->get_by_id($contract_id);

            $earneds = 0;
            $deductions = 0;
            foreach ($employee_items as $item) {
                if (!in_array($item->payroll_concept_id, [LAYOFFS, LAYOFFS_INTERESTS, SERVICE_BONUS, MONEY_VACATION])) {
                    if ($item->earned_deduction == EARNED) {
                        $earneds += $item->amount;
                    } else {
                        $deductions += $item->amount;
                    }
                } else {
                    if ($this->Payroll_settings->provision_options == 3) {
                        if ($item->earned_deduction == EARNED) {
                            $earneds += $item->amount;
                        } else {
                            $deductions += $item->amount;
                        }
                    } else {
                        if ($item->paid_to_employee == YES) {
                            if ($item->earned_deduction == EARNED) {
                                $earneds += $item->amount;
                            } else {
                                $deductions += $item->amount;
                            }
                        }
                    }
                }
            }
            $total_payment = $earneds - $deductions;

            $data = [
                "electronic_payroll_id" => $electronic_payroll_id,
                "creation_date" => date("Y-m-d"),
                "creation_time" => date("H:i:s"),
                "biller_id" => $biller_id,
                "employee_id" => $contract->companies_id,
                "contract_id" => $contract_id,
                "earneds" => $earneds,
                "deductions" => $deductions,
                "payment_total" => $total_payment,
                "status" => NOT_SENT
            ];

            $electronic_payroll_employee_id = $this->Payroll_electronic_model->insert_employees($data);
            if ($electronic_payroll_employee_id != FALSE) {
                if ($this->generates_electronic_payroll_items_data($electronic_payroll_employee_id, $employee_items, $biller_id) == FALSE) {
                    $errors++;
                }
            }
        }

        if ($errors == 0) {
            return TRUE;
        }
        return FALSE;
    }

    private function generates_electronic_payroll_items_data($electronic_payroll_employee_id, $employee_items_data, $biller_id)
    {
        $days_quantity_high_risk_pension = 0;
        $transportation_allowance        = 0;
        $days_quantity_pension           = 0;
        $health_rate_less_than           = 0;
        $days_quantity_health            = 0;
        $days_quantity_salary            = 0;
        $bearing_allowance               = 0;
        $days_quantity_ta                = 0;
        $hr_pension_rate                 = 0;
        $fiscal_seizures                 = 0;
        $pension_rate                    = 0;
        $salary                          = 0;

        $per_diem_constitutes_salary = $days_quantity_pdcs = 0;

        foreach ($employee_items_data as $item) {
            $employee_id = $item->employee_id;
            $concept_id = $item->payroll_concept_id;
            $concept_type_id = $item->payroll_concept_type;

            if (in_array($concept_type_id, [SALARY_PAYMENTS, PAID_LICENSE_TYPE, OVERTIME_OR_SURCHARGES.
                DISABILITIES, NOT_PAID_LICENSE_TYPE, TIME_VACATION_TYPE, MONEY_VACATION_TYPE, NON_SALARY_PAYMENTS,
                OTHER_ACCRUED_TYPE, SERVICE_BONUS_TYPE, LAYOFFS_TYPE, FOOD_BOND_TYPE, BUSINESS_LOANS, 
                OTHER_DEDUCTIONS_TYPE, ENDOWMENT, COMPENSATION_TYPE])) {

                    if (in_array($concept_type_id, [MONEY_VACATION_TYPE, SERVICE_BONUS_TYPE, LAYOFFS_TYPE])) {
                        if ($this->Payroll_settings->provision_options == 3) {
                            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $item->amount, $concept_id, $concept_type_id, $item->earned_deduction, $biller_id, $item->days_quantity, $item->hours_quantity,  $item->start_date, $item->end_date, $item->description, $item->paid_to_employee);
                        } else {
                            if ($item->paid_to_employee == YES) {
                                $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $item->amount, $concept_id, $concept_type_id, $item->earned_deduction, $biller_id, $item->days_quantity, $item->hours_quantity,  $item->start_date, $item->end_date, $item->description, $item->paid_to_employee);
                            }
                        }
                    } else {
                        $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $item->amount, $concept_id, $concept_type_id, $item->earned_deduction, $biller_id, $item->days_quantity, $item->hours_quantity,  $item->start_date, $item->end_date, $item->description, $item->paid_to_employee);
                    }
            } else {
                if ($concept_id == SALARY) {
                    $salary += $item->amount;
                    $concept_id_salary = $concept_id;
                    $concept_type_id_salary = $concept_type_id;
                    $days_quantity_salary += $item->days_quantity;
                }

                if ($concept_id == TRANSPORTATION_ALLOWANCE) {
                    $concept_id_ta = $concept_id;
                    $concept_type_id_ta = $concept_type_id;
                    $transportation_allowance += $item->amount;
                    $days_quantity_ta += $item->days_quantity;
                }

                if ($concept_id == PER_DIEM_CONSTITUTES_SALARY) {
                    $concept_id_pdcs = $concept_id;
                    $concept_type_id_pdcs = $concept_type_id;
                    $per_diem_constitutes_salary += $item->amount;
                    $days_quantity_pdcs += $item->days_quantity;
                }

                if ($concept_id == HEALTH_RATE_LESS_THAN || $concept_id == HEALTH_RATE) {
                    $concept_id_hrlt = $concept_id;
                    $concept_type_id_hrlt = $concept_type_id;
                    $health_rate_less_than += $item->amount;
                    $days_quantity_health += $item->days_quantity;
                }

                if ($concept_id == PENSION_RATE) {
                    $concept_id_pr = $concept_id;
                    $concept_type_id_pr = $concept_type_id;
                    $pension_rate += $item->amount;
                    $days_quantity_pension += $item->days_quantity;
                }

                if ($concept_id == HIGH_RISK_PENSION_RATE) {
                    $concept_id_hrpr = $concept_id;
                    $concept_type_id_hrpr = $concept_type_id;
                    $hr_pension_rate += $item->amount;
                    $days_quantity_high_risk_pension += $item->days_quantity;
                }

                if ($concept_id == BEARING_ALLOWANCE) {
                    $concept_id_ba = $concept_id;
                    $concept_type_id_ba = $concept_type_id;
                    $bearing_allowance += $item->amount;
                }

                if ($concept_id == FISCAL_SEIZURES) {
                    $concept_id_fs = $concept_id;
                    $concept_type_id_fs = $concept_type_id;
                    $fiscal_seizures += $item->amount;
                }
            }
        }

        if ($salary >= 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $salary, $concept_id_salary, $concept_type_id_salary, EARNED, $biller_id, $days_quantity_salary);
        }

        if (!empty($transportation_allowance)) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $transportation_allowance, $concept_id_ta, $concept_type_id_ta, EARNED, $biller_id, $days_quantity_ta);
        }

        if ($per_diem_constitutes_salary > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $per_diem_constitutes_salary , $concept_id_pdcs, $concept_type_id_pdcs, EARNED, $biller_id, $days_quantity_pdcs);
        }

        if ($health_rate_less_than > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $health_rate_less_than, $concept_id_hrlt, $concept_type_id_hrlt, DEDUCTION, $biller_id, $days_quantity_health);
        }

        if ($pension_rate > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $pension_rate, $concept_id_pr, $concept_type_id_pr, DEDUCTION, $biller_id, $days_quantity_pension);
        }

        if ($hr_pension_rate > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $hr_pension_rate, $concept_id_hrpr, $concept_type_id_hrpr, DEDUCTION, $biller_id, $days_quantity_high_risk_pension);
        }

        if ($bearing_allowance > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $bearing_allowance, $concept_id_ba, $concept_type_id_ba, EARNED, $biller_id);
        }

        if ($fiscal_seizures > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $fiscal_seizures, $concept_id_fs, $concept_type_id_fs, DEDUCTION, $biller_id);
        }

        if(!empty($data)) {
            return $this->Payroll_electronic_model->insert_items($data);
        }

        return FALSE;
    }

    private function get_electronic_payroll_concept_data($id, $employee_id, $amount, $concept_id, $concept_type_id, $earned_deduction, $biller_id, $days_quantity = NULL, $hours_quantity = NULL, $start_date = NULL, $end_date = NULL, $description = NULL, $paid_to_employee = 0)
    {
        $data = [
            "creation_date" => date("Y-m-d"),
            "electronic_payroll_employee_id" => $id,
            "employee_id" => $employee_id,
            "concept_type_id" => $concept_type_id,
            "concept_id" => $concept_id,
            "earned_deduction" => $earned_deduction,
            "amount" => $amount,
            "start_date" => (!empty($start_date)) ? $start_date : "0000-00-00",
            "end_date" => (!empty($end_date)) ? $end_date : "0000-00-00",
            "days_quantity" => (!empty($days_quantity)) ? $days_quantity : 0,
            "hours_quantity" => (!empty($hours_quantity)) ? $hours_quantity : 0.00,
            "biller_id" => $biller_id,
            "description" => $description,
            "paid_to_employee" => $paid_to_employee
        ];

        return $data;
    }

    private function mark_future_item_applied(array $payroll_items)
    {
        $future_items_ids = [];

        foreach ($payroll_items as $item) {
            if (!empty($item->future_item_id) && !in_array($item->future_item_id, $future_items_ids)) {
                $future_items_ids[] = $item->future_item_id;
            }
        }

        if (!empty($future_items_ids)) {

            foreach ($future_items_ids as $future_item_id) {
                $this->Payroll_management_model->update_future_item(['applied' => YES], $future_item_id, true);
            }
        }
    }

    public function see(int $id)
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $payroll = $this->Payroll_management_model->get_by_id($id);
        if (empty($payroll)) {
            $this->session->set_flashdata('error', lang("La Nómina a la que intenta ingresar no existe."));
            admin_redirect("Payroll_management");
        }
        

        $month_name = $this->getMonthName(date("Y-$payroll->month-d"));

        $payrroll_name = '';
            if ($payroll->frequency == WEEKLY) {
                $payrroll_name = ' (Semana '. $payroll->number .')';
            } else if ($payroll->frequency == BIWEEKLY) {
                if ($payroll->number == 1) {
                    $payrroll_name = ' (Primera Quincena)';
                } else {
                    $payrroll_name = ' (Segunda Quincena)';
                }
            }

        $hideShowParafiscalProvisions = FALSE;

        $this->data["payroll_id"] = $id;
        $this->data["payroll_data"] = $payroll;
        
        $this->page_construct("payroll_management/see", ["page_title" => "Detalle de nómina <small>". ucfirst($month_name) . ucfirst($payrroll_name). '</small>'], $this->data);
    }

    function getMonthName($fecha, $locale = 'es_ES') 
    {
        $formatter = new IntlDateFormatter(
            $locale,
            IntlDateFormatter::LONG, // Nivel de detalle
            IntlDateFormatter::NONE, // No incluir la hora
            'America/Bogota', // Zona horaria (opcional)
            IntlDateFormatter::GREGORIAN,
            'MMMM' // Solo el nombre del mes
        );
    
        return ucfirst($formatter->format(strtotime($fecha))); // Capitaliza la primera letra
    }

    public function get_payroll_employees_datatables(int $payroll_id)
    {
        $this->load->library('datatables');
        $this->datatables->select("payroll_items.employee_id AS id,
                                    payroll_items.payroll_id AS payroll_id,
                                    payroll_items.contract_id AS contract_id,
                                    LCASE(branch.name) AS branch,
                                    LCASE(" . $this->db->dbprefix('companies') . ".name),
                                    SUM(IF(earned_deduction = 1,
                                        (
                                            IF((payroll_concept_id = ".MONEY_VACATION." OR payroll_concept_id = ".SERVICE_BONUS." OR payroll_concept_id = ".LAYOFFS." OR payroll_concept_id = ".LAYOFFS_INTERESTS."),
                                            (
                                                IF(paid_to_employee = 1, amount,  0)
                                            ),
                                            amount)
                                        ), 0)
                                    ) AS earned,
                                    SUM(IF(earned_deduction = 2, amount, 0)) AS deduction,
                                    SUM(IF(earned_deduction = 1,
                                        (
                                            IF((payroll_concept_id = ".MONEY_VACATION." OR payroll_concept_id = ".SERVICE_BONUS." OR payroll_concept_id = ".LAYOFFS." OR payroll_concept_id = ".LAYOFFS_INTERESTS."),
                                            (
                                                IF(paid_to_employee = 1, amount, 0)
                                            ),
                                            amount)
                                        ), amount * - 1)
                                    ) AS total,
                                    SUM(IF(earned_deduction = 1,
                                        (
                                            IF((payroll_concept_id = ".MONEY_VACATION." OR payroll_concept_id = ".SERVICE_BONUS." OR payroll_concept_id = ".LAYOFFS." OR payroll_concept_id = ".LAYOFFS_INTERESTS."),
                                            (
                                                IF(paid_to_employee = 0, amount, 0)
                                            ),
                                            0)),
                                        0)
                                    ) AS provisions");
        $this->datatables->from("payroll_items");
        $this->datatables->join("companies", "companies.id = payroll_items.employee_id", "inner");
        $this->datatables->join("payroll_contracts", "payroll_contracts.id = payroll_items.contract_id", "inner");
        $this->datatables->join("companies branch", "branch.id = sma_payroll_contracts.biller_id", "inner");
        $this->datatables->where("payroll_id", $payroll_id);
        // $this->datatables->where("payroll_items.payroll_concept_id != ", LOAN_DISBURSEMENT);
        $this->datatables->group_by("payroll_items.contract_id, branch");
        $this->datatables->add_column("Actions", '<div class="text-center"><div class="btn-group text-left">
                                                    <button type="button" class="btn btn-default new-button btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                                                        <ul class="dropdown-menu pull-right" role="menu">
                                                            <li>
                                                                <a href="'. admin_url('payroll_management/see_employee_concepts/$1/$2/$3') .'" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i> ' . lang('payroll_management_see_details') . '</a>
                                                            </li>
                                                            <li class="option_add_payroll_concept">
                                                                <a href="'. admin_url('payroll_management/add_concept_employee/$1/$2/$3') .'" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> ' . lang('add_concept') . '</a>
                                                            </li>
                                                            <li class="option_remove_employee_payroll">
                                                                <a href="'. admin_url('payroll_management/remove_employee_payroll/$1/$2/$3') .'"><i class="fa fa-trash"></i> '. lang('remove_employee_payroll') .'</a>
                                                            </li>
                                                            <li class="option_pay_slip">
                                                                <a href="'. admin_url('payroll_management/pay_slip/$1/$2/$3') .'" target="_blank"><i class="fa fa-file-pdf-o"></i> '. lang('pay_slip') .'</a>
                                                            </li>
                                                            </ul>
                                                            </div>', "payroll_id, id, contract_id");
                                                            /* <li class="option_calculationParafiscalAndProvisions">
                                                                <a href="'. admin_url('payroll_management/calculationParafiscalAndProvisions/$1/$2/$3') .'"><i class="fa fa-refresh"></i> '. lang('calculationParafiscalAndProvisions') .'</a>
                                                            </li> */
        echo $this->datatables->generate();
    }

    public function see_employee_concepts(int $payroll_id, int $employee_id, int $contract_id)
    {
        $disabilities_data = [];
        $conceptsPaidEmployee = [];
        $payroll_items = $this->Payroll_management_model->get_payroll_items_by_payroll_id_employee_id($payroll_id, $employee_id, $contract_id);

        if (!empty($payroll_items)) {
            foreach ($payroll_items as $items) {
                $id = $items->id;
                $concept_id = $items->payroll_concept_id;
                $paidToEmployee = $items->paid_to_employee;
                if (($concept_id == PAID_LICENSE ||
                    $concept_id == COMMON_DISABILITY ||
                    $concept_id == WORK_DISABILITY ||
                    $concept_id == PROFESSIONAL_DISABILITY ||
                    $concept_id == UNEXCUSED_ABSENTEEISM ||
                    $concept_id == DISCIPLINARY_SUSPENSION ||
                    $concept_id == NOT_PAID_LICENSE ||
                    $concept_id == TIME_VACATION ||
                    $concept_id == FAMILY_ACTIVITY_PERMIT ||
                    $concept_id == DUEL_LICENSE ||
                    $concept_id == PAID_PERMIT ||
                    $concept_id == MATERNITY_LICENSE ||
                    $concept_id == PATERNITY_LICENSE ||
                    $concept_id == NOT_PAID_PERMIT ||
                    $concept_id == SUNDAY_DISCOUNT)) {
                    $disabilities_data[$concept_id][$id]["cantidad"] = $items->days_quantity;
                    $disabilities_data[$concept_id][$id]["id"] = $id;
                    if ($items->earned_deduction == EARNED) {
                        $disabilities_data[$concept_id][$id]["devengado"] = $items->amount;
                        $disabilities_data[$concept_id][$id]["deducido"] = 0;
                    } else {
                        $disabilities_data[$concept_id][$id]["devengado"] = 0;
                        $disabilities_data[$concept_id][$id]["deducido"] = $items->amount;
                    }
                }

                if (($concept_id == LAYOFFS || $concept_id == LAYOFFS_INTERESTS || $concept_id == MONEY_VACATION || $concept_id == SERVICE_BONUS) && $paidToEmployee == NOT) {
                    $conceptsPaidEmployee[$concept_id][$id]["cantidad"] = $items->days_quantity;
                    $conceptsPaidEmployee[$concept_id][$id]["id"] = $id;
                    if ($items->earned_deduction == EARNED) {
                        $conceptsPaidEmployee[$concept_id][$id]["devengado"] = $items->amount;
                        $conceptsPaidEmployee[$concept_id][$id]["deducido"] = 0;
                    } else {
                        $conceptsPaidEmployee[$concept_id][$id]["devengado"] = 0;
                        $conceptsPaidEmployee[$concept_id][$id]["deducido"] = $items->amount;
                    }
                }
            }
        }

        $payroll = $this->Payroll_management_model->get_by_id($payroll_id);
        $hideShowParafiscalProvisions = FALSE;
        /* if ($payroll->frequency == MONTHLY) {
            $hideShowParafiscalProvisions = TRUE;
        } else if ($payroll->frequency == BIWEEKLY) {
            if ($payroll->number == SECOND_FORTNIGHT) {
                $hideShowParafiscalProvisions = TRUE;
            }
        } */

        if ($hideShowParafiscalProvisions == TRUE) {
            $this->data["provisions"] = $this->Payroll_provisions_model->get_by_payroll_id_employee_id($payroll_id, $employee_id, $contract_id);
            $this->data["social_security_parafiscal"] = $this->Payroll_social_security_parafiscal_model->get_by_payroll_id_employee_id($payroll_id, $employee_id, $contract_id);
        }

        $this->data["payroll"] = $payroll;
        $this->data["payroll_id"] = $payroll_id;
        $this->data["employee_id"] = $employee_id;
        $this->data["contract_id"] = $contract_id;
        $this->data["disabilities"] = $disabilities_data;
        $this->data["conceptsPaidEmployee"] = $conceptsPaidEmployee;
        $this->data['hideShowParafiscalProvisions'] = $hideShowParafiscalProvisions;
        $this->data["page_title"] = lang("payroll_management_see_employee_concepts");
        $this->data["employee_data"] = $this->Employees_model->get_by_id($employee_id, $contract_id);
        $this->load_view($this->theme . "payroll_management/see_employee_concepts", $this->data);
    }

    public function get_employee_concepts_datatables($payroll_id, $employee_id, $contract_id)
    {
        $concepts_not_allowed = [
            /* LOAN_DISBURSEMENT, */
            COMMON_DISABILITY,
            WORK_DISABILITY,
            PROFESSIONAL_DISABILITY,
            UNEXCUSED_ABSENTEEISM,
            DISCIPLINARY_SUSPENSION,
            NOT_PAID_LICENSE,
            TIME_VACATION,
            PAID_LICENSE,
            FAMILY_ACTIVITY_PERMIT,
            DUEL_LICENSE,
            PAID_PERMIT,
            MATERNITY_LICENSE,
            PATERNITY_LICENSE,
            NOT_PAID_PERMIT,
            SUNDAY_DISCOUNT
        ];

        $this->load->library('datatables');
        $this->datatables->select("payroll_items.id,
                                payroll_concepts.name,
                                IF(payroll_concept_type = ".OVERTIME_OR_SURCHARGES.", hours_quantity, days_quantity) AS time_amount,
                                IF(earned_deduction = 1, amount , '') AS earned,
                                IF(earned_deduction = 2, amount , '') AS deduction,
                                payroll_items.payroll_concept_id,
                                IF(payroll_concept_type = ".OVERTIME_OR_SURCHARGES.", 'horas', 'días') AS time_unit,
                                payroll_concept_type.automatic,
                                payroll_concepts.id AS payroll_concepts_id,
                                payroll_items.description,
                                payroll_items.paid_to_employee");
        $this->datatables->from("payroll_items");
        $this->datatables->join("payroll_concepts", "payroll_concepts.id = payroll_items.payroll_concept_id", "inner");
        $this->datatables->join("payroll_concept_type", "payroll_concept_type.id = payroll_concepts.concept_type_id", "inner");
        $this->datatables->where(["payroll_items.payroll_id" => $payroll_id, 'payroll_items.contract_id'=>$contract_id, "payroll_items.employee_id" => $employee_id]);
        $this->datatables->where_not_in("payroll_items.payroll_concept_id", $concepts_not_allowed);
        $this->datatables->order_by("earned_deduction", "desc");
        $this->datatables->order_by("payroll_items.payroll_concept_id", "ASC");

        echo $this->datatables->generate();
    }

    public function add_concept_employee(int $payroll_id, int $employee_id, int $contract_id)
    {
        $this->sma->checkPermissions("add_novelty", NULL, "payroll_management");

        $this->data["payroll_id"] = $payroll_id;
        $this->data["employee_id"] = $employee_id;
        $this->data["page_title"] = lang("add_concept");
        $this->data["concepts"] = $this->Payroll_concepts_model->get_not_automatic();
        $this->data["payroll_data"] = $this->Payroll_management_model->get_by_id($payroll_id);
        $this->data["contract_data"] = $this->Payroll_contracts_model->get_by_id($contract_id);
        $this->data['employee_data'] = $this->Employees_model->get_by_id($employee_id, $contract_id);

        $this->load_view($this->theme . "payroll_management/add_concept_employee", $this->data);
    }

    public function save_concept_employee()
    {
        $this->sma->checkPermissions("add_novelty", NULL, "payroll_management");

        $hours_quantity = 0;
        $end_date = $this->input->post("end_date");
        $start_date = $this->input->post("start_date");
        $payroll_id = $this->input->post("payroll_id");
        $contract_id = $this->input->post("contract_id");
        $employee_id = $this->input->post("employee_id");
        $concept_type_id = $this->input->post("concept_type");
        $concept_id = $this->input->post("payroll_concept_id");
        $earned_deduction = $this->input->post("earned_deduction");
        $payroll_data = $this->Payroll_management_model->get_by_id($payroll_id);

        $this->form_validation->set_rules("payroll_concept_id", lang("payroll_concepts"), 'trim|required');

        if ($concept_id == COMPANY_LOAN || $concept_id == FREE_INVESTMENT_LOAN) {
            $frequency = RECURRENT;
            $this->form_validation->set_rules("amount", lang('parroll_management_value'), "trim|required");
            $this->form_validation->set_rules("amount_fees", lang('parroll_management_amount_fees'), "trim|required");
        } else if ($concept_id == COMMISSIONS || $concept_id == PER_DIEM_CONSTITUTES_SALARY ||
            $concept_id == INCENTIVES || $concept_id == BONUSES_CONSTITUTING_SALARY ||
            $concept_id == BONUSES_NOT_CONSTITUTING_SALARY || $concept_id == BEARING_ALLOWANCE ||
            $concept_id == MAINTENANCE_ALLOWANCE || $concept_id == PER_DIEM_NOT_CONSTITUTES_SALARY ||
            $concept_id == FOOD_BOND || $concept_id == LAYOFFS ||
            $concept_id == LAYOFFS_INTERESTS || $concept_id == PROVISION_LAYOFFS_INTERESTS ||
            $concept_id == MONEY_VACATION || $concept_id == WITHHOLDING ||
            $concept_id == SOLIDARITY_FUND_EMPLOYEES_1 || $concept_id == SOLIDARITY_FUND_EMPLOYEES_1_2 ||
            $concept_id == SOLIDARITY_FUND_EMPLOYEES_1_4 || $concept_id == SOLIDARITY_FUND_EMPLOYEES_1_6 ||
            $concept_id == SOLIDARITY_FUND_EMPLOYEES_1_8 || $concept_id == SOLIDARITY_FUND_EMPLOYEES_2 ||
            $concept_id == COMPENSATION || $concept_id == FISCAL_SEIZURES) {
            $this->form_validation->set_rules("amount", lang('parroll_management_value'), "trim|required");

            if ($this->input->post("frequency") == RECURRENT) {
                $this->form_validation->set_rules("number_of_time", lang('payroll_management_number_time'), "trim|required|integer");
            }

            if ($concept_id == LAYOFFS_INTERESTS || $concept_id == PROVISION_LAYOFFS_INTERESTS) {
                $this->form_validation->set_rules("working_days", lang('working_days'), "trim|required|integer");
            }
        } else if ($concept_id == DAYTIME_OVERTIME || $concept_id == NIGHT_OVERTIME || $concept_id == NIGHT_SURCHARGE || $concept_id == HOLIDAY_DAYTIME_OVERTIME || $concept_id == HOLIDAY_DAYTIME_SURCHARGE || $concept_id == HOLIDAY_NIGHT_OVERTIME || $concept_id == HOLIDAY_NIGHT_SURCHARGE ) {
            $this->form_validation->set_rules("overtime_start_date[]", lang("date"), 'trim|required');
        } else if ($concept_id == COMMON_DISABILITY || $concept_id == WORK_DISABILITY || $concept_id == PROFESSIONAL_DISABILITY) {
            $this->form_validation->set_rules('end_date', lang('end_date'), 'trim|required');
        } else if ($concept_id == PAID_LICENSE || $concept_id == MATERNITY_LICENSE || $concept_id == PATERNITY_LICENSE || $concept_id == DUEL_LICENSE || $concept_id == NOT_PAID_LICENSE || $concept_id == FAMILY_ACTIVITY_PERMIT || $concept_id == NOT_PAID_PERMIT || $concept_id == DISCIPLINARY_SUSPENSION || $concept_id == UNEXCUSED_ABSENTEEISM ||
            $concept_id == TIME_VACATION || $concept_id == PAID_PERMIT || $concept_id == SUNDAY_DISCOUNT) {
            $this->form_validation->set_rules('end_date', lang('end_date'), 'trim|required');
        }

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "payroll_id" => $payroll_id,
                "contract_id" => $contract_id,
                "employee_id" => $employee_id,
                "payroll_concept_type" => $concept_type_id,
                "payroll_concept_id" => $concept_id,
                "earned_deduction" => $earned_deduction,
                "creation_date" => date("Y-m-d"),
                'created_by' => $this->session->userdata('user_id')
            ];

            if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                if ($concept_id == COMPANY_LOAN || $concept_id == FREE_INVESTMENT_LOAN) {
                    $data["payroll_concept_type"] = OTHER_ACCRUED_TYPE;
                    $data["payroll_concept_id"] = LOAN_DISBURSEMENT;
                    $data["earned_deduction"] = EARNED;
                    $data["amount"] = $this->input->post("amount");
                    $data["start_date"] = $this->input->post("start_date");
                    $data["end_date"] = date("Y-m-d", strtotime($start_date . " + " . $this->input->post("amount_fees") . " month"));
                    $data["frequency"] = $this->input->post("frequency");
                    $data["description"] = $this->input->post("description");
                } else if ($concept_id == COMMISSIONS || $concept_id == INCENTIVES || $concept_id == BONUSES_CONSTITUTING_SALARY || $concept_id == PER_DIEM_CONSTITUTES_SALARY || $concept_id == BONUSES_NOT_CONSTITUTING_SALARY ||         $concept_id == BEARING_ALLOWANCE || $concept_id == MAINTENANCE_ALLOWANCE || $concept_id == PER_DIEM_NOT_CONSTITUTES_SALARY || $concept_id == FOOD_BOND) {
                    $data["amount"] = $this->input->post("amount");
                    $data["start_date"] = $start_date;
                    $data["end_date"] = $this->calculate_end_date_future_item();
                    $data["hours_quantity"] = $hours_quantity;
                    $data["frequency"] = $this->input->post("frequency");
                    $data["description"] = $this->input->post("description");
                } else if ($concept_id == DAYTIME_OVERTIME || $concept_id == NIGHT_OVERTIME || $concept_id == NIGHT_SURCHARGE || $concept_id == HOLIDAY_DAYTIME_OVERTIME || $concept_id == HOLIDAY_DAYTIME_SURCHARGE || $concept_id == HOLIDAY_NIGHT_OVERTIME || $concept_id == HOLIDAY_NIGHT_SURCHARGE) {
                    $data = $this->get_overtime_data();
                } else if ($concept_id == COMMON_DISABILITY || $concept_id == WORK_DISABILITY || $concept_id == PROFESSIONAL_DISABILITY) {
                    $disability_days = $this->calculate_number_days($payroll_id, $concept_id);
                    $data["amount"] = $this->calculate_concept_value($concept_id, $employee_id, $disability_days, $contract_id);
                    $data["start_date"] = $this->input->post("start_date");
                    $data["end_date"] = $this->input->post("end_date");
                    $data["frequency"] = $this->input->post("frequency");
                    $data["days_quantity"] = $disability_days;
                } else if ($concept_id == PAID_LICENSE || $concept_id == MATERNITY_LICENSE || $concept_id == PATERNITY_LICENSE || $concept_id == DUEL_LICENSE || $concept_id == NOT_PAID_LICENSE || $concept_id == FAMILY_ACTIVITY_PERMIT || $concept_id == NOT_PAID_PERMIT || $concept_id == DISCIPLINARY_SUSPENSION || $concept_id == UNEXCUSED_ABSENTEEISM || $concept_id == PAID_PERMIT || $concept_id == SUNDAY_DISCOUNT) {
                    $days_quantity = $this->calculate_number_days($payroll_id, $concept_id);
                    $data["amount"] = ($concept_id == NOT_PAID_LICENSE || $concept_id == NOT_PAID_PERMIT || $concept_id == DISCIPLINARY_SUSPENSION || $concept_id == UNEXCUSED_ABSENTEEISM || $concept_id == SUNDAY_DISCOUNT)
                        ? 0
                        : $this->calculate_amount_days($employee_id, $days_quantity, $contract_id);
                    $data["start_date"] = $start_date;
                    $data["end_date"] = $this->input->post('end_date');
                    $data["days_quantity"] = $days_quantity;
                    $data["frequency"] = $this->Payroll_settings->payment_frequency;
                } else if ($concept_id == TIME_VACATION) {
                    $days_quantity = $this->calculate_number_days($payroll_id, $concept_id);
                    $data["amount"] = $this->calculate_amount_days($employee_id, $days_quantity, $contract_id);
                    $data["end_date"] = $this->input->post("end_date");
                    $data["start_date"] = $this->input->post("start_date");
                    $data["days_quantity"] = $days_quantity;
                } else if ($concept_id == MONEY_VACATION) {
                    $data["amount"] = $this->input->post("amount");
                    $data["days_quantity"] = $this->input->post('days');
                    $data["description"] = $this->input->post("description");
                    $data["paid_to_employee"] = ($this->input->post('paid_to_employee') ? 1 : 0);
                } else if ($concept_id == SERVICE_BONUS) {
                    $data["amount"] = $this->input->post("amount");
                    $data["days_quantity"] = $this->input->post("working_days");
                    $data["description"] = $this->input->post("description");
                    $data["paid_to_employee"] = ($this->input->post('paid_to_employee') ? 1 : 0);
                } else if ($concept_id == LAYOFFS) {
                    $data["amount"] = $this->input->post("amount");
                    $data["days_quantity"] = $this->input->post("working_days");
                    $data["paid_to_employee"] = ($this->input->post('paid_to_employee') ? 1 : 0);
                } else if ($concept_id == LAYOFFS_INTERESTS) {
                    $percentage = $this->Payroll_settings->layoffs_interests / 100;
                    if ($this->input->post('paid_to_employee')) {
                        $total_value = ($this->input->post("amount") * $this->input->post("working_days") * $percentage) / 360;
                    } else {
                        $total_value = $this->input->post("amount") * $percentage;
                    }
                    $data["days_quantity"] = $this->input->post("working_days");
                    $data["amount"] = $total_value;
                    $data["paid_to_employee"] = ($this->input->post('paid_to_employee') ? 1 : 0);
                } else if ($concept_id == ENDOWMENT || $concept_id == COMPENSATION || $concept_id ==FISCAL_SEIZURES ) {
                    $data["amount"] = $this->input->post("amount");
                } else {
                    $data["frequency"] = 0;
                    $data["hours_quantity"] = $hours_quantity;
                    $data["amount"] = $this->input->post("amount");
                    $data["end_date"] = $this->input->post("end_date");
                    $data["start_date"] = $this->input->post("start_date");
                    $data["description"] = $this->input->post("description");
                }
            } else if ($this->Payroll_settings->payment_frequency == MONTHLY) {
                if ($concept_id == COMPANY_LOAN || $concept_id == FREE_INVESTMENT_LOAN) {
                    $data["payroll_concept_type"] = OTHER_ACCRUED_TYPE;
                    $data["payroll_concept_id"] = LOAN_DISBURSEMENT;
                    $data["earned_deduction"] = EARNED;
                    $data["amount"] = $this->input->post("amount");
                    $data["start_date"] = $this->input->post("start_date");
                    $data["end_date"] = date("Y-m-d", strtotime($start_date . " + " . $this->input->post("amount_fees") . " month"));
                    $data["frequency"] = $this->input->post("frequency");
                    $data["description"] = $this->input->post("description");
                } else if ($concept_id == COMMISSIONS || $concept_id == INCENTIVES || $concept_id == BONUSES_CONSTITUTING_SALARY || $concept_id == PER_DIEM_CONSTITUTES_SALARY || $concept_id == BONUSES_NOT_CONSTITUTING_SALARY || $concept_id == BEARING_ALLOWANCE || $concept_id ==  MAINTENANCE_ALLOWANCE || $concept_id == PER_DIEM_NOT_CONSTITUTES_SALARY || $concept_id == FOOD_BOND) {
                    $data["amount"] = $this->input->post("amount");
                    $data["start_date"] = $this->input->post("start_date");
                    $data["end_date"] = $this->calculate_end_date_future_item();
                    $data["frequency"] = $this->Payroll_settings->payment_frequency;
                    $data["description"] = $this->input->post("description");
                } else if ($concept_id == DAYTIME_OVERTIME || $concept_id == NIGHT_OVERTIME || $concept_id == NIGHT_SURCHARGE || $concept_id == HOLIDAY_DAYTIME_OVERTIME || $concept_id == HOLIDAY_DAYTIME_SURCHARGE || $concept_id == HOLIDAY_NIGHT_OVERTIME || $concept_id == HOLIDAY_NIGHT_SURCHARGE) {
                    $data = $this->get_overtime_data();
                } else if ($concept_id == COMMON_DISABILITY || $concept_id == WORK_DISABILITY || $concept_id == PROFESSIONAL_DISABILITY) {
                    $disability_days = $this->calculate_number_days($payroll_id, $concept_id);
                    $data["hours_quantity"] = 0;
                    $data["start_date"] = $this->input->post("start_date");
                    $data["days_quantity"] = $disability_days;
                    $data["frequency"] = $this->Payroll_settings->payment_frequency;
                    $data["end_date"] = date('Y-m-d', strtotime($this->input->post("start_date") . ' + ' . ($disability_days - 1) . ' days'));
                    $data["amount"] = $this->calculate_concept_value($this->input->post("payroll_concept_id"), $this->input->post("employee_id"), $disability_days, $this->input->post("contract_id"));
                } else if ($concept_id == PAID_LICENSE || $concept_id == MATERNITY_LICENSE || $concept_id == PATERNITY_LICENSE || $concept_id == DUEL_LICENSE || $concept_id == NOT_PAID_LICENSE || $concept_id == FAMILY_ACTIVITY_PERMIT || $concept_id == NOT_PAID_PERMIT || $concept_id == DISCIPLINARY_SUSPENSION || $concept_id == UNEXCUSED_ABSENTEEISM || $concept_id == PAID_PERMIT || $concept_id == SUNDAY_DISCOUNT) {
                    $days_quantity = $this->calculate_number_days($payroll_id, $concept_id);
                    $data["amount"] = ($concept_id == NOT_PAID_LICENSE || $concept_id == NOT_PAID_PERMIT || $concept_id == DISCIPLINARY_SUSPENSION || $concept_id == UNEXCUSED_ABSENTEEISM || $concept_id == SUNDAY_DISCOUNT)
                        ? 0
                        : $this->calculate_amount_days($employee_id, $days_quantity, $contract_id);
                    $data["start_date"] = $this->input->post("start_date");
                    $data["end_date"] = $this->input->post("end_date");
                    $data["days_quantity"] = $days_quantity;
                    $data["frequency"] = $this->Payroll_settings->payment_frequency;
                } else if ($concept_id == TIME_VACATION) {
                    $days_quantity = $this->calculate_number_days($payroll_id, $concept_id);
                    $data["amount"] = $this->calculate_amount_days($employee_id, $days_quantity, $contract_id);
                    $data["end_date"] = $this->input->post("end_date");
                    $data["start_date"] = $this->input->post("start_date");
                    $data["days_quantity"] = $days_quantity;
                } else if ($concept_id == MONEY_VACATION) {
                    $data["amount"] = $this->input->post("amount");
                    $data["description"] = $this->input->post("description");
                    $data["days_quantity"] = $this->input->post('days');
                    $data["paid_to_employee"] = ($this->input->post('paid_to_employee') ? 1 : 0);
                } else if ($concept_id == SERVICE_BONUS) {
                    $data["amount"] = $this->input->post("amount");
                    $data["days_quantity"] = $this->input->post("working_days");
                    $data["description"] = $this->input->post("description");
                    $data["paid_to_employee"] = ($this->input->post('paid_to_employee') ? 1 : 0);
                } else if ($concept_id == LAYOFFS) {
                    $data["amount"] = $this->input->post("amount");
                    $data["days_quantity"] = $this->input->post("working_days");
                    $data["paid_to_employee"] = ($this->input->post('paid_to_employee') ? 1 : 0);
                } else if ($concept_id == LAYOFFS_INTERESTS) {
                    $concept_data = $this->Payroll_concepts_model->get_by_id($concept_id);
                    $percentage = ($concept_data->percentage / 100);
                    if ($this->input->post('paid_to_employee')) {
                        $total_value = ($this->input->post("amount") * $this->input->post("working_days") * $percentage) / 360;
                    } else {
                        $total_value = $this->input->post("amount") * $percentage;
                    }
                    $numberDaysCalculation = ($this->input->post('paid_to_employee')) ? 360 : 30;
                    $data["days_quantity"] = $this->input->post("working_days");
                    $data["amount"] = $total_value;
                    $data["paid_to_employee"] = ($this->input->post('paid_to_employee') ? 1 : 0);
                } else if ($concept_id == ENDOWMENT || $concept_id == COMPENSATION || $concept_id == FISCAL_SEIZURES) {
                    $data["amount"] = $this->input->post("amount");
                } else {
                    $data["frequency"] = 0;
                    $data["hours_quantity"] = $hours_quantity;
                    $data["amount"] = $this->input->post("amount");
                    $data["end_date"] = $this->input->post("end_date");
                    $data["start_date"] = $this->input->post("start_date");
                    $data["description"] = $this->input->post("description");
                }
            }

            if (isset($data) && !empty($data)) {
                $items_payroll_saved = $this->Payroll_management_model->insert_item($data);
                if ($items_payroll_saved != FALSE) {
                    $future_concepts_data = $this->get_future_concepts_data($payroll_data, $items_payroll_saved);
                    if (!empty($future_concepts_data)) {
                        if ($this->Payroll_management_model->insert_future_items($future_concepts_data) == TRUE) {
                            if ($concept_id == COMMON_DISABILITY || $concept_id == WORK_DISABILITY ||
                            $concept_id == PROFESSIONAL_DISABILITY || $concept_id == PAID_LICENSE ||
                            $concept_id == MATERNITY_LICENSE || $concept_id == PATERNITY_LICENSE ||
                            $concept_id == DUEL_LICENSE || $concept_id == PAID_LICENSE ||
                            $concept_id == FAMILY_ACTIVITY_PERMIT || $concept_id == DISCIPLINARY_SUSPENSION ||
                            $concept_id == NOT_PAID_LICENSE || $concept_id == TIME_VACATION ||
                            $concept_id == UNEXCUSED_ABSENTEEISM || $concept_id == PAID_PERMIT ||
                            $concept_id == NOT_PAID_PERMIT || $concept_id == SUNDAY_DISCOUNT) {
                                $this->calculate_salary($this->input->post("payroll_id"), $this->input->post("employee_id"), $this->calculate_number_days($payroll_id, $concept_id), $contract_id);
                                $applyHolidays =  $concept_id == TIME_VACATION ? FALSE : TRUE;
                                $this->calculate_transportation_allowance($this->input->post("payroll_id"), $this->input->post("employee_id"), $this->calculate_number_days($payroll_id, $concept_id, $applyHolidays), $contract_id, $concept_id);
                            }

                            if ($concept_id == LAYOFFS || $concept_id == LAYOFFS_INTERESTS || $concept_id == TIME_VACATION || $concept_id == MONEY_VACATION || $concept_id == SERVICE_BONUS) {
                            } else {
                                $this->calculate_social_security_by_employee_id($payroll_id, $employee_id, $contract_id);
                                $this->calculate_social_security_parafiscal($payroll_id, $employee_id, $contract_id);
                            }
                            $this->calculate_payroll_earned_deductions($payroll_id);

                            $this->session->set_flashdata("message", lang("payroll_management_saved_item"));
                        } else {
                            $this->Payroll_management_model->delete_item($items_payroll_saved);

                            $this->session->set_flashdata("error", lang("payroll_management_not_saved_item"));
                        }
                    } else {
                        if ($concept_id == COMMON_DISABILITY || $concept_id == WORK_DISABILITY ||
                        $concept_id == PROFESSIONAL_DISABILITY || $concept_id == PAID_LICENSE ||
                        $concept_id == MATERNITY_LICENSE || $concept_id == PATERNITY_LICENSE ||
                        $concept_id == DUEL_LICENSE || $concept_id == PAID_LICENSE ||
                        $concept_id == FAMILY_ACTIVITY_PERMIT || $concept_id == DISCIPLINARY_SUSPENSION ||
                        $concept_id == NOT_PAID_LICENSE || $concept_id == TIME_VACATION ||
                        $concept_id == UNEXCUSED_ABSENTEEISM || $concept_id == PAID_PERMIT ||
                        $concept_id == NOT_PAID_PERMIT || $concept_id == SUNDAY_DISCOUNT) {
                            $this->calculate_salary($this->input->post("payroll_id"), $this->input->post("employee_id"), $this->calculate_number_days($payroll_id, $concept_id), $contract_id);
                            $applyHolidays =  $concept_id == TIME_VACATION ? FALSE : TRUE;
                            $this->calculate_transportation_allowance($this->input->post("payroll_id"), $this->input->post("employee_id"), $this->calculate_number_days($payroll_id, $concept_id, $applyHolidays), $contract_id, $concept_id);
                        }

                        if ($concept_id == LAYOFFS || $concept_id == LAYOFFS_INTERESTS || $concept_id == TIME_VACATION || $concept_id == MONEY_VACATION || $concept_id == SERVICE_BONUS) {
                        } else {
                            $this->calculate_social_security_by_employee_id($payroll_id, $employee_id, $contract_id);
                            $this->calculate_social_security_parafiscal($payroll_id, $employee_id, $contract_id);
                            // $this->calculate_provisions($payroll_id, $employee_id, $contract_id);
                        }
                        $this->calculate_payroll_earned_deductions($payroll_id);

                        $this->session->set_flashdata("message", lang("payroll_management_saved_item"));
                    }
                } else {
                    $this->session->set_flashdata("error", lang("payroll_management_not_saved_item"));
                }
            } else {
                $future_concepts_data = $this->get_future_concepts_data($payroll_data);
                if (!empty($future_concepts_data)) {
                    if ($this->Payroll_management_model->insert_future_items($future_concepts_data) == TRUE) {
                        if ($concept_id == LAYOFFS || $concept_id == LAYOFFS_INTERESTS || $concept_id == TIME_VACATION || $concept_id == MONEY_VACATION || $concept_id == SERVICE_BONUS) {

                        } else {
                            $this->calculate_social_security_by_employee_id($payroll_id, $employee_id, $contract_id);
                            $this->calculate_social_security_parafiscal($payroll_id, $employee_id, $contract_id);
                            // $this->calculate_provisions($payroll_id, $employee_id, $contract_id);
                        }
                        $this->calculate_payroll_earned_deductions($payroll_id);

                        $this->session->set_flashdata("message", lang("payroll_management_saved_item"));
                    } else {
                        $this->session->set_flashdata("error", lang("payroll_management_not_saved_item"));
                    }
                } else {
                    if ($concept_id == LAYOFFS || $concept_id == LAYOFFS_INTERESTS || $concept_id == TIME_VACATION || $concept_id == MONEY_VACATION || $concept_id == SERVICE_BONUS) {

                    } else {
                        $this->calculate_social_security_by_employee_id($payroll_id, $employee_id, $contract_id);
                        $this->calculate_social_security_parafiscal($payroll_id, $employee_id, $contract_id);
                        // $this->calculate_provisions($payroll_id, $employee_id, $contract_id);
                    }
                    $this->calculate_payroll_earned_deductions($payroll_id);

                    $this->session->set_flashdata("message", lang("payroll_management_saved_item"));
                }
            }            
            
            if (!in_array($concept_id, [LAYOFFS, LAYOFFS_INTERESTS, SERVICE_BONUS, MONEY_VACATION])) {
                $this->calculateProvisions($payroll_id, $contract_id, $employee_id);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function add()
    {
        $this->sma->checkPermissions("add", NULL, "payroll_management");

        $payroll = $this->Payroll_management_model->get_last_payroll();
        $this->data["payroll"] = $payroll;
        $this->data["page_title"] = lang("payroll_management_add");
        $this->load_view($this->theme . "payroll_management/add", $this->data);
    }

    public function save()
    {
        $this->sma->checkPermissions("add", NULL, "payroll_management");

        if (! $this->Payroll_settings->payment_frequency) {
            $this->session->set_flashdata("error", "No se ha configurado la frecuencia de pago en Ajustes de nómina");
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules("month", lang("payroll_management_month"), 'trim|required');
        if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
            $this->form_validation->set_rules("number", lang("payroll_management_biweekly"), 'trim|required');
        }

        if ($this->form_validation->run() == TRUE) {
            $creation_date = date("Y-m-d");
            $year = $this->input->post("year");
            $month = $this->input->post("month");
            $number = $this->input->post("number");
            $dates_payroll = $this->get_dates_payroll($year, $month, $number);

            $this->validate_existing_payroll($year, $month, $number);
            $this->validate_creation_next_payroll($year, $month, $number);
            $this->validate_contract_salaries();

            $data = [
                "code" => $this->get_code_payroll($year,$month, $number),
                "creation_date" => $creation_date,
                "biller_id" => $this->Settings->default_biller,
                "year" => $year,
                "month" => $month,
                "frequency" => $this->Payroll_settings->payment_frequency,
                "number" => $number,
                "start_date" => $dates_payroll->start_date,
                "end_date" => $dates_payroll->end_date,
                'created_by' => $this->session->userdata('user_id')
            ];

            $payroll_saved = $this->Payroll_management_model->insert($data);
            if ($payroll_saved != FALSE) {
                $this->saveItems($payroll_saved);
            } else {
                $this->session->set_flashdata("error", lang("payroll_management_not_saved") . "no payroll");
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata("error", validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function saveItems($payrollSaved) 
    {
        $creation_date = date("Y-m-d");

        $itemData        = $this->get_item_data($payrollSaved, $creation_date);
        $amountEmployees = $itemData["amount_employees"];
        $itemsData       = $itemData["items_data"]; 
        
        if (!empty($itemsData)) {
            $itemsPayrollSaved = $this->Payroll_management_model->insert_items($itemsData);
            if ($itemsPayrollSaved == TRUE) {
                $payrollData = $this->Payroll_management_model->get_by_id($payrollSaved);
                $employessProcess = [];
                foreach ($itemsData as $item) {
                    $item = (object) $item;

                    if (!in_array($item->employee_id, $employessProcess)) {
                        $this->calculate_social_security_by_employee_id($payrollData->id, $item->employee_id, $item->contract_id);
                        $this->calculateProvisions($payrollData->id, $item->contract_id, $item->employee_id);
                        $employessProcess[] = $item->employee_id;
                    }
                }

                $this->calculate_payroll_earned_deductions($payrollSaved, ["employee_no" => $amountEmployees]);
                $this->detect_contract_termination();

                $this->session->set_flashdata("message", lang("payroll_management_saved"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->Payroll_management_model->delete_items($payrollSaved);
                $this->Payroll_management_model->delete($payrollSaved);
                $this->detect_contract_termination();

                $this->session->set_flashdata("error", lang("payroll_management_not_saved") . " No fue posible almacenar los items de la nómina");
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->Payroll_management_model->delete($payrollSaved);

            $this->session->set_flashdata("error", lang("payroll_management_not_saved") . ". " . lang("payroll_management_no_contracts"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function calculateProvisions($payrollId, $contractId = null, $employeeId = null)
    {
        $payroll = $this->Payroll_management_model->get_by_id($payrollId);
        $number = $payroll->number;

        if ($this->allowAddingProvisions($number)) {
            $payrollIds   = $this->Payroll_management_model->get_payroll_grouped_by_month($payroll->year, $payroll->month);
            $payrollItems = $this->Payroll_management_model->get_payroll_items_by_payroll_id($payrollIds->ids, $employeeId, $contractId);

            $payrollConceptsPerEmployee = [];
            foreach ($payrollItems as $payrollItem) {
                $payrollConceptsPerEmployee[$payrollItem->employee_id][] = $payrollItem;
            }

            foreach ($payrollConceptsPerEmployee as $employeeId => $items) {
                $transportationAllowance = 0;
                $overtimeHours           = 0;
                $bonification            = 0;
                $commissions             = 0;
                $disability              = 0;
                $licenses                = 0;
                $salary                  = 0;  

                $contract = $this->Payroll_contracts_model->get_by_employee_id($employeeId, $items[0]->contract_id);
                $transportationAllowance = ($contract->trans_allowance == YES) ? $this->Payroll_settings->transportation_allowance_value : 0;

                foreach ($items as $item) {
                    $conceptId      = $item->payroll_concept_id;                    
                    $contractId     = $item->contract_id;
                    $amount         = $item->amount;

                    if ($conceptId == SALARY) {
                        $salary += $amount;
                    } else  if (in_array($conceptId, [DAYTIME_OVERTIME, NIGHT_OVERTIME, NIGHT_SURCHARGE, 
                        HOLIDAY_DAYTIME_OVERTIME, HOLIDAY_DAYTIME_SURCHARGE, HOLIDAY_NIGHT_OVERTIME, 
                        HOLIDAY_NIGHT_SURCHARGE])) {
                        $overtimeHours += $amount;
                    } else if (in_array($conceptId, [COMMON_DISABILITY, PROFESSIONAL_DISABILITY, 
                        WORK_DISABILITY])) {
                        $disability += $amount;
                    } else if ($conceptId == BONUSES_CONSTITUTING_SALARY) {
                        $bonification += $amount;
                    } else if ($conceptId == COMMISSIONS) {
                        $commissions += $amount;
                    } else if (in_array($conceptId, [PAID_LICENSE, DUEL_LICENSE, MATERNITY_LICENSE, 
                        PATERNITY_LICENSE])) {
                        $licenses += $amount;
                    }
                }
                
                $payrollData = (object) [
                    "payrollId"  => $payrollId,
                    "employeeId" => $employeeId,
                    "contractId"     => $contractId,
                ];
                $amounts = (object) [
                    "salary" => $salary,
                    "overtimeHours" => $overtimeHours,
                    "transportationAllowance" => $transportationAllowance,
                    "disability" => $disability,
                    "bonification" => $bonification,
                    "commissions" => $commissions,
                    "licenses" => $licenses,
                    "numberOfDays" => $this->getDateRangeForPayroll($payrollId, $contractId),
                ];
                $this->saveProvisionConcepts($payrollData, $amounts);
            }
        }
    }

    private function saveProvisionConcepts($payrollData, $amounts) 
    {
        $itemsData = [];
        $provisionsConcepts = [LAYOFFS, LAYOFFS_INTERESTS, SERVICE_BONUS, MONEY_VACATION];
        
        foreach ($provisionsConcepts as $concept) {            
            $conceptData = $this->Payroll_concepts_model->get_by_id($concept);

            $this->Payroll_management_model->delete_items_by_employee_id($payrollData->payrollId, $payrollData->employeeId, $payrollData->contractId, $conceptData->id);

            if ($concept == LAYOFFS || $concept == SERVICE_BONUS) {
                $amount = ($amounts->salary + $amounts->overtimeHours + $amounts->transportationAllowance + $amounts->disability + $amounts->bonification + $amounts->commissions) * ($conceptData->percentage / 100);
            } else if ($concept == LAYOFFS_INTERESTS) {
                $layoff = $this->Payroll_concepts_model->get_by_id(LAYOFFS);
                $amountLayoff = ($amounts->salary + $amounts->overtimeHours + $amounts->transportationAllowance + $amounts->disability + $amounts->bonification + $amounts->commissions) * ($layoff->percentage / 100);
                $amount = $amountLayoff * ($conceptData->percentage / 100);
            } else if ($concept == MONEY_VACATION) {
                if ($this->Payroll_settings->exempt_concepts) {
                    $amount = ($amounts->salary + $amounts->overtimeHours + $amounts->disability + $amounts->bonification + $amounts->commissions) * ($conceptData->percentage / 100);
                } else {
                    $amount = ($amounts->salary + $amounts->disability) * ($conceptData->percentage / 100);
                }
            }
    
            $itemsData[] = [
                "payroll_id"            => $payrollData->payrollId,
                "contract_id"           => $payrollData->contractId,
                "employee_id"           => $payrollData->employeeId,
                "payroll_concept_type"  => $conceptData->concept_type_id,
                "payroll_concept_id"    => $conceptData->id,
                "earned_deduction"      => $conceptData->type,
                "amount"                => $amount,
                "days_quantity"         => $amounts->numberOfDays,
                "creation_date"         => date("y-m-d"),
                "created_by"            => $this->session->userdata('user_id'),
            ];
        }

        $this->Payroll_management_model->insert_items($itemsData);
    }

    private function allowAddingProvisions($number)
    {
        $allow = false;
        $number = $number ?? $this->input->post("number");

        if ($this->Payroll_settings->provision_options != 1) {
            if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                if ($number == SECOND_FORTNIGHT) {
                    $allow = true;
                }
            } else if ($this->Payroll_settings->payment_frequency == MONTHLY) {
                $allow = true;
            }
        }

        return $allow;
    }

    private function validate_existing_payroll($year, $month, $number)
    {
        $existing_payroll = $this->Payroll_management_model->get_existing_payroll($year, $month, $number);
        if (!empty($existing_payroll)) {
            $this->session->set_flashdata("error", "Ya se encuentra una nómina registrada con los datos ingresados");
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function validate_creation_next_payroll($year, $month, $number)
    {
        $last_payroll = $this->Payroll_management_model->get_last_payroll();

        if (!empty($last_payroll)) {
            $lastYear = $last_payroll->year;
            $lastMonth = $last_payroll->month;
            $lastNumber = $last_payroll->number;
            $payrollDateToCreate = $year."-".$month;
            $lastPayrollDate = $lastYear."-".$lastMonth;
            $frequencyLastPayroll = $last_payroll->frequency;
            $nextPayrollDate = date("Y-m", strtotime("+1 month", strtotime($lastPayrollDate)));

            if ($this->Payroll_settings->payment_frequency == MONTHLY) {
                if (strtotime($payrollDateToCreate) != strtotime($nextPayrollDate)) {
                    $this->session->set_flashdata('error', "Es necesario crear la nómina del mes siguiente al último registrado");
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                if (strtotime($payrollDateToCreate) < strtotime($lastPayrollDate)) {
                    $this->session->set_flashdata('error', "No es posible registrar nóminas anteriores a la última registrada");
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if (strtotime($payrollDateToCreate) > strtotime($lastPayrollDate)) {
                    if ($frequencyLastPayroll != MONTHLY) {
                        if ($lastNumber != SECOND_FORTNIGHT) {
                            $this->session->set_flashdata('error', "Por favor, registre la nómina de la Segunda Quincena para el mes último registrado");
                            redirect($_SERVER["HTTP_REFERER"]);
                        }

                        if (strtotime($payrollDateToCreate) == strtotime($nextPayrollDate)) {
                            if ($number != FIRST_FORTNIGHT) {
                                $this->session->set_flashdata('error', "Por favor, registre la nómina de la primera quincena para el mes seleccionado");
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $this->session->set_flashdata('error', "Por favor, registre la nómina del mes siguiente a la última registrada.");
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                }
            }
        } else {
            $lastYear = date("Y", strtotime($this->Payroll_settings->payroll_start_date));
            $lastMonth = date("m", strtotime($this->Payroll_settings->payroll_start_date));
            $lastNumberMonth = date("n", strtotime($this->Payroll_settings->payroll_start_date));
            $payrollDateToCreate = $year."-".$month;
            $lastPayrollDate = $lastYear."-".$lastMonth;

            if (strtotime($payrollDateToCreate) < strtotime($lastPayrollDate)) {
                $this->session->set_flashdata('error', "No es posible registrar nóminas anteriores al mes: {$this->months[($lastNumberMonth - 1)]} del año: {$lastYear}");
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                if (strtotime($payrollDateToCreate) == strtotime($lastPayrollDate)) {
                    if ($number != FIRST_FORTNIGHT) {
                        $this->session->set_flashdata('error', "Por favor, registre la nómina de la primera quincena para el mes seleccionado");
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            }
        }
    }

    private function validate_contract_salaries()
    {
        $active_contracts = $this->Payroll_contracts_model->get_by_biller_id();

        foreach ($active_contracts as $key => $contract) {
            if ($contract->contract_status == ACTIVE) {
                if ($contract->employee_type != STUDENT_CONTRIBUTIONS_ONLY_LABOR_RISK) {
                    if ($contract->workday == FULL_TIME) {
                        if ($contract->base_amount < $this->Payroll_settings->minimum_salary_value) {
                            $employee_id = $this->Employees_model->get_by_id($contract->companies_id);
                            $this->session->set_flashdata("error", "El valor del contrato de ". $employee_id->name . " es menor al asignado en ajustes generales de nómina. Por favor actualice el valor del contrato.");
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                }
            }
        }
    }

    private function get_overtime_data()
    {
        foreach ($this->input->post('overtime_start_date') as $index => $start_date) {
            $horas = $this->input->post('hours')[$index];
            $minutos = $this->input->post('minutes')[$index];
            $horas_minutos = (!empty($horas)) ? ($horas * 60) : 0;
            $minutos_horas = (!empty($minutos)) ? ($minutos / 60) : 0;
            $total_minutos = $minutos + $horas_minutos;
            $total_horas = $horas + $minutos_horas;
            $concept_value = $this->calculate_concept_value($this->input->post('payroll_concept_id'), $this->input->post("employee_id"), $total_horas, $this->input->post("contract_id"));

            $data[] = [
                "payroll_id" => $this->input->post("payroll_id"),
                "contract_id" => $this->input->post("contract_id"),
                "employee_id" => $this->input->post("employee_id"),
                "payroll_concept_type" => $this->input->post("concept_type"),
                "payroll_concept_id" => $this->input->post("payroll_concept_id"),
                "earned_deduction" => $this->input->post("earned_deduction"),
                "amount" => $this->sma->formatDecimal($concept_value),
                "creation_date" => date('Y-m-d'),
                "start_date" => $start_date,
                "end_date" => date("Y-m-d H:i", strtotime($start_date . '+ ' . $total_minutos . ' minutes')),
                "days_quantity" => 0,
                "hours_quantity" => $total_horas,
                "frequency" => ONCE,
                'created_by' => $this->session->userdata('user_id')
            ];
        }

        return $data;
    }

    private function calculate_concept_value($concept_id, $employee_id, $days = null, $contract_id = null)
    {
        $total_value = 0;
        $employee_contract = $this->Payroll_contracts_model->get_by_employee_id($employee_id, $contract_id);

        $daily_hours = $this->Payroll_settings->weekly_working_hours / 6;
        $hours = $employee_contract->workday == FULL_TIME ? $daily_hours : $daily_hours / 2;
        $salary_hour = $employee_contract->base_amount / 30 / $hours;
        $salary_day = $salary_hour * $hours;
        $doYouWorkOnADayOff = $this->input->post('doYouWorkOnADayOff');

        if ($concept_id == COMMON_DISABILITY) {
            if ($days >= 1 && $days <= 2) {
                $value_days_disability = $salary_day * $days;

                $percentage = $this->Payroll_settings->illness_2days / 100;
                $total_value += $value_days_disability * $percentage;
            }

            if ($days >= 3 && $days <= 90) {
                $value_days_disability = $salary_day * 2;
                $percentage = $this->Payroll_settings->illness_2days / 100;
                $total_value += $value_days_disability * $percentage;

                $value_days_disability = $salary_day * $days -= 2;
                $percentage = $this->Payroll_settings->illness_90days / 100;
                $total_value += $value_days_disability * $percentage;
            }

            if ($days >= 91) {
                $value_days_disability = $salary_day * 2;
                $percentage = $this->Payroll_settings->illness_2days / 100;
                $total_value += $value_days_disability * $percentage;

                $value_days_disability = $salary_day * 90;
                $percentage = $this->Payroll_settings->illness_90days / 100;
                $total_value += $value_days_disability * $percentage;

                $value_days_disability = $salary_day * $days -= 92;
                $percentage = $this->Payroll_settings->illness_91 / 100;
                $total_value += $value_days_disability * $percentage;
            }

            return $total_value;
        } else if ($concept_id == WORK_DISABILITY || $concept_id == PROFESSIONAL_DISABILITY) {
            $discounted_value = $salary_day * $days;

            return $discounted_value;
        } else if ($concept_id == DAYTIME_OVERTIME ||
                    $concept_id == NIGHT_OVERTIME ||
                    $concept_id == NIGHT_SURCHARGE ||
                    $concept_id == HOLIDAY_DAYTIME_OVERTIME ||
                    $concept_id == HOLIDAY_DAYTIME_SURCHARGE ||
                    $concept_id == HOLIDAY_NIGHT_OVERTIME ||
                    $concept_id == HOLIDAY_NIGHT_SURCHARGE) {
            $concept_data = $this->Payroll_concepts_model->get_by_id($concept_id);

            $overtime_value = $salary_hour * $days;

            if ($concept_id == NIGHT_SURCHARGE || $concept_id == HOLIDAY_DAYTIME_SURCHARGE || $concept_id == HOLIDAY_NIGHT_SURCHARGE) {
                if ($concept_id == HOLIDAY_DAYTIME_SURCHARGE) {
                    if ($doYouWorkOnADayOff == TRUE) {
                        $percentage = ($concept_data->percentage / 100) + 1;
                    } else {
                        $percentage = ($concept_data->percentage / 100);
                    }
                } else {
                    $percentage = ($concept_data->percentage / 100);
                }
            } else {
                $percentage = ($concept_data->percentage / 100) + 1;
            }

            $total_value = $overtime_value * $percentage;

            return $total_value;
        }
    }

    private function get_future_concepts_data($payroll_data, $items_payroll_saved = 0)
    {
        $future_concepts_data = [];
        $payroll_concept_id = $this->input->post("payroll_concept_id");

        if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
            if ($payroll_concept_id == COMPANY_LOAN || $payroll_concept_id == FREE_INVESTMENT_LOAN) {
                $loan_code = $this->generate_alphanumeric_code();

                $dayStartDate = date("d", strtotime($this->input->post("start_date")));
                if ($dayStartDate <= 15) {
                    $newStartdate = date("Y-m-16", strtotime($this->input->post("start_date")));
                } else {
                    $newStartdate = date("Y-m-01", strtotime($this->input->post("start_date") . " + 1 month"));
                }

                $amount = $this->input->post("amount");
                $amount_fees = $this->input->post("amount_fees");
                $value_of_fees = ($amount / $amount_fees);

                for ($i = 0; $i < $amount_fees; $i++) {
                    $dayNewDate = date("d", strtotime($newStartdate));
                    if ($dayNewDate < 16) {
                        $newDuedate = date("Y-m-15", strtotime($newStartdate));
                        $new2Duedate = date("Y-m-15", strtotime($newStartdate));
                    } else {
                        $monthNewDuedate = date("t", strtotime($newStartdate));
                        if ($monthNewDuedate == 31) {
                            $monthNewDuedate -= 1;
                        }
                        $newDuedate = date("Y-m-{$monthNewDuedate}", strtotime($newStartdate));
                        $new2Duedate = date("Y-m-t", strtotime($newStartdate));
                    }

                    $future_concepts_data[] = [
                        "payroll_id"    => $this->input->post("payroll_id"),
                        "payroll_item_id"=> $items_payroll_saved,
                        "contract_id"   => $this->input->post("contract_id"),
                        "employee_id"   => $this->input->post("employee_id"),
                        "concept_type_id"=> $this->input->post("concept_type"),
                        "concept_id"    => $this->input->post("payroll_concept_id"),
                        "creation_date" => date("Y-m-d"),
                        "start_date"    => $newStartdate,
                        "due_date"      => $newDuedate,
                        "frequency"     => RECURRENT,
                        "amount"        => ($amount - ($value_of_fees * $i)),
                        "quantity"      => ($amount_fees - $i),
                        "fee_no"        => ($i + 1),
                        "fee_amount"    => $value_of_fees,
                        "term"          => $this->Payroll_settings->payment_frequency,
                        "term_no"       => $payroll_data->number,
                        "applied"       => NOT,
                        "loan_code"     => $loan_code,
                        'created_by'    => $this->session->userdata('user_id')
                    ];

                    $newStartdate = date("Y-m-d", strtotime($new2Duedate . " + 1 days")); // no suma
                }
            } else if (
                $payroll_concept_id == COMMISSIONS ||
                $payroll_concept_id == INCENTIVES ||
                $payroll_concept_id == BONUSES_CONSTITUTING_SALARY ||
                $payroll_concept_id == PER_DIEM_CONSTITUTES_SALARY ||
                $payroll_concept_id == BONUSES_NOT_CONSTITUTING_SALARY ||
                $payroll_concept_id == BEARING_ALLOWANCE ||
                $payroll_concept_id == MAINTENANCE_ALLOWANCE ||
                $payroll_concept_id == PER_DIEM_NOT_CONSTITUTES_SALARY ||
                $payroll_concept_id == FOOD_BOND
            ) {
                if ($this->input->post("frequency") != ONCE) {
                    $future_concepts_data[] = [
                        "payroll_id" => $this->input->post("payroll_id"),
                        "payroll_item_id"=> $items_payroll_saved,
                        "contract_id" => $this->input->post("contract_id"),
                        "employee_id" => $this->input->post("employee_id"),
                        "concept_type_id" => $this->input->post("concept_type"),
                        "concept_id" => $this->input->post("payroll_concept_id"),
                        "creation_date" => date("Y-m-d"),
                        "start_date" => $this->input->post("start_date"),
                        "due_date" => $this->calculate_end_date_future_item(),
                        "frequency" => $this->input->post("frequency"),
                        "amount" => $this->input->post("amount"),
                        "quantity" => 0,
                        "fee_no" => 0,
                        "fee_amount" => 0,
                        "term" => $this->Payroll_settings->payment_frequency,
                        "term_no" => $payroll_data->number,
                        "applied" => NOT,
                        'created_by' => $this->session->userdata('user_id')
                    ];
                }
            }
        } else if ($this->Payroll_settings->payment_frequency == MONTHLY) {
            if ($payroll_concept_id == COMPANY_LOAN || /*$payroll_concept_id == DRAFT_CREDIT ||*/ $payroll_concept_id == FREE_INVESTMENT_LOAN) {
                $loan_code = $this->generate_alphanumeric_code();
                for ($i = 0; $i < $this->input->post("amount_fees"); $i++) {
                    $amount = $this->input->post("amount");
                    $amount_fees = $this->input->post("amount_fees");
                    $value_of_fees = ($amount / $amount_fees);
                    $new_date = date("Y-m-d", strtotime($this->input->post("start_date") . " + " . ($i + 1) . " month"));
                    $due_date = (date('m-d', strtotime($new_date)) == '03-02') ? date('Y-m-d', strtotime($new_date . '- 2 days')) : $new_date;

                    $future_concepts_data[] = [
                        "payroll_id" => $this->input->post("payroll_id"),
                        "contract_id" => $this->input->post("contract_id"),
                        "employee_id" => $this->input->post("employee_id"),
                        "concept_type_id" => BUSINESS_LOANS,
                        "concept_id" => COMPANY_LOAN,
                        "creation_date" => date("Y-m-d"),
                        "start_date" => $this->input->post("start_date"),
                        "due_date" => $due_date,
                        "frequency" => RECURRENT,
                        "amount" => ($amount - ($value_of_fees * $i)),
                        "quantity" => ($amount_fees - $i),
                        "fee_no" => ($i + 1),
                        "fee_amount" => $value_of_fees,
                        "term" => $this->Payroll_settings->payment_frequency,
                        "term_no" => $payroll_data->number,
                        "applied" => NOT,
                        "loan_code" => $loan_code,
                        'created_by' => $this->session->userdata('user_id')
                    ];
                }
            } else if (
                $payroll_concept_id == COMMISSIONS ||
                $payroll_concept_id == INCENTIVES ||
                $payroll_concept_id == BONUSES_CONSTITUTING_SALARY ||
                $payroll_concept_id == PER_DIEM_CONSTITUTES_SALARY ||
                $payroll_concept_id == BONUSES_NOT_CONSTITUTING_SALARY ||
                $payroll_concept_id == BEARING_ALLOWANCE ||
                $payroll_concept_id == MAINTENANCE_ALLOWANCE ||
                $payroll_concept_id == PER_DIEM_NOT_CONSTITUTES_SALARY ||
                $payroll_concept_id == FOOD_BOND
            ) {
                if ($this->input->post("frequency") != ONCE) {
                    $future_concepts_data[] = [
                        "payroll_id" => $this->input->post("payroll_id"),
                        "contract_id" => $this->input->post("contract_id"),
                        "employee_id" => $this->input->post("employee_id"),
                        "concept_type_id" => $this->input->post("concept_type"),
                        "concept_id" => $this->input->post("payroll_concept_id"),
                        "creation_date" => date("Y-m-d"),
                        "start_date" => $this->input->post("start_date"),
                        "due_date" => $this->calculate_end_date_future_item(),
                        "frequency" => $this->input->post("frequency"),
                        "amount" => $this->input->post("amount"),
                        "quantity" => 0,
                        "fee_no" => 0,
                        "fee_amount" => 0,
                        "term" => $this->Payroll_settings->payment_frequency,
                        "term_no" => $payroll_data->number,
                        "applied" => NOT,
                        'created_by' => $this->session->userdata('user_id')
                    ];
                }
            }
        }

        return $future_concepts_data;
    }

    private function calculate_end_date_future_item()
    {
        if ($this->input->post("frequency") == RECURRENT) {
            if (date('n', strtotime($this->input->post("start_date"))) == 1) {
                if (date('d', strtotime($this->input->post("start_date"))) == 29) {
                    $start_date = date('Y-m-d', strtotime($this->input->post("start_date") . ' - 1 days'));
                } else if (date('d', strtotime($this->input->post("start_date"))) == 30) {
                    $start_date = date('Y-m-d', strtotime($this->input->post("start_date") . ' - 2 days'));
                } else {
                    $start_date = $this->input->post("start_date");
                }
            } else {
                $start_date = $this->input->post("start_date");
            }

            $new_date = date('Y-m-30', strtotime($start_date . ' + ' . ($this->input->post('number_of_time') - 1) . ' month'));
            $end_date = (date('m-d', strtotime($new_date)) == '03-02') ? date('Y-m-d', strtotime($new_date . '- 2 days')) : $new_date;
        } else if ($this->input->post("frequency") == PERMANENT) {
            $employee_contract = $this->Payroll_contracts_model->get_by_employee_id($this->input->post("employee_id"), $this->input->post("contract_id"));
            if ($employee_contract->contract_type == FIXED_TERM) {
                $end_date = $employee_contract->end_date;
            }
            if ($employee_contract->contract_type == UNDEFINED_TERM) {
                $end_date = NULL;
            }
        } else {
            $end_date = $this->input->post("start_date");
        }

        return $end_date;
    }

    private function calculate_number_days($payroll_id, $concept_id = NULL, $applyHolidays = TRUE)
    {
        $days_quantity = 0;
        $start_date = new DateTime($this->input->post("start_date"));
        $end_date = new DateTime($this->input->post("end_date"));
        $diff = $start_date->diff($end_date);
        $days_quantity = $diff->days + 1;
        $payroll = $this->Payroll_management_model->get_by_id($payroll_id);
        $month = date('n', strtotime($this->input->post("end_date")));

        if ($concept_id == TIME_VACATION) {
            if ($payroll->frequency == MONTHLY) {
                if ($days_quantity == 28 && $month == 2) {
                    $days_quantity += 2;
                }
            } else {
                if ($payroll->number == SECOND_FORTNIGHT) {
                    if ($days_quantity == 13 && $month == 2) {
                        $days_quantity += 2;
                    }
                }
            }

            $total_days = $days_quantity;
        } else if ($concept_id == MATERNITY_LICENSE || $concept_id == PATERNITY_LICENSE || $concept_id == COMMON_DISABILITY || $concept_id == PROFESSIONAL_DISABILITY || $concept_id == WORK_DISABILITY) {
            if ($payroll->month == 2) {
                if ($payroll->frequency == MONTHLY) {
                    if ($days_quantity == 28 || $days_quantity == 29) {
                        $days_quantity += (30 - $days_quantity);
                    }
                } else {
                    if ($payroll->number == SECOND_FORTNIGHT) {
                        if ($days_quantity == 13 || $days_quantity == 14) {
                            $days_quantity += (15 - $days_quantity);
                        }
                    }
                }
            }

            $total_days = $days_quantity;
        } else {
            $total_days = $days_quantity;
        }

        return $total_days;
    }

    public function calculate_number_days_ajax($payroll_id, $concept_id = NULL, $start_date = NULL, $end_date = NULL)
    {
        $days_quantity = 0;
        $initial_date = new DateTime($start_date);

        $final_date = new DateTime($end_date);
        $diff = $initial_date->diff($final_date);
        $days_quantity = $diff->days + 1;
        $month = date('n', strtotime($end_date));

        $payroll = $this->Payroll_management_model->get_by_id($payroll_id);
        if ($payroll->frequency == MONTHLY) {

            if ($days_quantity == 28 && $month == 2) {
                $days_quantity += 2;
            }
        } else {
            if ($payroll->number == SECOND_FORTNIGHT) {
                if ($days_quantity == 13 && $month == 2) {
                    $days_quantity += 2;
                }
            }
        }

        $falseDays = $this->calculateFalseDays($payroll_id, $concept_id, $days_quantity, $start_date, $end_date);
        $total_days = $days_quantity + $falseDays;

        echo $total_days;
    }

    private function calculateFalseDays($payrollId, $conceptId, $daysQuantity, $startDate, $endDate)
    {
        $payroll = $this->Payroll_management_model->get_by_id($payrollId);
        $month = date('n', strtotime($endDate));

        if ($conceptId == COMMON_DISABILITY || $conceptId == PROFESSIONAL_DISABILITY || $conceptId == WORK_DISABILITY) {
            $leapYear = date('L', strtotime($endDate));
            if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                if ($payroll->number == SECOND_FORTNIGHT) {
                    if ($leapYear) {
                        if ($daysQuantity == 14 && $month == 2) {
                            return 1;
                        }
                    } else {
                        if ($daysQuantity == 13 && $month == 2) {
                            return 2;
                        }
                    }
                }
            } else {
                if ($leapYear) {
                    if ($daysQuantity == 29 && $month == 2) {
                        return 1;
                    }
                } else {
                    if ($daysQuantity == 28 && $month == 2) {
                        return 2;
                    }
                }
            }
        }

        return 0;
    }

    private function get_dates_payroll($year, $month, $number)
    {
        if ($this->Payroll_settings->payment_frequency == MONTHLY) {
            $start_date = $year . "-" . (($month > 9) ? $month : "0" . $month) . "-01";
            if ($month == 2) {
                $end_date = $year . "-" . (($month > 9) ? $month : "0" . $month) . (date('L', strtotime($year . "-01-01")) ? '-29 23:59:59' : '-28 23:59:59');
            } else {
                $end_date = $year . "-" . (($month > 9) ? $month : "0" . $month) . "-30 23:59:59";
            }
        } else if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
            if ($number == FIRST_FORTNIGHT) {
                $start_date = $year . "-" . (($month > 9) ? $month : "0" . $month) . "-01";
                $end_date = $year . "-" . (($month > 9) ? $month : "0" . $month) . "-15 23:59:59";
            } else {
                $start_date = $year . "-" . (($month > 9) ? $month : "0" . $month) . "-16";
                if ($month == 2) {
                    $end_date = $year . "-" . (($month > 9) ? $month : "0" . $month) . (date('L', strtotime($year . "-01-01")) ? '-29 23:59:59' : '-28 23:59:59');
                } else {
                    $end_date = $year . "-" . (($month > 9) ? $month : "0" . $month) . "-30 23:59:59";
                }
            }
        }

        return (object)["start_date" => $start_date, "end_date" => $end_date];
    }

    private function get_code_payroll($year, $month, $number)
    {
        if ($this->Payroll_settings->payment_frequency == MONTHLY) {
            $code = (($month > 9) ? $year.$month : $year."0" . $month) . "M" . (($number > 9) ? $number : "0" . $number);
        } else if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
            if ($number == FIRST_FORTNIGHT) {
                $code = (($month > 9) ? $year.$month : $year."0" . $month) . "Q" . (($number > 9) ? $number : "0" . $number);
            } else {
                $code = (($month > 9) ? $year.$month : $year."0" . $month) . "Q" . (($number > 9) ? $number : "0" . $number);
            }
        }

        return $code;
    }

    public function get_item_data(int $payroll_id, string $creation_date, int $to_update = NOT)
    {
        $automatic_concepts = $this->Payroll_concepts_model->get_automatic();

        if ($to_update == NOT) {
            $employees_payroll = $this->get_employees_payroll($payroll_id);
        } else {
            $employees_payroll = $this->get_employees_payroll($payroll_id);
        }

        $items_data = [];
        foreach ($employees_payroll as $employee) {
            $this->add_concepts_from_the_contract($payroll_id, $employee->contract_id);
        }

        foreach ($employees_payroll as $employee) {
            if (!empty($automatic_concepts)) {
                foreach ($automatic_concepts as $concept) {
                    $real_salary = 0;

                    if ($concept->id == SALARY) {
                        $salary = $employee->salary;
                        if ($employee->workday == FULL_TIME || $employee->workday == HALFTIME) {
                            if ($employee->type_employee == APPRENTICES_SENA_LECTIVA){
                                $salary = $salary *  ($this->Payroll_settings->percentage_salary_apprentice_teaching / 100);
                            }

                            if ($employee->type_employee == APPRENTICES_SENA_PRODUCTIVE){
                                $salary = $salary *  ($this->Payroll_settings->percentage_salary_apprentice_productive / 100);
                            }

                            $daily_salary = $salary / 30;
                            $amount = $daily_salary * $employee->worked_days;
                        }

                        $items_data[] = [
                            "payroll_id" => $payroll_id,
                            "contract_id" => $employee->contract_id,
                            "employee_id" => $employee->employee_id,
                            "payroll_concept_type" => $concept->concept_type_id,
                            "payroll_concept_id" => $concept->id,
                            "earned_deduction" => $concept->type,
                            "amount" => $amount,
                            "creation_date" => $creation_date,
                            "days_quantity" => ($employee->worked_days),
                            "future_item_id" => NULL,
                            'created_by' => $this->session->userdata('user_id')
                        ];
                    }

                    if ($concept->id == TRANSPORTATION_ALLOWANCE) {
                        if ($employee->trans_allowance == YES) {
                            if ($employee->workday == FULL_TIME || $employee->workday == HALFTIME) {
                                $daily_transportation_allowance = $this->Payroll_settings->transportation_allowance_value / 30;
                                $worked_days = $employee->worked_days;
                                $amount = $daily_transportation_allowance * $worked_days;
                            }

                            $items_data[] = [
                                "payroll_id" => $payroll_id,
                                "contract_id" => $employee->contract_id,
                                "employee_id" => $employee->employee_id,
                                "payroll_concept_type" => $concept->concept_type_id,
                                "payroll_concept_id" => $concept->id,
                                "earned_deduction" => $concept->type,
                                "amount" => $amount,
                                "creation_date" => $creation_date,
                                "days_quantity" => $worked_days,
                                "future_item_id" => NULL,
                                'created_by' => $this->session->userdata('user_id')
                            ];
                        }
                    }

                    if ($employee->type_employee == APPRENTICES_SENA_PRODUCTIVE || $employee->type_employee == APPRENTICES_SENA_LECTIVA){
                    } else {
                        if ($this->Payroll_settings->exempt_parafiscal_health_payments == YES) {
                            if ($employee->salary < ($this->Payroll_settings->minimum_salary_value * 10)) {
                                if ($concept->id == HEALTH_RATE_LESS_THAN) {
                                    if ($employee->workday == FULL_TIME || $employee->workday == HALFTIME) {
                                        if ($employee->workday == HALFTIME) {
                                            $real_salary = ($employee->salary < $this->Payroll_settings->minimum_salary_value) ? $this->Payroll_settings->minimum_salary_value : $employee->salary;
                                        } else {
                                            $real_salary = $employee->salary;
                                        }

                                        $daily_salary = $real_salary / 30;
                                        $salary = ($daily_salary * $employee->worked_days);
                                        $new_salary = ($employee->integral_salary == YES) ? ($salary * 0.7) : $salary;
                                        $amount = ($new_salary * $concept->percentage) / 100;
                                    }

                                    $items_data[] = [
                                        "payroll_id" => $payroll_id,
                                        "contract_id" => $employee->contract_id,
                                        "employee_id" => $employee->employee_id,
                                        "payroll_concept_type" => $concept->concept_type_id,
                                        "payroll_concept_id" => $concept->id,
                                        "earned_deduction" => $concept->type,
                                        "amount" => $amount,
                                        "creation_date" => $creation_date,
                                        "days_quantity" => $employee->worked_days,
                                        "future_item_id" => NULL,
                                        'created_by' => $this->session->userdata('user_id')
                                    ];
                                }
                            } else {
                                if ($concept->id == HEALTH_RATE) {
                                    if ($employee->workday == FULL_TIME || $employee->workday == HALFTIME) {
                                        if ($employee->workday == HALFTIME) {
                                            $real_salary = ($employee->salary < $this->Payroll_settings->minimum_salary_value) ? $this->Payroll_settings->minimum_salary_value : $employee->salary;
                                        } else {
                                            $real_salary = $employee->salary;
                                        }

                                        $daily_salary = $real_salary / 30;
                                        $salary = ($daily_salary * $employee->worked_days);
                                        $new_salary = ($employee->integral_salary == YES) ? ($salary * 0.7) : $salary;
                                        $amount = ($new_salary * $concept->percentage) / 100;
                                    }

                                    $items_data[] = [
                                        "payroll_id" => $payroll_id,
                                        "contract_id" => $employee->contract_id,
                                        "employee_id" => $employee->employee_id,
                                        "payroll_concept_type" => $concept->concept_type_id,
                                        "payroll_concept_id" => $concept->id,
                                        "earned_deduction" => $concept->type,
                                        "amount" => $amount,
                                        "creation_date" => $creation_date,
                                        "days_quantity" => $employee->worked_days,
                                        "future_item_id" => NULL,
                                        'created_by' => $this->session->userdata('user_id')
                                    ];
                                }
                            }
                        } else {
                            if ($employee->salary < ($this->Payroll_settings->minimum_salary_value * 10)) {
                                if ($concept->id == HEALTH_RATE_LESS_THAN) {
                                    if ($employee->workday == FULL_TIME || $employee->workday == HALFTIME) {
                                        if ($employee->workday == HALFTIME) {
                                            $real_salary = ($employee->salary < $this->Payroll_settings->minimum_salary_value) ? $this->Payroll_settings->minimum_salary_value : $employee->salary;
                                        } else {
                                            $real_salary = $employee->salary;
                                        }

                                        $daily_salary = $real_salary / 30;
                                        $salary = ($daily_salary * $employee->worked_days);
                                        $new_salary = ($employee->integral_salary == YES) ? ($salary * 0.7) : $salary;
                                        $amount = ($new_salary * $concept->percentage) / 100;
                                    }

                                    $items_data[] = [
                                        "payroll_id" => $payroll_id,
                                        "contract_id" => $employee->contract_id,
                                        "employee_id" => $employee->employee_id,
                                        "payroll_concept_type" => $concept->concept_type_id,
                                        "payroll_concept_id" => $concept->id,
                                        "earned_deduction" => $concept->type,
                                        "amount" => $amount,
                                        "creation_date" => $creation_date,
                                        "days_quantity" => $employee->worked_days,
                                        "future_item_id" => NULL,
                                        'created_by' => $this->session->userdata('user_id')
                                    ];
                                }
                            } else {
                                if ($concept->id == HEALTH_RATE) {
                                    if ($employee->workday == FULL_TIME || $employee->workday == HALFTIME) {
                                        if ($employee->workday == HALFTIME) {
                                            $real_salary = ($employee->salary < $this->Payroll_settings->minimum_salary_value) ? $this->Payroll_settings->minimum_salary_value : $employee->salary;
                                        } else {
                                            $real_salary = $employee->salary;
                                        }

                                        $daily_salary = $real_salary / 30;
                                        $salary = ($daily_salary * $employee->worked_days);
                                        $new_salary = ($employee->integral_salary == YES) ? ($salary * 0.7) : $salary;
                                        $amount = ($new_salary * $concept->percentage) / 100;
                                    }


                                    $items_data[] = [
                                        "payroll_id" => $payroll_id,
                                        "contract_id" => $employee->contract_id,
                                        "employee_id" => $employee->employee_id,
                                        "payroll_concept_type" => $concept->concept_type_id,
                                        "payroll_concept_id" => $concept->id,
                                        "earned_deduction" => $concept->type,
                                        "amount" => $amount,
                                        "creation_date" => $creation_date,
                                        "days_quantity" => $employee->worked_days,
                                        "future_item_id" => NULL,
                                        'created_by' => $this->session->userdata('user_id')
                                    ];
                                }
                            }
                        }
                    }

                    if ($employee->high_risk_pension == YES) {
                        if ($concept->id == HIGH_RISK_PENSION_RATE) {
                            if ($employee->type_employee == PRE_PENSION_OF_ENTITY_IN_LIQUIDATION || $employee->type_employee == PRE_PENSIONED_WITH_VOLUNTARY_CONTRIBUTION_TO_HEALTH || $employee->type_employee == APPRENTICES_SENA_PRODUCTIVE || $employee->type_employee == APPRENTICES_SENA_LECTIVA) {
                            } else {
                                if ($employee->workday == FULL_TIME || $employee->workday == HALFTIME) {
                                    if ($employee->workday == HALFTIME) {
                                        $real_salary = ($employee->salary < $this->Payroll_settings->minimum_salary_value) ? $this->Payroll_settings->minimum_salary_value : $employee->salary;
                                    } else {
                                        $real_salary = $employee->salary;
                                    }

                                    $daily_salary = $real_salary / 30;
                                    $amount = (($daily_salary * $employee->worked_days) * $concept->percentage) / 100;
                                }

                                $items_data[] = [
                                    "payroll_id" => $payroll_id,
                                    "contract_id" => $employee->contract_id,
                                    "employee_id" => $employee->employee_id,
                                    "payroll_concept_type" => $concept->concept_type_id,
                                    "payroll_concept_id" => $concept->id,
                                    "earned_deduction" => $concept->type,
                                    "amount" => $amount,
                                    "creation_date" => $creation_date,
                                    "days_quantity" => $employee->worked_days,
                                    "future_item_id" => NULL,
                                    'created_by' => $this->session->userdata('user_id')
                                ];
                            }
                        }
                    } else {
                        if ($employee->type_employee == PRE_PENSION_OF_ENTITY_IN_LIQUIDATION || $employee->type_employee == PRE_PENSIONED_WITH_VOLUNTARY_CONTRIBUTION_TO_HEALTH || $employee->type_employee == APPRENTICES_SENA_PRODUCTIVE || $employee->type_employee == APPRENTICES_SENA_LECTIVA) {
                        } else {
                            if ($concept->id == PENSION_RATE) {
                                if ($employee->workday == FULL_TIME || $employee->workday == HALFTIME) {
                                    if ($employee->workday == HALFTIME) {
                                        $real_salary = ($employee->salary < $this->Payroll_settings->minimum_salary_value) ? $this->Payroll_settings->minimum_salary_value : $employee->salary;
                                    } else {
                                        $real_salary = $employee->salary;
                                    }

                                    $daily_salary = $real_salary / 30;
                                    $salary = ($daily_salary * $employee->worked_days);
                                    $new_salary = ($employee->integral_salary == YES) ? ($salary * 0.7) : $salary;
                                    $amount = ($new_salary * $concept->percentage) / 100;
                                }

                                $items_data[] = [
                                    "payroll_id" => $payroll_id,
                                    "contract_id" => $employee->contract_id,
                                    "employee_id" => $employee->employee_id,
                                    "payroll_concept_type" => $concept->concept_type_id,
                                    "payroll_concept_id" => $concept->id,
                                    "earned_deduction" => $concept->type,
                                    "amount" => $amount,
                                    "creation_date" => $creation_date,
                                    "days_quantity" => $employee->worked_days,
                                    "future_item_id" => NULL,
                                    'created_by' => $this->session->userdata('user_id')
                                ];
                            }
                        }
                    }
                }
            }

            $payroll_future_items = $this->Payroll_management_model->get_payroll_future_items_by_employee_id($employee->contract_id, $employee->employee_id);
            if (!empty($payroll_future_items)) {
                $payroll_data = $this->Payroll_management_model->get_by_id($payroll_id);

                foreach ($payroll_future_items as $key => $future_items) {
                    if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                        if (
                            $future_items->concept_id == COMMISSIONS ||
                            $future_items->concept_id == INCENTIVES ||
                            $future_items->concept_id == BONUSES_CONSTITUTING_SALARY ||
                            $future_items->concept_id == PER_DIEM_CONSTITUTES_SALARY ||
                            $future_items->concept_id == BONUSES_NOT_CONSTITUTING_SALARY ||
                            $future_items->concept_id == BEARING_ALLOWANCE ||
                            $future_items->concept_id == MAINTENANCE_ALLOWANCE ||
                            $future_items->concept_id == PER_DIEM_NOT_CONSTITUTES_SALARY ||
                            $future_items->concept_id == FOOD_BOND) {
                            if ($future_items->from_contract == YES) {
                                if ($this->Payroll_settings->bonus_payment == MONTHLY) {
                                    if ($payroll_data->number == 2) {
                                        $future_item_id = NULL;
                                        if ($future_items->due_date != "0000-00-00") {
                                            if ((date('Ynj', strtotime($payroll_data->end_date)) >= date('Ynj', strtotime($future_items->due_date)))) {
                                                $future_item_id = $future_items->id;
                                            }
                                        }

                                        $items_data[] = [
                                            "payroll_id" => $payroll_id,
                                            "contract_id" => $employee->contract_id,
                                            "employee_id" => $employee->employee_id,
                                            "payroll_concept_type" => $future_items->concept_type_id,
                                            "payroll_concept_id" => $future_items->concept_id,
                                            "earned_deduction" => $future_items->type,
                                            "amount" => $future_items->amount,
                                            "creation_date" => $creation_date,
                                            "days_quantity" => 0,
                                            "future_item_id" => $future_item_id,
                                            'created_by' => $this->session->userdata('user_id')
                                        ];
                                    }
                                } else {
                                    $future_item_id = NULL;
                                    if ($future_items->due_date != "0000-00-00") {
                                        if ((date('Ynj', strtotime($payroll_data->end_date)) >= date('Ynj', strtotime($future_items->due_date)))) {
                                            $future_item_id = $future_items->id;
                                        }
                                    }

                                    $items_data[] = [
                                        "payroll_id" => $payroll_id,
                                        "contract_id" => $employee->contract_id,
                                        "employee_id" => $employee->employee_id,
                                        "payroll_concept_type" => $future_items->concept_type_id,
                                        "payroll_concept_id" => $future_items->concept_id,
                                        "earned_deduction" => $future_items->type,
                                        "amount" => $future_items->amount / 2,
                                        "creation_date" => $creation_date,
                                        "days_quantity" => 0,
                                        "future_item_id" => $future_item_id,
                                        'created_by' => $this->session->userdata('user_id')
                                    ];
                                }
                            } else {
                                if ($future_items->term_no == $payroll_data->number) {
                                    $future_item_id = NULL;
                                    if ($future_items->due_date != "0000-00-00") {
                                        if ((date('Ynj', strtotime($payroll_data->end_date)) >= date('Ynj', strtotime($future_items->due_date)))) {
                                            $future_item_id = $future_items->id;
                                        }
                                    }

                                    $items_data[] = [
                                        "payroll_id" => $payroll_id,
                                        "contract_id" => $employee->contract_id,
                                        "employee_id" => $employee->employee_id,
                                        "payroll_concept_type" => $future_items->concept_type_id,
                                        "payroll_concept_id" => $future_items->concept_id,
                                        "earned_deduction" => $future_items->type,
                                        "amount" => $future_items->amount,
                                        "creation_date" => $creation_date,
                                        "days_quantity" => 0,
                                        "future_item_id" => $future_item_id,
                                        'created_by' => $this->session->userdata('user_id')
                                    ];
                                }
                            }
                        } else if ($future_items->concept_id == COMPANY_LOAN || $future_items->concept_id == FREE_INVESTMENT_LOAN) {
                            if ($future_items->due_date >= $payroll_data->start_date && $future_items->due_date <= $payroll_data->end_date) {
                                $items_data[] = [
                                    "payroll_id" => $payroll_id,
                                    "contract_id" => $employee->contract_id,
                                    "employee_id" => $employee->employee_id,
                                    "payroll_concept_type" => $future_items->concept_type_id,
                                    "payroll_concept_id" => $future_items->concept_id,
                                    "earned_deduction" => $future_items->type,
                                    "amount" => $future_items->fee_amount,
                                    "creation_date" => $creation_date,
                                    "days_quantity" => 0,
                                    "future_item_id" => $future_items->id,
                                    'created_by' => $this->session->userdata('user_id')
                                ];
                            }
                        } else {
                            $items_data[] = [
                                "payroll_id" => $payroll_id,
                                "contract_id" => $employee->contract_id,
                                "employee_id" => $employee->employee_id,
                                "payroll_concept_type" => $future_items->concept_type_id,
                                "payroll_concept_id" => $future_items->concept_id,
                                "earned_deduction" => $future_items->type,
                                "amount" => $future_items->amount,
                                "creation_date" => $creation_date,
                                "days_quantity" => 0,
                                "future_item_id" => $future_items->id,
                                'created_by' => $this->session->userdata('user_id')
                            ];
                        }
                    } else if ($this->Payroll_settings->payment_frequency == MONTHLY) {
                        $future_item_id = NULL;
                        if ($future_items->due_date != "0000-00-00") {
                            if ((date('Ynj', strtotime($payroll_data->end_date)) >= date('Ynj', strtotime($future_items->due_date)))) {
                                $future_item_id = $future_items->id;
                            }
                        }

                        if ($future_items->concept_id == COMPANY_LOAN || $future_items->concept_id == FREE_INVESTMENT_LOAN) {
                            $items_data[] = [
                                "payroll_id" => $payroll_id,
                                "contract_id" => $employee->contract_id,
                                "employee_id" => $employee->employee_id,
                                "payroll_concept_type" => $future_items->concept_type_id,
                                "payroll_concept_id" => $future_items->concept_id,
                                "earned_deduction" => $future_items->type,
                                "amount" => $future_items->fee_amount,
                                "creation_date" => $creation_date,
                                "days_quantity" => 0,
                                "future_item_id" => $future_item_id,
                                'created_by' => $this->session->userdata('user_id')
                            ];
                        } else {
                            $items_data[] = [
                                "payroll_id" => $payroll_id,
                                "contract_id" => $employee->contract_id,
                                "employee_id" => $employee->employee_id,
                                "payroll_concept_type" => $future_items->concept_type_id,
                                "payroll_concept_id" => $future_items->concept_id,
                                "earned_deduction" => $future_items->type,
                                "amount" => $future_items->amount,
                                "creation_date" => $creation_date,
                                "days_quantity" => 0,
                                "future_item_id" => $future_item_id,
                                'created_by' => $this->session->userdata('user_id')
                            ];
                        }
                    }
                }
            }
        }

        return ["items_data" => $items_data, "amount_employees" => count($employees_payroll)];
    }

    public function get_employees_payroll(int $payroll_id)
    {
        $payroll = $this->Payroll_management_model->get_by_id($payroll_id);
        $star_date_payroll = strtotime(date("Y-m-d", strtotime($payroll->start_date)));
        $end_date_payroll = strtotime(date("Y-m-d", strtotime($payroll->end_date)));

        $active_contracts = $this->Payroll_contracts_model->get_by_biller_id(date("Y-m-d", strtotime($payroll->end_date)));

        $data = [];
        foreach ($active_contracts as $contract) {
            $id = $contract->id;
            $start_date_contract = strtotime($contract->start_date);
            $end_date_contract = (strtotime($contract->end_date) <= 0) ? 0 : strtotime($contract->end_date);
            $settlement_date_contract = strtotime($contract->settlement_date);

            if ($settlement_date_contract > 0) {
                $end_date_contract = $settlement_date_contract;
            }

            $type_contract = $contract->contract_type;
            $employee_id = $contract->companies_id;
            $salary = $contract->base_amount;
            $workday = $contract->workday;
            $high_risk_pension = $contract->retired_risk;
            $status = $contract->contract_status;
            $trans_allowance = $contract->trans_allowance;
            $integral_salary = $contract->integral_salary;
            $type_employee = $contract->employee_type;

            if ($start_date_contract <= $star_date_payroll) {
                if ($end_date_contract <= $star_date_payroll) {
                    if ($end_date_contract == 0) {
                        if ($type_contract == UNDEFINED_TERM) {
                            $diff_days = $this->getNumberDaysPayroll($star_date_payroll, $end_date_payroll);

                            $data[$id] = (object)[
                                "contract_id" => $id,
                                "employee_id" => $employee_id,
                                "salary" => $salary,
                                "worked_days" => $diff_days,
                                "workday" => $workday,
                                "high_risk_pension" => $high_risk_pension,
                                "trans_allowance" => $trans_allowance,
                                "integral_salary" => $integral_salary,
                                "type_employee" => $type_employee
                            ];
                        }
                    } else {
                        // if ($end_date_contract >= $star_date_payroll) {
                        //     $diff_days = $this->getNumberDaysPayroll($star_date_payroll, $end_date_contract);

                        //     $data[$id] = (object)[
                        //         "contract_id" => $id,
                        //         "employee_id" => $employee_id,
                        //         "salary" => $salary,
                        //         "worked_days" => $diff_days,
                        //         "workday" => $workday,
                        //         "high_risk_pension" => $high_risk_pension,
                        //         "trans_allowance" => $trans_allowance,
                        //         "integral_salary" => $integral_salary,
                        //         "type_employee" => $type_employee
                        //     ];
                        // }
                    }
                } else {
                    if ($end_date_contract <= $end_date_payroll) {
                        $diff_days = $this->getNumberDaysPayroll($star_date_payroll, $end_date_contract);

                        $data[$id] = (object)[
                            "contract_id" => $id,
                            "employee_id" => $employee_id,
                            "salary" => $salary,
                            "worked_days" => $diff_days,
                            "workday" => $workday,
                            "high_risk_pension" => $high_risk_pension,
                            "trans_allowance" => $trans_allowance,
                            "integral_salary" => $integral_salary,
                            "type_employee" => $type_employee
                        ];
                    } else {
                        $diff_days = $this->getNumberDaysPayroll($star_date_payroll, $end_date_payroll);

                        $data[$id] = (object)[
                            "contract_id" => $id,
                            "employee_id" => $employee_id,
                            "salary" => $salary,
                            "worked_days" => $diff_days,
                            "workday" => $workday,
                            "high_risk_pension" => $high_risk_pension,
                            "trans_allowance" => $trans_allowance,
                            "integral_salary" => $integral_salary,
                            "type_employee" => $type_employee
                        ];
                    }
                }
            } else {
                if ($start_date_contract <= $end_date_payroll) {
                    if ($end_date_contract <= $end_date_payroll) {
                        if ($end_date_contract == NULL) {
                            if ($type_contract == UNDEFINED_TERM) {
                                $diff_days = $this->getNumberDaysPayroll($start_date_contract, $end_date_payroll);

                                $data[$id] = (object)[
                                    "contract_id" => $id,
                                    "employee_id" => $employee_id,
                                    "salary" => $salary,
                                    "worked_days" => $diff_days,
                                    "workday" => $workday,
                                    "high_risk_pension" => $high_risk_pension,
                                    "trans_allowance" => $trans_allowance,
                                    "integral_salary" => $integral_salary,
                                    "type_employee" => $type_employee
                                ];
                            }
                        } else {
                            $diff_days = $this->getNumberDaysPayroll($start_date_contract, $end_date_contract);

                            $data[$id] = (object)[
                                "contract_id" => $id,
                                "employee_id" => $employee_id,
                                "salary" => $salary,
                                "worked_days" => $diff_days,
                                "workday" => $workday,
                                "high_risk_pension" => $high_risk_pension,
                                "trans_allowance" => $trans_allowance,
                                "integral_salary" => $integral_salary,
                                "type_employee" => $type_employee
                            ];
                        }
                    } else {
                        $diff_days = $this->getNumberDaysPayroll($start_date_contract, $end_date_payroll);

                        $data[$id] = (object)[
                            "contract_id" => $id,
                            "employee_id" => $employee_id,
                            "salary" => $salary,
                            "worked_days" => $diff_days,
                            "workday" => $workday,
                            "high_risk_pension" => $high_risk_pension,
                            "trans_allowance" => $trans_allowance,
                            "integral_salary" => $integral_salary,
                            "type_employee" => $type_employee
                        ];
                    }
                }
            }
        }

        return $data;
    }

    private function getNumberDaysPayroll($startDate, $endDate)
    {
        $date1 = new DateTime(date("Y-m-d", $startDate));
        $date2 = new DateTime(date("Y-m-d", $endDate));

        $diff = $date1->diff($date2);
        $diff_days = ($diff->days + 1);

        if ($this->Payroll_settings->payment_frequency == MONTHLY) {
            $finalDayEndDate = date('j', $endDate);
            if (date('n', $endDate) == 2) {
                if ($finalDayEndDate == 28 || $finalDayEndDate == 29) {
                    if (date("L", $endDate) == 1) {
                        $diff_days += 1;
                    } else {
                        $diff_days += 2;
                    }
                } else {
                    if ($diff_days == 28) {
                        $diff_days += 2;
                    } else if ($diff_days == 29 && date("L", $endDate) == 1) {
                        $diff_days += 1;
                    }
                }
            }
        } else {
            if (date('n', $endDate) == 2) {
                if (date("L", $endDate) == 1) {
                    if (date('d', $endDate) == 29) {
                        $diff_days += 1;
                    }
                } else {
                    if (date('d', $endDate) == 28) {
                        $diff_days += 2;
                    }
                }
            }
        }

        return $diff_days;
    }

    private function add_concepts_from_the_contract(int $payroll_id, int $contract_id)
    {
        $payroll = $this->Payroll_management_model->get_by_id($payroll_id);
        $contract_concepts = $this->PayrollContractConcepts_model->get(["contract_id"=>$contract_id, "deleted"=>NOT]);

        if (!empty($contract_concepts)) {
            foreach ($contract_concepts as $concept) {
                if ($concept->applied == NOT) {
                    $contract = $this->Payroll_contracts_model->get_by_id($contract_id);

                    $futures_items[] = [
                        'payroll_id' => $payroll_id,
                        'contract_id' => $concept->contract_id,
                        'employee_id' => $concept->employee_id,
                        'concept_type_id' => $concept->concept_type_id,
                        'concept_id' => $concept->concept_id,
                        'creation_date' => date('Y-m-d'),
                        'start_date' => $contract->start_date,
                        'due_date' => $contract->end_date,
                        'frequency' => empty($contract->end_date) ? PERMANENT : RECURRENT,
                        'amount' => $concept->amount,
                        'quantity' => 0,
                        'fee_no' => 0,
                        'fee_amount' => 0,
                        "term" => $this->Payroll_settings->payment_frequency,
                        "term_no" => $payroll->number,
                        'applied' => NOT,
                        'from_contract' => YES,
                        'created_by' => $this->session->userdata('user_id')
                    ];
                }
            }

            if (!empty($futures_items)) {
                $saved_futures_items = $this->Payroll_management_model->insert_future_items($futures_items);
                if ($saved_futures_items == TRUE) {
                    foreach ($contract_concepts as $key => $concept) {
                        $this->PayrollContractConcepts_model->update(['applied' => YES], $concept->contract_id, $concept->employee_id, $concept->concept_id);
                    }
                }
            }
        }
    }

    public function update($id)
    {
        $this->sma->checkPermissions("update", NULL, "payroll_management");

        $date_creation = date("Y-m-d");
        $payroll = $this->Payroll_management_model->get_by_id($id);

        if ($payroll->status == APPROVED) {
            $this->session->set_flashdata("error", "No es posible actualizar la nómina debido a que se encuentra aprobada.");
            admin_redirect("payroll_management/index");
        }

        if ($payroll->status == IN_PREPARATION) {
            $itemDataForPayroll = $this->get_item_data($id, $date_creation, YES);
            $itemsData = $itemDataForPayroll["items_data"];

            $arrayFirst = [];
            foreach ($itemsData as $items) {
                $contract_id = $items['contract_id'];
                if (!in_array($contract_id, $arrayFirst)) {
                    $arrayFirst[] = $contract_id;
                }
            }

            $arraySecond = [];
            $payrollItems = $this->Payroll_management_model->get_payroll_items($id);
            foreach ($payrollItems as $payrollItem) {
                $contractId = $payrollItem->contract_id;
                if (!in_array($contractId, $arraySecond)) {
                    $arraySecond[] = $contractId;
                }
            }

            $employees      = [];
            $filtered_array = array_diff($arrayFirst, $arraySecond);

            if (!empty($filtered_array)) {
                foreach ($itemsData as $items) {
                    $contractId = $items['contract_id'];
                    foreach ($filtered_array as $filtered_contract_id) {
                        if ($filtered_contract_id == $contractId) {
                            $this->Payroll_management_model->insert_item($items);

                            $employeeId = $items["employee_id"];
                            if (!in_array($employees, $employees)) {
                                $this->calculateProvisions($payroll->id, $contractId, $employeeId);
                                $employees[] = $employeeId;
                            }
                        }
                    }
                }

                
                $this->calculate_payroll_earned_deductions($payroll->id);
                $this->detect_contract_termination();

                $this->session->set_userdata('employee_salary_change', FALSE);
                $this->session->set_userdata('employee_settlement_date_change', FALSE);
                $this->session->set_userdata('existing_configuration_data_changes', FALSE);

                $this->session->set_flashdata('message', lang("payroll_management_updated"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->calculate_payroll_earned_deductions($payroll->id);
                $this->detect_contract_termination();

                $this->session->set_flashdata('message', 'Todos los empleados se encuentran agregados correctamente en la nómina');
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('message', lang("payroll_management_not_updated") . '. ' . lang("payroll_management_state_not_preparation"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function calculate_payroll_earned_deductions(int $payroll_id)
    {
        $earnings = 0;
        $deductions = 0;
        $provisions = 0;
        $payroll_items = $this->Payroll_management_model->get_payroll_items($payroll_id);

        foreach ($payroll_items as $key => $item) {
            if ($item->payroll_concept_id != LOAN_DISBURSEMENT) {
                if (($item->payroll_concept_id == MONEY_VACATION || $item->payroll_concept_id == SERVICE_BONUS || $item->payroll_concept_id == LAYOFFS || $item->payroll_concept_id == LAYOFFS_INTERESTS) && $item->paid_to_employee == NOT) {
                    $provisions += $item->amount;
                } else {
                    if ($item->earned_deduction == EARNED) {
                        $earnings += $item->amount;
                    } else {
                        $deductions += $item->amount;
                    }

                    $employees[$item->employee_id] = $item->employee_id;
                }
            }
        }

        $data = [
            "employee_no"   => count($employees),
            "earnings"      => $earnings,
            "deductions"    => $deductions,
            "total_payment" => ($earnings - $deductions),
            "provisions"    => $provisions,
        ];
        // $this->sma->print_arrays($data);
        if ($this->Payroll_management_model->update($data, $payroll_id) == TRUE) {
            $this->session->set_flashdata('message', lang("payroll_management_updated"));
        } else {
            $this->session->set_flashdata('error', lang("payroll_management_not_updated"));
        }
    }

    private function calculate_salary(int $payroll_id, int $employee_id, int $disability_days = null, $contract_id)
    {
        $item_data = $this->Payroll_management_model->get_payroll_items_by_data($payroll_id, $employee_id, ORDINARY, SALARY, $contract_id);

        $worked_days = $item_data->days_quantity - $disability_days;
        if ($this->Payroll_settings->payment_frequency == MONTHLY) {
            if ($worked_days > 30) {
                $worked_days = 30;
                $disability_days = 0;
            }
        } else {
            if ($worked_days > 15) {
                $worked_days = 15;
                $disability_days = 0;
            }
        }

        if (abs($disability_days) > 0) {
            $common_disability_amount = $this->calculate_amount_days($employee_id, $disability_days, $contract_id);
            $amount = $item_data->amount - $common_disability_amount;
            $this->Payroll_management_model->update_item(['amount' => ($amount <= 0) ? 0 : $amount, 'days_quantity' => $worked_days], $item_data->id);
        }
    }

    private function calculate_amount_days(int $employee_id, int $days = NULL, $contract_id)
    {
        $employee_data = $this->Employees_model->get_by_id($employee_id, $contract_id);
        $hours = $employee_data->workday == FULL_TIME ? 8 : 4;

        $salary_hour = $employee_data->base_amount / 30 / $hours;
        $salary_day = $salary_hour * $hours;
        $total_salary = $salary_day * $days;

        return $total_salary;
    }

    private function calculate_transportation_allowance($payroll_id, $employee_id, $disability_days, $contract_id, $concept_id = NULL)
    {
        if ($concept_id == TIME_VACATION) {
            $payroll = $this->Payroll_management_model->get_by_id($payroll_id);
            if ($payroll->month == 2) {
                if ($payroll->frequency == MONTHLY) {
                    if ($disability_days == 28) {
                        $disability_days += (30 - $disability_days);
                    }
                } else {
                    if ($payroll->number == SECOND_FORTNIGHT) {
                        if ($disability_days == 13) {
                            $disability_days += (30 - $disability_days);
                        }
                    }
                }
            }
        }

        $item_data = $this->Payroll_management_model->get_payroll_items_by_data($payroll_id, $employee_id, TRANSPORTATION_ALLOWANCE_TYPE, TRANSPORTATION_ALLOWANCE, $contract_id);
        $daily_transportation_allowance = $this->Payroll_settings->transportation_allowance_value / 30;

        $worked_days = $item_data->days_quantity - $disability_days;
        if ($worked_days <= 0) {
            $worked_days = 0;
        } else {
            if ($payroll->frequency == MONTHLY) {
                $worked_days >= 30 ? 30 : $worked_days;
            } else {
                $worked_days >= 15 ? 15 : $worked_days;
            }
        }

        $amount = $daily_transportation_allowance * $worked_days;

        $this->Payroll_management_model->update_item(['amount' => $amount, 'days_quantity' => $worked_days], $item_data->id);
    }

    public function delete($payroll_id)
    {
        $deleted_items = $this->Payroll_management_model->delete_items($payroll_id);
        if ($deleted_items == TRUE) {
            $deleted_payroll = $this->Payroll_management_model->delete($payroll_id);
            if ($deleted_payroll == TRUE) {
                $payroll_future_items = $this->Payroll_management_model->get_payroll_future_items_by_data($payroll_id);
                if (!empty($payroll_future_items)) {
                    foreach ($payroll_future_items as $future_items) {
                        if ($future_items->from_contract == YES) {
                            $this->PayrollContractConcepts_model->update(['applied'=>NOT],
                            $future_items->contract_id,
                            $future_items->employee_id,
                            $future_items->concept_id);
                        }

                        $this->Payroll_management_model->delete_future_item($future_items->id);
                    }
                }

                $this->Payroll_management_model->delete_social_security_parafiscal($payroll_id);
                $this->Payroll_management_model->delete_provisions($payroll_id);

                $this->session->set_flashdata('message', lang('eliminated_payroll'));
                admin_redirect('payroll_management');
            } else {
                $this->session->set_flashdata('error', lang('payroll_not_eliminated'));
                admin_redirect('payroll_management');
            }
        } else {
            $this->session->set_flashdata('error', lang('payroll_items_not_deleted'));
            admin_redirect('payroll_management');
        }
    }

    private function calculate_social_security_by_employee_id(int $payroll_id, int $employee_id, $contract_id, $days_quantity_to_add = 0)
    {
        $payroll_items_data = $this->Payroll_management_model->get_payroll_items_by_payroll_id_employee_id($payroll_id, $employee_id, $contract_id);
        $employee_data = $this->Employees_model->get_by_id($employee_id, $contract_id);

        $salary_base = $days_quantity_salary = $days_quantity = $salaryBasePension = $days_quantity_not_paid_license = 0;
        foreach ($payroll_items_data as $payroll_item) {
            $amount = $payroll_item->amount;
            $workday = $employee_data->workday;
            $integral_salary = $employee_data->integral_salary;
            $concept_id = $payroll_item->payroll_concept_id;
            $days_quantity_salary = $payroll_item->days_quantity;
            $concept_type_id = $payroll_item->payroll_concept_type;

            if ($concept_type_id == ORDINARY || $concept_type_id == OVERTIME_OR_SURCHARGES || $concept_type_id == SALARY_PAYMENTS ) {
                if ($concept_id == SALARY) {
                    $salary_base += $this->getValuePartTimeSalary($concept_id, $workday, $days_quantity_salary, $amount);
                } else {
                    $salary_base += $payroll_item->amount;
                }
            }

            if ($concept_type_id == DISABILITIES || $concept_type_id == PAID_LICENSE_TYPE || $concept_type_id == TIME_VACATION_TYPE) {
                $salary_base += $this->getValuePartTimeSalary($concept_id, $workday, $days_quantity_salary, $amount);
                $days_quantity += $payroll_item->days_quantity;
            }

            if ($concept_type_id == OTHER_ACCRUED_TYPE && $concept_id == OTHER_ACCRUED_CONSTITUENTS_SALARY) {
                $salary_base += $payroll_item->amount;
            }

            if ($concept_type_id == NOT_PAID_LICENSE_TYPE || $concept_type_id == UNEXCUSED_ABSENTEEISM_TYPE) {
                $salaryBasePension += ($employee_data->base_amount / 30) * $days_quantity_salary;

                if ($payroll_item->applied == NOT) {
                    $days_quantity_salary = $payroll_item->days_quantity;
                    $days_quantity_not_paid_license = $payroll_item->days_quantity;

                    $this->Payroll_management_model->update_item(['applied'=>YES], $payroll_item->id);
                }
            }
        }

        $new_salary_base = $salary_base;
        $new_salary = ($integral_salary == YES) ? ($new_salary_base * 0.7) : $new_salary_base;

        foreach ($payroll_items_data as $payroll_item) {
            $amount = 0;
            $concept_id = $payroll_item->payroll_concept_id;
            if ($concept_id == HEALTH_RATE || $concept_id == HEALTH_RATE_LESS_THAN) {
                $amount = $new_salary * ($payroll_item->percentage / 100);
                $days_quantity = $payroll_item->days_quantity;

                $this->Payroll_management_model->update_item(['amount' => $amount, 'days_quantity' => $days_quantity - $days_quantity_not_paid_license + $days_quantity_to_add], $payroll_item->id);
            }

            if ($concept_id == PENSION_RATE || $concept_id == HIGH_RISK_PENSION_RATE) {
                $amount = ($new_salary + $salaryBasePension) * ($payroll_item->percentage / 100);
                $days_quantity = $payroll_item->days_quantity;

                $this->Payroll_management_model->update_item(['amount' => $amount, 'days_quantity' => $days_quantity /*- $days_quantity_not_paid_license */ + $days_quantity_to_add], $payroll_item->id);
            }
        }
    }

    private function getValuePartTimeSalary($concept_id, $workday, $days_quantity_salary, $amount)
    {
        if ($workday == HALFTIME) {
            if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                if ($amount < ($this->Payroll_settings->minimum_salary_value / 2)) {
                    $salaryBase = ($this->Payroll_settings->minimum_salary_value / 30) * $days_quantity_salary;
                } else {
                    $salaryBase = $amount;
                }
            } else {
                if ($amount < $this->Payroll_settings->minimum_salary_value) {
                    $salaryBase = ($this->Payroll_settings->minimum_salary_value / 30) * $days_quantity_salary;
                } else {
                    $salaryBase = $amount;
                }
            }
        } else {
            $salaryBase = $amount;
        }

        return $salaryBase;
    }

    public function delete_ajax()
    {
        $payroll_id = $this->input->get('payroll_id');

        $deleted_items = $this->Payroll_management_model->delete_items($payroll_id);
        if ($deleted_items == TRUE) {
            $deleted_payroll = $this->Payroll_management_model->delete($payroll_id);
            if ($deleted_payroll == TRUE) {
                echo json_encode(TRUE);
            } else {
                echo json_encode(FALSE);
            }
        } else {
            echo json_encode(FALSE);
        }
    }

    private function generate_alphanumeric_code($strength = 10)
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($permitted_chars);
        $random_string = '';
        for ($i = 0; $i < $strength; $i++) {
            $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }

    public function calculationParafiscalAndProvisions($payrollId, $employeeId, $contractId)
    {
        $this->calculate_social_security_parafiscal($payrollId, $employeeId, $contractId);
        // $this->calculate_provisions($payrollId, $employeeId, $contractId);

        $this->session->set_flashdata("message", "Calculo terminado");
        redirect($_SERVER["HTTP_REFERER"]);
    }

    private function calculate_social_security_parafiscal(int $payroll_id, int $employee_id, int $contract_id)
    {
        $this->Payroll_social_security_parafiscal_model->delete($payroll_id, $employee_id, $contract_id);


        $arl_risk_classes = $this->Payroll_contracts_model->get_arl_risk_classes();
        $employee_contract = $this->Payroll_contracts_model->get_by_id($contract_id);

        /**
         * PENSIÓN
         */
        if ($employee_contract->employee_type == PRE_PENSION_OF_ENTITY_IN_LIQUIDATION ||
            $employee_contract->employee_type == PRE_PENSIONED_WITH_VOLUNTARY_CONTRIBUTION_TO_HEALTH ||
            $employee_contract->employee_type == APPRENTICES_SENA_LECTIVA ||
            $employee_contract->employee_type == APPRENTICES_SENA_PRODUCTIVE) {
        } else {
            $base_salary = $this->calculation_salary_base($payroll_id, $employee_id, TRUE, $contract_id);
            $percentage = ($employee_contract->retired_risk == YES) ? $this->Payroll_settings->high_risk_pension_percentage : $this->Payroll_settings->pension_percentage;
            $description = ($employee_contract->retired_risk == YES) ? "Pensión de alto riesgo" : "Pensión";
            $data[] = [
                "payroll_id" => $payroll_id,
                "contract_id" => $contract_id,
                "employee_id" => $employee_id,
                "description" => $description,
                "base_salary" => $base_salary,
                "percentage" => $percentage,
                "amount" => $base_salary * ($percentage / 100)
            ];
        }

        /**
         * ARL
         */
        if ($employee_contract->employee_type == APPRENTICES_SENA_LECTIVA) {
        } else {
            $percentage = 0;
            $risk_level_name = "";
            $base_salary = $this->calculation_salary_base($payroll_id, $employee_id, FALSE, $contract_id);
            foreach ($arl_risk_classes as $key => $risk_classes) {
                if ($risk_classes->id == $employee_contract->arl_risk_classes) {
                    $risk_level_name = $risk_classes->name . " - " . $risk_classes->description;
                    $percentage = $risk_classes->percentage;
                }
            }
            $data[] = [
                "payroll_id" => $payroll_id,
                "contract_id" => $contract_id,
                "employee_id" => $employee_id,
                "description" => $risk_level_name,
                "base_salary" => $base_salary,
                "percentage" => $percentage,
                "amount" => $base_salary * ($percentage / 100)
            ];
        }

        /**
         * CAJA DE COMPENSACIÓN
         */
        $base_salary = $this->calculation_salary_base($payroll_id, $employee_id, FALSE, $contract_id);
        $percentage = $this->Payroll_settings->compensation_fund_percentage;
        $data[] = [
            "payroll_id" => $payroll_id,
            "contract_id" => $contract_id,
            "employee_id" => $employee_id,
            "description" => "Caja de compensación",
            "base_salary" => $base_salary,
            "percentage" => $percentage,
            "amount" => $base_salary * ($percentage / 100)
        ];

        /**
         * SALUD
         */
        $base_salary = $this->calculation_salary_base($payroll_id, $employee_id, TRUE, $contract_id);
        if ($employee_contract->employee_type == APPRENTICES_SENA_LECTIVA || $employee_contract->employee_type == APPRENTICES_SENA_PRODUCTIVE) {
            $percentage = 12.5;
            $data[] = [
                "payroll_id" => $payroll_id,
                "contract_id" => $contract_id,
                "employee_id" => $employee_id,
                "description" => "Salud",
                "base_salary" => $base_salary,
                "percentage" => $percentage,
                "amount" => $base_salary * ($percentage / 100)
            ];
        } else {
            if ($this->Payroll_settings->exempt_parafiscal_health_payments == NOT) {
                $percentage = 8.5;
                $data[] = [
                    "payroll_id" => $payroll_id,
                    "contract_id" => $contract_id,
                    "employee_id" => $employee_id,
                    "description" => "Salud",
                    "base_salary" => $base_salary,
                    "percentage" => $percentage,
                    "amount" => $base_salary * ($percentage / 100)
                ];
            } else {
                if ($base_salary > ($this->Payroll_settings->minimum_salary_value * 10)) {
                    $percentage = 8.5;
                    $data[] = [
                        "payroll_id" => $payroll_id,
                        "contract_id" => $contract_id,
                        "employee_id" => $employee_id,
                        "description" => "Salud",
                        "base_salary" => $base_salary,
                        "percentage" => $percentage,
                        "amount" => $base_salary * ($percentage / 100)
                    ];
                }
            }
        }

        /**
         * SENA
         */
        if ($this->Payroll_settings->exempt_parafiscal_health_payments == NOT) {
            $percentage = 2.0;
            $data[] = [
                "payroll_id" => $payroll_id,
                "contract_id" => $contract_id,
                "employee_id" => $employee_id,
                "description" => "SENA",
                "base_salary" => $base_salary,
                "percentage" => $percentage,
                "amount" => $base_salary * ($percentage / 100)
            ];
        } else {
            if ($base_salary > ($this->Payroll_settings->minimum_salary_value * 10)) {
                $percentage = 2.0;
                $data[] = [
                    "payroll_id" => $payroll_id,
                    "contract_id" => $contract_id,
                    "employee_id" => $employee_id,
                    "description" => "SENA",
                    "base_salary" => $base_salary,
                    "percentage" => $percentage,
                    "amount" => $base_salary * ($percentage / 100)
                ];
            }
        }

        /**
          * ICBF
          */
        if ($this->Payroll_settings->exempt_parafiscal_health_payments == NOT) {
            $percentage = 3.0;
            $data[] = [
                "payroll_id" => $payroll_id,
                "contract_id" => $contract_id,
                "employee_id" => $employee_id,
                "description" => "ICBF",
                "base_salary" => $base_salary,
                "percentage" => $percentage,
                "amount" => $base_salary * ($percentage / 100)
            ];
        } else {
            if ($base_salary > ($this->Payroll_settings->minimum_salary_value * 10)) {
                $percentage = 3.0;
                $data[] = [
                    "payroll_id" => $payroll_id,
                    "contract_id" => $contract_id,
                    "employee_id" => $employee_id,
                    "description" => "ICBF",
                    "base_salary" => $base_salary,
                    "percentage" => $percentage,
                    "amount" => $base_salary * ($percentage / 100)
                ];
            }
        }

        if (!empty($data)) { $this->Payroll_social_security_parafiscal_model->insert($data); }
    }

    private function calculation_salary_base(int $payroll_id, int $employee_id, $pension = FALSE, $contract_id = null)
    {
        $base_salary = 0;
        $additional_base_salary = 0;
        $payroll = $this->Payroll_management_model->get_by_id($payroll_id);
        $ids = $this->Payroll_management_model->get_payroll_grouped_by_month($payroll->year, $payroll->month);
        $payroll_items = $this->Payroll_management_model->get_payroll_items_by_payroll_id($ids->ids, $employee_id, $contract_id);

        if (!empty($payroll_items)) {
            foreach ($payroll_items as $item) {
                $concept_id = $item->payroll_concept_id;
                if ($concept_id == SALARY || $concept_id == COMMISSIONS ||
                    $concept_id == INCENTIVES || $concept_id == BONUSES_CONSTITUTING_SALARY ||
                    $concept_id == PER_DIEM_CONSTITUTES_SALARY || $concept_id == DAYTIME_OVERTIME ||
                    $concept_id == NIGHT_OVERTIME || $concept_id == NIGHT_SURCHARGE ||
                    $concept_id == HOLIDAY_DAYTIME_OVERTIME || $concept_id == HOLIDAY_DAYTIME_SURCHARGE ||
                    $concept_id == HOLIDAY_NIGHT_OVERTIME || $concept_id == HOLIDAY_NIGHT_SURCHARGE ||
                    $concept_id == PER_DIEM_NOT_CONSTITUTES_SALARY) {
                    if ($item->earned_deduction == EARNED) {
                        $base_salary += $item->amount;
                    }
                }

                if ($pension == TRUE) {
                    if ($concept_id == COMMON_DISABILITY || $concept_id == PROFESSIONAL_DISABILITY ||
                        $concept_id == WORK_DISABILITY || $concept_id == PAID_PERMIT ||
                        $concept_id == PAID_LICENSE || $concept_id == DUEL_LICENSE ||
                        $concept_id == MATERNITY_LICENSE || $concept_id == PATERNITY_LICENSE) {
                            $additional_base_salary = $item->amount;
                        }
                }
            }
        }
        return $base_salary + $additional_base_salary;
    }

    private function log_user_activity(int $id, int $event_type, stdClass $payroll_data = NULL)
    {
        $description = 'El usuario ' . $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name') . ' hizo lo siguiente cambios en Nómina: ';

        if ($event_type == EDITION) {
            if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                $description .= 'Actualizó la nómina de ' . lang('months')[$payroll_data->month] . ' de la ' . lang('biweekly_names')[$payroll_data->number];
            } else if ($this->Payroll_settings->payment_frequency == MONTHLY) {
                $description .= 'Actualizó la nómina de ' . lang('months')[$payroll_data->month];
            }
        } else if ($event_type == ELIMINATION) {
            if ($this->Payroll_settings->payment_frequency == BIWEEKLY) {
                $description .= 'Eliminó la nómina de ' . lang('months')[$payroll_data->month] . ' de la ' . lang('biweekly_names')[$payroll_data->number];
            } else if ($this->Payroll_settings->payment_frequency == MONTHLY) {
                $description .= 'Eliminó la nómina de ' . lang('months')[$payroll_data->month];
            }
        }


        $log_user_data = [
            'date' => date('Y-m-d H:m:i'),
            'type_id' => $event_type,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => 'payroll',
            'table_name' => 'payroll',
            'record_id' => $id,
            'description' => $description
        ];

        $this->UserActivities_model->insert($log_user_data);
    }

    public function remove_novetly($id)
    {
        $payroll_item = $this->Payroll_management_model->get_payroll_items_by_id($id);

        if ($this->Payroll_management_model->delete_item($id)) {
            $concept_id = $payroll_item->payroll_concept_id;
            $payroll_id = $payroll_item->payroll_id;
            $employee_id = $payroll_item->employee_id;
            $contract_id = $payroll_item->contract_id;
            $start_date = $payroll_item->start_date;
            $end_date = $payroll_item->end_date;
            $daysQuantityNotPaid = 0;

            $futureItems = $this->Payroll_management_model->find_payroll_future_items(["payroll_item_id"=>$id]);
            if (!empty($futureItems)) {
                foreach ($futureItems as $futureItem) {
                    $this->Payroll_management_model->delete_future_item($futureItem->id);
                }
            }

            if ($concept_id == PAID_LICENSE ||
                $concept_id == FAMILY_ACTIVITY_PERMIT ||
                $concept_id == DUEL_LICENSE ||
                $concept_id == PAID_PERMIT ||
                $concept_id == MATERNITY_LICENSE ||
                $concept_id == PATERNITY_LICENSE ||
                $concept_id == COMMON_DISABILITY ||
                $concept_id == PROFESSIONAL_DISABILITY ||
                $concept_id == WORK_DISABILITY ||
                $concept_id == DISCIPLINARY_SUSPENSION ||
                $concept_id == UNEXCUSED_ABSENTEEISM ||
                $concept_id == NOT_PAID_LICENSE ||
                $concept_id == NOT_PAID_PERMIT ||
                $concept_id == TIME_VACATION ||
                $concept_id == SUNDAY_DISCOUNT) {

                $days_quantity = $payroll_item->days_quantity;
                $days_quantity_by_transportation_allowance = $days_quantity;

                if ($concept_id == TIME_VACATION) {
                    $holidays = $this->Calendar_model->get_holidays($start_date, $end_date);
                    $number_holidays = ($holidays == FALSE) ? 0 : count($holidays);

                    $tiempoInicio = strtotime($start_date);
                    $tiempoFin = strtotime($end_date);
                    while ($tiempoInicio <= $tiempoFin) {
                        if (date('w', $tiempoInicio) == 0) {
                            $number_holidays++;
                        }
                        $tiempoInicio += 86400;
                    }

                    $days_quantity_by_transportation_allowance += $number_holidays;
                }

                $this->calculate_salary($payroll_id, $employee_id, -abs($days_quantity), $contract_id);
                $this->calculate_transportation_allowance($payroll_id, $employee_id, -abs($days_quantity_by_transportation_allowance), $contract_id);

                if ($concept_id == DISCIPLINARY_SUSPENSION || $concept_id == UNEXCUSED_ABSENTEEISM || $concept_id == NOT_PAID_LICENSE || $concept_id == NOT_PAID_PERMIT || $concept_id == SUNDAY_DISCOUNT) {
                    $daysQuantityNotPaid = $payroll_item->days_quantity;
                }
            }

            if (!in_array($concept_id, [ LAYOFFS, LAYOFFS_INTERESTS, TIME_VACATION, MONEY_VACATION, SERVICE_BONUS ])) {
                $this->calculate_social_security_by_employee_id($payroll_id, $employee_id, $contract_id, $daysQuantityNotPaid);
                $this->calculateProvisions($payroll_id);
                // $this->calculate_social_security_parafiscal($payroll_id, $employee_id, $contract_id);
                $this->calculate_payroll_earned_deductions($payroll_id);
            }


            $this->session->set_flashdata("message", "Novedad eliminada");
        } else {
            $this->session->set_flashdata("error", "La novedad no pudo ser eliminada");
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function detect_contract_termination()
    {
        $contracts = $this->Payroll_contracts_model->get();
        foreach ($contracts as $contract) {
            $currentDate = date('Y-m-d');
            $endDate = $contract->end_date;
            $settlementDate = $contract->settlement_date;

            if ($endDate != '0000-00-00' && $endDate < $currentDate) {
                $this->Payroll_contracts_model->update(['contract_status'=>INACTIVE], $contract->companies_id, $contract->id);
            } else if ($settlementDate != '0000-00-00' && $settlementDate < $currentDate) {
                $this->Payroll_contracts_model->update(['contract_status'=>INACTIVE], $contract->companies_id, $contract->id);
            }
        }
    }

    public function remove_employee_payroll($payroll_id, $employee_id, $contract_id)
    {
        $future_items = $this->FutureItems_model->get_by_data($payroll_id, $contract_id);
        if (!empty($future_items)) {
            foreach ($future_items as $future_item) {
                if ($future_item->from_contract == YES) {
                    $this->PayrollContractConcepts_model->update(['applied'=>NOT], $contract_id, $employee_id, $future_item->concept_id);
                }

                $this->FutureItems_model->delete($payroll_id, $employee_id, $contract_id, $future_item->concept_id);
            }
        }

        if ($this->Payroll_management_model->delete_items_by_employee_id($payroll_id, $employee_id, $contract_id)) {
            $payroll_items = $this->Payroll_management_model->get_payroll_items($payroll_id);
            if (count($payroll_items) <= 0) {
                if ($this->Payroll_management_model->delete($payroll_id)) {
                    $this->session->set_flashdata('message', lang('employee_removed'));
                    admin_redirect('payroll_management');
                }
            }

            $this->calculate_payroll_earned_deductions($payroll_id);

            $this->session->set_flashdata('message', lang('employee_removed'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->session->set_flashdata('error', lang('employee_was_not_removed'));
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function pay_slip($payrollId, $employeeId, $contractId)
    {
        $concepts = $this->Payroll_concepts_model->get();
        $payroll = $this->Payroll_management_model->get_by_id($payrollId);
        $paymentMeans = $this->Payroll_contracts_model->get_payment_means();
        $employee = $this->Employees_model->get_by_id($employeeId, $contractId);
        $biller = $this->site->getAllCompaniesWithState('biller', $payroll->biller_id);
        $items = $this->Payroll_management_model->get_payroll_items_by_payroll_id_employee_id($payrollId, $employeeId, $contractId);
        $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $conceptsPaidEmployee = $electronicPayrollItems = [];
        if (!empty($items)) {
            foreach ($items as $item) {
                $id = $item->id;
                $concept_id = $item->payroll_concept_id;
                $paidToEmployee = $item->paid_to_employee;
                if (($concept_id == MONEY_VACATION || $concept_id == SERVICE_BONUS || $concept_id == LAYOFFS || $concept_id == LAYOFFS_INTERESTS) && $paidToEmployee == NOT) {
                    $conceptsPaidEmployee[] = $item;
                } else {
                    $electronicPayrollItems[] = $item;
                }
            }
        }

        $imagePath = 'assets/uploads/logos/'.$biller->logo;
        $this->site->getExistingImage($imagePath);

        foreach ($paymentMeans as $paymentMean) {
            if ($paymentMean->code == $employee->payment_method) {
                $paymentMethod = $paymentMean->name;
            }
        }

        $periodMonth = date("n", strtotime($payroll->start_date));
        $periodYear = date("Y", strtotime($payroll->start_date));

        $this->data['fortnight'] = ($payroll->frequency == MONTHLY) ? '' : (($payroll->number == 1) ? '1ra Quincena' : '2da Quincena');
        $this->data['concepts'] = $concepts;
        $this->data['imagePath'] = $imagePath;
        $this->data['billerName'] = $biller->name;
        $this->data['company'] = $biller->company;
        $this->data['billerEmail'] = $biller->email;
        $this->data['billerPhone'] = $biller->phone;
        $this->data['pageTitle'] = lang('pay_slip');
        $this->data['paymentMethod'] = $paymentMethod;
        $this->data['employeeName'] = $employee->name;
        $this->data['electronicPayrollItems'] = $electronicPayrollItems;
        $this->data['billerAddress'] = $biller->address;
        $this->data['employeeEmail'] = $employee->email;
        $this->data['employeePhone'] = $employee->phone;
        $this->data['employeeVatNo'] = $employee->vat_no;
        $this->data['employeeAddress'] = $employee->address;
        $this->data['employeeBaseAmount'] = $employee->base_amount;
        $this->data['conceptsPaidEmployee'] = $conceptsPaidEmployee;
        $this->data['billerLocation'] = $biller->city.', '.$biller->state;
        $this->data['numeroDocumento'] = $this->Settings->numero_documento;
        $this->data['defaultCurrency'] = $this->Settings->default_currency;
        $this->data['period'] = $months[$periodMonth-1]. ' de '. $periodYear;
        $this->data['employeeLocation'] = $employee->city.', '.$employee->state;
        $this->data['generationDate'] = date("d/m/Y", strtotime($payroll->creation_date));
        $this->data['employeeProfessionalPositionName'] = $employee->professional_position_name;
        $this->data['ivaRetainer'] = ($this->Settings->iva_retainer == NOT) ? "No somos" : "Somos";
        $this->data['icaRetainer'] = ($this->Settings->ica_retainer == NOT) ? "No somos" : "Somos";
        $this->data['fuenteRetainer'] = ($this->Settings->fuente_retainer == NOT) ? "No somos" : "Somos";
        $this->data['greatContributor'] = ($this->Settings->great_contributor == NOT) ? "No somos" : "Somos";

        $this->load_view($this->theme. 'payroll_management/pay_slip.php', $this->data);
    }

    public function to_pay($payroll_id)
    {
        $approved_payroll = $this->Payroll_management_model->update(['status' => PAID], $payroll_id);
        if ($approved_payroll == TRUE) {
            $this->session->set_flashdata('message', lang("payroll_paid"));
        } else {
            $this->session->set_flashdata('error', lang("payroll_unpaid"));
        }

        admin_redirect($_SERVER["HTTP_REFERER"]);
    }

    public function generatePayrollPDF($payrollId)
    {
        $concepts = $this->Payroll_concepts_model->get();
        $payroll = $this->Payroll_management_model->get_by_id($payrollId);
        $biller = $this->site->getAllCompaniesWithState('biller', $payroll->biller_id);
        $items = $this->Payroll_management_model->get_payroll_items_to_PDF($payrollId);
        $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $socialSecurity = $this->Payroll_management_model->getSocialSecurityToPDF($payrollId);
        $healthPension = $this->Payroll_management_model->getHealthPensionEmployee($payrollId);
        $parafiscal = $this->Payroll_management_model->getParaficalToPDF($payrollId);
        $provisions = $this->Payroll_management_model->getProvisionsToPDF($payrollId);

        $hideShowParafiscalProvisions = FALSE;
        if ($payroll->frequency == MONTHLY) {
            $hideShowParafiscalProvisions = TRUE;
        } else if ($payroll->frequency == BIWEEKLY) {
            if ($payroll->number == SECOND_FORTNIGHT) {
                $hideShowParafiscalProvisions = TRUE;
            }
        }

        $imagePath = 'assets/uploads/logos/'.$biller->logo;
        $this->site->getExistingImage($imagePath);

        $periodMonth = date("n", strtotime($payroll->start_date));
        $periodYear = date("Y", strtotime($payroll->start_date));

        $this->data['concepts'] = $concepts;
        $this->data['payrollItems'] = $items;
        $this->data['imagePath'] = $imagePath;
        $this->data['parafiscal'] = $parafiscal;
        $this->data['provisions'] = $provisions;
        $this->data['billerName'] = $biller->name;
        $this->data['company'] = $biller->company;
        $this->data['billerEmail'] = $biller->email;
        $this->data['billerPhone'] = $biller->phone;
        $this->data['healthPension'] = $healthPension;
        $this->data['socialSecurity'] = $socialSecurity;
        $this->data['statusPayroll'] = $payroll->status;
        $this->data['pageTitle'] = lang('spreadsheet').' '.lang('payroll');
        $this->data['numeroDocumento'] = $this->Settings->numero_documento;
        $this->data['defaultCurrency'] = $this->Settings->default_currency;
        $this->data['period'] = $months[$periodMonth-1]. ' de '. $periodYear;
        $this->data['endDate']= date("Y/m/d", strtotime($payroll->end_date));
        $this->data['startDate']= date("Y/m/d", strtotime($payroll->start_date));
        $this->data['hideShowParafiscalProvisions'] = $hideShowParafiscalProvisions;
        $this->data['generationDate'] = date("d/m/Y", strtotime($payroll->creation_date));
        $this->data['minimumSalaryValue'] = $this->Payroll_settings->minimum_salary_value;
        $this->data['transportationAllowanceValue'] = $this->Payroll_settings->transportation_allowance_value;

        $this->load_view($this->theme. 'payroll_management/payroll.php', $this->data);
    }

    public function getFrequenciesPayroll()
    {
        setlocale(LC_TIME, 'es_ES.utf8');

        $frequenciesArray = [];
        $dateRecordsFilter = $this->input->post('date_records_filter');
        $start_date = $this->input->post('start_date');
        $startDate = $this->sma->fld($start_date);
        $end_date = $this->input->post('end_date');
        $endDate = $this->sma->fld($end_date);
        $message = '';
        $months =  [
            1 => "Enero",
            2 => "Febrero",
            3 => "Marzo",
            4 => "Abril",
            5 => "Mayo",
            6 => "Junio",
            7 => "Julio",
            8 => "Agosto",
            9 => "Septiembre",
            10 => "Octubre",
            11 => "Noviembre",
            12 => "Diciembre"
        ];

        $frequencies = $this->Payroll_management_model->getAmountFrequenciesPayroll(['dateRecordsFilter' => $dateRecordsFilter, 'start_date' => $startDate, "end_date" => $endDate]);
        if (!empty($frequencies)) {
            foreach ($frequencies as $frequency) {
                if (!in_array($frequency->frequency, $frequenciesArray)) {
                    $frequenciesArray[] = $frequency->frequency;
                }

                $frequencyString = ($frequency->frequency == MONTHLY) ? "Mensual" : "Quincenal";

                $message .= "Año: {$frequency->year} Mes: {$months[$frequency->month]}: {$frequencyString} \n";
            }
        }
        echo json_encode(["status" => ((count($frequenciesArray) > 1) ? TRUE : FALSE), "message" => $message]);
    }

    public function getDateRangeForPayroll($payrollId, $contractId) {
        $payroll = $this->Payroll_management_model->get_by_id($payrollId);
        $payrollIds   = $this->Payroll_management_model->get_payroll_grouped_by_month($payroll->year, $payroll->month);
        $datesPayroll = $this->Payroll_management_model->getMinimumMaximumPayrollDate($payrollIds->ids);

        $starDatePayroll = strtotime(date("Y-m-d", strtotime($datesPayroll->start_date)));
        $endDatePayroll = strtotime(date("Y-m-d", strtotime($datesPayroll->end_date)));


        $contract = $this->Payroll_contracts_model->get_by_id($contractId);
        $type_contract = $contract->contract_type;

        $start_date_contract = strtotime($contract->start_date);
        $end_date_contract = (strtotime($contract->end_date) <= 0) ? 0 : strtotime($contract->end_date);
        $settlement_date_contract = strtotime($contract->settlement_date);

        if ($settlement_date_contract > 0) {
            $end_date_contract = $settlement_date_contract;
        }

        if ($start_date_contract <= $starDatePayroll) {
            if ($end_date_contract <= $starDatePayroll) {
                if ($end_date_contract == 0) {
                    if ($type_contract == UNDEFINED_TERM) {
                        $initial= $starDatePayroll; 
                        $end    = $endDatePayroll;
                    }
                } 
            } else {
                if ($end_date_contract <= $endDatePayroll) { // YA
                    $initial= $starDatePayroll;
                    $end    = $end_date_contract;
                } else {
                    $initial = $starDatePayroll; 
                    $end     = $endDatePayroll;
                }
            }
        } else {
            if ($start_date_contract <= $endDatePayroll) {
                if ($end_date_contract <= $endDatePayroll) {
                    if ($end_date_contract == NULL) {
                        $initial = $start_date_contract;
                        $end     = $endDatePayroll;
                    } else {
                        $initial = $start_date_contract;
                        $end     = $$end_date_contract;
                    }
                } else {
                    $initial= $start_date_contract; 
                    $end    = $endDatePayroll;
                }
            }
        } 

        $diffDays = $this->getNumberDaysPayroll($initial, $end);

        return $diffDays;
    }

}

/* End of file Payroll_management.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/controllers/admin/Payroll_management.php */