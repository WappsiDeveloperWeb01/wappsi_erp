<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('sales/order_actions', 'id="action-form"');
    } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="paymentsTable" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><?= lang("payment"); ?></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("biller"); ?></th>
                                            <th><?= lang("customer"); ?></th>
                                            <th><?= lang("vat_no"); ?></th>
                                            <th><?= lang("capital"); ?></th>
                                            <th><?= lang("current_interest"); ?></th>
                                            <th><?= lang("default_interest"); ?></th>
                                            <th><?= lang("procredito_commission"); ?></th>
                                            <th><?= lang("procredito_interest"); ?></th>
                                            <th><?= lang("total_to_pay"); ?></th>
                                            <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="12" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        loadNoPrintFormat()
        openPrintWindow()
        loadDataTables()

        $(document).on('click', '.details td:not(:last-child)', function() {
            let reference = $(this).parent('tr').attr('reference')

            $('#myModal').modal({remote: site.base_url + 'financing/details/' + reference});
            $('#myModal').modal('show');
        })
    });

    function loadNoPrintFormat() {
        const noPrintFormat = '<?= $noPrintFormat ?>'
        if (noPrintFormat == 1) {
            setTimeout(function() {
                window.close()
            }, 3000)
        }
    }

    function loadDataTables()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#paymentsTable').dataTable({
            "aaSorting": [[2, 'desc']],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('financing/getPayments') ?>',
            dom: '<"col-sm-7 containerBtn"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>',
            fnServerData: function (sSource, aoData, fnCallback)
            {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (row, data, displayIndex)
            {
                let paymentReference = data[1]

                $(row).addClass('details')
                $(row).attr('reference', paymentReference)

                return row
            },
            drawCallback: function(settings)
            {
                $('.actionsButtonContainer').html(`<a href="<?= admin_url('financing/paymentFinancingFee') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>`);

                $('input[type="checkbox"]').not('.skip').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%'
                });

                $('[data-toggle-second="tooltip"]').tooltip();
            },
            "aoColumns": [
                { visible: false },
                null,
                null,
                null,
                null,
                null,
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                null,

            ]
        });
    }

    function openPrintWindow()
    {
        <?php if (isset($installmentNo)) { ?>
            <?php $this->session->unset_userdata('installmentNo'); ?>

            installmentNo = '<?= $installmentNo ?>'
            window.open(site.base_url + `financing/printReceipt/${installmentNo}`, '_blank');
        <?php } ?>
    }
</script>