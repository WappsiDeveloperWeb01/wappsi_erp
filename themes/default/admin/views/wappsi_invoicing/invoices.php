<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {

        oTable = $('#CURData').dataTable({
            "aaSorting": [1, "desc"],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('wappsi_invoicing/get_invoices') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex)
            {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "wappsi_invoicing_link";
                return nRow;
            },
            "aoColumns": [
                            {"bSortable": false, "mRender": checkbox}, 
                            null,
                            null,
                            {"mRender": tipo_mov_wappsi_invoicing }, 
                            null,
                            {"mRender": decimalFormat},
                            {"mRender": decimalFormat},
                            {"mRender": currencyFormat},
                            {"mRender": payment_status_wappsi_invoicing }, 
                            {"bSortable": false}
                        ]
        });
    });
</script>
<?= admin_form_open('system_settings/invoice_actions', 'id="action-form"') ?>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $page_title ?></h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?php echo admin_url('system_settings/add_invoice'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus"></i> <?= lang('add_invoice') ?>
                                        </a>
                                        </li>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#" id="delete" data-action="delete">
                                            <i class="fa fa-trash-o"></i> <?= lang('deactivate_invoices') ?>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-lg-12">
                        <div class="alert alert-warning" role="alert"><p><b><i class="fa fa-exclamation-circle"></i> Importante : </b>información disponible sólo a partir de Junio del 2017</p> </div>
                            <!-- <p class="introtext"><?//= lang("list_results"); ?></p> -->
                            <div class="table-responsive">
                                <table id="CURData" class="table table-bordered table-hover table-condensed reports-table">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkth" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("date"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("movement_type"); ?></th>
                                        <th><?= lang("description"); ?></th>
                                        <th><?= lang("quantity"); ?></th>
                                        <th><?= lang("unit_quantity"); ?></th>
                                        <th><?= lang("total"); ?></th>
                                        <th><?= lang("payment_status"); ?></th>
                                        <th style="width:65px;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->
<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>

