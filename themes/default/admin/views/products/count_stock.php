<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'stForm'); echo admin_form_open_multipart("products/count_stock", $attrib); ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "biller"); ?>
                                                <?php
                                                $wh[''] = '';
                                                foreach ($billers as $biller) {
                                                    $wh[$biller->id] = $biller->name;
                                                }
                                                echo form_dropdown('biller', $wh, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="biller" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } else {
                                        $biller_input = array(
                                            'type' => 'hidden',
                                            'name' => 'biller',
                                            'id' => 'biller',
                                            'value' => $this->session->userdata('biller_id'),
                                            );

                                        echo form_input($biller_input);
                                    } ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("reference_no", "document_type_id"); ?>
                                            <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                        </div>
                                    </div>
                                    <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("warehouse", "warehouse"); ?>
                                                <?php
                                                    $wh = [];
                                                $wh[''] = '';
                                                foreach ($warehouses as $warehouse) {
                                                    $wh[$warehouse->id] = $warehouse->name;
                                                }
                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="warehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } else {
                                        $warehouse_input = array(
                                            'type' => 'hidden',
                                            'name' => 'warehouse',
                                            'id' => 'warehouse',
                                            'value' => $this->session->userdata('warehouse_id'),
                                            );

                                        echo form_input($warehouse_input);
                                    } ?>

                                    <?php if ($Owner || $Admin) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("date", "date"); ?>
                                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld(date('Y-m-d H:i:s'))), 'class="form-control input-tip" id="date" required="required"'); ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="col-md-4 form-group">
                                        <?= lang('third', 'company_id') ?>
                                        <?php
                                        $cOptions[''] = lang('select_third');
                                        foreach ($companies as $row => $company) {
                                            $cOptions[$company['id']] = $company['name'];
                                        }

                                         ?>
                                        <?= form_dropdown('company_id', $cOptions, '',' id="company_id" class="form-control select" required style="width:100%;"'); ?>
                                    </div>


                                    <div class="col-md-4 form-group">
                                        <label><?= lang('do_automatic_adjustment') ?></label><br>
                                        <label>
                                            <input type="radio" name="automatic_adjustment" value="1" checked="true"> <?= lang('yes') ?>
                                        </label>
                                        <label>
                                            <input type="radio" name="automatic_adjustment" value="0"> <?= lang('no') ?>
                                        </label>

                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <label><?= lang("type"); ?> *</label>
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-2">
                                                            <input type="radio" class="checkbox type" value="full" name="type" id="full" <?= $this->input->post('type') ? 'checked="checked"' : ''; ?> required="required">
                                                            <label for="full" class="padding05">
                                                                <?= lang('full'); ?>
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-2">
                                                            <input type="radio" class="checkbox type" value="partial" name="type" id="partial" <?= $this->input->post('type') ? 'checked="checked"' : ''; ?>>
                                                            <label for="partial" class="padding05">
                                                                <?= lang('partial'); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="col-md-12 partials" style="display:none;">
                                    <div class="well well-sm">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("brands", "brand"); ?>
                                                    <?php
                                                    $wh = [];
                                                    foreach ($brands as $brand) {
                                                        $wh[$brand->id] = $brand->name;
                                                    }
                                                    echo form_dropdown('brand[]', $wh, (isset($_POST['brand']) ? $_POST['brand'] : 0), 'id="brand" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("brand") . '" style="width:100%;" multiple');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("categories", "category"); ?>
                                                    <?php
                                                    $wh = [];
                                                    foreach ($categories as $category) {
                                                        $wh[$category->id] = $category->name;
                                                    }
                                                    echo form_dropdown('category[]', $wh, (isset($_POST['category']) ? $_POST['category'] : 0), 'id="category" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("category") . '" style="width:100%;" multiple');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <div class="fprom-group">
                                            <?= form_submit('count_stock', lang("submit"), 'id="count_stock" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $("#brand option[value=''], #category option[value='']").remove();
        $('.type').on('ifChecked', function(e){
            var type_opt = $(this).val();
            if (type_opt == 'partial')
                $('.partials').slideDown();
            else
                $('.partials').slideUp();
            $('#stForm').bootstrapValidator('revalidateField', $(this));
        });
        $("#date").datetimepicker({format: site.dateFormats.js_ldate, fontAwesome: true, language: 'sma', weekStart: 1, todayBtn: 1, autoclose: 1, todayHighlight: 1, startView: 2, forceParse: 0, startDate: "<?= $this->sma->hrld(date('Y-m-d H:i:s')); ?>"});

        $('#biller').trigger('change');

    });

    $(document).on('change', '#biller', function (e) {
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/38/") ?>'+$('#biller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;

          $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }

          if (response.status == 0) {
            $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            localStorage.setItem('locked_for_biller_resolution', 1);
          }

          $('#document_type_id').trigger('change');
        });
    });
</script>