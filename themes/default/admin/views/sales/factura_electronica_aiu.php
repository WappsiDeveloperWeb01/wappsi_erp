<?php

/***********

FORMATO DE FACTURACIÓN ELECTRÓNICA (La colonia)

************/

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        //izquierda
        $this->RoundedRect(13, 7, 117, 29, 3, '1234', '');

        //izquierda
        if ($this->biller_logo == 2) {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,15,8.5,26);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,14,17,30);
        }

        $cx = 45;
        $cy = 13;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(mb_strtoupper($this->Settings->nombre_comercial)),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address." - ".ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 29, 3, '1234', '');

        $cx = 134;
        $cy =11;

        //derecha
        $this->SetFont('Arial','B',$this->fuente+(6));
        $this->setXY($cx, $cy);
        $this->MultiCell(40, 5,  $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'FACTURA DE VENTA'), '', 'L');
        $cy +=13;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'L');
        // $rutaQR = base_url().'themes/default/admin/assets/images/qr_code/'.$this->factura->reference_no.'.png';
        $rutaQR = FCPATH . 'themes/default/admin/assets/images/qr_code/' . $this->factura->reference_no . '.png';

        if (file_exists($rutaQR) && is_file($rutaQR)) {
            $this->Image($rutaQR, 180, 8.5, 26);
        }

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 38, 118, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 43, 118, 30, 3, '4', '');
        $cx = 13;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('INFORMACIÓN DEL CLIENTE'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->setXY($cx, $cy);
        // $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->customer->name != '-' ? $this->customer->name : $this->customer->company)),'',1,'L');
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 7;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->phone),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->email_negocio),'',1,'L');

        //derecha


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(131, 38, 78, 5, 3, '2', 'DF');
        $this->RoundedRect(131, 43, 78, 30, 3, '3', '');
        $cx = 131;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA FACTURA'),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('TOTAL'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente+1.5);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, $this->factura->grand_total * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA VENCE'),'LBR',1,'C',1);
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('PAGO'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".$this->factura->payment_term." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $payment_text = $this->factura->payment_status != 'paid' ? lang('credit').", " : '';
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($payment_text.$this->factura->payment_term ." ".($this->factura->payment_term > 1 ? "Días" : "Día")),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode('VENDEDOR'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $cy += $altura;
        $this->setXY($cx, $cy);
        if ($this->seller) {
            $this->Cell(78, $altura , $this->sma->utf8Decode(ucwords(mb_strtolower($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');
        } else {
            $this->Cell(78, $altura , $this->sma->utf8Decode('Ventas Varias'),'',1,'C');
        }

        $this->ln(2);


        $this->SetFont('Arial','B',$this->fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $current_X = $this->getX();
        $current_Y = $this->getY();
        $this->Cell(18.2, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(101, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(26.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        // $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(23.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(26.6, 8, $this->sma->utf8Decode(''),'TBLR',1,'C',1);

        $this->setXY($current_X, $current_Y);

        $this->Cell(18.2, 8, $this->sma->utf8Decode('Código'),'',0,'C',0);
        $this->Cell(101, 8, $this->sma->utf8Decode('Nombre producto'),'',0,'C',0);
        $this->Cell(26.6, 8, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        // $this->Cell(10.6, 8, $this->sma->utf8Decode('IVA'),'',0,'C',0);
        $this->Cell(23.6, 8, $this->sma->utf8Decode('Cantidad'),'',0,'C',0);
        $this->Cell(26.6, 8, $this->sma->utf8Decode('Subtotal'),'',1,'C',0);

        $this->setXY($current_X, $current_Y+4);

        $this->Cell(18.2, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(101, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(26.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        // $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(23.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(26.6, 4, $this->sma->utf8Decode(''),'',1,'C',0);
    }

    function Footer()
    {
        if ($this->description2 != '') {
            $this->SetFont('Arial','',$this->fuente);
            $this->SetXY(13, -24);
            $this->RoundedRect(13, 254, 196, 10, 0.8, '1234', '');
            $this->Cell(196, 4 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
            $this->Cell(196, 4 , $this->sma->utf8Decode('CUFE: '. $this->factura->cufe),'',1,'C');
        } else {
            $this->SetXY(13, -23);
            $this->RoundedRect(13, 255, 196, 10, 1, '1234', '');
            $this->Cell(196, 4 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
            $this->Cell(196, 4 , $this->sma->utf8Decode("CUFE : ".$this->factura->cufe),'',1,'C');
        }

        $this->SetXY(13, -14);
        $img_x = $this->getX()+125;
        $img_y = $this->getY()+1;
        $this->RotatedText(213, 270, $this->sma->utf8Decode('Fecha y hora validación DIAN: '. $this->validationDateTime), 90);
        $this->Cell(125, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS NIT 901.090.070-9     www.wappsi.com'),'',1,'L');
        $this->Image(base_url($this->technologyProviderLogo), $img_x, $img_y, 45);
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Página N° '.$this->PageNo()),'',1,'C');
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 805) {
            $text = substr($text, 0, 800);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();
$pdf->validationDateTime = $validationDateTime;
$pdf->technologyProviderLogo = $technologyProviderLogo;


$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->biller_logo = $biller_logo;
$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->tipo_regimen = $tipo_regimen;
$pdf->value_decimals = $value_decimals;
$pdf->AddPage();

$maximo_footer = 235;

$pdf->SetFont('Arial','',$fuente+2);

$taxes = [];
$total_bruto = 0;
$references = [];
    // exit(var_dump($rows));
    foreach ($rows as $item) {

        $total_bruto += (($item->net_unit_price + ($item->item_discount / $item->quantity)) * $item->quantity) * $trmrate;

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
        }

        $references[$item->id]['pr_code'] = ($show_code == 1 ? $item->product_code : $item->reference);
        $pr_name = $pdf->sma->reduce_text_length(($item->under_cost_authorized == 1 ? '     ' : '').
                                                $item->product_name.
                                                (!is_null($item->variant) && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? "( ".$item->variant." )" : '').
                                                ($pdf->Settings->show_brand_in_product_search && !is_null($item->brand_name) ? " - ".$item->brand_name : '').
                                                (!is_null($item->serial_no) ? " ".$item->serial_no : '').
                                                ($product_detail_promo == 1 && $item->price_before_promo > 0 ? " (".lang('discount_by_promo')." -".$this->sma->formatDecimal((($item->price_before_promo - ($item->tax_method == 1 ? $item->net_unit_price : $item->unit_price)) / $item->price_before_promo) * 100, 2)." %)" : '' ).
                                                strip_tags($this->sma->decode_html(($show_product_preferences == 1 && $item->preferences ? ' (' . $this->sma->print_preference_selection($item->preferences) . ')' : '')))
                                            , 500);
        $references[$item->id]['pr_name'] = $pr_name;
        $references[$item->id]['pr_discount'] = $item->discount;
        $references[$item->id]['pr_individual_discount'] = $item->item_discount / $item->quantity;
        $references[$item->id]['pr_tax'] = $item->tax;
        $references[$item->id]['pr_individual_tax'] = $item->item_tax / $item->quantity;
        $references[$item->id]['unit_price_1'] = ($item->net_unit_price + ($item->item_discount / $item->quantity)) * $trmrate;
        $references[$item->id]['unit_price_2'] = $item->net_unit_price * $trmrate;
        $references[$item->id]['unit_price_3'] = $item->unit_price * $trmrate;
        $references[$item->id]['pr_subtotal'] = $item->subtotal * $trmrate;
        $references[$item->id]['pr_quantity'] = $item->quantity;
        if (isset($item->operator)) {
            if ($item->operator == "*") {
                $references[$item->id]['pr_quantity'] = $item->quantity / $item->operation_value;
            } else if ($item->operator == "/") {
                $references[$item->id]['pr_quantity'] = $item->quantity * $item->operation_value;
            } else if ($item->operator == "+") {
                $references[$item->id]['pr_quantity'] = $item->quantity - $item->operation_value;
            } else if ($item->operator == "-") {
                $references[$item->id]['pr_quantity'] = $item->quantity + $item->operation_value;
            }
        }
        $references[$item->id]['product_unit_code'] = $item->product_unit_code;
    }

    $cnt = 1;
    foreach ($references as $reference => $data) {
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }

        $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        $columns[0]['inicio_x'] = $pdf->getX();
        $pdf->Cell(18.2, 5, $this->sma->utf8Decode($data['pr_code']),'',0,'C',0);
        $columns[0]['fin_x'] = $pdf->getX();
        $columns[1]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $pdf->setXY($cX, $cY+0.5);
        $pdf->MultiCell(101,3, $this->sma->utf8Decode($data['pr_name']),'','');
        $columns[1]['fin_x'] = $cX+101;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+101, $cY);
        $columns[2]['inicio_x'] = $pdf->getX();
        $pdf->Cell(26.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_3'])),'',0,'R',0);
        $columns[2]['fin_x'] = $pdf->getX();
        $columns[3]['inicio_x'] = $pdf->getX();
        $pdf->Cell(23.6, 5, $this->sma->utf8Decode($this->sma->formatQuantity($data['pr_quantity'], $qty_decimals)),'',0,'R',0);
        $columns[3]['fin_x'] = $pdf->getX();
        $columns[4]['inicio_x'] = $pdf->getX();
        $pdf->Cell(26.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['pr_subtotal'])),'',0,'R',0);
        $columns[4]['fin_x'] = $pdf->getX();
        $fin_fila_X = $pdf->getX();
            $fin_fila_Y = $pdf->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
            // exit(var_dump($columns));
            foreach ($columns as $key => $cell_data) {
                $ancho_column = $cell_data['fin_x'] - $cell_data['inicio_x'];
                if ($altura_fila < 5) {
                    $altura_fila = 5;
                }
                $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
            }
            $pdf->ln($altura_fila);
        $cnt++;

    }

if ($inv->order_tax > 0) {
    if (!isset($taxes[$inv->order_tax_id])) {
        $taxes[$inv->order_tax_id]['tax'] = $inv->order_tax;
        $taxes[$inv->order_tax_id]['base'] = $inv->total;
    } else {
        $taxes[$inv->order_tax_id]['tax'] += $inv->order_tax;
        $taxes[$inv->order_tax_id]['base'] += $inv->total;
    }
}

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(1,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(115.6,5, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->SetFont('Arial','',$fuente);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(14);
$pdf->SetFont('Arial','',$fuente);
// $pdf->Cell(115.6,6.125, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total, $currencies_names[$inv->sale_currency])),'',1,'L');
$pdf->MultiCell(115.6, 3, $this->sma->utf8Decode($number_convert->convertir((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total) * $trmrate), (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency))), 0, 'L');
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->setX(13);

$pdf->setXY($current_x, $current_y+8);


    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->Cell(38.53,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
    $pdf->Cell(38.53,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
    $pdf->Cell(38.53,5, $this->sma->utf8Decode('Valor Impuesto'),'',1,'C');
    // $pdf->Cell(38.53,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');
$pdf->SetFont('Arial','',$fuente+1.5);
$total = $inv->total;
if ($inv->order_tax_aiu_total > 0 && $inv->order_tax_aiu_base_apply_to == 1) {

    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $total+=$arr['tax'];

    $pdf->Cell(38.53,3, $this->sma->utf8Decode(isset($taxes_details[$inv->order_tax_aiu_id]) ? $taxes_details[$inv->order_tax_aiu_id] : 0),'',1,'C');

    $pdf->setXY($current_x + 38.53, $current_y);
    $pdf->setX($current_x + 38.53);
    $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatMoney($arr['base'] * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (38.53 * 2), $current_y);
    $pdf->setX($current_x + (38.53 * 2));
    $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->order_tax_aiu_total * $trmrate)),'',1,'C');

} else {
    foreach ($taxes as $tax => $arr) {
        $current_x = $pdf->getX();
        $current_y = $pdf->getY();

        $total+=$arr['tax'];

        $pdf->Cell(38.53,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : 0),'',1,'C');

        $minus_base = 0;
        if ($inv->order_tax_aiu_total > 0) {
            if ($inv->order_tax_aiu_base_apply_to == 2) {
                $minus_base = $inv->aiu_admin_total + $inv->aiu_imprev_total + $inv->aiu_utilidad_total;
            } else if ($inv->order_tax_aiu_base_apply_to == 3) {
                $minus_base = $inv->aiu_utilidad_total;
            }
        }

        $plus_base = 0;
        if ($inv->aiu_amounts_affects_grand_total == 1) {
            $plus_base = $inv->aiu_admin_total + $inv->aiu_imprev_total + $inv->aiu_utilidad_total;
        }

        $pdf->setXY($current_x + 38.53, $current_y);
        $pdf->setX($current_x + 38.53);
        $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatMoney(($arr['base'] - $minus_base + $plus_base) * $trmrate)),'',1,'C');

        $pdf->setXY($current_x + (38.53 * 2), $current_y);
        $pdf->setX($current_x + (38.53 * 2));
        $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatMoney($arr['tax'] * $trmrate)),'',1,'C');
    }
}



$total+=$inv->order_tax;

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$total+=$arr['tax'];
if ($inv->order_tax > 0) {
    $pdf->Cell(38.53,3, $this->sma->utf8Decode("(SP) ".$this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id]." %" : "0%"))),'',1,'C');
    $pdf->setXY($current_x + 38.53, $current_y);
    $pdf->setX($current_x + 38.53);
    $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->total * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (38.53 * 2), $current_y);
    $pdf->setX($current_x + (38.53 * 2));
    $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->order_tax * $trmrate)),'',1,'C');

}
if ($inv->order_tax_aiu_total > 0 && $inv->order_tax_aiu_base_apply_to != 1) {
    if ($inv->order_tax_aiu_base_apply_to == 2) {
        // code...
    } else if ($inv->order_tax_aiu_base_apply_to == 3) {
        // code...
    }
    $total+=$inv->order_tax_aiu_total;
    $pdf->Cell(38.53,3, $this->sma->utf8Decode((isset($taxes_details[$inv->order_tax_aiu_id]) ? $taxes_details[$inv->order_tax_aiu_id] : "0%")),'',1,'C');
    $pdf->setXY($current_x + 38.53, $current_y);
    $pdf->setX($current_x + 38.53);
    $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->order_tax_aiu_base * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (38.53 * 2), $current_y);
    $pdf->setX($current_x + (38.53 * 2));
    $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatMoney($inv->order_tax_aiu_total * $trmrate)),'',1,'C');
}

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

//derecha

$pdf->setXY($cX_items_finished+124, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

if ($inv->total_discount > 0) {
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Subtotal antes de Dcto'), 'BLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatMoney($total_bruto)), 'BR', 1, 'R');
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Descuento'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->total_discount > 0 ? "- " : "").$this->sma->formatMoney(($inv->total_discount) * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('Subtotal'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatMoney($inv->total * $trmrate)), 'TBR', 1, 'R');
$current_y+=5;
// if ($inv->aiu_amounts_affects_grand_total == 1) {
    if ($inv->aiu_admin_total > 0) {
        $pdf->setXY($current_x, $current_y);
        $pdf->cell(33.2, 5, $this->sma->utf8Decode('Administración ('.$this->sma->formatDecimals($inv->aiu_admin_percentage).')'), 'TBLR', 1, 'L');
        $pdf->setXY($current_x+33.2, $current_y);
        $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatMoney($inv->aiu_admin_total * $trmrate)), 'TBR', 1, 'R');
        $current_y+=5;
    }
    if ($inv->aiu_imprev_total > 0) {
        $pdf->setXY($current_x, $current_y);
        $pdf->cell(33.2, 5, $this->sma->utf8Decode('Imprevistos ('.$this->sma->formatDecimals($inv->aiu_imprev_percentage).')'), 'TBLR', 1, 'L');
        $pdf->setXY($current_x+33.2, $current_y);
        $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatMoney($inv->aiu_imprev_total * $trmrate)), 'TBR', 1, 'R');
        $current_y+=5;
    }
    if ($inv->aiu_utilidad_total > 0) {
        $pdf->setXY($current_x, $current_y);
        $pdf->cell(33.2, 5, $this->sma->utf8Decode('Utilidad ('.$this->sma->formatDecimals($inv->aiu_utilidad_percentage).')'), 'TBLR', 1, 'L');
        $pdf->setXY($current_x+33.2, $current_y);
        $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatMoney($inv->aiu_utilidad_total * $trmrate)), 'TBR', 1, 'R');
        $current_y+=5;
    }
// }

$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatMoney($inv->total_tax * $trmrate)), 'TBR', 1, 'R');
$current_y+=5;


$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('Retención en la fuente'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_fuente_total > 0 ? "- " : "").$this->sma->formatMoney($inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;

if ($inv->rete_iva_total > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Retención al IVA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_iva_total > 0 ? "- " : "").$this->sma->formatMoney($inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

if ($inv->rete_ica_total > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Retención al ICA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_ica_total > 0 ? "- " : "").$this->sma->formatMoney($inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

if ($inv->rete_other_total > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Otras retenciones'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_other_total > 0 ? "- " : "").$this->sma->formatMoney($inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
        $current_y+=5;
}

$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('TOTAL A PAGAR'), 'TBLR', 1, 'R', 1);
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatMoney(($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total) * $trmrate)), 'TBR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente+1.5);


$pdf->Ln(7);


//FIRMAS

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY();


$pdf->ln(1);
if ($inv->aiu_amounts_affects_grand_total == 0) {
    // $description1 .= " El AIU registrado para la venta no afecta el total";
}
if ($description1 != '') {
    $round_start_Y = $current_y;
    $current_x = $pdf->getX();
    $pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');
    $round_end_Y = $pdf->getY();
    $height_round = ($round_end_Y - $round_start_Y)+3;
    $pdf->RoundedRect($current_x, $round_start_Y, 196, $height_round, 3, '1234', '');
    $current_y = $pdf->getY()+3;
}

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

//derecha
$pdf->setXY($current_x+78.4, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Representante Legal'), '', 1, 'L');

if ($signature_root) {
    $pdf->Image($signature_root, $current_x+35,$current_y+1,35);
}

if ($for_email === true) {
    $pdf->Output(FCPATH."files/". $filename .".pdf", "F");
} else {
    if ($download) {
        $pdf->Output($filename.".pdf", "D");
    } else if ($internal_download == TRUE) {
        $pdf->Output("files/electronic_billing/". $filename .".pdf", "F");
    } else {
        $pdf->Output($filename.".pdf", "I");
    }
}