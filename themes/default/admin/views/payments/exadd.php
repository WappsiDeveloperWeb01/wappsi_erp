<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var expense_id = false;
    var biller_id = false;
    var supplier_id = false;
    var paying_commission = JSON.parse("<?= isset($paying_commission) && $paying_commission ? 'true' : 'false' ?>");
<?php if (isset($expense_id)): ?>
        expense_id = "<?= $expense_id ?>";
        biller_id = "<?= $expense->biller_id ?>";
        supplier_id = "<?= $expense->supplier_id ?>";
<?php endif ?>
var tax_rates = <?php echo json_encode($tax_rates); ?>;
var is_commision = false;

<?php if (isset($expense_commision)) { ?>
    is_commision = true;
    var commision = '<?= $expense_commision ?>';
    localStorage.setItem('expenses', commision);
    localStorage.setItem('supplier', '<?= $payment_company_id ?>');
<?php } ?>
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('add_multi_payment_expenses'); ?></h2>
    </div>
</div>
<?php 
$paying_commission = isset($paying_commission) && $paying_commission ? true : false ?>
 ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php $attrib = array('id' => 'add_multi_payment_expenses', 'target' => '_');
                    echo admin_form_open_multipart("payments/exadd", $attrib); ?>
                    <?php if (isset($payment_sales)): ?>
                        <?php foreach ($payment_sales as $key => $sale_id): ?>
                            <input type="hidden" name="payments_sale_id[]" value="<?= $sale_id ?>">
                        <?php endforeach ?>
                        <?php foreach ($to_complete_payments_ids as $key => $pid): ?>
                            <input type="hidden" name="to_complete_payments_ids[]" value="<?= $pid ?>">
                        <?php endforeach ?>
                            <input type="hidden" name="payment_start_date" value="<?= $payment_start_date ?>">
                            <input type="hidden" name="payment_end_date" value="<?= $payment_end_date ?>">
                    <?php endif ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?= lang('date', 'date') ?>
                                <input type="date" name="date" class="form-control" value="<?= date('Y-m-d') ?>">
                            </div>
                            <div class="col-md-3 form-group">
                                <?= lang("biller", "rcbiller"); ?>
                                <?php
                                $bl = [];
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller;
                                }
                                ?>
                                <select name="biller" class="form-control" id="rcbiller" required="required">
                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                        <?php foreach ($billers as $biller): ?>
                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endforeach ?>
                                    <?php } else { ?>
                                        <?php $biller = $bl[$this->session->userdata('biller_id')]; ?>
                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?= lang("reference_no", "document_type_id"); ?>
                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <?= lang('supplier', 'supplier') ?>
                                <?php
                                echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ""), 'id="supplier" data-placeholder="' . lang("select") . ' ' . lang("supplier") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                ?>
                            </div>

                        <?php if (isset($cost_centers)): ?>
                            <div class="form-group col-sm-3">
                                <?= lang('cost_center', 'cost_center_id') ?>
                                <?php
                                $ccopts[''] = lang('select');
                                if ($cost_centers) {
                                    foreach ($cost_centers as $cost_center) {
                                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                    }
                                }
                                 ?>
                                 <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                            </div>
                        <?php endif ?>

                          <hr class="col-sm-11">

                          <div class="form-group col-sm-3">
                              <?= lang('payment_date', 'payment_date') ?><br>
                              <input type="text" name="payment_date" id="payment_date" class="form-control datetime">
                          </div>

                          <div class="form-group col-sm-3">
                              <?= lang('consecutive_payment', 'consecutive_payment') ?><br>
                              <input type="text" name="consecutive_payment" id="consecutive_payment" class="form-control">
                          </div>

                          <div class="form-group col-sm-3">
                            <?= lang("attachment", "attachment") ?>
                            <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false" class="form-control file">
                          </div>

                          <hr class="col-sm-11">

                          <div class="col-md-12" id="sticker">
                                <div class="well well-sm">
                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="input-group wide-tip">
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                <i class="fa fa-2x fa-barcode addIcon"></i>
                                            </div>
                                                <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_expense_to_order") . '"'); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                          <table class="table col-sm-12" id="table_expenses">
                            <thead>
                              <tr>
                                <th style="width: 35% ;"><?= lang('expenses') ?></th>
                                <th style="width: 35% ;"><?= lang('amount_to_paid') ?></th>
                                <th style="width: 25% ;"><?= lang('retention') ?></th>
                                <th style="width: 5% ;"></th>
                              </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                              <tr>
                                <th style="width: 35% ;"><?= lang('expenses') ?></th>
                                <th style="width: 35% ;"><?= lang('amount_to_paid') ?></th>
                                <th style="width: 25% ;"><?= lang('retention') ?></th>
                                <th style="width: 5% ;"></th>
                              </tr>
                            </tfoot>
                          </table>
                          <!-- <div class="form-group col-sm-4">
                              <label><?= lang('base') ?></label>
                              <input type="number" name="base" id="base" class="form-control">
                          </div> -->
                          <!-- <div class="form-group col-sm-6">
                            <?= lang('total_paid', 'total_amount_paid') ?>
                            <input type="number" name="total_amount_paid" id="total_amount_paid" class="form-control" value="0" readonly>
                          </div> -->
                        <?php if (1==1): ?>
                        <div class="col-sm-12">
                            <label><?= lang('other_concepts') ?></label>
                            <table class="table" id="table_concepts" width="100%">
                                <?php
                                    $lopts[''] = lang('select');
                                    if (isset($ledgers)) {
                                        foreach ($ledgers as $ledger) {
                                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                                        }
                                    }

                                    $type = array(
                                                    '' => lang('select'),
                                                    '+' => lang('addition'),
                                                    '-' => lang('substraction')
                                                );
                                 ?>
                                <thead>
                                    <tr>
                                        <th style="width: 10%;"></th>
                                        <th style="width: 26.66%;">
                                            <?= lang('description') ?>
                                        </th>
                                        <th style="width: 26.66%;">
                                            <?= lang('amount') ?>
                                        </th>
                                        <?php if ($this->Settings->modulary): ?>
                                            <th style="width: 26.66%;">
                                                <?= lang('ledger') ?>
                                            </th>
                                        <?php endif ?>
                                        <th style="width: 10%;">
                                            <button class="btn btn-default btn-sm btn-outline add_concept" type="button"><span class="fa fa-plus"></span></button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <?php endif ?>
                        <div id="payments">
                            <div class="col-md-12">
                                <div class="well well-sm well_1">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <?= lang("paying_by", "paid_by_1"); ?>
                                                    <select name="paid_by" id="paid_by_1" class="form-control paid_by">
                                                        <?= $this->sma->paid_opts(); ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="payment">
                                                    <div class="form-group ngc">
                                                        <?= lang("total_amount_to_paid", "amount_1"); ?>
                                                        <input name="amount-paid" type="text" id="amount_1"
                                                            class="pa form-control kb-pad amount_1 text-right
                                                            " readonly />
                                                    </div>
                                                    <div class="form-group gc" style="display: none;">
                                                        <?= lang("gift_card_no", "gift_card_no"); ?>
                                                        <input name="gift_card_no" type="text" id="gift_card_no"
                                                            class="pa form-control kb-pad"/>

                                                        <div id="gc_details"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <?= lang('total_rete_amount', 'total_rete_amount_2') ?>
                                                <?php if ($paying_commission): ?>
                                                    <div class="input-group">
                                                <?php endif ?>
                                                    <input type="text" name="total_rete_amount_2" id="total_rete_amount_2" class="form-control text-right" readonly>
                                                <?php if ($paying_commission): ?>
                                                    <span class="input-group-addon" id="rt_paying_commission" style="cursor:pointer;"><i class="fa fa-pencil"></i></span>
                                                    </div>
                                                <?php endif ?>
                                            </div>
                                            <div class="col-sm-2">
                                                <?= lang('total_other_concepts', 'total_other_concepts') ?>
                                                <input type="text" name="total_other_concepts" id="total_other_concepts" class="form-control text-right" value="0" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                                <?= lang('total_received', 'total_received') ?>
                                                <input type="text" name="total_received" id="total_received" class="form-control text-right" readonly>
                                            </div>

                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="pcc_1" style="display:none;">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_no" type="text" id="pcc_no_1"
                                                            class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_holder" type="text" id="pcc_holder_1"
                                                            class="form-control"
                                                            placeholder="<?= lang('cc_holder') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select name="pcc_type" id="pcc_type_1"
                                                                class="form-control pcc_type"
                                                                placeholder="<?= lang('card_type') ?>">
                                                            <option value="Visa"><?= lang("Visa"); ?></option>
                                                            <option
                                                                value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                            <option value="Amex"><?= lang("Amex"); ?></option>
                                                            <option value="Discover"><?= lang("Discover"); ?></option>
                                                        </select>
                                                        <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input name="pcc_month" type="text" id="pcc_month_1"
                                                            class="form-control" placeholder="<?= lang('month') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <input name="pcc_year" type="text" id="pcc_year_1"
                                                            class="form-control" placeholder="<?= lang('year') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <input name="pcc_ccv" type="text" id="pcc_cvv2_1"
                                                            class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pcheque_1" style="display:none;">
                                            <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                <input name="cheque_no" type="text" id="cheque_no_1"
                                                    class="form-control cheque_no"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?= lang('payment_note', 'payment_note_1'); ?>
                                                <a href="<?= admin_url('payments/add_invoice_note') ?>" class="btn btn-primary" style="padding: 3px 8px; margin-left: 2px;" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span></a>
                                            <textarea name="payment_note" id="payment_note_1"
                                                    class="pa form-control kb-text payment_note"></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?= form_hidden('add_payment', '1'); ?>
                            <span class="text-danger negative_alert" style="display: none;"><?= lang('total_received_negative') ?></span><br>
                            <button class="btn btn-primary" type="button" id="add_payment"><?= lang('submit') ?></button>
                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <!-- retencion -->
        <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                  class="fa fa-2x">&times;</i></button>
                  <h4 class="modal-title" id="rtModalLabel"><?=lang('edit_order_tax');?></h4>
                </div>
                <div class="modal-body">
                  <div class="row">

                    <div class="col-sm-12">
                        <table class="table">
                            <tr>
                                <th>
                                    <?= lang('subtotal') ?>
                                </th>
                                <td class="text-right txt-subtotal">
                                    <input type="text" id="rete_base_subtotal" class="form-control number_mask" step="0.01" readonly>
                                </td>
                                <th>
                                    <?= lang('tax') ?>
                                </th>
                                <td class="text-right txt-tax">
                                    <input type="text" id="rete_base_tax" class="form-control number_mask" step="0.01" readonly>
                                </td>
                                <th>
                                    <?= lang('total') ?>
                                </th>
                                <td class="text-right txt-total">
                                    <input type="text" id="rete_base_total" class="form-control number_mask" step="0.01" readonly>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-sm-2">
                      <label>Retención</label>
                    </div>
                    <div class="col-sm-5">
                      <label>Opción</label>
                    </div>
                    <div class="col-sm-2">
                      <label>Porcentaje</label>
                    </div>
                    <div class="col-sm-3">
                      <label>Valor</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                      <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                      <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                      <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                      <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 text-right">
                      <label>Total : </label>
                    </div>
                    <div class="col-md-4 text-right">
                      <label id="total_rete_amount"> 0.00 </label>
                    </div>
                    <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                  </div>
                  <div class="col-sm-12">
                        <table class="table">
                            <tr>
                                <th>
                                    <?= lang('suggested_subtotal') ?>
                                </th>
                                <td class="text-right">
                                    <span id='suggested_subtotal'></span>
                                </td>
                                <th>
                                    <?= lang('suggested_tax') ?>
                                </th>
                                <td class="text-right">
                                    <span id='suggested_tax'></span>
                                </td>
                                <th>
                                    <?= lang('suggested_total') ?>
                                </th>
                                <td class="text-right">
                                    <span id='suggested_total'></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-sm-12">
                  <span class="text-danger advise_invalid_retention" style="display: none;">El total de la retención es mayor al monto a pagar</span>
                </div>
                <div class="modal-footer">
                  <button type="button" id="cancelOrderRete" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
                </div>
              </div>
            </div>
          </div>
        <!-- retencion -->

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="edit_purchase_expense">
                        <div class="form-group col-sm-6">
                            <label><?= lang('amount_to_paid') ?></label>
                            <?= form_input('pamount_to_paid', '', 'id="pamount_to_paid" class="form-control"'); ?>
                        </div>
                        <?php if ($Settings->tax1) { ?>
                            <div class="form-group">
                                <label class="col-sm-12"><?= lang('tax_1') ?></label>
                                <div class="col-sm-6">
                                    <?php
                                    $tr[""] = lang('n/a');
                                    foreach ($tax_rates as $tax) {
                                        $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                    ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= form_input('ptax_value', '', 'id="ptax_value" class="form-control"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12"><?= lang('tax_2') ?></label>
                                <div class="col-sm-6">
                                    <?php
                                    $tr[""] = lang('n/a');
                                    foreach ($tax_rates as $tax) {
                                        $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('ptax_2', $tr, "", 'id="ptax_2" class="form-control pos-input-tip" style="width:100%;"');
                                    ?>
                                </div>

                                <div class="col-sm-6">
                                    <?= form_input('ptax_2_value', '', 'id="ptax_2_value" class="form-control"'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <table class="table table-bordered">
                            <tr>
                                <th style="width:16.66%;"><?= lang('expense_cost'); ?></th>
                                <th style="width:16.66%;"><span id="net_cost_label"></span></th>
                                <th style="width:16.66%;"><?= lang('expense_tax'); ?></th>
                                <th style="width:16.66%;"><span id="pro_tax"></span></th>
                                <th style="width:16.66%;"><?= lang('total'); ?></th>
                                <th style="width:16.66%;"><span id="pro_total"></span></th>
                            </tr>
                        </table>
                    </div>
                    <input type="hidden" id="net_cost" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- prmodal -->
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change', '#rcbiller', function(){
          $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/24/") ?>'+$('#rcbiller').val(),
            type:'get',
            dataType:'JSON'
          }).done(function(data){
            response = data;

            $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }

            if (response.status == 0) {
              $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
              $('#document_type_id').trigger('change');
          });
        });
        // $('#rcbiller').select2('readonly', true);
        $('#rcbiller').trigger('change');

        $("#add_item").autocomplete({
            // source: '<?= admin_url('purchases/suggestions'); ?>',
            source: function (request, response) {
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('purchases/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        supplier_id: $("#supplier").val(),
                        purchase_type: 2
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    ui.item.is_new = true;
                    var row = add_expense_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        loadItems();
    });
</script>