<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AttributeValue extends CI_Model
{
    private $newDb;
    private $tableName = 'attribute_values';
    private $tableNameTranslation = 'attribute_value_translations';

    public function __construct()
    {
        parent::__construct();
        $this->load->integration_model('Connection');
        $this->load->integration_model('Translation');
    }

    public function open($connData)
    {
        $Connection = new Connection();

        $Connection->hostname = $connData->host;
        $Connection->username = $connData->username;
        $Connection->password = $connData->password;
        $Connection->dbname = $connData->databases;

        $this->newDb = $Connection->open();
    }

    public function find($data)
    {
        $this->newDb->where($data);
        $q = $this->newDb->get($this->tableName);
        return $q->row();
    }

    public function get($data)
    {
        $this->newDb->where($data);
        $q = $this->newDb->get($this->tableName);
        return $q->result();
    }

    public function getAll()
    {
        $q = $this->newDb->get($this->tableName);
        return $q->result();
    }

    public function create($data)
    {
        if ($this->newDb->insert($this->tableName, $data)) {
            return $this->createTranslation($data, $this->newDb->insert_id());
        }
        return false;
    }

    private function createTranslation($data, $attributeValueId)
    {
        $Translation = new Translation();
        $Translation->conn = $this->newDb;
        $Translation->tableName = $this->tableNameTranslation;
        $translationData = [
            "attribute_value_id" => $attributeValueId,
            "name" => $data["name"],
            "lang" => strtoupper($this->shop_settings->eshop_lang),
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
            "id_wappsi" => $data["id_wappsi"],
        ];
        if ($Translation->create($translationData)) {
            return true;
        }
        return false;
    }

    public function update($data, $id)
    {
        $this->newDb->where('id', $id);
        if ($this->newDb->update($this->tableName, $data)) {
            return $this->updateTranslation($data, $id);
        }
        return false;
    }

    private function updateTranslation($data, $attributeValueId)
    {
        $Translation = new Translation();
        $Translation->conn = $this->newDb;
        $Translation->tableName = $this->tableNameTranslation;
        $translation = $Translation->find(["attribute_value_id" => $attributeValueId]);
        $translationData = [
            "name" => $data["name"],
            "lang" => strtoupper($this->shop_settings->eshop_lang),
            "updated_at" => date("Y-m-d H:i:s"),
        ];
        if ($Translation->update($translationData, $translation->id)) {
            return true;
        }
        return false;
    }

    public function delete($id)
    {
        $this->newDb->where("id", $id);
        if ($this->newDb->delete($this->tableName)) {
            return $this->deleteTranslation($id);
        }
        return false;
    }

    public function delete_id_wappsi($id)
    {
        $this->newDb->where(['id_wappsi' => $id]);
        $q = $this->newDb->get($this->tableName);
        if ($q->num_rows() > 0) {
            $attributeValue = $q->row();
            $this->newDb->where("id", $attributeValue->id);
            if ($this->newDb->delete($this->tableName)) {
                return $this->deleteTranslation($attributeValue->id);
            }
        }
            
        return false;
    }

    private function deleteTranslation($categoryId)
    {
        $Translation = new Translation();
        $Translation->conn = $this->newDb;
        $Translation->tableName = $this->tableNameTranslation;
        $translation = $Translation->find(["attribute_value_id" => $categoryId]);
        if ($Translation->delete($translation->id)) {
            return true;
        }
        return false;
    }

    public function check_attribute_value_assigned($id){

        $q = $this->newDb
                ->join('product_attribute_values', 'product_attribute_values.attribute_value_id = attribute_values.id', 'inner')
                ->where('attribute_values.id_wappsi', $id)
                ->get('attribute_values');
        if ($q->num_rows() > 0) {
            return false;
        }
        return true;
    }

    public function close()
    {
        $this->newDb->close();
    }
}