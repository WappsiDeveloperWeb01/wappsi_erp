<div class="table-responsive">
		<table id="TableData" class="table table-stripped" >
			<thead>
				<tr>
					<td class="text-center"> <b>REFERENCIA COMPRA</b></td>
					<td class="text-center"> <b>FECHA COMPRA</b></td>
					<td class="text-center"> <b>VALOR COMPRA</b></td>
					<td class="text-center"> <b>SALDO PAGADO</b></td>
					<td class="text-center"> <b>SALDO PENDIENTE</b></td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$supplier =  '' ; 
					$total = $val_pago = $Saldo = 0;
					$total_total = $total_val_pago = $total_Saldo = 0;					
					$i = 0;
				?>
				<?php foreach ($purchases as $purchase): ?>
					<?php $i++; ?>		
					<?php if ($purchase['supplier'] != $supplier): ?>
						<?php if ($i > 1): ?>
							<tr > 
								<td ></td>
								<td class="text-right" >SubTotal Proveedor </td>
								<td class="text-right">$ <?php echo number_format($total_total,0,'','.');?></td>
								<td class="text-right">$ <?php echo number_format($total_val_pago,0,'','.');?></td>
								<td class="text-right">$ <?php echo number_format($total_Saldo,0,'','.');?></td>
							</tr>
							<?php 
								$total_total = $total_val_pago = $total_Saldo = 0;					
							?>
						<?php endif; ?>
						<tr class="active">
							<td ><h5><b>Proveedor: <?php echo $purchase['supplier']; ?></b></h5></td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ></td>
						</tr>
						<?php $supplier = $purchase['supplier'];?>
					<?php endif; ?>
					<tr >
						<td scope="row"><?php echo $purchase['Referencia']; ?></td>
						<td class="text-center"><?php echo substr($purchase['Fecha'],0,10); ?></td>
						<td class="text-right">$ <?php echo number_format($purchase['total'],0,'','.'); ?></td>
						<td class="text-right">$ <?php echo number_format($purchase['val_pago'],0,'','.'); ?></td>
						<td class="text-right">$ <?php echo number_format($purchase['Saldo'],0,'','.'); ?></td>
					</tr>	
					<?php 
						$total_total += $purchase['total'];
						$total_val_pago += $purchase['val_pago'];
						$total_Saldo += $purchase['Saldo'];
					?>
					<?php endforeach; ?>
					<tr > 
						<td ></td>
						<td class="text-right" >SubTotal Proveedor </td>
						<td class="text-right">$ <?php echo number_format($total_total,0,'','.');?></td>
						<td class="text-right">$ <?php echo number_format($total_val_pago,0,'','.');?></td>
						<td class="text-right">$ <?php echo number_format($total_Saldo,0,'','.');?></td>
					</tr>
			</tbody>
		</table>
	</div>