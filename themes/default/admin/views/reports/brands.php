<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";

if ($this->input->post('category')) {
    $v .= "&category=" . $this->input->post('category');
}
if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
?>
<script>
    $(document).ready(function () {
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        oTable = $('#PrRData').dataTable({
            "aaSorting": [[0, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('reports/getBrandsReport/?v=1'.$v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [null, {"mRender": decimalFormat, "bSearchable": false}, {"mRender": decimalFormat, "bSearchable": false}, {"mRender": currencyFormat, "bSearchable": false}, {"mRender": currencyFormat, "bSearchable": false}, {"mRender": currencyFormat, "bSearchable": false}],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var pQty = 0, sQty = 0, pAmt = 0, sAmt = 0, pl = 0;
                for (var i = 0; i < aaData.length; i++) {
                    pQty += parseFloat(aaData[aiDisplay[i]][1]);
                    sQty += parseFloat(aaData[aiDisplay[i]][2]);
                    pAmt += parseFloat(aaData[aiDisplay[i]][3]);
                    sAmt += parseFloat(aaData[aiDisplay[i]][4]);
                    pl += parseFloat(aaData[aiDisplay[i]][5]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[1].innerHTML = decimalFormat(parseFloat(pQty));
                nCells[2].innerHTML = decimalFormat(parseFloat(sQty));
                nCells[3].innerHTML = currencyFormat(parseFloat(pAmt));
                nCells[4].innerHTML = currencyFormat(parseFloat(sAmt));
                nCells[5].innerHTML = currencyFormat(parseFloat(pl));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('brand');?>]", filter_type: "text", data: []},
        ], "footer");
        <?php endif ?>
        
        
        <?php if ($this->session->userdata('warehouse_id')): ?>
            setTimeout(function() {
                $('#warehouse').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
    });

    $(document).ready(function () {
        $('#form').hide();
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });

    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo admin_form_open("reports/brands", 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("brand", "brand") ?>
                                            <?php
                                            $bt[''] = lang('select').' '.lang('brand');
                                            foreach ($brands as $brand) {
                                                $bt[$brand->id] = $brand->name;
                                            }
                                            echo form_dropdown('brand', $bt, (isset($_POST['brand']) ? $_POST['brand'] : ''), 'class="form-control select" id="brand" placeholder="' . lang("select") . " " . lang("brand") . '" style="width:100%"')
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                            <?php
                                            $wh[""] = lang('select').' '.lang('warehouse');
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                        <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="date_controls_dh">
                                        <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                            <div class="col-sm-4 form-group">
                                                <?= lang('filter_year', 'filter_year_dh') ?>
                                                <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("start_date", "start_date_dh"); ?>
                                                <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("end_date", "end_date_dh"); ?>
                                                <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div
                                        class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_filter"'); ?> </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-right dropdown">
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                        <li class="dropdown">
                                            <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                                <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?>
                                            </a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                                <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="PrRData"
                                        class="table  table-bordered table-condensed table-hover dfTable reports-table"
                                        style="margin-bottom:5px;">
                                        <thead>
                                        <tr class="active">
                                            <th><?= lang("brand"); ?></th>
                                            <th><?= lang("purchased"); ?></th>
                                            <th><?= lang("sold"); ?></th>
                                            <th><?= lang("purchased_amount"); ?></th>
                                            <th><?= lang("sold_amount"); ?></th>
                                            <th><?= lang("profit_loss"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th>
                                            <th><?= lang("purchased"); ?></th>
                                            <th><?= lang("sold"); ?></th>
                                            <th><?= lang("purchased_amount"); ?></th>
                                            <th><?= lang("sold_amount"); ?></th>
                                            <th><?= lang("profit_loss"); ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>

</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getBrandsReport/pdf/?v=1'.$v)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getBrandsReport/0/xls/?v=1'.$v)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });



        $('#submit_filter').on('click', function(e){
            // e.preventDefault();
            $('#filter_action').val(1);
            $('#sales_filter').submit();
        });
    });



    $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });
</script>