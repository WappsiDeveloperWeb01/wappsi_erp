<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= lang('adjustment_pos') ?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
        </style>
    </head>
    <body id="closeRegisterPrint">
        <div id="wrapper">

            <div id="receiptData" style="text-align:center;">
                <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                <h2 class="text-center"><?= $document_type->nombre ?></h2>
            </div>
            <div>
                <div class="col-xs-12">
                    <h4>
                        <strong><?= lang("date"); ?> :</strong> &nbsp;&nbsp;<?= $this->sma->hrld($transfer->date); ?>
                        <br>
                        <strong> <?= lang("ref_no"); ?>:</strong> <?= $transfer->reference_no; ?>
                    </h4>
                </div>
            </div>    
            <div class="clearfix"></div>
                <div class="col-xs-6">
                    <strong><p class="bold"><?= lang("origin"); ?>:</p></strong> 
                    <p style="margin-top:10px;"><?= $from_warehouse->name . " ( " . $from_warehouse->code . " )"; ?></p>
                    <!-- <?= "<p>" . $from_warehouse->phone . "</p><p>" . $from_warehouse->email . " " . $from_warehouse->address . "</p>";
                    ?> -->
                </div>
                <div class="col-xs-6">
                    <strong><p class="bold"><?= lang("destination"); ?>:</p></strong>  
                    <p style="margin-top:10px;"><?= $to_warehouse->name . " ( " . $to_warehouse->code . " )"; ?></p>
                    <!-- <?= "<p>" . $to_warehouse->phone . "</p><p>" . $to_warehouse->email . "" . $to_warehouse->address. "</p>";
                    ?> -->
                </div>
                <div class="col-lg-12">
                    <table class="table table-hover">    
                        <thead>
                            <tr>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("description"); ?></th>
                            <?php if ($Settings->product_serial): ?>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("serial_no"); ?></th>
                            <?php endif ?>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("quantity"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $r = 1;
                            $total_qty = 0;
                            foreach ($rows as $row): ?>
                                <tr>
                                    <td style="text-align:left; width: 60%">
                                        <?= $row->product_name .' - '. ($row->name_brand ? $row->name_brand . ' - ' : '' )  .($row->variant ? ' (' . $row->variant . ')' : '') .' - '. $row->codeVariant ; ?>
                                    </td>
                                    <?php if ($Settings->product_serial): ?>
                                        <td style="text-align:center; width:20%; "><?= $row->serial_no; ?></td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:40%; "><?= $this->sma->formatQuantity($row->quantity); ?></td>
                                </tr>
                                <?php $total_qty += $row->quantity; ?>
                            <?php $r++;
                            endforeach; ?>
                        </tbody>
                    <tfoot>
                        <?php $col = $Settings->indian_gst ? 4 : 3;
                        if ($Settings->tax1) {
                            $col += 1;
                        } 

                        ?>
                        <tr>
                            <th style="text-align:center;" colspan="<?= $Settings->product_serial ? 2 : 1 ?>" class="text-right"><?= lang('total') ?></th>
                            <th class="text-right"><?= $this->sma->formatQuantity($total_qty); ?></th>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <div class="col-xs-12">
                    <p style=" font-weight: bold;" ><?= lang("signature_received"); ?>: _________________________________________ </p>
                    <br>
                </div>

                <div class="col-xs-12">
                    <p style=" font-weight: bold;" > <?= lang("note") ?> : <?= $transfer->note ?> </p>
                    <br>
                </div>
                
                <div class="col-xs-12 pull-left">
                    <p><?= lang("created_by"); ?>: <?= $created_by->first_name.' '.$created_by->last_name; ?> </p>
                </div>
                <div class="modal-footer no-print">
                    <div>
                    <button  type="button" class="btn btn-block btn-primary " onclick="imprimirTirillaTraslado();">
                        <i class="fa fa-print"></i> <?= lang('print'); ?>
                    </button>
                    </div>
                    <button style="with:100%" type="button" class="btn btn-block btn btn-warning" onclick="window.location.href = '<?= admin_url().'transfers/index' ?>';">
                        <i class="fa fa-arrow-circle-left"></i> <?= lang('go_back'); ?>
                    </button>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url(), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
</script>
<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript">
        
    $(document).ready(function(){
        $(document).on('click', '.po', function (e) {
            e.preventDefault();
            $('.po').popover({
                html: true,
                placement: 'left',
                trigger: 'manual'
            }).popover('show').not(this).popover('hide');
            return false;
        });
        $(document).on('click', '.po-close', function () {
            $('.po').popover('hide');
            return false;
        });
        $(document).on('click', '.po-delete', function (e) {
            var row = $(this).closest('tr');
            e.preventDefault();
            $('.po').popover('hide');
            var link = $(this).attr('href');
            $.ajax({
                type: "get", url: link,
                success: function (data) {
                    row.remove();
                    addAlert(data, 'success');
                },
                error: function (data) {
                    addAlert('Failed', 'danger');
                }
            });
            return false;
        });
        setTimeout(function() {
            imprimirTirillaTraslado();
        }, 1200);
    });

    function imprimirTirillaTraslado()
    {
      $('.without_close').css('display', '');
      var divToPrint=document.getElementById('closeRegisterPrint');
      var newWin=window.open('','Print-Window');
      var header = $('head').html();
      newWin.document.open();
      newWin.document.write('<head>'+header+'</head><body onload="window.print()" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');
      newWin.document.close();
      setTimeout(function(){
            newWin.close();
        },500);
    }


</script>