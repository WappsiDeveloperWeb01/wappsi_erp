<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Transfers_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $warehouse_id, $tobiller_origin, $tobiller_destination)
    { 
        if ($this->Settings->variant_code_search == 0) {
            return $this->get_products_names($term, $warehouse_id, $tobiller_origin, $tobiller_destination);
        } else {
            if ($data = $this->get_products_names_variants($term, $warehouse_id, $tobiller_origin, $tobiller_destination)) {
                return $data;
            } else {
                return $this->get_products_names($term, $warehouse_id, $tobiller_origin, $tobiller_destination);
            }
        }
    }
    
    public function get_products_names($term, $warehouse_id, $tobiller_origin, $tobiller_destination){
        $limit = $this->Settings->max_num_results_display;
        $this->site->create_temporary_product_billers_assoc($tobiller_origin);
        $this->db->select('products.id,
                            code,
                            name,
                            ('.($warehouse_id ? $this->db->dbprefix('warehouses_products').".quantity" : "{$this->db->dbprefix('products')}.quantity").($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') AS quantity,
                            '.($this->Settings->include_pending_order_quantity_stock == 1 ? "COALESCE(OP.pending_quantity, 0) AS op_pending_quantity, " : "").'
                            cost,
                            tax_rate,
                            type,
                            unit,
                            purchase_unit,
                            tax_method,
                            attributes,
                            consumption_purchase_tax
                            ')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id AND warehouses_products.warehouse_id = '.$warehouse_id, 'left')
            ->join('products_billers_assoc_'.$tobiller_origin.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
            ->group_by('products.id');
        if ($this->Settings->include_pending_order_quantity_stock == 1) {
            $pending_order_sql = "(SELECT 
                                    OSI.product_id,
                                    SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
                                FROM sma_order_sale_items OSI
                                    INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
                                WHERE OS.sale_status != 'cancelled'
                                ".($warehouse_id ? ' AND OSI.warehouse_id = ' . $warehouse_id : '')."
                                GROUP BY OSI.product_id) OP";
            $this->db->join($pending_order_sql, 'OP.product_id=products.id', 'left');
        }
        if ($this->Settings->overselling) {
            $this->db->where("type != 'combo' AND (name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR  concat(name, ' (', code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")");
        } else {
            $this->db->where("type != 'combo' AND warehouses_products.warehouse_id = '" . $warehouse_id . "' AND warehouses_products.quantity > 0 AND "
                . "(name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR  concat(name, ' (', code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")");
        }
        if ($this->Settings->display_all_products == 0) {
            $this->db->where("warehouses_products.quantity > ", '0');
        }
        // $this->db->where('(products_billers.product_id IS NOT NULL)');
        $this->db->limit($limit);
        $this->db->where('products.discontinued', '0');
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
    }
    
    public function get_products_names_variants($term, $warehouse_id, $tobiller_origin, $tobiller_destination){
        $limit = $this->Settings->max_num_results_display;
        $this->site->create_temporary_product_billers_assoc($tobiller_origin);
        $this->db->select('
                                products.id, 
                                products.code, 
                                products.name, 
                                warehouses_products.quantity, 
                                products.cost, 
                                products.tax_rate, 
                                products.type, 
                                unit, 
                                purchase_unit, 
                                tax_method, 
                                attributes,
                                product_variants.id as variant_selected')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            // ->join('products_billers', 'products_billers.product_id = products.id AND products_billers.biller_id = '.$tobiller_destination, 'left')
            ->join('products_billers_assoc_'.$tobiller_origin.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
            ->join('product_variants', 'product_variants.product_id=products.id', 'left')
            ->group_by('products.id');
        if ($this->Settings->overselling == 1) {
            $this->db->where("
                type != 'combo' AND 
                (
                    {$this->db->dbprefix('product_variants')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                )");
        } else {
            $this->db->where("
                type != 'combo' AND 
                warehouses_products.warehouse_id = '" . $warehouse_id . "' AND 
                warehouses_products.quantity > 0 AND 
                (
                    {$this->db->dbprefix('product_variants')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                )");
        }
        if ($this->Settings->display_all_products == 0) {
            $this->db->where("warehouses_products.quantity > ", '0');
        }
        // $this->db->where('(products_billers.product_id IS NOT NULL)');
        $this->db->limit($limit);
        $this->db->where('products.discontinued', '0');
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
    }

    public function getWHProduct($id)
    {
        $this->db->select('products.id, code, name, warehouses_products.quantity, cost, tax_rate')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('products.id');
        $q = $this->db->get_where('products', array('warehouses_products.product_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function addTransfer($data = [], $items = [], $order_id = NULL)
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        if ($this->Settings->overselling == 0) {
            foreach ($items as $item) {
                if (!empty($item['option_id']) && is_numeric($item['option_id'])) {
                    $wpv = $this->db->get_where('warehouses_products_variants', ['product_id'=>$item['product_id'] ,'option_id'=>$item['option_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
                    // $this->sma->print_arrays($data, $item);
                    if ($wpv->num_rows() > 0) {
                        $wpv = $wpv->row();
                        if ($wpv->quantity < $item['quantity']) {
                            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else{
                        $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $wp = $this->db->get_where('warehouses_products', ['product_id'=>$item['product_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
                    if ($wp->num_rows() > 0) {
                        $wp = $wp->row();
                        if ($wp->quantity < $item['quantity']) {
                            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else{
                        $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            }
        }
        $status = $data['status'];

        if ($this->db->insert('transfers', $data)) {
            $transfer_id = $this->db->insert_id();
            $check_reference_data = [
                'table' => 'transfers', 'record_id' => $transfer_id, 'reference' => $data['reference_no'], 'biller_id' => $data['biller_id'], 'document_type_id' => $data['document_type_id']
                ];
            if ($new_reference = $this->site->check_reference($check_reference_data)) {
                $data['reference_no'] = $new_reference;
            }
            // $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($items as $item) {
                $item['transfer_id'] = $transfer_id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                $item['date'] = date('Y-m-d');
                $item['warehouse_id'] = $data['to_warehouse_id'];
                $item['quantity_balance'] = $item['quantity'];
                $item['status'] = !($data['status'] == 'sent' || $data['status'] == 'completed') ? 'pending' : 'received' ;
                $this->db->insert('purchase_items', $item);
                $this->db->insert('transfer_items', $item);
                if ($data['status'] == 'sent' || $data['status'] == 'completed') {
                    $this->sync_transfer_item($item, $data);
                    if ($this->Settings->update_cost) {
                        $this->db->update('products', array('cost' => $item['unit_cost']), array('id' => $item['product_id']));
                    }
                }
            }


            if ($this->Settings->data_synchronization_to_store == ACTIVE) {
                $items_store = [];
                foreach ($items as $item) {
                    $items_store[] = [
                            'product_id' => $item['product_id'],
                            'option_id' => $item['option_id'],
                            'warehouse_id' => $data['to_warehouse_id'],
                            'quantity' => $item['quantity'],
                        ];
                    $items_store[] = [
                            'product_id' => $item['product_id'],
                            'option_id' => $item['option_id'],
                            'warehouse_id' => $data['from_warehouse_id'],
                            'quantity' => ($item['quantity']) * -1,
                        ];
                }
                $this->site->syncStoreProductsQuantityMovement($items_store, 1);
            }

            if ($data['status'] == 'sent' || $data['status'] == 'completed') {
                if (isset($data['cost_center_id']) && isset($data['destination_cost_center_id']) && $data['cost_center_id'] != $data['destination_cost_center_id']) {
                    $this->site->wappsiContabilidadTraslados($data, $items);
                }
            }
            if ($order_id) {
                $this->db->update('order_transfers', ['status'=>'completed'], ['id'=>$order_id]);
            }
            return $transfer_id;  // modificacion para retornar el id de la transferencia, se requiere para la visualizacion del formato
        }
        return false;
    }

    public function sync_transfer_item($item, $data){
        if (!empty($item['option_id']) && is_numeric($item['option_id'])) {
            $wpv = $this->db->get_where('warehouses_products_variants', ['product_id'=>$item['product_id'] ,'option_id'=>$item['option_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
            if ($wpv->num_rows() > 0) {
                $wpv = $wpv->row();
                $this->db->update('warehouses_products_variants', ['quantity' => ($wpv->quantity - $item['quantity'])], ['product_id'=>$item['product_id'] ,'option_id'=>$item['option_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
            }

            $wpv = $this->db->get_where('warehouses_products_variants', ['product_id'=>$item['product_id'] ,'option_id'=>$item['option_id'], 'warehouse_id'=>$data['to_warehouse_id']]);
            if ($wpv->num_rows() > 0) {
                $wpv = $wpv->row();
                $this->db->update('warehouses_products_variants', ['quantity' => ($wpv->quantity + $item['quantity'])], ['product_id'=>$item['product_id'] ,'option_id'=>$item['option_id'], 'warehouse_id'=>$data['to_warehouse_id']]);
            } else {
                $this->db->insert('warehouses_products_variants', ['quantity' => $item['quantity'], 'product_id'=>$item['product_id'] ,'option_id'=>$item['option_id'], 'warehouse_id'=>$data['to_warehouse_id']]);
            }
        }

        $wp = $this->db->get_where('warehouses_products', ['product_id'=>$item['product_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
        if ($wp->num_rows() > 0) {
            $wp = $wp->row();
            $this->db->update('warehouses_products', ['quantity' => ($wp->quantity - $item['quantity'])], ['product_id'=>$item['product_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
        }

        $wp = $this->db->get_where('warehouses_products', ['product_id'=>$item['product_id'], 'warehouse_id'=>$data['to_warehouse_id']]);
        if ($wp->num_rows() > 0) {
            $wp = $wp->row();
            $this->db->update('warehouses_products', ['quantity' => ($wp->quantity + $item['quantity'])], ['product_id'=>$item['product_id'], 'warehouse_id'=>$data['to_warehouse_id']]);
        } else {
            $this->db->insert('warehouses_products', ['quantity' => $item['quantity'], 'product_id'=>$item['product_id'], 'warehouse_id'=>$data['to_warehouse_id']]);
        }

        $clause = array('product_id' => $item['product_id'], 'option_id' => $item['option_id'], 'warehouse_id' => $data['from_warehouse_id'], 'status' => 'received');
        $qty = 0 - $item['quantity'];
        $this->site->setPurchaseItem($clause, $qty);
    }

    public function updateTransfer($id, $data = array(), $items = array())
    {
        if ($this->Settings->overselling == 0) {
            foreach ($items as $item) {
                if (!empty($item['option_id']) && is_numeric($item['option_id'])) {
                    $wpv = $this->db->get_where('warehouses_products_variants', ['product_id'=>$item['product_id'] ,'option_id'=>$item['option_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
                    // $this->sma->print_arrays($data, $item);
                    if ($wpv->num_rows() > 0) {
                        $wpv = $wpv->row();
                        if ($wpv->quantity < $item['quantity']) {
                            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else{
                        $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $wp = $this->db->get_where('warehouses_products', ['product_id'=>$item['product_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
                    if ($wp->num_rows() > 0) {
                        $wp = $wp->row();
                        if ($wp->quantity < $item['quantity']) {
                            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else{
                        $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            }
        }
        $status = $data['status'];
        $odata = (array) $this->getTransferByID($id);
        if (($odata['status'] == 'sent' || $odata['status'] == 'completed') && ($data['status'] == 'sent' || $data['status'] == 'completed')) {
            $oitems = (array) $this->getAllTransferItems($id, false);
            foreach ($oitems as $oitem) {
                $oitem = (array) $oitem;
                $old_data = $odata;
                $old_data['from_warehouse_id'] = $odata['to_warehouse_id'];
                $old_data['to_warehouse_id'] = $odata['from_warehouse_id'];
                $this->sync_transfer_item($oitem, $old_data);
            }
        }
        if ($this->db->update('transfers', $data, array('id' => $id))) {
            $this->db->delete('purchase_items', array('transfer_id' => $id));
            $this->db->delete('transfer_items', array('transfer_id' => $id));
            foreach ($items as $item) {
                $item['transfer_id'] = $id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                $item['date'] = date('Y-m-d');
                $item['warehouse_id'] = $data['to_warehouse_id'];
                $item['quantity_balance'] = $item['quantity'];
                $item['status'] = !($data['status'] == 'sent' || $data['status'] == 'completed') ? 'pending' : 'received' ;
                $this->db->insert('purchase_items', $item);
                $this->db->insert('transfer_items', $item);
                if ($data['status'] == 'sent' || $data['status'] == 'completed') {
                    $this->sync_transfer_item($item, $data);
                    if ($this->Settings->update_cost) {
                        $this->db->update('products', array('cost' => $item['unit_cost']), array('id' => $item['product_id']));
                    }
                }
                    
            }
            if ($data['status'] == 'sent' || $data['status'] == 'completed') {
                if (isset($data['cost_center_id']) && isset($data['destination_cost_center_id']) && $data['cost_center_id'] != $data['destination_cost_center_id']) {
                    $this->site->wappsiContabilidadTraslados($data, $items);
                }
            }
            return true;
        }
        return false;
    }

    public function updateStatus($id, $status, $note)
    {
        $transfer = $this->getTransferByID($id);
        $items = $this->getAllTransferItems($id, $transfer->status);

        if ($this->Settings->overselling == 0) {
            foreach ($items as $item) {
                if (!empty($item['option_id']) && is_numeric($item['option_id'])) {
                    $wpv = $this->db->get_where('warehouses_products_variants', ['product_id'=>$item['product_id'] ,'option_id'=>$item['option_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
                    // $this->sma->print_arrays($data, $item);
                    if ($wpv->num_rows() > 0) {
                        $wpv = $wpv->row();
                        if ($wpv->quantity < $item['quantity']) {
                            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else{
                        $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $wp = $this->db->get_where('warehouses_products', ['product_id'=>$item['product_id'], 'warehouse_id'=>$data['from_warehouse_id']]);
                    if ($wp->num_rows() > 0) {
                        $wp = $wp->row();
                        if ($wp->quantity < $item['quantity']) {
                            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else{
                        $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), $item['product_name']));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            }
        }
        if ($this->db->update('transfers', array('status' => $status, 'note' => $note), array('id' => $id))) {
            $this->db->delete('purchase_items', array('transfer_id' => $id));
            $this->db->delete('transfer_items', array('transfer_id' => $id));
            foreach ($items as $item) {
                $item = (array) $item;
                $item['transfer_id'] = $id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                unset($item['id'], $item['variant'], $item['unit'], $item['hsn_code'], $item['second_name']);
                $item['date'] = date('Y-m-d');
                $item['warehouse_id'] = $transfer->to_warehouse_id;
                $item['status'] = !($transfer->status == 'sent' || $transfer->status == 'completed') ? 'pending' : 'received' ;
                $this->db->insert('purchase_items', $item);
                $this->db->insert('transfer_items', $item);
                if ($status == 'sent' || $status == 'completed') {
                    $data = (array) $transfer;
                    $this->sync_transfer_item($item, $data);
                    if ($this->Settings->update_cost) {
                        $this->db->update('products', array('cost' => $item['unit_cost']), array('id' => $item['product_id']));
                    }
                }
            }
            if ($transfer->status == 'sent' || $transfer->status == 'completed') {
                if (isset($transfer->cost_center_id) && isset($transfer->destination_cost_center_id) && $transfer->cost_center_id != $transfer->destination_cost_center_id) {
                    $data = (array) $transfer;
                    $this->site->wappsiContabilidadTraslados($data, $items);
                }
            }
            return true;
        }
        return false;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByCategoryID($id)
    {

        $q = $this->db->get_where('products', array('category_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return true;
        }

        return FALSE;
    }

    public function getProductQuantity($product_id, $warehouse = DEFAULT_WAREHOUSE)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($this->db->update('warehouses_products', array('quantity' => $quantity), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function getProductByCode($code)
    {

        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getProductByName($name)
    {

        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTransferByID($id)
    {

        $q = $this->db->get_where('transfers', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllTransferItems($transfer_id, $status)
    {
        if ($status == 'completed') {
            $this->db->select('purchase_items.*, product_variants.name as variant, products.unit, products.hsn_code as hsn_code, products.second_name as second_name, brands.name AS name_brand, product_variants.code AS codeVariant')
                ->from('purchase_items')
                ->join('products', 'products.id=purchase_items.product_id', 'left')
                ->join('brands', 'products.brand = brands.id', 'left')
                ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
                ->group_by('purchase_items.id')
                ->where('transfer_id', $transfer_id);
        } else {
            $this->db->select('transfer_items.*, product_variants.name as variant, products.unit, products.hsn_code as hsn_code, products.second_name as second_name, brands.name AS name_brand, product_variants.code AS codeVariant')
                ->from('transfer_items')
                ->join('products', 'products.id=transfer_items.product_id', 'left')
                ->join('brands', 'products.brand = brands.id', 'left')
                ->join('product_variants', 'product_variants.id=transfer_items.option_id', 'left')
                ->group_by('transfer_items.id')
                ->where('transfer_id', $transfer_id);
        }

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getWarehouseProduct($warehouse_id, $product_id, $variant_id)
    {
        if ($variant_id) {
            return $this->getProductWarehouseOptionQty($variant_id, $warehouse_id);
        } else {
            return $this->getWarehouseProductQuantity($warehouse_id, $product_id);
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function resetTransferActions($id, $delete = NULL)
    {
        $otransfer = $this->getTransferByID($id);
        $oitems = $this->getAllTransferItems($id, $otransfer->status);
        $ostatus = $otransfer->status;
        if ($ostatus == 'sent' || $ostatus == 'completed') {
            foreach ($oitems as $item) {
                $option_id = (isset($item->option_id) && ! empty($item->option_id)) ? $item->option_id : NULL;
                $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $item->product_id, 'warehouse_id' => $otransfer->from_warehouse_id, 'option_id' => $option_id);
                $this->site->setPurchaseItem($clause, $item->quantity);
                if ($delete) {
                    $option_id = (isset($item->option_id) && ! empty($item->option_id)) ? $item->option_id : NULL;
                    $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $item->product_id, 'warehouse_id' => $otransfer->to_warehouse_id, 'option_id' => $option_id);
                    $this->site->setPurchaseItem($clause, ($item->quantity_balance - $item->quantity));
                }
            }
        }
        return $ostatus;
    }

    public function deleteTransfer($id)
    {
        $ostatus = $this->resetTransferActions($id, 1);
        $oitems = $this->getAllTransferItems($id, $ostatus);
        $tbl = $ostatus == 'completed' ? 'purchase_items' : 'transfer_items';
        if ($this->db->delete('transfers', array('id' => $id)) && $this->db->delete($tbl, array('transfer_id' => $id))) {
            foreach ($oitems as $item) {
                $this->site->syncQuantity(NULL, NULL, NULL, $item->product_id);
            }
            return true;
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id, $zero_check = TRUE)
    {
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.code as code, product_variants.cost as cost, product_variants.quantity as total_quantity, warehouses_products_variants.quantity as quantity')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->group_by('product_variants.id');
        if ($this->Settings->overselling == 0 && $zero_check) {
            $this->db->where('warehouses_products_variants.warehouse_id', $warehouse_id)
                     ->where('warehouses_products_variants.quantity >', 0);
        }
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->where('warehouses_products.warehouse_id', $warehouse_id)
            ->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncTransderdItem($product_id, $warehouse_id, $quantity, $option_id = NULL)
    {
        if ($pis = $this->site->getPurchasedItems($product_id, $warehouse_id, $option_id)) {
            $balance_qty = $quantity;
            foreach ($pis as $pi) {
                if ($balance_qty <= $quantity && $quantity > 0) {
                    if ($pi->quantity_balance >= $quantity) {
                        $balance_qty = $pi->quantity_balance - $quantity;
                        $this->db->update('purchase_items', array('quantity_balance' => $balance_qty), array('id' => $pi->id));
                        $quantity = 0;
                    } elseif ($quantity > 0) {
                        $quantity = $pi->quantity_balance - $quantity;
                        $balance_qty = $quantity;
                        $this->db->update('purchase_items', array('quantity_balance' => $balance_qty), array('id' => $pi->id));
                    }
                }
                if ($quantity == 0) { break; }
            }
        } else {
            $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'option_id' => $option_id, 'status' => 'received');
            $this->site->setPurchaseItem($clause, (0-$quantity));
        }
        $this->site->syncQuantity(NULL, NULL, NULL, $product_id);
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function post_transfer($transfer_id){
        $data = (array) $this->getTransferByID($transfer_id);
        $items = (array) $this->getAllTransferItems($transfer_id, $data['status']);
        foreach ($items as $key => $item) {
            $items[$key] = (array) $item;
        }
        // $this->sma->print_arrays($items);
        $this->site->wappsiContabilidadTraslados($data, $items);
    }

    public function update_received($transfer_id)
    {
        if ($this->db->update('transfers', array('received' => 1 ), array('id' => $transfer_id))) {
            $reference_no = '';
            $q = $this->db->select('reference_no')->where('id', $transfer_id)->get('transfers');
            if ($q->num_rows() > 0) {
                $q->row();
                $reference_no = $q->result_object[0]->reference_no;
            }
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),  // fecha y hora actual
                'type_id' => 1,                 // 1 = Edición, 2 = Eliminación, 3 = Agregar, 4 = consulta, 5. contabilidad cierre, 6. Exportar
                'table_name' => 'transfers',    
                'record_id' => $transfer_id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => 'transfers',
                'description' => $this->session->first_name." ".$this->session->last_name." aceptó el traslado ".$reference_no
            ]);
            return true;
        }
        return false;
    }

    public function get_product_warehouse_last_cost($product_id, $warehouse_id){
        $sql = "SELECT 
                    PI.id, 
                    PI.product_id, 
                    PI.warehouse_id, 
                    PI.net_unit_cost, 
                    PI.unit_cost 
                FROM {$this->db->dbprefix('purchase_items')} PI
                INNER JOIN 
                    (SELECT MAX(id) pid FROM {$this->db->dbprefix('purchase_items')} PI WHERE PI.purchase_id IS NOT NULL AND PI.status = 'received' GROUP BY PI.product_id, PI.warehouse_id ) AS pi_last_cost ON pi_last_cost.pid = PI.id
                WHERE PI.product_id = {$product_id} AND PI.warehouse_id = {$warehouse_id}";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function addOrder($data = [], $items = [])
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        if ($this->db->insert('order_transfers', $data)) {
            $transfer_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($items as $item) {
                $item['transfer_id'] = $transfer_id;
                $this->db->insert('order_transfer_items', $item);
            }
            return $transfer_id;
        }
        return false;
    }


    public function getOrderTransferId($id)
    {

        $q = $this->db->get_where('order_transfers', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }
    public function getOrderTransferItems($transfer_id)
    {
        $this->db->select('order_transfer_items.*, products.unit, products.hsn_code as hsn_code, products.second_name as second_name, brands.name AS name_brand, products.code as code, products.name as name, colors.name as color, products.tax_rate as tax_rate')
            ->from('order_transfer_items')
            ->join('products', 'products.id=order_transfer_items.product_id', 'left')
            ->join('brands', 'products.brand = brands.id', 'left')
            ->join('colors', 'order_transfer_items.color_id = colors.id', 'left')
            ->group_by('order_transfer_items.id')
            ->where('transfer_id', $transfer_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function endOrder($id){
        $this->db->update('order_transfers', ['status'=>'completed'], ['id'=>$id]);
        return true;
    }
}
