<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Professional_positions extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('professional_positions', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('professional_positions_model');
	}

	public function save()
	{
		$area_id = $this->input->get("area_id");
		$name = $this->input->get("new_professional_position");

		$professional_positions_existing = $this->professional_positions_model->get_by_name($name);
		if ($professional_positions_existing == FALSE) {
			$professional_positions_saved = $this->professional_positions_model->insert(["name"=>$name, "area_id"=>$area_id]);

			if ($professional_positions_saved != FALSE) {
				$response_ajax = ["status"=>TRUE, "message"=>lang("professional_position_saved"), "option_selected"=>$professional_positions_saved];
			} else {
				$response_ajax = ["status"=>FALSE, "message"=>lang("professional_position_not_saved")];
			}

			$professional_positions = $this->professional_positions_model->get_by_area_id($area_id);
			$dropdown_options_string = '<option value="">'.lang("select").'</option>';
	        if ($professional_positions !== FALSE) {
	            foreach ($professional_positions as $position) {
	                $dropdown_options_string .= '<option value="'.$position->id.'">'.$position->name.'</option>';
	            }
	        }

			$response_ajax["data"] = $dropdown_options_string;
		} else {
			$response_ajax = ["status"=>FALSE, "message"=>lang("professional_position_existing")];
		}

		echo json_encode($response_ajax);
	}
}

/* End of file Professional_positions.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/controllers/admin/Professional_positions.php */