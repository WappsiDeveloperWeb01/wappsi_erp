<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .ob {
        list-style: none;
        padding: 0;
        margin: 0;
        margin-top: 10px;
    }
    .ob li {
        width: 49%;
        margin: 0 10px 10px 0;
        float: left;
    }
    @media only screen and (max-width: 799px) {
        .ob li {
            width: 100%;
        }
    }
    .ob li .btn {
        width: 100%;
    }
    .ob li:nth-child(2n+2) {
        margin-right: 0;
    }
</style>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header modal-primary">
            <button type="button" class="close" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <span class="modal-title" id="susModalLabel" style="font-size: 150%;"><?= "(".$pdata->code.") ".$pdata->name ?></span>
            <h3><?=lang('product_variant_selection');?></h3>
        </div>
        <div class="modal-body" style="padding-bottom:0; max-height: 600px !important; overflow: auto;">
            <div class="form-group" id="ui3">
                <div class="input-group">
                    <?php
                        if(!isset($bill_reference)){
                            $bill_reference = "";
                        }
                    ?>
                    <?php echo form_input('search_reference', $bill_reference, 'class="form-control pos-tip btn-pos-product" id="search_reference" data-placement="top" data-trigger="focus" placeholder="'.lang('key_word').'" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
                    <input type="hidden" name="pv_id" id="pv_id">
                    <div class="input-group-addon" style="padding: 2px 8px;border:none !important;">
                        <a id="pv_search" data-placement="bottom" data-html="true" data-toggle="ajax" tabindex="-1"> <i class="fa fa-search" id="xxd" style="font-size: 1.5em;"></i> </a>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
            <div class="html_con"><?= $html ?></div>
            <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            <!-- <button class="btn btn-primary set_product_variant"><?= lang('submit') ?></button> -->
            <!-- <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><?= lang('cancel') ?></button> -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#pv_search').on('click', function (e) {
            var reference = $('#search_reference').val();
            var destino = "<?= admin_url('products/adjustment_product_variants_selection/'.$product_id.'/'.$qaitems_id) ?>/"+$('#qawarehouse').val()+"/"+reference;
            $("#pv_search").attr("href", destino);
        });
        setTimeout(function() {
            $('#search_reference').focus();
        }, 800);

        qaitems = JSON.parse(localStorage.getItem('qaitems'));
        if (qaitems["<?= $qaitems_id ?>"].row.option > 0) {
            $('.product_variant_select[data-pvid="'+qaitems["<?= $qaitems_id ?>"].row.option+'"]').removeClass('btn-outline');
            $('#pv_id').val(qaitems["<?= $qaitems_id ?>"].row.option);
        }

    });
    $('.bootbox-close-button').on('click', function(e){
        $('#myModal').modal('hide');
        e.preventDefault();
    });
    $('#search_reference').on('keypress', function(e){
        if (e.keyCode == 13) {
            $('#pv_search').click();
        }
    });
    $('.product_variant_select').on('click', function(e){
        p_index = $($(this)).index('.product_variant_select');
        // $('#pv_id').val($(this).data('pvid'));
        $(this).removeClass('btn-outline');
        // pv_id = $('#pv_id').val();
        pv_id = $(this).data('pvid');
        qaitems = JSON.parse(localStorage.getItem('qaitems'));
        qaitems["<?= $qaitems_id ?>"].row.option = pv_id;
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
        $('#myModal').modal('hide');
    });
    $(document).on('keyup', '#myModal', function(e){
        if (e.keyCode == 27) {
            close_modal();
        }
    });
    $(document).on('click', '.close', function(){
        close_modal();
    });

    function close_modal(){
        $('#myModal').modal('hide');
        qaitems = JSON.parse(localStorage.getItem('qaitems'));
        delete qaitems["<?= $qaitems_id ?>"];
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
    }

</script>
