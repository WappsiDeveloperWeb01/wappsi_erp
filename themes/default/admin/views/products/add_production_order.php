<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var is_edit = false;
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    $(document).ready(function () {
        if (localStorage.getItem('remove_porls')) {
            if (localStorage.getItem('poritems')) {
                localStorage.removeItem('poritems');
            }
            if (localStorage.getItem('porref')) {
                localStorage.removeItem('porref');
            }
            if (localStorage.getItem('porwarehouse')) {
                localStorage.removeItem('porwarehouse');
            }
            if (localStorage.getItem('pornote')) {
                localStorage.removeItem('pornote');
            }
            if (localStorage.getItem('pordate')) {
                localStorage.removeItem('pordate');
            }
            localStorage.removeItem('remove_porls');
        }

        <?php if ($adjustment_items) { ?>
        localStorage.setItem('poritems', JSON.stringify(<?= $adjustment_items; ?>));
        <?php } ?>
        <?php if ($warehouse_id) { ?>
        localStorage.setItem('porwarehouse', '<?= $warehouse_id; ?>');
        $('#porwarehouse').select2('readonly', true);
        <?php } ?>

        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('pordate')) {
            $("#pordate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#pordate', function (e) {
            localStorage.setItem('pordate', $(this).val());
        });
        if (pordate = localStorage.getItem('pordate')) {
            $('#pordate').val(pordate);
        }
        <?php } ?>

        $("#add_item").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('products/por_suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        biller_id: $("#porbiller").val(),
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item").val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item").val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {

                    $.ajax({
                        url:"<?= admin_url('products/getProductAVGCostByWarehouse'); ?>/"+ui.item.item_id+"/"+$('#porwarehouse').val(),
                    }).done(function(data){

                        if (data != false) {
                            ui.item.row.price = data;
                        }
                        console.log(ui.item);
                        var row = add_adjustment_item(ui.item);
                        if (row){
                            $("#add_item").val('');
                        }

                    }).fail(function(data){
                        console.log(data);
                    });

                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });

        $('#porbiller').trigger('change');

    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php
                    $attrib = array('id'=>'por_form');
                    echo admin_form_open_multipart("products/add_production_order", $attrib);
                    ?>
                        <div class="row">
                            <?php if ($Owner || $Admin) { ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("date", "pordate"); ?>
                                        <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="pordate" required="required"'); ?>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("biller", "porbiller"); ?>
                                    <?php
                                    $bl[""] = "";
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    ?>
                                    <select name="biller" class="form-control" id="porbiller" required="required">
                                        <?php foreach ($billers as $biller): ?>
                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("reference_no", "document_type_id"); ?>
                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("warehouse", "porwarehouse"); ?>
                                        <?php
                                        $wh[''] = '';
                                        foreach ($warehouses as $warehouse) {
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                        echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ($warehouse_id ? $warehouse_id :$Settings->default_warehouse)), 'id="porwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" '.($warehouse_id ? 'readonly' : '').' style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            <?php } else {
                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'warehouse',
                                        'id' => 'porwarehouse',
                                        'value' => $this->session->userdata('warehouse_id'),
                                        );

                                    echo form_input($warehouse_input);
                            } ?>

                            <div class="col-md-4 form-group">
                                <?= lang('employee', 'companies_id') ?>
                                <?php
                                $cOptions[''] = lang('select_third');
                                if ($companies) {
                                    foreach ($companies as $row => $company) {
                                        $cOptions[$company->id] = $company->name;
                                    }
                                }
                                 ?>
                                <?= form_dropdown('companies_id', $cOptions, '',' id="companies_id" class="form-control select" required style="width:100%;"'); ?>
                            </div>

                            <?php if (isset($cost_centers)): ?>
                                <div class="form-group col-md-4">
                                    <?= lang('cost_center', 'cost_center_id') ?>
                                    <?php
                                    $ccopts[''] = lang('select');
                                    foreach ($cost_centers as $cost_center) {
                                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                    }
                                     ?>
                                     <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                </div>
                            <?php endif ?>

                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= lang("por_status", "por_status"); ?>
                                    <?php $sst = array('completed' => lang('approved'), 'pending' => lang('not_approved'));
                                    echo form_dropdown('por_status', $sst, '', 'class="form-control input-tip" required="required" id="porstatus"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" id="sticker">
                                <div class="well well-sm">
                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="input-group wide-tip">
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                <i class="fa fa-2x fa-barcode addIcon"></i>
                                            </div>
                                            <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("products"); ?> *</label>

                                            <div class="controls table-controls">
                                                <table id="porTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th class="col-md-2"><?= lang("unit"); ?></th>
                                                        <th class="col-md-1"><?= lang("type"); ?></th>
                                                        <th class="col-md-1"><?= lang("quantity"); ?></th>
                                                        <th class="col-md-1"><?= lang("product_cost"); ?></th>
                                                        <th style="max-width: 30px !important; text-align: center;">
                                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("components"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="porCTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th class="col-md-2"><?= lang("unit"); ?></th>
                                                        <th class="col-md-1"><?= lang("type"); ?></th>
                                                        <th class="col-md-1"><?= lang("quantity"); ?></th>
                                                        <th class="col-md-1"><?= lang("product_cost"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= lang("note", "pornote"); ?>
                                    <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="pornote" style="margin-top: 10px; height: 100px;"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="fprom-group">
                                    <button type="button" class="btn btn-primary" id="add_adjustment" style="padding: 6px 15px; margin:0 3px 0 0;"><?= lang('submit') ?>
                                    <button type="button" class="btn btn-warning" id="submit_pending" style="padding: 6px 15px; margin:0 3px 0 0;" disabled><?= lang('submit_pending') ?>
                                    <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#porbiller').on('change', function(){
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/18/") ?>'+$('#porbiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
            response = data;

            $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }

            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        });
    });

</script>