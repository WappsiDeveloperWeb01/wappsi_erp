<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_customer_branch') . " (" . $company->name . ")"; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("customers/add_address/" . $company->id, $attrib); ?>
        <div class="modal-body row">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= lang("vat_no", "vat_no"); ?>
                <?php echo form_input('vat_no', $company->vat_no, 'class="form-control" id="vat_no" input_required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('code', 'code'); ?>
                <?= form_input('code', $code, 'class="form-control" id="code" required="required" readonly="readonly"'); ?>
            </div>
            <div class="form-group">
                <?= lang('customer_branch', 'customer_branch'); ?>
                <?= form_input('sucursal', '', 'class="form-control" id="line2"'); ?>
            </div>
            <div class="form-group">
                <?= lang('address', 'address'); ?>
                <?= form_input('direccion', '', 'class="form-control" id="line1" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("country", "country"); ?>
                <select class="form-control select" name="country" id="country" required>
                  <option value="">Seleccione...</option>
                  <?php foreach ($countries as $row => $country): ?>
                    <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= $country->NOMBRE == $this->Settings->customer_default_country ? 'selected="selected"' : '' ?>><?= $country->NOMBRE ?></option>
                  <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <?= lang("state", "state"); ?>
                <select class="form-control select" name="state" id="state" required>
                    <option value="">Seleccione país</option>
                </select>
            </div>
            <div class="form-group">
              <?= lang("city", "city"); ?>
                <select class="form-control select" name="city" id="city" required>
                  <option value="">Seleccione departamento</option>
                </select>
                <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
            </div>

            <div class="form-group">
              <?= lang("zone", "zone"); ?>
              <select class="form-control select" name="zone" id="zone">
                <option value=""><?= lang("select") ?></option>
              </select>
              <input type="hidden" name="zone_code" id="zone_code" <?= (isset($_POST['zone_code'])) ? "value='".$_POST['zone_code']."'" : "" ?>>
            </div>
            <div class="form-group">
              <?= lang("subzone", "subzone"); ?>
              <select class="form-control select" name="subzone" id="subzone">
                <option value=""><?= lang("select") ?></option>
              </select>
              <input type="hidden" name="subzone_code" id="subzone_code" <?= (isset($_POST['subzone_code'])) ? "value='".$_POST['subzone_code']."'" : "" ?>>
            </div>

            <div class="form-group">
              <?= lang("postal_code", "postal_code"); ?>
              <?php echo form_input('postal_code', '', 'class="form-control postal_code" id="postal_code"'); ?>
            </div>

            <div class="form-group">
            <?= lang('phone', 'phone'); ?>
            <?= form_input('phone', '', 'class="form-control" id="phone" required'); ?>
            </div>

            <div class="form-group">
            <?= lang('email', 'email'); ?>
            <?= form_input('email', $company->email, 'class="form-control" id="email"'); ?>
            </div>

            <div class="form-group">
                <?= lang('latitude', 'latitude'); ?>
                <?= form_input('latitude', '', 'class="form-control" id="latitude"'); ?>
            </div>
            <div class="form-group">
                <?= lang('longitude', 'longitude'); ?>
                <?= form_input('longitude', '', 'class="form-control" id="longitude"'); ?>
            </div>

            <div class="form-group">
              <?= lang('customer_address_seller_id_assigned', 'customer_address_seller_id_assigned') ?>
              <?php
                $sellers_opt[''] = lang('select');
                foreach ($sellers as $seller) {
                    $sellers_opt[$seller->id] = $seller->name;
                }
               ?>
              <?=  form_dropdown('customer_address_seller_id_assigned', $sellers_opt, '', 'class="form-control select" id="customer_address_seller_id_assigned"'); ?>
            </div>

            <div class="form-group">
              <label class="control-label" for="price_group"><?php echo $this->lang->line("price_group"); ?></label>
              <?php
                $pgs[''] = lang('select').' '.lang('price_group');
                foreach ($price_groups as $price_group)
                {
                  $pgs[$price_group->id] = $price_group->name;
                }
              ?>
              <?= form_dropdown('price_group', $pgs, $Settings->price_group, 'class="form-control select" id="price_group" style="width:100%;"'); ?>
            </div>

            <div class="form-group">
                <label class="control-label" for="customer_group"><?php echo $this->lang->line("customer_group"); ?></label>
                <?php
                  foreach ($customer_groups as $customer_group)
                  {
                    $cgs[$customer_group->id] = $customer_group->name;
                  }
                ?>
                <?= form_dropdown('customer_group', $cgs, $Settings->customer_group, 'class="form-control select" id="customer_group" style="width:100%;" required="required"'); ?>
            </div>

            <div class="form-group col-md-6  col-sm-6">
              <?= lang('seller_sale_comision', 'seller_sale_comision') ?>
              <select name="seller_sale_comision" id="seller_sale_comision" class="form-control">
                <option value=""><?= lang('select') ?></option>
                <option value="0">0</option>
                <option value="2">2%</option>
                <option value="4">4%</option>
                <option value="5">5%</option>
                <option value="7">7%</option>
                <option value="8">8%</option>
                <option value="10">10%</option>
              </select>
            </div>

            <div class="form-group col-md-6  col-sm-6">
              <?= lang('seller_collection_comision', 'seller_collection_comision') ?>
              <select name="seller_collection_comision" id="seller_collection_comision" class="form-control">
                <option value=""><?= lang('select') ?></option>
                <option value="0">0</option>
                <option value="2">2%</option>
                <option value="4">4%</option>
                <option value="5">5%</option>
                <option value="7">7%</option>
                <option value="10">10%</option>
              </select>
            </div>

        </div>
        <div class="modal-footer">
            <?= form_submit('add_address', lang('add_address'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript">
    $(document).ready(function (e) {

      $(document).on('keydown', '#longitude', function(){
      val = $(this).val();
      if (val > 999) {
        Command: toastr.error('Valor ingresado incorrecto', '¡Error!', {onHidden : function(){}})
        $(this).val('');
      }
    });
    $(document).on('keydown', '#latitude', function(){
      val = $(this).val();
      if (val > 999) {
        Command: toastr.error('Valor ingresado incorrecto', '¡Error!', {onHidden : function(){}})
        $(this).val('');
      }
    });

      $('#seller_sale_comision').select2('readonly', true);

      $('#country').on('change', function(){
        dpto = $('#country option:selected').data('code');

        $.ajax({
          url:"<?= admin_url() ?>customers/get_states/"+dpto,
        }).done(function(data){
          $('#state').html(data);
          <?php if (!empty($this->Settings->customer_default_state)) { ?>
            $('#state').select2('val', '<?= $this->Settings->customer_default_state ?>');
            $('#state').trigger('change');
          <?php } ?>
        }).fail(function(data){
          console.log(data.responseText);
        });
      });

      $('#state').on('change', function(){
        dpto = $('#state option:selected').data('code');

        $.ajax({
          url:"<?= admin_url() ?>customers/get_cities/"+dpto,
        }).done(function(data){
          $('#city').html(data);
          <?php if (!empty($this->Settings->customer_default_city)) { ?>
            $('#city').select2('val', '<?= $this->Settings->customer_default_city ?>');
            $('#city').trigger('change');
          <?php } ?>
        }).fail(function(data){
          console.log(data.responseText);
        });
      });

      $('#city').on('change', function(){
        code_country = $('#country option:selected').data('code');
        code = $('#city option:selected').data('code');

        postal_code = code.toString().replace(code_country, '');
        $('.postal_code').val(postal_code);
        $('#city_code').val(code);

        $.ajax({
          url:"<?= admin_url().'customers/get_zones/' ?>"+code
        }).done(function(data){
          $('#zone').html(data);
        });
      });

      $('#zone').on('change', function(){
        code = $('#zone option:selected').data('code');
        $('.postal_code').val(code);
        $.ajax({
          url:"<?= admin_url().'customers/get_subzones/' ?>"+code
        }).done(function(data){
          $('#subzone').html(data);
        });
      });
      
      $('#subzone').on('change', function(){
        code = $('#subzone option:selected').data('code');
        $('.postal_code').val(code);
      });
      
      <?php if (!empty($this->Settings->customer_default_country)) { ?>
        $('#country').trigger('change');
      <?php } ?>
    });
</script>

