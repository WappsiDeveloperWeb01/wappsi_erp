<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('reports/customers', 'id="FilterForm"'); ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-md-2 col-xs-12" >
                                        <?= lang('Fecha de registro', 'Fecha de registro') ?>
                                        <select name="records_filter" id="records_filter" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>
                                    <div class="date_controls">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang("start_date", "start_date"); ?>
                                                <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang("end_date", "end_date"); ?>
                                                <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <button class="btn btn-primary new-button" name='filter' value='1' type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <li class="dropdown"><a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>"><i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?></a></li>
                                    <li class="dropdown"><a href="#" id="image" class="tip" title="<?= lang('save_image') ?>"><i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="CusData" class="table table-condensed table-hover reports-table">
                                    <thead>
                                    <tr class="primary">
                                        <th><?= lang("company"); ?></th>
                                        <th><?= lang("name"); ?></th>
                                        <th><?= lang("phone"); ?></th>
                                        <th><?= lang("email_address"); ?></th>
                                        <th><?= lang("total_sales"); ?></th>
                                        <th><?= lang("total_amount"); ?></th>
                                        <th><?= lang("paid"); ?></th>
                                        <th><?= lang("balance"); ?></th>
                                        <th style="width:85px;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr class="active">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-center"><?= lang("total_sales"); ?></th>
                                        <th class="text-center"><?= lang("total_amount"); ?></th>
                                        <th class="text-center"><?= lang("paid"); ?></th>
                                        <th class="text-center"><?= lang("balance"); ?></th>
                                        <th style="width:85px;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        <?php if (!isset($_POST['filter'])) { ?>
            $('#records_filter').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
        <?php } else { ?>
            $('#records_filter').val("<?= isset($_POST['records_filter']) ? $_POST['records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
        <?php } ?>
        <?php if (!isset($_POST['start_date'])): ?>
            // $('#start_date').val(fecha_inicial);
            // $('#end_date').val(fecha_final);}
        <?php endif ?>
        $('#records_filter').on('change', function(){
            filter = $(this).val();
            if (filter == 5) {
                $('.date_controls').css('display', '');
            }else{
                $('.date_controls').css('display', 'none');
            }
        }).trigger('change');
            
        $('#records_filter').val("<?= isset($_POST['records_filter']) ? $_POST['records_filter'] : $this->Settings->default_records_filter ?>").trigger('change');
        let $v = get_date_filter(   $('#records_filter').val(),
                                    $('#start_date').val(),
                                    $('#end_date').val(),
                                );

        oTable = $('#CusData').dataTable({
            "aaSorting": [[0, "asc"], [1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('reports/getCustomers?v=1') ?>'+$v,
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [null, null, null, null, {
                "mRender": decimalFormat,
                "bSearchable": false
            }, {"mRender": currencyFormat, "bSearchable": false}, {
                "mRender": currencyFormat,
                "bSearchable": false
            }, {"mRender": currencyFormat, "bSearchable": false}, {"bSortable": false}],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var purchases = 0, total = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    purchases += parseFloat(aaData[aiDisplay[i]][4]) || 0;                    
                    total += parseFloat(aaData[aiDisplay[i]][5]) || 0 ;
                    paid += parseFloat(aaData[aiDisplay[i]][6]) || 0;
                    balance += parseFloat(aaData[aiDisplay[i]][7]) || 0;
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[4].innerHTML = decimalFormat(parseFloat(purchases));
                nCells[5].innerHTML = currencyFormat(parseFloat(total));
                nCells[6].innerHTML = currencyFormat(parseFloat(paid));
                nCells[7].innerHTML = currencyFormat(parseFloat(balance));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('company');?>]", filter_type: "text", data: []},
            {column_number: 1, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('phone');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('email_address');?>]", filter_type: "text", data: []},
        ], "footer");

        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getCustomers/pdf')?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getCustomers/0/xls?')?>"+$v;
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
    });

    function get_date_filter(filter, start_date=null, end_date=null){
        let v = '';
        fecha_inicial = "";
        fecha_final = "<?= date('d/m/Y 23:59') ?>";
        if (filter == 5) { //RANGO DE FECHAS
            fecha_final = "";
        if (filter == 5) { //RANGO DE FECHAS
            fecha_inicial = start_date;
            fecha_final = end_date;
        }
        } else if (filter == 1) { // HOY
            fecha_inicial = "<?= date('d/m/Y 00:00') ?>";
        } else if (filter == 2) { // MES
            fecha_inicial = "<?= date('01/m/Y 00:00') ?>";
        } else if (filter == 3) { // TRIMESTRE
            fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
        } else if (filter == 8) { // SEMESTRE
            fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
        } else if (filter == 4) { // AÑO
            fecha_inicial = "<?= date('01/01/Y 00:00') ?>";
        } else if (filter == 6) { // MES PASADO
            fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 1 month")) ?>";
            fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
        } else if (filter == 9) { // TRIMESTRE PASADO
            fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
            fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))))) ?>";
        } else if (filter == 10) { // SEMESTRE PASADO
            fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
            fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))))) ?>";
        } else if (filter == 7) { // AÑO PASADO
            fecha_inicial = "<?= date("01/01/Y 00:00",strtotime(date("Y-m-01")."- 1 year")) ?>";
            fecha_final = "<?= date("31/12/Y 23:59",strtotime(date("Y-m-01")."- 1 year")) ?>";
        } else if (filter == 11) { // AÑO PASADO
            fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 2 month")) ?>";
        } else if (filter == 0) {
            fecha_inicial = "";
            fecha_final = "";
        }
        v += '&start_date='+fecha_inicial;
        v += '&end_date='+fecha_final;
        return v;
    }
</script>