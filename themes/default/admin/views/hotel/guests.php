<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    var oTable;
    if ($(window).width() < 1000) {
        var nums = [[10, 25], [10, 25]];
    } else {
        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
    }
    $(document).ready(function ()
    {
        var oTable = $('#SLData').dataTable({
            "aaSorting": [[1, "desc"], [3, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength":  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=admin_url('hotel/get_guests'); ?>',
            fnServerData: function (sSource, aoData, fnCallback)
            {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex)
            {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                return nRow;
            },
            "aoColumns": [
                {"mRender": checkbox},
                null,
                null,
                null,
                null,
                null,
                {"mRender": fls},
                null,
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {

            }
        }).fnSetFilteringDelay().dtFilter([
            // {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
        ], "footer");
    });

</script>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading no-printable">
    <div class="col-lg-8">
        <h2><?=lang('guests');?></h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<!-- /Header -->


<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
    	    echo admin_form_open('sales/sale_actions', 'id="action-form"');
    	}
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <?php if ($this->Owner || $this->Admin || $this->GP['hotel-add_guest_register'] || $this->GP['bulk_actions']): ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <?php if ($this->Owner || $this->Admin || $this->GP['hotel-add_guest']): ?>
                                    <li>
                                        <a href="<?=admin_url('hotel/add_guest')?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i> <?=lang('add_guest')?>
                                        </a>
                                    </li>
                                    <?php endif ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><?//=lang('list_results');?></p> -->
                            <div class="table-responsive">
                                <h4 class="text_filter"></h4>
                                <table id="SLData" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("names"); ?></th>
                                        <th><?= lang("surnames"); ?></th>
                                        <th><?= lang("nationality"); ?></th>
                                        <th><?= lang("phone"); ?></th>
                                        <th><?= lang("email"); ?></th>
                                        <th><?= lang("birth_date"); ?></th>
                                        <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr>
                                        <th style="text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("names"); ?></th>
                                        <th><?= lang("surnames"); ?></th>
                                        <th><?= lang("nationality"); ?></th>
                                        <th><?= lang("phone"); ?></th>
                                        <th><?= lang("email"); ?></th>
                                        <th><?= lang("birth_date"); ?></th>
                                        <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $Admin || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>