<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$v = "";
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<?php
	$fech_ini = date('Y-m-d');
	$fech_fin = date("Y-m-d",strtotime($fech_ini."+ 1 days"));
	if ($profitabilitys) {
		foreach ($profitabilitys as $profitability)
		{
			if($fech_ini>$profitability['date'])
			{
				$fech_ini = $profitability['date'];
			}
		}
		$fech_ini = substr($fech_ini,0,10);

		$fech_ini = (!empty($datos)) ? $datos['inicial'] : $fech_ini ;
		$fech_fin = (!empty($datos)) ? $datos['Final'] : $fech_fin ;
	}
		

?>

<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
					<div class="row">
                        <div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3><?php echo lang('filter_date');?> <span id="textini"><?php echo $fech_ini; ?></span> a <span id="textfin"><?php echo $fech_fin; ?></span> </h3>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
								<?php echo form_open('admin/reports/product_profitability' , 'id="form_filter"'); ?>
                            			<input type="hidden" name="filter_action" id="filter_action" value="0">
										<div class="row">
											<div class="col-md-4">
												<?= lang('product', 'Producto') ?>
												<select class="form-control " name="Producto" id="Producto">
													<option value="all" > <?php echo lang('select'); ?> </option>
													<?php foreach ($productos as $producto): ?>
														<option value="<?php echo $producto['id']; ?>" <?php echo ((!empty($datos)) AND ($datos['Producto']==$producto['id'])) ? 'selected' : ''; ?> ><?php echo "(".$producto['code'].") ".$producto['name']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
											<?php
	                                    $bl[""] = "";
	                                    $bldata = [];
	                                    foreach ($billers as $biller) {
	                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
	                                        $bldata[$biller->id] = $biller;
	                                    }
	                                    // exit(var_dump($this->session->userdata('biller_id')));
	                                    ?>

	                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <?= lang("biller", "slbiller"); ?>
	                                                <select name="biller" class="form-control" id="slbiller" required="required">
	                                                    <option value=""><?= lang('select') ?></option>
	                                                    <?php foreach ($billers as $biller) : ?>
	                                                        <option value="<?= $biller->id ?>" <?php echo ((!empty($datos)) AND ($datos['biller']==$biller->id)) ? 'selected' : ''; ?> ><?= $biller->company ?></option>
	                                                    <?php endforeach ?>
	                                                </select>
	                                            </div>
	                                        </div>
	                                    <?php } else { ?>
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <?= lang("biller", "slbiller"); ?>
	                                                <select name="biller" class="form-control" id="slbiller">
	                                                    <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
	                                                        $biller = $bldata[$this->session->userdata('biller_id')];
	                                                    ?>
	                                                        <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
	                                                    <?php endif ?>
	                                                </select>
	                                            </div>
	                                        </div>
	                                    <?php } ?>
		                                    <div class="col-md-2">
		                                    	<?= lang('year', 'year') ?>
		                                    	<select class="form-control" name="year" id="year">
											    <?php
											    $selected_year = isset($_POST['year']) ? $_POST['year'] : date('Y');
											    $current_year = date('Y');
											    $previous_year = $current_year - 1;

											    foreach ([$current_year, $previous_year] as $year) {
											        $selected = ($year == $selected_year) ? 'selected' : '';
											        echo "<option value='$year' $selected>$year</option>";
											    }
											    ?>
											</select>
		                                    </div>
		                                    <div class="col-md-2">
		                                    	<?= lang('month', 'month') ?>
		                                    	<select class="form-control" name="month" id="month">
												    <?php
												    $selected_month = isset($_POST['month']) ? $_POST['month'] : date('m');

												    foreach (lang('months') as $key => $value) {
												        $selected = ($key == $selected_month) ? 'selected' : '';
												        echo "<option value='$key' $selected>$value</option>";
												    }
												    ?>
												</select>
		                                    </div>
		                                    
										</div>
										<br>
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-3">
												<button type="submit" class="btn btn-success" class="Buscar" id="Buscar" ><?php echo lang('submit');?></button>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6">
											</div>
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="pull-right dropdown">
													<button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
													<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
														<!-- <li>
															<a class="ExportPdf" id="ExportPdf"> PDF </a>
														</li> -->
														<li>
															<a class="ExportXls" id="ExportXls"> Excel </a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
							<div class="col-lg-12" >
								<div class="table-responsive">
									<table id="TableData" class="table table-stripped" >
										<thead>
											<tr>
												<td class="text-center" style="width: 12%;"> <b><?php echo lang('code'); ?></b></td>
												<td class="text-center" style="width: 25%;"> <b><?php echo lang('product'); ?></b></td>
												<td class="text-center" style="width: 11%;"> <b><?php echo lang('quantity'); ?></b></td>
												<td class="text-right" style="width: 11%;"> <b><?php echo lang('sales'); ?></b></td>
												<td class="text-right" style="width: 11%;"> <b><?php echo lang('cost'); ?></b></td>
												<td class="text-right" style="width: 11%;"> <b><?php echo lang('profit'); ?></b></td>
												<td class="text-right" style="width: 11%;"> <b><?php echo lang('margin'); ?></b></td>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($profitabilitys as $profitability): ?>
												<tr >
													<td ><?php echo $profitability['cod_product']; ?></td>
													<td ><?php echo $profitability['name']; ?></td>
													<td class="text-right"><?php echo $this->sma->formatQuantity($profitability['quantity']); ?></td>
													<td class="text-right"> <?php echo $this->sma->formatMoney($profitability['total']); ?></td>
													<td class="text-right"> <?php echo $this->sma->formatMoney($profitability['costo']); ?></td>
													<td class="text-right"> <?php echo $this->sma->formatMoney($profitability['utilidad']); ?></td>
													<td class="text-right"> <?php echo $this->sma->formatDecimal($profitability['margen']); ?> </td>
												</tr>
											<?php endforeach; ?>
										</tbody>
										<tfoot>
											<tr>
												<th class="text-center" > <b><?php echo lang('code'); ?></b></th>
												<th class="text-center" > <b><?php echo lang('product'); ?></b></th>
												<th class="text-center" > <b><?php echo lang('quantity'); ?></b></th>
												<th class="text-right" > <b><?php echo lang('sales'); ?></b></th>
												<th class="text-right" > <b><?php echo lang('cost'); ?></b></th>
												<th class="text-right" > <b><?php echo lang('profit'); ?></b></th>
												<th class="text-right" > <b><?php echo lang('margin'); ?></b></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<script >

	var table = '';

	function currencyFormat(x) {
		return x;
	}


	$(document).ready(function() {
		$('#Producto').select2();
		table = $('#TableData').DataTable({
			"processing": false,
			pageLength: 100,
    		responsive: true,
			oLanguage: <?php echo $dt_lang; ?>
		});

		$("#ExportPdf").click(function() {
			var Producto = $("#Producto option:selected").val() ? $("#Producto option:selected").val() : '';
			var year = $('#year').val() ? $('#year').val() : '';
			var month = $('#month').val() ? $('#month').val() : '';
			var biller = $('#slbiller').val() ? $('#slbiller').val() : '';
			window.open(site.base_url+'reports/action_product_profitability/pdf/'+Producto+'/'+year+'/'+month+'/'+biller);
		});
		$("#ExportXls").click(function() {
			var Producto = $("#Producto option:selected").val() ? $("#Producto option:selected").val() : '';
			var year = $('#year').val() ? $('#year').val() : '';
			var month = $('#month').val() ? $('#month').val() : '';
			var biller = $('#slbiller').val() ? $('#slbiller').val() : '';
			window.open(site.base_url+'reports/action_product_profitability/xls/'+Producto+'/'+year+'/'+month+'/'+biller);
		});
		$('#Buscar').on('click', function(e){
		    $('#filter_action').val(1);
		    $('#form_filter').submit();
		});
	});


</script>
