<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_supplier'); ?></h4>
        </div>
        <?= admin_form_open_multipart("suppliers/edit/" . $supplier->id, ['id' => 'edit-supplier-form']); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Tipo de proveedor</label>
                    <?php
                    $tsupplier[1] = "Productos y Gastos";
                    $tsupplier[2] = "Productos";
                    $tsupplier[3] = "Gastos";
                    $tsupplier[4] = "Acreedores";
                    $tsupplier[5] = "Entidad Financiera";
                    $tsupplier[6] = "Entidad Promotora de Salud (EPS)";
                    $tsupplier[7] = "Fondo de Pensiones y Cesantías";
                    $tsupplier[8] = "Administradora de Riesgos Laborales (ARL)";
                    $tsupplier[9] = "Caja de compensación";
                    $tsupplier[10] = "Servicio Nacional de Aprendizaje (SENA)";
                    echo form_dropdown('supplier_type', $tsupplier, $supplier->supplier_type, 'class="form-control select" id="supplier_type" style="width:100%;" required="required"');
                    ?>
                </div>
                <div class="form-group col-md-6">
                    <label>Tipo de persona</label>
                    <?php
                    $typesPersonOptions[""] = lang('select');
                    foreach ($typesPerson as $typePerson) {
                        $typesPersonOptions[$typePerson->id] = lang($typePerson->description);
                    }
                    ?>
                    <?= form_dropdown('type_person', $typesPersonOptions, $supplier->type_person, 'class="form-control select" id="type_person" style="width:100%;" required="required"'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= form_label(lang("label_type_vat_regime"), "type_vat_regime"); ?>
                    <?php
                    $types_vat_regime_options[""] = lang('select');
                    foreach ($types_vat_regime as $type_vat_regime) {
                        $types_vat_regime_options[$type_vat_regime->id] = lang($type_vat_regime->description);
                    }
                    ?>
                    <?= form_dropdown(["name" => "tipo_regimen", "id" => "tipo_regimen", "class" => "form-control select", "required" => TRUE], $types_vat_regime_options, $supplier->tipo_regimen); ?>
                </div>
                <div class="form-group col-md-6">
                    <?= lang("id_document_type", "id_document_type"); ?>
                    <select class="form-control" name="tipo_documento" id="tipo_documento" style="width: 100%;" required>
                        <option value=""><?= lang("select"); ?></option>
                        <?php foreach ($id_document_types as $idt) : ?>
                            <option value="<?= $idt->id; ?>" data-code="<?= $idt->codigo_doc; ?>" <?= (isset($supplier) && $supplier->tipo_documento == $idt->id) ? "selected" : ""; ?>><?= lang($idt->nombre); ?></option>
                        <?php endforeach ?>
                    </select>
                    <?= form_hidden('document_code'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang("vat_no", "vat_no"); ?>
                    <?php echo form_input('vat_no', $supplier->vat_no, 'class="form-control" id="vat_no" required="required"'); ?>
                </div>
                <div class="form-group col-md-6 digito-verificacion" style="display: none;">
                    <?= lang("check_digit", "check_digit"); ?>
                    <?php echo form_input('digito_verificacion', $supplier->digito_verificacion, 'class="form-control" id="digito_verificacion" readonly'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6 person juridical_person">
                    <?= lang("name", "name"); ?>
                    <?php echo form_input('name', $supplier->name, 'class="form-control tip required" id="name" data-bv-notempty="true"'); ?>
                </div>

                <div class="form-group col-md-6 company hide_pos_customer natural_person"  style="display: none;">
                    <?= lang("label_tradename", "company"); ?>
                    <?= form_input('company', '', 'class="form-control tip" id="company"'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6 person natural_person" style="display: none;">
                        <label>Primer Nombre</label><span class='input_required'> *</span>
                        <input type="text" name="first_name" id="first_name" value="<?= $supplier->first_name ?>" class="form-control">
                </div>
                <div class="form-group col-md-6 person natural_person" style="display: none;">
                        <label>Segundo Nombre</label>
                        <input type="text" name="second_name" id="second_name" value="<?= $supplier->second_name ?>" class="form-control">
                </div>
                <div class="form-group col-md-6 person natural_person" style="display: none;">
                        <label>Primer Apellido</label><span class='input_required'> *</span>
                        <input type="text" name="first_lastname" id="first_lastname" value="<?= $supplier->first_lastname ?>" class="form-control">
                </div>
                <div class="form-group col-md-6 person natural_person" style="display: none;">
                        <label>Segundo Apellido</label>
                        <input type="text" name="second_lastname" id="second_lastname" value="<?= $supplier->second_lastname ?>" class="form-control">
                </div>
            </div>
            <div class="row">

                <div class="form-group col-md-6">
                    <?= lang("email_address", "email_address"); ?>
                    <input type="email" name="email" class="form-control" required="required" value="<?= $supplier->email ?>" id="email_address" />
                </div>
                <div class="form-group col-md-6">
                    <?= lang("phone", "phone"); ?>
                    <input type="tel" name="phone" class="form-control" required="required" value="<?= $supplier->phone ?>" id="phone" />
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang("address", "address"); ?>
                    <?php echo form_input('address', $supplier->address, 'class="form-control" id="address" required="required"'); ?>
                </div>
                <div class="form-group col-md-6">
                    <?= lang("postal_code", "postal_code"); ?>
                    <small><a href="https://visor.codigopostal.gov.co/472/visor/" target="_blank"> Buscar códigos postales</a></small>
                    <?php echo form_input('postal_code', $supplier->postal_code, 'class="form-control postal_code" id="postal_code"'); ?>
                </div>
            </div>
            <div class="row">

                <div class="form-group col-md-6">
                    <?= lang("country", "country"); ?>
                    <select class="form-control select" name="country" id="country" required>
                        <option value="">Seleccione...</option>
                        <?php foreach ($countries as $row => $country) : ?>
                            <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= ($country->NOMBRE == $supplier->country) ? "selected='selected'" : "" ?>><?= $country->NOMBRE ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <?= lang("state", "state"); ?>
                    <select class="form-control select" name="state" id="state" required>
                        <option value="">Seleccione...</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang("city", "city"); ?>
                    <select class="form-control select" name="city" id="city" required>
                        <option value="">Seleccione...</option>
                        <!-- <option value="<?= $supplier->city ?>"><?= $supplier->city ?></option> -->
                    </select>
                    <?php $cityCode = (isset($_POST['city_code'])) ? $_POST['city_code'] : (!empty($supplier->city_code) ? $supplier->city_code : ''); ?>
                    <input type="hidden" name="city_code" id="city_code" value="<?= $cityCode ?>">
                </div>
                <!-- <div class="form-group col-md-6">
                <label>
                    <input type="checkbox" name="customer_validate_min_base_retention" id="customer_validate_min_base_retention"  <?= $supplier->customer_validate_min_base_retention ? 'checked="checked"' : '' ?>>
                    <?= lang('customer_validate_min_base_retention') ?>
                </label>
            </div> -->
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <?= lang('supplier_payment_type', 'supplier_payment_type') ?>
                    <?php
                    $cptypes = [
                        '1' => lang('payment_type_cash'),
                        '0' => lang('credit'),
                    ];
                    ?>
                    <?= form_dropdown('supplier_payment_type', $cptypes, $supplier->customer_payment_type, 'class="form-control select" id="supplier_payment_type" style="width:100%;" required="required"'); ?>
                </div>
                <div class="col-md-6" id="generateSupportingDocumentContainer">
                    <div class="form-group">
                        <label style="margin-top: 35px;">
                            <input type="checkbox" name="generateSupportingDocument" id="generateSupportingDocument" <?= $supplier->generateSupportingDocument == 1 ? 'checked="checked"' : '' ?>> <?= lang('Do_you_handle_supporting_documents?') ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6 credit_customer" style="display: none;">
                    <?= lang('supplier_credit_limit', 'supplier_credit_limit') ?>
                    <input type="text" name="supplier_credit_limit" id="supplier_credit_limit" class="form-control only_number" value="<?= $supplier->customer_credit_limit ?>">
                </div>
                <div class="form-group col-md-6 credit_customer" style="display: none;">
                    <?= lang('supplier_payment_term', 'supplier_payment_term') ?>
                    <input type="text" name="supplier_payment_term" id="supplier_payment_term" class="form-control only_number" value="<?= $supplier->customer_payment_term ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= form_label(lang("label_types_obligations"), "types_obligations"); ?>
                    <?php
                    $typesObligationsOptions = [];
                    foreach ($typesObligations as $typesObligation) {
                        $typesObligationsOptions[$typesObligation->code] = $typesObligation->code . " - " . $typesObligation->description;
                    }
                    ?>
                    <?= form_dropdown(['name' => 'types_obligations[]', 'id' => 'types_obligations', 'class' => 'form-control select', 'multiple' => TRUE, 'style' => 'height: auto;', 'required' => TRUE], $typesObligationsOptions); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="taxResident" style="margin-top: 35px;">
                            <?php $checked = $supplier->taxResident == YES ? "checked" : "" ?>
                            <input type="checkbox" name="taxResident" id="taxResident" <?= $checked ?>> <?= lang('tax_resident') ?>
                        </label>
                    </div>
                </div>
            </div>
            <br>
            <?php if ($custom_fields) : ?>
                <div class="row">
                    <hr class="col-md-11">
                    <?php foreach ($custom_fields as $id => $arr) : ?>
                        <?php
                        $cf = $arr['data'];
                        $cf_values = isset($arr['values']) ? $arr['values'] : NULL;
                        ?>
                        <div class="col-md-6">
                            <input type="hidden" name="custom_field_code[]" value="<?= $cf->cf_code ?>">
                            <label><?= $cf->cf_name ?></label>
                            <?php if ($cf->cf_type == 'select' || $cf->cf_type == 'multiple') : ?>
                                <select name="custom_field_value[]" class="form-control" <?= $cf->cf_type == 'multiple' ? "multiple" : "" ?>>
                                    <?php if ($cf_values) : ?>
                                        <option value="">Seleccione...</option>
                                        <?php foreach ($cf_values as $cfv) : ?>
                                            <?php
                                            $selected = "";
                                            if ($cf->cf_type == 'multiple') {
                                                $selected = strpos($product->{$cf->cf_code}, $cfv->cf_value) !== -1 ? "selected" : "";
                                            } else {
                                                $selected = $product->{$cf->cf_code} == $cfv->cf_value ? "selected" : "";
                                            }
                                            ?>
                                            <option value="<?= $cfv->cf_value ?>" <?= $selected ?>><?= $cfv->cf_value ?></option>
                                        <?php endforeach ?>
                                    <?php else : ?>
                                        <option value="">Sin valores para el campo</option>
                                    <?php endif ?>
                                </select>
                            <?php endif ?>
                            <?php if ($cf->cf_type == 'date' || $cf->cf_type == 'text') : ?>
                                <input type="<?= $cf->cf_type ?>" name="custom_field_value[]" class="form-control" <?= $supplier->{$cf->cf_code} ? "value='" . $supplier->{$cf->cf_code} . "'" : "" ?>>
                            <?php endif ?>
                        </div>
                    <?php endforeach ?>
                </div>
                <hr>
            <?php endif ?>

            <div class="row">
                <div class="col-sm-6 form-group">
                    <?= lang('seller_phone', 'seller_phone') ?>
                    <input type="number" name="seller_phone" id="seller_phone" class="form-control only_number" value="<?= $supplier->seller_phone ?>">
                </div>
                <div class="col-sm-6 form-group">
                    <?= lang('seller_email', 'seller_email') ?>
                    <input type="text" name="seller_email" id="seller_email" class="form-control" value="<?= $supplier->seller_email ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <?= lang('seller_name', 'seller_name') ?>
                    <input type="text" name="seller_name" id="seller_name" class="form-control" value="<?= $supplier->seller_name ?>">
                </div>
                <div class="col-sm-6 form-group">
                    <?= lang('account_number', 'account_number') ?>
                    <input type="number" name="account_number" id="account_number" class="form-control only_number" value="<?= $supplier->account_number ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <?= lang('account_type', 'account_type') ?>
                    <input type="text" name="account_type" id="account_type" class="form-control" value="<?= $supplier->account_type ?>">
                </div>
                <div class="col-sm-6 form-group">
                    <?= lang('bank_id', 'bank_id') ?>
                    <input type="text" name="bank_id" id="bank_id" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <?= lang('delivery_days', 'delivery_days') ?>
                    <input type="number" name="delivery_days" id="delivery_days" class="form-control only_number" value="<?= $supplier->delivery_days ?>">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><?= lang('retention') ?></th>
                                <th><?= lang('percentage') ?></th>
                                <th><?= lang('min_base') ?></th>
                                <th><?= lang('ledger') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($retentions) > 0) : ?>
                                <?php if (isset($retentions['FUENTE'])) : ?>
                                    <tr class="row_rete_fuente" <?= $supplier->fuente_retainer ? 'style="display:none;"' : '' ?>>
                                        <td>
                                            <select name="default_rete_fuente_id" id="default_rete_fuente_id" class="form-control">
                                                <option value=""><?= lang('no_rete_fuente_default') ?></option>
                                                <?php foreach ($retentions['FUENTE'] as $row) : ?>
                                                    <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>" <?= $supplier->default_rete_fuente_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_fuente_percentage form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_fuente_minbase form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_fuente_ledger form-control" readonly>
                                        </td>
                                    </tr>
                                <?php endif ?>
                                <?php if (isset($retentions['IVA'])) : ?>
                                    <tr class="row_rete_iva" <?= $supplier->iva_retainer ? 'style="display:none;"' : '' ?>>
                                        <td>
                                            <select name="default_rete_iva_id" id="default_rete_iva_id" class="form-control">
                                                <option value=""><?= lang('no_rete_iva_default') ?></option>
                                                <?php foreach ($retentions['IVA'] as $row) : ?>
                                                    <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>" <?= $supplier->default_rete_iva_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_iva_percentage form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_iva_minbase form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_iva_ledger form-control" readonly>
                                        </td>
                                    </tr>
                                <?php endif ?>
                                <?php if (isset($retentions['ICA'])) : ?>
                                    <tr class="row_rete_ica" <?= $supplier->ica_retainer ? 'style="display:none;"' : '' ?>>
                                        <td>
                                            <select name="default_rete_ica_id" id="default_rete_ica_id" class="form-control">
                                                <option value=""><?= lang('no_rete_ica_default') ?></option>
                                                <?php foreach ($retentions['ICA'] as $row) : ?>
                                                    <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>" <?= $supplier->default_rete_ica_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_ica_percentage form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_ica_minbase form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_ica_ledger form-control" readonly>
                                        </td>
                                    </tr>
                                <?php endif ?>
                                <?php if (isset($retentions['OTRA'])) : ?>
                                    <tr class="row_rete_other">
                                        <td>
                                            <select name="default_rete_other_id" id="default_rete_other_id" class="form-control">
                                                <option value=""><?= lang('no_rete_other_default') ?></option>
                                                <?php foreach ($retentions['OTRA'] as $row) : ?>
                                                    <option value="<?= $row['id'] ?>" data-percentage="<?= $row['percentage'] ?>" data-account="<?= isset($row['ledger_name']) ? $row['ledger_name'] : '' ?>" data-minbase="<?= $row['min_base'] ?>" <?= $supplier->default_rete_other_id == $row['id'] ? 'selected="selected"' : '' ?>><?= $row['description'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_other_percentage form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_other_minbase form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="rete_other_ledger form-control" readonly>
                                        </td>
                                    </tr>
                                <?php endif ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-success" id="submit_edit_supplier" type="button"><?= lang('edit_supplier') ?></button>
        </div>
        <?php echo form_close(); ?>
    </div>

    <script type="text/javascript">
        $(document).ready(function(e) {
            load_types_customer_obligations();

            $("#edit-supplier-form").validate({
                ignore: []
            });

            $(document).on('click', '#submit_edit_supplier', function() {
                form = $('#edit-supplier-form');
                console.log(form.valid());
                if (form.valid()) {
                    form.submit();
                }
            });

            $('#tipo_documento').on('change', function() {
                if ($(this).val() == 6 && site.settings.get_companies_check_digit == 1) {
                    $('.digito-verificacion').css('display', '');
                    $('.digito-verificacion input').attr('required', true);
                } else {
                    $('.digito-verificacion').css('display', 'none');
                    $('.digito-verificacion input').attr('required', false);
                }

                $('input[name="document_code"]').val($('#tipo_documento option:selected').data('code'));
            });

            $('#type_person').on('change', function() {
                tp = $(this).val();
                if (tp == <?= NATURAL_PERSON ?>) {
                    $('input[name="first_name"]').attr('required', true);
                    $('input[name="first_lastname"]').attr('required', true);
                    var name_arr = $('#name').val().split(' ');
                    $('.natural_person').css('display', '');
                    $.each($('.natural_person').find('.required'), function() {
                        $(this).attr('required', true);
                    });
                    $('.juridical_person').css('display', 'none');
                    $.each($('.juridical_person').find('.required'), function() {
                        $(this).attr('required', false);
                    });
                    $('#name').val('xx');
                } else if (tp == <?= LEGAL_PERSON ?>) {
                    $('input[name="first_name"]').removeAttr('required');
                    $('input[name="first_lastname"]').removeAttr('required');
                    $('.natural_person').css('display', 'none');
                    $.each($('.natural_person').find('.required'), function() {
                        $(this).attr('required', false);
                    });
                    $('.juridical_person').css('display', '');
                    $.each($('.juridical_person').find('.required'), function() {
                        $(this).attr('required', true);
                    });
                    $('#name').val('<?= $supplier->name ?>');
                }
            });

            $('#state').on('change', function() {
                dpto = $('#state option:selected').data('code');
                $.ajax({
                    url: "<?= admin_url() ?>customers/get_cities/" + dpto,
                }).done(function(data) {
                    $('#city').html(data);
                }).fail(function(data) {
                    console.log(data);
                });
            });

            $('#country').on('change', function() {
                dpto = $('#country option:selected').data('code');
                $.ajax({
                    url: "<?= admin_url() ?>customers/get_states/" + dpto,
                }).done(function(data) {
                    $('#state').html(data);
                }).fail(function(data) {});
            });
            $('#city').on('change', function() {
                code = $('#city option:selected').data('code');
                $('#city_code').val(code);
            });
            $('#vat_no').on('change', function() {
                nit = $(this).val();
                if (site.settings.get_companies_check_digit == 1) {
                    dvf = calcularDigitoVerificacion(nit);
                    $('#digito_verificacion').val(dvf);
                }
            });

            $('#bank_id').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10,
                            ptype: 8,
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });

            $('#bank_id').val('<?= $supplier->bank_id ?>').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10,
                            ptype: 8,
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        });

        function calcularDigitoVerificacion(myNit) {
            var vpri,
                x,
                y,
                z;
            // Se limpia el Nit
            myNit = myNit.replace(/\s/g, ""); // Espacios
            myNit = myNit.replace(/,/g, ""); // Comas
            myNit = myNit.replace(/\./g, ""); // Puntos
            myNit = myNit.replace(/-/g, ""); // Guiones
            // Se valida el nit
            if (isNaN(myNit)) {
                console.log("El nit/cédula '" + myNit + "' no es válido(a).");
                return "";
            };
            // Procedimiento
            vpri = new Array(16);
            z = myNit.length;
            vpri[1] = 3;
            vpri[2] = 7;
            vpri[3] = 13;
            vpri[4] = 17;
            vpri[5] = 19;
            vpri[6] = 23;
            vpri[7] = 29;
            vpri[8] = 37;
            vpri[9] = 41;
            vpri[10] = 43;
            vpri[11] = 47;
            vpri[12] = 53;
            vpri[13] = 59;
            vpri[14] = 67;
            vpri[15] = 71;
            x = 0;
            y = 0;
            for (var i = 0; i < z; i++) {
                y = (myNit.substr(i, 1));
                x += (y * vpri[z - i]);
            }
            y = x % 11;
            return (y > 1) ? 11 - y : y;
        }

        $('#type_person').trigger('change');
        $('#tipo_documento').trigger('change');

        set_ubication();

        function set_ubication(){
          country = $('#country option:selected').data('code');
          state = "<?= $supplier->state ?>";
          set_city = "<?= $supplier->city ?>";
          $.ajax({
            url:"<?= admin_url() ?>customers/get_states",
            method : "POST",
            data : {
              "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
              country : country,
              state : state
            }
          })
          .done(function(data) {
            $('#state').html(data);
            $('#state').select2();
            state = $('#state option:selected').data('code');

            $.ajax({
              url:"<?= admin_url() ?>customers/get_cities",
              method : "POST",
              data : {
                "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
                state : state,
                city : set_city
              }
            }).done(function(data){
              $('#city').html(data);
              $('#city').select2('val', set_city);

            })
            .fail(function(data){ console.log(data.responseText); });

          }).fail(function(data){ console.log(data.respnoseText); });
        }

        $(document).on('change', '#default_rete_fuente_id', function() {
            var rete_fuente = $('#default_rete_fuente_id option:selected');
            var percentage = rete_fuente.data('percentage');
            var min_base = rete_fuente.data('minbase');
            var ledger = rete_fuente.data('account');
            $('.rete_fuente_percentage').val(percentage);
            $('.rete_fuente_minbase').val(min_base);
            $('.rete_fuente_ledger').val(ledger);
        });
        $(document).on('change', '#default_rete_iva_id', function() {
            var rete_iva = $('#default_rete_iva_id option:selected');
            var percentage = rete_iva.data('percentage');
            var min_base = rete_iva.data('minbase');
            var ledger = rete_iva.data('account');
            $('.rete_iva_percentage').val(percentage);
            $('.rete_iva_minbase').val(min_base);
            $('.rete_iva_ledger').val(ledger);
        });

        $('#city').on('change', function() {
            code = $('#city option:selected').data('code');
            $('#city_code').val(code);
        });
        $('#vat_no').on('change', function() {
            nit = $(this).val();
            if (site.settings.get_companies_check_digit == 1) {
                dvf = calcularDigitoVerificacion(nit);
                $('#digito_verificacion').val(dvf);
            }
        });

        $('#default_rete_fuente_id').trigger('change');
        $('#default_rete_iva_id').trigger('change');
        $('#default_rete_ica_id').trigger('change');
        $('#default_rete_other_id').trigger('change');
        $('#supplier_payment_type').on('change', function() {
            val = $(this).val();
            if (val == 0) {
                $('.credit_customer').css('display', '');
                $('#supplier_credit_limit').prop('required', true).prop('min', 1);
                $('#supplier_payment_term').prop('required', true).prop('min', 1);
            } else if (val == 1) {
                $('.credit_customer').css('display', 'none');
                $('#supplier_credit_limit').prop('required', false).prop('min', false);
                $('#supplier_payment_term').prop('required', false).prop('min', false);
            }
        });
        $('#supplier_payment_type').trigger('change');

        $('#supplier_type').on('change', function() {
            var st = $(this).val();
            if (st >= 5 && st <= 10) {
                $('input').prop('required', false);
                $('select').prop('required', false);
                $('input').removeClass('error');
                $('select').removeClass('error');
                $('#tipo_documento').prop('required', true);
                $('#type_person').prop('required', true);
                $('#vat_no').prop('required', true);
                $('#name').prop('required', true);
                $('#country').prop('required', true);
                $('#state').prop('required', true);
                $('#city').prop('required', true);
            } else {
                $('#type_person').trigger('change');
            }

            showHideGenerateSupportingDocumentContainer();
        });
        setTimeout(function() {
            $('#supplier_type').trigger('change');
        }, 850);

        $(document).on('change', '#tipo_regimen', function(){ showHideGenerateSupportingDocumentContainer(); });

        function showHideGenerateSupportingDocumentContainer() {
            var supplierType = $('#supplier_type').val();
            var typeRegime = $('#tipo_regimen').val();

            if (supplierType <= 4) {
                if (typeRegime != 1) {
                    // $('#generateSupportingDocumentContainer').fadeOut();
                    $('#generateSupportingDocument').iCheck('uncheck');
                } else {
                    // $('#generateSupportingDocumentContainer').fadeIn();
                }
            } else {
                // $('#generateSupportingDocumentContainer').fadeOut();
                $('#generateSupportingDocument').iCheck('uncheck');
            }
        }

        function load_types_customer_obligations() {
            var typesCustomerObligationsArray = [];

            <?php if (!empty($typesSupplierObligations)) { ?>
                <?php foreach ($typesSupplierObligations as $typeSupplierObligation) { ?>
                    typesCustomerObligationsArray.push('<?= $typeSupplierObligation->types_obligations_id; ?>');
                <?php } ?>
            <?php } ?>

            $('#types_obligations').select2({}).select2('val', typesCustomerObligationsArray);
        }
    </script>
    <?= $modal_js ?>