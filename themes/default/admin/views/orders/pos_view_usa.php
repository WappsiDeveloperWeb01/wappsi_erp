<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
    p {
        margin: 0 0 3px !important;
    }

    .table {
        margin-bottom: 0px !important;
    }
</style>
<!doctype html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?=lang('order_sale') . " " . lang("no") . " " . $inv->id;?></title>
        <base href="<?=base_url()?>"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            .text-center { text-align: center ; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
        </style>
    </head>

    <body>
    <div id="wrapper">
        <div id="receiptData">
            <div>
                
                <div style="text-align:center;">
                    <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                    <?php
                    echo "<p>" . $biller->address .
                    "<br>" . ucwords(mb_strtolower($biller->city . " " . $biller->postal_code)).
                    "<br>" . lang("tel") . ": " . $biller->phone.
                    "<br>" . $biller->email;
                    echo "<br>";
                    if ($this->pos_settings->cf_title1 != "" && $this->pos_settings->cf_value1 != "") {
                        echo $this->pos_settings->cf_title1 . ": " . $this->pos_settings->cf_value1 . "<br>";
                    }
                    if ($this->pos_settings->cf_title2 != "" && $this->pos_settings->cf_value2 != "") {
                        echo $this->pos_settings->cf_title2 . ": " . $this->pos_settings->cf_value2 . "<br>";
                    }
                    echo '</p>';
                    ?>
                    <?php if ($this->Settings->tipo_regimen): ?>
                        <p style="text-align:center;"><b><?= $tipo_regimen ?></b></p>
                    <?php endif ?>
                    <?php 
                    if ($this->Settings->url_web != "") {
                        echo $this->Settings->url_web."<br>";
                    } ?>
                </div>
                <?php
                if ($Settings->invoice_view == 1 || $Settings->indian_gst) {
                    ?>
                    <div class="col-sm-12 text-center">
                        <h4 style="font-weight:bold;"><?=  ($document_type ? $document_type->nombre : lang('sale_invoice'))." ".$inv->reference_no;?></h4>
                    </div>
                    <?php
                }
                echo "<p><b>" .lang("date") . ":</b> " . $this->sma->hrld($inv->date) . "<br>";
                if ($inv->payment_term > 0 && $inv->payment_status != 'paid') {
                    $fecha_exp = strtotime("+".$inv->payment_term." day", strtotime($inv->date));
                    $fecha_exp = $this->sma->hrld(date('d/m/Y H:i', $fecha_exp));
                    echo "<b>".lang("expiration_date")." :</b> ".$fecha_exp." <br>";
                }
                if ($seller) {
                    echo "<b>".lang("seller") . ":</b> " . ($seller->company != '-' ? $seller->company : $seller->name) . ($seller->phone != '-' ? ", ".$seller->phone : '') . "<br>";
                }
                if (!empty($inv->return_sale_ref)) {
                    echo '<p>'.lang("return_ref").': '.$inv->return_sale_ref;
                    if ($inv->return_id) {
                        echo ' <a data-target="#myModal2" data-toggle="modal" href="'.admin_url('sales/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                    } else {
                        echo '</p>';
                    }
                }
                // echo lang("sales_person") . ": " . $created_by->first_name." ".$created_by->last_name . "</p>";
                echo "<p>";
                echo "<b>".lang("customer") . ":</b> " . ($customer->company && $customer->company != '-' ? $customer->company : $customer->name). "<br>";
                if ($this->pos_settings->customer_details && $customer->vat_no != "222222222222") {
                    if ($address) {
                        echo "<b>".lang("tel") . ":</b> " . $address->phone . "<br>";
                        echo "<b>".lang("address") . ":</b> " . ucwords(mb_strtolower($address->direccion)) . "<br>";
                        echo "".ucwords(mb_strtolower($address->city . " " . $address->postal_code)) ."<br>";
                    } else {
                        echo lang("tel") . ": " . $customer->phone . "<br>";
                        echo lang("address") . ": " . ucwords(mb_strtolower($customer->address)) . "<br>";
                        echo ucwords(mb_strtolower($customer->city . " " . $customer->postal_code)) ."<br>";
                    }
                        echo $customer->email ."";
                }
                echo "</p>";
                ?>
                <p>
                <?php if (!empty($inv->sale_origin_reference_no)): ?>
                    <b><?= lang('reference_origin') ?> :</b> <?= $inv->sale_origin_reference_no?>
                <?php endif ?>
                </p>
                <?php if (isset($cost_center) && $cost_center): ?>
                    <div>
                        <label><?= lang('cost_center') ?></label>
                        <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                    </div>
                <?php endif ?>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 60%"><?= lang('product') ?></th>
                            <th style="text-align: center;">U.M.</th>
                            <th style="text-align: center;"><?= lang('price') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $r = 1; $category = 0;
                        $brand = 0;
                        $tax_summary = array();
                        $ccategories = [];
                        $inv_total_before_promo = 0;
                        // $this->sma->print_arrays($rows);
                        foreach ($rows as $row) {


                            echo '<tr>
                                    <td class="no-border" style="padding-bottom: 0px !important;">
                                        '.($row->product_name) .
                                        ($row->variant && (($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) || !$document_type_invoice_format) ? ' (' . $row->variant . ($row->variant_code ? ' - '.$row->variant_code : '') . ')' : '') .
                                        ($show_product_preferences == 1 && $row->preferences ? ' (' . $this->sma->print_preference_selection($row->preferences) . ')' : '') .
                                        ($this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : '') .
                                        '
                                    </td>
                                    <td>'.$row->product_unit_code.'</td>
                                    <td><span class="pull-right">' . $this->sma->formatValue($value_decimals, $row->subtotal) . '</span></td>
                                </tr>';
                            if (!empty($row->second_name)) {
                                echo '<tr>
                                        <td colspan="3" class="no-border">'.$row->second_name.'</td>
                                    </tr>';
                            }
                            echo '<tr>
                                    <td class="no-border border-bottom" style="padding-top: 0px !important;"  colspan="2">' . $this->sma->formatQuantity($row->quantity, $qty_decimals) . ' x '.$this->sma->formatValue($value_decimals, $row->unit_price).' '.($this->Settings->ipoconsumo && $row->tax_2 > 0 ? '+ '.sprintf(lang('ipoconsumo_additional_description_name'), $this->sma->formatMoney($row->item_tax_2)) : '').'
                                    </td>
                                    <td class="no-border border-bottom text-right" style="padding-top: 0px !important;" colspan="2">' . (!isset($document_type_invoice_format->view_item_tax) || (isset($document_type_invoice_format->view_item_tax) && $document_type_invoice_format->view_item_tax && $row->tax_code) ? '*'.$row->tax_code : '') . '
                                    </td>
                                </tr>';

                            $r++;
                        }

                        ?>
                    </tbody>
                    <tfoot>
                        <?php
                            $rete_total = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total;
                        ?>
                        <?php if ($rete_total != 0 || $inv->order_discount != 0): ?>
                            <tr>
                                <th colspan="2"><?=lang("total_before_discount");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax));?></th>
                            </tr>
                        <?php endif ?>
                        <?php
                        if ($inv->order_tax != 0) {
                            echo '<tr><th  colspan="2">' . lang("tax") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</th></tr>';
                        }

                        if ($rete_total != 0) {
                            echo '<tr>
                                    <th  colspan="2">'.lang('retention').'</th>
                                    <th class="text-right"> -' . $this->sma->formatValue($value_decimals, $rete_total) . '</th>
                                  </tr>';
                        }

                        if ($inv->order_discount != 0) {
                            echo '<tr><th  colspan="2">' . lang("order_discount") . '</th><th class="text-right"> -' . $this->sma->formatValue($value_decimals, $inv->order_discount) . '</th></tr>';
                        }

                        if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 1) {
                            echo '<tr><th  colspan="2">' . lang("shipping") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $inv->shipping) . '</th></tr>';
                        }

                        if ($this->pos_settings->rounding || $inv->rounding != 0) {
                            ?>
                            <tr>
                                <th colspan="2"><?=lang("rounding");?></th>
                                <th class="text-right"><?= $this->sma->formatValue($value_decimals, $inv->rounding);?></th>
                            </tr>
                            <tr>
                                <th colspan="2"><?=lang("total_to_pay");?></th> 
                                <th class="text-right" style="font-size: 130%;"><?=$this->sma->formatValue($value_decimals, $return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding)  - $rete_total );?></th>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <th colspan="2"><?=lang("sub_total");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, $inv->grand_total - $inv->total_tax);?></th>
                            </tr>
                            <tr>
                                <th colspan="2"><?=lang("tax");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, $inv->total_tax);?></th>
                            </tr>
                            <tr>
                                <th colspan="2"><?= ($inv->paid < ($inv->grand_total + $inv->rounding)) ? lang("total_to_pay") : lang("paid_amount");?></th>
                                <th class="text-right" style="font-size: 130%;"><?=$this->sma->formatValue($value_decimals, $return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total  - $rete_total );?></th>
                            </tr>
                            <?php
                        } ?>
                    </tfoot>
                </table>

                <?= $inv->note ? '<p><strong>' . lang('sale_note') . ':</strong>  ' . $this->sma->decode_html($inv->note) . '</p>' : ''; ?>
                <?= $inv->staff_note ? '<p><strong>' . lang('staff_note') . ':</strong> ' . $this->sma->decode_html($inv->staff_note) . '</p>' : ''; ?>


                <p class="text-center"><?= $this->sma->decode_html($invoice_footer ? $invoice_footer : '') ?></p>

                <?php if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 0): ?>
                    <hr>
                    <h4><?= sprintf(lang('shipping_not_include_in_grand_total'), $this->sma->formatValue($value_decimals, $inv->shipping)) ?></h4>
                <?php endif ?>

                <p class="text-center"><?= lang('printing_software_wappsi_info') ?></p>
                <p class="text-center"><?= lang('created_by')." : ".ucwords(mb_strtolower($created_by->first_name." ".$created_by->last_name)) ?></p>
            </div>

            <button onclick="window.print();" class="btn btn-block btn-primary btn_print_html no-print"><?= lang("print") ?></button>

        </div>
    </div>

        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

        <script type="text/javascript">
            $(window).ready(function(){
                setTimeout(function() {
                    window.print();
                }, 350);
            });
        </script>

</body>
</html>