<?php defined('BASEPATH') or exit('No direct script access allowed');

class Debit_notes extends MY_Controller {
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Supplier || $this->Customer) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('returns', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('debit_notes_model');
        $this->load->admin_model('documentsTypes_model');
        $this->load->admin_model('pos_model');
        $this->load->admin_model('electronic_billing_model');
        if (!$this->session->userdata('register_id')) {
            if ($register = $this->pos_model->registerData($this->session->userdata('user_id'))) {
                $register_data = array('register_id' => $register->id, 'cash_in_hand' => $register->cash_in_hand, 'register_open_time' => $register->date);
                $this->session->set_userdata($register_data);
            }
        }
    }

    public function add($invoice_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->session->userdata('cash_in_hand')) {
            $this->session->set_flashdata('error', lang("no_data_pos_register"));
            admin_redirect('pos/index');
        }

        if (!empty($invoice_id)){
            $reference_invoice = $this->debit_notes_model->get_sale($invoice_id);

            if (!empty($reference_invoice->debit_note_id)) {
                $this->session->set_flashdata('error', "Ya existe una Nota débito asociada para la factura ". $reference_invoice->reference_no);
                admin_redirect('sales/fe_index');
            }

            $this->data["reference_invoice"] = $reference_invoice;
        }

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['tax_rates'] = $this->site->getAllTaxRates();
        $this->data["debit_note_concepts"] = $this->debit_notes_model->get_debit_note_concepts();

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->page_construct('debit_note/add', ['page_title' => lang('debit_note_label_menu')], $this->data);
    }

    public function sale_reference_suggestions()
    {
        $term = $this->input->get("term");
        $module = $this->input->get("module");
        $customer_id = $this->input->get("customer_id");
        $is_electronic_document = $this->input->get("is_electronic_document");

        $result = $this->debit_notes_model->get_sale_reference($term, $customer_id, $is_electronic_document, $module);

        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function get_sale_reference()
    {
        $term = $this->input->get("term");
        $module = $this->input->get("module");
        $customer_id = $this->input->get("customer_id");
        $is_electronic_document = $this->input->get("is_electronic_document");

        $result = $this->debit_notes_model->get_sale_reference_by_id($term, $customer_id, $is_electronic_document, $module);

        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function get_sale_data()
    {
        $id = $this->input->post("id");

        $sale_data = $this->debit_notes_model->get_sale($id);

        if (empty($sale_data)) {
            echo json_encode([
                "status" => FALSE,
                "error_message" => "No existe una venta con la referencia insertada"
            ]);
        } else {
            echo json_encode([
                "status" => TRUE,
                "sale_data" => $sale_data
            ]);
        }
    }

    public function save()
    {
        $debit_note_data = $this->build_debit_note_data();
        $debit_note_item_data = $this->build_debit_note_item_data();
        $payment_data = $this->build_payment_data();
        $resolution_data = $this->site->getDocumentTypeById($this->input->post('document_type'));

        if (!$this->session->userdata('detal_post_processing')) {
            $this->session->set_userdata('detal_post_processing', 1);
        } else {
            $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
            if ($resolution_data->factura_electronica == YES) {
                admin_redirect("sales/fe_index");
            } else {
                admin_redirect("sales/index");
            }
        }

        if (empty($debit_note_data)) {
            $this->session->set_flashdata("error", "No fue posible generar los datos para el encabezado de la nota débito.");
            if ($resolution_data->factura_electronica == YES) {
                admin_redirect("sales/fe_index");
            } else {
                admin_redirect("sales/index");
            }
        }

        if (empty($debit_note_item_data)) {
            $this->session->set_flashdata("error", "No fue posible generar los datos para el detalle de la nota débito.");
            if ($resolution_data->factura_electronica == YES) {
                admin_redirect("sales/fe_index");
            } else {
                admin_redirect("sales/index");
            }
        }

        $credit_note_created = $this->debit_notes_model->save_debit_note($debit_note_data, $debit_note_item_data, $payment_data);

        if ($credit_note_created !== FALSE) {
            if ($resolution_data->factura_electronica == YES) {
                $debit_note_data = $this->site->getSaleByID($credit_note_created);
                $document_electronic_data = [
                    'sale_id' => $debit_note_data->id,
                    'biller_id' => $debit_note_data->biller_id,
                    'customer_id' => $debit_note_data->customer_id,
                    'reference' => $debit_note_data->reference_no,
                    'attachment' => $debit_note_data->attachment

                ];

                if ($resolution_data->factura_contingencia == NOT) {
                    $this->create_document_electronic($document_electronic_data);
                }
            }

            $this->session->set_flashdata("message", "La nota débito fue creada exitosamente.");
        } else {
            $this->session->set_flashdata("error", "No fue posible crear la nota débito.");
        }

        if ($this->session->userdata('detal_post_processing')) { $this->session->unset_userdata('detal_post_processing'); }

        if ($resolution_data->factura_electronica == YES) {
            if ($resolution_data->module == 26) {
                admin_redirect("pos/fe_index");
            } else {
                admin_redirect("sales/fe_index");
            }
        } else {
            if ($resolution_data->module == 26) {
                admin_redirect("pos/sales");
            } else {
                admin_redirect("sales/index");
            }
        }
    }

    private function build_debit_note_data()
    {
        $document_type_id = $this->input->post("document_type");
        $withholdings_data = json_decode($this->input->post("withholdings_data"));
        $concept_value = $this->input->post("concept_value");

        if (!empty($document_type)) {
            $consecutive = ($document_type->consecutive + 1);
        }

        if ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')) {
            $biller_id = $this->input->post("branch_id");
        } else {
            $biller_id = $this->input->post("biller");
        }

        $debit_note_data = [
            "date" => $this->input->post("date") ." ". date("H:i:s"),
            "customer_id" => $this->input->post("customer_id"),
            "customer" => $this->input->post("customer_name"),
            "biller_id" => $biller_id,
            "biller" => $this->input->post("customer_branch"),
            "warehouse_id"=>$this->input->post("warehouse_id"),
            "note" => $this->input->post("note"),
            "staff_note" => $this->input->post("staff_note"),
            "total" => ($concept_value),
            "product_tax" => $this->input->post("concept_tax"),
            "total_tax" => $this->input->post("concept_tax"),
            "grand_total" => $this->input->post("total_value_concept"),
            "sale_status" => "completed",
            "created_by" => $this->session->userdata("user_id"),
            "total_items" => 1,
            "reference_invoice_id" => $this->input->post("reference_no"),
            "reference_debit_note" => $this->input->post("invoice_reference"),
            "address_id" => $this->input->post("addresses_id"),
            "seller_id" => $this->input->post("seller_id"),
            "hash" => hash('sha256', microtime() . mt_rand()),
            "document_type_id" => $document_type_id,
            "sale_currency" => $this->Settings->default_currency,
            "fe_debit_credit_note_concept_dian_code" => $this->input->post("concept_dian_code")
        ];

        $debit_note_data = $this->calculateSelftRetention($debit_note_data);

        if ($this->input->post("module") == FACTURA_DETAL || $this->input->post("module") == NOTA_CREDITO_DETAL || $this->input->post("module") == NOTA_DEBITO_DETAL) {
            $debit_note_data["pos"] = NOT;
        } else if ($this->input->post("module") == FACTURA_POS || $this->input->post("module") == NOTA_CREDITO_POS || $this->input->post("module") == NOTA_DEBITO_POS) {
            $debit_note_data["pos"] = YES;
        }

        if ($withholdings_data->retention_total_fuente > 0) {
            $debit_note_data["rete_fuente_id"] = $withholdings_data->retention_id_fuente;
            $debit_note_data["rete_fuente_percentage"] = $withholdings_data->retention_percentage_fuente;
            $debit_note_data["rete_fuente_total"] = $withholdings_data->retention_total_fuente;
            $debit_note_data["rete_fuente_base"] = $withholdings_data->retention_base_fuente;
            $debit_note_data["rete_fuente_account"] = $withholdings_data->retention_account_fuente;
        }

        if ($withholdings_data->retention_total_iva > 0) {
            $debit_note_data["rete_iva_id"] = $withholdings_data->retention_id_iva;
            $debit_note_data["rete_iva_percentage"] = $withholdings_data->retention_percentage_iva;
            $debit_note_data["rete_iva_total"] = $withholdings_data->retention_total_iva;
            $debit_note_data["rete_iva_base"] = $withholdings_data->retention_base_iva;
            $debit_note_data["rete_iva_account"] = $withholdings_data->retention_account_iva;
        }

        if ($withholdings_data->retention_total_ica > 0) {
            $debit_note_data["rete_ica_id"] = $withholdings_data->retention_id_ica;
            $debit_note_data["rete_ica_percentage"] = $withholdings_data->retention_percentage_ica;
            $debit_note_data["rete_ica_total"] = $withholdings_data->retention_total_ica;
            $debit_note_data["rete_ica_base"] = $withholdings_data->retention_base_ica;
            $debit_note_data["rete_ica_account"] = $withholdings_data->retention_account_ica;
        }

        if ($withholdings_data->retention_total_other > 0) {
            $debit_note_data["rete_other_id"] = $withholdings_data->retention_id_other;
            $debit_note_data["rete_other_percentage"] = $withholdings_data->retention_percentage_other;
            $debit_note_data["rete_other_total"] = $withholdings_data->retention_total_other;
            $debit_note_data["rete_other_base"] = $withholdings_data->retention_base_other;
            $debit_note_data["rete_other_account"] = $withholdings_data->retention_account_other;
        }

        // Condición que valida el método de pago.
        if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
            $total_retenciones = 0;
            $total_retenciones += ($withholdings_data->retention_total_fuente > 0 ? $withholdings_data->retention_total_fuente : 0);
            $total_retenciones += ($withholdings_data->retention_total_iva > 0 ? $withholdings_data->retention_total_iva : 0);
            $total_retenciones += ($withholdings_data->retention_total_ica > 0 ? $withholdings_data->retention_total_ica : 0);
            $total_retenciones += ($withholdings_data->retention_total_other > 0 ? $withholdings_data->retention_total_other : 0);

            if (($this->input->post('amount-paid') + $total_retenciones) < $this->input->post("total_value_concept")) {
                $debit_note_data['payment_method_fe'] = CREDIT;
                $debit_note_data["payment_status"] = "partial";
                $debit_note_data["payment_term"] = $this->input->post("payment_term");
                $debit_note_data["due_date"] = $this->calculate_due_date($this->input->post("date"), $this->input->post("payment_term"));
            } else {
                $debit_note_data['payment_method_fe'] = CASH;
                $debit_note_data["payment_status"] = "paid";
            }
        } else {
            $debit_note_data['payment_method_fe'] = CREDIT;
            $debit_note_data["payment_term"] = $this->input->post("payment_term");
            $debit_note_data["due_date"] = $this->calculate_due_date($this->input->post("date"), $this->input->post("payment_term"));
        }

        // Condición que valida el medio del pago
        if ($this->input->post('mean_payment_code_fe')) {
            $debit_note_data['payment_mean_fe'] = $this->input->post('mean_payment_code_fe');
        } else {
            $debit_note_data['payment_mean_fe'] = 'ZZZ';
            $debit_note_data["payment_status"] = "due";
        }

        return $debit_note_data;
    }

    private function calculateSelftRetention($data)
    {
        $selfRetentionAmount = 0;

        if (!empty($this->Settings->self_withholding_percentage)) {
            $concept = $this->debit_notes_model->getDebitNoteConceptbyId($this->input->post("concept_id"));
            if (!empty($concept) && $concept->applied_selfretention == YES) {
                $selfRetentionAmount = (abs($data['total']) * $this->Settings->self_withholding_percentage) / 100;
            }
        }

        $data["self_withholding_amount"] = $selfRetentionAmount;
        $data["self_withholding_percentage"] = $this->Settings->self_withholding_percentage;

        return $data;
    }

    private function calculate_due_date($date, $payment_term)
    {
        return date("Y-m-d", strtotime($date ."+ ". $payment_term ." days"));
    }

    private function build_debit_note_item_data()
    {
        $debit_note_item_data = [
            "product_id" => 999999999,
            "product_code" => $this->input->post("concept_id"),
            "product_name" => $this->input->post("concept_name"),
            "product_type" => "standar",
            "net_unit_price" => $this->sma->formatDecimal($this->input->post("concept_value")),
            "unit_price" => $this->sma->formatDecimal($this->input->post("concept_value") + $this->input->post("concept_tax")),
            "quantity" => $this->sma->formatDecimal(1),
            "item_tax" => $this->sma->formatDecimal($this->input->post("concept_tax")),
            "tax_rate_id" => $this->input->post("concept_tax_rate_id"),
            "tax" => $this->sma->formatDecimal($this->input->post("concept_tax_rate"), 0). "%",
            "subtotal" => ($this->sma->formatDecimal(($this->input->post("concept_value") * 1) + $this->input->post("concept_tax"))),
            "real_unit_price" => $this->sma->formatDecimal($this->input->post("concept_value") + $this->input->post("concept_tax")),
            "unit_quantity" => $this->sma->formatDecimal(1)
        ];

        return $debit_note_item_data;
    }

    private function build_payment_data()
    {
        $payment_data = [];
        $pay_ref = $this->input->post('payment_reference_no') ? $this->input->post('payment_reference_no') : $this->site->getReference('pay');

        $document_type_id = $this->input->post("document_type");
        $document_type = $this->documentsTypes_model->getDocumentType($document_type_id);

        if ($this->input->post("withholdings") > 0) {
            $payment_data[] = [
                'date' => $this->input->post("date") ." ". date("H:i:s"),
                'amount' => ($this->input->post("withholdings")),
                'reference_no' => 'retencion',
                'paid_by' => 'retencion',
                'created_by' => $this->session->userdata('user_id'),
                'type' => 'received',
                'note' => 'Retenciones'
            ];
        }

        if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
            $payment_data[] = [
                "date" => $this->input->post("date") ." ". date("H:i:s"),
                "reference_no" => $this->input->post('payment_reference_no'),
                "paid_by" => $this->input->post("paid_by"),
                "amount" => ($this->input->post("total_value_concept")),
                "created_by" => $this->session->userdata("user_id"),
                "type" => "received",
                "mean_payment_code_fe" => ($this->input->post("mean_payment_code_fe") != '') ? $this->input->post("mean_payment_code_fe") : "ZZZ"
            ];
        }

        return $payment_data;
    }

    private function create_document_electronic($document_electronic_data)
    {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            return true;
        }

        $document_electronic_data = (object) $document_electronic_data;
        $filename = $this->site->getFilename($document_electronic_data->sale_id);
        $technologyProvider = $this->Settings->fe_technology_provider;
        $created_file = $this->electronic_billing_model->send_document_electronic($document_electronic_data);

        if ($created_file->response == 0) {
            $this->session->set_flashdata('error', $created_file->message);
        } else {
            $email_delivery_response = '';

            if ($technologyProvider == CADENA || $technologyProvider == BPM) {
                $this->download_fe_xml($document_electronic_data->sale_id, TRUE);
                $this->download_fe_pdf($document_electronic_data->sale_id, TRUE);

                if (file_exists('files/electronic_billing/'.$document_electronic_data->reference.'.xml') && file_exists('files/electronic_billing/'.$document_electronic_data->reference.'.pdf')) {
                    $zip = new ZipArchive();

                    $zip->open('files/electronic_billing/' . $filename . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
                    $zip->addFile("files/electronic_billing/" . $filename . '.xml', $filename . ".xml");
                    $zip->addFile("files/electronic_billing/" . $filename . '.pdf', $filename . ".pdf");

                    if ($document_electronic_data->attachment != '') {
                        $zip->addFile("files/" . $document_electronic_data->attachment, $document_electronic_data->attachment);
                    }

                    $zip->close();

                    unlink('files/electronic_billing/' . $filename . '.xml');
                    unlink('files/electronic_billing/' . $filename . '.pdf');

                    if ($_SERVER['SERVER_NAME'] != 'localhost') {
                        $email_delivery_response = $this->electronic_billing_model->receipt_delivery($document_electronic_data, $filename);
                    }
                }
            }

            $this->session->set_flashdata('message', 'Documento electrónico creado correctamente <br>'. $email_delivery_response);
        }
    }

    public function download_fe_xml($sale_id, $internal_download = FALSE)
    {
        $sale = $this->site->getSaleByID($sale_id);
        $filename = $this->site->getFilename($sale_id);
        $technologyProvider = $this->Settings->fe_technology_provider;

        if ($technologyProvider == CADENA || $technologyProvider == BPM) {
            $xml_file = base64_decode($sale->fe_xml);

            if ($internal_download === TRUE) {
                $xml = new DOMDocument("1.0", "ISO-8859-15");
                $xml->loadXML($xml_file);

                $xml->save('files/electronic_billing/'.$filename.'.xml');
            } else {

                header('Content-Type: application/xml;');
                header('Content-Disposition: attachment; filename="'.$filename.'.xml"');
                $xml = new DOMDocument("1.0", "ISO-8859-15");

                $xml->loadXML($xml_file);
                echo $xml->saveXML();
            }
        }
    }

    public function download_fe_pdf($sale_id, $internal_download = FALSE)
    {
        $technologyProvider = $this->Settings->fe_technology_provider;
        if ($technologyProvider == CADENA || $technologyProvider == BPM) {
            $this->saleView($sale_id, $internal_download);
        }
    }
}
