<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">

    var edit = false;
    localStorage.removeItem('gstbiller');
    localStorage.removeItem('gstcustomer');
    localStorage.removeItem('gstroom');
    localStorage.removeItem('gstnights_no');
    localStorage.removeItem('gstguests_no');
    localStorage.removeItem('gsttotal');
    localStorage.removeItem('rows');

    
    id_document_types = JSON.parse('<?= json_encode($id_document_types) ?>');
    <?php 
    if ($this->session->userdata('remove_gstls')) {?>
        if (localStorage.getItem('gstbiller')) {
            localStorage.removeItem('gstbiller');
        }
        if (localStorage.getItem('gstcustomer')) {
            localStorage.removeItem('gstcustomer');
        }
        if (localStorage.getItem('gstroom')) {
            localStorage.removeItem('gstroom');
        }
        if (localStorage.getItem('gstnights_no')) {
            localStorage.removeItem('gstnights_no');
        }
    <?php $this->sma->unset_data('remove_gstls'); } ?>

    $(document).ready(function () {
        if (localStorage.getItem('remove_gstls')) {
            if (localStorage.getItem('gstbiller')) {
                localStorage.removeItem('gstbiller');
            }
            if (localStorage.getItem('gstcustomer')) {
                localStorage.removeItem('gstcustomer');
            }
            if (localStorage.getItem('gstroom')) {
                localStorage.removeItem('gstroom');
            }
            if (localStorage.getItem('gstnights_no')) {
                localStorage.removeItem('gstnights_no');
            }
            localStorage.removeItem('remove_gstls');
        }
        $(document).on('change', '#gstbiller', function (e) {
            localStorage.setItem('gstbiller', $('#gstbiller option:selected').val());
            bdata = billers_data[$('#gstbiller option:selected').val()];

            biller = $('#gstbiller');
            $.ajax({
              url:'<?= admin_url("billers/getBillersDocumentTypes/47/") ?>'+$('#gstbiller').val(),
              type:'get',
              dataType:'JSON'
            }).done(function(data){
              response = data;
              $('#document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                                "showDuration": "500",
                                "hideDuration": "1000",
                                "timeOut": "6000",
                                "extendedTimeOut": "1000",
                            });
                }
              if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                localStorage.setItem('locked_for_biller_resolution', 1);
                verify_locked_submit();
              } else {
                if (localStorage.getItem('locked_for_biller_resolution')) {
                    localStorage.removeItem('locked_for_biller_resolution');
                }
                verify_locked_submit();
              }
                $('#document_type_id').trigger('change');
            });
            $.ajax({
              url:'<?= admin_url("billers/getBillersDocumentTypes/8/") ?>'+$('#gstbiller').val(),
              type:'get',
              dataType:'JSON'
            }).done(function(data){
              response = data;
              $('#order_sale_document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                                "showDuration": "500",
                                "hideDuration": "1000",
                                "timeOut": "6000",
                                "extendedTimeOut": "1000",
                            });
                }
              if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                localStorage.setItem('locked_for_biller_resolution', 1);
                verify_locked_submit();
              } else {
                if (localStorage.getItem('locked_for_biller_resolution')) {
                    localStorage.removeItem('locked_for_biller_resolution');
                }
                verify_locked_submit();
              }
                $('#order_sale_document_type_id').trigger('change');
                
            });

        });//fin change gstbiller
        if (gstbiller = localStorage.getItem('gstbiller')) {
            $('#gstbiller').val(gstbiller);
        }

        $("#gstdate").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            startDate: min_input_date,
            endDate: max_input_date,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', (localStorage.getItem('gstdate') ? localStorage.getItem('gstdate') : new Date()));

        $(document).on('change', '#gstdate', function (e) {
            localStorage.setItem('gstdate', $(this).val());
        });
    });
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('add_guest_register'); ?></h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="no-print notifications_container">
                                <?php if ($error) { ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?= is_array($error) ? print_r($error, true) : $error;?>
                                    </div>
                                <?php } ?>

                                <?php if ($warning) { ?>
                                    <div class="alert alert-warning alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?= is_array($warning) ? print_r($warning, true) : $warning;?>
                                    </div>
                                <?php } ?>
                            </div>

                            <?= admin_form_open_multipart("hotel/add_guest_register", ['id' => 'add_guest_register_form']); ?>

                            <div class="row">
                                <div class="col-lg-12 resAlert"></div>
                                <div class="col-lg-12">
                                    <?php if ($Owner || $Admin) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("date", "gstdate"); ?>
                                                <input type="datetime" name="date" class="form-control" id="gstdate" required="required" value="<?= isset($_POST['date']) ? $_POST['date'] : date('Y-m-d H:i:s') ?>">
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                        $bl[""] = "";
                                        $bldata = [];
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                            $bldata[$biller->id] = $biller;
                                        }
                                    ?>
                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "gstbiller"); ?>
                                                <select name="biller" class="form-control" id="gstbiller" required="required">
                                                    <?php foreach ($billers as $biller): ?>
                                                      <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>"><?= $biller->company ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-md-4" style="display: none;">
                                            <div class="form-group">
                                                <?= lang("biller", "gstbiller"); ?>
                                                <select name="biller" class="form-control" id="gstbiller">
                                                    <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                        $biller = $bldata[$this->session->userdata('biller_id')];
                                                        ?>
                                                        <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>" selected><?= $biller->company ?></option>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("add_reference_no_label", "gstref"); ?>
                                            <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?= lang('hotel_room', 'gstroom') ?>
                                                <select name="room" id="gstroom" class="form-control">
                                                    <?php if ($rooms): ?>
                                                        <option value="">Seleccione...</option>
                                                        <?php foreach ($rooms as $room): ?>
                                                            <option value="<?= $room->id ?>" data-maxoccupancy="<?= $room->max_occupancy ?>" data-price="<?= $room->price ?>"><?= $room->room_no." - ".$room->room_name." (".$room->max_occupancy.")" ?></option>
                                                        <?php endforeach ?>
                                                    <?php else: ?>
                                                        <option value="">Sin habitaciones</option>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <?= lang('guests_no', 'gstguests_no') ?>
                                                <select name="guests_no" id="gstguests_no" class="form-control">
                                                    <option value="">Seleccione Habitación...</option>
                                                </select>
                                                <!-- <input type="number" name="guests_no" id="gstguests_no" min="1" class="form-control only_number" readonly> -->
                                            </div>
                                            <div class="col-md-4">
                                                <?= lang('nights_no', 'gstnights_no') ?>
                                                <input type="number" name="nights_no" id="gstnights_no" min="1" class="form-control only_number">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?= lang('total', 'gsttotal') ?>
                                                <input type="number" name="total" id="gsttotal" class="form-control only_number">
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("customer", "gstcustomer"); ?>
                                                    <div class="input-group">
                                                        <?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="gstcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="max-width:325px;"'); ?>
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px; border-left: 0;">
                                                            <a href="#" id="toogle-customer-read-attr" class="external">
                                                                <i class="fa fa-pencil" id="addIcon" style="font-size: 1.2em;"></i>
                                                            </a>
                                                        </div>
                                                        <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
                                                            <a id="view-customer">
                                                                <i class="fa fa-eye" id="addIcon" style="font-size: 1.2em;"></i>
                                                            </a>
                                                        </div>
                                                        <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                            <a href="<?= admin_url('customers/add'); ?>" id="add-customer"class="external" data-toggle="modal" data-target="#myModal">
                                                                <i class="fa fa-plus-circle" id="addIcon"  style="font-size: 1.2em;"></i>
                                                            </a>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <em class="text-danger txt-error" style="display: none;"></em>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("customer_branch", "gstcustomerbranch"); ?>
                                                    <?php
                                                    echo form_dropdown('address_id', array('0' => lang("select") . ' ' . lang("customer")), (isset($_POST['address_id']) ? $_POST['address_id'] : ''), 'id="gstcustomerbranch" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 div_order_sale_document_type_id">
                                                <div class="form-group">
                                                    <?= lang("order_sale_document_type_id", "order_sale_document_type_id"); ?>
                                                    <select name="order_sale_document_type_id" class="form-control" id="order_sale_document_type_id" required="required"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 customer_info" style="display:none;">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre</th>
                                                            <th>Dirección</th>
                                                            <th>Teléfono</th>
                                                            <th>Ciudad</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-center customer_name"></td>
                                                            <td class="text-center customer_address"></td>
                                                            <td class="text-center customer_phone"></td>
                                                            <td class="text-center customer_city"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("guests"); ?> *</label>

                                            <div class="controls table-controls">
                                                <table id="gstTable" class="table items  table-bordered table-condensed table-hover sortable_table">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang('document_type'); ?></th>
                                                        <th><?= lang('vat_no'); ?></th>
                                                        <th><?= lang('names'); ?></th>
                                                        <th><?= lang('surnames'); ?></th>
                                                        <th><?= lang('nationality'); ?></th>
                                                        <th><?= lang('phone'); ?></th>
                                                        <th><?= lang('email'); ?></th>
                                                        <th><?= lang('birth_date'); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary" id="add_guest_register" style="padding: 6px 15px; margin:15px 0;"><?= lang('submit') ?></button>
                                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>