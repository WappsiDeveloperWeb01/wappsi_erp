<?php

/***********

FORMATO PARA VISUALIZACIÓN DE RECIBOS DE CAJA MÚLTIPLES

************/

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Comprobande de anticipo'));
        //izquierda
        $this->RoundedRect(13, 7, 117, 26, 3, '1234', '');

        if (file_exists('assets/uploads/logos/'.$this->biller->logo)) {
            $imagen = getimagesize(base_url().'assets/uploads/logos/'.$this->biller->logo);
            $ancho = $imagen[0] / 3.77952;
            $alto = $imagen[1] / 3.77952;
            $altura = 14.5;
        }

        $this->Image((isset($this->biller->logo) ? base_url().'assets/uploads/logos/'.$this->biller->logo : ''),16,9,21.5);
        $cx = 45;
        $cy = 11;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(mb_strtoupper($this->biller->company != "-" ? $this->biller->company : $this->biller->name)),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->digito_verificacion != "" ? '-'.$this->biller->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address." - ".ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 26, 3, '1234', '');

        $cx = 132;

        //derecha
        $cy = 9;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Comprobante de anticipo'),'',1,'C');
        $cy +=6;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode('N°. '.$this->deposit->reference_no),'',1,'C');
        $cy +=10;
        $this->setXY($cx, $cy);
        $this->Cell(77, 3 , $this->sma->utf8Decode('Valor '.$this->sma->formatMoney($this->deposit->amount)),'',1,'C');

        //izquierda

        $cx = 13;
        $cy = 35;

        $this->setXY($cx, $cy);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Cliente : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->customer->company ? $this->customer->company : $this->customer->name),'',1,'L');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Nit / CC :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->customer->vat_no),'',1,'L');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Fecha :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->deposit->date),'',1,'L');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Valor aplicado :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->sma->formatMoney($this->deposit->amount - $this->deposit->balance)),'',1,'L');

        //derecha

        $cx = 132;
        $cy = 35;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(28.1, 4, $this->sma->utf8Decode('Fecha : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(48.9, 4, $this->sma->utf8Decode($this->deposit->date),'',1,'L');
        $cx = 132;
        $cy += 4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(28.1, 4, $this->sma->utf8Decode('Forma de pago :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(48.9, 4, $this->sma->utf8Decode($this->paid_by),'',1,'L');
        $cx = 132;
        $cy += 4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(28.1, 4, $this->sma->utf8Decode('Balance :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(48.9, 4, $this->sma->utf8Decode($this->sma->formatMoney($this->deposit->balance)),'',1,'L');

        $this->ln(6);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
    }

    function Footer()
    {
        // Print centered page number
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 805) {
            $text = substr($text, 0, 800);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->customer = $customer;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->document_type = $document_type;
$pdf->paid_by = $paid_by;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->Settings = $this->Settings;
$pdf->deposit = $deposit;
$pdf->sma = $this->sma;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}
$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();

$maximo_footer = 235;

if ($payments) {
    $concepto = false;
    foreach ($payments as $key_pmnt => $payment) {
        if ($payment->sale_id == NULL) {
            if (!$concepto || ($concepto && $concepto->reference_no != $payment->reference_no)) {
                if($concepto && $concepto->reference_no != $payment->reference_no){
                    $concepto->sale_reference_no = 'Otros conceptos';
                    $payments[] = $concepto;
                    $concepto = false;
                }
                $concepto = $payment;
                unset($payments[$key_pmnt]);
            } else if ($concepto && $concepto->reference_no == $payment->reference_no) {
                $concepto->amount += $payment->amount;
                unset($payments[$key_pmnt]);
            }
        }
    }
    if ($concepto) {
        $concepto->sale_reference_no = 'Otros conceptos';
        $payments[] = $concepto;
        $concepto = false;
    }
}

if ($payments) {
    $pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
    $pdf->Cell(196, 5, $this->sma->utf8Decode('Aplicación detallada del anticipo'),'',1,'L');
    $pdf->SetFont('Arial','B',$fuente+1);
    $pdf->Cell(49, 5, $this->sma->utf8Decode('Tipo de documento'),'B',0,'C');
    $pdf->Cell(49, 5, $this->sma->utf8Decode('Tipo de documento de venta'),'B',0,'C');
    $pdf->Cell(49, 5, $this->sma->utf8Decode('Fecha'),'B',0,'C');
    $pdf->Cell(49, 5, $this->sma->utf8Decode('Valor'),'B',1,'C');
    $pdf->SetFont('Arial','',$fuente+1);
    foreach ($payments as $pmnt) {
        $pdf->Cell(49, 5, $this->sma->utf8Decode($pmnt->reference_no),'B',0,'C');
        $pdf->Cell(49, 5, $this->sma->utf8Decode($pmnt->sale_reference_no),'B',0,'C');
        $pdf->Cell(49, 5, $this->sma->utf8Decode($pmnt->date),'B',0,'R');
        $pdf->Cell(49, 5, $this->sma->utf8Decode($this->sma->formatMoney($pmnt->amount - $pmnt->rete_fuente_total - $pmnt->rete_iva_total - $pmnt->rete_ica_total - $pmnt->rete_other_total)),'B',1,'R');
    }
}

$pdf->ln(5);

$pdf->MultiCell(196, 5 , $this->sma->utf8Decode('Nota : '.$deposit->note),'','L');

$pdf->ln(5);

$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Elaborado'),'T',0,'C');

$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Aprobado'),'T',0,'C');

$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Contabilizado'),'T',0,'C');

$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx, $cy);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Firma y sello recibido:'),'',0,'L');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Cc o Nit :'),'T',1,'L');

$pdf->ln(5);

$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
$pdf->setXY($cx+185, $cy);
$pdf->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$pdf->PageNo()),'',1,'C');


// $pdf->Cell(28.9,3, $this->sma->utf8Decode($sma->formatMoney($total)),'',1,'C');
$pdf->Output("comprobante.pdf", "I");