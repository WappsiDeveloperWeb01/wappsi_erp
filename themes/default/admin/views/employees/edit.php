<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
    .col-sm-3, .col-md-2, .col-sm-1 {
        padding-right: 5px;
        padding-left: 5px;
    }

    .iradio_square-blue {
        margin-top: 5px;
    }

    .fileinput .thumbnail>img { height: 130px; }
</style>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row" id="sortable-view">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <?= admin_form_open_multipart("employees/update", ["class" => "wizard-big wizard", "id" => "add_employee_form"]); ?>
                        <?= form_hidden('employee_id', $employee_data->employee_id); ?>
                        <?php if (empty($employee_data->contract_id)) : ?>
                            <?= form_hidden("add_contract", "0"); ?>
                        <?php endif; ?>
                        <h1><?= lang("basic_data") ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <span class="fileinput-info"><i class="fa fa-info-circle fa-lg" data-html="true" data-toggle="popover" data-placement="bottom" data-content="<small><b>Formato: </b>.jpg, .png<br><b>Dimensión: </b>800px*800px<br><b>Peso max: </b> 512 KB</small>"></i></span>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new img-thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px; overflow: hidden;">
                                            <?php if(!empty($employee_data->customer_profile_photo)): ?>
                                                <img id="employee_photo_preview" src="<?= base_url() . "assets/uploads/avatars/". $employee_data->customer_profile_photo?>" style="width: 100%">
                                            <?php endif ?>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 140px; max-height: 140px;  margin-bottom: 5px;"></div>
                                        <div>
                                            <span class="btn btn-primary btn-file btn-sm">
                                                <span class="fileinput-new">Seleccionar</span>
                                                <span class="fileinput-exists">Cambiar</span>
                                                <input type="file" name="employee_photo" id="employee_photo" accept=".jpg,.png,.jpeg">
                                            </span>
                                            <a href="#" class="btn btn-default btn-sm fileinput-exists" data-dismiss="fileinput">Borrar</a>
                                        </div>
                                    </div>
                                    <label id="employee_photo-error" class="error" for="employee_photo" style="display: none;"></label>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("first_name"), "first_name"); ?>
                                        <?= form_input(["name"=>"first_name", "id"=>"first_name", "class"=>"form-control", "required"=>TRUE], $employee_data->first_name); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("second_name"), "second_name"); ?>
                                        <?= form_input(["name"=>"second_name", "id"=>"second_name", "class"=>"form-control"], $employee_data->second_name); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("first_lastname"), 'first_lastname'); ?>
                                        <?= form_input(["name"=>"first_lastname", "id"=>"first_lastname", "class"=>"form-control", "required"=>TRUE], $employee_data->first_lastname); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("second_lastname"), 'second_lastname'); ?>
                                        <?= form_input(["name"=>"second_lastname", "id"=>"second_lastname", "class"=>"form-control"], $employee_data->second_lastname); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("document_type"), 'tipo_documento'); ?>
                                        <select class="form-control not_select" name="tipo_documento" id="tipo_documento" required="required">
                                            <option value=""><?= lang("select"); ?></option>
                                            <?php foreach ($document_types as $document_type): ?>
                                                <?php
                                                    $selected = "";
                                                    if (isset($employee_data) && $employee_data->tipo_documento == $document_type->id) {
                                                        $selected = "selected";
                                                    } else if ($document_type->id == 3) {
                                                        $selected = "selected";
                                                    }
                                                ?>
                                                <option value="<?= $document_type->id; ?>" data-code="<?= $document_type->codigo_doc; ?>" <?= $selected; ?>><?= lang($document_type->nombre); ?></option>
                                            <?php endforeach ?>
                                            <?= form_hidden("document_code", ""); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("document_number"), 'vat_no'); ?>
                                        <?= form_input(["name"=>"vat_no", "id"=>"vat_no", "class"=>"form-control tip", "required"=>TRUE], $employee_data->vat_no); ?>
                                        <?= form_hidden("digito_verificacion", ""); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("educational_level"), "educational_level"); ?>
                                        <?php $bopts = [""=>lang("select"), 1=>"Básica primaria", 2=>"Básica Secundaria", 3=>"Educación Media técnica", 4=>"Técnico profesional", 5=>"Tecnología", 6=>"Profesional Universitario", 7=>"Especialización", 8=>"Maestría", 9=>"Doctorado", 10=>"Postdoctorado"]; ?>
                                        <?= form_dropdown(["name"=>"educational_level", "id"=>"educational_level", "class"=>"form-control not_select", "required"=>TRUE], $bopts, $employee_data->educational_level); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("profession"), 'profession'); ?>
                                        <?= form_input(["name"=>"profession", "id"=>"profession", "class"=>"form-control", "required"=>TRUE], $employee_data->profession); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("marital_status"), 'marital_status'); ?>
                                        <?php $mopts = [""=>lang("select"), 1=>lang("free_union"), 2=>lang("married"), 3=>lang("divorced"), 4=>lang("single"), 5=>lang("widower")]; ?>
                                        <?= form_dropdown(["name"=>"marital_status", "id"=>"marital_status", "class"=>"form-control not_select", "required"=>TRUE], $mopts, $employee_data->marital_status); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("blood_type"), "blood_type"); ?>
                                        <?php $bopts = [""=>lang("select"), 1=>"A+", 2=>"A-", 3=>"B+", 4=>"B-", 5=>"O+", 6=>"O-", 7=>"AB+", 8=>"AB-"]; ?>
                                        <?= form_dropdown(["name"=>"blood_type", "id"=>"blood_type", "class"=>"form-control not_select", "required"=>TRUE], $bopts, $employee_data->blood_type); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("birthdate"), 'birthdate'); ?>
                                        <?= form_input(["name"=>"birthdate", "id"=>"birthdate", "type"=>"date", "class"=>"form-control", "required"=>TRUE], $employee_data->birthdate); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("gender"), 'gender'); ?>
                                        <?php $gopts = [""=>lang("select"), 1=>lang("male"), 2=>lang("female")]; ?>
                                        <?= form_dropdown(["name"=>"gender", "id"=>"gender", "class"=>"form-control not_select", "required"=>TRUE], $gopts, $employee_data->gender); ?>
                                    </div>
                                </div>

                                <div class="col-md-3" id="military_card_container">
                                    <div class="form-group">
                                        <?= form_label(lang("military_card"), 'military_card'); ?>
                                        <?= form_input(["name"=>"military_card", "id"=>"military_card", "class"=>"form-control"], $employee_data->military_card); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("country").' '.lang('residency'), 'country'); ?>
                                        <select class="form-control not_select" name="country" id="country" required>
                                            <option value=""><?= lang("select"); ?></option>
                                            <?php foreach ($countries as $country): ?>
                                                <?php $selected = (!empty($employee_data) && $employee_data->country == $country->NOMBRE) ? "selected":""; ?>
                                                <option value="<?= $country->NOMBRE; ?>" data-code="<?= $country->CODIGO ?>" <?= $selected; ?>><?= $country->NOMBRE ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <?= form_hidden("country_code", ""); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("state").' '.lang('residency'), "state"); ?>
                                        <select class="form-control not_select" name="state" id="state" required>
                                            <option value=""><?= lang("select"); ?></option>
                                        </select>
                                        <?= form_hidden("state_code", ""); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("city").' '.lang('residency'), "city"); ?>
                                        <select class="form-control not_select" name="city" id="city" required>
                                            <option value=""><?= lang("select"); ?></option>
                                        </select>
                                        <?= form_hidden("city_code", ""); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("address"), 'address'); ?>
                                        <?= form_input(["name"=>"address", "id"=>"address", "class"=>"form-control", "required"=>TRUE], $employee_data->address); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("postal_code"), 'postal_code'); ?>
                                        <?= form_input(["name"=>"postal_code", "id"=>"postal_code", "class"=>"form-control postal_code"], $employee_data->postal_code); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("employees_email"), 'email'); ?>
                                        <?= form_input(["name"=>"email", "id"=>"email", "type"=>"email", "class"=>"form-control", "required"=>TRUE], $employee_data->email); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("employees_phone"), 'phone'); ?>
                                        <?= form_input(["name"=>"phone", "id"=>"phone", "type"=>"tel", "class"=>"form-control", "required"=>TRUE], $employee_data->phone); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("home_phone"), 'home_phone'); ?>
                                        <?= form_input(["name"=>"home_phone", "id"=>"home_phone", "type"=>"tel", "class"=>"form-control"], $employee_data->home_phone); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang('status'), 'status'); ?>
                                        <br>
                                        <div class="radio-inline">
                                            <label>
                                                <?= form_radio(['name'=>'status', 'id'=>'status1'], ACTIVE, ($employee_data->status == ACTIVE ? TRUE : FALSE)) ?>
                                                <?= lang('active') ?>
                                            </label>
                                        </div>

                                        <div class="radio-inline">
                                            <label>
                                                <?= form_radio(['name'=>'status', 'id'=>'status2'], INACTIVE, ($employee_data->status == INACTIVE ? TRUE : FALSE)) ?>
                                                <?= lang('inactive') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h1><?= lang('contacts_data') ?></h1>
                        <section>
                            <div id="contact_data_container">
                                <?php if (!empty($employee_contacts)) : ?>
                                    <?php foreach ($employee_contacts as $key => $employee_contact) : ?>
                                        <?php $index = $key; ?>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <?= form_label(lang("name"), "contact_name"); ?>
                                                    <?= form_input(["name"=>"contact_name[".$index."]", "id"=>"contact_name".$index, "class"=>"form-control", "required"=>TRUE], $employee_contact->name); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= form_label(lang("relationship"), "relationship") ?>
                                                    <?php $ropts = [""=>lang("select"), 1=>"Conyuge", 2=>"Padre", 3=>"Madre", 4=>"Hijo", 5=>"Hija", 6=>"Abuelo", 7=>"Abuela", 8=>"Otros"]; ?>
                                                    <?= form_dropdown(["name"=>"relationship[".$index."]", "id"=>"relationship".$index, "class"=>"form-control not_select", "required"=>TRUE], $ropts, $employee_contact->relationship); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <?= form_label(lang("address"), "contact_address"); ?>
                                                    <?= form_input(["name"=>"contact_address[".$index."]", "id"=>"contact_address".$index, "class"=>"form-control", "required"=>TRUE], $employee_contact->address); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-1">
                                                <div class="form-group">
                                                    <?= form_label(lang("phone"), "contact_phone"); ?>
                                                    <?= form_input(["name"=>"contact_phone[".$index."]", "id"=>"contact_phone".$index, "type"=>"tel", "class"=>"form-control", "required"=>TRUE], $employee_contact->phone); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <?= form_label(lang("email"), "contact_email"); ?>
                                                    <?= form_input(["name"=>"contact_email[".$index."]", "id"=>"contact_email".$index, "type"=>"email", "class"=>"form-control"], $employee_contact->email); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-1">
                                                <div class="form-group text-center">
                                                    <?= form_label(lang('employees_main_contact'), 'main_contact'); ?>
                                                    <br>
                                                    <?= form_radio(['name'=>'main_contact', 'id'=>'main_contact', 'required'=>TRUE], $index, ($employee_contact->main_contact == YES ? TRUE: FALSE)); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-1 text-center">
                                                <?php if ($key == 0) : ?>
                                                    <?= form_button(["type"=>"button", "class"=>"btn btn-default new-button remove_contact_fields tip", "disabled"=>TRUE, "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>lang("delete"), "style"=>"margin-top: 29px;"], '<i class="fa fa-trash"></i>'); ?>
                                                <?php else : ?>
                                                    <?= form_button(["type"=>"button", "class"=>"btn btn-default new-button remove_contact_fields tip", "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>lang("delete"), "style"=>"margin-top: 29px;"], '<i class="fa fa-trash"></i>'); ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <?php $index = 0; ?>
                                    <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <?= form_label(lang("name"), "contact_name"); ?>
                                                    <?= form_input(["name"=>"contact_name[".$index."]", "id"=>"contact_name".$index, "class"=>"form-control", "required"=>TRUE]); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= form_label(lang("employees_relationship"), "relationship") ?>
                                                    <?php $ropts = [""=>lang("select"), 1=>"Conyuge", 2=>"Padre", 3=>"Madre", 4=>"Hijo", 5=>"Hija", 6=>"Abuelo", 7=>"Abuela", 8=>"Otros"]; ?>
                                                    <?= form_dropdown(["name"=>"relationship[".$index."]", "id"=>"relationship".$index, "class"=>"form-control not_select", "required"=>TRUE], $ropts); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <?= form_label(lang("address"), "contact_address"); ?>
                                                    <?= form_input(["name"=>"contact_address[".$index."]", "id"=>"contact_address".$index, "class"=>"form-control", "required"=>TRUE]); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-1">
                                                <div class="form-group">
                                                    <?= form_label(lang("phone"), "contact_phone"); ?>
                                                    <?= form_input(["name"=>"contact_phone[".$index."]", "id"=>"contact_phone".$index, "type"=>"tel", "class"=>"form-control", "required"=>TRUE]); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <?= form_label(lang("email"), "contact_email"); ?>
                                                    <?= form_input(["name"=>"contact_email[".$index."]", "id"=>"contact_email".$index, "type"=>"email", "class"=>"form-control"]); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-1">
                                                <div class="form-group text-center">
                                                    <?= form_label(lang('employees_main_contact'), 'main_contact'); ?>
                                                    <?= form_radio(['name'=>'main_contact', 'id'=>'main_contact', 'required'=>TRUE], $index, TRUE); ?>
                                                </div>
                                            </div>

                                            <div class="col-sm-1 text-center">
                                                <?= form_button(["type"=>"button", "class"=>"btn btn-default new-button remove_contact_fields tip", "disabled"=>TRUE, "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>lang("delete"), "style"=>"margin-top: 29px;"], '<i class="fa fa-trash"></i>'); ?>
                                            </div>
                                        </div>
                                <?php endif; ?>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <hr>
                                    <?= form_button(["id"=>"add_contact_fields", "type"=>"button", "class"=>"btn btn-primary new-button tip", "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>lang("add"), "style"=>"margin-right: 34px"], '<i class="fa fa-plus"></i>'); ?>
                                </div>
                            </div>
                        </section>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        _number_contact_fields = <?= ((!empty($employee_contacts))) ? count($employee_contacts) : 0; ?>;
        _consecutive_contact_fields = <?= ((!empty($employee_contacts))) ? count($employee_contacts) : 0; ?>;

        $('[data-toggle="tooltip"]').tooltip();
        $("[data-toggle=popover]").popover();

        load_wizard_configuration();

        $('#remove_contact_fields').fadeOut();
        $(document).on('change', '#vat_no', function(){ validate_existing_document_number(); });
        $(document).on('change', '#country', function() { load_states(); });
        $(document).on('change', '#state', function(){ load_cities(); });
        $(document).on('change', '#city', function(){ load_zones(); });
        $(document).on('change', '#gender', function(){ enable_disable_military_card_input(); });
        $(document).on('change', '#email', function(){ validate_existing_email(); });
        $(document).on('click', '#add_contact_fields', function(){ add_contact_fields();});
        $(document).on('click', '.remove_contact_fields', function(){ remove_contact_fields($(this)); });
        <?php if (empty($employee_data->contract_id)) : ?>
            $(document).on('click', '#add_contract', function(){ $('input[name="add_contract"]').val(1); $("#add_employee_form").steps("finish"); });
        <?php endif; ?>
        $(document).on('click', '#cancel_button', function(){ window.location = '<?= admin_url("employees"); ?>'; });

        $('#employee_photo').change(function() {
            $('#employee_photo').removeData('imageWidth');
            $('#employee_photo').removeData('imageHeight');
            var file = this.files[0];
            var tmpImg = new Image();
            tmpImg.src=window.URL.createObjectURL( file );
            tmpImg.onload = function() {
                width = tmpImg.naturalWidth,
                height = tmpImg.naturalHeight;
                $('#employee_photo').data('imageWidth', width);
                $('#employee_photo').data('imageHeight', height);
            }
        });

        $('#vat_no').trigger('change');
        $('#gender').trigger('change');
        $('#country').trigger('change');
    });

    function load_wizard_configuration()
    {
        var form = $("#add_employee_form");
        wizard = form.steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: '<i class="fa-solid fa-check" data-toggle="tooltip" title="Guardar" data-placement="top"></i>',
                next: '<i class="fa-solid fa-angles-right" data-toggle="tooltip" title="Siguiente" data-placement="top"></i>',
                previous: '<i class="fa-solid fa-angles-left"  data-toggle="tooltip" title="Anterior" data-placement="top"></i>',
                loading: "Loading ..."
            },
            onInit: function(event, currentIndex)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();

                var cancel_button = $("<li>").attr("aria-disabled",false).append('<a id="cancel_button"><i class="fas fa-times fa-lg" data-toggle="tooltip" title="Cancelar" data-placement="top"></i></a>');
                $(document).find(".actions ul").prepend(cancel_button);
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                $.validator.addMethod("filesize_max", function(value, element, param) {
                    return this.optional(element) || (element.files[0].size <= param)
                }, 'El tamaño de la imágen debe ser menor a {0} KB');

                $.validator.addMethod('ratio_aspecto', function(value, element, param) {
                    if (this.optional(element)) {
                        return true;
                    }

                    var width = $(element).data('imageWidth');
                    var height = $(element).data('imageHeight');

                    function gcd(a, b) {
                        if (b > a) {
                            temp = a;
                            a = b;
                            b = temp
                        }
                        while (b != 0) {
                            m = a % b;
                            a = b;
                            b = m;
                        }
                        return a;
                    }

                    function ratio(x, y) {
                        c = gcd(x, y);
                        return "" + (x / c) + ":" + (y / c)
                    }

                    var ratio = ratio(width, height);
                    if (ratio == param) {
                        return true;
                    }

                    return false;
                },'Por favor ingresar una imágen cuadrada');

                $.validator.addMethod('resolution', function(value, element, param){
                    if (this.optional(element)) {
                        return true;
                    }

                    var width = $(element).data('imageWidth');
                    var height = $(element).data('imageHeight');

                    if (width <= param && height <= param) {
                        if (width == height) {
                            return true;
                        }
                    }
                }, 'El resolución de la imágen debe ser de {0}px * {0}px o menor');

                form.validate({
                    rules: {
                        employee_photo: {
                            accept: "image/png,image/jpeg",
                            filesize_max : 512000,
                            ratio_aspecto:"1:1",
                            resolution:"800"
                        }
                    },
                    focusCleanup: true,
                    highlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                        } else {
                            elem.addClass(errorClass);
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                        } else {
                            elem.removeClass(errorClass);
                        }
                    }
                }).settings.ignore = ":disabled,:hidden:not(.validate)";
                return form.valid();
            },
            onStepChanged: function(event, currentIndex)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();

                <?php if (empty($employee_data->contract_id)) : ?>
                    if(currentIndex == 1) {
                        var saveBtn = $("<li>").attr("aria-disabled",false).append('<a id="add_contract" style="background: #fff;color: #1484c6;border: 1px solid #1484c6">Guardar y crear Contrato</a>');

                        $(document).find(".actions ul").append(saveBtn)
                    }else{
                      $(".actions").find("#add_contract").remove();
                    }
                <?php endif; ?>
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                if(form.valid()){
                    $(form).submit();
                }
            }
        });

        $(document).on("select2-opening", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror")) {
                $(".select2-drop ul").addClass("mierror");
            } else {
                $(".select2-drop ul").removeClass("mierror");
            }
        });

        $(document).on("select2-close", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror") && $("#" + elem.attr("id")).val() != '') {
                $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');

                $("#" + elem.attr("id") + "-error").css('display', 'none');
            }
        });
    }

    function validate_existing_document_number()
    {
        var document_number = $("#vat_no").val();

        if (document_number != '') {
            $.ajax({
                url: '<?= admin_url("employees/validate_existing_document_number"); ?>',
                type: 'GET',
                dataType: 'json',
                data: {
                    'document_number': document_number,
                    'id': '<?= $employee_data->employee_id ?>'
                },
            })
            .done(function(data) {
                if (data == true) {
                    command:toastr.error('<?= lang("employees_existing_document_number"); ?>', '<?= lang("toast_error_title") ?>', {onHidden:function() { $("#vat_no").val(''); $("#vat_no").focus(); }});
                } else {
                    get_check_digit();
                }
            })
            .fail(function(data) {
                command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
            });
        }
    }

    function get_check_digit()
    {
        var check_digit = '';
        var document_number = $("#vat_no").val();
        var document_code = $("#tipo_documento option:selected").data('code');

        if (document_code == '<?= NIT; ?>') {
            if (site.settings.get_companies_check_digit == 1) {
                check_digit = calculate_check_digit(document_number);
                $('input[name="digito_verificacion"]').val(check_digit);
            }
        } else {
            check_digit = 0;
        }

        $('input[name="digito_verificacion"]').val(check_digit);
        $('input[name="document_code"]').val(document_code);
    }

    function validate_existing_email()
    {
        var email = $("#email").val();

        if (email != '') {
            $.ajax({
                url: '<?= admin_url("employees/validate_existing_email") ?>',
                type: 'GET',
                dataType: 'json',
                data: {
                    'email': email
                },
            })
            .done(function(data) {
                if (data == true) {
                    command:toastr.error('<?= lang("employees_existing_email"); ?>', '<?= lang("toast_error_title") ?>', {onHidden:function() { $("#email").val(''); $("#email").focus(); }});
                }
            })
            .fail(function(data) {
                command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
            });
        }
    }

    function load_states()
    {
        var country_code = $('#country option:selected').data('code');
        $('input[name="country_code"]').val(country_code);

        $.ajax({
            url:"<?= admin_url() ?>employees/get_states/"+country_code,
        }).done(function(data){
            $('#state').html(data);
            <?php if (!empty($employee_data->state)) : ?>
                $('#state').select2('val', '<?= $employee_data->state; ?>');
                $('#state').trigger('change');
            <?php endif; ?>
        }).fail(function(data){
            console.log(data);
        });
    }

    function load_cities()
    {
        state_code = $('#state option:selected').data('code');
        $('input[name="state_code"]').val(state_code);

        $.ajax({
            url:"<?= admin_url() ?>employees/get_cities/"+state_code,
        }).done(function(data){
            $('#city').html(data);
            <?php if (!empty($employee_data->city)) : ?>
                $('#city').select2('val', '<?= $employee_data->city; ?>');
                $('#city').trigger('change');
            <?php endif; ?>
        }).fail(function(data){
            console.log(data);
        });
    }

    function load_zones()
    {
        city_code = $('#city option:selected').data('code');
        $('input[name="city_code"]').val(city_code);

        code_country = $('#country option:selected').data('code');
        postal_code = '';
        if (city_code) {
          postal_code = city_code.toString().replace(code_country, '');
          $('#postal_code').val(postal_code);
        }

        $.ajax({
            url:"<?= admin_url().'employees/get_zones/' ?>"+city_code
        }).done(function(data){
            $('#zone').html(data);
        });
    }

    function enable_disable_military_card_input()
    {
        var gender = $('#gender').val();
        if(gender != ''){
            if(gender == 1){
                $('#military_card').prop("disabled", false);
            }else{
                $('#military_card').prop("disabled", true);
            }
        }else{
            $('#military_card').prop("disabled", false);
        }
    }

    function add_contact_fields()
    {
        if(_number_contact_fields < 3){
            $.ajax({
                dataType: 'HTML',
                url:"<?= admin_url() ?>employees/get_contact_fields/"+_consecutive_contact_fields,
            }).done(function(data){
                $('#contact_data_container').append(data);
                $('[data-toggle="tooltip"]').tooltip();
                $('select:not(:hidden)').select2();
                $('input').iCheck({radioClass: 'iradio_square-blue' });

                _number_contact_fields++;
                _consecutive_contact_fields++;
            }).fail(function(data){
                command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function(){ console.log(data.reponseText); }});
            });
        }else{
            command: toastr.error('<?= lang("employees_maximum_contacts_allowed"); ?>', '<?= lang("toast_alert_title") ?>');
        }
    }

    function remove_contact_fields(element)
    {
        element.parent().parent('.row').remove()
        _number_contact_fields--;

        $('input[name="main_contact"]').each(function(index, el) {
            $('#'+$(this).prop('id')).iCheck('check');
        });
    }

    function  calculate_check_digit(myNit)
    {
        var vpri, x, y, z;

        // Se limpia el Nit
        myNit = myNit.replace ( /\s/g, "" ) ; // Espacios
        myNit = myNit.replace ( /,/g,  "" ) ; // Comas
        myNit = myNit.replace ( /\./g, "" ) ; // Puntos
        myNit = myNit.replace ( /-/g,  "" ) ; // Guiones

        // Se valida el nit
        if(isNaN(myNit)){
            console.log ("El nit/cédula '" + myNit + "' no es válido(a).") ;
            return "" ;
        };

        vpri = new Array(16) ;
        z = myNit.length ;

        vpri[1]  =  3 ;
        vpri[2]  =  7 ;
        vpri[3]  = 13 ;
        vpri[4]  = 17 ;
        vpri[5]  = 19 ;
        vpri[6]  = 23 ;
        vpri[7]  = 29 ;
        vpri[8]  = 37 ;
        vpri[9]  = 41 ;
        vpri[10] = 43 ;
        vpri[11] = 47 ;
        vpri[12] = 53 ;
        vpri[13] = 59 ;
        vpri[14] = 67 ;
        vpri[15] = 71 ;

        x = 0 ;
        y = 0 ;

        for(var i=0; i<z; i++){
            y = (myNit.substr(i, 1));
            x += (y * vpri[z-i]) ;
        }

        y = x % 11;

        return (y > 1) ? 11 - y : y;
    }
</script>

<?= $jasny_js ?>