<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_concepts_model extends CI_Model {

	public function get()
	{
		$q = $this->db->get("payroll_concepts");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_by_id($id)
	{
		$this->db->select("payroll_concepts.id,payroll_concepts.name,payroll_concepts.concept_type_id,payroll_concepts.payment_frequency_concept,payroll_concepts.percentage,payroll_concepts.status,payroll_concept_type.type, dian_code");
		$this->db->where("payroll_concepts.id", $id);
		$this->db->join("payroll_concept_type", "payroll_concept_type.id = payroll_concepts.concept_type_id", "inner");
		$q = $this->db->get("payroll_concepts");
		if ($q->num_rows() > 0) {
			return $q->row();
		}
		return FALSE;
	}

	public function get_not_fixed()
	{
		$q = $this->db->where("fixed", NOT)->get("payroll_concept_type");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_automatic()
	{
		$this->db->select("payroll_concepts.id,payroll_concepts.name,payroll_concepts.concept_type_id,payroll_concepts.percentage,payroll_concepts.status,payroll_concept_type.type");
		$this->db->where("status", YES);
		$this->db->where("automatic", YES);
		$this->db->not_like( $this->db->dbprefix("payroll_concepts").".name", "Empleador");
		$this->db->join("payroll_concept_type", "payroll_concept_type.id = payroll_concepts.concept_type_id", "inner");
		$q = $this->db->get("payroll_concepts");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_not_automatic()
	{
		$this->db->select("payroll_concepts.id, payroll_concepts.name, payroll_concepts.concept_type_id, payroll_concepts.percentage, payroll_concepts.status, payroll_concept_type.type, payroll_concept_type.name AS concept_type_name");
		$this->db->where("status", YES);
		$this->db->where("automatic", NOT);
		$this->db->not_like( $this->db->dbprefix("payroll_concepts").".name", "Empleador");
		$this->db->where_not_in($this->db->dbprefix("payroll_concepts").".id", [PER_DIEM_CONSTITUTES_SALARY, FINAL_CLEARANCE_VACATION, SOCIAL_SECURITY_RECOGNITION, LOAN_DISBURSEMENT, DEDUCTION_CANCELLATION_CBTE, LAYOFF_ADJUSTMENTS, PREVIOUS_YEAR_LAYOFFS, PROVISION_LAYOFFS_INTERESTS]);
		$this->db->join("payroll_concept_type", "payroll_concept_type.id = payroll_concepts.concept_type_id", "inner");
		$this->db->order_by("payroll_concepts.name", "asc");
		$q = $this->db->get("payroll_concepts");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function get_by_concept_type($concept_type)
	{
		if (!is_array($concept_type)) { $concept_type = (array) $concept_type; }

		$this->db->where_in('concept_type_id', $concept_type);
		$q = $this->db->get('payroll_concepts');
		return $q->result();
	}

	public function get_concept_type($id)
	{
		$this->db->where("id", $id);
		$q = $this->db->get("payroll_concept_type");
		if ($q->num_rows() > 0) {
			return $q->row();
		}
		return FALSE;
	}

	public function get_frequency_options(int $payment_frecuency)
	{
		if ($payment_frecuency == BIWEEKLY) {
        	$options = [
    			BIWEEKLY => 'Cada quincena',
    			MONTHLY => 'En la última quincena'
    		];
        } else if ($payment_frecuency == MONTHLY) {
        	$options = [MONTHLY => 'Mensual'];
        }

        return $options;
	}

	public function insert($data)
	{
		if ($this->db->insert("payroll_concepts", $data)) {
			return TRUE;
		}
		return FALSE;
	}

	public function update($data, $id)
	{
		$this->db->where("id", $id);
		if ($this->db->update("payroll_concepts", $data)) {
			return TRUE;
		}
		return FALSE;
	}

}

/* End of file payroll_concepts_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/payroll_concepts_model.php */