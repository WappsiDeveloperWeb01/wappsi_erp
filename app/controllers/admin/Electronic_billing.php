<?php defined('BASEPATH') or exit('No direct script access allowed');

class Electronic_billing extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->lang->admin_load('sales', $this->Settings->user_language);
        $this->load->admin_model("Electronic_billing_model");
        $this->load->library("encryption");
    }

    public function createJSON($documentElectronic)
    {
        $issuer = $this->site->get_setting();
        $customer = $this->site->getCompanyByID($documentElectronic->customer_id);
        $documentTypeCode = $this->getDocumentoType($documentElectronic);

        $json = [
            'supplierId' => $issuer->numero_documento,
            'receiverId' => $customer->vat_no,
            'partnershipId' => '901090070',
            'documentTypeCode' => $documentTypeCode,
            'documentId' => $documentElectronic->reference_no,
            'username' => $documentElectronic->customer
        ];

        echo '<pre>';
        print_r($json);
        print_r($documentElectronic);
        // print_r($customer);
        echo '</pre>';
        exit();
    }

    public function getDocumentoType($documentElectronic)
    {
        $resolution = $this->site->getDocumentTypeById($documentElectronic->document_type_id);

        if ($resolution->module == FACTURA_POS || $resolution->module == FACTURA_DETAL) {
            return INVOICE;
        } else if ($resolution->module == NOTA_CREDITO_POS || $resolution->module == NOTA_CREDITO_DETAL) {
            return CREDIT_NOTE;
        } else if ($resolution->module == NOTA_DEBITO_POS || $resolution->module == NOTA_DEBITO_DETAL) {
            return DEBIT_NOTE;
        }
    }

    public function consumeWebServices($json)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apivp.efacturacadena.com/staging/recepcion/estados',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                supplierId: 1234123
            }',
            CURLOPT_HTTPHEADER => array(
                'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}
