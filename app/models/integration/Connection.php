<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Connection extends CI_Model
{
    public $hostname;
    public $username;
    public $password;
    public $dbname;
    public $dbdriver;

    public function __construct()
    {
        parent::__construct();
    }

    public function open()
    {
        $instanceConfiguration = [
            "hostname" => $this->hostname,
            "username" => $this->username,
            "password" => $this->password,
            "database" => $this->dbname,
            "dbdriver" => "mysqli",
        ];

        if ($this->checkDatabase($instanceConfiguration)) {
            $newDB = $this->load->database($instanceConfiguration, true);
            return $newDB;
        }
        return false;
    }

    public function checkDatabase($config)
    {
        if( $config['dbdriver'] === 'mysqli' )
        {
            @$mysqli = new mysqli( $config['hostname'] , $config['username'] , $config['password'] , $config['database'] );
            if( !$mysqli->connect_error ) {
                @$mysqli->close();

                return true;
            } else {
                exit(var_dump($mysqli->connect_error));
            }
        }
        return false;
    }

    public function close($instance)
    {
        $instance->close();
    }

}