<?php defined('BASEPATH') or exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Pos extends MY_Controller
{
    private $dianStates = [];
    private $paymentStatus = [];
    private $electronicEmailStates = [];

    const ELECTRONICPOSMODE1 = 1;
    const ELECTRONICPOSMODE2 = 2;

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->load->admin_model('pos_model');
        $this->load->admin_model('products_model');
        $this->load->admin_model('sales_model');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('Restobar_model');
        $this->load->admin_model('Electronic_billing_model');
        $this->load->admin_model('DocumentsTypes_model');
        $this->load->helper('text');
        $this->pos_settings = $this->pos_model->getSetting();
        $this->pos_settings->pin_code = $this->pos_settings->pin_code ? md5($this->pos_settings->pin_code) : NULL;
        $this->data['pos_settings'] = $this->pos_settings;
        $this->session->set_userdata('last_activity', now());
        $this->lang->admin_load('pos', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|json';
        $this->allowed_file_size = '1024';
        $this->paymentStatus = [
            ['id' => 'pending', 'name' => $this->lang->line('pending')],
            ['id' => 'due', 'name' => $this->lang->line('due')],
            ['id' => 'partial', 'name' => $this->lang->line('partial')],
            ['id' => 'paid', 'name' => $this->lang->line('paid')]
        ];
        $this->dianStates = [
            ['id' => "0", 'name' => $this->lang->line('not_sended')],
            ['id' => "1", 'name' => $this->lang->line('pending')],
            ['id' => "2", 'name' => $this->lang->line('acepted')],
            ['id' => "3", 'name' => $this->lang->line('sent')],
            ['id' => "4", 'name' => $this->lang->line('not_sended').' y '. $this->lang->line('pending')]
        ];
        $this->electronicEmailStates = [
            ['id' => "0", 'name' => $this->lang->line('pending')],
            ['id' => "1", 'name' => $this->lang->line('sended')]
        ];
    }

    public function sales($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('sales');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->Owner) {
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $user = $this->site->getUser();
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        if ($this->input->post('dias_vencimiento')) {
            $this->data['dias_vencimiento'] = $this->input->post('dias_vencimiento');
        }

        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['users'] = $this->site->get_all_users();
        $this->data['salesOrigins'] = $this->site->getSalesOrigins();
        $this->data['paymentsMethods'] = $this->site->getPaymentsMethods();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
        $this->data['paymentsStatus'] = json_decode(json_encode($this->paymentStatus));
        $this->data['documentsTypes'] = $this->site->getDocumentsTypeByBillerId($this->input->post('biller_id'), [1, 3, 26]);

        if ($this->input->post('warehouse_id') || $this->input->post('payment_status') || $this->input->post('payment_method') || $this->input->post('fe_aceptado') || $this->input->post('fe_correo_enviado') || $this->input->post('filter_user')) {
            $this->data['advancedFiltersContainer'] = TRUE;
        }

        $this->page_construct('pos/sales', ['page_title' => lang('pos_sales')], $this->data);
    }

    public function getSales()
    {
        $this->sma->checkPermissions('index');

        $duplicate_link = anchor('admin/pos/?duplicate=$1', '<i class="fa fa-plus-square"></i> ' . lang('duplicate_sale'), 'class="duplicate_pos"');
        $to_fe_link = anchor('admin/sales/add?fe_pos_sale_id=$1', '<i class="fa fa-copy"></i> ' . lang('pos_to_fe'), 'class="duplicate_pos"');
        $to_posfe_link = anchor('admin/pos/?fe_pos_sale_id=$1', '<i class="fa fa-copy"></i> ' . lang('pos_to_posfe'));
        $detail_link = anchor('admin/pos/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('view_receipt'), 'target="_blank"');
        $detail_link2 = anchor('admin/sales/modal_view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details_modal'), 'data-toggle="modal" data-target="#myModal"');
        $detail_link3 = anchor('admin/sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $payments_link = anchor('admin/sales/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('admin/payments/add/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), '');
        $packagink_link = anchor('admin/sales/packaging/$1', '<i class="fa fa-archive"></i> ' . lang('packaging'), 'data-toggle="modal" data-target="#myModal"');
        $add_delivery_link = anchor('admin/sales/add_delivery/$1', '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $return_link_1 = anchor('admin/sales/return_sale/$1/1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_partial'));
        $return_link_2 = anchor('admin/sales/return_sale/$1/2', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_total'));
        $return_link_3 = anchor('admin/returns/credit_note_other_concepts/$1/3', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_others'));
        $debit_note_link = anchor('admin/debit_notes/add/$1', '<i class="fa fa-files-o"></i> ' . lang('debit_note_label_menu'));
        $contabilizar_venta = '<a class="reaccount_sale_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> '.lang('post_sale').' </a>';
        $sync_payments = anchor('admin/sales/sync_payments/$1', '<i class="fas fa-file-invoice"></i> ' . lang('sync_payments'));
        $change_payment_method = anchor('admin/pos/change_payment_method/$1', '<i class="fa fa-file-text-o"></i> ' . lang('change_payment_method'), 'data-toggle="modal" data-target="#myModal"');
        $downloadPdf = anchor('admin/pos/downloadPdf/' . '$1', '<i class="far fa-file-pdf"></i> ' . lang('pdf_download'),'target="_blank"'); 

        if (isset($this->GP)) {
            $duplicateLinkItem = ($this->GP['sales-add']) ? '<li>' . $duplicate_link . '</li>' : '';
            $paymentsLinkItem = ($this->GP['sales-payments']) ? '<li>' . $payments_link . '</li> <li>' . $add_payment_link . '</li>' : '';
            $returnLinkItem = ($this->GP['sales-return_sales']) ? '<li>' . $return_link_1 . '</li>'. '<li>' . $return_link_2 . '</li>' : '';
            $feLinkItem = ($this->GP['sales-fe_index']) ? '<li>' . $to_fe_link . '</li>' : '';
            $posfeLinkItem = ($this->GP['sales-fe_index']) ? '<li>' . $to_posfe_link . '</li>' : '';
            $returnLinkOtherConceptsItem = ($this->GP['returns-credit_note_other_concepts']) ? '<li>' . $return_link_3 . '</li>' : '';
            $changePaymentMethodLinkItem = ($this->GP['pos-change_payment_method']) ? '<li>' . $change_payment_method . '</li>' : '';
            $debit_note_link = ($this->GP['debit_notes-add']) ? '<li>' . $debit_note_link  : '';

            $action = '<div class="text-center">'.
                '<div class="btn-group text-left">'.
                    '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'. lang('actions') .' <span class="caret"></span></button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>' . $detail_link . '</li>'.
                        $duplicateLinkItem .
                        '<li>' . $downloadPdf . '</li>'.
                        $paymentsLinkItem .
                        $returnLinkItem .
                        $feLinkItem .
                        $posfeLinkItem .
                        $returnLinkOtherConceptsItem .
                        $changePaymentMethodLinkItem .
                        $debit_note_link .
                    '</ul>
                </div>'.
            '</div>';
        } else {
            $action = '<div class="text-center"><div class="btn-group text-left">'.
                    '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'. lang('actions') .' <span class="caret"></span></button>'.
                    '<ul class="dropdown-menu pull-right" role="menu">
                        <li>' . $detail_link . '</li>
                        <li>' . $to_fe_link . '</li>
                        <li>' . $to_posfe_link . '</li>
                        <li>' . $duplicate_link . '</li>
                        <li>' . $downloadPdf . '</li>
                        <li>' . $payments_link . '</li>
                        <li>' . $add_payment_link . '</li>
                        <li>' . $return_link_1 . '</li>
                        <li>' . $return_link_2 . '</li>
                        <li>' . $return_link_3 . '</li>
                        <li class="link_invoice_action link_debit_note">' . $debit_note_link . '</li>
                        <li>' . $contabilizar_venta . '</li>
                        <li>' . $sync_payments . '</li>
                        <li>' . $change_payment_method . '</li>
                        <li>' . $add_delivery_link . '</li>
                    </ul>
                </div>'.
            '</div>';
        }

        $tabFilterData = $this->input->post("tabFilterData");
        $optionFilter = $this->input->post('optionFilter') != 'false' ? $this->input->post('optionFilter') : 'all';

        if (!empty($tabFilterData)) {
            $returns = $this->manageResponseRequest($action, 'returns');
            $pendingPayment = $this->manageResponseRequest($action, 'pendingPayment');
            $pendingDispatch = $this->manageResponseRequest($action, 'pendingDispatch');
            $all = $this->manageResponseRequest($action, FALSE);

            echo json_encode(['returns' => $returns, /* 'notApproved' => $notApproved, */ 'pendingPayment' => $pendingPayment, /* 'pendingDian' => $pendingDian, */ 'pendingDispatch' => $pendingDispatch, 'all' => $all]);
        } else {
            echo $this->manageResponseRequest($action, $optionFilter);
        }
    }

    public function manageResponseRequest($action, $optionFilter)
    {
        $this->load->library('datatables');

        $tabFilterData = $this->input->post("tabFilterData");
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $client = $this->input->post('client') ? $this->input->post('client') : NULL;
        $biller_id = $this->input->post('biller_id') ? $this->input->post('biller_id') : NULL;
        $seller_id = $this->input->post('seller') ? $this->input->post('seller') : NULL;
        $sale_status = $this->input->post('sale_status') ? $this->input->post('sale_status') : NULL;
        $sale_origin = $this->input->post('sale_origin') ? $this->input->post('sale_origin') : NULL;
        $document_type = $this->input->post('document_type') ? $this->input->post('document_type') : NULL;
        $payment_method = $this->input->post('payment_method') ? $this->input->post('payment_method') : NULL;
        $payment_status = $this->input->post('payment_status') ? $this->input->post('payment_status') : NULL;
        $warehouse_id = ($this->input->get('warehouse_id') != "") ? $this->input->get('warehouse_id') : NULL;
        $end_date = ($this->input->post('end_date')) ? $this->sma->fld($this->input->post('end_date')) : NULL;
        $start_date = ($this->input->post('start_date')) ? $this->sma->fld($this->input->post('start_date')) : NULL;
        $pos_document_type = $this->input->post('pos_reference_no') ? $this->input->post('pos_reference_no') : NULL;
        $dias_vencimiento = ($this->input->get('dias_vencimiento') != "") ? $this->input->get('dias_vencimiento') : NULL;

        $biller_id = (!$this->Owner && !$this->Admin) ? $this->session->userdata('biller_id') : $biller_id;
        $warehouse_id = (!$this->Owner && !$this->Admin && !$warehouse_id) ? $warehouse_id = $this->session->userdata('warehouse_id') : $warehouse_id;

        $this->datatables->select($this->db->dbprefix('sales') . ".id as id,
            DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date,
            ".$this->db->dbprefix('sales').".payment_term,
            sales.reference_no,
            return_sale_ref as affects_to,
            biller,
            {$this->db->dbprefix('sales')}.customer,
            (grand_total+COALESCE(rounding, 0)),
            paid,
            ((grand_total+rounding)-paid) as balance,
            sale_status,
            payment_status,
            companies.email as cemail");
        $this->datatables->from('sales');
        $this->datatables->join('companies', 'companies.id=sales.customer_id', 'left');
        $this->datatables->join('documents_types', 'documents_types.id = sales.document_type_id', 'left');
        if ($payment_method) {
            $this->datatables->join('payments', 'payments.sale_id = sales.id AND payments.paid_by = "'.$payment_method.'"', 'inner');
        }
        $this->datatables->where('(documents_types.factura_electronica = 0 OR documents_types.factura_electronica IS NULL)');
        $this->datatables->where('pos', 1);
        $this->datatables->group_by('sales.id');

        if ($warehouse_id) {
            $this->datatables->where('sales.warehouse_id', $warehouse_id);
        }
        if ($biller_id) {
            $this->datatables->where('sales.biller_id', $biller_id);
        }
        if ($document_type) {
            $this->datatables->where('documents_types.id', $document_type);
        }
        if ($sale_origin) {
            $this->datatables->where('sales.sale_origin', $sale_origin);
        }
        if ($user_id) {
            $this->datatables->where('sales.created_by', $user_id);
        }
        if ($seller_id) {
            $this->datatables->where('sales.seller_id', $seller_id);
        }
        if ($pos_document_type) {
            $this->datatables->where('sales.document_type_id', $pos_document_type);
        }
        if ($dias_vencimiento) {
            $this->datatables->where('sales.payment_term', $dias_vencimiento);
        }
        if ($start_date) {
            $this->datatables->where('sales.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('sales.date <=', $end_date);
        }
        if ($client) {
            $this->datatables->where('sales.customer_id =', $client);
        }
        if ($sale_status) {
            $this->datatables->where('sales.sale_status =', $sale_status);
        }
        if ($payment_status) {
            $this->datatables->where('sales.payment_status =', $payment_status);
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
            }
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "id, cemail")->unset_column('cemail');

        if ($optionFilter == 'returns') {
            $this->datatables->where('documents_types.module', 3);
        /* } else if ($optionFilter == 'notApproved') {
            $this->datatables->where('sales.sale_status', 'pending'); */
        } else if ($optionFilter == 'pendingPayment') {
            $this->datatables->where('sales.payment_status !=', 'paid');
        /* } else if ($optionFilter == 'pendingDian') {
            $this->datatables->where('sales.fe_aceptado !=', ACCEPTED); */
        } else if ($optionFilter == 'pendingDispatch') {
            $this->datatables->join('deliveries', 'deliveries.sale_id = sales.id', 'inner');
        }

        if (!empty($tabFilterData)) {
            return $this->datatables->get_result_num_rows();
        } else {
            return $this->datatables->generate();
        }
    }

    public function fe_index($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('sales');

        if ($this->Owner) {
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $user = $this->site->getUser();

            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        if ($this->input->post('dias_vencimiento')) {
            $this->data['dias_vencimiento'] = $this->input->post('dias_vencimiento');
        }

        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['users'] = $this->site->get_all_users();
        $this->data['salesOrigins'] = $this->site->getSalesOrigins();
        $this->data['paymentsMethods'] = $this->site->getPaymentsMethods();
        $this->data['dianStates'] = json_decode(json_encode($this->dianStates));
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
        $this->data['paymentsStatus'] = json_decode(json_encode($this->paymentStatus));
        $this->data['electronicEmailStates'] = json_decode(json_encode($this->electronicEmailStates));
        $this->data['documentsTypes'] = $this->site->getDocumentsTypeByBillerId($this->input->post('biller_id'), [1, 3, 26], YES);
        if ($this->input->post('warehouse_id') || $this->input->post('payment_status') || $this->input->post('payment_method') || $this->input->post('fe_aceptado') || $this->input->post('fe_correo_enviado') || $this->input->post('filter_user')) {
            $this->data['advancedFiltersContainer'] = TRUE;
        }

        $pending_sales_dian = $this->site->get_pending_sales_dian(1);
        if ($pending_sales_dian->quantity > 0) {
            $this->data['pending_electronic_document_message'] = sprintf(lang('pending_electronic_document_message'), $this->Settings->days_after_current_date);
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('pos/fe_index', array('page_title' => lang('pos_fe_index')), $this->data);
    }

    public function getSalesFE()
    {
        $this->sma->checkPermissions('sales');

        $duplicate_link = anchor('admin/pos/?duplicate=$1', '<i class="fa fa-plus-square"></i> ' . lang('duplicate_sale'), 'class="duplicate_pos"');
        $to_fe_link = anchor('admin/sales/add?fe_pos_sale_id=$1', '<i class="fa fa-copy"></i> ' . lang('pos_to_fe'), 'class="duplicate_pos"');
        $detail_link = anchor('admin/pos/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('view_receipt'), 'target="_blank"');
        $detail_link2 = anchor('admin/sales/modal_view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details_modal'), 'data-toggle="modal" data-target="#myModal"');
        $detail_link3 = anchor('admin/sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $payments_link = anchor('admin/sales/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('admin/payments/add/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), '');
        $packagink_link = anchor('admin/sales/packaging/$1', '<i class="fa fa-archive"></i> ' . lang('packaging'), 'data-toggle="modal" data-target="#myModal"');
        $add_delivery_link = anchor('admin/sales/add_delivery/$1', '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $return_link_1 = anchor('admin/sales/return_sale/$1/1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_partial'));
        $return_link_2 = anchor('admin/sales/return_sale/$1/2', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_total'));
        $return_link_3 = anchor('admin/returns/credit_note_other_concepts/$1/3', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_others'));
        $debit_note_link = anchor('admin/debit_notes/add/$1', '<i class="fa fa-files-o"></i> ' . lang('debit_note_label_menu'));
        $contabilizar_venta = '<a class="reaccount_sale_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> '.lang('post_sale').' </a>';
        $sync_payments = anchor('admin/sales/sync_payments/$1', '<i class="fas fa-file-invoice"></i> ' . lang('sync_payments'));
        $change_payment_method = anchor('admin/pos/change_payment_method/$1', '<i class="fa fa-file-text-o"></i> ' . lang('change_payment_method'), 'data-toggle="modal" data-target="#myModal"');

        if (isset($this->GP)) {
            $duplicateLinkItem = ($this->GP['sales-add']) ? '<li>' . $duplicate_link . '</li>' : '';
            $paymentsLinkItem = ($this->GP['sales-payments']) ? '<li>' . $payments_link . '</li> <li>' . $add_payment_link . '</li>' : '';
            $returnLinkItem = ($this->GP['sales-return_sales']) ? '<li>' . $return_link_1 . '</li>'. '<li>' . $return_link_2 . '</li>' : '';
            $feLinkItem = ($this->GP['sales-fe_index']) ? '<li>' . $to_fe_link . '</li>' : '';
            $returnLinkOtherConceptsItem = ($this->GP['returns-credit_note_other_concepts']) ? '<li>' . $return_link_3 . '</li>' : '';
            $changePaymentMethodLinkItem = ($this->GP['pos-change_payment_method']) ? '<li>' . $change_payment_method . '</li>' : '';
            $debit_note_link = ($this->GP['debit_notes-add']) ? '<li>' . $debit_note_link. '</li>'  : '';

            $action = '<div class="text-center">
                <div class="btn-group text-left">
                    <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                        <i class="fas fa-ellipsis-v fa-lg"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>' . $detail_link . '</li>'.
                        $duplicateLinkItem .
                        $paymentsLinkItem .
                        $returnLinkItem .
                        $feLinkItem .
                        $returnLinkOtherConceptsItem .
                        $changePaymentMethodLinkItem .
                        $debit_note_link .
                   '</ul>
                </div>
            </div>';
        } else {
            $action = '<div class="text-center">
                <div class="btn-group text-left">
                    <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                        <i class="fas fa-ellipsis-v fa-lg"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>' . $detail_link . '</li>
                        <li>' . $to_fe_link . '</li>
                        <li>' . $duplicate_link . '</li>
                        <li>' . $payments_link . '</li>
                        <li>' . $add_payment_link . '</li>
                        <li>' . $return_link_1 . '</li>
                        <li>' . $return_link_2 . '</li>
                        <li>' . $return_link_3 . '</li>
                        <li class="link_invoice_action link_debit_note">' . $debit_note_link . '</li>
                        <li>' . $contabilizar_venta . '</li>
                        <li>' . $sync_payments . '</li>
                        <li>' . $change_payment_method . '</li>
                        <li>' . $add_delivery_link . '</li>
                    </ul>
                </div>
            </div>';
        }
        $tabFilterData = $this->input->post("tabFilterData");
        $optionFilter = $this->input->post('optionFilter') != 'false' ? $this->input->post('optionFilter') : 'all';

        if (!empty($tabFilterData)) {
            $returns = $this->manageResponseRequestFE($action, 'returns');
            $notApproved = $this->manageResponseRequestFE($action, 'notApproved');
            $pendingPayment = $this->manageResponseRequestFE($action, 'pendingPayment');
            $pendingDian = $this->manageResponseRequestFE($action, 'pendingDian');
            $pendingDispatch = $this->manageResponseRequestFE($action, 'pendingDispatch');
            $all = $this->manageResponseRequestFE($action, FALSE);

            echo json_encode(['returns' => $returns, 'notApproved' => $notApproved, 'pendingPayment' => $pendingPayment, 'pendingDian' => $pendingDian, 'pendingDispatch' => $pendingDispatch, 'all' => $all]);
        } else {
            echo $this->manageResponseRequestFE($action, $optionFilter);
        }
    }

    public function manageResponseRequestFE($action, $optionFilter)
    {
        $this->load->library('datatables');

        $tabFilterData = $this->input->post("tabFilterData");
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $client = $this->input->post('client') ? $this->input->post('client') : NULL;
        $biller_id = $this->input->post('biller_id') ? $this->input->post('biller_id') : NULL;
        $seller_id = $this->input->post('seller') ? $this->input->post('seller') : NULL;
        $campaign = $this->input->post('campaign') ? $this->input->post('campaign') : NULL;
        $sale_status = $this->input->post('sale_status') ? $this->input->post('sale_status') : NULL;
        $fe_aceptado = $this->input->post('fe_aceptado') != "" ? $this->input->post('fe_aceptado') : NULL;
        $sale_origin = $this->input->post('sale_origin') ? $this->input->post('sale_origin') : NULL;
        $document_type = $this->input->post('document_type') ? $this->input->post('document_type') : NULL;
        $payment_method = $this->input->post('payment_method') ? $this->input->post('payment_method') : NULL;
        $payment_status = $this->input->post('payment_status') ? $this->input->post('payment_status') : NULL;
        $warehouse_id = ($this->input->get('warehouse_id') != "") ? $this->input->get('warehouse_id') : NULL;
        $pos_document_type = $this->input->post('pos_reference_no') ? $this->input->post('pos_reference_no') : NULL;
        $fe_correo_enviado = $this->input->post('fe_correo_enviado') != "" ? $this->input->post('fe_correo_enviado') : NULL;
        $dias_vencimiento = ($this->input->get('dias_vencimiento') != "") ? $this->input->get('dias_vencimiento') : NULL;
        $end_date = ($this->input->post('end_date')) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('end_date')))) : NULL;
        $start_date = ($this->input->post('start_date')) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('start_date')))) : NULL;

        $biller_id = (!$this->Owner && !$this->Admin && !$biller_id) ? $this->session->userdata('biller_id') : $biller_id;
        $warehouse_id = (!$this->Owner && !$this->Admin && !$warehouse_id) ? $warehouse_id = $this->session->userdata('warehouse_id') : $warehouse_id;

        $this->datatables->select("{$this->db->dbprefix('sales')}.id as id,
            DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date,
            {$this->db->dbprefix('sales')}.payment_term,
            sale_origin_reference_no,
            sales.reference_no,
            IF({$this->db->dbprefix('documents_types')}.module = '1' OR {$this->db->dbprefix('documents_types')}.module = '2' OR {$this->db->dbprefix('documents_types')}.module = '5',
                (
                    if(" . $this->db->dbprefix('sales') . ".return_sale_ref IS NULL OR " . $this->db->dbprefix('sales') . ".return_sale_ref = '', " . $this->db->dbprefix('sales') . ".reference_debit_note, CONCAT_WS(', ', " . $this->db->dbprefix('sales') . ".return_sale_ref, " . $this->db->dbprefix('sales') . ".reference_debit_note))
                ),
                (
                    IF({$this->db->dbprefix('documents_types')}.module = '3' OR {$this->db->dbprefix('documents_types')}.module = '4' OR {$this->db->dbprefix('documents_types')}.module = '6' OR {$this->db->dbprefix('documents_types')}.module = '22', " . $this->db->dbprefix('sales') . ".return_sale_ref, " . $this->db->dbprefix('sales') . ".reference_debit_note)
                )
            ) AS affects_to,
            biller,
            {$this->db->dbprefix('sales')}.customer,
            (grand_total+COALESCE(rounding, 0)),
            paid,
            ((grand_total+rounding)-paid) as balance,
            sale_status,
            payment_status,
            {$this->db->dbprefix('sales')}.fe_aceptado,
            {$this->db->dbprefix('sales')}.fe_correo_enviado,
            {$this->db->dbprefix('documents_types')}.module,
            {$this->db->dbprefix('documents_types')}.factura_electronica,
            companies.email as cemail");
        $this->datatables->from('sales');
        $this->datatables->join('companies', 'companies.id=sales.customer_id', 'left');
        $this->datatables->join('documents_types', 'documents_types.id = sales.document_type_id', 'left');
        if ($payment_method) {
            $this->datatables->join('payments', 'payments.sale_id = sales.id AND payments.paid_by = "'.$payment_method.'"', 'inner');
        }
        $this->datatables->where('documents_types.factura_electronica', 1);
        $this->datatables->where('pos', 1);
        $this->datatables->group_by('sales.id');

        if ($warehouse_id) {
            $this->datatables->where('sales.warehouse_id', $warehouse_id);
        }
        if ($biller_id) {
            $this->datatables->where('sales.biller_id', $biller_id);
        }
        if ($document_type) {
            $this->datatables->where('documents_types.id', $document_type);
        }
        if ($sale_origin) {
            $this->datatables->where('sales.sale_origin', $sale_origin);
        }
        if ($user_id) {
            $this->datatables->where('sales.created_by', $user_id);
        }
        if ($seller_id) {
            $this->datatables->where('sales.seller_id', $seller_id);
        }
        if ($pos_document_type) {
            $this->datatables->where('sales.document_type_id', $pos_document_type);
        }
        if ($dias_vencimiento) {
            $this->datatables->where('sales.payment_term', $dias_vencimiento);
        }
        if ($start_date) {
            $this->datatables->where("CAST({$this->db->dbprefix('sales')}.date AS DATE) >=", $start_date);
        }
        if ($end_date) {
            $this->datatables->where("CAST({$this->db->dbprefix('sales')}.date AS DATE) <=", $end_date);
        }
        if ($client) {
            $this->datatables->where('sales.customer_id =', $client);
        }
        if ($sale_status) {
            $this->datatables->where('sales.sale_status', $sale_status);
        }
        if ($payment_status) {
            $this->datatables->where('sales.payment_status', $payment_status);
        }
        if ($fe_aceptado != NULL) {
            if ($fe_aceptado == 4) {
                $this->datatables->where('sales.fe_aceptado !=', 2);
            } else {
                $this->datatables->where('sales.fe_aceptado', $fe_aceptado);
            }
        }
        if ($fe_correo_enviado) {
            $this->datatables->where('sales.fe_correo_enviado', $fe_correo_enviado);
        }

        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
            }
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "id, cemail")->unset_column('cemail');

        if ($optionFilter == 'returns') {
            $this->datatables->where('documents_types.module', 3);
        } else if ($optionFilter == 'notApproved') {
            $this->datatables->where('sales.sale_status', 'pending');
        } else if ($optionFilter == 'pendingPayment') {
            $this->datatables->where('sales.payment_status !=', 'paid');
        } else if ($optionFilter == 'pendingDian') {
            $this->datatables->where('sales.fe_aceptado !=', ACCEPTED);
        } else if ($optionFilter == 'pendingDispatch') {
            $this->datatables->join('deliveries', 'deliveries.sale_id = sales.id', 'inner');
        }

        if (!empty($tabFilterData)) {
            return $this->datatables->get_result_num_rows();
        } else {
            return $this->datatables->generate();
        }
    }

    public function index($sid = NULL, $type_pos = 1, $quote = FALSE, $order = FALSE)
    {
        $this->sma->checkPermissions();
        $this->site->validate_random_pin_code_date();
        $this->session->unset_userdata('pos_submit_target');

        if (!$this->pos_settings->default_biller || !$this->pos_settings->default_customer || !$this->pos_settings->default_section) {
            $this->session->set_flashdata('warning', lang('please_update_settings'));
            admin_redirect('pos/settings');
        }
        if ($this->session->userdata('register_cash_movements_with_another_user')) {
            if (!$this->pos_model->registerData($this->session->userdata('register_cash_movements_with_another_user'))) {
                $this->session->set_flashdata('error', lang('another_user_cash_not_open'));
                admin_redirect('welcome');
            }
        } else {
            if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register/1');
            }
        }
        if ($register = $this->pos_model->registerData()) {
            $register_data = array('register_id' => $register->id, 'cash_in_hand' => $register->cash_in_hand, 'register_open_time' => $register->date);
            $this->session->set_userdata($register_data);
        }
        // $this->data['sid'] = $this->input->get('suspend_id') ? $this->input->get('suspend_id') : $sid;
        $did = $this->input->post('delete_id') ? $this->input->post('delete_id') : NULL;
        $suspend = $this->input->post('suspend') ? TRUE : FALSE;
        $suspend_note = $this->input->post('suspend_note');
        $birthday_year_applied = $this->input->post('birthday_year_applied');
        $quote_id = $this->input->post('quote_id');
        $convert_to_order = $this->input->post('convert_to_order') ? TRUE : FALSE;
        $convert_order_doc_type = $this->input->post('convert_order_doc_type');
        $convert_order_as = $this->input->post('convert_order_as');
        $convert_order_employer_branch = $this->input->post('convert_order_employer_branch');
        $convert_order_employer = $this->input->post('convert_order_employer');
        $unique_field = $this->input->post('unique_field');
        $fe_pos_sale_id = false;
        if ($this->input->get('fe_pos_sale_id')) {
            $fe_pos_sale_id = $this->input->get('fe_pos_sale_id');
        } else if ($this->input->post('fe_pos_sale_id')) {
            $fe_pos_sale_id = $this->input->post('fe_pos_sale_id');
            $document_type_default_return = $this->input->post('document_type_default_return');
        }

        if ($fe_pos_sale_id) { // if exists param get with fe_pos_sale_id, we starting validations
            $dev_pos_data = $this->site->getSaleByID($fe_pos_sale_id); // get all information from sales
            $dev_pos = $this->sales_model->get_biller_document_type($dev_pos_data->biller_id, 3);  // three is default number to sales return
            if (!$dev_pos) {  // validation for devolution document
                $this->session->set_flashdata('error', lang("invalid_return_pos_biller_document_type_id"));
                admin_redirect('pos/sales');
            }

            if ($dev_pos_data->return_id > 0 || $dev_pos_data->sale_status == 'returned') { // if pos sale have devolution, this sale cant converted
                $this->session->set_flashdata('error', lang("invalid_pos_to_fe"));
                admin_redirect('pos/sales');
            }
        }

        if ($quote_id && !$suspend_note) {
            $suspend = FALSE;
        }
        $count = $this->input->post('count') ? $this->input->post('count') : NULL;
        $duplicate_sale = $this->input->get('duplicate') ? $this->input->get('duplicate') : NULL;
        $this->form_validation->set_rules('customer', $this->lang->line("customer"), 'trim|required');
        $this->form_validation->set_rules('warehouse', $this->lang->line("warehouse"), 'required');
        $this->form_validation->set_rules('biller', $this->lang->line("biller"), 'required');
        $this->form_validation->set_rules('seller_id', $this->lang->line("seller"), 'required');
        if (!$suspend && !$convert_to_order) {
            $this->form_validation->set_rules('payment_document_type_id', $this->lang->line("payment_document_type_id"), 'required');
        }
        if ($this->Settings->cost_center_selection == 1 && $this->Settings->modulary == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if (!$suspend && !$convert_to_order) {
            $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        }
        if ($this->input->post("restobar_mode_module") && $this->input->post("restobar_mode_module") == 2 && $this->pos_settings->required_shipping_restobar == 1) {
            $this->form_validation->set_rules('shipping', lang("shipping"), 'required|greater_than[0]');
        }
        $type_pos = $this->input->post('type_pos');
        $url_redireccionar = $type_pos == 1 ? 'pos' : 'pos/add_wholesale';
        $this->session->set_userdata('redirect_pos', $url_redireccionar);

        $amountToFinance = 0;
        if ($this->Settings->financing_module == YES) {
            $amounPaid = $this->input->post("amount");
            $paidBy = $this->input->post("paid_by");

            if (!empty($amounPaid)) {
                for ($i=0; $i < count($amounPaid); $i++) {
                    if ($amounPaid[$i] > 0) {
                        if ($paidBy[$i] == $this->Settings->payment_method_for_credit_financing) {
                            $amountToFinance = $amounPaid[$i];
                            $this->form_validation->set_rules('frequency', $this->lang->line("frequency"), 'required');
                            $this->form_validation->set_rules('installment', $this->lang->line("installment"), 'required');
                        }
                    }
                }
            }

            if ($amountToFinance > 0) {
                $documentType = $this->DocumentsTypes_model->getDocumentTypeBiller(["biller_id"=>$this->input->post('biller'), "module"=>58]);
                if (empty($documentType)) {
                    $this->session->set_flashdata('error', lang("document_type_unassigned_credit_financing"));
                    admin_redirect("billers/edit/{$this->input->post('biller')}");
                }
            }
        }

        // $this->createCreditFinancing($saleId = 1, $amountToFinance);
        // exit();

        if ($this->form_validation->run() == TRUE) {
            $date = date('Y-m-d H:i:s');
            $except_category_taxes = $this->input->post('except_category_taxes') ? true : false;
            $tax_exempt_customer = $this->input->post('tax_exempt_customer') ? true : false;
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $address_id = $this->input->post('address_id');
            $order = $this->input->post('order');
            $submit_target = $this->input->post('submit_target') ? $this->input->post('submit_target') : NULL;
            $origin_order_sale_id = $this->input->post('origin_order_sale_id');
            // exit($customer_id.", ".$address_id);
            if (!$this->site->getCustomerBranchByCustomerAndAddresId($customer_id, $address_id)) {
                $this->session->set_flashdata('error', lang("invalid_customer_branch"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect($url_redireccionar);
            }
            $sale_tip_amount = $this->input->post('sale_tip_amount');
            $restobar_table = $this->input->post('restobar_table');
            $shipping_in_grand_total = $this->input->post('shipping_in_grand_total');
            $seller_id = $this->input->post('seller_id');
            $total_items = $this->input->post('total_items');
            $document_type_id = $this->input->post('document_type_id');
            $sale_status = 'completed';
            $payment_status = 'due';
            $payment_term = 0;
            $due_date = date('Y-m-d', strtotime('+' . $payment_term . ' days'));
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $employer_name = NULL;
            $employer_details = NULL;
            if ($convert_order_as == 2) {
                $employer_details = $this->site->getCompanyByID($convert_order_employer);
            }
            $convert_order_data = [
                'convert_order_as'=>$convert_order_as,
                'convert_order_employer_name'=> $employer_details ? ($employer_details->company ? $employer_details->company : $employer_details->name) : null,
                'convert_order_employer_branch'=>$convert_order_employer_branch,
                'convert_order_employer_id'=>$convert_order_employer,
            ];
            $customer = $customer_details->name != '-'  ? $customer_details->name : $customer_details->company;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('pos_note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
            $locator_details = $this->input->post('locator_details');
            if ($suspend) { //SI ES VENTA SUSPENDIDA NO AUMENTAMOS CONSECUTIVO
                $reference = "5";
            }
            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $ipoconsumo_total = 0;
            $digital = FALSE;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            //Retenciones
        	if ($this->input->post('rete_applied')) {
        		$rete_fuente_percentage = $this->input->post('rete_fuente_tax');
        		$rete_fuente_total = $this->input->post('rete_fuente_valor');
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
        		$rete_fuente_id = $this->input->post('rete_fuente_id');
        		$rete_iva_percentage = $this->input->post('rete_iva_tax');
        		$rete_iva_total = $this->input->post('rete_iva_valor');
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
        		$rete_iva_id = $this->input->post('rete_iva_id');
        		$rete_ica_percentage = $this->input->post('rete_ica_tax');
        		$rete_ica_total = $this->input->post('rete_ica_valor');
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
        		$rete_ica_id = $this->input->post('rete_ica_id');
        		$rete_other_percentage = $this->input->post('rete_otros_tax');
        		$rete_other_total = $this->input->post('rete_otros_valor');
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
        		$rete_other_id = $this->input->post('rete_otros_id');
                $rete_bomberil_percentage = $this->input->post('rete_bomberil_tax');
                $rete_bomberil_total = $this->input->post('rete_bomberil_valor');
                $rete_bomberil_account = $this->input->post('rete_bomberil_account');
                $rete_bomberil_base = $this->input->post('rete_bomberil_base');
                $rete_bomberil_id = $this->input->post('rete_bomberil_id');
                $rete_autoaviso_percentage = $this->input->post('rete_autoaviso_tax');
                $rete_autoaviso_total = $this->input->post('rete_autoaviso_valor');
                $rete_autoaviso_account = $this->input->post('rete_autoaviso_account');
                $rete_autoaviso_base = $this->input->post('rete_autoaviso_base');
                $rete_autoaviso_id = $this->input->post('rete_autoaviso_id');
                $total_retenciones = (Double) $rete_fuente_total + (Double) $rete_iva_total + (Double) $rete_ica_total + (Double) $rete_other_total + (Double) $rete_bomberil_total + (Double) $rete_autoaviso_total;

                $rete_applied = TRUE;
        	} else {
                $rete_applied = FALSE;
            }
            //Retenciones
            $txt_pprices_changed = false;
            $new_products = [];
            $cancelled_products = [];
            $preparation_area_products = NULL;
            for ($r = 0; $r < $i; $r++) {
                $state_porduct_ordered = $this->pos_model->get_state_suspended_sale_item($_POST["product_ordered_product_id"][$r]);
                if ($this->pos_settings->restobar_mode == "1") {
                    if (isset($_POST['state_readiness'][$r]) && $state_porduct_ordered) {
                        if ($_POST['state_readiness'][$r] != CANCELLED) {
                            if ($_POST['state_readiness'][$r] == $state_porduct_ordered->state_readiness && $_POST['state_readiness'][$r] == DISPATCHED) {
                                $item_state_readiness = $_POST['state_readiness'][$r];
                            } else {
                                $item_state_readiness = $state_porduct_ordered->state_readiness;
                            }
                        } else {
                            $item_state_readiness = $_POST['state_readiness'][$r];
                        }
                    } else {
                        $item_state_readiness = NULL;
                        if (isset($_POST['state_readiness'][$r]) && !$state_porduct_ordered) {
                            $item_state_readiness = ORDERED;
                        }
                    }
                } else {
                    $item_state_readiness = NULL;
                }
                $item_id = $_POST['product_id'][$r];
                $ignore_hide_parameters = isset($_POST['ignore_hide_parameters'][$r]) ? $_POST['ignore_hide_parameters'][$r] : NULL;
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_comment = $_POST['product_comment'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : NULL;
                $real_unit_price = $_POST['real_unit_price'][$r];
                $unit_price = $_POST['unit_price'][$r];
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : 1;
                $product_tax_rate = isset($_POST['product_tax_rate'][$r]) ? $_POST['product_tax_rate'][$r] : 1; /**/
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null; /**/
                // exit(var_dump($item_unit_tax_val));
                $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                $product_tax_rate_2 = isset($_POST['product_tax_rate_2'][$r]) ? $_POST['product_tax_rate_2'][$r] : null; /**/
                $item_unit_tax_val_2 = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : null; /**/
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : NULL;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];
                $under_cost_authorized = isset($_POST['under_cost_authorized'][$r]) ? $_POST['under_cost_authorized'][$r] : null; /**/
                $item_aquantity = $_POST['product_aqty'][$r];
                $item_preferences = isset($_POST['product_preferences_text'][$r]) ? $this->sma->preferences_selection($_POST['product_preferences_text'][$r]) : null;
                $item_is_new = isset($_POST['product_is_new'][$r]) ? $_POST['product_is_new'][$r] : NULL;
                $item_net_price = $_POST['net_price'][$r]; /**/
                $pr_item_discount = isset($_POST['product_discount_val'][$r]) ? $_POST['product_discount_val'][$r] : null; /**/
                $product_unit_id_selected = isset($_POST['product_unit_id_selected'][$r]) ? $_POST['product_unit_id_selected'][$r] : null; /**/
                $price_before_promo = isset($_POST['price_before_promo'][$r]) ? $_POST['price_before_promo'][$r] : null; /**/

                $product_gift_card_no = isset($_POST['product_gift_card_no'][$r]) ? $_POST['product_gift_card_no'][$r] : null; /**/
                $product_gift_card_value = isset($_POST['product_gift_card_value'][$r]) ? $_POST['product_gift_card_value'][$r] : null; /**/
                $product_gift_card_expiry = isset($_POST['product_gift_card_expiry'][$r]) ? $_POST['product_gift_card_expiry'][$r] : null; /**/
                $product_seller_id = isset($_POST['product_seller_id'][$r]) ? $_POST['product_seller_id'][$r] : null; /**/
                $product_current_gold_price = isset($_POST['product_current_gold_price'][$r]) ? $_POST['product_current_gold_price'][$r] : null; /**/

                if ($item_is_new == 'true') {
                    $new_products[$item_id] = 1;
                }
                if ($item_state_readiness == 3) {
                    $cancelled_products[$item_id] = 1;
                }
                if (($item_code  && $item_quantity && ($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2) > 0) || $ignore_hide_parameters == 1) {
                    $product_details = $item_type != 'manual' ? $this->pos_model->getProductByCode($item_code) : NULL;
                    if ($item_type == 'digital') {
                        $digital = TRUE;
                    }
                    $pr_item_tax = $this->sma->formatDecimal((($item_unit_tax_val) * $item_quantity));
                    $pr_item_tax_2 = $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_quantity));
                    $ipoconsumo_total += $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_unit_quantity));
                    $unit = $this->site->getUnitByID($item_unit);
                    $subtotal = $this->sma->formatDecimalNoRound(($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2) * $item_quantity);
                    $product_discount += $pr_item_discount;
                    $product_tax += ($this->pos_settings->restobar_mode != '1' || $this->pos_settings->restobar_mode == '1' && $item_state_readiness != CANCELLED) ? ($pr_item_tax + $pr_item_tax_2) : 0;
                    $product_data = $this->site->getProductByID($item_id);
                    if (!$unit) {
                        $unit = $this->site->getUnitByID($product_data->unit);
                    }
                    $p_unit_id = NULL;
                    if ($product_unit_id_selected > 0) {
                        $p_unit_id = $product_unit_id_selected;
                    } else if ($unit) {
                        $p_unit_id = $unit->id;
                    }
                    if ($price_before_promo > 0 && $under_cost_authorized == 1) {
                        if (!$txt_pprices_changed) {
                            $txt_pprices_changed .= sprintf(lang('user_sale_pprice_changed'), ($this->session->first_name." ".$this->session->last_name));
                        }
                        $txt_pprices_changed .= sprintf(lang('product_sale_pprice_changed'), $item_name." (".$item_code.")", $this->sma->formatMoney($price_before_promo), $this->sma->formatMoney($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2));
                    }
                    $product = array(
                        'product_id'      => $item_id,
                        'product_code'    => $item_code,
                        'product_name'    => $item_name,
                        'net_unit_price'  => $item_net_price,
                        'unit_price'      => $this->sma->formatDecimal($item_net_price + $item_unit_tax_val),
                        'quantity'        => $item_quantity,
                        'warehouse_id'    => $warehouse_id,
                        'item_tax'        => $pr_item_tax,
                        'tax_rate_id'     => $item_tax_rate,
                        'tax'             => $product_tax_rate,
                        'item_tax_2'      => $pr_item_tax_2,
                        'tax_rate_2_id'   => $item_tax_rate_2,
                        'tax_2'           => $product_tax_rate_2,
                        'discount'        => $item_discount,
                        'item_discount'   => $pr_item_discount,
                        'subtotal'        => $subtotal,
                        'serial_no'       => $item_serial,
                        'option_id'       => $item_option,
                        'product_type'    => $item_type,
                        'real_unit_price' => $real_unit_price,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity'   => $item_unit_quantity,
                        'comment'         => $item_comment,
                        'preferences'     => $item_preferences,
                        'state_readiness' => empty($item_state_readiness) ? '1' : $item_state_readiness,
                        'price_before_tax' => $item_net_price + ($pr_item_discount / $item_quantity),
                        'product_unit_id_selected'   => $p_unit_id,
                        'consumption_sales'   => $item_unit_tax_val_2,
                        'price_before_promo'   => $price_before_promo,
                        'under_cost_authorized'   => $under_cost_authorized,

                        'product_gift_card_no'   => $product_gift_card_no,
                        'product_gift_card_value'   => $product_gift_card_value,
                        'product_gift_card_expiry'   => $product_gift_card_expiry,
                        'seller_id'   => $product_seller_id,
                        'current_gold_price' => $product_current_gold_price,
                    );
                    // exit(var_dump($product_tax));
                    if (!$tax_exempt_customer && !$suspend && !$convert_to_order && $item_fixed = $this->site->fix_item_tax($document_type_id, $product)) {
                        $product_tax -= ($this->pos_settings->restobar_mode != '1' || $this->pos_settings->restobar_mode == '1' && $item_state_readiness != CANCELLED) ? ($pr_item_tax + $pr_item_tax_2) : 0;
                        unset($product);
                        $product = $item_fixed;
                        $product_tax += ($this->pos_settings->restobar_mode != '1' || $this->pos_settings->restobar_mode == '1' && $item_state_readiness != CANCELLED) ? ($item_fixed['item_tax'] + $item_fixed['item_tax_2']) : 0;
                        $item_net_price = $item_fixed['net_unit_price'];
                    }
                    $products[] = ($product + $gst_data);
                    if ($this->pos_settings->restobar_mode == '1') {
                        if ($item_state_readiness != CANCELLED) {
                            $total += $this->sma->formatDecimalNoRound(($item_net_price * $item_quantity));
                        }
                    } else {
                        $total += $this->sma->formatDecimalNoRound(($item_net_price * $item_quantity));
                    }
                    // ORDENES DE PREPARACION A ÁREAS DE PREPARACIÓN
                    if ($this->pos_settings->restobar_mode == 1 && $this->pos_settings->order_print_mode == 2) {
                        if ($item_is_new == 'true' && isset($_POST['preparation_area'][$r]) && $_POST['preparation_area'][$r] && $_POST['preparation_area'][$r] != 'null') {
                            $preparation_area_products[$_POST['preparation_area'][$r]][] = $product;
                        }
                    }
                    // ORDENES DE PREPARACION A ÁREAS DE PREPARACIÓN
                } else {
                    $this->session->set_flashdata('error', lang('invalid_products_data'));
                    admin_redirect($url_redireccionar);
                }
            } //FIN RECORRIDO ITEMS

            // $this->sma->print_arrays($products);
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } elseif ($this->pos_settings->item_order == 1) {
                krsort($products);
            }
            $order_discount = $this->site->calculateDiscount($this->input->post('discount'), ($total));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimalNoRound(($total + $total_tax + $this->sma->formatDecimal($shipping_in_grand_total == 1 ? $shipping : 0) - $order_discount + $sale_tip_amount));
            // exit(var_dump($grand_total));
            $rounding = 0;
            if ($this->pos_settings->rounding) {
                $round_total = $this->sma->roundNumber($grand_total, $this->pos_settings->rounding);
                $rounding = $this->sma->formatMoney($round_total - $grand_total);
            }
            // Retenciones
        	$paid = $this->input->post('amount-paid') ? $this->input->post('amount-paid') : 0;
        	if ($rete_applied) {
        		$paid = $paid + $total_retenciones;
        	}
            // Retenciones
            $document_type = $this->site->getDocumentTypeById($document_type_id);
            if (!$suspend && !$convert_to_order) {
                $resolucion = "";
                if ($document_type->save_resolution_in_sale == 1) {
                    $resolucion = $this->site->textoResolucion($document_type);
                }
            } else {
                $resolucion = "";
            }
            $payment_term = $this->input->post('payment_term');
            $due_date = (!empty($payment_term) && $payment_term > 0) ? date('Y-m-d', strtotime('+' . $payment_term . ' days')) : NULL;
            if ($except_category_taxes && $except_category_taxes != "null") {
                $note.=lang('except_category_taxes_text');
            }
            $autorrete_amount = 0;
            $autorrete_perc = 0;
            if ($this->Settings->self_withholding_percentage > 0 && $document_type->key_log == 1) {
                $autorrete_perc_op = $this->Settings->self_withholding_percentage / 100;
                $autorrete_amount = $total * $autorrete_perc_op;
                $autorrete_perc = $this->Settings->self_withholding_percentage;
            }
            $data = array('date'  => $date,
                'customer_id'       => $customer_id,
                'customer'          => $customer,
                'address_id'        => $address_id,
                'biller_id'         => $biller_id,
                'biller'            => $biller,
                'seller_id'         => $seller_id,
                'warehouse_id'      => $warehouse_id,
                'note'              => $note,
                'staff_note'        => $staff_note,
                'total'             => $total,
                'product_discount'  => $product_discount,
                'order_discount_id' => $this->input->post('discount'),
                'order_discount'    => $order_discount,
                'total_discount'    => $total_discount,
                'product_tax'       => $product_tax,
                'order_tax_id'      => $this->input->post('order_tax'),
                'order_tax'         => $order_tax,
                'total_tax'         => $total_tax,
                'shipping'          => $this->sma->formatDecimal($shipping),
                'grand_total'       => $this->sma->formatDecimal($grand_total, 2),
                'payment_term'      => $payment_term,
                'total_items'       => $total_items,
                'sale_status'       => $sale_status,
                'payment_status'    => $payment_status,
                'due_date'      => $due_date,
                'rounding'          => $rounding,
                'suspend_note'      => $this->input->post('suspend_note'),
                'pos'               => 1,
                'paid'              => $paid,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'hash'              => hash('sha256', microtime() . mt_rand()),
                'resolucion'        => $resolucion,
                'document_type_id'  => $document_type_id,
                'tip_amount'        => $sale_tip_amount,
                'shipping_in_grand_total' => $shipping_in_grand_total,
                'restobar_table_id' => isset($restobar_table) && $restobar_table ? $restobar_table : NULL,
                'self_withholding_amount' => $autorrete_amount,
                'self_withholding_percentage' => $autorrete_perc,
                'sale_origin_reference_no' => $this->input->post('sale_origin_reference_no'),
                'sale_origin'           => 'suspend_sale',
                'sale_currency'         => $this->Settings->default_currency,
                'sale_currency_trm'     => $this->Settings->default_tax_rate,
                'technology_provider'   => $this->Settings->fe_technology_provider,
                'consumption_sales' => $ipoconsumo_total,
                'locator_details' => $locator_details,
                'unique_field' => $unique_field,
            );
            // $this->sma->print_arrays($data, $products);
            // exit(var_dump($locator_details));
            $biller_data = $this->site->getAllCompaniesWithState('biller', $biller_id);
            if ($this->Settings->modulary && $biller_data->rete_autoica_percentage > 0) {
                $autoica_perc_op = $biller_data->rete_autoica_percentage / 100;
                $autoica_amount = $total * $autoica_perc_op;
                $data['rete_autoica_percentage'] = $biller_data->rete_autoica_percentage;
                $data['rete_autoica_total'] = $autoica_amount;
                $data['rete_autoica_account'] = $biller_data->rete_autoica_account;
                $data['rete_autoica_account_counterpart'] = $biller_data->rete_autoica_account_counterpart;
                $data['rete_autoica_base'] = $total;
                if ($biller_data->rete_bomberil_percentage > 0) {
                    $bomberil_perc_op = $biller_data->rete_bomberil_percentage / 100;
                    $bomberil_amount = $autoica_amount * $bomberil_perc_op;
                    $data['rete_bomberil_percentage'] = $biller_data->rete_bomberil_percentage;
                    $data['rete_bomberil_total'] = $bomberil_amount;
                    $data['rete_bomberil_account'] = $biller_data->rete_bomberil_account;
                    $data['rete_bomberil_account_counterpart'] = $biller_data->rete_bomberil_account_counterpart;
                    $data['rete_bomberil_base'] = $autoica_amount;
                }
                if ($biller_data->rete_autoaviso_percentage > 0) {
                    $autoaviso_perc_op = $biller_data->rete_autoaviso_percentage / 100;
                    $autoaviso_amount = $autoica_amount * $autoaviso_perc_op;
                    $data['rete_autoaviso_percentage'] = $biller_data->rete_autoaviso_percentage;
                    $data['rete_autoaviso_total'] = $autoaviso_amount;
                    $data['rete_autoaviso_account'] = $biller_data->rete_autoaviso_account;
                    $data['rete_autoaviso_account_counterpart'] = $biller_data->rete_autoaviso_account_counterpart;
                    $data['rete_autoaviso_base'] = $autoica_amount;
                }
            }

            if (!$suspend && !$convert_to_order && $biller_data->min_sale_amount > 0 && $grand_total < $biller_data->min_sale_amount) {
                $this->session->set_flashdata('error', sprintf(lang('min_sale_amount_error'), lang('sale'), $this->sma->formatMoney($biller_data->min_sale_amount)));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }
            if ($rete_applied) {
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;
                $data['rete_bomberil_percentage'] = $rete_bomberil_percentage;
                $data['rete_bomberil_total'] = $rete_bomberil_total;
                $data['rete_bomberil_account'] = $rete_bomberil_account;
                $data['rete_bomberil_base'] = $rete_bomberil_base;
                $data['rete_bomberil_id'] = $rete_bomberil_id;
                $data['rete_autoaviso_percentage'] = $rete_autoaviso_percentage;
                $data['rete_autoaviso_total'] = $rete_autoaviso_total;
                $data['rete_autoaviso_account'] = $rete_autoaviso_account;
                $data['rete_autoaviso_base'] = $rete_autoaviso_base;
                $data['rete_autoaviso_id'] = $rete_autoaviso_id;
            }
            if (!$suspend && !$convert_to_order) {
                $p = isset($_POST['amount']) ? count($_POST['amount'])+1 : 0;
                $paid = 0;
                $sale_payment_method = NULL;
                $pmnt_cnt = 0;
                for ($r = 0; $r < $p; $r++) {
                    $payment_reference_no = $this->input->post('payment_document_type_id');
                    if (isset($_POST['due_payment'][$r]) && $_POST['due_payment'][$r] == 1) {
                        $pm = $this->site->getPaymentMethodByCode($_POST['paid_by'][$r]);
                        $data['due_payment_method_id'] = $pm->id;
                        $pmnt_cnt++;
                        $sale_payment_method = $_POST['paid_by'][$r];
                    } else {
                        if (isset($_POST['amount'][$r]) && !empty($_POST['amount'][$r]) && isset($_POST['paid_by'][$r]) && !empty($_POST['paid_by'][$r])) {
                            $sale_payment_method = $_POST['paid_by'][$r];
                            $pmnt_cnt++;
                            $amount = $this->sma->formatDecimal(($_POST['balance_amount'][$r] > 0 ? $_POST['amount'][$r] - $_POST['balance_amount'][$r] : $_POST['amount'][$r]), 2);
                            $paid += $amount;
                            if ($_POST['paid_by'][$r] == 'deposit') {
                                //Verificación balance del depósito del cliente
                                if (!$this->site->check_customer_deposit($customer_id, $_POST['amount'][$r])) {
                                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            }
                            if ($_POST['paid_by'][$r] == 'gift_card') {
                                $gc = $this->site->getGiftCardByNO($_POST['paying_gift_card_no'][$r]);
                                if (!isset($_POST['paying_gift_card_no'][$r]) || empty($_POST['paying_gift_card_no'][$r])) {
                                    $this->session->set_flashdata('error', lang("payment_by_gift_card_withoud_number"));
                                    redirect($_SERVER["HTTP_REFERER"]);
                                } else if (!$gc) {
                                    $this->session->set_flashdata('error', lang("payment_by_gift_card_invalid_number"));
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                                $amount_paying = $_POST['amount'][$r] >= $gc->balance ? $gc->balance : $_POST['amount'][$r];
                                $gc_balance = $gc->balance - $amount_paying;
                                $payment[] = array(
                                    'date'         => $date,
                                    // 'reference_no' => $reference_payment,
                                    'amount'       => $amount,
                                    'paid_by'      => $_POST['paid_by'][$r],
                                    'cheque_no'    => $_POST['cheque_no'][$r],
                                    'cc_no'        => $_POST['paying_gift_card_no'][$r],
                                    'cc_holder'    => $_POST['cc_holder'][$r],
                                    'cc_month'     => $_POST['cc_month'][$r],
                                    'cc_year'      => $_POST['cc_year'][$r],
                                    'cc_type'      => $_POST['cc_type'][$r],
                                    'cc_cvv2'      => $_POST['cc_cvv2'][$r],
                                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                    'type'         => 'received',
                                    'mean_payment_code_fe' =>$this->input->post('mean_payment_code_fe'),
                                    'note'         => $_POST['payment_note'][$r],
                                    'pos_paid'     => $_POST['amount'][$r],
                                    'pos_balance'  => $_POST['balance_amount'][$r],
                                    'gc_balance'  => $gc_balance,
                                    'document_type_id' => $payment_reference_no,
                                );
                            } else {
                                $referenceBiller = $this->site->getReferenceBiller($biller_id, $payment_reference_no);
                                if($referenceBiller){
                                    $reference_payment = $referenceBiller;
                                }else{
                                    $reference_payment = $this->site->getReference('rc');
                                }
                                $payment[] = array(
                                    'date'         => $date,
                                    'reference_no' => $reference_payment,
                                    'amount'       => $amount,
                                    'paid_by'      => $_POST['paid_by'][$r],
                                    'cheque_no'    => $_POST['cheque_no'][$r],
                                    'cc_no'        => $_POST['cc_no'][$r],
                                    'cc_holder'    => $_POST['cc_holder'][$r],
                                    'cc_month'     => $_POST['cc_month'][$r],
                                    'cc_year'      => $_POST['cc_year'][$r],
                                    'cc_type'      => $_POST['cc_type'][$r],
                                    'cc_cvv2'      => $_POST['cc_cvv2'][$r],
                                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                    'type'         => 'received',
                                    'mean_payment_code_fe' =>$this->input->post('mean_payment_code_fe'),
                                    'note'         => $_POST['payment_note'][$r],
                                    'pos_paid'     => $_POST['amount'][$r],
                                    'pos_balance'  => $_POST['balance_amount'][$r],
                                    'document_type_id' => $payment_reference_no,
                                );
                            }
                        }
                    }
                }
                if ($pmnt_cnt > 1) {
                    $sale_payment_method = 'mixed';
                }
                
                //RETENCIONCES
                if ($rete_applied) {
                    $payment[] = array(
                        'date'         => $date,
                        'amount'       => $total_retenciones,
                        'paid_by'      => 'retencion',
                        'cheque_no'    => '',
                        'cc_no'        => '',
                        'cc_holder'    => '',
                        'cc_month'     => '',
                        'cc_year'      => '',
                        'cc_type'      => '',
                        'cc_cvv2'      => '',
                        'created_by'   => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                        'type'         => 'received',
                        'note'         => 'Retenciones',
                        'pos_paid'     => $total_retenciones,
                        'pos_balance'  => '0'
                    );
                    $paid+=$total_retenciones;
                }
                $data['payment_method'] = $sale_payment_method == NULL ? 'Credito' : $sale_payment_method;
                // $this->sma->print_arrays($data, $products);
                // exit(var_dump($data['grand_total'])." - ".var_dump($paid));
                if (($paid != $data['grand_total'] || $paid == 0)) {
                    if (($this->input->post('payment_term') <= 0  || !$this->input->post('payment_term'))) {
                        $this->session->set_flashdata('error', lang("payment_term_almost_be_greather_than_zero"));
                        admin_redirect('pos');
                    }
                    if (!$this->site->check_customer_credit_limit($customer_id, ($data['grand_total'] - $paid))) {
                        $this->session->set_flashdata('error', lang("customer_credit_limit_out_of_balance"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
                if ($this->sma->formatDecimal($paid) > ($data['grand_total'])) {
                    $this->session->set_flashdata('error', lang("payment_amount_cant_be_greather_than_invoice_total"));
                    admin_redirect('pos');
                }
            }

            if (!isset($payment) || empty($payment)) {
                $payment = array();
            }
            if (!$suspend && $this->due_payment_method['sale'] == false && $this->sma->formatDecimal($paid) < ($data['grand_total'] + $data['rounding']) ) {
                $this->session->set_flashdata('error', sprintf(lang("due_method_disabled"), lang('sale')));
                admin_redirect('pos');
            }
            // Condición que valida el método de pago.
            if ($this->input->post('paid_by_1') && $this->input->post('paid_by_1') > 0) {
                if ($this->input->post('paid_by_1') < $grand_total) {
                    $data['payment_method_fe'] = CREDIT;
                } else {
                    $data['payment_method_fe'] = CASH;
                }
            } else {
                $data['payment_method_fe'] = CREDIT;
            }
            // Condición que valida el medio del pago
            if ($this->input->post('mean_payment_code_fe')) {
                $data['payment_mean_fe'] = $this->input->post('mean_payment_code_fe');
            } else {
                $data['payment_mean_fe'] = 'ZZZ';
            }

            // $this->sma->print_arrays($data, $products);
            if (!$this->session->userdata('pos_post_processing')) {
                $this->session->set_userdata('pos_post_processing', 1);
            } else {
                $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
                admin_redirect("pos/index");
            }
            $txt_pprices_changed = trim($txt_pprices_changed, ', ');
            if ($grand_total <= 0) {
                $this->session->set_flashdata('error', 'El valor final de la factura no puede ser negativo.');
                admin_redirect("pos");
            }
            if ($origin_order_sale_id) {
                $order = true;
                $quote_id = $origin_order_sale_id;
            }
            $this->session->set_userdata('remove_posls', 1);

            // $this->createCreditFinancing();
            // exit();
        }

        if ($this->form_validation->run() == TRUE && !empty($products) && !empty($data)) {
            if ($suspend) {
                if ($this->pos_model->suspendSale($data, $products, $did, NULL, ($order ? $quote_id : NULL))) {
                    if ($this->session->userdata('pos_post_processing')) {
                        $this->session->unset_userdata('pos_post_processing');
                    }
                    $this->session->set_userdata('remove_posls', 1);
                    if ($this->pos_settings->restobar_mode == '1') {
                        if ($this->pos_settings->order_print_mode == 1) { //SI LA ORDEN SE ENVIA A IMPRESORA
                            if ($restobar_table) {
                                if ($this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4) { //SI ESTÁ CONFIGURADA LA IMPRESIÓN VÍA GOOGLE CLOUD PRINT
                                    $restobar_table = $this->pos_model->get_restobar_table_by_id($restobar_table);
                                    $this->print_preparation($products, $seller_id, $restobar_table->numero, $new_products, $cancelled_products);
                                }
                                admin_redirect($url_redireccionar);
                            }
                        } else if ($this->pos_settings->order_print_mode == 2) { //SE ENVIA A ÁREA DE PREPARACIÓN
                            if (isset($preparation_area_products) && $preparation_area_products) {
                                $this->pa_processor(false, $preparation_area_products, false, false, $locator_details);
                            } else {
                                admin_redirect($url_redireccionar);
                            }
                        } else if ($this->pos_settings->order_print_mode == 3) { //SE ENVIA A ÁREA DE PREPARACIÓN
                            admin_redirect($url_redireccionar);
                        } else {  //SI LA ORDEN SE ENVIA A PANTALLA
                            $preparation_orders = $this->pos_model->get_preparation_orders_by_preparer_user();
                            if (!empty($preparation_orders)) {
                                $preparation_order_array = [];
                                foreach ($preparation_orders as $preparation_order) {
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['id_preparador'] = $preparation_order->id_preparador;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['hora_orden'] = $preparation_order->hora_orden;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['numero_mesa'] = $preparation_order->numero_mesa;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['fecha_orden'] = $preparation_order->fecha_orden;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['diferencia_minutos'] = $preparation_order->diferencia_minutos;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['id_venta_suspendida'] = $preparation_order->id_venta_suspendida;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['id_vendedor'] = $preparation_order->id_vendedor;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['nombre_vendedor'] = $preparation_order->nombre_vendedor;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['apellido_vendedor'] = $preparation_order->apellido_vendedor;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['tipo'] = $preparation_order->tipo;
                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['id_mesa'] = $preparation_order->id_mesa;

                                    $preparation_order_array[$preparation_order->id_venta_suspendida]['productos'][] = [
                                        'nombre_producto' => $preparation_order->nombre_producto,
                                        'cantidad_producto' => $preparation_order->cantidad_producto,
                                        'estado_producto' => $preparation_order->estado_producto
                                    ];
                                }
                                echo json_encode($preparation_order_array);
                            }
                        }
                    } else {
                        admin_redirect($url_redireccionar);
                    }
                }
            } else if ($convert_to_order) {
                if ($order_converted_id = $this->pos_model->convert_to_order($data, $products, $convert_order_doc_type, $convert_order_data, $did)) {
                    if ($this->session->userdata('pos_post_processing')) {
                        $this->session->unset_userdata('pos_post_processing');
                    }
                    $this->session->set_userdata('remove_posls', 1);
                    $this->db->update('companies', [
                            'customer_employer_id'=>$convert_order_data['convert_order_employer_id'],
                            'customer_employer_branch_id'=>$convert_order_data['convert_order_employer_branch']], ['id'=>$customer_id]);
                    admin_redirect('sales/orderView/'.$order_converted_id);
                }
            } else {
                if ($this->pos_settings->restobar_mode == 1) {
                    if (
                        (
                            $this->input->post("restobar_mode_module") != 2 && //Domicilio
                            $this->input->post("restobar_mode_module") != 3 //Cliente recoge
                        ) &&
                        (
                            $this->pos_settings->order_print_mode != 1 && //Cloud Print
                            $this->pos_settings->order_print_mode != 2 // Impresión por área de preparación
                        )
                    ) {
                        if ($this->input->post("restobar_mode_module") == 2 || $this->input->post("restobar_mode_module") == 3) {

                            $table_available = $this->Restobar_model->get_table_by_restobar_module($this->input->post("restobar_mode_module"));

                            if (empty($table_available)) {
                                $this->session->set_flashdata('error', 'No existe '.lang('restobar').' disponibles para la creación de la venta.');
                                admin_redirect("pos/index/");
                            }

                            $data["restobar_table_id"] = $table_available->id;

                            if ($this->pos_model->suspendSale($data, $products)) {
                                if ($this->session->userdata('pos_post_processing')) {
                                    $this->session->unset_userdata('pos_post_processing');
                                }
                                $this->change_table_status($table_available->id, OCCUPIED);
                            }
                        } else {
                            if ($this->pos_settings->order_print_mode == 0) {
                                if ($this->session->userdata('pos_post_processing')) {
                                    $this->session->unset_userdata('pos_post_processing');
                                }
                                $suspend_sale_id = $this->input->post("suspend_sale_id");
                                $existing_undelivered_items = $this->pos_model->get_items_shipped_by_suspended_sale_id($suspend_sale_id);

                                if ($existing_undelivered_items === TRUE) {
                                   $this->session->set_flashdata('error', 'No es posible realizar el pago. Todavía existen productos en preparación.');
                                   admin_redirect("pos/index/$suspend_sale_id");
                                }
                            }
                        }
                    } else {

                        if ($this->pos_settings->order_print_mode == 0) {
                            $table_available = $this->Restobar_model->get_table_by_restobar_module($this->input->post("restobar_mode_module"));

                            if (empty($table_available)) {
                                $this->session->set_flashdata('error', 'No existe '.lang('restobar').' disponibles para la creación de la venta.');

                                admin_redirect("pos/index/");
                            }

                            $data["restobar_table_id"] = $table_available->id;

                            if ($this->pos_model->suspendSale($data, $products)) {
                                if ($this->session->userdata('pos_post_processing')) {
                                    $this->session->unset_userdata('pos_post_processing');
                                }
                                $this->change_table_status($table_available->id, OCCUPIED);
                            }
                        }
                    }
                }

                if ($sale = $this->pos_model->addSale($data, $document_type_id, $products, $payment, $did, $quote_id, $order)) {
                    if ($submit_target) {
                        $this->session->set_userdata('pos_submit_target', $submit_target);
                    }
                    if ($birthday_year_applied) {
                        $this->db->update('companies', ['birthday_year_applied' => $birthday_year_applied], ['id'=>$customer_id]);
                    }
                    $this->session->set_userdata('remove_posls', 1);
                    $this->session->set_userdata('pos_last_used_document_type_id', $document_type_id);
                    $msg = $this->lang->line("sale_added");
                    if (!empty($sale['message'])) {
                        foreach ($sale['message'] as $m) {
                            $msg .= '<br>' . $m;
                        }
                    }
                    $this->session->set_flashdata('message', $msg);
                    $redirect_to = $this->pos_settings->after_sale_page ? "pos" : "pos/view/" . $sale['sale_id']."/0/1";
                    if ($this->pos_settings->auto_print) {
                        if ($this->pos_settings->remote_printing != 1) {
                            $redirect_to .= '?print='.$sale['sale_id'];
                        }
                    }

                    if (!empty($txt_pprices_changed)) {
                        $this->db->insert('user_activities', [
                                    'date' => date('Y-m-d H:i:s'),
                                    'type_id' => 1,
                                    'table_name' => 'sales',
                                    'record_id' => $sale['sale_id'],
                                    'user_id' => $this->session->userdata('user_id'),
                                    'module_name' => $this->m,
                                    'description' => $txt_pprices_changed,
                                ]);
                    }
                    /**********************************************************************************************************/
                        if ($fe_pos_sale_id) {  // agregar la devolucion a la venta anterior si, es una conversion
                            $this->pos_model->return_sale_pos_for_fe($fe_pos_sale_id, $document_type_default_return);
                        }

                        $saleId = $sale['sale_id'];
                        $resolutionData = $this->site->getDocumentTypeById($this->input->post('document_type_id'));

                        if ($resolutionData->factura_electronica == YES) {
                            $saleData = $this->site->getSaleByID($saleId);

                            $documentElectronicData = [
                                'sale_id' => $saleId,
                                'biller_id' => $saleData->biller_id,
                                'customer_id' => $saleData->customer_id,
                                'reference' => $saleData->reference_no,
                                'attachment' => $saleData->attachment
                            ];

                            if ($resolutionData->factura_contingencia == NOT) {
                                $this->createDocumentElectronic($documentElectronicData);
                                if ($this->session->userdata('pos_post_processing')) {
                                    $this->session->unset_userdata('pos_post_processing');
                                }
                            }
                        } else {
                            if ($this->session->userdata('pos_post_processing')) {
                                $this->session->unset_userdata('pos_post_processing');
                            }
                        }
                    // /**********************************************************************************************************/
                    if ($restobar_table) {
                        $this->pos_model->update_restobar_table_status($restobar_table);
                    }

                    if (
                        (
                            (
                                (
                                    $this->input->post("restobar_mode_module") != 2 && //Domicilio
                                    $this->input->post("restobar_mode_module") != 3 //Cliente recoge
                                ) &&
                                (
                                    $this->pos_settings->order_print_mode != 1 && //Cloud Print
                                    $this->pos_settings->order_print_mode != 2 // Impresión por área de preparación
                                )
                            ) && $this->pos_settings->restobar_mode == 1
                        ) || $this->pos_settings->restobar_mode == 0
                    ) {
                        admin_redirect($redirect_to);
                    } else {
                        if ($this->pos_settings->order_print_mode == 1) { //SI LA ORDEN SE ENVIA A IMPRESORA
                            if ($restobar_table) {
                                if ($this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4) { //SI ESTÁ CONFIGURADA LA IMPRESIÓN VÍA GOOGLE CLOUD PRINT
                                    $restobar_table = $this->pos_model->get_restobar_table_by_id($restobar_table);
                                    $this->print_preparation($products, $seller_id, $restobar_table->numero, $new_products, $cancelled_products);
                                }
                                admin_redirect($redirect_to);
                            } else {
                                admin_redirect($redirect_to);
                            }
                        } else if ($this->pos_settings->order_print_mode == 2) { //SE ENVIA A ÁREA DE PREPARACIÓN
                            if (isset($preparation_area_products) && $preparation_area_products) {
                                $this->pa_processor(false, $preparation_area_products, NULL, $sale['sale_id'], $locator_details);
                            } else {
                                admin_redirect($redirect_to);
                            }
                        } else if ($this->pos_settings->order_print_mode == 0) {
                            admin_redirect($redirect_to);
                        }
                    }
                }
            }
        } else {
            $this->data['old_sale'] = NULL;
            $this->data['oid'] = NULL;
            if ($duplicate_sale) {
                if ($old_sale = $this->pos_model->getInvoiceByID($duplicate_sale)) {
                    $inv_items = $this->pos_model->getSaleItems($duplicate_sale);
                    $this->data['oid'] = $duplicate_sale;
                    $this->data['old_sale'] = $old_sale;
                    $this->data['message'] = lang('old_sale_loaded');
                    $this->data['customer'] = $this->pos_model->getCompanyByID($old_sale->customer_id);
                } else {
                    $this->session->set_flashdata('error', lang("bill_x_found"));
                    admin_redirect($url_redireccionar);
                }
            }

            if ($fe_pos_sale_id) {  // if form_validation not running and exists in get or pos fe_pos_sale_id
                if ($old_sale = $this->pos_model->getInvoiceByID($fe_pos_sale_id)) {
                    $inv_items = $this->pos_model->getSaleItems($fe_pos_sale_id);
                    $this->data['oid'] = $fe_pos_sale_id;
                    $this->data['old_sale'] = $old_sale;
                    $this->data['message'] = lang('old_sale_loaded');
                    $this->data['customer'] = $this->pos_model->getCompanyByID($old_sale->customer_id);
                    if ($this->companies_model->getBillerDocumentTypes($old_sale->biller_id, 1, 1)) {
                        if($this->companies_model->getDocumentTypeDevolutionDefaultPos($old_sale->biller_id)){
                            $this->data['pos_document_type_default_returns'] = $this->companies_model->getDocumentTypeDevolutionDefaultPos($old_sale->biller_id);
                            $this->data['fe_pos_sale_id'] = $fe_pos_sale_id;
                        }else{
                            $this->session->set_flashdata('error', lang("return_pos_documentype_default_not_assigned"));
                        }
                    }else{
                        $this->session->set_flashdata('error', lang("biller_without_documents_types"));
                    }
                } else {
                    $this->session->set_flashdata('error', lang("bill_x_found"));
                    admin_redirect($url_redireccionar);
                }
            }

            $this->data['suspend_sale'] = NULL;
            $this->data['quote'] = NULL;
            $this->data['order'] = NULL;
            $tax_exempt_customer = false;
            if ($sid && !$quote && !$order) {
                if ($suspended_sale = $this->pos_model->getOpenBillByID($sid)) {
                    $restobar_table = $this->pos_model->get_restobar_table_by_id($suspended_sale->table_id);
                    $inv_items = $this->pos_model->getSuspendedSaleItems($sid);

                    $this->data['sid'] = $sid;
                    $this->data['suspend_sale'] = $suspended_sale;
                    $this->data['inv_items'] = $inv_items;
                    $this->data['restobar_table'] = $restobar_table;
                    $this->data['message'] = lang('suspended_sale_loaded');
                    $this->data['customer'] = $this->pos_model->getCompanyByID($suspended_sale->customer_id);
                    $this->data['reference_note'] = $suspended_sale->suspend_note;
                    if ($this->data['customer']->tax_exempt_customer == 1) {
                        $tax_exempt_customer = true;
                    }
                } else {
                    $this->session->set_flashdata('error', lang("bill_x_found"));
                    admin_redirect($url_redireccionar);
                }
            } else if ($sid && $quote == 1) {
                $inv_items = $this->sales_model->getAllQuoteItems($sid);
                $this->data['quote'] = $this->sales_model->getQuoteByID($sid);
                $this->data['customer'] = $this->pos_model->getCompanyByID($this->data['quote']->customer_id);
                $this->data['quote_id'] = $sid;
                if ($this->data['customer']->tax_exempt_customer == 1) {
                    $tax_exempt_customer = true;
                }
            } else if ($sid && $order == 1) {
                $this->data['order'] = true;
                $inv_items = $this->site->getAllOrderItems($sid);
                $this->data['quote'] = $this->sales_model->getOrderByID($sid);
                $this->data['customer'] = $this->pos_model->getCompanyByID($this->data['quote']->customer_id);
                $this->data['quote_id'] = $sid;
                if ($this->data['customer']->tax_exempt_customer == 1) {
                    $tax_exempt_customer = true;
                }
            } else if ($duplicate_sale) {
                $this->data['duplicate_sale'] = true;
            }

            if (($sid || $duplicate_sale || $fe_pos_sale_id) && $inv_items) {
                    $c = rand(100000, 9999999);
                    foreach ($inv_items as $item) {
                        if ($quote && $this->Settings->product_order == 1) {
                            if (!isset($max_order_id)) {
                                $max_order_id = $item->id;
                            } else {
                                $max_order_id--;
                            }
                        }
                        if (isset($this->data['order']) && $item->quantity_to_bill == 0) {
                            continue;
                        }
                        $row = $this->site->getProductByID($item->product_id);
                        if (!$row) {
                            $row = json_decode('{}');
                            $row->tax_method = 0;
                            $row->quantity = 0;
                        } else {
                            $category = $this->site->getCategoryByID($row->category_id);
                            $row->category_name = $category ? $category->name : NULL;
                            unset($row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                        }
                        $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                        if ($pis) {
                            foreach ($pis as $pi) {
                                $row->quantity += $pi->quantity_balance;
                            }
                        }
                        $row->id = $item->product_id;
                        $row->code = $item->product_code;
                        $row->seller_id = $item->seller_id;
                        $row->name = trim($item->product_name);
                        $row->type = $item->product_type;
                        $row->price_before_promo = isset($item->price_before_promo) ? $item->price_before_promo : NULL;
                        $row->quantity += isset($this->data['order']) ? $item->quantity_to_bill : $item->quantity;
                        $row->discount = $item->discount ? $item->discount : '0';
                        $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                        $new_net_unit_price = ($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                        $new_item_tax = $this->sma->calculateTax($item->tax_rate_id, $new_net_unit_price, 0);
                        $new_unit_price = $this->sma->formatDecimals($new_net_unit_price + $new_item_tax);
                        $row->unit_price = $row->tax_method ?
                                                $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity)
                                                :
                                                $new_unit_price;
                        $row->real_unit_price = $item->real_unit_price;
                        $row->base_quantity = $item->quantity;
                        $row->base_unit = isset($row->unit) ? $row->unit : $item->product_unit_id;
                        $row->base_unit_price = $item->unit_price;
                        $row->unit = $item->product_unit_id;
                        $row->product_unit_id_selected = $item->product_unit_id;
                        $row->product_unit_id = $item->product_unit_id;
                        $row->prev_unit_price = $row->tax_method == 1 ? $item->net_unit_price : $item->unit_price;
                        $row->qty = isset($this->data['order']) ? $item->quantity_to_bill : $item->quantity;
                        $row->diferent_tax_alert = false;
                        if ($item->tax_rate_id != $row->tax_rate) {
                            $row->diferent_tax_alert = true;
                            $row->real_tax_rate = $row->tax_rate;
                        }

                        if ($tax_exempt_customer) {
                            $row->diferent_tax_alert = false;
                            $row->tax_exempt_customer = true;
                            $exempt_tax_rate_id = $this->Settings->customer_territorial_decree_tax_rate_id ? $this->Settings->customer_territorial_decree_tax_rate_id : $this->site->get_tax_exempt();
                            $item->tax_rate_id = $exempt_tax_rate_id;
                        }
                        $row->tax_rate = $item->tax_rate_id;
                        $row->serial = $item->serial_no;
                        $row->option = $item->option_id;
                        $options = $this->pos_model->getProductOptions($row->id, $item->warehouse_id);

                        $row->comment = isset($item->comment) ? $item->comment : '';
                        $row->ordered = 1;
                        $combo_items = false;
                        if ($row->type == 'combo') {
                            $combo_items = $this->pos_model->getProductComboItems($row->id, $item->warehouse_id);
                        }
                        $units = $this->site->get_product_units($row->id);
                        $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                        $real_tax_rate = null;
                        if ($row->diferent_tax_alert) {
                            $real_tax_rate = $this->site->getTaxRateByID($row->real_tax_rate);
                        }
                        $ri = ($this->Settings->item_addition ? $row->id : $c).($row->option ? $row->option : '');
                        if ($this->Settings->product_preferences_management == 1) {
                            $preferences = $this->sales_model->getProductPreferences($row->id);
                        } else {
                            $preferences = false;
                        }
                        $row->consumption_sale_tax = $item->tax_2;
                        $gift_card = [];
                        if ($item->product_id == $this->Settings->gift_card_product_id) {
                            $gift_card = [
                                'gc_card_no'=>NULL,
                                'gc_value'=>$row->unit_price,
                                'gc_expiry'=>date("Y-m-d", strtotime("+1 year")),
                            ];
                        }
                        if ($this->Settings->handle_jewerly_products) {
                            if ($row->based_on_gram_value != '0') {
                                $row->current_price_gram = $item->current_gold_price;
                            }
                        }
        
                        $pr[$ri] = array(
                            'id' => $c,
                            'item_id' => $row->id,
                            'label' => trim($row->name) . " (" . $row->code . ")",
                            'row' => $row,
                            'combo_items' => $combo_items,
                            'tax_rate' => $tax_rate,
                            'real_tax_rate' => $real_tax_rate,
                            'units' => $units,
                            'options' => $options,
                            'category' => $category,
                            'preferences' => $preferences,
                            'gift_card' => $gift_card,
                            'preferences_text' => $this->sma->print_preference_selection($item->preferences),
                            'preferences_selected' => json_decode($item->preferences ? $item->preferences : ""),
                            'state_readiness' => isset($item->state_readiness) ? $item->state_readiness : NULL,
                            'ordered_product_id' => $item->id
                        );
                        if (isset($max_order_id)) {
                            $pr[$ri]['order'] = $max_order_id;
                        }
                        $c++;
                    }
                    // $this->sma->print_arrays($pr);
                    $this->data['items'] = json_encode($pr);
            } else {
                $this->data['customer'] = $this->pos_model->getCompanyByID($this->pos_settings->default_customer);
                $this->data['reference_note'] = NULL;
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['message'] = isset($this->data['message']) ? $this->data['message'] : $this->session->flashdata('message');
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['user'] = $this->site->getUser();
            $this->data["tcp"] = 1;
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['new_customer_added'] = $this->input->get('new_customer_added') ? $this->input->get('new_customer_added') : NULL;
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['subcategories'] = $this->site->getSubCategories($this->pos_settings->default_section < 995 ? $this->pos_settings->default_section : NULL, false, true);
            $this->data['allsubcategories'] = $this->site->getSubCategories();
            $this->data['promotions'] = $this->site->getPromotions();
            $this->data['favorites'] = $this->pos_settings->allow_favorities == 0 ? false : $this->site->getFavorites();
            $this->data['featured'] = $this->Settings->big_data_limit_reports == 1 ? false : $this->site->getFeatured();
            $this->data['printer'] = $this->pos_model->getPrinterByID($this->pos_settings->printer);
            $this->data['count_suspended_bills'] = $this->pos_model->get_count_suspended_bills();
            $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }

            $order_printers = json_decode($this->pos_settings->order_printers);
            $printers = array();
            if (!empty($order_printers)) {
                foreach ($order_printers as $printer_id) {
                    $printers[] = $this->pos_model->getPrinterByID($printer_id);
                }
            }
            $this->data['order_printers'] = $printers;
            $this->data['pos_settings'] = $this->pos_settings;
            if ($this->pos_settings->after_sale_page && $saleid = $this->input->get('print', true)) {
                if ($inv = $this->pos_model->getInvoiceByID($saleid)) {
                    $this->load->helper('pos');
                    if (!$this->session->userdata('view_right')) {
                        $this->sma->view_rights($inv->created_by, true);
                    }
                    $this->data['rows'] = $this->pos_model->getAllInvoiceItems($inv->id);
                    $this->data['biller'] = $this->pos_model->getCompanyByID($inv->biller_id);
                    $this->data['customer'] = $this->pos_model->getCompanyByID($inv->customer_id);
                    $this->data['payments'] = $this->pos_model->getInvoicePayments($inv->id);
                    $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
                    $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
                    $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
                    $this->data['inv'] = $inv;
                    $this->data['print'] = $inv->id;
                    $this->data['created_by'] = $this->site->getUser($inv->created_by);
                }
            }

            $this->data['ISPOS'] = TRUE;
            if ($this->session->userdata('pos_post_processing')) {
                $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
            }
            $this->page_construct('pos/add', ['page_title' => 'POS'], $this->data);
        }
    }

    public function add_wholesale($sid = NULL, $quote = FALSE, $order = FALSE)
    {
        $this->sma->checkPermissions();
        if (!$this->pos_settings->default_biller || !$this->pos_settings->default_customer || !$this->pos_settings->default_section) {
            $this->session->set_flashdata('warning', lang('please_update_settings'));
            admin_redirect('pos/settings');
        }
        if ($this->session->userdata('register_cash_movements_with_another_user')) {
            if (!$this->pos_model->registerData($this->session->userdata('register_cash_movements_with_another_user'))) {
                $this->session->set_flashdata('error', lang('another_user_cash_not_open'));
                admin_redirect('welcome');
            }
        } else {
            if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register/1');
            }
        }
        if ($register = $this->pos_model->registerData($this->session->userdata('user_id'))) {
            $register_data = array('register_id' => $register->id, 'cash_in_hand' => $register->cash_in_hand, 'register_open_time' => $register->date);
            $this->session->set_userdata($register_data);
        }
        $this->data['sid'] = $this->input->get('suspend_id') ? $this->input->get('suspend_id') : $sid;
        $did = $this->input->post('delete_id') ? $this->input->post('delete_id') : NULL;
        $suspend = $this->input->post('suspend') ? TRUE : FALSE;
        $count = $this->input->post('count') ? $this->input->post('count') : NULL;
        $duplicate_sale = $this->input->get('duplicate') ? $this->input->get('duplicate') : NULL;
        $this->data['old_sale'] = NULL;
        $this->data['oid'] = NULL;
        if ($duplicate_sale) {
            if ($old_sale = $this->pos_model->getInvoiceByID($duplicate_sale)) {
                $inv_items = $this->pos_model->getSaleItems($duplicate_sale);
                $this->data['oid'] = $duplicate_sale;
                $this->data['old_sale'] = $old_sale;
                $this->data['message'] = lang('old_sale_loaded');
                $this->data['customer'] = $this->pos_model->getCompanyByID($old_sale->customer_id);
            } else {
                $this->session->set_flashdata('error', lang("bill_x_found"));
                admin_redirect("pos");
            }
        }
        $this->data['suspend_sale'] = NULL;
        $this->data['quote'] = NULL;
        if ($sid && !$quote && !$order) {
            if ($suspended_sale = $this->pos_model->getOpenBillByID($sid)) {
                $inv_items = $this->pos_model->getSuspendedSaleItems($sid);
                $this->data['sid'] = $sid;
                $this->data['suspend_sale'] = $suspended_sale;
                $this->data['message'] = lang('suspended_sale_loaded');
                $this->data['customer'] = $this->pos_model->getCompanyByID($suspended_sale->customer_id);
                $this->data['reference_note'] = $suspended_sale->suspend_note;
            } else {
                $this->session->set_flashdata('error', lang("bill_x_found"));
                admin_redirect("pos");
            }
        } else if ($sid && $quote == 1) {
            $inv_items = $this->sales_model->getAllQuoteItems($sid);
            $this->data['quote'] = $this->sales_model->getQuoteByID($sid);
            $this->data['customer'] = $this->pos_model->getCompanyByID($this->data['quote']->customer_id);
            $this->data['quote_id'] = $sid;
        } else if ($sid && $order == 1) {
            $inv_items = $this->site->getAllOrderItems($sid);
            $this->data['order'] = true;
            $this->data['quote'] = $this->sales_model->getOrderByID($sid);
            $this->data['customer'] = $this->pos_model->getCompanyByID($this->data['quote']->customer_id);
            $this->data['quote_id'] = $sid;
        }
        if (($sid || $duplicate_sale) && $inv_items) {
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    $category = $this->site->getCategoryByID($row->category_id);
                    $row->category_name = $category->name;
                    unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                }
                $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id = $item->product_id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->quantity += isset($this->data['order']) ? $item->quantity_to_bill : $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                $row->real_unit_price = $item->real_unit_price;
                $row->base_quantity = $item->quantity;
                $row->base_unit = isset($row->unit) ? $row->unit : $item->product_unit_id;
                $row->base_unit_price = $row->price ? $row->price : $item->unit_price;
                $row->unit = $item->product_unit_id;
                $row->qty = isset($this->data['order']) ? $item->quantity_to_bill : $item->quantity;
                $row->tax_rate = $item->tax_rate_id;
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;
                $options = $this->pos_model->getProductOptions($row->id, $item->warehouse_id);
                $row->comment = isset($item->comment) ? $item->comment : '';
                $row->ordered = 1;
                $combo_items = false;
                $row->prev_unit_price = $item->unit_price;
                if ($row->type == 'combo') {
                    $combo_items = $this->pos_model->getProductComboItems($row->id, $item->warehouse_id);
                }
                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $ri = $this->Settings->item_addition ? $row->id : $c;
                $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                $c++;
            }
            $this->data['items'] = json_encode($pr);
        } else {
            $this->data['customer'] = $this->pos_model->getCompanyByID($this->pos_settings->default_customer);
            $this->data['reference_note'] = NULL;
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['message'] = isset($this->data['message']) ? $this->data['message'] : $this->session->flashdata('message');
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        // $this->sma->print_arrays($this->data['billers']);
        $this->data['warehouses'] = $this->site->getAllWarehouses(1);
        $this->data['tax_rates'] = $this->site->getAllTaxRates();
        $this->data['user'] = $this->site->getUser();
        // $this->data["tcp"] = $this->pos_model->products_count($this->pos_settings->default_section);
        $this->data["tcp"] = 1;
        $this->data['products'] = $this->ajaxproducts($this->pos_settings->default_section);
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['subcategories'] = $this->site->getSubCategories($this->pos_settings->default_section < 995 ? $this->pos_settings->default_section : NULL);
        $this->data['promotions'] = $this->site->getPromotions();
        $this->data['printer'] = $this->pos_model->getPrinterByID($this->pos_settings->printer);
        $this->data['count_suspended_bills'] = $this->pos_model->get_count_suspended_bills();
        $order_printers = json_decode($this->pos_settings->order_printers);
        $printers = array();
        if (!empty($order_printers)) {
            foreach ($order_printers as $printer_id) {
                $printers[] = $this->pos_model->getPrinterByID($printer_id);
            }
        }
        $this->data['order_printers'] = $printers;
        $this->data['pos_settings'] = $this->pos_settings;
        $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;

        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }
        if ($this->pos_settings->after_sale_page && $saleid = $this->input->get('print', true)) {
            if ($inv = $this->pos_model->getInvoiceByID($saleid)) {
                $this->load->helper('pos');

                if (!$this->session->userdata('view_right')) {
                    $this->sma->view_rights($inv->created_by, true);
                }

                $this->data['rows'] = $this->pos_model->getAllInvoiceItems($inv->id);
                $this->data['biller'] = $this->pos_model->getCompanyByID($inv->biller_id);
                $this->data['customer'] = $this->pos_model->getCompanyByID($inv->customer_id);
                $this->data['payments'] = $this->pos_model->getInvoicePayments($inv->id);
                $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
                $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
                $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
                $this->data['inv'] = $inv;
                $this->data['print'] = $inv->id;
                $this->data['created_by'] = $this->site->getUser($inv->created_by);
            }
        }

        $meta = array('page_title' => 'POS');
        $this->data['ISPOSWS'] = TRUE;
        $this->page_construct('pos/add_wholesale', $meta, $this->data);
    }

    public function view_bill()
    {
        $this->sma->checkPermissions('index');
        $this->data['tax_rates'] = $this->site->getAllTaxRates();
        $this->load_view($this->theme . 'pos/view_bill', $this->data);
    }

    public function stripe_balance()
    {
        if (!$this->Owner && !$this->Admin) {
            return FALSE;
        }
        $this->load->admin_model('stripe_payments');

        return $this->stripe_payments->get_balance();
    }

    public function paypal_balance()
    {
        if (!$this->Owner && !$this->Admin) {
            return FALSE;
        }
        $this->load->admin_model('paypal_payments');

        return $this->paypal_payments->get_balance();
    }

    public function registers()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['registers'] = $this->pos_model->getOpenRegisters();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('pos'), 'page' => lang('pos')), array('link' => '#', 'page' => lang('open_registers')));
        $meta = array('page_title' => lang('open_registers'), 'bc' => $bc);
        $this->page_construct('pos/registers', $meta, $this->data);
    }

    public function open_register($from_pos = NULL)
    {
        $this->sma->checkPermissions('index');
        $this->form_validation->set_rules('cash_in_hand', lang("cash_in_hand"), 'greater_than_equal_to[0]');
        $prev_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : admin_url();
        if ($this->pos_model->getOpenRegisterByUserId($this->session->userdata('user_id'))) {
            $this->session->set_flashdata('error', lang("cannot_open_register_exists_opened"));
            $this->sma->md();
        }
        if ($this->form_validation->run() == TRUE) {

            $post_prev_url = $this->input->post('prev_url');
            $from_pos = $this->input->post('from_pos');
            $data = array(
                'date' => date('Y-m-d H:i:00'),
                'cash_in_hand' => $this->input->post('cash_in_hand'),
                'user_id'      => $this->session->userdata('user_id'),
                'status'       => 'open',
                );
        }
        if ($this->form_validation->run() == TRUE && $this->pos_model->openRegister($data)) {
            $this->session->set_flashdata('message', lang("welcome_to_pos"));
            if ($post_prev_url && !$from_pos) {
                redirect($post_prev_url);
            } else {
                admin_redirect("pos");
            }
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['prev_url'] = $prev_url;
            $this->data['from_pos'] = $from_pos;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('open_register')));
            $meta = array('page_title' => lang('open_register'), 'bc' => $bc);
            $this->page_construct('pos/open_register', $meta, $this->data);
        }
    }

    public function close_register($user_id = NULL, $register_id = NULL, $sync = false)
    {
        if (!($this->Admin || $this->Owner || $this->GP['pos-close_register'] || $this->GP['reports-register'])) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 10);</script>");
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->form_validation->set_rules('total_cash', lang("total_cash"), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('total_cheques', lang("total_cheques"), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('total_cc_slips', lang("total_cc_slips"), 'greater_than_equal_to[0]');
        // $this->sma->print_arrays($_POST);
        if ($this->form_validation->run() == TRUE) {
            $sync = $this->input->post('sync') ? 1 : 0;
            $rid = $this->input->post('rid') ? $this->input->post('rid') : 0;
            if ($sync) {
                $user_register = $this->pos_model->registerData(NULL, $rid);
                $user_id = $user_register->user_id;
            } else {
                if ($this->Owner || $this->Admin) {
                    $user_register = $user_id ? $this->pos_model->registerData($user_id) : NULL;
                    $rid = $user_register ? $user_register->id : $this->session->userdata('register_id');
                    $user_id = $user_register ? $user_register->user_id : $this->session->userdata('user_id');
                } else {
                    $rid = $this->session->userdata('register_id');
                    $user_id = $this->session->userdata('user_id');
                }
            }
            $note = $this->input->post('note');
           if ($sync) {
               $note.=" Sincronización hecha en ".date('Y-m-d H:i:s')." Por : ".$this->session->first_name." ".$this->session->last_name;
           }
            $data = array(
                'closed_at'                => date('Y-m-d H:i:s'),
                'total_cash'               => $this->input->post('total_cash'),
                'total_cheques'            => $this->input->post('total_cheques'),
                'total_cc_slips'           => $this->input->post('total_cc_slips'),
                'total_cash_submitted'     => $this->input->post('total_cash_submitted'),
                'total_cheques_submitted'  => $this->input->post('total_cheques_submitted'),
                'total_cc_slips_submitted' => $this->input->post('total_cc_slips_submitted'),
                'note'                     => $note,
                'status'                   => 'close',
                'transfer_opened_bills'    => $this->input->post('transfer_opened_bills'),
                'closed_by'                => $this->session->userdata('user_id'),
                'refunds'                   => $this->input->post('returns'),
                'expenses'                  => $this->input->post('expenses'),
                'deposits'                  => $this->input->post('deposits'),
                'purchases_payments'        => $this->input->post('ppayments'),
                'deposits_other_methods'    => $this->input->post('deposits_other_methods'),
                'suppliers_deposits'        => $this->input->post('suppliers_deposits'),
                'tips_cash'                 => $this->input->post('tips_cash'),
                'tips_other_methods'        => $this->input->post('tips_other_methods'),
                'tips_due'                  => $this->input->post('tips_due'),
                'shipping_cash'             => $this->input->post('shipping_cash'),
                'shipping_other_methods'    => $this->input->post('shipping_other_methods'),
                'shipping_due'              => $this->input->post('shipping_due'),
                'movements_in'              => $this->input->post('movements_in'),
                'movements_out'              => $this->input->post('movements_out'),
                'total_retention'              => $this->input->post('total_retention'),
                'total_return_retention'       => $this->input->post('total_return_retention'),
                'total_rc_retention'         => $this->input->post('total_rc_retention'),
                'gc_topups_cash'         => $this->input->post('gc_topups_cash'),
                'gc_topups_other'         => $this->input->post('gc_topups_other'),
                );
            $popts = $this->input->post('popt_id');
            $popt_cash_counted = $this->input->post('popt_cash_counted');
            $popt_description = $this->input->post('popt_description');
            $popt_payments = $this->input->post('popt_payments');
            $popt_sales = $this->input->post('popt_sales');
            $popt_returns = $this->input->post('popt_returns');
            $popt_payments_collections = $this->input->post('popt_payments_collections');
            $popt_sold_gift_cards = $this->input->post('popt_sold_gift_cards');
            $popt_returned_gift_cards = $this->input->post('popt_returned_gift_cards');
            $popt_paid_installments = $this->input->post('popt_paid_installments');
            $aa = count($popts)-1;
            $items = [];
            for ($i=0; $i <= $aa ; $i++) {
                $items[] = array(
                                'pos_register_id'       => $rid,
                                'payment_method_id'     => $popts[$i] != 'false' ? $popts[$i] : NULL,
                                'description'           => $popt_description[$i],
                                'payment_counted'       => $popt_cash_counted[$i],
                                'payments_amount'       => (Double) $popt_payments[$i],
                                'sales_amount'          => (Double) $popt_sales[$i],
                                'devolutions_amount'    => (Double) $popt_returns[$i],
                                'total_amount'          => (Double) $popt_payments[$i] + (Double) $popt_sales[$i] + (Double) $popt_returns[$i],
                                'payments_collections_amount'       => (Double) $popt_payments_collections[$i],
                                'sold_gift_cards'       => (Double) $popt_sold_gift_cards[$i],
                                'returned_gift_cards'       => (Double) $popt_returned_gift_cards[$i],
                                'paid_installments'       => (Double) $popt_paid_installments[$i],
                                );
            }
            // $this->sma->print_arrays($items);
            $copts = $this->input->post('category_id');
            $category_description = $this->input->post('category_description');
            $category_quantity = $this->input->post('category_quantity');
            $category_payments = $this->input->post('category_payments');
            $category_sales = $this->input->post('category_sales');
            $category_returns = $this->input->post('category_returns');
            $ca = $copts ? count($copts)-1 : 1;
            for ($i=0; $i <= $ca ; $i++) {
                if (isset($copts[$i])) {
                    $items[] = array(
                                    'pos_register_id'       => $rid,
                                    'category_id'           => $copts[$i],
                                    'description'           => $category_description[$i],
                                    'category_quantity'     => (Double) $category_quantity[$i],
                                    'payments_amount'       => (Double) $category_payments[$i],
                                    'sales_amount'          => (Double) $category_sales[$i],
                                    'devolutions_amount'    => (Double) $category_returns[$i],
                                    'total_amount'          => (Double) $category_payments[$i] + (Double) $category_sales[$i] + (Double) $category_returns[$i],
                                    );
                }

            }

            $expenses_categories = $this->input->post('expense_category_id');
            $expense_category_description = $this->input->post('expense_category_description');
            $expense_category_amount = $this->input->post('expense_category_amount');
            $ca = $expenses_categories ? count($expenses_categories)-1 : 1;
            for ($i=0; $i <= $ca ; $i++) {
                if (isset($expenses_categories[$i])) {
                    $items[] = array(
                                    'pos_register_id'       => $rid,
                                    'expense_category_id'   => $expenses_categories[$i],
                                    'description'           => $expense_category_description[$i],
                                    'total_amount'          => (Double) $expense_category_amount[$i],
                                    );
                }
            }
            // $this->sma->print_arrays($items);
        } elseif ($this->input->post('close_register')) {
            $this->session->set_flashdata('error', (validation_errors() ? validation_errors() : $this->session->flashdata('error')));
            admin_redirect("pos");
        }
        if ($this->form_validation->run() == TRUE && $this->pos_model->closeRegister($rid, $user_id, $data, $items, $sync)) {
            $this->session->unset_userdata('register_open_time');
            $this->session->unset_userdata('cash_in_hand');
            $this->session->unset_userdata('register_id');
            $this->session->set_flashdata('message', lang("register_closed"));
            admin_redirect("pos/register_details/".$rid."/0");
        } else {
            if (validation_errors()) {
                exit(var_dump(validation_errors()));
            }
            if ($register_id && $sync) {
                $user_register = $this->pos_model->registerData(NULL, $register_id);
                $register_open_time = $user_register->date;
                $this->data['cash_in_hand'] = $user_register->cash_in_hand;
                $this->data['note'] = $user_register->note;
                $this->data['register_open_time'] = $register_open_time;
                $this->pos_model->close_register_end_date = $user_register->closed_at ? $user_register->closed_at : date('Y-m-d H:i:s');
                $user_id = $user_register->user_id;
                $this->data['sync'] = $sync;
                $this->data['rid'] = $register_id;
            } else {
                if ($this->Owner || $this->Admin) {
                    $user_register = $user_id ? $this->pos_model->registerData($user_id) : NULL;
                    $register_open_time = $user_register ? $user_register->date : NULL;
                    $this->data['cash_in_hand'] = $user_register ? $user_register->cash_in_hand : NULL;
                    $this->data['register_open_time'] = $user_register ? $register_open_time : NULL;
                } else {
                    $register_open_time = $this->session->userdata('register_open_time');
                    $this->data['cash_in_hand'] = $this->session->userdata('cash_in_hand');
                    $this->data['register_open_time'] = $register_open_time;
                }
                $this->pos_model->close_register_end_date = date('Y-m-d H:i:s');
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            // //Credit Card Sales - Payments - Returns
            $this->data['ccsales'] = $this->pos_model->getRegisterCCSales($register_open_time, $user_id);
            // //Cheque Sales - Payments - Returns
            $this->data['chsales'] = $this->pos_model->getRegisterChSales($register_open_time, $user_id);
            $popts = $this->site->getPaidOpts(1,0);
            $this->data['popts'] = $popts;
            foreach ($popts as $popt) {
                $this->data['popts_data'][$popt->code]['Sales'] = $this->pos_model->getSalesByPaymentOption($popt->code, $register_open_time, $user_id);
                $this->data['popts_data'][$popt->code]['RC'] = $this->pos_model->getRegisterSalesRCByPaymentOption($popt->code, $register_open_time, $user_id);
                $this->data['popts_data'][$popt->code]['Returned'] = $this->pos_model->getRegisterReturnedSalesByPaymentOption($popt->code, $register_open_time, $user_id);
                $this->data['popts_data'][$popt->code]['payments_collections'] = $this->pos_model->getPaymentsCollectionsByPaymentOption($popt->code, $register_open_time, $user_id);
                $this->data['popts_data'][$popt->code]['sold_gift_cards'] = $this->pos_model->getGiftCardsSoldByPaymentOption($popt->code, $register_open_time, $user_id);
                $this->data['popts_data'][$popt->code]['returned_gift_cards'] = $this->pos_model->getGiftCardsReturnedByPaymentOption($popt->code, $register_open_time, $user_id);
                $this->data['popts_data'][$popt->code]['paid_installments'] = $this->pos_model->getPaidInstallmentsByPaymentOption($popt->code, $register_open_time, $user_id);
                // $this->sma->print_arrays($this->data['popts_data'][$popt->code]['Sales']);
                $this->data['popts_data'][$popt->code]['num_transactions'] = 
                ( $this->data['popts_data'][$popt->code]['Sales'] ? $this->data['popts_data'][$popt->code]['Sales']->num_transactions  : 0 )+
                ( $this->data['popts_data'][$popt->code]['RC'] ? $this->data['popts_data'][$popt->code]['RC']->num_transactions  : 0 )+
                ( $this->data['popts_data'][$popt->code]['Returned'] ? $this->data['popts_data'][$popt->code]['Returned']->num_transactions  : 0 )+
                ( $this->data['popts_data'][$popt->code]['payments_collections'] ? $this->data['popts_data'][$popt->code]['payments_collections']->num_transactions  : 0 )+
                ( $this->data['popts_data'][$popt->code]['sold_gift_cards'] ? $this->data['popts_data'][$popt->code]['sold_gift_cards']->num_transactions  : 0 )+
                ( $this->data['popts_data'][$popt->code]['returned_gift_cards'] ? $this->data['popts_data'][$popt->code]['returned_gift_cards']->num_transactions  : 0 )+
                ( $this->data['popts_data'][$popt->code]['paid_installments'] ? $this->data['popts_data'][$popt->code]['paid_installments']->num_transactions : 0 );
            }
            $this->data['duesales'] = $this->pos_model->getRegisterDueSales($register_open_time, $user_id);
            $this->data['duereturns'] = $this->pos_model->getRegisterDueReturns($register_open_time, $user_id);
            //Expenses
            $this->data['expenses'] = $this->pos_model->getRegisterExpenses($register_open_time, $user_id);
            $this->data['purchases_expenses'] = $this->pos_model->getRegisterPurchasesExpenses($register_open_time, $user_id);
            $this->data['purchases_expenses_payments'] = $this->pos_model->getRegisterPurchasesExpensesPayments($register_open_time, $user_id);
            //Purchases payments
            $this->data['ppayments'] = $this->pos_model->getPurchasesPayments($register_open_time, $user_id);
            $this->data['gc_topups_cash'] = $this->pos_model->get_gift_card_topups($register_open_time, $user_id);
            $this->data['gc_topups_other'] = $this->pos_model->get_gift_card_topups($register_open_time, $user_id, null, null, 'other');
            //Deposits
            $this->data['customer_deposits'] = $this->pos_model->getRegisterDeposits($register_open_time, $user_id);
            $this->data['customer_deposits_other_payments'] = $this->pos_model->getRegisterDepositsOtherPayments($register_open_time, $user_id);
            $this->data['supplier_deposits'] = $this->pos_model->getRegisterSupplierDeposits($register_open_time, $user_id);
            $this->data['pos_register_movements'] = $this->pos_model->getRegisterMovements($register_open_time, $user_id);
            if ($this->pos_settings->detail_sale_by_category == 1 || $this->pos_settings->detail_sale_by_category == 3) {
                $categories = $this->pos_model->getSalesByCategory(NULL, $register_open_time, $user_id);
                $this->data['categories'] = $categories;
            } else if ($this->pos_settings->detail_sale_by_category == 2 || $this->pos_settings->detail_sale_by_category == 4) {
                $r_products = $this->pos_model->getSalesByProduct(NULL, $register_open_time, $user_id);
                $this->data['r_products'] = $r_products;
            }
            if ($this->pos_settings->detail_sale_by_category == 3 || $this->pos_settings->detail_sale_by_category == 4 || $this->pos_settings->detail_sale_by_category == 5) {
                $expense_categories = $this->pos_model->getExpensesByCategory($register_open_time, $user_id);
                $this->data['expense_categories'] = $expense_categories;
            }
            $tipping_cash_data = $this->pos_model->getTipsByCash($register_open_time, $user_id);
            $tipping_other_data = $this->pos_model->getTipsByOtherPayments($register_open_time, $user_id,
                (isset($tipping_cash_data['sale_tipping']) ? $tipping_cash_data['sale_tipping'] : NULL),
                (isset($tipping_cash_data['payment_method_tipping']) ? $tipping_cash_data['payment_method_tipping'] : NULL)
            );
            $tipping_due_data = $this->pos_model->getTipsByDue($register_open_time, $user_id,
                (isset($tipping_other_data['sale_tipping']) ? $tipping_other_data['sale_tipping'] : NULL),
                (isset($tipping_other_data['payment_method_tipping']) ? $tipping_other_data['payment_method_tipping'] : NULL)
            );
            $this->data['tips_cash'] = (object) ['amount'=> isset($tipping_cash_data['amount']) ? $tipping_cash_data['amount'] : 0];
            $this->data['tips_other_payments'] = (object) ['amount'=> isset($tipping_other_data['amount']) ? $tipping_other_data['amount'] : 0];
            $this->data['tips_due'] = (object) ['amount'=> isset($tipping_due_data['amount']) ? $tipping_due_data['amount'] : 0];
            $this->data['payment_method_tipping'] = isset($tipping_due_data['payment_method_tipping']) ? $tipping_due_data['payment_method_tipping'] : NULL;
            $shipping_cash_data = $this->pos_model->getShippingByCash($register_open_time, $user_id);
            $shipping_other_data = $this->pos_model->getShippingByOtherPayments($register_open_time, $user_id,
                (isset($shipping_cash_data['sale_shipping']) ? $shipping_cash_data['sale_shipping'] : NULL),
                (isset($shipping_cash_data['payment_method_shipping']) ? $shipping_cash_data['payment_method_shipping'] : NULL)
            );
            $shipping_due_data = $this->pos_model->getShippingByDue($register_open_time, $user_id,
                (isset($shipping_other_data['sale_shipping']) ? $shipping_other_data['sale_shipping'] : NULL),
                (isset($shipping_other_data['payment_method_shipping']) ? $shipping_other_data['payment_method_shipping'] : NULL)
            );
            $this->data['shipping_cash'] = (object) ['amount'=> isset($shipping_cash_data['amount']) ? $shipping_cash_data['amount'] : 0];
            $this->data['shipping_other_payments'] = (object) ['amount'=> isset($shipping_other_data['amount']) ? $shipping_other_data['amount'] : 0];
            $this->data['shipping_due'] = (object) ['amount'=> isset($shipping_due_data['amount']) ? $shipping_due_data['amount'] : 0];
            $this->data['payment_method_shipping'] = isset($shipping_due_data['payment_method_shipping']) ? $shipping_due_data['payment_method_shipping'] : NULL;
            $this->data['totalsales'] = $this->pos_model->getRegisterSales($register_open_time, $user_id);
            $this->data['users'] = $this->pos_model->getUsers($user_id);
            $this->data['suspended_bills'] = $this->pos_model->getSuspendedsales($user_id);
            $this->data['user_id'] = $user_id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['v'] = $this->input->get('v');
            $this->data['m'] = $this->input->get('m');
            $this->data['register_movements'] = $this->pos_model->get_pos_register_movements($register_open_time, $user_id);
            if ($this->session->userdata('biller_id') != null) {
                $this->data['biller_name'] = $this->pos_model->getBillerById($this->session->userdata('biller_id'));
            }
            $this->load_view($this->theme . 'pos/close_register', $this->data);
        }
    }

    public function getProductDataByCode($code = NULL, $warehouse_id = NULL, $id = NULL)
    {
        $this->sma->checkPermissions('index');
        $var = null;
        $warehouse_id = null;
        $customer_id = null;
        $biller_id = null;
        $address_id = null;
        if ($this->input->get('code')) {
            $code = $this->input->get('code', TRUE);
        }
        if ($this->input->get('id')) {
            $id = $this->input->get('id', TRUE);
        }
        if ($this->input->get('warehouse_id')) {
            $warehouse_id = $this->input->get('warehouse_id', TRUE);
        }
        if ($this->input->get('customer_id')) {
            $customer_id = $this->input->get('customer_id', TRUE);
        }
        if ($this->input->get('biller_id')) {
            $biller_id = $this->input->get('biller_id', TRUE);
        }
        if ($this->input->get('address_id')) {
            $address_id = $this->input->get('address_id', TRUE);
        }
        if (!$code && !$id) {
            echo NULL;
            die();
        }
        $warehouse = $this->site->getWarehouseByID($warehouse_id);
        $customer = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $biller = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : false;
        $row = $this->pos_model->getWHProduct($code, $warehouse_id, $id);
        $option = false;

        if ($row) {
            $label_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $unit_price_id = null);

            if ($row->tax_method == 0 && $this->Settings->ipoconsumo) {
                $consumption_sale_tax = $row->consumption_sale_tax;
                $label_price['new_price'] = $label_price['new_price'] + $consumption_sale_tax;
            }

            $label_price = $label_price ? "(".$this->sma->formatMoney($label_price['new_price']).")" : "";
            $c = mt_rand();
            unset($row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
            $option = false;
            $row->item_tax_method = $row->tax_method;
            $row->qty = $row->paste_balance_value == 1 ? 0 : 1;
            $row->discount = '0';
            $row->serial = '';
            $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);

            $opt = json_decode('{}');
            $opt->price = 0;
            $option_id = FALSE;

            $row->option = $option_id;
            $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
            if ($pis) {
                $row->quantity = 0;
                foreach ($pis as $pi) {
                    $row->quantity += $pi->quantity_balance;
                }
            }

            if ($this->Settings->product_preferences_management == 1) {
                $preferences = $this->sales_model->getProductPreferences($row->id);
            } else {
                $preferences = false;
            }

            $data_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $row->sale_unit);
            $row->price = $data_price['new_price'];
            $row->discount = $data_price['new_discount']."%";

            if (!$this->sma->isPromo($row)) {
                $row->promotion = 0;
            }
            $row->base_quantity = $row->paste_balance_value == 1 ? 0 : 1;

            // Wappsi carne
            if ($this->pos_settings->balance_settings == 2) {
                // if($peso > 0){
                //     $row->base_quantity = $peso;
                // }
            }
            // Termina Wappsi carne

            $row->profitability_margin = $row->profitability_margin;

            $row->base_unit = $row->unit;
            $row->base_unit_price = $row->price;
            $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
            $row->comment = '';
            $combo_items = false;
            if ($row->type == 'combo') {
                $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
            }
            $units = $this->site->get_product_units($row->id);
            $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
            $cnt_units_prices =  $this->site->get_all_product_units_prices($row->id) ? count($this->site->get_all_product_units_prices($row->id))+1 : 1;
            $row->cnt_units_prices = $cnt_units_prices;
            if ($cnt_units_prices == 1 && $this->Settings->prioridad_precios_producto == 10) {
                $row->product_unit_id_selected = $row->unit;
            }
            if ($row->tax_method == 1) {
                $tax = $this->sma->calculateTax($row->tax_rate, $row->price, 0);
                $row->real_unit_price = $row->price + $tax;
                $row->unit_price = $row->price + $tax;
            } else {
                $row->real_unit_price = $row->price;
                $row->unit_price = $row->price;
            }
            $row->price_before_tax = $row->real_unit_price / (($tax_rate->rate / 100)+1);

            $pr_name = $row->name . " (" . $row->code . ")";
            if ($this->Settings->show_brand_in_product_search && isset($row->brand_name) && $row->brand_name) {
                $pr_name.=" - ".$row->brand_name;
            }

            $ri = $this->Settings->item_addition ? $row->id : $c;

            $category = $this->site->getCategoryById($row->category_id);
            $subcategory = $this->site->getCategoryById($row->subcategory_id);
            $row->except_category_taxes = false;
            if ($this->sma->validate_except_category_taxes($category, $subcategory)) {
                $row->tax_rate = $this->Settings->category_tax_exception_tax_rate_id;
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $row->except_category_taxes = true;
            }
            $hybrid_prices = false;
            if ($this->Settings->prioridad_precios_producto == 11) { //híbrido
                $hybrid_prices = $this->site->get_all_product_hybrid_prices($row->id);
                $hybrid_prices = $hybrid_prices[$row->id];
            }

            if (!$this->Settings->ipoconsumo) {
                $row->consumption_sale_tax = 0;
            }

            $pr = array('id' => $ri, 'item_id' => $row->id, 'label' => $pr_name.$label_price, 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'options' => $options, 'preferences' => $preferences, 'category' => $category, 'subcategory' => $subcategory, 'hybrid_prices' => $hybrid_prices);
            $this->sma->send_json($pr);
        } else {
            echo NULL;
        }
    }

    public function ajaxproducts($category_id = NULL, $brand_id = NULL, $biller_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('index');
        if ($this->input->get('brand_id')) {
            $brand_id = $this->input->get('brand_id');
        }
        if ($this->input->get('category_id')) {
            $category_id = $this->input->get('category_id');
        }
        if ($this->input->get('subcategory_id')) {
            $subcategory_id = $this->input->get('subcategory_id');
        } else {
            $subcategory_id = NULL;
        }
        if ($this->input->get('per_page') == 'n') {
            $page = 0;
        } else {
            $page = $this->input->get('per_page');
        }

        $this->load->library("pagination");

        $config = array();
        $config["base_url"] = base_url() . "pos/ajaxproducts";
        $config["per_page"] = $this->pos_settings->pro_limit;
        $config['prev_link'] = FALSE;
        $config['next_link'] = FALSE;
        $config['display_pages'] = FALSE;
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        if ($category_id == 998) {
            $products = $this->site->getPromotions($biller_id, $warehouse_id);
        } else if ($category_id == 999) {
            if ($this->pos_settings->allow_favorities == 1) {
                $products = $this->site->getFavorites($biller_id, $warehouse_id);
            } else {
                $products = false;
            }
        } else if ($category_id == 997) {
            $products = $this->site->getFeatured($biller_id, $warehouse_id);
        } else {
            $products = $this->pos_model->fetch_products($category_id, $config["per_page"], $page, $subcategory_id, $brand_id, $biller_id, $warehouse_id);
        }
        $config["total_rows"] = $products ? count($products) : 0;
        $this->pagination->initialize($config);

        $pro = 1;
        $prods = '<div>';
        if (!empty($products)) {
            foreach ($products as $product) {

                if ($product->hide_pos == 1) {
                    continue;
                }

                $count = $product->id;
                if ($count < 10) {
                    $count = "0" . ($count / 100) * 100;
                }
                if ($category_id < 10) {
                    $category_id = "0" . ($category_id / 100) * 100;
                }

                $show_price_favorite_products = false;
                if ($this->pos_settings->show_price_favorite_products == 1) {
                    $show_price_favorite_products = true;
                }

                $prods .="<div class='btn-prni product btn-white' data-code='". $product->code ."'>
                            ".($this->sma->isNew($product) ? '<div class="icon_promo"><img class="promo_pos_img" src="'.base_url('assets/images/new_product.png').'"></div>' : '')."
                            <div class='pos-product-img'>
                            ".($this->sma->isPromo($product) && $show_price_favorite_products ? '<div class="icon_promo"><img class="promo_pos_img" src="'.base_url('assets/images/promo_tag.png').'"></div>' : '')."
                            ".($product->quantity <= 0 ? '<div class="icon_promo"><img class="promo_pos_img" src="'.base_url('assets/images/no_stock.png').'"></div>' : '')."

                                <img src=\"" . $this->sma->get_img_url($product->image, true) . "\" alt=\"" . $product->name . "\" class='img-rounded' loading='lazy' />
                                ".($show_price_favorite_products ? ("<p class='fav_products_price ".($this->sma->isPromo($product) ? "pos_promo_price" : "")." ".($this->sma->isNew($product) ? "pos_new_price" : "")." ".($product->quantity <= 0 ? "pos_outstock_price" : "")." '>".$this->sma->formatMoney($this->sma->isPromo($product) ? $product->promo_price : $product->price)."</p>") : "")."
                            </div>
                            <div class='pos-product-name'>
                                " . ucfirst(mb_strtolower(character_limiter($product->name,  40))) . "
                            </div>
                          </div>";

                $pro++;
            }
        }
        $prods .= "</div>";

        if ($this->input->get('per_page')) {
            echo $prods;
        } else {
            return $prods;
        }
    }

    public function ajaxcategorydata($category_id = NULL)
    {
        $this->sma->checkPermissions('index');
        if ($this->input->get('category_id')) {
            $category_id = $this->input->get('category_id');
        } else {
            $category_id = $this->pos_settings->default_section;
        }
        $biller_id = $this->input->get('biller_id');
        $warehouse_id = $this->input->get('warehouse_id');
        $scats = '';
        if (isset($category_id) && $category_id < 995 && $subcategories) {
            $subcategories = $this->site->getSubCategories($category_id);
            foreach ($subcategories as $category) {
                $scats .= "<button id=\"subcategory-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn-prni subcategory btn-white\" >
                                                                    <div class=\"pos-product-img\">
                                                                        <img src=\"" . $this->sma->get_img_url($category->image, true) . "\" class='img-rounded' loading='lazy' />
                                                                    </div>
                                                                    <div class=\"pos-product-name\">
                                                                        <span>" . $category->name . "</span>
                                                                    </div>
                                                                </button>";
            }
        }

        $products = $this->ajaxproducts($category_id, NULL, $biller_id, $warehouse_id);

        if (!($tcp = $this->pos_model->products_count($category_id))) {
            $tcp = 0;
        }

        $this->sma->send_json(array('products' => $products, 'subcategories' => $scats, 'tcp' => $tcp));
    }

    public function ajaxbranddata($brand_id = NULL)
    {
        $this->sma->checkPermissions('index');
        if ($this->input->get('brand_id')) {
            $brand_id = $this->input->get('brand_id');
        }

        $products = $this->ajaxproducts(FALSE, $brand_id);

        if (!($tcp = $this->pos_model->products_count(FALSE, FALSE, $brand_id))) {
            $tcp = 0;
        }

        $this->sma->send_json(array('products' => $products, 'tcp' => $tcp));
    }

    public function view($sale_id = NULL, $modal = FALSE, $redirect_to_pos = FALSE, $internal_download = FALSE, $for_email = FALSE)
    {
        if ($this->session->userdata('print_pos_finalized_invoice') == 1 || ($this->session->userdata('print_pos_finalized_invoice') == 0 && $modal)) {
            $this->pos_model->update_print_status($sale_id);
        }
        $this->sma->checkPermissions('index');
        if ($this->session->userdata('pos_submit_target')) {
            $this->data['submit_target'] = $this->session->userdata('pos_submit_target');
            $this->session->unset_userdata('pos_submit_target');
            // exit(var_dump($this->data['submit_target']));
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->load->helper('pos');
        if ($this->input->get('id')) {
            $sale_id = $this->input->get('id');
        }
        $inv = $this->pos_model->getInvoiceByID($sale_id);
        // if (!$this->session->userdata('view_right')) {
        //     $this->sma->view_rights($inv->created_by, true);
        // }
        $this->data['message'] = $this->session->flashdata('message');
        $biller_id = $inv->biller_id;
        $customer_id = $inv->customer_id;
        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($biller_id);
        $this->data['customer'] = $this->pos_model->getCompanyByID($customer_id);
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['payments'] = $this->pos_model->getInvoicePayments($sale_id);
        $this->data['pos'] = $this->pos_model->getSetting();
        $this->data['barcode'] = $this->barcode($inv->reference_no, 'code128', 30);
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) :  $this->pos_model->getAllInvoiceItems($inv->sale_id);
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
        $this->data['inv'] = $inv;
        $this->data['sid'] = $sale_id;
        $this->data['modal'] = $modal;
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['biller_categories_concession'] = $this->companies_model->get_all_biller_categories_concessions($biller_id);
        $this->data['printer'] = $this->pos_model->getPrinterByID($this->pos_settings->printer);
        $this->data['page_title'] = $this->lang->line("invoice");
        $this->data['sale_id'] = $sale_id;
        $this->data['direct_print_job_id'] = NULL;
        $this->data['seller'] = $this->site->getCompanyByID($inv->seller_id);
        $this->data['redirect_to_pos'] = $redirect_to_pos ? true : false;
        $this->data['document_type'] = $this->site->getDocumentTypeById($inv->document_type_id);
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) { $taxes_details[$tax->id] = $tax->rate; }
        $this->data['taxes_details'] = $taxes_details;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = $this->Settings->tipo_regimen ? lang($tipo_regimen->description) : '';
        $view_tax = true;
        $tax_inc = true;
        $this->data['view_tax'] = $view_tax;
        $this->data['tax_inc'] = $tax_inc;
        $this->data['sma'] = $this->sma;
        $trmrate = 1;
        $this->data['trmrate'] = $trmrate;
        $this->data['signature_root'] = is_file("assets/uploads/signatures/".$this->Settings->digital_signature) ? base_url().'assets/uploads/signatures/'.$this->Settings->digital_signature : false;
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['invoice_header'] = $this->site->getInvoiceHeader($inv->document_type_id, $inv->reference_no);
        $this->data['show_document_type_header'] = 1;
        $this->data['filename'] = $this->site->getFilename($sale_id);
        $this->data['download'] = false;
        $this->data['for_email'] = $for_email;
        $ciius_rows = $this->site->get_ciiu_code_by_ids(explode(",", $this->Settings->ciiu_code));
        $ciius = "";
        if ($ciius_rows) {
            foreach ($ciius_rows as $ciiu) {
                $ciius.= $ciiu->code.", ";
            }
        }
        $this->data['ciiu_code'] = trim($ciius, ", ");
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;
        $this->data['print_directly'] = $print_directly;

        $quickPrintFormatId = $inv->quick_print_format_id;

        if (!empty($quickPrintFormatId)) {
            $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->quick_print_format_id);
        } else {
            if ($modal == 1) {
                $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->quick_print_format_id);
            } else {
                $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
            }
        }
        $this->data['quickPrintFormatId'] = $quickPrintFormatId;
        // $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
        $this->data['rows'] = $this->pos_model->getAllInvoiceItems($sale_id, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));

        $this->createQr($inv);

        if (!empty($inv->fe_xml) && $inv->technology_provider != SIMBA) {
            $xml_file = base64_decode($inv->fe_xml);

            $xml = new DOMDocument("1.0", "ISO-8859-15");
            $xml->loadXML($xml_file);

            $path = 'files/electronic_billing';
            if (!file_exists($path)) {
                mkdir($path, 0777);
            }
            $xml->save('files/electronic_billing/' . $inv->reference_no . '.xml');

            $positionDate = strpos($xml_file, '<cbc:ValidationDate>');
            $substringDate = substr($xml_file, $positionDate);
            $substringDate2 = substr($substringDate, 20, 10);

            $positionHour = strpos($xml_file, '<cbc:ValidationTime>');
            $substringHour = substr($xml_file, $positionHour);
            $substringHour2 = substr($substringHour, 20, 8);

            $validationDateTime = $substringDate2 .' '. $substringHour2;

            $this->data['validationDateTime'] = $validationDateTime;

            if (file_exists(FCPATH. 'files/electronic_billing/'.$inv->reference_no.'.xml')) {
                unlink(FCPATH. 'files/electronic_billing/'.$inv->reference_no.'.xml');
            }
        } else {
            $this->data['validationDateTime'] = '';
            if (isset($inv->fe_validation_dian) && $inv->fe_validation_dian !== null) {
                $this->data['validationDateTime'] = $inv->fe_validation_dian;
            }
        }

        $original_payment_method = '';
        $original_payment_method_with_amount = '';
        $total_original_payments_amount = 0;
        $direct_payments_num = 0;
        $payments = $this->sales_model->getPaymentsForSale($sale_id);
        if ($payments) {
            foreach ($payments as $payment) {
                if ($payment->paid_by == "retencion") {
                    $total_original_payments_amount += $payment->amount;
                }
                if (date('Y-m-d H', strtotime($payment->date)) == date('Y-m-d H', strtotime($inv->date)) && $payment->paid_by != "retencion") {
                    if ($original_payment_method == '') {
                        $original_payment_method = lang($payment->paid_by) . ", ";
                    } else {
                        $original_payment_method .= lang($payment->paid_by) . ", ";
                    }
                    if ($original_payment_method_with_amount == '') {
                        $original_payment_method_with_amount = lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    } else {
                        $original_payment_method_with_amount .= lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    }
                    $direct_payments_num++;
                    $total_original_payments_amount += $payment->amount;
                }
            }
            if ($direct_payments_num > 1) {
                $inv->note .= $original_payment_method_with_amount;
                $original_payment_method = 'Varios, ver nota';
            } else {
            }
        }
        if ($original_payment_method != '') {
            $original_payment_method = trim($original_payment_method, ", ");
        }
        if ($original_payment_method == '' || ($total_original_payments_amount < $inv->grand_total)) {
            $original_payment_method .= ($original_payment_method != '' ? ", " : "") . lang('due') . " " . $inv->payment_term . ($inv->payment_term > 1 ? " Días" : " Día");
        }

        $currencies = $this->site->getAllCurrencies();
        $currencies_names = [];
        if ($currencies) {
            foreach ($currencies as $currency) {
                $currencies_names[$currency->code] = $currency->name;
            }
        }
        $this->data['currencies_names'] = $currencies_names;
        $this->data['sale_payment_method'] = $original_payment_method;
        if ($print_directly == 1) {
            if (empty($this->pos_settings->cloud_print_mail) || empty($this->pos_settings->cloud_print_json_name) || empty($this->pos_settings->cloud_print_service_name)) {
                $this->session->set_flashdata('error', lang("invalid_google_cloud_print_settings"));
                $this->load_view($this->theme.'pos/settings', $this->data);
            }
            $view = $this->load_view($this->theme . 'pos/view', $this->data, true);
            $new_file_path = FCPATH.'assets/print_directly-'.date('Y-m-d-H-i-s');
            file_put_contents($new_file_path, $view);
            $file = fopen($new_file_path, "w");
            fwrite($file, $view . PHP_EOL);
            fclose($file);
            $direct_printing_response = $this->direct_printing($new_file_path);
            unlink($new_file_path);

            if ($direct_printing_response->success) {
                $this->data['direct_print_job_id'] = $direct_printing_response->job->id;
                $this->load_view($this->theme . 'pos/view', $this->data);
            } else {
                $this->session->set_flashdata('error', lang("there_is_a_problem_with_direct_printing"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $url_format = "pos/view";
            if ($inv->sale_status == 'returned') {
                $url_format = "pos/return_view";
            }
            $this->data['document_type_invoice_format'] = false;
            $this->data['qty_decimals'] = $this->Settings->decimals;
            $this->data['value_decimals'] = $this->Settings->qty_decimals;
            $this->data['biller_logo'] = 2;
            $this->data['tax_indicator'] = 0;
            $this->data['product_detail_promo'] = 1;
            $this->data['show_code'] = 1;
            $this->data['show_award_points'] = 1;
            $this->data['show_product_preferences'] = 1;
            $this->data['product_detail_font_size'] = 0;
            $this->data['print_copies'] = 0;
            $this->data['internal_download'] = $internal_download;
            if ($document_type_invoice_format) {
                if ($document_type_invoice_format->print_copies > 0 && ($this->session->userdata('pos_print_copies') == NULL || $this->session->userdata('pos_print_copies') > 0)) {
                    if (!$this->session->userdata('pos_print_copies')) {
                        $this->session->set_userdata('pos_print_copies', $document_type_invoice_format->print_copies);
                        $this->data['print_copies'] = $document_type_invoice_format->print_copies;
                    } else {
                        $pos_print_copies = $this->session->userdata('pos_print_copies') - 1;
                        $this->session->set_userdata('pos_print_copies', $pos_print_copies);
                        $this->data['print_copies'] = $pos_print_copies;
                    }
                }
                $this->data['document_type_invoice_format'] = $document_type_invoice_format;
                if ($document_type_invoice_format->show_logo == 0) {
                    $this->data['biller']->logo = NULL;
                }
                if ($document_type_invoice_format->show_seller== 0) {
                    $this->data['seller'] = NULL;
                }
                $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
                $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
                $this->data['biller_logo'] = $document_type_invoice_format->logo;
                $this->data['tax_indicator'] = $document_type_invoice_format->tax_indicator;
                $this->data['product_detail_promo'] = $document_type_invoice_format->product_detail_promo;
                $this->data['show_code'] = $document_type_invoice_format->product_show_code;
                $this->data['show_award_points'] = $document_type_invoice_format->show_award_points;
                $this->data['show_product_preferences'] = $document_type_invoice_format->show_product_preferences;
                $this->data['product_detail_font_size'] = $document_type_invoice_format->product_detail_font_size > 0 ? $document_type_invoice_format->product_detail_font_size : 0;
                $url_format = $document_type_invoice_format->format_url;
                $view_tax = $document_type_invoice_format->view_item_tax ? true : false;
                $tax_inc = $document_type_invoice_format->tax_inc ? true : false;
            }

            if ($inv->technology_provider == CADENA) {
                $this->data["technologyProviderLogo"] = "assets/images/cadena_logo.jpeg";
            } else if ($inv->technology_provider == BPM) {
                $this->data["technologyProviderLogo"] = "assets/images/bpm_logo.jpeg";
            } else if ($inv->technology_provider == SIMBA) {
                $this->data["technologyProviderLogo"] = "assets/images/simba_logo.png";
            } else if ($inv->technology_provider == DELCOP) {
                $this->data["technologyProviderLogo"] = "assets/images/delcop_logo.png";
            } else {
                $this->data["technologyProviderLogo"] = "assets/images/simba_logo.png";
            }

            if (!empty($inv->return_sale_ref)) {
                $affects = $inv = $this->pos_model->getInvoiceByID($inv->sale_id);
                $this->data["affects"] = $affects;
            }

            if ($document_type_invoice_format && $this->config->item('language') != $document_type_invoice_format->language) {
                $this->lang->admin_load('sma', $document_type_invoice_format->language);
            }

            // $url_format = "pos/view_17";
            if ($for_email === true) {
                $html = $this->load_view($this->theme.$url_format, $this->data, true);
                $this->sma->generate_pos_inv_pdf($html, $inv->reference_no);
            } else {
                $this->load_view($this->theme.$url_format, $this->data);
            }
        }
    }

    private function createQr($document)
    {
        $qrCode = $document->codigo_qr;
        $UUID = $document->cufe;

        if (!empty($UUID)) {
            $this->load->library('qr_code');

            $dir = 'themes/default/admin/assets/images/qr_code/';
            $filename = $dir . $document->reference_no .".png";

            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }

            QRcode::png($qrCode, $filename, 'L', 3, 0);
        }
    }

    public function print_order()
    {
        $this->sma->checkPermissions('index');
        if ($this->input->get('id')) {
            $sale_id = $this->input->get('id');
        }

        $biller_id = $this->input->get('biller_id');
        $customer_id = $this->input->get('customer_id');
        $customer_branch_id = $this->input->get('customer_branch_id');
        $seller_id = $this->input->get('seller_id');
        $document_type_id = $this->input->get('document_type_id');
        $suspend_sale_id = $this->input->get('suspend_sale_id');

        $this->load->helper('pos');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['modal'] = false;

        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($biller_id);
        $this->data['customer'] = $this->companies_model->getCompanyByID($customer_id);
        $this->data['address'] = $this->site->getAddressByID($customer_branch_id);
        $this->data['created_by'] = $this->site->getUser($this->session->userdata('user_id'));
        $this->data['seller'] = $this->site->getCompanyByID($seller_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($document_type_id, '');
        $this->data['tipo_regimen'] = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['suspend_sale_id'] = $suspend_sale_id;

        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;

        $this->data['print_directly'] = $print_directly;

        if ($print_directly == 1) {
            if (empty($this->pos_settings->cloud_print_mail) || empty($this->pos_settings->cloud_print_json_name) || empty($this->pos_settings->cloud_print_service_name)) {
                $this->session->set_flashdata('error', lang("invalid_google_cloud_print_settings"));
                $this->load_view($this->theme.'pos/settings', $this->data);
            }
            $view = $this->load_view($this->theme . 'pos/print_order', $this->data, true);
            $new_file_path = FCPATH.'assets/print_directly-'.date('Y-m-d-H-i-s');
            file_put_contents($new_file_path, $view);
            $file = fopen($new_file_path, "w");
            fwrite($file, $view . PHP_EOL);
            fclose($file);
            $direct_printing_response = $this->direct_printing($new_file_path);
            unlink($new_file_path);

            if ($direct_printing_response->success) {
                $this->data['direct_print_job_id'] = $direct_printing_response->job->id;
                $this->load_view($this->theme . 'pos/print_order', $this->data);
            } else {
                $this->session->set_flashdata('error', lang("there_is_a_problem_with_direct_printing"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $url_format = "pos/print_order";
            $this->load_view($this->theme . $url_format, $this->data);
        }
    }

    public function print_preparation($products, $seller_id, $table_number, $new_products, $cancelled_products)
    {
        $this->sma->checkPermissions('index');
        $this->load->helper('pos');
        $this->data['modal'] = false;
        $this->data['seller'] = $this->site->getCompanyByID($seller_id);
        $this->data['table_number'] = $table_number;
        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;
        $this->data['print_directly'] = $print_directly;
        if (empty($this->pos_settings->cloud_print_mail) || empty($this->pos_settings->cloud_print_json_name) || empty($this->pos_settings->cloud_print_service_name)) {
            $this->session->set_flashdata('error', lang("invalid_google_cloud_print_settings"));
            $this->load_view($this->theme.'pos/settings', $this->data);
        }
        $categories = [];
        $categories_products = [];
        foreach ($products as $product) {
            if (!isset($new_products[$product['product_id']])) {
                continue;
            }
            if (isset($cancelled_products[$product['product_id']])) {
                $product['cancelled'] = 1;
            }
            $pdata = $this->site->getProductByID($product['product_id']);
            $product['product_name'] = $pdata->name;
            $categories[$pdata->category_id] = 1;
            $categories_products[$pdata->category_id][] = $product;
        }
        foreach ($categories as $category_id => $setted) {

            $category = $this->site->getCategoryByID($category_id);
            $printer =  $this->pos_model->getPrinterByID($category->printer_id);
            $this->data['products'] = $categories_products[$category_id];
            $this->data['preparation_area'] = $printer && $printer->title ? $printer->title : 'SIN DEFINIR';
            // exit($this->load_view($this->theme . 'pos/print_preparation', $this->data, true));
            $view = $this->load_view($this->theme . 'pos/print_preparation', $this->data, true);
            $new_file_path = FCPATH.'assets/print_directly-'.date('Y-m-d-H-i-s');
            file_put_contents($new_file_path, $view);
            $file = fopen($new_file_path, "w");
            fwrite($file, $view . PHP_EOL);
            fclose($file);
            $printer_id = $category->printer_id ? $category->printer_id : NULL;
            $direct_printing_response = $this->direct_printing($new_file_path, $printer_id);
            unlink($new_file_path);
            if ($direct_printing_response->success) {
            } else {
                exit(json_encode($direct_printing_response));
            }
        }
    }

    public function register_details($register_id = NULL, $modal = true)
    {
        $this->sma->checkPermissions('index');
        $popts = $this->site->getPaidOpts(1,0);
        $this->data['popts'] = $popts;
        $this->data['modal'] = $modal;
        $this->data['modal_js'] = $this->site->modal_js();

        $register_data = $this->pos_model->get_register_details($register_id);

        $this->data['register'] = $register_data['register'];
        $this->data['register_details'] = $register_data['register_details'];
        $this->data['register_details_counted'] = $register_data['register_details_counted'];
        $this->data['register_details_categories'] = $register_data['register_details_categories'];
        $this->data['register_details_categories_expenses'] = $register_data['register_details_categories_expenses'];
        $this->data['close_user'] = $this->site->getUser($this->data['register']->user_id);

        $this->load_view($this->theme . 'pos/register_details', $this->data);
    }

    public function today_sale()
    {
        if (!$this->Owner && !$this->Admin && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->sma->md();
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['ccsales'] = $this->pos_model->getTodayCCSales();
        $this->data['cashsales'] = $this->pos_model->getTodayCashSales();
        $this->data['chsales'] = $this->pos_model->getTodayChSales();
        $this->data['pppsales'] = $this->pos_model->getTodayPPPSales();
        $this->data['stripesales'] = $this->pos_model->getTodayStripeSales();
        $this->data['authorizesales'] = $this->pos_model->getTodayAuthorizeSales();
        $this->data['totalsales'] = $this->pos_model->getTodaySales();
        $this->data['refunds'] = $this->pos_model->getTodayRefunds();
        $this->data['returns'] = $this->pos_model->getTodayReturns();
        $this->data['expenses'] = $this->pos_model->getTodayExpenses();
        $this->load_view($this->theme . 'pos/today_sale', $this->data);
    }

    public function check_pin()
    {
        $pin = $this->input->post('pw', TRUE);
        if ($pin == $this->pos_pin) {
            $this->sma->send_json(array('res' => 1));
        }
        $this->sma->send_json(array('res' => 0));
    }

    public function barcode($text = NULL, $bcs = 'code128', $height = 50)
    {
        return admin_url('products/gen_barcode/' . $text . '/' . $bcs . '/' . $height);
    }

    public function settings()
    {
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line('no_zero_required'));
        $this->form_validation->set_rules('pro_limit', $this->lang->line('pro_limit'), 'required|is_natural_no_zero');
        $this->form_validation->set_rules('pin_code', $this->lang->line('delete_code'), 'numeric');
        $this->form_validation->set_rules('customer', $this->lang->line('default_customer'), 'required|is_natural_no_zero');
        $this->form_validation->set_rules('biller', $this->lang->line('default_biller'), 'required|is_natural_no_zero');
        if ($this->input->post('balance_settings') == 2) {
            $this->form_validation->set_rules('balance_label_prefix', $this->lang->line('balance_label_prefix'), 'required');
        }
        if ($this->input->post('mobile_print_automatically_invoice') == 2) {
            $this->form_validation->set_rules('mobile_print_wait_time', $this->lang->line('mobile_print_wait_time'), 'required|greater_than[0]');
        }
        if ($this->input->post('remote_printing') == 4) {
            $this->form_validation->set_rules('cloud_print_mail', $this->lang->line('cloud_print_mail'), 'required');
            $this->form_validation->set_rules('cloud_print_service_name', $this->lang->line('cloud_print_service_name'), 'required');
        }
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                'pro_limit'                 => $this->input->post('pro_limit'),
                'pin_code'                  => $this->input->post('pin_code') ? $this->input->post('pin_code') : NULL,
                'default_customer'          => $this->input->post('customer'),
                'default_section'           => $this->input->post('default_section'),
                'default_biller'            => $this->input->post('biller'),
                'display_time'              => $this->input->post('display_time'),
                'receipt_printer'           => $this->input->post('remote_printing') == 4 ? $this->input->post('direct_receipt_printer') : $this->input->post('receipt_printer'),
                'cash_drawer_codes'         => $this->input->post('cash_drawer_codes'),
                'cf_title1'                 => $this->input->post('cf_title1'),
                'cf_title2'                 => $this->input->post('cf_title2'),
                'cf_value1'                 => $this->input->post('cf_value1'),
                'cf_value2'                 => $this->input->post('cf_value2'),
                'focus_add_item'            => $this->input->post('focus_add_item'),
                'add_manual_product'        => $this->input->post('add_manual_product'),
                'customer_selection'        => $this->input->post('customer_selection'),
                'add_customer'              => $this->input->post('add_customer'),
                'toggle_category_slider'    => $this->input->post('toggle_category_slider'),
                'toggle_subcategory_slider' => $this->input->post('toggle_subcategory_slider'),
                'toggle_brands_slider'      => $this->input->post('toggle_brands_slider'),
                'cancel_sale'               => $this->input->post('cancel_sale'),
                'suspend_sale'              => $this->input->post('suspend_sale'),
                'print_items_list'          => $this->input->post('print_items_list'),
                'finalize_sale'             => $this->input->post('finalize_sale'),
                'today_sale'                => $this->input->post('today_sale'),
                'open_hold_bills'           => $this->input->post('open_hold_bills'),
                'close_register'            => $this->input->post('close_register'),
                'tooltips'                  => $this->input->post('tooltips'),
                'keyboard'                  => $this->input->post('keyboard'),
                'pos_printers'              => $this->input->post('pos_printers'),
                'java_applet'               => $this->input->post('enable_java_applet'),
                'product_button_color'      => $this->input->post('product_button_color'),
                'paypal_pro'                => $this->input->post('paypal_pro'),
                'stripe'                    => $this->input->post('stripe'),
                'authorize'                 => $this->input->post('authorize'),
                'rounding'                  => $this->input->post('rounding'),
                'item_order'                => $this->input->post('item_order'),
                'after_sale_page'           => $this->input->post('after_sale_page'),
                'printer'                   => $this->input->post('receipt_printer'),
                'order_printers'            => json_encode($this->input->post('order_printers')),
                'auto_print'                => $this->input->post('remote_printing') == 1 ? 0 : $this->input->post('auto_print'),
                'remote_printing'           => DEMO ? 1 : $this->input->post('remote_printing'),
                'customer_details'          => $this->input->post('customer_details'),
                'local_printers'            => $this->input->post('local_printers'),
                'balance_settings'          => $this->input->post('balance_settings'),
                'balance_label_prefix'      => $this->input->post('balance_label_prefix'),
                'time_focus_quantity'       => $this->input->post('time_focus_quantity'),
                'restobar_mode'             => $this->input->post('restobar_mode_status'),
                'table_service'             => $this->input->post('table_service'),
                'order_print_mode'          => $this->input->post('order_print_mode'),
                'apply_suggested_tip'       => $this->input->post('apply_suggested_tip'),
                'apply_suggested_home_delivery_amount' => $this->input->post('apply_suggested_home_delivery_amount'),
                'home_delivery_in_invoice'  => $this->input->post('home_delivery_in_invoice'),
                'cloud_print_mail'          => $this->input->post('cloud_print_mail'),
                'cloud_print_service_name'  => $this->input->post('cloud_print_service_name'),
                'detail_sale_by_category'   => $this->input->post('detail_sale_by_category'),
                'show_suspended_bills_automatically'   => $this->input->post('show_suspended_bills_automatically'),
                'print_voucher_delivery'    => $this->input->post('print_voucher_delivery'),
                'allow_print_command'       => $this->input->post('allow_print_command'),
                'command_only_for_suspended'=> $this->input->post('command_only_for_suspended'),
                'express_payment'           => $this->input->post('express_payment'),
                'search_product_price'      => $this->input->post('search_product_price'),
                'except_tip_restobar'       => $this->input->post('except_tip_restobar'),
                'required_shipping_restobar'=> $this->input->post('required_shipping_restobar'),
                'mobile_print_automatically_invoice'   => $this->input->post('mobile_print_automatically_invoice'),
                'use_barcode_scanner'       => $this->input->post('use_barcode_scanner'),
                'show_variants_and_preferences'   => $this->input->post('show_variants_and_preferences'),
                'order_to_table_default_restobar'=>$this->input->post('order_to_table_default_restobar'),
                'mobile_print_wait_time'=>$this->input->post('mobile_print_wait_time'),
                'show_client_modal_on_select'=>$this->input->post('show_client_modal_on_select'),
                'max_net_sale'=>$this->input->post('max_net_sale'),
                'withholding_management'    => $this->input->post('withholding_management'),
                'creation_order_sale'=>$this->input->post('creation_order_sale'),
                'last_update'=>date('Y-m-d H:i:s'),
                'preparation_panel_update_time' => $this->input->post('preparation_panel_update_time'),
                'show_price_favorite_products' => $this->input->post('show_price_favorite_products'),
                'focus_last_product_quantity' => $this->input->post('focus_last_product_quantity'),
                'open_cash_register'        => $this->input->post('open_cash_register'),
                'enter_to_exact_search'     => $this->input->post('enter_to_exact_search'),
                // 'read_weight'            => $this->input->post('read_weight'),
                'show_list_produts'         => $this->input->post('show_list_produts'),
                'check_prices_with_stock'   => $this->input->post('check_prices_with_stock'),
                'apply_credit_to_the_payment_balance'   => $this->input->post('apply_credit_to_the_payment_balance'),
                'current_gold_price'   => $this->input->post('current_gold_price'),
                'minimun_gold_price'   => $this->input->post('minimun_gold_price'),
                'current_italian_gold_price'   => $this->input->post('current_italian_gold_price'),
                'minimun_italian_gold_price'   => $this->input->post('minimun_italian_gold_price'),
                'allow_favorities'   => $this->input->post('allow_favorities'),
                'locator_management'   => $this->input->post('locator_management'),
                'suspended_sales_default_sorting'   => $this->input->post('suspended_sales_default_sorting'),
                'suspended_sales_limited_users_filter'   => $this->input->post('suspended_sales_limited_users_filter'),
                'pos_navbar_default_category'   => $this->input->post('default_section') == 1001 ? $this->input->post('pos_navbar_default_category') : $this->input->post('pos_navbar_default_subcategory'),
            );
            if ($this->Owner) {
                $data['activate_electronic_pos'] = $this->input->post('activate_electronic_pos');
                $data['technology_provider_id'] = $this->input->post('technology_provider_id');
                $data['work_environment'] = $this->input->post('work_environment');
                $data['mode_electronic_pos'] = $this->input->post('mode_electronic_pos');
                $data['uvt_value'] = $this->input->post('uvt_value');
            }

            $payment_config = array(
                'APIUsername'            => $this->input->post('APIUsername'),
                'APIPassword'            => $this->input->post('APIPassword'),
                'APISignature'           => $this->input->post('APISignature'),
                'stripe_secret_key'      => $this->input->post('stripe_secret_key'),
                'stripe_publishable_key' => $this->input->post('stripe_publishable_key'),
                'api_login_id'           => $this->input->post('api_login_id'),
                'api_transaction_key'    => $this->input->post('api_transaction_key'),
            );
            if ($_FILES['cloud_print_json']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path."google_cloud_print/";
                $config['allowed_types'] = '*';
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('cloud_print_json')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("pos/settings");
                }
                $file = $this->upload->file_name;
                $data['cloud_print_json_name'] = $file;
            }
            // exit(var_dump($_FILES['cloud_print_json']));
        } elseif ($this->input->post('update_settings')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("pos/settings");
        }

        if ($this->form_validation->run() == TRUE && $this->pos_model->updateSetting($data)) {

            if ($this->Settings->set_focus == 0 && $this->pos_settings->balance_settings == 1) {
                $this->db->query("UPDATE sma_pos_settings
                                    SET
                                        focus_add_item = IF(focus_add_item = 'F8' OR focus_add_item = 'f8', NULL, focus_add_item),
                                        add_manual_product = IF(add_manual_product = 'F8' OR add_manual_product = 'f8', NULL, add_manual_product),
                                        customer_selection = IF(customer_selection = 'F8' OR customer_selection = 'f8', NULL, customer_selection),
                                        add_customer = IF(add_customer = 'F8' OR add_customer = 'f8', NULL, add_customer),
                                        toggle_category_slider = IF(toggle_category_slider = 'F8' OR toggle_category_slider = 'f8', NULL, toggle_category_slider),
                                        toggle_subcategory_slider = IF(toggle_subcategory_slider = 'F8' OR toggle_subcategory_slider = 'f8', NULL, toggle_subcategory_slider),
                                        toggle_brands_slider = IF(toggle_brands_slider = 'F8' OR toggle_brands_slider = 'f8', NULL, toggle_brands_slider),
                                        cancel_sale = IF(cancel_sale = 'F8' OR cancel_sale = 'f8', NULL, cancel_sale),
                                        suspend_sale = IF(suspend_sale = 'F8' OR suspend_sale = 'f8', NULL, suspend_sale),
                                        print_items_list = IF(print_items_list = 'F8' OR print_items_list = 'f8', NULL, print_items_list),
                                        finalize_sale = IF(finalize_sale = 'F8' OR finalize_sale = 'f8', NULL, finalize_sale),
                                        today_sale = IF(today_sale = 'F8' OR today_sale = 'f8', NULL, today_sale),
                                        open_hold_bills = IF(open_hold_bills = 'F8' OR open_hold_bills = 'f8', NULL, open_hold_bills),
                                        close_register = IF(close_register = 'F8' OR close_register = 'f8', NULL, close_register),
                                        search_product_price = IF(search_product_price = 'F8' OR search_product_price = 'f8', NULL, search_product_price)
                                WHERE pos_id = 1;");
            }

            if (DEMO) {
                $this->session->set_flashdata('message', $this->lang->line('pos_setting_updated'));
                admin_redirect("pos/settings");
            }
            if ($this->write_payments_config($payment_config)) {
                $this->session->set_flashdata('message', $this->lang->line('pos_setting_updated'));
                admin_redirect("pos/settings");
            } else {
                $this->session->set_flashdata('error', $this->lang->line('pos_setting_updated_payment_failed'));
                admin_redirect("pos/settings");
            }
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['pos'] = $this->pos_model->getSetting();
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->config->load('payment_gateways');
            $this->data['stripe_secret_key'] = $this->config->item('stripe_secret_key');
            $this->data['stripe_publishable_key'] = $this->config->item('stripe_publishable_key');
            $authorize = $this->config->item('authorize');
            $this->data['api_login_id'] = $authorize['api_login_id'];
            $this->data['api_transaction_key'] = $authorize['api_transaction_key'];
            $this->data['APIUsername'] = $this->config->item('APIUsername');
            $this->data['APIPassword'] = $this->config->item('APIPassword');
            $this->data['APISignature'] = $this->config->item('APISignature');
            $this->data['printers'] = $this->pos_model->getAllPrinters();
            $this->data['paypal_balance'] = NULL;
            $this->data['stripe_balance'] = NULL;
            $this->data["technology_providers"] = $this->settings_model->get_technology_providers();
            $this->data['ELECTRONICPOSMODE1'] = self::ELECTRONICPOSMODE1;
            $this->data['ELECTRONICPOSMODE2'] = self::ELECTRONICPOSMODE2;
            $this->data['subcategories'] = $this->site->getSubCategories();
            $shorcurts = [""=>"-Seleccione-",
                "F1"=>"F1", "F2"=>"F2", "F3"=>"F3", "F4"=>"F4", "F5"=>"F5", "F6"=>"F6",
                "F7"=>"F7", "F8"=>"F8", "F9"=>"F9", "F10"=>"F10", "F11"=>"F11", "F12"=>"F12",
                "Ctrl+F1"=>"Ctrl+F1", "Ctrl+F2"=>"Ctrl+F2", "Ctrl+F3"=>"Ctrl+F3", "Ctrl+F5"=>"Ctrl+F5", "Ctrl+F6"=>"Ctrl+F6",
                "Ctrl+F7"=>"Ctrl+F7", "Ctrl+F8"=>"Ctrl+F8", "Ctrl+F9"=>"Ctrl+F9", "Ctrl+F10"=>"Ctrl+F10", "Ctrl+F11"=>"Ctrl+F11", "Ctrl+F12"=>"Ctrl+F12"
            ];
            $this->data['shortcurts'] = $shorcurts;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('pos_settings')));
            $meta = array('page_title' => lang('pos_settings'), 'bc' => $bc);
            $this->page_construct('pos/settings', $meta, $this->data);
        }
    }

    public function write_payments_config($config)
    {
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        if (DEMO) {
            return TRUE;
        }
        $file_contents = file_get_contents('./assets/config_dumps/payment_gateways.php');
        $output_path = APPPATH . 'config/payment_gateways.php';
        $this->load->library('parser');
        $parse_data = array(
            'APIUsername'            => $config['APIUsername'],
            'APIPassword'            => $config['APIPassword'],
            'APISignature'           => $config['APISignature'],
            'stripe_secret_key'      => $config['stripe_secret_key'],
            'stripe_publishable_key' => $config['stripe_publishable_key'],
            'api_login_id'           => $config['api_login_id'],
            'api_transaction_key'    => $config['api_transaction_key'],
        );
        $new_config = $this->parser->parse_string($file_contents, $parse_data);

        $handle = fopen($output_path, 'w+');
        @chmod($output_path, 0777);

        if (is_writable($output_path)) {
            if (fwrite($handle, $new_config)) {
                @chmod($output_path, 0644);
                return TRUE;
            } else {
                @chmod($output_path, 0644);
                return FALSE;
            }
        } else {
            @chmod($output_path, 0644);
            return FALSE;
        }
    }

    public function opened_bills($per_page = 0)
    {
        $this->load->library('pagination');

        if ($this->input->get('per_page')) {
            $per_page = $this->input->get('per_page');
        }
        $sort_order = $this->pos_settings->suspended_sales_default_sorting;
        if ($this->input->get('sort_order')) {
            $sort_order = $this->input->get('sort_order');
        }

        $config['base_url'] = admin_url('pos/opened_bills');
        $config['total_rows'] = $this->pos_model->bills_count(null, $sort_order);
        $config['per_page'] = 6;
        $config['num_links'] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($config);
        $data['r'] = TRUE;
        $bills = $this->pos_model->fetch_bills_reference($config['per_page'], $per_page, NULL, $sort_order);
        setlocale(LC_TIME, 'es_ES.UTF-8'); // Configurar el idioma

        if (!empty($bills)) {

            $html = "";
            $html .= '<ul class="ob">';
            foreach ($bills as $bill) {
                $date = new DateTime($bill->date);
                $formatter = new IntlDateFormatter(
                    'es_ES',
                    IntlDateFormatter::FULL,
                    IntlDateFormatter::NONE,
                    'America/Los_Angeles', // Ajusta según tu zona horaria
                    IntlDateFormatter::GREGORIAN,
                    "EEEE d 'de' MMMM 'del' yyyy"
                );
                $timeFormatter = new IntlDateFormatter(
                    'es_ES',
                    IntlDateFormatter::NONE,
                    IntlDateFormatter::SHORT,
                    'America/Los_Angeles',
                    IntlDateFormatter::GREGORIAN,
                    "h:mm a" // Formato con AM/PM
                );
                $html .= '<li>
                            <button type="button" class="btn btn-wappsi-pausadas btn-outline sus_sale" id="' . $bill->id . '">
                                <strong style="font-size:120%;" class="suspend_note">' . $bill->suspend_note . '</strong><br>
                                <strong>' . $bill->customer . '</strong>
                                <br>'.lang('items').': ' . $bill->count .
                                '<br>'.lang('total').': ' . $this->sma->formatMoney($bill->total) .
                                '<br> ' . ucwords(mb_strtolower($formatter->format($date))).
                                '<br> ' . ucwords(mb_strtolower($timeFormatter->format($date))).
                                '<br><strong>' . ucwords(mb_strtolower($bill->name_seller ?? '')) . '</strong>'.
                            '</button>
                        </li>';
            }
            $html .= '</ul>';
        } else {
            $html = "<h3>" . lang('no_opeded_bill') . "</h3><p>&nbsp;</p>";
            $data['r'] = FALSE;
        }
        $data['html'] = $html;
        $data['electronic_billing_environment'] = $this->site->electronic_billing_environment();
        $data['page'] = $this->pagination->create_links();
        echo $this->load_view($this->theme . 'pos/opened', $data, TRUE);
    }

    public function opened_bills_by_reference($bill_reference = NULL, $per_page = 0)
    {
        if(!$bill_reference){
            $bill_reference_route = 0;
            $bill_reference_text = '';
        }else{
            $bill_reference_route =  $bill_reference;
            $bill_reference_text =  $bill_reference;
            $bill_reference_text = urldecode($bill_reference_text);
        }
        $this->load->library('pagination');
        if ($this->input->get('per_page')) {
            $per_page = $this->input->get('per_page');
        }
        $sort_order = $this->pos_settings->suspended_sales_default_sorting;
        if ($this->input->get('sort_order')) {
            $sort_order = $this->input->get('sort_order');
        }
        $config['base_url'] = admin_url('pos/opened_bills_by_reference')."/".$bill_reference_route;
        $config['total_rows'] = $this->pos_model->bills_count_reference($bill_reference_text, $sort_order);
        $config['per_page'] = 6;
        $config['num_links'] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($config);
        $data['r'] = TRUE;
        $bills = $this->pos_model->fetch_bills_reference($config['per_page'], $per_page, $bill_reference_text, $sort_order);
        if (!empty($bills)) {
            $html = "";
            $html .= '<ul class="ob">';
            foreach ($bills as $bill) {
                $date = new DateTime($bill->date);
                $formatter = new IntlDateFormatter(
                    'es_ES',
                    IntlDateFormatter::FULL,
                    IntlDateFormatter::NONE,
                    'America/Los_Angeles', // Ajusta según tu zona horaria
                    IntlDateFormatter::GREGORIAN,
                    "EEEE d 'de' MMMM 'del' yyyy"
                );
                $timeFormatter = new IntlDateFormatter(
                    'es_ES',
                    IntlDateFormatter::NONE,
                    IntlDateFormatter::SHORT,
                    'America/Los_Angeles',
                    IntlDateFormatter::GREGORIAN,
                    "h:mm a" // Formato con AM/PM
                );
                $html .= '<li>
                            <button type="button" class="btn btn-wappsi-pausadas btn-outline sus_sale" id="' . $bill->id . '">
                                <strong style="font-size:120%;" class="suspend_note">' . $bill->suspend_note . '</strong><br>
                                <strong>' . $bill->customer . '</strong>
                                <br>'.lang('items').': ' . $bill->count .
                                '<br>'.lang('total').': ' . $this->sma->formatMoney($bill->total) .
                                '<br> ' . ucwords(mb_strtolower($formatter->format($date))).
                                '<br> ' . ucwords(mb_strtolower($timeFormatter->format($date))).
                                '<br><strong>' . ucwords(mb_strtolower($bill->name_seller ? $bill->name_seller : "")) . '</strong>'.
                            '</button>
                        </li>';
            }
            $html .= '</ul>';
        } else {
            $html = "<h3>" . lang('no_opeded_bill') . "</h3><p>&nbsp;</p>";
            $data['r'] = FALSE;
        }
        $data['html'] = $html;
        $data['sort_order'] = $sort_order;
        $data['bill_reference'] = $bill_reference_text;
        $data['electronic_billing_environment'] = $this->site->electronic_billing_environment();
        $data['page'] = $this->pagination->create_links();
        echo $this->load_view($this->theme . 'pos/opened', $data, TRUE);
    }

    public function search_product_price()
    {
        $data = [];
        echo $this->load_view($this->theme . 'pos/search_product_price', $data, TRUE);
    }

    public function delete($id = NULL)
    {
        $this->sma->checkPermissions('index');

        $id = (!empty($id)) ? $id : $this->input->get('id');
        $suspendedBills = $this->Restobar_model->getSuspendedeBill($id);

        if (!empty($suspendedBills->table_id)) {
            $this->change_table_status($suspendedBills->table_id);
        }

        $suspended_bills_deleted = $this->pos_model->deleteBill($id);
        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 2,
            'table_name' => 'suspended_bills',
            'record_id' => $id,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => ($this->session->first_name." ".$this->session->last_name)." Eliminó la venta suspendida {$suspendedBills->suspend_note}",
        ]);
        if ($suspended_bills_deleted) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("suspended_sale_deleted")));
        }
    }

    public function email_receipt($sale_id = NULL, $view = null)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->post('id')) {
            $sale_id = $this->input->post('id');
        }

        if ( ! $sale_id) {
            die('No sale selected.');
        }

        if ($this->input->post('email')) {
            $to = $this->input->post('email');
        }


        $inv = $this->pos_model->getInvoiceByID($sale_id);

        $biller_id = $inv->biller_id;
        $customer_id = $inv->customer_id;

        $this->data['inv'] = $inv;
        $this->data['sid'] = $sale_id;
        $this->data['pos'] = $this->pos_model->getSetting();
        $this->data['page_title'] = $this->lang->line("invoice");
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['rows'] = $this->pos_model->getAllInvoiceItems($sale_id);
        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['customer'] = $this->pos_model->getCompanyByID($customer_id);
        $this->data['payments'] = $this->pos_model->getInvoicePayments($sale_id);
        $this->data['barcode'] = $this->barcode($inv->reference_no, 'code128', 30);
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

        $receipt = $this->load_view($this->theme . 'pos/email_receipt', $this->data, TRUE);

        if ($view) {
            echo $receipt;
            die();
        }

        if (!$to) {
            $to = $this->data['customer']->email;
        }
        if (!$to) {
            $this->sma->send_json(array('msg' => $this->lang->line("no_meil_provided")));
        }

        try {
            if ($this->sma->send_email($to, lang('receipt_from') .' ' . $this->data['biller']->company, $receipt, NULL, NULL, FCPATH. 'files/pos_invoice/'.$inv->reference_no.'.pdf')) {
                $this->sma->send_json(array('msg' => $this->lang->line("email_sent")));
            } else {
                $this->sma->send_json(array('msg' => $this->lang->line("email_failed")));
            }
        } catch (Exception $e) {
            $this->sma->send_json(array('msg' => $e->getMessage()));
        }

    }

    public function active()
    {
        $this->session->set_userdata('last_activity', now());
        if ((now() - $this->session->userdata('last_activity')) <= 20) {
            die('Successfully updated the last activity.');
        } else {
            die('Failed to update last activity.');
        }
    }

    public function add_payment($id = NULL)
    {
        $this->sma->checkPermissions('payments', TRUE, 'sales');
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->Settings->cashier_close != 2 && !$this->Owner && !$this->Admin) {
            if ($register = $this->pos_model->registerData($this->session->userdata('user_id'))) {
                $register_data = array('register_id' => $register->id, 'cash_in_hand' => $register->cash_in_hand, 'register_open_time' => $register->date);
                // $this->session->set_userdata($register_data);
            } else {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register');
            }
        }

        $this->form_validation->set_rules('document_type_id', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == TRUE) {
        $sale = $this->pos_model->getInvoiceByID($this->input->post('sale_id'));
            if ($this->input->post('paid_by') == 'deposit') {
                $customer_id = $sale->customer_id;
                //Validación balance depósito de cliente.
                if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $customer_id = null;
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $biller_id = $sale->biller_id;
            $document_type_id = $this->input->post('document_type_id');
            $referenceBiller = $this->site->getReferenceBiller($biller_id, $document_type_id);

            if($referenceBiller){
                $reference = $referenceBiller;
            } else {
                $reference = $this->site->getReference('rc');
            }

            $payment = array(
                'date'         => $date,
                'sale_id'      => $this->input->post('sale_id'),
                'reference_no' => $reference,
                'amount'       => $this->input->post('amount-paid'),
                'paid_by'      => $this->input->post('paid_by'),
                'cheque_no'    => $this->input->post('cheque_no'),
                'cc_no'        => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                'cc_holder'    => $this->input->post('pcc_holder'),
                'cc_month'     => $this->input->post('pcc_month'),
                'cc_year'      => $this->input->post('pcc_year'),
                'cc_type'      => $this->input->post('pcc_type'),
                'cc_cvv2'      => $this->input->post('pcc_ccv'),
                'note'         => $this->input->post('note'),
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'type'         => 'received',
                'document_type_id' => $document_type_id,
            );

            if ($this->Settings->cost_center_selection != 2 && $this->Settings->modulary == 1) {

                if ($sale->cost_center_id > 0) { //SI LA COMPRA YA TIENE CENTRO DE COSTO DEFINIDO

                    $payment['cost_center_id'] = $sale->cost_center_id;

                } else { //SI NO TIENE CENTRO DE COSTO

                    if ($this->Settings->cost_center_selection == 0) { //SI EL CENTRO SE DEFINE POR SUCURSAL

                        if ($sale->biller_id == NULL && $this->input->post('biller')) {
                            $biller_id = $this->input->post('biller');
                        } else if ($sale->biller_id > 0) {
                            $biller_id = $sale->biller_id;
                        }

                        $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
                        if ($biller_cost_center) {
                            $payment['cost_center_id'] = $biller_cost_center->id;
                        } else {
                            $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                            admin_redirect($_SERVER["HTTP_REFERER"]);
                        }

                    } else if ($this->Settings->cost_center_selection == 1) { //SI EL CENTRO SE DEFINE ESCOGIÉNDOLO

                        if ($this->input->post('cost_center_id')) {
                            $payment['cost_center_id'] = $this->input->post('cost_center_id');
                        } else {
                            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
                        }

                    }

                }

            }

            //Retenciones
            if ($this->input->post('rete_applied') == 1) {

                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');

                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');

                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');

                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');

                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;

                // $payment['amount'] -= $total_retenciones;

                $retencion = [];
                $retencion['rete_fuente_percentage'] = $rete_fuente_percentage;
                $retencion['rete_fuente_total'] = $rete_fuente_total;
                $retencion['rete_fuente_account'] = $rete_fuente_account;
                $retencion['rete_fuente_base'] = $rete_fuente_base;
                $retencion['rete_iva_percentage'] = $rete_iva_percentage;
                $retencion['rete_iva_total'] = $rete_iva_total;
                $retencion['rete_iva_account'] = $rete_iva_account;
                $retencion['rete_iva_base'] = $rete_iva_base;
                $retencion['rete_ica_percentage'] = $rete_ica_percentage;
                $retencion['rete_ica_total'] = $rete_ica_total;
                $retencion['rete_ica_account'] = $rete_ica_account;
                $retencion['rete_ica_base'] = $rete_ica_base;
                $retencion['rete_other_percentage'] = $rete_other_percentage;
                $retencion['rete_other_total'] = $rete_other_total;
                $retencion['rete_other_account'] = $rete_other_account;
                $retencion['rete_other_base'] = $rete_other_base;
                $retencion['total_retenciones'] = $total_retenciones;

                // exit('Total Fuente : '.$rete_fuente_total."\n Total IVA : ".$rete_iva_total."\n Total ICA : ".$rete_ica_total."\n Total Otros : ".$rete_other_total."\n Total Retención : ".$total_retenciones);
            }
            //Retenciones

            if (($this->input->post('amount-paid') + (isset($total_retenciones) ? $total_retenciones : 0)) > ($sale->grand_total - $sale->paid)) {
                $this->session->set_flashdata('error', lang('mount_paid_greather_than_balance'));
                redirect($_SERVER["HTTP_REFERER"]);
            }


            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        // $this->sma->print_arrays($payment);
        if ($this->form_validation->run() == TRUE && $msg = $this->pos_model->addPayment($payment, $customer_id, (isset($retencion) ? $retencion : null))) {
            if ($msg) {
                if ($msg['status'] == 0) {
                    unset($msg['status']);
                    $error = '';
                    foreach ($msg as $m) {
                        if (is_array($m)) {
                            foreach ($m as $e) {
                                $error .= '<br>'.$e;
                            }
                        } else {
                            $error .= '<br>'.$m;
                        }
                    }
                    $this->session->set_flashdata('error', '<pre>' . $error . '</pre>');
                } else {
                    $this->session->set_flashdata('message', lang("payment_added"));
                    // Wappsi - LLamado a la función que actualiza el consecutivo incrementando en uno.
                    $this->site->updateReference('rc');                }
            } else {
                $this->session->set_flashdata('error', lang("payment_failed"));
            }
            admin_redirect("pos/sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $sale = $this->pos_model->getInvoiceByID($id);
            $this->data['inv'] = $sale;

            if ($sale->rete_fuente_total != 0 || $sale->rete_iva_total != 0 || $sale->rete_ica_total != 0 || $sale->rete_other_total != 0 ) {
                $rete_applied = true;
            } else {
                $rete_applied = false;
            }
            $this->data['rete_applied'] = $rete_applied;
            // Cambiando pay x rc - para que tome nuevo campo de consecutivos
            // para pagos despues de la venta
            $this->data['payment_ref'] = $this->site->getReference('rc');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');

            if ($this->Settings->cost_center_selection != 2 && $sale->cost_center_id == NULL) {
                if ($this->Settings->cost_center_selection == 1) {
                    $this->data['cost_centers'] = $this->site->getAllCostCenters();
                }
            }

            $this->load_view($this->theme . 'pos/add_payment', $this->data);
        }
    }

    public function checkCustomerDeposit($customer_id, $amount_paid){
       echo $this->site->check_customer_deposit($customer_id, $amount_paid);
    }

    public function updates()
    {
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->form_validation->set_rules('purchase_code', lang("purchase_code"), 'required');
        $this->form_validation->set_rules('envato_username', lang("envato_username"), 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->db->update('pos_settings', array('purchase_code' => $this->input->post('purchase_code', TRUE), 'envato_username' => $this->input->post('envato_username', TRUE)), array('pos_id' => 1));
            admin_redirect('pos/updates');
        } else {
            $fields = array('version' => $this->pos_settings->version, 'code' => $this->pos_settings->purchase_code, 'username' => $this->pos_settings->envato_username, 'site' => base_url());
            $this->load->helper('update');
            $protocol = is_https() ? 'https://' : 'http://';
            $updates = get_remote_contents($protocol . 'api.tecdiary.com/v1/update/', $fields);
            $this->data['updates'] = json_decode($updates);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('updates')));
            $meta = array('page_title' => lang('updates'), 'bc' => $bc);
            $this->page_construct('pos/updates', $meta, $this->data);
        }
    }

    public function install_update($file, $m_version, $version)
    {
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->load->helper('update');
        save_remote_file($file . '.zip');
        $this->sma->unzip('./files/updates/' . $file . '.zip');
        if ($m_version) {
            $this->load->library('migration');
            if (!$this->migration->latest()) {
                $this->session->set_flashdata('error', $this->migration->error_string());
                admin_redirect("pos/updates");
            }
        }
        $this->db->update('pos_settings', array('version' => $version), array('pos_id' => 1));
        unlink('./files/updates/' . $file . '.zip');
        $this->session->set_flashdata('success', lang('update_done'));
        admin_redirect("pos/updates");
    }

    public function open_drawer()
    {

        $data = json_decode($this->input->get('data'));
        $this->load->library('escpos');
        $this->escpos->load($data->printer);
        $this->escpos->open_drawer();
    }

    public function p()
    {
        $data = json_decode($this->input->get('data'));
        $this->load->library('escpos');
        $this->escpos->load($data->printer);
        $this->escpos->print_receipt($data);

    }

    function printers()
    {
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("pos");
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['page_title'] = lang('printers');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('pos'), 'page' => lang('pos')), array('link' => '#', 'page' => lang('printers')));
        $meta = array('page_title' => lang('list_printers'), 'bc' => $bc);
        $this->page_construct('pos/printers', $meta, $this->data);
    }

    function get_printers()
    {
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->sma->md();
        }

        $this->load->library('datatables');
        $this->datatables
        ->select("id, title, type, profile, path, ip_address, port")
        ->from("printers")
        ->add_column("Actions", "<div class='text-center'> <a href='" . admin_url('pos/edit_printer/$1') . "' class='btn-warning btn-xs tip' title='".lang("edit_printer")."'><i class='fa fa-edit'></i></a> <a href='#' class='btn-danger btn-xs tip po' title='<b>" . lang("delete_printer") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('pos/delete_printer/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id")
        ->unset_column('id');
        echo $this->datatables->generate();
    }

    function add_printer()
    {

        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("pos");
        }

        $this->form_validation->set_rules('title', $this->lang->line("title"), 'required');
        $this->form_validation->set_rules('type', $this->lang->line("type"), 'required');
        $this->form_validation->set_rules('profile', $this->lang->line("profile"), 'required');
        $this->form_validation->set_rules('char_per_line', $this->lang->line("char_per_line"), 'required');
        if ($this->input->post('type') == 'network') {
            $this->form_validation->set_rules('ip_address', $this->lang->line("ip_address"), 'required|is_unique[printers.ip_address]');
            $this->form_validation->set_rules('port', $this->lang->line("port"), 'required');
        } else {
            $this->form_validation->set_rules('path', $this->lang->line("path"), 'required|is_unique[printers.path]');
        }

        if ($this->pos_settings->remote_printing == 4) {
            $this->form_validation->set_rules('google_cloud_print_id', $this->lang->line("google_cloud_print_id"), 'required');
        }

        if ($this->form_validation->run() == true) {

            $data = array('title' => $this->input->post('title'),
                'type' => $this->input->post('type'),
                'profile' => $this->input->post('profile'),
                'char_per_line' => $this->input->post('char_per_line'),
                'path' => $this->input->post('path'),
                'ip_address' => $this->input->post('ip_address'),
                'google_cloud_print_id' => $this->input->post('google_cloud_print_id'),
                'port' => ($this->input->post('type') == 'network') ? $this->input->post('port') : NULL,
            );

        }

        if ( $this->form_validation->run() == true && $cid = $this->pos_model->addPrinter($data)) {

            $this->session->set_flashdata('message', $this->lang->line("printer_added"));
            admin_redirect("pos/printers");

        } else {
            if($this->input->is_ajax_request()) {
                echo json_encode(array('status' => 'failed', 'msg' => validation_errors())); die();
            }

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['page_title'] = lang('add_printer');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('pos'), 'page' => lang('pos')), array('link' => admin_url('pos/printers'), 'page' => lang('printers')), array('link' => '#', 'page' => lang('add_printer')));
            $meta = array('page_title' => lang('add_printer'), 'bc' => $bc);
            $this->page_construct('pos/add_printer', $meta, $this->data);
        }
    }

    function edit_printer($id = NULL)
    {

        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("pos");
        }
        if($this->input->get('id')) { $id = $this->input->get('id', TRUE); }

        $printer = $this->pos_model->getPrinterByID($id);
        $this->form_validation->set_rules('title', $this->lang->line("title"), 'required');
        $this->form_validation->set_rules('type', $this->lang->line("type"), 'required');
        $this->form_validation->set_rules('profile', $this->lang->line("profile"), 'required');
        $this->form_validation->set_rules('char_per_line', $this->lang->line("char_per_line"), 'required');
        if ($this->input->post('type') == 'network') {
            $this->form_validation->set_rules('ip_address', $this->lang->line("ip_address"), 'required');
            if ($this->input->post('ip_address') != $printer->ip_address) {
                $this->form_validation->set_rules('ip_address', $this->lang->line("ip_address"), 'is_unique[printers.ip_address]');
            }
            $this->form_validation->set_rules('port', $this->lang->line("port"), 'required');
        } else {
            $this->form_validation->set_rules('path', $this->lang->line("path"), 'required');
            if ($this->input->post('path') != $printer->path) {
                $this->form_validation->set_rules('path', $this->lang->line("path"), 'is_unique[printers.path]');
            }
        }

        if ($this->pos_settings->remote_printing == 4) {
            $this->form_validation->set_rules('google_cloud_print_id', $this->lang->line("google_cloud_print_id"), 'required');
        }

        if ($this->form_validation->run() == true) {

            $data = array('title' => $this->input->post('title'),
                'type' => $this->input->post('type'),
                'profile' => $this->input->post('profile'),
                'char_per_line' => $this->input->post('char_per_line'),
                'path' => $this->input->post('path'),
                'ip_address' => $this->input->post('ip_address'),
                'google_cloud_print_id' => $this->input->post('google_cloud_print_id'),
                'port' => ($this->input->post('type') == 'network') ? $this->input->post('port') : NULL,
            );

        }

        if ( $this->form_validation->run() == true && $this->pos_model->updatePrinter($id, $data)) {

            $this->session->set_flashdata('message', $this->lang->line("printer_updated"));
            admin_redirect("pos/printers");

        } else {

            $this->data['printer'] = $printer;
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['page_title'] = lang('edit_printer');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('pos'), 'page' => lang('pos')), array('link' => admin_url('pos/printers'), 'page' => lang('printers')), array('link' => '#', 'page' => lang('edit_printer')));
            $meta = array('page_title' => lang('edit_printer'), 'bc' => $bc);
            $this->page_construct('pos/edit_printer', $meta, $this->data);

        }
    }

    function delete_printer($id = NULL)
    {
        if(DEMO) {
            $this->session->set_flashdata('error', $this->lang->line("disabled_in_demo"));
            $this->sma->md();
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->sma->md();
        }

        if ($this->input->get('id')) { $id = $this->input->get('id', TRUE); }

        if ($this->pos_model->deletePrinter($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("printer_deleted")));
        }
    }

    public function payment_term_expire($customer_id = NULL, $just_validate = FALSE)
    {

        $this->data['sales_expired'] = $this->pos_model->getPaymentTermToExpire($this->Settings->alert_sale_expired, $customer_id, $just_validate);

        if ($just_validate != FALSE) {
            if ($this->data['sales_expired'] !== FALSE) {
                echo TRUE;
            } else {
                echo FALSE;
            }
        } else {
            $this->load_view($this->theme . 'modal_sales_expired', $this->data);
        }
    }

    public function validate_biller_resolution($document_type_id)
    {

        $parametro_porc_aviso = ($this->Settings->resolucion_porc_aviso / 100);
        $parametro_dias_aviso = $this->Settings->resolucion_dias_aviso;
        $biller = $this->db->where('id', $document_type_id)->get('documents_types');
        if ($biller->num_rows() > 0) {
            $biller = $biller->row();
            $res_actual = $biller->sales_consecutive;
            $res_fin = $biller->fin_resolucion;
            if ($res_actual == 0 || $res_fin == 0 || $res_fin == "") {
                $response = array(
                                'mensaje' => '<span class="fa fa-user"></span> '.lang('invalid_biller_data')."<a href='".admin_url('billers')."' target='_blank'>Click</a>",
                                'disable' => TRUE
                            );
                exit(json_encode($response));
            }
            $porc_aviso = $res_fin * $parametro_porc_aviso;
            $porc_aviso = floor($porc_aviso);
            $res_restante = $res_fin - $res_actual;
            if ($res_restante <= $porc_aviso) {
                if ($res_restante > 0) {
                    $mensaje = sprintf(lang('biller_resolution_advice'), $res_restante);
                    $disable = FALSE;
                } else {
                    $mensaje = sprintf(lang('biller_resolution_finished'), abs($res_restante));
                    $disable = TRUE;
                }
                $response = array(
                                'mensaje' => $mensaje,
                                'disable' => $disable
                            );
                echo json_encode($response);
            } else { //No hay aviso por rango de resolución
                if (date('Y-m-d') > $biller->vencimiento_resolucion) { //Ya se cumplió la fecha de vencimiento
                    $response = array(
                                'mensaje' => 'Ya se venció la fecha de la resolución.',
                                'disable' => TRUE
                                );
                    exit(json_encode($response));
                }
                $fecha1 = New DateTime(date('Y-m-d'));
                $fecha2 = New DateTime($biller->vencimiento_resolucion);
                $diferencia = $fecha1->diff($fecha2);
                $diasDiff = $diferencia->format('%d');
                $mesesDiff = $diferencia->format('%m');
                $añosDiff = $diferencia->format('%Y');
                if ($añosDiff <= 0 && $mesesDiff <= 0 && $diasDiff <= $parametro_dias_aviso) { //Se avisa por fecha de vencimiento
                    $mensaje = sprintf(lang('biller_resolution_expired'), ($diasDiff+1));
                    $disable = FALSE;
                    $response = array(
                                     'mensaje' => $mensaje,
                                     'disable' => $disable
                                    );
                    echo json_encode($response);
                } else { //No es necesario ningún aviso.
                    echo FALSE;
                }
            }
        } else { //No se encontró biller.
            echo FALSE;
        }
    }

    public function pos_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->input->post('form_action') != 'mark_all_printed_sale' && (!empty($_POST['val']))) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->pos_model->deleteSale($id);
                    }
                    $this->session->set_flashdata('message', lang("sales_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'combine') {

                    // $html = $this->combine_pdf($_POST['val']);

                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('grand_total'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('paid'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('payment_status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sale = $this->pos_model->getInvoiceByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($sale->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sale->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sale->biller);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sale->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $sale->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($sale->paid));
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, lang($sale->payment_status));
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'sales_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } else if ($this->input->post('form_action') == 'post_sale') {

                    $msg = '';
                    foreach ($_POST['val'] as $id) {
                        $msg .= $this->sales_model->recontabilizarVenta($id)."\n";
                    }
                    if ($this->session->userdata('reaccount_error')) {
                        $this->session->set_flashdata('error', $msg);
                        $this->session->unset_userdata('reaccount_error');
                    } else {
                        $this->session->set_flashdata('message', $msg);
                    }
                    redirect($_SERVER["HTTP_REFERER"]);

                } else if ($this->input->post('form_action') == 'mark_printed_sale') {
                    $msg = '';
                    foreach ($_POST['val'] as $id) {
                        $msg .= $this->pos_model->update_print_status($id)."\n";
                    }
                    if ($this->session->userdata('mark_as_printed_error')) {
                        $this->session->set_flashdata('error', $msg);
                        $this->session->unset_userdata('mark_as_printed_error');
                    } else {
                        $this->session->set_flashdata('message', lang('marked_as_printed_succesfully'));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);

                }
            } else if ($this->input->post('form_action') == 'mark_all_printed_sale') {
                $electronic = $this->input->post('electronic');
                $this->pos_model->mark_all_sales_as_printed($electronic);
                $this->session->set_flashdata('message', lang("sales_marked_as_printed"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function getItemsWholeSale($warehouse_id)
    {

        $term = "";

        $this->load->library('datatables');

        $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";

        $this->datatables->select('0 as cnt,
                                   products.code,
                                   products.name,
                                   FWP.quantity as quantity,
                                   products.id')
            ->join($wp, 'FWP.product_id=products.id', 'left')
            // ->join('warehouses_products FWP', 'FWP.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('products.id');
        if ($this->Settings->overselling) {
            $this->datatables->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->datatables->where("(FWP.quantity > 0) AND FWP.warehouse_id = '" . $warehouse_id . "' AND "
                . "({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }
        // $this->db->order_by('products.name ASC');
        // $this->datatables->limit($limit);
        $this->datatables->from('products');

        echo $this->datatables->generate();
    }

    public function itemSelectUnit($product_id, $warehouse_id, $customer_id)
    {
        if ($this->Settings->prioridad_precios_producto == 11) {
            $q = $this->db->query("
                SELECT U.name, 0 as valor_unitario, UP.cantidad, UP.id, U.id as product_unit_id FROM {$this->db->dbprefix('unit_prices')} AS UP
                    INNER JOIN {$this->db->dbprefix('units')} AS U ON U.id = UP.unit_id
                WHERE UP.id_product = {$product_id} AND UP.status = 1
                GROUP BY product_unit_id
                ");
        } else {
            $q = $this->db->query("
                SELECT
                    1 as num,
                    U2.name,
                    P.price as valor_unitario,
                    1 as cantidad,
                    0 as id,
                    U2.id as product_unit_id,
                    IF(U2.operator = '*', 1 / U2.operation_value, 1 * U2.operation_value) as operation_value,
                    U2.operator
                FROM {$this->db->dbprefix('products')} AS P
                    INNER JOIN {$this->db->dbprefix('units')} AS U2 ON U2.id = P.unit
                WHERE P.id = {$product_id}
                    UNION
                SELECT
                    2 as num,
                    U.name,
                    UP.valor_unitario,
                    IF(UP.cantidad > 0, UP.cantidad, 1) AS cantidad,
                    UP.id,
                    U.id as product_unit_id,
                    IF(U.operator = '*', 1 / U.operation_value, 1 * U.operation_value) as operation_value,
                    U.operator
                FROM {$this->db->dbprefix('unit_prices')} AS UP
                    INNER JOIN {$this->db->dbprefix('units')} AS U ON U.id = UP.unit_id
                WHERE UP.id_product = {$product_id} AND UP.status = 1
                ORDER BY operation_value ASC, valor_unitario DESC, num ASC, product_unit_id ASC
            ");
        }
        $data = [];
        $customer = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $qty = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id));
        if ($qty->num_rows() > 0) {
            $iqty = $qty->row();
            $item_qty = $iqty->quantity;
        } else {
            $item_qty = 0;
        }
        $cg_units = $customer_group->units ? json_decode($customer_group->units) : [];
        if ($q->num_rows() > 0) {
            $cnt = 0;
            foreach (($q->result()) as $up) {
                if ($cg_units && count($cg_units) > 0 && in_array($up->product_unit_id, $cg_units)) {
                    continue;
                }
                $cnt++;
                $unit = $this->site->getUnitByID($up->product_unit_id);
                $product = $this->site->getProductByID($product_id);
                $valor_unitario = $up->valor_unitario + (($up->valor_unitario * $customer_group->percent) / 100);
                if ($product->tax_method == 0 && $this->Settings->ipoconsumo) {
                    $consumption_sale_tax = $product->consumption_sale_tax;
                    if ($unit->operator && $this->Settings->precios_por_unidad_presentacion == 2) {
                        if ($unit->operator == "*") {
                            $consumption_sale_tax = $consumption_sale_tax * $unit->operation_value;
                        } else if ($unit->operator == "/") {
                            $consumption_sale_tax = $consumption_sale_tax / $unit->operation_value;
                        }
                    }
                    $valor_unitario = $valor_unitario + $consumption_sale_tax;
                }
                $operation_value_qty = (isset($up->operator) && $up->operator == '*' ? 1 * $up->cantidad : 1 / $up->cantidad);
                $data[] = [
                    $cnt,
                    $up->name,
                    $valor_unitario,
                    $this->sma->formatDecimal($item_qty * $operation_value_qty),
                    $up->id,
                    $product_id,
                    $up->product_unit_id,
                    (isset($up->operation_value) ? $up->operation_value : 1)
                ];
            }
        }
        $dataJSON = array(
                        'aaData' => $data,
                        'iTotalDisplayRecords' => count($data),
                        'iTotalRecords' => count($data),
                        'sEcho' => count($data),
                        'sColumns' => 'cnt,units.name,unit_prices.valor_unitario,unit_prices.cantidad,unit_prices.id, product_id',
                        );
        echo json_encode($dataJSON);
    }

    public function buscar_productos()
    {
        $producto = $this->input->post("producto");
        $descripcion = $this->input->post("descripcion");
        $marca = $this->input->post("marca");
        $warehouse = $this->input->post("warehouse");
        $this->load->library('datatables');
        $this->datatables->select("
                                    products.code AS codigo,
                                    products.name AS nombre,
                                    products.product_details AS descripcion,
                                    brands.name as brand_name,
                                    products.price AS precio,
                                    ".$this->db->dbprefix('warehouses_products').".quantity as quantity,
                                    products.id AS id
                                ");
        if ($producto) {
            $this->datatables->where("((".$this->db->dbprefix('products').".name LIKE '%".$producto."%') OR (".$this->db->dbprefix('products').".code LIKE '%".$producto."%') OR (".$this->db->dbprefix('products').".reference LIKE '%".$producto."%'))");
        }
        // $this->datatables->like($this->db->dbprefix('products').".name", $producto);
        if (! empty($descripcion))
        {
            $this->datatables->like($this->db->dbprefix('products').".product_details", $descripcion);
        }
        if (!empty($marca)) {
            $this->datatables->where('products.brand', $marca);
        }
        $this->datatables->join('brands', 'brands.id = products.brand', 'left');
        $this->datatables->join('warehouses_products', 'warehouses_products.product_id = products.id', 'left');
        if (!empty($warehouse)) {
            $this->datatables->where('warehouses_products.warehouse_id', $warehouse);
        }
        $this->datatables->from('products');
        echo $this->datatables->generate();
    }

    public function remote_printing(){
        $this->data['page_title'] = lang('edit_printer');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('pos'), 'page' => lang('pos')), array('link' => admin_url('pos/printers'), 'page' => lang('printers')), array('link' => '#', 'page' => lang('edit_printer')));
        $meta = array('page_title' => lang('edit_printer'), 'bc' => $bc);
        $this->page_construct('pos/remote_printing', $meta, $this->data);
    }

        /*
    IMPRESIÓN DIRECTA CON GOOGLE CLOUD PRINT
    */
    public function direct_printing($new_file_path = NULL, $printer_id = NULL)
    {
        $this->load->library('google');
        $service_account_email = $this->pos_settings->cloud_print_mail;
        $key_file_location =  $this->upload_path."google_cloud_print/".$this->pos_settings->cloud_print_json_name;
        $client = new Google_Client();
        $client->setAuthConfig($key_file_location);
        $client->setApplicationName($this->pos_settings->cloud_print_service_name);
        $client->setScopes(array('https://www.googleapis.com/auth/cloudprint'));
        $httpClient = $client->authorize();
        $printer = $this->pos_model->getPrinterByID($printer_id ? $printer_id : $this->pos_settings->printer);
        $params = array(
                            'printerid' => $printer->google_cloud_print_id,
                            'title' => 'NUEVO',
                            'ticket' => '{"version":"1.0","print":{}}',
                            // 'content' => file_get_contents(base_url().'assets/print.html'),
                            'content' => file_get_contents($new_file_path),
                            'contentType' => 'text/html'
             );
        $response = $httpClient->post('https://www.google.com/cloudprint/submit', array('form_params'=>$params));
        $response = json_decode($response->getBody());
        return $response;
    }

    public function get_direct_printers()
    {
        $this->load->library('google');
        $service_account_email = $this->pos_settings->cloud_print_mail;
        $key_file_location =  $this->upload_path."google_cloud_print/".$this->pos_settings->cloud_print_json_name;
        $client = new Google_Client();
        $client->setAuthConfig($key_file_location);
        $client->setApplicationName($this->pos_settings->cloud_print_service_name);
        $client->setScopes(array('https://www.googleapis.com/auth/cloudprint'));
        $httpClient = $client->authorize();
        $params = array();
        $response = json_decode($httpClient->post('https://www.google.com/cloudprint/search')->getBody());
        $printers = [];
        foreach ($response->printers as $printer) {
            $printers[$printer->displayName] = $printer->id;
        }
        $this->sma->print_arrays($printers);
    }

    public function get_direct_printers_jobs($job_id = NULL, $printer_id = NULL)
    {
        $this->load->library('google');
        $service_account_email = $this->pos_settings->cloud_print_mail;
        $key_file_location =  $this->upload_path."google_cloud_print/".$this->pos_settings->cloud_print_json_name;
        $client = new Google_Client();
        $client->setAuthConfig($key_file_location);
        $client->setApplicationName($this->pos_settings->cloud_print_service_name);
        $client->setScopes(array('https://www.googleapis.com/auth/cloudprint'));
        $httpClient = $client->authorize();
        $params = array(
                            'printerid' => $printer_id,
                        );
        $response = json_decode($httpClient->post('https://www.google.com/cloudprint/jobs', $params)->getBody());

        $jobs_status = [];

        foreach ($response->jobs as $job) {
            $jobs_status[$job->id] = $job->status;
        }

        echo $jobs_status[$job_id];
    }

    public function restobar()
    {
        $area_id = $this->input->post('id_area');
        $branch_id = $this->input->post('id_sucursal');

        if ($this->Owner || $this->Admin) {
            $branches = $this->Restobar_model->get_branches();
            $branch_id = (! empty($branch_id)) ? $branch_id : $this->Settings->default_biller;
        } else {
            $branch_id = (! empty($branch_id)) ? $branch_id : $this->session->biller_id;
        }

        $branch_areas = $this->Restobar_model->get_branch_areas($branch_id);

        $area_id = (! empty($area_id)) ? $area_id : $branch_areas[0]->id;

        $tables = $this->Restobar_model->get_tables($area_id);

        $this->data['branches'] = isset($branches) ? $branches : NULL;
        $this->data['branch_areas'] = $branch_areas;
        $this->data['area_id'] = $area_id;
        $this->data['branch_id'] = $branch_id;
        $this->data['tables'] = $tables;

        $this->page_construct('pos/restobar', ['page_title' => lang('restobar')], $this->data);
    }

    public function create_suspended_sale()
    {
        $table_id = $this->input->post('id_mesa');
        $biller_id = $this->input->post('id_sucursal');
        $table_number = $this->input->post('numero_mesa');

        $this->checkAvailableTable($table_id);

        $suspend_note = (! empty($table_number)) ? lang('mesa').' '.$table_number : '';

        $suspended_sale_data = [
            'date'=>date('Y-m-d H:i:s'),
            'customer_id'=>1,
            'customer'=>'Cliente Ocasional',
            'count'=>0,
            'total'=>0,
            'biller_id'=>$biller_id,
            'created_by'=>$this->session->userdata('user_id'),
            'suspend_note'=>$suspend_note,
            'table_id'=> (! empty($table_number)) ? (int) $table_id : NULL
        ];
        $suspended_sale_created = $this->Restobar_model->insert_suspended_sale($suspended_sale_data);

        if ($suspended_sale_created !== FALSE) {
            $this->change_table_status($table_id, OCCUPIED);

            admin_redirect('pos/index/'. $suspended_sale_created);
        } else {
            $this->session->flashdata('error', 'No fue posible abrir pedido para la '.lang('mesa').' '. $table_id);
            admin_redirect('pos/index');
        }
    }

    public function checkAvailableTable($tableId)
    {
        $tableInSaleSupended = $this->pos_model->getTableInSaleSuspended($tableId);

        if ($tableInSaleSupended != false) {
            $this->change_table_status($tableId, OCCUPIED);
            $this->session->flashdata('error', 'No fue posible abrir pedido para la '.lang('mesa').' '. $tableId .' debido a que se encuentra asociado.');
            admin_redirect('pos/index');
        }
    }

    public function change_table_status($table_id = NULL, $status = AVAILABLE)
    {
        $table_id = (!empty($table_id)) ? $table_id : $this->input->get("table_id");
        $table_status_changed = $this->Restobar_model->update_table(['estado'=>$status], $table_id);
    }

    public function order_preparation()
    {
        $this->sma->checkPermissions();
        $preparation_order_array = [];

        $preparation_orders = $this->pos_model->get_preparation_orders_by_preparer_user($this->session->userdata('user_id'));

        if (!empty($preparation_orders)) {
            foreach ($preparation_orders as $preparation_order) {
                $preparation_order_array[$preparation_order->id_venta_suspendida]['hora_orden'] = $preparation_order->hora_orden;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['estado_orden'] = $preparation_order->estado_orden;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['numero_mesa'] = $preparation_order->numero_mesa;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['fecha_orden'] = $preparation_order->fecha_orden;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['diferencia_minutos'] = $preparation_order->diferencia_minutos;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_venta_suspendida'] = $preparation_order->id_venta_suspendida;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_vendedor'] = $preparation_order->id_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['nombre_vendedor'] = $preparation_order->nombre_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['apellido_vendedor'] = $preparation_order->apellido_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['tipo'] = $preparation_order->tipo;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_mesa'] = $preparation_order->id_mesa;

                $preparation_order_array[$preparation_order->id_venta_suspendida]['productos'][] = [
                    'nombre_producto' => $preparation_order->nombre_producto,
                    'cantidad_producto' => $preparation_order->cantidad_producto,
                    'preferencias' => $this->getTextPrefences($preparation_order->preferences),
                    'estado_producto' => $preparation_order->estado_producto,
                    'browser_readiness_status' => $preparation_order->browser_readiness_status
                ];
            }
        }

        $this->data['preparation_orders'] = $preparation_order_array;

        $this->page_construct('pos/order_preparation', ['page_title' => lang('order_preparation')], $this->data);
    }

    private function getTextPrefences($preparationOrderPreferences)
    {
        $preferences = '';
        $textPreferences = '';
        if (!empty($preparationOrderPreferences)) {
            $preferences = json_decode($preparationOrderPreferences);
            foreach ($preferences as $preference) {
                $productPreferences = $this->pos_model->getProductPreferencesRestobar($preference);
            }

            foreach ($productPreferences as $productPreference) {
                $textPreferences .= $productPreference->name .', ';
            }
        }

        return trim($textPreferences, ', ');
    }

    public function get_order_preparations()
    {
        $activateNotification = false;
        $existingPendingStatus = false;
        $preparation_order_container = "";
        $preparation_orders = $this->pos_model->get_preparation_orders_by_preparer_user($this->session->userdata('user_id'));

        if (!empty($preparation_orders)) {
            foreach ($preparation_orders as $preparation_order) {
                $preparation_order_array[$preparation_order->id_venta_suspendida]['hora_orden'] = $preparation_order->hora_orden;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['estado_orden'] = $preparation_order->estado_orden;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['numero_mesa'] = $preparation_order->numero_mesa;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['fecha_orden'] = $preparation_order->fecha_orden;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['diferencia_minutos'] = $preparation_order->diferencia_minutos;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_venta_suspendida'] = $preparation_order->id_venta_suspendida;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_vendedor'] = $preparation_order->id_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['nombre_vendedor'] = $preparation_order->nombre_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['apellido_vendedor'] = $preparation_order->apellido_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['tipo'] = $preparation_order->tipo;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_mesa'] = $preparation_order->id_mesa;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['productos'][] = [
                    'nombre_producto' => $preparation_order->nombre_producto,
                    'cantidad_producto' => $preparation_order->cantidad_producto,
                    'preferencias' => $this->getTextPrefences($preparation_order->preferences),
                    'estado_producto' => $preparation_order->estado_producto,
                    'browser_readiness_status' => $preparation_order->browser_readiness_status
                ];
            }


            foreach ($preparation_order_array as $i => $preparation_order) {
                $products_string = '';
                $order_in_preparation = 0;
                $fecha = $preparation_order['fecha_orden'];
                $fecha1 = new DateTime($fecha);
                $fecha2 = new DateTime(date('Y-m-d H:i:s'));
                $intervalo = $fecha1->diff($fecha2);
                $minutes = $preparation_order['diferencia_minutos'] < 10 ? '0'.$preparation_order['diferencia_minutos'] : $preparation_order['diferencia_minutos'];
                $seconds = $intervalo->format('%s') < 10 ? '0'. $intervalo->format('%s') : $intervalo->format('%s');

                if ($preparation_order['tipo'] == "M") {
                    $tipo = lang('mesa')." ";
                } else if ($preparation_order['tipo'] == "D") {
                    $tipo = "Domicilio ";
                } else {
                    $tipo = "Llevar ";
                }

                foreach ($preparation_order['productos'] as $product) {
                    if ($product['browser_readiness_status'] == PREPARATION) { $order_in_preparation++; }

                    if ($product['estado_producto'] == CANCELLED) {
                        $products_string .= '<p class="bg-danger panel_body_item">'. $product['cantidad_producto'] .' '. $product['nombre_producto'] .' </p>';
                        if ($product['preferencias'] != '') {
                            $products_string .=  '<em class="preferences">'.$product['preferencias'].'</em>';
                        }
                    } else {
                        $products_string .= '<p class="panel_body_item">'. $product['cantidad_producto'] .' '. $product['nombre_producto'] .'</p>';
                        if ($product['preferencias'] != '') {
                            $products_string .= '<em class="preferences">'.$product['preferencias'].'</em>';
                        }
                    }
                }

                $panel_color = ($order_in_preparation > 0) ? "panel-warning" : "panel-success";
                if ($order_in_preparation == 0) { $existingPendingStatus = true; }

                $preparation_order_container .= '<div class="col-md-2">
                    <div class="panel '. $panel_color .' order_panel" id="'. $i . '" data-suspended_sale="'. $preparation_order["id_venta_suspendida"] .'" data-suspended_status_order="'. $preparation_order["estado_orden"] .'">
                        <div class="panel-heading order_panel_heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h3 style="font-weight: bold;">'. $tipo . $preparation_order['numero_mesa'] .'</h3>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label>Orden No: '. $preparation_order['id_venta_suspendida'] .'</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>'. $preparation_order['nombre_vendedor'] .'</label>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label>'. $minutes . ':'. $seconds . '
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body panel_body">'. $products_string .'</div>
                        </div>
                    </div>';
            }

            $activateNotification = ($existingPendingStatus) ? true : false;
        }

        $data['order_container'] = $preparation_order_container;
        $data['activateNotification'] = $activateNotification;

        echo json_encode($data);
    }

    public function get_order_preparation_modal($table_num)
    {
        $preparation_orders = $this->pos_model->get_preparation_orders_by_preparer_user($this->session->userdata('user_id'), $table_num, TRUE);

        $preparation_order_array = [];
        if (!empty($preparation_orders)) {
            foreach ($preparation_orders as $preparation_order) {
                $preparation_order_array[$preparation_order->id_venta_suspendida]['hora_orden'] = $preparation_order->hora_orden;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['numero_mesa'] = $preparation_order->numero_mesa;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['fecha_orden'] = $preparation_order->fecha_orden;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['diferencia_minutos'] = $preparation_order->diferencia_minutos;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_venta_suspendida'] = $preparation_order->id_venta_suspendida;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_vendedor'] = $preparation_order->id_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['nombre_vendedor'] = $preparation_order->nombre_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['apellido_vendedor'] = $preparation_order->apellido_vendedor;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['tipo'] = $preparation_order->tipo;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['id_mesa'] = $preparation_order->id_mesa;
                $preparation_order_array[$preparation_order->id_venta_suspendida]['productos'][] = [
                    'id_producto_cuenta' => $preparation_order->id_producto_cuenta,
                    'nombre_producto' => $preparation_order->nombre_producto,
                    'cantidad_producto' => $preparation_order->cantidad_producto,
                    'preferencias' => $this->getTextPrefences($preparation_order->preferences),
                    'estado_producto' => $preparation_order->estado_producto,
                    'browser_readiness_status' => $preparation_order->browser_readiness_status
                ];
            }
        }

        $preparation_order_modal_container = '';

        foreach ($preparation_order_array as $i => $preparation_order) {
            $fecha = $preparation_order['fecha_orden'];
            $fecha1 = new DateTime($fecha);
            $fecha2 = new DateTime(date('Y-m-d H:i:s'));
            $intervalo = $fecha1->diff($fecha2);
            $minutes = $preparation_order['diferencia_minutos'] < 10 ? '0'.$preparation_order['diferencia_minutos'] : $preparation_order['diferencia_minutos'];
            $seconds = $intervalo->format('%s') < 10 ? '0'. $intervalo->format('%s') : $intervalo->format('%s');

            if ($preparation_order['tipo'] == "M") {
                $tipo = lang('mesa')." ";
            } else if ($preparation_order['tipo'] == "D") {
                $tipo = "Domicilio ";
            } else {
                $tipo = "Llevar ";
            }

            $row_index = 0;
            $products_string = '';
            $order_in_preparation = 0;
            foreach ($preparation_order['productos'] as $product) {
                if ($product['estado_producto'] == CANCELLED) { $product_status_color = "danger"; }
                else if ($product['estado_producto'] == DISPATCHED) { $product_status_color = "success"; }
                else { $product_status_color = ""; }

                if ($product['browser_readiness_status'] == PREPARATION) { $order_in_preparation++; }

                $product_check=($product['estado_producto'] == ORDERED || $product['estado_producto']==PREPARATION) ? '<input class="product_check" type="checkbox" id="product_check_'. $row_index .'" value="'. $product["id_producto_cuenta"] .'">' : '';

                $product_preferences = ($product['preferencias'] != '') ? '<em class="preferences">'.$product['preferencias'].'</em>' : '';

                $products_string .= '<tr class="product_table_row '. $product_status_color .'" data-row_index="'. $row_index .'">
                                                        <td class="col-sm-1 text-center">'. $product_check .'</td>
                                                        <td class="col-sm-11">
                                                            <p class="panel_body_item">'.$product['cantidad_producto'] . '  '. strtoupper($product['nombre_producto']) .'</p>' . $product_preferences .'</td>
                                                    </tr>';

                $row_index++;
            }

            $panel_color = ($order_in_preparation > 0) ? "panel-warning" : "panel-success";

            $preparation_order_modal_container .= '<div class="modal fade" tabindex="-1" role="dialog" id="orden_modal_'.$i.'">
                            <div class="modal-dialog modal-md" role="document">
                                <div class="panel '. $panel_color .'" style=" border-radius: 10px;">
                                    <div class="panel-heading" style="padding: 15px 12px; border-top-left-radius: 10px; border-top-right-radius: 10px;">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <h1 style="font-size: 40px; margin-top: 10px; font-weight: bold; color: #fff; left: 0;">'. $tipo .$preparation_order['numero_mesa'].'</h1>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <h2 style="margin-top: 15px;">Orden No: '.$preparation_order['id_venta_suspendida'].'</h2>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <h2>'.$preparation_order['nombre_vendedor'].'</h2>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <h2>'.$minutes.':'.$seconds.'</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body" style="font-size: 20px; padding: 5px;">
                                        <table class="table">'. $products_string .'</table></div>
                                    <div class="panel-footer text-right" style="padding: 5px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
                                        <button class="btn btn-success btn-lg btn-block" id="dispatch_products" value="'.$preparation_order['tipo'].'" data-suspended_sale_id="'.$preparation_order['id_venta_suspendida'].'" data-table_id="'.$preparation_order['id_mesa'].'" style="font-size: 20px;">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>';
        }

        $data['modal_container'] = $preparation_order_modal_container;

        echo json_encode($data);
    }

    public function change_order_status()
    {
        $states_order = 0;
        $table_num = $this->input->post("order_panel_id");

        $preparation_orders = $this->pos_model->get_preparation_orders_by_preparer_user($this->session->userdata('user_id'), $table_num, TRUE);
        foreach ($preparation_orders as $order) {
            $browser_readiness_state_changed = $this->pos_model->change_browser_readiness_state(["browser_readiness_status" => PREPARATION], $order->id_producto_cuenta);
            if ($browser_readiness_state_changed == TRUE) {
                $states_order++;
            }
        }

        if ($states_order > 0) {
            $response_ajax = [
                "response" => 1,
                "message" => "El estado de la orden se encuentra en preparación"
            ];
        } else {
            $response_ajax = [
                "response" => 0,
                "message" => "No fue posible cambiar el estado de la orden"
            ];
        }

        echo json_encode($response_ajax);
    }

    public function dispatch_all_products_order()
    {
        $product_account = $this->input->post("suspend_sale_id");
        $dispatched_products = $this->pos_model->update_suspended_item_by_suspend_id(["state_readiness" => DISPATCHED], $product_account);

        echo $dispatched_products;
    }

    public function dispatch_products_order()
    {
        $product_account = json_decode($this->input->post("product_account"));

        $data = [];
        foreach ($product_account as $product) {
            $data[] = [
                "state_readiness" => DISPATCHED,
                "id" => $product
            ];
        }

        $dispatched_products = $this->pos_model->update_batch_suspended_item($data);
        if ($dispatched_products === TRUE) {
            $response_ajax = [
                "response" => "1",
                "message" => "Productos despachados correctamente"
            ];
        } else {
            $response_ajax = [
                "response" => "0",
                "message" => "Los productos no se han podido despachar"
            ];
        }

        echo json_encode($response_ajax);
    }

    public function delete_suspended_sale()
    {
        $suspended_sale_id = $this->input->post('suspended_sale_id');
        $table_id = $this->input->post('table_id');
        $suspend_sale_deleted = $this->Restobar_model->delete_suspend_sale($suspended_sale_id);

        if  ($suspend_sale_deleted == TRUE) {
            $this->Restobar_model->update_table(["estado" => AVAILABLE], $table_id);

            $response_ajax = [
                "response" => TRUE,
                "message" => "El pedido ha sido despachado"
            ];
        } else {
            $response_ajax = [
                "response" => FALSE,
                "message" => "El pedido no pudo ser despachado"
            ];
        }

        echo json_encode($response_ajax);
    }

    public function existing_products_not_dispatched()
    {
        $suspended_sale_id = $this->input->post('suspended_sale_id');
        $quantity_products_dispatched = $this->Restobar_model->get_existing_products_not_dispatched($suspended_sale_id);

        $existings_products = (count($quantity_products_dispatched) > 0) ? TRUE : FALSE;

        echo json_encode($existings_products);
    }

    public function existing_products_dispatched()
    {
        $suspended_sale_id = $this->input->get('suspended_sale_id');
        $quantity_products_dispatched = $this->Restobar_model->get_existing_products_dispatched($suspended_sale_id);

        $existings_products = (count($quantity_products_dispatched) > 0) ? TRUE : FALSE;

        echo json_encode($existings_products);
    }

    public function voucher_delivery($sale_id = NULL, $redirect_to_pos = FALSE)
    {
        $this->sma->checkPermissions('index');
        if ($this->input->get('id')) {
            $sale_id = $this->input->get('id');
        }
        $this->load->helper('pos');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['message'] = $this->session->flashdata('message');
        $inv = $this->pos_model->getInvoiceByID($sale_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['rows'] = $this->pos_model->getAllInvoiceItems($sale_id);
        $biller_id = $inv->biller_id;
        $customer_id = $inv->customer_id;
        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($biller_id);

        // Consulta que busca los productos agrupados de acuerdo al parametro configurado en biller_data
        $product_order = $this->data['biller_data']->product_order;
        $this->data['customer'] = $this->pos_model->getCompanyByID($customer_id);
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['payments'] = $this->pos_model->getInvoicePayments($sale_id);

        $this->data['pos'] = $this->pos_model->getSetting();
        $this->data['barcode'] = $this->barcode($inv->reference_no, 'code128', 30);
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
        $this->data['inv'] = $inv;
        $this->data['sid'] = $sale_id;
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['printer'] = $this->pos_model->getPrinterByID($this->pos_settings->printer);
        $this->data['page_title'] = $this->lang->line("invoice");
        $this->data['sale_id'] = $sale_id;
        $this->data['direct_print_job_id'] = NULL;
        $this->data['seller'] = $this->site->getCompanyByID($inv->seller_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['tipo_regimen'] = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['redirect_to_pos'] = $redirect_to_pos ? true : false;
        $this->data['ciiu_code'] = $this->site->get_ciiu_code_by_id($this->Settings->ciiu_code);

        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;

        $this->data['print_directly'] = $print_directly;
        $url_format = "pos/voucher_delivery";
        $this->load_view($this->theme . $url_format, $this->data);
    }

    public function generate_pos_invoice_pdf()
    {
        $trmrate = 1;
        $taxes_details = [];

        $invoice_id = $this->input->post("id");
        $taxes = $this->site->getAllTaxRates();
        $invoice_data = $this->pos_model->getInvoiceByID($invoice_id);

        if (isset($invoice_data->sale_currency)) {
            $currency = $this->site->getCurrencyByCode($invoice_data->sale_currency);
        }

        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);

        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = $tax->rate;
        }

        if (!empty($invoice_data->sale_currency) && $invoice_data->sale_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $invoice_data->sale_currency_trm;
        }

        $this->data['tax_inc'] = TRUE;
        $this->data['view_tax'] = TRUE;
        $this->data['sma'] = $this->sma;
        $this->data['trmrate'] = $trmrate;
        $this->data['inv'] = $invoice_data;
        $this->data['internal_download'] = TRUE;
        $this->data['taxes_details'] = $taxes_details;
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $this->data['rows'] = $this->pos_model->getAllInvoiceItems($invoice_id);
        $this->data['seller'] = $this->site->getCompanyByID($invoice_data->seller_id);
        $this->data['biller'] = $this->site->getCompanyByID($invoice_data->biller_id);
        $this->data['customer'] = $this->site->getCompanyByID($invoice_data->customer_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($invoice_data->document_type_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($invoice_data->document_type_id, $invoice_data->reference_no);
        $this->data['signature_root'] = is_file("assets/uploads/signatures/".$this->Settings->digital_signature) ? base_url().'assets/uploads/signatures/'.$this->Settings->digital_signature : FALSE;


        $this->load_view($this->theme . "pos/pos_invoice", $this->data);

        echo json_encode(["response" => 1]);
    }

    public function validate_fe_customer_data($customer_id = NULL)
    {
        if (empty($customer_id)) {
            return FALSE;
            exit();
        }

        $customer = $this->site->getCompanyByID($customer_id);

        if (empty($customer)) {
            return FALSE;
            exit();
        }

        $tipo_documento = $this->site->get_document_type_by_id($customer->tipo_documento);
        $country = $this->site->getCountryByName($customer->country);
        $is_invalid = false;
        $empty_columns = [];

        $required_data = [
            'type_person',
            'vat_no',
            'tipo_regimen',
            'address',
            'country',
            'state',
            'city',
            "email"
        ];

        if ($customer->type_person == LEGAL_PERSON) {
            $required_data[] = "digito_verificacion";
            $required_data[] = "name";
            $required_data[] = "company";
        } else if ($customer->type_person == NATURAL_PERSON) {
            $required_data[] = "first_name";
            $required_data[] = "first_lastname";
        }

        foreach ($required_data as $key => $column) {
            if ($column != "digito_verificacion") {
                if (empty($customer->{$column})) {
                    $empty_columns[] = $column;
                    if ($is_invalid == false) {
                        $is_invalid = true;
                    }
                }
            } else {
                if ($customer->{$column} == "") {
                    $empty_columns[] = $column;
                    if ($is_invalid == false) {
                        $is_invalid = true;
                    }
                }
            }
        }

        if (!$tipo_documento) {
            $empty_columns[] = lang('id_document_type');
        }

        if (!$country || empty($country->codigo_iso)) {
            $empty_columns[] = lang('codigo_iso');
        }

        if ($is_invalid) {
            $msg = lang('empty_customer_columns');
            $msg.= ' <a href="'.admin_url('customers/edit/').$customer_id.'" data-toggle="modal" data-target="#myModal"> '.lang('clic_x_edit').' </a>';
        } else {
            $msg = '';
        }

        echo $msg;
    }

    private function createDocumentElectronic($invoiceData)
    {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            return true;
            return true;
            return true;
        }
        $invoiceData = (object) $invoiceData;
        $createdFile = $this->Electronic_billing_model->send_document_electronic($invoiceData);
        $filename = $this->site->getFilename($invoiceData->sale_id);
        $technologyProvider = $this->pos_settings->technology_provider_id;

        if ($createdFile->response == 0) {
            $this->session->set_flashdata('error', $createdFile->message);
        } else {
            $emailDeliveryResponse = '';

            if (in_array($technologyProvider, [CADENA, BPM, DELCOP, SIMBA])) {
                if ($technologyProvider == SIMBA) {
                    $sale = $this->site->getSaleByID($invoiceData->sale_id);
                    $biller_data = $this->companies_model->getBillerData(['biller_id' => $sale->biller_id]); // <- biller data para validar el consumidor por defecto en la sucursal
                    if ($biller_data->default_customer_id == $sale->customer_id) { // <- Sí, el cliente de la factura es el mismo cliente por defecto de la sucursal, entra aca y no envia correo
                        $this->session->set_flashdata('message', 'Documento electrónico creado correctamente');
                        $this->setUserActivities($invoiceData->sale_id);
                        return true;
                    }
                }
                $this->downloadFeXml($invoiceData->sale_id, TRUE);
                $this->downloadFePdf($invoiceData->sale_id, TRUE);

                if (file_exists('files/electronic_billing/' . $filename . '.xml') && file_exists('files/electronic_billing/' . $filename . '.pdf')) {
                    $zip = new ZipArchive();

                    $zip->open('files/electronic_billing/' . $filename . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
                    $zip->addFile("files/electronic_billing/" . $filename . '.xml', $filename . ".xml");
                    $zip->addFile("files/electronic_billing/" . $filename . '.pdf', $filename . ".pdf");

                    if ($invoiceData->attachment != '') {
                        $zip->addFile("files/" . $invoiceData->attachment, $invoiceData->attachment);
                    }

                    $zip->close();

                    unlink('files/electronic_billing/' . $filename . '.xml');
                    unlink('files/electronic_billing/' . $filename . '.pdf');

                    if ($_SERVER['SERVER_NAME'] != 'localhost') {
                        if ($this->validateDefaultBranchCustomerExists($invoiceData)) {
                            $emailDeliveryResponse = $this->Electronic_billing_model->receipt_delivery($invoiceData, $filename);
                        } else {
                            $this->site->updateSale(["fe_correo_enviado" => 2], $invoiceData->sale_id);
                        }
                    }
                }
                if (file_exists('files/electronic_billing/' . $filename . '.xml')) {
                    unlink('files/electronic_billing/' . $filename . '.xml');
                }
                if (file_exists('files/electronic_billing/' . $filename . '.pdf')) {
                    unlink('files/electronic_billing/' . $filename . '.pdf');
                }
            }

            $this->session->set_flashdata('message', 'Documento electrónico creado correctamente <br>' . $emailDeliveryResponse);

            $this->setUserActivities($invoiceData->sale_id);
        }
    }

    public function validateDefaultBranchCustomerExists($documentData)
    {
        $emails = $this->Electronic_billing_model->getDefaultBranchesCustomer(['default_customer_id' => $documentData->customer_id, 'biller_id' => $documentData->biller_id]);
        if (!empty($emails)) {
            return false;
        }
        return true;
    }

    public function send_electronic_invoice_email($document_id = NULL, $email = NULL, $from_change_status_method = FALSE)
    {
        $document_id = !empty($document_id) ? $document_id : $this->input->get("sale_id");
        $email = !empty($email) ? $email : $this->input->get("email");

        $email_delivery_response = '';
        $sale = $this->site->getSaleByID($document_id);
        $filename = $this->site->getFilename($document_id);

        $invoice_data = (object) [
            'sale_id' => $sale->id,
            'biller_id' => $sale->biller_id,
            'customer_id' => $sale->customer_id,
            'reference' => $sale->reference_no
        ];

        if (!empty($email)) {
            $invoice_data->email = $email;
        }

        $this->downloadFeXml($invoice_data->sale_id, TRUE);
        $this->downloadFePdf($invoice_data->sale_id, TRUE);

        if (file_exists('files/electronic_billing/' . $filename . '.xml') && file_exists('files/electronic_billing/' . $filename . '.pdf')) {
            $zip = new ZipArchive();

            $zip->open('files/electronic_billing/' . $filename . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
            $zip->addFile("files/electronic_billing/" . $filename . '.xml', $filename . ".xml");
            $zip->addFile("files/electronic_billing/" . $filename . '.pdf', $filename . ".pdf");

            if ($sale->attachment != '') {
                $zip->addFile("files/" . $sale->attachment, $sale->attachment);
            }

            $zip->close();

            $email_delivery_response = $this->Electronic_billing_model->receipt_delivery($invoice_data, $filename);
        }
        if (file_exists('files/electronic_billing/' . $filename . '.xml')) {
            unlink('files/electronic_billing/' . $filename . '.xml');
        }
        if (file_exists('files/electronic_billing/' . $filename . '.pdf')) {
            unlink('files/electronic_billing/' . $filename . '.pdf');
        }

        if ($from_change_status_method == FALSE) {
            if (empty($email)) {
                $this->session->set_flashdata('message', 'Documento electrónico enviado correctamente' . $email_delivery_response);
            } else {
                $this->session->set_flashdata('message', $email_delivery_response);
            }

            admin_redirect('pos/fe_index');
        } else {
            return $email_delivery_response;
        }
    }

    public function downloadFeXml($documentId, $internal_download = FALSE)
    {
        $sale = $this->site->getSaleByID($documentId);
        $technologyProvider = $this->pos_settings->technology_provider_id;
        $filename = $this->site->getFilename($documentId);

        if ($technologyProvider == CADENA) {
            if (!empty($sale->fe_xml)) {
                $xml_file = base64_decode($sale->fe_xml);

                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $path = 'files/electronic_billing';
                    if (!file_exists($path)) {
                        mkdir($path, 0777);
                    }

                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                } else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            } else {
                $prefix = strstr($sale->reference_no, '-', TRUE);
                $reference = str_replace('-', '', $sale->reference_no);
                $document_type_code = ($sale->total > 0) ? '01' : '92';

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/documentos?nit_emisor=' . $this->Settings->numero_documento . '&id_documento=' . $reference . '&codigo_tipo_documento=' . $document_type_code . '&prefijo=' . $prefix,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => [
                        'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                        'Content-Type: application/json',
                        'Partnership-Id: 901090070'
                    ],
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                $response = json_decode($response);
                if (!empty($response)) {
                    if (isset($response->statusCode)) {
                        if ($response->statusCode == 200) {
                            $updated_sale = $this->site->updateSale([
                                'fe_aceptado' => 2,
                                'fe_mensaje' => 'Documento aceptado por la DIAN',
                                'fe_mensaje_soporte_tecnico' => 'La Factura electrónica ' . $reference . ', ha sido autorizada. Código es estado ' . $response->statusCode,
                                'fe_xml' => $response->document
                            ], $documentId);

                            if ($updated_sale == TRUE) {
                                $xml_file = base64_decode($response->document);

                                if ($internal_download === TRUE) {
                                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                                    $xml->loadXML($xml_file);

                                    $path = 'files/electronic_billing';
                                    if (!file_exists($path)) {
                                        mkdir($path, 0777);
                                    }

                                    $xml->save('files/electronic_billing/' . $sale->reference_no . '.xml');
                                } else {
                                    header('Content-Type: application/xml;');
                                    header('Content-Disposition: attachment; filename="' . $sale->reference_no . '.xml"');
                                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                                    $xml->loadXML($xml_file);
                                    echo $xml->saveXML();
                                }
                            } else {
                                $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML.');
                                admin_redirect('sales/fe_index');
                            }
                        } else if ($response->statusCode == 400) {
                            $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML. El prefijo no coincide con el prefijo del id del documento.');
                            admin_redirect('sales/fe_index');
                        } else if ($response->statusCode == 404) {
                            $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML. Registro no encontrado con los parámetros de búsqueda enviados.');
                            admin_redirect('sales/fe_index');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Se ha perdido la conexión con el Web Services.');
                        admin_redirect('sales/fe_index');
                    }
                } else {
                    $this->session->set_flashdata('error', 'No es posible realizar conexión con Web Services.');
                    admin_redirect('sales/fe_index');
                }
            }

        } else if ($technologyProvider == DELCOP) {
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
            $response_token = $this->Electronic_billing_model->delcopGetAuthorizationoken();
            if ($response_token->success == FALSE) {
                $this->session->set_flashdata('error', "Los campos de Usuario o Contraseña DELCOP no son correctos. " . $response_token->error);
                admin_redirect('pos/fe_index');
            }

            if ($this->Settings->fe_work_environment == TEST) {
                $url = "https://www-prueba.titanio.com.co/PDE/public/api/PDE/descargar";
            } else {
                $url = "https://www.titanio.com.co/PDE/public/api/PDE/descargar";
            }

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => '{"token": "' . $response_token->token . '", "documentos": [{"transaccion_id": "' . $sale->fe_id_transaccion . '", "tipo_descarga": "1"}]}',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $response = json_decode($response);

            if ($response->error_id == 0) {
                $sale_data["fe_xml"] = $response->documentos[0]->data;
                $updated_sale = $this->site->updateSale($sale_data, $sale->id);

                $xml_file = base64_decode($response->documentos[0]->data);
                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                } else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            }
        } else if ($technologyProvider == BPM) {
            $xml_file = base64_decode($sale->fe_xml);

            if ($internal_download === TRUE) {
                $xml = new DOMDocument("1.0", "ISO-8859-15");
                $xml->loadXML($xml_file);

                $path = 'files/electronic_billing';
                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }

                $xml->save('files/electronic_billing/' . $filename . '.xml');
            } else {
                header('Content-Type: application/xml;');
                header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                $xml = new DOMDocument("1.0", "ISO-8859-15");

                $xml->loadXML($xml_file);
                echo $xml->saveXML();
            }
        } else if ($technologyProvider == SIMBA) {
            if (!empty($sale->fe_xml)){
                $fe_xml = $sale->fe_xml;
                $fe_xml = $this->Electronic_billing_model->getXMLSimba($sale->id);
            }else{
                $typeDocument = $this->site->getTypeElectronicDocument($sale->id);
                if ($typeDocument == INVOICE) {
                    sleep(5);
                }
                if ($typeDocument == CREDIT_NOTE) {
                    sleep(7);
                }
                $fe_xml = $this->Electronic_billing_model->getXMLSimba($sale->id);
            }
            if ($fe_xml) {
                $xml_file = base64_decode($fe_xml);
                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);
                    $path = 'files/electronic_billing';
                    if (!file_exists($path)) {
                        mkdir($path, 0777);
                    }
                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                }else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            } else {
                $this->session->set_flashdata('error', 'No es posible realizar conexión con Web Services.');
                admin_redirect('pos/fe_index');
            }
        }
    }

    public function downloadFePdf($sale_id, $internal_download = FALSE)
    {
        if ($this->pos_settings->technology_provider_id == CADENA) {
            $this->saleView($sale_id, $internal_download);
        } else if ($this->pos_settings->technology_provider_id == DELCOP) {
            if ($this->Settings->wappsi_print_format == YES) {
                $this->saleView($sale_id, $internal_download);
            }
        } else if($this->pos_settings->technology_provider_id == SIMBA){
            $this->saleView($sale_id, $internal_download);
        }
    }

    public function restobar_table_grid($id_sucursal)
    {
        $branch_id = $id_sucursal;

        if ($this->Owner || $this->Admin) {
            $branch_id = (! empty($branch_id)) ? $branch_id : $this->Settings->default_biller;
        } else {
            $branch_id = (! empty($branch_id)) ? $branch_id : $this->session->biller_id;
        }

        $branch_areas = $this->Restobar_model->get_branch_areas($branch_id);
        foreach ($branch_areas as $key => $area) {
            if ($area->nombre == "Domicilio" || $area->nombre == "Llevar") {
            } else {

                $tables = $this->Restobar_model->get_tables($area->id);
                $area->tables = $tables;
            }
        }

        $this->data['branch_areas'] = $branch_areas;

        $this->load_view($this->theme.'pos/restobar_table_grid', $this->data);
    }

    public function pos_register_movements($id = null)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('pos_register_movements')));
        $meta = array('page_title' => lang('pos_register_movements'), 'bc' => $bc);

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([37]);
        $this->data['users'] = $this->site->get_all_users();

        $this->page_construct('pos/pos_register_movements', $meta, $this->data);
    }

    public function get_register_movements()
    {
        // $this->sma->checkPermissions('expenses');


        if ($this->input->post('start_date')) {
             $start_date = $this->input->post('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        $document_type_id = NULL;
        if ($this->input->post('posrmpayment_reference_no')) {
            $document_type_id = $this->input->post('posrmpayment_reference_no');
        }
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $movement_type = $this->input->post('movement_type') ? $this->input->post('movement_type') : NULL;

        $this->load->library('datatables');

        $this->datatables
            ->select(
                        $this->db->dbprefix('pos_register_movements') . ".id as id,
                        date,
                        reference_no,
                        amount,
                        IF(movement_type = 1, 'Entrada', 'Salida') as movement_type,
                        note,
                        CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user"
                        , false)
            ->from('pos_register_movements')
            ->join('users', 'users.id=pos_register_movements.created_by', 'left');



        /* FILTROS */

        if ($start_date) {
            $this->db->where('pos_register_movements.date >= ', $start_date);
        }
        if ($end_date) {
            $this->db->where('pos_register_movements.date <= ', $end_date);
        }
        if ($document_type_id) {
            $this->db->where('pos_register_movements.document_type_id', $document_type_id);
        }
        if ($user_id) {
            $this->db->where('pos_register_movements.created_by', $user_id);
        }
        if ($biller_id) {
            $this->db->where('pos_register_movements.biller_id', $biller_id);
        }
        if ($movement_type) {
            $this->db->where('pos_register_movements.movement_type', $movement_type);
        }



        /* FILTROS */

        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }
        echo $this->datatables->generate();
    }

    public function pos_register_movement_view($id){
        $register_movement = $this->pos_model->get_register_movement($id);
        $this->data['user'] = $this->site->getUser($register_movement->created_by);
        $this->data['biller'] = $register_movement->biller_id ? $this->site->getCompanyByID($register_movement->biller_id) : NULL;
        $this->data['register_movement'] = $register_movement;
        $this->data['page_title'] = $this->lang->line("pos_register_movement_view");
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($register_movement->cost_center_id);
        }
        $this->load_view($this->theme . 'pos/pos_register_movement_view', $this->data);
    }

    public function pos_register_add_movement()
    {
        if ($this->session->userdata('register_cash_movements_with_another_user')) {
            if (!$this->pos_model->registerData($this->session->userdata('register_cash_movements_with_another_user'))) {
                $this->session->set_flashdata('error', lang('another_user_cash_not_open'));
                admin_redirect('welcome');
            }
        } else {
            if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register/1');
            }
        }
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('mv_amount', lang("amount"), 'required');
        $this->form_validation->set_rules('mv_biller', lang("biller"), 'required');
        $this->form_validation->set_rules('mv_document_type_id', lang("reference"), 'required');
        $this->form_validation->set_rules('mv_movement_type', lang("movement_type"), 'required');
        if ($this->form_validation->run() == true) {
            // $this->sma->print_arrays($_POST);
            $date = date('Y-m-d H:i:s');
            $biller_id = $this->input->post('mv_biller');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            $movement_type = $this->input->post('mv_movement_type');
            $amount = $this->input->post('mv_amount');
            $note = $this->input->post('mv_note');
            $document_type_id = $this->input->post('mv_document_type_id');
            $mv_origin_paid_by = $this->input->post('mv_origin_paid_by');
            $mv_destination_paid_by = $this->input->post('mv_destination_paid_by');
            $mv_destination_paid_by = $this->input->post('mv_destination_paid_by');
            $destination_biller_id = $this->input->post('destination_mv_biller');
            $destination_user_id = $this->input->post('mv_user');
            if ($this->Settings->cashier_close != 2 && $movement_type == 2) {
                if (!$this->pos_model->getRegisterState($amount)) {
                    $this->session->set_flashdata('error', lang('not_enough_cash_register'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect($_SERVER["HTTP_REFERER"]);
            }
            $data = array(
                        'date' => $date,
                        'movement_type' => $movement_type,
                        'amount' => $amount,
                        'note' => $note,
                        'origin_paid_by' => $mv_origin_paid_by,
                        'destination_paid_by' => $mv_destination_paid_by,
                        'biller_id' => $biller_id,
                        'document_type_id' => $document_type_id,
                        'destination_biller_id' => $movement_type == 3 ? $destination_biller_id : NULL,
                        'destination_user_id' => $movement_type == 3 ? $destination_user_id : NULL,
                        'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    );
            $destination_data = [];
            if ($movement_type == 3) {
                $destination_data = array(
                            'date' => $date,
                            'movement_type' => 1,
                            'amount' => $amount,
                            'note' => $note,
                            'origin_paid_by' => $mv_destination_paid_by ,
                            'destination_paid_by' => $mv_origin_paid_by,
                            'biller_id' => $destination_biller_id,
                            'document_type_id' => $document_type_id,
                            'created_by' => $destination_user_id,
                        );
            }
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('mv_cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('mv_cost_center_id');
            }
        }
        if ($this->form_validation->run() == true && $this->pos_model->add_movement($data, $destination_data)) {
            $this->session->set_flashdata('message', lang("pos_register_movement_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->session->set_flashdata('error', validation_errors());
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['users'] = $this->site->get_all_users();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->load_view($this->theme . 'pos/pos_register_add_movement', $this->data);
        }
    }

    public function register_movements_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');
        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('expenses'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('amount'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('movement_type'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('note'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('created_by'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $register_movement = $this->pos_model->get_register_movement($id);
                        $user = $this->site->getUser($register_movement->created_by);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($register_movement->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $register_movement->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $this->sma->formatMoney($register_movement->amount));
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $register_movement->movement_type == 1 ? 'Entrada' : 'Salida');
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $register_movement->note);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $user->first_name . ' ' . $user->last_name);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'pos_register_movements_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } else if ($this->input->post('form_action') == 'reaccount') {
                    foreach ($_POST['val'] as $id) {
                        $register_movement = $this->pos_model->recontabilizar_movimiento_caja($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("register_movements_posted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_expense_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function pa_processor($pa_view = false, $preparation_area_products = NULL, $preparation_area_id = NULL, $finished_invoice_id = false, $locator = false)
    {
        if ($pa_view == 'false') {
            $pa_view = false;
        }
        if ($pa_view === false) {
            $this->data['locator'] = $locator;
            $this->data['preparation_area_products'] = $preparation_area_products;
            $this->data['finished_invoice_id'] = $finished_invoice_id;
            $this->load_view($this->theme . 'pos/pa_processor', $this->data);
        } else if ($pa_view) {
            $this->data['locator'] = $locator;
            $this->data['preparation_area_id'] = $preparation_area_id;
            $this->data['preparation_area_data'] = $this->pos_model->get_preparation_area_by_id($preparation_area_id);
            $this->load_view($this->theme . 'pos/preparation_area_view', $this->data);
        }
    }

    public function product_variants_selection($product_id, $positems_id, $warehouse_id, $search = NULL)
    {
        $options = $this->products_model->getProductOptions($product_id, $warehouse_id, $search);
        if (!empty($options)) {
            $html = "";
            $html .= '<ul class="ob">';
            foreach ($options as $option) {
                if (($option->quantity <= 0 && $this->Settings->overselling == 0)) {
                    continue;
                }
                $html .= '<li>
                            <button type="button" class="btn btn-wappsi-pausadas btn-outline product_variant_select" data-weigth="'.$option->weight.'"  data-pvid="' . $option->id . '">
                                <strong style="font-size:120%;" class="suspend_note">' . $option->name . '</strong><br>
                                <strong>' . $this->sma->formatQuantity($option->quantity) . '</strong>
                            </button>
                        </li>';
            }
            $html .= '</ul>';
        } else {
            $html = "<h3>" . lang('no_product_variants') . "</h3><p>&nbsp;</p>";
        }

        $data['html'] = $html;
        $data['product_id'] = $product_id;
        $data['positems_id'] = $positems_id;
        $data['pdata'] = $this->site->getProductByID($product_id);
        $this->load_view($this->theme . 'pos/product_variants_selection', $data);
    }

    public function product_preferences_selection($product_id, $positems_id, $arritems_name = 'positems', $search = NULL)
    {
        $arritems_name = $arritems_name == 'null' ? 'positems' : $arritems_name;
        $preferences = $this->products_model->getProductPreferences($product_id, $search);
        if (!empty($preferences)) {
            $html = "";
            $html .= '<ul class="ob">';
            $actual_category = null;
            foreach ($preferences as $preference) {
                if ($actual_category == null || ($actual_category != $preference->preference_category_id)) {
                    $html.="<li style='width:100%;' class='text-center head_cat' data-pcat='".$preference->prf_cat_name."' data-pcat_req='".$preference->required."' data-pcat_sl='".$preference->selection_limit."'>
                                <hr>
                                <h2 class='category_preference_name'>".$preference->prf_cat_name."</h2>
                                <br>
                                <em class='text-danger cat_error' style='display:none;' data-pcat='".$preference->prf_cat_name."'>No ha seleccionado ninguna preferencia para esta categoría</em>
                                ".($preference->selection_limit > 0 ? "<em>(".$preference->selection_limit." máximo)</em>" : "")."
                            </li>";
                    $actual_category = $preference->preference_category_id;
                }
                $html .= '<li class="li_preferences_selection body_cat" data-pcat="'.$preference->prf_cat_name.'" data-pcat_req="'.$preference->required.'" data-pcat_sl="'.$preference->selection_limit.'">
                            <button type="button" class="product_preference_select" data-pfcatid="'.$preference->preference_category_id.'" data-pfid="' . $preference->id . '">
                                <strong style="font-size:120%;" class="suspend_note">' . $preference->name . '</strong>
                                <input type="radio" class="form-control radio_pref" style="float:right;">
                            </button>
                        </li>';
            }
            $html .= '</ul>';
        } else {
            $html = "<h3>" . lang('no_product_preferences') . "</h3><p>&nbsp;</p>";
        }

        $data['html'] = $html;
        $data['product_id'] = $product_id;
        $data['arritems_id'] = $positems_id;
        $data['arritems_name'] = $arritems_name;
        $data['pdata'] = $this->site->getProductByID($product_id);
        $data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'pos/product_preferences_selection', $data);
    }

    public function change_payment_method($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('paid_by[]', lang('paid_by'), 'required');
        $inv = $this->pos_model->getInvoiceByID($id);
        if ($this->form_validation->run() == TRUE) {
            $data = [];
            $pm_commision_value = [];
            $pm_commision_value_info = [];
            $pm_commision_changed = [];
            $pm_retefuente_value = [];
            $pm_retefuente_value_info = [];
            $pm_retefuente_changed = [];
            $pm_reteiva_value = [];
            $pm_reteiva_value_info = [];
            $pm_reteiva_changed = [];
            $pm_reteica_value = [];
            $pm_reteica_value_info = [];
            $pm_reteica_changed = [];
            $payments_id = $_POST['payment_id'];
            foreach ($_POST['paid_by'] as $key => $paid_by) {
                $data[$payments_id[$key]] = $paid_by;
                $setted_retcom = $_POST['setted_retcom'][$payments_id[$key]];
                if ($setted_retcom == 1) {
                    $pm_commision_value[$payments_id[$key]] = $_POST['pm_commision_value'][$payments_id[$key]];
                    $pm_commision_value_info[$payments_id[$key]] = $_POST['pm_commision_value_info'][$payments_id[$key]];
                    $pm_commision_changed[$payments_id[$key]] = $_POST['pm_commision_changed'][$payments_id[$key]];
                    $pm_retefuente_value[$payments_id[$key]] = $_POST['pm_retefuente_value'][$payments_id[$key]];
                    $pm_retefuente_value_info[$payments_id[$key]] = $_POST['pm_retefuente_value_info'][$payments_id[$key]];
                    $pm_retefuente_changed[$payments_id[$key]] = $_POST['pm_retefuente_changed'][$payments_id[$key]];
                    $pm_reteiva_value[$payments_id[$key]] = $_POST['pm_reteiva_value'][$payments_id[$key]];
                    $pm_reteiva_value_info[$payments_id[$key]] = $_POST['pm_reteiva_value_info'][$payments_id[$key]];
                    $pm_reteiva_changed[$payments_id[$key]] = $_POST['pm_reteiva_changed'][$payments_id[$key]];
                    $pm_reteica_value[$payments_id[$key]] = $_POST['pm_reteica_value'][$payments_id[$key]];
                    $pm_reteica_value_info[$payments_id[$key]] = $_POST['pm_reteica_value_info'][$payments_id[$key]];
                    $pm_reteica_changed[$payments_id[$key]] = $_POST['pm_reteica_changed'][$payments_id[$key]];
                }
            }
            $ret_com = [
                            'pm_commision_value' => $pm_commision_value,
                            'pm_commision_value_info' => $pm_commision_value_info,
                            'pm_commision_changed' => $pm_commision_changed,
                            'pm_retefuente_value' => $pm_retefuente_value,
                            'pm_retefuente_value_info' => $pm_retefuente_value_info,
                            'pm_retefuente_changed' => $pm_retefuente_changed,
                            'pm_reteiva_value' => $pm_reteiva_value,
                            'pm_reteiva_value_info' => $pm_reteiva_value_info,
                            'pm_reteiva_changed' => $pm_reteiva_changed,
                            'pm_reteica_value' => $pm_reteica_value,
                            'pm_reteica_value_info' => $pm_reteica_value_info,
                            'pm_reteica_changed' => $pm_reteica_changed,
                        ];
        } else if ($this->input->post('change_payment_method')) {
            $this->session->set_flashdata('error', validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            admin_redirect("pos/sales");
        }
        if ($this->form_validation->run() == TRUE && $this->pos_model->update_inv_payment_method($id, $data, $inv->customer_id, $ret_com)) {
            $msg = $this->sales_model->recontabilizarVenta($id);
            $this->site->syncSalePayments($id);
            if ($this->session->userdata('reaccount_error')) {
                $this->session->set_flashdata('error', lang("payment_method_updated").", ".$msg);
                $this->session->unset_userdata('reaccount_error');
            } else {
                $this->session->set_flashdata('message', lang("payment_method_updated").", ".$msg);
            }
            admin_redirect("pos/sales");
        } else {
            $pos_register_status = $this->pos_model->get_invoice_pos_register_status($inv);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['inv'] = $inv;
            $this->data['pos_register_status'] = $pos_register_status;
            $this->data['payments'] = $this->site->getSalePayments($id);
            if (!$pos_register_status) {
                $this->session->set_flashdata('error', lang('invoice_from_pos_register_closed'));
            }
            if ($inv->return_sale_total < 0) {
                $this->session->set_flashdata('error', lang('invoice_already_returned'));
                $this->data['pos_register_status'] = false;
            }
            $this->load_view($this->theme . 'pos/change_payment_method', $this->data);
        }
    }

    public function see_order_preparation()
    {
        $this->sma->checkPermissions();
        $this->data['preparation_order'] = $this->pos_model->get_all_preparation_order();

        $this->page_construct('pos/see_order_preparation', ['page_title' => lang('see_order_preparation')], $this->data);
    }

    public function email($id = null)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->pos_model->getInvoiceByID($id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);
            $this->send_sale_mail($id, $to, $subject);
        } elseif ($this->input->post('send_email')) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('invoice') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
            );
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'pos/email', $this->data);
        }
    }

    public function send_sale_mail($id, $to, $subject){
        $this->view($id, false, false, false, true);
        $inv = $this->sales_model->getInvoiceByID($id);
        $template = file_get_contents('./themes/default/admin/views/email_templates/pos_sale.html');
        $attachment =  FCPATH.'files/'.$inv->reference_no.'.pdf';
        $this->load->library('parser');
        $customer = $this->site->getCompanyByID($inv->customer_id);
        $biller = $this->site->getCompanyByID($inv->biller_id);
        $parse_data = array(
            'reference_number' => $inv->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $biller->logo . '" alt="' . ($biller->company != '-' ? $biller->company : $biller->name) . '"/>',
        );
        $email_content = $this->parser->parse_string($template, $parse_data);
        $parse_data = array(
            'email_title' => $subject,
            'email_content' => $email_content,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
        );
        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/email_template_background.php');
        $message = $this->parser->parse_string($msg, $parse_data);
        try {
            if ($this->sma->send_email($to, $subject, $message, null, null, $attachment)) {
                unlink($attachment);
                $this->session->set_flashdata('message', lang("email_sent"));
                admin_redirect("pos/sales");
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function pos_print_server()
    {
        $data = [];
        $invoiceToPrint = '';

        if (!empty($this->session->userdata("pos_document_type_id"))) {
            $data[] = $this->session->userdata("pos_document_type_id");
        }
        if (!empty($this->session->userdata("fe_pos_document_type_id"))) {
            $data[] = $this->session->userdata("fe_pos_document_type_id");
        }

        $unprintedDocuments = $this->pos_model->select_unprinted_documents($data);

        foreach ($unprintedDocuments as $key => $document) {
            if (($document->factura_electronica == 1 && !empty($document->cufe)) || ($document->factura_electronica == 0)) {
                if ($document->printed == 0) {
                    $invoiceToPrint = $document->id;
                    break;
                }
            }
        }

        $this->data["invoiceToPrint"] = $invoiceToPrint;
        $this->data["unprinted_documents"] = $unprintedDocuments;

        $meta = ['page_title' => lang("pos_print_server")];
        $this->page_construct('pos/print_server', $meta, $this->data);
    }

    public function get_unprinted_documents()
    {
        $data = [];
        $invoiceToPrint = '';

        if (!empty($this->session->userdata("pos_document_type_id"))) {
            $data[] = $this->session->userdata("pos_document_type_id");
        }

        if (!empty($this->session->userdata("fe_pos_document_type_id"))) {
            $data[] = $this->session->userdata("fe_pos_document_type_id");
        }

        $unprintedDocuments = $this->pos_model->select_unprinted_documents($data);
        foreach ($unprintedDocuments as $key => $document) {
            if (($document->factura_electronica == 1 && !empty($document->cufe)) || ($document->factura_electronica == 0)) {
                if ($document->printed == 0) {
                    $invoiceToPrint = $document->id;
                    break;
                }
            }
        }

        echo json_encode([
                "unprinted_documents" =>$unprintedDocuments,
                "invoiceToPrint" => $invoiceToPrint
            ]
        );
    }

    public function print_server()
    {
        $document_id = $this->input->post('document_id');
        $inv = $this->pos_model->getInvoiceByID($document_id);
        $document_type = $this->site->getDocumentTypeById($inv->document_type_id);
        $this->createQr($inv);

        $url_format = "pos/view";
        $biller_id = $inv->biller_id;
        $customer_id = $inv->customer_id;
        $quickPrintFormatId = $document_type->quick_print_format_id;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;

        if ($inv->sale_status == 'returned') { $url_format = "pos/return_view"; }

        $this->data['inv'] = $inv;
        $this->data['show_code'] = 1;
        $this->data['modal'] = FALSE;
        $this->data['biller_logo'] = 2;
        $this->data['tax_indicator'] = 0;
        $this->data['redirect_to_pos'] = FALSE;
        $this->data['product_detail_promo'] = 1;
        $this->data['show_product_preferences'] = 1;
        $this->data['print_directly'] = $print_directly;
        $this->data['pos'] = $this->pos_model->getSetting();
        $this->data['document_type_invoice_format'] = FALSE;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['page_title'] = $this->lang->line("invoice");
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['value_decimals'] = $this->Settings->qty_decimals;
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['seller'] = $this->site->getCompanyByID($inv->seller_id);
        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['rows'] = $this->pos_model->getAllInvoiceItems($document_id);
        $this->data['customer'] = $this->pos_model->getCompanyByID($customer_id);
        $this->data['payments'] = $this->pos_model->getInvoicePayments($document_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($inv->document_type_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($biller_id);
        $this->data['printer'] = $this->pos_model->getPrinterByID($this->pos_settings->printer);
        $this->data['ciiu_code'] = $this->site->get_ciiu_code_by_id($this->Settings->ciiu_code);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
        $this->data['validationDateTime'] = '';
        if (isset($inv->fe_validation_dian) && $inv->fe_validation_dian !== null) {
            $this->data['validationDateTime'] = $inv->fe_validation_dian;
        }
        if ($inv->technology_provider == CADENA) {
            $this->data["technologyProviderLogo"] = "assets/images/cadena_logo.jpeg";
        } else if ($inv->technology_provider == BPM) {
            $this->data["technologyProviderLogo"] = "assets/images/bpm_logo.jpeg";
        } else if ($inv->technology_provider == SIMBA) {
            $this->data["technologyProviderLogo"] = "assets/images/simba_logo.png";
        } else if ($inv->technology_provider == DELCOP) {
            $this->data["technologyProviderLogo"] = "assets/images/delcop_logo.png";
        } else {
            $this->data["technologyProviderLogo"] = "assets/images/simba_logo.png";
        }
        if (!empty($quickPrintFormatId)) {
            $document_type_invoice_format = $this->site->getInvoiceFormatById($document_type->quick_print_format_id);
        } else {
            $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
        }

        if ($document_type_invoice_format) {
            $this->data['document_type_invoice_format'] = $document_type_invoice_format;
            $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
            $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
            $this->data['biller_logo'] = $document_type_invoice_format->logo;
            $this->data['tax_indicator'] = $document_type_invoice_format->tax_indicator;
            $this->data['product_detail_promo'] = $document_type_invoice_format->product_detail_promo;
            $this->data['show_code'] = $document_type_invoice_format->product_show_code;
            $this->data['show_product_preferences'] = $document_type_invoice_format->show_product_preferences;
            $url_format = $document_type_invoice_format->format_url;
            $view_tax = $document_type_invoice_format->view_item_tax ? TRUE : FALSE;
            $tax_inc = $document_type_invoice_format->tax_inc ? TRUE : FALSE;
        }

        $totalItems = 0;
        foreach ($this->data['rows'] as $items) {
            $totalItems += $items->quantity;
        }

        if ($inv->total_items == ceil($totalItems)) {
            $html = $this->load_view($this->theme.$url_format, $this->data, TRUE);
            $this->pos_model->update_print_status($document_id);
        }

        echo $html;
    }

    public function print_server_voucher_delivery()
    {
        $this->sma->checkPermissions('index');
        $sale_id = $this->input->post('document_id');
        $this->load->helper('pos');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['message'] = $this->session->flashdata('message');
        $inv = $this->pos_model->getInvoiceByID($sale_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, TRUE);
        }
        $this->data['rows'] = $this->pos_model->getAllInvoiceItems($sale_id);
        $biller_id = $inv->biller_id;
        $customer_id = $inv->customer_id;
        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($biller_id);

        // Consulta que busca los productos agrupados de acuerdo al parametro configurado en biller_data
        $product_order = $this->data['biller_data']->product_order;
        $this->data['customer'] = $this->pos_model->getCompanyByID($customer_id);
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['payments'] = $this->pos_model->getInvoicePayments($sale_id);

        $this->data['pos'] = $this->pos_model->getSetting();
        $this->data['barcode'] = $this->barcode($inv->reference_no, 'code128', 30);
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
        $this->data['inv'] = $inv;
        $this->data['sid'] = $sale_id;
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['printer'] = $this->pos_model->getPrinterByID($this->pos_settings->printer);
        $this->data['page_title'] = $this->lang->line("invoice");
        $this->data['sale_id'] = $sale_id;
        $this->data['direct_print_job_id'] = NULL;
        $this->data['seller'] = $this->site->getCompanyByID($inv->seller_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['tipo_regimen'] = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['redirect_to_pos'] = /*$redirect_to_pos ? true :*/ FALSE;
        $this->data['ciiu_code'] = $this->site->get_ciiu_code_by_id($this->Settings->ciiu_code);

        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;

        $this->data['print_directly'] = $print_directly;
        $url_format = "pos/voucher_delivery";

        $html = $this->load_view($this->theme.$url_format, $this->data, TRUE);

        echo $html;
    }

    public function validate_user_has_pos_register_open($user_id)
    {
        $register = $this->pos_model->registerData($user_id);
        if ($register) {
            echo json_encode(['open' => true]);
        } else {
            echo json_encode(['open' => false]);
        }
    }

    public function showPreviousRequest($documentId)
    {
        $document = $this->site->getSaleByID($documentId);
        $resolution = $this->site->getDocumentTypeById($document->document_type_id);

        if ($resolution->factura_electronica == YES) {
            $documentElectronicData = (object) [
                'sale_id' => $document->id,
                'biller_id' => $document->biller_id,
                'customer_id' => $document->customer_id,
                'reference' => $document->reference_no
            ];

            $this->Electronic_billing_model->buildRequestFile($documentElectronicData, TRUE);
        }
    }

    public function resendElectronicDocument($documentId)
    {
        $document = $this->site->getSaleByID($documentId);
        $resolutionData = $this->site->getDocumentTypeById($document->document_type_id);

        if ($resolutionData->factura_electronica == 1) {
            $invoiceData = [
                'sale_id' => $document->id,
                'biller_id' => $document->biller_id,
                'customer_id' => $document->customer_id,
                'reference' => $document->reference_no,
                'attachment' => $document->attachment
            ];

            $this->createDocumentElectronic($invoiceData);
        }

        admin_redirect('pos/fe_index');
    }

    private function setUserActivities($documentId)
    {
        $document = $this->site->getSaleByID($documentId);
        if ($this->pos_settings->activate_electronic_pos == YES && $this->pos_settings->mode_electronic_pos == 2) {
            if ($this->sma->formatDecimal($document->total) <  $this->sma->formatDecimal($this->pos_settings->uvt_value)) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 3,
                    'table_name' => 'sales',
                    'record_id' => $document->id,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => "Se crea factura POS electrónica menor a las 5 UVT. Subtotal: {$document->total}",
                ]);
            }
        }
    }

    public function getMessage()
    {
        $documentId = $this->input->post('documentId');
        $document = $this->site->getSaleByID($documentId);

        $response = [
            'title' => $document->fe_mensaje,
            'text' => $document->fe_mensaje_soporte_tecnico
        ];

        echo json_encode($response);
    }

    public function pa_print_server()
    {
        $meta = array('page_title' => lang("pa_print_server"));
        $this->page_construct('pos/pa_print_server', $meta, $this->data);
    }

    public function get_pa_print_server_data()
    {
        $user_id = $this->session->userdata('user_id');
        $data = $this->pos_model->get_pending_pa_orders($user_id);
        $html = "";
        $wdata = false;
        if ($data) {
            foreach ($data as $row) {
                $html .= "<tr class='pa_order_link' data-saleid='".$row->sale_id."'>
                        <td>".$row->date."</td>
                        <td>".$row->reference_no."</td>
                        <td>".$row->customer."</td>
                    </tr>";
                $wdata = true;
            }
        } else {
            $html = "<tr><td colspan='3'>Sin datos</td></tr>";
        }
        echo json_encode(['html'=>$html, 'wdata'=>$wdata]);
    }

    public function pa_print_server_printing($sale_id)
    {
        $meta = array('page_title' => lang("pa_print_server_printing"));
        $user_id = $this->session->userdata('user_id');
        $data = $this->pos_model->get_pending_pa_orders($user_id, $sale_id);
        if (isset($data[0]->suspend_id)) {
            $this->db->delete('suspended_items', ['state_readiness'=>3, 'suspend_id'=>$data[0]->suspend_id]);
            foreach ($data as $row) {
                $this->db->update('suspended_items', ['state_readiness'=>4], ['id'=>$row->id]);
            }
        } else {
            foreach ($data as $row) {
                $this->db->update('sale_items', ['state_readiness'=>4], ['id'=>$row->id]);
            }
        }
        $this->data['data'] = $data;
        $this->load_view($this->theme.'pos/pa_print_server_printing', $this->data);
    }

    public function concession_print($sale_id = NULL, $redirect_to_pos = FALSE, $curr_category = NULL)
    {
        $this->sma->checkPermissions('index');
        if ($this->input->get('id')) {
            $sale_id = $this->input->get('id');
        }
        $this->load->helper('pos');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['message'] = $this->session->flashdata('message');
        $inv = $this->pos_model->getInvoiceByID($sale_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $rows = $this->pos_model->getAllInvoiceItems($sale_id);
        $this->data['rows'] = $rows;
        $biller_id = $inv->biller_id;
        $customer_id = $inv->customer_id;
        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($biller_id);
        $biller_categories_concession = $this->companies_model->get_all_biller_categories_concessions($biller_id);
        // Consulta que busca los productos agrupados de acuerdo al parametro configurado en biller_data
        $product_order = $this->data['biller_data']->product_order;
        $categories = $this->site->getAllCategories();
        $this->data['categories'] = $categories;
        $products_categories = [];
        $concessions_category = [];
        $total_prc = 0;
        if ($curr_category != NULL) {
            foreach ($rows as $product) {
                if ($product->product_category_id != $categories[$curr_category]->id) {
                    continue;
                } else {
                    $total_prc += $product->subtotal;
                }
                $products_categories[$product->product_category_id][] = $product;
            }
            foreach ($biller_categories_concession as $biller_category_concession) {
                $concessions_category[$biller_category_concession->category_id] = $biller_category_concession->concession_name;
            }
            $this->data['concession_category'] = isset($concessions_category[$categories[$curr_category]->id]) ? $concessions_category[$categories[$curr_category]->id] : ' - ';
            $this->data['total_prc'] = $total_prc;
        }
        $this->data['products_categories'] = $products_categories;
        $this->data['curr_category'] = $curr_category;
        $this->data['invoice_products_returned'] = $inv->return_id ? $this->pos_model->get_invoice_products_by_order_in_biller_data($inv->return_id, $product_order) : NULL;

        $this->data['customer'] = $this->pos_model->getCompanyByID($customer_id);
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['payments'] = $this->pos_model->getInvoicePayments($sale_id);

        $this->data['pos'] = $this->pos_model->getSetting();
        $this->data['barcode'] = $this->barcode($inv->reference_no, 'code128', 30);
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
        $this->data['inv'] = $inv;
        $this->data['sid'] = $sale_id;
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['printer'] = $this->pos_model->getPrinterByID($this->pos_settings->printer);
        $this->data['page_title'] = $this->lang->line("invoice");
        $this->data['sale_id'] = $sale_id;
        $this->data['direct_print_job_id'] = NULL;
        $this->data['seller'] = $this->site->getCompanyByID($inv->seller_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['tipo_regimen'] = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['redirect_to_pos'] = $redirect_to_pos ? true : false;
        $this->data['ciiu_code'] = $this->site->get_ciiu_code_by_id($this->Settings->ciiu_code);
        $this->data['curr_category'] = $curr_category;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;
        $this->data['print_directly'] = $print_directly;
        if ($curr_category == NULL) {
            $curr_category = reset($categories);
            $primer_id = key($categories);
            admin_redirect('pos/concession_print/'.$sale_id.'/FALSE/'.$primer_id);
        } else if ($curr_category != NULL && (!isset($products_categories[$categories[$curr_category]->id]) || !isset($concessions_category[$categories[$curr_category]->id]))) {
            if ($categories[$curr_category] == end($categories)) {
                admin_redirect('pos');
            } else {
                $curr_category++;
                admin_redirect('pos/concession_print/'.$sale_id.'/FALSE/'.$curr_category);
            }
        } else {
            $this->load_view($this->theme . "pos/concession_print", $this->data);
        }

    }

    public function change_status_accepted_DIAN($document_id)
    {
        $document = $this->site->getSaleByID($document_id);
        $resolution = $this->site->getDocumentTypeById($document->document_type_id);

        if (in_array($resolution->module, [FACTURA_DETAL, FACTURA_POS])) {
            $resolutionCode = '01';
        } else if (in_array($resolution->module, [NOTA_CREDITO_DETAL, NOTA_CREDITO_POS])) {
            $resolutionCode = '91';
        } else if (in_array($resolution->module, [NOTA_DEBITO_DETAL, NOTA_DEBITO_POS])) {
            $resolutionCode = '92';
        }

        $prefix = strstr($document->reference_no, '-', TRUE);
        $reference = str_replace('-', '', $document->reference_no);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/documentos?nit_emisor=' . $this->Settings->numero_documento . '&id_documento=' . $reference . '&codigo_tipo_documento=' . $resolutionCode . '&prefijo=' . $prefix,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                'Content-Type: application/json',
                'Partnership-Id: 901090070'
            ],
        ));
        $response = curl_exec($curl);
        log_message('debug', 'CADENA FE-Consulta documento: '. $response);
        curl_close($curl);

        $response = json_decode($response);


        if (!empty($response)) {
            if (isset($response->statusCode)) {
                if ($response->statusCode == 200) {
                    $updated_sale = $this->site->updateSale([
                        'fe_aceptado' => 2,
                        'fe_mensaje' => 'Documento aceptado por la DIAN',
                        'fe_mensaje_soporte_tecnico' => 'La Factura electrónica ' . $reference . ', ha sido autorizada. Código es estado ' . $response->statusCode,
                        'fe_xml' => $response->document
                    ], $document_id);

                    if ($updated_sale == TRUE) {
                        $customer = $this->sales_model->get_email_customer($document->customer_id);
                        $email_send = $this->send_electronic_invoice_email($document_id, $customer->email, TRUE);

                        $this->session->set_flashdata('message', 'El cambio de estado del Documento fue realizado correctamente. ' . $email_send);
                        admin_redirect('pos/fe_index');
                    } else {
                        $this->session->set_flashdata('error', 'No fue posible cambiar el estado del Documento.');
                        admin_redirect('pos/fe_index');
                    }
                } else if ($response->statusCode == 400) {
                    $this->session->set_flashdata('error', 'No fue posible cambiar el estado del Documento. El prefijo no coincide con el prefijo del id del documento.');
                    admin_redirect('pos/fe_index');
                } else if ($response->statusCode == 404) {
                    $this->session->set_flashdata('error', 'No fue posible cambiar el estado del Documento. Registro no encontrado con los parámetros de búsqueda enviados.');
                    admin_redirect('pos/fe_index');
                }
            } else {
                $this->session->set_flashdata('error', 'Se ha perdido la conexión con el Web Services.');
                admin_redirect('pos/fe_index');
            }
        } else {
            $this->session->set_flashdata('error', 'No es posible realizar conexión con Web Services.');
            admin_redirect('pos/fe_index');
        }
    }

    public function checkQuotaKiowa()
    {
        $customerId = $this->input->post('customerId');
        $kaiowaReference = $this->input->post('kaiowaReference');
        $aSchemacia = $this->Settings->a_schemacia;
        $aIdMarca = $this->Settings->a_id_marca;
        $aLogin = $this->Settings->a_login;
        $aPassword = $this->Settings->a_password;
        $customer = $this->site->getCompanyByID($customerId);
        $vatNo = $customer->vat_no;

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://kaiowa.app/APIS/API_REST_SERVICE_KAIOWA/services/transactions/get_saldo_disponible",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"a_schemacia\":\"$aSchemacia\", \"a_id_marca\":\"$aIdMarca\", \"a_login\":\"$aLogin\", \"a_password\": \"$aPassword\", \"a_documento\":\"$vatNo\"}",
            CURLOPT_HTTPHEADER => [
                "Accept: */*",
                "Content-Type: application/json",
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        echo $this->manageResponseCheckQuoteKaiowa($response, $err);
    }


    private function manageResponseCheckQuoteKaiowa($response, $err)
    {
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response);

            if ($response->err_code == 0) {
                if (isset($response->data)) {
                    return json_encode([
                        'status' => false
                    ]);
                }

                $person = $response->datos->persona[0];
                $emails = $response->datos->emails[0]->row_to_json;
                $phones = $response->datos->celulares[0]->row_to_json;
                $name = $person->nombre1. ' '.$person->nombre2. ' '.$person->apellido1. ' '.$person->apellido2;
                $availableQuota = number_format($response->valor_disponible_2,2,',','.');
                $maximumQuota = number_format($response->cupo,2,',','.');

                $modal = "<div class='modal-dialog'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <h2 id='myModalLabel'>Kiowa <small>Consulta Saldo Disponible</small></h2>
                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>
                                <i class='fa fa-lg'>&times;</i>
                            </button>
                        </div>
                        <div class='modal-body'>
                            <div class='row'>
                                <div class='col-sm-6 text-right'>
                                    <strong>Nombre: </strong>
                                </div>
                                <div class='col-sm-6'>
                                    {$name}
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-sm-6 text-right'>
                                    <strong>Email: </strong>
                                </div>
                                <div class='col-sm-6'>
                                    {$emails->dato_contacto}
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-sm-6 text-right'>
                                    <strong>Telefono: </strong>
                                </div>
                                <div class='col-sm-6'>
                                    {$phones->dato_contacto}
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-sm-6 text-right'>
                                    <strong>Cupo Máximo: </strong>
                                </div>
                                <div class='col-sm-6'>
                                    $ {$maximumQuota}
                                </div>
                            </div>
                            <hr>
                            <div class='row'>
                                <div class='col-sm-3'>
                                </div>
                                <div class='col-sm-6 text-center'>
                                    <h3>Cupo Disponible: <br><br> <span class='text-success'>$ {$availableQuota}</span></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";

                return json_encode([
                    'status' => true,
                    'message' => '',
                    'data' => str_replace('\r\n', '', $modal)
                ]);
            } else {
                return json_encode([
                    'status' => false,
                    'message' => $response->msg
                ]);
            }
        }
    }

    public function createCreditFinancing($saleId = 1, $amountCredit = null)
    {
        $this->load->admin_model("Financing_model");

        $billerId = $this->input->post('biller');
        $customerId = $this->input->post('customer');
        $documentType = $this->DocumentsTypes_model->getDocumentTypeBiller(["biller_id"=>$billerId, "module"=>58]);
        $reference = $this->DocumentsTypes_model->getReference($documentType->id);

        $data = [
            "document_type_id"  => $documentType->id,
            "biller_id"         => $this->input->post('biller'),
            "credit_no"         => $reference->referenceNo,
            "sale_id"           => $saleId,
            "customer_id"       => $customerId,
            "credit_date"       => date("Y-m-d"),
            "capital_amount"    => $amountCredit,
        ];
        // if ($creditFiancingId = $this->Financing_model->create($data)) {
            $this->createCredintFinancingInstallments($creditFiancingId = 1000, $amountCredit, $customerId);
        // }

        // $this->sma->print_arrays($data);
    }

    public function createCredintFinancingInstallments($creditFiancingId, $amountCredit, $customerId)
    {
        $frequency  = $this->input->post("frequency");
        $installment= $this->input->post("installment");

        for ($i=0; $i < $installment; $i++) {
            $data[] = [
                "financing_credit_id" => $creditFiancingId,
                "customer_id"         => $customerId,
                "installment_no"      => ($i + 1),
                "installment_due_date"=> date("Y-m-d"),
                "installment_amount"  => $amountCredit / $installment,
                "capital_amount"      => $amountCredit / $installment,
            ];
        }

        $this->sma->print_arrays($data);
    }


    public function documentStatusDELCOP($document_id, $multiple = false)
    {
        $sale = $this->site->getSaleByID($document_id);
        $response_token = $this->Electronic_billing_model->delcopGetAuthorizationoken();

        if ($response_token->success == FALSE) {
            $response = [
                'response' => FALSE,
                'message' => "Los campos de Usuario o Contraseña DELCOP no son correctos. " . $response_token->error
            ];

            return (object) $response;
        }

        if ($this->Settings->fe_work_environment == TEST) {
            $url = "https://www-prueba.titanio.com.co/PDE/public/api/PDE/detalle";
        } else {
            $url = "https://www.titanio.com.co/PDE/public/api/PDE/detalle";
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '{"token": "' . $response_token->token . '", "transaccion_id": "' . trim($sale->fe_id_transaccion) . '"}',
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        log_message('debug', 'DELCOP consulta de estado'. $response);

        $response = json_decode($response);

        curl_close($curl);

        if ($response->error_id == 0) {
            if ($response->transaccion_id != 0) {
                $document_status = $response->detalleTransaccion->estado_id;
                $acepted_DIAN = strstr($document_status, 'Validado por la DIAN');
                $rejected_DIAN = strstr($document_status, 'Rechazado por la DIAN');
                $sent_DIAN = strstr($document_status, 'Enviado a la DIAN');
                $validated_DELCOP = strstr($document_status, 'Validado por la DIAN');
                $transaction_received = strstr($document_status, 'Transacción recibida,');

                if ($rejected_DIAN != "") {
                    $this->site->updateSale(["fe_aceptado" => 1, "fe_mensaje" => "Transacción con errores detectados en la DIAN.", "fe_mensaje_soporte_tecnico" => $response->error], $sale->id);
                    $this->session->set_flashdata('error', $document_status);
                } else if ($acepted_DIAN != "") {
                    $this->site->updateSale(["fe_aceptado" => 2, "fe_mensaje" => "Documento aceptado por la DIAN", "fe_mensaje_soporte_tecnico" => ""], $sale->id);
                    $this->session->set_flashdata('message', $document_status);
                } else if ($sent_DIAN != "") {
                    $this->site->updateSale(["fe_aceptado" => 3, "fe_mensaje" => "Documento aceptado y enviado a la DIAN", "fe_mensaje_soporte_tecnico" => ""], $sale->id);
                    $this->session->set_flashdata('warning', $document_status);
                } else if ($validated_DELCOP != "") {
                    $this->site->updateSale(["fe_aceptado" => 3, "fe_mensaje" => "Documento aceptado y validado por DELCOP", "fe_mensaje_soporte_tecnico" => ""], $sale->id);
                    $this->session->set_flashdata('warning', $document_status);
                } else if ($transaction_received != "") {
                    if ($response->error != "") {
                        $this->site->updateSale(["fe_aceptado" => 1, "fe_mensaje" => "Transacción recibida por DELCOP con posibles errores.", "fe_mensaje_soporte_tecnico" => $response->error], $sale->id);
                        $this->session->set_flashdata('warning', $document_status);
                    } else {
                        $this->site->updateSale(["fe_aceptado" => 3, "fe_mensaje" => "Transacción recibida por DELCOP.", "fe_mensaje_soporte_tecnico" => "Proceso de validación DELCOP pendiente"], $sale->id);
                        $this->session->set_flashdata('error', $document_status);
                    }
                }

                if ($multiple == false) {
                    admin_redirect('pos/fe_index');
                } else {
                    return true;
                }
            } else {
                $this->site->updateSale(["fe_aceptado" => 1, "fe_mensaje" => "No existe registro del Documento en DELCOP"], $sale->id);

                $this->session->set_flashdata('error', "No existe registro del Documento en DELCOP");
                if ($multiple == false) {
                    admin_redirect('pos/fe_index');
                } else {
                    return false;
                }
            }
        } else {
            $this->site->updateSale(["fe_mensaje" => "Error al consumir: Estado del documentos en DELCOP"], $sale->id);

            $this->session->set_flashdata('error', $response->error_msg);
            if ($multiple == false) {
                admin_redirect('pos/fe_index');
            } else {
                return false;
            }
        }
    }

    public function changeStatesDelcopAjax()
    {
        $documents = $this->input->post("documents");
        $message = "";

        if (!empty($documents)) {
            foreach ($documents as $document) {
                if ($this->documentStatusDELCOP($document, true) == false) {
                    $sale = $this->site->getSaleByID($document_id);
                    $message += "Error al consultar el documento $sale->reference_no";
                }
            }
        }

        echo json_encode([
            "status" => (empty($message)),
            "message" => $message
        ]);
    }

    public function changeStatesSimbaAjax()
    {
        $documents = $this->input->post("documents");
        $message = "";

        if (!empty($documents)) {
            foreach ($documents as $document) {
                $sale = $this->site->getSaleByID($document);
                if ($sale->technology_provider != SIMBA) {
                    $message .= "El documento $sale->reference_no no es del proveedor tecnologico SIMBA. \n";
                }
                else if ($sale->fe_aceptado != ACCEPTED) {
                    $res = $this->Electronic_billing_model->getStatusSimba($sale);
                    if ($res['error']) {
                        $message .= "El documento $sale->reference_no, " . $res['message'] . "\n";
                    }  
                }else{
                    $message .= "El documento $sale->reference_no ya fue aceptado por la DIAN" . "\n";
                }
            }
        }

        echo json_encode([
            "status" => (empty($message)),
            "message" => $message
        ]);
    }

    public function curren_price_gram(){
        $pos_settings = $this->db->get_where('pos_settings', array('pos_id' => 1));
        if ($pos_settings->num_rows() > 0) {
            $respuesta = $pos_settings->row();
            if ($this->input->get('valuedOnGram') == '1') {
                $current_gold_price = $respuesta->current_gold_price;
                $dataJSON = array( 'current_price' => $current_gold_price );
            }else if($this->input->get('valuedOnGram') == '2'){
                $current_gold_price = $respuesta->current_italian_gold_price;
                $dataJSON = array( 'current_price_italian' => $current_gold_price );
            }
        } else {
            $current_gold_price = 0;
            $dataJSON = array( 'current_price' => $current_gold_price );
        }
        echo json_encode($dataJSON);
    }

    public function restobar_table_change($suspendOrderId)
    {
        $order = $this->Restobar_model->getSuspendedeBill($suspendOrderId);
        $tableToChange = $this->Restobar_model->get_table_by_id($order->table_id);

        $tables = $this->Restobar_model->getAvailableTables([
            "estado"=> 1,
            "tipo"  => "M",
            "area_id"  => $tableToChange->area_id
        ]);

        $this->data['tableToChange'] = $tableToChange;
        $this->data['tables'] = $tables;
        $this->data['order'] = $order;

        $this->load_view($this->theme.'pos/restobar_table_change', $this->data);
    }

    public function restobar_update_table()
    {
        $tableToChange = $this->input->post("tableToChange");
        $table = $this->input->post("table");
        $order = $this->input->post("order");

        $updatedTable = $this->Restobar_model->update_suspended_bills(["table_id" => $table], $order);
        if ($updatedTable) {
            $this->Restobar_model->update_table(["estado" => 1], $tableToChange);
            $this->Restobar_model->update_table(["estado" => 3], $table);
        }

        admin_redirect("pos/index/$order");
    }

    public function get_price_minimun($id){
        $data = $this->pos_model->get_price_minimun($id);
        if ($data) {
            $res = [
                "product_id" => $data->product_id,
                "price"      => $data->price,
            ];
        }else{
            $res = [
                "product_id" => false,
                "price"      => false,
            ];
        }
        echo json_encode($res);
    }
   
    public function markPrintedAjax() {
        $documentId = $this->input->post('documentId');
        $documentsUpdate = $this->pos_model->update_print_status($documentId);

        echo json_encode($documentsUpdate);
    }

    public function downloadPdf($sale_id){
        $this->saleView($sale_id, false);
    }
}