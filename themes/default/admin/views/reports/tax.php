<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";
if ($this->input->post('biller')) {
    $v .= "&biller=" . $this->input->post('biller');
}
if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}
if ($this->input->post('taxRate')) {
    $v .= "&taxRate=" . $this->input->post('taxRate');
    $varTaxRate = $this->input->post('taxRate');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
if ($this->input->post('sale_document_type')) {
    $v .= "&sale_document_type=" . $this->input->post('sale_document_type');
}
if ($this->input->post('purchase_document_type')) {
    $v .= "&purchase_document_type=" . $this->input->post('purchase_document_type');
}
if ($this->input->post('customer')) {
    $v .= "&customer=" . $this->input->post('customer');
}
if ($this->input->post('supplier')) {
    $v .= "&supplier=" . $this->input->post('supplier');
}
?>

<style type="text/css">
    .widget {
        min-height: 100px;
    }
    .widget h2, .widget h3 {
        font-size: 130%;
        padding-bottom: 8%;
    }
</style>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo admin_form_open("reports/tax", 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
                                <div class="row">
                                    <?php
                                $bl[""] = "";
                                $bldata = [];
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    $bldata[$biller->id] = $biller;
                                }
                                ?>

                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("biller", "biller"); ?>
                                            <select name="biller" class="form-control" id="biller">
                                                <option value=""><?= lang('select') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" <?= (isset($_POST['biller']) && $_POST['biller'] == $biller->id ? "selected" : "") ?> ><?= $biller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-6" >
                                        <div class="form-group">
                                            <?= lang("biller", "biller"); ?>
                                            <select name="biller" class="form-control" id="biller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                ?>
                                                    <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                    <!-- <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                            <?php
                                            $wh[""] = lang('select').' '.lang('warehouse');
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                            ?>
                                        </div>
                                    </div> -->

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label" for="taxRate"><?= lang("taxRate"); ?></label>
                                            <?php
                                            $txr[""] = lang('select').' '.lang('taxRate');
                                            foreach ($taxRates as $taxRate) {
                                                $txr[$taxRate->id] = $taxRate->name;
                                            }
                                            echo form_dropdown('taxRate', $txr, (isset($_POST['taxRate']) ? $_POST['taxRate'] : ""), 'class="form-control" id="taxRate" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("taxRate") . '"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("sale_document_type", "sale_document_type"); ?>
                                            <select name="sale_document_type" class="form-control" id="sale_document_type">
                                                <option value=""><?= lang('select') ?></option>
                                                <?php if ($sale_document_types): ?>
                                                    <?php foreach ($sale_document_types as $sdt): ?>
                                                        <option value="<?= $sdt->id ?>" <?= isset($_POST['sale_document_type']) && $_POST['sale_document_type'] == $sdt->id ? 'selected="selected"' : '' ?>><?= $sdt->sales_prefix ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("purchase_document_type", "purchase_document_type"); ?>
                                            <select name="purchase_document_type" class="form-control" id="purchase_document_type">
                                                <option value=""><?= lang('select') ?></option>
                                                <?php if ($purchase_document_types): ?>
                                                    <?php foreach ($purchase_document_types as $sdt): ?>
                                                        <option value="<?= $sdt->id ?>" <?= isset($_POST['purchase_document_type']) && $_POST['purchase_document_type'] == $sdt->id ? 'selected="selected"' : '' ?>><?= $sdt->sales_prefix ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                            <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'class="form-control" id="customer" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("customer") . '"'); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= lang("supplier", "supplier"); ?>
                                            <?php echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ""), 'class="form-control" id="supplier" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("supplier") . '"'); ?>
                                        </div>
                                    </div>

                                    <hr class="col-sm-11">
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('date_records_filter', 'date_records_filter_h') ?>
                                        <select name="date_records_filter" id="date_records_filter_h" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>

                                    <div class="date_controls_h">
                                        <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                            <div class="col-sm-4 form-group">
                                                <?= lang('filter_year', 'filter_year_h') ?>
                                                <select name="filter_year" id="filter_year_h" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <?= lang('start_date', 'start_date') ?>
                                            <input type="date" name="start_date" id="start_date_h" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                        </div>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <?= lang('end_date', 'end_date') ?>
                                            <input type="date" name="end_date" id="end_date_h" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls" style="padding-top: 3%">
                                        <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_filter"'); ?>
                                        <a href="<?= admin_url('reports/tax') ?>" class="btn btn-danger"><?= lang('clean') ?></a>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1"><?= lang('sales') ?></a></li>
                    <li class=""><a data-toggle="tab" href="#tab-2"><?= lang('purchases') ?></a></li>
                </ul>
                <div class="tab-content">

                    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <h2><?= lang('sales_tax_report'); ?> <?php if ($this->input->post('start_date')) { echo "desde " . $this->input->post('start_date') . " hasta " . $this->input->post('end_date'); } ?></h2>


                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pull-right dropdown">
                                        <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                        <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                            <li class="dropdown">
                                                <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                                    <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?>
                                                </a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" id="xls_2" class="tip" title="<?= lang('download_xls_summary') ?>">
                                                    <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls_summary') ?>
                                                </a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                                    <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <hr>
                                    <?php if ($Settings->indian_gst) { ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php } ?>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <div class="widget style1 navy-bg">
                                                            <h3 class="font-bold text-right text_subtotal"></h3>
                                                            <span> <?= lang('sub_total') ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <div class="widget style1 navy-bg">
                                                            <h3 class="font-bold text-right text_product_tax"></h3>
                                                            <span> <?= lang('total_product_tax') ?> </span>
                                                        </div>
                                                    </div>
                                                    <?php if (!isset($_POST['taxRate']) || $_POST['taxRate'] == ''): ?>
                                                        <div class="col-sm-2">
                                                            <div class="widget style1 navy-bg">
                                                                <h3 class="font-bold text-right text_order_tax"></h3>
                                                                <span> <?= lang('total_order_tax') ?> </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <div class="widget style1 navy-bg">
                                                                <h3 class="font-bold text-right text_shipping"></h3>
                                                                <span> <?= lang('shipping') ?> </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <div class="widget style1 navy-bg">
                                                                <h3 class="font-bold text-right  text_order_discount"></h3>
                                                                <span> <?= lang('total_order_discount') ?> </span>
                                                            </div>
                                                        </div>
                                                    <?php endif ?>

                                                    <div class="col-sm-2">
                                                        <div class="widget style1 navy-bg">
                                                            <h3 class="font-bold text-right  text_total_sales"></h3>
                                                            <span> <?= lang('sales_amount') ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th><?= lang('tax_name') ?></th>
                                                                    <th class="text-right"><?= lang('net_price') ?></th>
                                                                    <th class="text-right"><?= lang('tax') ?></th>
                                                                    <th class="text-right"><?= lang('total') ?></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="tbody_sale_tax">

                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                                <?php if ($Settings->indian_gst) { ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="small-box col-sm-4 padding1010 bdarkGreen">
                                                    <h3><?= isset($sale_tax->igst) ? $this->sma->formatMoney($sale_tax->igst) : '0.00' ?></h3>
                                                    <p><?= lang('igst') ?></p>
                                                </div>
                                                <div class="small-box col-sm-4 padding1010 bpurple">
                                                    <h3><?= isset($sale_tax->cgst) ? $this->sma->formatMoney($sale_tax->cgst) : '0.00' ?></h3>
                                                    <p><?= lang('cgst') ?></p>
                                                </div>
                                                <div class="small-box col-sm-4 padding1010 borange">
                                                    <h3><?= isset($sale_tax->sgst) ? $this->sma->formatMoney($sale_tax->sgst) : '0.00' ?></h3>
                                                    <p><?= lang('sgst') ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="table-responsive table1">
                                        <table id="TAXData"
                                            class="table table-bordered table-hover table-condensed reports-table">
                                            <thead>
                                            <tr>
                                                <th style="width:15.9%;"><?= lang("date"); ?></th>
                                                <th style="width:3%;"><?= lang("reference_no"); ?></th>
                                                <th style="width:3%;"><?= lang("affects_to"); ?></th>
                                                <th style="width:6%;"><?= lang("status"); ?></th>
                                                <th style="width:6%;"><?= lang("payment_method"); ?></th>
                                                <th style="width:8.3%;"><?= lang("warehouse"); ?></th>
                                                <th style="width:8.3%;"><?= lang("biller"); ?></th>
                                                <th style="width:8.3%;"><?= lang("customer"); ?></th>
                                                <th style="width:8.3%;"><?= lang("sub_total"); ?></th>
                                                <th style="width:8.3%;"><?= lang("product_tax"); ?></th>
                                                <th style="width:8.3%;"><?= lang("order_discount"); ?></th>
                                                <th ><?= lang("rete_fuente"); ?></th>
                                                <th ><?= lang("rete_iva"); ?></th>
                                                <th ><?= lang("rete_ica"); ?></th>
                                                <th ><?= lang("rete_other"); ?></th>
                                                <th style="width:8.3%;"><?= lang("grand_total"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                            </tbody>
                                            <tfoot class="dtFilter">
                                            <tr class="active">
                                                <th><?= lang("date"); ?></th>
                                                <th><?= lang("reference_no"); ?></th>
                                                <th><?= lang("affects_to"); ?></th>
                                                <th><?= lang("status"); ?></th>
                                                <th><?= lang("payment_method"); ?></th>
                                                <th><?= lang("warehouse"); ?></th>
                                                <th><?= lang("biller"); ?></th>
                                                <th><?= lang("customer_name"); ?></th>
                                                <th><?= lang("sub_total"); ?></th>
                                                <th><?= lang("product_tax"); ?></th>
                                                <th><?= lang("order_discount"); ?></th>
                                                <th ><?= lang("rete_fuente"); ?></th>
                                                <th ><?= lang("rete_iva"); ?></th>
                                                <th ><?= lang("rete_ica"); ?></th>
                                                <th ><?= lang("rete_other"); ?></th>
                                                <th><?= lang("grand_total"); ?></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            <h2><?= lang('purchases_tax_report'); ?> <?php if ($this->input->post('start_date')) { echo "desde " . $this->input->post('start_date') . " hasta " . $this->input->post('end_date'); } ?></h2>
                            <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pull-right dropdown">
                                        <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                        <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                            <li>
                                                <a href="#" id="xls1" class="tip" title="<?= lang('download_xls') ?>">
                                                    <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" id="xls1_2" class="tip" title="<?= lang('download_xls_summary') ?>">
                                                    <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls_summary') ?>
                                                </a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" id="image1" class="tip" title="<?= lang('save_image') ?>">
                                                    <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <hr>
                                    <?php if ($Settings->indian_gst) { ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php } ?>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <div class="widget style1 navy-bg">
                                                            <h3 class="font-bold text-right text_purchase_subtotal"></h3>
                                                            <span> <?= lang('sub_total') ?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <div class="widget style1 navy-bg">
                                                            <h3 class="font-bold text-right text_purchase_product_tax"></h3>
                                                            <span> <?= lang('total_product_tax') ?> </span>
                                                        </div>
                                                    </div>
                                                    <?php if (!isset($_POST['taxRate']) || $_POST['taxRate'] == ''): ?>
                                                        <div class="col-sm-2">
                                                            <div class="widget style1 navy-bg">
                                                                <h3 class="font-bold text-right text_purchase_order_tax"></h3>
                                                                <span> <?= lang('total_order_tax') ?> </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <div class="widget style1 navy-bg">
                                                                <h3 class="font-bold text-right text_purchase_shipping"></h3>
                                                                <span> <?= lang('shipping') ?> </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <div class="widget style1 navy-bg">
                                                                <h3 class="font-bold text-right  text_purchase_order_discount"></h3>
                                                                <span> <?= lang('total_order_discount') ?> </span>
                                                            </div>
                                                        </div>
                                                    <?php endif ?>

                                                    <div class="col-sm-2">
                                                        <div class="widget style1 navy-bg">
                                                            <h3 class="font-bold text-right  text_purchase_total_sales"></h3>
                                                            <span> <?= lang('purchases_amount') ?> </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th><?= lang('tax_name') ?></th>
                                                                    <th class="text-right"><?= lang('net_price') ?></th>
                                                                    <th class="text-right"><?= lang('tax') ?></th>
                                                                    <th class="text-right"><?= lang('total') ?></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="tbody_purchase_tax">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <?php if ($Settings->indian_gst) { ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="small-box col-sm-4 padding1010 bdarkGreen">
                                                    <h3><?= isset($purchase_tax->igst) ? $this->sma->formatMoney($purchase_tax->igst) : '0.00' ?></h3>
                                                    <p><?= lang('igst') ?></p>
                                                </div>
                                                <div class="small-box col-sm-4 padding1010 bpurple">
                                                    <h3><?= isset($purchase_tax->cgst) ? $this->sma->formatMoney($purchase_tax->cgst) : '0.00' ?></h3>
                                                    <p><?= lang('cgst') ?></p>
                                                </div>
                                                <div class="small-box col-sm-4 padding1010 borange">
                                                    <h3><?= isset($purchase_tax->sgst) ? $this->sma->formatMoney($purchase_tax->sgst) : '0.00' ?></h3>
                                                    <p><?= lang('sgst') ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="table-responsive table2">
                                        <table id="PTAXData"
                                            class="table table-bordered table-hover table-condensed reports-table">
                                            <thead>
                                            <tr>
                                                <th style="min-width:150px !important;"><?= lang("date"); ?></th>
                                                <th style="min-width:120px !important;"><?= lang("reference_no"); ?></th>
                                                <th style="min-width:120px !important;"><?= lang("affects_to"); ?></th>
                                                <th style="min-width:80px !important;"><?= lang("status"); ?></th>
                                                <th style="min-width:150px !important;"><?= lang("warehouse"); ?></th>
                                                <th style="min-width:150px !important;"><?= lang("supplier"); ?></th>
                                                <th style="min-width:100px !important;">Subtotal</th>
                                                <th style="min-width:80px !important;"><?= lang("product_tax"); ?></th>
                                                <th ><?= lang("order_discount"); ?></th>
                                                <th ><?= lang("rete_fuente"); ?></th>
                                                <th ><?= lang("rete_iva"); ?></th>
                                                <th ><?= lang("rete_ica"); ?></th>
                                                <th ><?= lang("rete_other"); ?></th>
                                                <th style="min-width:120px !important;"><?= lang("grand_total"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                            </tbody>
                                            <tfoot class="dtFilter">
                                            <tr>
                                                <th ><?= lang("date"); ?></th>
                                                <th ><?= lang("reference_no"); ?></th>
                                                <th ><?= lang("affects_to"); ?></th>
                                                <th ><?= lang("status"); ?></th>
                                                <th ><?= lang("warehouse"); ?></th>
                                                <th ><?= lang("supplier"); ?></th>
                                                <th >Subtotal</th>
                                                <th ><?= lang("product_tax"); ?></th>
                                                <th ><?= lang("order_discount"); ?></th>
                                                <th ><?= lang("rete_fuente"); ?></th>
                                                <th ><?= lang("rete_iva"); ?></th>
                                                <th ><?= lang("rete_ica"); ?></th>
                                                <th ><?= lang("rete_other"); ?></th>
                                                <th ><?= lang("grand_total"); ?></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Scripts Pestaña 1 -->
<script>
$(document).ready(function () {
    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
    $('#modal-loading').fadeIn();
    oTable = $('#TAXData').dataTable({
        "aaSorting": [[0, "desc"]],
        "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
        "iDisplayLength": <?= $Settings->rows_per_page ?>,
        'bProcessing': true, 'bServerSide': true,
        'sAjaxSource': '<?= admin_url('reports/get_sale_taxes/?v=1' . $v) ?>',
        'fnServerData': function (sSource, aoData, fnCallback) {
            aoData.push({
                "name": "<?= $this->security->get_csrf_token_name() ?>",
                "value": "<?= $this->security->get_csrf_hash() ?>"
            });
            $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
        },
        'fnRowCallback': function (nRow, aData, iDisplayIndex) {
            nRow.id = aData[16];
            nRow.className = (aData[3] == 'completed') ? "invoice_link2" : "invoice_link2 warning";
            return nRow;
        },
        "aoColumns": [
            {"mRender": fsd},
            null,
            null,
            {"mRender": row_status},
            {"mRender": paid_by},
            null,
            null,
            null,
            {"mRender": currencyFormat},
            {"mRender": currencyFormat},
            {"mRender": currencyFormat},
            {"mRender": currencyFormat},
            {"mRender": currencyFormat},
            {"mRender": currencyFormat},
            {"mRender": currencyFormat},
            {"mRender": currencyFormat},
        ],

        "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
            var t1 = 0;
            var t2 = 0;
            var t3 = 0;
            var t4 = 0;
            var t5 = 0;
            var t6 = 0;
            var t7 = 0;
            var t8 = 0;
            for (var i = 0; i < aaData.length; i++) {
                t1 += aaData[aiDisplay[i]][8] != null ? parseFloat(aaData[aiDisplay[i]][8]) : 0;
                t2 += aaData[aiDisplay[i]][9] != null ? parseFloat(aaData[aiDisplay[i]][9]) : 0;
                t3 += aaData[aiDisplay[i]][10] != null ? parseFloat(aaData[aiDisplay[i]][10]) : 0;
                t4 += aaData[aiDisplay[i]][11] != null ? parseFloat(aaData[aiDisplay[i]][11]) : 0;
                t5 += aaData[aiDisplay[i]][12] != null ? parseFloat(aaData[aiDisplay[i]][12]) : 0;
                t6 += aaData[aiDisplay[i]][13] != null ? parseFloat(aaData[aiDisplay[i]][13]) : 0;
                t7 += aaData[aiDisplay[i]][14] != null ? parseFloat(aaData[aiDisplay[i]][14]) : 0;
                t8 += aaData[aiDisplay[i]][15] != null ? parseFloat(aaData[aiDisplay[i]][15]) : 0;
            }
            var nCells = nRow.getElementsByTagName('th');
            nCells[8].innerHTML = currencyFormat(parseFloat(t1));
            nCells[9].innerHTML = currencyFormat(parseFloat(t2));
            nCells[10].innerHTML = currencyFormat(parseFloat(t3));
            nCells[11].innerHTML = currencyFormat(parseFloat(t4));
            nCells[12].innerHTML = currencyFormat(parseFloat(t5));
            nCells[13].innerHTML = currencyFormat(parseFloat(t6));
            nCells[14].innerHTML = currencyFormat(parseFloat(t7));
            nCells[15].innerHTML = currencyFormat(parseFloat(t8));
        },
        "initComplete": function(settings, json) {
            $('#modal-loading').fadeOut();
          }
    }).fnSetFilteringDelay().dtFilter([
        {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
        {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
        {column_number: 2, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
        {column_number: 3, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
        {column_number: 4, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
    ], "footer");
    <?php endif ?>

    setTimeout(function() {
        <?php if ($this->input->post('customer')) { ?>
            $('#customer').val(<?= $this->input->post('customer') ?>).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url + "customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: '<?= lang('no_match_found') ?>'}]};
                        }
                    }
                }
            });

            $('#customer').val(<?= $this->input->post('customer') ?>);
        <?php } ?>

        <?php if ($this->input->post('supplier')) { ?>
            $('#supplier').val(<?= $this->input->post('supplier') ?>).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: '<?= lang('no_match_found') ?>'}]};
                        }
                    }
                }
            });
            $('#supplier').val(<?= $this->input->post('supplier') ?>);
        <?php } ?>
    }, 850);
    $.ajax({
        url : '<?= admin_url('reports/get_sale_taxes/?v=1&totales=1' . $v) ?>',
        dataType : "JSON"
    }).done(function(data){
        $('.text_subtotal').text(formatMoney(data.subtotal));
        $('.text_product_tax').text(formatMoney(data.product_tax));
        $('.text_order_tax').text(formatMoney(data.order_tax));
        $('.text_shipping').text(formatMoney(data.shipping));
        $('.text_order_discount').text(formatMoney(data.total_discount));
        $('.text_total_sales').text(formatMoney(data.total_sales));
        $('.tbody_sale_tax').html(data.table_html);
        <?php if ($this->Owner): ?>
           taxes_table_base = $('.text_sales_taxes_base').text();
           text_subtotal = $('.text_subtotal').text();
           if (parseFloat(formatDecimal(taxes_table_base)) != parseFloat(formatDecimal(text_subtotal))) {
            $("<i class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='top' title='"+"<?= lang('report_tax_incorrect_base') ?>"+"'></i>").appendTo('.text_sales_taxes_base');
           } else {
            $("<i class='fa fa-check-circle text-info'></i>").appendTo('.text_sales_taxes_base');
           }

           taxes_table_amount = $('.text_sales_taxes_amount').text();
           text_product_tax = $('.text_product_tax').text();
           if (parseFloat(formatDecimal(taxes_table_amount)) != parseFloat(formatDecimal(text_product_tax))) {
            $("<i class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='top' title='"+"<?= lang('report_tax_incorrect_amount') ?>"+"'></i>").appendTo('.text_sales_taxes_amount');
           } else {
            $("<i class='fa fa-check-circle text-info'></i>").appendTo('.text_sales_taxes_amount');
           }

           taxes_table_total = $('.text_sales_taxes_total').text();
           text_total_sales = $('.text_total_sales').text();
           if (parseFloat(formatDecimal(taxes_table_total)) != parseFloat(formatDecimal(text_total_sales))) {
            $("<i class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='top' title='"+"<?= lang('report_tax_incorrect_total') ?>"+"'></i>").appendTo('.text_sales_taxes_total');
           } else {
            $("<i class='fa fa-check-circle text-info'></i>").appendTo('.text_sales_taxes_total');
           }

            $('[data-toggle="tooltip"]').tooltip();
        <?php endif ?>
    });

    $.ajax({
        url : '<?= admin_url('reports/get_purchase_taxes/?v=1&totales=1' . $v) ?>',
        dataType : "JSON"
    }).done(function(data){
        $('.text_purchase_subtotal').text(formatMoney(data.subtotal));
        $('.text_purchase_product_tax').text(formatMoney(data.product_tax));
        $('.text_purchase_order_tax').text(formatMoney(data.order_tax));
        $('.text_purchase_shipping').text(formatMoney(data.shipping));
        $('.text_purchase_order_discount').text(formatMoney(data.total_discount));
        $('.text_purchase_total_sales').text(formatMoney(data.total_sales));
        $('.tbody_purchase_tax').html(data.table_html);

        <?php if ($this->Owner): ?>
           taxes_table_base = $('.text_purchases_taxes_base').text();
           text_subtotal = $('.text_purchase_subtotal').text();
           if (parseFloat(formatDecimal(taxes_table_base)) != parseFloat(formatDecimal(text_subtotal))) {
            $("<i class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='top' title='"+"<?= lang('report_tax_incorrect_base') ?>"+"'></i>").appendTo('.text_purchases_taxes_base');
           } else {
            $("<i class='fa fa-check-circle text-info'></i>").appendTo('.text_purchases_taxes_base');
           }

           taxes_table_amount = $('.text_purchases_taxes_amount').text();
           text_product_tax = $('.text_purchase_product_tax').text();
           if (parseFloat(formatDecimal(taxes_table_amount)) != parseFloat(formatDecimal(text_product_tax))) {
            $("<i class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='top' title='"+"<?= lang('report_tax_incorrect_amount') ?>"+"'></i>").appendTo('.text_purchases_taxes_amount');
           } else {
            $("<i class='fa fa-check-circle text-info'></i>").appendTo('.text_purchases_taxes_amount');
           }

           taxes_table_total = $('.text_purchases_taxes_total').text();
           text_total_purchases = $('.text_purchase_total_sales').text();
           console.log(parseFloat(formatDecimal(taxes_table_total))+" - "+parseFloat(formatDecimal(text_total_purchases)));
           if (parseFloat(formatDecimal(taxes_table_total)) != parseFloat(formatDecimal(text_total_purchases))) {
            $("<i class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='top' title='"+"<?= lang('report_tax_incorrect_total') ?>"+"'></i>").appendTo('.text_purchases_taxes_total');
           } else {
            $("<i class='fa fa-check-circle text-info'></i>").appendTo('.text_purchases_taxes_total');
           }

            $('[data-toggle="tooltip"]').tooltip();
        <?php endif ?>
    });
});

    
</script>

<script type="text/javascript">
$(document).ready(function () {
  $('#form').hide();
  $('.toggle_down').click(function () {
    $("#form").slideDown();
    return false;
  });
  $('.toggle_up').click(function () {
    $("#form").slideUp();
    return false;
  });
});
</script>
<!-- /Scripts Pestaña 1 -->

<!-- Scripts Pestaña 2 -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#purchaseform').hide();
        $('.purchasetoggle_down').click(function () {
            $("#purchaseform").slideDown();
            return false;
        });
        $('.purchasetoggle_up').click(function () {
            $("#purchaseform").slideUp();
            return false;
        });
    });
</script>

<script>
    $(document).ready(function () {
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        oTable = $('#PTAXData').dataTable({
            "aaSorting": [[0, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('reports/get_purchase_taxes/?v=1' . $v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[14];
                nRow.className = (aData[2] == 'received') ? "purchase_link" : "purchase_link warning";
                return nRow;
            },
            "aoColumns": [
                {"mRender": fsd},
                null,
                null,
                {"mRender": row_status},
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var t1 = 0;
                var t2 = 0;
                var t3 = 0;
                var t4 = 0;
                var t5 = 0;
                var t6 = 0;
                var t7 = 0;
                var t8 = 0;
                for (var i = 0; i < aaData.length; i++) {
                    t1 += aaData[aiDisplay[i]][5] != null ? parseFloat(aaData[aiDisplay[i]][5]) : 0;
                    t2 += aaData[aiDisplay[i]][6] != null ? parseFloat(aaData[aiDisplay[i]][6]) : 0;
                    t3 += aaData[aiDisplay[i]][7] != null ? parseFloat(aaData[aiDisplay[i]][7]) : 0;
                    t4 += aaData[aiDisplay[i]][8] != null ? parseFloat(aaData[aiDisplay[i]][8]) : 0;
                    t5 += aaData[aiDisplay[i]][9] != null ? parseFloat(aaData[aiDisplay[i]][9]) : 0;
                    t6 += aaData[aiDisplay[i]][10] != null ? parseFloat(aaData[aiDisplay[i]][10]) : 0;
                    t7 += aaData[aiDisplay[i]][11] != null ? parseFloat(aaData[aiDisplay[i]][11]) : 0;
                    t8 += aaData[aiDisplay[i]][12] != null ? parseFloat(aaData[aiDisplay[i]][12]) : 0;
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[5].innerHTML = currencyFormat(parseFloat(t1));
                nCells[6].innerHTML = currencyFormat(parseFloat(t2));
                nCells[7].innerHTML = currencyFormat(parseFloat(t3));
                nCells[8].innerHTML = currencyFormat(parseFloat(t4));
                nCells[9].innerHTML = currencyFormat(parseFloat(t5));
                nCells[10].innerHTML = currencyFormat(parseFloat(t6));
                nCells[11].innerHTML = currencyFormat(parseFloat(t7));
                nCells[12].innerHTML = currencyFormat(parseFloat(t8));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
        ], "footer");
        <?php endif ?>
    });
</script>
<!-- /Scripts Pestaña 2 -->
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/get_sale_taxes/xls/?v=1'.$v)?>";
            return false;
        });
        $('#xls_2').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/get_sale_taxes/xls_2/?v=1'.$v)?>";
            return false;
        });
        $('#xls1').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/get_purchase_taxes/xls/?v=1'.$v)?>";
            return false;
        });
        $('#xls1_2').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/get_purchase_taxes/xls_2/?v=1'.$v)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.table1'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
        $('#image1').click(function (event) {
            event.preventDefault();
            html2canvas($('.table2'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
        $('#submit_filter').on('click', function(e){
            // e.preventDefault();
            $('#filter_action').val(1);
            $('#sales_filter').submit();
        });
    });

    $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_h').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_h').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });
</script>
