<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
	.ibox-content {
		height: 100%;
		overflow: auto;
		padding: 10px 0;
	}
	.contenedor-canvas {
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.225);
		height: 700px;
		margin: auto;
		width:1200px;
	}
	.nav-link label {
		cursor: inherit;
	}
</style>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="row">
	    <div class="col-md-10">
	        <h2>
	        	<?= lang('restobar'); ?>
	        </h2>
	    </div>
	    <?php if ($this->Owner || $this->Admin) { ?>
		    <div class="col-md-2" style="padding-top: 12px;">
		    	<div class="form-group">
		    		<label for="sucursal" class="form-label col-md-3">sucursal</label>
		    		<div class="col-md-9">
			        	<select class="form-control" name="sucursal" id="sucursal">
			        		<?php foreach ($branches as $key => $branch) { ?>
			        			<option value="<?= $branch->id; ?>" <?= ($branch_id == $branch->id) ? 'selected' : ''; ?>><?= $branch->nombre; ?></option>
			        		<?php } ?>
			        	</select>
		    		</div>
		    	</div>
		    </div>
		<?php } ?>
	</div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
            	<div class="tabs-container">
                    <ul class="nav nav-tabs" role="tablist">
            			<?php foreach ($branch_areas as $key => $area) { ?>
            				<?php $active = ($area_id == $area->id) ? 'active' : ''; ?>
                        	<li class="<?= $active; ?>"><a class="nav-link area_sucursal <?= $active; ?>" id="<?= $area->id ?>" data-toggle="tab" href="#area_<?= $area->id ?>"> <label><?= $area->nombre ?></label></a></li>
        				<?php } ?>
                    </ul>
                    <div class="tab-content">
            			<?php foreach ($branch_areas as $key => $area) { ?>
							<?php $active = ($area_id == $area->id) ? 'active' : ''; ?>
	                        <div role="tabpanel" class="tab-pane <?= $active ?>" id="area_<?= $area->id ?>">
	                            <div class="panel-body">
	                            	<div style="overflow: auto;">
										<div class="contenedor-canvas" id="container_<?= $area->id ?>"></div>
	                            	</div>
	                            </div>
	                        </div>
        				<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= form_open(admin_url('pos/restobar'), 'id="formulario_sucursal"'); ?>
	<input type="hidden" name="id_sucursal" id="id_sucursal" value="<?= $branch_id ?>">
	<input type="hidden" name="id_area" id="id_area">
<?= form_close(); ?>

<?= form_open(admin_url('pos/create_suspended_sale'), 'id="formulario_crear_cuenta"'); ?>
	<input type="hidden" name="id_sucursal" id="id_sucursal" value="<?= $branch_id ?>">
	<input type="hidden" name="numero_mesa" id="numero_mesa">
	<input type="hidden" name="id_mesa" id="id_mesa">
<?= form_close(); ?>

<script type="text/javascript" src="<?= $assets ?>js/konva.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		cargar_mesas(<?= $area_id ?>);

		$(document).on('change', '#sucursal', cargar_areas_sucursal);

		$('body').addClass('mini-navbar');

		$(document).on('click', '.area_sucursal', function() { cargar_mesas_areas($(this).attr('id')); });
	});

	function cargar_areas_sucursal()
	{
		var branch_id = $('#sucursal').val();
		$('#formulario_sucursal #id_sucursal').val(branch_id);

		$('#formulario_sucursal').submit();
	}

	function cargar_mesas_areas(area_id)
	{
		$('#formulario_sucursal #id_area').val(area_id);

		$('#formulario_sucursal').submit();
	}

	function cargar_mesas(area_id)
	{
		var blockSnapSize = 20;
		var width = $('#container_'+area_id).width();
      	var height = $('#container_'+area_id).height();

      	var stage = new Konva.Stage({
        	container: 'container_'+area_id,
        	width: width,
        	height: height
      	});

		var padding = blockSnapSize;
		var gridLayer = new Konva.Layer();
		var newLayer = new Konva.Layer();

		for (var i = 0; i < width / padding; i++) {
		  gridLayer.add(new Konva.Line({
		    points: [Math.round(i * padding) + 0.5, 0, Math.round(i * padding) + 0.5, height],
		    stroke: '#ddd',
		    strokeWidth: 1,
		  }));
		}

		gridLayer.add(new Konva.Line({points: [0,0,10,10]}));
		for (var j = 0; j < height / padding; j++) {
		  gridLayer.add(new Konva.Line({
		    points: [0, Math.round(j * padding), width, Math.round(j * padding)],
		    stroke: '#ddd',
		    strokeWidth: 0.5,
		  }));
		}

      	stage.add(gridLayer);

      	<?php if (! empty($tables)) { ?>
      		<?php foreach ($tables as $i => $table) { ?>
        		/*********************************************************/
        		<?php
        			if ($table->forma_mesa == SQUARE) {
    					$ancho = $table->tamano;
    					$alto = $table->tamano;
    				} else if ($table->forma_mesa == CIRCLE) {
    					$ancho = $table->radio;
    					$alto = $table->radio;
        			} else if ($table->forma_mesa == ELLIPSE) {
        				$ancho = $table->radio;
    					$alto = $table->radio / 1.5;
        			} else {
        				$ancho = $table->ancho;
    					$alto = $table->alto;
        			}

        			if ($table->estado == AVAILABLE) {
        				$color = '';
        				$text_color = '#676a6c';
        			} else if ($table->estado == NOT_AVAILABLE) {
        				$color = '_amarillo';
        				$text_color = '#676a6c';
        			} else if ($table->estado == OCCUPIED) {
        				$color = '_verde';
        				$text_color = '#fefefe';
        			} else if ($table->estado == RESERVED) {
        				$color = '_rojo';
        				$text_color = '#fefefe';
        			}
        		?>

        		var group<?= $i; ?> = new Konva.Group({
        			id: '<?= $table->id; ?>',
        			numero: '<?= $table->numero; ?>',
        			estado: '<?= $table->estado; ?>',
        			venta_suspendida: '<?= isset($table->suspended_bill_id) ? $table->suspended_bill_id : ''; ?>',
			        x: <?= $table->posicion_X; ?>,
          			y: <?= $table->posicion_Y; ?>,
			        rotation: <?= $table->rotacion; ?>
		    	});

		    	var text<?= $i; ?> = new Konva.Text({
			        width: <?= $ancho; ?>,
          			height: <?= $alto; ?>,
			        fontSize: 20,
			        fill: '<?= $text_color; ?>',
			        stroke: '<?= $text_color; ?>',
			        strokeWidth: 2,
			        fontFamily: 'Arial',
			        text: '<?= $table->tipo ." ". $table->numero; ?>',
			        fill: 'gray',
			        align: 'center',
			        verticalAlign: 'middle'
                });

        		var mesa<?= $i; ?> = new Konva.Image({
          			width: <?= $ancho; ?>,
          			height: <?= $alto; ?>
        		});

        		var imageObj<?= $i; ?> = new Image();
        		imageObj<?= $i; ?>.onload = function() {
          			mesa<?= $i; ?>.image(imageObj<?= $i; ?>);
          			gridLayer.draw();
        		};
        		imageObj<?= $i; ?>.src = '<?= base_url("themes/default/admin/assets/images/tables/".$table->imagen.$color.".png"); ?>';

        		group<?= $i; ?>.add(mesa<?= $i; ?>).add(text<?= $i; ?>);
        		gridLayer.add(group<?= $i; ?>);

			    group<?= $i; ?>.on('mousedown touchstart', function() {
			    	var estado_mesa = this.attrs.estado;
			    	var venta_suspendida = this.attrs.venta_suspendida;

			    	if (estado_mesa == <?= AVAILABLE; ?>) {
				    	$('#formulario_crear_cuenta #id_mesa').val(this.attrs.id);
				    	$('#formulario_crear_cuenta #numero_mesa').val(this.attrs.numero);

						$('#formulario_crear_cuenta').submit();
			    	} else {
			    		if (estado_mesa == <?= NOT_AVAILABLE; ?>) {
			    			Command: toastr.error('La mesa se encuentra: No disponible', 'Mensaje del sistema.');
			    		} else if (estado_mesa == <?= OCCUPIED; ?>) {
			    			if (venta_suspendida != '') {
			    				window.location.href = '<?= admin_url('pos/index/'); ?>'+venta_suspendida;
			    			}
			    		} else if (estado_mesa == <?= RESERVED; ?>) {
			    			Command: toastr.error('La mesa se encuentra: Reservada', 'Mensaje del sistema.');
			    		}
			    	}
			    });
        		/*********************************************************/
      		<?php } ?>

			gridLayer.on('mouseover', function(evt) {
				document.body.style.cursor = 'pointer';
				gridLayer.draw();
			});

			gridLayer.on('mouseout', function(evt) {
				document.body.style.cursor = 'default';
				gridLayer.draw();
			});
  		<?php } ?>
	}
</script>