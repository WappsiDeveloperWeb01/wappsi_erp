<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript">if (parent.frames.length !== 0) { top.location = '<?= site_url(); ?>'; }</script>
    <title><?= $page_title; ?></title>
    <meta name="description" content="<?= $page_desc; ?>">
    <link rel="shortcut icon" href="<?= is_file('themes/default/shop/assets/images/'.$shop_settings->shop_favicon) ? $assets.'images/'.$shop_settings->shop_favicon : $assets.'images/icon.png' ?>">
    <link href="<?= $assets; ?>css/libs.min.css<?= $files_version ?>" rel="stylesheet">
    <link href="<?= $assets; ?>css/styles.min.css<?= $files_version ?>" rel="stylesheet">
    <link href="<?= $assets; ?>fonts/iconwappsi/style.css<?= $files_version ?>" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <meta property="og:url" name="shareUrl" content="<?= site_url(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?= $page_title; ?>" />
    <meta property="og:description" content="<?= $page_desc; ?>" />
    <meta property="og:image" name="shareImage" content="<?= site_url('assets/uploads/logos/'.$shop_settings->logo); ?>" />
    <script type="text/javascript">
        var m = '<?= $m; ?>', v = '<?= $v; ?>', products = {}, filters = <?= isset($filters) && !empty($filters) ? json_encode($filters) : '{}'; ?>, shop_color, shop_grid, sorting;
    var cart = <?= isset($cart) && !empty($cart) ? json_encode($cart) : '{}' ?>;
    var site = {base_url: '<?= base_url(); ?>', site_url: '<?= site_url('/'); ?>', shop_url: '<?= shop_url(); ?>', csrf_token: '<?= $this->security->get_csrf_token_name() ?>', csrf_token_value: '<?= $this->security->get_csrf_hash() ?>', 
        lang : {
                'are_u_sure' : '<?= lang('are_u_sure') ?>',
                'action_cant_be_reverted' : '<?= lang('action_cant_be_reverted') ?>',
                'field_required' : '<?= lang('field_required') ?>',
                },
        settings: JSON.parse('<?= json_encode($this->Settings) ?>'), 
        shop_settings: { 
                        use_product_variants : "<?= $shop_settings->use_product_variants ?>", 
                        use_product_preferences : "<?= $shop_settings->use_product_preferences ?>",
                        product_view : "<?= $shop_settings->product_view ?>",
                        }
                    }
    var wishlist_products = JSON.parse('<?= $wishlist_products ?>');

    </script>
    <script src="<?= $assets; ?>js/libs.min.js<?= $files_version ?>"></script>
    <script src="<?= $assets; ?>js/scripts.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $admin_assets ?>js/select2.min.js<?= $files_version ?>"></script>
    <link href="<?= site_url() ?>vendor/select2-4.0.11/dist/css/select2.css<?= $files_version ?>" rel="stylesheet" />
    <script type="text/javascript" src="<?= $admin_assets ?>inspinia/js/plugins/iCheck/icheck.min.js<?= $files_version ?>"></script>
    <link href="<?= site_url() ?>vendor/icheck-1.x/skins/all.css<?= $files_version ?>&v=1.0.2" rel="stylesheet">
    <script type="text/javascript" src="<?= $admin_assets ?>js/jquery.validate.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $admin_assets ?>inspinia/js/plugins/toastr/toastr.min.js<?= $files_version ?>"></script>
    <link href="<?= $admin_assets ?>inspinia/css/plugins/toastr/toastr.min.css<?= $files_version ?>" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css<?= $files_version ?>" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= $admin_assets ?>inspinia/font-awesome/css/font-awesome.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $admin_assets ?>inspinia/font-awesome-5.8.1/css/fontawesome.css<?= $files_version ?>">
    <link href="<?= $admin_assets ?>fontawesome-free-6.3.0-web/css/fontawesome.css<?= $files_version ?>" rel="stylesheet">
    <link href="<?= $admin_assets ?>fontawesome-free-6.3.0-web/css/brands.css<?= $files_version ?>" rel="stylesheet">
    <link href="<?= $admin_assets ?>fontawesome-free-6.3.0-web/css/solid.css<?= $files_version ?>" rel="stylesheet">

    <script type="text/javascript">
        store('shop_grid',  "<?= $shop_settings->product_view == 1 ? '.two-col' : '.three-col' ?>");
        var search_query = '';
        <?php if (isset($_GET['search'])): ?>
            search_query = '<?= $_GET['search'] ?>';
        <?php endif ?>
    </script>
    <style type="text/css">
        .error {
            color :#f17272;
        }
        
    </style>
</head>
<body>
    <?php if (!$store_open): ?>
        <div class="row header_message">
            <div class="col-md-12 alert alert-warning">
                <i class="fa fa-exclamation-triangle"></i> La tienda se encuentra cerrada en el momento, no se aceptan nuevos pedidos.
            </div>
        </div>
        <script type="text/javascript">
            swal({
                title: 'Tienda cerrada',
                html: 'La tienda se encuentra cerrada en el momento, no se aceptan nuevos pedidos.',
                type: "warning",
                confirmButtonText: "Entendido"
            });
        </script>
    <?php endif ?>
    <section id="wrapper" class="blue">
        <header>
            <!-- Top Header -->
            <section class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                        <?php
                        if (!empty($pages)) {
                            echo '<ul class="list-inline nav pull-left hidden-xs">';
                            foreach ($pages as $page) {
                                echo '<li><a href="'.site_url('page/'.$page->slug).'">'.$page->name.'</a></li>';
                            }
                            echo '</ul>';
                        }
                        ?>

                            <ul class="list-inline nav pull-right">
                                <?php 
                                    $vbillers_style = "";
                                    if ($this->virtual_billers && count($this->virtual_billers) == 1) {
                                        $vbillers_style = "style='display:none;'";
                                    }
                                 ?>
                                <li <?= $vbillers_style ?>>
                                    <a href="#" class="dropdown-toggle hidden-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <?= sprintf(lang('shop_navbar_biller_ubication_details'), $this->shop_settings->biller_data->name) ?>
                                        <i class="fa fa-caret-down"></i>
                                    </a>
                                    <a href="#" class="dropdown-toggle visible-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <?= sprintf($this->shop_settings->biller_data->name) ?>
                                        <i class="fa fa-caret-down"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <?php if ($this->virtual_billers): ?>
                                            <?php foreach ($this->virtual_billers as $vbiller): ?>
                                                <?php if ($vbiller->biller_id != $this->shop_settings->biller_data->biller_id): ?>
                                                    <li class="set_session_biller" data-billerid="<?= $vbiller->biller_id ?>">
                                                        <a href="#"><?= $vbiller->name ?></a>
                                                    </li>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </ul>
                                </li>
                                <?php
                                if (DEMO) {
                                    echo '<li class="hidden-xs hidden-sm"><a href="https://codecanyon.net/item/shop-module/20046278?ref=Tecdiary" class="green" target="_blank"><i class="fa fa-shopping-cart"></i> Buy Now!</a></li>';
                                }
                                ?>
                                <?= $loggedIn && $Staff ? '<li class="hidden-xs"><a href="'.admin_url().'"><i class="fa fa-dashboard"></i> '.lang('admin_area').'</a></li>' : ''; ?>
                                <?php 
                                    $scanned_lang_dir = array_map(function ($path) {
                                        return basename($path);
                                        }, glob(APPPATH . 'language/*', GLOB_ONLYDIR));

                                    $language_style = "";
                                    if (count($scanned_lang_dir) == 1) {
                                        $language_style = "style='display:none;'";
                                    }
                                 ?>
                                <li class="dropdown" <?= $language_style ?>>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <img src="<?= base_url('assets/images/' . $Settings->user_language . '.png'); ?>" alt="">
                                        <span class="hidden-xs">&nbsp;&nbsp;<?= ucwords(lang($Settings->user_language)); ?></span>
                                        <i class="fa fa-caret-down margin-left-md"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <?php
                                        foreach ($scanned_lang_dir as $entry) {
                                        if (file_exists(APPPATH.'language'.DIRECTORY_SEPARATOR.$entry.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'shop_lang.php')) {
                                        ?>
                                        <li>
                                            <a href="<?= site_url('main/language/' . $entry); ?>">
                                                <img src="<?= base_url('assets/images/'.$entry.'.png'); ?>" class="language-img">
                                                &nbsp;&nbsp;<?= ucwords(lang($entry)); ?>
                                            </a>
                                        </li>
                                        <?php }
                                        } ?>
                                    </ul>
                                </li>
                            <?php if (!empty($currencies)) { ?>
                            <?php 
                                $currencies_styles = "";
                                if (count($currencies) == 1) {
                                    $currencies_styles = "style='display:none;'";
                                }
                             ?>
                            <li class="dropdown" <?= $currencies_styles ?>>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <?= $selected_currency->code.' '.$selected_currency->name; ?>
                                    <span class="fa fa-caret-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php
                                    foreach ($currencies as $currency) {
                                        echo '<li><a href="'.site_url('main/currency/' . $currency->code).'">'.$currency->code.' '.$currency->name.'</a></li>';
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php } ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Top Header -->

            <!-- Main Header -->
            <section class="main-header">
                <div class="container padding-y-md">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 logo" style="text-align: center;">
                            <a href="<?= site_url(); ?>">
                                <img alt="<?= $shop_settings->shop_name; ?>" src="<?= base_url('assets/uploads/logos/'.$shop_settings->logo); ?>" class="site_logo" />
                            </a>
                        </div>
                        <div class="col-sm-8 col-md-8 margin-top-lg">
                            <div class="row">
                                <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 search-box">
                                    <?= shop_form_open('products', 'id="product-search-form"'); ?>
                                    <!-- <div class="input-group"> -->
                                        <input name="query" type="text" class="form-control" id="product-search" aria-label="<?= lang('search_products') ?>" placeholder="<?= lang('search_products') ?>" value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>" style="border-radius:30px;">
                                        <!-- <span class="input-group-btn"> -->
                                            <button type="submit" class="btn btn-default btn-search"><i class="fa fa-search"></i></button>
                                        <!-- </span> -->
                                    <!-- </div> -->
                                    <?= form_close(); ?>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 cart-btn">
                                    <span class="dropdown-toggle shopping-cart" id="dropdown-cart" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                    style="font-size: 170%; padding: 2%;">
                                        <i class="iconw-cart_outlined"></i>
                                        <span class="badge total_cart_items" id="total_cart_items">0</span>
                                        <!-- <span class="cart-total-items"></span> -->
                                        <!-- <i class="fa fa-caret-down margin-left-md"></i> -->
                                    </span>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-cart">
                                        <div id="cart-contents">
                                            <table class="table table-cart" id="cart-items"></table>
                                            <div id="cart-links" class="text-center margin-bottom-md">
                                                <div class="btn-group btn-group-justified" role="group" aria-label="View Cart and Checkout Button">
                                                    <div class="btn-group">
                                                        <a class="btn btn-default btn-sm" id="empty-cart-2" href="<?= site_url('cart/destroy'); ?>"><i class="fa fa-trash"></i> <?= lang('empty_cart'); ?></a>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a class="btn btn-default btn-sm" href="<?= site_url('cart'); ?>"><i class="fa fa-shopping-cart"></i> <?= lang('view_cart'); ?></a>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a class="btn btn-default btn-sm" href="<?= site_url('cart/checkout'); ?>"><i class="fa fa-check"></i> <?= lang('checkout'); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="cart-empty"><?= lang('please_add_item_to_cart'); ?></div>
                                    </div>
                                </div>

                                <?php if ($loggedIn) { ?>
                                    <div class="col-lg-4 col-md-2 col-sm-2 col-xs-3 text-right">
                                        <span href="#" class="dropdown-toggle shopping-cart" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="font-size: 200%; padding: 2%;cursor:pointer;">
                                            <!-- <?= lang('shop_welcome').', '.$loggedInUser->first_name; ?> <span class="caret"></span> -->
                                            <i class="iconw-user_outlined"></i>  <span class="caret"></span>
                                        </span>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li class=""><a class=" shopping-cart" href="<?= site_url('profile'); ?>"><i class="mi fa fa-user"></i> <?= lang('profile'); ?></a></li>
                                            <li class=""><a class=" shopping-cart" href="<?= shop_url('orders'); ?>"><i class="mi fa fa-heart"></i> <?= lang('orders'); ?></a></li>
                                            <li class=""><a class=" shopping-cart" href="<?= shop_url('quotes'); ?>"><i class="mi fa fa-heart-o"></i> <?= lang('quotes'); ?></a></li>
                                            <li class=""><a class=" shopping-cart" href="<?= shop_url('downloads'); ?>"><i class="mi fa fa-download"></i> <?= lang('downloads'); ?></a></li>
                                            <li class=""><a class=" shopping-cart" href="<?= shop_url('addresses'); ?>"><i class="mi fa fa-building"></i> <?= lang('addresses'); ?></a></li>
                                            <li class="divider"></li>
                                            <li class=""><a class=" shopping-cart" href="<?= site_url('logout'); ?>"><i class="mi fa fa-sign-out"></i> <?= lang('logout'); ?></a></li>
                                        </ul>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-lg-4 col-md-2 col-sm-2 col-xs-3 text-right">
                                        <span class="shopping-cart login_div" type="button" style="font-size: 170%; padding: 2%;cursor:pointer;" onclick="$('#loginModal').modal('show');">
                                                <i class="iconw-user_outlined"></i> <span class="caret"></span>
                                            </span>
                                        <!-- <div class="dropdown">
                                            <span class="dropdown-toggle shopping-cart login_div" type="button" id="dropdownLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="font-size: 170%; padding: 2%;cursor:pointer;">
                                                <i class="iconw-user_outlined"></i> <span class="caret"></span>
                                            </span>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-login" aria-labelledby="dropdownLogin" data-dropdown-in="zoomIn" data-dropdown-out="fadeOut" style="padding: 2.5%;">
                                                <?php /* include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR.'login_form.php'; */ ?>
                                            </div>
                                        </div> -->
                                    </div>
                                    <?php
                                }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Main Header -->

            <!-- Nav Bar -->
            <?php $xs_categories = $categories; ?>
            <nav class="navbar navbar-default" role="navigation" style="max-height: 40px;">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="btn btn-primary navbar-toggle dropdown">
                                <span href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-th"></i>
                                </span>
                                <?php
                                if (count($xs_categories) <= 10) {
                                    ?>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach($xs_categories as $category) {
                                            echo '<li class=""><a href="'.site_url('category/'.$category->slug.'"').' class="line-height-lg">'.$category->name.'</a>';
                                            echo '</li>';
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                } elseif (count($xs_categories) <= 20) {
                                    ?>
                                    <div class="dropdown-menu">
                                        <div class="dropdown-menu-content">
                                            <?php
                                            $xs_categories_chunks = array_chunk($xs_categories, 10);
                                            foreach($xs_categories_chunks as $xs_categories) {
                                                ?>
                                                <div class="col-sm-6 padding-x-no line-height-md">
                                                    <ul class="nav">
                                                        <?php
                                                        foreach($xs_categories as $category) {
                                                            echo '<li class="">
                                                                    <a href="'.site_url('category/'.$category->slug.'"').' class="line-height-lg">
                                                                        <i class="no-desktop fa fa-chevron-right" style="font-size:70%;"></i>  '
                                                                        .$category->name.
                                                                    '</a>';
                                                            echo '</li>';
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                } elseif (count($xs_categories) > 20) {
                                    ?>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="mega-menu-content">
                                                <div class="row">
                                                    <?php
                                                    $xs_categories_chunks = array_chunk($xs_categories, ceil(count($xs_categories)/4));
                                                    foreach($xs_categories_chunks as $xs_categories) {
                                                        ?>
                                                        <div class="col-sm-3">
                                                            <ul class="list-unstyled">
                                                                <?php
                                                                foreach($xs_categories as $category) {
                                                                    echo '<li class="line-height-lg "><a href="'.site_url('category/'.$category->slug.'"').'>'.$category->name.'</a>';
                                                                    echo'</li>';
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php
                                }
                                ?>
                        </button>
                        
                        <?php if ($this->shop_settings->show_promotion_products == 1 && $isPromo) { ?>
                            <button type="button" class="btn btn-primary navbar-toggle" onclick="window.location.href = '<?= shop_url('products?promo=yes'); ?>';">
                                <span>
                                    <i class="fas fa-tags"></i>
                                </span>
                            </button>
                        <?php } ?>
                        <?php if ($this->shop_settings->show_featured_products == 1 && $isFeatured) { ?>
                            <button type="button" class="btn btn-primary navbar-toggle" onclick="window.location.href = '<?= shop_url('products?featured=yes'); ?>';">
                                <span>
                                    <i class="fas fa-star"></i>
                                </span>
                            </button>
                        <?php } ?>
                        <?php if ($this->shop_settings->show_new_products == 1 && $isNew) { ?>
                            <button type="button" class="btn btn-primary navbar-toggle" onclick="window.location.href = '<?= shop_url('products?new=yes'); ?>';">
                                <span>
                                    <i class="fa-solid fa-calendar-day"></i>
                                </span>
                            </button>
                        <?php } ?>
                        <?php if ($this->shop_settings->show_new_products == 1): ?>
                            <button type="button" class="btn btn-primary navbar-toggle" onclick="window.location.href = '<?= shop_url('products?best_selling=yes'); ?>';">
                                <span>
                                    <i class="fa-solid fa-bolt-lightning"></i>
                                </span>
                            </button>
                        <?php endif ?>
                        <?php if ($this->shop_settings->show_favorite_products == 1): ?>
                            <button type="button" class="btn btn-primary navbar-toggle" onclick="window.location.href = '<?= shop_url('wishlist'); ?>';">
                                <span>
                                    <i class="fa fa-heart"></i>
                                </span>
                            </button>
                        <?php endif ?>


                        <!-- <a href="<?= site_url('cart'); ?>" class="btn btn-default btn-cart-xs visible-xs pull-right shopping-cart">
                            <i class="fa fa-shopping-cart"></i> <span class="cart-total-items"></span>
                        </a> -->
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-ex1-collapse">
                        <ul class="nav navbar-nav">
                            <li class="dropdown<?= (count($categories) > 20) ? ' mega-menu' : ''; ?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-th"></i> <?= lang('categories'); ?> <span class="caret"></span>
                                </a>
                                <?php
                                if (count($categories) <= 10) {
                                    ?>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach($categories as $category) {
                                            echo '<li class="'.($category->subcategories ? 'dropdown dropdown-submenu' : '').'"><a '.($category->subcategories ? 'class="dropdown-toggle" data-toggle="dropdown"' : '').' '.($category->subcategories ? 'href="#"' : 'href="'.site_url('category/'.$category->slug).'"').' class="line-height-lg">'.$category->name.'</a>';
                                            if ($category->subcategories) {
                                                echo '<ul class="dropdown-menu">';
                                                foreach($category->subcategories as $sc) {
                                                    echo '<li><a href="'.site_url('category/'.$category->slug.'/'.$sc->slug).'"><i class="no-desktop fa fa-chevron-right" style="font-size:70%;"></i> '.$sc->name.'</a></li>';
                                                }
                                                echo '<li class="divider"></li>';
                                                echo '<li><a href="'.site_url('category/'.$category->slug).'">'.lang('all_products').'</a></li>';
                                                echo '</ul>';
                                            }
                                            echo '</li>';
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                } elseif (count($categories) <= 20) {
                                    ?>
                                    <div class="dropdown-menu dropdown-menu-2x">
                                        <div class="dropdown-menu-content">
                                            <?php
                                            $categories_chunks = array_chunk($categories, 10);
                                            foreach($categories_chunks as $categories) {
                                                ?>
                                                <div class="col-sm-6 padding-x-no line-height-md">
                                                    <ul class="nav">
                                                        <?php
                                                        foreach($categories as $category) {
                                                            echo '<li class="'.($category->subcategories ? 'dropdown dropdown-submenu' : '').'">
                                                                    <a '.($category->subcategories ? 'class="dropdown-toggle" data-toggle="dropdown"' : '').' '.($category->subcategories ? 'href="#"' : 'href="'.site_url('category/'.$category->slug).'"').' class="line-height-lg">
                                                                        <i class="no-desktop fa fa-chevron-right" style="font-size:70%;"></i>  '
                                                                        .$category->name.
                                                                    '</a>';
                                                            if ($category->subcategories) {
                                                                echo '<ul class="dropdown-menu">';
                                                                foreach($category->subcategories as $sc) {
                                                                    echo '<li><a href="'.site_url('category/'.$category->slug.'/'.$sc->slug).'"><i class="no-desktop fa fa-chevron-right" style="font-size:70%;"></i> '.$sc->name.'</a></li>';
                                                                }
                                                                echo '<li class="divider"></li>';
                                                                echo '<li><a href="'.site_url('category/'.$category->slug).'">'.lang('all_products').'</a></li>';
                                                                echo '</ul>';
                                                            }
                                                            echo '</li>';
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                } elseif (count($categories) > 20) {
                                    ?>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="mega-menu-content">
                                                <div class="row">
                                                    <?php
                                                    $categories_chunks = array_chunk($categories, ceil(count($categories)/4));
                                                    foreach($categories_chunks as $categories) {
                                                        ?>
                                                        <div class="col-sm-3">
                                                            <ul class="list-unstyled">
                                                                <?php
                                                                foreach($categories as $category) {
                                                                    echo '<li class="line-height-lg '.($category->subcategories ? 'dropdown dropdown-submenu' : '').'"><a '.($category->subcategories ? 'class="dropdown-toggle" data-toggle="dropdown"' : '').' '.($category->subcategories ? 'href="#"' : 'href="'.site_url('category/'.$category->slug).'"').'>'.$category->name.'</a>';
                                                                    if ($category->subcategories) {
                                                                        echo '<ul class="dropdown-menu">';
                                                                        foreach($category->subcategories as $sc) {
                                                                            echo '<li><a href="'.site_url('category/'.$category->slug.'/'.$sc->slug).'"><i class="no-desktop fa fa-chevron-right" style="font-size:70%;"></i> '.$sc->name.'</a></li>';
                                                                        }
                                                                        echo '<li class="divider"></li>';
                                                                        echo '<li><a href="'.site_url('category/'.$category->slug).'">'.lang('all_products').'</a></li>';
                                                                        echo '</ul>';
                                                                    }
                                                                    echo'</li>';
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php
                                }
                                ?>
                            </li>
                            <?php if ($this->shop_settings->show_promotion_products == 1 && $isPromo) { ?>
                            <li class="<?= $m == 'shop' && $v == 'products' && $this->input->get('promo') == 'yes' ? 'active' : ''; ?>">
                                <a href="<?= shop_url('products?promo=yes'); ?>"><i class="fas fa-tags"></i> <?= lang('day_promotions'); ?></a>
                            </li>
                            <?php } ?>
                            <?php if ($this->shop_settings->show_featured_products == 1 && $isFeatured) { ?>
                            <li class="<?= $m == 'shop' && $v == 'products' && $this->input->get('featured') == 'yes' ? 'active' : ''; ?>">
                                <a href="<?= shop_url('products?featured=yes'); ?>"><i class="fas fa-star"></i>  <?= lang('featured_products'); ?></a>
                            </li>
                            <?php } ?>
                            <?php if ($this->shop_settings->show_new_products == 1 && $isNew) { ?>
                            <li class="<?= $m == 'shop' && $v == 'products' && $this->input->get('new') == 'yes' ? 'active' : ''; ?>">
                                <a href="<?= shop_url('products?new=yes'); ?>"><i class="fa-solid fa-calendar-day"></i>  <?= lang('new_products'); ?></a>
                            </li>
                            <?php } ?>
                            <?php if ($this->shop_settings->show_new_products == 1): ?>
                                <li class="<?= $m == 'shop' && $v == 'products' && $this->input->get('best_selling') == 'yes' ? 'active' : ''; ?>"><a href="<?= shop_url('products?best_selling=yes'); ?>"><i class="fa-solid fa-bolt-lightning"></i> <?= lang('best_selling') ?></a></li>
                            <?php endif ?>
                            <?php if ($this->shop_settings->show_favorite_products == 1): ?>
                                <li class="<?= $m == 'shop' && $v == 'wishlist' ? 'active' : ''; ?>">
                                    <a href="<?= shop_url('wishlist'); ?>"><i class="fa fa-heart"></i> <?= lang('wishlist'); ?> (<span id="total-wishlist"><?= $wishlist; ?></QAspan>)</a>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Nav Bar -->
        </header>
        <?php if (DEMO && ($m != 'main' || $v != 'index')) { ?>
        <div class="page-contents padding-bottom-no">
            <div class="container">
                <div class="alert alert-info margin-bottom-no">
                    <p>
                        <strong>Shop module is not complete item but add-on to Stock Manager Advance and is available separately.</strong><br>
                        This is joint demo for main item (Stock Manager Advance) and add-ons (POS & Shop Module). Please check the item page on codecanyon.net for more info about what's not included in the item and you must read the page there before purchase. Thank you
                    </p>
                </div>
            </div>
        </div>
        <?php } ?>
