<?php
require_once APPPATH.'../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


$nit_old = $sucursal_old = $vendedor_old =  '' ; 
$total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;
$total_vendedor = $total_30_vendedor = $total_60_vendedor = $total_90_vendedor = $total_120_vendedor =  $total_mas_vendedor = 0;
$i = $j = 0 ;
$saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';
$total_sucursal = $total_30_sucursal = $total_60_sucursal = $total_90_sucursal = $total_120_sucursal =  $total_mas_sucursal = 0;
$prin_ven = $prin_clien = false; 

$j++;
$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(30);

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1:I1")->getFont()->setBold(true);

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, 'DOCUMENTO')
			->setCellValue('B'.$j, 'FECHA')
			->setCellValue('C'.$j, 'DIAS EN MORA')
			->setCellValue('D'.$j, 'SALDO 0 - 30')
			->setCellValue('E'.$j, 'SALDO 31 - 60')
			->setCellValue('F'.$j, 'SALDO 61 - 90')
			->setCellValue('G'.$j, 'SALDO 91 - 120')
			->setCellValue('H'.$j, 'SALDO 121 o mas') 
			->setCellValue('i'.$j, 'SALDO TOTAL') ;

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(16);

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultColumnDimension()
			->setWidth(16);

foreach ($sales as $sale) 
{
    $validador = true;
    $i++;
	$saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';

    if ($sale['documento'] != $nit_old)
    {
        if ($i > 1)
        {
            $j++;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':H'.$j)->getAlignment()->setHorizontal(
				PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
			);
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$j, 'SubTotal Cliente')
						->setCellValue('D'.$j, number_format($total_30,0,'',''))
						->setCellValue('E'.$j, number_format($total_60,0,'',''))
						->setCellValue('F'.$j, number_format($total_90,0,'',''))
						->setCellValue('G'.$j, number_format($total_120,0,'',''))
						->setCellValue('H'.$j, number_format($total_mas,0,'',''))
						->setCellValue('I'.$j, number_format($total,0,'','')) ;
            $validador = false;
        }
        $total_vendedor += $total;
        $total_30_vendedor += $total_30;
        $total_60_vendedor += $total_60;
        $total_90_vendedor += $total_90;
        $total_120_vendedor += $total_120; 
        $total_mas_vendedor += $total_mas;  
	}
	
	if ($sale['cod_Sucursal'] != $sucursal_old)
    {
        if ($i > 1)
        {
            if ($prin_ven == false)
            {
                if ($prin_clien == false) 
                {
                    $j++;
					$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
					$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':I'.$j)->getAlignment()->setHorizontal(
						PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
					);
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, 'SubTotal Cliente')
								->setCellValue('D'.$j, number_format($total_30,0,'',''))
								->setCellValue('E'.$j, number_format($total_60,0,'',''))
								->setCellValue('F'.$j, number_format($total_90,0,'',''))
								->setCellValue('G'.$j, number_format($total_120,0,'',''))
								->setCellValue('H'.$j, number_format($total_mas,0,'',''))
								->setCellValue('I'.$j, number_format($total,0,'','')) ;
					$total_vendedor += $total;
					$total_30_vendedor += $total_30;
					$total_60_vendedor += $total_60;
					$total_90_vendedor += $total_90;
					$total_120_vendedor += $total_120; 
					$total_mas_vendedor += $total_mas;
                }
                $j++;
				$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
				$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':I'.$j)->getAlignment()->setHorizontal(
					PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
				);
				$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue('A'.$j, 'SubTotal Vendedor')
									->setCellValue('D'.$j, number_format($total_30_vendedor,0,'',''))
									->setCellValue('E'.$j, number_format($total_60_vendedor,0,'',''))
									->setCellValue('F'.$j, number_format($total_90_vendedor,0,'',''))
									->setCellValue('G'.$j, number_format($total_120_vendedor,0,'',''))
									->setCellValue('H'.$j, number_format($total_mas_vendedor,0,'',''))
									->setCellValue('I'.$j, number_format($total_vendedor,0,'','')) ; 
                $total_sucursal += $total_vendedor;
                $total_30_sucursal += $total_30_vendedor;
                $total_60_sucursal += $total_60_vendedor;
                $total_90_sucursal += $total_90_vendedor;
                $total_120_sucursal += $total_120_vendedor; 
                $total_mas_sucursal += $total_mas_vendedor;
                $total_vendedor = $total_30_vendedor = $total_60_vendedor = $total_90_vendedor = $total_120_vendedor =  $total_mas_vendedor = 0;
            }
            $total_sucursal += $total_vendedor;
            $total_30_sucursal += $total_30_vendedor;
            $total_60_sucursal += $total_60_vendedor;
            $total_90_sucursal += $total_90_vendedor;
            $total_120_sucursal += $total_120_vendedor; 
            $total_mas_sucursal += $total_mas_vendedor;

			$j++;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':I'.$j)->getAlignment()->setHorizontal(
				PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
			);
			$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, 'SubTotal Sucursal')
								->setCellValue('D'.$j, number_format($total_30_sucursal,0,'',''))
								->setCellValue('E'.$j, number_format($total_60_sucursal,0,'',''))
								->setCellValue('F'.$j, number_format($total_90_sucursal,0,'',''))
								->setCellValue('G'.$j, number_format($total_120_sucursal,0,'',''))
								->setCellValue('H'.$j, number_format($total_mas_sucursal,0,'',''))
								->setCellValue('I'.$j, number_format($total_sucursal,0,'','')) ;
								
        }
        $j++;		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':H'.$j.''); 
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':B'.$j)->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, 'Sucursal: '.$sale['Sucursal']);		
        $sucursal_old = $sale['cod_Sucursal']; 
        $vendedor_old =  '' ;
        $total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;
        $total_vendedor = $total_30_vendedor = $total_60_vendedor = $total_90_vendedor = $total_120_vendedor =  $total_mas_vendedor = 0;
        $i = 0;
        $saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';
        $total_sucursal = $total_30_sucursal = $total_60_sucursal = $total_90_sucursal = $total_120_sucursal =  $total_mas_sucursal = 0;
        $prin_ven = $prin_clien = false; 
    }

	if ($sale['cod_seller'] != $vendedor_old)
    {
        if ($i > 1)
        {
            $j++;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':I'.$j)->getAlignment()->setHorizontal(
				PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
			);
			$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, 'SubTotal Vendedor')
								->setCellValue('D'.$j, number_format($total_30_vendedor,0,'',''))
								->setCellValue('E'.$j, number_format($total_60_vendedor,0,'',''))
								->setCellValue('F'.$j, number_format($total_90_vendedor,0,'',''))
								->setCellValue('G'.$j, number_format($total_120_vendedor,0,'',''))
								->setCellValue('H'.$j, number_format($total_mas_vendedor,0,'',''))
								->setCellValue('I'.$j, number_format($total_vendedor,0,'','')) ;

            $total_sucursal += $total_vendedor;
            $total_30_sucursal += $total_30_vendedor;
            $total_60_sucursal += $total_60_vendedor;
            $total_90_sucursal += $total_90_vendedor;
            $total_120_sucursal += $total_120_vendedor; 
            $total_mas_sucursal += $total_mas_vendedor;
            $total_vendedor = $total_30_vendedor = $total_60_vendedor = $total_90_vendedor = $total_120_vendedor =  $total_mas_vendedor = 0;
        }
        $j++;
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':I'.$j)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':I'.$j.''); 
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, 'Vendedor: '.$sale['nom_seller']);
        $total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;  
        $vendedor_old = $sale['cod_seller']; 
        if ($sale['documento'] == $nit_old)
        {
            $j++;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$j.':E'.$j.'');
			$objPHPExcel->getActiveSheet()->mergeCells('F'.$j.':G'.$j.'');
			$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, 'CLIENTE: '.$sale['nom_companie'])
								->setCellValue('D'.$j, 'NIT: '.$sale['documento'])
								->setCellValue('F'.$j, 'TEL.: '.$sale['phone'])
								->setCellValue('H'.$j, $sale['city']) ;

			$nit_old = $sale['documento']; 
			$total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0; 
        }
    }

	if ($sale['documento'] != $nit_old)
    {
        $j++;
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
		$objPHPExcel->getActiveSheet()->mergeCells('D'.$j.':E'.$j.'');
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$j.':G'.$j.'');
		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$j, 'CLIENTE: '.$sale['nom_companie'])
							->setCellValue('D'.$j, 'NIT: '.$sale['documento'])
							->setCellValue('F'.$j, 'TEL.: '.$sale['phone'])
							->setCellValue('H'.$j, $sale['city']) ;

        $nit_old = $sale['documento']; 
        $total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0; 
    }

    $fecha = substr($sale['due_date'],0,10);
    $fecha_now = date('Y-m-d');
    $dias	= (strtotime($fecha)-strtotime($fecha_now))/86400;
    $dias 	= abs($dias); 
    $dias 	= floor($dias);

    $Saldo = number_format($sale['Saldo'],0,'','');

    if ($dias <=30) 
    {
        $saldo_30 = $Saldo;
        $total_30 += $sale['Saldo'];
    }
    else if ($dias <=60)
    {
        $saldo_60 = $Saldo;
        $total_60 += $sale['Saldo'];
    }
    else if ($dias <=90)
    {
        $saldo_90 = $Saldo;
        $total_90 += $sale['Saldo'];
    }
    elseif ($dias <=120) 
    { 
        $saldo_120 = $Saldo;
        $total_120 += $sale['Saldo'];
    }
    else
    {
        $saldo_mas = $Saldo;
        $total_mas += $sale['Saldo'];
    }
	$total += number_format($sale['Saldo'],0,'','');

	$j++;
	$objPHPExcel->setActiveSheetIndex(0)->getStyle('C'.$j.':H'.$j)->getAlignment()->setHorizontal(
		PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	);

	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, $sale['Referencia'])
				->setCellValue('B'.$j, $fecha)
				->setCellValue('C'.$j, $dias)
				->setCellValue('D'.$j, $saldo_30)
				->setCellValue('E'.$j, $saldo_60)
				->setCellValue('F'.$j, $saldo_90)
				->setCellValue('G'.$j, $saldo_120)
				->setCellValue('H'.$j, $saldo_mas) 
				->setCellValue('I'.$j, $total) ;
}

$j++;
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':I'.$j)->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$j, 'SubTotal Cliente')
					->setCellValue('D'.$j, number_format($total_30,0,'',''))
					->setCellValue('E'.$j, number_format($total_60,0,'',''))
					->setCellValue('F'.$j, number_format($total_90,0,'',''))
					->setCellValue('G'.$j, number_format($total_120,0,'',''))
					->setCellValue('H'.$j, number_format($total_mas,0,'',''))
					->setCellValue('I'.$j, number_format($total,0,'','')) ;		

$total_vendedor += $total;
$total_30_vendedor += $total_30;
$total_60_vendedor += $total_60;
$total_90_vendedor += $total_90;
$total_120_vendedor += $total_120; 
$total_mas_vendedor += $total_mas;

$j++;
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':I'.$j)->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$j, 'SubTotal Vendedor')
					->setCellValue('D'.$j, number_format($total_30_vendedor,0,'',''))
					->setCellValue('E'.$j, number_format($total_60_vendedor,0,'',''))
					->setCellValue('F'.$j, number_format($total_90_vendedor,0,'',''))
					->setCellValue('G'.$j, number_format($total_120_vendedor,0,'',''))
					->setCellValue('H'.$j, number_format($total_mas_vendedor,0,'',''))
					->setCellValue('I'.$j, number_format($total_vendedor,0,'','')) ;

$total_sucursal += $total_vendedor;
$total_30_sucursal += $total_30_vendedor;
$total_60_sucursal += $total_60_vendedor;
$total_90_sucursal += $total_90_vendedor;
$total_120_sucursal += $total_120_vendedor; 
$total_mas_sucursal += $total_mas_vendedor;

$j++;
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j.':H'.$j)->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
$objPHPExcel->getActiveSheet()->mergeCells('A'.$j.':C'.$j.''); 
$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$j, 'SubTotal Sucursal')
					->setCellValue('D'.$j, number_format($total_30_sucursal,0,'',''))
					->setCellValue('E'.$j, number_format($total_60_sucursal,0,'',''))
					->setCellValue('F'.$j, number_format($total_90_sucursal,0,'',''))
					->setCellValue('G'.$j, number_format($total_120_sucursal,0,'',''))
					->setCellValue('H'.$j, number_format($total_mas_sucursal,0,'',''))
					->setCellValue('I'.$j, number_format($total_sucursal,0,'','')) ;

$objPHPExcel->getActiveSheet()->getStyle('D1:D'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('E1:E'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('F1:F'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('H1:H'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('I1:I'.$j)->getNumberFormat()->setFormatCode('#,##0.00');

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('CARTERA POR VENCIMIENTOS');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="CARTERA POR VENCIMIENTOS.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
