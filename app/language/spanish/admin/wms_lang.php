<?php defined('BASEPATH') OR exit('No direct script access allowed');

// menú
$lang['wms_pickings'] 						= 'Selecciones (Pickings)';
$lang['add_picking']                        = "Agregar selección (Picking)";

//general
$lang['notices'] 							= 'Novedades';
$lang['order_sale_reference_no'] 			= 'N° orden de pedido';
$lang['available_quantity'] 				= 'Cant. disponible';
$lang['required_quantity'] 					= 'Cant. requerida';
$lang['picking_notice'] 					= 'Novedad';
$lang['quantity_notice'] 					= 'Cant. novedad';
$lang['packing_warehouse'] 					= 'Bodega Empaque';
$lang['wms_pickings'] 						= 'Selecciones (Pickings)';
$lang['wms_picking'] 						= 'Selección (Picking)';
$lang['wms_picking_added'] 					= 'Selección (Picking) añadido correctamente';
$lang['picking_reference_no'] 			    = 'N° Selección (Picking)';
$lang['add_packing_box'] 			    	= 'Añadir nueva caja para empaque';