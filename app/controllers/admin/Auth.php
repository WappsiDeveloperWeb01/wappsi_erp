<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->lang->admin_load('auth', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->admin_model('auth_model');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('settings_model');
        $this->load->library('ion_auth');
    }

    function index()
    {

        if (!$this->loggedIn) {
            // if ($this->Settings->close_year_step >= 1 || $this->Settings->close_year_step == 1) {
            //     redirect($this->site->url_origin().'/wappsi_consola');
            // } else {
                admin_redirect('login');
            // }
        } else {
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function users()
    {
        if ( ! $this->loggedIn) {
            admin_redirect('login');
        }
        $this->sma->checkPermissions();
        $this->data["countByProfiles"] = $this->auth_model->getCountByProfile();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->page_construct('auth/index', ['page_title' => lang('users')], $this->data);
    }

    function getUsers()
    {
        $optionFilter = $this->input->post('optionFilter') != 'false' ? $this->input->post('optionFilter') : '';

        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('users').".id as id, avatar, first_name, last_name, username, email, company, award_points, " . $this->db->dbprefix('groups') . ".name, login_status, login_hour, active")
            ->from("users")
            ->join('groups', 'users.group_id=groups.id', 'left')
            ->group_by('users.id')
            ->edit_column('active', '$1__$2', 'active, id')
            ->edit_column('login_status', '$1__$2', 'login_status, id');
        if ($this->Admin || $this->Owner || $this->GP['auth-profile']) {
            if ($this->Admin) {
                $this->datatables->where("groups.name !=", "owner");
            }
            $this->datatables->add_column("Actions", '<div class="text-center">
                <div class="btn-group text-left">
                    <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                        <i class="fas fa-ellipsis-v fa-lg"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>
                            <a href="' . admin_url('auth/profile/$1') . '" class="tip"><i class="fa fa-edit"></i> '. lang("edit_user") .'</a>
                        </li>
                    </ul>
                </div>
            </div>', "id");
        } else {
            $this->datatables->add_column("Actions", "", "id");
        }

        if (!empty($optionFilter)) {
            $this->datatables->where("groups.id", $optionFilter);
        }

        echo $this->datatables->generate();
    }

    function getUserLogins($id = NULL)
    {
        if (!$this->ion_auth->in_group(array('owner', 'admin'))) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            admin_redirect('welcome');
        }
        $this->load->library('datatables');
        $this->datatables
            ->select("login, ip_address, time")
            ->from("user_logins")
            ->where('user_id', $id);

        echo $this->datatables->generate();
    }

    function delete_avatar($id = NULL, $avatar = NULL)
    {

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group('owner') && $id != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . $_SERVER["HTTP_REFERER"] . "'; }, 0);</script>");
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            unlink('assets/uploads/avatars/' . $avatar);
            unlink('assets/uploads/avatars/thumbs/' . $avatar);
            if ($id == $this->session->userdata('user_id')) {
                $this->session->unset_userdata('avatar');
            }
            $this->db->update('users', array('avatar' => NULL), array('id' => $id));
            $this->session->set_flashdata('message', lang("avatar_deleted"));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . $_SERVER["HTTP_REFERER"] . "'; }, 0);</script>");
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function profile($id = NULL)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group('owner') && $id != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'admin');
        }
        if (!$id || empty($id)) {
            admin_redirect('auth');
        }
        $this->data['title'] = lang('profile');
        $user = $this->ion_auth->user($id)->row();
        // $this->sma->print_arrays($user);
        $groups = $this->ion_auth->groups()->result_array();
        $this->data['csrf'] = $this->_get_csrf_nonce();
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['sellers'] = $this->site->getAllCompanies('seller');
        $this->data['users'] = $this->site->get_all_users();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['dashboards'] = $this->site->get_all_dashboards();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'class' => 'form-control',
            'type' => 'password',
            'value' => ''
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'class' => 'form-control',
            'type' => 'password',
            'value' => ''
        );
        $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
        $this->data['old_password'] = array(
            'name' => 'old',
            'id' => 'old',
            'class' => 'form-control',
            'type' => 'password',
        );
        $this->data['new_password'] = array(
            'name' => 'new',
            'id' => 'new',
            'type' => 'password',
            'class' => 'form-control',
            'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
        );
        $this->data['new_password_confirm'] = array(
            'name' => 'new_confirm',
            'id' => 'new_confirm',
            'type' => 'password',
            'class' => 'form-control',
            'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
        );
        $this->data['user_id'] = array(
            'name' => 'user_id',
            'id' => 'user_id',
            'type' => 'hidden',
            'value' => $user->id,
        );
        $this->data['id'] = $id;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('auth/users'), 'page' => lang('users')), array('link' => '#', 'page' => lang('profile')));
        $meta = array('page_title' => lang('profile'), 'bc' => $bc);
        $this->page_construct('auth/profile', $meta, $this->data);
    }

    public function captcha_check($cap)
    {
        $expiration = time() - 300; // 5 minutes limit
        $this->db->delete('captcha', array('captcha_time <' => $expiration));
        $this->db->select('COUNT(*) AS count')
            ->where('word', $cap)
            ->where('ip_address', $this->input->ip_address())
            ->where('captcha_time >', $expiration);
        if ($this->db->count_all_results('captcha')) {
            return true;
        } else {
            $this->form_validation->set_message('captcha_check', lang('captcha_wrong'));
            return FALSE;
        }
    }

    function login($m = NULL)
    {
        if ($this->loggedIn) {
            $this->session->set_flashdata('error', $this->session->flashdata('error'));
            admin_redirect('dashboard');
        }
        $this->data['title'] = lang('login');
        $from_shop = $this->input->post('from_shop');
        if ($this->Settings->captcha) {
            $this->form_validation->set_rules('captcha', lang('captcha'), 'required|callback_captcha_check');
        }
        if ($this->form_validation->run() == true) {
            $remember = (bool) $this->input->post('remember');
            $account_locked = $this->input->post('account_locked');
            $login_status = $this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember);
            if ($login_status['success']) {
 
                /******** registro de inicio de session en user activities *******************/
                $this->db->insert( 'user_activities', [
                                        'date' => date('Y-m-d H:i:s'),
                                        'type_id' => 7,
                                        'table_name' => 'users',
                                        'user_id' => $this->session->userdata('user_id'),
                                        'module_name' => 'auth',
                                        'description' => $this->session->first_name." ".$this->session->last_name." ingreso al sistema.",
                                ]);
                /******** registro de inicio de session en user activities *******************/
                
                $this->session->set_flashdata('message', lang($login_status['msg']));
                $this->session->set_userdata('account_locked', $account_locked);
                $this->session->set_userdata('clean_localstorage', 1);

                if ($this->Settings->mmode) {
                    if (!$this->ion_auth->in_group('owner')) {
                        $this->session->set_flashdata('error', lang('site_is_offline_plz_try_later'));
                        admin_redirect('auth/logout');
                    }
                }
                if ($this->ion_auth->in_group('customer') || $this->ion_auth->in_group('supplier')) {
                    if(file_exists(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'Shop.php')) {
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect(base_url());
                    } else {
                        admin_redirect('auth/logout/1');
                    }
                }
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $referrer = ($this->session->userdata('requested_page') && $this->session->userdata('requested_page') != 'admin') ? $this->session->userdata('requested_page') : 'dashboard';
                $referrer = str_replace("admin/", "", $referrer);
                admin_redirect($referrer);
            } else {
                $this->session->set_flashdata('error', lang($login_status['msg']));
                if (isset($login_status['link'])) {
                    $this->session->set_userdata('link_log_out', $login_status['link']);
                }
                if ($from_shop) {
                    redirect('main?login=1');
                } else {
                    admin_redirect('auth/login');
                }
            }
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['message'] = $this->session->flashdata('message');
            if ($this->Settings->captcha) {
                $this->load->helper('captcha');
                $vals = array(
                    'img_path' => './assets/captcha/',
                    'img_url' => base_url('assets/captcha/'),
                    'img_width' => 150,
                    'img_height' => 34,
                    'word_length' => 5,
                    'colors' => array('background' => array(255, 255, 255), 'border' => array(204, 204, 204), 'text' => array(102, 102, 102), 'grid' => array(204, 204, 204))
                );
                $cap = create_captcha($vals);
                $capdata = array(
                    'captcha_time' => $cap['time'],
                    'ip_address' => $this->input->ip_address(),
                    'word' => $cap['word']
                );

                $query = $this->db->insert_string('captcha', $capdata);
                $this->db->query($query);
                $this->data['image'] = $cap['image'];
                $this->data['captcha'] = array('name' => 'captcha',
                    'id' => 'captcha',
                    'type' => 'text',
                    'class' => 'form-control',
                    'required' => 'required',
                    'placeholder' => lang('type_captcha')
                );
            }
            $this->data['allow_reg'] = $this->Settings->allow_reg;
            if ($m == 'db') {
                $this->data['message'] = lang('db_restored');
            } elseif ($m) {
                $this->data['error'] = lang('we_are_sorry_as_this_sction_is_still_under_development.');
            }
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data["types_obligations"] = $this->site->get_types_obligations();
            // if ($this->Settings->close_year_step >= 1  || $this->Settings->close_year_step == 1) {
            //     redirect($this->site->url_origin().'/wappsi_consola');
            // } else {
                $this->load_view($this->theme . 'auth/login', $this->data);
            // }
        }
    }

    function reload_captcha()
    {
        $this->load->helper('captcha');
        $vals = array(
            'img_path' => './assets/captcha/',
            'img_url' => base_url('assets/captcha/'),
            'img_width' => 150,
            'img_height' => 34,
            'word_length' => 5,
            'colors' => array('background' => array(255, 255, 255), 'border' => array(204, 204, 204), 'text' => array(102, 102, 102), 'grid' => array(204, 204, 204))
        );
        $cap = create_captcha($vals);
        $capdata = array(
            'captcha_time' => $cap['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => $cap['word']
        );
        $query = $this->db->insert_string('captcha', $capdata);
        $this->db->query($query);
        //$this->data['image'] = $cap['image'];

        echo $cap['image'];
    }

    function logout($m = NULL)
    {

        $logout = $this->ion_auth->logout();
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        admin_redirect('login/' . $m);
    }

    function change_password()
    {
        if (!$this->ion_auth->logged_in()) {
            admin_redirect('login');
        }
        $this->form_validation->set_rules('old_password', lang('old_password'), 'required');
        $this->form_validation->set_rules('new_password', lang('new_password'), 'required|min_length[8]|max_length[25]');
        $this->form_validation->set_rules('new_password_confirm', lang('confirm_password'), 'required|matches[new_password]');

        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('auth/profile/' . $user->id . '/#cpassword');
        } else {
            if (DEMO) {
                $this->session->set_flashdata('warning', lang('disabled_in_demo'));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

            $change = $this->ion_auth->change_password($identity, $this->input->post('old_password'), $this->input->post('new_password'));

            if ($change) {
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->errors());
                admin_redirect('auth/profile/' . $user->id . '/#cpassword');
            }
        }
    }

    function forgot_password()
    {
        $this->form_validation->set_rules('forgot_email', lang('email_address'), 'required|valid_email');
        if ($this->form_validation->run() == false) {
            $error = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->session->set_flashdata('error', $error);
            admin_redirect("login#forgot_password");
        } else {
            $identity = $this->ion_auth->where('email', strtolower($this->input->post('forgot_email')))->users()->row();
            if (empty($identity)) {
                $this->ion_auth->set_message('forgot_password_email_not_found');
                $this->session->set_flashdata('error', $this->ion_auth->messages());
                admin_redirect("login#forgot_password");
            }
            $forgotten = $this->ion_auth->forgotten_password($identity->email);
            if ($forgotten) {
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                admin_redirect("login#forgot_password");
            } else {
                $this->session->set_flashdata('error', $this->ion_auth->errors());
                admin_redirect("login#forgot_password");
            }
        }
    }

    public function reset_password($code = NULL)
    {
        if (!$code) {
            show_404();
        }
        $user = $this->ion_auth->forgotten_password_check($code);
        if ($user) {
            $this->form_validation->set_rules('new', lang('password'), 'required|min_length[8]|max_length[25]|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', lang('confirm_password'), 'required');
            if ($this->form_validation->run() == false) {
                $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
                $this->data['message'] = $this->session->flashdata('message');
                $this->data['title'] = lang('reset_password');
                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'class' => 'form-control',
                    'pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
                    'data-bv-regexp-message' => lang('pasword_hint'),
                    'placeholder' => lang('new_password')
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'class' => 'form-control',
                    'data-bv-identical' => 'true',
                    'data-bv-identical-field' => 'new',
                    'data-bv-identical-message' => lang('pw_not_same'),
                    'placeholder' => lang('confirm_password')
                );
                $this->data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id,
                );
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['code'] = $code;
                $this->data['identity_label'] = $user->email;
                //render
                $this->load_view($this->theme . 'auth/reset_password', $this->data);
            } else {
                // do we have a valid request?
                if ($user->id != $this->input->post('user_id')) {
                    //something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);
                    show_error(lang('error_csrf'));
                } else {
                    // finally change the password
                    $identity = $user->email;
                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));
                    if ($change) {
                        //if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        //$this->logout();
                        admin_redirect('login');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->errors());
                        admin_redirect('auth/reset_password/' . $code);
                    }
                }
            }
        } else {
            //if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('error', $this->ion_auth->errors());
            admin_redirect("login#forgot_password");
        }
    }

    function activate($id, $code = false)
    {

        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } else if ($this->Owner) {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            if ($this->Owner) {
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                admin_redirect("auth/login");
            }
        } else {
            $this->session->set_flashdata('error', $this->ion_auth->errors());
            admin_redirect("forgot_password");
        }
    }

    function deactivate($id = NULL)
    {
        $this->sma->checkPermissions('users', TRUE);
        $id = $this->config->item('use_mongodb', 'ion_auth') ? (string)$id : (int)$id;
        $this->form_validation->set_rules('confirm', lang("confirm"), 'required');

        if ($this->form_validation->run() == FALSE) {
            if ($this->input->post('deactivate')) {
                $this->session->set_flashdata('error', validation_errors());
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['user'] = $this->ion_auth->user($id)->row();
                $this->data['modal_js'] = $this->site->modal_js();
                $this->load_view($this->theme . 'auth/deactivate_user', $this->data);
            }
        } else {

            if ($this->input->post('confirm') == 'yes') {
                if ($id != $this->input->post('id')) {
                    show_error(lang('error_csrf'));
                }

                if ($this->ion_auth->logged_in() && $this->Owner) {
                    $this->ion_auth->deactivate($id);
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                }
            }

            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function create_user()
    {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['title'] = "Create User";
        $this->form_validation->set_rules('username', lang("username"), 'trim|is_unique[users.username]');
        $this->form_validation->set_rules('email', lang("email"), 'trim|is_unique[users.email]');
        $this->form_validation->set_rules('status', lang("status"), 'trim|required');
        $this->form_validation->set_rules('group', lang("group"), 'trim|required');
        if ($this->input->post('relationedcompanies') == 1) {
            $this->form_validation->set_rules('company_id', lang("company"), 'trim|required');
        }
        if ($this->form_validation->run() == true) {
            // $this->sma->print_arrays($_POST);
            $username = strtolower($this->input->post('username'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $notify = $this->input->post('notify');
            $group_name = $this->input->post('group_name');
            $document_type_id = $this->input->post('doc_type_id') ? $this->input->post('doc_type_id') : NULL;
            $pos_doc_type_id = $this->input->post('pos_doc_type_id') ? $this->input->post('pos_doc_type_id') : NULL;
            $fe_pos_doc_type_id = $this->input->post('fe_pos_doc_type_id') ? $this->input->post('fe_pos_doc_type_id') : NULL;
            $sale_document_type_id = $this->input->post('sale_document_type_id') ? $this->input->post('sale_document_type_id') : NULL;
            $main_administrator = NULL;
            
            if ($this->input->post('group') == 2) {
                $main_administrator = $this->input->post('main_administrator') ? $this->input->post('main_administrator') : 0;
                if ($main_administrator == 1) {
                    $ma = $this->db->get_where('users', ['main_administrator' => 1, 'id !=' => $id]);
                    if ($ma->num_rows() > 0) {
                        $this->session->set_flashdata('error', lang('exists_another_main_administrator'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            }

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'group_id' => $this->input->post('group') ? $this->input->post('group') : '3',
                'biller_id' => $this->input->post('biller'),
                'warehouse_id' => $this->input->post('warehouse'),
                'view_right' => $this->input->post('view_right'),
                'edit_right' => $this->input->post('edit_right'),
                'allow_discount' => $this->input->post('allow_discount'),
                'company_id' => $this->input->post('company_id'),
                'seller_id' => $this->input->post('seller'),
                'document_type_id' => $document_type_id,
                'pos_document_type_id' => $pos_doc_type_id,
                'fe_pos_document_type_id' => $fe_pos_doc_type_id,
                'sale_document_type_id' => $sale_document_type_id,
                'dashboard_id' => $this->input->post('dashboard_id'),
                'print_pos_finalized_invoice' => $this->input->post('print_pos_finalized_invoice'),
                'register_cash_movements_with_another_user' => $this->input->post('register_cash_movements_with_another_user'),
                'cant_finish_pos_invoice' => $this->input->post('cant_finish_pos_invoice') ? 1 : 0 ,
                'user_pc_serial' => $this->input->post('user_pc_serial'),
                'main_administrator' => $this->input->post('main_administrator'),
            );
            $active = $this->input->post('status');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, $active, $notify, $group_name)) {
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            admin_redirect("auth/users");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $this->data['groups'] = $this->ion_auth->groups()->result_array();
            
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['sellers'] = $this->site->getAllCompanies('seller');
            $this->data['users'] = $this->site->get_all_users();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['dashboards'] = $this->site->get_all_dashboards();
            $bc = array(array('link' => admin_url('home'), 'page' => lang('home')), array('link' => admin_url('auth/users'), 'page' => lang('users')), array('link' => '#', 'page' => lang('create_user')));
            $meta = array('page_title' => lang('users'), 'bc' => $bc);
            $this->page_construct('auth/create_user', $meta, $this->data);
        }
    }

    function edit_user($id = NULL)
    {
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }
        $this->data['title'] = lang("edit_user");
        if (!$this->loggedIn || !$this->Owner && $id != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $user = $this->ion_auth->user($id)->row();
        if ($user->username != $this->input->post('username')) {
            $this->form_validation->set_rules('username', lang("username"), 'trim|is_unique[users.username]');
        }
        if ($user->email != $this->input->post('email')) {
            $this->form_validation->set_rules('email', lang("email"), 'trim|is_unique[users.email]');
        }
        if ($this->form_validation->run() === TRUE) {
            $group_name = $this->input->post('group_name');
            if ($this->Owner) {
                if ($id == $this->session->userdata('user_id')) {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'company' => $this->input->post('company'),
                        'phone' => $this->input->post('phone'),
                        'gender' => $this->input->post('gender'),
                        'user_pc_serial' => $this->input->post('user_pc_serial'),
                    );
                } elseif ($this->ion_auth->in_group('customer', $id) || $this->ion_auth->in_group('supplier', $id)) {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'company' => $this->input->post('company'),
                        'phone' => $this->input->post('phone'),
                        'gender' => $this->input->post('gender'),
                        'user_pc_serial' => $this->input->post('user_pc_serial'),
                        'active' => $this->input->post('status'),
                        'username' => $this->input->post('username'),
                    );
                } else {
                    if ($this->input->post('group') == 1 || $this->input->post('group') == 2) {
                        $biller_id = NULL;
                        $seller_id = NULL;
                        $document_type_id = NULL;
                        $warehouse_id = NULL;
                        $company_id = NULL;
                        $pos_doc_type_id = NULL;
                        $fe_pos_doc_type_id = NULL;
                        $sale_doc_type_id = NULL;
                    } else {
                        $company_id = $this->input->post('company_id') ? $this->input->post('company_id') : NULL;
                        $biller_id = $this->input->post('biller[]') ? $this->input->post('biller[]') : NULL;
                        $seller_id = $company_id ? NULL : ($this->input->post('seller') ? $this->input->post('seller') : NULL);
                        $document_type_id = $this->input->post('doc_type_id') ? $this->input->post('doc_type_id') : NULL;
                        $pos_doc_type_id = $this->input->post('pos_doc_type_id') ? $this->input->post('pos_doc_type_id') : NULL;
                        $fe_pos_doc_type_id = $this->input->post('fe_pos_doc_type_id') ? $this->input->post('fe_pos_doc_type_id') : NULL;
                        $sale_document_type_id = $this->input->post('sale_document_type_id') ? $this->input->post('sale_document_type_id') : NULL;
                        $warehouse_id = $this->input->post('warehouse') ? $this->input->post('warehouse') : NULL;
                    }
                    $main_administrator = NULL;
                    if ($this->input->post('group') == 2) {
                        $main_administrator = $this->input->post('main_administrator') ? $this->input->post('main_administrator') : 0;
                        if ($main_administrator) {
                            $ma = $this->db->get_where('users', ['main_administrator' => 1, 'id !=' => $id]);
                            if ($ma->num_rows() > 0) {
                                $this->session->set_flashdata('error', lang('exists_another_main_administrator'));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }
                    }

                        // exit(var_dump(json_encode($biller_id)));
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'company' => $this->input->post('company'),
                        'username' => $this->input->post('username'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone'),
                        'gender' => $this->input->post('gender'),
                        'active' => $this->input->post('status'),
                        'group_id' => $this->input->post('group'),
                        'award_points' => $this->input->post('award_points'),
                        'view_right' => $this->input->post('view_right'),
                        'edit_right' => $this->input->post('edit_right'),
                        'allow_discount' => $this->input->post('allow_discount'),
                        'biller_id' => json_encode($biller_id),
                        'seller_id' => $company_id ? NULL : $seller_id,
                        'document_type_id' => $document_type_id,
                        'pos_document_type_id' => $pos_doc_type_id,
                        'fe_pos_document_type_id' => $fe_pos_doc_type_id,
                        'sale_document_type_id' => $sale_document_type_id,
                        'warehouse_id' => $warehouse_id,
                        'company_id' => $seller_id ? NULL : $company_id,
                        'main_administrator' => $main_administrator,
                        'dashboard_id' => $this->input->post('dashboard_id'),
                        'print_pos_finalized_invoice' => $this->input->post('print_pos_finalized_invoice'),
                        'user_pc_serial' => $this->input->post('user_pc_serial'),
                        'language' => $this->input->post('language'),
                        'register_cash_movements_with_another_user' => $this->input->post('register_cash_movements_with_another_user'),
                        'cant_finish_pos_invoice' => $this->input->post('cant_finish_pos_invoice') ? 1 : 0,
                    );
                }
            } elseif ($this->Admin) {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                    'gender' => $this->input->post('gender'),
                    'active' => $this->input->post('status'),
                    'award_points' => $this->input->post('award_points'),
                        'company_id' => $this->input->post('company_id'),
                        'group_id' => $this->input->post('group'),
                        'user_pc_serial' => $this->input->post('user_pc_serial'),
                );
            } else {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                    'gender' => $this->input->post('gender'),
                    'user_pc_serial' => $this->input->post('user_pc_serial'),
                );
            }
            if ($this->Owner) {
                if ($this->input->post('password')) {
                    if (DEMO) {
                        $this->session->set_flashdata('warning', lang('disabled_in_demo'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $this->form_validation->set_rules('password', lang('edit_user_validation_password_label'), 'required|min_length[8]|max_length[25]|matches[password_confirm]');
                    $this->form_validation->set_rules('password_confirm', lang('edit_user_validation_password_confirm_label'), 'required');
                    $data['password'] = $this->input->post('password');
                }
            }
        }
        if ($this->form_validation->run() === TRUE && $this->ion_auth->update($user->id, $data, array(), $group_name)) {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 1,
                'table_name' => 'users',
                'record_id' => $user->id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name." editó el usuario",
            ]);
            $this->session->set_flashdata('message', lang('user_updated'));
            // admin_redirect("auth/profile/" . $id);
        } else {
            $this->session->set_flashdata('error', validation_errors());
            // redirect($_SERVER["HTTP_REFERER"]);
        }

        admin_redirect("auth/users");
    }

    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }
    function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')
        ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function _render_page($view, $data = null, $render = false)
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;
        $view_html = $this->load_view('header', $this->viewdata, $render);
        $view_html .= $this->load_view($view, $this->viewdata, $render);
        $view_html = $this->load_view('footer', $this->viewdata, $render);

        if (!$render)
            return $view_html;
    }

    /**
     * @param null $id
     */
    function update_avatar($id = NULL)
    {
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }

        if (!$this->ion_auth->logged_in() || !$this->Owner && $id != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //validate form input
        $this->form_validation->set_rules('avatar', lang("avatar"), 'trim');

        if ($this->form_validation->run() == true) {

            if ($_FILES['avatar']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/avatars';
                $config['allowed_types'] = 'gif|jpg|png';
                //$config['max_size'] = '500';
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('avatar')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $this->load->helper('file');
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/avatars/' . $photo;
                $config['new_image'] = 'assets/uploads/avatars/thumbs/' . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                $user = $this->ion_auth->user($id)->row();
            } else {
                $this->form_validation->set_rules('avatar', lang("avatar"), 'required');
            }
        }

        if ($this->form_validation->run() == true && $this->auth_model->updateAvatar($id, $photo)) {
            unlink('assets/uploads/avatars/' . $user->avatar);
            unlink('assets/uploads/avatars/thumbs/' . $user->avatar);
            $this->session->set_userdata('avatar', $photo);
            $this->session->set_flashdata('message', lang("avatar_updated"));
            admin_redirect("auth/profile/" . $id);
        } else {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("auth/profile/" . $id);
        }
    }
    //MÉTODO REGISTRO DE USUARIO
    function register()
    {
        $this->data['title'] = "Register";
        if (!$this->Settings->allow_reg) {
            $this->session->set_flashdata('error', lang('registration_is_disabled'));
            admin_redirect("login");
        }

        $this->form_validation->set_message('is_unique', lang('account_exists'));
        $this->form_validation->set_rules('email', lang('email_address'), 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('username', lang('username'), 'required|is_unique[users.username]');
        $this->form_validation->set_rules('password', lang('password'), 'required|min_length[8]|max_length[25]|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', lang('confirm_password'), 'required');
        if ($this->Settings->captcha) {
            $this->form_validation->set_rules('captcha', lang('captcha'), 'required|callback_captcha_check');
        }

        if ($this->form_validation->run() == true) {

            if ($this->input->post('type_person') == NATURAL_PERSON) {
                $nombreCompleto = $this->input->post('first_name')." ".$this->input->post('second_name')." ".$this->input->post('first_lastname')." ".$this->input->post('second_lastname');
            } else {
                $nombreCompleto = $this->input->post('name');
            }


            $data = array(
                'name' => $this->input->post('name'),
                'group_id' => '3',
                'group_name' => 'customer',
                'customer_group_id' => NULL,
                'customer_group_name' => NULL,
                'price_group_id' => NULL,
                'price_group_name' => NULL,
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $nombreCompleto,
                'country' => $this->input->post('country'),
                'email' => $this->input->post('type_register') == 1 ? $this->input->post('email') : $this->input->post('company_email'),
                'phone' => $this->input->post('type_register') == 1 ? $this->input->post('phone') : $this->input->post('company_phone'),
                'type_person' => $this->input->post('type_person'),
                'tipo_regimen' => $this->input->post('type_vat_regime'),
                'tipo_documento' => $this->input->post('tipo_documento'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code'),
                "location" => $this->input->post("location"),
                "document_code" => $this->input->post("document_code"),
                "commercial_register" => $this->input->post("commercial_register"),
                "birth_month" => $this->input->post("birth_month"),
                "birth_day" => $this->input->post("birth_day"),
                "customer_only_for_pos" => $this->input->post('type_register') == 1 ? 1 : 0,
                "location" => $this->input->post("zone"),
                "subzone" => $this->input->post("subzone"),
                "birth_month" => $this->input->post("birth_month"),
                "birth_day" => $this->input->post("birth_day"),
            );

            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $email = $this->input->post('email');

            $additional_data = array(
                'first_name' => $this->input->post('type_register') == 1 ? $this->input->post('first_name') : $this->input->post('user_first_name'),
                'last_name' => $this->input->post('type_register') == 1 ? $this->input->post('first_lastname') : $this->input->post('user_first_lastname'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'group_id' => $this->input->post('group') ? $this->input->post('group') : '3',
                'biller_id' => $this->input->post('biller'),
                'warehouse_id' => $this->input->post('warehouse'),
                'view_right' => 0,
                'edit_right' => 0,
                'allow_discount' => 1,
                'company_id' => $this->input->post('company_id'),
            );

        }
        if ($this->form_validation->run() == true && $saved_customer_id = $this->companies_model->addCompany($data))
        {
            if ($this->form_validation->run() == true && $user_id = $this->ion_auth->register($username, $password, $email, $additional_data)) {
                $type_customer_obligations = $this->input->post("types_obligations");
                $customer_obligations_array = [];
                if ($type_customer_obligations) {
                  foreach ($type_customer_obligations as $customer_obligation)
                  {
                    $customer_obligations_array[] = [
                      "customer_id" => $saved_customer_id,
                      "types_obligations_id" => $customer_obligation,
                      "relation" => ACQUIRER
                    ];
                  }
                }
                if (count($customer_obligations_array) > 0) {
                    $this->companies_model->insert_type_customer_obligations($customer_obligations_array);
                }
                $this->ion_auth->set_user_company_id($user_id, $saved_customer_id);
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("shop");
            }
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['groups'] = $this->ion_auth->groups()->result_array();

            $this->load->helper('captcha');
            $vals = array(
                'img_path' => './assets/captcha/',
                'img_url' => admin_url() . 'assets/captcha/',
                'img_width' => 150,
                'img_height' => 34,
            );
            $cap = create_captcha($vals);
            $capdata = array(
                'captcha_time' => $cap['time'],
                'ip_address' => $this->input->ip_address(),
                'word' => $cap['word']
            );

            $query = $this->db->insert_string('captcha', $capdata);
            $this->db->query($query);
            $this->data['image'] = $cap['image'];
            $this->data['captcha'] = array('name' => 'captcha',
                'id' => 'captcha',
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => lang('type_captcha')
            );

            $this->load_view($this->theme . 'auth/login', $this->data);
        }
    }

    function user_actions()
    {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        if ($id != $this->session->userdata('user_id')) {
                            $this->auth_model->delete_user($id);
                        }
                    }
                    $this->session->set_flashdata('message', lang("users_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('first_name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('last_name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('group'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $user = $this->site->getUser($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $user->first_name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $user->last_name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $user->email);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $user->company);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $user->group_name);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $user->active);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'users_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_user_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function delete($id = NULL)
    {
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->input->get('id')) { $id = $this->input->get('id'); }

        if ( ! $this->Owner || $id == $this->session->userdata('user_id')) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'admin/welcome');
        }

        if ($this->auth_model->delete_user($id)) {
            //echo lang("user_deleted");
            $this->session->set_flashdata('message', 'user_deleted');
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function get_companies(){
        $group_name = $this->input->get('group_name');
        $selected = $this->input->get('selected');

        $html = false;

        if ($group_name) {
            $companies = $this->site->getAllCompanies($group_name);
            if ($companies) {
                $html.= '<option value="">'.lang('select').'</option>';
                foreach ($companies as $company) {
                    $html .= '<option value="'.$company->id.'" '.($selected && $company->id == $selected ? 'selected="selected"' : '').'> '.$company->vat_no.' - '.$company->name.'</option>';
                }
            }
        }

        echo $html;


    }

    function get_states($country, $cur_state = null, $response = null){
        $cur_state = urldecode($cur_state);
        $states = $this->companies_model->getStatesByCountry($country);
        if ($response === null) {
            $options = '<option value="">Seleccione...</option>';
            foreach ($states as $row => $state) {
                if ($cur_state !== null && $cur_state == $state->DEPARTAMENTO) {
                    $selected =' selected="selected"';
                } else {
                    $selected = "";
                }

                $options.='<option value="'.$state->DEPARTAMENTO.'" data-code="'.$state->CODDEPARTAMENTO.'"'.$selected.'>'.$state->DEPARTAMENTO.'</option>';
            }
            echo $options;
        } else {
            foreach ($states as $row => $state) {
                $options[$state->DEPARTAMENTO] = $state->DEPARTAMENTO;
            }
            return $option;
        }
    }

    function get_cities($state, $cur_city = null, $response = null){
        $cur_city = urldecode($cur_city);
        $cities = $this->companies_model->getCitiesByState($state);
        if ($response === null) {
            $options = '<option value="">Seleccione...</option>';
            foreach ($cities as $row => $city) {

                if ($cur_city !== null && $cur_city == $city->DESCRIPCION) {
                    $selected =' selected="selected"';
                } else {
                    $selected = "";
                }

                $options.='<option value="'.$city->DESCRIPCION.'" data-code="'.$city->CODIGO.'"'.$selected.'>'.$city->DESCRIPCION.'</option>';
            }
            echo $options;
        } else {
            foreach ($cities as $row => $city) {
                $options[$city->DESCRIPCION] = $city->DESCRIPCION;
            }
            return $option;
        }
    }

    function get_zones($city, $cur_zone = null, $response = null){
        $zones = $this->companies_model->getZonesByCity($city);
        if ($response === null) {
            $options = '<option value="">Seleccione...</option>';
            if ($zones) {
                foreach ($zones as $row => $zone) {

                    if ($cur_zone !== null && $cur_zone == $zone->zone_name) {
                        $selected =' selected="selected"';
                    } else {
                        $selected = "";
                    }

                    $options.='<option value="'.$zone->zone_name.'" data-code="'.$zone->zone_code.'"'.$selected.'>'.$zone->zone_name.'</option>';
                }
            }
            echo $options;
        } else {
            foreach ($zones as $row => $zone) {
                $options[$zone->zone_name] = $zone->zone_name;
            }
            return $option;
        }
    }

    function get_subzones($zone, $cur_subzone = null, $response = null){
        $subzones = $this->companies_model->getSubZonesByZones($zone);
        if ($response === null) {
            $options = '<option value="">Seleccione...</option>';
            if ($subzones) {
                foreach ($subzones as $row => $subzone) {
                    if ($cur_subzone !== null && $cur_subzone == $subzone->subzone_name) {
                        $selected =' selected="selected"';
                    } else {
                        $selected = "";
                    }
                    $options.='<option value="'.$subzone->subzone_name.'" data-code="'.$subzone->subzone_code.'"'.$selected.'>'.$subzone->subzone_name.'</option>';
                }
            }
            echo $options;
        } else {
            foreach ($subzones as $row => $subzone) {
                $options[$subzone->subzone_name] = $subzone->subzone_name;
            }
            return $option;
        }
    }

    function verify_user_exists(){
        $username = urldecode($this->input->get('username'));
        $email = urldecode($this->input->get('email'));
        echo $this->ion_auth->verify_user_exists($username, $email);
    }

    function log_out_user($user_id = NULL)
    {
        $this->sma->checkPermissions('users', TRUE);
        $this->form_validation->set_rules('confirm', lang("confirm"), 'required');
        if ($this->form_validation->run() == true && $this->ion_auth->update_log_out_order($user_id)) {
            $this->session->set_flashdata('message', lang('log_out_order_updated'));
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['user_id'] = $user_id;
            $this->load_view($this->theme . 'auth/log_out_user', $this->data);
        }
    }

    function log_out_session($user_id = NULL)
    {
        if ($this->db->update('users', ['login_status' => 0, 'login_hour' => NULL, 'login_code' => NULL], ['id' => $user_id])) {
            $lo_user = $this->site->getUserById($user_id);
            $this->db->insert( 'user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 7,
                    'table_name' => 'users',
                    'user_id' => $user_id,
                    'module_name' => 'auth',
                    'description' => $lo_user->first_name." ".$lo_user->last_name." se cerró sesión desde logueo para nuevo ingreso.",
            ]);
            $this->session->set_flashdata('message', lang('log_out_order_updated'));
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['user_id'] = $user_id;
            $this->load_view($this->theme . 'auth/log_out_user', $this->data);
        }
    }

    function validate_user_email($email){
        if (!empty($email)) {
            $email = str_replace("999666", "@", $email);
            if ($this->Settings->interconect_login == 1 && $this->erp_db) {
                $q = $this->erp_db->get_where('users', ['email'=>$email]);
            } else {
                $q = $this->db->get_where('users', ['email'=>$email]);
            }
            if ($q->num_rows()>0) {
                echo json_encode(['error'=>0, 'exists'=>1]);
                exit();
            } else {
                echo json_encode(['error'=>0, 'exists'=>0]);
                exit();
            }
        }
        echo json_encode(['error'=>1, 'exists'=>0]);
        exit();
    }

    function validate_username($email){
        if (!empty($email)) {
            if ($this->Settings->interconect_login == 1 && $this->erp_db) {
                $q = $this->erp_db->get_where('users', ['username'=>$email]);
            } else {
                $q = $this->db->get_where('users', ['username'=>$email]);
            }
            if ($q->num_rows()>0) {
                echo json_encode(['error'=>0, 'exists'=>1]);
                exit();
            } else {
                echo json_encode(['error'=>0, 'exists'=>0]);
                exit();
            }
        }
        echo json_encode(['error'=>1, 'exists'=>0]);
        exit();
    }

    function interconnect_login(){
        $interconect_password = $this->input->get('interconect_password');
        $user_data = json_decode($this->input->get('user_data'));
        if ($interconect_password == sha1($this->Settings->interconect_password)) {
            $random_login_code = rand(1000000, 9999999);
            $user_data->login_code = $random_login_code;
            $user_current_data = $this->db->get_where('users', ['email'=>$user_data->email])->row();
            $user_data->id = $user_current_data->id;
            $user_group = $this->settings_model->getGroupByID($user_data->group_id);
            $user_data->group_name = $user_group->name;
            $this->auth_model->set_session($user_data);
            $this->auth_model->update_last_login($user_data->id, $random_login_code);
            echo json_encode(['error'=>0]);
        } else {
            echo json_encode(['error'=>1]);
        }
    }

    function interconect_forgot_password()
    {
        $identity = $this->ion_auth->where('email', strtolower($this->input->post('email_forgotten_password')))->users()->row();
        if (empty($identity)) {
            echo json_encode(['error'=>0, 'message'=>'Usuario Incorrecto']);
            exit();
        }
        $forgotten = $this->ion_auth->forgotten_password($identity->email);
        if ($forgotten) {
            echo json_encode(['error'=>0, 'message'=>'Correcto']);
            exit();
        } else {
            echo json_encode(['error'=>1, 'message'=>'Incorrecto']);
            exit();
        }
    }
}
