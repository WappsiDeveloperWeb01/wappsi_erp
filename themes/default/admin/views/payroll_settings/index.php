<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $_post = $this->session->flashdata("post") ? $this->session->flashdata("post") : ""; ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row" id="sortable-view">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <?= admin_form_open_multipart("payroll_settings/save", ["id" => "payroll_form"]); ?>
                        <?= form_hidden('payroll_settings_id', (!empty($payroll_settings->id)) ? $payroll_settings->id : ""); ?>
                        <h1><?= lang("payroll_general_adjustments") ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("work_environment"), "work_environment"); ?>
                                        <?php $wsopts=[""=>lang("select"), PRODUCTION=>lang("production"), TEST=>lang("tests")]; ?>
                                        <?php $work_environment = isset($_post) && !empty($_post) ? $_post->work_environment : (!empty($payroll_settings->work_environment) ? $payroll_settings->work_environment : ""); ?>
                                        <?= form_dropdown(["name"=>"work_environment", "id"=>"work_environment", "class"=>"form-control not_select", "required"=>TRUE], $wsopts, $work_environment); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("payment_frequency"), "payment_frequency"); ?>
                                        <?php
                                            $popts = [""=>lang("select")];
                                            foreach ($payment_frequencies as $payment_frequency) {
                                                if ($payment_frequency->id != WEEKLY) {
                                                    $popts[$payment_frequency->id]=$payment_frequency->period;
                                                }
                                            }
                                        ?>
                                        <?php $payment_frequency = isset($_post) && !empty($_post) ? $_post->payment_frequency : (!empty($payroll_settings->payment_frequency) ? $payroll_settings->payment_frequency : ""); ?>
                                        <?= form_dropdown(["name"=>"payment_frequency", "id"=>"payment_frequency", "class"=>"form-control not_select", "required"=>TRUE], $popts, $payment_frequency); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("payment_schedule"), "payment_schedule"); ?>
                                        <?php $psopts=[""=>lang("select"), 1=>"Meses de 30 días"]; ?>
                                        <?php $payment_schedule = isset($_post) && !empty($_post) ? $_post->payment_schedule : (!empty($payroll_settings->payment_schedule) ? $payroll_settings->payment_schedule : ""); ?>
                                        <?= form_dropdown(["name"=>"payment_schedule", "id"=>"payment_schedule", "class"=>"form-control not_select", "required"=>TRUE], $psopts, $payment_schedule); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("weekly_working_hours"), "weekly_working_hours"); ?>
                                            <?php $wwhopts = [""=>lang("select"), 48=>"hasta 2022 (48 horas)", 47=>"2023 (47 horas)", 46=>"2024 (46 horas)", 44=>"2025 (44 horas)", 42=>"2026 (42 horas)"]; ?>
                                            <?php $weekly_working_hours = isset($_post) && !empty($_post) ? $_post->weekly_working_hours : (!empty($payroll_settings->weekly_working_hours) ? $payroll_settings->weekly_working_hours : ""); ?>
                                            <?= form_dropdown(["name"=>"weekly_working_hours", "id"=>"weekly_working_hours", "class"=>"form-control not_select", "required"=>TRUE], $wwhopts, $weekly_working_hours); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("minimum_salary_value"), "minimum_salary_value"); ?>
                                        <?php $minimum_salary_value = isset($_post) && !empty($_post) ? $_post->minimum_salary_value : (!empty($payroll_settings->minimum_salary_value) ? $payroll_settings->minimum_salary_value : ""); ?>
                                        <?= form_input(["name"=>"minimum_salary_value", "id"=>"minimum_salary_value", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], $minimum_salary_value); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("integral_salary_value"), "integral_salary_value"); ?>
                                        <?php $integral_salary_value = isset($_post) && !empty($_post) ? $_post->integral_salary_value : (!empty($payroll_settings->integral_salary_value) ? $payroll_settings->integral_salary_value : ""); ?>
                                        <?= form_input(["name"=>"integral_salary_value", "id"=>"integral_salary_value", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], $integral_salary_value); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("transportation_allowance_value"), "transportation_allowance_value"); ?>
                                        <?php $transportation_allowance_value = isset($_post) && !empty($_post) ? $_post->transportation_allowance_value : (!empty($payroll_settings->transportation_allowance_value) ? $payroll_settings->transportation_allowance_value : ""); ?>
                                        <?= form_input(["name"=>"transportation_allowance_value", "id"=>"transportation_allowance_value", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], $transportation_allowance_value); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("UVT_value"), "UVT_value"); ?>
                                        <?php $UVT_value = isset($_post) && !empty($_post) ? $_post->UVT_value : (!empty($payroll_settings->UVT_value) ? $payroll_settings->UVT_value : ""); ?>
                                        <?= form_input(["name"=>"UVT_value", "id"=>"UVT_value", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE, "readonly"=>TRUE], $UVT_value); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("withholding_base_value"), "withholding_base_value"); ?>
                                        <?php $withholding_base_value = isset($_post) && !empty($_post) ? $_post->withholding_base_value : (!empty($payroll_settings->withholding_base_value) ? $payroll_settings->withholding_base_value : ""); ?>
                                        <?= form_input(["name"=>"withholding_base_value", "id"=>"withholding_base_value", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], $withholding_base_value); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("prima_payment"), "prima_payment"); ?>
                                        <?php $psopts=[1=>lang("payroll_prima_payment_option_1"), 2=>lang("payroll_prima_payment_option_2")]; ?>
                                        <?php $prima_payment = isset($_post) && !empty($_post) ? $_post->prima_payment : (!empty($payroll_settings->prima_payment) ? $payroll_settings->prima_payment : ""); ?>
                                        <?= form_dropdown(["name"=>"prima_payment", "id"=>"prima_payment", "class"=>"form-control not_select"], $psopts, $prima_payment); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("bonus_payment"), "bonus_payment"); ?>
                                        <?php
                                            $bopts = [""=>lang("select")];
                                            if (!empty($options)) {
                                                foreach ($options as $key => $option) {
                                                    $bopts[$key] = $option;
                                                }
                                            }
                                        ?>
                                        <?php $bonus_payment = isset($_post) && !empty($_post) ? $_post->bonus_payment : (!empty($payroll_settings->bonus_payment) ? $payroll_settings->bonus_payment : ""); ?>
                                        <?= form_dropdown(["name"=>"bonus_payment", "id"=>"bonus_payment", "class"=>"form-control not_select", "required"=>TRUE], $bopts, $bonus_payment); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("enable_saturday_vacation"), "enable_saturday_vacation"); ?>
                                        <?php $ssopts=[NOT=>lang("no"), YES=>lang("yes")]; ?>
                                        <?php $enable_saturday_vacation = isset($_post) && !empty($_post) ? $_post->enable_saturday_vacation : (!empty($payroll_settings->enable_saturday_vacation) ? $payroll_settings->enable_saturday_vacation : ""); ?>
                                        <?= form_dropdown(["name"=>"enable_saturday_vacation", "id"=>"enable_saturday_vacation", "class"=>"form-control not_select", "required"=>TRUE], $ssopts, $enable_saturday_vacation); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("bonus_limit_percentage"), "bonus_limit_percentage"); ?>
                                        <?php $bonus_limit_percentage = isset($_post) && !empty($_post) ? $_post->bonus_limit_percentage : (!empty($payroll_settings->bonus_limit_percentage) ? $payroll_settings->bonus_limit_percentage : ""); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"bonus_limit_percentage", "id"=>"bonus_limit_percentage", "class"=>"form-control"], $bonus_limit_percentage); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3" id="set_test_id_container">
                                    <div class="form-group">
                                        <?= form_label(lang("set_test_id"), "set_test_id"); ?>
                                        <?php $set_test_id = isset($_post) && !empty($_post) ? $_post->set_test_id : (!empty($payroll_settings->set_test_id) ? $payroll_settings->set_test_id : ""); ?>
                                        <?= form_input(["name"=>"set_test_id", "id"=>"set_test_id", "class"=>"form-control"], $set_test_id); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3" id="_container">
                                    <div class="form-group">
                                        <?= form_label(lang("percentage_salary_apprentice_teaching"), "percentage_salary_apprentice_teaching"); ?>
                                        <?php $percentage_salary_apprentice_teaching = isset($_post) && !empty($_post) ? $_post->percentage_salary_apprentice_teaching : (!empty($payroll_settings->percentage_salary_apprentice_teaching) ? $payroll_settings->percentage_salary_apprentice_teaching : ""); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"percentage_salary_apprentice_teaching", "id"=>"percentage_salary_apprentice_teaching", "type"=>"number", "class"=>"form-control", "min"=>50, 'required'=>TRUE], $percentage_salary_apprentice_teaching); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3" id="_container">
                                    <div class="form-group">
                                        <?= form_label(lang("percentage_salary_apprentice_productive"), "percentage_salary_apprentice_productive"); ?>
                                        <?php $percentage_salary_apprentice_productive = isset($_post) && !empty($_post) ? $_post->percentage_salary_apprentice_productive : (!empty($payroll_settings->percentage_salary_apprentice_productive) ? $payroll_settings->percentage_salary_apprentice_productive : ""); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"percentage_salary_apprentice_productive", "id"=>"percentage_salary_apprentice_productive","type"=>"number", "class"=>"form-control", "min"=>75, 'required'=>TRUE], $percentage_salary_apprentice_productive); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="sunday_discount_for_absenteeism"><?= $this->lang->line("sunday_discount_for_absenteeism") ?></label>
                                        <?php $selectedDSda = (isset($payroll_settings) && !empty($payroll_settings->sunday_discount_for_absenteeism)) ? $payroll_settings->sunday_discount_for_absenteeism : '' ?>
                                        <select class="form-control select" name="sunday_discount_for_absenteeism" id="sunday_discount_for_absenteeism">
                                            <option value="0" <?= ($selectedDSda == 0) ? 'selected': '' ?>>No</option>
                                            <option value="1" <?= ($selectedDSda == 1) ? 'selected': '' ?>>Sí</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <?= form_label(lang("layoffs_interests"), "layoffs_interests") ?>
                                    <?php
                                        $layoffsInterests = isset($_post) && !empty($_post) ? $_post->layoffs_interests : (!empty($payroll_settings->layoffs_interests) ? $payroll_settings->layoffs_interests : "");
                                        $loOpts = [
                                            12 =>  "12 %",
                                            1  =>  "1 %"
                                        ];
                                    ?>
                                    <?= form_dropdown(["name"=>"layoffs_interests", "id"=>"layoffs_interests", "class"=>"form-control"], $loOpts, $layoffsInterests) ?>
                                </div>
                                <?php if ($this->Owner) { ?>
                                    <div class="col-sm-3">
                                        <?= form_label(lang("payroll_start_date"), "payroll_start_date") ?>
                                        <?php $payrollStartDate = (isset($payroll_settings) && !empty($payroll_settings->payroll_start_date)) ? $payroll_settings->payroll_start_date : date("Y-m-d") ?>
                                        <?= form_input(["name"=>"payroll_start_date", "id"=>"payroll_start_date", "class"=>"form-control", "type"=>"date"], $payrollStartDate) ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </section>

                        <h1><?= lang("payroll_grants_exemptions") ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("arl"), "arl"); ?>
                                        <?php
                                            $aopts = [""=>lang("select")];
                                            if (!empty($arls)) {
                                                foreach ($arls as $arl) {
                                                    $aopts[$arl->id]=$arl->company;
                                                }
                                            }
                                        ?>
                                        <?php $arl = isset($_post) && !empty($_post) ? $_post->arl : (!empty($payroll_settings->arl) ? $payroll_settings->arl : ""); ?>
                                        <?= form_dropdown(["name"=>"arl", "id"=>"arl", "class"=>"form-control not_select", "required"=>TRUE], $aopts, $arl); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("ccf"), "ccf"); ?>
                                        <?php
                                            $copts = [""=>lang("select")];
                                            if (!empty($ccfs)) {
                                                foreach ($ccfs as $ccf) {
                                                    $copts[$ccf->id]=$ccf->company;
                                                }
                                            }
                                        ?>
                                        <?php $ccf = isset($_post) && !empty($_post) ? $_post->ccf : (!empty($payroll_settings->ccf) ? $payroll_settings->ccf : ""); ?>
                                        <?= form_dropdown(["name"=>"ccf", "id"=>"ccf", "class"=>"form-control not_select", "required"=>TRUE], $copts, $ccf); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang('compensation_fund_percentage'), 'compensation_fund_percentage'); ?>
                                        <?php $compensation_fund_percentage = isset($_post) && !empty($_post) ? $_post->compensation_fund_percentage : (!empty($payroll_settings->compensation_fund_percentage) ? $payroll_settings->compensation_fund_percentage : 0.00); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"compensation_fund_percentage", "id"=>"compensation_fund_percentage", "type"=>"number", "class"=>"form-control", "required"=>TRUE], $compensation_fund_percentage); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang('pension_percentage'), 'pension_percentage'); ?>
                                        <?php $pension_percentage = isset($_post) && !empty($_post) ? $_post->pension_percentage : (!empty($payroll_settings->pension_percentage) ? $payroll_settings->pension_percentage : 0.00); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"pension_percentage", "id"=>"pension_percentage", "type"=>"number", "class"=>"form-control", "required"=>TRUE], $pension_percentage); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang('high_risk_pension_percentage'), 'high_risk_pension_percentage'); ?>
                                        <?php $high_risk_pension_percentage = isset($_post) && !empty($_post) ? $_post->high_risk_pension_percentage : (!empty($payroll_settings->high_risk_pension_percentage) ? $payroll_settings->high_risk_pension_percentage : 0.00); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"high_risk_pension_percentage", "id"=>"high_risk_pension_percentage", "type"=>"number", "class"=>"form-control", "required"=>TRUE], $high_risk_pension_percentage); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("illness_2days"), "illness_2days"); ?>
                                        <?php $illness_2days = isset($_post) && !empty($_post) ? $_post->illness_2days : (!empty($payroll_settings->illness_2days) ? $payroll_settings->illness_2days : ""); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"illness_2days", "id"=>"illness_2days", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], $illness_2days); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("illness_90days"), "illness_90days"); ?>
                                        <?php $illness_90days = isset($_post) && !empty($_post) ? $_post->illness_90days : (!empty($payroll_settings->illness_90days) ? $payroll_settings->illness_90days : ""); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"illness_90days", "id"=>"illness_90days", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], $illness_90days); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("illness_91"), "illness_91"); ?>
                                        <?php $illness_91 = isset($_post) && !empty($_post) ? $_post->illness_91 : (!empty($payroll_settings->illness_91) ? $payroll_settings->illness_91 : ""); ?>
                                        <div class="input-group">
                                            <?= form_input(["name"=>"illness_91", "id"=>"illness_91", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], $illness_91); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("exempt_parafiscal_health_payments"), "exempt_parafiscal_health_payments"); ?>
                                        <?php $eopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                        <?php $exempt_parafiscal_health_payments = isset($_post) && !empty($_post) ? $_post->exempt_parafiscal_health_payments : (!empty($payroll_settings->exempt_parafiscal_health_payments) ? $payroll_settings->exempt_parafiscal_health_payments : ""); ?>
                                        <?= form_dropdown(["name"=>"exempt_parafiscal_health_payments", "id"=>"exempt_parafiscal_health_payments", "class"=>"form-control not_select", "required"=>TRUE], $eopts, $exempt_parafiscal_health_payments); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("small_business"), "small_business"); ?>
                                        <?php $sopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                        <?php $small_business = isset($_post) && !empty($_post) ? $_post->small_business : (!empty($payroll_settings->small_business) ? $payroll_settings->small_business : ""); ?>
                                        <?= form_dropdown(["name"=>"small_business", "id"=>"small_business", "class"=>"form-control not_select", "required"=>TRUE], $sopts, $small_business); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("exempt_concepts"), "exempt_concepts"); ?>
                                        <?php $sopts = [NOT=>lang("no"), YES=>lang("yes")]; ?>
                                        <?php $exempt_concepts = isset($_post) && !empty($_post) ? $_post->exempt_concepts : (!empty($payroll_settings->exempt_concepts) ? $payroll_settings->exempt_concepts : ""); ?>
                                        <?= form_dropdown(["name"=>"exempt_concepts", "id"=>"exempt_concepts", "class"=>"form-control not_select", "required"=>TRUE], $sopts, $exempt_concepts); ?>
                                        <h4><small>(Bonificaciones, comisiones, horas extras)</small></h4>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("provision_options"), "provision_options"); ?>
                                        <?php $opsopts = [1 => lang("do_not_provision"), 2 => lang("provision_without_reporting_to_DIAN"), 3 => lang("provision_and_report_to_DIAN")]; ?> 
                                        <?php $provision_options = (!empty($payroll_settings->provision_options) ? $payroll_settings->provision_options : ""); ?>  
                                        <?= form_dropdown(["name"=>"provision_options", "id"=>"provision_options", "class"=>"form-control not_select", "required"=>TRUE], $opsopts, $provision_options); ?>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h1><?= lang("payroll_bank_data") ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("bank_id"), "bank_id"); ?>
                                        <?php
                                            $bopts = [""=>lang("select")];
                                            if (!empty($banks)) {
                                                foreach ($banks as $bank) {
                                                    $bopts[$bank->id]=$bank->company;
                                                }
                                            }
                                        ?>
                                        <?php $bank_id = isset($_post) && !empty($_post) ? $_post->bank_id : (!empty($payroll_settings->bank_id) ? $payroll_settings->bank_id : ""); ?>
                                        <?= form_dropdown(["name"=>"bank_id", "id"=>"bank_id", "class"=>"form-control not_select", "required"=>TRUE], $bopts, $bank_id); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("bank_account_number"), "bank_account_number"); ?>
                                        <?php $bank_account_number = isset($_post) && !empty($_post) ? $_post->bank_account_number : (!empty($payroll_settings->bank_account_number) ? $payroll_settings->bank_account_number : ""); ?>
                                        <?= form_input(["name"=>"bank_account_number", "id"=>"bank_account_number", "type"=>"number", "class"=>"form-control", "required"=>TRUE], $bank_account_number); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("bank_account_type"), "bank_account_type"); ?>
                                        <?php $atopts = [""=>lang("select"), 1=>lang("payroll_savings_account"), 2=>lang("payroll_current_account"), 3=>lang("payroll_account_virtual_wallet")]; ?>
                                        <?php $bank_account_type = isset($_post) && !empty($_post) ? $_post->bank_account_type : (!empty($payroll_settings->bank_account_type) ? $payroll_settings->bank_account_type : ""); ?>
                                        <?= form_dropdown(["name"=>"bank_account_type", "id"=>"bank_account_type", "class"=>"form-control not_select", "required"=>TRUE], $atopts, $bank_account_type); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("file_format"), "file_format"); ?>
                                        <?php $fopts = [""=>lang("select"), 1=>lang("SAP"), 2=>lang("PAB")]; ?>
                                        <?php $file_format = isset($_post) && !empty($_post) ? $_post->file_format : (!empty($payroll_settings->file_format) ? $payroll_settings->file_format : ""); ?>
                                        <?= form_dropdown(["name"=>"file_format", "id"=>"file_format", "class"=>"form-control not_select", "required"=>TRUE], $fopts, $file_format); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("ss_operators"), "ss_operators"); ?>
                                        <?php
                                            $ssopts = [""=>lang("select")];
                                            foreach ($ss_operators as $ss_operator) {
                                                $ssopts[$ss_operator->id]=$ss_operator->name;
                                            }
                                        ?>
                                        <?php $ss_operators = isset($_post) && !empty($_post) ? $_post->ss_operators : (!empty($payroll_settings->ss_operators) ? $payroll_settings->ss_operators : ""); ?>
                                        <?= form_dropdown(["name"=>"ss_operators", "id"=>"ss_operators", "class"=>"form-control not_select", "required"=>TRUE], $ssopts, $ss_operators); ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        load_wizard_configuration();

        $('[data-toggle="tooltip"]').tooltip();
        <?php if ($approved_payroll_number > 0) { ?>
            $('#payment_frequency').attr('readonly', true);
            $('#bonus_payment').attr('readonly', true);
        <?php } ?>

        <?php if ($payroll_in_preparation_number > 0) { ?>
            $('#minimum_salary_value').attr('readonly', true);
            $('#integral_salary_value').attr('readonly', true);
            $('#transportation_allowance_value').attr('readonly', true);
            $('#withholding_base_value').attr('readonly', true);
            $('#generate_payroll_per_branch').attr('readonly', true);
            $('#weekly_working_hours').attr('readonly', true);
        <?php } ?>

        $(document).on('change', '#small_business', function() { enable_percentage_contributions(); });
        $(document).on('change', '#payment_frequency', function() { confirm_deletion_payroll($(this).val()); });
        $(document).on('change', '#work_environment', function() { enable_disable_set_test_id($(this).val()); });

        $('#small_business').trigger('change');
        $('#work_environment').trigger('change');
    });

    function load_wizard_configuration()
    {
        var form = $("#payroll_form");
        wizard = form.steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
             labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: '<i class="fa-solid fa-check" data-toggle="tooltip" title="Guardar" data-placement="top"></i>',
                next: '<i class="fa-solid fa-angles-right" data-toggle="tooltip" title="Siguiente" data-placement="top"></i>',
                previous: '<i class="fa-solid fa-angles-left"  data-toggle="tooltip" title="Anterior" data-placement="top"></i>',
                loading: "Loading ..."
            },
            onInit: function(event, currentIndex)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate({
                    focusCleanup: true,
                    highlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        console.log(elem[0].tagName);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                        } else {
                            elem.addClass(errorClass);
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                        } else {
                            elem.removeClass(errorClass);
                        }
                    }
                }).settings.ignore = ":disabled,:hidden:not(.validate)";
                return form.valid();
            },
            onStepChanged: function(event)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                if(form.valid()){
                    $(form).submit();
                }
            }
        });

        $(document).on("select2-opening", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror")) {
                $(".select2-drop ul").addClass("mierror");
            } else {
                $(".select2-drop ul").removeClass("mierror");
            }
        });

        $(document).on("select2-close", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror") && $("#" + elem.attr("id")).val() != '') {
                $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');

                $("#" + elem.attr("id") + "-error").css('display', 'none');
            }
        });
    }

    function enable_percentage_contributions()
    {
        var small_business = $("#small_business").val();

        if (small_business == '<?= YES; ?>') {
            $("#percentage_contributions").prop('disabled', false);
        } else {
            $("#percentage_contributions").select2('val', '');
            $("#percentage_contributions").prop('disabled', true);
        }
    }

    function confirm_deletion_payroll(frequency_id)
    {
        $.ajax({
            url: '<?= admin_url('payroll_settings/validate_existing_current_payroll_ajax') ?>',
            type: 'GET',
            dataType: 'JSON'
        })
        .done(function(data) {
            if (data != false) {
                swal({
                    title: "¿Seguro de cambiar la frecuencia de pago?",
                    text: "Esto proceso eliminará la nómina actual",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: "No",
                    confirmButtonText: "¡Si, cambiarlo!",
                    closeOnConfirm: false
                }, function (dismiss) {
                    if (dismiss == true) {
                        delete_current_payroll(data);
                    } else {
                        $('#payment_frequency').select2('val', '<?= $payment_frequency ?>');
                    }
                });
            } else {
                load_options(frequency_id);
            }
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function delete_current_payroll(payroll_id)
    {
        $.ajax({
            url: '<?= admin_url('payroll_management/delete_ajax') ?>',
            type: 'GET',
            dataType: 'JSON',
            data: {
                'payroll_id': payroll_id
            }
        })
        .done(function(data) {
            if (data == true) {
                swal("¡Eliminado!", "La nómina actual ha sido eliminada.", "success");
            }
        })
        .fail(function(data) {
            console.log("error");
        });
    }

    function enable_disable_set_test_id(work_environment)
    {
        if (work_environment == '<?= TEST ?>') {
            $('#set_test_id_container').show();
            $('#set_test_id').attr('required', true);
        } else {
            $('#set_test_id_container').hide();
            $('#set_test_id').attr('required', false);
        }
    }

    function load_options(frequency_id)
    {
        var options = '<option value=""><?= lang("select") ?></options>';
        if (frequency_id == '<?= BIWEEKLY ?>') {
            options += '<option value="<?= BIWEEKLY ?>">Cada quincena</options>'+
                       '<option value="<?= MONTHLY ?>">En la última quincena</options>';
        } else if (frequency_id == '<?= MONTHLY ?>') {
            options += '<option value="<?= MONTHLY ?>">Mensual</options>';
        }
        $('#bonus_payment').html(options);

        if (frequency_id == '<?= MONTHLY ?>') {
            $('#bonus_payment').select2('val', '<?= MONTHLY ?>');
        } else {
            $('#bonus_payment').select2('val', '');
        }
    }
</script>
