<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
    .spp_price_detail{
        font-size: 110%;
        color: #0c689e;
        font-weight: 500;
    }
    .spp_price{
        font-size: 400%;
        font-weight: 600;
    }
    .spp_div{
        padding-bottom: 4%;
        padding-top: 4%;
    }
    .spp_price_product_name{
        font-size: 150%;
        font-weight: 600;
    }
    input[type=search] {
        border: 2px solid #c2c2c2 !important;
        border-radius: 30px !important;
        line-height: 30px;
        padding-left: 20px;
        padding-right: 15px;
        height: 45px;
    }
</style>

<div class="modal-dialog modal-lg" style="width: 70%">
    <div class="modal-content">
        <div class="modal-header modal-primary">
            <h2 id="susModalLabel"><?=lang('product_price_checker');?></h2>
            <button type="button" class="btn btn-danger new-button pull-right" data-dismiss="modal" aria-hidden="true"><i class="fas fa-times fa-lg"></i>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="radio">
                            <label for="searchType1">
                                <?= form_radio(['name'=>'searchType', 'id'=>'searchType1', 'class'=>'searchType', 'value' => 1]) ?>
                                <?= lang('Código de Producto') ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="radio">
                            <label for="searchType2">
                                <?= form_radio(['name'=>'searchType', 'id'=>'searchType2', 'class'=>'searchType', 'value' => 2]) ?>
                                <?= lang('Código de Talla') ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="radio">
                            <label for="searchType3">
                                <?= form_radio(['name'=>'searchType', 'id'=>'searchType3', 'class'=>'searchType', 'value' => 3]) ?>
                                <?= lang('Nombre de Producto') ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="radio">
                            <label for="searchType4">
                                <?= form_radio(['name'=>'searchType', 'id'=>'searchType4', 'class'=>'searchType', 'value' => 4]) ?>
                                <?= lang('Búsqueda Múltiple') ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div id="searchContainer"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div style="display: inline-block";>
                <button type="button" class="btn btn-danger new-button" data-dismiss="modal" data-toggle="tooltip" data-placement="left" title="<?= lang('cancel') ?>">
                    <i class="fas fa-times fa-lg"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        let _searchContentHtml = '';
        let util = false;

        loadUtilities()

        $('.searchType').on('ifChecked', function(e) { searchContainer($(this)) })

        $(document).on('click', '#searchButton', function () { searchProductData(); util = true })
        $(document).on('keydown', '#search', function (event) { if (event.which === 13) { searchProductData(); util = true } })
        $(document).on('click', '#multipleSearchButton', function () { multipleSearchProductData($(this)); util = true })

        $(document).on ('click', '.change_img', function(event) {
            event.preventDefault();
            var img_src = $(this).attr('href');
            $('#pr-image').attr('src', img_src);
            return false;
        });

        $('#myModal').on('hidden.bs.modal', function (e) {
            if (util == true) {
                location.reload()
            }
        })
    });

    function loadUtilities()
    {
        // $('.searchType').iCheck({
        //     checkboxClass: 'icheckbox_square-blue',
        //     radioClass: 'iradio_square-blue',
        //     increaseArea: '20%'
        // })

        $('[data-toggle="tooltip"]').tooltip()

        loadSearchTypeDefault()
    }

    function loadSearchTypeDefault()
    {
        let searchType = localStorage.getItem('searchType')
        if (searchType) {
            localStorage.setItem('searchType', searchType)

            $('.searchType').each(function() {
                if ($(this).val() === searchType) {
                    $(this).iCheck('check')
                    searchContainer($(this))
                    // return false
                }
            })
        } else {
            $('#searchType1').iCheck('check')
            searchContainer($('#searchType1'))
        }

        
    }

    function searchContainer(element)
    {
        let searchType = element.val()
        let textSearchType = element.parents('.radio').find('label').text().trim()

        localStorage.setItem('searchType', searchType)

        if ([1,2,3].includes(parseInt(searchType))) {
            searchContentHtml(textSearchType)
            $('#searchContainer').html(_searchContentHtml)

            if (parseInt(searchType) == 3) {
                productsAutocomplete()
            } else if (parseInt(searchType) == 1) {
                productCodesAutocomplete()
            } else {
                $('#search').select('destroy')
                $('#search').focus();
            }
        } else {
            multipleSearchContainerHtml(textSearchType)
            $('#searchContainer').html(_searchContentHtml)

            referenceAutocomplete()
            categoryAutocomplete()
            subcategoryAutocomplete()
            subcategory2Autocomplete()
            variantsAutocomplete()
            colorAutocomplete()
            brandAutocomplete()
            materialAutocomplete()
        }

        $('[data-toggle="tooltip"]').tooltip()
    }

    function searchContentHtml(textSearchType)
    {
        _searchContentHtml = `<div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="input-group">
                    <input type="search" name="search" id="search" class="form-control" value="" placeholder="${textSearchType}"/>
                    <span class="input-group-btn">
                        <button class="btn btn-primary new-button" id="searchButton" type="button" data-toggle="tooltip" data-placement="right" title="<?= lang("search") ?>">
                            <i class="fas fa-search fa-lg"></i>
                        </button>
                    </span>
                </div>
            </div>
            <div class="col-sm-12">
                <div id="searchAnswerContent"></div>
            </div>
        </div>`
    }

    function searchProductData()
    {
        let searchValue = $('#search').val().trim()
        let searchType = localStorage.getItem('searchType')

        if (validateSearchValue(searchValue)) {
            $.ajax({
                type: "post",
                url: site.base_url+`products/checkPricesStockAjax`,
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'searchValue' : searchValue,
                    'searchType' : searchType,
                },
                dataType: "json",
                success: function (response) {
                    let searchAnswerHtml = ``

                    if (response.length !== 0) {
                        

                        searchAnswerHtml = `<br>
                        <div class="row">
                            <div class="col-xs-5">
                                <img id="pr-image" src="${response.productImage}" alt="${response.product.name}" class="img-responsive" />

                                ${getGallery(response)}
                            </div>
                            <div class="col-xs-7">
                                <div class="row">
                                    <h1 class="text-center">${response.product.name}
                                    <br>
                                    <small>${response.product.brand_name}</small></h1>
                                </div>
                                <hr>                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        ${productPriceGroupsHtml(response)}
                                        <hr>
                                        ${getMessages(response)}
                                    </div>
                                
                                    <div class="col-md-6" id="productVariantQuantity">
                                        ${getProductvariantsQuantity(response)}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-5">
                                <div>${quantityProductWarehousesHtml(response)}</div>
                            </div>
                            <div class="col-xs-7">
                                <div>${quantityProductVariantsWarehousesHtml(response)}</div>
                            </div>
                        </div>`
                    } else {
                        searchAnswerHtml = `<br>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-center" style="font-size: 20px; font-weight: bolder;">Sin registros</p>
                            </div>
                        </div>`
                    }

                    $('#searchAnswerContent').html(searchAnswerHtml)
                }
            });
        }
    }

    function validateSearchValue(searchValue)
    {
        if (searchValue == '') {
            swal({
                title: '<?= $this->lang->line('toast_alert_title') ?>',
                text: `El campo ${$('#search').attr('placeholder')} es Obligatorio`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            });

            $('#searchAnswerContent').html(``)

            $('#search').focus()
            return false
        }

        return true
    }

    function getGallery(response)
    {
        let images        = response.images
        let galleryImages = ''
        let gallery       = ''

        if (images) {
            images.forEach(image => {
                galleryImages += `<div class="col-sm-3">
                    <div class="img-responsive gallery-image">
                        <a class="change_img" href="${image}">
                            <img class="img-responsive" src="${image}" />
                        </a>
                    </div>
                </div>`
            })

            gallery += `<div style="padding: 10px 0 !important;">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="gallery-image">
                            <a class="img-responsive change_img" href="${response.productImage}">
                                <img class="img-responsive" src="${response.productImage}" />
                            </a>
                        </div>
                    </div>
                    ${galleryImages}
                </div>
            </div>`
        }

        return gallery
    }
    
    function productPriceGroupsHtml(response)
    {
        let productPriceGroupsHtml = ''
        let productPriceGroups = response.productPriceGroups

        if (productPriceGroups) {
            let tbody = ''
            let ipoconsumo = '<?= $this->Settings->ipoconsumo ?>'
            let prioridadPreciosProducto = '<?= $this->Settings->prioridad_precios_producto ?>'

            for (const prop in productPriceGroups) {
                let priceGroups = productPriceGroups[prop]

                tbody += `<tr>
                    <th>${priceGroups['name']}</th>
                    <td class="text-right">
                        ${ formatMoney(priceGroups['price'])}
                    </td>
                </tr>`
            }

            productPriceGroupsHtml = `<h3 class="bold"><?= lang('price_groups') ?></h3>
            <table class="table">
                <thead>
                    <tr>
                        <th><?= lang('price_group_name') ?></th>
                        <th class="text-right"><?= lang('price_group_price') ?></th>
                    </tr>
                </thead>
                <tbody>
                    ${tbody}
                </tbody>
            </table>`
        }

        return productPriceGroupsHtml
    }

    function getMessages(response)
    {
        let product     = response.product
        let discount    = product.birthday_discount
        let promo_price = product.promo_price
        let promotion   = product.promotion
        let variations   = response.productVariants
        let overPrice = false
        let message = ''

        if (variations) {
            variations.forEach(variation => {
                if (parseFloat(variation.price) > 0) {
                    overPrice = true
                }
            });
    
            if (overPrice) {
                message += `<div class="alert alert-warning" style="margin-bottom: 20px !important; position:unset; width:100%">
                    <i class="fas fa-hand-holding-usd"></i> Este producto tiene Sobreprecio en sus variantes
                </div>`
            }
        }
            
        if (discount) {
            message += `<div class="alert alert-success" style="margin-bottom: 20px !important; position:unset; width:100%">
                <i class="fas fa-tag"></i> Este producto tiene Descuento de <b>${ formatMoney(discount) }</b> por cumpleaños
            </div>`
        }

        if (promotion == 1) {
            message += `<div class="alert alert-success" style="margin-bottom: 20px !important; position:unset; width:100%">
                <i class="fas fa-tag"></i> Este producto tiene Promoción de <b>${ formatMoney(promo_price) }</b>
            </div>`
        }

        return message;
    }

    function getProductvariantsQuantity(response)
    {
        let options = response.options
        let productVariantQuantityHtml = ''

        if (options != null) {
            let variants = [];
            let tbody = ''

            options.forEach(option => {
                let quantity = parseFloat(option.wh_qty) || 0
                let name = option.name

                let existing = variants.find(item => item.name === name);
                if (existing) {
                    existing.quantity += quantity;
                } else {
                    variants.push({ name: name, quantity: quantity });
                }

            })
            
            variants.forEach(item => {
                tbody += `<tr>
                    <td>${item.name}</td>
                    <td class="text-center">${item.quantity}</td>
                </tr>`
            })

            productVariantQuantityHtml = `<h3 class="bold"><?= lang('Cantidades por talla'); ?></h3>
            <div class="table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th><?= lang('product_variant') ?></th>
                            <th class="text-center"><?= lang('quantity') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        ${tbody}
                    </tbody>
                </table>
            </div>`
        }

        return productVariantQuantityHtml
    }

    function quantityProductWarehousesHtml(response)
    {
        let quantityProductWarehousesHtml = '';
        if (response.productWarehouses && ((response.product.type == 'standard' || response.product.type == 'raw' || response.product.type == 'subproduct' || response.product.type == 'pfinished'))) {
            let tbody = '';
            response.productWarehouses.forEach(warehouse => {
                if (!(warehouse.quantity == 0 || warehouse.quantity === null)) {
                    tbody += `<tr>
                        <td>
                            ${warehouse.name} <small>(${warehouse.code})</small>
                        </td>
                        <td class="text-center">
                            ${ formatQuantity2(warehouse.quantity) }
                        </td>
                    </tr>`
                }
            })

            if (tbody != '') {
                quantityProductWarehousesHtml = `<h3 class="bold"><?= lang('warehouse_quantity') ?></h3>
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th><?= lang('warehouse_name') ?></th>
                                <th class="text-center"><?= lang('quantity') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            ${ tbody }
                        </tbody>
                    </table>
                </div>`
            }
        }

        return quantityProductWarehousesHtml;
    }

    function quantityProductVariantsWarehousesHtml(response)
    {
        let quantityProductVariantsWarehousesHtml = ''

        if (response.options) {
            let tbody = ''
            response.options.forEach(option => {
                if (option.whqty != 0) {
                    tbody += `<tr>
                        <td>${option.wh_name}</td>
                        <td>${option.name} <small>(${option.code})</small></td>
                        <td class="text-center">${ formatQuantity2(option.wh_qty)}</td>
                        <td class="text-right">
                            ${ (option.price > 0) ? formatMoney(parseFloat(response.product.price) + parseFloat(option.price)) : formatMoney(response.product.price) }
                        </td>                        
                        <td class="text-center">
                            ${ (option.price > 0) ? '<i class="fas fa-hand-holding-usd fa-lg text-success"></i>' : '' }
                        </td>                        
                    </tr>`
                }
            })

            if (tbody != '') {
                quantityProductVariantsWarehousesHtml = `<h3 class="bold"><?= lang('product_variants_quantity'); ?></h3>
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th><?= lang('warehouse_name') ?></th>
                                <th><?= lang('product_variant') ?></th>
                                <th class="text-center"><?= lang('quantity') . ' <small>(' . lang('rack') . ')</small>' ?></th>
                                <th class="text-right"><?= lang('price') ?></th>
                                <th class="text-right"><?= lang('surcharges') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            ${tbody}
                        </tbody>
                    </table>
                </div>`
            }
        }

        return quantityProductVariantsWarehousesHtml
    }

    function multipleSearchContainerHtml()
    {
        _searchContentHtml = `<div class="row">
            <div class="col-sm-3  form-group">
                <label for="reference"><?= lang('reference') ?></label>
                <input type="text" name="reference" id="reference" class="form-control multipleSearchInput"/>
            </div>
            <div class="col-sm-3  form-group">
                <label for="category"><?= lang('category') ?></label>
                <input type="text" name="category" id="category" class="form-control multipleSearchInput"/>
            </div>
            <div class="col-sm-3  form-group">
                <label for="subcategory"><?= lang('subcategory') ?></label>
                <input type="text" name="subcategory" id="subcategory" class="form-control multipleSearchInput"/>
            </div>
            <div class="col-sm-3  form-group">
                <label for="subcategory2"><?= lang('second_level_subcategory_id') ?></label>
                <input type="text" name="subcategory2" id="subcategory2" class="form-control multipleSearchInput"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3  form-group">
                <label for="variants"><?= lang('variants') ?></label>
                <input type="text" name="variants" id="variants" class="form-control multipleSearchInput"/>
            </div>
            <div class="col-sm-3  form-group">
                <label for="color"><?= lang('color') ?></label>
                <input type="text" name="color" id="color" class="form-control multipleSearchInput"/>
            </div>
            <div class="col-sm-3  form-group">
                <label for="brand"><?= lang('brand') ?></label>
                <input type="text" name="brand" id="brand" class="form-control multipleSearchInput"/>
            </div>
            <div class="col-sm-3  form-group">
                <label for="material"><?= lang('material') ?></label>
                <input type="text" name="material" id="material" class="form-control multipleSearchInput"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-primary new-button pull-right" id="multipleSearchButton" type="button" data-toggle="tooltip" data-placement="top" title="<?= lang('search') ?>">
                    <i class="fas fa-search fa-lg"></i>
                </button>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div id="searchAnswerContent"></div>
            </div>
        </div>`
    }

    function productsAutocomplete()
    {
        $('#search').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/productsSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>'
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#search').select2('open');

        $(document).on('change', '#search', function() {
            searchProductData()
        });    
    }

    function productCodesAutocomplete()
    {
        $('#search').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/productCodesSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>'
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#search').select2('open');

        $(document).on('change', '#search', function() {
            searchProductData()
        });    
    }

    function referenceAutocomplete()
    {
        $('#reference').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/referenceSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>'
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        
        $('#reference').select2('open');
    }

    function categoryAutocomplete()
    {
        $('#category').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/categorySuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>'
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function subcategoryAutocomplete()
    {
        $('#subcategory').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/categorySuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>',
                        parentId: $('#category').val()
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function subcategory2Autocomplete()
    {
        $('#subcategory2').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/categorySuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>',
                        parentId: $('#category').val(),
                        subcategoryId: $('#subcategory').val()
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function variantsAutocomplete()
    {
        $('#variants').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/variantsSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>'
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function colorAutocomplete()
    {
        $('#color').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/colorSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>'
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function brandAutocomplete()
    {
        $('#brand').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/brandSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>'
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function materialAutocomplete()
    {
        $('#material').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "products/materialSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: '<?= $this->Settings->max_num_results_display ?>'
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function multipleSearchProductData()
    {
        if (validateMultipleSearchValue()) {
            $.ajax({
                type: "post",
                url: site.base_url+'products/multipleSearchProductAjax',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    reference       : $('#reference').val(),
                    category        : $('#category').val(),
                    subcategory     : $('#subcategory').val(),
                    subsubcategory  : $('#subcategory2').val(),
                    variants        : $('#variants').val(),
                    color           : $('#color').val(),
                    brand           : $('#brand').val(),
                    material        : $('#material').val(),
                },
                dataType: "json",
                success: function (response) {
                    let table = ''
                    if (response) {
                        tbody = ''
                        response.forEach(product => {
                            let quantity = (product.variantName == null) ? product.productQuantity : product.variantQuantity ?? 0

                            if (quantity > 0) {
                                tbody += `<tr>
                                    <td>${product.productCode}</td>
                                    <td>${product.productReference}</td>
                                    <td>${product.productName}</td>
                                    <td>${(product.variantName == null) ? 'Todas' : product.variantName}</td>
                                    <td>${(product.variantCode == null) ? '' : product.variantCode}</td>
                                    <td>${(product.variantName == null) ? product.productWarehouseName : product.variantWarehouseName}</td>
                                    <?php if ($this->pos_settings->check_prices_with_stock == 1) { ?>
                                        <td class="text-right">${(product.variantName == null) ? product.productQuantity : product.variantQuantity ?? 0}</td>
                                    <?php } ?>
                                    <td class="text-right">${(product.variantPrice == null) ? product.productPrice : formatMoney(product.productPrice + product.variantPrice)}</td>
                                </tr>`
                            }

                        });

                        table = `<div class="table-responsive">
                            <table id="productLookupTable" class="table table-striped">
                            <thead>
                                <th>Código</th>
                                <th>Referencia</th>
                                <th>Nombre</th>
                                <th>Talla</th>
                                <th>Código EAN</th>
                                <th>Almacén</th>
                                <?php if ($this->pos_settings->check_prices_with_stock == 1) { ?>
                                    <th>Cantidad</th>
                                <?php } ?>
                                <th>Precio</th>
                            </thead>
                                <tbody>
                                ${tbody}
                                </tbody>
                            </table>
                        </div>`

                        $('#searchAnswerContent').html(table)
                        $('#productLookupTable').dataTable();
                    } else {
                        let string = `<div class="row">
                            <div class="col-sm-12 text-center "> 
                                <h2 clas="text-info"><smal>No se encontraron resultados para la búsqueda</small></h2>
                            </div>
                        </div>`
                        $('#searchAnswerContent').html(string)
                    }
                }
            });
        }
    }

    function validateMultipleSearchValue()
    {
        let emptyInputs = true

        $('.multipleSearchInput').each(function() {
            if ($(this).val() !== '') {
                emptyInputs = false
                return false
            }
        });

        if (emptyInputs) {
            swal({
                title: '<?= $this->lang->line('toast_alert_title') ?>',
                text: `Debe ingrear al menos un criterio de búsqueda`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            })

            $('#reference').select2('open');
            return false
        }

        return true
    }
</script>
