<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Purchases_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('settings_model');
    }

    public function getProductNames($term, $biller_id, $supplier_id)
    {
        if ($this->Settings->variant_code_search == 0) {
            return $this->get_product_names($term, $biller_id, $supplier_id);
        } else {
            if ($data = $this->get_variant_names($term, $biller_id, $supplier_id)) {
                return $data;
            } else {
                return $this->get_product_names($term, $biller_id, $supplier_id);
            }
        }
    }
    
    public function get_product_names($term, $biller_id, $supplier_id){
        $limit = $this->Settings->max_num_results_display;

        $this->site->create_temporary_product_billers_assoc($biller_id);

        $this->db->select('products.*')
            ->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
        if ($this->Settings->purchases_products_supplier_code == 1) {
                    $this->db->select("(
                        IF({$this->db->dbprefix("products")}.supplier1 = {$supplier_id} AND {$this->db->dbprefix("products")}.supplier1_part_no LIKE '%{$term}%', {$this->db->dbprefix("products")}.supplier1_part_no,
                            IF({$this->db->dbprefix("products")}.supplier2 = {$supplier_id} AND {$this->db->dbprefix("products")}.supplier2_part_no LIKE '%{$term}%', {$this->db->dbprefix("products")}.supplier2_part_no,
                                IF({$this->db->dbprefix("products")}.supplier3 = {$supplier_id} AND {$this->db->dbprefix("products")}.supplier3_part_no LIKE '%{$term}%', {$this->db->dbprefix("products")}.supplier3_part_no,
                                    IF({$this->db->dbprefix("products")}.supplier4 = {$supplier_id} AND {$this->db->dbprefix("products")}.supplier4_part_no LIKE '%{$term}%', {$this->db->dbprefix("products")}.supplier4_part_no,
                                        IF({$this->db->dbprefix("products")}.supplier5 = {$supplier_id} AND {$this->db->dbprefix("products")}.supplier5_part_no LIKE '%{$term}%', {$this->db->dbprefix("products")}.supplier5_part_no,
                                            NULL
                                        )
                                    )
                                )
                            )
                        )
                    ) AS supplier_part_no");
            $this->db->where("
                            (".$this->db->dbprefix("products").".type = 'standard' or ".$this->db->dbprefix("products").".type = 'raw')
                             AND (
                                ".$this->db->dbprefix("products").".name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                                OR (".$this->db->dbprefix("products").".supplier1 = {$supplier_id} AND ".$this->db->dbprefix("products").".supplier1_part_no ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")
                                OR (".$this->db->dbprefix("products").".supplier2 = {$supplier_id} AND ".$this->db->dbprefix("products").".supplier2_part_no ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")
                                OR (".$this->db->dbprefix("products").".supplier3 = {$supplier_id} AND ".$this->db->dbprefix("products").".supplier3_part_no ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")
                                OR (".$this->db->dbprefix("products").".supplier4 = {$supplier_id} AND ".$this->db->dbprefix("products").".supplier4_part_no ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")
                                OR (".$this->db->dbprefix("products").".supplier5 = {$supplier_id} AND ".$this->db->dbprefix("products").".supplier5_part_no ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")

                            )");
        } else {
            $this->db->where("
                            (".$this->db->dbprefix("products").".type = 'standard' or ".$this->db->dbprefix("products").".type = 'raw')
                             AND (
                                ".$this->db->dbprefix("products").".name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                                OR ".$this->db->dbprefix("products").".reference ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                                OR ".$this->db->dbprefix("products").".code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                                OR  concat(".$this->db->dbprefix("products").".name, ' (', ".$this->db->dbprefix("products").".code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                            )");
        }
        $this->db->limit($limit);
        $this->db->where('products.discontinued', '0');
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return FALSE;
    }
    
    
    public function get_variant_names($term, $biller_id, $supplier_id){
        $limit = $this->Settings->max_num_results_display;
        $this->site->create_temporary_product_billers_assoc($biller_id);
        $this->db->select('products.*,
                            product_variants.id as variant_selected')
            ->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
            ->join('product_variants', 'product_variants.product_id=products.id', 'left');
        $this->db->where("
                        (".$this->db->dbprefix("products").".type = 'standard' or ".$this->db->dbprefix("products").".type = 'raw')
                          AND "
                            . "(
                                {$this->db->dbprefix('product_variants')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                            )");
        $this->db->limit($limit);
        $this->db->where('products.discontinued', '0');
        $this->db->where('product_variants.status', '1');
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return FALSE;
    }

    public function getExpensesCategoriesNames($term, $support_document = null, $purchase_type = NULL){
        $limit = $this->Settings->max_num_results_display;
        $this->db->select('expense_categories.id, expense_categories.code, expense_categories.name, 0 as cost, expense_categories.tax_rate_id as tax_rate, expense_categories.tax_rate_2_id as tax_rate_2, expense_categories.creditor_ledger_id')
                ->join('tax_rates', 'tax_rates.id = expense_categories.tax_rate_id')
                ->where('( expense_categories.name LIKE "%'.$term.'%" or expense_categories.code LIKE "%'.$term.'%")');
        if ($support_document == 1) {
            $this->db->where('tax_rates.rate', 0);
        }
        if ($purchase_type == 3) {
            $this->db->where('expense_categories.expense_import', 1);
        } else {
            $this->db->where('expense_categories.expense_import', 0);
        }
        $q = $this->db->limit($limit)->get('expense_categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getAllProducts()
    {
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductsByCode($code)
    {
        $this->db->select('*')->from('products')->like('code', $code, 'both');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllPurchases()
    {
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getAllPurchaseItems($purchase_id, $arr = false)
    {
        $this->db->select('
                            purchase_items.*,
                            tr.code as tax_code,
                            tr.name as tax_name,
                            tr.rate as tax_rate,
                            tr.tax_indicator as tax_indicator,
                            tr2.code as tax_2_code,
                            tr2.name as tax_2_name,
                            tr2.rate as tax_2_rate,
                            tr2.tax_indicator as tax_indicator2,
                            tr2ic.tax_indicator as tax_indicator2ic,
                            tr2ic.code as tax_code2ic,
                            products.unit,
                            products.details as details,
                            CONCAT('.$this->db->dbprefix('product_variants').'.name, '.$this->db->dbprefix('product_variants').'.suffix) as variant,
                            products.hsn_code as hsn_code,
                            products.second_name as second_name,
                            IF('.$this->db->dbprefix('units').'.code IS NOT NULL, '.$this->db->dbprefix('units').'.code, '.$this->db->dbprefix('purchase_items').'.product_unit_code) as product_unit_code,
                            units.operation_value,
                            units.operator,
                            product_variants.name AS nombre_variante,
                            product_variants.code AS codigo_variante
                           ')
            ->join('products', 'products.id=purchase_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
            ->join('tax_rates tr', 'tr.id=purchase_items.tax_rate_id', 'left')
            ->join('tax_rates tr2', 'tr2.id=purchase_items.tax_rate_2_id', 'left')
            ->join('tax_secondary tr2ic', 'tr2ic.id=purchase_items.tax_rate_2_id', 'left')
            ->join('units', 'purchase_items.product_unit_id_selected=units.id', 'left')
            ->join('unit_prices', 'purchase_items.product_unit_id_selected = unit_prices.unit_id and unit_prices.id_product = purchase_items.product_id', 'left')
            ->group_by('purchase_items.id')
            ->order_by('id', 'desc');
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                foreach (($q->result_array()) as $row) {
                    $data[] = $row;
                }
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemByID($id)
    {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByCode($code)
    {
        $q = $this->db->get_where('tax_rates', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchaseByID($id, $arr = false)
    {
        $q = $this->db->select('purchases.*,
                           tax_rates.rate as tax_rate,
                           tax_rates.type as type_tax_rate,
                           documents_types.module_invoice_format_id,
                           documents_types.factura_contingencia,
                           documents_types.factura_electronica
                           ')
            ->where(array('purchases.id' => $id))
            ->join('documents_types', 'purchases.document_type_id = documents_types.id', 'left')
            ->join('tax_rates', 'tax_rates.id = purchases.order_tax_id', 'left')
            ->get('purchases');
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                return $q->row();
            } else {
                return $q->row_array();
            }
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                return TRUE;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function resetProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getOverSoldCosting($product_id)
    {
        $q = $this->db->get_where('costing', array('overselling' => 1));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function get_reference($data)
    {
        $manual = (isset($data['reference_no']) && $data['reference_no']) ? true : false;
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id'], false);
        $update_ref = false;
        if ($manual) {
            $refbil = explode("-", $referenceBiller);
            $reference = $refbil[0]."-".$data['reference_no'];
        } else {
            $reference = $referenceBiller;
            $update_ref = true;
        }

        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];

        if (isset($data['supplier_id']) && $this->site->validate_supplier_reference($data['reference_no'], $data['supplier_id'])) {
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            return $this->get_reference($data);
        } else {
            return ['data' => $data, 'consecutive' => $consecutive, 'update_ref'=>$update_ref];
        }
    }

    public function addPurchase($data, $items, $payment = array(), $ptype = 1, $si_return = false, $data_taxrate_traslate = array(), $quote_id = NULL, $unit_update_price = NULL, $pg_update_price = NULL, $unit_update_margin = NULL, $new_pv = [])
    {
        // $this->sma->print_arrays($data, $items);
        $ref_data = $this->get_reference($data);
        $data = $ref_data['data'];
        $consecutive = $ref_data['consecutive'];
        $update_ref = $ref_data['update_ref'];
        if ($this->Settings->overselling == 0 && $si_return) {
            foreach ($items as $reitem) {
                $row = $this->getProductByID($reitem['product_id']);
                if ($row->attributes) {
                    $wh_product=$this->site->getWarehouseProductsVariants($reitem['option_id'], $reitem['warehouse_id']);
                } else {
                    $wh_product=$this->site->getWarehouseProducts($reitem['product_id'], $reitem['warehouse_id']);
                }
                foreach ($wh_product as $key_wh => $value_wh) {
                    if ($value_wh->warehouse_id == $reitem['warehouse_id']) {
                        if (abs($reitem['quantity']) > $value_wh->quantity) {
                            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), (($reitem['product_name']))));
                            $this->session->unset_userdata('purchase_processing');
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                }
            }
        }
            
        if ($this->db->insert('purchases', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($update_ref) {
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            }
            $supplier = $this->site->getCompanyByID($data['supplier_id']);
            if ($quote_id) {
                $this->db->update('quotes', array('status' => 'completed', 'destination_reference_no' => $data['reference_no']), array('id' => $quote_id));
            }
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            } else if ($this->site->getReference('ex') == $data['reference_no']) {
                $this->site->updateReference('ex');
            }
            foreach ($items as $item_key => $item) {
                $profitability_margin = isset($item['profitability_margin']) ? $item['profitability_margin'] : NULL;
                if (isset($item['profitability_margin'])) {
                    unset($item['profitability_margin']);
                }
                $item['purchase_id'] = $purchase_id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                if ($this->Settings->product_variant_per_serial == 1 && !empty($item['serial_no'])) {
                    $v_exist = $this->db->get_where('product_variants', ['name'=>$item['serial_no'], 'product_id' => $item['product_id']]);
                    if ($v_exist->num_rows() > 0) {
                        $v_exist = $v_exist->row();
                        $item['option_id'] =  $v_exist->id;
                    } else {
                        $DB2 = false;
                        $new_id = NULL;
                        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
                            $DB2 = $this->settings_model->connect_new_year_database();
                            if (!$this->settings_model->check_same_id_insert($DB2, 'product_variants')) {
                                $new_id = $this->settings_model->get_new_year_new_id_for_insertion($DB2,'product_variants');
                            }
                        }
                        $this->db->insert('product_variants', [
                                                        'id' => $new_id,
                                                        'product_id' => $item['product_id'],
                                                        'name' => $item['serial_no'],
                                                        'cost' => $item['unit_cost'],
                                                        'price' => 0,
                                                        'quantity' => $item['quantity'],
                                                            ]);
                        if ($DB2) {
                            $DB2->insert('product_variants', [
                                                        'id' => $new_id,
                                                        'product_id' => $item['product_id'],
                                                        'name' => $item['serial_no'],
                                                        'cost' => $item['unit_cost'],
                                                        'price' => 0,
                                                        'quantity' => $item['quantity'],
                                                            ]);
                        }
                        $item['option_id'] =  $this->db->insert_id();
                        $this->db->insert('warehouses_products_variants', [
                                            'product_id' => $item['product_id'],
                                            'warehouse_id' => $item['warehouse_id'],
                                            'option_id' => $item['option_id'],
                                            'quantity' => $item['quantity']
                                            ]);
                    }
                }
                if ($this->Settings->management_weight_in_variants == 1 && isset($item['new_option_assigned'])) {
                    $new_option_assigned = $item['new_option_assigned'];
                    unset($item['new_option_assigned']);
                    if (isset($new_pv[$new_option_assigned])) {
                        $new_option_assigned_data = $new_pv[$new_option_assigned];
                        $this->db->insert('product_variants', [
                                                        'product_id' => $new_option_assigned_data['product_id'],
                                                        'name' => $new_option_assigned_data['name'],
                                                        'code' => $new_option_assigned_data['code'],
                                                        'pv_code_consecutive' => $new_option_assigned_data['pv_code_consecutive'],
                                                        'weight' => $new_option_assigned_data['weight'],
                                                        'cost' => $item['unit_cost'],
                                                        'quantity' => $item['quantity'],
                                                            ]);
                        $item['option_id'] =  $this->db->insert_id();
                        $this->db->insert('warehouses_products_variants', [
                                            'product_id' => $item['product_id'],
                                            'warehouse_id' => $item['warehouse_id'],
                                            'option_id' => $item['option_id'],
                                            'quantity' => $item['quantity']
                                            ]);
                    }
                }
                $return_purchase_item_id = isset($item['purchase_item_id']) ? $item['purchase_item_id'] : NULL;
                $this->db->insert('purchase_items', $item);
                $item['unit_cost'] = $item['unit_cost'] - (isset($item['consumption_purchase']) ? $this->sma->formatDecimal($item['consumption_purchase'] / $item['quantity']) : 0 );
                $purchase_item_id = $this->db->insert_id();
                $items[$item_key]['purchase_item_id'] = $purchase_item_id;
                $item['purchase_item_id'] = $purchase_item_id;
                if ($data['purchase_type'] == 1) {
                    $p_s_data = $this->getProductByID($item['product_id']);
                    $to_update_cost = ($this->Settings->tax_method == 0 ? $item['unit_cost'] : $item['net_unit_cost']);
                    if ($this->Settings->tax_method == 0 && $item['original_tax_rate_id'] != NULL) {
                        $tax = $this->site->getTaxRateByID($item['original_tax_rate_id']);
                        $to_update_cost_tax = $this->site->calculateTax(NULL, $tax, $to_update_cost, NULL, 1);
                        $to_update_cost += $to_update_cost_tax['amount'];
                    }
                    if ($data['supplier_id'] == $p_s_data->supplier1) {

                        $this->db->update('products', ['supplier1price' => $to_update_cost, 'supplier1price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                        if ($item['supplier_part_no']) {
                            $this->db->update('products', ['supplier1_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                        }

                    } else if ($data['supplier_id'] == $p_s_data->supplier2) {

                        $this->db->update('products', ['supplier2price' => $to_update_cost, 'supplier2price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                        if ($item['supplier_part_no']) {
                            $this->db->update('products', ['supplier2_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                        }

                    } else if ($data['supplier_id'] == $p_s_data->supplier3) {

                        $this->db->update('products', ['supplier3price' => $to_update_cost, 'supplier3price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                        if ($item['supplier_part_no']) {
                            $this->db->update('products', ['supplier3_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                        }

                    } else if ($data['supplier_id'] == $p_s_data->supplier4) {

                        $this->db->update('products', ['supplier4price' => $to_update_cost, 'supplier4price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                        if ($item['supplier_part_no']) {
                            $this->db->update('products', ['supplier4_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                        }

                    } else if ($data['supplier_id'] == $p_s_data->supplier5) {

                        $this->db->update('products', ['supplier5price' => $to_update_cost, 'supplier5price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                        if ($item['supplier_part_no']) {
                            $this->db->update('products', ['supplier5_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                        }

                    } else {

                        if (!$p_s_data->supplier1price_date || ((!empty($p_s_data->supplier5price_date) && !empty($p_s_data->supplier4price_date) && !empty($p_s_data->supplier3price_date) && !empty($p_s_data->supplier2price_date)) && ($p_s_data->supplier5price_date > $p_s_data->supplier4price_date) && ($p_s_data->supplier5price_date > $p_s_data->supplier3price_date) && ($p_s_data->supplier5price_date > $p_s_data->supplier2price_date) && ($p_s_data->supplier5price_date > $p_s_data->supplier1price_date))) {

                            $this->db->update('products', ['supplier1' => $data['supplier_id'], 'supplier1price' => $to_update_cost, 'supplier1price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                            if ($item['supplier_part_no']) {
                                $this->db->update('products', ['supplier1_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                            }

                        } else if (!$p_s_data->supplier2price_date || ((!empty($p_s_data->supplier5price_date) && !empty($p_s_data->supplier4price_date) && !empty($p_s_data->supplier3price_date) && !empty($p_s_data->supplier1price_date)) && ($p_s_data->supplier1price_date > $p_s_data->supplier2price_date) && ($p_s_data->supplier1price_date > $p_s_data->supplier3price_date) && ($p_s_data->supplier1price_date > $p_s_data->supplier4price_date) && ($p_s_data->supplier1price_date > $p_s_data->supplier5price_date))) {

                            $this->db->update('products', ['supplier2' => $data['supplier_id'], 'supplier2price' => $to_update_cost, 'supplier2price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                            if ($item['supplier_part_no']) {
                                $this->db->update('products', ['supplier2_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                            }

                        } else if (!$p_s_data->supplier3price_date || ((!empty($p_s_data->supplier5price_date) && !empty($p_s_data->supplier4price_date) && !empty($p_s_data->supplier1price_date) && !empty($p_s_data->supplier2price_date)) && ($p_s_data->supplier2price_date > $p_s_data->supplier1price_date) && ($p_s_data->supplier2price_date > $p_s_data->supplier3price_date) && ($p_s_data->supplier2price_date > $p_s_data->supplier4price_date) && ($p_s_data->supplier2price_date > $p_s_data->supplier5price_date))) {

                            $this->db->update('products', ['supplier3' => $data['supplier_id'], 'supplier3price' => $to_update_cost, 'supplier3price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                            if ($item['supplier_part_no']) {
                                $this->db->update('products', ['supplier3_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                            }

                        } else if (!$p_s_data->supplier4price_date || ((!empty($p_s_data->supplier5price_date) && !empty($p_s_data->supplier1price_date) && !empty($p_s_data->supplier3price_date) && !empty($p_s_data->supplier2price_date)) && ($p_s_data->supplier3price_date > $p_s_data->supplier1price_date) && ($p_s_data->supplier3price_date > $p_s_data->supplier2price_date) && ($p_s_data->supplier3price_date > $p_s_data->supplier4price_date) && ($p_s_data->supplier3price_date > $p_s_data->supplier5price_date))) {

                            $this->db->update('products', ['supplier4' => $data['supplier_id'], 'supplier4price' => $to_update_cost, 'supplier4price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                            if ($item['supplier_part_no']) {
                                $this->db->update('products', ['supplier4_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                            }

                        } else if (!$p_s_data->supplier5price_date || ((!empty($p_s_data->supplier1price_date) && !empty($p_s_data->supplier4price_date) && !empty($p_s_data->supplier3price_date) && !empty($p_s_data->supplier2price_date)) && ($p_s_data->supplier4price_date > $p_s_data->supplier1price_date) && ($p_s_data->supplier4price_date > $p_s_data->supplier2price_date) && ($p_s_data->supplier4price_date > $p_s_data->supplier3price_date) && ($p_s_data->supplier4price_date > $p_s_data->supplier5price_date))) {

                            $this->db->update('products', ['supplier5' => $data['supplier_id'], 'supplier5price' => $to_update_cost, 'supplier5price_date' => date('Y-m-d H:i:s')], ['id'=>$item['product_id']]);

                            if ($item['supplier_part_no']) {
                                $this->db->update('products', ['supplier5_part_no' => $item['supplier_part_no']], ['id'=>$item['product_id']]);
                            }

                        }

                    }
                }
                if ($this->Settings->update_cost && $ptype == 1 && !$si_return) {
                    $to_update_cost = ($this->Settings->tax_method == 0 ? $item['unit_cost'] : $item['net_unit_cost']);
                    if ($this->Settings->tax_method == 0 && $item['original_tax_rate_id'] != NULL) {
                        $tax = $this->site->getTaxRateByID($item['original_tax_rate_id']);
                        $to_update_cost_tax = $this->site->calculateTax(NULL, $tax, $to_update_cost, NULL, 1);
                        $to_update_cost += $to_update_cost_tax['amount'];
                    }
                    $to_update = [
                        'cost' => $to_update_cost,
                        'profitability_margin' => $profitability_margin,
                    ];
                    if ($item['item_tax_2'] > 0 && strpos($item['tax_2'], '%') !== false) {
                        $to_update['consumption_purchase_tax'] = $item['item_tax_2'] / $item['quantity'];
                        $to_update['purchase_tax_rate_2_percentage'] = $item['tax_2'];
                    }
                    $this->db->update('products', $to_update, array('id' => $item['product_id']));
                }
                if($item['option_id']) {
                    $this->db->update('product_variants', array('cost' => $item['unit_cost']), array('id' => $item['option_id'], 'product_id' => $item['product_id']));
                }
                if ($ptype != 1) { //SI NO ES COMPRA DE PRODUCTOS, NO MUEVE INVENTARIO NI ACTUALIZA COSTOS DE NADA.
                    continue;
                }
                if ($data['status'] == 'received' || $data['status'] == 'returned') {
                    $this->site->updateAVCO($item);
                }
                if ($si_return && $ptype == 1) {
                    $this->db->query("UPDATE {$this->db->dbprefix('purchase_items')} SET {$this->db->dbprefix('purchase_items')}.returned_quantity = COALESCE({$this->db->dbprefix('purchase_items')}.returned_quantity, 0) + ".($item['quantity'] * -1)." WHERE {$this->db->dbprefix('purchase_items')}.id = ".$return_purchase_item_id);
                   $rq = $this->db->select('*')
                                  ->where('product_id', $item['product_id'])
                                  ->where('purchase_id >', $data['purchase_id'])
                                  ->where('purchase_id !=', $purchase_id)
                                  ->where('quantity > 0')
                                  ->group_by('product_id')
                                  ->get('purchase_items');
                   if ($rq->num_rows() == 0) {
                        $av_qty = $this->db->select('(quantity - returned_quantity) AS available_quantity, unit_cost, (consumption_purchase / quantity) AS consumption_purchase')
                                 ->where('product_id', $item['product_id'])
                                 ->where('purchase_id', $data['purchase_id'])
                                 ->having('available_quantity >', 0)
                                 ->limit(1)
                                 ->get('purchase_items');
                        if ($av_qty->num_rows() > 0) {
                            $av_qty = $av_qty->row();
                            $this->db->update('products', ['cost'=>($av_qty->unit_cost - $av_qty->consumption_purchase)], ['id'=>$item['product_id']]);
                        } else {
                            $rqu = $this->db->select('unit_cost, (consumption_purchase / quantity) AS consumption_purchase')
                                            ->where('product_id', $item['product_id'])
                                            ->where('purchase_id <', $data['purchase_id'])
                                            ->where('purchase_id !=', NULL)
                                            ->order_by('purchase_id DESC')
                                            ->limit(1)
                                            ->get('purchase_items');
                            if ($rqu->num_rows() > 0) {
                                $rqu = $rqu->row();
                                $this->db->update('products', ['cost'=>($rqu->unit_cost - $rqu->consumption_purchase)], ['id'=>$item['product_id']]);
                            }
                        } 
                   }
                }
            }
            $this->site->syncStoreProductsQuantityMovement($items, 1);
            
            if ($si_return) {
                $this->db->query("UPDATE {$this->db->dbprefix('purchases')} SET {$this->db->dbprefix('purchases')}.return_purchase_total = COALESCE({$this->db->dbprefix('purchases')}.return_purchase_total, 0) + ".($data['grand_total'] * -1)." WHERE {$this->db->dbprefix('purchases')}.id = ".$data['purchase_id']);
                $this->db->query("UPDATE {$this->db->dbprefix('purchases')} SET {$this->db->dbprefix('purchases')}.return_purchase_ref = CONCAT(IF({$this->db->dbprefix('purchases')}.return_purchase_ref IS NOT NULL, CONCAT({$this->db->dbprefix('purchases')}.return_purchase_ref, ', '), ''), '".$data['reference_no']."') WHERE {$this->db->dbprefix('purchases')}.id = ".$data['purchase_id']);
                $this->db->insert('purchases_return', [
                                                            'purchase_id' => $data['purchase_id'],
                                                            'purchase_reference_no' => $data['return_purchase_ref'],
                                                            'return_id' => $purchase_id,
                                                            'return_reference_no' => $data['reference_no'],
                                                      ]);

            }

            $total_payment = 0;
            if (($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid') || !empty($payment)) {
                foreach ($payment as $pmnt) {
                    $update_pmnt_reference = false;
                    if ((!isset($pmnt['reference_no']) || $pmnt['reference_no'] != 'retencion') && isset($pmnt['document_type_id'])) {
                        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt['document_type_id'], false);
                        $reference = $referenceBiller;
                        $pmnt['reference_no'] = $reference;
                        $ref = explode("-", $reference);
                        $p_consecutive = $ref[1];
                        $update_pmnt_reference = true;
                    }
                    $pmnt['purchase_id'] = $purchase_id;
                    if ($pmnt['paid_by'] == 'deposit') {
                        if ($si_return) {
                            // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
                            $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt['deposit_document_type_id']);
                            if($referenceBiller){
                                $reference_deposit = $referenceBiller;
                            }else{
                                $reference_deposit = $this->site->getReference('rc');
                            }
                            $ref = explode("-", $reference_deposit);
                            $deposit_consecutive = $ref[1];
                            // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
                            $deposit_data = array(
                                'date' => date('Y-m-d H:i:s'),
                                'reference_no' => $reference_deposit,
                                'amount' => $pmnt['amount'] * -1,
                                'balance' => $pmnt['amount'] * -1,
                                'paid_by' => $pmnt['paid_by'],
                                'note' => 'Anticipo generado automáticamente por devolución',
                                'company_id' => $data['supplier_id'],
                                'created_by' => $this->session->userdata('user_id'),
                                'biller_id' => $data['biller_id'],
                                'document_type_id' => $pmnt['deposit_document_type_id'],
                                'origen_reference_no' => $data['reference_no'],
                                'origen_document_type_id' => $data['document_type_id'],
                                'origen' => '2',
                            );

                            if (isset($data['cost_center_id'])) {
                                $deposit_data['cost_center_id'] = $data['cost_center_id'];
                            }

                            if ($this->db->insert('deposits', $deposit_data)) {
                                $this->site->updateBillerConsecutive($pmnt['deposit_document_type_id'], $deposit_consecutive+1);
                                $this->site->wappsiContabilidadDeposito($deposit_data, 2);
                                $this->db->update('companies', ['deposit_amount' => ($supplier->deposit_amount + ($pmnt['amount'] * -1) )], ['id'=> $supplier->id]);
                            }
                            unset($pmnt['deposit_document_type_id']);

                            if ($this->db->insert('payments', $pmnt) && $update_pmnt_reference) {
                                $this->site->updateBillerConsecutive($pmnt['document_type_id'], $p_consecutive+1);
                            }
                        } else {
                            $p_arr[] = $pmnt;
                            $p_arr = $this->site->set_payments_affected_by_deposits($supplier->id, $p_arr);
                            $count = 0;
                            foreach ($p_arr as $pmnt2) {
                                if ($count > 0) {
                                    $this->site->updateBillerConsecutive($pmnt2['document_type_id'], $p_consecutive+1);
                                    // NUEVA REFERENCIA POR REGISTRO DE OTRO DEPÓSITO
                                    $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt2['document_type_id']);
                                    if($referenceBiller){
                                        $reference_payment = $referenceBiller;
                                    }else{
                                        $reference_payment = $this->site->getReference('rc');
                                    }
                                    $pmnt2['reference_no'] = $reference_payment;
                                    $ref = explode("-", $reference_payment);
                                    $p_consecutive = $ref[1];
                                    // NUEVA REFERENCIA POR REGISTRO DE OTRO DEPÓSITO
                                }
                                $this->db->insert('payments', $pmnt2);
                                $count++;
                            }
                        }
                    } else {
                        if ($this->db->insert('payments', $pmnt) && $update_pmnt_reference) {
                            $this->site->updateBillerConsecutive($pmnt['document_type_id'], $p_consecutive+1);
                        }
                    }

                    if ($pmnt['paid_by'] != 'due' && $pmnt['paid_by'] != 'retencion') {
                        $total_payment += $pmnt['amount'];
                    }
                }
            }
            $this->site->syncPurchasePayments($purchase_id);
            if ($data['status'] == 'returned') {
                // $this->db->update('purchases', array('return_purchase_ref' => $data['reference_no'], 'surcharge' => $data['surcharge'],'return_purchase_total' => $data['grand_total'], 'return_id' => $purchase_id), array('id' => $data['purchase_id']));
                if ($this->Settings->modulary == 1) { //Si se indicó que el POS es modular con Contabilidad.
                    //LLamado a la función de Wappsi Contabilidad Devolución de Compras
                    if ($ptype == 1) {
                        $this->site->wappsiContabilidadComprasDevolucion($purchase_id, $data, $items, $payment, false, $si_return);
                    } else if ($ptype == 2 || $ptype == 3) {
                        $data['category_id'] = $items[0]['product_id'];
                        $this->site->wappsiContabilidadGastosDevolucion($purchase_id, $data, $items, $payment);;
                    }
                }
                $purchase = $this->site->getWappsiPurchaseById($data['purchase_id']);
                $total_retenciones =
                            (isset($data['rete_fuente_total']) && $data['rete_fuente_assumed'] == 0 ? $data['rete_fuente_total'] : 0) +
                            (isset($data['rete_iva_total']) && $data['rete_iva_assumed'] == 0 ? $data['rete_iva_total'] : 0) +
                            (isset($data['rete_ica_total']) && $data['rete_ica_assumed'] == 0 ? $data['rete_ica_total'] : 0) +
                            (isset($data['rete_bomberil_total']) && $data['rete_ica_assumed'] == 0 ? $data['rete_bomberil_total'] : 0) +
                            (isset($data['rete_autoaviso_total']) && $data['rete_ica_assumed'] == 0 ? $data['rete_autoaviso_total'] : 0) +
                            (isset($data['rete_other_total']) && $data['rete_other_assumed'] == 0 ? $data['rete_other_total'] : 0);

                if($data['payment_status'] == 'pending'){
                    //Traer datos de la venta
                    // Actualizar el campo paid en la venta
                    $paid = $purchase->paid + ($purchase->return_purchase_total * -1);
                    $tabla = 'purchases';
                    $this->db->update($tabla, array('paid' => $paid), array('id' => $data['purchase_id']));
                    $this->db->update($tabla, array('payment_status' => 'paid', 'paid' => $data['grand_total']), array('id' => $purchase_id));
                    // Insertando registro en la tabla de payments
                    $tabla = 'payments';
                    $payment = array();
                    $payment['date'] = $data['date'];
                    $payment['purchase_id'] = $data['purchase_id'];
                    $payment['amount'] = ($purchase->return_purchase_total - $total_retenciones) * -1;
                    $payment['reference_no'] = $purchase->return_purchase_ref;
                    $payment['paid_by'] = "due";
                    $payment['type'] = "devolución";
                    $payment['note'] = "Devolución factura Credito ".$data['reference_no'];
                    if ($total_retenciones > 0) {
                        $payment['rete_fuente_total'] = $data['rete_fuente_total'];
                        $payment['rete_iva_total'] = $data['rete_iva_total'];
                        $payment['rete_ica_total'] = $data['rete_ica_total'];
                        $payment['rete_other_total'] = $data['rete_other_total'];
                    }

                    $payment['created_by'] = $this->session->userdata('user_id');
                    $this->db->insert($tabla,$payment);
                } else if ($purchase->payment_status == 'partial') {
                    $tabla = 'payments';
                    $payment_add = array();
                    $payment_add['date'] = $data['date'];
                    $payment_add['purchase_id'] = $data['purchase_id'];
                    $payment_add['amount'] = ($data['grand_total'] * -1) - $total_retenciones - ($total_payment * -1);
                    $payment_add['reference_no'] = (!empty($purchase->return_purchase_ref) ? $purchase->return_purchase_ref : $data['reference_no']);
                    $payment_add['paid_by'] = "due";
                    $payment_add['type'] = "devolución";
                    $payment_add['note'] = "Saldo de devolución restante de crédito ".$data['reference_no'];
                    $payment_add['created_by'] = $this->session->userdata('user_id');
                    $this->db->insert($tabla,$payment_add);
                } else if ($data['payment_status'] == 'paid') {
                    $tabla = 'payments';
                    $payment_add = array();
                    $payment_add['date'] = $data['date'];
                    $payment_add['purchase_id'] = $data['purchase_id'];
                    $payment_add['amount'] = ($data['grand_total'] * -1) - $total_retenciones;
                    $payment_add['reference_no'] = (!empty($purchase->return_purchase_ref) ? $purchase->return_purchase_ref : $data['reference_no']);
                    $payment_add['paid_by'] = "due";
                    $payment_add['type'] = "devolución";
                    $payment_add['note'] = "Saldo de devolución restante de crédito ".$data['reference_no'];
                    $payment_add['created_by'] = $this->session->userdata('user_id');
                    $this->db->insert($tabla,$payment_add);
                }

            }else{

                if ($ptype == 1) {

                    if ($data['status'] != 'pending') {
                        foreach ($payment as $key => $pmnt) {
                            if (isset($pmnt['reference_no']) && $pmnt['reference_no'] == 'retencion') {
                                unset($payment[$key]);
                            }
                        }
                        $this->site->wappsiContabilidadCompras($purchase_id, $data, $items, $payment, $data_taxrate_traslate);
                    }

                } else if ($ptype == 2 || $ptype == 3) {
                    if ($data['status'] != 'pending') {
                        $this->site->wappsiContabilidadGastos($purchase_id, $data, $items, $payment);
                    }
                }

            }

            if ($ptype == 1) {
                if ($data['status'] == 'received' || $data['status'] == 'returned') {
                    $this->site->syncQuantity(NULL, $purchase_id);
                }
            }
            if ($unit_update_price && $this->Settings->update_prices_from_purchases == 1) {
                $txt_changed = "";
                foreach ($unit_update_price as $product_id => $uparr) {
                    $txt_changed_product = " product_id = ".$product_id." : ";
                    $txt_changed_product_bool = false;
                    foreach ($uparr as $up_id => $up_valor) {
                        $updata = $this->db->get_where('unit_prices', ['id_product'=>$product_id, 'unit_id'=>$up_id]);
                        if ($updata->num_rows() > 0) {
                            $updata = $updata->row();
                            if ($updata->valor_unitario != $up_valor) {
                                $this->db->update('unit_prices', ['valor_unitario'=>$up_valor, 'last_update'=>date('Y-m-d H:i:s')], ['id_product'=>$product_id, 'unit_id'=>$up_id]);
                                $txt_changed.=($txt_changed_product_bool == false ? $txt_changed_product : "" )." unit_id = ".$up_id." de ".$updata->valor_unitario." a ".$up_valor.", ";
                                if ($txt_changed_product_bool == false) {
                                    $txt_changed_product_bool = true;
                                }
                            }

                            if ($updata->margin_update_price != $unit_update_margin[$product_id][$up_id]) {
                                $this->db->update('unit_prices', ['margin_update_price'=>$unit_update_margin[$product_id][$up_id], 'last_update'=>date('Y-m-d H:i:s')], ['id_product'=>$product_id, 'unit_id'=>$up_id]);
                                $txt_changed.=($txt_changed_product_bool == false ? $txt_changed_product : "" )." unit_id = ".$up_id." margen de ".$updata->margin_update_price." a ".$unit_update_margin[$product_id][$up_id].", ";
                                if ($txt_changed_product_bool == false) {
                                    $txt_changed_product_bool = true;
                                }
                            }
                        } else {
                            $this->db->update('products', ['price'=>$up_valor, 'main_unit_margin_update_price'=>$unit_update_margin[$product_id][$up_id]], ['id'=>$product_id]);
                        }
                        if ($this->Settings->prioridad_precios_producto == 10) {
                            $unit_data = $this->db->get_where('units', ['id'=>$up_id]);
                            if ($unit_data->num_rows() > 0) {
                                $unit_data = $unit_data->row();
                                $this->db->update('product_prices', ['price'=>$up_valor], ['price_group_id'=>$unit_data->price_group_id, 'product_id'=>$product_id]);
                            }
                        }
                    }
                }

                if (!empty($txt_changed)) {
                    $txt_changed = "Precios cambiados desde la compra : ".$txt_changed;
                    $this->db->insert('user_activities', [
                        'date' => date('Y-m-d H:i:s'),
                        'type_id' => 1,
                        'table_name' => 'purchases',
                        'record_id' => $purchase_id,
                        'user_id' => $this->session->userdata('user_id'),
                        'module_name' => $this->m,
                        'description' => $txt_changed,
                    ]);
                }
            }

            if ($pg_update_price && $this->Settings->update_prices_from_purchases == 1) {
                $txt_changed = "";
                foreach ($pg_update_price as $product_id => $pgarr) {
                    $txt_changed_product = " product_id = ".$product_id." : ";
                    $txt_changed_product_bool = false;
                    foreach ($pgarr as $pg_id => $pg_valor) {
                        $pg_data = $this->db->get_where('product_prices', ['product_id'=>$product_id, 'price_group_id'=>$pg_id]);
                        if ($pg_data->num_rows() > 0) {
                            $pg_data = $pg_data->row();
                            if ($pg_data->price != $pg_valor) {
                                $this->db->update('product_prices', ['price'=>$pg_valor, 'last_update'=>date('Y-m-d H:i:s')], ['product_id'=>$product_id, 'price_group_id'=>$pg_id]);
                                $txt_changed.=($txt_changed_product_bool == false ? $txt_changed_product : "" )."price_group_id = ".$pg_id." de ".$pg_data->price." a ".$pg_valor.", ";
                                if ($txt_changed_product_bool == false) {
                                    $txt_changed_product_bool = true;
                                }
                            }
                        }
                    }
                }
                if (!empty($txt_changed)) {
                    $txt_changed = "Precios cambiados desde la compra : ".$txt_changed;
                    $this->db->insert('user_activities', [
                        'date' => date('Y-m-d H:i:s'),
                        'type_id' => 1,
                        'table_name' => 'purchases',
                        'record_id' => $purchase_id,
                        'user_id' => $this->session->userdata('user_id'),
                        'module_name' => $this->m,
                        'description' => $txt_changed,
                    ]);
                }
            }
            // exit('A');
            return $purchase_id;
        }
        return false;
    }

    public function updatePurchase($id, $data, $items = array(), $payment = array(), $prev_reference = null, $new_pv = [], $pg_update_price = [])
    {
        $opurchase = $this->getPurchaseByID($id);
        $oitems = $this->getAllPurchaseItems($id);
        $opayments = $this->get_purchase_direct_payments($id);
        $delete_items = [];
        $item_cost = [];
        $item_qty = [];
        $purchase_type = isset($data['purchase_type']) ? $data['purchase_type'] : 1;
        foreach ($oitems as $oitem) {
            $delete_items[$oitem->id] = 1;
            $item_net_cost[$oitem->id] = $oitem->net_unit_cost;
            $item_cost[$oitem->id] = $oitem->unit_cost;
            $item_qty[$oitem->id] = $oitem->quantity;
        }
        foreach ($items as $item) {
            unset($delete_items[$item['id']]);
            if ($purchase_type == 1 && ($data['status'] == 'received' || $data['status'] == 'partial')) {
                $update_costing = false;
                if ( (isset($item_cost[$item['id']]) && $item_cost[$item['id']] != $item['unit_cost']) ) {
                    $update_costing = true;
                } else if ( (isset($item_qty[$item['id']]) && $item_qty[$item['id']] != $item['quantity']) ) {
                    $update_costing = true;
                }
                if ($update_costing) {
                    $item['quantity'] = ($item_qty[$item['id']] * -1);
                    $item['unit_cost'] = $item_cost[$item['id']];
                    $item['purchase_id'] = $id;
                    $this->site->updateAVCO($item);
                }
            }
        }

        $this->db->update('purchases',
                                    [
                                        'rete_fuente_percentage' => NULL,
                                        'rete_fuente_total' => NULL,
                                        'rete_fuente_account' => NULL,
                                        'rete_fuente_base' => NULL,
                                        'rete_fuente_id' => NULL,
                                        'rete_iva_percentage' => NULL,
                                        'rete_iva_total' => NULL,
                                        'rete_iva_account' => NULL,
                                        'rete_iva_base' => NULL,
                                        'rete_iva_id' => NULL,
                                        'rete_ica_percentage' => NULL,
                                        'rete_ica_total' => NULL,
                                        'rete_ica_account' => NULL,
                                        'rete_ica_base' => NULL,
                                        'rete_ica_id' => NULL,
                                        'rete_other_percentage' => NULL,
                                        'rete_other_total' => NULL,
                                        'rete_other_account' => NULL,
                                        'rete_other_base' => NULL,
                                        'rete_other_id' => NULL,
                                        'rete_fuente_assumed' => NULL,
                                        'rete_iva_assumed' => NULL,
                                        'rete_ica_assumed' => NULL,
                                        'rete_other_assumed' => NULL,
                                        'rete_bomberil_percentage' => NULL,
                                        'rete_bomberil_total' => NULL,
                                        'rete_bomberil_account' => NULL,
                                        'rete_bomberil_base' => NULL,
                                        'rete_bomberil_id' => NULL,
                                        'rete_autoaviso_percentage' => NULL,
                                        'rete_autoaviso_total' => NULL,
                                        'rete_autoaviso_account' => NULL,
                                        'rete_autoaviso_base' => NULL,
                                        'rete_autoaviso_id' => NULL,
                                        'rete_fuente_assumed_account' => NULL,
                                        'rete_iva_assumed_account' => NULL,
                                        'rete_ica_assumed_account' => NULL,
                                        'rete_bomberil_assumed_account' => NULL,
                                        'rete_autoaviso_assumed_account' => NULL,
                                        'rete_other_assumed_account' => NULL
                                    ]
                                    ,['id'=>$id]);
        $this->db->delete('payments', ['purchase_id' => $id, 'paid_by' => 'retencion']);
        foreach ($delete_items as $purchase_item_id => $set) {
            $this->db->delete('purchase_items', array('id' => $purchase_item_id, 'purchase_id' => $id));
        }
        $update_ref = false;
        if ($this->Settings->management_consecutive_suppliers == 0) {
            $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id'], false);
            if ($data['reference_no']) {
                $refbil = explode("-", $referenceBiller);
                $reference = $refbil[0]."-".$data['reference_no'];
            } else {
                $reference = $referenceBiller;
                if ($opurchase->document_type_id !=  $data['document_type_id']) {
                    $update_ref = true;
                }
            }
        } else{
            $reference = $opurchase->reference_no;
        }

        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        $delete_payments = false;
        if (
                $opurchase->grand_total != $data['grand_total'] ||
                (isset($data['payment_status']) && $opurchase->payment_status != $data['payment_status']) ||
                (isset($data['paid']) && $opurchase->paid != $data['paid']) ||
                (isset($data['rete_fuente_total']) && $opurchase->rete_fuente_total != $data['rete_fuente_total']) ||
                (isset($data['rete_iva_total']) && $opurchase->rete_iva_total != $data['rete_iva_total']) ||
                (isset($data['rete_ica_total']) && $opurchase->rete_ica_total != $data['rete_ica_total']) ||
                (isset($data['rete_other_total']) && $opurchase->rete_other_total != $data['rete_other_total']) ||
                !isset($data['rete_fuente_total']) && !empty($opurchase->rete_fuente_total) ||
                !isset($data['rete_iva_total']) && !empty($opurchase->rete_iva_total) ||
                !isset($data['rete_ica_total']) && !empty($opurchase->rete_ica_total) ||
                !isset($data['rete_other_total']) && !empty($opurchase->rete_other_total)
            ) {
            $delete_payments = true;
        }

        if ($this->db->update('purchases', $data, array('id' => $id)) ) {
            $purchase_id = $id;
            if ($update_ref) {
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            }
            foreach ($items as $item) {
                $profitability_margin = $item['profitability_margin'];
                unset($item['profitability_margin']);
                $item['purchase_id'] = $id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                $purchase_item_exists = $this->db->get_where('purchase_items', array('id' => $item['id'], 'product_id' => $item['product_id']));

                if ($this->Settings->product_variant_per_serial == 1 && !empty($item['serial_no']) && $data['status'] == 'received') {
                    $v_exist = $this->db->get_where('product_variants', ['name'=>$item['serial_no'], 'product_id' => $item['product_id']]);
                    if ($v_exist->num_rows() > 0) {
                        $v_exist = $v_exist->row();
                        $item['option_id'] =  $v_exist->id;
                    } else {
                        $DB2 = false;
                        $new_id = NULL;
                        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
                            $DB2 = $this->settings_model->connect_new_year_database();
                            if (!$this->settings_model->check_same_id_insert($DB2, 'product_variants')) {
                                $new_id = $this->settings_model->get_new_year_new_id_for_insertion($DB2, 'product_variants');
                            }
                        }
                        $this->db->insert('product_variants', [
                                                        'id' => $new_id,
                                                        'product_id' => $item['product_id'],
                                                        'name' => $item['serial_no'],
                                                        'cost' => $item['unit_cost'],
                                                        'price' => 0,
                                                        'quantity' => $item['quantity'],
                                                            ]);
                        if ($DB2) {
                            $DB2->insert('product_variants', [
                                                        'id' => $new_id,
                                                        'product_id' => $item['product_id'],
                                                        'name' => $item['serial_no'],
                                                        'cost' => $item['unit_cost'],
                                                        'price' => 0,
                                                        'quantity' => $item['quantity'],
                                                            ]);
                        }
                        $item['option_id'] =  $this->db->insert_id();
                        $this->db->insert('warehouses_products_variants', [
                                            'product_id' => $item['product_id'],
                                            'warehouse_id' => $item['warehouse_id'],
                                            'option_id' => $item['option_id'],
                                            'quantity' => $item['quantity']
                                            ]);
                    }
                }

                if ($this->Settings->management_weight_in_variants == 1 && isset($item['new_option_assigned'])) {
                    $new_option_assigned = $item['new_option_assigned'];
                    unset($item['new_option_assigned']);
                    if (isset($new_pv[$new_option_assigned])) {
                        $new_option_assigned_data = $new_pv[$new_option_assigned];
                        $this->db->insert('product_variants', [
                                                        'product_id' => $new_option_assigned_data['product_id'],
                                                        'name' => $new_option_assigned_data['name'],
                                                        'code' => $new_option_assigned_data['code'],
                                                        'pv_code_consecutive' => $new_option_assigned_data['pv_code_consecutive'],
                                                        'weight' => $new_option_assigned_data['weight'],
                                                        'cost' => $item['unit_cost'],
                                                        'quantity' => $item['quantity'],
                                                            ]);
                        $item['option_id'] =  $this->db->insert_id();
                        $this->db->insert('warehouses_products_variants', [
                                            'product_id' => $item['product_id'],
                                            'warehouse_id' => $item['warehouse_id'],
                                            'option_id' => $item['option_id'],
                                            'quantity' => $item['quantity']
                                            ]);
                    }
                }

                if ($purchase_item_exists->num_rows() > 0) {
                    $this->db->update('purchase_items', $item, array('id' => $item['id'], 'product_id' => $item['product_id']));
                } else {
                    unset($item['id']);
                    $this->db->insert('purchase_items', $item);
                }
                if ($this->Settings->update_cost && $purchase_type == 1) {
                    $this->db->update('products', array('cost' => $item['unit_cost'], 'profitability_margin' => $profitability_margin), array('id' => $item['product_id']));
                }
                if ($opurchase->status == 'pending' && $data['status'] != 'pending' && $purchase_type == 1) {
                    $item['purchase_id'] = $id;
                    $this->site->updateAVCO($item);
                } else {
                    if ($purchase_type == 1 && ($data['status'] == 'received' || $data['status'] == 'partial')) {
                        $update_costing = false;
                        if ( (isset($item_cost[$item['id']]) && $item_cost[$item['id']] != $item['unit_cost']) || !isset($item_cost[$item['id']])  ) {
                            $update_costing = true;
                        } else if ( (isset($item_qty[$item['id']]) && $item_qty[$item['id']] != $item['quantity']) || !isset($item_qty[$item['id']]) ) {
                            $update_costing = true;
                        }
                        if ($update_costing) {
                            $item['prev_quantity'] = (isset($item_qty[$item['id']]) ? $item_qty[$item['id']] : 0);
                            $item['purchase_id'] = $id;
                            $this->site->updateAVCO($item);
                        }
                    }
                }
                if ($purchase_type == 1) {
                    $last_purchase = $this->db->where('product_id', $item['product_id'])->where('purchase_id IS NOT NULL')->order_by('purchase_id desc')->get('purchase_items', 1);
                    $is_last_purchase = false;
                    if ($last_purchase->num_rows() > 0) {
                        $last_purchase = $last_purchase->row();
                        if ($last_purchase->purchase_id == $id) {
                            $is_last_purchase = true;
                        }
                    }
                    if ($this->Settings->update_cost && $is_last_purchase) {
                        $this->db->update('products', array('cost' => $item['unit_cost']), array('id' => $item['product_id']));
                    }
                }
                    
            }
            $this->site->syncQuantity(NULL, NULL, $oitems);
            $this->site->syncQuantity(NULL, $id);
            $payment_references = false;
            if ($opayments && $delete_payments) {
                foreach ($opayments as $row) {
                    if ($row->paid_by == 'deposit') {
                        $deposit = $this->db->get_where('deposits', ['id' => $row->affected_deposit_id])->row();
                        $this->db->update('deposits', ['balance'=> $deposit->balance+($row->amount)], ['id'=>$deposit->id]);
                        $company = $this->db->get_where('companies', ['id' => $deposit->company_id])->row();
                        $this->db->update('companies', ['deposit_amount'=> $company->deposit_amount+($row->amount)], ['id'=>$company->id]);
                    }
                    $this->db->delete('payments', array('id' => $row->id));
                    $payment_references[] = $row->reference_no;
                }
            }
            ///REVISAR TEMA DE  CONSECUTIVOS DE PAGO
            $cnt_references = 0;
            $total_payment = 0;
            if (($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid') || !empty($payment)) {
                foreach ($payment as $pmnt) {
                    $pmnt['purchase_id'] = $id;
                    $update_pmnt_reference = false;
                    if ($delete_payments && $pmnt['paid_by'] != 'retencion' && isset($pmnt['document_type_id'])) {
                        if ($pmnt['paid_by'] != 'deposit') {
                            if (isset($payment_references[$cnt_references])) {
                                $pmnt['reference_no'] = $payment_references[$cnt_references];
                                unset($payment_references[$cnt_references]);
                            } else {
                                $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt['document_type_id'], false);
                                $reference = $referenceBiller;
                                $pmnt['reference_no'] = $reference;
                                $ref = explode("-", $reference);
                                $p_consecutive = $ref[1];
                                $update_pmnt_reference = true;
                            }
                            $pmnt['purchase_id'] = $purchase_id;
                            if ($this->db->insert('payments', $pmnt) && $update_pmnt_reference) {
                                $this->site->updateBillerConsecutive($pmnt['document_type_id'], $p_consecutive+1);
                            }
                            if ($pmnt['paid_by'] != 'due') {
                                $total_payment += $pmnt['amount'];
                            }
                            $cnt_references++;
                        } else {
                            $p_arr[] = $pmnt;
                            $p_arr = $this->site->set_payments_affected_by_deposits($data['supplier_id'], $p_arr);
                            $count = 0;
                            foreach ($p_arr as $pmnt2) {
                                $update_pmnt_reference = false;
                                if (isset($payment_references[$cnt_references])) {
                                    $pmnt['reference_no'] = $payment_references[$cnt_references];
                                    unset($payment_references[$cnt_references]);
                                } else {
                                    $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt2['document_type_id']);
                                    $reference = $referenceBiller;
                                    $pmnt2['reference_no'] = $reference;
                                    $ref = explode("-", $reference);
                                    $p_consecutive = $ref[1];
                                    $update_pmnt_reference = true;
                                }
                                if ($this->db->insert('payments', $pmnt2) && $update_pmnt_reference) {
                                    $this->site->updateBillerConsecutive($pmnt2['document_type_id'], $p_consecutive+1);
                                }
                                $count++;
                                $cnt_references++;
                            }
                        }
                    }
                    else if (!$delete_payments && $pmnt['paid_by'] != 'retencion' && isset($pmnt['document_type_id'])) { 
                        // delete_payments es una variable de control que pasa a true, si cambia algun valor de pago, como el total,
                        // retenciones, descuentos etc.. con el fin de no modificar ese funcionamiento se agrega esta condicion en la
                        // cual entraran los pagos cuando no cambian su valor es decir solo estarian modificando el medio mas no el valor
                        if ($pmnt['paid_by'] != 'deposit') {
                            $this->db->update('payments', ['paid_by' => $pmnt['paid_by']], ['id' => $opayments[0]->id]);
                            if ($pmnt['paid_by'] != 'due') {
                                $total_payment += $pmnt['amount'];
                            }
                            $cnt_references++;
                        }
                    } 
                    else if ($pmnt['paid_by'] == 'retencion') {
                        $rt = $this->db->get_where('payments', ['purchase_id'=>$id, 'paid_by'=>'retencion']);
                        if ($rt->num_rows() > 0) {
                            $rt = $rt->row();
                            $this->db->update('payments', $pmnt, ['id'=>$rt->id]);
                        } else {
                            $this->db->insert('payments', $pmnt);
                        }
                    }
                }
            }

            $this->site->syncPurchasePayments($id);
            if ($data['status'] == 'received') {
                $this->recontabilizarCompra($id, $prev_reference, true);
            }
            if ($opurchase->return_id) {
                $this->db->update('purchases',
                                    [
                                        'supplier_id' => $data['supplier_id'],
                                        'supplier' => $data['supplier'],
                                    ],
                                    [
                                        'id' => $opurchase->return_id,
                                    ]
                                );

                $this->recontabilizarCompra($opurchase->return_id, null, true);
            }

            if ($pg_update_price && $this->Settings->update_prices_from_purchases == 1) {
                $txt_changed = "";
                foreach ($pg_update_price as $product_id => $pgarr) {
                    $txt_changed_product = " product_id = ".$product_id." : ";
                    $txt_changed_product_bool = false;
                    foreach ($pgarr as $pg_id => $pg_valor) {
                        $pg_data = $this->db->get_where('product_prices', ['product_id'=>$product_id, 'price_group_id'=>$pg_id]);
                        if ($pg_data->num_rows() > 0) {
                            $pg_data = $pg_data->row();
                            if ($pg_data->price != $pg_valor) {
                                $this->db->update('product_prices', ['price'=>$pg_valor, 'last_update'=>date('Y-m-d H:i:s')], ['product_id'=>$product_id, 'price_group_id'=>$pg_id]);
                                $txt_changed.=($txt_changed_product_bool == false ? $txt_changed_product : "" )." price_group_id = ".$pg_id." de ".$pg_data->price." a ".$pg_valor.", ";
                                if ($txt_changed_product_bool == false) {
                                    $txt_changed_product_bool = true;
                                }
                            }
                        }
                    }
                }
                // exit(var_dump($txt_changed));
                if (!empty($txt_changed)) {
                    $txt_changed = "Precios cambiados desde la compra : ".$txt_changed;
                    $this->db->insert('user_activities', [
                        'date' => date('Y-m-d H:i:s'),
                        'type_id' => 1,
                        'table_name' => 'purchases',
                        'record_id' => $purchase_id,
                        'user_id' => $this->session->userdata('user_id'),
                        'module_name' => $this->m,
                        'description' => $txt_changed,
                    ]);
                }
            }

            return true;
        }
        return false;
    }

    public function updateStatus($id, $status, $note)
    {
        $items = $this->site->getAllPurchaseItems($id);
        $purchase = $this->getPurchaseByID($id);
        if ($purchase->purchase_type == 1) {
            if ($this->db->update('purchases', array('status' => $status, 'note' => $note), array('id' => $id))) {
                foreach ($items as $item) {
                    $qb = $status == 'completed' ? ($item->quantity_balance + ($item->quantity - $item->quantity_received)) : $item->quantity_balance;
                    $qr = $status == 'completed' ? $item->quantity : $item->quantity_received;
                    $this->db->update('purchase_items', array('status' => $status, 'quantity_balance' => $qb, 'quantity_received' => $qr), array('id' => $item->id));
                    $item_arr=[];
                    foreach ($item as $key => $value) {
                        $item_arr[$key] = $value;
                    }
                    $item_arr['unit_cost'] = $item_arr['unit_cost']-$item_arr['shipping_unit_cost'];
                    $item_arr['purchase_item_id'] = $item->id;
                    $this->site->updateAVCO($item_arr);
                    $p_s_data = $this->getProductByID($item->product_id);
                    $to_update_cost = ($this->Settings->tax_method == 0 ? $item->unit_cost : $item->net_unit_cost);
                    if ($this->Settings->tax_method == 0 && $item->original_tax_rate_id != NULL) {
                        $tax = $this->site->getTaxRateByID($item->original_tax_rate_id);
                        $to_update_cost = $this->Settings->tax_method == 0 ? $to_update_cost - $item->shipping_unit_cost : $to_update_cost;
                        $to_update_cost_tax = $this->site->calculateTax(NULL, $tax, $to_update_cost, NULL, 1);
                        $to_update_cost += $to_update_cost_tax['amount'];
                        $to_update_cost = $this->Settings->tax_method == 0 ? $to_update_cost + $item->shipping_unit_cost : $to_update_cost;
                    }
                    if ($purchase->supplier_id == $p_s_data->supplier1) {
                        $this->db->update('products', ['supplier1price' => $to_update_cost, 'supplier1price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                        if ($item->supplier_part_no) {
                            $this->db->update('products', ['supplier1_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                        }
                    } else if ($purchase->supplier_id == $p_s_data->supplier2) {
                        $this->db->update('products', ['supplier2price' => $to_update_cost, 'supplier2price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                        if ($item->supplier_part_no) {
                            $this->db->update('products', ['supplier2_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                        }
                    } else if ($purchase->supplier_id == $p_s_data->supplier3) {
                        $this->db->update('products', ['supplier3price' => $to_update_cost, 'supplier3price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                        if ($item->supplier_part_no) {
                            $this->db->update('products', ['supplier3_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                        }
                    } else if ($purchase->supplier_id == $p_s_data->supplier4) {
                        $this->db->update('products', ['supplier4price' => $to_update_cost, 'supplier4price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                        if ($item->supplier_part_no) {
                            $this->db->update('products', ['supplier4_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                        }
                    } else if ($purchase->supplier_id == $p_s_data->supplier5) {
                        $this->db->update('products', ['supplier5price' => $to_update_cost, 'supplier5price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                        if ($item->supplier_part_no) {
                            $this->db->update('products', ['supplier5_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                        }
                    } else {
                        if (!$p_s_data->supplier1price_date || ((!empty($p_s_data->supplier5price_date) && !empty($p_s_data->supplier4price_date) && !empty($p_s_data->supplier3price_date) && !empty($p_s_data->supplier2price_date)) && ($p_s_data->supplier5price_date > $p_s_data->supplier4price_date) && ($p_s_data->supplier5price_date > $p_s_data->supplier3price_date) && ($p_s_data->supplier5price_date > $p_s_data->supplier2price_date) && ($p_s_data->supplier5price_date > $p_s_data->supplier1price_date))) {
                            $this->db->update('products', ['supplier1' => $purchase->supplier_id, 'supplier1price' => $to_update_cost, 'supplier1price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                            if ($item->supplier_part_no) {
                                $this->db->update('products', ['supplier1_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                            }
                        } else if (!$p_s_data->supplier2price_date || ((!empty($p_s_data->supplier5price_date) && !empty($p_s_data->supplier4price_date) && !empty($p_s_data->supplier3price_date) && !empty($p_s_data->supplier1price_date)) && ($p_s_data->supplier1price_date > $p_s_data->supplier2price_date) && ($p_s_data->supplier1price_date > $p_s_data->supplier3price_date) && ($p_s_data->supplier1price_date > $p_s_data->supplier4price_date) && ($p_s_data->supplier1price_date > $p_s_data->supplier5price_date))) {
                            $this->db->update('products', ['supplier2' => $purchase->supplier_id, 'supplier2price' => $to_update_cost, 'supplier2price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                            if ($item->supplier_part_no) {
                                $this->db->update('products', ['supplier2_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                            }
                        } else if (!$p_s_data->supplier3price_date || ((!empty($p_s_data->supplier5price_date) && !empty($p_s_data->supplier4price_date) && !empty($p_s_data->supplier1price_date) && !empty($p_s_data->supplier2price_date)) && ($p_s_data->supplier2price_date > $p_s_data->supplier1price_date) && ($p_s_data->supplier2price_date > $p_s_data->supplier3price_date) && ($p_s_data->supplier2price_date > $p_s_data->supplier4price_date) && ($p_s_data->supplier2price_date > $p_s_data->supplier5price_date))) {
                            $this->db->update('products', ['supplier3' => $purchase->supplier_id, 'supplier3price' => $to_update_cost, 'supplier3price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                            if ($item->supplier_part_no) {
                                $this->db->update('products', ['supplier3_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                            }
                        } else if (!$p_s_data->supplier4price_date || ((!empty($p_s_data->supplier5price_date) && !empty($p_s_data->supplier1price_date) && !empty($p_s_data->supplier3price_date) && !empty($p_s_data->supplier2price_date)) && ($p_s_data->supplier3price_date > $p_s_data->supplier1price_date) && ($p_s_data->supplier3price_date > $p_s_data->supplier2price_date) && ($p_s_data->supplier3price_date > $p_s_data->supplier4price_date) && ($p_s_data->supplier3price_date > $p_s_data->supplier5price_date))) {
                            $this->db->update('products', ['supplier4' => $purchase->supplier_id, 'supplier4price' => $to_update_cost, 'supplier4price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);
                            if ($item->supplier_part_no) {
                                $this->db->update('products', ['supplier4_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                            }
                        } else if (!$p_s_data->supplier5price_date || ((!empty($p_s_data->supplier1price_date) && !empty($p_s_data->supplier4price_date) && !empty($p_s_data->supplier3price_date) && !empty($p_s_data->supplier2price_date)) && ($p_s_data->supplier4price_date > $p_s_data->supplier1price_date) && ($p_s_data->supplier4price_date > $p_s_data->supplier2price_date) && ($p_s_data->supplier4price_date > $p_s_data->supplier3price_date) && ($p_s_data->supplier4price_date > $p_s_data->supplier5price_date))) {

                            $this->db->update('products', ['supplier5' => $purchase->supplier_id, 'supplier5price' => $to_update_cost, 'supplier5price_date' => date('Y-m-d H:i:s')], ['id'=>$item->product_id]);

                            if ($item->supplier_part_no) {
                                $this->db->update('products', ['supplier5_part_no' => $item->supplier_part_no], ['id'=>$item->product_id]);
                            }
                        }
                    }
                    if ($this->Settings->tax_method == 0) {
                        $tax = $this->site->getTaxRateByID($p_s_data->tax_rate);
                        //Aumento IVA FLETE
                        $item_shipping_unit_cost = $item->shipping_unit_cost;
                        $item_shipping_unit_cost_tax = $this->site->calculateTax(NULL, $tax, $item_shipping_unit_cost, NULL, 1);
                        $to_update_cost += $item_shipping_unit_cost_tax['amount']; //se suma sólo iva de flete por que flete unitario ya está sumado en el costo final de la compra
                        //Aumento IVA AJUSTE COSTO
                        $item_import_adjustment_cost = $item->import_adjustment_cost;
                        $item_import_adjustment_cost_tax = $this->site->calculateTax(NULL, $tax, $item_import_adjustment_cost, NULL, 1);
                        $item_import_adjustment_cost += $item_import_adjustment_cost_tax['amount'];
                        $to_update_cost += $item_import_adjustment_cost; //se suma ajuste+iva ya que el ajuste si se guarda aparte sin afectar el costo final de la compra
                    } else if ($this->Settings->tax_method == 1) {
                        $to_update_cost += $item->shipping_unit_cost;
                        $to_update_cost += $item->import_adjustment_cost;
                    }
                    $this->db->update('products', array('cost' => ($to_update_cost)), array('id' => $item->product_id));
                }
                $this->site->syncQuantity(NULL, NULL, $items);
                $this->site->syncQuantity(NULL, $id);
                return true;
            }
        } else {
            $this->db->update('purchases', array('status' => $status, 'note' => $note), array('id' => $id));
            foreach ($items as $item) {
                $this->db->update('purchase_items', array('status' => ($status == 'received' ? 'service_received' : $status)), array('id' => $item->id));
            }
        }
        $msg = $this->recontabilizarCompra($id);
        return false;
    }

    public function deletePurchase($id)
    {
        $purchase = $this->getPurchaseByID($id);
        $purchase_items = $this->site->getAllPurchaseItems($id);
        if ($this->db->delete('purchase_items', array('purchase_id' => $id)) && $this->db->delete('purchases', array('id' => $id))) {
            $this->db->delete('payments', array('purchase_id' => $id));
            if ($purchase->status == 'received' || $purchase->status == 'partial') {
                foreach ($purchase_items as $oitem) {
                    $item_arr=[];
                    foreach ($item as $key => $value) {
                        $item_arr[$key] = $value;
                    }
                    $item_arr['quantity']=$item_arr['quantity']*-1;
                    $item_arr['unit_cost']=$item_arr['real_unit_cost'];
                    $this->site->updateAVCO($item_arr);
                    $received = $oitem->quantity_received ? $oitem->quantity_received : $oitem->quantity;
                    if ($oitem->quantity_balance < $received) {
                        $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $oitem->product_id, 'warehouse_id' => $oitem->warehouse_id, 'option_id' => $oitem->option_id);
                        $this->site->setPurchaseItem($clause, ($oitem->quantity_balance - $received));
                    }
                }
            }
            $this->site->syncQuantity(NULL, NULL, $purchase_items);
            return true;
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehousesProductQuantity($product_id)
    {
        $q = $this->db->select_sum('quantity')->select('avg_cost')->where('product_id', $product_id)->get('warehouses_products');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasePayments($purchase_id, $without_retention = false)
    {
        $this->db->order_by('id', 'asc');
        $conditions['purchase_id'] = $purchase_id;
        if ($without_retention) {
            $conditions['paid_by !='] = 'retencion';
        }
        $q = $this->db->get_where('payments', $conditions);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPaymentByID($id)
    {
        $q = $this->db->get_where('payments', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getPaymentsForPurchase($purchase_id, $arr = false)
    {
        $this->db->select('
                            payments.*,
                            users.first_name,
                            users.last_name,
                            type,
                            payment_methods.name,
                            (
                                COALESCE('.$this->db->dbprefix('payments').'.rete_fuente_total, 0) +
                                COALESCE('.$this->db->dbprefix('payments').'.rete_iva_total, 0) +
                                COALESCE('.$this->db->dbprefix('payments').'.rete_ica_total, 0) +
                                COALESCE('.$this->db->dbprefix('payments').'.rete_other_total, 0) +
                                COALESCE('.$this->db->dbprefix('payments').'.rete_autoaviso_total, 0) +
                                COALESCE('.$this->db->dbprefix('payments').'.rete_bomberil_total, 0)
                            ) as ce_total_retention
                        ')
            ->join('users', 'users.id=payments.created_by', 'left')
            ->join('payment_methods', 'payment_methods.code=payments.paid_by', 'left');
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {

            if ($arr == false) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                foreach (($q->result_array()) as $row) {
                    $row['purchase_id'] = $purchase_id;
                    $data[] = $row;
                }
            }

            return $data;
        }
        return FALSE;
    }

    public function getPaymentsForPurchaseDev($purchase_id, $arr = false)
    {
        $this->db->select('payments.date, payments.paid_by, payments.amount, payments.cc_no, payments.cheque_no, payments.reference_no, users.first_name, users.last_name, type')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id, 'type' => 'returned'));
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                $data = $q->row();
            } else {
                $data = $q->row_array();
            }
            return $data;
        }
        return FALSE;
    }

    public function getPaymentsForPurchaseReAccounting($purchase_id, $arr = false)
    {
        $this->db->select('payments.*, users.first_name, users.last_name, type')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id, 'type !=' => 'devolución'));
        if ($q->num_rows() > 0) {

            if ($arr == false) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                foreach (($q->result_array()) as $row) {
                    $row['purchase_id'] = $purchase_id;
                    $data[] = $row;
                }
            }

            return $data;
        }
        return FALSE;
    }

    public function addPayment($data = array(), $retencion = array(), $data_taxrate_traslate = array())
    {
        //SI ES PAGO A GASTO, SE CONSULTA LA CUENTA QUE SE CONFIGURÓ CÓMO ACREEDORA EN LA CATEGORÍA DE GASTO
        $purchase = $this->getPurchaseByID($data['purchase_id']);
        $expense_payment = false;
        if ($purchase->purchase_type == 2 || $purchase->purchase_type == 3) {
            $purchase_items = $this->getAllPurchaseItems($data['purchase_id']);
            $expense_category = $this->site->getExpenseCategory($purchase_items[0]->product_id);
            $expense_payment = $expense_category->creditor_ledger_id;
        }


        // exit($purchase->purchase_type);

        //TRATAMIENTO DE PAGO INDIVIDUAL CON RETENCIÓN APLICADA
        if ($retencion != null) {

            $rete_payment = array(
                'date'         => $data['date'],
                'purchase_id'  => $data['purchase_id'],
                'amount'       => $retencion['total_retenciones'],
                'reference_no' => 'retencion',
                'paid_by'      => 'retencion',
                'cheque_no'    => '',
                'cc_no'        => '',
                'cc_holder'    => '',
                'cc_month'     => '',
                'cc_year'      => '',
                'cc_type'      => '',
                'created_by'   => $this->session->userdata('user_id'),
                'type'         => 'sent',
                'note'         => 'Retenciones',
            );

            unset($retencion['total_retenciones']);

            // exit(var_dump($data)."<br>".var_dump($retencion)."<br>".var_dump($rete_payment));

            if ($this->db->insert('payments', $rete_payment)) { //SE INSERTA EL PAGO DE RETENCIÓN
                if ($this->db->update('purchases', $retencion, array('id' => $data['purchase_id']))) { //SE ACTUALIZAN DATOS DE RETENCIÓN EN LA COMPRA
                    # code...
                } else {
                    exit('Error al actualizar base de datos : '.$this->db->last_query());
                }
            } else {
                return FALSE;
            }

        }
        //TRATAMIENTO DE PAGO INDIVIDUAL CON RETENCIÓN APLICADA


        if ($this->db->insert('payments', $data)) {
            if ($this->site->getReference('ppay') == $data['reference_no']) {
                $this->site->updateReference('ppay');
            }
            $this->site->syncPurchasePayments($data['purchase_id']);
            if ($this->Settings->modulary == 1 && $data['paid_by'] != 'retencion') { //Si se indicó que el POS es modular con Contabilidad.
                /* LLamado a la función wappsiContabilidad para pagos de compras
                para alimentar las tablas del modulo de contabilidad */
                if($this->site->wappsiContabilidadPagoCompras($data, $retencion, $data_taxrate_traslate, $expense_payment)){

                }
            }
            return true;
        }
        return false;
    }

    public function updatePayment($id, $data = array())
    {
        if ($this->db->update('payments', $data, array('id' => $id))) {
            $this->site->syncPurchasePayments($data['purchase_id']);
            return true;
        }
        return false;
    }

    public function deletePayment($id)
    {
        $opay = $this->getPaymentByID($id);
        if ($this->db->delete('payments', array('id' => $id))) {
            $this->site->syncPurchasePayments($opay->purchase_id);
            return true;
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $option_id = NULL)
    {
        $this->db->select('product_variants.*, CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "") ) as name')
                ->where('product_id', $product_id);
        if ($option_id != NULL) {
            $this->db->where('id', $option_id);
        }
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getProductVariantByCode($code, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('code' => $code, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getExpenseByID($id, $arr = false)
    {
        $q = $this->db->get_where('expenses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            if ($arr) {
                return $q->row_array();
            } else {
                return $q->row();
            }
        }
        return FALSE;
    }

    public function addExpense($data = array(), $item = array())
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $ref = explode("-", $reference);
        $data['reference'] = $reference;
        $consecutive = $ref[1];
        if ($this->db->insert('expenses', $data)) {
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            $this->session->set_userdata('post_sending_saved', 1);
            $this->site->wappsiContabilidadGastosPOS(null, $data, null, $item);
            return true;
        }
        return false;
    }

    public function updateExpense($id, $data = array(), $item = array())
    {
        if ($this->db->update('expenses', $data, array('id' => $id))) {
            $data['expense_edit'] = true;
            $this->site->wappsiContabilidadGastosPOS(null, $data, null, $item);
            return true;
        }
        return false;
    }

    public function deleteExpense($id)
    {
        if ($this->db->delete('expenses', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getQuoteByID($id)
    {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id)
    {
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('return_purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllReturnItems($return_id)
    {
        $this->db->select('return_purchase_items.*, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=return_purchase_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=return_purchase_items.option_id', 'left')
            ->group_by('return_purchase_items.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('return_purchase_items', array('return_id' => $return_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPurcahseItemByID($id)
    {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function returnPurchase($data = array(), $items = array())
    {

        $purchase_items = $this->site->getAllPurchaseItems($data['purchase_id']);

        if ($this->db->insert('return_purchases', $data)) {
            $return_id = $this->db->insert_id();
            if ($this->site->getReference('rep') == $data['reference_no']) {
                $this->site->updateReference('rep');
            }
            foreach ($items as $item) {
                $item['return_id'] = $return_id;
                $this->db->insert('return_purchase_items', $item);

                if ($purchase_item = $this->getPurcahseItemByID($item['purchase_item_id'])) {
                    if ($purchase_item->quantity == $item['quantity']) {
                        $this->db->delete('purchase_items', array('id' => $item['purchase_item_id']));
                    } else {
                        $nqty = $purchase_item->quantity - $item['quantity'];
                        $bqty = $purchase_item->quantity_balance - $item['quantity'];
                        $rqty = $purchase_item->quantity_received - $item['quantity'];
                        $tax = $purchase_item->unit_cost - $purchase_item->net_unit_cost;
                        $discount = $purchase_item->item_discount / $purchase_item->quantity;
                        $item_tax = $tax * $nqty;
                        $item_discount = $discount * $nqty;
                        $subtotal = $purchase_item->unit_cost * $nqty;
                        $this->db->update('purchase_items', array('quantity' => $nqty, 'quantity_balance' => $bqty, 'quantity_received' => $rqty, 'item_tax' => $item_tax, 'item_discount' => $item_discount, 'subtotal' => $subtotal), array('id' => $item['purchase_item_id']));
                    }

                }
            }
            $this->calculatePurchaseTotals($data['purchase_id'], $return_id, $data['surcharge']);
            $this->site->syncQuantity(NULL, NULL, $purchase_items);
            $this->site->syncQuantity(NULL, $data['purchase_id']);
            return true;
        }
        return false;
    }

    public function calculatePurchaseTotals($id, $return_id, $surcharge)
    {
        $purchase = $this->getPurchaseByID($id);
        $items = $this->getAllPurchaseItems($id);
        if (!empty($items)) {
            $total = 0;
            $product_tax = 0;
            $order_tax = 0;
            $product_discount = 0;
            $order_discount = 0;
            foreach ($items as $item) {
                $product_tax += $item->item_tax;
                $product_discount += $item->item_discount;
                $total += $item->net_unit_cost * $item->quantity;
            }
            if ($purchase->order_discount_id) {
                $percentage = '%';
                $order_discount_id = $purchase->order_discount_id;
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = (($total + $product_tax) * (Float)($ods[0])) / 100;
                } else {
                    $order_discount = $order_discount_id;
                }
            }
            if ($purchase->order_tax_id) {
                $order_tax_id = $purchase->order_tax_id;
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $order_tax_details->rate;
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = (($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100;
                    }
                }
            }
            $total_discount = $order_discount + $product_discount;
            $total_tax = $product_tax + $order_tax;
            $grand_total = $total + $total_tax + $purchase->shipping - $order_discount + $surcharge;
            $data = array(
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'grand_total' => $grand_total,
                'return_id' => $return_id,
                'surcharge' => $surcharge
            );

            if ($this->db->update('purchases', $data, array('id' => $id))) {
                return true;
            }
        } else {
            $this->db->delete('purchases', array('id' => $id));
        }
        return FALSE;
    }

    public function getExpenseCategories()
    {
        $q = $this->db->get('expense_categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getExpenseCategoryByID($id)
    {
        $q = $this->db->get_where("expense_categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWithHolding($type, $affects){
        $ano = (substr($this->db->database, -2, 2));
        if ($this->site->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $ano = $contabilidadSufijo;
        } else {
            if (!is_numeric($ano)) {
                $year_actual = date('Y');
                $ano = substr($year_actual, -2, 2);
            }
        }
        $sufijo = "_con".$ano;
        $ledgers = 'ledgers'.$sufijo;
        $wholdings = 'withholdings';

        if ($this->Settings->modulary == 1) {
            $conditions[$wholdings.'.affects'] = $affects;
            if ($type) {
                $conditions[$wholdings.'.type'] = $type;
            }
            $q = $this->db->select($wholdings.".*, ".$ledgers.".name as ledger_name")
                    ->join($ledgers, $ledgers.'.id = '.$wholdings.'.account_id')
                    ->where($conditions)
                    ->get($wholdings);
            if ($q->num_rows() > 0) {
               return $q->result_array();
            }
        } else {

            $conditions['affects'] = $affects;
            if ($type) {
                $conditions['type'] = $type;
            }
            $q = $this->db->where($conditions)->get($wholdings);
            if ($q->num_rows() > 0) {
               return $q->result_array();
            }
        }
        return FALSE;
    }

    public function recontabilizarCompra($purchase_id, $prev_reference = null, $from_edit = false)
    {

        $purchase = $this->getPurchaseByID($purchase_id, true);
        $items = $this->getAllPurchaseItems($purchase_id, true);
        $payments = $this->getPaymentsForPurchaseReAccounting($purchase_id, true);
        $msg = '';
        if ($mAccountSettings = $this->site->getSettingsCon($purchase['date'])) {
            if ($mAccountSettings->year_closed == 1) {
                $this->session->set_userdata('reaccount_error', true);
                return "La compra (".$purchase['reference_no'].") no se contabilizó, año contable cerrado. </br>";
            }
        }
        if (strpos($purchase['reference_no'], "SI") !== false) {
            $this->session->set_userdata('reaccount_error', true);
            return "La compra (".$purchase['reference_no'].") es saldo inicial, no se contabilizó. </br>";
        }
        if ($purchase['status'] == 'pending') {
            $this->session->set_userdata('reaccount_error', true);
            return "La compra (".$purchase['reference_no'].") tiene estado pendiente, no se contabilizó. </br>";
        }
        if(!$this->site->getEntryTypeNumberExisting($purchase, true, ($purchase['status'] == 'returned' ? true : false), $prev_reference)){
            $msg .= "La compra (".$purchase['reference_no'].") no estaba contabilizada y se contabilizó. </br>";
        } else {
            $msg .= "La compra (".$purchase['reference_no'].") ya estaba contabilizada y se reprocesó la contabilización. </br>";
        }
        $payment_interno = [];
        $payments_externo = [];
        $payments_multi = [];
        $has_payment_retention = false;
        $si_return = [];
        if ($payments) {
            foreach ($payments as $pmnt) {
                if (strpos($pmnt['note'], 'Retención posterior') !== false) {
                    if ($pmnt['rete_fuente_total'] > 0) {
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_fuente_total'] = $pmnt['rete_fuente_total'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_fuente_base'] = $pmnt['rete_fuente_base'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_fuente_account'] = $pmnt['rete_fuente_account'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_fuente_assumed'] = $pmnt['rete_fuente_assumed'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_fuente_assumed_account'] = $pmnt['rete_fuente_assumed_account'];
                    }
                    if ($pmnt['rete_iva_total'] > 0) {
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_iva_total'] = $pmnt['rete_iva_total'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_iva_base'] = $pmnt['rete_iva_base'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_iva_account'] = $pmnt['rete_iva_account'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_iva_assumed'] = $pmnt['rete_iva_assumed'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_iva_assumed_account'] = $pmnt['rete_iva_assumed_account'];
                    }
                    if ($pmnt['rete_ica_total'] > 0) {
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_ica_total'] = $pmnt['rete_ica_total'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_ica_base'] = $pmnt['rete_ica_base'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_ica_account'] = $pmnt['rete_ica_account'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_ica_assumed'] = $pmnt['rete_ica_assumed'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_ica_assumed_account'] = $pmnt['rete_ica_assumed_account'];
                    }
                    if ($pmnt['rete_other_total'] > 0) {
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_other_total'] = $pmnt['rete_other_total'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_other_base'] = $pmnt['rete_other_base'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_other_account'] = $pmnt['rete_other_account'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_other_assumed'] = $pmnt['rete_other_assumed'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_other_assumed_account'] = $pmnt['rete_other_assumed_account'];
                    }
                    if ($pmnt['rete_autoaviso_total'] > 0) {
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_autoaviso_total'] = $pmnt['rete_autoaviso_total'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_autoaviso_base'] = $pmnt['rete_autoaviso_base'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_autoaviso_account'] = $pmnt['rete_autoaviso_account'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_other_autoaviso_account'] = $pmnt['rete_other_autoaviso_account'];
                    }
                    if ($pmnt['rete_bomberil_total'] > 0) {
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_bomberil_total'] = $pmnt['rete_bomberil_total'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_bomberil_base'] = $pmnt['rete_bomberil_base'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_bomberil_account'] = $pmnt['rete_bomberil_account'];
                        $si_return['ce_retentions_data'][$pmnt['reference_no']]['rete_other_bomberil_account'] = $pmnt['rete_other_bomberil_account'];
                    }
                }
                if ($pmnt['multi_payment'] == 1) {
                    $payments_multi[] = $pmnt;
                    if ($pmnt['rete_fuente_total'] > 0 || $pmnt['rete_iva_total'] > 0 || $pmnt['rete_ica_total'] > 0 || $pmnt['rete_other_total'] > 0) {
                        $has_payment_retention = true;
                    }
                } else {
                    // if ($pmnt['paid_by'] != 'retencion' && $pmnt['paid_by'] != 'due') {
                    if ($pmnt['paid_by'] != 'retencion' && $pmnt['paid_by'] != 'due') {
                        if ($purchase['date'] == $pmnt['date'] && (($purchase['purchase_type'] == 1 && $purchase['status'] != 'returned') || ($purchase['purchase_type'] == 2 || $purchase['purchase_type'] == 3))) {
                            $payment_interno[] = $pmnt;
                        } else {
                            $payments_externo[] = $pmnt;
                        }
                    }
                }
            }
        }
        $nc_oc = false;
        foreach ($items as $row) {
            if ($row['product_id'] == '999999999') {
                $nc_oc = true;
            }
        }
        if ($this->Settings->tax_rate_traslate && $purchase['total_tax'] > 0 && count($payment_interno) > 0) {
            $inv_items = $items;
            $purchase_taxes = [];
            $pagadoBB = $payment_interno[0]['amount'];
            foreach ($inv_items as $row) {
                $proporcion_pago = ($pagadoBB * 100) / $purchase['grand_total'];
                $total_iva = ($row['item_tax'] + $row['item_tax_2']) * ($proporcion_pago / 100);
                if (!isset($purchase_taxes[$row['tax_rate_id']])) {
                    $purchase_taxes[$row['tax_rate_id']] = $total_iva;
                } else {
                    $purchase_taxes[$row['tax_rate_id']] += $total_iva;
                }
            }
            $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
            $data_tax_rate_traslate = $data_taxrate_traslate = array(
                    'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->payment_ledger_id,
                    'purchase_taxes' => $purchase_taxes,
                );
        }
        if ($purchase['status'] == 'returned') {
            if ($nc_oc) {
                $this->site->wappsiContabilidadComprasDevolucionOtherConcepts($purchase_id, $purchase, $items[0], $payment_interno);
            } else {
                if ($purchase['purchase_type'] == 2 || $purchase['purchase_type'] == 3) {
                    $this->site->wappsiContabilidadGastosDevolucion($purchase_id, $purchase, $items, $payment_interno, $prev_reference);
                } else {
                    $payment_dev[0] = $this->getPaymentsForPurchaseDev($purchase_id, true);
                    $this->site->wappsiContabilidadComprasDevolucion($purchase['purchase_id'], $purchase, $items, $payments_externo, $from_edit, $si_return);
                }
            }
        } else {
            if ($purchase['purchase_type'] == 2 || $purchase['purchase_type'] == 3) {
                $this->site->wappsiContabilidadGastos($purchase_id, $purchase, $items, $payment_interno, $prev_reference, $has_payment_retention);
            } else {
                $this->site->wappsiContabilidadCompras($purchase_id, $purchase, $items, $payment_interno, (isset($data_taxrate_traslate) ? $data_taxrate_traslate : null), $prev_reference, $has_payment_retention, $from_edit);
            }
        }
        if ($this->session->userdata('swal_flash_message')) {
            return $this->session->userdata('swal_flash_message');
        } else {
            return $msg;
        }
    }

    public function validateSupplierReferenceNo($supplier_id, $reference_no, $po_edit, $id_purchase, $document_type_prefix)
    {

        $condiciones['supplier_id'] = $supplier_id;
        if ($reference_no) {
            $condiciones['reference_no'] = ($document_type_prefix."-".$reference_no);
        }
        if ($po_edit != 'false') {
            $condiciones['id !='] = $id_purchase;
        }
        $q = $this->db->get_where('purchases', $condiciones);

        if ($q->num_rows() > 0) {
            return "true";
        }

        return "false";

    }

    public function getPurchasesPaymentPending($rdata){

        $supplier_id = $rdata['supplier_id'];
        $biller_id = $rdata['biller_id'];
        $purchase_id = $rdata['purchase_id'];
        $pp_reference_no = $rdata['pp_reference_no'];
        $rows_start = $rdata['rows_start'];
        $num_rows = $rdata['num_rows'];

        if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id') && !$biller_id) {
            $biller_id = $this->session->userdata('biller_id');
        }
        $supplier = $this->site->getCompanyByID($supplier_id);
        if (!$purchase_id) {
            if ($supplier->supplier_type == 1) {

            } else if ($supplier->supplier_type == 2) {
                $this->db->where('purchase_type', 1);
            } else if ($supplier->supplier_type == 3) {
                $this->db->where('purchase_type', 2);
            }
        }
            
        $this->db->where('supplier_id', $supplier_id)
                 ->where('payment_status !=', 'paid')
                 ->where('status', 'received')
                 ->where('grand_total > paid');
        if (isset($biller_id)) {
              $this->db->where('biller_id', $biller_id);
        }
        if (isset($purchase_id)) {
              $this->db->where('id', $purchase_id);
        }
        $purchases_pending = $this->db->order_by('date asc')->limit($num_rows, $rows_start)->get('purchases');
        if ($purchases_pending->num_rows() > 0) {
            foreach (($purchases_pending->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function updatePurchasesPayments($paid_by, $payments, $supplier_id, $retenciones, $conceptos, $cost_center, $deposit, $manual_reference, $biller_id = NULL)
    {
        $total_paid = 0;
        $reference_no = '';
        $note = '';
        $date = '';
        if (count($payments) > 0) {
            $ref_p = $payments[0]['reference_no'];
            $consecutive = explode("-", $ref_p);
            $consecutive = $consecutive[1];
            $document_type_id = $payments[0]['document_type_id'];
            $date = $payments[0]['date'];
        } else if (count($conceptos) > 0) {
            $ref_p = $conceptos[0]['reference_no'];
            $consecutive = explode("-", $ref_p);
            $consecutive = $consecutive[1];
            $document_type_id = $conceptos[0]['document_type_id'];
            $date = $conceptos[0]['date'];
        }

        $update_consecutive = true;
        if ($manual_reference) {
            $update_consecutive = false;
        }

        if (count($payments) > 0) {
            if ($paid_by == 'deposit') {
                if ($conceptos && count($conceptos) > 0) {
                    foreach ($conceptos as $key => $concepto) {
                        $ordenar_conceptos[$key] = $concepto['amount'];
                    }
                    array_multisort($ordenar_conceptos, SORT_ASC, $conceptos);
                    $conceptos = $this->site->set_payments_affected_by_deposits($supplier_id, $conceptos);
                }
                $payments = $this->site->set_payments_affected_by_deposits($supplier_id, $payments);
            }
            foreach ($payments as $key => $data) {
                $data['reference_no'] = $ref_p;
                $total_paid += $data['amount'];
                $reference_no = $data['reference_no'];
                $note = $data['note'];
                if ($this->db->insert('payments', $data)) {
                    $this->site->syncpurchasePayments($data['purchase_id']);
                    if ($data['paid_by'] == 'gift_card') {
                        $gc = $this->site->getGiftCardByNO($data['cc_no']);
                        $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
                    }
                    //SE COMENTA PARA NO AFECTAR A VENTA, DATOS DE RETENCIÓN SÓLO AFECTAN RECIBO DE CAJA
                    // if (isset($retenciones[$data['purchase_id']])) {
                    //     $retencion = $retenciones[$data['purchase_id']];
                    //     $purchase = $this->db->get_where('purchases', array('id' => $data['purchase_id']))->row();
                    //     $payments[$key]['cost_center_id'] = $cost_center;
                    //     foreach ($retencion as $rete => $valor) {
                    //         if (
                    //                 $rete =='rete_fuente_base' ||
                    //                 $rete =='rete_iva_base' ||
                    //                 $rete =='rete_ica_base' ||
                    //                 $rete =='rete_other_base' ||
                    //                 $rete =='rete_bomberil_base' ||
                    //                 $rete =='rete_autoaviso_base' ||

                    //                 $rete =='rete_fuente_total' ||
                    //                 $rete =='rete_iva_total' ||
                    //                 $rete =='rete_ica_total' ||
                    //                 $rete =='rete_other_total' ||
                    //                 $rete =='rete_bomberil_total' ||
                    //                 $rete =='rete_autoaviso_total'
                    //             ) {
                    //             $prev_rete = (Double) $purchase->{$rete};
                    //             $prev_rete += (Double) $valor;
                    //             $retencion[$rete] = $prev_rete;
                    //         }
                    //     }
                    //     $this->db->update('purchases', $retencion, array('id' => $data['purchase_id']));
                    //     unset($retencion);
                    // }
                    $purchase = $this->getPurchaseByID($data['purchase_id']);
                    $expense_payment = false;
                    if ($purchase->purchase_type == 2 || $purchase->purchase_type == 3) {
                        $purchase_items = $this->getAllPurchaseItems($data['purchase_id']);
                        $payments[$key]['purchase_items'] = $purchase_items;
                        $payments[$key]['purchase'] = $purchase;
                    }
                } else {
                    return false;
                }
            }
        }

        foreach ($conceptos as $id => $concepto) {
            $ledger = $concepto['ledger_id'];
            $concepto['concept_ledger_id'] = $concepto['ledger_id'];
            $conceptos[$id]['concept_ledger_id'] = $concepto['ledger_id'];
            $reference_no = $concepto['reference_no'];
            unset($concepto['ledger_id']);
            $this->db->insert('payments', $concepto);
        }

        if ($this->Settings->modulary == 1) { //Si se indicó que el POS es modular con Contabilidad.
            $data_tax_rate_traslate = [];

            $payment = array(
                            'reference_no' => $reference_no,
                            'amount' => $total_paid,
                            'date' => $date,
                            'payments' => $payments,
                            'companies_id' => $supplier_id,
                            'note' => $note,
                            'document_type_id' => $document_type_id,
                            'biller_id' => $biller_id,
                            'created_by' => $payments[0]['created_by']
                            );
            if($this->site->wappsiContabilidadPagosMultiplesCompras($payment, $supplier_id, $retenciones, $data_tax_rate_traslate, $conceptos, $cost_center)){}
        }

        if ($update_consecutive) {
            $this->site->updateBillerConsecutive($document_type_id, $consecutive+1);
        }

        if ($deposit) {
            // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
            $referenceBiller = $this->site->getReferenceBiller($deposit['biller_id'], $deposit['document_type_id']);
            if($referenceBiller){
                $reference_deposit = $referenceBiller;
            }else{
                $reference_deposit = $this->site->getReference('dp');
            }
            $deposit['reference_no'] = $reference_deposit;
            $deposit['origen_reference_no'] = $ref_p;
            $ref = explode("-", $reference_deposit);
            $deposit_consecutive = $ref[1];
            $supplier = $this->site->getCompanyByID($supplier_id);
            // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
            if ($this->db->insert('deposits', $deposit)) {
                $this->site->updateBillerConsecutive($deposit['document_type_id'], $deposit_consecutive+1);
                $this->site->wappsiContabilidadDeposito($deposit, 2);
                $this->db->update('companies', ['deposit_amount' => ($supplier->deposit_amount + $deposit['amount'])], ['id'=> $supplier->id]);
            }
        }
        return true;
    }

    public function getProductNamesIU($product_id)
    {
        $limit = $this->Settings->max_num_results_display;
        $this->db->where("(type = 'standard' or type = 'raw') AND id = ".$product_id);
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function get_purchase_multi_payments_by_reference($reference_no)
    {
        $q = $this->db->where(['multi_payment' => 1, 'reference_no' => $reference_no])->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function updateExpensesPayments($payments, $supplier_id, $retenciones, $conceptos, $cost_center, $expenses)
    {
        $ref_p = $payments[0]['reference_no'];
        $consecutive = explode("-", $ref_p);
        $consecutive = $consecutive[1];
        $document_type_id = $payments[0]['document_type_id'];
        $reference_no = $payments[0]['reference_no'];
        $note = $payments[0]['note'];
        $date = $payments[0]['date'];
        $total_paid = 0;
        $biller_id = null;
        foreach ($payments as $p_id => $payment) {
            if ($biller_id == null) {
                $biller_id = $payment['biller_id'];
            }
            $total_paid += $payment['amount'];
            $expense = $expenses[$payment['expense_id']];
            $payments[$p_id]['expense_data'] = $expense;
            unset($payment['biller_id']);
            if ($this->db->insert('payments', $payment)) {
                $payment_id = $this->db->insert_id();
                $expense['payment_id'] = $payment_id;
                $expense['biller_id'] = $biller_id;
                $this->db->insert('expenses', $expense);
            }
        }

        foreach ($conceptos as $id => $concepto) {
            $ledger = $concepto['ledger_id'];
            $concepto['concept_ledger_id'] = $concepto['ledger_id'];
            $conceptos[$id]['concept_ledger_id'] = $concepto['ledger_id'];
            unset($concepto['ledger_id']);
            $this->db->insert('payments', $concepto);
        }
        $this->site->updateBillerConsecutive($document_type_id, $consecutive+1);
        if ($this->Settings->modulary == 1) { //Si se indicó que el POS es modular con Contabilidad.
            $data_tax_rate_traslate = [];

            $payment = array(
                            'reference_no' => $reference_no,
                            'amount' => $total_paid,
                            'date' => $date,
                            'payments' => $payments,
                            'companies_id' => $supplier_id,
                            'document_type_id' => $document_type_id,
                            'note' => $note,
                            'biller_id' => $biller_id,
                            'created_by' => $payments[0]['created_by']
                            );
            if($this->site->wappsiContabilidadPagosMultiplesGastos($payment, $supplier_id, $retenciones, $data_tax_rate_traslate, $conceptos, $cost_center)){}
        }
        return true;
    }

    public function getExpenseCommision()
    {
        $category_id = $this->Settings->default_expense_id_to_pay_commisions;
        $q = $this->db->select('id, code, name, 0 as cost, tax_rate_id as tax_rate, tax_rate_2_id as tax_rate_2')
                      ->where(['id' => $category_id])
                      ->get('expense_categories');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function recontabilizar_pagos_multiples($reference_no)
    {
        $multi_payments = [];
        $conceptos = [];
        $retenciones = [];
        $mpayment_enc = $this->site->getMultiPaymentEnc($reference_no) ? $this->site->getMultiPaymentEnc($reference_no) : $this->site->getMultiPaymentEncOnlyConcepts($reference_no);
        $cost_center_id = $mpayment_enc[0]->cost_center_id;
        $details = $this->purchases_model->get_purchase_multi_payments_by_reference($reference_no);
        $total_paid = 0;
        $supplier_id = null;
        $biller_id = null;
        if ($details) {
            foreach ($details as $mpayment) {
                $expense = $this->site->get_expense_by_payment_id($mpayment->id, $mpayment->expense_id);
                if ($expense) {
                    $supplier_id = $expense->supplier_id;
                    $biller_id = $expense->biller_id;
                    if ($mpayment->expense_id) {
                        $q_ex = $this->db->get_where('expenses', ['reference'=>$mpayment->reference_no, 'category_id' => $mpayment->expense_id]);
                        if ($q_ex->num_rows() > 0) {
                            $q_ex = $q_ex->row_array();
                        }
                        $payment = [];
                        $total_paid += $mpayment->amount;
                        foreach ($mpayment as $key => $value) {
                            $payment[$key] = $value;
                        }
                        $payment['expense_data'] = $q_ex;
                        $multi_payments[] = $payment;
                        if ($mpayment->rete_fuente_total != 0) {
                            if (isset($retenciones[$mpayment->expense_id]['rete_fuente_total'])) {
                                $retenciones[$mpayment->expense_id]['rete_fuente_total'] += $mpayment->rete_fuente_total;
                                $retenciones[$mpayment->expense_id]['rete_fuente_base'] += $mpayment->rete_fuente_base;
                            } else {
                                $retenciones[$mpayment->expense_id]['rete_fuente_total'] = $mpayment->rete_fuente_total;
                                $retenciones[$mpayment->expense_id]['rete_fuente_percentage'] = $mpayment->rete_fuente_percentage;
                                $retenciones[$mpayment->expense_id]['rete_fuente_base'] = $mpayment->rete_fuente_base;
                                $retenciones[$mpayment->expense_id]['rete_fuente_account'] = $mpayment->rete_fuente_account;
                                $retenciones[$mpayment->expense_id]['rete_fuente_assumed'] = $mpayment->rete_fuente_assumed;
                                $retenciones[$mpayment->expense_id]['rete_fuente_assumed_account'] = $mpayment->rete_fuente_assumed_account;
                            }
                        }

                        if ($mpayment->rete_iva_total != 0) {
                            if (isset($retenciones[$mpayment->expense_id]['rete_iva_total'])) {
                                $retenciones[$mpayment->expense_id]['rete_iva_total'] += $mpayment->rete_iva_total;
                                $retenciones[$mpayment->expense_id]['rete_iva_base'] += $mpayment->rete_iva_base;
                            } else {
                                $retenciones[$mpayment->expense_id]['rete_iva_total'] = $mpayment->rete_iva_total;
                                $retenciones[$mpayment->expense_id]['rete_iva_percentage'] = $mpayment->rete_iva_percentage;
                                $retenciones[$mpayment->expense_id]['rete_iva_base'] = $mpayment->rete_iva_base;
                                $retenciones[$mpayment->expense_id]['rete_iva_account'] = $mpayment->rete_iva_account;
                                $retenciones[$mpayment->expense_id]['rete_iva_assumed'] = $mpayment->rete_iva_assumed;
                                $retenciones[$mpayment->expense_id]['rete_iva_assumed_account'] = $mpayment->rete_iva_assumed_account;
                            }
                        }

                        if ($mpayment->rete_ica_total != 0) {
                            if (isset($retenciones[$mpayment->expense_id]['rete_ica_total'])) {
                                $retenciones[$mpayment->expense_id]['rete_ica_total'] += $mpayment->rete_ica_total;
                                $retenciones[$mpayment->expense_id]['rete_ica_base'] += $mpayment->rete_ica_base;
                            } else {
                                $retenciones[$mpayment->expense_id]['rete_ica_total'] = $mpayment->rete_ica_total;
                                $retenciones[$mpayment->expense_id]['rete_ica_percentage'] = $mpayment->rete_ica_percentage;
                                $retenciones[$mpayment->expense_id]['rete_ica_base'] = $mpayment->rete_ica_base;
                                $retenciones[$mpayment->expense_id]['rete_ica_account'] = $mpayment->rete_ica_account;
                                $retenciones[$mpayment->expense_id]['rete_ica_assumed'] = $mpayment->rete_ica_assumed;
                                $retenciones[$mpayment->expense_id]['rete_ica_assumed_account'] = $mpayment->rete_ica_assumed_account;
                            }
                        }

                        if ($mpayment->rete_other_total != 0) {
                            if (isset($retenciones[$mpayment->expense_id]['rete_other_total'])) {
                                $retenciones[$mpayment->expense_id]['rete_other_total'] += $mpayment->rete_other_total;
                                $retenciones[$mpayment->expense_id]['rete_other_base'] += $mpayment->rete_other_base;
                            } else {
                                $retenciones[$mpayment->expense_id]['rete_other_total'] = $mpayment->rete_other_total;
                                $retenciones[$mpayment->expense_id]['rete_other_percentage'] = $mpayment->rete_other_percentage;
                                $retenciones[$mpayment->expense_id]['rete_other_base'] = $mpayment->rete_other_base;
                                $retenciones[$mpayment->expense_id]['rete_other_account'] = $mpayment->rete_other_account;
                                $retenciones[$mpayment->expense_id]['rete_other_assumed'] = $mpayment->rete_other_assumed;
                                $retenciones[$mpayment->expense_id]['rete_other_assumed_account'] = $mpayment->rete_other_assumed_account;
                            }
                        }

                        if ($mpayment->rete_autoaviso_total != 0) {
                            if (isset($retenciones[$mpayment->expense_id]['rete_autoaviso_total'])) {
                                $retenciones[$mpayment->expense_id]['rete_autoaviso_total'] += $mpayment->rete_autoaviso_total;
                                $retenciones[$mpayment->expense_id]['rete_autoaviso_base'] += $mpayment->rete_autoaviso_base;
                            } else {
                                $retenciones[$mpayment->expense_id]['rete_autoaviso_total'] = $mpayment->rete_autoaviso_total;
                                $retenciones[$mpayment->expense_id]['rete_autoaviso_percentage'] = $mpayment->rete_autoaviso_percentage;
                                $retenciones[$mpayment->expense_id]['rete_autoaviso_base'] = $mpayment->rete_autoaviso_base;
                                $retenciones[$mpayment->expense_id]['rete_autoaviso_account'] = $mpayment->rete_autoaviso_account;
                                $retenciones[$mpayment->expense_id]['rete_autoaviso_assumed_account'] = $mpayment->rete_autoaviso_assumed_account;
                            }
                        }

                        if ($mpayment->rete_bomberil_total != 0) {
                            if (isset($retenciones[$mpayment->expense_id]['rete_bomberil_total'])) {
                                $retenciones[$mpayment->expense_id]['rete_bomberil_total'] += $mpayment->rete_bomberil_total;
                                $retenciones[$mpayment->expense_id]['rete_bomberil_base'] += $mpayment->rete_bomberil_base;
                            } else {
                                $retenciones[$mpayment->expense_id]['rete_bomberil_total'] = $mpayment->rete_bomberil_total;
                                $retenciones[$mpayment->expense_id]['rete_bomberil_percentage'] = $mpayment->rete_bomberil_percentage;
                                $retenciones[$mpayment->expense_id]['rete_bomberil_base'] = $mpayment->rete_bomberil_base;
                                $retenciones[$mpayment->expense_id]['rete_bomberil_account'] = $mpayment->rete_bomberil_account;
                                $retenciones[$mpayment->expense_id]['rete_bomberil_assumed_account'] = $mpayment->rete_bomberil_assumed_account;
                            }
                        }

                    } else {
                        $concepto = [];
                        $total_paid += $mpayment->amount;
                        foreach ($mpayment as $key => $value) {
                            $concepto[$key] = $value;
                        }
                        $conceptos[] = $concepto;
                    }
                }//si es gasto
            }
            $payment = array(
                                'reference_no' => $mpayment_enc[0]->reference_no,
                                'document_type_id' => $mpayment_enc[0]->document_type_id,
                                'amount' => $total_paid,
                                'date' => $mpayment_enc[0]->date,
                                'note' => $mpayment_enc[0]->note,
                                'payments' => $multi_payments,
                                'companies_id' => $supplier_id,
                                'supplier_id' => $supplier_id,
                                'biller_id' => $biller_id,
                                );
            $this->site->wappsiContabilidadPagosMultiplesGastos($payment, $supplier_id, $retenciones, null, $conceptos, $cost_center_id);
        }
    }

    public function delete_payment($reference, $data)
    {
        $payment = $this->db->get_where('payments', ['reference_no' => $reference]);
        $payments = [];
        if ($payment->num_rows() > 0) {
            $entry_deleted = false;
            $reaccount_id = false;
            $reaccount_type = false;
            foreach (($payment->result()) as $row) {
                $payments[] = $row;
                if ($row->purchase_id) {
                    $purchase = $this->getpurchaseByID($row->purchase_id);
                    if ($purchase->return_id) {
                        return [
                                'response' => false,
                                'reaccount' => $reaccount_id,
                                'reaccount_type' => $reaccount_type,
                                'message' => sprintf(lang('payment_with_return'), $purchase->reference_no),
                               ];
                    }
                }
                if ($row->paid_by == 'deposit') {
                    if ($row->affected_deposit_id) {
                        $deposit = $this->db->get_where('deposits', ['id' => $row->affected_deposit_id])->row();
                        $this->db->update('deposits', ['balance'=> $deposit->balance+($row->amount - ($row->rete_fuente_total + $row->rete_iva_total + $row->rete_ica_total + $row->rete_other_total))], ['id'=>$deposit->id]);
                        $company = $this->db->get_where('companies', ['id' => $deposit->company_id])->row();
                        $this->db->update('companies', ['deposit_amount'=> $company->deposit_amount+($row->amount - ($row->rete_fuente_total + $row->rete_iva_total + $row->rete_ica_total + $row->rete_other_total))], ['id'=>$company->id]);
                    }
                }
            }

            $date = date('Y-m-d H:i:s');
            $multi_payments = [];
            $conceptos = [];
            $retenciones = [];
            $total_paid = 0;

            $referenceBiller = $this->site->getReferenceBiller($data['biller'], $data['document_type_id']);
            if($referenceBiller){
                $reference = $referenceBiller;
            }
            $reference_no = $reference;
            $ref = explode("-", $reference);
            $consecutive = $ref[1];
            foreach ($payments as $pmnt) {
                $pmnt->expense_payment = false;
                $txt = $this->site->text_delete_payment($pmnt);
                $updated = $this->db->insert('payments',
                                    [
                                        'date' => $date,
                                        'note' => $txt,
                                        'reference_no' => $reference_no,
                                        'amount' => $pmnt->amount * -1,
                                        'purchase_id' => $pmnt->purchase_id,
                                        'rete_fuente_total' => $pmnt->rete_fuente_total * -1,
                                        'rete_fuente_percentage' => $pmnt->rete_fuente_percentage,
                                        'rete_fuente_base' => $pmnt->rete_fuente_base * -1,
                                        'rete_fuente_account' => $pmnt->rete_fuente_account,
                                        'rete_fuente_id' => $pmnt->rete_fuente_id,
                                        'rete_iva_total' => $pmnt->rete_iva_total * -1,
                                        'rete_iva_percentage' => $pmnt->rete_iva_percentage,
                                        'rete_iva_base' => $pmnt->rete_iva_base * -1,
                                        'rete_iva_account' => $pmnt->rete_iva_account,
                                        'rete_iva_id' => $pmnt->rete_iva_id,
                                        'rete_ica_total' => $pmnt->rete_ica_total * -1,
                                        'rete_ica_percentage' => $pmnt->rete_ica_percentage,
                                        'rete_ica_base' => $pmnt->rete_ica_base * -1,
                                        'rete_ica_account' => $pmnt->rete_ica_account,
                                        'rete_ica_id' => $pmnt->rete_ica_id,
                                        'rete_other_total' => $pmnt->rete_other_total * -1,
                                        'rete_other_percentage' => $pmnt->rete_other_percentage,
                                        'rete_other_base' => $pmnt->rete_other_base * -1,
                                        'rete_other_account' => $pmnt->rete_other_account,
                                        'rete_other_id' => $pmnt->rete_other_id,
                                        'rete_bomberil_total' => $pmnt->rete_bomberil_total * -1,
                                        'rete_bomberil_percentage' => $pmnt->rete_bomberil_percentage,
                                        'rete_bomberil_base' => $pmnt->rete_bomberil_base * -1,
                                        'rete_bomberil_account' => $pmnt->rete_bomberil_account,
                                        'rete_bomberil_id' => $pmnt->rete_bomberil_id,
                                        'rete_autoaviso_total' => $pmnt->rete_autoaviso_total * -1,
                                        'rete_autoaviso_percentage' => $pmnt->rete_autoaviso_percentage,
                                        'rete_autoaviso_base' => $pmnt->rete_autoaviso_base * -1,
                                        'rete_autoaviso_account' => $pmnt->rete_autoaviso_account,
                                        'rete_autoaviso_id' => $pmnt->rete_autoaviso_id,

                                        'rete_fuente_assumed' => $pmnt->rete_fuente_assumed,
                                        'rete_iva_assumed' => $pmnt->rete_iva_assumed,
                                        'rete_ica_assumed' => $pmnt->rete_ica_assumed,
                                        'rete_other_assumed' => $pmnt->rete_other_assumed,

                                        'rete_fuente_assumed_account' => $pmnt->rete_fuente_assumed_account,
                                        'rete_iva_assumed_account' => $pmnt->rete_iva_assumed_account,
                                        'rete_ica_assumed_account' => $pmnt->rete_ica_assumed_account,
                                        'rete_bomberil_assumed_account' => $pmnt->rete_bomberil_assumed_account,
                                        'rete_autoaviso_assumed_account' => $pmnt->rete_autoaviso_assumed_account,
                                        'rete_other_assumed_account' => $pmnt->rete_other_assumed_account,

                                        'document_type_id' => $data['document_type_id'],
                                        'multi_payment' => $pmnt->multi_payment,
                                        'return_id' => $pmnt->id,
                                        'return_reference_no' => $pmnt->reference_no,
                                        'paid_by' => $pmnt->paid_by,
                                        'created_by' => $this->session->userdata('user_id'),
                                        'type' => $pmnt->type == 'sent_2' ? 'cancelled_2' : 'cancelled',
                                        'affected_deposit_id' => $pmnt->affected_deposit_id,
                                        'concept_company_id' => $pmnt->concept_company_id,
                                        'concept_base' => $pmnt->concept_base,
                                    ]
                                );

                if ($updated) { //SI SE ACTUALIZÓ CORRECTAMENTE EL PAGO
                    $payment_return_id = $this->db->insert_id();
                    if ($pmnt->purchase_id) {
                        $this->site->syncPurchasePayments($pmnt->purchase_id); //SINCRONIZACIÓN DE VENTA CON LOS PAGOS QUE QUEDARON
                        $purchase = $this->getPurchaseByID($pmnt->purchase_id);
                        $this->db->update('purchases',
                                                [
                                                    'rete_fuente_total' => $purchase->rete_fuente_total - $pmnt->rete_fuente_total,
                                                    'rete_fuente_base' => $purchase->rete_fuente_base - $pmnt->rete_fuente_base,
                                                    'rete_iva_total' => $purchase->rete_iva_total - $pmnt->rete_iva_total,
                                                    'rete_iva_base' => $purchase->rete_iva_base - $pmnt->rete_iva_base,
                                                    'rete_ica_total' => $purchase->rete_ica_total - $pmnt->rete_ica_total,
                                                    'rete_ica_base' => $purchase->rete_ica_base - $pmnt->rete_ica_base,
                                                    'rete_bomberil_total' => $purchase->rete_bomberil_total - $pmnt->rete_bomberil_total,
                                                    'rete_autoaviso_base' => $purchase->rete_autoaviso_base - $pmnt->rete_autoaviso_base,
                                                    'rete_other_total' => $purchase->rete_other_total - $pmnt->rete_other_total,
                                                    'rete_other_base' => $purchase->rete_other_base - $pmnt->rete_other_base
                                                ],
                                                [
                                                    'id' => $purchase->id
                                                ]);
                    }
                    if (!$pmnt->multi_payment) { //Si no es pago múltiple, si no uno directo de la factura, se activa bandera para recontabilizar la venta o compra según sea el caso
                        if ($pmnt->purchase_id) {
                            if ($pmnt->date == $purchase->date) {
                                $reaccount_id = $pmnt->purchase_id;
                                $reaccount_type = 2;
                            }
                        }
                    }
                    $this->db->update('payments', ['return_id' => $payment_return_id, 'return_reference_no' => $reference_no], ['id' => $pmnt->id]);
                }

                if ($pmnt->purchase_id) {
                    $payment = [];
                    $total_paid += $pmnt->amount;
                    foreach ($pmnt as $key => $value) {
                        $payment[$key] = $value;
                    }
                    $multi_payments[] = $payment;
                    if ($pmnt->rete_fuente_total > 0) {
                        $retenciones[$pmnt->purchase_id]['rete_fuente_total'] = $pmnt->rete_fuente_total;
                        $retenciones[$pmnt->purchase_id]['rete_fuente_percentage'] = $pmnt->rete_fuente_percentage;
                        $retenciones[$pmnt->purchase_id]['rete_fuente_base'] = $pmnt->rete_fuente_base;
                        $retenciones[$pmnt->purchase_id]['rete_fuente_account'] = $pmnt->rete_fuente_account;
                        $retenciones[$pmnt->purchase_id]['rete_fuente_assumed'] = $pmnt->rete_fuente_assumed;
                        $retenciones[$pmnt->purchase_id]['rete_fuente_assumed_account'] = $pmnt->rete_fuente_assumed_account;
                    }
                    if ($pmnt->rete_iva_total > 0) {
                        $retenciones[$pmnt->purchase_id]['rete_iva_total'] = $pmnt->rete_iva_total;
                        $retenciones[$pmnt->purchase_id]['rete_iva_percentage'] = $pmnt->rete_iva_percentage;
                        $retenciones[$pmnt->purchase_id]['rete_iva_base'] = $pmnt->rete_iva_base;
                        $retenciones[$pmnt->purchase_id]['rete_iva_account'] = $pmnt->rete_iva_account;
                        $retenciones[$pmnt->purchase_id]['rete_iva_assumed'] = $pmnt->rete_iva_assumed;
                        $retenciones[$pmnt->purchase_id]['rete_iva_assumed_account'] = $pmnt->rete_iva_assumed_account;
                    }
                    if ($pmnt->rete_ica_total > 0) {
                        $retenciones[$pmnt->purchase_id]['rete_ica_total'] = $pmnt->rete_ica_total;
                        $retenciones[$pmnt->purchase_id]['rete_ica_percentage'] = $pmnt->rete_ica_percentage;
                        $retenciones[$pmnt->purchase_id]['rete_ica_base'] = $pmnt->rete_ica_base;
                        $retenciones[$pmnt->purchase_id]['rete_ica_account'] = $pmnt->rete_ica_account;
                        $retenciones[$pmnt->purchase_id]['rete_ica_assumed'] = $pmnt->rete_ica_assumed;
                        $retenciones[$pmnt->purchase_id]['rete_ica_assumed_account'] = $pmnt->rete_ica_assumed_account;
                    }
                    if ($pmnt->rete_other_total > 0) {
                        $retenciones[$pmnt->purchase_id]['rete_other_total'] = $pmnt->rete_other_total;
                        $retenciones[$pmnt->purchase_id]['rete_other_percentage'] = $pmnt->rete_other_percentage;
                        $retenciones[$pmnt->purchase_id]['rete_other_base'] = $pmnt->rete_other_base;
                        $retenciones[$pmnt->purchase_id]['rete_other_account'] = $pmnt->rete_other_account;
                        $retenciones[$pmnt->purchase_id]['rete_other_assumed'] = $pmnt->rete_other_assumed;
                        $retenciones[$pmnt->purchase_id]['rete_other_assumed_account'] = $pmnt->rete_other_assumed_account;
                    }
                    if ($pmnt->rete_bomberil_total > 0) {
                        $retenciones[$pmnt->purchase_id]['rete_bomberil_total'] = $pmnt->rete_bomberil_total;
                        $retenciones[$pmnt->purchase_id]['rete_bomberil_percentage'] = $pmnt->rete_bomberil_percentage;
                        $retenciones[$pmnt->purchase_id]['rete_bomberil_base'] = $pmnt->rete_bomberil_base;
                        $retenciones[$pmnt->purchase_id]['rete_bomberil_account'] = $pmnt->rete_bomberil_account;
                        $retenciones[$pmnt->purchase_id]['rete_bomberil_assumed_account'] = $pmnt->rete_bomberil_assumed_account;
                    }
                    if ($pmnt->rete_autoaviso_total > 0) {
                        $retenciones[$pmnt->purchase_id]['rete_autoaviso_total'] = $pmnt->rete_autoaviso_total;
                        $retenciones[$pmnt->purchase_id]['rete_autoaviso_percentage'] = $pmnt->rete_autoaviso_percentage;
                        $retenciones[$pmnt->purchase_id]['rete_autoaviso_base'] = $pmnt->rete_autoaviso_base;
                        $retenciones[$pmnt->purchase_id]['rete_autoaviso_account'] = $pmnt->rete_autoaviso_account;
                        $retenciones[$pmnt->purchase_id]['rete_autoaviso_assumed_account'] = $pmnt->rete_autoaviso_assumed_account;
                    }
                } else {
                    $concepto = [];
                    $total_paid += $pmnt->amount;
                    foreach ($pmnt as $key => $value) {
                        if ($key == 'concept_ledger_id') {
                            $concepto['ledger_id'] = $value;
                            unset($pmnt->{$key});
                        }
                        $concepto[$key] = $value;
                    }
                    $conceptos[] = $concepto;
                }

            }

            $payment = array(
                            'reference_no' => $reference_no,
                            'amount' => $total_paid,
                            'date' => $date,
                            'payments' => $multi_payments,
                            'companies_id' => $data['supplier_id'],
                            'document_type_id' => $data['document_type_id'],
                            'created_by' => $data['created_by'],
                            'note' => '',
                            );
            $data_tax_rate_traslate = [];
            if($this->site->wappsiContabilidadPagosMultiplesComprasAnulacion($payment, $data['supplier_id'], $retenciones, $data_tax_rate_traslate, $conceptos, $data['cost_center_id'])){}

            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);

            return [
                    'response' => true,
                    'reaccount' => $reaccount_id,
                    'reaccount_type' => $reaccount_type,
                    'message' => lang('payment_succesfully_cancelled'),
                   ];
        }
        return false;
    }

    public function validate_supplier_consecutive($consecutive, $supplier_id)
    {
        $q = $this->db->get_where('purchases', ['supplier_id'=>$supplier_id, 'consecutive_supplier'=>$consecutive]);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function getAllReturnsItems($purchase_id, $arr = false)
    {
        $this->db->select('
                            purchase_items.*,
                            tr.code as tax_code,
                            tr.name as tax_name,
                            tr.rate as tax_rate,
                            tr.tax_indicator as tax_indicator,
                            tr2.code as tax_2_code,
                            tr2.name as tax_2_name,
                            tr2.rate as tax_2_rate,
                            products.unit,
                            products.details as details,
                            product_variants.name as variant,
                            products.hsn_code as hsn_code,
                            products.second_name as second_name,
                            units.code as product_unit_code,
                            units.operation_value,
                            units.operator
                           ')
            ->join('purchase_items', 'purchase_items.purchase_id = purchases_return.return_id', 'left')
            ->join('products', 'products.id=purchase_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
            ->join('tax_rates tr', 'tr.id=purchase_items.tax_rate_id', 'left')
            ->join('tax_rates tr2', 'tr2.id=purchase_items.tax_rate_2_id', 'left')
            ->join('units', 'purchase_items.product_unit_id_selected=units.id', 'left')
            ->join('unit_prices', 'purchase_items.product_unit_id_selected = unit_prices.unit_id and unit_prices.id_product = purchase_items.product_id', 'left')
            ->group_by('purchase_items.id')
            ->order_by('id', 'asc');
        $this->db->where('purchases_return.purchase_id', $purchase_id);
        $q = $this->db->get('purchases_return');
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                foreach (($q->result_array()) as $row) {
                    $data[] = $row;
                }
            }
            return $data;
        }
        return FALSE;
    }

    public function getReturnsInvoicesByPurchaseID($id, $arr = false)
    {
        $q = $this->db->select("
                SUM( {$this->db->dbprefix('purchases')}.grand_total ) AS grand_total,
                SUM( {$this->db->dbprefix('purchases')}.surcharge ) AS surcharge,
                SUM( {$this->db->dbprefix('purchases')}.order_discount ) AS order_discount,
                SUM( {$this->db->dbprefix('purchases')}.order_tax ) AS order_tax,
                SUM( {$this->db->dbprefix('purchases')}.paid ) AS paid,
                SUM( {$this->db->dbprefix('purchases')}.product_tax ) AS product_tax,
                SUM(IF({$this->db->dbprefix('purchases')}.rete_fuente_assumed = 0, {$this->db->dbprefix('purchases')}.rete_fuente_total, 0) ) AS rete_fuente_total,
                SUM(IF({$this->db->dbprefix('purchases')}.rete_iva_assumed = 0, {$this->db->dbprefix('purchases')}.rete_iva_total, 0) ) AS rete_iva_total,
                SUM(IF({$this->db->dbprefix('purchases')}.rete_ica_assumed = 0, {$this->db->dbprefix('purchases')}.rete_ica_total, 0) ) AS rete_ica_total,
                SUM(IF({$this->db->dbprefix('purchases')}.rete_other_assumed = 0, {$this->db->dbprefix('purchases')}.rete_other_total, 0) ) AS rete_other_total,
                SUM(IF({$this->db->dbprefix('purchases')}.rete_ica_total = 0, {$this->db->dbprefix('purchases')}.rete_bomberil_total, 0) ) AS rete_bomberil_total,
                SUM(IF({$this->db->dbprefix('purchases')}.rete_ica_total = 0, {$this->db->dbprefix('purchases')}.rete_autoaviso_total, 0) ) AS rete_autoaviso_total")
            ->where(array('purchases_return.purchase_id' => $id))
            ->join('purchases', 'purchases.id = purchases_return.return_id', 'left')
            ->join('documents_types', 'purchases.document_type_id = documents_types.id', 'left')
            ->join('tax_rates', 'tax_rates.id = purchases.order_tax_id', 'left')
            ->get('purchases_return');
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                return $q->row();
            } else {
                return $q->row_array();
            }
        }
        return FALSE;
    }

    public function recontabilizar_pago($reference_no)
    {
        $multi_payments = [];
        $conceptos = [];
        $retenciones = [];
        // exit(var_dump($reference_no));
        $mpayment_enc = $this->site->getMultiPaymentEnc($reference_no) ? $this->site->getMultiPaymentEnc($reference_no) : $this->site->getMultiPaymentEncOnlyConcepts($reference_no);
        $supplier_id = $mpayment_enc[0]->company_id ? $mpayment_enc[0]->company_id : $mpayment_enc[0]->supplier_id;
        $cost_center_id = $mpayment_enc[0]->cost_center_id;
        $details = $this->purchases_model->get_purchase_multi_payments_by_reference($reference_no);
        $total_paid = 0;

        foreach ($details as $mpayment) {
            if ($mpayment->purchase_id) {
                $purchase = $this->getPurchaseByID($mpayment->purchase_id);
                $expense_payment = false;
                if ($purchase->purchase_type == 2 || $purchase->purchase_type == 3) {
                    $purchase_items = $this->getAllPurchaseItems($mpayment->purchase_id);
                    $mpayment->purchase = $purchase;
                    $mpayment->purchase_items = $purchase_items;
                    $mpayment->expense_payment = $purchase->credit_ledger_id;
                }
                $payment = [];
                $total_paid += $mpayment->amount;
                foreach ($mpayment as $key => $value) {
                    $payment[$key] = $value;
                }
                $multi_payments[] = $payment;
                if ($mpayment->rete_fuente_total != 0) {
                    $retenciones[$mpayment->purchase_id]['rete_fuente_total'] = $mpayment->rete_fuente_total < 0 ? ($mpayment->rete_fuente_total * -1) : $mpayment->rete_fuente_total;
                    $retenciones[$mpayment->purchase_id]['rete_fuente_percentage'] = $mpayment->rete_fuente_percentage;
                    $retenciones[$mpayment->purchase_id]['rete_fuente_base'] = $mpayment->rete_fuente_base < 0 ? ($mpayment->rete_fuente_base * -1) : $mpayment->rete_fuente_base;
                    $retenciones[$mpayment->purchase_id]['rete_fuente_account'] = $mpayment->rete_fuente_account;
                    $retenciones[$mpayment->purchase_id]['rete_fuente_assumed'] = $mpayment->rete_fuente_assumed;
                    $retenciones[$mpayment->purchase_id]['rete_fuente_assumed_account'] = $mpayment->rete_fuente_assumed_account;
                }
                if ($mpayment->rete_iva_total != 0) {
                    $retenciones[$mpayment->purchase_id]['rete_iva_total'] = $mpayment->rete_iva_total < 0 ? ($mpayment->rete_iva_total * -1) : $mpayment->rete_iva_total;
                    $retenciones[$mpayment->purchase_id]['rete_iva_percentage'] = $mpayment->rete_iva_percentage;
                    $retenciones[$mpayment->purchase_id]['rete_iva_base'] = $mpayment->rete_iva_base < 0 ? ($mpayment->rete_iva_base * -1) : $mpayment->rete_iva_base;
                    $retenciones[$mpayment->purchase_id]['rete_iva_account'] = $mpayment->rete_iva_account;
                    $retenciones[$mpayment->purchase_id]['rete_iva_assumed'] = $mpayment->rete_iva_assumed;
                    $retenciones[$mpayment->purchase_id]['rete_iva_assumed_account'] = $mpayment->rete_iva_assumed_account;
                }
                if ($mpayment->rete_ica_total != 0) {
                    $retenciones[$mpayment->purchase_id]['rete_ica_total'] = $mpayment->rete_ica_total < 0 ? ($mpayment->rete_ica_total * -1) : $mpayment->rete_ica_total;
                    $retenciones[$mpayment->purchase_id]['rete_ica_percentage'] = $mpayment->rete_ica_percentage;
                    $retenciones[$mpayment->purchase_id]['rete_ica_base'] = $mpayment->rete_ica_base < 0 ? ($mpayment->rete_ica_base * -1) : $mpayment->rete_ica_base;
                    $retenciones[$mpayment->purchase_id]['rete_ica_account'] = $mpayment->rete_ica_account;
                    $retenciones[$mpayment->purchase_id]['rete_ica_assumed'] = $mpayment->rete_ica_assumed;
                    $retenciones[$mpayment->purchase_id]['rete_ica_assumed_account'] = $mpayment->rete_ica_assumed_account;
                }
                if ($mpayment->rete_bomberil_total != 0) {
                    $retenciones[$mpayment->purchase_id]['rete_bomberil_total'] = $mpayment->rete_bomberil_total < 0 ? ($mpayment->rete_bomberil_total * -1) : $mpayment->rete_bomberil_total;
                    $retenciones[$mpayment->purchase_id]['rete_bomberil_percentage'] = $mpayment->rete_bomberil_percentage;
                    $retenciones[$mpayment->purchase_id]['rete_bomberil_base'] = $mpayment->rete_bomberil_base < 0 ? ($mpayment->rete_bomberil_base * -1) : $mpayment->rete_bomberil_base;
                    $retenciones[$mpayment->purchase_id]['rete_bomberil_account'] = $mpayment->rete_bomberil_account;
                    $retenciones[$mpayment->purchase_id]['rete_bomberil_assumed_account'] = $mpayment->rete_bomberil_assumed_account;
                }
                if ($mpayment->rete_autoaviso_total != 0) {
                    $retenciones[$mpayment->purchase_id]['rete_autoaviso_total'] = $mpayment->rete_autoaviso_total < 0 ? ($mpayment->rete_autoaviso_total * -1) : $mpayment->rete_autoaviso_total;
                    $retenciones[$mpayment->purchase_id]['rete_autoaviso_percentage'] = $mpayment->rete_autoaviso_percentage;
                    $retenciones[$mpayment->purchase_id]['rete_autoaviso_base'] = $mpayment->rete_autoaviso_base < 0 ? ($mpayment->rete_autoaviso_base * -1) : $mpayment->rete_autoaviso_base;
                    $retenciones[$mpayment->purchase_id]['rete_autoaviso_account'] = $mpayment->rete_autoaviso_account;
                    $retenciones[$mpayment->purchase_id]['rete_autoaviso_assumed_account'] = $mpayment->rete_autoaviso_assumed_account;
                }
                if ($mpayment->rete_other_total != 0) {
                    $retenciones[$mpayment->purchase_id]['rete_other_total'] = $mpayment->rete_other_total < 0 ? ($mpayment->rete_other_total * -1) : $mpayment->rete_other_total;
                    $retenciones[$mpayment->purchase_id]['rete_other_percentage'] = $mpayment->rete_other_percentage;
                    $retenciones[$mpayment->purchase_id]['rete_other_base'] = $mpayment->rete_other_base < 0 ? ($mpayment->rete_other_base * -1) : $mpayment->rete_other_base;
                    $retenciones[$mpayment->purchase_id]['rete_other_account'] = $mpayment->rete_other_account;
                    $retenciones[$mpayment->purchase_id]['rete_other_assumed'] = $mpayment->rete_other_assumed;
                    $retenciones[$mpayment->purchase_id]['rete_other_assumed_account'] = $mpayment->rete_other_assumed_account;
                }
            } else {
                $concepto = [];
                $total_paid +=  $mpayment->amount;
                foreach ($mpayment as $key => $value) {
                    if ($key == 'concept_ledger_id') {
                        $concepto['ledger_id'] = $value;
                        unset($mpayment->{$key});
                    }
                    $concepto[$key] = $value;
                }
                $conceptos[] = $concepto;
            }
        }
        $payment = array(
                            'reference_no' => $mpayment_enc[0]->reference_no,
                            'amount' => $total_paid,
                            'date' => $mpayment_enc[0]->date,
                            'note' => $mpayment_enc[0]->note,
                            'payments' => $multi_payments,
                            'companies_id' => $supplier_id,
                            'supplier_id' => $supplier_id,
                            'document_type_id' => $mpayment_enc[0]->document_type_id,
                            );

        if ($mpayment_enc[0]->amount >= 0) {
            $this->site->wappsiContabilidadPagosMultiplesCompras($payment, $supplier_id, $retenciones, null, $conceptos, $cost_center_id);
        } else {
            foreach ($multi_payments as $cid => $mpayment) {
                $multi_payments[$cid]['amount'] = $multi_payments[$cid]['amount'] * -1;
            }
            foreach ($conceptos as $cid => $concepto) {
                $conceptos[$cid]['amount'] = $conceptos[$cid]['amount'] * -1;
            }
            $payment['amount'] = $payment['amount'] * -1;
            $payment['payments'] = $multi_payments;
            $this->site->wappsiContabilidadPagosMultiplesComprasAnulacion($payment, $supplier_id, $retenciones, null, $conceptos, $cost_center_id);
        }

    }

    public function recontabilizar_gasto($id)
    {
        $q = $this->db->get_where('expenses', ['id' => $id]);
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
            $data['expense_edit'] = true;
            $item = array(
                        'tax_rate_id' => $data['tax_rate_id'],
                        'item_tax' => $data['tax_val'],
                        'tax_rate_2_id' => $data['tax_rate_2_id'],
                        'item_tax_2' => $data['tax_val_2'],
                        'net_unit_cost' => ($data['amount'] - $data['tax_val'] - $data['tax_val_2']),
                        );
            $this->site->wappsiContabilidadGastosPOS(null, $data, null, $item);
        }
    }

    public function get_credit_note_concepts($isElectronic = false)
    {
        $this->db->select("debit_credit_notes_concepts.id,
                        debit_credit_notes_concepts.dian_code,
                        debit_credit_notes_concepts.description,
                        COALESCE(".$this->db->dbprefix("tax_rates").".rate, 0) AS concept_tax_rate,
                        tax_rates.id AS concept_tax_rate_id");
        $this->db->from("debit_credit_notes_concepts");
        $this->db->join("tax_rates", "tax_rates.id = debit_credit_notes_concepts.tax_id", "left");

        if ($isElectronic) {
            $this->db->not_like("description", "IVA");
        }

        $this->db->where("debit_credit_notes_concepts.type", "NCC");
        $this->db->where("debit_credit_notes_concepts.description IS NOT NULL");
        $this->db->where("debit_credit_notes_concepts.view", 1);
        $response = $this->db->get();

        return $response->result();
    }

    public function insert_return_other_concepts($data = [], $items = [], $payments =[])
    {
        if (empty($data) || empty($items)) { return FALSE; }

        $document_type = $this->get_document_type($data["document_type_id"]);
        $data["reference_no"] = $document_type->prefix ."-". $document_type->consecutive;


        if ($this->db->insert("purchases", $data)) {
            $return_id = $this->db->insert_id();

            $this->update_consecutive_document_type(["sales_consecutive" => ($document_type->consecutive + 1)], $data["document_type_id"]);

            $this->db->query("UPDATE {$this->db->dbprefix('purchases')}
                SET {$this->db->dbprefix('purchases')}.return_purchase_ref = CONCAT(IF({$this->db->dbprefix('purchases')}.return_purchase_ref IS NOT NULL, CONCAT({$this->db->dbprefix('purchases')}.return_purchase_ref, ', '), ''), '".$data['reference_no']."'), return_other_concepts = 1
                WHERE {$this->db->dbprefix('purchases')}.id = ".$data['purchase_id']);

            $this->db->insert('purchases_return', [
                'purchase_id'=> $data['purchase_id'],
                'purchase_reference_no'=> $data['return_purchase_ref'],
                'return_id'=> $return_id,
                'return_reference_no'=> $data['reference_no']
            ]);

            $items["purchase_id"] = $return_id;

            $total_payment = 0;
            foreach ($payments as $payment) {
                $payment['purchase_id'] = $return_id;

                if ($payment['paid_by'] != 'due' && $payment['paid_by'] != 'retencion') {
                    $total_payment += $payment['amount'];
                }

                if ($payment['paid_by'] == 'deposit') {
                    $this->saveDeposit($data, $payment, $return_id);
                    unset($payment['deposit_document_type_id']);

                    // $this->db->insert('payments', $payment);
                    /*} else {
                        $p_arr[] = $payment;
                        $p_arr = $this->site->set_payments_affected_by_deposits($supplier->id, $p_arr);
                        $count = 0;
                        foreach ($p_arr as $pmnt2) {
                            if ($count > 0) {
                                $this->site->updateBillerConsecutive($pmnt2['document_type_id'], $p_consecutive+1);
                                // NUEVA REFERENCIA POR REGISTRO DE OTRO DEPÓSITO
                                $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt2['document_type_id']);
                                if($referenceBiller){
                                    $reference_payment = $referenceBiller;
                                }else{
                                    $reference_payment = $this->site->getReference('rc');
                                }
                                $pmnt2['reference_no'] = $reference_payment;
                                $ref = explode("-", $reference_payment);
                                $p_consecutive = $ref[1];
                                // NUEVA REFERENCIA POR REGISTRO DE OTRO DEPÓSITO
                            }
                            $this->db->insert('payments', $pmnt2);
                            $count++;
                        }
                    }*/
                } /*else {*/
                    $billerId = $data['biller_id'];
                    $referenceNo = $this->site->getReferenceBiller($billerId, $payment['document_type_id']);
                    $payment['reference_no'] = $referenceNo;
                    $ref = explode("-", $referenceNo);
                    $consecutivePayment = $ref[1];

                    if ($this->db->insert('payments', $payment)) {
                        $this->site->updateBillerConsecutive($payment['document_type_id'], $consecutivePayment+1);
                    }
                // }
            }

            $purchase = $this->site->getWappsiPurchaseById($data['purchase_id']);

            if ($purchase->payment_status == 'partial') {
                $total_retenciones = (isset($data['rete_fuente_total']) ? $data['rete_fuente_total'] : 0) + (isset($data['rete_iva_total']) ? $data['rete_iva_total'] : 0) + (isset($data['rete_ica_total']) ? $data['rete_ica_total'] : 0) + (isset($data['rete_other_total']) ? $data['rete_other_total'] : 0);

                $payment_add = array();
                $payment_add['date'] = $data['date'];
                $payment_add['purchase_id'] = $data['purchase_id'];
                $payment_add['amount'] = ($data['grand_total'] * -1) - $total_retenciones - ($total_payment * -1);
                $payment_add['reference_no'] = $purchase->return_purchase_ref;
                $payment_add['paid_by'] = "due";
                $payment_add['type'] = "devolución";
                $payment_add['note'] = "Saldo de devolución restante de crédito ".$data['reference_no'];
                $payment_add['created_by'] = $this->session->userdata('user_id');
                $this->db->insert('payments', $payment_add);
            } else if ($data['payment_status'] == 'paid' && $purchase->payment_status != 'paid') {
                $total_retenciones = (isset($data['rete_fuente_total']) ? $data['rete_fuente_total'] : 0) + (isset($data['rete_iva_total']) ? $data['rete_iva_total'] : 0) + (isset($data['rete_ica_total']) ? $data['rete_ica_total'] : 0) + (isset($data['rete_other_total']) ? $data['rete_other_total'] : 0);

                $payment_add = array();
                $payment_add['date'] = $data['date'];
                $payment_add['purchase_id'] = $data['purchase_id'];
                $payment_add['amount'] = ($data['grand_total'] * -1) - $total_retenciones;
                $payment_add['reference_no'] = $purchase->return_purchase_ref;
                $payment_add['paid_by'] = "due";
                $payment_add['type'] = "devolución";
                $payment_add['note'] = "Saldo de devolución restante de crédito ".$data['reference_no'];
                $payment_add['created_by'] = $this->session->userdata('user_id');
                $this->db->insert('payments', $payment_add);
            }
            $this->site->syncPurchasePayments($return_id);
            $this->site->syncPurchasePayments($data['purchase_id']);

            if ($this->db->insert("purchase_items", $items)) {
                $this->site->wappsiContabilidadComprasDevolucionOtherConcepts($return_id, $data, $items, $payments);
                return TRUE;
            }
            return FALSE;
        }

        return FALSE;
    }

    public function saveDeposit($data, $payment, $return_id)
    {
        $billerId = $data['biller_id'];
        $payment['purchase_id'] = $return_id;
        $depositDocumentTypeId = $payment['deposit_document_type_id'];
        $supplier = $this->site->getCompanyByID($data['supplier_id']);
        $referenceNo = $this->site->getReferenceBiller($billerId, $depositDocumentTypeId);
        $ref = explode("-", $referenceNo);
        $deposit_consecutive = $ref[1];
        $deposit_data = [
            'date'                      => date('Y-m-d H:i:s'),
            'reference_no'              => $referenceNo,
            'amount'                    => abs($payment['amount']),
            'balance'                   => abs($payment['amount']),
            'paid_by'                   => $payment['paid_by'],
            'note'                      => 'Anticipo generado automáticamente por devolución',
            'company_id'                => $data['supplier_id'],
            'created_by'                => $this->session->userdata('user_id'),
            'biller_id'                 => $data['biller_id'],
            'document_type_id'          => $payment['deposit_document_type_id'],
            'origen_reference_no'       => $data['reference_no'],
            'origen_document_type_id'   => $data['document_type_id'],
            'origen'                    => '2',
        ];

        if (isset($data['cost_center_id'])) {
            $deposit_data['cost_center_id'] = $data['cost_center_id'];
        }

        if ($this->db->insert('deposits', $deposit_data)) {
            $this->site->updateBillerConsecutive($depositDocumentTypeId, $deposit_consecutive+1);
            $this->site->wappsiContabilidadDeposito($deposit_data, 2);
            $this->db->update('companies', ['deposit_amount' => ($supplier->deposit_amount + abs($payment['amount']))], ['id'=> $supplier->id]);
        }
    }

    public function get_document_type($id)
    {
        $this->db->select("sales_consecutive AS consecutive, sales_prefix AS prefix");
        $this->db->from("documents_types");
        $this->db->where("id", $id);
        $response = $this->db->get();

        return $response->row();
    }

    public function update_consecutive_document_type($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("documents_types", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function update_reference_invoice($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("purchases", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function product_has_movements_after_date($product_id, $date, $purchase_id)
    {
        $q = $this->db->select('purchase_items.*')
                    ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'inner')
                    ->where('purchase_items.product_id', $product_id)
                    ->where('purchases.date >', $date)
                    ->where('purchase_items.purchase_id !=', $purchase_id)
                    ->get('purchase_items');
        if ($q->num_rows() > 0) {
            return true;
        }
        $q = $this->db->select('sale_items.*')
                    ->join('sales', 'sales.id = sale_items.sale_id', 'inner')
                    ->where('sale_items.product_id', $product_id)
                    ->where('sales.date >=', $date)
                    ->get('sale_items');
        if ($q->num_rows() > 0) {
            return true;
        }
        $q = $this->db->select('adjustment_items.*')
                    ->join('adjustments', 'adjustments.id = adjustment_items.adjustment_id', 'inner')
                    ->where('adjustment_items.product_id', $product_id)
                    ->where('adjustments.date >=', $date)
                    ->get('adjustment_items');
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function get_purchase_direct_payments($purchase_id)
    {
        $q = $this->db->where('paid_by !=', 'retencion')
                      ->where('multi_payment', 0)
                      ->where('purchase_id', $purchase_id)
                      ->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getPurchaseByReferenceNo($referenceNo)
    {
        $this->db->like('reference_no', $referenceNo);
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_total_payments_for_purchase($id){
        $q = $this->db->select('
            SUM(COALESCE(amount, 0)) as total_paid,
            SUM(COALESCE(rete_fuente_base, 0)) as fuente_base,
            SUM(COALESCE(rete_iva_base, 0)) as iva_base,
            SUM(COALESCE(rete_ica_base, 0)) as ica_base,
            SUM(COALESCE(rete_other_base, 0)) as other_base,
            SUM(COALESCE(rete_autoaviso_base, 0)) as autoaviso_base,
            SUM(COALESCE(rete_bomberil_base, 0)) as bomberil_base,
            SUM(COALESCE(rete_fuente_total, 0)) as fuente_total,
            SUM(COALESCE(rete_iva_total, 0)) as iva_total,
            SUM(COALESCE(rete_ica_total, 0)) as ica_total,
            SUM(COALESCE(rete_other_total, 0)) as other_total,
            SUM(COALESCE(rete_autoaviso_total, 0)) as autoaviso_total,
            SUM(COALESCE(rete_bomberil_total, 0)) as bomberil_total
            ')
            ->where('purchase_id', $id)
            ->where('multi_payment', 1)
            ->group_by('purchase_id')
            ->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_tagged_purchases_pending(){

        $this->db->select('
                            purchase_items.*,
                            tr.code as tax_code,
                            tr.name as tax_name,
                            tr.rate as tax_rate,
                            tr.tax_indicator as tax_indicator,
                            tr2.code as tax_2_code,
                            tr2.name as tax_2_name,
                            tr2.rate as tax_2_rate,
                            products.unit,
                            products.details as details,
                            CONCAT('.$this->db->dbprefix('product_variants').'.name, '.$this->db->dbprefix('product_variants').'.suffix) as variant,
                            products.hsn_code as hsn_code,
                            products.second_name as second_name,
                            IF('.$this->db->dbprefix('units').'.code IS NOT NULL, '.$this->db->dbprefix('units').'.code, '.$this->db->dbprefix('purchase_items').'.product_unit_code) as product_unit_code,
                            units.operation_value,
                            units.operator,
                            purchases.reference_no,
                            purchases.date as purchase_date,
                            purchases.grand_total,
                            purchases.total as grand_sub_total,
                            purchases.total_tax,
                            purchases.shipping,
                            purchases.prorated_shipping_cost,
                            '.$this->db->dbprefix('purchase_items').'.shipping_unit_cost,
                            purchases.supplier as purchase_supplier,
                            purchases.purchase_type
                           ')
            ->join('products', 'products.id=purchase_items.product_id', 'left')
            ->join('purchases', 'purchases.id=purchase_items.purchase_id', 'left')
            ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
            ->join('tax_rates tr', 'tr.id=purchase_items.tax_rate_id', 'left')
            ->join('tax_rates tr2', 'tr2.id=purchase_items.tax_rate_2_id', 'left')
            ->join('units', 'purchase_items.product_unit_id_selected=units.id', 'left')
            ->join('unit_prices', 'purchase_items.product_unit_id_selected = unit_prices.unit_id and unit_prices.id_product = purchase_items.product_id', 'left')
            ->where('purchases.status', 'pending')
            ->group_by('purchase_items.id')
            ->order_by('purchases.date', 'desc');
        if ($this->biller_id) {
            $this->db->where('purchases.biller_id', $this->biller_id);
        }
        if ($this->tag_id) {
            $this->db->where('purchases.tag_id', $this->tag_id);
        }
        if ($this->purchase_id) {
            $this->db->where('purchase_items.purchase_id', $this->purchase_id);
        }
        $q = $this->db->get('purchase_items');
        $purchases = [];
        $expenses = [];
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->purchase_type == 1) {
                    if (!isset($purchases[$row->purchase_id."_".$row->reference_no])) {
                        $purchases[$row->purchase_id."_".$row->reference_no]['date'] = $row->purchase_date;
                        $purchases[$row->purchase_id."_".$row->reference_no]['supplier'] = $row->purchase_supplier;
                        $purchases[$row->purchase_id."_".$row->reference_no]['grand_sub_total'] = $row->grand_sub_total;
                        $purchases[$row->purchase_id."_".$row->reference_no]['grand_total'] = $row->grand_total;
                        $purchases[$row->purchase_id."_".$row->reference_no]['shipping'] = $row->shipping;
                        $purchases[$row->purchase_id."_".$row->reference_no]['prorated_shipping_cost'] = $row->prorated_shipping_cost;
                        $purchases[$row->purchase_id."_".$row->reference_no]['total_tax'] = $row->total_tax;
                    } 
                    if(isset($purchases[$row->purchase_id."_".$row->reference_no])){
                        $purchases[$row->purchase_id."_".$row->reference_no]['rows'][] = $row;
                    }
                } else {
                    if (!isset($expenses[$row->purchase_id."_".$row->reference_no])) {
                        $expenses[$row->purchase_id."_".$row->reference_no]['date'] = $row->purchase_date;
                        $expenses[$row->purchase_id."_".$row->reference_no]['supplier'] = $row->purchase_supplier;
                        $expenses[$row->purchase_id."_".$row->reference_no]['grand_sub_total'] = $row->grand_sub_total;
                        $expenses[$row->purchase_id."_".$row->reference_no]['grand_total'] = $row->grand_total;
                        $expenses[$row->purchase_id."_".$row->reference_no]['shipping'] = $row->shipping;
                        $expenses[$row->purchase_id."_".$row->reference_no]['prorated_shipping_cost'] = $row->prorated_shipping_cost;
                        $expenses[$row->purchase_id."_".$row->reference_no]['total_tax'] = $row->total_tax;
                    } 
                    if(isset($expenses[$row->purchase_id."_".$row->reference_no])){
                        $expenses[$row->purchase_id."_".$row->reference_no]['rows'][] = $row;
                    }
                }

            }
            return ['purchases'=>$purchases, 'expenses'=>$expenses];
        }
        return false;
    }
    public function get_purchases_pending_by_reference(){
        $q = $this->db->like('reference_no', $this->term)
                      ->where('status', 'pending')
                      ->where('purchase_type', $this->ptype)
                      ->where('biller_id', $this->biller_id)
                      ->where('(tag_id IS NULL OR tag_id = 0)')
                      ->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }
    public function addImport(){
        $data_ref =  $this->get_reference($this->import);
        $consecutive = $data_ref['consecutive'];
        $update_ref = $data_ref['update_ref'];
        $this->import = $data_ref['data'];
        if ($this->db->insert('imports', $this->import)) {
            $import_id = $this->db->insert_id();
            if ($update_ref) {
                $this->site->updateBillerConsecutive($this->import['document_type_id'], $consecutive+1);
            }
            $purchases_total = [];
            foreach ($this->import_detail as $row) {
                $row['import_id'] = $import_id;
                $this->db->insert('import_detail', $row);
                if ($this->import['status'] == 1) {
                    if (!isset($purchases_total[$row['purchase_id']])) {
                        $purchases_total[$row['purchase_id']] = ($row['adjustment_cost'] * $row['quantity']);
                    } else {
                        $purchases_total[$row['purchase_id']] += ($row['adjustment_cost'] * $row['quantity']);
                    } // se guardan todas las compras afectadas
                    if ($row['adjustment_cost'] > 0) {
                        // $pi = $this->db->get_where('purchase_items', ['id'=>$row['purchase_item_id']])->row();
                        $this->db->update('purchase_items', [
                                        'import_adjustment_cost'=>$row['adjustment_cost'],
                                        'status'=>'received',
                                        // 'net_unit_cost' => $pi->net_unit_cost + $row['adjustment_cost'],
                                        // 'unit_cost' => $pi->unit_cost + $row['adjustment_cost'],
                                            ], ['id'=>$row['purchase_item_id']]);
                    }//aumento con ajuste de costo a detalle de purchase_items
                } else {
                    $this->db->update('purchases', ['import_id'=>$import_id], ['id'=>$row['purchase_id']]);
                }
            }
            if ($this->import['status'] == 1) {
                foreach ($purchases_total as $purchase_id => $purchase_plus) {
                    $data_update = [];
                    if ($purchase_plus > 0) {
                        // $p = $this->db->get_where('purchases', ['id'=>$purchase_id])->row();
                        // $data_update['grand_total'] = ($p->grand_total+$purchase_plus);
                        // $data_update['total'] = ($p->total+$purchase_plus);
                    } // si tiene costo a ajustar para el encabezado
                    $data_update['import_id'] = $import_id;
                    $data_update['status'] = 'received';
                    $data_update['tag_id'] = $this->import['tag_id'];
                    $this->db->update('purchases', $data_update, ['id'=>$purchase_id]);
                    $this->updateStatus($purchase_id, 'received', '');
                    //completado y asignada de importación a compra
                }
                $this->site->wappsiContabilidadImportaciones($this->import, $this->import_detail);
            }
            // $this->sma->print_arrays($this->import, $this->import_detail);
            return true;
        }
    }
    public function updateImport(){
        if ($this->db->update('imports', $this->import, ['id'=>$this->import_id])) {
            $import_id = $this->import_id;
            $purchases_total = [];
            foreach ($this->import_detail as $row) {
                $import_detail_id = $row['import_detail_id']; unset($row['import_detail_id']);
                if ($import_detail_id) {
                    $this->db->update('import_detail', $row, ['id'=>$import_detail_id]);
                } else {
                    $this->db->insert('import_detail', $row);
                }
                if ($this->import['status'] == 1) {
                    if (!isset($purchases_total[$row['purchase_id']])) {
                        $purchases_total[$row['purchase_id']] = ($row['adjustment_cost'] * $row['quantity']);
                    } else {
                        $purchases_total[$row['purchase_id']] += ($row['adjustment_cost'] * $row['quantity']);
                    } // se guardan todas las compras afectadas
                    if ($row['adjustment_cost'] > 0) {
                        // $pi = $this->db->get_where('purchase_items', ['id'=>$row['purchase_item_id']])->row();
                        $this->db->update('purchase_items', [
                                        'import_adjustment_cost'=>$row['adjustment_cost'],
                                        'status'=>'received',
                                        // 'net_unit_cost' => $pi->net_unit_cost + $row['adjustment_cost'],
                                        // 'unit_cost' => $pi->unit_cost + $row['adjustment_cost'],
                                            ], ['id'=>$row['purchase_item_id']]);
                    }//aumento con ajuste de costo a detalle de purchase_items
                } else {
                    $this->db->update('purchases', ['import_id'=>$import_id], ['id'=>$row['purchase_id']]);
                }
            }
            if ($this->import['status'] == 1) {
                foreach ($purchases_total as $purchase_id => $purchase_plus) {
                    $data_update = [];
                    if ($purchase_plus > 0) {
                        // $p = $this->db->get_where('purchases', ['id'=>$purchase_id])->row();
                        // $data_update['grand_total'] = ($p->grand_total+$purchase_plus);
                        // $data_update['total'] = ($p->total+$purchase_plus);
                    } // si tiene costo a ajustar para el encabezado
                    $data_update['import_id'] = $import_id;
                    $data_update['status'] = 'received';
                    $data_update['tag_id'] = $this->import['tag_id'];
                    $this->db->update('purchases', $data_update, ['id'=>$purchase_id]);
                    $this->updateStatus($purchase_id, 'received', '');
                    //completado y asignada de importación a compra
                }
                $import_data = $this->db->get_where('imports', ['id'=>$this->import_id])->row_array();
                $this->site->wappsiContabilidadImportaciones($import_data, $this->import_detail);
            }
            if ($this->deleted_detail) {
                foreach ($this->deleted_detail as $dkey => $did) {
                    $this->db->delete('import_detail', ['id'=>$did]);
                }
            }
            return true;
        }
    }
    public function get_import_by_id($id){
        $q = $this->db->get_where('imports', ['id'=>$id])->row();
        return $q;
    }
    public function get_import_detail($id){
        $q = $this->db->select('
                            import_detail.*,
                            import_detail.id as import_detail_id,
                            IF('.$this->db->dbprefix("import_detail").'.product_id IS NULL, '.$this->db->dbprefix("import_detail").'.expense_category_id, '.$this->db->dbprefix("import_detail").'.product_id) as product_id,
                            purchase_items.id as id,
                            purchase_items.item_tax as item_tax,
                            purchase_items.net_unit_cost as net_unit_cost,
                            purchase_items.unit_cost as unit_cost,
                            purchase_items.product_code as product_code,
                            purchase_items.product_name as product_name,
                            purchases.purchase_type,
                            purchases.reference_no,
                            purchases.date as purchase_date,
                            purchases.supplier as purchase_supplier,
                            purchases.total as grand_sub_total,
                            purchases.grand_total
                            ')
                 ->join('purchases', 'purchases.id = import_detail.purchase_id')
                 ->join('purchase_items', 'purchase_items.id = import_detail.purchase_item_id')
                 ->where('import_detail.import_id', $id)
                 ->get('import_detail');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->purchase_type == 1) {
                    if (!isset($purchases[$row->purchase_id."_".$row->reference_no])) {
                        $purchases[$row->purchase_id."_".$row->reference_no]['date'] = $row->purchase_date;
                        $purchases[$row->purchase_id."_".$row->reference_no]['supplier'] = $row->purchase_supplier;
                        $purchases[$row->purchase_id."_".$row->reference_no]['grand_sub_total'] = $row->grand_sub_total;
                        $purchases[$row->purchase_id."_".$row->reference_no]['grand_total'] = $row->grand_total;
                    }
                    if(isset($purchases[$row->purchase_id."_".$row->reference_no])){
                        $purchases[$row->purchase_id."_".$row->reference_no]['rows'][] = $row;
                    }
                } else {
                    if (!isset($expenses[$row->purchase_id."_".$row->reference_no])) {
                        $expenses[$row->purchase_id."_".$row->reference_no]['date'] = $row->purchase_date;
                        $expenses[$row->purchase_id."_".$row->reference_no]['supplier'] = $row->purchase_supplier;
                        $expenses[$row->purchase_id."_".$row->reference_no]['grand_sub_total'] = $row->grand_sub_total;
                        $expenses[$row->purchase_id."_".$row->reference_no]['grand_total'] = $row->grand_total;
                    }
                    if(isset($expenses[$row->purchase_id."_".$row->reference_no])){
                        $expenses[$row->purchase_id."_".$row->reference_no]['rows'][] = $row;
                    }
                }

            }
            return ['purchases'=>$purchases, 'expenses'=>$expenses];
        }
    }
    public function change_purchase_tag($id, $tag_id){
        $this->db->update('purchases', ['tag_id'=>$tag_id], ['id'=>$id]);
        return true;
    }
    public function delete_purchase($id){
        if ($this->db->delete('purchase_items', ['purchase_id'=>$id])) {
            $this->db->delete('payments', ['purchase_id'=>$id]);
            if ($this->db->delete('purchases', ['id'=>$id])) {
                return true;
            }
        }
        return false;
    }
    public function updatePurchaseEvaluation($id, $data){
        $data['purchase_id'] = $id;
        if ($this->db->insert('purchases_evaluation', $data)) {
            $this->db->update('purchases', ['purchase_evaluated'=>1], ['id'=>$id]);
            return true;
        }
        return false;
    }
    public function getPurchaseEvaluationById($id)
    {
        $q = $this->db->get_where('purchases_evaluation', array('purchase_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}