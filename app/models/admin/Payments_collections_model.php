<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Payments_collections_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function find($data)
    {
        $this->db->where($data);
        $q = $this->db->get("payment_collection");
        return $q->row();
    }

    public function add($data)
    {
        $referenceBiller = $this->site->getReferenceBiller($data[0]['biller_id'], $data[0]['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        $cons_updated = false;
        foreach ($data as $keyD => $valueD) {
            // deposits
            if ($valueD['paid_by'] == 'deposit') {
                if (!$this->site->check_customer_deposit($valueD['customer_id'], $valueD['amount'])) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $dataAux[1] = $data[$keyD];  // se crea este auxiliar para no enviar todo el array de pagos
                $res = $this->site->set_payments_affected_by_deposits($valueD['customer_id'], $dataAux);
                $dataD[] = $res[2];
                // como se envia el array con indice 1 y en el metodo se ejecuta un unset y unset no reindexa siempre la respuesta sera el indice 2
            }else{
                $dataD[] = $valueD;
            }
        }

        $ref_checked = false;
        foreach ($dataD as $key => $dataArr) {
            $data[$key]['reference_no'] = $reference;
            $dataArr['reference_no'] = $reference;
            if ($this->db->insert('payment_collection', $dataArr)) {
                $payment_id = $this->db->insert_id();
                if (!$ref_checked) {
                    $check_reference_data = [
                        'table' => 'payment_collection', 'record_id' => $payment_id, 'reference' => $dataArr['reference_no'], 'biller_id' => $dataArr['biller_id'], 'document_type_id' => $dataArr['document_type_id']
                        ];
                    if ($new_reference = $this->site->check_reference($check_reference_data)) {
                        $dataArr['reference_no'] = $new_reference;
                        $ref_nueva = explode("-", $new_reference);
                        $consecutive = $ref_nueva[1];
                        $reference = $new_reference;
                    }
                    $ref_checked = true;
                }
                if (!$cons_updated) {
                    $this->site->updateBillerConsecutive($dataArr['document_type_id'], $consecutive+1);
                    $cons_updated = true;
                }
            }
        }
        if ($this->Settings->modulary == 1) {
            $this->site->wappsiContabilidadRecaudo($data);
        }
        return $ref[0]."-".$consecutive; // se cambiar el return true para enviar la reference, necesaria para cargar la vista
    }

    public function get_payment_collection($reference)
    {
        $q = $this->db->select('payment_collection.*, companies.name as customer_name, companies.vat_no AS vat_no')
                      ->join('companies', 'companies.id = payment_collection.customer_id', 'left')
                      ->where('payment_collection.reference_no', $reference)
                      ->get('payment_collection');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function getPaymentCollectionByID($id)
    {
        $this->db->select('payment_collection.*')
            ->where(array('payment_collection.id' => $id));
        $q = $this->db->get('payment_collection');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentCollectionByRef($ref)
    {
        $this->db->select('payment_collection.*')
            ->where(array('payment_collection.reference_no' => $ref));
        $q = $this->db->get('payment_collection');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function update($data)
    {
        // deposits
        if ($data['paid_by'] == 'deposit') {
            if (!$this->site->check_customer_deposit($data['customer_id'], $data['amount'])) {
                $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $dataAux[1] = $data;  // se crea este auxiliar para no enviar todo el array de pagos
            $res = $this->site->set_payments_affected_by_deposits($data['customer_id'], $dataAux);
        }

        if ($this->db->update('payment_collection', array('paid_by' => $data['paid_by']), array('id' => $data['id']) )) {
            return true;
        }
        return false;
    }
}