<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('sales/orders', ['id'=>'ordersFilterForm']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('date_records_filter') ?></label>
                                            <select class="form-control select" name="date_records_filter" id="date_records_filter_dh">
                                                <?php $dateRecordsFilter = (isset($_POST["date_records_filter"]) ? $_POST["date_records_filter"] : ''); ?>
                                                <?= $this->sma->get_filter_options($dateRecordsFilter); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if (!$this->session->userdata('biller_id')): ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label><?= $this->lang->line('biller') ?></label>
                                                <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                                <?php $billerId = (empty($billerId)) ? $this->session->userdata('biller_id') : $billerId; ?>
                                                <select class="form-control select" id="biller_id" name="biller_id">
                                                    <option value=""><?= $this->lang->line('allsf') ?></option>
                                                    <?php foreach ($billers as $biller) : ?>
                                                        <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="client"><?= $this->lang->line('customer') ?></label>
                                            <?php $client = (isset($_POST["client"])) ? $_POST["client"] : ''; ?>
                                            <!-- <select class="form-control" name="client" id="client">
                                                <option value="">Todillo</option>
                                            </select> -->
                                            <input type="text" class="form-control input-tip" name="client" id="client" value="<?= $client ?>" placeholder="<?= lang('alls') ?>">
                                        </div>
                                    </div>
                                    <?php if (!$this->session->userdata('company_id')): ?>
                                        <div class="col-sm-2">
                                            <label><?= $this->lang->line('seller') ?></label>
                                            <?php $sellerId = (isset($_POST["seller"])) ? $_POST["seller"] : ''; ?>
                                            <select class="form-control" name="seller" id="seller">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($sellers as $seller): ?>
                                                    <option value="<?= $seller->id ?>" <?= ($seller->id == $sellerId) ? 'selected' : '' ?>><?= $seller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                        

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('origin') ?></label>
                                            <?php $saleOrigin = (isset($_POST["order_sale_origin"])) ? $_POST["order_sale_origin"] : ''; ?>
                                            <select class="form-control" id="order_sale_origin" name="order_sale_origin">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($salesOrigins as $origin) : ?>
                                                    <option value="<?= $origin->id ?>" <?= ($origin->id == $saleOrigin) ? 'selected' : '' ?>><?= $origin->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="sale_status"><?= $this->lang->line('sale_status') ?></label>
                                            <?php $saleStatusId = (isset($_POST["sale_status"])) ? $_POST["sale_status"] : ''; ?>
                                            <select name="sale_status" id="sale_status" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($saleStatus as $saleState) : ?>
                                                    <option value="<?= $saleState->id ?>" <?= ($saleState->id == $saleStatusId) ? 'selected' : '' ?>><?= $saleState->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('document_type') ?></label>
                                            <?php $documentTypeId = (isset($_POST["document_type"])) ? $_POST["document_type"] : ''; ?>
                                            <select class="form-control select" id="document_type" name="document_type">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($documentsTypes as $documentType) : ?>
                                                    <option value="<?= $documentType->id ?>" <?= ($documentType->id == $documentTypeId) ? 'selected' : 'no' ?>><?= $documentType->sales_prefix." - ".$documentType->nombre ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="warehouse_id"><?= $this->lang->line('warehouse') ?></label>
                                            <?php $warehouseId = (isset($_POST["warehouse_id"])) ? $_POST["warehouse_id"] : ''; ?>
                                            <select class="form-control" id="warehouse_id" name="warehouse_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($warehouses as $warehouse) : ?>
                                                    <option value="<?= $warehouse->id ?>" <?= ($warehouse->id == $warehouseId) ? 'selected' : '' ?>><?= $warehouse->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('day_delivery_time') ?></label>
                                            <?php $deliveryDay = (isset($_POST["delivery_day"])) ? $_POST["delivery_day"] : ''; ?>
                                            <input class="form-control date" type="text" name="delivery_day" id="delivery_day" value="<?= $deliveryDay ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('delivery_time') ?></label>
                                            <div class="input-group input-daterange">
                                                <?php $time_1 = (isset($_POST["time_1"])) ? $_POST["time_1"] : ''; ?>
                                                <input class="form-control" type="time" name="time_1" id="time_1" value="<?= $time_1 ?>">
                                                <div class="input-group-addon">a</div>
                                                <?php $time_2 = (isset($_POST["time_2"])) ? $_POST["time_2"] : ''; ?>
                                                <input class="form-control" type="time" name="time_2" id="time_2" value="<?= $time_2 ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <?php if ($this->Owner || $this->Admin): ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="user"><?= $this->lang->line('user') ?></label>
                                                <?php $userId = (isset($_POST["user"])) ? $_POST["user"] : ''; ?>
                                                <select class="form-control" name="user" id="user">
                                                    <option value=""><?= lang('alls') ?></option>
                                                    <?php foreach ($users as $user): ?>
                                                        <option value="<?= $user->id ?>" <?= ($user->id == $userId) ? 'selected' : '' ?>><?= $user->first_name." ".$user->last_name ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <div class="date_controls_dh">
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for="start_date_dh"><?= $this->lang->line('start_date') ?></label>
                                                <?php $startDate = isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>
                                                <input class="form-control datetime" type="text" name="start_date" id="start_date_dh" value="<?= $startDate ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="date_controls_dh">
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for="end_date_dh"><?= $this->lang->line('end_date') ?></label>
                                                <?php $endDate = isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>
                                                <input class="form-control datetime" type="text" name="end_date" id="end_date_dh" value="<?= $endDate ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div id="locationContainer">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang("country", "country"); ?>
                                                <?php $countryId = (isset($_POST['country'])) ? $_POST['country']: '' ?>
                                                <select class="form-control select" name="country" id="country">
                                                    <option value=""><?= $this->lang->line("alls") ?></option>
                                                    <?php if (!empty($countries)) : ?>
                                                        <?php foreach ($countries as $country): ?>
                                                            <option value="<?= $country->NOMBRE ?>" code="<?= $country->CODIGO ?>" <?= ($country->NOMBRE == $countryId) ? 'selected' : '' ?>><?= $country->NOMBRE ?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                                <input type="hidden" name="countryCode" id="countryCode" value="<?= $_POST['countryCode'] ?? '' ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="state"><?= $this->lang->line("state") ?></label>
                                                <?php $stateId = (isset($_POST['state'])) ? $_POST['state']: '' ?>
                                                <select class="form-control" name="state" id="state">
                                                    <option value=""><?= $this->lang->line("alls") ?></option>
                                                    <?php if (!empty($states)) : ?>
                                                        <?php foreach ($states as $state): ?>
                                                            <option value="<?= $state->DEPARTAMENTO ?>" data-code="<?= $state->CODDEPARTAMENTO ?>" <?= ($state->DEPARTAMENTO == $stateId) ? 'selected' : '' ?>><?= $state->DEPARTAMENTO ?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                                <input type="hidden" name="stateCode" id="stateCode" value="<?= $_POST['stateCode'] ?? '' ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="city"><?= $this->lang->line("city"); ?></label>
                                                <?php $cityId = (isset($_POST['city'])) ? $_POST['city']: '' ?>
                                                <select class="form-control" name="city" id="city">
                                                    <option value=""><?= $this->lang->line("allsf") ?></option>
                                                    <?php if (!empty($cities)) : ?>
                                                        <?php foreach ($cities as $city): ?>
                                                            <option value="<?= $city->DESCRIPCION ?>" data-code="<?= $city->CODIGO ?>" <?= ($city->DESCRIPCION == $cityId) ? 'selected' : '' ?>><?= $city->DESCRIPCION ?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                                <input type="hidden" name="cityCode" id="cityCode" value="<?= $_POST['cityCode'] ?? '' ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="zone"><?= $this->lang->line("zone"); ?></label>
                                                <?php $zoneId = (isset($_POST['zone'])) ? $_POST['zone']: '' ?>
                                                <select class="form-control select" name="zone" id="zone">
                                                    <option value=""><?= $this->lang->line("allsf") ?></option>
                                                    <?php if (!empty($zones)) : ?>
                                                        <?php foreach ($zones as $zone): ?>
                                                            <option value="<?= $zone->id ?>" data-code="<?= $zone->zone_code ?>" <?= ($zone->id == $zoneId) ? 'selected' : '' ?>><?= $zone->zone_name ?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                                <input type="hidden" name="zoneCode" id="zoneCode" value="<?= $_POST['zoneCode'] ?? '' ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="subzone"><?= $this->lang->line("subzone"); ?></label>
                                                <?php $subzoneId = (isset($_POST['subzone'])) ? $_POST['subzone']: '' ?>
                                                <select class="form-control select" name="subzone" id="subzone">
                                                    <option value=""><?= $this->lang->line("allsf") ?></option>
                                                    <?php if (!empty($subzones)) : ?>
                                                        <?php foreach ($subzones as $subzone): ?>
                                                            <option value="<?= $subzone->id ?>" data-code="<?= $subzone->subzone_code ?>" <?= ($subzone->id == $subzoneId) ? 'selected' : '' ?>><?= $subzone->subzone_name ?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                                <input type="hidden" name="subzoneCode" id="subzoneCode" value="<?= $_POST['subzoneCode'] ?? '' ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>

    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('sales/order_actions', 'id="action-form"');
    } ?>
    <input type="hidden" name="new_order_sale_status" id="new_order_sale_status">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="oSLData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("destination_reference_no"); ?></th>
                                            <th><?= lang("biller"); ?></th>
                                            <th><?= lang("customer"); ?></th>
                                            <th><?= lang("seller"); ?></th>
                                            <th><?= lang("order_status"); ?></th>
                                            <?php if ($this->Settings->management_order_sale_delivery_time == 1) : ?>
                                                <th><?= lang("delivery_time"); ?></th>
                                            <?php endif ?>
                                            <th><?= lang("grand_total"); ?></th>
                                            <th><?= lang("origin"); ?></th>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                            <th></th>
                                            <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="12" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<div class="modal fade" id="orderSaleStatus" tabindex="-1" role="dialog" aria-labelledby="orderSaleStatusLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h3><?= lang('change_order_status') ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-2x">&times;</i>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-sm-12">
                        <?= lang('order_status', 'order_status') ?>
                        <select name="order_status" class="form-control" id="order_status">
                            <option value=""><?= lang('select') ?></option>
                            <option value="pending"><?= lang('pending') ?></option>
                            <option value="enlistment"><?= lang('enlistment') ?></option>
                            <option value="completed"><?= lang('invoiced') ?></option>
                            <option value="sent"><?= lang('sent') ?></option>
                            <option value="delivered"><?= lang('delivered') ?></option>
                        </select>
                    </div>
                    <div class="col-lg-12">
                        <h5 style="cursor: pointer;" class="show_info"> <i class="fa fa-exclamation-circle"></i> Clic para información</h5>
                        <div class="change_os_info" style="display: none;">
                            <p>
                                1. Se puede cambiar de "Pendiente" sólo a "En alistamiento" o viceversa
                                <br>
                                2. El estado cambia automáticamente a "Parcial" cuándo se facture uno o unos productos de la orden, o a "Facturado" si se facturó la orden completamente
                                <br>
                                3. si la orden de pedido está en estado "parcial", no deja cambiar estado
                                <br>
                                4. si la orden está en estado "Facturado", se puede cambiar a "Enviado" o "Entregado"
                                <br>
                                5. se puede cambiar de "Enviado" o de "Entregado" sólo a "Facturado"
                                <br>
                                6. Si la orden ya está anulada, no se puede cambiar el estado
                                <br>
                                <br>
                                <b>Importante </b>: Se ignorarán las ordenes que no cumplan con estas condiciones según el estado escogido a cambiar; Por lo tanto, sólo se actualizarán las que sí cumplen las condiciones.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12">
                    <button class="btn btn-success" id="update_order_status" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var filtered = 0;
        _tabFilterFill = false;

        removeOslls();
        loadDataTables();
        loadAutocompleteClient();

        $(document).on('change', '#date_records_filter_dh', function() { showHidefilterContent($(this)) });
        $(document).on('change', '#biller_id', function() { loadDocumentsType($(this)); /* loadWarehouses($(this)); */ });
        $(document).on('change', '#client', function() { showHideLocation($(this)); /* loadWarehouses($(this)); */ });
        $(document).on('change', '#country', function(){ getStates(); });
        $(document).on('change', '#state', function(){ getCities(); });
        $(document).on('change', '#city', function() { getZones(); });
        $(document).on('change', '#zone', function() { getSubzones() });
        $(document).on('change', '#subzone', function() { getSubzoneCode() });
        $(document).on('click', '#get_csv_orders', function() { getCsvOrders() });
        $(document).on('click', '#change_order_status', function() { changeOrderStatus() });
        $(document).on('click', '#complete_order_sales', function() { completeOrderSales() });
        $(document).on('click', '.show_info', function() { showInfo() });
        $(document).on('click', '#update_order_status', function() { updateOrderStatus() });
        $(document).on('click', '.oslduplicate', function(e) { oslduplicate(e) });
        $(document).on('click', '.osledit', function(e) { osledit(e) });
        $(document).on('click', '#sync', function (e) { synchronizeFromEcommerce(e); });

        $('[data-toggle="tooltip"]').tooltip();
    });

    function removeOslls()
    {
        if (localStorage.getItem('remove_oslls')) {
            if (localStorage.getItem('oslitems')) {
                localStorage.removeItem('oslitems');
            }
            if (localStorage.getItem('osldiscount')) {
                localStorage.removeItem('osldiscount');
            }
            if (localStorage.getItem('osltax2')) {
                localStorage.removeItem('osltax2');
            }
            if (localStorage.getItem('oslref')) {
                localStorage.removeItem('oslref');
            }
            if (localStorage.getItem('oslshipping')) {
                localStorage.removeItem('oslshipping');
            }
            if (localStorage.getItem('oslwarehouse')) {
                localStorage.removeItem('oslwarehouse');
            }
            if (localStorage.getItem('oslnote')) {
                localStorage.removeItem('oslnote');
            }
            if (localStorage.getItem('oslinnote')) {
                localStorage.removeItem('oslinnote');
            }
            if (localStorage.getItem('oslcustomer')) {
                localStorage.removeItem('oslcustomer');
            }
            if (localStorage.getItem('oslbiller')) {
                localStorage.removeItem('oslbiller');
            }
            if (localStorage.getItem('oslcurrency')) {
                localStorage.removeItem('oslcurrency');
            }
            if (localStorage.getItem('osldate')) {
                localStorage.removeItem('osldate');
            }
            if (localStorage.getItem('oslsale_status')) {
                localStorage.removeItem('oslsale_status');
            }
            if (localStorage.getItem('oslpayment_status')) {
                localStorage.removeItem('oslpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('oslpayment_term')) {
                localStorage.removeItem('oslpayment_term');
            }
            if (localStorage.getItem('oslcustomerbranch')) {
                localStorage.removeItem('oslcustomerbranch');
            }
            if (localStorage.getItem('oslseller')) {
                localStorage.removeItem('oslseller');
            }
            if (localStorage.getItem('oslcustomerspecialdiscount')) {
                localStorage.removeItem('oslcustomerspecialdiscount');
            }
            localStorage.removeItem('remove_oslls');
        }

        <?php if ($this->session->userdata('remove_oslls')) { ?>
            if (localStorage.getItem('oslitems')) {
                localStorage.removeItem('oslitems');
            }
            if (localStorage.getItem('osldiscount')) {
                localStorage.removeItem('osldiscount');
            }
            if (localStorage.getItem('osltax2')) {
                localStorage.removeItem('osltax2');
            }
            if (localStorage.getItem('oslref')) {
                localStorage.removeItem('oslref');
            }
            if (localStorage.getItem('oslshipping')) {
                localStorage.removeItem('oslshipping');
            }
            if (localStorage.getItem('oslwarehouse')) {
                localStorage.removeItem('oslwarehouse');
            }
            if (localStorage.getItem('oslnote')) {
                localStorage.removeItem('oslnote');
            }
            if (localStorage.getItem('oslinnote')) {
                localStorage.removeItem('oslinnote');
            }
            if (localStorage.getItem('oslcustomer')) {
                localStorage.removeItem('oslcustomer');
            }
            if (localStorage.getItem('oslbiller')) {
                localStorage.removeItem('oslbiller');
            }
            if (localStorage.getItem('oslcurrency')) {
                localStorage.removeItem('oslcurrency');
            }
            if (localStorage.getItem('osldate')) {
                localStorage.removeItem('osldate');
            }
            if (localStorage.getItem('oslsale_status')) {
                localStorage.removeItem('oslsale_status');
            }
            if (localStorage.getItem('oslpayment_status')) {
                localStorage.removeItem('oslpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('oslpayment_term')) {
                localStorage.removeItem('oslpayment_term');
            }
            if (localStorage.getItem('oslcustomerbranch')) {
                localStorage.removeItem('oslcustomerbranch');
            }
            if (localStorage.getItem('oslseller')) {
                localStorage.removeItem('oslseller');
            }
            if (localStorage.getItem('oslcustomerspecialdiscount')) {
                localStorage.removeItem('oslcustomerspecialdiscount');
            }
            <?php $this->sma->unset_data('remove_oslls'); ?>
        <?php } ?>
    }

    function loadAutocompleteClient()
    {
        $('#client').val('<?= $client ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0])
                        showHideLocation($(element))
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        data.results.unshift({id: '', text: 'Todos'})
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                },
            }
        });
    }

    function loadDataTables()
    {
        var _tabFilterFill = false;
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        oTable = $('#oSLData').dataTable({
            aaSorting: [ [1, "desc"], [2, "desc"] ],
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            // sAjaxSource: '<?php //admin_url('sales/getOrders' . ($warehouse_id ? '/' . $warehouse_id : '') . '?v=1' . ($this->input->get('shop') ? '&shop=' . $this->input->get('shop') : '') . ($this->input->get('attachment') ? '&attachment=' . $this->input->get('attachment') : '') . ($this->input->get('delivery') ? '&delivery=' . $this->input->get('delivery') : '') . '&dias_vencimiento=' . (isset($dias_vencimiento) ? $dias_vencimiento : '')); ?>',
            sAjaxSource: '<?= admin_url('sales/getOrders') ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({ "name": "<?= $this->security->get_csrf_token_name() ?>", "value": "<?= $this->security->get_csrf_hash() ?>" })
                aoData.push({ "name": "start_date", "value": $('#start_date_dh').val() })
                aoData.push({ "name": "end_date", "value": $('#end_date').val() })
                aoData.push({ "name": "biller_id", "value": $('#biller_id').val() })
                aoData.push({ "name": "client", "value": $('#client').val() })
                aoData.push({ "name": "seller", "value": $('#seller').val() })
                aoData.push({ "name": "order_sale_origin", "value": $('#order_sale_origin').val() })
                aoData.push({ "name": "sale_status", "value": $('#sale_status').val() })
                aoData.push({ "name": "document_type", "value": $('#document_type').val() })
                aoData.push({ "name": "warehouse", "value": $('#warehouse_id').val() })
                aoData.push({ "name": "delivery_day", "value": $('#delivery_day').val() })
                aoData.push({ "name": "time_1", "value": $('#time_1').val() })
                aoData.push({ "name": "time_2", "value": $('#time_2').val() })
                aoData.push({ "name": "user", "value": $('#user').val() })
                aoData.push({ "name": "country", "value": $('#country').val() })
                aoData.push({ "name": "state", "value": $('#state').val() })
                aoData.push({ "name": "city", "value": $('#city').val() })
                aoData.push({ "name": "zone", "value": $('#zone').val() })
                aoData.push({ "name": "subzone", "value": $('#subzone').val() })
                aoData.push({ "name": "option_filter", "value": option_filter })

                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.setAttribute('data-return-id', aData[14]);
                nRow.className = "order_link re" + aData[14];
                return nRow;
            },
            aoColumns: [{
                    "bSortable": false,
                    "mRender": checkbox
                },
                {
                    "mRender": fld
                },
                null,
                null,
                null,
                null,
                null,
                {
                    "mRender": order_row_status
                },
                <?php if ($this->Settings->management_order_sale_delivery_time == 1) : ?> {
                        "mRender": order_delivery_time
                    },
                <?php endif ?> {
                    "mRender": currencyFormat
                },
                {
                    "mRender": order_origin
                },
                {
                    "bSortable": false,
                    "mRender": attachment
                },
                {
                    "bVisible": false
                },
                {
                    "bSortable": false
                }
            ],
            fnDrawCallback: function (settings) {
                if (!_tabFilterFill) {
                    $('.additionalControlsContainer').html('<div class="wizard">' +
                        '<div class="steps clearfix">' +
                            '<ul role="tablist">' +
                                '<li role="tab" class="first current index_list" aria-disabled="false" aria-selected="true">' +
                                    '<a class="wizard_index_step" data-optionfilter="all" href="#" aria-controls="edit_biller_form-p-0">' +
                                        '<span class="none_span"></span><br>Todos ' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pending" href="#">'+
                                        '<span class="pending_span"></span><br>Pendientes' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false">' +
                                    '<a  class="wizard_index_step" data-optionfilter="completed" href="#">'+
                                        '<span class="completed_span"></span><br>Facturados ' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false">' +
                                    '<a  class="wizard_index_step" data-optionfilter="cancelled" href="#">'+
                                        '<span class="cancelled_span"></span><br>Anuladas' +
                                    '</a>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>').addClass('col-without-padding');

                    _tabFilterFill = true;
                }

                $('.actionsButtonContainer').html(`<a href="<?= admin_url('sales/add_order') ?>" class="btn btn-primary new-button pull-right" id="add" data-toggle-second="tooltip" data-placement="top" title="<?= lang("add") ?>">
                    <i class="fas fa-plus fa-lg"></i>
                </a>
                <div class="dropdown pull-right"">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a id="get_csv_orders">
                                <i class="fa fa-plus-circle"></i> Crear ordenes desde CSV en FTP
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" data-action="print_batch">
                                <i class="fa fa-file-pdf-o"></i> <?= lang('print_batch') ?>
                            </a>
                        </li>
                        <li>
                            <a id="change_order_status">
                                <i class="fa fa-plus-circle"></i> <?= lang('change_status') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" data-action="complete_order_sales">
                                <i class="fa fa-copy"></i> <?= lang('complete_order_sales') ?>
                            </a>
                        </li>
                        <li>
                            <a id="sync">
                                <i class="fa fa-sync"></i> <?= lang('synchronize_order_orders_from_store') ?>
                            </a>
                        </li>`+
                        (site.settings.recurring_sale_management ? `<li>
                            <a href="`+site.base_url+`sales/create_recurring_orders">
                                <i class="fa fa-sync"></i> <?= lang('create_recurring_orders') ?>
                            </a>
                        </li>` : '')+
                    `</ul>
                </div>`);

                $('[data-toggle-second="tooltip"]').tooltip();
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue',
                    increaseArea: '20%'
                });

                loadDataTabFilters();
            }
        });
    }

    function loadDataTabFilters()
    {
        $.ajax({
            // url: '<?= admin_url('sales/getOrders' . ($warehouse_id ? '/' . $warehouse_id : '') . '?v=1' . ($this->input->get('shop') ? '&shop=' . $this->input->get('shop') : '') . ($this->input->get('attachment') ? '&attachment=' . $this->input->get('attachment') : '') . ($this->input->get('delivery') ? '&delivery=' . $this->input->get('delivery') : '') . '&dias_vencimiento=' . (isset($dias_vencimiento) ? $dias_vencimiento : '')); ?>',
            url: '<?= admin_url('sales/getOrders') ?>',
            dataType: 'JSON',
            type: 'POST',
            data: {
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                start_date: $('#start_date_dh').val(),
                end_date: $('#end_date').val(),
                biller_id: $('#biller_id').val(),
                client: $('#client').val(),
                seller: $('#seller').val(),
                order_sale_origin: $('#order_sale_origin').val(),
                sale_status: $('#sale_status').val(),
                document_type: $('#document_type').val(),
                warehouse: $('#warehouse_id').val(),
                delivery_day: $('#delivery_day').val(),
                time_1: $('#time_1').val(),
                time_2: $('#time_2').val(),
                user: $('#user').val(),
                country: $('#country').val(),
                state: $('#state').val(),
                city: $('#city').val(),
                zone: $('#zone').val(),
                subzone: $('#subzone').val(),
                option_filter: option_filter,
                get_json: true
            }
        }).done(function(data) {
            $('.none_span').text(data.none);
            $('.completed_span').text(data.completed);
            $('.pending_span').text(data.pending);
            $('.cancelled_span').text(data.cancelled);
        });
    }

    function showHidefilterContent(element)
    {
        var advancedFiltersContainer = '<?= $advancedFiltersContainer ? 1 : 0 ?>';

        var dateFilter = element.val();
        if (dateFilter != '') {
            if (dateFilter == 5) {
                $('#advancedFiltersContainer').fadeIn();
                $('.new-button-container .collapse-link').html('<i class="fa fa-lg fa-chevron-up"></i>');
            }
        }
    }

    function loadDocumentsType(element)
    {
        var billerId = element.val();
        $('#document_type').select2('val', '');

        var options = '<option value=""><?= $this->lang->line('allsf') ?></option>';

        $.ajax({
            type: "post",
            url: "<?= admin_url("sales/getDocumentTypes") ?>",
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'billerId': element.val(),
                'asynchronous': 1,
                'electronicDocument': 0,
                'modules'       : [8],

            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(documentType => {
                        options += '<option value="'+documentType.id+'">'+documentType.sales_prefix+' - '+documentType.nombre+'</option>';
                    });
                }

                $('#document_type').html(options);
            }
        });
    }

    function showHideLocation(element)
    {
        var client = element.val()
        if (client) {
            $('#locationContainer').fadeOut();
        } else {
            $('#locationContainer').fadeIn();
        }
    }

    function getCsvOrders()
    {
        $.ajax({
            url: "<?= admin_url('sales/get_order_sales_ftp') ?>",
            dataType: "JSON"
        }).done(function(data) {

            $.each(data, function(index, response) {
                if (response.successfully == "true") {
                    Command: toastr.success(response.msg, '¡Correcto!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                }
                else {
                    Command: toastr.error(response.msg, '¡Error!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "8000",
                        "extendedTimeOut": "1000",
                    });
                }
            });


            setTimeout(function() {
                location.reload();
            }, 8000);

        });
    }

    function changeOrderStatus()
    {
        if ($('input[name="val[]"]:checked').length > 0) {
            $('#orderSaleStatus').modal('show');
        } else {
            command: toastr.error('Debe escoger al menos una orden de pedido a afectar.', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
        }
    }

    function completeOrderSales()
    {
        bootbox.confirm(lang.r_u_sure, function(result) {
            if (result) {

            }
        });
    }

    function showInfo()
    {
        $('.change_os_info').fadeIn();
    }

    function updateOrderStatus()
    {
        $('#new_order_sale_status').val($('#order_status').val());
        $('#form_action').val('change_order_status');
        $('#action-form-submit').trigger('click');
    }

    function oslduplicate(e)
    {
        if (localStorage.getItem('oslitems')) {
            e.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm("<?= lang('you_will_loss_sale_data') ?>", function(result) {
                if (result) {
                    window.location.href = href;
                }
            });
        }
    }

    function osledit(e)
    {
        if (localStorage.getItem('oslitems')) {
            e.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm("<?= lang('you_will_loss_sale_data') ?>", function(result) {
                if (result) {
                    window.location.href = href;
                }
            });
        }
    }

    function synchronizeFromEcommerce()
    {
        swal({
            title: 'Sincronizando Órdenes de Pedido...',
            text: 'Por favor, espere un momento.',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });

        sync();
    }

    function sync() {
        $.ajax({
            type: "post",
            url: site.base_url + 'sales/synchronizeFromEcommerce',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: "json",
            success: function (response) {
                if (response.length !== 0) {
                    $.each(response, function (type, message) {
                        if (type == 'warning') {
                            header_alert(type, message, null, function () {
                                window.location.reload();
                                swal.close();
                            });
                        }
                    });
                } else {
                    header_alert('success', '¡Sincronización completada!', null, function () {
                        window.location.reload();
                        swal.close();
                    });
                }
            }
        });
    }
</script>