$(document).ready(function(){
    $("#picking_form").validate({
        ignore: []
    });
    $(document).on('click', '#reset_picking', function(){
        clean_packing_localstorage();
        location.reload();
    });

	$(document).on('change', '#wmspacustomer', function(){
		localStorage.setItem('wmspacustomer', $(this).val());
        if ($('#wmspapicking_reference').val() == "") {
            $.ajax({
                url : site.base_url+"wms/customer_pickings",
                type : "GET",
                dataType: "JSON",
                data : {
                    'customer_id' :  $('#wmspacustomer').val(),
                    'biller_id' :  $('#wmspabiller').val(),
                }
            }).done(function(data){
                if (data.response !== undefined && data.response == 0) {
                    command: toastr.warning('El cliente seleccionado no tiene cartera pendiente', 'Sin resultados', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
                    localStorage.removeItem('wms_pa_pickings');
                    loadItems();
                } else {
                    localStorage.removeItem('wms_pa_pickings');
                    localStorage.setItem('wms_pa_pickings', JSON.stringify(data));
                    if (Object.keys(data).length == 1) {
                        $.each(data, function(os_id, os){
                            data[os_id].cancelled = false;
                            $.each(os.rows, function(os_item_id, os_item){
                                data[os_id].rows[os_item_id].cancelled = false;
                            });
                        });
                    }
                    loadItems();
                }
            }).fail(function(){
                loadItems();
            });
        }
	});

    $(document).on('change', '#wmspapicking_reference', function(){
        localStorage.setItem('wmspapicking_reference', $(this).val());
        $.ajax({
            url : site.base_url+"wms/picking_by_reference",
            type : "GET",
            dataType: "JSON",
            data : {
                'picking_reference' :  $('#wmspapicking_reference').val(),
            }
        }).done(function(data){
            if (data.response !== undefined && data.response == 0) {
                command: toastr.warning('El cliente seleccionado no tiene cartera pendiente', 'Sin resultados', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                localStorage.removeItem('wms_pa_pickings');
                loadItems();
            } else {
                localStorage.removeItem('wms_pa_pickings');
                if (Object.keys(data).length == 1) {
                    $.each(data, function(os_id, os){
                        data[os_id].cancelled = false;
                        $.each(os.rows, function(os_item_id, os_item){
                            data[os_id].rows[os_item_id].cancelled = false;
                        });
                    });
                }
                localStorage.setItem('wms_pa_pickings', JSON.stringify(data));
                loadItems();
            }
        }).fail(function(){
            loadItems();
        });
    });

    if (wmspabiller = localStorage.getItem('wmspabiller')) {
        $('#wmspabiller').select2('val', wmspabiller).trigger('change');
    }
    if (wmspapicking_reference = localStorage.getItem('wmspapicking_reference')) {
        $('#wmspapicking_reference').val(wmspapicking_reference).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"wms/getPicking/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "wms/picking_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        biller_id: $('#wmspabiller').val(),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        }).trigger('change');
    } else {
        nsOrderSale();
    }

    if (wmspacustomer = localStorage.getItem('wmspacustomer')) {
        nsCustomer(wmspacustomer);
    } else {
        nsCustomer();
    }

    $(document).on('click', '.add_new_box', function(){
        if (wms_packing_boxes = JSON.parse(localStorage.getItem('wms_packing_boxes'))) {
            packing_box = [];
            wms_packing_boxes.push(packing_box);
            localStorage.setItem('wms_packing_boxes', JSON.stringify(wms_packing_boxes));
        } else {
            packing_boxes = [];
            packing_box = [];
            packing_boxes.push(packing_box);
            localStorage.setItem('wms_packing_boxes', JSON.stringify(packing_boxes));
        }
        loadItems();
    });

    $(document).on('keyup', '.verify_product', function(){
        wms_pa_pickings = JSON.parse(localStorage.getItem('wms_pa_pickings'));
        $.each(wms_pa_pickings, function(pid, picking){
            $.each(picking.rows, function(pr_id, picking_item){

            });
        });
    });

});

function nsCustomer(customer = null) {
    if (customer == null) {
        $('#wmspacustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    } else {
        $('#wmspacustomer').val(customer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        }).trigger('change');
    }
}

function nsOrderSale(){
    $('#wmspapicking_reference').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "wms/picking_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function clean_packing_localstorage(){
    localStorage.removeItem('wmspacustomer');
    localStorage.removeItem('wmspabiller');
    localStorage.removeItem('wmspapicking_reference');
    localStorage.removeItem('wms_pa_pickings');
    localStorage.removeItem('wms_packing_boxes');
}
//aca load
function loadItems(){
    $(".div_packing_boxes").empty();
    if (wms_packing_boxes = JSON.parse(localStorage.getItem('wms_packing_boxes'))) {
        last_box = Object.keys(wms_packing_boxes).length;
        num_box = 0;
        picking_html = "";
        if (wms_pa_pickings = JSON.parse(localStorage.getItem('wms_pa_pickings'))) {
            console.log(wms_pa_pickings);
            $.each(wms_pa_pickings, function(picking_id, picking){
                picking_html += "<option value='"+picking_id+"'>"+picking.reference_no+"</option>";
            });
        }   
        $.each(wms_packing_boxes, function(pb_id, pb){
            num_box++;
            var newDiv = $("<div class='col-md-12 packing_box' "+pb_id+"></div>");
            html = "";
            html += "<div class='col-md-4'>"+
                        "<label>Selección (Picking)</label>"+
                        "<select class='form-control pickings_select' data-pboxnum='"+pb_id+"' multiple>"+picking_html+"</select></div>";
            html += "<div class='col-md-4'>"+
                        "<label>Añadir producto</label>"+
                        "<input type='text' class='form-control' data-pboxnum='"+pb_id+"' placeholder='Ingrese código de producto'>"+
                    "</div>";
            html += "<div class='col-md-4'>"+
                        "<label>N° caja</label>"+
                        "<input type='text' class='form-control' data-pboxnum='"+pb_id+"' value='"+num_box+"' readonly>"+
                    "</div>";
                        // 
            newDiv.html(html);
            newDiv.prependTo(".div_packing_boxes");
        });
    }
    set_packing_boxes();
    $('.pickings_select').select2();
}

function set_packing_boxes(){

    if (wms_packing_boxes = JSON.parse(localStorage.getItem('wms_packing_boxes'))) {
        last_box = Object.keys(wms_packing_boxes).length;
        html_opts = "";
        for (var i = 1; i <= last_box; i++) {
            html_opts += "<option value='"+i+"'>"+i+"</option>";
        }
        $('select.verify_product_box').html(html_opts);
        $('select.verify_product_box').select2();
    }
    
}