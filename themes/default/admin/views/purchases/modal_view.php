<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
@media print {
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 3px !important;
    }
}
</style>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <div class="col-xs-12">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-2x">&times;</i>
                </button>
                <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                    <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('purchases/adjustment_zebra_products_print/'.$inv->id) ?>" target="_blank">
                        <i class="fa fa-print"></i> <?= lang('print_zebra'); ?>
                    </a>
                <?php endif ?>
                    <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/print_barcodes/?purchase='.$inv->id) ?>" target="_blank">
                        <i class="fa fa-print"></i> <?= lang('print_barcodes'); ?>
                    </a>
                <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="imprimir_factura_mayorista();">
                    <i class="fa fa-print"></i> <?= lang('print'); ?>
                </button>
            </div>
            <div id="contenido_factura">
                <div class="row bold">
                    <div class="col-xs-6">
                        <?php if ($logo) { ?>
                            <div class="text-center" style="margin-bottom:20px;">
                                <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>" alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>" style="max-width: 300px;">
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-6">
                        <p class="bold">
                            <?= lang("date"); ?>: <?= $this->sma->hrsd($inv->date); ?><br>
                            <?php

                            $fecha_exp = strtotime("+".$inv->payment_term." day", strtotime($inv->date));
                            $fecha_exp = date('d/m/Y', $fecha_exp);

                             ?>
                            <?= lang("expiration_date"); ?>: <?= $fecha_exp; ?><br>
                            <?= lang("purchase_no_ref"); ?>: <?= $inv->reference_no; ?><br>
                            <?php if ($this->Settings->management_consecutive_suppliers == 1): ?>
                                <?= lang("consecutive_supplier"); ?>: <?= $inv->consecutive_supplier; ?><br>
                            <?php endif ?>
                            <?php if (!empty($inv->return_purchase_ref)) {
                                echo lang("return_ref").': '.$inv->return_purchase_ref;
                                if ($inv->return_id) {
                                    echo ' <a data-target="#myModal2" data-toggle="modal" href="'.admin_url('purchases/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                                } else {
                                    echo '<br>';
                                }
                            } ?>
                            <?= lang("status"); ?>: <?= lang($inv->status); ?><br>
                            <?= lang("payment_status"); ?>: <?= lang($inv->payment_status); ?><br>
                            <?= lang("paid_by"); ?>: <?= lang($purchase_main_payment_method); ?>
                            <?php if ($purchase_payments_method != ''): ?>
                                <br><?= lang('payment_methods') ?>: <?= $purchase_payments_method ?>
                            <?php endif ?>
                            <?php if ($inv->purchase_origin): ?>
                                <br><?= lang('origin') ?> : <?= lang($inv->purchase_origin) ?>
                                <br><?= lang('reference_origin') ?> : <?= $inv->purchase_origin_reference_no ?>
                            <?php endif ?>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="row" style="margin-bottom:15px;">

                    <?php if ($Settings->invoice_view == 1) { ?>
                        <div class="col-xs-12 text-center">
                            <h2><?= $document_type ? $document_type->nombre :  lang('purchase_invoice'); ?></h2>
                        </div>
                    <?php } ?>
                </div>
                <div class="row" style="margin-bottom:15px;">

                    <div class="col-xs-6">
                        <?php echo $this->lang->line("to"); ?>:
                        <h3 style="margin-top:10px;"><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h3>
                        <span>
                            <?= $biller->address ?><br>
                            <?= $biller->postal_code." - ".$biller->city.", ".$biller->state ?><br>
                            <?= $biller->country ?><br>
                            <?php
                            echo "NIT : ".$this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '');
                             ?><br>
                            <?= $biller->phone ?><br>
                            <?= $biller->email ?><br>
                        </span>

                        <?php

                        if ($biller->cf1 != "-" && $biller->cf1 != "") {
                            echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                        }
                        if ($biller->cf2 != "-" && $biller->cf2 != "") {
                            echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                        }
                        if ($biller->cf3 != "-" && $biller->cf3 != "") {
                            echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                        }
                        if ($biller->cf4 != "-" && $biller->cf4 != "") {
                            echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                        }
                        if ($biller->cf5 != "-" && $biller->cf5 != "") {
                            echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                        }
                        if ($biller->cf6 != "-" && $biller->cf6 != "") {
                            echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                        }
                        ?>
                    </div>




                    <div class="col-xs-6">
                        <?php echo $this->lang->line("from"); ?>:<br/>
                        <h3 style="margin-top:10px;"><?= $supplier->company ? $supplier->company : $supplier->name; ?></h3>
                        <?= $supplier->company ? "" : "Attn: " . $supplier->name ?>
                        <?php
                        if ($supplier->vat_no != "-" && $supplier->vat_no != "") {
                            if ($supplier->tipo_documento == 6) {
                                $vatNo =  lang("vat_no") . ": " . $supplier->vat_no."-".$supplier->digito_verificacion;
                            } else {
                                $vatNo =  lang("vat_no") . ": " . $supplier->vat_no;
                            }
                        }
                         ?>
                        <?= $vatNo ?><br>
                        <span><?= $supplier->address ?><br>
                        <?= $supplier->postal_code." - ".$supplier->city.", ".$supplier->state ?><br>
                        <?= $supplier->country ?><br>
                        <?= $supplier->phone ?><br>
                        <?= $supplier->email ?><br>
                        <?php

                        if ($supplier->cf1 != "-" && $supplier->cf1 != "") {
                            echo "<br>" . lang("ccf1") . ": " . $supplier->cf1;
                        }
                        if ($supplier->cf2 != "-" && $supplier->cf2 != "") {
                            echo "<br>" . lang("ccf2") . ": " . $supplier->cf2;
                        }
                        if ($supplier->cf3 != "-" && $supplier->cf3 != "") {
                            echo "<br>" . lang("ccf3") . ": " . $supplier->cf3;
                        }
                        if ($supplier->cf4 != "-" && $supplier->cf4 != "") {
                            echo "<br>" . lang("ccf4") . ": " . $supplier->cf4;
                        }
                        if ($supplier->cf5 != "-" && $supplier->cf5 != "") {
                            echo "<br>" . lang("ccf5") . ": " . $supplier->cf5;
                        }
                        if ($supplier->cf6 != "-" && $supplier->cf6 != "") {
                            echo "<br>" . lang("ccf6") . ": " . $supplier->cf6;
                        }

                        ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom:15px;">

                    <div class="clearfix"></div>

                    <div class="clearfix"></div>

                    <?php if ($inv->purchase_currency != $this->Settings->default_currency): ?>
                        <div class="col-xs-6 no-print">
                            <label><?= lang('choose_purchase_currency_view') ?></label>
                            <div class="row">
                                <div class="col-xs-6">
                                    <select name="view_currency" id="view_currency" class="form-control">
                                        <option value="<?= $inv->purchase_currency ?>"><?= $inv->purchase_currency ?></option>
                                        <option value="<?= $this->Settings->default_currency ?>"><?= $this->Settings->default_currency ?></option>
                                    </select>
                                </div>
                                <div class="col-xs-6 inv_currency_trm">
                                    <input type="text" name="inv_currency_trm" class="form-control text-right" value="TRM : <?= $inv->purchase_currency_trm ?>" readonly>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>

                    <?php if (isset($cost_center) && $cost_center): ?>
                        <hr class="col-xs-11">
                        <div class="col-xs-12">
                            <label><?= lang('cost_center') ?></label>
                            <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                        </div>
                    <?php endif ?>
                </div>
                <div class="inv_currency">
                    <div class="table-responsive" style="min-height: 0px !important;">
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                                <tr>
                                    <th><?= lang("no"); ?></th>
                                    <th><?= lang("description"); ?></th>
                                    <?php if ($Settings->indian_gst) { ?>
                                        <th><?= lang("hsn_code"); ?></th>
                                    <?php } ?>
                                    <th><?= lang('gross_net_unit_cost') ?></th>
                                    <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                        <th><?= lang("discount") ?></th>
                                    <?php endif ?>
                                    <th><?= lang("cost_x_discount"); ?></th>
                                    <?php if ($Settings->tax1): ?>
                                        <th><?= lang("tax") ?></th>
                                    <?php endif ?>
                                    <?php if ($Settings->ipoconsumo && $inv->purchase_type == 1): ?>
                                        <th><?= lang("second_product_tax") ?></th>
                                    <?php elseif ($Settings->ipoconsumo && $inv->purchase_type == 2): ?>
                                        <th><?= lang("tax_2") ?></th>
                                    <?php endif ?>
                                    <th><?= lang("cost_x_tax"); ?></th>
                                    <th><?= lang("quantity"); ?></th>
                                    <th><?= lang("subtotal"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $r = 1;
                                $tax_summary = array();
                                $tax_1 = 0;
                                foreach ($rows as $rindex => $row):
                                    $rows[$rindex]->purchase_type = $inv->purchase_type;
                                    $netunitcosttrm = $this->sma->formatDecimalNoRound($trmrate * $row->net_unit_cost, 6);
                                    $tax_1 += $row->item_tax;
                                ?>
                                <tr>
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                        <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                        <?= $row->details ? '<br>' . $row->details : ''; ?>
                                        <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                        <?= $row->supplier_part_no ? '<br>' . $row->supplier_part_no : ''; ?>
                                    </td>
                                    <?php if ($Settings->indian_gst) { ?>
                                        <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                    <?php } ?>
                                    <td style="text-align:right; width:120px;">
                                        <?= $this->sma->formatMoney($trmrate * ($row->net_unit_cost + ($row->item_discount / $row->quantity))); ?>
                                    </td>
                                    <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                        <td style="text-align:right; width:120px;">
                                            <?= $this->sma->get_percentage_from_amount($row->discount, ($row->net_unit_cost + ($row->item_discount / $row->quantity))); ?>
                                        </td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:100px;">
                                        <?= $trmrate != 1 ? $this->sma->formatMoney($netunitcosttrm) :  $this->sma->formatMoney($row->net_unit_cost); ?>
                                    </td>
                                    <?php if ($Settings->tax1): ?>
                                        <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                    <?php endif ?>
                                    <?php if ($Settings->ipoconsumo): ?>
                                        <td style="text-align:right; width:120px;"><?= $row->tax_2; ?></td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->unit_cost); ?></td>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                                </tr>
                                <?php
                                $r++;
                                endforeach;
                                if ($return_rows) {
                                    echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                                    foreach ($return_rows as $row):

                                    $netunitcosttrm = $this->sma->formatDecimalNoRound($trmrate * $row->net_unit_cost, 6);
                                    ?>
                                    <tr class="warning">
                                        <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                        <td style="vertical-align:middle;">
                                            <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                            <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                            <?= $row->details ? '<br>' . $row->details : ''; ?>
                                            <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                        </td>
                                        <?php if ($Settings->indian_gst) { ?>
                                        <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                        <?php } ?>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * ($row->net_unit_cost + ($row->item_discount / $row->quantity))); ?></td>
                                        <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                            <td style="text-align:right; width:120px;"><?= $this->sma->get_percentage_from_amount($row->discount, ($row->net_unit_cost + ($row->item_discount / $row->quantity))); ?></td>
                                        <?php endif ?>
                                        <td style="text-align:right; width:100px;"><?= $trmrate != 1 ? $this->sma->formatMoney($netunitcosttrm) :  $this->sma->formatMoney($row->net_unit_cost); ?></td>
                                        <?php if ($Settings->tax1): ?>
                                            <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                        <?php endif ?>
                                        <?php if ($Settings->ipoconsumo): ?>
                                            <td style="text-align:right; width:120px;"><?= $row->tax_2; ?></td>
                                        <?php endif ?>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->unit_cost); ?></td>
                                        <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php
                            $col = $Settings->indian_gst ? 7 : 6;
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                $col++;
                            }
                            if ($Settings->tax1) {
                                $col++;
                            }
                            if ($Settings->ipoconsumo) {
                                $col++;
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                                $tcol = $col - 2;
                            } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                                $tcol = $col - 1;
                            } elseif ($Settings->tax1) {
                                $tcol = $col - 1;
                            } else {
                                $tcol = $col;
                            }
                            ?>
                            <?php 
                            $total_retencion = 
                                            ($inv->rete_fuente_assumed == 0 ? $inv->rete_fuente_total : 0) + 
                                            ($inv->rete_iva_assumed == 0 ? $inv->rete_iva_total : 0) + 
                                            ($inv->rete_ica_assumed == 0 ? $inv->rete_ica_total : 0) +
                                            ($inv->rete_ica_assumed == 0 ? $inv->rete_bomberil_total : 0) +
                                            ($inv->rete_ica_assumed == 0 ? $inv->rete_autoaviso_total : 0) + 
                                            ($inv->rete_other_assumed == 0 ? $inv->rete_other_total : 0);
                            $total_returned_retencion = 
                                            ($return_purchase->rete_fuente_total) + 
                                            ($return_purchase->rete_iva_total) + 
                                            ($return_purchase->rete_ica_total) +
                                            ($return_purchase->rete_bomberil_total) +
                                            ($return_purchase->rete_autoaviso_total) +
                                            ($return_purchase->rete_other_total);
                            ?>
                            <?php if ($inv->grand_total > 0): ?>
                                <?php if ($total_retencion > 0): ?>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                            <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * $inv->grand_total); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right;"><?= lang("retention"); ?>
                                            <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right;"> - <?= $this->sma->formatMoney($trmrate * $total_retencion); ?></td>
                                    </tr>
                                    <?php 
                                        if ($return_purchase) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * ($return_purchase->grand_total + $total_returned_retencion)) . '</td></tr>';
                                        } ?>

                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("paid"); ?>
                                            <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>

                                        <?php if ($inv->grand_total > 0): ?>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * ($inv->paid)); ?></td>
                                        <?php else: ?>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($trmrate * (($return_purchase ? ($inv->paid+$return_purchase->paid) : $inv->paid) + $total_retencion)*-1)); ?></td>
                                        <?php endif ?>
                                    </tr>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                            <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * (($return_purchase ? ($inv->grand_total+($return_purchase->grand_total+$total_returned_retencion)) : $inv->grand_total) - $inv->paid)); ?></td>
                                    </tr>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("retention"); ?>
                                            <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"> <?= $this->sma->formatMoney($total_retencion); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                            <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * ($inv->grand_total + $total_retencion)); ?></td>
                                    </tr>
                                <?php endif ?>
                            <?php endif ?>
                            <?php
                            if ($inv->surcharge != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $inv->surcharge) . '</td></tr>';
                            }
                            ?>

                            <?php if ($Settings->indian_gst) {
                                if ($inv->cgst > 0) {
                                    $cgst = $return_purchase ? $inv->cgst + $return_purchase->cgst : $inv->cgst;
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('cgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                                }
                                if ($inv->sgst > 0) {
                                    $sgst = $return_purchase ? $inv->sgst + $return_purchase->sgst : $inv->sgst;
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('sgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                                }
                                if ($inv->igst > 0) {
                                    $igst = $return_purchase ? $inv->igst + $return_purchase->igst : $inv->igst;
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('igst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                                }
                            } ?>

                            <?php if ($inv->order_discount != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($trmrate * ($return_purchase ? ($inv->order_discount+$return_purchase->order_discount) : $inv->order_discount)) . '</td></tr>';
                            }
                            ?>
                            <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * ($return_purchase ? ($inv->order_tax+$return_purchase->order_tax) : $inv->order_tax)) . '</td></tr>';
                            }
                            ?>


                            <?php if ($inv->shipping != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $inv->shipping) . '</td></tr>';
                            }
                            ?>

                            </tfoot>
                        </table>
                    </div>
                    <?php
                    $inv_currency = new stdClass();
                    $inv_currency->currency = $inv->purchase_currency;
                    $inv_currency->rate = 1 / ($inv->purchase_currency_trm != 0 ? $inv->purchase_currency_trm : 1);
                     ?>
                    <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, $return_rows, ($return_purchase ? $inv->product_tax+$return_purchase->product_tax : $inv->product_tax), true, $inv_currency) : ''; ?>
                </div>
                <div class="default_currency" style="display: none;">
                    <div class="table-responsive" style="min-height: 0px !important;">
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                             <tr>
                                <th><?= lang("no"); ?></th>
                                <th><?= lang("description"); ?></th>
                                <?php if ($Settings->indian_gst) { ?>
                                    <th><?= lang("hsn_code"); ?></th>
                                <?php } ?>
                                <th><?= lang('gross_net_unit_cost') ?></th>
                                <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                    <th><?= lang("discount") ?></th>
                                <?php endif ?>
                                <th><?= lang("net_unit_cost"); ?></th>
                                <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                    <th><?= lang("tax") ?></th>
                                <?php endif ?>
                                <th><?= lang("unit_cost"); ?></th>
                                <th><?= lang("quantity"); ?></th>
                                <th><?= lang("subtotal"); ?></th>
                            </tr>

                            </thead>

                            <tbody>

                            <?php $r = 1;

                            // var_dump($rows);

                            foreach ($rows as $row):
                                $trmrate = 1;
                            ?>
                                <tr>
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                        <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                        <?= $row->details ? '<br>' . $row->details : ''; ?>
                                        <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                    </td>
                                    <?php if ($Settings->indian_gst) { ?>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                    <?php } ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * ($row->net_unit_cost + ($row->item_discount / $row->quantity))); ?></td>
                                    <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->get_percentage_from_amount($row->discount, ($row->net_unit_cost + ($row->item_discount / $row->quantity))); ?></td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:100px;"><?= $trmrate != 1 ? $netunitcosttrm :  $this->sma->formatMoney($row->net_unit_cost); ?></td>
                                    <?php
                                    // if ($Settings->tax1 && $inv->product_tax > 0) {
                                    //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($trmrate * $row->item_tax) . '</td>';
                                    // }
                                    // if ($Settings->product_discount && $inv->product_discount != 0) {
                                    //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($trmrate * $row->item_discount) . '</td>';
                                    // }
                                    ?>
                                    <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                        <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->unit_cost); ?></td>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                            if ($return_rows) {
                                echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                                foreach ($return_rows as $row):
                                ?>
                                    <tr class="warning">
                                        <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                        <td style="vertical-align:middle;">
                                            <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                            <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                            <?= $row->details ? '<br>' . $row->details : ''; ?>
                                            <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                        </td>
                                        <?php if ($Settings->indian_gst) { ?>
                                        <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                        <?php } ?>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * ($row->net_unit_cost + ($row->item_discount / $row->quantity))); ?></td>
                                        <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                            <td style="text-align:right; width:120px;"><?= $this->sma->get_percentage_from_amount($row->discount, ($row->net_unit_cost + ($row->item_discount / $row->quantity))); ?></td>
                                        <?php endif ?>
                                        <td style="text-align:right; width:100px;"><?= $trmrate != 1 ? $netunitcosttrm :  $this->sma->formatMoney($row->net_unit_cost); ?></td>
                                        <?php
                                        // if ($Settings->tax1 && $inv->product_tax > 0) {
                                        //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($trmrate * $row->item_tax) . '</td>';
                                        // }
                                        // if ($Settings->product_discount && $inv->product_discount != 0) {
                                        //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($trmrate * $row->item_discount) . '</td>';
                                        // }
                                        ?>
                                        <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                            <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                        <?php endif ?>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->unit_cost); ?></td>
                                        <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php
                            $col = $Settings->indian_gst ? 7 : 6;
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                $col++;
                            }
                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                $col++;
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                                $tcol = $col - 2;
                            } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                                $tcol = $col - 1;
                            } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                                $tcol = $col - 1;
                            } else {
                                $tcol = $col;
                            }
                            ?>
                            <?php
                            if ($return_purchase) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_purchase->grand_total) . '</td></tr>';
                            }
                            if ($inv->surcharge != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->surcharge) . '</td></tr>';
                            }
                            ?>

                            <?php if ($Settings->indian_gst) {
                                if ($inv->cgst > 0) {
                                    $cgst = $return_purchase ? $inv->cgst + $return_purchase->cgst : $inv->cgst;
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('cgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                                }
                                if ($inv->sgst > 0) {
                                    $sgst = $return_purchase ? $inv->sgst + $return_purchase->sgst : $inv->sgst;
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('sgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                                }
                                if ($inv->igst > 0) {
                                    $igst = $return_purchase ? $inv->igst + $return_purchase->igst : $inv->igst;
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('igst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                                }
                            } ?>

                            <?php if ($inv->order_discount != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($return_purchase ? ($inv->order_discount+$return_purchase->order_discount) : $inv->order_discount) . '</td></tr>';
                            }
                            ?>
                            <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_purchase ? ($inv->order_tax+$return_purchase->order_tax) : $inv->order_tax) . '</td></tr>';
                            }
                            ?>
                            <?php if ($inv->shipping != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                            }

                            $total_retencion = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total;
                            ?>
                            <tr>
                                <td colspan="<?= $col; ?>"
                                    style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                    <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                </td>
                                <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_purchase ? ($inv->grand_total+$return_purchase->grand_total) : $inv->grand_total); ?></td>
                            </tr>
                                <?php if ($inv->grand_total > 0): ?>
                                    <?php if ($total_retencion > 0): ?>
                                        <tr>
                                            <td colspan="<?= $col; ?>"
                                                style="text-align:right; font-weight:bold;"><?= lang("retention"); ?>
                                                <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"> - <?= $this->sma->formatMoney($total_retencion); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="<?= $col; ?>"
                                                style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                                <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_purchase ? ($inv->grand_total+$return_purchase->grand_total) : $inv->grand_total) - $total_retencion); ?></td>
                                        </tr>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="<?= $col; ?>"
                                                style="text-align:right; font-weight:bold;"><?= lang("retention"); ?>
                                                <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"> <?= $this->sma->formatMoney($total_retencion); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="<?= $col; ?>"
                                                style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                                <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_purchase ? ($inv->grand_total+$return_purchase->grand_total) : $inv->grand_total) + $total_retencion); ?></td>
                                        </tr>
                                    <?php endif ?>
                                <?php endif ?>
                            <tr>
                                <td colspan="<?= $col; ?>"
                                    style="text-align:right; font-weight:bold;"><?= lang("paid"); ?>
                                    <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                </td>
                                <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_purchase ? ($inv->paid+$return_purchase->paid) : $inv->paid) - $total_retencion); ?></td>
                            </tr>
                            <tr>
                                <td colspan="<?= $col; ?>"
                                    style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                    <span class="currency_text">(<?= $inv->purchase_currency ? $inv->purchase_currency : $this->Settings->default_currency; ?>)</span>
                                </td>
                                <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_purchase ? ($inv->grand_total+$return_purchase->grand_total) : $inv->grand_total) - ($return_purchase ? ($inv->paid+$return_purchase->paid) : $inv->paid)); ?></td>
                            </tr>

                            </tfoot>
                        </table>
                    </div>
                    <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, $return_rows, ($return_purchase ? $inv->product_tax+$return_purchase->product_tax : $inv->product_tax), true, $inv_currency) : ''; ?>
                </div>
                <?php if ($invoice_footer): ?>
                    <div class="well well-sm">
                        <div><?= $invoice_footer ?></div>
                    </div>
                <?php endif ?>
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                            if ($inv->note || $inv->note != "") { ?>
                                <div class="well well-sm">
                                    <p class="bold"><?= lang("note"); ?>:</p>
                                    <div><?= $this->sma->decode_html($inv->note); ?></div>
                                </div>
                            <?php
                            }
                        ?>

                    </div>

                    <!-- <?php if ($supplier->award_points != 0 && $Settings->each_spent > 0) { ?>
                    <div class="col-xs-5 pull-left">
                        <div class="well well-sm">
                            <?=
                            '<p>'.lang('this_purchase').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                            .'<br>'.
                            lang('total').' '.lang('award_points').': '. $supplier->award_points . '</p>';?>
                        </div>
                    </div>
                    <?php } ?> -->
                </div>
                <div class="row">
                    <div class="col-xs-4 pull-left">
                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p style="border-bottom: 1px solid #666;">&nbsp;</p>

                        <p><?= lang("stamp_sign"); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="col-xs-12 text-center">
                <p>
                    <?= lang("created_by"); ?>: <?= $inv->created_by ? $created_by->first_name . ' ' . $created_by->last_name : $supplier->name; ?> <br>
                    <?= lang("date"); ?>: <?= $this->sma->hrld($inv->registration_date); ?>
                </p>
                <?php if ($inv->updated_by) { ?>
                <p>
                    <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                    <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                </p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php if (!$Supplier || !$supplier) { ?>
    <div class="buttons row div_buttons_modal div_buttons_modal_lg">
        <div class="col-sm-3 col-xs-6">
            <a href='#' class='tip po btn btn-outline btn-primary' title='' data-placement="top" data-content="
                        <p> <?= lang('where_to_duplicate') ?> </p>
                        <a class='btn btn-primary' href='<?= admin_url('purchases/add/?purchase_id=').$inv->id ?>'> <?= lang('detal_purchase') ?> </a>
                        <a class='btn btn-primary' href='<?= admin_url('pos/index/?duplicate=').$inv->id ?>'> <?= lang('pos_purchase') ?> </a>
                        <button class='btn po-close'> <?= lang('cancel') ?> </button>" rel='popover' style="width: 100%;">
                        <i class='fa fa-download'></i>
                <span class="hidden-sm hidden-xs"><?= lang('duplicate') ?></span>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6">
            <a href="<?= admin_url('purchases/add_delivery/' . $inv->id) ?>" class="tip btn btn-outline btn-primary" title="<?= lang('add_delivery') ?>" data-toggle="modal" data-target="#myModal2" style="width: 100%;">
                <i class="fa fa-truck"></i>
                <span class="hidden-sm hidden-xs"><?= lang('delivery') ?></span>
            </a>
        </div>
        <?php if ($inv->attachment) { ?>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= admin_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-outline btn-primary" title="<?= lang('attachment') ?>">
                    <i class="fa fa-chain"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                </a>
            </div>
        <?php } ?>
        <div class="col-sm-3 col-xs-6">
            <a href="<?= admin_url('purchases/email/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal2" class="tip btn btn-outline btn-primary" title="<?= lang('email') ?>" style="width: 100%;">
                <i class="fa fa-envelope-o"></i>
                <span class="hidden-sm hidden-xs"><?= lang('email') ?></span>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6">
            <a href="<?= admin_url("purchases/purchaseView/").$inv->id."/FALSE/TRUE" ?>" class="tip btn btn-outline btn-primary" title="<?= lang('download_pdf') ?>" style="width: 100%;">
                <i class="fa fa-download"></i>
                <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
            </a>
        </div>
        <?php if ( ! $inv->purchase_id) { ?>
        <!-- <div class="">
            <a href="<?= admin_url('purchases/edit/' . $inv->id) ?>" class="tip btn btn-warning sledit" title="<?= lang('edit') ?>">
                <i class="fa fa-edit"></i>
                <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
            </a>
        </div> -->
        <!-- <div class="">
            <a href="#" class="tip btn btn-danger bpo" title="<b><?= $this->lang->line("delete_purchase") ?></b>"
                data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('purchases/delete/' . $inv->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                data-html="true" data-placement="top">
                <i class="fa fa-trash-o"></i>
                <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
            </a>
        </div> -->
        <?php } ?>
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });

    $('#view_currency').on('change', function(){
        actual_currency = $(this).val();
        default_currency = site.settings.default_currency;

        $('.currency_text').text("("+actual_currency+")");

        if (actual_currency != default_currency) {
            $('.inv_currency').css('display', '');
            $('.inv_currency_trm').css('display', '');
            $('.default_currency').css('display', 'none');
        } else {
            $('.inv_currency').css('display', 'none');
            $('.inv_currency_trm').css('display', 'none');
            $('.default_currency').css('display', '');
        }

    });
</script>

<script>
    $(document).ready(function(){
       $(document).on('click', '#boton_imprimir_anticipo', function() { imprimir_factura_mayorista(); });
    });

    function imprimir_factura_mayorista()
    {
        window.open('<?= admin_url("purchases/purchaseView/").$inv->id ?>');
    }
</script>
