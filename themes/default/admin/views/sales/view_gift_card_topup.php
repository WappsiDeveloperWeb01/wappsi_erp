<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if ($modal) { ?>
<style type="text/css">
    @media print {
      #wrapper {
        padding: 0px !important;
        margin: 0px !important;
      }
      #receiptData {
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-body{
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-content{
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-dialog{
        padding: 0px !important;
        margin: 0px !important;
      }
    }
</style>

<div class="modal-dialog no-modal-header" role="document"><div class="modal-content"><div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
    <?php
} else {
    ?><!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=lang("no") . " " . $gc->card_no;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            .text-center { text-align: center ; }
            table { width: 100% ; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
            <?php if ($product_detail_font_size != 0): ?>
                table > tbody > tr > td {
                    font-size: <?= 100 + ($product_detail_font_size * 10) ?>%;
                }
            <?php endif ?>
        </style>
    </head>

    <body>
        <?php
    } ?>
    <div id="wrapper">
        <div id="receiptData">
            <div>
                <div style="text-align:center;">
                    <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                    <h4 style="text-transform:uppercase;"><?=$biller->name;?></h4>
                    <?php
                    echo "NIT : ".$biller->vat_no.($biller->digito_verificacion != "" ? '-'.$biller->digito_verificacion : '');
                    if ($ciiu_code) {
                        echo "</br> Códigos CIIU : ".$ciiu_code."</br>";
                    }
                    echo "<p>" . $biller->address .
                    "<br>" . lang("tel") . ": " . $biller->phone .
                    "<br>" . $biller->city . " " . $biller->postal_code . " " . $biller->state . " " . $biller->country;
                    ?>
                </div>
                <h4 style="font-weight:bold; text-align: center;"><?=  $document_type ? $document_type->nombre : sprintf(lang('gift_card_topup'), lang('gift_card'));?></h4>
                <?php
                echo "<p>" .lang("date") . ": " . $this->sma->hrld($inv->date) . "<br>";
                echo lang("sale_no_ref") . ": " . $inv->reference_no . "<br>";
                echo lang("created_by") . ": " . $created_by->first_name." ".$created_by->last_name. "<br>";
                echo lang("customer") . ": " . ($customer->company && $customer->company != '-' ? $customer->company : $customer->name) . "<br>";
                echo ((isset($cost_center) && $cost_center) ? lang("cost_center").": ".$cost_center->name." (".$cost_center->code.")" : "");
                echo "<h4  style='font-weight:bold;'>".lang("card_no") . ": " . $gc->card_no . "</h4>";
                echo lang("paying_by") . ": " . lang($inv->paid_by) . "<br>";
                echo $inv->note ? (lang("note") . ": " . $inv->note . "<br>") : "";
                ?>
                <h2 style="font-weight:bold; text-align: center;"><?= lang('total') ?>: <?= $this->sma->formatMoney($inv->amount) ?></h2>
                <?= "</p>"; ?>
                <p class="text-center">Software Wappsi POS desarrollado por Web Apps Innovation SAS  www.wappsi.com</p>
            </div>

        </div>
                <span class="col-xs-12 no-print">
                    <a class="btn btn-block btn-warning" href="<?= admin_url("sales/gift_cards") ?>"><?= sprintf(lang("back_to_giftcards"), lang('gift_cards')); ?></a>
                </span>
                <span class="col-xs-12 no-print">
                    <a class="btn btn-block btn-primary" onclick="window.print();"><?= lang("print"); ?></a>
                </span>
    </div>
    <?php
    if( ! $modal) {
        ?>
        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
        <?php
    }
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            imprimir_factura_pos();
        });

        function imprimir_factura_pos()
        {
            if ("<?= $this->pos_settings->mobile_print_automatically_invoice ?>" == 0) {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    $('.btn_print_html').fadeOut();
                    return false;
                }
            }
            setTimeout(function() {
                window.print();
            }, 350);
        }
    </script>
    <?php if($modal) { ?>
    </div>
</div>
</div>
<?php } else { ?>
</body>
</html>
<?php } ?>
