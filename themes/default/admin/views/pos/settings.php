<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-12">
                    <?= admin_form_open_multipart("pos/settings",  array('id' => 'pos_setting')); ?>
                    <input type="password" name="papap" style="width: 0px; visibility: hidden;">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('pos_config') ?></legend>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('pro_limit', 'limit'); ?>
                                    <?= form_input('pro_limit', $pos->pro_limit, 'class="form-control" id="limit" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('allow_print_command', 'allow_print_command'); ?>
                                    <?php $col = array('0' => lang('no'), '1' => lang('yes'));
                                    echo form_dropdown('allow_print_command', $col, $pos->allow_print_command, 'class="form-control" id="allow_print_command" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('command_only_for_suspended', 'command_only_for_suspended'); ?>
                                    <?php $col = array('0' => lang('no'), '1' => lang('yes'));
                                    echo form_dropdown('command_only_for_suspended', $col, $pos->command_only_for_suspended, 'class="form-control" id="command_only_for_suspended" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('default_section', 'default_section'); ?>
                                    <?php
                                    $ct[''] = lang('select').' '.lang('default_section');
                                    $ct[997] = lang('featured');
                                    $ct[998] = lang('promotions');
                                    $ct[999] = lang('most_selled');
                                    $ct[1001] = lang('pos_navbar_categories');
                                    $ct[1002] = lang('pos_navbar_subcategories');
                                    foreach ($categories as $catrgory) {
                                        $ct[$catrgory->id] = "(".lang('category').") ".$catrgory->name;
                                    }
                                    echo form_dropdown('default_section', $ct, $pos->default_section, 'class="form-control" id="default_section" required="required" style="width:100%;" autocomplete="off"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 div_pos_navbar_default_category" style="display: none;">
                                <div class="form-group">
                                    <?= lang('pos_navbar_default_category', 'pos_navbar_default_category'); ?>
                                    <?php
                                    $ct = [];
                                    $ct[''] = lang('select');
                                    foreach ($categories as $catrgory) {
                                        $ct[$catrgory->id] = $catrgory->name;
                                    }
                                    echo form_dropdown('pos_navbar_default_category', $ct, $pos->pos_navbar_default_category, 'class="form-control" id="pos_navbar_default_category" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 div_pos_navbar_default_subcategory" style="display: none;">
                                <div class="form-group">
                                    <?= lang('pos_navbar_default_category', 'pos_navbar_default_category'); ?>
                                    <?php
                                    $ct = [];
                                    $ct[''] = lang('select');
                                    foreach ($subcategories as $catrgory) {
                                        if (!$catrgory->parent_id) {
                                            continue;
                                        }
                                        $ct[$catrgory->id] = $catrgory->name;
                                    }
                                    echo form_dropdown('pos_navbar_default_subcategory', $ct, $pos->pos_navbar_default_category, 'class="form-control" id="pos_navbar_default_subcategory" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('default_biller', 'default_biller'); ?>
                                    <?php
                                    $bl[0] = "";
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    if (isset($_POST['biller'])) {
                                        $biller = $_POST['biller'];
                                    } else {
                                        $biller = "";
                                    }
                                    echo form_dropdown('biller', $bl, $pos->default_biller, 'class="form-control" id="default_biller" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('default_customer', 'customer1'); ?>
                                    <?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : $pos->default_customer), 'id="customer1" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control" style="width:100%;"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('display_time', 'display_time'); ?>
                                    <?php
                                    $yn = array('1' => lang('yes'), '0' => lang('no'));
                                    echo form_dropdown('display_time', $yn, $pos->display_time, 'class="form-control" id="display_time" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('onscreen_keyboard', 'keyboard'); ?>
                                    <?php
                                    echo form_dropdown('keyboard', $yn, $pos->keyboard, 'class="form-control" id="keyboard" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('product_button_color', 'product_button_color'); ?>
                                    <?php $col = array('default' => lang('default'), 'primary' => lang('primary'), 'info' => lang('info'), 'warning' => lang('warning'), 'danger' => lang('danger'));
                                    echo form_dropdown('product_button_color', $col, $pos->product_button_color, 'class="form-control" id="product_button_color" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('tooltips', 'tooltips'); ?>
                                    <?php
                                    echo form_dropdown('tooltips', $yn, $pos->tooltips, 'class="form-control" id="tooltips" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('rounding', 'rounding'); ?>
                                    <?php
                                    $rnd = array('0' => lang('disable'), '1' => lang('to_nearest_005'), '2' => lang('to_nearest_050'), '3' => lang('to_nearest_number'), '4' => lang('to_next_number'));
                                    echo form_dropdown('rounding', $rnd, $pos->rounding, 'class="form-control" id="rounding" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('item_order', 'item_order'); ?>
                                    <?php $oopts = array(0 => lang('default'), 1 => lang('category'), 2 => lang("brands")); ?>
                                    <?= form_dropdown('item_order', $oopts, $pos->item_order, 'class="form-control" id="item_order" required="required"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('after_sale_page', 'after_sale_page'); ?>
                                    <?php $popts = array(0 => lang('receipt'), 1 => lang('pos')); ?>
                                    <?= form_dropdown('after_sale_page', $popts, $pos->after_sale_page, 'class="form-control" id="after_sale_page" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('display_customer_details', 'customer_details'); ?>
                                    <?php $popts = array(0 => lang('no'), 1 => lang('yes')); ?>
                                    <?= form_dropdown('customer_details', $popts, $pos->customer_details, 'class="form-control" id="customer_details" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('balance_settings', 'balance_settings'); ?>
                                    <?php $popts = array(0 => lang('no_balance'), 1 => lang('balance_no_label'), 2 => lang('balance_label')); ?>
                                    <!-- s -->
                                    <?= form_dropdown('balance_settings', $popts, $pos->balance_settings, 'class="form-control" id="balance_settings" required="required"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 bl_prefix" style="<?= $pos->balance_settings == 2 ? '' : 'display: none;' ; ?>">
                                <div class="form-group">
                                    <?= lang('balance_label_prefix', 'balance_label_prefix'); ?>
                                    <!--  -->
                                    <?= form_input('balance_label_prefix', $pos->balance_label_prefix, 'class="form-control tip" id="balance_label_prefix"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Tiempo de cambio de foco en cantidad (Segundos)</label>
                                <input type="number" name="time_focus_quantity" class="form-control" value="<?= $pos->time_focus_quantity; ?>" step="0.0001">
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('detail_sale_by_category', 'detail_sale_by_category'); ?>
                                    <?php $popts = array(
                                            0 => lang('no_detail'), 
                                            1 => lang('detail_by_category'),
                                            2 => lang('detail_by_product'),
                                            3 => lang('detail_by_category_and_expense_category'),
                                            4 => lang('detail_by_product_and_expense_category'),
                                            5 => lang('detail_by_expense_category'),
                                        ); ?>
                                    <?= form_dropdown('detail_sale_by_category', $popts, $pos->detail_sale_by_category, 'class="form-control" id="detail_sale_by_category" required="required"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <?= lang('show_suspended_bills_automatically', 'show_suspended_bills_automatically'); ?>
                                    <?php $popts = array(0 => lang('no'), 1 => lang('yes')); ?>
                                    <?= form_dropdown('show_suspended_bills_automatically', $popts, $pos->show_suspended_bills_automatically, 'class="form-control" id="show_suspended_bills_automatically" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label id="apply_suggested_tip"> <?= sprintf(lang('apply_suggested_tip'), lang('tip')) ?>  (%)</label>
                                <input type="number" name="apply_suggested_tip" id="apply_suggested_tip" value="<?= $pos->apply_suggested_tip ?>" class="form-control" step="0.0001" min="0" max="100">
                                <em class=""><?= lang('let_zero_if_doesnt_apply') ?></em>

                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang('apply_suggested_home_delivery_amount', 'apply_suggested_home_delivery_amount') ?>
                                <input type="number" name="apply_suggested_home_delivery_amount" id="apply_suggested_home_delivery_amount" value="<?= $pos->apply_suggested_home_delivery_amount ?>" class="form-control" min="0" step="0.0001">
                                <em class=""><?= lang('let_zero_if_doesnt_apply') ?></em>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?= lang('home_delivery_in_invoice', 'home_delivery_in_invoice') ?>
                                <?php
                                    $home_delivery_in_invoice = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                 ?>
                                <?= form_dropdown('home_delivery_in_invoice', $home_delivery_in_invoice, $pos->home_delivery_in_invoice, 'required="required" id="home_delivery_in_invoice" class="form-control"') ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang('print_voucher_delivery', 'print_voucher_delivery') ?>
                                <?php
                                    $print_voucher_delivery = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                 ?>
                                <?= form_dropdown('print_voucher_delivery', $print_voucher_delivery, $pos->print_voucher_delivery, 'required="required" id="print_voucher_delivery" class="form-control"') ?>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <?= lang('express_payment', 'express_payment') ?>
                                <?php
                                    $express_payment = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                 ?>
                                <?= form_dropdown('express_payment', $express_payment, $pos->express_payment, 'required="required" id="express_payment" class="form-control"') ?>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?= lang('mobile_print_automatically_invoice', 'mobile_print_automatically_invoice') ?>
                                <?php
                                    $mobile_print_automatically_invoice = array(
                                                                    '0' => lang('mobile_print_automatically_invoice_no'),
                                                                    '1' => lang('mobile_print_automatically_invoice_yes'),
                                                                    '2' => lang('mobile_print_automatically_invoice_yes_wait_time'),
                                                                );
                                 ?>
                                <?= form_dropdown('mobile_print_automatically_invoice', $mobile_print_automatically_invoice, $pos->mobile_print_automatically_invoice, 'required="required" id="mobile_print_automatically_invoice" class="form-control"') ?>
                            </div>

                            <div class="col-md-4 mobile_print_wait_time" style="display: none;">
                                <label><?= lang('mobile_print_wait_time') ?></label>
                                <input type="number" name="mobile_print_wait_time" id="mobile_print_wait_time" class="form-control" value="<?= $pos->mobile_print_wait_time; ?>" step="0.0001">
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <?= lang('use_barcode_scanner', 'use_barcode_scanner') ?>
                                <?php
                                    $use_barcode_scanner = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                 ?>
                                <?= form_dropdown('use_barcode_scanner', $use_barcode_scanner, $pos->use_barcode_scanner, 'required="required" id="use_barcode_scanner" class="form-control"') ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4 col-sm-4">
                                <?= lang('automatically_show_product_variants_and_preferences', 'automatically_show_product_variants_and_preferences') ?>
                                <?php
                                    $automatically_show_product_variants_and_preferences = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                 ?>
                                <?= form_dropdown('show_variants_and_preferences', $automatically_show_product_variants_and_preferences, $pos->show_variants_and_preferences, 'required="required" id="automatically_show_product_variants_and_preferences" class="form-control"') ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang('show_client_modal_on_select', 'show_client_modal_on_select') ?>
                                <?php
                                    $show_client_modal_on_select = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                 ?>
                                <?= form_dropdown('show_client_modal_on_select', $show_client_modal_on_select, $pos->show_client_modal_on_select, 'required="required" id="show_client_modal_on_select" class="form-control"') ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang('max_net_sale', 'max_net_sale') ?>
                                <input type="number" name="max_net_sale" id="max_net_sale" value="<?= $pos->max_net_sale ?>" class="form-control" min="0" step="0.0001">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?= lang('withholding_management', 'withholding_management') ?>
                                <?php
                                    $withholding_management = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                ?>
                                <?= form_dropdown('withholding_management', $withholding_management, $pos->withholding_management, 'required="required" id="withholding_management" class="form-control"') ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang('creation_order_sale', 'creation_order_sale') ?>
                                <?php
                                    $creation_order_sale = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                ?>
                                <?= form_dropdown('creation_order_sale', $creation_order_sale, $pos->creation_order_sale, 'required="required" id="creation_order_sale" class="form-control"') ?>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?= lang('show_price_favorite_products', 'show_price_favorite_products') ?>
                                <?php
                                    $show_price_favorite_products = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                ?>
                                <?= form_dropdown('show_price_favorite_products', $show_price_favorite_products, $pos->show_price_favorite_products, 'required="required" id="show_price_favorite_products" class="form-control"') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <?= lang('enter_to_exact_search', 'enter_to_exact_search') ?>
                                <?php
                                    $enter_to_exact_search = array(
                                                                    '0' => lang('no'),
                                                                    '1' => lang('yes'),
                                                                );
                                ?>
                                <?= form_dropdown('enter_to_exact_search', $enter_to_exact_search, $pos->enter_to_exact_search, 'required="required" id="enter_to_exact_search" class="form-control"') ?>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('check_prices_with_stock'), 'check_prices_with_stock') ?>
                                    <?= form_dropdown(['name'=>'check_prices_with_stock', 'id'=>'check_prices_with_stock', 'class'=>'form-control'], [NOT =>lang('Solo precios'), YES=>lang('Precios y existencias')], $pos->check_prices_with_stock) ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('apply_credit_to_the_payment_balance'), 'apply_credit_to_the_payment_balance') ?>
                                    <?= form_dropdown(['name'=>'apply_credit_to_the_payment_balance', 'id'=>'apply_credit_to_the_payment_balance', 'class'=>'form-control'], [NOT =>lang('no'), YES=>lang('yes')], $pos->apply_credit_to_the_payment_balance) ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('current_gold_price'), 'current_gold_price') ?>
                                    <?= form_input('current_gold_price', $pos->current_gold_price, 'class="form-control" id="current_gold_price"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('minimun_gold_price'), 'minimun_gold_price') ?>
                                    <?= form_input('minimun_gold_price', $pos->minimun_gold_price, 'class="form-control" id="minimun_gold_price"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('current_italian_gold_price'), 'current_italian_gold_price') ?>
                                    <?= form_input('current_italian_gold_price', $pos->current_italian_gold_price, 'class="form-control" id="current_italian_gold_price"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('minimun_italian_gold_price'), 'minimun_italian_gold_price') ?>
                                    <?= form_input('minimun_italian_gold_price', $pos->minimun_italian_gold_price, 'class="form-control" id="minimun_italian_gold_price"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('allow_favorities'), 'allow_favorities') ?>
                                    <?= form_dropdown(['name'=>'allow_favorities', 'id'=>'allow_favorities', 'class'=>'form-control'], [NOT =>lang('no'), YES=>lang('yes')], $pos->allow_favorities) ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('locator_management'), 'locator_management') ?>
                                    <?= form_dropdown(['name'=>'locator_management', 'id'=>'locator_management', 'class'=>'form-control'], [NOT =>lang('no'), YES=>lang('yes')], $pos->locator_management) ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('suspended_sales_default_sorting'), 'suspended_sales_default_sorting') ?>
                                    <?= form_dropdown(['name'=>'suspended_sales_default_sorting', 'id'=>'suspended_sales_default_sorting', 'class'=>'form-control'], [
                                        1 => lang('date_ascent_sorting_today'),
                                        2 => lang('date_descent_sorting_today'),
                                        3 => lang('date_descent_sorting_all')
                                    ], $pos->suspended_sales_default_sorting) ?>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= form_label(lang('suspended_sales_limited_users_filter'), 'suspended_sales_limited_users_filter') ?>
                                    <?= form_dropdown(['name'=>'suspended_sales_limited_users_filter', 'id'=>'suspended_sales_limited_users_filter', 'class'=>'form-control'], [
                                        1 => lang('biller'),
                                        2 => lang('warehouse'),
                                        3 => lang('biller_and_warehouse')
                                    ], $pos->suspended_sales_limited_users_filter) ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('pos_printing') ?></legend>

                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang('remote_printing', 'remote_printing'); ?>
                                <?php
                                // $opts = array(0 => lang('local_install'), 1 => lang('web_browser_print'), 3 => lang('php_pos_print_app'));
                                $opts = array(1 => lang('web_browser_print'), 4 => lang('direct_printing_with_google_cloud_print'));
                                ?>
                                <?= form_dropdown('remote_printing', $opts, $pos->remote_printing, 'class="form-control select2" id="remote_printing" style="width:100%;" required="required"'); ?>
                                <span class="help-block"><?= lang('print_recommandations'); ?></span>
                                <span class="help-block"><?= lang('download').': <a href="https://github.com/Tecdiary/ppp" target="_blank">PHP Pos Print Server</a>'; ?></span>
                                <?php if (DEMO) { ?>
                                <span class="help-block">On demo, you can test web printing only.</span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="printers">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= lang("auto_print", 'auto_print'); ?> <strong>*</strong>
                                    <?= form_dropdown('auto_print', $yn, $pos->auto_print, 'class="form-control select2" id="auto_print" style="width:100%;"'); ?>
                                </div>
                            </div>

                            <div class="ppp">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('use_local_printers', 'local_printers'); ?>
                                        <?= form_dropdown('local_printers', $yn, set_value('local_printers', $pos->local_printers), 'class="form-control tip" id="local_printers"  required="required"'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="lp">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('receipt_printer', 'receipt_printer'); ?> <strong>*</strong>
                                        <?php
                                        $printer_opts = array();
                                        if (!empty($printers)) {
                                            foreach ($printers as $printer) {
                                                $printer_opts[$printer->id] = $printer->title;
                                            }
                                        }
                                        ?>
                                        <?= form_dropdown('receipt_printer', $printer_opts, $pos->printer, 'class="form-control select2" id="receipt_printer" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('order_printers', 'order_printers'); ?> <strong>*</strong>
                                        <?= form_dropdown('order_printers[]', $printer_opts, '', 'multiple class="form-control select2" id="order_printers" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('cash_drawer_codes', 'cash_drawer_codes'); ?>
                                        <?= form_input('cash_drawer_codes', $pos->cash_drawer_codes, 'class="form-control" id="cash_drawer_codes" placeholder="\x1C"'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="gcp">
                                <div class="col-md-6 form-group">
                                    <?= lang('cloud_print_mail', 'cloud_print_mail') ?>
                                    <input type="text" name="cloud_print_mail" id="cloud_print_mail" value="<?= $pos->cloud_print_mail ?>" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                    <?= lang('cloud_print_service_name', 'cloud_print_service_name') ?>
                                    <input type="text" name="cloud_print_service_name" id="cloud_print_service_name" value="<?= $pos->cloud_print_service_name ?>" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                    <?= lang("cloud_print_json", "cloud_print_json") ?>
                                    <input name="cloud_print_json" id="cloud_print_json" type="file" data-browse-label="<?= lang('browse'); ?>" data-show-upload="false" data-show-preview="false" accept="application/json" class="form-control file">
                                </div>
                                <div class="col-md-6 form-group">
                                    <?= lang('direct_receipt_printer', 'direct_receipt_printer'); ?> <strong>*</strong>
                                    <?php
                                    $printer_opts = array();
                                    if (!empty($printers)) {
                                        foreach ($printers as $printer) {
                                            $printer_opts[$printer->id] = $printer->title;
                                        }
                                    }
                                    ?>
                                    <?= form_dropdown('direct_receipt_printer', $printer_opts, $pos->printer, 'class="form-control select2" id="direct_receipt_printer" style="width:100%;"'); ?>
                                </div>
                            </div>

                        </div>
                    </fieldset>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('custom_fileds') ?></legend>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <?= lang('cf_title1', 'tcf1'); ?>
                                <?= form_input('cf_title1', $pos->cf_title1, 'class="form-control tip" id="tcf1"'); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <?= lang('cf_value1', 'vcf1'); ?>
                                <?= form_input('cf_value1', $pos->cf_value1, 'class="form-control tip" id="vcf1"'); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <?= lang('cf_title2', 'tcf2'); ?>
                                <?= form_input('cf_title2', $pos->cf_title2, 'class="form-control tip" id="tcf2"'); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <?= lang('cf_value2', 'vcf2'); ?>
                                <?= form_input('cf_value2', $pos->cf_value2, 'class="form-control tip" id="vcf2"'); ?>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('shortcuts') ?></legend>
                        <p><?= lang('shortcut_heading') ?></p>

                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('focus_add_item', 'focus_add_item'); ?>
                                <?= form_dropdown(  'focus_add_item',
                                                    $shortcurts,
                                                    $pos->focus_add_item,
                                                    'class="form-control text-center tip atajo select2" id="focus_add_item" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('add_manual_product', 'add_manual_product'); ?>
                                <?= form_dropdown(  'add_manual_product',
                                                    $shortcurts,
                                                    $pos->add_manual_product,
                                                    'class="form-control text-center tip atajo select2" id="add_manual_product" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('customer_selection', 'customer_selection'); ?>
                                <?= form_dropdown(  'customer_selection',
                                                    $shortcurts,
                                                    $pos->customer_selection,
                                                    'class="form-control text-center tip atajo select2" id="customer_selection" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('add_customer', 'add_customer'); ?>
                                <?= form_dropdown(  'add_customer',
                                                    $shortcurts,
                                                    $pos->add_customer,
                                                    'class="form-control text-center tip atajo select2" id="add_customer" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('toggle_category_slider', 'toggle_category_slider'); ?>
                                <?= form_dropdown(  'toggle_category_slider',
                                                    $shortcurts,
                                                    $pos->toggle_category_slider,
                                                    'class="form-control text-center tip atajo select2" id="toggle_category_slider" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('toggle_subcategory_slider', 'toggle_subcategory_slider'); ?>
                                <?= form_dropdown(  'toggle_subcategory_slider',
                                                    $shortcurts,
                                                    $pos->toggle_subcategory_slider,
                                                    'class="form-control text-center tip atajo select2" id="toggle_subcategory_slider" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('toggle_brands_slider', 'toggle_brands_slider'); ?>
                                <?= form_dropdown(  'toggle_brands_slider',
                                                    $shortcurts,
                                                    $pos->toggle_brands_slider,
                                                    'class="form-control text-center tip atajo select2" id="toggle_brands_slider" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('cancel_sale', 'cancel_sale'); ?>
                                <?= form_dropdown(  'cancel_sale',
                                                    $shortcurts,
                                                    $pos->cancel_sale,
                                                    'class="form-control text-center tip atajo select2" id="cancel_sale" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('suspend_sale', 'suspend_sale'); ?>
                                <?= form_dropdown(  'suspend_sale',
                                                    $shortcurts,
                                                    $pos->suspend_sale,
                                                    'class="form-control text-center tip atajo select2" id="suspend_sale" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('print_items_list', 'print_items_list'); ?>
                                <?= form_dropdown(  'print_items_list',
                                                    $shortcurts,
                                                    $pos->print_items_list,
                                                    'class="form-control text-center tip atajo select2" id="print_items_list" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('finalize_sale', 'finalize_sale'); ?>
                                <?= form_dropdown(  'finalize_sale',
                                                    $shortcurts,
                                                    $pos->finalize_sale,
                                                    'class="form-control text-center tip atajo select2" id="finalize_sale" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('today_sale', 'today_sale'); ?>
                                <?= form_dropdown(  'today_sale',
                                                    $shortcurts,
                                                    $pos->today_sale,
                                                    'class="form-control text-center tip atajo select2" id="today_sale_id" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('open_hold_bills', 'open_hold_bills'); ?>
                                <?= form_dropdown(  'open_hold_bills',
                                                    $shortcurts,
                                                    $pos->open_hold_bills,
                                                    'class="form-control text-center tip atajo select2" id="open_hold_bills" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('close_register', 'close_register'); ?>
                                <?= form_dropdown(  'close_register',
                                                    $shortcurts,
                                                    $pos->close_register,
                                                    'class="form-control text-center tip atajo select2" id="close_register_id" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('search_product_price', 'search_product_price'); ?>
                                <?= form_dropdown(  'search_product_price',
                                                    $shortcurts,
                                                    $pos->search_product_price,
                                                    'class="form-control text-center tip atajo select2" id="search_product_price" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('focus_last_product_quantity', 'focus_last_product_quantity'); ?>
                                <?= form_dropdown(  'focus_last_product_quantity',
                                                    $shortcurts,
                                                    $pos->focus_last_product_quantity,
                                                    'class="form-control text-center tip atajo select2" id="focus_last_product_quantity" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('open_cash_register', 'open_cash_register'); ?>
                                <?= form_dropdown(  'open_cash_register',
                                                    $shortcurts,
                                                    $pos->open_cash_register,
                                                    'class="form-control text-center tip atajo select2" id="open_cash_register" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <!-- <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('read_weight', 'open_cash_register'); ?>
                                <?= form_dropdown(  'read_weight',
                                                    $shortcurts,
                                                    $pos->read_weight,
                                                    'class="form-control text-center tip atajo select2" id="read_weight" style="width:100%;"'); ?>
                            </div>
                        </div> -->
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('show_list_produts', 'show_list_produts'); ?>
                                <?= form_dropdown(  'show_list_produts',
                                                    $shortcurts,
                                                    $pos->show_list_produts,
                                                    'class="form-control text-center tip atajo select2" id="show_list_produts" style="width:100%;"'); ?>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('payment_gateways') ?></legend>
                        <?php
                        if ($paypal_balance) {
                            if (! isset ($paypal_balance['error']) ) {
                                echo '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button><strong>' . lang('paypal_balance') . '</strong><p>';
                                $blns = sizeof($paypal_balance['amount']);
                                $r = 1;
                                foreach ($paypal_balance['amount'] as $balance) {
                                    echo lang('balance') . ': ' . $balance['L_AMT'] . ' (' . $balance['L_CURRENCYCODE'] . ')';
                                    if ($blns != $r) {
                                        echo ', ';
                                    }
                                    $r++;
                                }
                                echo '</p></div>';
                            } else {
                                echo '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button><p>';
                                foreach ($paypal_balance['message'] as $msg) {
                                    echo $msg['L_SHORTMESSAGE'].' ('.$msg['L_ERRORCODE'].'): '.$msg['L_LONGMESSAGE'].'<br>';
                                }
                                echo '</p></div>';
                            }
                        }
                        ?>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('paypal_pro', 'paypal_pro'); ?>
                                <?= form_dropdown('paypal_pro', $yn, $pos->paypal_pro, 'class="form-control" id="paypal_pro" required="required"'); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="paypal_pro_con">
                            <div class="col-md-3 col-sm-3">
                                <div class="form-group">
                                    <?= lang('APIUsername', 'APIUsername'); ?>
                                    <?= form_input('APIUsername', $APIUsername, 'class="form-control tip" id="APIUsername"'); ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="form-group">
                                    <?= lang('APIPassword', 'APIPassword'); ?>
                                    <?= form_input('APIPassword', $APIPassword, 'class="form-control tip" id="APIPassword"'); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= lang('APISignature', 'APISignature'); ?>
                                    <?= form_input('APISignature', $APISignature, 'class="form-control tip" id="APISignature"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                        if ($stripe_balance) {
                            echo '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button><strong>' . lang('stripe_balance') . '</strong>';
                            echo '<p>' . lang('pending_amount') . ': ' . $stripe_balance['pending_amount'] . ' (' . $stripe_balance['pending_currency'] . ')';
                            echo ', ' . lang('available_amount') . ': ' . $stripe_balance['available_amount'] . ' (' . $stripe_balance['available_currency'] . ')</p>';
                            echo '</div>';
                        }
                        ?>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('stripe', 'stripe'); ?>
                                <?= form_dropdown('stripe', $yn, $pos->stripe, 'class="form-control" id="stripe" required="required"'); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="stripe_con">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= lang('stripe_secret_key', 'stripe_secret_key'); ?>
                                    <?= form_input('stripe_secret_key', $stripe_secret_key, 'class="form-control tip" id="stripe_secret_key"'); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= lang('stripe_publishable_key', 'stripe_publishable_key'); ?>
                                    <?= form_input('stripe_publishable_key', $stripe_publishable_key, 'class="form-control tip" id="stripe_publishable_key"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <?= lang('authorize', 'authorize'); ?>
                                <?= form_dropdown('authorize', $yn, $pos->authorize, 'class="form-control" id="authorize" required="required"'); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="authorize_con">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= lang('api_login_id', 'api_login_id'); ?>
                                    <?= form_input('api_login_id', $api_login_id, 'class="form-control tip" id="api_login_id"'); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= lang('api_transaction_key', 'api_transaction_key'); ?>
                                    <?= form_input('api_transaction_key', $api_transaction_key, 'class="form-control tip" id="api_transaction_key"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('restobar_settings') ?></legend>
                        <div class="col-md-4 col-sm-4">
                            <?= lang('restobar_mode_status', 'restobar_mode_status') ?>
                            <?php
                                $restobar_mode_status = array(
                                                                '0' => lang('inactive'),
                                                                '1' => lang('active'),
                                                            );
                             ?>
                            <?= form_dropdown('restobar_mode_status', $restobar_mode_status, $pos->restobar_mode, 'required="required" id="restobar_mode_status" class="form-control"') ?>
                        </div>
                        <div class="col-md-4 col-sm-4 div_restobar">
                            <?= lang('table_service', 'table_service') ?>
                            <?php
                                $table_service = array(
                                                                '0' => lang('inactive'),
                                                                '1' => lang('active'),
                                                            );
                             ?>
                            <?= form_dropdown('table_service', $table_service, $pos->table_service, 'required="required" id="table_service" class="form-control"') ?>
                        </div>
                        <div class="col-md-4 col-sm-4 div_restobar">
                            <?= lang('order_print_mode', 'order_print_mode') ?>
                            <?php
                                $order_print_mode = array(
                                                                '0' => lang('to_screen'),
                                                                // '1' => lang('to_printer'),
                                                                '2' => lang('to_preparation_area'),
                                                                '3' => lang('to_print_server'),
                                                            );
                             ?>
                            <?= form_dropdown('order_print_mode', $order_print_mode, $pos->order_print_mode, 'required="required" id="order_print_mode" class="form-control"') ?>
                        </div>
                        <div class="col-md-4 col-sm-4 div_restobar">
                            <label id="except_tip_restobar"> <?= sprintf(lang('except_tip_restobar'), lang('tip')) ?>  (%)</label>
                            <?php
                                $except_tip_restobar = array(
                                                                '0' => lang('no'),
                                                                '1' => lang('yes'),
                                                            );
                             ?>
                            <?= form_dropdown('except_tip_restobar', $except_tip_restobar, $pos->except_tip_restobar, 'required="required" id="except_tip_restobar" class="form-control"') ?>
                        </div>
                        <div class="col-md-4 col-sm-4 div_restobar">
                            <?= lang('required_shipping_restobar', 'required_shipping_restobar') ?>   <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('required_shipping_restobar_detail') ?>"></i>
                            <?php
                                $required_shipping_restobar = array(
                                                                '0' => lang('no'),
                                                                '1' => lang('yes'),
                                                            );
                             ?>
                            <?= form_dropdown('required_shipping_restobar', $required_shipping_restobar, $pos->required_shipping_restobar, 'required="required" id="required_shipping_restobar" class="form-control"') ?>
                        </div>
                        <div class="col-md-4 col-sm-4 div_restobar">
                            <?= lang('order_to_table_default_restobar', 'order_to_table_default_restobar'); ?>
                            <?= form_dropdown('order_to_table_default_restobar', ['0' => lang('no'), '1' => lang('yes')], $pos->order_to_table_default_restobar, 'required="required" id="order_to_table_default_restobar" class="form-control"') ?>
                        </div>

                        <div class="col-sm-4 div_restobar">
                            <?= lang('preparation_panel_update_time', 'preparation_panel_update_time'); ?>
                            <?php
                                $defaultTime = [
                                    1 => 5,
                                    2 => 10,
                                    3 => 15,
                                    4 => 30,
                                    5 => 45,
                                    6 => 60,
                                ];
                            ?>
                            <?= form_dropdown('preparation_panel_update_time', $defaultTime, $pos->preparation_panel_update_time, 'class="form-control" required="required" id="preparation_panel_update_time"'); ?>
                        </div>
                    </fieldset>

                    <?php if ($this->Owner) : ?>
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('electronicPOS') ?></legend>
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for=""><?= lang('activate') ?></label>
                                        <select class="form-control" name="activate_electronic_pos" id="activate_electronic_pos">
                                            <option value="<?= NOT; ?>" <?= ($this->pos_settings->activate_electronic_pos == NOT) ? "selected" : ""; ?>><?= lang("no"); ?></option>
                                            <option value="<?= YES; ?>" <?= ($this->pos_settings->activate_electronic_pos == YES) ? "selected" : ""; ?>><?= lang("yes"); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="electronig_billing_container" <?= ($this->pos_settings->activate_electronic_pos == NOT) ? 'style="display:none"' : '' ?>>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <label><?= lang('technologyProvider') ?></label>
                                            <select name="technology_provider_id" id="technology_provider_id" class="form-control">
                                                <?php if ($technology_providers): ?>
                                                    <option value=""><?= lang('select') ?></option>
                                                    <?php foreach ($technology_providers as $tec_provider): ?>
                                                        <option value="<?= $tec_provider->id ?>" <?= $this->pos_settings->technology_provider_id == $tec_provider->id ? 'selected="selected"' : '' ?>><?= $tec_provider->name ?></option>
                                                    <?php endforeach ?>
                                                <?php else: ?>
                                                    <option value=""><?= lang('no_data_available') ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <?= lang("work_environment", "work_environment"); ?>
                                            <select class="form-control" name="work_environment" id="work_environment">
                                                <option value="<?= PRODUCTION; ?>" <?= ($this->pos_settings->work_environment == PRODUCTION) ? "selected" : ""; ?>><?= lang("production"); ?></option>
                                                <option value="<?= TEST; ?>" <?= ($this->pos_settings->work_environment == TEST) ? "selected" : ""; ?>><?= lang("tests"); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row electronig_billing_container" <?= ($this->pos_settings->activate_electronic_pos == NOT) ? 'style="display:none"' : '' ?>>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="mode_electronic_pos"><?= $this->lang->line("mode"); ?></label>
                                        <select class="form-control" name="mode_electronic_pos" id="mode_electronic_pos">
                                            <option value="1" <?= ($this->pos_settings->mode_electronic_pos == $ELECTRONICPOSMODE1) ? "selected" : ""; ?>><?= $this->lang->line("mode1") ?></option>
                                            <option value="2" <?= ($this->pos_settings->mode_electronic_pos == $ELECTRONICPOSMODE2) ? "selected" : ""; ?>><?= $this->lang->line("mode2") ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="uvt_value"><?= $this->lang->line("uvt_value"); ?> <small class="text-info">Este valor se utiliza para validar topes mínimo</small></label>
                                        <input class="form-control" type="number" name="uvt_value" id="uvt_value" placeholder="Valor UVT * 5" min="0" value="<?= $this->pos_settings->uvt_value; ?>">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    <?php endif ?>

                    <div class="col-md-12 col-sm-12" style="margin-bottom: 2%">
                        <button class="btn btn-primary" id="update_settings" type="button"><?= lang('update_settings') ?></button>
                    </div>

                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function (e) {
        setTimeout(function() {
            $('.hide_password').css('display', 'none');
        }, 500);
        $("#order_printers").select2().select2('val', <?= $pos->order_printers; ?>);
        if ($('#remote_printing').val() == 1) {
            $('.printers').slideUp();
        } else if ($('#remote_printing').val() == 0) {
            $('.printers').slideDown();
            $('.ppp').slideUp();
            $('.lp').slideDown();
            $('.gcp').slideDown();
        } else if ($('#remote_printing').val() == 4) {
            $('.printers').slideDown();
            $('.ppp').slideUp();
            $('.lp').slideUp();
            $('.gcp').slideDown();
        } else {
            $('.gcp').slideDown();
            $('.printers').slideDown();
            $('.ppp').slideDown();
            if ($('#local_printers').val() == 1) {
                $('.lp').slideUp();
            } else {
                $('.lp').slideDown();
            }
        }
        $('#remote_printing').change(function () {
            if ($('#remote_printing').val() == 1) {
                $('.printers').slideUp();
            } else if ($('#remote_printing').val() == 0) {
                $('.printers').slideDown();
                $('.ppp').slideUp();
                $('.lp').slideDown();
                $('.gcp').slideDown();
            } else if ($('#remote_printing').val() == 4) {
                $('.printers').slideDown();
                $('.ppp').slideUp();
                $('.lp').slideUp();
                $('.gcp').slideDown();
            } else {
                $('.gcp').slideDown();
                $('.printers').slideDown();
                $('.ppp').slideDown();
                if ($('#local_printers').val() == 1) {
                    $('.lp').slideUp();
                } else {
                    $('.lp').slideDown();
                }
            }
        });
        $('#local_printers').change(function () {
            if ($(this).val() == 1) {
                $('.lp').slideUp();
            } else {
                $('.lp').slideDown();
            }
        });


        // $('#pos_setting').bootstrapValidator({
        //     feedbackIcons: {
        //         valid: 'fa fa-check',
        //         invalid: 'fa fa-times',
        //         validating: 'fa fa-refresh'
        //     }, excluded: [':disabled']
        // });
        $('select.select').select2({minimumResultsForSearch: 7});
        $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#customer1').val('<?= $pos->default_customer; ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#paypal_pro').change(function () {
            var pp = $(this).val();
            if (pp == 1) {
                $('#paypal_pro_con').slideDown();
            } else {
                $('#paypal_pro_con').slideUp();
            }
        });
        $('#stripe').change(function () {
            var st = $(this).val();
            if (st == 1) {
                $('#stripe_con').slideDown();
            } else {
                $('#stripe_con').slideUp();
            }
        });
        $('#authorize').change(function () {
            var st = $(this).val();
            if (st == 1) {
                $('#authorize_con').slideDown();
            } else {
                $('#authorize_con').slideUp();
            }
        });
        var st = '<?=$pos->stripe?>';
        var pp = '<?=$pos->paypal_pro?>';
        var az = '<?=$pos->authorize?>';
        if (st == 1) {
            $('#stripe_con').slideDown();
        } else {
            $('#stripe_con').slideUp();
        }
        if (pp == 1) {
            $('#paypal_pro_con').slideDown();
        } else {
            $('#paypal_pro_con').slideUp();
        }
        if (st == 1) {
            $('#authorize_con').slideDown();
        } else {
            $('#authorize_con').slideUp();
        }

        $('#balance_settings').on('change', function(){

            value = $(this).val();

            if (value == 2) {
                $('.bl_prefix').css('display', '');
            } else {
                $('#balance_label_prefix').val('');
                $('.bl_prefix').css('display', 'none');
            }

        });

        $(document).on('change', '#restobar_mode_status', function(){
            var val = $(this).val();
            if (val == 0) {
                $('#table_service').select2('val', 0).select2('readonly', true);
                $('#order_print_mode').select2('readonly', true);
                $('.div_restobar').fadeOut();
                // $('#apply_suggested_tip').select2('val', 0).select2('readonly', true);
                // $('#apply_suggested_home_delivery_amount').val(0).prop('readonly', true);
                // $('#home_delivery_in_invoice').select2('val', 0).select2('readonly', true);
            } else if (val == 1) {
                $('.div_restobar').fadeIn();
                $('#table_service').select2('val', "<?= $pos->table_service ?>").select2('readonly', false);
                $('#order_print_mode').select2('readonly', false);
                // $('#apply_suggested_tip').select2('readonly', false);
                // $('#apply_suggested_home_delivery_amount').prop('readonly', false);
                // $('#home_delivery_in_invoice').select2('readonly', false);
            }
        });

        $(document).on('change', '#mobile_print_automatically_invoice', function(){
            var val = $(this).val();
            if (val == 2) {
                $('.mobile_print_wait_time').fadeIn();
                $('#mobile_print_wait_time').prop('required', true);
            } else {
                $('.mobile_print_wait_time').fadeOut();
                $('#mobile_print_wait_time').prop('required', false);
            }
        });


        $(document).on('change', '#default_section', function () {
            if ($(this).val() == 1001 || $(this).val() == 1002) {
                if ($(this).val() == 1001) {
                    $('.div_pos_navbar_default_category').fadeIn();
                    $('.div_pos_navbar_default_subcategory').fadeOut();
                    $('#pos_navbar_default_category').attr('required', true);
                    $('#pos_navbar_default_subcategory').attr('required', false);
                } else if ($(this).val() == 1002) {
                    $('.div_pos_navbar_default_category').fadeOut();
                    $('.div_pos_navbar_default_subcategory').fadeIn();
                    $('#pos_navbar_default_category').attr('required', false);
                    $('#pos_navbar_default_subcategory').attr('required', true);
                }
            } else {
                $('.div_pos_navbar_default_category').fadeOut();
                $('.div_pos_navbar_default_subcategory').fadeOut();
                $('#pos_navbar_default_category').attr('required', false);
            }
        });


        setTimeout(function() {
            $('#restobar_mode_status').trigger('change');
            $('#mobile_print_automatically_invoice').trigger('change');
        }, 800);
        $('#default_section').trigger('change');
        $('[data-toggle="tooltip"]').tooltip();


        $(document).on('change', '#activate_electronic_pos', function() { show_hide_electronic_billing_container($(this)); });
        disable_or_enable_shorts_options();

        $("#pos_setting").validate({
            ignore: []
        });
        $(document).on('click', '#update_settings', function(){
            if ($('#pos_setting').valid()) {
                $('#pos_setting').submit();
            }
        });
    });

    function show_hide_electronic_billing_container(element) {
        var activate_electronic_pos = element.val();

        if (activate_electronic_pos == "1") {
            $('.electronig_billing_container').fadeIn();

            $('#technology_provider_id').val('<?= $this->Settings->fe_technology_provider; ?>');
            $('#technology_provider_id').attr('required', 'required');
        } else {
            $('.electronig_billing_container').fadeOut();

            $('#technology_provider_id').val('');
            $('#technology_provider_id').removeAttr('required');
        }
    }

    $(document).on('change', '.atajo', function(){
        disable_or_enable_shorts_options();
        if (site.settings.set_focus == 0 && site.pos_settings.balance_settings == 1) { // si parámetros generales set focus esta en agregar productos y parametros pos balance settings manejar balanza de peso
            if ($(this).val() == 'F8' || $(this).val() == 'f8') {
                $(this).select2('val', '');
                command: toastr.error('La tecla F8 es exclusiva del uso de balanza de peso.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "50000",
                            "extendedTimeOut": "1000",
                        });
            }
        }
    });

    function disable_or_enable_shorts_options(){
        $('.atajo').find('option').prop('disabled', false);
        let shorts = new Array();
        if ($('#focus_add_item').val() != '') { shorts.push($('#focus_add_item').val()) }
        if ($('#add_manual_product').val() != '') { shorts.push($('#add_manual_product').val()) }
        if ($('#customer_selection').val() != '') { shorts.push($('#customer_selection').val()) }
        if ($('#add_customer').val() != '') { shorts.push($('#add_customer').val()) }
        if ($('#toggle_category_slider').val() != '') { shorts.push($('#toggle_category_slider').val()) }
        if ($('#toggle_subcategory_slider').val() != '') { shorts.push($('#toggle_subcategory_slider').val()) }
        if ($('#toggle_brands_slider').val() != '') { shorts.push($('#toggle_brands_slider').val()) }
        if ($('#cancel_sale').val() != '') { shorts.push($('#cancel_sale').val()) }
        if ($('#suspend_sale').val() != '') { shorts.push($('#suspend_sale').val()) }
        if ($('#print_items_list').val() != '') { shorts.push($('#print_items_list').val()) }
        if ($('#finalize_sale').val() != '') { shorts.push($('#finalize_sale').val()) }
        if ($('#today_sale_id').val() != '') { shorts.push($('#today_sale_id').val()) }
        if ($('#open_hold_bills').val() != '') { shorts.push($('#open_hold_bills').val()) }
        if ($('#close_register_id').val() != '') { shorts.push($('#close_register_id').val()) }
        if ($('#search_product_price').val() != '') { shorts.push($('#search_product_price').val()) }
        if ($('#focus_last_product_quantity').val() != '') { shorts.push($('#focus_last_product_quantity').val()) }
        if ($('#open_cash_register').val() != '') { shorts.push($('#open_cash_register').val()) }
        // if ($('#read_weight').val() != '') { shorts.push($('#read_weight').val()) }
        if ($('#show_list_produts').val() != '') { shorts.push($('#show_list_produts').val()) }

        shorts.forEach(function(element) {
            if (element != $('#focus_add_item').val()) { $('#focus_add_item').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#add_manual_product').val()) { $('#add_manual_product').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#customer_selection').val()) { $('#customer_selection').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#add_customer').val()) { $('#add_customer').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#toggle_category_slider').val()) { $('#toggle_category_slider').find('option[value="' + element + '"]').prop('disabled', true);}
            if (element != $('#toggle_subcategory_slider').val()) { $('#toggle_subcategory_slider').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#toggle_brands_slider').val()) { $('#toggle_brands_slider').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#cancel_sale').val()) { $('#cancel_sale').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#suspend_sale').val()) { $('#suspend_sale').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#print_items_list').val()) { $('#print_items_list').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#finalize_sale').val()) { $('#finalize_sale').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#today_sale_id').val()) { $('#today_sale_id').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#open_hold_bills').val()) { $('#open_hold_bills').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#close_register_id').val()) { $('#close_register_id').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#search_product_price').val()) { $('#search_product_price').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#focus_last_product_quantity').val()) { $('#focus_last_product_quantity').find('option[value="' + element + '"]').prop('disabled', true); }
            if (element != $('#open_cash_register').val()) { $('#open_cash_register').find('option[value="' + element + '"]').prop('disabled', true); }
            // if (element != $('#read_weight').val()) { $('#read_weight').find('option[value="' + element + '"]').prop('disabled', true);}
            if (element != $('#show_list_produts').val()) { $('#show_list_produts').find('option[value="' + element + '"]').prop('disabled', true);}
        });
    }


</script>
