<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('system_settings/ubication', ['id'=>'ubicationFilterForm']) ?>
                <input type="hidden" name="statusFilter" id="statusFilter" value="<?= $statusFilter ?>">
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= lang('countries') ?></label>
                                            <?php $countryId = (isset($_POST["countryId"])) ? $_POST["countryId"] : ''; ?>
                                            <select class="form-control" id="countryId" name="countryId">
                                                <option value=""><?= lang('allsf') ?></option>
                                                <?php foreach ($countries as $country) : ?>
                                                    <option value="<?= $country->CODIGO ?>" <?= ($country->CODIGO == $countryId) ? 'selected' : '' ?>><?= ucfirst(strtolower($country->NOMBRE)) ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= lang('states') ?></label>
                                            <?php $stateId = (isset($_POST["stateId"])) ? $_POST["stateId"] : ''; ?>
                                            <select class="form-control" id="stateId" name="stateId">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php if (!empty($states)) : ?>
                                                    <?php foreach ($states as $state) : ?>
                                                        <option value="<?= $state->CODDEPARTAMENTO ?>" <?= ($state->CODDEPARTAMENTO == $stateId) ? 'selected' : '' ?> ><?= ucfirst(strtolower($state->DEPARTAMENTO)) ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= lang('origin') ?></label>
                                            <?php $cityId = (isset($_POST["cityId"])) ? $_POST["cityId"] : ''; ?>
                                            <select class="form-control" id="cityId" name="cityId">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php if (!empty($cities)) : ?>
                                                    <?php foreach ($cities as $city) : ?>
                                                        <option value="<?= $city->CODIGO ?>" <?= ($city->CODIGO == $cityId) ? 'selected' : '' ?> ><?= ucfirst(strtolower($city->DESCRIPCION)) ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="client"><?= lang('status') ?></label>
                                            <?php $statusId = (isset($_POST["status"])) ? $_POST["status"] : ''; ?>
                                            <select class="form-control" id="status" name="status">
                                                <option value=""><?= lang('alls') ?></option>
                                                <option value="<?= ACTIVE ?>" <?= (ACTIVE == $statusId) ? 'selected' : '' ?> ><?= lang("active") ?></option>
                                                <option value="<?= INACTIVE ?>" <?= (INACTIVE == $statusId) ? 'selected' : '' ?> ><?= lang("inactive") ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label><?= lang('synchronized_store') ?></label>
                                        <?php $synchronizedId = (isset($_POST["synchronized"])) ? $_POST["synchronized"] : ''; ?>
                                            <select class="form-control" id="synchronized" name="synchronized">
                                                <option value=""><?= lang('alls') ?></option>
                                                <option value="<?= YES ?>" <?= (YES == $synchronizedId) ? 'selected' : '' ?> ><?= lang("yes") ?></option>
                                                <option value="<?= NOT ?>" <?= (NOT == $synchronizedId) ? 'selected' : '' ?> ><?= lang("no") ?></option>
                                            </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="ubicationTable" class="table table-hover reports-table">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th><?= $this->lang->line("country"); ?></th>
                                            <th><?= $this->lang->line("state"); ?></th>
                                            <th><?= $this->lang->line("city"); ?></th>
                                            <th><?= $this->lang->line("zone"); ?></th>
                                            <th><?= $this->lang->line("subzone"); ?></th>
                                            <th><?= $this->lang->line("status"); ?></th>
                                            <th><?= $this->lang->line("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty">
                                                <?= lang('loading_data_from_server') ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        loadDataTables()

        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $(document).on('change', '#countryId', function() { loadStates($(this).val()); });
        $(document).on('change', '#stateId', function(){ loadCities($(this).val()); });

        $(document).on('click', '#syncUbications', function (e) {
            e.preventDefault()
            confirmSyncUbications()
        })
        $(document).on('click', '#activeInactive', function (e) {
            e.preventDefault()
            validateActiveInactive()
        })
        $(document).on('click', '.inactivate', function () {
            confirmActiveInactive($(this), false)
        });
        $(document).on('click', '.activate', function () {
            confirmActiveInactive($(this), true)
        });
    });

    function loadDataTables()
    {
        oTable = $('#ubicationTable').dataTable({
            aaSorting: [[1, "asc"]],
            aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true, 'bServerSide': true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('system_settings/getCountries') ?>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "countryId",
                    "value": $('#countryId').val()
                }, {
                    "name": "stateId",
                    "value": $('#stateId').val()
                }, {
                    "name": "cityId",
                    "value": $('#cityId').val()
                }, {
                    "name": "status",
                    "value": $('#status').val()
                }, {
                    "name": "synchronized",
                    "value": $('#synchronized').val()
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                {
                    bSortable: false,
                    mRender: function (data, type, row) {
                        return renderCheckbox(data, row)
                    }
                },
                null,
                null,
                null,
                null,
                null,
                {
                    className: 'text-center',
                    mRender: function (data, type, row) {                        
                        return renderStatus(data, row)
                    }
                },
                {
                    bSortable: false,
                    sWidth: '65px',
                    className: 'text-center',
                    mRender: function (data, type, row) {                        
                        return renderActionsUbications(data, row)
                    }
                }
            ],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html(`<div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('system_settings/add_ubication/1'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_country') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('system_settings/add_ubication/2'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_state') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('system_settings/add_ubication/3'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_city') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('system_settings/add_ubication/4'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_zone') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('system_settings/add_ubication/5'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_subzone') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="" id="activeInactive">
                                <i class="fas fa-toggle-on"></i> <?= lang('active/inactive') ?>
                            </a>
                        </li>
                        <li>
                            <a href="" id="syncUbications">
                                <i class="fa-solid fa-rotate"></i> <?= lang('sync_ubications') ?>
                            </a>
                        </li>
                    </ul>
                </div>`).addClass('col-without-padding');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });
    }

    function renderCheckbox(data, row)
    {
        let check = checkbox(data)
        let actions = $(check)

        let country = row[1]
        let state = row[2]
        let city = row[3]

        if (city !== null) {
            actions.find('.checkbox').attr('data-type', 'city');
        } else if (state !== null) {
            actions.find('.checkbox').attr('data-type', 'state');
        } else {
            actions.find('.checkbox').attr('data-type', 'country');
        }

        let str = actions.prop('outerHTML')                     

        return str;
    }

    function renderStatus(data, row)
    {
        let id = row[0]
        let country = row[1]
        let state = row[2]
        let city = row[3]

        if (city !== null) {
            type = 'city';
        } else if (state !== null) {
            type = 'state';
        } else {
            type = 'country';
        }
        
        if (data == 1) {
            return `<i class="fas fa-toggle-on fa-3x text-primary pointer inactivate" 
                data-toggle-second="tooltip" data-placement="top" title="Inactivar" 
                data-id="${id}" data-type="${type}"></i>`
        } else {
            return `<i class="fas fa-toggle-off fa-3x text-default pointer activate" 
                data-toggle-second="tooltip" data-placement="top" title="Activar" 
                data-id="${id}" data-type="${type}"></i>`
        }
    }

    function renderActionsUbications(data, row) 
    {
        let actions = $(data)
                        
        let country = row[1]
        let state = row[2]
        let city = row[3]
        let zone = row[4]
        let subzone = row[5]
        
        if (country == null) {
            actions.find('.dropdown-menu').find('.country').remove();
        }
        if (state == null) {
            actions.find('.dropdown-menu').find('.state').remove();
            actions.find('.dropdown-menu').find('.deleteState').remove();
        }
        if (city == null) {
            actions.find('.dropdown-menu').find('.city').remove();
            actions.find('.dropdown-menu').find('.deleteCity').remove();
        }
        if (zone == null) {
            actions.find('.dropdown-menu').find('.zone').remove();
            actions.find('.dropdown-menu').find('.deleteZone').remove();
        }
        if (subzone == null) {
            actions.find('.dropdown-menu').find('.subzone').remove();
            actions.find('.dropdown-menu').find('.deleteSubzone').remove();
        }

        let str = actions.prop('outerHTML')

        return str;
    }

    function loadStates(country)
    {
        $.ajax({
            type: "POST",
            url: site.base_url+`system_settings/getState`,
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'countryId': country
            },
            dataType: "HTML",
            success: function (response) {
                $('#stateId').html(response);
            }
        });
    }

    function loadCities(state)
    {
        $.ajax({
            type: "POST",
            url: site.base_url+`system_settings/getCities`,
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'stateId': state
            },
            dataType: "HTML",
            success: function (response) {
                $('#cityId').html(response);
            }
        });
    }

    function confirmSyncUbications()
    {
        swal({
            title: "Sincronización de Ubicaciones a Tienda PRO.",
            text: "¿Está seguro de realizar la sincronización?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: 'Realizando sincronización de Ubicaciones...',
                text: 'Por favor, espere un momento.',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            syncUbications()
        });
    }

    function syncUbications()
    {
        $.ajax({
            type: "POST",
            url: site.base_url +`system_settings/syncUbications`,
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
            },
            dataType: "JSON",
            success: function (response) {
                if (response == true) {
                    swal({
                        title: '<?= lang("toast_success_title") ?>',
                        text: 'Los ubicaciones fueron sincronizados en Tienda Pro',
                        type: 'success',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    });
                } else {
                    swal({
                        title: '<?= lang(("toast_error_title")) ?>',
                        text: `Hubo un problema durante el proceso.`,
                        type: 'error',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    });
                }
            }
        });
    }

    function validateActiveInactive()
    {        
        if ($('#statusFilter').val() == 0) {
            swal({
                title: 'Validación de estado',
                text: 'No se ha aplicado el filtro por estado',
                type: 'info',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            })

            return;
        }
        
        if ($('.multi-select:checked').length == 0) {
            swal({
                title: 'Validación de selección',
                text: 'No ha seleccionado ninguna ubicación a procesar',
                type: 'info',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            })
            return;
        } 
        
        confirmAllActiveInactive()
    }

    function confirmAllActiveInactive()
    {
        let status = $('#status').val() == 0 ? 1 : 0 
        let text = (status) ? "Activar" : "Inactivar";
        let text2 = (status) ? "Activación" : "Inactivación";

        swal({
            title: `Proceso para ${text} ubicaciones`,
            text: `¿Está seguro de ${text} las ubicaciones seleccionadas?`,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: `Realizando ${text2} de Ubicaciones...`,
                text: 'Por favor, espere un momento.',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            allActiveInactive()
        });
    }

    function allActiveInactive(element) 
    {

        let ubications = [];
        let status = $('#status').val() == 0 ? 1 : 0 

        $('.multi-select:checked').each(function(index, element) {
            ubications.push({ 
                "code": $(this).val(),
                "type": $(this).data('type')
            });
        });        

        $.ajax({
            type: "POST",
            url: site.base_url+`system_settings/activeInactive`,
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'status': status,
                'ubications': JSON.stringify(ubications)
            },
            dataType: "JSON",
            success: function(response) {
                if (response == true) {
                    let text = (status) ? "Activadas" : "Inactivadas";
                    let text2 = (status) ? "Activación" : "Inactivación";
                    swal({
                        title: `Proceso de ${text2} éxitosa`,
                        text: `Las ubicaciones fueron ${text}`,
                        type: 'success',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    }, function (isConfirm) {
                        if (!isConfirm) {
                            location.reload()
                        }
                    });
                } else {
                    swal({
                        title: `Proceso de fallido`,
                        text: `Hubo un problema durante el proceso.`,
                        type: 'error',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    });
                }
            }
        });
    }

    function confirmActiveInactive(element, activate)
    {
        let process = (activate == true) ? 'Activar'  : 'Inactivar'
        let process2 = (activate == true) ? 'Activación'  : 'Inactivarción'
        swal({
            title: `Proceso para ${process} ubicación`,
            text: `¿Está seguro de ${process} las ubicación seleccionada?`,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: `Realizando ${process2} de Ubicación...`,
                text: 'Por favor, espere un momento.',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            activateInactivate(element, activate)
        });
    }

    function activateInactivate(element, activate) {
        let id = element.data('id')
        let type = element.data('type')
        let process = (activate == true) ? 'Activación'  : 'Inactivarción'

        $.ajax({
            type: "POST",
            url: site.base_url + `system_settings/activateInactivateUbication`,
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'id': id,
                'type': type,
                'activate' : activate,
            },
            dataType: "JSON",
            success: function (response) {
                if (response == true) {
                    swal({
                        title: `Proceso de ${process} éxitosa`,
                        text: `Las ubicación se Inactivó`,
                        type: 'success',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    }, function (isConfirm) {
                        if (!isConfirm) {
                            location.reload()
                        }
                    });
                } else {
                    swal({
                        title: `Proceso de fallido`,
                        text: `Hubo un problema durante el proceso.`,
                        type: 'error',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    });
                }
            }
        })
    }
</script>