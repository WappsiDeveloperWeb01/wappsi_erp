<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<!-- c3 Charts -->
<link href="<?= $assets ?>inspinia/css/plugins/c3/c3.min.css" rel="stylesheet">
<link href="<?= $assets ?>inspinia/css/plugins/chartist/chartist.min.css" rel="stylesheet">

<style type="text/css">
    .smaller_left_padding {
        padding-left: 3px;
    }

    .smaller_right_padding {
        padding-right: 3px;
    }

    .chart_titles {
        font-size: 18px;
    }

    h2 {
        font-size: 18px;
    }
</style>

<div class="wrapper wrapper-content animated fadeInRight ">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control box_shadow" name="quarte" id="quarte">
                    <option value="1" <?= (date('m') == 1 || date('m') == 2 || date('m') == 3) ? "selected" : ""; ?>>Trimestre 1 <?= date("Y"); ?> &nbsp; ENE - FEB - MAR</option>
                    <option value="2" <?= (date('m') == 4 || date('m') == 5 || date('m') == 6) ? "selected" : ""; ?>>Trimestre 2 <?= date("Y"); ?> &nbsp; ABR - MAY - JUN</option>
                    <option value="3" <?= (date('m') == 7 || date('m') == 8 || date('m') == 9) ? "selected" : ""; ?>>Trimestre 3 <?= date("Y"); ?> &nbsp; JUL - AGO - SEP</option>
                    <option value="4" <?= (date('m') == 10 || date('m') == 11 || date('m') == 12) ? "selected" : ""; ?>>Trimestre 4 <?= date("Y"); ?> &nbsp; OCT - NOV - DIC</option>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control box_shadow" name="month" id="month">
                    <option value="">Todos los meses</option>
                    <option value="1">Enero <?= date("Y"); ?></option>
                    <option value="2">Febrero <?= date("Y"); ?></option>
                    <option value="3">Marzo <?= date("Y"); ?></option>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control box_shadow" name="branch_office" id="branch_office">
                    <option value="">Todas las sucursales</option>
                    <?php if (!empty($billers)) { ?>
                        <?php $userBillerId = ""; ?>
                        <?php if ($this->session->userdata('biller_id')) { ?>
                            <?php $userBillerId = $this->session->userdata('biller_id') ?>
                        <?php } else { ?>
                            <?php $selected = (count($billers) == 1) ? "selected" : ""; ?>
                        <?php } ?>

                        <?php foreach ($billers as $biller) { ?>
                            <option value="<?= $biller->id; ?>" <?= ($biller->id == $userBillerId) ? "selected" : '' ?>><?= $biller->company; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary new-button" id="search_sales_data" data-toggle="tooltip" data-placement="left" title="Aplicar Filtros" style="margin-top: -10px">
                        <i class="fa fa-filter"></i>
                    </button>
                    <?php if ($this->Owner || $this->Admin) { ?>
                        <button class="btn btn-primary new-button" id="update_view" data-toggle="tooltip" data-placement="bottom" title="Actualizar Datos" style="margin-top: -10px">
                            <i class="fa fa-refresh"></i>
                        </button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="ibox box_shadow">
                <div class="ibox-title" style="min-height: 0px; padding: 10px 20px 7px;">
                    <h3 class="chart_titles" style="margin-bottom: 5px;" id="variations_graph">Rentabilidad </h3>
                </div>
                <div class="ibox-content test" style="padding: 1px 10px 7px;">
                    <div class="row" style="display: contents;">
                        <div class="col-md-12">
                            <div class=" text-center" style="margin-top: 40px; margin-bottom: 40px">
                                <h2 id="profitability_margin_sales">0%</h2>
                                <div class="progress progress-mini" style="margin-bottom: 10px;" id="profitability_margin_progress-bar">
                                    <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="display: contents;">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="text-bold text-primary"><strong id="previous_profitability_margin_sales" style="font-size: 14px">0%</strong></div>
                                    <h4>Anterior</h4>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <div class="text-bold text-primary"><strong id="variation_profit_margin" style="font-size: 14px">0%</strong></div>
                                    <h4>Variación</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="ibox box_shadow">
                <div class="ibox-title" style="min-height: 0px; padding: 10px 20px 7px;">
                    <h3 class="chart_titles" style="margin-bottom: 5px;" id="variations_graph_guage">Cumplimiento &nbsp; <i class="fa fa-arrow-down fa-1x text-danger"></i></h3>
                </div>
                <div class="ibox-content test" style="padding: 1px 10px 7px;">
                    <div class="row" style="display: contents;">
                        <div class="col-lg-12">
                            <div>
                                <div id="gauge"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="display: contents;">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="text-bold text-primary"><strong id="percentage_sale_budget" style="font-size: 14px">0%</strong></div>
                                    <h4>Actual</h4>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <div class="text-bold text-primary"><strong id="previous_percentage_sale_budget" style="font-size: 14px">0%</strong></div>
                                    <h4>Anterior</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-3">
            <div class="ibox box_shadow">
                <div class="ibox-title" style="min-height: 0px; padding: 10px 20px 7px;">
                    <h3 class="chart_titles" style="margin-bottom: 5px;">Tipos de factura</h3>
                </div>
                <div class="ibox-content test" style="padding: 1px 10px 7px;">
                    <div class="row" style="display: contents;">
                        <div class="col-lg-10">
                            <h4 style=" font-weight: bolder;">Venta</h4>
                            <div class="progress progress-mini" style="margin-bottom: 10px;" id="invoices_detal_progress-bar">
                                <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                            </div>

                            <h4 style="font-weight: bolder;">Ventas Electrónicas</h4>
                            <div class="progress progress-mini" style="margin-bottom: 10px;" id="invoices_detal_fe_progress-bar">
                                <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                            </div>

                            <h4 style="font-weight: bolder;">POS</h4>
                            <div class="progress progress-mini" style="margin-bottom: 10px;" id="invoices_pos_progress-bar">
                                <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                            </div>

                            <h4 style="font-weight: bolder;">POS Electrónicas</h4>
                            <div class="progress progress-mini" style="margin-bottom: 10px;" id="invoices_pos_fe_progress-bar">
                                <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                            </div>
                        </div>
                        <div class="col-lg-2 text-right">
                            <h4 id="invoices_detal_percentage" style="margin-bottom: 25px;">0%</h4>
                            <h4 id="invoices_detal_fe_percentage" style="margin-bottom: 25px;">0%</h4>
                            <h4 id="invoices_pos_percentage" style="margin-bottom: 25px;">0%</h4>
                            <h4 id="invoices_pos_fe_percentage" style="margin-bottom: 25px;">0%</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="row">
                <div class="col-lg-6 text-center smaller_right_padding">
                    <div class="ibox box_shadow">
                        <div class="ibox-content" style="padding: 5px 10px; min-height: 210px;">
                            <h3>Clientes</h3>
                            <h2 class="text-primary"><label style="margin-top: 20px"><span id="total_customers">0</span>/<?= $total_customers; ?></label></h2>
                            <div class="progress progress-mini" style="margin-bottom: 25px;" id="total_customer_progress-bar">
                                <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                            </div>

                            <h3>Productos</h3>
                            <h2 class="text-primary"><label style="margin-top: 20px"><span id="total_products">0</span>/<?= $total_products; ?></label></h2>
                            <div class="progress progress-mini" id="total_products_progress-bar">
                                <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-center smaller_left_padding">
                    <div class="ibox box_shadow">
                        <div class="ibox-content" style="padding: 5px 10px; min-height: 210px;">
                            <h3>Ventas</h3>
                            <h2 class="text-primary "><label style="margin-top: 20px; margin-bottom: 35px"><span id="amount_total_sales">0</span></label></h2>

                            <h3>Promedio</h3>
                            <h2 class="text-primary"><label style="margin-top: 20px;" id="average_sales">0</label></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox box_shadow">
                <div class="ibox-title">
                    <h3 class="chart_titles">Comparativa de ventas</h3>
                </div>
                <div class="ibox-content" id="sales_data_container" style="padding-top: 0px;">
                    <p class="default_message">No existen registros</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox box_shadow">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-lg-10">
                            <h3 class="chart_titles">Ventas por clientes</h3>
                        </div>
                        <div class="col-lg-2">
                            <select class="form-control" id="customers_search_option" name="customers_search_option">
                                <option value="1">Valor total de ventas</option>
                                <option value="2">Número de Ventas</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="ibox-content" id="customers_data_container">
                    <div>
                        <p class="default_message">No existen registros</p>
                    </div>
                </div>
                <div class="ibox-footer text-right">
                    <button class="btn btn-primary btn-outline" id="show_more_customer_data">Mostrar más...</button>
                    <button class="btn btn-primary btn-outline" id="show_less_customer_data" style="display: none;">Mostrar menos...</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox box_shadow">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-lg-7">
                            <h3 class="chart_titles">Ventas por productos</h3>
                        </div>
                        <div class="col-lg-3">
                            <label class="radio-inline" style="margin: 5px;">
                                <input class="groupBy" type="radio" name="groupBy" id="groupBy1" value="1" checked> Por producto
                            </label>
                            <label class="radio-inline" style="margin: 5px;">
                                <input class="groupBy" type="radio" name="groupBy" id="groupBy2" value="2"> Por referecia
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <select class="form-control" id="products_search_option" name="products_search_option">
                                <option value="1">Valor total de ventas</option>
                                <option value="2">Número de Ventas</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="ibox-content" id="products_data_container">
                    <div>
                        <p class="default_message">No existen registros</p>
                    </div>
                </div>
                <div class="ibox-footer text-right">
                    <button class="btn btn-primary btn-outline" id="show_more_products_data">Mostrar más...</button>
                    <button class="btn btn-primary btn-outline" id="show_less_products_data" style="display: none;">Mostrar menos...</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox box_shadow">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-lg-10">
                            <h3 class="chart_titles">Ventas por vendedores</h3>
                        </div>
                        <div class="col-lg-2">
                            <select class="form-control" id="sellers_search_option" name="sellers_search_option">
                                <option value="1">Valor total de ventas</option>
                                <option value="2">Número de Ventas</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="ibox-content" id="sellers_data_container">
                    <div>
                        <p class="default_message">No existen registros</p>
                    </div>
                </div>
                <div class="ibox-footer text-right">
                    <button class="btn btn-primary btn-outline" id="show_more_seller_data">Mostrar más...</button>
                    <button class="btn btn-primary btn-outline" id="show_less_seller_data" style="display: none;">Mostrar menos...</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox box_shadow">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-lg-10">
                            <h3 class="chart_titles">Ventas por Marcas</h3>
                        </div>
                        <div class="col-lg-2">
                            <select class="form-control" id="brands_search_option" name="brands_search_option">
                                <option value="1">Valor total de ventas</option>
                                <option value="2">Número de Ventas</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="ibox-content" id="brands_data_container">
                    <div>
                        <p class="default_message">No existen registros</p>
                    </div>
                </div>
                <div class="ibox-footer text-right">
                    <button class="btn btn-primary btn-outline" id="show_more_brands_data">Mostrar más...</button>
                    <button class="btn btn-primary btn-outline" id="show_less_brands_data" style="display: none;">Mostrar menos...</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox box_shadow">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-lg-10">
                            <h3 class="chart_titles">Ventas por Categorias</h3>
                        </div>
                        <div class="col-lg-2">
                            <select class="form-control" id="categories_search_option" name="categories_search_option">
                                <option value="1">Valor total de ventas</option>
                                <option value="2">Número de Ventas</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="ibox-content" id="categories_data_container">
                    <div>
                        <p class="default_message">No existen registros</p>
                    </div>
                </div>
                <div class="ibox-footer text-right">
                    <button class="btn btn-primary btn-outline" id="show_more_categories_data">Mostrar más...</button>
                    <button class="btn btn-primary btn-outline" id="show_less_categories_data" style="display: none;">Mostrar menos...</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 ">
            <div class="ibox box_shadow">
                <div class="ibox-title">
                    <h3 class="chart_titles">Ventas por impuestos</h3>
                </div>
                <div class="ibox-content" id="taxes_data_container">
                    <p class="default_message">No existen registros</p>
                </div>
            </div>
        </div>

        <div class="col-lg-6 ">
            <div class="ibox box_shadow">
                <div class="ibox-title">
                    <h3 class="chart_titles">Ventas por medios de pago</h3>
                </div>
                <div class="ibox-content" id="payment_means_data_container">
                    <p class="default_message">No existen registros</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ChartJS-->
<script src="<?= $assets ?>inspinia/js/plugins/chartJs/Chart.min.js"></script>

<!-- d3 and c3 charts -->
<script src="<?= $assets ?>inspinia/js/plugins/d3/d3.min.js"></script>
<script src="<?= $assets ?>inspinia/js/plugins/c3/c3.min.js"></script>

<!-- Chartist -->
<script src="<?= $assets ?>inspinia/js/plugins/chartist/chartist.min.js"></script>

<script language="javascript">
    $(document).ready(function() {
        $('body').addClass('mini-navbar');

        _total = 0;
        _previous_total = 0;
        _totalWithOutDiscount = 0;
        _created_chart = false;
        _brands_barChart = false;
        _customers_barChart = false;
        _products_barChart = false;
        _sellers_barChart = false;
        _categories_barChart = false;
        _quartes = {
            '1': [{
                    'number': 1,
                    'name': 'Enero'
                },
                {
                    'number': 2,
                    'name': 'Febrero'
                },
                {
                    'number': 3,
                    'name': 'Marzo'
                }
            ],
            '2': [{
                    'number': 4,
                    'name': 'Abril'
                },
                {
                    'number': 5,
                    'name': 'Mayo'
                },
                {
                    'number': 6,
                    'name': 'Junio'
                }
            ],
            '3': [{
                    'number': 7,
                    'name': 'Julio'
                },
                {
                    'number': 8,
                    'name': 'Agosto'
                },
                {
                    'number': 9,
                    'name': 'Septiembre'
                }
            ],
            '4': [{
                    'number': 10,
                    'name': 'Octubre'
                },
                {
                    'number': 11,
                    'name': 'Noviembre'
                },
                {
                    'number': 12,
                    'name': 'Diciembre'
                }
            ]
        };

        preLoadUserData()
        // load_fulfillment();
        equalize_height_containers();

        $(document).on('click', '#update_view', function() {
            confirm_update();
        });
        $(document).on('change', '#quarte', function() {
            load_months($(this).val());
        });
        $(document).on('click', '#search_sales_data', function() {
            search_sales_data();
            search_sales_budget_data();
            search_invoices_types();
            search_customers_data();
            search_products_data();
            search_sellers_data();
            search_taxes_data();
            search_brands_data();
            search_categories_data();
            search_payment_means_data();
            reset_graphics();
        });

        $(document).on('change', '#customers_search_option', function() {
            search_customers_data();
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });
        $(document).on('click', '#show_more_customer_data', function() {
            search_customers_data(50);
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });
        $(document).on('click', '#show_less_customer_data', function() {
            search_customers_data();
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });

        $(document).on('change', '#products_search_option', function() {
            search_products_data();
            if (_products_barChart == true) {
                _products_barChart.destroy();
            }
        });
        $(document).on('click', '#show_more_products_data', function() {
            search_products_data(50);
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });
        $(document).on('click', '#show_less_products_data', function() {
            search_products_data();
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });

        $('.groupBy:checked').on('ifChanged', function(event) {
            search_products_data();
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });

        $(document).on('change', '#sellers_search_option', function() {
            search_sellers_data();
            if (_sellers_barChart == true) {
                _sellers_barChart.destroy();
            }
        });
        $(document).on('click', '#show_more_seller_data', function() {
            search_sellers_data(50);
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });
        $(document).on('click', '#show_less_seller_data', function() {
            search_sellers_data();
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });

        $(document).on('change', '#brands_search_option', function() {
            search_brands_data();
            if (_brands_barChart == true) {
                _brands_barChart.destroy();
            }
        });
        $(document).on('click', '#show_more_brands_data', function() {
            search_brands_data(50);
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });
        $(document).on('click', '#show_less_brands_data', function() {
            search_brands_data();
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });

        $(document).on('change', '#categories_search_option', function() {
            search_categories_data();
            if (_categories_barChart == true) {
                _categories_barChart.destroy();
            }
        });
        $(document).on('click', '#show_more_categories_data', function() {
            search_categories_data(50);
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });
        $(document).on('click', '#show_less_categories_data', function() {
            search_categories_data();
            if (_customers_barChart == true) {
                _customers_barChart.destroy();
            }
        });

        $('#quarte').trigger('change');
        $('#search_sales_data').trigger('click');
    });

    function preLoadUserData() {
        <?php if ($this->session->userdata('biller_id')): ?>
            setTimeout(function() {
                $('#branch_office').select2('readonly', true);
			}, 250);
		<?php endif ?>
    }

    function confirm_update() {
        swal({
            title: "Actualización de Datos",
            text: "Se realizará actualización completa de la información. Esto podria tardar un poco. ¿Desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Actualizando Datos...',
                text: 'Por favor, espere un momento.',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            update_view();
        });
    }

    function update_view() {
        $.ajax({
                url: '<?= admin_url("dashboard/update_view"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>'
                },
            })
            .done(function(data) {
                if (data.status == true) {
                    header_alert('success', data.message);
                    swal.close()

                    $('#search_sales_data').trigger('click');
                } else {
                    header_alert('error', data.message);
                }
            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function load_months(quarte_id) {
        var quarte = _quartes[quarte_id]
        var options_months = '<option value="">Todos los meses</option>';
        var date = new Date();
        var current_month = date.getMonth() + 1;

        $(quarte).each(function(i, month) {
            selected = (month.number == current_month) ? 'selected' : '';
            options_months += '<option value="' + month.number + '" ' + selected + '>' + month.name + ' ' + date.getFullYear().toString() + '</option>';
        });

        $('#month').html(options_months);
        $('#month').select2('val', '');
    }

    function reset_graphics() {
        if (_created_chart == true) {
            _chartico.destroy();
        }
        if (_brands_barChart == true) {
            _brands_barChart.destroy();
        }
        if (_customers_barChart == true) {
            _customers_barChart.destroy();
        }
        if (_products_barChart == true) {
            _products_barChart.destroy();
        }
        if (_sellers_barChart == true) {
            _sellers_barChart.destroy();
        }
        if (_categories_barChart == true) {
            _categories_barChart.destroy();
        }
    }

    function search_sales_data() {
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var month = $('#month').val();
        var branch_office = $('#branch_office').val();
        var previous_quarte_array = [];
        var previous_month = '';

        if (quarte_id > 1) {
            previous_quarte_array = _quartes[quarte_id - 1];
        }

        if (month > 1) {
            previous_month = month - 1;
        }

        $.ajax({
                url: '<?= admin_url("dashboard/search_sales_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'quarte_id': quarte_id,
                    'quarte': quarte_array,
                    'month': month,
                    'branch_office': branch_office,
                    'previous_quarte': previous_quarte_array,
                    'previous_month': previous_month
                },
            })
            .done(function(data) {
                if (data.length == 0) {
                    $('#sales_data_container').html('<p class="default_message">No existen registros</p>');
                } else {
                    $('#sales_data_container').html('<div class="row">' +
                        '<div class="col-lg-9">' +
                        '<div>' +
                        '<canvas id="lineChart" height="110"></canvas>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-lg-3">' +
                        '<ul class="stat-list">' +
                        '<li>' +
                        '<h3 class="no-margins" id="total_sales">$ 0.00</h3>' +
                        '<small>Total ventas</small>' +
                        '<div class="progress progress-mini" id="total_sales_progress-bar">' +
                        '<div style="width: 0%;" class="progress-bar progress-bar-success"></div>' +
                        '</div>' +
                        '</li>' +
                        '<li>' +
                        '<h3 class="no-margins" id="subtotal_sales_label">$ 0.00</h3>' +
                        '<small>Ventas antes de impuestos</small>' +
                        '<div class="progress progress-mini" id="subtotal_sales_progress-bar">' +
                        '<div style="width: 0%;" class="progress-bar progress-bar-success"></div>' +
                        '</div>' +
                        '</li>' +
                        '<li>' +
                        '<h3 class="no-margins" id="discount_sales">$ 0.00</h3>' +
                        '<small>Descuentos</small>' +
                        '<div class="progress progress-mini" id="discount_sales_progress-bar">' +
                        '<div style="width: 0%;" class="progress-bar progress-bar-success"></div>' +
                        '</div>' +
                        '</li>' +
                        '<li>' +
                        '<h3 class="no-margins" id="tax_sales">0.00</h3>' +
                        '<small>Impuestos</small>' +
                        '<div class="progress progress-mini" id="tax_sales_progress-bar">' +
                        '<div style="width: 0%;" class="progress-bar progress-bar-success"></div>' +
                        '</div>' +
                        '</li>' +
                        '<li>' +
                        '<h3 class="no-margins" id="cost_subtotal_sales">0.00</h3>' +
                        '<small>Costos antes de impuestos</small>' +
                        '<div class="progress progress-mini" id="cost_subtotal_progress-bar">' +
                        '<div style="width: 0%;" class="progress-bar progress-bar-success"></div>' +
                        '</div>' +
                        '</li>' +
                        '<li>' +
                        '<h3 class="no-margins" id="return_total_sales">0.00</h3>' +
                        '<small>Notas créditos</small>' +
                        '<div class="progress progress-mini" id="return_total_progress-bar">' +
                        '<div style="width: 0%;" class="progress-bar progress-bar-success"></div>' +
                        '</div>' +
                        '</li>' +
                        '<li>' +
                        '<h3 class="no-margins" id="gross_profit_sales">0.00</h3>' +
                        '<small>Utilidad bruta</small>' +
                        '<div class="progress progress-mini" id="gross_profit_progress-bar">' +
                        '<div style="width: 0%;" class="progress-bar progress-bar-success"></div>' +
                        '</div>' +
                        '</li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>');

                    load_sales_summary(data.day_array, data.cost_array, data.total_array, data.previous_total_array);
                }

                load_total_data(data.sub_total_array, data.tax_array, data.discount_array, data.amount_total_sales);
                load_subtotal_data(data.sub_total_array);
                load_discount_data(data.discount_array);
                load_tax_data(data.tax_array);
                load_cost_subtotal_data(data.cost_subtotal_array);
                load_return_total_data(data.return_total_array);
                load_gross_profit_data();
                load_profitability_margin_data(data.previous_subtotal, data.previous_discount, data.previous_cost_subtotal, data.previous_return_total, data.previous_tax_array);
                search_sales_budget_data();
            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function load_sales_summary(day_array, cost_array, total_array, previous_total_array) {
        selected_month = $('#month').val();
        selected_quarter = $('#quarte').val();
        previous_label_month = '';
        label_month = '';

        if (selected_month != '') {
            $(_quartes[selected_quarter]).each(function(i, month) {
                if (month.number == selected_month) {
                    label_month = month.name;
                }

                if (month.number == selected_month - 1) {
                    previous_label_month = month.name;
                }
            });

            previous_label = previous_label_month;
            current_label = label_month;
        } else {
            previous_label = 'Trimestre ' + (parseInt(selected_quarter) - 1).toString();
            current_label = 'Trimestre ' + selected_quarter;
        }

        var lineData = {
            labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
            datasets: [{
                    label: "Costos",
                    type: "line",
                    backgroundColor: 'rgba(20, 132, 198, 0.3)',
                    borderColor: "rgba(20, 132, 198,0.3)",
                    pointBackgroundColor: "rgba(20, 132, 198,0.5)",
                    pointBorderColor: "#fff",
                    data: cost_array
                },
                {
                    label: previous_label,
                    type: "bar",
                    backgroundColor: 'rgba(186, 186, 186, 1)',
                    pointBorderColor: "#fff",
                    data: previous_total_array
                },
                {
                    label: current_label,
                    type: "bar",
                    backgroundColor: 'rgba(20, 132, 198, 1)',
                    pointBorderColor: "#fff",
                    data: total_array
                },
            ]
        };

        var ctx = document.getElementById("lineChart").getContext("2d");
        _chartico = new Chart(ctx, {
            type: 'bar',
            data: lineData,
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                }
            }
        });
        _created_chart = true;
    }

    function load_total_data(sub_total_array, tax_array, discount_array, amount_total_sales) {
        subtotal = 0;
        if (sub_total_array.length > 0) {
            $(sub_total_array).each(function(i, value) {
                subtotal += parseInt(value);
            });
        }

        tax = 0;
        if (tax_array.length > 0) {
            $(tax_array).each(function(i, value) {
                tax += parseInt(value);
            });
        }

        discount = 0;
        if (discount_array.length > 0) {
            $(discount_array).each(function(i, value) {
                discount += parseInt(value);
            });
        }

        total = (subtotal + tax - discount);
        totalWithOutDiscount = (subtotal + tax);

        _total = total;
        _totalWithOutDiscount = totalWithOutDiscount

        average_sales = total / amount_total_sales;

        $('#total_sales').html(formatMoney(total));
        $('#total_sales_progress-bar').html('<div style="width:100%;" class="progress-bar progress-bar-success"></div>');
        $('#amount_total_sales').text(amount_total_sales);
        $('#average_sales').text(formatMoney(average_sales));
    }

    function load_subtotal_data(sub_total_array) {
        porcentage_subtotal = subtotal * 100 / total;

        $('#subtotal_sales_label').html(formatMoney(subtotal));
        $('#subtotal_sales_progress-bar').html('<div style="width:' + porcentage_subtotal + '%;" class="progress-bar progress-bar-success"></div>');
    }

    function load_discount_data(discount_array) {
        discount = 0;
        $(discount_array).each(function(i, value) {
            discount += parseInt(value);
        });

        porcentage_discount = discount * 100 / total;

        $('#discount_sales').html(formatMoney(discount));
        $('#discount_sales_progress-bar').html('<div style="width:' + porcentage_discount + '%;" class="progress-bar progress-bar-success"></div>');
    }

    function load_tax_data(tax_array) {
        tax = 0;
        $(tax_array).each(function(i, value) {
            tax += parseInt(value);
        });

        porcentage_tax = tax * 100 / total;

        $('#tax_sales').html(formatMoney(tax));
        $('#tax_sales_progress-bar').html('<div style="width:' + porcentage_tax + '%;" class="progress-bar progress-bar-success"></div>');
    }

    function load_cost_subtotal_data(cost_subtotal_array) {
        cost_subtotal = 0;
        $(cost_subtotal_array).each(function(i, value) {
            cost_subtotal += parseInt(value);
        });

        porcentage_cost_subtotal = cost_subtotal * 100 / total;

        $('#cost_subtotal_sales').html(formatMoney(cost_subtotal));
        $('#cost_subtotal_progress-bar').html('<div style="width:' + porcentage_cost_subtotal + '%;" class="progress-bar progress-bar-success"></div>');
    }

    function load_return_total_data(return_total_array) {
        return_total = 0;
        $(return_total_array).each(function(i, value) {
            return_total += parseInt(value);
        });

        percentage_return_total = return_total * 100 / total;

        $('#return_total_sales').html(formatMoney(return_total));
        $('#return_total_progress-bar').html('<div style="width:' + percentage_return_total + '%;" class="progress-bar progress-bar-success"></div>');
    }

    function load_gross_profit_data() {
        gross_profit = subtotal - discount - cost_subtotal - (return_total * -1);
        percentage_return_total = gross_profit * 100 / total;

        $('#gross_profit_sales').html(formatMoney(gross_profit));
        $('#gross_profit_progress-bar').html('<div style="width:' + percentage_return_total + '%;" class="progress-bar progress-bar-success"></div>');
    }

    function load_profitability_margin_data(previous_subtotal, previous_discount, previous_cost_subtotal, previous_return_total, previous_tax_total) {
        variations = 0;

        previous_gross_profit = previous_subtotal + previous_tax_total - previous_discount - previous_cost_subtotal - previous_return_total;
        _previous_total = previous_gross_profit;

        profitability_margin = (gross_profit / subtotal) * 100;
        previous_profitability_margin = (previous_gross_profit / previous_subtotal) * 100;

        if (previous_profitability_margin > 0) {
            variations = formatDecimal(profitability_margin, 2) - formatDecimal(previous_profitability_margin, 2);
        }

        $('#profitability_margin_sales').html(formatDecimal(profitability_margin, 2) + '%');
        $('#profitability_margin_percentage').html(formatDecimal(profitability_margin, 2) + '% <i class="fa fa-bolt text-navy"></i>');
        $('#profitability_margin_progress-bar').html('<div style="width:' + formatDecimal(profitability_margin, 2) + '%;" class="progress-bar progress-bar-success"></div>');
        $('#previous_profitability_margin_sales').html(formatDecimal(previous_profitability_margin, 2) + '%');
        $('#variation_profit_margin').html(formatDecimal(variations, 2) + '%');

        if (variations < 0) {
            $('#variations_graph').html('Rentabilidad &nbsp; <i class="fa fa-arrow-down fa-1x text-danger"></i>');
        } else {
            $('#variations_graph').html('Rentabilidad &nbsp; <i class="fa fa-arrow-up fa-1x text-info"></i>');
        }
    }

    function search_sales_budget_data() {
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var month = $('#month').val();
        var branch_office = $('#branch_office').val();
        var previous_quarte_array = [];
        var previous_month = '';

        if (quarte_id > 1) {
            previous_quarte_array = _quartes[quarte_id - 1];
        }

        if (month > 1) {
            previous_month = month - 1;
        }

        $.ajax({
                url: '<?= admin_url("dashboard/search_sales_budget_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'month': month,
                    'quarte': quarte_array,
                    'quarte_id': quarte_id,
                    'branch_office': branch_office,
                    'previous_month': previous_month,
                    'previous_quarte': previous_quarte_array
                },
            })
            .done(function(data) {
                if (data.sale_budget_amount.length != 0) {
                    if (data.sale_budget_amount.amount == 0 || data.sale_budget_amount.amount == null) {
                        percentage_sale_budget = 0;
                    } else {
                        percentage_sale_budget = (_totalWithOutDiscount * 100) / data.sale_budget_amount.amount;
                    }

                    if (data.previous_sale_budget_data.amount == 0 || data.sale_budget_amount.amount == null) {
                        previous_percentage_sale_budget = 0;
                    } else {
                        previous_percentage_sale_budget = (_previous_total * 100) / data.previous_sale_budget_data.amount;
                    }

                    load_fulfillment(formatDecimal(percentage_sale_budget, 2));

                    varitions_sale_gauge = formatDecimal(percentage_sale_budget, 2) - formatDecimal(previous_percentage_sale_budget, 2);

                    $('#percentage_sale_budget').html(formatDecimal(percentage_sale_budget, 2) + '%');
                    $('#previous_percentage_sale_budget').html(formatDecimal(previous_percentage_sale_budget, 2) + '%');

                    if (varitions_sale_gauge > 0) {
                        $('#variations_graph_guage').html('Cumplimiento &nbsp; <i class="fa fa-arrow-up fa-1x text-success"></i>');
                    } else {
                        $('#variations_graph_guage').html('Cumplimiento &nbsp; <i class="fa fa-arrow-down fa-1x text-danger"></i>');
                    }
                }
            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function load_fulfillment(percentage_sale_budget) {
        maximun_graph = (percentage_sale_budget <= 100) ? 100 : percentage_sale_budget;

        c3.generate({
            bindto: '#gauge',
            data: {
                columns: [
                    ['Presupuesto', formatDecimal(percentage_sale_budget, 2)]
                ],
                type: 'gauge'
            },
            gauge: {
                label: {
                    format: function (value, ratio) {
                        return formatDecimal(value, 2);
                    }
                },
                max: maximun_graph
            },
            color: {
                pattern: ['#1484c6', '#bababa']

            },
            size: {
                height: 110
            }
        });
    }

    function search_invoices_types() {
        var month = $('#month').val();
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var branch_office = $('#branch_office').val();

        $.ajax({
                url: '<?= admin_url("dashboard/search_invoices_type_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'month': month,
                    'quarte': quarte_array,
                    'quarte_id': quarte_id,
                    'branch_office': branch_office
                },
            })
            .done(function(data) {
                if (data.invoices_total.amount == 0 || data.invoices_total.amount == null) {
                    $('#invoices_pos_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + 0 + '%;"></div>');
                    $('#invoices_detal_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + 0 + '%;"></div>');
                    $('#invoices_detal_fe_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + 0 + '%;"></div>');

                    $('#invoices_detal_percentage').html(formatDecimal(0, 2) + '%');
                    $('#invoices_detal_fe_percentage').html(formatDecimal(0, 2) + '%');
                    $('#invoices_pos_percentage').html(formatDecimal(0, 2) + '%');
                    $('#invoices_pos_fe_percentage').html(formatDecimal(0, 2) + '%');
                } else {
                    var percentage_invoices_pos = (data.invoices_pos.amount * 100) / data.invoices_total.amount;
                    var percentage_invoices_pos_fe = (data.invoices_pos_fe.amount * 100) / data.invoices_total.amount;
                    var percentage_invoices_detal = (data.invoices_detal.amount * 100) / data.invoices_total.amount;
                    var percentage_invoices_detal_fe = (data.invoices_detal_fe.amount * 100) / data.invoices_total.amount;

                    $('#invoices_pos_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + percentage_invoices_pos + '%;"></div>');
                    $('#invoices_pos_fe_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + percentage_invoices_pos_fe + '%;"></div>');
                    $('#invoices_detal_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + percentage_invoices_detal + '%;"></div>');
                    $('#invoices_detal_fe_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + percentage_invoices_detal_fe + '%;"></div>');

                    $('#invoices_detal_percentage').html(formatDecimal(percentage_invoices_detal, 2) + '%');
                    $('#invoices_detal_fe_percentage').html(formatDecimal(percentage_invoices_detal_fe, 2) + '%');
                    $('#invoices_pos_percentage').html(formatDecimal(percentage_invoices_pos, 2) + '%');
                    $('#invoices_pos_fe_percentage').html(formatDecimal(percentage_invoices_pos_fe, 2) + '%');
                }
            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function search_customers_data(limit = 15) {
        var month = $('#month').val();
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var branch_office = $('#branch_office').val();
        var customers_search_option = $('#customers_search_option').val();

        $.ajax({
                url: '<?= admin_url("dashboard/search_customers_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'month': month,
                    'quarte': quarte_array,
                    'branch_office': branch_office,
                    'customers_search_option': customers_search_option,
                    'limit': limit
                },
            })
            .done(function(data) {
                if (data.names_array.length == 0) {
                    $('#customers_data_container').html('<p class="default_message">No existen registros</p>');
                } else {
                    var height = (limit == 15) ? 70 : 140;

                    if (limit == 15) {
                        $('#show_more_customer_data').show();
                        $('#show_less_customer_data').hide();
                    } else {
                        $('#show_less_customer_data').show();
                        $('#show_more_customer_data').hide();
                    }

                    $('#customers_data_container').html('<div><canvas id="customers_barChart" height="' + height + '"></canvas></div>');
                    loadBarChartSalesCustomers(data.names_array, data.total_sales_array, data.number_transactions_array, customers_search_option);

                    var total_customers = '<?= $total_customers; ?>';
                    var percentage_total_customer = (data.total_customers * 100) / total_customers;


                    $('#total_customer_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + percentage_total_customer + '%;"></div>');
                    $('#total_customers').html(data.total_customers);
                }

            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function loadBarChartSalesCustomers(names_array, total_sales_array, number_transactions_array, customers_search_option) {
        if (customers_search_option == 1) {
            datasets = [{
                label: "Total ventas",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: total_sales_array
            }];

        } else {
            datasets = [{
                label: "N° transacciones",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: number_transactions_array
            }];
        }

        var ctx2 = document.getElementById("customers_barChart").getContext("2d");
        _customers_barChart = new Chart(ctx2, {
            type: 'horizontalBar',
            data: {
                labels: names_array,
                datasets: datasets
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value, index, values) {
                                return (customers_search_option == 1) ? formatMoney(value) : formatDecimal(value);
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            return (customers_search_option == 1) ? formatMoney(tooltipItem.xLabel) : tooltipItem.xLabel;
                        },
                    },
                    backgroundColor: '#f7f7f7',
                    bodyFontColor: '#000',
                }
            }
        });
    }

    function search_products_data(limit = 15) {
        var month = $('#month').val();
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var branch_office = $('#branch_office').val();
        var products_search_option = $('#products_search_option').val();
        let groupBy = $('input[name="groupBy"]:checked').val();

        console.log(groupBy);



        $.ajax({
                url: '<?= admin_url("dashboard/search_products_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'month': month,
                    'quarte': quarte_array,
                    'branch_office': branch_office,
                    'products_search_option': products_search_option,
                    'limit': limit,
                    'groupBy': groupBy
                },
            })
            .done(function(data) {
                if (data.names_array.length == 0) {
                    $('#products_data_container').html('<p class="default_message">No existen registros</p>');
                } else {
                    var height = (limit == 15) ? 70 : 140;

                    if (limit == 15) {
                        $('#show_more_products_data').show();
                        $('#show_less_products_data').hide();
                    } else {
                        $('#show_less_products_data').show();
                        $('#show_more_products_data').hide();
                    }

                    $('#products_data_container').html('<div><canvas id="products_barChart" height="' + height + '"></canvas></div>');
                    loadBarChartSalesProducts(data.names_array, data.total_sales_array, data.number_transactions_array, products_search_option);

                    var total_products = '<?= $total_products; ?>';
                    var percentage_total_products = (total_products == 0) ? (data.total_products * 100) / total_products : 0;

                    $('#total_products_progress-bar').html('<div class="progress-bar progress-bar-success" style="width: ' + percentage_total_products + '%;"></div>');
                    $('#total_products').html(data.total_products);
                }

            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function loadBarChartSalesProducts(names_array, total_sales_array, number_transactions_array, products_search_option) {
        if (products_search_option == 1) {
            datasets = [{
                label: "Total ventas",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: total_sales_array
            }];
        } else {
            datasets = [{
                label: "N° transacciones",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: number_transactions_array
            }];
        }

        var ctx3 = document.getElementById("products_barChart").getContext("2d");
        _products_barChart = new Chart(ctx3, {
            type: 'horizontalBar',
            data: {
                labels: names_array,
                datasets: datasets
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value, index, values) {
                                return (products_search_option == 1) ? formatMoney(value) : formatDecimal(value);
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            return (products_search_option == 1) ? formatMoney(tooltipItem.xLabel) : tooltipItem.xLabel;
                        },
                    },
                    backgroundColor: '#FFF',
                    bodyFontColor: '#000',
                }
            }
        });
    }

    function search_sellers_data(limit = 15) {
        var month = $('#month').val();
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var branch_office = $('#branch_office').val();
        var sellers_search_option = $('#sellers_search_option').val();

        $.ajax({
                url: '<?= admin_url("dashboard/search_sellers_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'month': month,
                    'quarte': quarte_array,
                    'branch_office': branch_office,
                    'sellers_search_option': sellers_search_option,
                    'limit': limit
                },
            })
            .done(function(data) {
                if (data.names_array.length == 0) {
                    $('#sellers_data_container').html('<p class="default_message">No existen registros</p>');
                } else {
                    var height = (limit == 15) ? 70 : 140;

                    if (limit == 15) {
                        $('#show_more_seller_data').show();
                        $('#show_less_seller_data').hide();
                    } else {
                        $('#show_less_seller_data').show();
                        $('#show_more_seller_data').hide();
                    }

                    $('#sellers_data_container').html('<div><canvas id="sellers_barChart" height="' + height + '"></canvas></div>');
                    loadBarChartSalesSellers(data.names_array, data.total_sales_array, data.number_transactions_array, sellers_search_option);
                }

            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function loadBarChartSalesSellers(names_array, total_sales_array, number_transactions_array, sellers_search_option) {
        if (sellers_search_option == 1) {
            datasets = [{
                label: "Total ventas",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: total_sales_array
            }];
        } else {
            datasets = [{
                label: "N° transacciones",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: number_transactions_array
            }];
        }

        let ctx4 = document.getElementById("sellers_barChart").getContext("2d");
        _sellers_barChart = new Chart(ctx4, {
            type: 'horizontalBar',
            data: {
                labels: names_array,
                datasets: datasets
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value, index, values) {
                                return (sellers_search_option == 1) ? formatMoney(value) : formatDecimal(value);
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            console.log(tooltipItem);
                            return (sellers_search_option == 1) ? formatMoney(tooltipItem.xLabel) : tooltipItem.xLabel;
                        },
                    },
                    backgroundColor: '#FFF',
                    bodyFontColor: '#000',
                }
            }
        });
    }

    function search_brands_data(limit = 15) {
        var month = $('#month').val();
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var branch_office = $('#branch_office').val();
        var brands_search_option = $('#brands_search_option').val();

        $.ajax({
                url: '<?= admin_url("dashboard/search_brands_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'month': month,
                    'quarte': quarte_array,
                    'branch_office': branch_office,
                    'brands_search_option': brands_search_option,
                    'limit': limit
                },
            })
            .done(function(data) {
                if (data.names_array.length == 0) {
                    $('#brands_data_container').html('<p class="default_message">No existen registros</p>');
                } else {
                    var height = (limit == 15) ? 70 : 140;

                    if (limit == 15) {
                        $('#show_more_brands_data').show();
                        $('#show_less_brands_data').hide();
                    } else {
                        $('#show_less_brands_data').show();
                        $('#show_more_brands_data').hide();
                    }

                    $('#brands_data_container').html('<div><canvas id="brands_barChart" height="' + height + '"></canvas></div>');
                    load_brands_barChart(data.names_array, data.total_sales_array, data.number_transactions_array, brands_search_option);
                }

            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function load_brands_barChart(names_array, total_sales_array, number_transactions_array, brands_search_option) {
        if (brands_search_option == 1) {
            datasets = [{
                label: "Total ventas",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: total_sales_array
            }];
        } else {
            datasets = [{
                label: "N° transacciones",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: number_transactions_array
            }];
        }

        var ctx2 = document.getElementById("brands_barChart").getContext("2d");
        _brands_barChart = new Chart(ctx2, {
            type: 'horizontalBar',
            data: {
                labels: names_array,
                datasets: datasets
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value, index, values) {
                                return (brands_search_option == 1) ? formatMoney(value) : formatDecimal(value);
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            console.log(tooltipItem);
                            return (brands_search_option == 1) ? formatMoney(tooltipItem.xLabel) : tooltipItem.xLabel;
                        },
                    },
                    backgroundColor: '#FFF',
                    bodyFontColor: '#000',
                }
            }
        });
    }

    function search_categories_data(limit = 15) {
        var month = $('#month').val();
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var branch_office = $('#branch_office').val();
        var categories_search_option = $('#categories_search_option').val();

        $.ajax({
                url: '<?= admin_url("dashboard/search_categories_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'quarte': quarte_array,
                    'month': month,
                    'branch_office': branch_office,
                    'categories_search_option': categories_search_option,
                    'limit': limit
                },
            })
            .done(function(data) {
                var height = (limit == 15) ? 70 : 140;

                if (limit == 15) {
                    $('#show_more_categories_data').show();
                    $('#show_less_categories_data').hide();
                } else {
                    $('#show_less_categories_data').show();
                    $('#show_more_categories_data').hide();
                }

                if (data.names_array.length == 0) {
                    $('#categories_data_container').html('<p class="default_message">No existen registros</p>');
                } else {
                    $('#categories_data_container').html('<div><canvas id="categories_barChart" height="' + height + '"></canvas></div>');
                    load_categories_barChart(data.names_array, data.total_sales_array, data.number_transactions_array, categories_search_option);
                }

            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function load_categories_barChart(names_array, total_sales_array, number_transactions_array, categories_search_option) {
        if (categories_search_option == 1) {
            datasets = [{
                label: "Total ventas",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: total_sales_array
            }];
        } else {
            datasets = [{
                label: "N° transacciones",
                backgroundColor: 'rgba(20, 132, 198, 1)',
                pointBorderColor: "#fff",
                data: number_transactions_array
            }];
        }

        var ctx2 = document.getElementById("categories_barChart").getContext("2d");
        _categories_barChart = new Chart(ctx2, {
            type: 'horizontalBar',
            data: {
                labels: names_array,
                datasets: datasets
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value, index, values) {
                                return (categories_search_option == 1) ? formatMoney(value) : formatDecimal(value);
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            console.log(tooltipItem);
                            return (categories_search_option == 1) ? formatMoney(tooltipItem.xLabel) : tooltipItem.xLabel;
                        },
                    },
                    backgroundColor: '#FFF',
                    bodyFontColor: '#000',
                }
            }
        });
    }

    function search_taxes_data() {
        var month = $('#month').val();
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var branch_office = $('#branch_office').val();

        $.ajax({
                url: '<?= admin_url("dashboard/search_taxes_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'month': month,
                    'quarte': quarte_array,
                    'branch_office': branch_office
                },
            })
            .done(function(data) {
                if (data.names_array.length == 0) {
                    $('#taxes_data_container').html('<p class="default_message">No existen registros</p>');
                } else {
                    $('#taxes_data_container').html('<div><canvas id="taxes_doughnutChart" height="140"></canvas></div>');
                    loadDoughnutTaxes(data.names_array, data.total_sales_array);
                }

            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function loadDoughnutTaxes(names_array, total_sales_array) {
        datasets = [{
            data: total_sales_array,
            backgroundColor: ["rgba(20, 132, 198, 1)", "rgba(20, 132, 198, 0.3)", "#dedede", "#6C5980", "#A097B3", "#A3BFBB", "#CED9C3", "#b5b8cf"],
            label: 'asdf'
        }];


        var ctx4 = document.getElementById("taxes_doughnutChart").getContext("2d");
        _doughnutchart = new Chart(ctx4, {
            type: 'doughnut',
            data: {
                labels: names_array,
                datasets: datasets
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            return formatMoney(data.datasets[0].data[tooltipItem.index])
                        },
                    },
                    backgroundColor: '#FFF',
                    bodyFontColor: '#000',
                }
            }
        });
    }

    function search_payment_means_data() {
        var month = $('#month').val();
        var quarte_id = $('#quarte').val();
        var quarte_array = _quartes[quarte_id];
        var branch_office = $('#branch_office').val();

        $.ajax({
                url: '<?= admin_url("dashboard/search_payment_means_data"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                    'quarte': quarte_array,
                    'month': month,
                    'branch_office': branch_office,
                },
            })
            .done(function(data) {
                if (data.names_array.length == 0) {
                    $('#payment_means_data_container').html('<p class="default_message">No existen registros</p>');
                } else {
                    $('#payment_means_data_container').html('<div><canvas id="payment_means_doughnutChart" height="140"></canvas></div>');
                    load_payment_means_barChart(data.names_array, data.total_sales_array);
                }
            })
            .fail(function(data) {
                console.log(data.responseText);
            });
    }

    function load_payment_means_barChart(names_array, total_sales_array) {
        datasets = [{
            data: total_sales_array,
            backgroundColor: ["rgba(20, 132, 198, 1)", "rgba(20, 132, 198, 0.3)", "#dedede", "#6C5980", "#A097B3", "#A3BFBB", "#CED9C3", "#b5b8cf"]
        }];


        var ctx4 = document.getElementById("payment_means_doughnutChart").getContext("2d");
        new Chart(ctx4, {
            type: 'doughnut',
            data: {
                labels: names_array,
                datasets: datasets
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            return formatMoney(data.datasets[0].data[tooltipItem.index])
                        },
                    },
                    backgroundColor: '#FFF',
                    bodyFontColor: '#000',
                }
            }
        });
    }

    function equalize_height_containers() {
        var heights = $(".test").map(function() {
                return $(this).height();
            }).get(),
            maxHeight = Math.max.apply(null, heights);

        $(".test").height(maxHeight);
    }
</script>