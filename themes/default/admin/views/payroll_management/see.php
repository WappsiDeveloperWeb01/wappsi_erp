<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="see_payroll_form">
                                <h1><?= "Detalle de nómina" ?></h1>
                                <section>
                                    <div class="table-responsive">
                                        <table id="employee_payroll_table" class="table table-hover pointer" style="width: 100%;">
                                            <thead>
                                                <tr class="primary">
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th><?= lang("branch"); ?></th>
                                                    <th><?= lang("payroll_management_employee"); ?></th>
                                                    <th><?= lang("payroll_management_earnings"); ?></th>
                                                    <th><?= lang("payroll_management_deductions"); ?></th>
                                                    <th><?= lang("payroll_management_total_payment"); ?></th>
                                                    <th><?= lang("payroll_management_total_provisions"); ?></th>
                                                    <th><?= lang("actions"); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="5"></th>
                                                    <th class="total_earned text-right"></th>
                                                    <th class="total_deductions text-right"></th>
                                                    <th class="total text-right"></th>
                                                    <th class="total_provisions text-right"></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </section>
                                <?php /* if ($hideShowParafiscalProvisions == TRUE) { */ ?>
                                    <!-- <h1><?= "Seguridad social y Parafiscales" ?></h1>
                                    <section>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <h3>Cálculos seguridad social</h3>
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Concepto</th>
                                                            <th>Base</th>
                                                            <th>Porcentaje</th>
                                                            <th>Valor</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($social_security_parafiscal as $key => $ssp) { ?>
                                                            <?php if (strpos($ssp->description, 'Pensión') !== FALSE) { ?>
                                                                <tr>
                                                                    <td><?= $ssp->description; ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                                    <td class="text-right"  ><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <?php foreach ($social_security_parafiscal as $key => $ssp) { ?>
                                                            <?php if (strpos($ssp->description, 'CLASE') !== FALSE) { ?>
                                                                <tr>
                                                                    <td><?= $ssp->description; ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                                    <td class="text-right"  ><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                                <hr>
                                                <h3>Cálculos parafiscales</h3>
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Concepto</th>
                                                            <th>Base</th>
                                                            <th>Porcentaje</th>
                                                            <th>Valor</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($social_security_parafiscal as $key => $ssp) { ?>
                                                            <?php if (strpos($ssp->description, 'Caja de compensación') !== FALSE) { ?>
                                                                <tr>
                                                                    <td><?= $ssp->description; ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                                    <td class="text-right"  ><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                                </tr>
                                                            <?php } ?>

                                                            <?php if (strpos($ssp->description, 'SENA') !== FALSE) { ?>
                                                                <tr>
                                                                    <td><?= $ssp->description; ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                                    <td class="text-right"  ><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                                </tr>
                                                            <?php } ?>

                                                            <?php if (strpos($ssp->description, 'ICBF') !== FALSE) { ?>
                                                                <tr>
                                                                    <td><?= $ssp->description; ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->base_salary, 2, ",", "."); ?></td>
                                                                    <td class="text-right"><?= number_format($ssp->percentage, 2, ",", "."); ?></td>
                                                                    <td class="text-right"  ><?= number_format($ssp->amount, 2, ",", "."); ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </section>

                                    <h1><?= "Provisiones prestaciones sociales" ?></h1>
                                    <section>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <h3>Provisiones</h3>
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Concepto</th>
                                                            <th>Base</th>
                                                            <th>Porcentaje</th>
                                                            <th>Valor</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($provisions as $key => $provisions) { ?>
                                                            <tr>
                                                                <td><?= $provisions->name; ?></td>
                                                                <td class="text-right"><?= number_format($provisions->base_salary, 2, ",", "."); ?></td>
                                                                <td class="text-right"><?= number_format($provisions->percentage, 2, ",", "."); ?></td>
                                                                <td class="text-right"  ><?= number_format($provisions->amount, 2, ",", "."); ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </section> -->
                                <?php /* } */ ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        _number_overtime_fields = 1;

        $("#see_payroll_form").show().steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            bodyTag: "section",
            transitionEffect: "fade",
            enableAllSteps: true,
            enablePagination: false
        });

        load_datatable();
        existing_configuration_data_changes();

        $(document).on('click', '.see_employee_concepts td:not(:last-child)', function() {
            var employee_id = $(this).parent('.see_employee_concepts').attr('id');
            var contract_id = $(this).parent('.see_employee_concepts').data('contract_id');
            $('#myModal').modal({remote: '<?= admin_url('payroll_management/see_employee_concepts/'.$payroll_id."/") ?>'+ employee_id +'/'+ contract_id});
            $('#myModal').modal('show');
        });

        $(document).on('click', '#add_overtime_field_button', function() { add_overtime_field_button(); });
        $(document).on('click', '#delete_overtime_field_button', function() { delete_overtime_field_button(); });
        $(document).on('change', '.hours_quantity', function() {
            var index = $(this).data('index');
            if ($(this).val() != '') {
                $('#overtime_end_date'+index).attr('disabled', true);
            } else {
                $('#overtime_end_date'+index).attr('disabled', false);
            }
        });
        $(document).on('click', '.option_approve_payroll', function() { confirm_approval_current_payroll(<?= $payroll_id ?>); });
        $(document).on('click', '.approve_payroll', function() {
            window.location.href = site.base_url + 'payroll_management/approve/'+ $('#payroll_id').val();
        });
        $(document).on('click', '.option_delete_payroll', function() { confirm_deletion_payroll($(this).data('payroll_id')) });
        $(document).on('click', '.option_update_payroll a', function(event) { confirm_update_payroll(event, $(this)) });
        $(document).on('click', '.option_remove_employee_payroll a', function(event) { confirm_employee_deletion(event, $(this)) });
        $(document).on('click', '.optionToPayPayroll a', function(event) { confirmToPayPayroll(event, $(this)); });
        $(document).on('click', '.option_calculationParafiscalAndProvisions a', function(event) { confirm_calculationParafiscalAndProvisions(event, $(this)) });
    });

    function load_datatable()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#employee_payroll_table').dataTable({
            "aaSorting": [[4, "asc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            dom: '<"col-sm-7 containerBtn"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6">',
            'sAjaxSource': '<?= admin_url('payroll_management/get_payroll_employees_datatables/'.$payroll_id) ?>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.dataset.contract_id = aData[2];
                nRow.className = "see_employee_concepts";
                return nRow;
            },
            drawCallback: function(settings) {
                option_update_payroll = '<li class="optionSpreadsheet"><a href="<?= admin_url('payroll_management/payroll_management/see/'.$payroll_id) ?>" target="_blank"><i class="fa fa-file-text-o"></i> <?= lang('spreadsheet') ?></a></li>';

                <?php if ($payroll_data->status == IN_PREPARATION) { ?>
                    <?php if (($this->Admin || $this->Owner)) { ?>
                        $('.actionsButtonContainer').html('<div class="dropdown pull-right">'+
                            '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="left" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                            '<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu">'+
                                '<li class="option_update_payroll"><a href="<?= admin_url('payroll_management/update/'.$payroll_id) ?>"><i class="fa fa-refresh"></i> <?= lang('update') ?></a></li>'+
                                '<li class="option_approve_payroll"><a><i class="fa fa-thumbs-o-up"></i> <?= lang('approve') ?></a></li>'+
                                '<li class="option_delete_payroll" data-payroll_id="<?= $payroll_id ?>"><a><i class="fa fa-trash" aria-hidden="true"></i> <?= lang('delete') ?></a></li>'+
                                option_update_payroll +
                            '</ul>'+
                        '</div>');
                    <?php } else { ?>
                        <?php if (($GP['payroll_management-update'] || $GP['payroll_management-approve'])) { ?>
                            var option_update_payroll = '';
                            var option_approve_payroll_li = '';
                            <?php if ($GP['payroll_management-update']) { ?>
                                option_update_payroll = '<li><a href="<?= admin_url('payroll_management/update/'.$payroll_id) ?>"><i class="fa fa-refresh"></i> <?= lang('update') ?></a></li>';
                            <?php } ?>
                            <?php if ($GP['payroll_management-approve']) { ?>
                                option_approve_payroll_li = '<li class="option_approve_payroll"><a><i class="fa fa-thumbs-o-up"></i> <?= lang('approve') ?></a></li>';
                            <?php } ?>

                            $('.actions_button_container').html('<div class="dropdown pull-right">'+
                                '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="left" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                                '<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu">'+
                                    option_update_payroll +
                                    option_approve_payroll_li +
                                    option_update_payroll +
                                '</ul>'+
                            '</div>');
                        <?php } ?>
                    <?php } ?>
                <?php } else if ($payroll_data->status == APPROVED) { ?>
                    $('.actions_button_container').html('<div class="dropdown pull-right">'+
                        '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="left" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                        '<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu">'+
                            '<li class="optionToPayPayroll"><a href="<?= admin_url('payroll_management/to_pay/'.$payroll_id) ?>"><i class="fa fa-money"></i> <?= lang('to_pay') ?></a></li>'+
                            option_update_payroll +
                        '</ul>'+
                    '</div>');
                <?php } ?>
            },
            fnFooterCallback: function (nRow, aData, iStart, iEnd, aiDisplay) {
                var total_earned = total_deductions = total = total_provisions = 0;

                $.each(aData, function(index, data) {
                    total_earned += parseFloat(data[5]);
                    total_deductions += parseFloat(data[6]);
                    total += parseFloat(data[7]);
                    total_provisions += parseFloat(data[8]);
                });

                $('.total_earned').html(currencyFormat(total_earned, 2));
                $('.total_deductions').html(currencyFormat(total_deductions, 2));
                $('.total').html(currencyFormat(total, 2));
                $('.total_provisions').html(currencyFormat(total_provisions, 2));
            },
            "aoColumns": [
                { visible: false},
                { visible: false},
                { visible: false},
                { className: 'text-capitalize' },
                { className: 'text-capitalize' },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                { className: 'text-right', render: currencyFormat },
                {
                    bSortable: false,
                    render: function(data, type, row) {
                        var actions = $(data);

                        <?php if ($payroll_data->status == APPROVED || $payroll_data->status == PAID) { ?>
                            <?php if ($this->Owner || $this->Admin) { ?>
                                actions.find('.dropdown-menu').find('.option_add_payroll_concept a').remove();
                            <?php } else { ?>
                                <?php if (!$GP['payroll_management-add_novelty']) { ?>
                                    actions.find('.dropdown-menu').find('.option_add_payroll_concept a').remove();
                                <?php } ?>
                            <?php } ?>

                            actions.find('.dropdown-menu').find('.option_remove_employee_payroll a').remove();
                        <?php } ?>

                        <?php if ($payroll_data->status != PAID) { ?>
                            actions.find('.dropdown-menu').find('.option_pay_slip a').remove();
                        <?php } ?>

                        <?php /* if($hideShowParafiscalProvisions == FALSE) { */ ?>
                            // actions.find('.dropdown-menu').find('.option_calculationParafiscalAndProvisions a').remove();
                        <?php /* } */ ?>

                        var data = actions.prop('outerHTML');
                        return data;
                    }
                }
            ]
        });
    }

    function add_overtime_field_button()
    {
        var overtime_field = '<tr>'+
                                '<td>'+
                                    '<div class="form-group">'+
                                        '<input class="form-control" name="overtime_start_date['+_number_overtime_fields+']" id="overtime_start_date'+_number_overtime_fields+'"  type="text" required="1" />'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="form-group">'+
                                        '<input class="form-control" name="hours['+_number_overtime_fields+']" id="hours'+_number_overtime_fields+'" type="number" min="0" value="0" required="1" />'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+
                                    '<div class="form-group">'+
                                        '<input class="form-control" name="minutes['+_number_overtime_fields+']" id="minutes'+_number_overtime_fields+'" type="number" data-index="'+_number_overtime_fields+'" min="0" max="59" value="0" required="1" />'+
                                    '</div>'+
                                '</td>'+
                            '</tr>';
        $('#add_concept_table').append(overtime_field);

        $('#overtime_start_date'+_number_overtime_fields).datetimepicker({
            autoclose: true,
            format: 'yyyy-mm-dd HH:ii',
            language: 'sma',
            startDate : '<?= $payroll_data->start_date; ?>',
            endDate: $('#max_date').val(),
            minuteStep: 5
        }).on('changeDate', function(e) {
            var index = (_number_overtime_fields-1);
            $('#overtime_end_date'+index).datetimepicker('setStartDate', $('#overtime_start_date'+index).val());
        });

        _number_overtime_fields++;
    }

    function delete_overtime_field_button()
    {
        if ($('#add_concept_table tbody tr').length > 1) {
            $('#add_concept_table tbody tr:last-child').remove();
            _number_overtime_fields--;
        }
    }

    function confirm_approval_current_payroll(payroll_id)
    {
        swal({
            title: "¿Seguro de Aprobar la Nómina?",
            text: "Después de aprobar no se podrá modificar ningún dato",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Aprobar!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                <?php if (!empty($payroll_data)) { ?>
                    <?php if ($this->Payroll_settings->payment_frequency == BIWEEKLY) { ?>
                        <?php if ($payroll_data->number == 2) { ?>
                            second_confirm_approval_current_payroll(payroll_id);
                        <?php } else { ?>
                            approve_current_payroll(payroll_id);
                        <?php } ?>
                    <?php } else { ?>
                        approve_current_payroll(payroll_id);
                    <?php } ?>
                <?php } ?>
            }
        });
    }

    function second_confirm_approval_current_payroll(payroll_id)
    {
        swal({
            title: "¡Advertencia!",
            text: "Este proceso consolidará los datos de nómina electrónica y no podrá revertirse. ¿Aún desea continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Continuar!",
            closeOnConfirm: false
        }, function () {
            approve_current_payroll(payroll_id);
        });
    }

    function approve_current_payroll(payroll_id)
    {
        swal({
            title: 'Aprobando nómina',
            text: 'Por favor, espere un momento',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });
        window.location.href = site.base_url + 'payroll_management/approve/'+ payroll_id;
    }

    function confirm_deletion_payroll(payroll_id)
    {
        swal({
            title: "¿Seguro de eliminar la Nómina?",
            text: "Se perderá todo el proceso realizado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, eliminarlo!",
            closeOnConfirm: false
        }, function () {
            delete_current_payroll(payroll_id);
        });
    }

    function delete_current_payroll(payroll_id)
    {
        swal({
            title: 'Elimando nómina',
            text: 'Por favor, espere un momento',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });

        window.location.href = site.base_url + 'payroll_management/delete/'+ payroll_id;
    }

    function confirm_update_payroll(event, element)
    {
        event.preventDefault();
        swal({
            title: "¿Seguro de actualizar la Nómina?",
            text: "Se recalculará los valores de la nómina",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, actualizar!",
            closeOnConfirm: false
        }, function () {
            update_current_payroll(element);
        });
    }

    function update_current_payroll(element)
    {
        swal({
            title: 'Actualizando nómina',
            text: 'Por favor, espere un momento',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });

        window.location.href = element.attr('href');
    }

    function existing_configuration_data_changes()
    {
        <?php if ($this->session->userdata('existing_configuration_data_changes') == TRUE && $payroll_data->status == IN_PREPARATION) { ?>
            swal({
                title: 'Mensaje de Advertencia',
                text: 'Los porcentajes para el cálculo de los parafiscales han sido cambiados. Es necesario Actualizar para reflejar el cambio en la nómina',
                type: 'warning',
                confirmButtonText: "¡Entendido!",
                closeOnConfirm: true
            });
        <?php } ?>

        <?php if ($this->session->userdata('employee_settlement_date_change') == TRUE && $payroll_data->status == IN_PREPARATION) { ?>
            swal({
                title: 'Mensaje de Advertencia',
                text: 'La fecha final de liquidación de un empleado ha sido cambiado. Es necesario Actualizar para reflejar el cambio en la nómina',
                type: 'warning',
                confirmButtonText: "¡Entendido!",
                closeOnConfirm: true
            });
        <?php } ?>
    }

    function confirm_employee_deletion(event, element)
    {
        event.preventDefault();
        swal({
            title: "¿Seguro de Eliminar al Empleado?",
            text: "No se tendrá en cuenta a este empleado en la nómina",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Eliminar!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                swal({
                    title: 'Eliminando empleado de la Nómina',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false,
                });
                window.location.href = element.attr('href');
            }
        });
    }

    function confirm_calculationParafiscalAndProvisions(event, element)
    {
        event.preventDefault();
        swal({
            title: "¿Desea recalcular parafiscales y provisiones para este empleado?",
            text: "Se realizará el calculo en base a los devengados",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                swal({
                    title: 'Calculando parafiscales y provisiones para el empleado',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false,
                });
                window.location.href = element.attr('href');
            }
        });
    }

    function confirmToPayPayroll(event, element)
    {
        event.preventDefault();
        swal({
            title: "¿Seguro de pagar La nómina?",
            text: "Este proceso cambiará el estado de la nómina",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Pagar!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                swal({
                    title: 'Pagando la Nómina',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false,
                });
                window.location.href = element.attr('href');
            }
        });
    }
</script>