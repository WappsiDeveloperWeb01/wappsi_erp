<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <?= admin_form_open_multipart("sales/change_payment_method/".$inv->id.'', ['id' => 'change_payment_method']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
                <span class="modal-title" style="font-size: 150%;"><?=lang('change_payment_method');?></span>
                <h3><?= lang('sale_no_ref')." : ".$inv->reference_no ?></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php if ($payments): ?>
                        <?php $cnt = 1; ?>
                        <?php foreach ($payments as $pmnt): ?>
                            <?php if ($pmnt->paid_by != 'retencion' && $pmnt->amount > 0): ?>
                                <div class="form-group col-sm-12">
                                    <label class="pm_msg"><?= ucfirst(mb_strtolower(lang('payment_method')." N°.".$cnt)." por valor de ".$this->sma->formatMoney($pmnt->amount)) ?></label>
                                    <select name="paid_by[]" class="form-control paid_by" <?= $pmnt->multi_payment == 0 ? "" : "disabled" ?>>
                                        <?= $this->sma->paid_opts($pmnt->paid_by); ?>
                                    </select>
                                    <em class="text-danger deposits_affected_error" style="display: none;">El valor afectaría dos o más anticipos, no se puede editar a esta forma de pago</em>
                                    <input type="hidden" name="payment_id[]" value="<?= $pmnt->id ?>">
                                    <input type="hidden" name="payment_amount[]" class="pmnt_amount" value="<?= $pmnt->amount ?>">
                                    <?= $pmnt->multi_payment == 0 ? "" : "<em class='text-danger'>".lang('cannot_edit_payment_method_multi_payment')."</em>" ?>
                                    <div class="deposit_message">
                                        
                                    </div>
                                </div>
                                <?php $cnt++; ?>
                            <?php elseif ($pmnt->paid_by == 'retencion'): ?>
                                <div class="form-group col-sm-12">
                                    <label><?= sprintf(lang('invoice_with_retention'), $this->sma->formatMoney($pmnt->amount)) ?></label>
                                </div>
                            <?php endif ?>
                            <?php 
                            $p_comm_amount = 0;
                            $p_retefuente_amount = 0;
                            $p_reteiva_amount = 0;
                            $p_reteica_amount = 0;
                            if ($pmnt->pm_commision_value != NULL) {
                                if (strpos($pmnt->pm_commision_value, '%') !== false) {
                                    $p_comm_perc = (Double)(str_replace("%", "", $pmnt->pm_commision_value));
                                    $p_comm_base = $inv->total * ($pmnt->amount / $inv->grand_total);
                                    $p_comm_amount = $this->sma->formatDecimal($p_comm_base * ($p_comm_perc / 100));
                                } else {
                                    $p_comm_base = $inv->total * ($pmnt->amount / $inv->grand_total);
                                    $p_comm_amount = $pmnt->pm_commision_value;
                                }
                            }
                            if ($pmnt->pm_retefuente_value != NULL) {
                                if (strpos($pmnt->pm_retefuente_value, '%') !== false) {
                                    $p_retefuente_perc = (Double)(str_replace("%", "", $pmnt->pm_retefuente_value));
                                    $p_retefuente_base = $inv->total * ($pmnt->amount / $inv->grand_total);
                                    $p_retefuente_amount = $this->sma->formatDecimal($p_retefuente_base * ($p_retefuente_perc / 100));
                                } else {
                                    $p_retefuente_base = $inv->total * ($pmnt->amount / $inv->grand_total);
                                    $p_retefuente_amount = $pmnt->pm_retefuente_value;
                                }
                            }
                            if ($pmnt->pm_reteiva_value != NULL) {
                                if (strpos($pmnt->pm_reteiva_value, '%') !== false) {
                                    $p_reteiva_perc = (Double)(str_replace("%", "", $pmnt->pm_reteiva_value));
                                    $p_reteiva_base = $inv->total_tax * ($pmnt->amount / $inv->grand_total);
                                    $p_reteiva_amount = $this->sma->formatDecimal($p_reteiva_base * ($p_reteiva_perc / 100));
                                } else {
                                    $p_reteiva_base = $inv->total_tax * ($pmnt->amount / $inv->grand_total);
                                    $p_reteiva_amount = $pmnt->pm_reteiva_value;
                                }
                            }
                            if ($pmnt->pm_reteica_value != NULL) {
                                if (strpos($pmnt->pm_reteica_value, '%') !== false) {
                                    $p_reteica_perc = (Double)(str_replace("%", "", $pmnt->pm_reteica_value));
                                    $p_reteica_base = $inv->total * ($pmnt->amount / $inv->grand_total);
                                    $p_reteica_amount = $this->sma->formatDecimal($p_reteica_base * ($p_reteica_perc / 100));
                                } else {
                                    $p_reteica_base = $inv->total * ($pmnt->amount / $inv->grand_total);
                                    $p_reteica_amount = $pmnt->pm_reteica_value;
                                }
                            }
                            ?>
                            <?php 
                            $retcom_display = 'style="display:none;"';
                            $setted_retcom = 0;
                            if ($p_comm_amount > 0 || $p_retefuente_amount > 0 || $p_reteiva_amount > 0 || $p_reteica_amount > 0) {
                                $retcom_display = '';
                                $setted_retcom = 1;
                            }
                            ?>
                                <div class="retcom_div" <?= $retcom_display ?> >
                                    <input type="hidden" name="setted_retcom[<?= $pmnt->id ?>]" class="setted_retcom" value="<?= $setted_retcom ?>">
                                    <div class="col-sm-6">
                                        <label>Valor o porcentaje de comisión</label>
                                        <input type="text" name="pm_commision_value_info[<?= $pmnt->id ?>]" class="form-control pm_commision_value_info" value="<?= !empty($pmnt->pm_commision_value) ? $pmnt->pm_commision_value : '' ?>" readonly>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Valor calculado de comisión</label>
                                        <input type="text" name="pm_commision_value[<?= $pmnt->id ?>]" class="form-control pm_commision_value" value="<?= $p_comm_amount > 0 ? $p_comm_amount : '' ?>" <?= $p_comm_amount > 0 ? 'data-originalamount="'.$p_comm_amount.'"' : '' ?>>
                                        <input type="hidden" name="pm_commision_changed[<?= $pmnt->id ?>]" class="pm_commision_changed">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Valor o porcentaje de retefuente</label>
                                        <input type="text" name="pm_retefuente_value_info[<?= $pmnt->id ?>]" class="form-control pm_retefuente_value_info" value="<?= !empty($pmnt->pm_retefuente_value) ? $pmnt->pm_retefuente_value : '' ?>" readonly>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Valor calculado de retefuente</label>
                                        <input type="text" name="pm_retefuente_value[<?= $pmnt->id ?>]" class="form-control pm_retefuente_value" value="<?= $p_retefuente_amount > 0 ? $p_retefuente_amount : '' ?>"  <?= $p_retefuente_amount > 0 ? 'data-originalamount="'.$p_retefuente_amount.'"' : '' ?>>
                                        <input type="hidden" name="pm_retefuente_changed[<?= $pmnt->id ?>]" class="pm_retefuente_changed">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Valor o porcentaje de reteiva</label>
                                        <input type="text" name="pm_reteiva_value_info[<?= $pmnt->id ?>]" class="form-control pm_reteiva_value_info" value="<?= !empty($pmnt->pm_reteiva_value) ? $pmnt->pm_reteiva_value : '' ?>" readonly>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Valor calculado de reteiva</label>
                                        <input type="text" name="pm_reteiva_value[<?= $pmnt->id ?>]" class="form-control pm_reteiva_value" value="<?= $p_reteiva_amount > 0 ?  $p_reteiva_amount : '' ?>"  <?= $p_reteiva_amount > 0 ? 'data-originalamount="'.$p_reteiva_amount.'"' : '' ?>>
                                        <input type="hidden" name="pm_reteiva_changed[<?= $pmnt->id ?>]" class="pm_reteiva_changed">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Valor o porcentaje de reteica</label>
                                        <input type="text" name="pm_reteica_value_info[<?= $pmnt->id ?>]" class="form-control pm_reteica_value_info" value="<?= !empty($pmnt->pm_reteica_value) ? $pmnt->pm_reteica_value : '' ?>" readonly>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Valor calculado de reteica</label>
                                        <input type="text" name="pm_reteica_value[<?= $pmnt->id ?>]" class="form-control pm_reteica_value" value="<?= $p_reteica_amount > 0 ? $p_reteica_amount : '' ?>"  <?= $p_reteica_amount > 0 ? 'data-originalamount="'.$p_reteica_amount.'"' : '' ?>>
                                        <input type="hidden" name="pm_reteica_changed[<?= $pmnt->id ?>]" class="pm_reteica_changed">
                                    </div>
                                    <div class="col-sm-12">
                                        <label><?= lang('supplier') ?></label>
                                        <input type="text" name="" class="form-control supplier_name" readonly>
                                    </div>
                                </div>
                        <?php endforeach ?>
                        <?php if ($inv->grand_total - $inv->paid > 0): ?>
                            <div class="form-group col-sm-12">
                                <label><?= sprintf(lang('invoice_with_balance'), $this->sma->formatMoney($inv->grand_total - $inv->paid)) ?></label>
                            </div>
                        <?php endif ?>
                    <?php else: ?>
                        <div class="form-group col-sm-12">
                            <label><?= lang('invoice_without_payments') ?></label>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="change_payment_method" value="1">
                <?php if(!$invoice_already_returned && !empty($payments)): ?>
                    <button class="btn btn-success submit" type="button"><?= lang('submit') ?></button>
                <?php endif; ?>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function(){

        <?php if ($invoice_already_returned): ?>
            location.reload();
        <?php endif ?>

        $.each($('select.paid_by'), function(index, select){
            supplier_name = $(select).find(':selected').data('suppliername');
            if (supplier_name != '') {
                $('.supplier_name').eq(index).val(supplier_name);
            }
        });

    });
    $(document).on('click', '.submit', function(){
        if ($('#change_payment_method').valid()) {
            $('#change_payment_method').submit();
        }
    });
    $(document).on('change', 'select.paid_by', function(){
        index_p = $('select.paid_by').index($(this));
        console.log('index_p '+index_p);
        amount_to_pay = $('.pmnt_amount').eq(index_p).val();

        selected = $(this).find(':selected');
        console.log(selected.data());

        commision_value = selected.data('commisionvalue');
        retefuente_value = selected.data('retefuentevalue');
        reteiva_value = selected.data('reteivavalue');
        reteica_value = selected.data('reteicavalue');
        supplier_name = selected.data('suppliername');

        if ((commision_value != '' && commision_value !== undefined) || (retefuente_value != '' && retefuente_value !== undefined ) || (reteiva_value != '' && reteiva_value !== undefined) || (reteica_value != '' && reteica_value !== undefined)) {

            $('.setted_retcom').eq(index_p).val(1);
            $('.supplier_name').eq(index_p).val(supplier_name);

            subtotal = parseFloat("<?= $inv->total ?>");
            iva = parseFloat("<?= $inv->total_tax ?>");
            gtotal = parseFloat("<?= $inv->grand_total ?>");
            $('.retcom_div').eq(index_p).fadeIn();

            if (commision_value != '' && commision_value !== undefined ) {
                $('.pm_commision_value_info').eq(index_p).val(commision_value);
                if (commision_value.toString().indexOf("%") !== -1) {
                    pds = commision_value.split("%");
                    p_commision_perc = parseFloat(pds[0]);
                    p_commision_base = subtotal * (amount_to_pay / gtotal);
                    p_commision_amount = formatDecimal(parseFloat(p_commision_base * (p_commision_perc / 100)));
                    $('.pm_commision_value').eq(index_p).val(p_commision_amount);
                    $('.pm_commision_value').eq(index_p).data('originalamount', p_commision_amount);
                } else {
                    p_commision_base = subtotal * (amount_to_pay / gtotal);
                    p_commision_amount = commision_value;
                    $('.pm_commision_value').eq(index_p).val(p_commision_amount);
                    $('.pm_commision_value').eq(index_p).data('originalamount', p_commision_amount);
                }
            }

            if (retefuente_value != '' && retefuente_value !== undefined) {
                $('.pm_retefuente_value_info').eq(index_p).val(retefuente_value);
                if (retefuente_value.toString().indexOf("%") !== -1) {
                    pds = retefuente_value.split("%");
                    p_retefuente_perc = parseFloat(pds[0]);
                    p_retefuente_base = subtotal * (amount_to_pay / gtotal);
                    p_retefuente_amount = formatDecimal(parseFloat(p_retefuente_base * (p_retefuente_perc / 100)));
                    $('.pm_retefuente_value').eq(index_p).val(p_retefuente_amount);
                    $('.pm_retefuente_value').eq(index_p).data('originalamount', p_retefuente_amount);
                } else {
                    p_retefuente_base = subtotal * (amount_to_pay / gtotal);
                    p_retefuente_amount = retefuente_value;
                    $('.pm_retefuente_value').eq(index_p).val(p_retefuente_amount);
                    $('.pm_retefuente_value').eq(index_p).data('originalamount', p_retefuente_amount);
                }
            }

            if (reteiva_value != '' && reteiva_value !== undefined) {
                $('.pm_reteiva_value_info').eq(index_p).val(reteiva_value);
                if (reteiva_value.toString().indexOf("%") !== -1) {
                    pds = reteiva_value.split("%");
                    p_reteiva_perc = parseFloat(pds[0]);
                    p_reteiva_base = iva * (amount_to_pay / gtotal);
                    p_reteiva_amount = formatDecimal(parseFloat(p_reteiva_base * (p_reteiva_perc / 100)));
                    $('.pm_reteiva_value').eq(index_p).val(p_reteiva_amount);
                    $('.pm_reteiva_value').eq(index_p).data('originalamount', p_reteiva_amount);
                } else {
                    p_reteiva_base = iva * (amount_to_pay / gtotal);
                    p_reteiva_amount = reteiva_value;
                    $('.pm_reteiva_value').eq(index_p).val(p_reteiva_amount);
                    $('.pm_reteiva_value').eq(index_p).data('originalamount', p_reteiva_amount);
                }
            }


            if (reteica_value != '' && reteica_value !== undefined) {
                $('.pm_reteica_value_info').eq(index_p).val(reteica_value);
                if (reteica_value.toString().indexOf("%") !== -1) {
                    pds = reteica_value.split("%");
                    p_reteica_perc = parseFloat(pds[0]);
                    p_reteica_base = subtotal * (amount_to_pay / gtotal);
                    p_reteica_amount = formatDecimal(parseFloat(p_reteica_base * (p_reteica_perc / 100)));
                    $('.pm_reteica_value').eq(index_p).val(p_reteica_amount);
                    $('.pm_reteica_value').eq(index_p).data('originalamount', p_reteica_amount);
                } else {
                    p_reteica_base = subtotal * (amount_to_pay / gtotal);
                    p_reteica_amount = reteica_value;
                    $('.pm_reteica_value').eq(index_p).val(p_reteica_amount);
                    $('.pm_reteica_value').eq(index_p).data('originalamount', p_reteica_amount);
                }
            }

        } else {
            $('.retcom_div').eq(index_p).fadeOut();
            $('.setted_retcom').eq(index_p).val(0);
        }

        console.log(commision_value);

        if ($(this).val() == 'deposit') {
            $.ajax({
                url:  site.base_url+"customers/deposit_balance/<?= $inv->customer_id ?>/"+amount_to_pay,
            }).done(function(data) {
                $('.deposit_message').eq(index_p).empty();
                $('.deposit_message').eq(index_p).append(data);
                $.ajax({
                    url:  site.base_url+"customers/get_deposits_will_be_affected/<?= $inv->customer_id ?>/"+amount_to_pay,
                    dataType: 'JSON',
                }).done(function(data) {
                    if (data.num == 0 || data.num > 1) {
                        $('.submit').prop('disabled', true);
                        $('.deposits_affected_error').fadeIn();
                    } else if (data.error == 1) {
                        $('.submit').prop('disabled', true);
                    } else {
                        $('.submit').prop('disabled', false);
                        $('.deposits_affected_error').fadeOut();
                    }
                });
            });
        } else {
            $('.deposit_message').eq(index_p).empty();
        }
    });


    $(document).on('change', '.pm_commision_value', function(){
        index_p = $('.pm_commision_value').index($(this));
        if ($(this).val() != $(this).data('originalamount') && $(this).val() != 0) {
            $('.pm_commision_changed').eq(index_p).val(1);
        } else {
            $('.pm_commision_changed').eq(index_p).val(0);
        }
    });

    $(document).on('change', '.pm_retefuente_value', function(){
        index_p = $('.pm_retefuente_value').index($(this));
        if ($(this).val() != $(this).data('originalamount') && $(this).val() != 0) {
            $('.pm_retefuente_changed').eq(index_p).val(1);
        } else {
            $('.pm_retefuente_changed').eq(index_p).val(0);
        }
    });

    $(document).on('change', '.pm_reteiva_value', function(){
        index_p = $('.pm_reteiva_value').index($(this));
        if ($(this).val() != $(this).data('originalamount') && $(this).val() != 0) {
            $('.pm_reteiva_changed').eq(index_p).val(1);
        } else {
            $('.pm_reteiva_changed').eq(index_p).val(0);
        }
    });

    $(document).on('change', '.pm_reteica_value', function(){
        index_p = $('.pm_reteica_value').index($(this));
        if ($(this).val() != $(this).data('originalamount') && $(this).val() != 0) {
            $('.pm_reteica_changed').eq(index_p).val(1);
        } else {
            $('.pm_reteica_changed').eq(index_p).val(0);
        }
    });

</script>