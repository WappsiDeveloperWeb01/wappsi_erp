<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <!-- <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/view_adjustment_pos/'.$receipts[0]->id) ?>" target="_blank">
                <i class="fa fa-print"></i> POS
            </a> -->
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
            </div>
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-12">
                        <b><?= lang("created_by"); ?></b> : <?= $receipts[0]->created_by; ?> <br>
                        <b><?= lang("date"); ?></b> : <?= $this->sma->hrld($receipts[0]->date); ?><br>
                        <b>ID</b> : <?= $receipts[0]->id; ?>
                        <?php if ($type == POR_CUTTING_TYPE): ?>
                            <h4>Registro de recibido para la orden de corte N° <?= $reference_no ?></h4>
                        <?php elseif ($type == POR_ASSEMBLE_TYPE): ?>
                            <h4>Registro de recibido para la orden de ensamble N° <?= $reference_no ?></h4>
                        <?php elseif ($type == POR_PACKING_TYPE): ?>
                            <h4>Registro de recibido para la orden de empaque N° <?= $reference_no ?></h4>
                        <?php endif ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 pull-right">
                    <div class="well well-sm">
                            <?php foreach ($receipts as $receipt): ?>
                                    <b><?= lang('component') ?> </b>: <?= "(".$receipt->product_code.") ".$receipt->product_name ?></br>
                                    <b><?= lang('product') ?> </b>: <?= $receipt->pfinished_code.' - '.$receipt->pfinished_name.($receipt->pfinished_variant ? ' - '.$receipt->pfinished_variant : ''); ?></br>
                                    <b><?= lang('ok_quantity') ?> </b>: <?= $this->sma->formatQuantity($receipt->ok_quantity) ?></br>
                                    <b><?= lang('fault_quantity') ?> </b>: <?= $this->sma->formatQuantity($receipt->fault_quantity) ?><br>
                                    <hr>
                            <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
