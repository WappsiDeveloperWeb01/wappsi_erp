<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    var max_qpr = "<?= isset($max_qpr) ? $max_qpr : 0 ?>";
    var POR_SUGGESTIONS_TYPE = "<?= POR_ASSEMBLE_TYPE ?>";
    var cuttings_id = JSON.parse('<?= isset($cuttings_ids) ? json_encode(["ids" => $cuttings_ids]) : "false" ?>');
    <?php if (isset($cutting_order_id)): ?>
        if (localStorage.getItem('assitems')) {
            localStorage.removeItem('assitems');
        }
        if (localStorage.getItem('asscomproducts')) {
            localStorage.removeItem('asscomproducts');
        }
        if (localStorage.getItem('assdate')) {
            localStorage.removeItem('assdate');
        }
        if (localStorage.getItem('assest_date')) {
            localStorage.removeItem('assest_date');
        }
        if (localStorage.getItem('assproduction_order')) {
            localStorage.removeItem('assproduction_order');
        }
        if (localStorage.getItem('asscutting_order')) {
            localStorage.removeItem('asscutting_order');
        }
        if (localStorage.getItem('assreference_no')) {
            localStorage.removeItem('assreference_no');
        }
        if (localStorage.getItem('asssupplier')) {
            localStorage.removeItem('asssupplier');
        }

        localStorage.setItem('asscutting_order', '<?= $cutting_order_id ?>');
    <?php endif ?>

        if (localStorage.getItem('remove_culs')) {
            if (localStorage.getItem('cuitems')) {
                localStorage.removeItem('cuitems');
            }
            if (localStorage.getItem('cucomproducts')) {
                localStorage.removeItem('cucomproducts');
            }
            if (localStorage.getItem('cudate')) {
                localStorage.removeItem('cudate');
            }
            if (localStorage.getItem('cuproduction_order')) {
                localStorage.removeItem('cuproduction_order');
            }
            if (localStorage.getItem('cureference_no')) {
                localStorage.removeItem('cureference_no');
            }
            if (localStorage.getItem('cusupplier')) {
                localStorage.removeItem('cusupplier');
            }
            if (localStorage.getItem('cuemployee')) {
                localStorage.removeItem('cuemployee');
            }
            if (localStorage.getItem('cuemployee2')) {
                localStorage.removeItem('cuemployee2');
            }
            if (localStorage.getItem('cutender')) {
                localStorage.removeItem('cutender');
            }
            if (localStorage.getItem('cutender2')) {
                localStorage.removeItem('cutender2');
            }
            if (localStorage.getItem('cupacker')) {
                localStorage.removeItem('cupacker');
            }
            if (localStorage.getItem('cupacker2')) {
                localStorage.removeItem('cupacker2');
            }
            localStorage.removeItem('remove_culs');
        }

        <?php if ($this->session->userdata('remove_culs')) { ?>
            if (localStorage.getItem('cuitems')) {
                localStorage.removeItem('cuitems');
            }
            if (localStorage.getItem('cucomproducts')) {
                localStorage.removeItem('cucomproducts');
            }
            if (localStorage.getItem('cudate')) {
                localStorage.removeItem('cudate');
            }
            if (localStorage.getItem('cuproduction_order')) {
                localStorage.removeItem('cuproduction_order');
            }
            if (localStorage.getItem('cureference_no')) {
                localStorage.removeItem('cureference_no');
            }
            if (localStorage.getItem('cusupplier')) {
                localStorage.removeItem('cusupplier');
            }
            if (localStorage.getItem('cuemployee')) {
                localStorage.removeItem('cuemployee');
            }
            if (localStorage.getItem('cuemployee2')) {
                localStorage.removeItem('cuemployee2');
            }
            if (localStorage.getItem('cutender')) {
                localStorage.removeItem('cutender');
            }
            if (localStorage.getItem('cutender2')) {
                localStorage.removeItem('cutender2');
            }
            if (localStorage.getItem('cupacker')) {
                localStorage.removeItem('cupacker');
            }
            if (localStorage.getItem('cupacker2')) {
                localStorage.removeItem('cupacker2');
            }
            localStorage.removeItem('remove_culs');
        <?php $this->sma->unset_data('remove_culs');}
        ?>

    $(document).ready(function () {
        if (localStorage.getItem('remove_assls')) {
            if (localStorage.getItem('assitems')) {
                localStorage.removeItem('assitems');
            }
            if (localStorage.getItem('asscomproducts')) {
                localStorage.removeItem('asscomproducts');
            }
            if (localStorage.getItem('assdate')) {
                localStorage.removeItem('assdate');
            }
            if (localStorage.getItem('assest_date')) {
                localStorage.removeItem('assest_date');
            }
            if (localStorage.getItem('assproduction_order')) {
                localStorage.removeItem('assproduction_order');
            }
            if (localStorage.getItem('asscutting_order')) {
                localStorage.removeItem('asscutting_order');
            }
            if (localStorage.getItem('assreference_no')) {
                localStorage.removeItem('assreference_no');
            }
            if (localStorage.getItem('asssupplier')) {
                localStorage.removeItem('asssupplier');
            }
            localStorage.removeItem('remove_assls');
        }

        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('assdate')) {
            $("#assdate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#assdate', function (e) {
            localStorage.setItem('assdate', $(this).val());
        });
        if (assdate = localStorage.getItem('assdate')) {
            $('#assdate').val(assdate);
        }
        $(document).on('change', '#assestimated_date', function (e) {
            localStorage.setItem('assestimated_date', $(this).val());
        });
        if (cuestimated_date = localStorage.getItem('assestimated_date')) {
            $('#assestimated_date').val(assestimated_date);
        }
        <?php } ?>
    });

    $(document).on('change', '#assbiller', function (e) {
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/39/") ?>'+$('#assbiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;
          $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
          if (response.status == 0) {
            $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            localStorage.setItem('locked_for_biller_resolution', 1);
          }
          $('#document_type_id').trigger('change');
        });
        $('#asswarehouse').select2('val', $('#assbiller option:selected').data('warehousedefault')).trigger('change');
        localStorage.setItem('assbiller', $('#assbiller').val());
    });


</script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('add_assemble_order'); ?></h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="introtext"><?php echo lang('enter_info'); ?></p>
                            <?php
                            $attrib = array('id' => 'add_assemble');
                            echo admin_form_open_multipart("production_order/add_assemble_order", $attrib);
                            ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("date", "assdate"); ?>
                                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="assdate" required="required"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("making_order", "production_order"); ?>
                                                <input type="hidden" name="production_order_id" value="" id="assproduction_order" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                                <input type="hidden" name="production_order_reference_no" value="" id="assproduction_order_reference_no">
                                            </div>
                                        </div>
                                        <?php if (!isset($cuttings_ids)): ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("cutting_order", "cutting_order"); ?>
                                                    <input type="hidden" name="cutting_order_id" value="" id="asscutting_order" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                                    <input type="hidden" name="cutting_order_reference_no" value="" id="asscutting_order_reference_no">
                                                </div>
                                            </div>
                                        <?php else: ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("cutting_order", "cutting_order"); ?>
                                                    <input type="text" name="cuttings_ids" class="form-control" value="<?= $cuttings_ids ?>" readonly>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    </div>

                                    <div class="row">
                                        <!-- <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("assemble_reference_no", "assreference_no"); ?>
                                                <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ""), 'class="form-control" id="assreference_no" required="required"'); ?>
                                            </div>
                                        </div> -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("assembler", "asssupplier"); ?>
                                                <input type="hidden" name="supplier_id" value="" id="asssupplier" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("estimated_date", "assest_date"); ?>
                                                <?php echo form_input('est_date', (isset($_POST['est_date']) ? $_POST['est_date'] : ""), 'class="form-control input-tip datetime" id="assest_date" required="required"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("pfinished_products_in_assemble_order"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="assTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th class="col-md-1"><?= lang("type"); ?></th>
                                                        <th class="col-md-2"><?= lang("variant"); ?></th>
                                                        <th class="col-md-1"><?= lang("cutted_quantity"); ?></th>
                                                        <th style="max-width: 30px !important; text-align: center;">
                                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("components"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="assCTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th><?= lang("type"); ?></th>
                                                        <th><?= lang("order_quantity"); ?></th>
                                                        <th><?= lang("finished_quantity"); ?></th>
                                                        <th><?= lang("in_process_quantity"); ?></th>
                                                        <th><?= lang("to_assemble_quantity"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <?= lang("note", "assnote"); ?>
                                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="assnote" style="margin-top: 10px; height: 100px;"'); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="fprom-group">
                                            <button type="button" class="btn btn-primary" id="add_assemble_submit"><?= lang('submit') ?></button>
                                            <?php if ($this->Owner || $this->Admin || $this->GP['production_order-add_packing_order']): ?>
                                                <button type="button" class="btn btn-primary" id="add_assemble_order_submit_continue"><?= lang('submit_init_packing') ?></button>
                                            <?php endif ?>
                                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="scontinue" name="continue" value="0">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $("#add_assemble").validate({
            ignore: []
        });
    });

    $(document).on('click', '#add_assemble_submit', function(){
        $('#scontinue').val(0);
        if ($('#add_assemble').valid()) {
            $('#add_assemble').submit();
        }
    });

    $(document).on('click', '#add_assemble_order_submit_continue', function(){
        $('#scontinue').val(1);
        if ($('#add_assemble').valid()) {
            $('#add_assemble').submit();
        }
    });
</script>