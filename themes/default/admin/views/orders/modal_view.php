<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <!-- <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button> -->
            <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('sales/orderView/'.$inv->id) ?>" target="_blank">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </a>
            <?php if ($logo) { ?>
                <div class="text-center" style="margin-bottom:20px;">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                         alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                </div>
            <?php } ?>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-5">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("sale_no_ref"); ?>: <?= $inv->reference_no; ?><br>
                        <?= lang("sale_status"); ?>: <?= lang($inv->sale_status); ?><br>
                        <?= lang("day_delivery_time"); ?>: <?= $inv->delivery_day ? lang($inv->delivery_day) : ''; ?><br>
                    </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row" style="margin-bottom:15px;">

                <?php if ($Settings->invoice_view == 1) { ?>
                    <div class="col-xs-12 text-center">
                        <h1><?= lang('order_invoice'); ?></h1>
                    </div>
                <?php } ?>

                <div class="col-xs-6">
                    <?php lang('to'); ?>:<br/>
                    <h3 style="margin-top:10px;"><?= $customer->company ? $customer->company : $customer->name; ?></h3>
                    <?= $customer->company ? "" : "Attn: " . $customer->name ?>

                    <?php
                    echo $customer->address . "<br>" . $customer->city . " " . $customer->postal_code . " " . $customer->state . "<br>" . $customer->country;

                    echo "<p>";

                    if ($customer->vat_no != "-" && $customer->vat_no != "") {
                        if ($customer->tipo_documento == 6) {
                            echo  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion." <br>";
                        } else {
                            echo  lang("vat_no") . ": " . $customer->vat_no." <br>";
                        }
                    }
                    if ($customer->cf1 != "-" && $customer->cf1 != "") {
                        echo "<br>" . lang("ccf1") . ": " . $customer->cf1;
                    }
                    if ($customer->cf2 != "-" && $customer->cf2 != "") {
                        echo "<br>" . lang("ccf2") . ": " . $customer->cf2;
                    }
                    if ($customer->cf3 != "-" && $customer->cf3 != "") {
                        echo "<br>" . lang("ccf3") . ": " . $customer->cf3;
                    }
                    if ($customer->cf4 != "-" && $customer->cf4 != "") {
                        echo "<br>" . lang("ccf4") . ": " . $customer->cf4;
                    }
                    if ($customer->cf5 != "-" && $customer->cf5 != "") {
                        echo "<br>" . lang("ccf5") . ": " . $customer->cf5;
                    }
                    if ($customer->cf6 != "-" && $customer->cf6 != "") {
                        echo "<br>" . lang("ccf6") . ": " . $customer->cf6;
                    }

                    echo "</p>";
                    echo lang("tel") . ": " . $customer->phone . "<br>" . lang("email") . ": " . $customer->email;
                    ?>
                </div>

                <div class="col-xs-6">
                    <?php lang('from'); ?>:
                    <h3 style="margin-top:10px;"><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h3>
                    <?= $biller->company ? "" : "Attn: " . $biller->name ?>

                    <?php
                    echo $biller->address . "<br>" . $biller->city . " " . $biller->postal_code . " " . $biller->state . "<br>" . $biller->country;

                    echo "<p>";

                    if ($biller->vat_no != "-" && $biller->vat_no != "") {
                        echo "<br>" . lang("vat_no") . ": " . $biller->vat_no;
                    }
                    if ($biller->cf1 != "-" && $biller->cf1 != "") {
                        echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                    }
                    if ($biller->cf2 != "-" && $biller->cf2 != "") {
                        echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                    }
                    if ($biller->cf3 != "-" && $biller->cf3 != "") {
                        echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                    }
                    if ($biller->cf4 != "-" && $biller->cf4 != "") {
                        echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                    }
                    if ($biller->cf5 != "-" && $biller->cf5 != "") {
                        echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                    }
                    if ($biller->cf6 != "-" && $biller->cf6 != "") {
                        echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                    }

                    echo "</p>";
                    echo lang("tel") . ": " . $biller->phone . "<br>" . lang("email") . ": " . $biller->email;
                    ?>
                </div>

            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover print-table order-table">

                    <thead>

                    <tr>
                        <th><?= lang("no"); ?></th>
                        <th><?= lang("description"); ?></th>
                        <?php if ($Settings->indian_gst) { ?>
                            <th><?= lang("hsn_code"); ?></th>
                        <?php } ?>
                        <th><?= lang('gross_net_unit_price') ?></th>
                        <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                            <th><?= lang("discount") ?></th>
                        <?php endif ?>
                        <th><?= lang("price_x_discount"); ?></th>
                        <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                            <th><?= lang("tax") ?></th>
                        <?php endif ?>
                        <th><?= lang("price_x_tax"); ?></th>
                        <!-- <th><?= lang("picking_quantity"); ?></th>
                        <th><?= lang("packing_quantity"); ?></th> -->
                        <th><?= lang("quantity_stock"); ?></th>
                        <th><?= lang("quantity"); ?></th>
                        <th><?= lang("quantity_dispatch"); ?></th>
                        <th><?= lang("quantity_dispatch_pending"); ?></th>
                        <th><?= lang("subtotal"); ?></th>
                    </tr>

                    </thead>

                    <tbody>

                    <?php $r = 1;

                    // var_dump($rows);

                    foreach ($rows as $row):
                    ?>
                        <tr>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    <?= $this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : ''; ?>
                                    <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                    <?= $row->preferences ? '<br>' . $this->sma->print_preference_selection($row->preferences) : ''; ?>
                                </td>
                                <?php if ($Settings->indian_gst) { ?>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                <?php } ?>
                                <?php 
                                $qty = $row->quantity;
                                $price = $row->unit_price;
                                $net_price = $row->net_unit_price;
                                $price_before_tax = $row->net_unit_price;
                                if (isset($row->operator)) {
                                    if ($row->operator == "*") {
                                        $qty = $row->quantity / $row->operation_value;
                                        $price = $row->unit_price * $row->operation_value;
                                        $net_price = $row->net_unit_price * $row->operation_value;
                                        $price_before_tax = $row->price_before_tax * $row->operation_value;
                                    } else if ($row->operator == "/") {
                                        $qty = $row->quantity * $row->operation_value;
                                        $price = $row->unit_price / $row->operation_value;
                                        $net_price = $row->net_unit_price / $row->operation_value;
                                        $price_before_tax = $row->price_before_tax / $row->operation_value;
                                    } else if ($row->operator == "+") {
                                        $qty = $row->quantity - $row->operation_value;
                                        $price = $row->unit_price - $row->operation_value;
                                    } else if ($row->operator == "-") {
                                        $qty = $row->quantity + $row->operation_value;
                                        $price = $row->unit_price + $row->operation_value;
                                    }
                                }
                                 ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($price_before_tax); ?></td>
                                <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->get_percentage_from_amount($row->discount, $row->price_before_tax); ?></td>
                                <?php endif ?>
                                <td style="text-align:right; width:100px;"><?= $this->sma->formatMoney($net_price); ?></td>
                                <?php
                                // if ($Settings->tax1 && $inv->product_tax > 0) {
                                //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                // }
                                // if ($Settings->product_discount && $inv->product_discount != 0) {
                                //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                // }
                                ?>
                                <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                    <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                <?php endif ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($price); ?></td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->in_stock_quantity); ?></td>
                                <!-- <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->picking_quantity).' '.$row->product_unit_code; ?></td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->packing_quantity).' '.$row->product_unit_code; ?></td> -->
                                
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($qty).' '.$row->product_unit_code; ?></td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity_delivered).' '.$row->product_unit_code; ?></td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity - $row->quantity_delivered).' '.$row->product_unit_code; ?></td>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                            </tr>
                        <?php
                        $r++;
                    endforeach;
                    ?>
                    </tbody>
                    <tfoot>
                    <?php
                    $col = $Settings->indian_gst ? 10 : 9;
                    if ($Settings->product_discount && $inv->product_discount != 0) {
                        $col++;
                    }
                    if ($Settings->tax1 && $inv->product_tax > 0) {
                        $col++;
                    }
                    if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                        $tcol = $col - 2;
                    } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                        $tcol = $col - 1;
                    } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                        $tcol = $col - 1;
                    } else {
                        $tcol = $col;
                    }
                    ?>
                    <?php
                    if ($return_sale) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->grand_total) . '</td></tr>';
                    }
                    if ($inv->surcharge != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->surcharge) . '</td></tr>';
                    }
                    ?>

                    <?php if ($Settings->indian_gst) {
                        if ($inv->cgst > 0) {
                            $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                            echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('cgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                        }
                        if ($inv->sgst > 0) {
                            $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                            echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('sgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                        }
                        if ($inv->igst > 0) {
                            $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                            echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('igst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                        }
                    } ?>

                    <?php if ($inv->order_discount != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount) . '</td></tr>';
                    }
                    ?>
                    <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</td></tr>';
                    }
                    ?>
                    <?php if ($inv->shipping != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                    }
                    ?>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total); ?></td>
                    </tr>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("paid_by"); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= lang($inv->payment_method); ?></td>
                    </tr>

                    </tfoot>
                </table>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <?php
                        if ($inv->note || $inv->note != "") { ?>
                            <div class="well well-sm">
                                <p class="bold"><?= lang("note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->note); ?></div>
                            </div>
                        <?php
                        }
                        if ($inv->staff_note || $inv->staff_note != "") { ?>
                            <div class="well well-sm staff_note">
                                <p class="bold"><?= lang("staff_note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->staff_note); ?></div>
                            </div>
                        <?php } ?>

                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <div class="col-xs-12 pull-right text-center">
                    <p>
                        <?= lang("created_by"); ?>: <?= $inv->created_by ? $created_by->first_name . ' ' . $created_by->last_name : $customer->name; ?> <br>
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                    </p>
                    <?php if ($inv->updated_by) { ?>
                    <p>
                        <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                        <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                    </p>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php if (!$Supplier || !$Customer) { ?>
    <?php if ($inv->sale_status == 'partial' || $inv->sale_status == 'pending') { ?>
        <div class="buttons row div_buttons_modal div_buttons_modal_lg">
            <div class="col-sm-6 col-xs-6">
                <a href="<?= admin_url('sales/edit_order/' . $inv->id.'/1') ?>" class="tip btn btn-primary sledit btn-outline" title="<?= lang('partial_convert') ?>" style="width: 100%;">
                    <i class="fa fa-edit"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('partial_convert') ?></span>
                </a>
            </div>
            <?php if ($inv->sale_status == 'pending'): ?>
                <?php if ($this->Settings->order_sales_conversion == 1): ?>
                    <div class="col-sm-6 col-xs-6">
                        <a href='#' class='po btn btn-primary btn-outline' title='' data-placement="top" data-content="
                                    <p> <?= lang('where_to_convert') ?> </p>
                                    <a class='btn btn-primary' href='<?= admin_url('sales/complete_convert_order/3/').$inv->id ?>'> <?= lang('convert_to_detal_sale') ?> </a>
                                    <a class='btn btn-primary' href='<?= admin_url('sales/complete_convert_order/1/').$inv->id.'/1/1' ?>'> <?= lang('convert_to_pos_sale') ?> </a>
                                    <button class='btn po-close'> <?= lang('cancel') ?> </button>" rel='popover' style="width: 100%;">
                                    <i class='fa fa-download'></i>
                            <?= lang('convert_complete') ?>
                        </a>
                    </div>
                <?php elseif ($this->Settings->order_sales_conversion == 2): ?>
                    <div class="col-sm-6 col-xs-6">
                        <a class='btn btn-primary btn-outline' href='<?= admin_url('sales/complete_convert_order/1/').$inv->id.'/1/1' ?>' style="width: 100%;"> <i class='fa fa-download'></i> <?= lang('convert_to_pos_sale') ?> </a>
                    </div>
                <?php elseif ($this->Settings->order_sales_conversion == 3): ?>
                    <div class="col-sm-6 col-xs-6">
                        <a class='btn btn-primary btn-outline' href='<?= admin_url('sales/complete_convert_order/3/').$inv->id ?>' style="width: 100%;"> <i class='fa fa-download'></i> <?= lang('convert_to_detal_sale') ?> </a>
                    </div>
                <?php endif ?>
                    
            <?php endif ?>
        </div>
    <?php } ?>
<?php } ?>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });
</script>
