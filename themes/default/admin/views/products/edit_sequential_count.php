<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    $(document).ready(function () {
        if (localStorage.getItem('remove_qals')) {
            if (localStorage.getItem('qaitems')) {
                localStorage.removeItem('qaitems');
            }
            if (localStorage.getItem('qaref')) {
                localStorage.removeItem('qaref');
            }
            if (localStorage.getItem('qawarehouse')) {
                localStorage.removeItem('qawarehouse');
            }
            if (localStorage.getItem('qanote')) {
                localStorage.removeItem('qanote');
            }
            if (localStorage.getItem('qadate')) {
                localStorage.removeItem('qadate');
            }
            localStorage.removeItem('remove_qals');
        }
        <?php if ($adjustment) { ?>
        localStorage.setItem('qadate', '<?= $this->sma->hrld($adjustment->date); ?>');
        localStorage.setItem('qaref', '<?= $adjustment->reference_no; ?>');
        localStorage.setItem('qawarehouse', '<?= $adjustment->warehouse_id; ?>');
        localStorage.setItem('qanote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($adjustment->note)); ?>');
        localStorage.setItem('qaitems', JSON.stringify(<?= $adjustment_items; ?>));
        localStorage.setItem('inventoryItems', JSON.stringify(<?= $adjustment_items; ?>));
        localStorage.setItem('remove_qals', '1');
        <?php } ?>

    //     $("#add_item").autocomplete({
    //         source: '<?= admin_url('products/sc_suggestions'); ?>',
    //         minLength: 1,
    //         autoFocus: false,
    //         delay: 250,
    //         response: function (event, ui) {
    //             if ($(this).val().length >= 16 && ui.content[0].id == 0) {
    //                 bootbox.alert('<?= lang('no_match_found') ?>', function () {
    //                     $('#add_item').focus();
    //                 });
    //                 $(this).removeClass('ui-autocomplete-loading');
    //                 $(this).removeClass('ui-autocomplete-loading');
    //                 $(this).val('');
    //             }
    //             else if (ui.content.length == 1 && ui.content[0].id != 0) {
    //                 ui.item = ui.content[0];
    //                 $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
    //                 $(this).autocomplete('close');
    //                 $(this).removeClass('ui-autocomplete-loading');
    //             }
    //             else if (ui.content.length == 1 && ui.content[0].id == 0) {
    //                 bootbox.alert('<?= lang('no_match_found') ?>', function () {
    //                     $('#add_item').focus();
    //                 });
    //                 $(this).removeClass('ui-autocomplete-loading');
    //                 $(this).val('');
    //             }
    //         },
    //         select: function (event, ui) {
    //             event.preventDefault();
    //             if (ui.item.id !== 0) {
    //                 var row = add_adjustment_item(ui.item);
    //                 if (row)
    //                     $(this).val('');
    //             } else {
    //                 bootbox.alert('<?= lang('no_match_found') ?>');
    //             }
    //         }
    //     });
    // });
    if (localStorage.getItem('inventoryItems')) {
        loadItems();
    }

    $("#add_item").autocomplete({
      source: function(request, response) {
        var src = '<?= admin_url('products/sc_suggestions'); ?>';
        //Necesitamos armar array con todas las categorias y todas alas marcas
        categories = [];
        categories = $('#category').val();
        brands = [];
        brands =  $('#brand').val();
        $.ajax({
          url: src,
          dataType: "json",
          data: {
            term : request.term,
            brands:brands,
            categories:categories
          },
          success: function(ui) {
            response(ui);
          }
        });
      },
      minLength: 1,
      autoFocus: false,
      delay: 250,
      response: function (event, ui) {
        console.log('Termina la busqueda');
        if ($(this).val().length >= 16 && ui.content[0].id == 0) {
           bootbox.alert('<?= lang('no_match_found') ?>', function () {
               $('#add_item').focus();
           });
           $(this).removeClass('ui-autocomplete-loading');
           $(this).removeClass('ui-autocomplete-loading');
           $(this).val('');
        }
        else if (ui.content.length == 1 && ui.content[0].id != 0) {
           ui.item = ui.content[0];
           $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
           $(this).autocomplete('close');
           $(this).removeClass('ui-autocomplete-loading');
           console.log('Resultados = 1');
        }
        else if (ui.content.length == 1 && ui.content[0].id == 0) {
           bootbox.alert('<?= lang('no_match_found') ?>', function () {
               $('#add_item').focus();
           });
           $(this).removeClass('ui-autocomplete-loading');
           $(this).val('');
        }
      },
      select: function (event, ui) {
        event.preventDefault();
        console.log('hola');
        console.log(ui);
        if (ui.item.id !== 0) {
          var row = add_inventory_item(ui.item);
          if (row)
          $(this).val('');
        } else {
          bootbox.alert('<?= lang('no_match_found') ?>');
        }
      }
    });
}); //Termina el document Ready



 //Aceite 33320729
function add_inventory_item(item) {





    if (count == 1) {
        inventoryItems = {};
    }
    if (item == null)
        return;

    //var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    var item_id = item.item_id;
    console.log('Agregando Item '+item_id);
    //console.log(item);
    console.log(inventoryItems);

    if (inventoryItems[item_id]) {

        console.log('Encontrado');

        var new_qty = parseFloat(inventoryItems[item_id].row.qty) + 1;
        inventoryItems[item_id].row.base_quantity = new_qty;
        if(inventoryItems[item_id].row.unit != inventoryItems[item_id].row.base_unit) {
            $.each(inventoryItems[item_id].units, function(){
                if (this.id == inventoryItems[item_id].row.unit) {
                    inventoryItems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        inventoryItems[item_id].row.qty = new_qty;

    } else {
        console.log('No Encontrado');
        inventoryItems[item_id] = item;
    }
    inventoryItems[item_id].order = new Date().getTime();
    localStorage.setItem('inventoryItems', JSON.stringify(inventoryItems));
    loadItems();
    return true;
}

function loadItems() {

    if (localStorage.getItem('inventoryItems')) {
        count = 1;
        an = 1;
        $("#qaTable tbody").empty();
        inventoryItems = JSON.parse(localStorage.getItem('inventoryItems'));
        console.log('Revisando');
        console.log(inventoryItems);
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(inventoryItems, function(o){return [parseInt(o.order)];}) : inventoryItems;
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_qty = item.row.qty, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");


            var type = item.row.type ? item.row.type : '';
            var opt = $("<select id=\"poption\" name=\"variant\[\]\" class=\"form-control select rvariant\" />");

            // if(item.options !== false) {
            //     $.each(item.options, function () {
            //         if (item.row.option == this.id)
            //             $("<option />", {value: this.id, text: this.name, selected: 'selected'}).appendTo(opt);
            //         else
            //             $("<option />", {value: this.id, text: this.name}).appendTo(opt);
            //     });
            // } else {
            //     $("<option />", {value: 0, text: 'n/a'}).appendTo(opt);
            //     opt = opt.hide();
            // }

            var row_no = (new Date).getTime();
            //var row_no = item_id;
            var newTr = $('<tr id="rows_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');

            tr_html = '<td><input name="product_id[]" type="hidden" class="rid" value="' + product_id + '"><span class="sname" id="name_' + row_no + '">' + item_code +' - ' + item_name +'</span></td>';

            tr_html += '<td><input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" readonly value="' + formatQuantity2(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
            if (site.settings.product_serial == 1) {
                tr_html += '<td class="text-right"><input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'"></td>';
            }

            tr_html += '<td class="text-center"><i class="fa fa-times tip qadel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#qaTable");
            count += parseFloat(item_qty);
            an++;

        });

        //var col = 3;
        var col = 1;
        var tfoot = '<tr id="tfoot" class="tfoot active"><th colspan="'+col+'">Total</th><th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
        if (site.settings.product_serial == 1) { tfoot += '<th></th>'; }
        tfoot += '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th></tr>';
        $('#qaTable tfoot').html(tfoot);
        //$('select.select').select2({minimumResultsForSearch: 7});
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        set_page_focus();
    }
}

/* ----------------------
 * Delete Row Method
 * ---------------------- */
$(document).on('click', '.qadel', function () {
    var row = $(this).closest('tr');
    //var item_id = row.attr('data-item-id');
    var item_id =  $(row).find(".rid").val();
    console.log(item_id);
    console.log(inventoryItems);
    delete inventoryItems[item_id];
    row.remove();
    if(inventoryItems.hasOwnProperty(item_id)) { } else {
        localStorage.setItem('inventoryItems', JSON.stringify(inventoryItems));
        loadItems();
        return;
    }
});

</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i>Editar conteo secuencial
          <?php //var_dump($adjustment); ?>
          <?php //var_dump($adjustment_items); ?>
          <?php //var_dump($adjustment_items); ?>

        </h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("products/edit_sequentialCount/".$adjustment->id, $attrib);
                ?>
                <div class="row">
                    <div class="col-lg-12">

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("biller", "biller"); ?>
                                <?php
                                $wh[''] = '';
                                foreach ($billers as $biller) {
                                    $wh[$biller->id] = $biller->name;
                                }
                                echo form_dropdown('biller', $wh, ($adjustment->biller_id), 'id="biller" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" style="width:100%;" disabled');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("reference_no", "qaref"); ?>
                                <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : $adjustment->reference_no), 'class="form-control input-tip" id="qaref" disabled'); ?>
                            </div>
                        </div>

                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("date", "qadate"); ?>
                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($adjustment->date)), 'class="form-control input-tip datetime" id="qadate" required="required"'); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("warehouse", "qawarehouse"); ?>
                                    <?php
                                    $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $adjustment->warehouse_id), 'id="qawarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>
                            <?php } else {
                                $warehouse_input = array(
                                    'type' => 'hidden',
                                    'name' => 'warehouse',
                                    'id' => 'qawarehouse',
                                    'value' => $this->session->userdata('warehouse_id'),
                                    );

                                echo form_input($warehouse_input);
                            } ?>

                        <div class="col-md-4 form-group">
                            <?= lang('third', 'company_id') ?>
                            <?php 
                            $cOptions[''] = lang('select_third');
                            foreach ($companies as $row => $company) {
                                $cOptions[$company['id']] = $company['name'];
                            }

                             ?>
                            <?= form_dropdown('company_id', $cOptions, $adjustment->company_id,' id="company_id" class="form-control select" required style="width:100%;"'); ?>
                        </div>



                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <label><?= lang("type"); ?> *</label>
                            <div class="form-group">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-2">
                                                <!-- <?php var_dump($adjustment); ?> -->
                                                <!-- type -->

                                                <input type="radio" class="checkbox type" value="full" name="type" id="full"
                                                <?= $this->input->post('type') ? 'checked="checked"' : ''; ?>
                                                <?= $adjustment->type=='full' ? 'checked="checked"' : ''; ?>



                                                required="required">
                                                <label for="full" class="padding05">
                                                    <?= lang('full'); ?>
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-2">
                                                <input type="radio" class="checkbox type" value="partial" name="type" id="partial"
                                                <?= $this->input->post('type') ? 'checked="checked"' : ''; ?>
                                                <?= $adjustment->type=='partial' ? 'checked="checked"' : ''; ?>>
                                                <label for="partial" class="padding05">
                                                    <?= lang('partial'); ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        <div class="col-md-12 partials" style="display:none;">
                        <div class="well well-sm">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("brands", "brand"); ?>
                                        <?php
                                        unset($wh);
                                        foreach ($brands as $brand) {
                                            $wh[$brand->id] = $brand->name;
                                        }
                                        echo form_dropdown('brand[]', $wh, (isset($_POST['brands']) ? $_POST['brands'] : $adjustment->brands), 'id="brand" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("brand") . '" style="width:100%;" multiple');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("categories", "category"); ?>
                                        <?php
                                        unset($wh);
                                        foreach ($categories as $category) {
                                            $wh[$category->id] = $category->name;
                                        }
                                        echo form_dropdown('category[]', $wh, (isset($_POST['categories']) ? $_POST['categories'] : $adjustment->categories), 'id="category" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("category") . '" style="width:100%;" multiple');
                                        ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>











                        <div class="clearfix"></div>
                        <div class="col-md-12" id="sticker">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("products"); ?> *</label>

                                <div class="controls table-controls">
                                    <table id="qaTable" class="table items  table-bordered table-condensed table-hover">
                                      <thead>
                                      <tr>
                                          <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                           <!-- <th class="col-md-2"><?= lang("variant"); ?></th> -->
                                          <!-- <th class="col-md-1"><?= lang("type"); ?></th> -->
                                          <th class="col-md-1"><?= lang("quantity"); ?></th>
                                          <?php
                                          if ($Settings->product_serial) {
                                              echo '<th class="col-md-4">' . lang("serial_no") . '</th>';
                                          }
                                          ?>
                                          <th style="max-width: 30px !important; text-align: center;">
                                              <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                          </th>
                                      </tr>
                                      </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php //var_dump($adjustment); ?>
                                    <?= lang("note", "qanote"); ?>
                                    <?php
                                      $aux = $adjustment->note;
                                    ?>
                                    <?php


                                    echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : $aux), 'class="form-control" id="qanote" style="margin-top: 10px; height: 100px;"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        <div class="col-md-12">
                            <div
                                class="fprom-group"><?php echo form_submit('edit_sequentialCount', lang("submit"), 'id="edit_sequentialCount" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#brand option[value=''], #category option[value='']").remove();
        $('.type').on('ifChecked', function(e){
            var type_opt = $(this).val();
            if (type_opt == 'partial')
                $('.partials').slideDown();
            else
                $('.partials').slideUp();
            $('#stForm').bootstrapValidator('revalidateField', $(this));
        });

        var condiciones = $("#partial").is(":checked");
        if (condiciones) {
          $('.partials').slideDown();
        }

        $('#qanote').val(localStorage.getItem('qanote'));


        //$("#date").datetimepicker({format: site.dateFormats.js_ldate, fontAwesome: true, language: 'sma', weekStart: 1, todayBtn: 1, autoclose: 1, todayHighlight: 1, startView: 2, forceParse: 0, startDate: "<?= $this->sma->hrld(date('Y-m-d H:i:s')); ?>"});

    });
</script>
