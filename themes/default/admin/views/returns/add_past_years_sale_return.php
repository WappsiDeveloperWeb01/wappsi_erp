<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?= admin_form_open_multipart("returns/add_past_years_sale_return", array("id"=>'form_add_return')); ?>
<input type="hidden" name="sale_reference_no" id="sale_reference_no">
<input type="hidden" name="pos" id="sale_pos">
<script type="text/javascript">
    var sale_payments;
    var return_sale = true;
    var odt_is_pos;
    localStorage.removeItem('re_retenciones');
</script>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("year_database", "year_database"); ?>
                                <?php
                                $lydb_opts[""] = lang('select');
                                foreach ($last_years_databases as $lydb) {
                                    $lyarr = explode("_", $lydb->Database);
                                    $lydb_opts[$lydb->Database] = "20".$lyarr[2]." (".$lydb->Database.")";
                                }
                                echo form_dropdown('year_database', $lydb_opts, (isset($_POST['lydb_opts']) ? $_POST['year_database'] : $Settings->default_biller), 'id="reyear_database" data-placeholder="' . lang("select") . ' ' . lang("year_database") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("sale", "resale"); ?>
                                <div class="input-group">
                                  <?php
                                    echo form_input('sale', (isset($_POST['sale']) ? $_POST['sale'] : ""), 'id="resale" data-placeholder="' . lang("select") . ' ' . lang("sale") . '" required="required" class="form-control" style="width:100%;"');
                                    ?>
                                  <span class="input-group-addon search_sale"><i class="fa fa-search"></i></span>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("date", "redate"); ?>
                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ''), 'class="form-control input-tip datetime" id="redate" required="required"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("add_reference_no_label", "document_type_id"); ?>
                                <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                            </div>
                        </div>
                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("biller", "rebiller"); ?>
                                    <?php
                                    $bl[""] = "";
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="rebiller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } else {
                            $biller_input = array(
                                'type' => 'hidden',
                                'name' => 'biller',
                                'id' => 'rebiller',
                                'value' => $this->session->userdata('biller_id'),
                            );

                            echo form_input($biller_input);
                        } ?>
                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("warehouse", "rewarehouse"); ?>
                                    <?php
                                    $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="rewarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>
                        <?php } else {
                            $warehouse_input = array(
                                'type' => 'hidden',
                                'name' => 'warehouse',
                                'id' => 'rewarehouse',
                                'value' => $this->session->userdata('warehouse_id'),
                            );

                            echo form_input($warehouse_input);
                        } ?>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("customer", "recustomer"); ?>
                                <?php
                                    echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="recustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control ssr-customer input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <?php if ($Settings->tax2) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("order_tax", "retax2"); ?>
                                    <?php
                                    $tr[""] = "";
                                    foreach ($tax_rates as $tax) {
                                        $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('order_tax', $tr, (isset($_POST['order_tax']) ? $_POST['order_tax'] : $Settings->default_tax_rate2), 'id="retax2" data-placeholder="' . lang("select") . ' ' . lang("order_tax") . '" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("order_discount", "rediscount"); ?>
                                    <?php echo form_input('order_discount', '', 'class="form-control input-tip" id="rediscount"'); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                    data-show-preview="false" class="form-control file">
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-12" id="sticker">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i>
                                        </div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                        <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <a href="#" id="addManually" class="tip" title="<?= lang('add_product_manually') ?>">
                                                <i class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i>
                                            </a>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("order_items"); ?> *</label>

                                <div class="controls table-controls">
                                    <table id="reTable" class="table items  table-bordered table-condensed table-hover sortable_table">
                                        <thead>
                                        <tr>
                                            <th class="col-md-4"><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                                            <?php
                                            if ($Settings->product_serial) {
                                                echo '<th class="col-md-2">' . lang("serial_no") . '</th>';
                                            }
                                            ?>
                                            <th class="col-md-1"><?= lang("net_unit_price"); ?></th>
                                            <th class="col-md-1"><?= lang("quantity"); ?></th>
                                            <?php
                                            if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {
                                                echo '<th class="col-md-1">' . lang("discount") . '</th>';
                                            }
                                            ?>
                                            <?php
                                            if ($Settings->tax1) {
                                                echo '<th class="col-md-1">' . lang("product_tax") . '</th>';
                                            }
                                            ?>
                                            <th>
                                                <?= lang("subtotal"); ?>
                                                (<span class="currency"><?= $default_currency->code ?></span>)
                                            </th>
                                            <th style="width: 30px !important; text-align: center;">
                                                <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                        <?php if ( ( $Owner || $Admin || $GP['sales-payments'] )) { ?>
                                <div id="payments">
                                    <hr>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                            <select name="payment_reference_no" class="form-control" id="payment_reference_no" required="required"></select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="payments_div">
                                        <div class="col-md-12 pay_option" data-num="1">
                                            <div class="well well-sm well_1">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-4 deposit_div_1" style="display:none;">
                                                            <?= lang('deposit_document_type_id', 'deposit_document_type_id') ?>
                                                            <select name="deposit_document_type_id[]" class="form-control deposit_document_type_id" data-pbnum="1" required>
                                                                <option value="">Seleccione</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <?= lang("paying_by", "paid_by_1"); ?>
                                                                <select name="paid_by[]" id="paid_by_1" data-pbnum="1" class="form-control paid_by">
                                                                    <?= $this->sma->paid_opts(null, false, true); ?>
                                                                </select>
                                                                <input type="hidden" name="due_payment[]" class="due_payment_1">
                                                            </div>
                                                            <input type="hidden" name="mean_payment_code_fe[]" class="mean_payment_code_fe">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="payment">
                                                <input type="hidden" name="amount-discounted" id="discounted"/>
                                                                <div class="form-group ngc_1">
                                                                    <?= lang("value", "amount_1"); ?>
                                                                    <input name="amount-paid[]" type="text" id="amount_1"
                                                                        class="pa form-control kb-pad amount only_number"/>
                                                                </div>
                                                                <div class="form-group gc_1" style="display: none;">
                                                                    <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                    <input name="gift_card_no[]" type="text" id="gift_card_no_1"
                                                                        class="pa form-control gift_card_no kb-pad"/>
                                                                    <div id="gc_details_1"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 div_slpayment_term_1" style="display:none;">
                                                            <div class="form-group">
                                                                <?= lang("payment_term", "slpayment_term_1"); ?>
                                                                <?php echo form_input('payment_term[]', '', 'class="form-control tip sale_payment_term" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="slpayment_term_1"'); ?>
                                                                <input type="hidden" name="customerpaymentterm" id="customerpaymentterm">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="pcc_1" style="display:none;">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_no[]" type="text" id="pcc_no_1"
                                                                        class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_holder[]" type="text" id="pcc_holder_1"
                                                                        class="form-control"
                                                                        placeholder="<?= lang('cc_holder') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <select name="pcc_type[]" id="pcc_type_1"
                                                                            class="form-control pcc_type"
                                                                            placeholder="<?= lang('card_type') ?>">
                                                                        <option value="Visa"><?= lang("Visa"); ?></option>
                                                                        <option
                                                                            value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                        <option value="Amex"><?= lang("Amex"); ?></option>
                                                                        <option value="Discover"><?= lang("Discover"); ?></option>
                                                                    </select>
                                                                    <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_month[]" type="text" id="pcc_month_1"
                                                                        class="form-control" placeholder="<?= lang('month') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <input name="pcc_year[]" type="text" id="pcc_year_1"
                                                                        class="form-control" placeholder="<?= lang('year') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <input name="pcc_ccv[]" type="text" id="pcc_cvv2_1"
                                                                        class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pcheque_1" style="display:none;">
                                                        <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                            <input name="cheque_no[]" type="text" id="cheque_no_1"
                                                                class="form-control cheque_no"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                   <!--  <div class="col-md-4" style="padding-bottom: 4%">
                                        <button type="button" class="btn btn-primary" id="add_more_payments"><?= lang('add_more_payments') ?></button>
                                    </div> -->
                                </div>
                            <?php } ?>
                        <div class="row" id="bt">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("return_note", "renote"); ?>
                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="renote" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("staff_note", "reinnote"); ?>
                                        <?php echo form_textarea('staff_note', (isset($_POST['staff_note']) ? $_POST['staff_note'] : ""), 'class="form-control" id="reinnote" style="margin-top: 10px; height: 100px;"'); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="fprom-group">
                                <button type="button" id="add_return" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"><?= lang('submit') ?></button>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                            </div>
                        </div>
                    </div>
                            <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                    <tr class="warning">
                                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                        <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                        <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                                        <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                                        <?php }?>
                                        <?php if ($Settings->tax2) { ?>
                                            <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                                        <?php } ?>
                                        <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                          class="fa fa-2x">&times;</i></button>
                                          <h4 class="modal-title" id="rtModalLabel"><?=lang('apply_order_retention');?></h4>
                                        </div>
                                        <div class="modal-body">
                                          <div class="row">
                                            <div class="col-sm-2">
                                              <label>Retención</label>
                                            </div>
                                            <div class="col-sm-5">
                                              <label>Opción</label>
                                            </div>
                                            <div class="col-sm-2">
                                              <label>Porcentaje</label>
                                            </div>
                                            <div class="col-sm-3">
                                              <label>Valor</label>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-sm-2">
                                              <label>
                                                <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                                              </label>
                                            </div>
                                            <div class="col-sm-5">
                                              <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                                                <option>Seleccione...</option>
                                              </select>
                                              <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                              <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                              <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                                            </div>
                                            <div class="col-sm-2">
                                              <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right" readonly>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-sm-2">
                                              <label>
                                                <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                                              </label>
                                            </div>
                                            <div class="col-sm-5">
                                              <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                                                <option>Seleccione...</option>
                                              </select>
                                              <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                              <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                              <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                                            </div>
                                            <div class="col-sm-2">
                                              <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right" readonly>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-sm-2">
                                              <label>
                                                <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                                              </label>
                                            </div>
                                            <div class="col-sm-5">
                                              <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                                                <option>Seleccione...</option>
                                              </select>
                                              <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                              <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                              <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                                            </div>
                                            <div class="col-sm-2">
                                              <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right" readonly>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-sm-2">
                                              <label>
                                                <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                                              </label>
                                            </div>
                                            <div class="col-sm-5">
                                              <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                                                <option>Seleccione...</option>
                                              </select>
                                              <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                              <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                              <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                                            </div>
                                            <div class="col-sm-2">
                                              <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right" readonly>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-sm-2">
                                              <label>
                                                <input type="checkbox" name="rete_bomberil" class="skip" id="rete_bomberil" disabled> <?= lang('rete_bomberil') ?>
                                              </label>
                                            </div>
                                            <div class="col-sm-5">
                                              <select class="form-control" name="rete_bomberil_option" id="rete_bomberil_option" disabled='true'>
                                                <option>Seleccione...</option>
                                              </select>
                                              <input type="hidden" name="rete_bomberil_account" id="rete_bomberil_account">
                                              <input type="hidden" name="rete_bomberil_base" id="rete_bomberil_base">
                                              <input type="hidden" name="rete_bomberil_id" id="rete_bomberil_id">
                                            </div>
                                            <div class="col-sm-2">
                                              <input type="text" name="rete_bomberil_tax" id="rete_bomberil_tax" class="form-control" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="text" name="rete_bomberil_valor" id="rete_bomberil_valor" class="form-control text-right" readonly>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-sm-2">
                                              <label>
                                                <input type="checkbox" name="rete_autoaviso" class="skip" id="rete_autoaviso" disabled> <?= lang('rete_autoaviso') ?>
                                              </label>
                                            </div>
                                            <div class="col-sm-5">
                                              <select class="form-control" name="rete_autoaviso_option" id="rete_autoaviso_option" disabled='true'>
                                                <option>Seleccione...</option>
                                              </select>
                                              <input type="hidden" name="rete_autoaviso_account" id="rete_autoaviso_account">
                                              <input type="hidden" name="rete_autoaviso_base" id="rete_autoaviso_base">
                                              <input type="hidden" name="rete_autoaviso_id" id="rete_autoaviso_id">
                                            </div>
                                            <div class="col-sm-2">
                                              <input type="text" name="rete_autoaviso_tax" id="rete_autoaviso_tax" class="form-control" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="text" name="rete_autoaviso_valor" id="rete_autoaviso_valor" class="form-control text-right" readonly>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-md-8 text-right">
                                              <label>Total : </label>
                                            </div>
                                            <div class="col-md-4 text-right">
                                              <label id="total_rete_amount"> 0.00 </label>
                                            </div>
                                            <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                                          </div>
                                          <!-- <div class="form-group">
                                            <?=lang("order_tax", "order_tax_input");?>
                                            <?php
                                            $tr[""] = "";
                                            foreach ($tax_rates as $tax) {
                                              $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                                            ?>
                                          </div> -->
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
                                          <button type="button" id="cancelOrderRete" class="btn btn-danger" data-dismiss="modal"><?= lang('reset') ?></button>
                                          <button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script type="text/javascript">
    var count = 1, an = 1, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0, invoice_tax = 0, product_discount = 0, order_discount = 0, total_discount = 0, total = 0, allow_discount = <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? 1 : 0; ?>,
        tax_rates = <?php echo json_encode($tax_rates); ?>;

    $(document).ready(function () {
        $("#form_add_return").validate({
              ignore: []
          });
        ItemnTotals();
        $('.bootbox').on('hidden.bs.modal', function (e) {
            $('#add_item').focus();
        });
        $("#add_item").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('returns/suggestions'); ?>',
                    dataType: "json",
                    data: { term: request.term },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_return_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
    });
$(document).on('click', '#add_return', function(){
    if ($('#form_add_return').valid()) {
        $('#add_return').prop('disabled', true);
        $('#form_add_return').submit();
    }
});
</script>