<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['sales_budget_execution']         = 'Ejecución Presupuesto de Ventas';
$lang['sales_budget_execution_report']  = 'Informe Ejecución Presupuesto de Ventas';
$lang['see_units']                      = 'Ver Unidades';
$lang['include_credit_notes']           = 'Incluir Notas Crédito';
$lang['category']                       = 'Categoría';