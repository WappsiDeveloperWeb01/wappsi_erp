<?php defined('BASEPATH') OR exit('No direct script access allowed');

class GroupsModel extends CI_Model
{
    public $tableName = 'groups';

    public function __construct() {
        parent::__construct();
    }

    public function find($filters) {
        $this->db->where($filters);
        return $this->db->get($this->tableName)->row();
    }

    public function create($data) {
        if ($this->db->insert($this->tableName, $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
}
