<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var potype = 1;
    var id_purchase = "<?= $inv->id ?>";
    var suggested_paid_by = "<?= $inv->suggested_paid_by ? $inv->suggested_paid_by : $direct_paid_by ?>";
    var suggested_paid_amount = "<?= $inv->suggested_paid_amount ? $inv->suggested_paid_amount : $direct_paid_amount ?>";
    var purchase_has_payments = "<?= $product_has_movements_after_date ? true : $purchase_has_payments ?>";
    var purchase_has_multi_payments = "<?= $product_has_movements_after_date ? true : $purchase_has_multi_payments ?>";
    var product_has_movements_after_date = "<?= $product_has_movements_after_date ?>";
    var is_quote = false;
    var support_document = JSON.parse("<?= !empty($inv->resolucion) ? 'true' : 'false' ?>");
    var is_csv = false;
    <?php if($inv) { ?>
        <?php
            $poref = explode('-', $inv->reference_no);
            $poref = $poref[1];
         ?>
        localStorage.setItem('powarehouse', '<?= $inv->warehouse_id ?>');
        localStorage.setItem('purchase_type', '<?= $inv->purchase_type ?>');
        localStorage.setItem('po_form_import', JSON.parse('<?= $inv->purchase_from_import == 1 ? "true" : "false" ?>'));
        localStorage.setItem('potag', '<?= $inv->tag_id ?>');
        localStorage.setItem('pobiller', '<?= $inv->biller_id ?>');
        <?php 
            $note_p = ''; 
            if (strpos(str_replace(array("\r", "\n", "'", '"'), "", $this->sma->decode_html($inv->note)), '~') !== false) { 
                $note_p = explode('~', $this->sma->decode_html($inv->note)); 
                $note_p = nl2br($note_p[0]); 
                ;
            }else{
                $note_p = nl2br($this->sma->decode_html($inv->note));
            } 
        ?>
        localStorage.setItem('podiscount', '<?= $inv->order_discount_id ?>');
        localStorage.setItem('potax2', '<?= $inv->order_tax_id ?>');
        localStorage.setItem('poref', '<?= $poref ?>');
        <?php 
        $poshipping = ($inv->prorated_shipping_cost > 0 ? $inv->prorated_shipping_cost : $inv->shipping);
        if ($poshipping > 0 && $inv->purchase_currency_trm > 0) {
            $poshipping = $poshipping / $inv->purchase_currency_trm;
        }
         ?>
        localStorage.setItem('poshipping', '<?= $poshipping ?>');
        localStorage.setItem('pooriginal_shipping', '<?= $inv->prorated_shipping_cost > 0 ? $inv->prorated_shipping_cost : $inv->shipping ?>');
        localStorage.setItem('poorderdiscountmethod', '<?= $inv->order_discount_method ?>');
        localStorage.setItem('postatus', '<?= $inv->status ?>');
        <?php if ($inv->supplier_id) { ?>
            localStorage.setItem('posupplier', '<?= $inv->supplier_id ?>');
        <?php } ?>
        localStorage.setItem('poitems', JSON.stringify(<?= $inv_items; ?>));
        <?php if ($inv->purchase_currency != $this->Settings->default_currency && $inv->purchase_currency_trm > 0) { ?>
            localStorage.setItem('othercurrency', true);
            localStorage.setItem('othercurrencycode', '<?= $inv->purchase_currency ?>');
            localStorage.setItem('othercurrencytrm', '<?= $this->sma->formatDecimals($inv->purchase_currency_trm) ?>');
        <?php } ?>

        <?php if ($inv->purchase_type) { ?>
            localStorage.setItem('purchase_type', '<?= $inv->purchase_type ?>');
            potype = '<?= $inv->purchase_type ?>';
        <?php } ?>

        <?php if  ($inv->rete_fuente_total != 0 || $inv->rete_iva_total != 0 || $inv->rete_ica_total != 0 || $inv->rete_other_total != 0): ?>
            var poretenciones = {
                'gtotal' : '<?= $inv->grand_total ?>',
                'total_rete_amount' : '<?= $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total ?>',
                'fuente_option' : '<?= $inv->rete_fuente_percentage ?>',
                'id_rete_fuente' : '<?= $inv->rete_fuente_id ?>',
                'iva_option' : '<?= $inv->rete_iva_percentage ?>',
                'id_rete_iva' : '<?= $inv->rete_iva_id ?>',
                'ica_option' : '<?= $inv->rete_ica_percentage ?>',
                'id_rete_ica' : '<?= $inv->rete_ica_id ?>',
                'otros_option' : '<?= $inv->rete_other_percentage ?>',
                'id_rete_otros' : '<?= $inv->rete_other_id ?>',
                'bomberil_option' : '<?= $inv->rete_bomberil_percentage ?>',
                'id_rete_bomberil' : '<?= $inv->rete_bomberil_id ?>',
                'autoaviso_option' : '<?= $inv->rete_autoaviso_percentage ?>',
                'id_rete_autoaviso' : '<?= $inv->rete_autoaviso_id ?>',
                'rete_fuente_assumed' : JSON.parse('<?= $inv->rete_fuente_assumed == 1 ? "true" : "false" ?>'),
                'rete_iva_assumed' : JSON.parse('<?= $inv->rete_iva_assumed == 1 ? "true" : "false" ?>'),
                'rete_ica_assumed' : JSON.parse('<?= $inv->rete_ica_assumed == 1 ? "true" : "false" ?>'),
                'rete_otros_assumed' : JSON.parse('<?= $inv->rete_other_assumed == 1 ? "true" : "false" ?>'),
                'base_rete_fuente' : '<?= $inv->rete_fuente_base ?>',
                'base_rete_otros' : '<?= $inv->rete_other_base ?>',
                'calculated_base_rete_fuente' : '<?= $inv->rete_fuente_base ?>',
                'calculated_base_rete_otros' : '<?= $inv->rete_other_base ?>',
            };
            poretenciones.total_discounted = '<?= $inv->grand_total - ($inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total) ?>';
            localStorage.setItem('poretenciones', JSON.stringify(poretenciones));
        <?php endif ?>
        // ACÁ PAGO NUEVO
        $('#paid_by_1').select2('val', "<?= $purchase_payments ? $purchase_payments[0]->paid_by : 'Credito' ?>");
    <?php } ?>
    var edit_paid_by = false;
    var edit_paid_amount = false;
    <?php if ($purchase_has_payments == true): ?>
        edit_paid_by = '<?= $purchase_payments[0]->paid_by ?>';
        edit_paid_amount = '<?= $purchase_payments[0]->amount ?>';
    <?php endif ?>
    var count = 1, an = 1, po_edit = true, product_variant = 0, DT = <?= $Settings->purchase_tax_rate ?>, order_discount = 0, DC = '<?= $default_currency->code ?>', shipping = 0,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>, poitems = {},
        audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3'),
        audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function () {
        $('#poref').on('input', function() {
            var texto = $(this).val();
            var newTexto = texto.replace(/[-]/g, '');
            if (texto !== newTexto) {
                $(this).val(newTexto);
            }
        });
        <?php if($this->input->get('supplier')) { ?>
        if (!localStorage.getItem('poitems')) {
            localStorage.setItem('posupplier', <?=$this->input->get('supplier');?>);
        }
        <?php } ?>
        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('podate')) {

        }
        <?php
            $purchase_date = date('Y-m-d', strtotime($inv->date));
            if ($this->Settings->ignore_purchases_edit_validations == 0) {
                $fecha = new DateTime();
                $fecha->modify('last day of this month');
                $last_day = $fecha->format('d');
                $startDate = $this->Settings->purchase_datetime_management == 1 ? date('01/m/Y') : date('Y-m-d');
                $endDate = $this->Settings->purchase_datetime_management == 1 ? $last_day.date('/m/Y') : date('Y-m-').$last_day;
            } else {
                $año_inicio = date('Y', strtotime($this->Settings->system_start_date));
                $startDate = $this->Settings->purchase_datetime_management == 1 ? date('01/01/'.$año_inicio) : $año_inicio.date('-01-01');
                $endDate = $this->Settings->purchase_datetime_management == 1 ? date('31/12/Y') : date('Y-12-31');
            }
         ?>
        <?php if ($this->Settings->purchase_datetime_management == 1): ?>
            $("#podate").datetimepicker({
                format: site.dateFormats.js_ldate,
                startDate : '<?= $startDate ?> 00:00',
                endDate : '<?= $endDate ?> 00:00',
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
            });
        <?php else: ?>
                $('#podate').val("<?= date('Y-m-d') ?>").prop('min', min_input_date).prop('max', max_input_date);
        <?php endif ?>
        $(document).on('change', '#podate', function (e) {
            localStorage.setItem('podate', $(this).val());
            $('#purchase_date_changed').val(1);
        });
        if (podate = localStorage.getItem('podate')) {
            $('#podate').val(podate);
        }
        <?php } ?>
        if (!localStorage.getItem('potax2')) {
            localStorage.setItem('potax2', <?=$Settings->purchase_tax_rate;?>);
            setTimeout(function(){ $('#extras').iCheck('check'); }, 1000);
        }
        ItemnTotals();
        $("#add_item").autocomplete({
            // source: '<?= admin_url('purchases/suggestions'); ?>',
            source: function (request, response) {
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('purchases/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        supplier_id: $("#posupplier").val(),
                        biller_id: $("#pobiller").val(),
                        purchase_type: "<?= $inv->purchase_type ?>"
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    if (site.pos_settings.use_barcode_scanner == 0) {
                        command: toastr.error('No se encontró ningún producto que coincida con lo digitado', 'Búsqueda sin resultados', { onHidden : function(){ $('#producto').focus(); } });
                    } else {
                        bootbox.alert('<?= lang('no_match_found') ?>', function () {
                            $('#add_item').focus();
                        });
                        $(this).val('');
                    }
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    if (site.pos_settings.use_barcode_scanner == 0) {
                        command: toastr.error('No se encontró ningún producto que coincida con lo digitado', 'Búsqueda sin resultados', { onHidden : function(){ $('#producto').focus(); } });
                    } else {
                        bootbox.alert('<?= lang('no_match_found') ?>', function () {
                            $('#add_item').focus();
                        });
                        $(this).val('');
                    }
                    $(this).removeClass('ui-autocomplete-loading');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {

                    ui.item.is_new = true;
                    if (ui.item.row.cnt_units_prices !== undefined && ui.item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && ui.item.row.promotion != 1) {
                        var item_id = ui.item.item_id;
                        var warehouse_id = $('#powarehouse').val();
                        $('.product_name_spumodal').text(ui.item.label);
                        $('#sPUModal').appendTo("body").modal('show');
                        add_item_unit(item_id, warehouse_id);
                        $(this).val('');
                    } else {
                        var row = add_purchase_item(ui.item);
                        if (row)
                            $(this).val('');
                    }

                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });

        $(document).on('click', '#addItemManually', function (e) {
            if (!$('#mcode').val()) {
                $('#mError').text('<?= lang('product_code_is_required') ?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#mname').val()) {
                $('#mError').text('<?= lang('product_name_is_required') ?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#mcategory').val()) {
                $('#mError').text('<?= lang('product_category_is_required') ?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#munit').val()) {
                $('#mError').text('<?= lang('product_unit_is_required') ?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#mcost').val()) {
                $('#mError').text('<?= lang('product_cost_is_required') ?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#mprice').val()) {
                $('#mError').text('<?= lang('product_price_is_required') ?>');
                $('#mError-con').show();
                return false;
            }

            var msg, row = null, product = {
                type: 'standard',
                code: $('#mcode').val(),
                name: $('#mname').val(),
                tax_rate: $('#mtax').val(),
                tax_method: $('#mtax_method').val(),
                category_id: $('#mcategory').val(),
                unit: $('#munit').val(),
                cost: $('#mcost').val(),
                price: $('#mprice').val()
            };

            $.ajax({
                type: "get", async: false,
                url: site.base_url + "products/addByAjax",
                data: {token: "<?= $csrf; ?>", product: product},
                dataType: "json",
                success: function (data) {
                    if (data.msg == 'success') {
                        row = add_purchase_item(data.result);
                    } else {
                        msg = data.msg;
                    }
                }
            });
            if (row) {
                $('#mModal').modal('hide');
                //audio_success.play();
            } else {
                $('#mError').text(msg);
                $('#mError-con').show();
            }
            return false;

        });
    });

</script>
<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
<?php if ($product_has_movements_after_date): ?>
    <div class="alert alert-danger" role="alert">Algunos de los productos de esta compra tiene movimientos posteriores, por lo que la edición está limitada</div>
<?php endif ?>
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                            <?php
                            $attrib = array('id' => 'purchase_form');
                            echo admin_form_open_multipart("purchases/edit/" . $inv->id, $attrib, $attrib);
                            ?>
                            <?=
                            form_hidden('id_purchase', $inv->id);
                             ?>
                             <?=
                            form_hidden('prev_reference', $inv->reference_no);
                             ?>
                            <input type="hidden" name="purchase_payment_term" id="purchase_payment_term" value="<?= $inv->payment_term ?>">
                            <input type="hidden" name="purchase_date_changed" id="purchase_date_changed" value="0">
                            <input type="hidden" name="payment_method" id="payment_method" value="<?= $purchase_payments ? $purchase_payments[0]->paid_by : 'Credito' ?>">
                            <div class="row">

                                <?php // if ($Owner || $Admin) { ?>
                                    <div class="col-md-4 same-height">
                                        <div class="form-group">
                                            <?= lang("date", "podate"); ?>
                                            <?php if ($this->Settings->purchase_datetime_management == 1): ?>
                                                <?php echo form_input('date', ($this->sma->hrld($inv->date)), 'class="form-control input-tip" id="podate" required="required"'); ?>
                                            <?php else: ?>
                                                <input type="date" name="date" class="form-control skip" id="podate" required>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                <?php // } ?>

                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4  same-height form-group">
                                        <?= lang("biller", "pobiller"); ?>
                                        <?php
                                        $bl[""] = "";
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        }
                                        ?>
                                        <select name="biller" class="form-control" id="pobiller" required="required">
                                            <?php foreach ($billers as $biller): ?>
                                              <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "pobiller"); ?>
                                            <select name="biller" class="form-control" id="pobiller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):

                                                    $biller = $bldata[$this->session->userdata('biller_id')];

                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="col-md-4 same-height">
                                    <?= lang('purchase_type', 'purchase_type') ?>
                                    <select name="purchase_type" id="purchase_type" class="form-control">
                                        <option value="1"><?= lang('purchase_type_purchase') ?></option>
                                        <option value="2"><?= lang('purchase_type_expense') ?></option>
                                        <option value="3"><?= lang('purchase_type_expense_import') ?></option>
                                    </select>
                                </div>

                            </div>
                            <div class="row">

                                <?php

                                $reference = explode('-', $inv->reference_no);

                                 ?>

                                <?php if ($this->Settings->management_consecutive_suppliers): ?>
                                    <div class="col-md-4 same-height">
                                        <div class="form-group">
                                            <?= lang("reference_no", "poref"); ?><br>
                                                <div style="display: flex;">
                                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required">
                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="col-md-4 same-height">
                                        <div class="form-group">
                                            <?= lang("reference_no", "poref"); ?>
                                            <label style="margin-left: 9%;">Número</label>
                                            <br>
                                                <div style="display: flex;">
                                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required" style="width: 30%;">
                                                    </select>
                                                    <?php echo form_input('reference_no', $reference[1], 'class="form-control input-tip" id="poref" style="width: 70%;"'); ?>
                                                </div>
                                                <em class="text-danger referencia_duplicada text-bold" style="display: none;"> * Referencia duplicada</em>
                                        </div>
                                    </div>
                                <?php endif ?>

                                <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                    <div class="col-md-4 same-height">
                                        <div class="form-group">
                                            <?= lang("warehouse", "powarehouse"); ?>
                                            <?php
                                            $wh[''] = '';
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="powarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                            ?>
                                        </div>
                                    </div>
                                <?php } else {
                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'warehouse',
                                        'id' => 'powarehouse',
                                        'value' => $this->session->userdata('warehouse_id'),
                                    );

                                    echo form_input($warehouse_input);
                                } ?>

                                <div class="col-md-4 same-height">
                                    <div class="form-group">
                                        <?= lang("supplier", "posupplier"); ?>
                                        <?php if ($Owner || $Admin || $GP['suppliers-add'] || $GP['suppliers-index']) { ?><div class="input-group"><?php } ?>
                                            <input type="hidden" name="supplier" value="" id="posupplier"
                                                class="form-control" style="width:100%;"
                                                placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                                            <input type="hidden" name="supplier_id" value="" id="supplier_id"
                                                class="form-control">
                                            <?php if ($Owner || $Admin || $GP['suppliers-index']) { ?>
                                                <div class="input-group-addon no-print" style="padding: 2px 5px; border-left: 0;">
                                                    <a id="view-supplier">
                                                        <i class="fa fa-2x fa-user" id="addIcon"></i>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                            <?php if ($Owner || $Admin || $GP['suppliers-add']) { ?>
                                            <div class="input-group-addon no-print" style="padding: 2px 5px;">
                                                <a href="<?= admin_url('suppliers/add'); ?>" id="add-supplier" class="external" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <?php if ($Owner || $Admin || $GP['suppliers-add'] || $GP['suppliers-index']) { ?></div><?php } ?>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>
                                        <input type="checkbox" name="purchase_from_import" id="purchase_from_import" <?= $inv->status == 'received' ? 'disabled' : '' ?>>
                                        <?= lang('purchase_from_import') ?>
                                    </label>
                                </div>
                                <div class="col-md-4 tag_div" style="display:none;">
                                    <?= lang('tag', 'tag') ?>
                                    <select name="tag" id="potag" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($tags): ?>
                                            <?php foreach ($tags as $tag_row): ?>
                                                <option value="<?= $tag_row->id ?>" data-color="<?= $tag_row->color ?>" data-type="<?= $tag_row->type ?>"><?= $tag_row->description ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">

                                <?php if ($this->Settings->descuento_orden == 3): ?>
                                    <?php

                                    $dopts = array(
                                            '' => lang('select'),
                                            '1' => lang('descuento_orden_afecta_iva'),
                                            '2' => lang('descuento_orden_no_afecta_iva'),
                                    );

                                     ?>

                                    <div class="col-md-4 form-group">
                                        <?= lang("descuento_orden", "order_discount_method") ?>
                                        <?= form_dropdown('order_discount_method', $dopts, $this->Settings->descuento_orden,'id="order_discount_method" class="form-control" required'); ?>
                                    </div>

                                   <!--  <?php

                                    $dopts2 = array(
                                                    '2' => lang('order_discount_to_products_no'),
                                                    '1' => lang('order_discount_to_products_yes'),
                                                    );

                                     ?>
                                     <div class="col-md-4 form-group">
                                        <?= lang("order_discount_to_products", "order_discount_to_products") ?>
                                        <?= form_dropdown('order_discount_to_products', $dopts2, '','id="order_discount_to_products" class="form-control" required'); ?>
                                     </div> -->
                                <?php else: ?>
                                    <input type="hidden" name="order_discount_method" id="order_discount_method" value="<?= $this->Settings->descuento_orden ?>">
                                    <!-- <input type="hidden" name="order_discount_to_products" id="order_discount_to_products" value="2"> -->
                                <?php endif ?>


                                <?php if (count($currencies) > 1): ?>
                                <div class="col-md-4 same-height row">
                                        <div class="col-md-6 form-group">
                                            <?= lang('currency', 'currency') ?>
                                            <select name="currency" class="form-control" id="currency">
                                                <?php foreach ($currencies as $currency): ?>
                                                    <option value="<?= $currency->code ?>" data-rate="<?= $currency->rate ?>" <?= $currency->code == $this->Settings->default_currency ? "selected='selected'" : "" ?>><?= $currency->code ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <div class="col-md-6 form-group trm-control" style="display: none;">
                                        <?= lang('trm', 'trm') ?>
                                        <input type="number" name="trm" id="trm" class="form-control" required="true">
                                    </div>
                                </div>
                                <?php else: ?>
                                    <input type="hidden" name="currency" value="<?= $this->Settings->default_currency ?>" id="currency">
                                <?php endif ?>

                                <?php if (isset($cost_centers)): ?>
                                    <div class="col-md-4 form-group">
                                        <?= lang('cost_center', 'cost_center_id') ?>
                                        <?php
                                        $ccopts[''] = lang('select');
                                        foreach ($cost_centers as $cost_center) {
                                            $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                        }
                                         ?>
                                         <?= form_dropdown('cost_center_id', $ccopts, $inv->cost_center_id, 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                <?php endif ?>

                            </div>
                            <div class="row">
                                <?php if ($this->Settings->management_consecutive_suppliers == 1): ?>
                                    <div class="col-md-4  same-height">
                                        <?= lang('consecutive_supplier', 'consecutive_supplier') ?>
                                        <?= form_input('consecutive_supplier', $inv->consecutive_supplier, 'class="form-control" id="consecutive_supplier" required') ?>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="row">
                                <div class="clearfix"></div>

                                <div class="col-md-12" id="sticker">
                                    <div class="well well-sm">
                                        <div class="form-group" style="margin-bottom:0;">
                                            <div class="input-group wide-tip">
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                                <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_product_to_order") . '"'); ?>
                                                <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <a href="<?= admin_url('products/add') ?>" id="addManually1"><i
                                                            class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i></a></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="control-group table-group">
                                        <label class="table-label"><?= lang("order_items"); ?></label>

                                        <div class="controls table-controls">
                                            <table id="poTable"
                                                class="table items  table-bordered table-condensed table-hover sortable_table">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <?= lang('product'); ?>
                                                    </th>
                                                    <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                                                        <th><?= lang("variant") ?></th>
                                                    <?php endif ?>
                                                    <th class="thead_variant"><?= lang("variant") ?></th>
                                                    <?php if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10): ?>
                                                        <th><?= lang('unit') ?></th>
                                                        <th><?= lang('product_unit_quantity') ?></th>
                                                        <th class="thead_um_cost"><?= lang('net_product_unit_cost') ?></th>
                                                    <?php endif ?>
                                                    <?php if ($this->Settings->product_serial): ?>
                                                        <th><?= lang("serial_no") ?></th>
                                                    <?php endif ?>
                                                    <?php
                                                    if ($Settings->product_expiry) {
                                                        echo '<th >' . $this->lang->line("expiry_date") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="thead_discount thead_unit_cost"><?= lang("net_unit_cost"); ?></th>
                                                    <?php
                                                    if ($Settings->product_discount) {
                                                        echo '<th class="thead_discount">' . $this->lang->line("discount") . '</th>';
                                                    }
                                                    ?>
                                                    <th style="width:160px;" class="thead_unit_cost"><?= lang("net_unit_cost"); ?></th>
                                                    <th><?= lang('original_quantity') ?></th>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        // echo '<th class="col-md-1">' . $this->lang->line("product_tax") . '</th>';
                                                        echo '<th>' . $this->lang->line("product_tax") . ' (<span class="currency">'. $default_currency->code.'</span>)  </th>';
                                                    }
                                                    if ($Settings->ipoconsumo) {
                                                        echo '<th>' . $this->lang->line("second_product_tax") . ' (<span class="currency">'. $default_currency->code.'</span>)  </th>';
                                                    }
                                                    ?>
                                                    <th style="width:160px;" class="thead_unit_cost"><?= lang('cost_x_tax') ?></th>

                                                    <?php if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10): ?>
                                                        <th class="thead_um_cost"><?= lang('product_unit_cost') ?></th>
                                                    <?php endif ?>
                                                    <th class="thead_shipping"><?= lang('purchase_shipping') ?></th>
                                                    <th style="width: 110px !important;"><?= lang("quantity"); ?></th>
                                                    <th>
                                                        <?= lang("subtotal"); ?> (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th>
                                                    <th style="width: 30px !important; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                                </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot></tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <input type="hidden" name="total_items" value="" id="total_items"/>

                                <div class="col-md-12">
                                    <!-- <div class="form-group">
                                        <input type="checkbox" class="checkbox" id="extras" value=""/>
                                        <label for="extras" class="padding05"><?= lang('more_options') ?></label>
                                    </div> -->
                                    <div class="row" id="extras-con" style="">
                                        <?php if ($Settings->tax3) { ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <span class="fa fa-plus-square-o"></span> <?= lang('order_tax', 'potax2') ?>
                                                    <?php
                                                    $tr[""] = "";
                                                    foreach ($tax_rates as $tax) {
                                                        $tr[$tax->id] = $tax->name;
                                                    }
                                                    echo form_dropdown('order_tax', $tr, $Settings->purchase_tax_rate, 'id="potax2" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="col-md-4 same-height">
                                            <div class="form-group">
                                                <span class="fa fa-minus-square-o"></span> <?= lang("discount_label", "podiscount"); ?>
                                                <?php echo form_input('discount', '', 'class="form-control input-tip text-right" id="podiscount"'); ?>
                                            </div>
                                        </div>


                                        <div class="col-md-4 same-height">
                                            <div class="form-group">
                                                <?= lang("status", "postatus"); ?>
                                                <?php
                                                $post = array('received' => lang('received'), 'pending' => lang('not_approved'), 'ordered' => lang('ordered'));
                                                echo form_dropdown('status', $post, (isset($_POST['status']) ? $_POST['status'] : $inv->status), 'id="postatus" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4" style="<?= $this->Settings->prorate_shipping_cost != 2 ? 'display: none;' : '' ?>">
                                            <div class="form-group">
                                                <?= lang('prorate_shipping_cost', 'prorate_shipping_cost') ?>
                                                <select name="prorate_shipping_cost" id="prorate_shipping_cost" class="form-control">
                                                    <option value=""><?= lang('select') ?></option>
                                                    <option value="0" <?= $this->Settings->prorate_shipping_cost == 0 ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                                    <option value="1" <?= $this->Settings->prorate_shipping_cost == 1 || $inv->prorated_shipping_cost > 0 ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 same-height">
                                            <div class="form-group">
                                                <?= lang("purchase_shipping", "poshipping"); ?>
                                                <?php echo form_input('shipping', '', 'class="form-control input-tip" id="poshipping"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4 same-height">
                                            <label onclick="$('#rtModal').modal('show');" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención </label>
                                            <input type="text" name="retencion_show" id="retencion_show" class="form-control text-right" readonly>
                                            <input type="hidden" name="retencion" id="rete">
                                        </div>

                                        <div class="col-md-4 same-height">
                                            <div class="form-group">
                                                <?= lang("document", "document") ?>
                                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                                    data-show-preview="false" class="form-control file">
                                            </div>
                                        </div>

                                        <?php if ($Owner || $Admin || $GP['sales-payments']) { ?>
                                            <!-- <div class="col-sm-4 same-height div_payment">
                                                <div class="form-group">
                                                    <?= lang("payment_status", "popayment_status"); ?>
                                                    <?php $pst = array('pending' => lang('pending'), 'due' => lang('due'), 'partial' => lang('partial'), 'paid' => lang('paid'));
                                                    echo form_dropdown('payment_status', $pst, '', 'class="form-control input-tip" required="required" id="popayment_status"'); ?>

                                                </div>
                                            </div> -->
                                            <?php
                                            } else {
                                                // echo form_hidden('payment_status', 'pending');
                                                // echo form_hidden('payment_affects_register', '1');
                                            }
                                            ?>
                                            <?php if ($purchase_has_multi_payments == false): ?>
                                                <div id="payments" style="display: none;">
                                        <hr>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("payment_reference_no", "payment_reference_no"); ?><br>
                                                <div style="display: flex;">
                                                    <select name="payment_reference_no" class="form-control" id="payment_reference_no">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="well well-sm well_1">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <?php if ($this->Settings->purchase_payment_affects_cash_register == 0): ?>
                                                            <?= form_hidden('payment_affects_register', '0') ?>
                                                        <?php elseif ($this->Settings->purchase_payment_affects_cash_register == 1): ?>
                                                            <?= form_hidden('payment_affects_register', '1') ?>
                                                        <?php else: ?>
                                                            <div class="col-sm-4" id="div_affects_register">
                                                                <?= lang('payment_affects_register', 'payment_affects_register') ?><br>
                                                                <label>
                                                                  <input type="radio" name="payment_affects_register" id="payment_affects_register" value="1">
                                                                  <?= lang('affects_register') ?>
                                                                </label>
                                                                <label>
                                                                  <input type="radio" name="payment_affects_register" id="payment_no_affects_register" value="0">
                                                                  <?= lang('no_affects_register') ?>
                                                                </label>
                                                                <label for="payment_affects_register" class="error">* Obligatorio</label>
                                                            </div>
                                                        <?php endif ?>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <?= lang("paying_by", "paid_by_1"); ?>
                                                                <select name="paid_by[]" id="paid_by_1" class="form-control paid_by" data-pbnum="1">
                                                                    <option value="expense_causation"><?= lang('expense_causation') ?></option>
                                                                    <?= $this->sma->paid_opts(null, true); ?>
                                                                </select>
                                                                <input type="hidden" name="due_payment[]" class="due_payment_1">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="payment">
                                                                <div class="form-group ngc">
                                                                    <?= lang("amount", "amount_1"); ?>
                                                                    <input name="amount-paid[]" type="text" id="amount_1"
                                                                        class="pa form-control kb-pad amount"/>
                                                                </div>
                                                                <div class="form-group gc" style="display: none;">
                                                                    <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                    <input name="gift_card_no[]" type="text" id="gift_card_no"
                                                                        class="pa form-control kb-pad"/>

                                                                    <div id="gc_details"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4 div_popayment_term_1" style="display:none;">
                                                            <div class="form-group">
                                                                <?= lang("payment_term", "popayment_term_1"); ?>
                                                                <?php echo form_input('payment_term[]', $inv->payment_term, 'class="form-control tip purchase_payment_term" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="popayment_term_1"'); ?>
                                                                <input type="hidden" name="supplierpaymentterm" id="supplierpaymentterm">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="pcc_1" style="display:none;">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_no[]" type="text" id="pcc_no_1"
                                                                        class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_holder[]" type="text" id="pcc_holder_1"
                                                                        class="form-control"
                                                                        placeholder="<?= lang('cc_holder') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <select name="pcc_type[]" id="pcc_type_1"
                                                                            class="form-control pcc_type"
                                                                            placeholder="<?= lang('card_type') ?>">
                                                                        <option value="Visa"><?= lang("Visa"); ?></option>
                                                                        <option
                                                                            value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                        <option value="Amex"><?= lang("Amex"); ?></option>
                                                                        <option value="Discover"><?= lang("Discover"); ?></option>
                                                                    </select>
                                                                    <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input name="pcc_month[]" type="text" id="pcc_month_1"
                                                                        class="form-control" placeholder="<?= lang('month') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <input name="pcc_year[]" type="text" id="pcc_year_1"
                                                                        class="form-control" placeholder="<?= lang('year') ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <input name="pcc_ccv[]" type="text" id="pcc_cvv2_1"
                                                                        class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pcheque_1" style="display:none;">
                                                        <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                            <input name="cheque_no[]" type="text" id="cheque_no_1"
                                                                class="form-control cheque_no"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang('payment_note', 'payment_note_1'); ?>
                                                        <textarea name="payment_note[]" id="payment_note_1"
                                                                class="pa form-control kb-text payment_note"></textarea>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                            <?php endif ?>

                                    </div>
                                    <div id="" class="well well-sm" style="margin-bottom: 0;">
                                        <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                            <tr class="warning">
                                                <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                                <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                                <!-- <td>IVA <span class="totals_val pull-right" id="ivaamount">0.00</span></td> -->
                                                <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                                                <?php if ($Settings->tax2) { ?>
                                                    <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                                                <?php } ?>
                                                <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                                                <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <?= lang("note", "ponote"); ?>
                                        <?php echo form_textarea('note', $note_p, 'class="form-control" id="ponote" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <b class="text-danger error_debug" style="display: none;"></b>
                                    <div class="from-group">
                                        <button type="button" class="btn btn-primary" id="add_purchase" style="padding: 6px 15px; margin:15px 0;"><?= lang('submit') ?></button>
                                        <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                    </div>
                                </div>
                            </div>


<!-- retencion -->

<div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                      class="fa fa-2x">&times;</i></button>
                                      <h4 class="modal-title" id="rtModalLabel"><?=lang('apply_order_retention');?></h4>
                                    </div>
                                    <div class="modal-body">
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>Retención</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>Base</label>
                                        </div>
                                        <div class="col-sm-1">
                                          <label>Asumida</label>
                                        </div>
                                        <div class="col-sm-3">
                                          <label>Opción</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>%</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>Valor</label>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                                          </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" name="input_rete_fuente_base" id="input_rete_fuente_base" class="form-control">
                                        </div>
                                        <div class="col-sm-1">
                                          <label>
                                            <input type="checkbox" name="rete_fuente_assumed" class="skip" id="rete_fuente_assumed">
                                          </label>
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                          <input type="hidden" name="rete_fuente_assumed_account" id="rete_fuente_assumed_account">
                                          <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                          <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_fuente_valor_show" id="rete_fuente_valor_show" class="form-control text-right" readonly>
                                          <input type="hidden" name="rete_fuente_valor" id="rete_fuente_valor">
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> Fuente 2
                                          </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" name="input_rete_otros_base" id="input_rete_otros_base" class="form-control">
                                        </div>
                                        <div class="col-sm-1">
                                          <label>
                                            <input type="checkbox" name="rete_otros_assumed" class="skip" id="rete_otros_assumed">
                                          </label>
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                          <input type="hidden" name="rete_otros_assumed_account" id="rete_otros_assumed_account">
                                          <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_otros_valor_show" id="rete_otros_valor_show" class="form-control text-right" readonly>
                                          <input type="hidden" name="rete_otros_valor" id="rete_otros_valor">
                                          <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>Retención</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>Asumida</label>
                                        </div>
                                        <div class="col-sm-3">
                                          <label>Opción</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>%</label>
                                        </div>
                                        <div class="col-sm-3">
                                          <label>Valor</label>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                                          </label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_iva_assumed" class="skip" id="rete_iva_assumed">
                                          </label>
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                          <input type="hidden" name="rete_iva_assumed_account" id="rete_iva_assumed_account">
                                          <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="rete_iva_valor_show" id="rete_iva_valor_show" class="form-control text-right" readonly>
                                          <input type="hidden" name="rete_iva_valor" id="rete_iva_valor">
                                          <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                                          </label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_ica_assumed" class="skip" id="rete_ica_assumed">
                                          </label>
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                          <input type="hidden" name="rete_ica_assumed_account" id="rete_ica_assumed_account">
                                          <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="rete_ica_valor_show" id="rete_ica_valor_show" class="form-control text-right" readonly>
                                          <input type="hidden" name="rete_ica_valor" id="rete_ica_valor">
                                          <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_bomberil" class="skip" id="rete_bomberil" disabled> <?= lang('rete_bomberil') ?>
                                          </label>
                                        </div>
                                        <div class="col-sm-5">
                                          <select class="form-control" name="rete_bomberil_option" id="rete_bomberil_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_bomberil_account" id="rete_bomberil_account">
                                          <input type="hidden" name="rete_bomberil_assumed_account" id="rete_bomberil_assumed_account">
                                          <input type="hidden" name="rete_bomberil_base" id="rete_bomberil_base">
                                          <input type="hidden" name="rete_bomberil_id" id="rete_bomberil_id">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_bomberil_tax" id="rete_bomberil_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="rete_bomberil_valor" id="rete_bomberil_valor" class="form-control text-right" readonly>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_autoaviso" class="skip" id="rete_autoaviso" disabled> <?= lang('rete_autoaviso') ?>
                                          </label>
                                        </div>
                                        <div class="col-sm-5">
                                          <select class="form-control" name="rete_autoaviso_option" id="rete_autoaviso_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_autoaviso_account" id="rete_autoaviso_account">
                                          <input type="hidden" name="rete_autoaviso_assumed_account" id="rete_autoaviso_assumed_account">
                                          <input type="hidden" name="rete_autoaviso_base" id="rete_autoaviso_base">
                                          <input type="hidden" name="rete_autoaviso_id" id="rete_autoaviso_id">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_autoaviso_tax" id="rete_autoaviso_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="rete_autoaviso_valor" id="rete_autoaviso_valor" class="form-control text-right" readonly>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-8 text-right">
                                          <label>Total : </label>
                                        </div>
                                        <div class="col-md-4 text-right">
                                          <label id="total_rete_amount" style="display: none;"> 0.00 </label>
                                          <label id="total_rete_amount_show"> 0.00 </label>
                                        </div>
                                        <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                                      </div>


                                      <!-- <div class="form-group">
                                        <?=lang("order_tax", "order_tax_input");?>
                                        <?php
                                        $tr[""] = "";
                                        foreach ($tax_rates as $tax) {
                                          $tr[$tax->id] = $tax->name;
                                        }
                                        echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                                        ?>
                                      </div> -->
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
                                      <button type="button" id="cancelOrderRete" class="btn btn-danger" data-dismiss="modal"><?= lang('reset') ?></button>
                                      <button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
                                    </div>
                                  </div>
                                </div>
                            </div>
<!-- retencion -->


<div class="modal fade in" id="sPUModal" tabindex="-1" role="dialog" aria-labelledby="sPUModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_spumodal"></h2>
                <h3><?= lang('unit_prices') ?></h3>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 10%;"></th>
                            <th style="width: 35%;"><?= lang('unit') ?></th>
                            <th style="width: 20%;"><?= lang('quantity') ?></th>
                        </tr>
                    </thead>
                </table>
                <table class="table" id="unit_prices_table" style="width: 100%;">
                    <tbody>

                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <div class="col-sm-3">
                    <?= lang('unit_quantity', 'unit_quantity') ?>
                    <input type="text" name="unit_quantity" id="unit_quantity" class="form-control">
                </div>
                <div class="col-sm-9">
                    <button class="btn btn-success send_item_unit_select" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

                            <?php echo form_close(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
                <h4 class="modal-title" id="prModalLabelCode"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if (!isset($quote) || (isset($quote) && $quote->quote_type == 1)): ?>
                        <div class="edit_purchase">
                            <?php if ($this->Settings->purchases_products_supplier_code == 1): ?>
                                <div class="form-group pedit_input_group">
                                    <label for="psupplier_part_no" class="col-sm-4"><?= lang('supplier_part_no') ?></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="psupplier_part_no" required>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="form-group">
                                <label for="punit" class="col-sm-4"><?= lang('purchase_product_unit') ?></label>
                                <div class="col-sm-8">
                                    <div id="punits-div"></div>
                                </div>
                            </div>
                            <?php if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10): ?>
                                <div class="form-group pedit_input_group">
                                    <label for="punit_quantity" class="col-sm-4"><?= lang('purchase_product_unit_quantity') ?><span class="um_name"></span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="punit_quantity" readonly>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="form-group">
                                <label for="pquantity" class="col-sm-4"><?= ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? lang('purchase_product_quantity') : lang('quantity') ?><span class="um_name"></span></label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="pquantity">
                                </div>
                            </div>
                            <div class="form-group variants_div">
                                <label for="poption" class="col-sm-4"><?= lang('product_option') ?></label>
                                <div class="col-sm-8">
                                    <div id="poptions-div"></div>
                                </div>
                            </div>
                            <?php if ($Settings->tax1) { ?>
                                <div class="form-group">
                                    <label class="col-sm-4"><?= lang('product_tax') ?></label>
                                    <div class="col-sm-8">
                                        <select name="ptax" id="ptax" class="form-control" readonly>
                                            <?php foreach ($tax_rates as $tax): ?>
                                                <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <br>
                                        <em class="tax_changed_warning" style="display:none;"><?= lang('tax_changed_warning') ?></em>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->Settings->ipoconsumo == 2 || $this->Settings->ipoconsumo == 1): ?>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="control-label"><?= lang('second_product_tax') ?></label>
                                    </div>
                                    <div class="col-sm-8 ptax2_div">
                                        <input type="text" name="ptax2" id="ptax2" class="form-control" readonly>
                                    </div>
                                    <div class="col-sm-4 ptax2_percentage_div" style="display:none;">
                                        <input type="text" name="ptax2_percentage" id="ptax2_percentage" class="form-control" readonly>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if ($Settings->product_serial) { ?>
                                <div class="form-group">
                                    <label for="pserial" class="col-sm-4"><?= lang('serial_no') ?></label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="pserial">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($Settings->product_expiry) { ?>
                                <div class="form-group">
                                    <label for="pexpiry" class="col-sm-4"><?= lang('product_expiry') ?></label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control date" id="pexpiry">
                                    </div>
                                </div>
                            <?php } ?>
                            <!--  -->

                            <div class="form-group">
                                <label class="col-sm-4"></label>
                                <div class="col-sm-4">
                                    <label class=""><?= lang('exclusive') ?></label>
                                </div><div class="col-sm-4">
                                    <label class=""><?= lang('inclusive') ?></label>
                                </div>
                            </div>

                            <?php if ($this->Settings->precios_por_unidad_presentacion == 1 && ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10)): ?>
                                <div class="form-group">
                                    <label for="pcost_um" class="col-sm-4"><?= lang('unit_cost_um') ?> <span class="um_name"></span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pcost_um_net" <?= (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) ? 'readonly' : '' ?>>
                                    </div><div class="col-sm-4">
                                        <input type="text" class="form-control" id="pcost_um" <?= (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) ? 'readonly' : '' ?>>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if ($this->Settings->precios_por_unidad_presentacion == 1 && ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10)): ?>
                                <div class="form-group">
                                    <label for="pcost_um_total" class="col-sm-4"><?= lang('total_cost_um') ?> <span class="um_name"></span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pcost_um_total_net" <?= (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) ? 'readonly' : '' ?>>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pcost_um_total" <?= (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) ? 'readonly' : '' ?>>
                                    </div>
                                </div>
                            <?php endif ?>

                            <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "style='display:none;'" : "" ?>>
                                <label for="pcost" class="col-sm-4"><?= lang('unit_cost') ?></label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="pcost_net" <?= (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) ? 'readonly' : '' ?>>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="pcost" <?= (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) ? 'readonly' : '' ?>>
                                </div>
                            </div>
                            
                            <?php if ($Settings->product_discount && ($this->Admin || $this->Owner || $this->GP['products-cost'])) { ?>
                                <div class="form-group">
                                    <label for="pdiscount" class="col-sm-4"><?= lang('product_unit_discount') ?>
                                        <i class="fa fa-exclamation-circle error_order_discount text-danger" data-toggle="tooltip" data-placement="top" title="<?= lang('order_discount_affecting_tax_already_applied') ?>"></i></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="pdiscount"  <?= (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) ? 'readonly' : '' ?>>
                                    </div>
                                </div>
                            <?php } ?>
                            <!--  -->
                            <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "" : "style='display:none;'" ?>>
                                <label for="pproduct_unit_cost" class="col-sm-4"><?= lang('purchase_form_edit_product_unit_cost') ?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="pproduct_unit_cost">
                                </div>
                            </div>
                            <?php if ($Settings->ipoconsumo): ?>
                                <!-- <div class="form-group">
                                    <label for="pcost_ipoconsumo" class="col-sm-4"><?= lang('unit_cost_ipoconsumo') ?></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="pcost_ipoconsumo"><br>
                                        <em><?= lang('unit_cost_ipoconsumo_detail') ?></em>
                                    </div>
                                </div> -->
                            <?php endif ?>
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width:25%;"><?= lang('net_unit_cost'); ?></th>
                                    <th style="width:25%;"><span id="net_cost"></span></th>
                                    <th style="width:25%;"><?= lang('product_taxes'); ?></th>
                                    <th style="width:25%;"><span id="pro_tax"></span></th>
                                </tr>
                            </table>
                            <!-- <div class="panel panel-default">
                                <div class="panel-heading"><?= lang('calculate_unit_cost'); ?></div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label for="pcost" class="col-sm-4"><?= lang('subtotal_before_tax') ?></label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="psubtotal">
                                                <div class="input-group-addon" style="padding: 2px 8px;">
                                                    <a href="#" id="calculate_unit_price" class="tip" title="<?= lang('calculate_unit_cost'); ?>">
                                                        <i class="fa fa-calculator"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <?php if (!isset($quote)): ?>
                            <div class="edit_purchase_expense" style="display: none;">
                                <div class="form-group pname-div">
                                    <label for="pname" class="col-sm-4 control-label"><?= lang('name') ?></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="pname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pcost" class="col-sm-4 control-label"><?= lang('expense_cost') ?></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="pcost">
                                    </div>
                                </div>
                                <?php if ($Settings->tax1) { ?>
                                    <div class="form-group">
                                        <label class="col-sm-12"><?= lang('tax_1') ?></label>
                                        <div class="col-sm-6">
                                            <?php
                                            $tr[""] = lang('n/a');
                                            foreach ($tax_rates as $tax) {
                                                $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                            ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= form_input('ptax_value', '', 'id="ptax_value" class="form-control"'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12"><?= lang('tax_2') ?></label>
                                        <div class="col-sm-6">
                                            <?php
                                            $tr[""] = lang('n/a');
                                            foreach ($tax_rates as $tax) {
                                                $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('ptax_2', $tr, "", 'id="ptax_2" class="form-control pos-input-tip" style="width:100%;"');
                                            ?>
                                        </div>

                                        <div class="col-sm-6">
                                            <?= form_input('ptax_2_value', '', 'id="ptax_2_value" class="form-control"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <th style="width:16.66%;"><?= lang('expense_cost'); ?></th>
                                        <th style="width:16.66%;"><span id="net_cost"></span></th>
                                        <th style="width:16.66%;"><?= lang('expense_tax'); ?></th>
                                        <th style="width:16.66%;"><span id="pro_tax"></span></th>
                                        <th style="width:16.66%;"><?= lang('total'); ?></th>
                                        <th style="width:16.66%;"><span id="pro_total"></span></th>
                                    </tr>
                                </table>
                            </div>
                        <?php endif ?>
                        <?php if ($this->Settings->update_prices_from_purchases == 1 &&($this->Owner || $this->Admin || $this->GP['system_settings-update_products_group_prices'])): ?>
                            <div class="div_update_prices">
                                <div class="col-sm-12">
                                    <h3>Actualización de precios</h3>
                                </div>
                                <?php if ($this->Settings->prioridad_precios_producto == 1 || $this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) { ?>
                                    <?php foreach ($units as $pg): ?>
                                        <div class="row">
                                            <div class="col-sm-12 pgprice_div" style="display:none;" data-pgid="<?= $pg->id ?>">
                                                <label><?= $pg->name ?></label>
                                            </div>
                                            <div class="col-sm-4 pgprice_div" style="display:none;" data-pgid="<?= $pg->id ?>">
                                                <label>Actual</label>
                                                <input type="text" name="pgprice" data-pgid="<?= $pg->id ?>" class="form-control pgprice" readonly>
                                            </div>
                                            <div class="col-sm-4 pgprice_div" style="display:none;" data-pgid="<?= $pg->id ?>">
                                                <label>Margen</label>
                                                <input type="text" name="pgmargin" data-pgid="<?= $pg->id ?>" class="form-control pgmargin">
                                            </div>
                                            <div class="col-sm-4 pgprice_div" style="display:none;" data-pgid="<?= $pg->id ?>">
                                                <label>Nuevo</label>
                                                <input type="text" name="pprice_group" data-pgid="<?= $pg->id ?>" class="form-control pgnew_price">
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                <?php } else { ?>
                                    <?php foreach ($price_groups as $pg): ?>
                                        <div class="col-sm-6">
                                            <label><?= $pg->name ?></label>
                                            <input type="text" name="pprice_group" data-pgid="<?= $pg->id ?>" class="form-control pgprice">
                                        </div>
                                    <?php endforeach ?>
                                <?php } ?>
                            </div>

                            <hr class="col-sm-11">
                        <?php endif ?>
                    <?php elseif(isset($quote) && $quote->quote_type == 2): ?>
                        <div class="edit_purchase_expense">
                            <?php if ($Settings->tax1) { ?>
                                <div class="form-group">
                                    <label class="col-sm-12"><?= lang('tax_1') ?></label>
                                    <div class="col-sm-6">
                                        <?php
                                        $tr[""] = lang('n/a');
                                        foreach ($tax_rates as $tax) {
                                            $tr[$tax->id] = $tax->name;
                                        }
                                        echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= form_input('ptax_value', '', 'id="ptax_value" class="form-control"'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12"><?= lang('tax_2') ?></label>
                                    <div class="col-sm-6">
                                        <?php
                                        $tr[""] = lang('n/a');
                                        foreach ($tax_rates as $tax) {
                                            $tr[$tax->id] = $tax->name;
                                        }
                                        echo form_dropdown('ptax_2', $tr, "", 'id="ptax_2" class="form-control pos-input-tip" style="width:100%;"');
                                        ?>
                                    </div>

                                    <div class="col-sm-6">
                                        <?= form_input('ptax_2_value', '', 'id="ptax_2_value" class="form-control"'); ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width:16.66%;"><?= lang('expense_cost'); ?></th>
                                    <th style="width:16.66%;"><span id="net_cost"></span></th>
                                    <th style="width:16.66%;"><?= lang('expense_tax'); ?></th>
                                    <th style="width:16.66%;"><span id="pro_tax"></span></th>
                                    <th style="width:16.66%;"><?= lang('total'); ?></th>
                                    <th style="width:16.66%;"><span id="pro_total"></span></th>
                                </tr>
                            </table>
                        </div>
                    <?php endif ?>

                    <input type="hidden" id="punit_cost" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_cost" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                    <input type="hidden" id="pcost" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_standard_product') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <div class="alert alert-danger" id="mError-con" style="display: none;">
                    <!--<button data-dismiss="alert" class="close" type="button">×</button>-->
                    <span id="mError"></span>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <?= lang('product_code', 'mcode') ?> *
                            <input type="text" class="form-control" id="mcode">
                        </div>
                        <div class="form-group">
                            <?= lang('product_name', 'mname') ?> *
                            <input type="text" class="form-control" id="mname">
                        </div>
                        <div class="form-group">
                            <?= lang('category', 'mcategory') ?> *
                            <?php
                            $cat[''] = "";
                            foreach ($categories as $category) {
                                $cat[$category->id] = $category->name;
                            }
                            echo form_dropdown('category', $cat, '', 'class="form-control select" id="mcategory" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
                            ?>
                        </div>
                        <div class="form-group">
                            <?= lang('unit', 'munit') ?> *
                            <input type="text" class="form-control" id="munit">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <?= lang('cost', 'mcost') ?> *
                            <input type="text" class="form-control" id="mcost">
                        </div>
                        <div class="form-group">
                            <?= lang('price', 'mprice') ?> *
                            <input type="text" class="form-control" id="mprice">
                        </div>

                        <?php if ($Settings->tax3) { ?>
                            <div class="form-group">
                                <?= lang('product_tax', 'mtax') ?>
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                            <div class="form-group all">
                                <?= lang("tax_method", "mtax_method") ?>
                                <?php
                                $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                                echo form_dropdown('tax_method', $tm, '', 'class="form-control select" id="mtax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"')
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_serialModal"></h2>
                <h3><?= lang('ticket_data') ?></h3>
            </div>
            <div class="modal-body">
                <form id="serialModal_form">
                    <input type="hidden" id="serialModal_product_id">
                    <div class="col-sm-12 form-group">
                        <?= lang('serial_no', 'serial') ?>
                        <div class="input-group">
                            <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                            <span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;">
                                <i class="fa fa-random"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= lang('meters', 'meters') ?>
                        <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($this->Settings->management_weight_in_variants == 1): ?>
    <div class="modal fade in" id="optionWeightModal" tabindex="-1" role="dialog" aria-labelledby="optionWeightModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="product_name_optionWeightModal"></h2>
                    <h3><?= lang('option_weight_data') ?></h3>
                </div>
                <div class="modal-body">
                    <form id="optionWeightModal_form">
                        <input type="hidden" id="optionWeightModal_product_id">
                        <div class="col-sm-12 form-group">
                            <?= lang('product_variant', 'product_variant_weight') ?>
                            <select class="form-control" name="product_variant_weight" id="product_variant_weight" required>
                                <option value=""><?= lang('select') ?></option>
                                <?php if ($variants): ?>
                                    <?php foreach ($variants as $variant): ?>
                                        <?php

                                        if (isset($pv_setted[$variant->name])) {
                                            continue;
                                        }

                                         ?>
                                        <option data-weight="<?= $variant->weight ?>" value="<?= $variant->id ?>"><?= $variant->name ?></option>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </select>
                            <br>
                            <br>
                            <em class="text-danger error_option_weight" style="display:none;"><?= lang('option_weight_not_relatione') ?></em>
                        </div>
                        <div class="col-sm-12 form-group div_option_weight_not_relationed" style="display:none;">
                            <div class="input-group">
                                <?= form_input('code', '', 'class="form-control validate_code" data-module="variant" id="product_variant_weight_code"') ?>
                                <span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('random_code_text') ?>">
                                    <i class="fa fa-random"></i>
                                </span>
                                <span class="input-group-addon pointer" id="consecutive_code" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('consecutive_code_text') ?>">
                                    <b>123</b>
                                </span>
                            </div>
                            <input type="hidden" name="product_variant_weight_consecutive" id="product_variant_weight_consecutive" class="code_consecutive_setted">
                            <em class="text-warning"><?= sprintf(lang('option_weight_not_relationed'), lang('product_variant')) ?></em>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <div class="col-sm-12 form-group">
                        <button class="btn btn-success send_option_weight" type="button"><?= lang('submit') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

<script type="text/javascript">
    if ("<?= $edit_status ?>" == 0) {
        setTimeout(function() {
            $('#postatus').select2('readonly', true);
            // $('#powarehouse').select2('readonly', true);
        }, 2000);
    }

    $(document).on('change', '#pobiller', function (e) {
        var document_type = 5;
        var payment_document_type = 20;
        if ("<?= $inv->purchase_type ?>" == 2) {
            document_type = 21;
            payment_document_type = 23;
        } else if ($('#purchase_type').val() == 3) {
            document_type = 55;
            payment_document_type = 23;
        }
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/") ?>'+document_type+'/'+$('#pobiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;
          $('#document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                                "showDuration": "500",
                                "hideDuration": "1000",
                                "timeOut": "6000",
                                "extendedTimeOut": "1000",
                            });
                }
              if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
              }
            $('#document_type_id').select2('val', '<?= $inv->document_type_id ?>').trigger('change');
        });

        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/") ?>'+payment_document_type+'/'+$('#pobiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;
          $('#payment_reference_no').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
              if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
              }
            $('#payment_reference_no').trigger('change');
        });
        localStorage.setItem('pobiller', $('#pobiller').val());
    });

    function add_item_unit(item_id, warehouse_id){
        // $('#sPModal').modal('hide');

        var ooTable = $('#unit_prices_table').dataTable({
            aaSorting: [[1, "asc"]],
            aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true, 'bServerSide': true,
            sAjaxSource: site.base_url+"purchases/itemSelectUnit/"+item_id+"/"+warehouse_id,
            "bDestroy": true,
            fnServerData: function (sSource, aoData, fnCallback)
            {
            aoData.push({
              name: "<?= $this->security->get_csrf_token_name() ?>",
              value: "<?= $this->security->get_csrf_hash() ?>"
            });
            $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback });
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                var oSettings = ooTable.fnSettings();

                nRow.setAttribute('data-itemunitid', aData[3]);
                nRow.setAttribute('data-productid', aData[4]);
                nRow.setAttribute('data-productunitid', aData[5]);
                nRow.className = "add_item_unit";

                return nRow;
            },
            aoColumns: [
            {bSortable: false, "mRender": radio_2},
            {bSortable: false},
            {bSortable: false, className : 'text-right'},
            {bSortable: false,  bVisible: false},
            {bSortable: false,  bVisible: false},
            ],
            initComplete: function(settings, json) {
                $('#sPUModal').modal('show');
            }
            }).dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", mdata: []},
            {column_number: 1, filter_default_label: "[<?=lang('price');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
            ], "footer");

            $('#unit_prices_table thead').remove();
    }

    $(document).on('click', '.add_item_unit', function() {

        var product_id = $(this).data('productid');
        var unit_price_id = $(this).data('itemunitid');
        var product_unit_id = $(this).data('productunitid');

        var unit_data = {
                            'product_id' : product_id,
                            'unit_price_id' : unit_price_id,
                            'product_unit_id' : product_unit_id,
                        };

        localStorage.setItem('unit_data', JSON.stringify(unit_data));

        $('#unit_quantity').val(1).select();

    });

    $(document).on('keyup', '#unit_quantity', function(e){
        if (e.keyCode == 13) {
            if (unit_data = JSON.parse(localStorage.getItem('unit_data'))) {
                localStorage.removeItem('unit_data');
                var warehouse_id = $('#powarehouse').val();
                var unit_quantity = $(this).val();
                $.ajax({
                    url:site.base_url+"purchases/iusuggestions",
                    type:"get",
                    data:{
                            'product_id' : unit_data.product_id,
                            'unit_price_id' : unit_data.unit_price_id,
                            'product_unit_id' : unit_data.product_unit_id,
                            'warehouse_id' : warehouse_id,
                            'unit_quantity' : unit_quantity,
                            'biller_id' : $('#pobiller').val(),
                            'supplier_id' : $('#posupplier').val(),
                        }
                }).done(function(data){
                    add_purchase_item(data);
                    $('#sPUModal').modal('hide');
                });
            } else {
                $('#unit_quantity').val(1).select();
            }
        }
    });

    $('#sPUModal').on('shown.bs.modal', function () {
        // $('#unit_prices_table_filter .input-xs').focus();
        $('#unit_prices_table_length').remove();
        $('#unit_prices_table_filter').remove();
        $('#unit_prices_table_info').remove();
        $('#unit_prices_table_paginate').remove();
        $('#unit_quantity').val(1);
        $('.select_auto_2:first').iCheck('check').focus();
    });

    $(document).on('hide.bs.modal', '#sPUModal', function (e) {
      e.stopPropagation() // stops modal from being shown
    });

    $(document).on('ifClicked', '.select_auto_2', function(e){
        var index = $('.select_auto_2').index($(this));
        $('.add_item_unit').eq(index).trigger('click');
    });

    $(document).on('keyup', '.select_auto_2', function(e){
            if (e.keyCode == 13) {
                $(this).closest('.add_item_unit').trigger('click');
            }
        });

    $(document).on('click', '.send_item_unit_select', function(){

        var itemunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('itemunitid');
        var productid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productid');
        var productunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productunitid');

        var unit_data = {
                            'product_id' : productid,
                            'unit_price_id' : itemunitid,
                            'product_unit_id' : productunitid,
                        };
        var warehouse_id = $('#powarehouse').val();
        var unit_quantity = $('#unit_quantity').val();
        $.ajax({
            url:site.base_url+"purchases/iusuggestions",
            type:"get",
            data:{
                    'product_id' : unit_data.product_id,
                    'unit_price_id' : unit_data.unit_price_id,
                    'product_unit_id' : unit_data.product_unit_id,
                    'warehouse_id' : warehouse_id,
                    'unit_quantity' : unit_quantity,
                    'biller_id' : $('#pobiller').val(),
                    'supplier_id' : $('#posupplier').val(),
                }
        }).done(function(data){
            add_purchase_item(data);
            $('#sPUModal').modal('hide');
        });

    });

    $(document).ready(function(){
        setTimeout(function() {
        $('#pobiller').select2('val', '<?= $inv->biller_id ?>').trigger('change');
        <?php if ($inv->status != 'pending' && ($product_has_movements_after_date || $purchase_has_multi_payments)): ?>
            $('#pobiller').select2('readonly', true);
            $('#powarehouse').select2('readonly', true);
            $('#document_type_id').select2('readonly', true);
        <?php endif ?>
        }, 1000);
        <?php if ($this->Settings->ignore_purchases_edit_validations == 1): ?>
            header_alert('error', 'Parámetro activo para ignorar validaciones de edición de compra aprobada, los cambios que realice podrán causar inconsistencia de datos como costo promedio y control de inventario', 60);
        <?php endif ?>
        <?php if ($inv->status == 'received' && !$product_has_movements_after_date && !$purchase_has_multi_payments): ?>
            header_alert('success', 'La compra está aprobada sin movimientos posteriores en productos ni pagos posteriores, datos de encabezado habilitados para edición', 30);
        <?php endif ?>
        <?php if ($inv->status == 'received'): ?>
            header_alert('warning', 'La compra está aprobada, cambios de fecha, bodega o costos pueden generar inconsistencias de costos promedio e inventarios', 30);
        <?php endif ?>
        <?php if ($inv->status == 'received' && ($product_has_movements_after_date || $purchase_has_multi_payments)): ?>
            header_alert('error', 'La compra está aprobada con movimientos posteriores en productos y/o con pagos posteriores, datos de encabezado inhabilitados para edición', 30);
        <?php endif ?>
    });
</script>
