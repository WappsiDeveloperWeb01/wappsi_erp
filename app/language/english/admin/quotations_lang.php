<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Quotations
 * 
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */


$lang['add_quote']                      = "Add Quotation";
$lang['edit_quote']                     = "Edit Quotation";
$lang['delete_quote']                   = "Delete Quotation";
$lang['delete_quotes']                  = "Delete Quotations";
$lang['quote_added']                    = "Quotation successfully added";
$lang['quote_updated']                  = "Quotation successfully updated";
$lang['quote_deleted']                  = "Quotation successfully deleted";
$lang['quotes_deleted']                 = "Quotations successfully deleted";
$lang['quote_details']                  = "Quotation Details";
$lang['email_quote']                    = "Email Quotation";
$lang['view_quote_details']             = "View Quotation Details";
//quote purchase/expenses
$lang['edit_quote_purchase_expense']                     = "Edit purchase/expense order";
$lang['delete_quote_purchase_expense']                   = "Delete purchase/expense order";
$lang['delete_quotes_purchase_expense']                  = "Delete  purchase/expense orders";
$lang['quote_purchase_expense_added']                    = "The purchase/expense order has been added successfully";
$lang['quote_purchase_expense_updated']                  = "The purchase/expense order has been updated succesfully";
$lang['quote_purchase_expense_deleted']                  = "The purchase/expense order has been deleted succesfully";
$lang['quotes_purchase_expense_deleted']                 = "The purchase/expense orders have been deleted succesfully";
$lang['quote_purchase_expense_details']                  = "Purchase/expense order details";
$lang['email_quote_purchase_expense']                    = "Purchase/expense order email";
$lang['view_quote_purchase_expense_details']             = "View purchase/expense order details";
//quote purchase/expenses
$lang['quote_no']                       = "Quotation No";
$lang['send_email']                     = "Send Email";
$lang['quote_items']                    = "Quotation Items";
$lang['no_quote_selected']              = "No quotation selected. Please select at least one quotation.";
$lang['create_sale']                    = "Create Sale";
$lang['create_purchase']                = "Create Purchase";
$lang['create_invoice']                 = "Create Sale";


$lang['type_quote_purchase']			="Purchase order type";
$lang['type_quote_purchase_products']			="Products";
$lang['type_quote_purchase_expenses']			="Expenses";

//NO TRADUCIDO

$lang['stamp_sign'] = 'Stamp & signature'; 
$lang['cant_create_to'] = 'Cannot create %s , please check Purchase order'; 
$lang['quote_purchase'] = 'Purchase Order'; 
$lang['quote_expense'] 	= 'Expense Order'; 

$lang['choose_quote_currency_view']				="Choose the currency to view Purchase Order.";