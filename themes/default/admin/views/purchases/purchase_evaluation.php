<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    <style>
         .rating {
            direction: ltr; /* Establece que la calificación sea de izquierda a derecha */
        }

        .star {
            font-size: 2em; /* Tamaño de las estrellas */
            color: #ddd; /* Color gris para las estrellas no seleccionadas */
            cursor: pointer;
        }

        .star.selected {
            color: gold; /* Color dorado para las estrellas seleccionadas */
        }
    </style>
</style>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('purchase_evaluation'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("purchases/purchase_evaluation/" . $inv->id, $attrib); ?>
        <div class="modal-body">
        <?= lang('purchase_details'); ?>
        <div class="row">
            <div class="col-sm-4">
                <label><?= lang('reference_no'); ?></label><br>
                <?= $inv->reference_no; ?>
            </div>
            <div class="col-sm-4">
                <label><?= lang('supplier'); ?></label><br>
                <?= $inv->supplier; ?>
            </div>
            <div class="col-sm-4">
                <label><?= lang('warehouse'); ?></label><br>
                <?= $warehouse->name; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label><?= lang('status'); ?></label><br>
                <?= lang($inv->status); ?>
            </div>
            <div class="col-sm-4">
                <label><?= lang('payment_status'); ?></label><br>
                <?= lang($inv->payment_status); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <hr class="col-sm-11">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h3><?= lang('returns') ?></h3>
                <div class="rating">
                    <i class="star fa fa-star returns selected" data-value="1" data-evaluation="returns"></i>
                    <i class="star fa fa-star returns" data-value="2" data-evaluation="returns"></i>
                    <i class="star fa fa-star returns" data-value="3" data-evaluation="returns"></i>
                    <i class="star fa fa-star returns" data-value="4" data-evaluation="returns"></i>
                </div>
            </div>
            <div class="col-sm-6">
                <h3><?= lang('delivery_supply') ?></h3>
                <div class="rating">
                    <i class="star fa fa-star delivery_supply selected" data-value="1" data-evaluation="delivery_supply"></i>
                    <i class="star fa fa-star delivery_supply" data-value="2" data-evaluation="delivery_supply"></i>
                    <i class="star fa fa-star delivery_supply" data-value="3" data-evaluation="delivery_supply"></i>
                    <i class="star fa fa-star delivery_supply" data-value="4" data-evaluation="delivery_supply"></i>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h3><?= lang('discount_variations') ?></h3>
                <div class="rating">
                    <i class="star fa fa-star discount_variations selected" data-value="1" data-evaluation="discount_variations"></i>
                    <i class="star fa fa-star discount_variations" data-value="2" data-evaluation="discount_variations"></i>
                    <i class="star fa fa-star discount_variations" data-value="3" data-evaluation="discount_variations"></i>
                    <i class="star fa fa-star discount_variations" data-value="4" data-evaluation="discount_variations"></i>
                </div>
            </div>
            <div class="col-sm-6">
                <h3><?= lang('pqrs') ?></h3>
                <div class="rating">
                    <i class="star fa fa-star pqrs selected" data-value="1" data-evaluation="pqrs"></i>
                    <i class="star fa fa-star pqrs" data-value="2" data-evaluation="pqrs"></i>
                    <i class="star fa fa-star pqrs" data-value="3" data-evaluation="pqrs"></i>
                    <i class="star fa fa-star pqrs" data-value="4" data-evaluation="pqrs"></i>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h3><?= lang('terms_of_payments') ?></h3>
                <div class="rating">
                    <i class="star fa fa-star terms_of_payments selected" data-value="1" data-evaluation="terms_of_payments"></i>
                    <i class="star fa fa-star terms_of_payments" data-value="2" data-evaluation="terms_of_payments"></i>
                    <i class="star fa fa-star terms_of_payments" data-value="3" data-evaluation="terms_of_payments"></i>
                    <i class="star fa fa-star terms_of_payments" data-value="4" data-evaluation="terms_of_payments"></i>
                </div>
            </div>
            <div class="col-sm-6">
                <h3><?= lang('warranty') ?></h3>
                <div class="rating">
                    <i class="star fa fa-star warranty selected" data-value="1" data-evaluation="warranty"></i>
                    <i class="star fa fa-star warranty" data-value="2" data-evaluation="warranty"></i>
                    <i class="star fa fa-star warranty" data-value="3" data-evaluation="warranty"></i>
                    <i class="star fa fa-star warranty" data-value="4" data-evaluation="warranty"></i>
                </div>
            </div>
        </div>
        <input type="hidden" name="returns" id="returns" value="1">
        <input type="hidden" name="delivery_supply" id="delivery_supply" value="1">
        <input type="hidden" name="discount_variations" id="discount_variations" value="1">
        <input type="hidden" name="pqrs" id="pqrs" value="1">
        <input type="hidden" name="terms_of_payments" id="terms_of_payments" value="1">
        <input type="hidden" name="warranty" id="warranty" value="1">

        </div>
        <div class="modal-footer">
            <?php if (!$evaluation): ?>
                <?php echo form_submit('update', lang('submit'), 'class="btn btn-primary"'); ?>
            <?php else: ?>
                <b><?= lang('evaluation_date') ?></b> : <?= $evaluation->registration_date ?> <br>
                <b><?= lang('evaluated_by') ?></b> : <?= ucwords(mb_strtolower($user_evaluator->first_name." ".$user_evaluator->last_name)) ?>
            <?php endif ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script>
    $(document).ready(function(){
        <?php if (!$evaluation): ?>
            $('.star').click(function(){
                var rating = $(this).data('value');
                var evaluation = $(this).data('evaluation');
                $('.star.'+evaluation+'').removeClass('selected'); // Elimina la selección de todas las estrellas
                for (var i = 1; i <= rating; i++) {
                    $('.star.'+evaluation+'[data-value="' + i + '"]').addClass('selected'); // Añade la clase a las estrellas seleccionadas
                }
                $('#'+evaluation).val(rating);
            });
        <?php endif ?>
    });
    <?php if ($evaluation): ?>
        $('.star').removeClass('selected');
        for (var i = 1; i <= <?= $evaluation->returns ?>; i++) {
            $('.star.returns[data-value="' + i + '"]').addClass('selected'); // Añade la clase a las estrellas seleccionadas
        }
        for (var i = 1; i <= <?= $evaluation->delivery_supply ?>; i++) {
            $('.star.delivery_supply[data-value="' + i + '"]').addClass('selected'); // Añade la clase a las estrellas seleccionadas
        }
        for (var i = 1; i <= <?= $evaluation->discount_variations ?>; i++) {
            $('.star.discount_variations[data-value="' + i + '"]').addClass('selected'); // Añade la clase a las estrellas seleccionadas
        }
        for (var i = 1; i <= <?= $evaluation->pqrs ?>; i++) {
            $('.star.pqrs[data-value="' + i + '"]').addClass('selected'); // Añade la clase a las estrellas seleccionadas
        }
        for (var i = 1; i <= <?= $evaluation->terms_of_payments ?>; i++) {
            $('.star.terms_of_payments[data-value="' + i + '"]').addClass('selected'); // Añade la clase a las estrellas seleccionadas
        }
        for (var i = 1; i <= <?= $evaluation->warranty ?>; i++) {
            $('.star.warranty[data-value="' + i + '"]').addClass('selected'); // Añade la clase a las estrellas seleccionadas
        }
        $('#returns').val('<?= $evaluation->returns ?>');
        $('#delivery_supply').val('<?= $evaluation->delivery_supply ?>');
        $('#discount_variations').val('<?= $evaluation->discount_variations ?>');
        $('#pqrs').val('<?= $evaluation->pqrs ?>');
        $('#terms_of_payments').val('<?= $evaluation->terms_of_payments ?>');
        $('#warranty').val('<?= $evaluation->warranty ?>');
    <?php endif ?>
</script>
