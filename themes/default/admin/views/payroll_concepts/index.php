<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $_post = $this->session->flashdata("post") ? $this->session->flashdata("post") : ""; ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-hover" id="payroll_concepts_table"  style="width: 100%">
                                    <thead>
                                        <tr class="primary">
                                            <th></th>
                                            <th><?= lang("payroll_concepts_name"); ?></th>
                                            <th><?= lang("payroll_concepts_type"); ?></th>
                                            <th><?= lang("payroll_concepts_earned_deducted"); ?></th>
                                            <th><?= lang("payroll_concepts_percentage"); ?></th>
                                            <th><?= lang("payroll_concepts_status"); ?></th>
                                            <th></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="11" class="dataTables_empty"><?= lang("loading_data_from_server") ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        load_datatable();
    });

    function load_datatable()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#payroll_concepts_table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('payroll_concepts/get_datatables') ?>',
            dom: '<"col-sm-7 containerBtn"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>',
            fnServerData: function (sSource, aoData, fnCallback)
            {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            drawCallback: function(settings)
            {
                $('.actionsButtonContainer').html('<div class="dropdown pull-right">'+
                    '<a href="<?= admin_url('payroll_concepts/add') ?>" class="btn btn-success new-button" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>'+
                '</div>');

                $('[data-toggle-second="tooltip"]').tooltip();
            },
            "aoColumns": [
                { visible: false },
                null,
                null,
                {
                    className: "text-center",
                    render: function(data, type, row) {
                        if (data == '<?= YES ?>') {
                            data = 'Devengado';
                        } else if (data == '<?= NOT ?>') {
                            data = 'N/A';
                        } else {
                            data ='Deducido';
                        }
                        return '<span class="label label-default">'+ data +'</span>';
                    }
                },
                {
                    className: "text-right",
                    render: function(data, type, row) {
                        return data +' %';
                    }
                },
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        return (data == 1) ? '<span class="label label-primary">Activo</span>': '<span class="label label-danger">Inactivo</span>';
                    }
                },
                { visible: false },
                {
                    bSortable: false,
                    render: function(data, type, row) {
                        var fixed = row[6];
                        var actions = $(row[7]);

                        if (fixed == '<?= YES; ?>') {
                            actions.find('.dropdown-toggle').addClass('disabled');
                        } else {
                            actions.find('.dropdown-toggle').removeClass('disabled');
                        }

                        data = actions.prop('outerHTML');
                        return data;
                    }
                }
            ]
        });
    }
</script>
