<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                        	<?php if ($this->Settings->big_data_limit_reports == 0 || ($this->Settings->big_data_limit_reports == 1 && $can_filter)): ?>
								<?php echo form_open('admin/reports/portfolio_report' , 'id="form_filter"'); ?>
		                            <input type="hidden" name="filter_action" id="filter_action" value="0">
		                            <input type="hidden" name="action" id="action">
										<div class="row">
											<div class="col-xs-12 col-md-3">
												<label for="Sucursal" class="col-sm-3 control-label"> <?= lang('branch') ?></label>
												<?php if ($this->Settings->big_data_limit_reports == 1): ?>
													<select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;" required>
														<option value="" > <?= lang('select').' '.lang('branch') ?> </option>
														<?php foreach ($billers as $biller): ?>
															<option value="<?php echo $biller->id; ?>"><?php echo $biller->name; ?></option>
														<?php endforeach; ?>
													</select>
												<?php else: ?>
													<div class="form-group">
														<?php if ((!$this->session->userdata('biller_id'))): ?>
															<label class="radio-inline">
																<input type="radio" name="valueSucursal" id="biller1" value="Todas" <?= !isset($_POST['valueSucursal']) || (isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
															</label>
														<?php endif ?>
														<label class="radio-inline">
															<input type="radio" name="valueSucursal" id="biller2" value="Una"  <?= ((isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Una') || ($this->session->userdata('biller_id'))) ? 'checked' : '' ?>> <?= lang('aa') ?>
														</label>
													</div>
													
													<?php
					                                $bl[""] = "";
					                                $bldata = [];
					                                foreach ($billers as $biller) {
					                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
					                                    $bldata[$biller->id] = $biller;
					                                }
					                                // exit(var_dump($this->session->userdata('biller_id')));
					                                ?>

					                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
														<div class="collapse" id="collapseBiller">
															<select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;">
																<option value="all" > <?= lang('select').' '.lang('branch') ?> </option>
																<?php foreach ($billers as $biller): ?>
																	<option value="<?php echo $biller->id; ?>"  <?= (isset($_POST['Sucursal']) && $_POST['Sucursal'] == $biller->id ? "selected" : "") ?> ><?php echo $biller->name; ?></option>
																<?php endforeach; ?>
															</select>
														</div>    
					                                <?php } else { ?>
															<select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;">
					                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
					                                                    $biller = $bldata[$this->session->userdata('biller_id')];
					                                                ?>
					                                                    <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
					                                                <?php endif ?>
					                                            </select>
					                                <?php } ?>
													
													
												<?php endif ?>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="inputEmail3" class="col-sm-3 control-label"> <?= lang('seller') ?> </label>
												<div class="form-group">
													<?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
														<label class="radio-inline">
															<input type="radio" name="valueVendedor" id="seller2" value="Un" checked> <?= lang('a').' '.lang('seller') ?>
														</label>
													<?php else: ?>
														<label class="radio-inline">
															<input type="radio" name="valueVendedor" id="seller1" value="Todas" <?= !isset($_POST['valueVendedor']) || (isset($_POST['valueVendedor']) && $_POST['valueVendedor'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
														</label>
														<label class="radio-inline">
															<input type="radio" name="valueVendedor" id="seller2" value="Un" <?= isset($_POST['valueCliente']) || isset($_POST['valueVendedor']) && $_POST['valueVendedor'] == 'Un' ? 'checked' : '' ?>> <?= lang('a').' '.lang('seller') ?>
														</label>
													<?php endif ?>
												</div>
												<div class="collapse" id="collapseSeller">
													<select class="form-control " onchange="Search_Vendedor(this.value);" name="Vendedor" id="Vendedor" style="width:100%;">
														<?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
															<?php foreach ($sellers as $seller): ?>
																<?php if ($seller['cod_seller'] == $this->session->userdata('company_id')): ?>
																	<option value="<?php echo $seller['Document']; ?>"><?php echo $seller['nombre']; ?></option>
																<?php endif ?>
															<?php endforeach; ?>
														<?php else: ?>
															<option value="all" > <?= lang('select').' '.lang('seller') ?> </option>
															<?php foreach ($sellers as $seller): ?>
																<option value="<?php echo $seller['Document']; ?>"><?php echo $seller['nombre']; ?></option>
															<?php endforeach; ?>
														<?php endif ?>
													</select>
												</div>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="inputEmail3" class="col-sm-3 control-label"> <?= lang('view_customer_total') ?> </label>
												<div class="form-group">
													<label class="radio-inline">
														<input type="radio" name="valueCliente" id="Cliente1" value="Todas" <?= !isset($_POST['valueCliente']) ||  (isset($_POST['valueCliente']) && $_POST['valueCliente'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
													</label>
													<label class="radio-inline">
														<input type="radio" name="valueCliente" id="Cliente2" value="Un" <?= isset($_POST['valueCliente']) && $_POST['valueCliente'] == 'Un' ? 'checked' : '' ?>> <?= lang('a').' '.lang('view_customer_total') ?>
													</label>
												</div>
												<div class="collapse" id="collapseBustomer">
													<select class="form-control " onchange="Search_Cliente(this.value);" name="Cliente" id="Cliente" style="width:100%;">
														<option value="all" > <?= lang('select').' '.lang('view_customer_total') ?> </option>
														<?php foreach ($customers as $customer): ?>
															<option value="<?php echo $customer['cod_id']; ?>"><?php echo $customer['nombre']; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-3">
												<input type="hidden" name="filter_action" value="1">
												<button type="button" class="btn btn-success" class="Buscar" id="Buscar"><?php echo lang('submit');?></button>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6">
											</div>
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="pull-right dropdown">
													<button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
													<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
														<li>
															<a class="ExportPdf" id="ExportPdf"> PDF </a>
														</li>
														<li>
															<a class="ExportXls" id="ExportXls"> Excel </a>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<br>
	                            <?php echo form_close(); ?>
							<?php else: ?>
								<div class="row">
									<div class="col-sm-12">
										<h3><?= lang('portfolio_report_temp_tables_title') ?></h3>
										<p><?= lang('portfolio_report_temp_tables_description') ?></p>
									</div>
									<div class="col-sm-12">
										<h4><?= lang('portfolio_report_temp_tables_1_step_title') ?></h4>
										<p><?= sprintf(lang('portfolio_report_temp_tables_1_step_description'), $this->Settings->system_start_date) ?></p>
									</div>
									<div class="form-group col-md-4" style="display:none;">
                                      <label><?= lang('cut_off_date') ?></label>
                                      <input type="date" name="end_date" id="end_date" class="form-control" value="<?= isset($_POST['end_date']) && !empty($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d') ?>" >
                                    </div>
									<div class="col-sm-12">
										<button class="btn btn-primary" id="generate_temp_tables"><?= lang('generate_temp_tables') ?></button>
									</div>
									<div class="col-sm-12 divpbar" style="display:none;">
										<hr>
										<label>Progreso</label>
										<div class="progress">
										  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
										    0%
										  </div>
										</div>
										<br>
										<em class="text_pb">Generando....</em>
									</div>
								</div>
							<?php endif ?>
						</div>
						<?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
							<div class="col-lg-12" id="ajax-content-container">
								<table id="TableData" class="table table-stripped" >
									<thead>
										<tr>
											<td class="text-center"> <b><?= lang('ref') ?></b></td>
											<td class="text-center"> <b><?= lang('date').' '.lang('sale') ?></b></td>
											<td class="text-center"> <b><?= lang('day_arrears') ?></b></td>
											<td class="text-center"> <b><?= lang('amount_paid') ?></b></td>
											<td class="text-center"> <b><?= lang('sbalance').' '.lang('paid') ?></b></td>
											<td class="text-center"> <b><?= lang('sbalance').' '.lang('pending') ?></b></td>
										</tr>
									</thead>
									<tbody>
										<?php
											$nit_old = $sucursal_old = $vendedor_old =  'old' ;
											$total = $val_pago = $Saldo = 0;
											$total_total = $total_val_pago = $total_Saldo = 0;
											$total_total_Sucursal = $total_val_pago_Sucursal = $total_Saldo_Sucursal = 0;
											$i = 0;
											$prin_ven = $prin_clien = false;
										?>
										<?php foreach ($sales as $sale): ?>
											<?php
											$i++;
											$prin_clien = false;
											?>

											<?php if ($sale['documento'] != $nit_old): ?>
												<?php if ($i > 1): ?>
													<tr >
														<td ></td>
														<td></td>
														<th class="text-right" >SubTotal Cliente </th>
														<th class="text-right"><?php echo $this->sma->formatMoney($total);?></th>
														<th class="text-right"><?php echo $this->sma->formatMoney($val_pago);?></th>
														<th class="text-right"><?php echo $this->sma->formatMoney($Saldo);?></th>
													</tr>
													<?php $total = $val_pago = $Saldo = 0;	$prin_clien = true;	?>
												<?php endif; ?>
											<?php endif; ?>

											<?php if ($sale['cod_Sucursal'] != $sucursal_old): ?>
												<?php if ($i > 1): ?>
													<?php if ($prin_ven == false): ?>
														<?php if ($prin_clien == false): ?>
															<tr >
																<td ></td>
																<td></td>
																<th class="text-right" >SubTotal Cliente </th>
																<th class="text-right"><?php echo $this->sma->formatMoney($total);?></th>
																<th class="text-right"><?php echo $this->sma->formatMoney($val_pago);?></th>
																<th class="text-right"><?php echo $this->sma->formatMoney($Saldo);?></th>
															</tr>
															<?php $total = $val_pago = $Saldo = 0;		?>
														<?php endif; ?>
														<tr class="active" >
															<td ></td>
															<td></td>
															<th class="text-right">SubTotal Vendedor </th>
															<th class="text-right"><?php echo $this->sma->formatMoney($total_total);?></th>
															<th class="text-right"><?php echo $this->sma->formatMoney($total_val_pago);?></th>
															<th class="text-right"><?php echo $this->sma->formatMoney($total_Saldo);?></th>
														</tr>
														<?php $total_total = $total_val_pago = $total_Saldo = 0;	$prin_ven = true;	?>
													<?php endif; ?>
													<tr class="active" >
														<td ></td>
														<td></td>
														<th class="text-right">SubTotal Sucursal </th>
														<th class="text-right"><?php echo $this->sma->formatMoney($total_total_Sucursal);?></th>
														<th class="text-right"><?php echo $this->sma->formatMoney($total_val_pago_Sucursal);?></th>
														<th class="text-right"><?php echo $this->sma->formatMoney($total_Saldo_Sucursal);?></th>
													</tr>
													<?php $total_total_Sucursal = $total_val_pago_Sucursal = $total_Saldo_Sucursal = 0;			?>
												<?php endif; ?>
												<tr class="active">
													<td ><h5><b>Sucursal: <?php echo $sale['Sucursal']; ?></b></h5></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
												</tr>
												<?php
													$nit_old = $sucursal_old = $vendedor_old =  'old' ;
													$total = $val_pago = $Saldo = 0;
													$total_total = $total_val_pago = $total_Saldo = 0;
													$total_total_Sucursal = $total_val_pago_Sucursal = $total_Saldo_Sucursal = 0;
													$i = 0;
													$prin_ven = $prin_clien = false;
												?>
											<?php endif; ?>

											<?php if ($sale['cod_seller'] != $vendedor_old): ?>
												<?php if ($i > 1): ?>
													<tr class="active" >
														<td ></td>
														<td></td>
														<th class="text-right">SubTotal Vendedor </th>
														<th class="text-right"><?php echo $this->sma->formatMoney($total_total);?></th>
														<th class="text-right"><?php echo $this->sma->formatMoney($total_val_pago);?></th>
														<th class="text-right"><?php echo $this->sma->formatMoney($total_Saldo);?></th>
													</tr>
												<?php endif; ?>
												<tr class="active">
													<td ><b>Vendedor: </b><?php echo $sale['nom_seller']; ?></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
												</tr>
												<?php if ($sale['documento'] == $nit_old): ?>
													<tr class="active">
														<td style="border-right: 1px solid transparent;">
															<b> Cliente:</b> <?php echo $sale['nom_companie']; ?>
														</td>
														<td style="border-right: 1px solid transparent;">
															<b>NIT:</b> <?php echo $sale['documento']; ?>
														</td>
														<td style="border-right: 1px solid transparent;">

														</td>
														<td ></td>
														<td ></td>
														<td ></td>
													</tr>
													<?php $total_total = $total_val_pago = $total_Saldo = 0;  ?>
												<?php endif; ?>
											<?php endif; ?>

											<?php if ($sale['documento'] != $nit_old): ?>
												<?php if ($prin_clien == false): ?>
													<?php if ($sale['cod_seller'] == $vendedor_old): ?>
														<tr >
															<td ></td>
															<td></td>
															<th class="text-right" >SubTotal Cliente </th>
															<th class="text-right"><?php echo $this->sma->formatMoney($total);?></th>
															<th class="text-right"><?php echo $this->sma->formatMoney($val_pago);?></th>
															<th class="text-right"><?php echo $this->sma->formatMoney($Saldo);?></th>
														</tr>
														<?php $total = $val_pago = $Saldo = 0;		?>
													<?php endif; ?>
												<?php endif; ?>
												<tr class="active">
													<td style="border-right: 1px solid transparent;">
														<b> Cliente:</b> <?php echo $sale['nom_companie']; ?>
													</td>
													<td style="border-right: 1px solid transparent;">
														<b>NIT:</b> <?php echo $sale['documento']; ?>
													</td>
													<td style="border-right: 1px solid transparent;">

													</td>
													<td ></td>
													<td ></td>
													<td ></td>
												</tr>
												<?php $total_total = $total_val_pago = $total_Saldo = 0;  ?>
											<?php endif; ?>

											<?php $sucursal_old = $sale['cod_Sucursal']; ?>
											<?php $vendedor_old = $sale['cod_seller']; ?>
											<?php $nit_old = $sale['documento']; ?>
											<?php
												$fecha = substr($sale['Fecha_Creacion'],0,10);
												$fecha_now = date('Y-m-d');
												$dias	= (strtotime($fecha)-strtotime($fecha_now))/86400;
												$dias 	= abs($dias);
												$dias 	= floor($dias);
											?>
											<tr >
												<td scope="row"><?php echo $sale['Referencia']; ?></td>
												<td class="text-center"><?php echo substr($sale['Fecha_Creacion'],0,10); ?></td>
												<td class="text-center"><?php echo $dias; ?></td>
												<td class="text-right"><?php echo $this->sma->formatMoney($sale['total']);?></td>
												<td class="text-right"><?php echo $this->sma->formatMoney($sale['val_pago']);?></td>
												<td class="text-right"><?php echo $this->sma->formatMoney($sale['Saldo']);?></td>
											</tr>
											<?php
												$total +=  $sale['total'];
												$val_pago += $sale['val_pago'];
												$Saldo += $sale['Saldo'];
												$total_total_Sucursal += $sale['total'];
												$total_val_pago_Sucursal += $sale['val_pago'];
												$total_Saldo_Sucursal +=  $sale['Saldo'];
												$total_total += $sale['total'];
												$total_val_pago += $sale['val_pago'];
												$total_Saldo +=  $sale['Saldo'];
											?>
										<?php endforeach; ?>

										<tr >
											<td ></td>
											<td ></td>
											<th class="text-right" >SubTotal Cliente </th>
											<th class="text-right"><?php echo $this->sma->formatMoney($total);?></th>
											<th class="text-right"><?php echo $this->sma->formatMoney($val_pago);?></th>
											<th class="text-right"><?php echo $this->sma->formatMoney($Saldo);?></th>
										</tr>
										<tr class="active" >
											<td ></td>
											<td></td>
											<th class="text-right">SubTotal Vendedor </th>
											<th class="text-right"><?php echo $this->sma->formatMoney($total_total);?></th>
											<th class="text-right"><?php echo $this->sma->formatMoney($total_val_pago);?></th>
											<th class="text-right"><?php echo $this->sma->formatMoney($total_Saldo);?></th>
										</tr>
										<tr class="active" >
											<td ></td>
											<td></td>
											<th class="text-right">SubTotal Sucursal </th>
											<th class="text-right"><?php echo $this->sma->formatMoney($total_total_Sucursal);?></th>
											<th class="text-right"><?php echo $this->sma->formatMoney($total_val_pago_Sucursal);?></th>
											<th class="text-right"><?php echo $this->sma->formatMoney($total_Saldo_Sucursal);?></th>
										</tr>
									</tbody>
								</table>
							</div>
						<?php endif ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script >

	function ActualizarVendedores(valor, selected = 'all') {
		<?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
		<?php else: ?>
			$("#Vendedor option").remove();
			$('#Vendedor').html('<option value="all" selected="selected">-- Seleccione Vendedor --</option>');
			jQuery.ajax({
				url: site.base_url+'reports/search_Seller/'+valor + '/',
				type: 'GET',
				dataType: 'json',
				success: function(data, textStatus, xhr) {
					var obj = Object.values(data.sellers);
					obj.forEach(function(element) {
						$('#Vendedor').append('<option value="'+element.cod_seller+'" >'+element.nombre+'</option>');
					});

					if (selected != 'all') {
						$('#Vendedor').select2('val', selected);
					}

				},
				error: function(xhr, textStatus, errorThrown) {
					$('#ajax-content-container').html('<div class="alert alert-danger" role="alert"><strong>Error:</strong> Lo sentimos, se presento un error por favor refrescar la pantalla volver a intentarlo.</div>');
				}
			});
		<?php endif ?>
	}

	function Search_Vendedor(valor, selected = 'all') {
		$("#Cliente option").remove();
		$('#Cliente').append('<option value="all" selected="selected">-- Seleccione Cliente --</option>');
		var sucursal = $("#Sucursal option:selected").val();
		jQuery.ajax({
			url: site.base_url+'reports/search_customer/'+sucursal+'/'+valor + '/',
			type: 'GET',
			dataType: 'json',
			success: function(data, textStatus, xhr) {
				var obj = Object.values(data.sellers);
				obj.forEach(function(element) {
					$('#Cliente').append('<option value="'+element.cod_id+'" >'+element.nombre+'</option>');
				});
				if (selected != 'all') {
					$('#Cliente').select2('val', selected);
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				$('#ajax-content-container').html('<div class="alert alert-danger" role="alert"><strong>Error:</strong> Lo sentimos, se presento un error por favor refrescar la pantalla volver a intentarlo.</div>');
			}
		});

	}

	function Search_Cliente(valor) {
		var sucursal = $("#Sucursal option:selected").val();
		var vendedor = $("#Vendedor option:selected").val();
	}



$('#collapseBiller').collapse('hide');
<?php if ((!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id'))): ?>
	$('#collapseSeller').collapse('show');
<?php else: ?>
	$('#collapseSeller').collapse('hide');
<?php endif ?>
$('#collapseBustomer').collapse('hide');

	$(document).ready(function() {
		$("#form_filter").validate({
		    ignore: []
		});

		if ("<?= isset($_POST['valueSucursal']) || isset($_POST['valueVendedor']) || isset($_POST['valueCliente']) ? true : false ?>") {
			$('#collapseBiller').collapse('show');
		}

		if ("<?= isset($_POST['valueVendedor']) || isset($_POST['valueCliente']) ? true : false ?>") {
			$('#collapseSeller').collapse('show');
		}

		if ("<?= isset($_POST['valueCliente']) ? true : false ?>") {
			$('#collapseBustomer').collapse('show');
		}

		$('#TableData').DataTable({
			searching: false,
 		    ordering:  false,
			pageLength: 10,
    		responsive: true,
            "aaSorting": [],
            "aoColumns": [
	            {
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
			                "bSortable": false
			            }],
			dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    		buttons : [{extend:'excel', title:'Menus', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
			oLanguage: <?php echo $dt_lang; ?>
		});
		/*AQUI <?= lang('actions') ?>*/
		$("#ExportPdf").click(function() {
			$('#action').val('pdf');
			form_action();
		});
		$("#ExportXls").click(function() {
			$('#action').val('xls');
			form_action();
		});

		function form_action(){
			$('#form_filter').prop('target', '_blank').submit();
			$('#form_filter').prop('target', false);
			$('#action').val('');
		}
		<?php if (isset($_POST['Sucursal']) && $_POST['Sucursal'] != 'all' ) { ?>
			$('#Sucursal').val("<?= $_POST['Sucursal'] ?>");
			ActualizarVendedores("<?= $_POST['Sucursal'] ?>", "<?= $_POST['Vendedor'] ?>");
		<?php } ?>
		<?php if (isset($_POST['Vendedor']) && $_POST['Vendedor'] != 'all' ) { ?>
			$('#Vendedor').val("<?= $_POST['Vendedor'] ?>");
			Search_Vendedor("<?= $_POST['Vendedor'] ?>", "<?= $_POST['Cliente'] ?>");
		<?php } ?>
	} );

	// Sucursal
$('#biller2').on('ifChecked', function(event){
	$('#Sucursal').select2('readonly', false);
	var Sucursal = $("#Sucursal option:selected").val();
	ActualizarVendedores(Sucursal);
	$('#collapseBiller').collapse('show');
});
$('#biller1').on('ifChecked', function(event){
	$("#Sucursal").val('all');
	$('#collapseBiller').collapse('hide');
	ActualizarVendedores('all');
	$('#Sucursal').select2('val', 'all').trigger('change');
	$('#Sucursal').select2('readonly', true);
});
// Vendedor
$('#seller2').on('ifChecked', function(event){
	$('#Vendedor').select2('readonly', false);
	$('#collapseSeller').collapse('show');
	var Vendedor = $("#Vendedor option:selected").val();
	Search_Vendedor(Vendedor);
	var Sucursal = $("#Sucursal option:selected").val();
	if(Sucursal=='all')
	{
		$("input[name=valueSucursal]").attr('readonly', true);
		$("#Sucursal").val('all');
	}
});
$('#seller1').on('ifChecked', function(event){
	$("#Vendedor").val('all');
	$('#collapseSeller').collapse('hide');
	Search_Vendedor('all');
	$('#Vendedor').prop('selectedIndex',0);
	$('#Vendedor').select2('val', 'all').trigger('change');
	$('#Vendedor').select2('readonly', true);
});
// cliente
$('#Cliente2').on('ifChecked', function(event){
	$('#Cliente').select2('readonly', false);
	var vendedor = $("#Vendedor option:selected").val();
	if(vendedor=='all')
	{
		$("input[name=valueSucursal]").attr('readonly', true);
		$("input[name=valueVendedor]").attr('readonly', true);
		$("#Vendedor").val('all');
		$("#Sucursal").val('all');
	}
	$('#collapseBustomer').collapse('show');
	var Cliente = $("#Cliente option:selected").val();
	Search_Cliente(Cliente);
});
$('#Cliente1').on('ifChecked', function(event){
	$('#Vendedor').select2('readonly', false);
	$('#Sucursal').select2('readonly', false);
	$("input[name=valueSucursal]").attr('disabled', false);
	$("input[name=valueVendedor]").attr('disabled', false);
	$("#Cliente").val('all');
	$('#collapseBustomer').collapse('hide');
	Search_Cliente('all');
	$('#Cliente').prop('selectedIndex',0);
	$('#Cliente').select2('val', 'all').trigger('change');
	$('#Cliente').select2('readonly', true);
});


$('#Buscar').on('click', function(e){
	if ($('#form_filter').valid()) {
	    $('#filter_action').val(1);
	    $('#form_filter').submit();
	}
});


var filter_year_options = JSON.parse('<?= json_encode($this->filter_year_options) ?>');
var years = [];
var yn = 1;
$.each(filter_year_options, function(index, year){
	years[yn] = year;
	yn++;
});
var periodos = JSON.parse('<?= $periodos ?>');
var num_periodos = Object.keys(periodos).length;
var num_anos = Object.keys(filter_year_options).length;
var max_iterations = num_anos * num_anos;
var periodo_actual = 1;
var ano_actual = 1;
var total_pb = 100/(num_periodos * num_anos);
var executed_pb = 0;

$(document).on('click', '#generate_temp_tables', function(){
	$('.divpbar').fadeIn();
	$('#generate_temp_tables').html("Generando, por favor espere ... <i class='fa fa-spinner fa-spin'></i>");
	$('#generate_temp_tables').prop('disabled', true);
	execute_ajax();
});

function execute_ajax(){
	text_pb = "Generando de "+parseInt(years[ano_actual])+periodos[periodo_actual].start_month+" a "+parseInt(years[ano_actual])+periodos[periodo_actual].end_month;
	$('.text_pb').text(text_pb);
	console.log('1');
	$.ajax({
		url : site.base_url+"reports/portfolio_report_2_generate_temp",
		dataType : "JSON",
		type : "GET",
		data : {
				end_month : periodos[periodo_actual].end_month,
				start_month : periodos[periodo_actual].start_month,
				end_date : $('#end_date').val(),
				'year_to_execute': parseInt(years[ano_actual])
			}
	}).done(function(data){
		if (data.success == 0) {
			return;
		} else {
			$('.text_pb').text("Generado");
			if (ano_actual <= num_anos) {
				periodo_actual++;
				if (periodo_actual <= num_periodos) {
					execute_ajax();
				} else if (periodo_actual > num_periodos) {
					ano_actual++;
					periodo_actual = 1;
					execute_ajax();
				}
				executed_pb += total_pb;
				$('.progress-bar').css('width', executed_pb+"%").text(formatDecimal(executed_pb, 0)+"%");
			} else if (ano_actual > num_anos) {
				location.reload();
			}
		}
	});
}
<?php if ($this->session->userdata('portfolio_temp_end_date')): ?>
	$('#end_date').val('<?= $this->session->userdata('portfolio_temp_end_date') ?>').prop('readonly', true);
<?php endif ?>


</script>
