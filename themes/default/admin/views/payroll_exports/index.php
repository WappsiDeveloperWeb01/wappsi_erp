<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    if ($this->session->userdata('detal_post_processing')) { $this->session->unset_userdata('detal_post_processing'); }
?>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('payroll_exports/getPayrollReport', ['id'=>'PayrollReportsFilterForm']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('date_records_filter') ?></label>
                                            <select class="form-control" name="date_records_filter" id="date_records_filter_dh">
                                                <?php $dateRecordsFilter = (isset($_POST["date_records_filter"]) ? $_POST["date_records_filter"] : ''); ?>
                                                <?= $this->sma->get_filter_options($dateRecordsFilter); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('biller') ?></label>
                                            <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                            <?php $billerId = (empty($billerId)) ? $this->session->userdata('biller_id') : $billerId; ?>
                                            <select class="form-control" id="biller_id" name="biller_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('area') ?></label>
                                            <?php $areaId = (isset($_POST["area"])) ? $_POST["area"] : ''; ?>
                                            <select class="form-control" id="area" name="area">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($areas as $area) : ?>
                                                    <option value="<?= $area->id ?>" <?= ($area->id == $areaId) ? 'selected' : '' ?>><?= $area->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('professional_position') ?></label>
                                            <?php $saleOrigin = (isset($_POST["professional_position"])) ? $_POST["professional_position"] : ''; ?>
                                            <select class="form-control" id="professional_position" name="professional_position">
                                                <option value=""><?= lang('alls') ?></option>
                                                <!-- <?php foreach ($salesOrigins as $origin) : ?>
                                                    <option value="<?= $origin->id ?>" <?= ($origin->id == $saleOrigin) ? 'selected' : '' ?>><?= $origin->name ?></option>
                                                <?php endforeach ?> -->
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="frequency"><?= $this->lang->line('frequency') ?></label>
                                            <?php $frequencyId = (isset($_POST["frequency"])) ? $_POST["frequency"] : ''; ?>
                                            <select class="form-control" id="frequency" name="frequency" required>
                                            <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($frequencies as $frequency) : ?>
                                                    <option value="<?= $frequency->id ?>" <?= ($frequency->id == $frequencyId) ? 'selected' : '' ?>><?= $frequency->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label><?= $this->lang->line('status') ?></label>
                                        <?php $stateId = (isset($_POST["status"])) ? $_POST["status"] : ''; ?>
                                        <select class="form-control" name="status" id="status">
                                            <option value=""><?= lang('alls') ?></option>
                                            <?php foreach ($status as $state): ?>
                                                <option value="<?= $state->id ?>" <?= ($state->id == $stateId) ? 'selected' : '' ?>><?= $state->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="generatePayrollReportButton" type="button" data-toggle="tooltip" data-placement="bottom" title="Generar Reporte"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="employee_id"><?= $this->lang->line('employee') ?></label>
                                            <?php $employeeId = (isset($_POST["employee_id"])) ? $_POST["employee_id"] : ''; ?>
                                            <input class="form-control" type="input" name="employee_id" id="employee_id" value="<?= $employeeId ?>" placeholder="<?= lang('alls') ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="report_type"><?= lang('report_type') ?></label>
                                            <?php $reportTypeId = (isset($_POST["report_type"])) ? $_POST["report_type"] : ''; ?>
                                            <select name="report_type" id="report_type" class="form-control">
                                                <?php foreach ($reportTypes as $reportType) : ?>
                                                    <option value="<?= $reportType->id ?>" <?= ($reportType->id == $reportTypeId) ? 'selected' : '' ?>><?= $reportType->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="include_provisions_social_benefits" style="margin-top: 25px">
                                                <?php $includeProvisionsSocialBenefits = (isset($_POST["include_provisions_social_benefits"])) ? $_POST["include_provisions_social_benefits"] : ''; ?>
                                                <input type="checkbox" name="include_provisions_social_benefits" id="include_provisions_social_benefits" <?= (!empty($includeProvisionsSocialBenefits) ? 'checked' : '') ?>> <?= $this->lang->line('include_provisions_social_benefits') ?>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="include_personal_data_employee" style="margin-top: 25px">
                                                <?php $includePersonalDataEmployee = (isset($_POST["include_personal_data_employee"])) ? $_POST["include_personal_data_employee"] : ''; ?>
                                                <input type="checkbox" name="include_personal_data_employee" id="include_personal_data_employee" <?= (!empty($includePersonalDataEmployee) ? 'checked' : '') ?>> <?= $this->lang->line('include_personal_data_employee') ?>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="date_controls_dh">
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for="start_date_dh"><?= $this->lang->line('start_date') ?></label>
                                                <?php $startDate = isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>
                                                <input class="form-control datetime" type="text" name="start_date" id="start_date_dh" value="<?= $startDate ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for="end_date_dh"><?= $this->lang->line('end_date') ?></label>
                                                <?php $endDate = isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>
                                                <input class="form-control datetime" type="text" name="end_date" id="end_date_dh" value="<?= $endDate ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        getEmployees();

        $("#PayrollReportsFilterForm").validate({ ignore: [] });

        $(document).on('change', '#date_records_filter_dh', function() { showHidefilterContent($(this)); validateFrequencies(); });
        $(document).on('click', '#generatePayrollReportButton', function () { validateInputForm(); });
        $(document).on('change', '#area', function(){ getprofessionalPosition(); });
        $(document).on('change', '#frequency', function(){ validateFrequencies() });
        $(document).on('change', '#employee_id', function () { $(this).val() });
    });

    function getEmployees()
    {
        $('#employee_id').val('<?= $employeeId ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"employees/getCustomerSelect2/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "employees/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        data.results.unshift({id: '', text: 'Todos'})
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Todos'}, {id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function showHidefilterContent(element)
    {
        let advancedFiltersContainer = '<?= $advancedFiltersContainer ? 1 : 0 ?>';

        let dateFilter = element.val();
        if (dateFilter != '') {
            if (dateFilter == 5) {
                $('#advancedFiltersContainer').fadeIn();
                $('.new-button-container .collapse-link').html('<i class="fa fa-lg fa-chevron-up"></i>');
            }
        }
    }

    function getprofessionalPosition()
    {
        $.ajax({
            url: site.base_url + 'payroll_exports/getProfessionalPosition',
            type: 'POST',
            dataType: 'HTML',
            data: {
                '<?=$this->security->get_csrf_token_name()?>': '<?=$this->security->get_csrf_hash()?>',
                'area': $('#area').val()
            },
        })
        .done(function(data) {
            $('#professional_position').html(data);
            $('#professional_position').select2('val', "");
        })
        .fail(function(data) {
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function validateFrequencies()
    {
        let frequency = $('#frecuency').val();

        $.ajax({
            type: "POST",
            url: site.base_url + 'payroll_management/getFrequenciesPayroll',
            data: {
                '<?=$this->security->get_csrf_token_name()?>': '<?=$this->security->get_csrf_hash()?>',
                'date_records_filter': $('#date_records_filter_dh').val(),
                'start_date': $('#start_date_dh').val(),
                'end_date': $('#end_date_dh').val()
            },
            dataType: "JSON",
            success: function (response) {
                if (response.status == true) {
                    $('#frecuency').attr('required', true);
                    swal({
                        title: 'Listado de nómina y frecuencias',
                        text: response.message,
                        type: 'info',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    });
                } else {
                    $('#frecuency').attr('required', false);
                }
            }
        });
    }

    function validateInputForm()
    {
        let form = $('#PayrollReportsFilterForm');
        if (form.valid()) {
            form.submit();
        }
    }
</script>