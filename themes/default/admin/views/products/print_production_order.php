<?php

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

//$this->SetTextColor( 255, 0, 0); color rojo
//$this->SetTextColor( 0, 0, 0); color negro

// var_dump($POST);

class PDF extends FPDF
  {

    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Orden de producción'));
        //izquierda
        if ($this->factura->status == 'pending') {
            $this->setXY(90, 90);
            $this->SetFont('Arial','B',50);
            $this->SetTextColor(125,125,125);
            $this->RotatedImage(base_url().'assets/images/not_approved.png',20,220,240,18,45);
            $this->SetFont('Arial','',$this->fuente);
            $this->SetTextColor(0,0,0);
        }
        if (!$this->invoice_format || ($this->invoice_format && $this->invoice_format->show_logo == 1)) {
            if ($this->invoice_format && $this->invoice_format->logo == 2) {
                $this->Image(base_url().'assets/uploads/logos/'.$this->logo,25,7,22);
            } else {
                $this->Image(base_url().'assets/uploads/logos/'.$this->logo,13,13,42);
            }
            $cx = 58;
            $cy = 10;
        } else {
            $cx = 13;
            $cy = 10;
        }
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(mb_strtoupper($this->biller->company != '-' ? $this->biller->company : $this->biller->name)),'',1,'l');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente);
        $this->setXY($cx, $cy);
        
        $this->MultiCell(72,3, $this->sma->utf8Decode($this->biller->address),0,'L');
        $cy = $this->getY();
        // $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*4));
        $this->RoundedRect(132, 7, 77, 22, 3, '1234', '');
        $cx = 132;
        $cy = 7;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode('ORDEN DE PRODUCCIÓN'),'',1,'C');
        $this->setXY($cx, $cy+10);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'C');
        $cy +=10;
        $this->setXY($cx, $cy);
        // $this->Cell(77, 10 , $this->sma->utf8Decode('N°. '.$this->factura->reference_no),'',1,'C');

        //izquierda
        $cx = 13;
        $cy = 31;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect($cx, $cy, 118, 5, 3, '1', 'DF');
        $this->RoundedRect($cx, $cy+5, 118, 20, 3, '4', '');
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('INFORMACIÓN DE ORDEN'),'B',1,'C');
        $cx += 3;
        $cy += 6;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(20, 5 , $this->sma->utf8Decode('Bodega : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(95, 5 , $this->sma->utf8Decode($this->warehouse->name),'',1,'L');
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(20, 5 , $this->sma->utf8Decode('Creado por : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(95, 5 , $this->sma->utf8Decode($this->created_by->first_name . ' ' . $this->created_by->last_name),'',1,'L');
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(20, 5 , $this->sma->utf8Decode('Registro : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(95, 5 , $this->sma->utf8Decode($this->factura->registration_date),'',1,'L');
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(20, 5 , $this->sma->utf8Decode('Act. Estado : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(95, 5 , $this->sma->utf8Decode($this->factura->completed_at),'',1,'L');

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $altura = 5;
        $adicional_altura = 2.3;
        $cx = 131;
        $cy = 31;
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect($cx, $cy, 78, 5, 3, '2', 'DF');
        $this->RoundedRect($cx, $cy+5, 78, 20, 3, '3', '');
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA'),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('ESTADO'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode(lang($this->factura->status)),'BR',1,'C');
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode('RESPONSABLE'),'LBR',1,'C',1);
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell(78, $altura+$adicional_altura , $this->sma->utf8Decode($this->employee->name),'',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->ln(8);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->Cell(95 ,5, $this->sma->utf8Decode('Productos'),'',0,'L',0);
        $this->Cell(6 ,5, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(95 ,5, $this->sma->utf8Decode('Componentes'),'',1,'L',0);
        
        $this->Cell(33.75,5, $this->sma->utf8Decode('Descripción'),'TBLR',0,'C',1);
        $this->Cell(9.75,5, $this->sma->utf8Decode('UM'),'TBLR',0,'C',1);
        $this->Cell(12.75,5, $this->sma->utf8Decode('Tipo'),'TBLR',0,'C',1);
        $this->Cell(19,5, $this->sma->utf8Decode('Cant'),'TBLR',0,'C',1);
        $this->Cell(19.75,5, $this->sma->utf8Decode('Costo'),'TBLR',0,'C',1);
        
        $this->Cell(6   ,5, $this->sma->utf8Decode(''),'',0,'C',0);
        
        $this->Cell(33.75,5, $this->sma->utf8Decode('Descripción'),'TBLR',0,'C',1);
        $this->Cell(9.75,5, $this->sma->utf8Decode('UM'),'TBLR',0,'C',1);
        $this->Cell(12.75,5, $this->sma->utf8Decode('Tipo'),'TBLR',0,'C',1);
        $this->Cell(19,5, $this->sma->utf8Decode('Cant'),'TBLR',0,'C',1);
        $this->Cell(19.75,5, $this->sma->utf8Decode('Costo'),'TBLR',1,'C',1);
    }

    function Footer()
    {
        // Print centered page number
        $this->RoundedRect(13, 248, 196, 17.5, 3, '1234', '');
        $this->SetXY(13, -30);
        // $this->Cell(196, 5 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
        $this->MultiCell(196,3,$this->sma->utf8Decode("Nota : ".$this->reduceTextToDescription2($this->description2)), 0, 'L');
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');

    }

    function reduceTextToDescription1($text){
        $text="Nota : ".$text;
        if (strlen($text) > 390) {
            $text = substr($text, 0, 385);
            $text.="...";
            return $text;
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        if (strlen($text) > 380) {
            $text = substr($text, 0, 375);
            $text.="...";
            return $text;
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function crop_text($text, $length = 50){
        if (strlen($text) > $length) {
            $text = substr($text, 0, ($length-5));
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
    var $angle=0;
    function Rotate($angle,$x=-1,$y=-1)
    {
        if($x==-1)
            $x=$this->x;
        if($y==-1)
            $y=$this->y;
        if($this->angle!=0)
            $this->_out('Q');
        $this->angle=$angle;
        if($angle!=0)
        {
            $angle*=M_PI/180;
            $c=cos($angle);
            $s=sin($angle);
            $cx=$x*$this->k;
            $cy=($this->h-$y)*$this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
        }
    }

    function RotatedText($x,$y,$txt,$angle)
    {
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }

    function RotatedImage($file,$x,$y,$w,$h,$angle)
    {
        $this->Rotate($angle,$x,$y);
        $this->Image($file,$x,$y,$w,$h);
        $this->Rotate(0);
    }

}


$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);
$number_convert = new number_convert();
$fuente = 8;
$adicional_fuente = 0.5;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$description1 = $inv->note;
$pdf->description2 = $inv->note;
$pdf->sma = $this->sma;
$pdf->logo = $invoice_format && $invoice_format->logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->invoice_format = $invoice_format;
$pdf->warehouse = $warehouse;
$pdf->created_by = $created_by;
$pdf->employee = $employee;
$value_decimals = ($invoice_format ? $invoice_format->value_decimals : NULL);
$qty_decimals = ($invoice_format ? $invoice_format->qty_decimals : NULL);

$pdf->AddPage();

$maximo_footer = 235;

// exit(var_dump($rows));
$plus_font_size = 0;
$pdf->SetFont('Arial','',$fuente-0.3);
$old_Y = $pdf->getY();
$total_cost = 0;
foreach ($rows as $item) {
    if ($item->type != 'addition') {
        continue;
    }
    if ($pdf->getY() > $maximo_footer) {
        $pdf->AddPage();
    }
    $columns = [];
        $inicio_fila_X = $pdf->getX();
        $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_x'] = $pdf->getX();
        $cX = $pdf->getX();
        $cY = $pdf->getY();
        $inicio_altura_fila = $cY;                             
    $pdf->MultiCell(33.75,5, $this->sma->utf8Decode($item->product_code." - ".$item->product_name),'','');
    $columns[0]['fin_x'] = $cX+33.75;
        $fin_altura_fila = $pdf->getY();
        $pdf->setXY($cX+33.75, $cY);
    $columns[1]['inicio_x'] = $pdf->getX();
    $pdf->Cell(9.75,5, $this->sma->utf8Decode($item->unit_code),'',0,'C');
    $columns[1]['fin_x'] = $pdf->getX();
    $columns[2]['inicio_x'] = $pdf->getX();
    $pdf->Cell(12.75,5, $this->sma->utf8Decode(lang($item->type)),'',0,'R');
    $columns[2]['fin_x'] = $pdf->getX();
    $columns[3]['inicio_x'] = $pdf->getX();
    $pdf->Cell(19,5, $this->sma->utf8Decode($this->sma->formatQuantity($item->quantity, $qty_decimals)),'',0,'R');
    $columns[3]['fin_x'] = $pdf->getX();
    $columns[4]['inicio_x'] = $pdf->getX();
    $pdf->Cell(19.75,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($item->quantity * ($item->adjustment_cost ? $item->adjustment_cost : $item->avg_cost)))),'',0,'R');
    $total_cost += $item->quantity * ($item->adjustment_cost ? $item->adjustment_cost : $item->avg_cost);
    $columns[4]['fin_x'] = $pdf->getX();
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = $fin_altura_fila - $inicio_altura_fila;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    foreach ($columns as $key => $data) {
        $ancho_column = $data['fin_x'] - $data['inicio_x'];
        if ($altura_fila < 5) {
            $altura_fila = 5;
        }
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
    }
    $pdf->ln($altura_fila);
}
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(75.25,5, $this->sma->utf8Decode('Total'),'LBR',0,'R');
$pdf->Cell(19.75,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_cost)),'LBR',0,'R');
$pdf->SetFont('Arial','',$fuente-0.3);

$new_X = 114;
$altura_fila = 0;
$total_cost = 0;
foreach ($rows as $item) {
    $pdf->setXY($new_X, $old_Y+$altura_fila);
    if ($item->type != 'subtraction') {
        continue;
    }
    if ($pdf->getY() > $maximo_footer) {
        $pdf->AddPage();
    }
    $columns = [];
        $inicio_fila_X = $new_X;
        $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_x'] = $new_X;
        $cX = $new_X;
        $cY = $pdf->getY();
        $inicio_altura_fila = $cY;                             
    $pdf->MultiCell(33.75,5, $this->sma->utf8Decode($item->product_code." - ".$item->product_name),'','');
    $columns[0]['fin_x'] = $cX+33.75;
        $fin_altura_fila = $pdf->getY();
        $pdf->setXY($cX+33.75, $cY);
    $columns[1]['inicio_x'] = $pdf->getX();
    $pdf->Cell(9.75,5, $this->sma->utf8Decode($item->unit_code),'',0,'C');
    $columns[1]['fin_x'] = $pdf->getX();
    $columns[2]['inicio_x'] = $pdf->getX();
    $pdf->Cell(12.75,5, $this->sma->utf8Decode(lang($item->type)),'',0,'R');
    $columns[2]['fin_x'] = $pdf->getX();
    $columns[3]['inicio_x'] = $pdf->getX();
    $pdf->Cell(19,5, $this->sma->utf8Decode($this->sma->formatQuantity($item->quantity, $qty_decimals)),'',0,'R');
    $columns[3]['fin_x'] = $pdf->getX();
    $columns[4]['inicio_x'] = $pdf->getX();
    $pdf->Cell(19.75,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($item->quantity * ($item->adjustment_cost ? $item->adjustment_cost : $item->avg_cost)))),'',0,'R');
    $total_cost += $item->quantity * ($item->adjustment_cost ? $item->adjustment_cost : $item->avg_cost);
    $columns[4]['fin_x'] = $pdf->getX();
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = $fin_altura_fila - $inicio_altura_fila;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    foreach ($columns as $key => $data) {
        $ancho_column = $data['fin_x'] - $data['inicio_x'];
        if ($altura_fila < 5) {
            $altura_fila = 5;
        }
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
    }
    $old_Y = $pdf->getY();
}
$pdf->setXY($new_X, $old_Y+$altura_fila);
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(75.25,5, $this->sma->utf8Decode('Total'),'LBR',0,'R');
$pdf->Cell(19.75,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_cost)),'LBR',0,'R');
$pdf->SetFont('Arial','',$fuente-0.3);
//izquierda
$pdf->ln(35);

if ($pdf->getY() > $maximo_footer) {
    $pdf->AddPage();
}

$pdf->Cell(60.33,5, $this->sma->utf8Decode('Creado por'),'T',0,'L');
$pdf->Cell(5    ,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->Cell(60.33,5, $this->sma->utf8Decode('Responsable'),'T',0,'L');
$pdf->Cell(5    ,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->Cell(60.33,5, $this->sma->utf8Decode('Revisó'),'T',1,'L');



$descargar = false;
$for_email = false;

if ($for_email === true) {
  $pdf->Output(FCPATH."files/".$inv->reference_no.".pdf", "F");
} else {
    if ($descargar) {
      $pdf->Output($inv->reference_no.".pdf", "D");
    } else {
      $pdf->Output($inv->reference_no.".pdf", "I");
    }    
}



