<style type="text/css">
    .site_logo{
        /* content: url("<?= base_url() ?>assets/uploads/logos/logo_wappsi.png"); */
        content: url("<?= base_url() ?>assets/uploads/logos/<?= $this->Settings->logo_app ?>");
        width: 70%;padding: 6% 0% 5.9%;
    }

    @media (max-width: 1000px) {
        .site_logo{ content: url("<?= base_url() ?>assets/uploads/logos/<?= $this->Settings->logo_symbol ?>"); }

        body.mini-navbar .nav-header > .site_logo{ padding: 6% 0% 5.9% !important; }
    }

    body.mini-navbar .nav-header > .site_logo{
        content: url("<?= base_url("assets/uploads/logos/".$this->Settings->logo_symbol); ?>");
        padding: 0;
    }

    .isDisabled {
      color: currentColor;
      cursor: not-allowed;
      opacity: 0.5;
      text-decoration: none;
    }

    @media (max-width: 800px) {
        #pageTitle{
            padding-left : 0 !important;
            padding-right : 0 !important;
            margin-right : 0 !important;
            font-size: 120%;
        }
        #navbar_header_with{
            width : 82% !important;
            position : absolute;
        }
    }
</style>

<script type="text/javascript">
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "onShown": function() {
            isToastShowing = true;
          },
          "onHidden": function() {
            isToastShowing = false;
          }
    };

    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una dirección de correo válida",
        url: "Por favor, escribe una URL válida.",
        date: "Por favor, escribe una fecha válida.",
        dateISO: "Por favor, escribe una fecha (ISO) válida.",
        number: "Por favor, escribe un número entero válido.",
        digits: "Por favor, escribe sólo dígitos.",
        creditcard: "Por favor, escribe un número de tarjeta válido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        accept: "Por favor, escribe un valor con una extensión aceptada.",
        maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
        minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
        rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
        range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
        max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
        pattern: jQuery.validator.format("Formato inválido."),
        min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });

    var _fullScreen = 0;
    var _darkMode = 0;
    var _intervalId = '';
    var _callbackAlert = '';

    function launchFullScreen(element) {
        if (_fullScreen == 0) {
            if(element.requestFullScreen) {
                element.requestFullScreen();
            } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if(element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
            }
            _fullScreen = 1;
        } else {
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
            _fullScreen = 0;
        }
    }

    function darkMode()
    {
        if (_darkMode == 0) {
            $('#page-wrapper').addClass('darkMode');
            _darkMode = 1;
        } else {
            $('#page-wrapper').removeClass('darkMode');
            _darkMode = 0;
        }
    }


</script>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side no-print" role="navigation">
        <div class="sombra-navbar-static-side"></div>
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header" style="padding: 5px 0px; text-align: center;">
                    <img class="site_logo">
                </li>
                <li style="text-align: center;"></li>
                <?php include 'menu_wappsi.php'; ?>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row">
            <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
                <div id="navbar_header_with" class="navbar-header" style="width: 45%">
                    <a class="navbar-minimalize minimalize-styl-2" href="#">
                        <i class="fa fa-bars" style="font-size: 22px"></i>
                    </a>
                    <?php
                        if ($page_title == lang('pos') || $page_title == lang('electronic_pos')) {
                            if ($this->pos_settings->activate_electronic_pos == 1) {
                                $page_title = lang('electronic_pos');
                            }
                        }
                    ?>
                    <h1 class="minimalize-styl-2 hidden-xs hidden-sm title-size text-main" id="pageTitle"><?= $page_title ?></h1>
                    <h1 class="minimalize-styl-2 visible-xs visible-sm" id="pageTitle"><?= ($this->m == 'pos') ? lang('pos_movil') : $page_title ?></h1>
                </div>

                <ul class="nav navbar-top-links navbar-right text-right">

                    <li class="notification dropdown">
                        <a class="link-navbar hidden-xs hidden-sm" id="fullScreen" onclick="launchFullScreen(document.documentElement);" title="Pantalla completa">
                            <span class="fa fa-arrows-alt"></span>
                        </a>
                    </li>

                    <?php if ($page_title == lang('pos') || $page_title == lang('electronic_pos') || $page_title == lang('add_sale') ): ?>
                        <li class="dropdown notification">
                            <button class="new-button dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="Opciones clientes">
                                <i class="fas fa-ellipsis-v fa-lg"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <?php if($page_title !== lang('add_sale')): ?>
                                    <li>
                                        <a onclick="enable_poscustomer_change();" class="external"><i class="fas fa-pen-square" style="font-size: 1.2em;"></i><?= lang('action_edit_customer') ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if($page_title !== lang('add_sale')): ?>
                                    <li>
                                        <a id="view-customer"><i class="fas fa-info-circle" style="font-size: 1.3em;"></i><?= lang('view_customer') ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                    <li>
                                        <a href="<?=admin_url('customers/add?pos_call=1');?>" id="add-customer" class="external" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-square" style="font-size: 1.5em;"></i><?= lang('add_customer') ?>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li role="separator" class="divider"></li>
                                <?php if($page_title !== lang('add_sale')): ?>
                                    <li>
                                        <a class="external" id="sellGiftCard" title="<?=lang('sell_gift_card')?>">
                                            <i class="fa fa-credit-card" style="font-size: 1.3em;"></i><?= lang('gift_card') ?>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if($page_title !== lang('add_sale')): ?>
                                    <?php if ($this->pos_settings->restobar_mode == "1") { ?>
                                        <?php if (!empty($sid)) { ?>
                                            <li role="separator" class="divider"></li>
                                            <li>
                                                <a href="" id="cancel_order"><i class="fas fa-trash" style="font-size: 1.3em;"></i>Anular orden de la mesa</a>
                                            </li>
                                            <li>
                                                <a href="" id="table_change"><i class="fas fa-table" style="font-size: 1.3em;"></i><?= lang("table_change") ?></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif ?>

                    <?php if ($page_title == 'Órdenes de preparación'): ?>
                        <li class="notification dropdown" id="darkMode" onclick="darkMode()">
                            <a class="link-navbar">
                                <span class="fa fa fa-adjust"></span>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php if ($page_title == lang('pos') || $page_title == lang('electronic_pos')): ?>
                        <li class="notification dropdown hidden-xs hidden-sm" title="<?= lang('pos_register_status') . (!$pos_register_status ? lang('pos_register_status_closed') : lang('pos_register_status_open')) ?>">
                            <a class="link-navbar" <?= !$pos_register_status ? 'href="'.admin_url('pos/open_register/1').'"' : '' ?> >
                                <span class="fas fa-store"></span>
                                <?php if (!$pos_register_status): ?>
                                    <span class="label label-danger float-right notification_label">&nbsp;&nbsp;</span>
                                <?php else: ?>
                                    <span class="label label-primary float-right notification_label">&nbsp;&nbsp;</span>
                                <?php endif ?>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php if ($page_title == lang('pos') || $page_title == lang('electronic_pos')): ?>
                        <li class="notification dropdown hidden-xs hidden-sm" title="<?= lang('search_product_price') ?>">
                            <a class="link-navbar" id="search_product_price" data-toggle="modal" data-target="#myModal" href="<?=admin_url('pos/search_product_price')?>" data-toggle="ajax" data-backdrop="static">
                                <span class="fa fa-dollar"></span>
                            </a>
                        </li>

                        <li class="notification dropdown" title="<?= lang('suspended_sales') ?>">
                            <a class="link-navbar opened_bills_sup" id="opened_bills" data-toggle="modal" data-target="#myModal" href="<?=admin_url('pos/opened_bills')?>" data-toggle="ajax" data-backdrop="static">
                                <span class="fas fa-play"></span>
                            </a>
                        </li>
                    <?php endif ?>

                    <li class="notification dropdown">
                        <?php
                            $cntNotif = 0;
                            if ($info) {
                                foreach ($info as $n) {
                                    if (!$this->session->userdata('hidden' . $n->id)) {
                                        $cntNotif++;
                                    }
                                }
                            }
                        ?>

                        <?php if ($qty_alert_num > 0) { $cntNotif++; } ?>
                        <?php if ($new_order_sales > 0) { $cntNotif++; } ?>
                        <?php if ($new_transfers > 0) { $cntNotif++; } ?>
                        <?php if ($Settings->product_expiry) { $cntNotif++; } ?>
                        <?php if ($shop_sale_alerts) {  $cntNotif++; } ?>
                        <?php if ($shop_payment_alerts) { $cntNotif++; } ?>
                        <?php if ($customer_birthday_alerts) { $cntNotif++; } ?>
                        <?php if ($pending_electronic_invoice->quantity > 0) { $cntNotif++; } ?>
                        <a class="dropdown-toggle link-navbar hidden-xs hidden-sm" id="notification_4" data-toggle="dropdown">
                            <span class="fa fa-bell"></span>
                            <?php if ($cntNotif > 0): ?>
                                <span class="label label-danger float-right notification_label"><?= $cntNotif ?></span>
                            <?php endif ?>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="notification_4" style="color: black;">
                            <li class="divider"></li>
                            <?php if ($qty_alert_num > 0) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('reports/quantity_alerts') ?>"> <?= $qty_alert_num; ?> <?= lang('quantity_alerts') ?></a>
                                </li>
                            <?php } ?>
                            <?php if ($new_order_sales > 0) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('sales/orders') ?>"> <?= $new_order_sales; ?> <?= lang('new_sale_orders') ?></a>
                                </li>
                            <?php } ?>
                            <?php if ($new_transfers > 0) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('transfers?new_transfers=1') ?>"> <?= $new_transfers; ?> <?= lang('new_transfers_not_accepted') ?></a>
                                </li>
                            <?php } ?>
                            <?php if ($Settings->product_expiry) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('reports/expiry_alerts') ?>"> <?= $exp_alert_num; ?> <?= lang('expiry_alerts') ?></a>
                                </li>
                            <?php } ?>

                            <?php if ($shop_sale_alerts) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('sales?shop=yes&delivery=no') ?>"> <?= $shop_sale_alerts; ?> <?= lang('sales_x_delivered') ?></a>
                                </li>
                            <?php } ?>

                            <?php if ($shop_payment_alerts) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('sales?shop=yes&attachment=yes') ?>"> <?= $shop_payment_alerts; ?> <?= lang('manual_payments') ?></a>
                                </li>
                            <?php } ?>

                            <?php if ($customer_birthday_alerts) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('customers/customers_birthdays') ?>"> <?= $customer_birthday_alerts; ?> <?= lang('customer_birthday_alerts') ?></a>
                                </li>
                            <?php } ?>
                            <?php if ($today_customer_birthday_alerts) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('customers/customers_birthdays/1') ?>"> <?= $today_customer_birthday_alerts; ?> <?= lang('today_customer_birthday_alerts') ?></a>
                                </li>
                            <?php } ?>
                            <?php
                                if ($info) {
                                    foreach ($info as $n) {
                                        if (!$this->session->userdata('hidden' . $n->id)) {
                                            ?>
                                            <li class="notif-text-content">  <a class="text-wrap" href="#"><?= $n->comment; ?></a></li>
                                        <?php }
                                    }
                                }
                            ?>
                            <?php if ($cntNotif == 0): ?>
                                <li class="notif-text-content" ><a class="text-wrap" href="#"><i class="fa fa-bell-slash-o"></i> Sin notificaciones</a></li>
                            <?php endif ?>

                            <?php if ($pending_electronic_invoice->quantity > 0) { ?>
                                <li class="notif-text-content">
                                    <a class="text-wrap" href="<?= admin_url('sales/fe_index') ?>"> <?= $pending_electronic_invoice->quantity; ?> <?= lang('pending_electronic_document'); ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>

                   <!--  <li class="notification dropdown hidden-lg hidden-md">
                        <a class="dropdown-toggle link-navbar" data-toggle="dropdown" id="menu_user_profile_mobile">
                            <img alt="image" class="user-img-profile" src="<?= $this->session->userdata('avatar') ? base_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : base_url('assets/images/' . $this->session->userdata('gender') . '.png'); ?>">
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="menu_user_profile_mobile" style="color: black; left: -180px;">
                            <?php if ($page_title == lang('pos') || $page_title == lang('electronic_pos')): ?>
                                <li class="dropdown text-right" title="<?= lang('suspended_sales') ?>">
                                    <a class="dropdown-toggle" id="opened_bills" data-toggle="modal" data-target="#myModal" href="<?=admin_url('pos/opened_bills')?>" data-toggle="ajax" data-backdrop="static" data-keyboard="false">
                                        <span class="fas fa-play"> </span> <?= lang('suspended_sales') ?>
                                    </a>
                                </li>
                            <?php endif ?>

                            <li class="dropdown text-right">
                                <a class="dropdown-toggle" id="notification_1" data-toggle="dropdown">
                                    <span class="fa fa-wifi"> </span> <?= lang('internet_status') ?>
                                    <span class="label label-primary float-right notification_label_wifi">&nbsp;&nbsp;</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?= admin_url('wappsi_invoicing/invoices'); ?>"> <i class="fa fa-file-alt"></i> <?= lang('wappsi_invoicing'); ?> </a> </li>
                            <li><a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id')); ?>"> <i class="fa fa-user"></i> <?= lang('profile'); ?> </a> </li>
                            <li><a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id') . '/#cpassword'); ?>"> <i class="fa fa-cogs"></i><?= lang('change_password'); ?></a></li>
                            <li><a href="<?= admin_url('logout'); ?>"> <i class="fa fa-sign-out"></i> <?= lang('logout'); ?></a></li>
                        </ul>
                    </li> -->

                    <li class="user_section dropdown hidden-xs hidden-sm">
                        <a class="dropdown-toggle row link-navbar" data-toggle="dropdown" id="menu_user_profile">
                            <div class="col-sm-3">
                                <img alt="image" class="user-img-profile" src="<?= $this->session->userdata('avatar') ? base_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : base_url('assets/images/' . $this->session->userdata('gender') . '.png'); ?>">
                            </div>
                            <div class="col-sm-9 row">
                                <div class="col-sm-12">
                                    <b><?= $this->session->userdata('username'); ?></b>
                                </div>
                                <div class="col-sm-12">
                                    <p style="font-size: 90%;">
                                        <?= $this->session->userdata('group_name'); ?> <b class="caret"></b>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="menu_user_profile" style="color: black;">
                            <?php if ($page_title == lang('pos') || $page_title == lang('electronic_pos')): ?>
                                <li class="dropdown text-right" title="<?= lang('suspended_sales') ?>">
                                    <a class="dropdown-toggle" id="opened_bills" data-toggle="modal" data-target="#myModal" href="<?=admin_url('pos/opened_bills')?>" data-toggle="ajax" data-backdrop="static" data-keyboard="false">
                                        <span class="fas fa-play"> </span> <?= lang('suspended_sales') ?>
                                    </a>
                                </li>
                            <?php endif ?>

                            <li class="dropdown text-right">
                                <a class="dropdown-toggle" id="notification_1" data-toggle="dropdown">
                                    <span class="fa fa-wifi"> </span> <?= lang('internet_status') ?>
                                    <span class="label label-primary float-right notification_label_wifi">&nbsp;&nbsp;</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?= admin_url('wappsi_invoicing/invoices'); ?>"> <i class="fa fa-file-alt"></i> <?= lang('wappsi_invoicing'); ?> </a> </li>
                            <li><a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id')); ?>"> <i class="fa fa-user"></i> <?= lang('profile'); ?> </a> </li>
                            <li><a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id') . '/#cpassword'); ?>"> <i class="fa fa-cogs"></i><?= lang('change_password'); ?></a></li>
                            <li><a href="<?= admin_url('logout'); ?>"> <i class="fa fa-sign-out"></i> <?= lang('logout'); ?></a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="col-sm-12 div_header_alerts" style="
/*            position: fixed;*/
    z-index: 99999;
    top: 0.5%;
/*    width: 85%;*/
    ">
                <div data-alertnum="0" class="header_alert alert alert-success" role="alert" style="display:none;"> <i class="fa-solid fa-circle-check"></i> <span class="text_alert"> LOREM IPSUM DOLOR SIT AMET </span> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                <div data-alertnum="0" class="header_alert alert alert-info" role="alert" style="display:none;"> <i class="fa fa-solid fa-circle-info"></i> <span class="text_alert"> LOREM IPSUM DOLOR SIT AMET </span> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                <div data-alertnum="0" class="header_alert alert alert-warning" role="alert" style="display:none;"> <i class="fa-solid fa-circle-exclamation"></i> <span class="text_alert"> LOREM IPSUM DOLOR SIT AMET </span> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                <div data-alertnum="0" class="header_alert alert alert-danger" role="alert" style="display:none;"> <i class="fa-solid fa-circle-xmark"></i> <span class="text_alert"> LOREM IPSUM DOLOR SIT AMET </span> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
            </div>
        </div>
        <div class = "animated fadeInRight <?= $this->m == 'dashboard' ? 'row col-sm-12' : '' ?>">

            <?php
                if ($info) {
                    foreach ($info as $n) {
                        if (!$this->session->userdata('hidden' . $n->id)) {
                            ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?= $n->comment; ?>
                            </div>
                        <?php }
                    }
                }
            ?>
            <div class="col-sm-12">
                <?php
                if ($this->session->userdata('swal_flash_message')) {
                    $this->session->set_userdata('flash_error', $this->session->userdata('swal_flash_message'));
                    $this->session->unset_userdata('swal_flash_message');
                }
                 ?>

                <?php if ($this->session->userdata('swal_flash_message_2')): ?>
                    <?php $swal_msg = $this->session->userdata('swal_flash_message_2') ?>
                    <script type="text/javascript">
                        swal(
                          '<?= lang('toast_warning_title') ?>',
                          '<?= $swal_msg ?>',
                          'warning'
                        );
                    </script>
                    <?php $this->session->unset_userdata('swal_flash_message_2') ?>
                <?php endif ?>

                <?php if ($this->session->userdata('swal_flash_message')): ?>
                    <?php $swal_msg = $this->session->userdata('swal_flash_message') ?>
                    <script type="text/javascript">
                        swal(
                          '<?= lang('toast_warning_title') ?>',
                          '<?= $swal_msg ?>',
                          'warning'
                        );
                    </script>
                    <?php $this->session->unset_userdata('swal_flash_message') ?>
                    <?php if ($this->modules_index[$this->v]): ?>
                        <?php $this->session->set_userdata('swal_flash_message_2', $swal_msg) ?>
                    <?php endif ?>
                <?php endif ?>

                <div class="alerts-con"></div>
                <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
                <div class="clearfix"></div>
            </div>
            <div>