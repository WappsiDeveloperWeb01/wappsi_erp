<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-sm" role="document" style="width: 340px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Tipo de reclamo</h4>
        </div>
        <div class="modal-body">
            <label for="">Seleccione el motivo del reclamo</label>
            <div class="row">
                <div class="col-lg-12">
                    <div class="radio">
                        <label>
                            <input type="radio" class="claimCode" name="claimCode" id="claimCode1" value="01">
                            Documento con inconsistencias
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" class="claimCode" name="claimCode" id="claimCode2" value="02">
                            Mercancía no entregada totalmente
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" class="claimCode" name="claimCode" id="claimCode3" value="03">
                            Mercancía no entregada parcialmente
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" class="claimCode" name="claimCode" id="claimCode4" value="04">
                            Servicio no prestado
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <a href="<?= admin_url("documentsReception/sendEvent/") ?>" class="btn btn-primary sendEvent" eventType="<?= $evenType ?>" documentId="<?= $documentId ?>">Guardar</a>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#claimCode1').iCheck('check');

        getClimCode();

        $('.claimCode').on('ifChanged', function(event){
            $('.sendEvent').attr('claimCode', $(this).val());
        });
    });

    function getClimCode()
    {
        $('.claimCode').each(function(){
            if ($(this).is(':checked')) {
                $('.sendEvent').attr('claimCode', $(this).val());
            }
        });
    }
</script>