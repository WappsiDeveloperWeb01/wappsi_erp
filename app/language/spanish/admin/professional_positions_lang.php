<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang["professional_position_saved"] = "Cargo guardado exitosamente";
$lang["professional_position_not_saved"] = "Cargo no ha sido guardado";
$lang["professional_position_existing"] = "El cargo ya se encuentra registrado";