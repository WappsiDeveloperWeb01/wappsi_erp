<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    p {
        word-break: break-all;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("date"); ?></h4>
                    <p><?= $ua->user; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("user"); ?></h4>
                    <p><?= $ua->user; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("type"); ?></h4>
                    <?php $ua_type = [1=>lang('edit'),2=>lang('delete'),3=>lang('add'),4=>lang('search'),5=>lang('modulary_close'),6=>lang('export'), 7=>lang('income')] ?>
                    <p><?= $ua_type[$ua->type_id]; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("module"); ?></h4>
                    <p><?= lang($ua->module_name); ?></p>
                </div>
            </div>
            <?php //1 = Edición, 2 = Eliminación, 3 = Agregar, 4 = consulta, 5. contabilidad cierre ?>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("table"); ?></h4>
                    <p><?= lang($ua->table_name); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4>ID registro afectado</h4>
                    <p><?= $ua->record_id; ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h4><?= lang("description"); ?></h4>
                    <p><?= $ua->description; ?></p>
                </div>
            </div>

            <div class="modal-footer no-print">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= lang('close'); ?></button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>