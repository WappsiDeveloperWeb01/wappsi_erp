<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Quotes_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $warehouse_id = NULL, $biller_id = NULL)
    {
        $limit = $this->Settings->max_num_results_display;
        $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";

        $this->site->create_temporary_product_billers_assoc($biller_id);

        $this->db->select('
                            products.*,
                            FWP.quantity as quantity,
                            categories.id as category_id,
                            categories.name as category_name,
                            brands.name as brand_name
                        ', FALSE)
            ->join($wp, 'FWP.product_id=products.id '.($warehouse_id ? "AND FWP.warehouse_id = '" . $warehouse_id . "'" : ""), 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
            ->join('brands', 'brands.id=products.brand', 'left')
            ->group_by('products.id');
        $this->db->where("
            (
                {$this->db->dbprefix('products')}.name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR 
                {$this->db->dbprefix('products')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR 
                {$this->db->dbprefix('products')}.reference ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
            )");
        $this->db->where('products.type !=', 'raw');
        $this->db->where('categories.hide !=', '1');
        $this->db->where('products.discontinued', '0');
        $this->db->order_by($this->db->dbprefix('products').'.name', 'asc');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
    }

    public function getProductNamesIU($product_id, $warehouse_id, $unit_price_id = NULL, $limit = 1)
    {
        $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";

        $this->db->select('products.*, FWP.quantity as quantity, categories.id as category_id, categories.name as category_name', FALSE)
            ->join($wp, 'FWP.product_id=products.id', 'left')
            // ->join('warehouses_products FWP', 'FWP.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('products.id');
        $this->db->where("({$this->db->dbprefix('products')}.id = ".$product_id." )");
        $this->db->where('products.discontinued', '0');
        // $this->db->order_by('products.name ASC');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWHProduct($id)
    {
        $this->db->select('products.id, code, name, warehouses_products.quantity, cost, tax_rate')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('products.id');
        $q = $this->db->get_where('products', array('warehouses_products.product_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItemByID($id)
    {
        $q = $this->db->get_where('quote_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItemsWithDetails($quote_id)
    {
        $this->db->select('quote_items.id, quote_items.product_name, quote_items.product_code, quote_items.quantity, quote_items.serial_no, quote_items.tax, quote_items.unit_price, quote_items.val_tax, quote_items.discount_val, quote_items.gross_total, products.details, products.hsn_code as hsn_code, products.second_name as second_name');
        $this->db->join('products', 'products.id=quote_items.product_id', 'left');
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('quotes_items', array('quote_id' => $quote_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getQuoteByID($id)
    {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id, $product_order = NULL)
    {
        $this->db->select('
                            quote_items.*,
                            tax_rates.code as tax_code,
                            tax_rates.name as tax_name,
                            tax_rates.rate as tax_rate,
                            products.image,
                            products.details as details,
                            CONCAT('.$this->db->dbprefix('product_variants').'.name, '.$this->db->dbprefix('product_variants').'.suffix) as variant,
                            products.hsn_code as hsn_code,
                            products.second_name as second_name,
                            brands.name as brand_name,
                            products.reference as reference,
                            main_unit.code as main_unit_code,
                            main_unit.id as main_unit_id,
                            units.code as product_unit_code,
                            units.operation_value,
                            units.operator
                        ')
            ->join('products', 'products.id=quote_items.product_id', 'left')
            ->join('brands', 'brands.id=products.brand', 'left')
            ->join('product_variants', 'product_variants.id=quote_items.option_id', 'left')
            ->join('units', 'quote_items.product_unit_id=units.id', 'left')
            ->join('units main_unit', 'products.unit=main_unit.id', 'left')
            ->join('tax_rates', 'tax_rates.id=quote_items.tax_rate_id', 'left')
            ->group_by('quote_items.id');
        if ($product_order == NULL || $product_order == 4) {
            if ($this->Settings->product_order == 1) {
                $this->db->order_by('quote_items.id', 'desc');
            } else {
                $this->db->order_by('quote_items.id', 'asc');
            }
        } else if ($product_order == 1) {
            $this->db->order_by('products.code', 'asc');
        } else if ($product_order == 2) {
            $this->db->order_by('products.name', 'asc');
        } else if ($product_order == 3) {
            $this->db->order_by('products.name', 'desc');
        } else if ($product_order == 5) {
            if ($this->Settings->product_order == 2) {
                $this->db->order_by('quote_items.id', 'asc');
            } else {
                $this->db->order_by('quote_items.id', 'desc');
            }
        }
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addQuote($data = array(), $items = array())
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        } else {
            $reference = $this->site->getReference('qu');
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];

        if ($this->db->insert('quotes', $data)) {
            $quote_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($items as $item) {

                if ($this->Settings->product_variant_per_serial == 1 && !empty($item['serial_no']) && !empty($data['supplier_id'])) {
                    $v_exist = $this->db->get_where('product_variants', ['name'=>$item['serial_no'], 'product_id' => $item['product_id']]);
                    if ($v_exist->num_rows() > 0) {
                        $v_exist = $v_exist->row();
                        $item['option_id'] =  $v_exist->id;
                    } else {
                        $this->db->insert('product_variants', [
                                                        'product_id' => $item['product_id'],
                                                        'name' => $item['serial_no'],
                                                        'cost' => $item['net_unit_price'],
                                                        'price' => 0,
                                                        'quantity' => $item['quantity'],
                                                            ]);

                        $item['option_id'] =  $this->db->insert_id();

                        $this->db->insert('warehouses_products_variants', [
                                            'product_id' => $item['product_id'],
                                            'warehouse_id' => $item['warehouse_id'],
                                            'option_id' => $item['option_id'],
                                            ]);
                    }
                }

                
                $item['quote_id'] = $quote_id;
                $this->db->insert('quote_items', $item);

            }
            return true;
        }
        return false;
    }


    public function updateQuote($id, $data, $items = array())
    {
        if ($this->db->update('quotes', $data, array('id' => $id)) && $this->db->delete('quote_items', array('quote_id' => $id))) {
            foreach ($items as $item) {
                $item['quote_id'] = $id;
                $this->db->insert('quote_items', $item);
            }
            return true;
        }
        return false;
    }

    public function updateStatus($id, $status, $note)
    {
        if ($this->db->update('quotes', array('status' => $status, 'note' => $note), array('id' => $id))) {
            return true;
        }
        return false;
    }


    public function deleteQuote($id)
    {
        if ($this->db->delete('quote_items', array('quote_id' => $id)) && $this->db->delete('quotes', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->where('warehouses_products.warehouse_id', $warehouse_id)
            ->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id)
    {
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, warehouses_products_variants.quantity as quantity')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->where('warehouses_products_variants.warehouse_id', $warehouse_id)
            ->where('warehouses_products_variants.quantity >', 0)
            ->group_by('product_variants.id');
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}
