<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= sprintf(lang('cancel_gift_card'), lang('gift_card')); ?></h4>
        </div>
        <?php $attrib = array('id'=>'cancel_gc');
        echo admin_form_open("sales/cancel_gift_card/" . $gift_card->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group div_creation" <?= $gift_card->creation_type != 4 ? 'style="display:none;"' : '' ?>>
                <?= lang("biller", "gcbiller"); ?>
                <?php
                $bl[""] = "";
                foreach ($billers as $biller) {
                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                }
                echo form_dropdown('biller', $bl, $gift_card->biller_id, 'id="gcbiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" class="form_creation form-control input-tip select" style="width:100%;" '.($gift_card->creation_type == 4 ? 'required="required"' : ''));
                ?>
            </div>
            <div class="form-group div_creation" <?= $gift_card->creation_type != 4 ? 'style="display:none;"' : '' ?>>
                <?= lang("document_type", "gc_document_type_id"); ?>
                <select name="document_type_id" class="form-control form_creation" id="gc_document_type_id" <?= $gift_card->creation_type == 4 ? 'required="required"' : '' ?>>

                </select>
            </div>

            <div class="form-group">
                <?= lang("card_no", "card_no"); ?>
                <?php echo form_input('card_no', $gift_card->card_no, 'class="form-control" id="card_no" readonly'); ?>
            </div>
            <div class="form-group">
                <?= lang("value", "value"); ?>
                <?php echo form_input('value', $this->sma->formatDecimal($gift_card->value), 'class="form-control" id="value" readonly'); ?>
            </div>
            <div class="form-group">
                <?= lang("customer", "customer"); ?>
                <?php echo form_input('customer', $gift_card->customer_id, 'class="form-control" id="customer" readonly'); ?>
            </div>
            <div class="form-group">
                <?= lang("expiry_date", "expiry"); ?>
                <?php echo form_input('expiry', $this->sma->hrsd($gift_card->expiry), 'class="form-control date" id="expiry" readonly'); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?php if ($gift_card->balance == $gift_card->value && ($gift_card->creation_type == 2 || $gift_card->creation_type == 4) && $gift_card->status == 1): ?>
                <button type="button" id="cancel_gift_card" class="btn btn-danger"><?= sprintf(lang('cancel_gift_card'), lang('gift_card')) ?></button>
            <?php else: ?>
                <?php if ($gift_card->creation_type == 1): ?>
                    <em class="text-danger">No se puede anular la tarjeta por este medio</em>
                <?php elseif ($gift_card->status == 3): ?>
                    <em class="text-danger">La tarjeta ya está anulada</em>
                <?php elseif ($gift_card_movements): ?>
                    <em class="text-danger">La tarjeta ya tiene movimientos, no se puede anular</em>
                <?php else: ?>
                    <em class="text-danger">No se puede anular la tarjeta</em>
                <?php endif ?>
            <?php endif ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript">
    $(document).ready(function () {
        
        $("#edit_gc").validate({
              ignore: []
          });
        
        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $('#customer').val('<?=$gift_card->customer_id?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: "<?= admin_url('customers/getCustomer') ?>/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        if (data != null) {
                            callback(data[0]);
                        }
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });//.select2("val", "<?=$gift_card->customer_id?>");
        $('#genNo').click(function () {
            var no = generateCardNo();
            $(this).parent().parent('.input-group').children('input').val(no);
            return false;
        });
        
        $('#cancel_gift_card').on('click', function(){
            if ($('#cancel_gc').valid()) {
                $('#cancel_gc').submit();
            }
        });
        
        $(document).on('change', '#gcbiller', function(){
                $.ajax({
                    url: site.base_url+'billers/getBillersDocumentTypes/62/'+$('#gcbiller').val(),
                    type:'get',
                    dataType:'JSON'
                }).done(function(data){
                    response = data;
                    $('#gc_document_type_id').html(response.options).select2();
                    if (response.not_parametrized != "") {
                        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                                    "showDuration": "500",
                                    "hideDuration": "1000",
                                    "timeOut": "6000",
                                    "extendedTimeOut": "1000",
                                });
                    }
                    if (response.status == 0) {
                      $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                    }
                    $('#gc_document_type_id').trigger('change');
                });
            });
        $('#gcbiller').trigger('change');
    });

</script>    