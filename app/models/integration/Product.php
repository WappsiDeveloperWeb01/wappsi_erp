<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Model
{
    private $newDb;
    private $tableName = 'products';
    private $tableNameTranslation = 'product_translations';

    public function __construct()
    {
        parent::__construct();
        $this->load->integration_model('ProductAttribute');
        $this->load->integration_model('ProductCategory');
        $this->load->integration_model('Translation');
        $this->load->integration_model('Connection');
    }

    public function open($connData)
    {
        $Connection = new Connection();

        $Connection->hostname = $connData->host;
        $Connection->username = $connData->username;
        $Connection->password = $connData->password;
        $Connection->dbname = $connData->databases;

        $this->newDb = $Connection->open();
    }

    public function find($data, $select = '')
    {
        if (!empty($select)) {
            $this->newDb->select($select);
        }
        $this->newDb->where($data);
        $q = $this->newDb->get($this->tableName);
        return $q->row();
    }

    public function getAll()
    {
        $q = $this->newDb->get($this->tableName);
        return $q->result();
    }

    public function create($data)
    {
        if ($this->newDb->insert($this->tableName, $data)) {
            $id = $this->newDb->insert_id();
            $this->createTranslation($data, $id);
            return true;
        }
        return false;
    }

    private function createTranslation($data, $productId)
    {
        $Translation = new Translation();
        $Translation->conn = $this->newDb;
        $Translation->tableName = $this->tableNameTranslation;
        $translationData = [
            "product_id"    => $productId,
            "name"          => $data["name"],
            "unit"          => $data["unit"],
            "description"   => $data["description"],
            "lang"          => strtoupper($this->shop_settings->eshop_lang),
            "created_at"    => date("Y-m-d H:i:s"),
            "updated_at"    => date("Y-m-d H:i:s"),
            "id_wappsi"     => $data["id_wappsi"],
        ];
        if ($Translation->create($translationData)) {
            return true;
        }
        return false;
    }

    public function update($data, $id)
    {
        $this->newDb->where('id', $id);
        if ($this->newDb->update($this->tableName, $data)) {
            if (array_key_exists('name', $data) && array_key_exists('description', $data)) {
                return $this->updateTranslation($data, $id);
            }
            return true;
        }
        return false;
    }

    public function updateProductsWithOutPhoto($data)
    {
        $this->newDb->where('photos is null');
        if ($this->newDb->update($this->tableName, $data)) {
            return true;
        }
    }

    private function updateTranslation($data, $productId)
    {
        $Translation = new Translation();
        $Translation->conn = $this->newDb;
        $Translation->tableName = $this->tableNameTranslation;
        $translation = $Translation->find(["product_id" => $productId]);
        $translationData = [
            "name"          => $data["name"],
            "unit"          => $data["unit"],
            "description"   => $data["description"],
            "updated_at"    => date("Y-m-d H:i:s"),
        ];
        if ($Translation->update($translationData, $translation->id)) {
            return true;
        }
        return false;
    }

    public function delete($id)
    {
        $this->newDb->where("id", $id);
        if ($this->newDb->delete($this->tableName)) {
            return $this->deleteTranslation($id);
        }
        return false;
    }

    private function deleteTranslation($productId)
    {
        $Translation = new Translation();
        $Translation->conn = $this->newDb;
        $Translation->tableName = $this->tableNameTranslation;
        $translation = $Translation->find(["product_id" => $productId]);
        if ($Translation->delete($translation->id)) {
            return true;
        }
        return false;
    }

    public function close()
    {
        $this->newDb->close();
    }
}