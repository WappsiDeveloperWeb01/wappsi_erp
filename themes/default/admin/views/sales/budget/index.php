<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <?= admin_form_open('budget', ['id'=>'budgetFilterForm']) ?>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('year') ?></label>
                                    <?php $year = (isset($_POST["year"])) ? $_POST["year"] : ($this->session->userdata('year') ? $this->session->userdata('year') : date('Y')); ?>
                                    <input class="form-control" type="number" name="year" id="year" value="<?= $year ?>">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('month') ?></label>
                                    <?php $monthId = (isset($_POST["month"])) ? $_POST["month"] : ($this->session->userdata('month') ? $this->session->userdata('month') : date('n')); ?>
                                    <select class="form-control" name="month" id="month">
                                        <?php if (!empty($months)) : ?>
                                            <?php foreach ($months as $month) : ?>
                                                <option value="<?= $month->id ?>" <?= ($month->id == $monthId) ? 'selected' : '' ?>><?= ucfirst(mb_strtolower($month->name)) ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="new-button-container">
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="top" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                    <a href="<?= admin_url("budget/add/{$year}/$monthId") ?>" class="btn btn-primary new-button" data-toggle-second="tooltip" data-placement="top" title="Agregar" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus fa-lg"></i></a>
                                </div>
                            </div>
                        </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-9">
            <div class="ibox">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover" id="budgetTable">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th width="18.66%">Sucursal</th>
                                            <th width="30.64%">Vendedor</th>
                                            <th width="16.66%">Categoria producto</th>
                                            <th width="14%">Valor</th>
                                            <th width="10%">Unidades</th>
                                            <th width="10%">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Total Ventas</h3>
                                    <ul class="list-group">
                                        <?php if (!empty($sumByBranches)) : ?>
                                            <?php $totalAmount = 0 ?>
                                            <?php foreach ($sumByBranches as $sumBranche) { ?>
                                                <?php $totalAmount += $sumBranche->amount ?>
                                            <?php } ?>
                                            <li class="list-group-item">
                                                <label style="float: right; margin: 0"><?= $this->sma->formatMoney($totalAmount) ?></label>
                                                <?= $this->lang->line('allsf') ?>
                                            </li>
                                            <?php else : ?>
                                            <li class="list-group-item">
                                                <label style="float: right; margin: 0"><?= $this->sma->formatMoney(0) ?></label>
                                                <?= "Sin datos" ?>
                                            </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Sumatorias por Sucursal</h3>
                                    <ul class="list-group">
                                        <?php if (!empty($sumByBranches)) : ?>
                                            <?php foreach ($sumByBranches as $sumBranche) { ?>
                                                <li class="list-group-item">
                                                    <label style="float: right; margin: 0"><?= $this->sma->formatMoney($sumBranche->amount) ?></label>
                                                    <?= !empty($sumBranche->name) ? $sumBranche->name : $this->lang->line('allsf') ?>
                                                </li>
                                            <?php } ?>
                                            <?php else : ?>
                                            <li class="list-group-item">
                                                <label style="float: right; margin: 0"><?= $this->sma->formatMoney(0) ?></label>
                                                <?= "Sin datos" ?>
                                            </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Sumatorias por Vendedor</h3>
                                    <ul class="list-group">
                                        <?php if (!empty($sumBySellers)) : ?>
                                            <?php foreach ($sumBySellers as $sumSeller) { ?>
                                                <li class="list-group-item">
                                                    <label style="float: right; margin: 0"><?= $this->sma->formatMoney($sumSeller->amount) ?></label>
                                                    <?= !empty($sumSeller->name) ? $sumSeller->name : $this->lang->line('alls') ?>
                                                </li>
                                            <?php } ?>
                                            <?php else : ?>
                                            <li class="list-group-item">
                                                <label style="float: right; margin: 0"><?= $this->sma->formatMoney(0) ?></label>
                                                <?= "Sin datos" ?>
                                            </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="col-sm-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Sumatorias por Categoría</h3>
                                        <ul class="list-group">
                                            <?php if (!empty($sumByCategories)) : ?>
                                                <?php foreach ($sumByCategories as $sumCategory) { ?>
                                                    <li class="list-group-item">
                                                        <label style="float: right; margin: 0"><?= $this->sma->formatMoney($sumCategory->amount) ?></label>
                                                        <?= !empty($sumCategory->name) ? $sumCategory->name : $this->lang->line('allsf') ?>
                                                    </li>
                                                <?php } ?>
                                            <?php else : ?>
                                                <li class="list-group-item">
                                                    <label style="float: right; margin: 0"><?= $this->sma->formatMoney(0) ?></label>
                                                    <?= "Sin datos" ?>
                                                </li>
                                            <?php endif ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        loadDataTables();

        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle-second="tooltip"]').tooltip();

        $(document).on('change', '#biller_id', function() { loadSellers($(this).val())});
        $(document).on('click', '#saveBudgetButton', function() { saveBudget(); });
        $(document).on('click', '.deleteBudgetButton', function() { deleteBudget($(this)); });
        $(document).on('click', '#saveDoubleBudgetButton', function() { confirmSaveDoubleBudget(); });
    });

    function loadDataTables()
    {
        var _tabFilterFill = false;
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#budgetTable').dataTable({
            aaSorting: [[1, "asc"], [2, "asc"]],
            aLengthMenu: nums,
            iDisplayLength:  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: site.base_url+'budget/getDataTables',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                }, {
                    "name": "year",
                    "value": $('#year').val()
                }, {
                    "name": "month",
                    "value": $('#month').val()
                });

                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                { bVisible: false},
                {
                    mRender: function (data, type, row) {
                        return (data != null) ? data : '<?= $this->lang->line('allsf') ?>';
                    }
                },
                {
                    mRender: function(data, type, row) {
                        return (data != null) ? data : '<?= $this->lang->line('alls') ?>'
                    }
                },
                {
                    mRender: function(data, type, row) {
                        return (data != null) ? data : '<?= $this->lang->line('allsf') ?>'
                    }
                },
                {
                    className: 'text-right',
                    mRender: function(data, type, row) {
                        var opciones = { minimumFractionDigits: 2, maximumFractionDigits: 2 };
                        return new Intl.NumberFormat('en-US', opciones).format(data)
                        // return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    mRender: function(data, type, row) {
                        var opciones = { minimumFractionDigits: 2, maximumFractionDigits: 2 };
                        return new Intl.NumberFormat('en-US', opciones).format(data)
                    }
                },
                {
                    bSortable: false,
                    className: 'text-center'
                }
            ],
            fnDrawCallback: function (oSettings) {
                if (oSettings.aoData.length) {
                    $('.actionsButtonContainer').html('<div class="dropdown">'+
                        '<button class="btn btn-primary btn-outline new-button dropdown-toggle center-block" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                        '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                           /*  '<li>'+
                                '<a href="#" id="excel" data-action="export_excel">'+
                                    '<i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>'+
                                '</a>'+
                            '</li>'+
                            '<li>'+
                                '<a href="#" id="combine" data-action="combine">'+
                                    '<i class="fa fa-file-pdf-o"></i> <?=lang('combine_to_pdf')?>'+
                                '</a>'+
                            '</li>'+
                            '<li>'+
                                '<a href="#" id="post_sale" data-action="post_sale">'+
                                    '<i class="fa fa-file-pdf-o"></i> <?= lang('post_sale') ?>'+
                                '</a>'+
                            '</li>'+
                            '<li class="divider"></li>'+ */
                            '<?php if ($this->Owner || $this->Admin) { ?>'+
                                '<li>'+
                                    '<a href="<?= admin_url("budget/doubleBudget/{$year}/{$monthId}") ?>" data-toggle="modal" data-target="#myModal">'+
                                        '<i class="fa-solid fa-clone"></i> Duplicar Presupuesto'+
                                    '</a>'+
                                '</li>'+
                            '<?php } ?>'+
                        '</ul>'+
                    '</div>');
                }

                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    function loadSellers(billerId, sellerId)
    {
        var options = '<option value=""><?= $this->lang->line('select') ?></option>';
        $('#seller_id').select2('val', '');

        $.ajax({
            type: "post",
            url: site.base_url+'/budget/getSeller',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'billerId': billerId
            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(seller => {
                        options += '<option value="'+seller.companies_id+'">'+seller.name+'</option>';
                    });
                }

                $('#seller_id').html(options);

                if (sellerId) {
                    $('#seller_id').select2('val', sellerId);
                }
            }
        });
    }

    function saveBudget()
    {
        form = $('#budgetForm');

        form.validate({ ignore: [] });

        if(form.valid()){
            $(form).submit();
        }
    }

    function deleteBudget(element)
    {
        swal({
            title: "Eliminación de Presupuesto.",
            text: "¿Está seguro de eliminar el Presupuesto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: 'Eliminando Presupuesto...',
                text: 'Por favor, espere un momento.',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            var year = $('#year').val();
            var month = $('#month').val();
            var budgetId = element.attr('budgetId');
            window.location.href = site.base_url + 'budget/delete/' + budgetId + '/' + year + '/' + month;
        });
    }

    function confirmSaveDoubleBudget()
    {
        var nameMonths = [
            'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ];
        var year = $('#year').val();
        var month = nameMonths[$('#month').val()-1];
        var yearToDouble = $('#yearToDouble').val();
        var monthToDouble = nameMonths[$('#monthToDouble').val()-1];

        swal({
            title: "Duplicación de Presupuesto.",
            text: "¿Está seguro de duplicar el Presupuesto del mes "+ month +" de "+ year +" al mes "+ monthToDouble +" de "+ yearToDouble +"?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: 'Duplicando presupuesto...',
                text: 'Por favor, espere un momento.',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            $('#doubleBudgetForm').submit();
        });
    }
</script>