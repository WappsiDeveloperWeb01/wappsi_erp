<?php

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Órden de pedido'));

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 14.5, 3, '1234', '');
        $cx = 132;
        $cy = 7;
        $this->setXY($cx, $cy);
        $this->Cell(77, 7.25 , $this->sma->utf8Decode('ORDEN DE PEDIDO'),'',1,'C');
        $cy +=7.25;
        $this->setXY($cx, $cy);
        $this->Cell(77, 7.25 , $this->sma->utf8Decode('N°. '.$this->factura->reference_no),'',1,'C');

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 24, 118, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 29, 118, 30, 3, '4', '');
        $cx = 13;
        $cy = 24;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('INFORMACIÓN DEL CLIENTE'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->customer->company != '-' ? $this->customer->company : $this->customer->name)),'',1,'L');
        $cy += 5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Nit/Cc : ".$this->customer->vat_no.($this->customer->tipo_documento == 6 ? "-".$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Sucursal Cliente : ".$this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Dirección : ".$this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Teléfonos : ".$this->customer->phone),'',1,'L');
        //derecha


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(131, 24, 78, 5, 3, '2', 'DF');
        $this->RoundedRect(131, 29, 78, 30, 3, '3', '');
        $cx = 131;
        $cy = 24;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA FACTURA'),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('TOTAL'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, $this->factura->grand_total)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA VENCE'),'LBR',1,'C',1);
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('PLAZO'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".$this->factura->payment_term." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->payment_term ." ".($this->factura->payment_term > 1 ? "Días" : "Día")),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode('VENDEDOR'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode(ucwords(mb_strtolower($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');

        $this->ln();


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->Cell(17,5, $this->sma->utf8Decode('Ref.'),'TBLR',0,'C',1);
        $this->Cell(84,5, $this->sma->utf8Decode('Descripción'),'TBLR',0,'C',1);
        $this->Cell(17,5, $this->sma->utf8Decode('Cant.'),'TBLR',0,'C',1);
        $this->Cell(39.2,5, $this->sma->utf8Decode('Vr. unit.'),'TBLR',0,'C',1);
        $this->Cell(39.2,5, $this->sma->utf8Decode('Vr. total'),'TBLR',1,'C',1);

    }

    function Footer()
    {
        // Print centered page number
        $this->RoundedRect(13, 248, 196, 17.5, 3, '1234', '');
        $this->SetXY(7, -30);
        $this->Cell(196, 5 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
        $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');

    }

    function reduceTextToDescription1($text){
        if (strlen($text) > 390) {
            $text = substr($text, 0, 385);
            $text.="...";
            return $text;
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        if (strlen($text) > 380) {
            $text = substr($text, 0, 375);
            $text.="...";
            return $text;
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$number_convert = new number_convert();

$fuente = 8;
$adicional_fuente = 2;

$color_R = 200;
$color_G = 200;
$color_B = 200;

$pdf->setFillColor($color_R,$color_G,$color_B);

$pdf->biller = $biller;
$pdf->value_decimals = $value_decimals;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $sma;
// exit(var_dump($inv));
$description1 = $inv->note;
$pdf->description2 = $biller->invoice_footer;

$pdf->AddPage();

$maximo_footer = 242;
$taxes = [];
$pdf->SetFont('Arial','',$fuente);
foreach ($rows as $item) {

    if (!isset($taxes[$item->tax_rate_id])) {
        $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
        $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
    } else {
        $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
        $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
    }

    if ($pdf->getY() > $maximo_footer) {
        $pdf->AddPage();
    }

    $inicio_fila_X = $pdf->getX();
    $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_X'] = $pdf->getX();
        $pdf->Cell(17,5, $this->sma->utf8Decode($show_code == 1 ? $item->product_code : $item->reference),'',0,'C');
    $columns[0]['fin_X'] = $pdf->getX();
    $pr_name =  $item->product_name.
                    (!is_null($item->variant) ? "( ".$item->variant." )" : '').
                    ($this->Settings->show_brand_in_product_search ? " - ".$item->brand_name : '').
                    (!is_null($item->serial_no) ? " - ".$item->serial_no : '').
                    (!is_null($item->preferences) ? ", ".$this->sma->print_preference_selection($item->preferences) : "");
    $cX = $pdf->getX();
    $cY = $pdf->getY();
    $inicio_altura_fila = $cY;
    $columns[1]['inicio_X'] = $pdf->getX();
    $pdf->setXY($cX, $cY+0.5);
        $pdf->MultiCell(84,3, $this->sma->utf8Decode($pr_name),'','L');
    $columns[1]['fin_X'] = $cX+84;
    $fin_altura_fila = $pdf->getY();
    $pdf->setXY($cX+84, $cY);
    $columns[2]['inicio_X'] = $pdf->getX();
        $pdf->Cell(17,5, $this->sma->utf8Decode($this->sma->formatQuantity($item->quantity, $qty_decimals)),'',0,'C');
    $columns[2]['fin_X'] = $pdf->getX();
    $columns[3]['inicio_X'] = $pdf->getX();
        $pdf->Cell(39.2,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->unit_price)),'',0,'R');
    $columns[3]['fin_X'] = $pdf->getX();
    $columns[4]['inicio_X'] = $pdf->getX();
        $pdf->Cell(39.2,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->unit_price * $item->quantity)),'',0,'R');
    $columns[4]['fin_X'] = $pdf->getX();
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = $fin_altura_fila - $inicio_altura_fila;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    // $this->sma->print_arrays($columns);
    foreach ($columns as $key => $data) {
        $altura_fila = $altura_fila >= 5 ? $altura_fila : 5;
        $ancho_column = $data['fin_X'] - $data['inicio_X'];
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
    }
    $pdf->ln($altura_fila);
}

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(115.6,6.125, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->setX(14);
$pdf->Cell(115.6,6.125, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total)),'',1,'L');
$pdf->setX(13);

$current_x = $pdf->getX();
$current_y = $pdf->getY();
$total = $inv->total;

$pdf->Cell(28.9,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Impuesto'),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');

foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $total+=$arr['tax'];

    $pdf->Cell(28.9,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"),'',1,'C');

    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['base'])),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['tax'])),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total)),'',1,'C');
}

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;


//derecha

$pdf->setXY($cX_items_finished+118, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->cell(39.2, 5, $this->sma->utf8Decode('TOTAL BRUTO'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney(($inv->total + $inv->product_tax))), 'TBR', 1, 'R');
$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('DESCUENTO'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney($inv->order_discount)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Sub Total'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney(($inv->total + $inv->product_tax - $inv->order_discount))), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney($inv->order_tax)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Retención en la fuente'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(0), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('TOTAL'), 'TBLR', 1, 'R', 1);
$pdf->setXY($current_x+39.2, $current_y);
$pdf->SetFont('Arial','',$fuente);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney($inv->grand_total)), 'TBR', 1, 'R');


//FIRMAS

//izquierda
$pdf->ln(2);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;

$pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Representante Legal'), '', 1, 'L');

//derecha
$pdf->setXY($current_x+115.6, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();
$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

$descargar = false;

if (isset($for_email)) {
  $pdf->Output(FCPATH."files/".$inv->reference_no.".pdf", "F");
} else {
    if ($descargar) {
      $pdf->Output($inv->reference_no.".pdf", "D");
    } else {
      $pdf->Output($inv->reference_no.".pdf", "I");
    }
}
