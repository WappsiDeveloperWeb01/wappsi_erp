<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Payment_method extends CI_Model
{
    private $table_name = 'payment_methods';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($data)
    {
        $this->db->where($data);
        $q = $this->db->get($this->table_name);
        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }
}