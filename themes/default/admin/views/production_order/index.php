
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script>
    $(document).ready(function () {
        oTable = $('#dmpData').dataTable({
            "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('production_order/getProductionOrders/'); ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "start_date",
                    "value": "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
                }, {
                    "name": "end_date",
                    "value": "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>"
                }, {
                    "name": "biller",
                    "value": "<?= isset($_POST['biller']) ? $_POST['biller'] : NULL ?>"
                }, {
                    "name": "employee",
                    "value": "<?= isset($_POST['employee']) ? $_POST['employee'] : NULL ?>"
                }, {
                    "name": "product",
                    "value": "<?= isset($_POST['product']) ? $_POST['product'] : NULL ?>"
                }, {
                    "name": "only_cutting_pending",
                    "value": ($('#only_cutting_pending').prop('checked') ? 1 : 0)
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                    {"bSortable": false, "mRender": checkbox},
                    {"mRender": fld},
                    null,
                    null,
                    null,
                    null,
                    {"mRender": por_num, "bSearchable" : false},
                    {"mRender": por_num, "bSearchable" : false},
                    {"mRender": por_status},
                    {"mRender": por_num, "bSearchable" : false},
                    {"mRender": por_num, "bSearchable" : false},
                    {"mRender": por_num, "bSearchable" : false},
                    {"mRender": por_num, "bSearchable" : false},
                    {"mRender": por_status},
                    {"mRender": print_por_notification},
                    {"bSortable": false}
                ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "making_order_link";
                return nRow;
            },
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('created_by');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang(' note');?>]", filter_type: "text", data: []},
        ], "footer");

        $(document).on('ifUnchecked', '#only_cutting_pending', function(){
            oTable.fnUpdate();
        });

        $(document).on('ifChecked', '#only_cutting_pending', function(){
            oTable.fnUpdate();
        });

        if (localStorage.getItem('remove_porls')) {
            if (localStorage.getItem('poritems')) {
                localStorage.removeItem('poritems');
            }
            if (localStorage.getItem('porwarehouse')) {
                localStorage.removeItem('porwarehouse');
            }
            if (localStorage.getItem('pornote')) {
                localStorage.removeItem('pornote');
            }
            if (localStorage.getItem('pordate')) {
                localStorage.removeItem('pordate');
            }
            if (localStorage.getItem('porestimated_date')) {
                localStorage.removeItem('porestimated_date');
            }
            if (localStorage.getItem('document_type_id')) {
                localStorage.removeItem('document_type_id');
            }
            if (localStorage.getItem('poremployee')) {
                localStorage.removeItem('poremployee');
            }
            if (localStorage.getItem('porbiller')) {
                localStorage.removeItem('porbiller');
            }
            if (localStorage.getItem('oslsale_status')) {
                localStorage.removeItem('oslsale_status');
            }
            localStorage.removeItem('remove_porls');
        }

        <?php if ($this->session->userdata('remove_porls')) { ?>
            if (localStorage.getItem('poritems')) {
                localStorage.removeItem('poritems');
            }
            if (localStorage.getItem('porwarehouse')) {
                localStorage.removeItem('porwarehouse');
            }
            if (localStorage.getItem('pornote')) {
                localStorage.removeItem('pornote');
            }
            if (localStorage.getItem('pordate')) {
                localStorage.removeItem('pordate');
            }
            if (localStorage.getItem('porestimated_date')) {
                localStorage.removeItem('porestimated_date');
            }
            if (localStorage.getItem('document_type_id')) {
                localStorage.removeItem('document_type_id');
            }
            if (localStorage.getItem('poremployee')) {
                localStorage.removeItem('poremployee');
            }
            if (localStorage.getItem('porbiller')) {
                localStorage.removeItem('porbiller');
            }
        <?php $this->sma->unset_data('remove_porls');}
        ?>
    });
</script>


<div class="row wrapper border-bottom white-bg page-heading no-print">
    <div class="col-lg-8">
        <h2><?= lang('making_order').' ('.($warehouse ? $warehouse->name : lang('all_warehouses')).')'; ?></h2>
        <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
            <!-- <?//= lang('your_ip') . ' ' . $ip_address . " <span class='hidden-sm'>( " . lang('last_login_at') . ": " . date($dateFormats['php_ldate'], $this->session->userdata('old_last_login')) . " " . ($this->session->userdata('last_ip') != $ip_address ? lang('ip:') . ' ' . $this->session->userdata('last_ip') : '') . " )</span>" ?> -->
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight no-print">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins border-bottom">
            <div class="ibox-title">
                <h5><?= lang('filter') ?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <?= admin_form_open('production_order') ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?= lang("product", "suggest_product"); ?>
                                    <?php echo form_input('sproduct', (isset($_POST['sproduct']) ? $_POST['sproduct'] : ""), 'class="form-control" data-type="pfinished" id="suggest_product"'); ?>
                                    <input type="hidden" name="product" value="<?= isset($_POST['product']) ? $_POST['product'] : "" ?>" id="report_product_id"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                <?php
                                $biller_selected = '';
                                $biller_readonly = false;
                                if ($this->session->userdata('biller_id')) {
                                    $biller_selected = $this->session->userdata('biller_id');
                                    $biller_readonly = true;
                                }

                                $bl[""] = lang('select');
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                ?>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="biller"><?= lang("responsable"); ?></label>
                                <?php
                                $empl[""] = lang('select');
                                if ($employees) {
                                    foreach ($employees as $employee) {
                                        $empl[$employee->id] = $employee->company != '-' ? $employee->company : $employee->name;
                                    }
                                }
                                echo form_dropdown('employee', $empl, (isset($_POST['employee']) ? $_POST['employee'] : ''), 'class="form-control" id="employee" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("employee") . '"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                <?= lang('date_records_filter', 'date_records_filter') ?>
                                <select name="date_records_filter" id="date_records_filter" class="form-control">
                                    <option value="5" selected="selected"><?= lang('filter_records_date_range') ?></option>
                                    <option value="1"><?= lang('filter_records_today') ?></option>
                                    <option value="2"><?= lang('filter_records_month') ?></option>
                                    <option value="3"><?= lang('filter_records_trimester') ?></option>
                                    <option value="4"><?= lang('filter_records_year') ?></option>
                                    <option value="6"><?= lang('filter_records_last_month') ?></option>
                                    <option value="7"><?= lang('filter_records_last_year') ?></option>
                                </select>
                            </div>
                            <div class="date_controls">
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('start_date', 'start_date') ?>
                                    <input type="date" name="start_date" id="start_date" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                </div>
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('end_date', 'end_date') ?>
                                    <input type="date" name="end_date" id="end_date" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" id="submit-sales-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                            </div>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $GP['bulk_actions']) {
        echo admin_form_open('production_order/actions', 'id="action-form"');
    }
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle">
                                    <?= lang('actions') ?> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?= admin_url('production_order/add') ?>">
                                            <i class="fa fa-plus-circle"></i> <?= lang('add_making_order') ?>
                                        </a>
                                    </li>
                                    <!-- <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li> -->
                                    <li class="divider"></li>
                                </ul>
                            </div>

                            <?php if (!empty($warehouses)) { ?>
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle">
                                <?= lang("warehouses") ?> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                                    <li><a href="<?= admin_url('products/quantity_adjustments') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                                    <li class="divider"></li>
                                    <?php
                                    foreach ($warehouses as $warehouse) {
                                        echo '<li><a href="' . admin_url('products/quantity_adjustments/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php } ?>

                            <label class="pull-right" style="padding-right: 5px;">
                                <?= lang('view_only_cutting_pending') ?>
                                <input type="checkbox" name="only_cutting_pending" id="only_cutting_pending" checked>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="text_filter"></h4>
                            <div class="table-responsive">
                                <table id="dmpData" class="table table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th class=""><?= lang("date"); ?></th>
                                        <th class=""><?= lang("days"); ?></th>
                                        <th class=""><?= lang("reference_no"); ?></th>
                                        <th class=""><?= lang("responsable"); ?></th>
                                        <th class=""><?= lang("reference"); ?></th>
                                        <th><?= lang('ordered') ?></th>
                                        <th><?= lang('cutted') ?></th>
                                        <th><?= lang('production_order_cutted_status') ?></th>
                                        <th><?= lang('in_assemble') ?></th>
                                        <th><?= lang('assembled') ?></th>
                                        <th><?= lang('in_packing') ?></th>
                                        <th><?= lang('packed') ?></th>
                                        <th class=""><?= lang("status"); ?></th>
                                        <th></th>
                                        <th style="text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr class="active">
                                        <th style="width:1.14%;text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th style="width:10.14%;"></th>
                                        <th style="width:2.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:15.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:7.14%;"></th>
                                        <th style="width:7.14%; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">

    $(document).ready(function(){
        setFilterText();
    });

    function setFilterText(){
        var start_date_text = $('#start_date').val();
        var end_date_text = $('#end_date').val();
        var suggest_product = $('#suggest_product').val();
        var text = "Filtros configurados : ";
        coma = false;
        if (start_date != '' && start_date !== undefined) {
            text+= coma ? "," : "";
            text+=" Fecha de inicio ("+start_date_text+")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text+= coma ? "," : "";
            text+=" Fecha final ("+end_date_text+")";
            coma = true;
        }
        if (suggest_product != '' && suggest_product !== undefined) {
            text+= coma ? "," : "";
            text+=" Producto ("+suggest_product+")";
            coma = true;
        }
        $('.text_filter').html(text);
    }

    $('#date_records_filter').on('change', function(){
        filter = $(this).val();
        fecha_inicial = "";
        fecha_final = "<?= date('Y-m-d') ?>";
        hide_date_controls = true;
        if (filter == 5) { //RANGO DE FECHAS
            fecha_final = "";
            hide_date_controls = false;
        } else if (filter == 1) { // HOY
            fecha_inicial = "<?= date('Y-m-d') ?>";
        } else if (filter == 2) { // MES
            fecha_inicial = "<?= date('Y-m-01') ?>";
        } else if (filter == 3) { // TRIMESTRE
            fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
        } else if (filter == 4) { // AÑO
            fecha_inicial = "<?= date('Y-01-01') ?>";
        }else if (filter == 6) { // MES PASADO
            fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 1 month")) ?>";
            fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
        }else if (filter == 7) { // AÑO PASADO
            fecha_inicial = "<?= date("01/01/Y 00:00",strtotime(date("Y-m-01")."- 1 year")) ?>";
            fecha_final = "<?= date("31/12/Y 23:59",strtotime(date("Y-m-01")."- 1 year")) ?>";
        } else if (filter == 0) {
            fecha_inicial = "";
            fecha_final = "";
        }
        $('#start_date').val(fecha_inicial);
        $('#end_date').val(fecha_final);
        if (hide_date_controls) {
            $('.date_controls').css('display', 'none');
        } else {
            $('.date_controls').css('display', '');
        }
    });

    <?php if (!isset($_POST['date_records_filter']) && $this->Settings->default_records_filter != 0) { ?>
        $('#date_records_filter').val("<?= $this->Settings->default_records_filter ?>").trigger('change');
    <?php } else if (isset($_POST['date_records_filter'])) { ?>
        $('#date_records_filter').val("<?= $_POST['date_records_filter'] ?>").trigger('change');
        <?php if ($_POST['date_records_filter'] == 5) { ?>
            <?php if ($_POST['start_date'] != '') { ?>
                $('#start_date').val("<?= $_POST['start_date'] ?>");
            <?php } ?>
            <?php if ($_POST['end_date'] != '') { ?>
                $('#end_date').val("<?= $_POST['end_date'] ?>");
            <?php } ?>
        <?php } ?>
    <?php } ?>
</script>