<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
	.ibox-content {
		height: 100%;
		overflow: auto;
		padding: 10px 0;
	}
	.contenedor-canvas {
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.225);
		height: 700px;
		margin: auto;
		width:1200px;
	}
	.nav-link label {
		cursor: inherit;
	}
	.panel {
		cursor: pointer;
	}
	.panel_heading {
		padding: 8px 5px;
	}
	.panel_body {
		padding: 5px;
	}
	.panel_body_item {
		margin: 0px;
		padding: 5px;
	}
	.preferences{
		padding: 0px 10px 0px 10px !important;
	    margin: : 0px !important;
	    background-color: antiquewhite;
	    font-size: 85%;
	    width: 100%;
	}
    .darkMode {
        background-color: rgb(0 0 0 / 60%) !important;
    }
    .textInfoNoResult {
        color: #989fb0;
    }
    #preparation_orders_container {
        margin: 15px -10px;
        min-height: 610px;
    }
</style>

<div id="preparation_orders_container">
    <?php $activateNotification = false; ?>
    <?php $existingPendingStatus = false; ?>

    <?php if (!empty($preparation_orders)) { ?>
        <?php foreach ($preparation_orders as $i => $preparation_order) { ?>
            <?php $order_in_preparation = 0; ?>

            <?php foreach ($preparation_order['productos'] as $product) { ?>
                <?php if ($product['browser_readiness_status'] == PREPARATION) { $order_in_preparation++; } ?>
            <?php } ?>

            <?php $panel_color = ($order_in_preparation > 0) ? "panel-warning" : "panel-success"; ?>
            <?php if ($order_in_preparation == 0) { $existingPendingStatus = true; } ?>

            <?php
                if ($preparation_order['tipo'] == "M") {
                    $tipo = "Mesa ";
                } else if ($preparation_order['tipo'] == "D") {
                    $tipo = "Domicilio ";
                } else {
                    $tipo = "Llevar ";
                }
            ?>
            <div class="col-md-2">
                <div class="panel <?= $panel_color; ?> order_panel" id="<?= $i; ?>" data-suspended_sale="<?= $preparation_order["id_venta_suspendida"]; ?>" data-suspended_status_order="<?= $preparation_order["estado_orden"]; ?>">
                    <div class="panel-heading order_panel_heading">
                        <div class="row">
                            <div class="col-xs-6  col-sm-6">
                                <h3 style="font-weight: bold;"> <?= $tipo . $preparation_order['numero_mesa']; ?></h3>
                            </div>
                            <div class="col-xs-6  col-sm-6 text-right">
                                <label>Orden No: <?= $preparation_order['id_venta_suspendida']; ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6  col-sm-6">
                                <label><?= $preparation_order['nombre_vendedor']; ?></label>
                            </div>
                            <div class="col-xs-6  col-sm-6 text-right">
                                <label>
                                    <?php
                                        $fecha = $preparation_order['fecha_orden'];
                                        $fecha1 = new DateTime($fecha);
                                        $fecha2 = new DateTime(date('Y-m-d H:i:s'));

                                        $intervalo = $fecha1->diff($fecha2);

                                        $minutes = $preparation_order['diferencia_minutos'] < 10 ? '0'.$preparation_order['diferencia_minutos'] : $preparation_order['diferencia_minutos'];
                                        $seconds = $intervalo->format('%s') < 10 ? '0'. $intervalo->format('%s') : $intervalo->format('%s');
                                    ?>
                                    <?= $minutes; ?>:<?= $seconds; ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body panel_body">
                        <?php foreach ($preparation_order['productos'] as $product) { ?>
                            <?php if ($product['estado_producto'] == CANCELLED) { ?>
                                <p class="bg-danger panel_body_item"><?= $product['cantidad_producto'] . '  '. $product['nombre_producto'] ?></p>
                                <?php if (!empty($product['preferencias'])) { ?>
                                    <em class="preferences"><?= $product['preferencias'] ?></em>
                                <?php } ?>
                            <?php } else { ?>
                                <p class="panel_body_item"><?= $product['cantidad_producto'] .' '. $product['nombre_producto'] ?></p>
                                <?php if (!empty($product['preferencias'])) { ?>
                                    <em class="preferences"><?= $product['preferencias'] ?></em>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php $activateNotification = ($existingPendingStatus) ? true : false; ?>
        <?php } ?>

    <?php } else { ?>
        <div class="col-sm-12 text-center" style="display: flex; align-items: center; height: 500px;">
            <div style=" margin: auto;">
                <img src="<?= base_url('assets/images/NoResults.svg'); ?>" alt="">
                <h3 class="text-center textInfoNoResult"> No existen ordenes de preparación </h3>
            </div>
        </div>
    <?php } ?>
</div>

<?= form_open(admin_url('pos/restobar'), 'id="formulario_sucursal"'); ?>
	<input type="hidden" name="id_sucursal" id="id_sucursal" value="">
	<input type="hidden" name="id_area" id="id_area">
<?= form_close(); ?>

<?= form_open(admin_url('pos/create_suspended_sale'), 'id="formulario_crear_cuenta"'); ?>
	<input type="hidden" name="id_sucursal" id="id_sucursal" value="">
	<input type="hidden" name="numero_mesa" id="numero_mesa">
	<input type="hidden" name="id_mesa" id="id_mesa">
<?= form_close(); ?>

<div id="modal_container"></div>

<script type="text/javascript">
	$(document).ready(function() {
		_current_modal_id = '';

		load_order_preparation();

		set_same_height('order_panel');
		$('body').addClass('mini-navbar');

		sessionStorage.setItem("user_id", '<?= $this->session->userdata("user_id"); ?>');

		$(document).on('click', '.order_panel', function() { open_modal($(this).attr('id'), $(this).data('suspended_sale'), $(this).data('suspended_status_order')); });
		$(document).on('click', '.product_table_row', function() { select_product($(this).data('row_index')); });
		$(document).on('click', '#dispatch_products', function() { dispatch_products($(this).val(), $(this).data('suspended_sale_id'), $(this).data('table_id')); });


		$(document).on('ifToggled', '.product_check', function(event) {
		  	select_product();
		});

        <?php if($activateNotification){ ?>
            if (site.settings.pos_order_preparation_notification != 0) {
                pos_order_preparation_notification.loop = (site.settings.repeat_pos_order_preparation_notification == '1') ? true : false;
                pos_order_preparation_notification.play();
            }
        <?php } ?>

        toastr.options.timeOut = 2000;
	});

	function load_order_preparation()
	{
        <?php
            if ($this->pos_settings->preparation_panel_update_time == 1) {
                $time = 5000;
            } else if ($this->pos_settings->preparation_panel_update_time == 2) {
                $time = 10000;
            } else if ($this->pos_settings->preparation_panel_update_time == 3) {
                $time = 15000;
            } else if ($this->pos_settings->preparation_panel_update_time == 4) {
                $time = 30000;
            } else if ($this->pos_settings->preparation_panel_update_time == 5) {
                $time = 45000;
            } else if ($this->pos_settings->preparation_panel_update_time == 6) {
                $time = 60000;
            }
        ?>
		setInterval(function(){
            $.ajax({
                url : '<?= admin_url('pos/get_order_preparations') ?>',
				dataType : 'JSON'
			}).done(function(data) {
                if (data.order_container != '') {
                    $('#preparation_orders_container').html(data.order_container);

                    if (site.settings.pos_order_preparation_notification != 0) {
                        pos_order_preparation_notification.loop = false;
                        if (data.activateNotification === true) {
                            pos_order_preparation_notification.loop = (site.settings.repeat_pos_order_preparation_notification == '1') ? true : false;
                            pos_order_preparation_notification.play();
                        }
                    }
				} else {
                    $('#preparation_orders_container').html('<div class="col-sm-12 text-center" style="display: flex; align-items: center; height: 500px;">'+
                        '<div style=" margin: auto;">'+
                            '<img src="<?= base_url('assets/images/NoResults.svg') ?>" />'+
                            '<h3 class="text-center textInfoNoResult"> No existen ordenes de preparación </h3>' +
                        '</div>'+
                    '</div>');
				}

				set_same_height('order_panel');
			});
		} , <?= $time; ?>);
	}

	function open_modal(order_panel_id, suspended_sale_id, suspended_status_order)
	{
		change_order_status(suspended_sale_id, order_panel_id);

		$.ajax({
			url : '<?= admin_url('pos/get_order_preparation_modal/') ?>'+order_panel_id,
			dataType : 'JSON'
		})
		.done(function(data) {
			$('#modal_container').html(data.modal_container);
			$('#orden_modal_'+order_panel_id).modal();
			_current_modal_id = order_panel_id;

			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
	    		radioClass: 'iradio_square-blue'
			});
		});
	}

	function change_order_status(suspended_sale_id, order_panel_id)
	{
		$.ajax({
			url: '<?= admin_url("pos/change_order_status"); ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
				'suspended_sale_id': suspended_sale_id,
				'order_panel_id': order_panel_id
			},
		})
		.done(function(data) {
			if (data.response == 1) {
				Command: toastr.success(data.message, "Proceso exitoso");
			}
		})
		.fail(function(data) {
			console.log(data.responseText);
		});
	}

	function select_product(index = '')
	{
		if (index != '') {
			$('#product_check_'+index).iCheck('toggle');
		}

		product_account_id_array = [];

		$('.product_check').each(function(){
			if($(this).is(':checked')) {
				product_account_id_array.push($(this).val());
			}
		});

		if (product_account_id_array == "") {
			$('#dispatch_products').html('Cerrar');
		} else {
			$('#dispatch_products').html('Marcar despachado');
		}
	}

	function dispatch_products(restobar_module, suspended_sale_id, table_id)
	{
		product_account_id_array = [];

		$('.product_check').each(function(){
			if($(this).is(':checked')) {
				product_account_id_array.push($(this).val());
			}
		});

		if (product_account_id_array == "") {
			$('#orden_modal_'+_current_modal_id).modal('hide');

			return false;
		}

		$.ajax({
			url: '<?= admin_url("pos/dispatch_products_order"); ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
				'product_account': JSON.stringify(product_account_id_array)
			},
		})
		.done(function(data) {
			if (data.response == "1") {
				if (restobar_module != "M") {
					existing_products_not_dispatched(suspended_sale_id, table_id);
				}

				Command: toastr.success(data.message, 'Proceso exitoso.', { onHidden: function(){ $('#orden_modal_'+_current_modal_id).modal('hide'); }});
			} else  {
				Command: toastr.error(data.message, '¡Proceso fallido!');
			}
		})
		.fail(function(data) {
			console.log(data.responseText);
		});
	}

	function existing_products_not_dispatched(suspended_sale_id, table_id)
	{
		$.ajax({
			url: '<?= admin_url("pos/existing_products_not_dispatched"); ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
				'suspended_sale_id': suspended_sale_id
			},
		})
		.done(function(data) {
			if (data == false) {
				delete_suspended_sale(suspended_sale_id, table_id);
			}
		})
		.fail(function(data) {
			console.log(data.responseText);
		});
	}

	function delete_suspended_sale(suspended_sale_id, table_id)
	{
		$.ajax({
			url: '<?= admin_url("pos/delete_suspended_sale"); ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
				'suspended_sale_id': suspended_sale_id,
				'table_id' : table_id
			},
		})
		.done(function(data) {
			if (data.response == "1") {
				Command: toastr.success(data.message, 'Proceso exitoso.', { onHidden: function(){ $('#orden_modal_'+_current_modal_id).modal('hide'); }});
			} else  {
				Command: toastr.error(data.message, '¡Proceso fallido!');
			}
		})
		.fail(function(data) {
			console.log(data.responseText);
		});
	}

</script>