<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<script type="text/javascript">
    <?php if($this->Settings->allow_returns_date_before_current_date == 0) { ?>
        var end_date = "<?= date('d/m/Y H:i') ?>";  // esta opcion es si la fecha va a ser como minimo el día que se esta devolviendo la venta
    <?php } ?>
    <?php if($this->Settings->allow_returns_date_before_current_date == 1) { ?>
        var end_date = "<?= date('d/m/Y H:i', strtotime($inv->date)) ?>"; // esta opcion es si la fecha va a ser como minimo el día que se creo la venta
    <?php } ?>
    var total_ce_rete_amount = parseFloat("<?= $total_ce_rete_amount ?>");
    if (total_ce_rete_amount > 0) {
        command: toastr.warning('La compra sólo se puede devolver completamente, tiene retenciones en pagos posteriores', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
    }
</script>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open_multipart("purchases/return_purchase/" . $inv->id, array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-resl-form')); ?>
                            <input type="hidden" name="order_discount_method" value="<?= $inv->order_discount_method ?>">
                            <?php if ($discounts): ?>
                                <?php foreach ($discounts as $discount): ?>
                                    <input type="hidden" name="discount_amount[]" value="<?= $discount['amount'] ?>" data-originalamount="<?= $discount['amount'] ?>">
                                    <input type="hidden" name="discount_ledger_id[]" value="<?= $discount['ledger_id'] ?>">
                                    <input type="hidden" name="discount_description[]" value="<?= $discount['description'] ?>">
                                <?php endforeach ?>
                            <?php endif ?>

                            <?php if ($ce_retentions): ?>
                                <?php foreach ($ce_retentions as $ce_reference => $ce_rete_type): ?>
                                    <?php foreach ($ce_rete_type as $ce_type => $ce_retention): ?>
                                        <input type="hidden" name="ce_retention_type[<?= $ce_reference ?>][]" value="<?= $ce_type ?>">
                                        <input type="hidden" name="ce_retention_total[<?= $ce_reference ?>][]" value="<?= $ce_retention['total'] ?>">
                                        <input type="hidden" name="ce_retention_base[<?= $ce_reference ?>][]" value="<?= $ce_retention['base'] ?>">
                                        <input type="hidden" name="ce_retention_account[<?= $ce_reference ?>][]" value="<?= $ce_retention['account'] ?>">
                                        <input type="hidden" name="ce_retention_percentage[<?= $ce_reference ?>][]" value="<?= $ce_retention['percentage'] ?>">
                                        <input type="hidden" name="ce_retention_assumed[<?= $ce_reference ?>][]" value="<?= $ce_retention['assumed'] ?>">
                                        <input type="hidden" name="ce_retention_assumed_account[<?= $ce_reference ?>][]" value="<?= $ce_retention['assumed_account'] ?>">
                                    <?php endforeach ?>
                                <?php endforeach ?>
                            <?php endif ?>
                            <input type="hidden" name="shipping" id="shipping">
                            <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                            <input type="hidden" name="order_tax" value="" id="retax2" required="required"/>
                            <input type="hidden" name="discount" value="" id="rediscount" required="required"/>
                            <input type="hidden" name="supportDocument" id="supportDocument" value="<?= $supportDocument ?>">
                            <input type="hidden" name="invoiceDate" id="invoiceDate" value="<?= $inv->date ?>">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <?php if ($Owner || $Admin) { ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("date", "redate"); ?>
                                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="redate" required="required"'); ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php
                                            $bl[""] = "";
                                            $bldata = [];
                                            foreach ($billers as $biller) {
                                                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                $bldata[$biller->id] = $biller;
                                            }
                                        ?>
                                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                            <div class="col-md-4  same-height form-group">
                                                <?= lang("biller", "rebiller"); ?>
                                                <?php
                                                $bl[""] = "";
                                                foreach ($billers as $biller) {
                                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                }
                                                ?>
                                                <select name="biller" class="form-control" id="rebiller" required="required">
                                                    <?php foreach ($billers as $biller): ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" <?= $inv->biller_id == $biller->id ? 'selected="selected"' : '' ?>><?= $biller->company ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("biller", "rebiller"); ?>
                                                    <select name="biller" class="form-control" id="rebiller">
                                                        <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                            $biller = $bldata[$this->session->userdata('biller_id')];
                                                            ?>
                                                            <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                        <?php endif ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="col-md-4 same-height">
                                            <div class="form-group">
                                                <?= lang("reference_no", "reref"); ?><br>
                                                <div style="display: flex;">
                                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required" style="<?= empty($supportDocument) ? 'width: 30%' : 'width: 100%;' ?>"></select>
                                                    <?php if (empty($supportDocument)) : ?>
                                                        <?= form_input('reference_no', '', 'class="form-control input-tip" id="reref" style="width: 70%;"'); ?>
                                                    <?php endif ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("return_surcharge", "return_surcharge"); ?>
                                                <?php echo form_input('return_surcharge', (isset($_POST['return_surcharge']) ? $_POST['return_surcharge'] : ''), 'class="form-control input-tip" id="return_surcharge" required="required"'); ?>
                                            </div>
                                        </div>

                                        <?php if (isset($cost_centers)): ?>
                                            <div class="col-md-4 form-group">
                                                <?= lang('cost_center', 'cost_center_id') ?>
                                                <?php
                                                $ccopts[''] = lang('select');
                                                if ($cost_centers) {
                                                    foreach ($cost_centers as $cost_center) {
                                                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                                    }
                                                }
                                                ?>
                                                <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                            </div>
                                        <?php endif ?>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("document", "document") ?>
                                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                                    data-show-preview="false" class="form-control file">
                                            </div>
                                        </div>

                                        <div class="col-md-4 same-height">
                                            <label> Retención </label>
                                            <input type="text" name="retencion_show" id="retencion_show" class="form-control text-right" readonly>
                                            <input type="hidden" name="retencion" id="rete">
                                        </div>

                                        <div class="col-md-4 div_add_shipping" style="display: none;">
                                            <?= lang('add_shipping', 'add_shipping') ?>
                                            <select id="add_shipping" class="form-control">
                                                <option value="0"><?= lang('no') ?></option>
                                                <option value="1"><?= lang('yes') ?></option>
                                            </select>
                                            <em class="text-danger"><?= lang('add_shipping_detail') ?></em>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <?= lang('supplier_cosecutive', 'supplier_cosecutive') ?>
                                            <input type="text" name="supplier_cosecutive" class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="control-group table-group">
                                                <label class="table-label"><?= lang("order_items"); ?></label> (<?= lang('return_tip'); ?>)

                                                <div class="controls table-controls">
                                                    <table id="reTable"
                                                        class="table items  table-bordered table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th class="col-md-4">
                                                                    <?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?>
                                                                </th>
                                                                <?php if ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10): ?>
                                                                    <th><?= lang('unit') ?></th>
                                                                    <th><?= lang('product_unit_quantity') ?></th>
                                                                    <th><?= lang('product_unit_cost') ?></th>
                                                                <?php endif ?>
                                                                <?php if ($this->Settings->product_serial): ?>
                                                                    <th><?= lang("serial_no") ?></th>
                                                                <?php endif ?>
                                                                <?php
                                                                if ($Settings->product_expiry) {
                                                                    echo '<th class="col-md-2">' . $this->lang->line("expiry_date") . '</th>';
                                                                }
                                                                ?>
                                                                <th class="col-md-1"><?= lang("gross_net_unit_cost"); ?></th>
                                                                <?php
                                                                if ($Settings->product_discount) {
                                                                    echo '<th class="col-md-1">' . $this->lang->line("discount") . '</th>';
                                                                }
                                                                ?>
                                                                <th class="col-md-1"><?= lang("net_unit_cost"); ?></th>
                                                                <?php
                                                                if ($Settings->tax1) {
                                                                    // echo '<th class="col-md-1">' . $this->lang->line("product_tax") . '</th>';
                                                                    echo '<th class="col-md-2">' . $this->lang->line("product_tax") . ' (<span class="currency">'. $default_currency->code.'</span>)  </th>';
                                                                }
                                                                if ($Settings->ipoconsumo) {
                                                                    echo '<th class="col-md-2">' . $this->lang->line("second_product_tax") . ' (<span class="currency">'. $default_currency->code.'</span>)  </th>';
                                                                }
                                                                ?>
                                                                <th><?= lang('cost_x_tax') ?></th>
                                                                <th><?= lang('purchase_shipping') ?></th>
                                                                <th class="col-md-1"><?= lang("available"); ?></th>
                                                                <th class="col-md-1"><?= lang("to_return"); ?></th>
                                                                <th>
                                                                    <?= lang("subtotal"); ?> (<span class="currency"><?= $default_currency->code ?></span>)
                                                                </th>
                                                                <th style="width: 30px !important; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                        <tfoot></tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                                <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                                    <tr class="warning">
                                                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                                        <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                                        <!-- <td>IVA <span class="totals_val pull-right" id="ivaamount">0.00</span></td> -->
                                                        <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                                                        <?php if ($this->Settings->ipoconsumo): ?>
                                                            <td><?= lang('second_product_tax') ?> <span class="totals_val pull-right" id="total_ipoconsumo">0.00</span></td>
                                                        <?php endif ?>
                                                        <?php if ($Settings->tax2) { ?>
                                                            <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                                                        <?php } ?>
                                                        <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                                                        <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="height:15px; clear: both;"></div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php
                                            if ($inv->payment_status == 'paid' || $inv->payment_status == 'partial') {
                                                echo '<div class="alert alert-success"><p>' . lang('payment_status') . ': <strong>' . $inv->payment_status . '</strong> & ' . lang('paid_amount') . ' <strong>' . $this->sma->formatMoney($inv->paid) . '</strong></p><p>'.lang('adjust_payments').'</p></div>';
                                            } else {
                                                echo '<div class="alert alert-warning"><p>' . lang('payment_status') . ': <strong>' . $inv->payment_status . '</strong> & ' . lang('paid_amount') . ' <strong>' . $this->sma->formatMoney($inv->paid) . '</strong></p><p>'.lang('adjust_payments').'</p></div>';
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <?php if (($Owner || $Admin || $GP['purchases-payments']) && $allow_payment) { ?>
                                        <div class="row" id="payments" style="display: none;">
                                            <div class="col-md-12">
                                                <div class="well well-sm well_1">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                                                        <select name="payment_reference_no" class="form-control" id="payment_reference_no" required="required"></select>
                                                                        <div class="deposit_div" style="display: none;">
                                                                            <?= lang('deposit_document_type_id', 'deposit_document_type_id') ?>
                                                                            <select name="deposit_document_type_id" id="deposit_document_type_id" class="form-control"></select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="payment">
                                                                        <div class="form-group">
                                                                            <?= lang("amount", "amount_1"); ?>
                                                                            <input type="text" class="form-control amount_to_paid" readonly>
                                                                            <input type="hidden" name="amount-paid" id="amount_1"/>
                                                                            <input type="hidden" name="amount-discounted" id="discounted"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <?= lang("paying_by", "paid_by_1"); ?>
                                                                        <select name="paid_by" id="paid_by_1" class="form-control paid_by">
                                                                            <?= $this->sma->paid_opts(null, true); ?>
                                                                        </select>
                                                                        <input type="hidden" name="mean_payment_code_fe" id="mean_payment_code_fe">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="pcc_1" style="display:none;">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <input name="pcc_no" type="text" id="pcc_no_1" class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <input name="pcc_holder" type="text" id="pcc_holder_1" class="form-control" placeholder="<?= lang('cc_holder') ?>"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <select name="pcc_type" id="pcc_type_1" class="form-control pcc_type"  placeholder="<?= lang('card_type') ?>">
                                                                                <option value="Visa"><?= lang("Visa"); ?></option>
                                                                                <option  value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                                <option value="Amex"><?= lang("Amex"); ?></option>
                                                                                <option value="Discover"><?= lang("Discover"); ?></option>
                                                                            </select>
                                                                            <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <input name="pcc_month" type="text" id="pcc_month_1"
                                                                                class="form-control" placeholder="<?= lang('month') ?>"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">

                                                                            <input name="pcc_year" type="text" id="pcc_year_1"
                                                                                class="form-control" placeholder="<?= lang('year') ?>"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">

                                                                            <input name="pcc_ccv" type="text" id="pcc_cvv2_1"
                                                                                class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="pcheque_1" style="display:none;">
                                                                <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                                    <input name="cheque_no" type="text" id="cheque_no_1" class="form-control cheque_no"/>
                                                                </div>
                                                            </div>
                                                            <div class="gc" style="display: none;">
                                                                <div class="form-group">
                                                                    <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                    <div class="input-group">
                                                                        <input name="gift_card_no" type="text" id="gift_card_no" class="pa form-control kb-pad"/>
                                                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px; height:25px;">
                                                                            <a href="#" id="sellGiftCard" class="tip" title="<?= lang('sell_gift_card') ?>"><i class="fa fa-credit-card"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="gc_details"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="row" id="bt">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= lang("return_note", "renote"); ?>
                                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="renote" style="margin-top: 10px; height: 100px;"'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="fprom-group">
                                                <?php echo form_submit('add_return', $this->lang->line("submit"), 'id="add_return" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                      class="fa fa-2x">&times;</i></button>
                                      <h4 class="modal-title" id="rtModalLabel"><?=lang('apply_order_retention');?></h4>
                                    </div>
                                    <div class="modal-body">
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>Retención</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>Base</label>
                                        </div>
                                        <div class="col-sm-1">
                                          <label>Asumida</label>
                                        </div>
                                        <div class="col-sm-3">
                                          <label>Opción</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>%</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>Valor</label>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                                          </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" name="input_rete_fuente_base" id="input_rete_fuente_base" class="form-control">
                                        </div>
                                        <div class="col-sm-1">
                                          <label>
                                            <input type="checkbox" name="rete_fuente_assumed" class="skip" id="rete_fuente_assumed">
                                          </label>
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                          <input type="hidden" name="rete_fuente_assumed_account" id="rete_fuente_assumed_account">
                                          <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                          <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_fuente_valor_show" id="rete_fuente_valor_show" class="form-control text-right" readonly>
                                          <input type="hidden" name="rete_fuente_valor" id="rete_fuente_valor">
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> Fuente 2
                                          </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" name="input_rete_otros_base" id="input_rete_otros_base" class="form-control">
                                        </div>
                                        <div class="col-sm-1">
                                          <label>
                                            <input type="checkbox" name="rete_otros_assumed" class="skip" id="rete_otros_assumed">
                                          </label>
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                          <input type="hidden" name="rete_otros_assumed_account" id="rete_otros_assumed_account">
                                          <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_otros_valor_show" id="rete_otros_valor_show" class="form-control text-right" readonly>
                                          <input type="hidden" name="rete_otros_valor" id="rete_otros_valor">
                                          <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>Retención</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>Asumida</label>
                                        </div>
                                        <div class="col-sm-3">
                                          <label>Opción</label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>%</label>
                                        </div>
                                        <div class="col-sm-3">
                                          <label>Valor</label>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                                          </label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_iva_assumed" class="skip" id="rete_iva_assumed">
                                          </label>
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                          <input type="hidden" name="rete_iva_assumed_account" id="rete_iva_assumed_account">
                                          <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="rete_iva_valor_show" id="rete_iva_valor_show" class="form-control text-right" readonly>
                                          <input type="hidden" name="rete_iva_valor" id="rete_iva_valor">
                                          <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                                          </label>
                                        </div>
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_ica_assumed" class="skip" id="rete_ica_assumed">
                                          </label>
                                        </div>
                                        <div class="col-sm-3">
                                          <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                          <input type="hidden" name="rete_ica_assumed_account" id="rete_ica_assumed_account">
                                          <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="rete_ica_valor_show" id="rete_ica_valor_show" class="form-control text-right" readonly>
                                          <input type="hidden" name="rete_ica_valor" id="rete_ica_valor">
                                          <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_bomberil" class="skip" id="rete_bomberil" disabled> <?= lang('rete_bomberil') ?>
                                          </label>
                                        </div>
                                        <div class="col-sm-5">
                                          <select class="form-control" name="rete_bomberil_option" id="rete_bomberil_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_bomberil_account" id="rete_bomberil_account">
                                          <input type="hidden" name="rete_bomberil_assumed_account" id="rete_bomberil_assumed_account">
                                          <input type="hidden" name="rete_bomberil_base" id="rete_bomberil_base">
                                          <input type="hidden" name="rete_bomberil_id" id="rete_bomberil_id">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_bomberil_tax" id="rete_bomberil_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="rete_bomberil_valor" id="rete_bomberil_valor" class="form-control text-right" readonly>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <label>
                                            <input type="checkbox" name="rete_autoaviso" class="skip" id="rete_autoaviso" disabled> <?= lang('rete_autoaviso') ?>
                                          </label>
                                        </div>
                                        <div class="col-sm-5">
                                          <select class="form-control" name="rete_autoaviso_option" id="rete_autoaviso_option" disabled='true'>
                                            <option>Seleccione...</option>
                                          </select>
                                          <input type="hidden" name="rete_autoaviso_account" id="rete_autoaviso_account">
                                          <input type="hidden" name="rete_autoaviso_assumed_account" id="rete_autoaviso_assumed_account">
                                          <input type="hidden" name="rete_autoaviso_base" id="rete_autoaviso_base">
                                          <input type="hidden" name="rete_autoaviso_id" id="rete_autoaviso_id">
                                        </div>
                                        <div class="col-sm-2">
                                          <input type="text" name="rete_autoaviso_tax" id="rete_autoaviso_tax" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="text" name="rete_autoaviso_valor" id="rete_autoaviso_valor" class="form-control text-right" readonly>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-8 text-right">
                                          <label>Total : </label>
                                        </div>
                                        <div class="col-md-4 text-right">
                                          <label id="total_rete_amount" style="display: none;"> 0.00 </label>
                                          <label id="total_rete_amount_show"> 0.00 </label>
                                        </div>
                                        <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                                      </div>


                                      <!-- <div class="form-group">
                                        <?=lang("order_tax", "order_tax_input");?>
                                        <?php
                                        $tr[""] = "";
                                        foreach ($tax_rates as $tax) {
                                          $tr[$tax->id] = $tax->name;
                                        }
                                        echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                                        ?>
                                      </div> -->
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
                                      <button type="button" id="cancelOrderRete" class="btn btn-danger" data-dismiss="modal"><?= lang('reset') ?></button>
                                      <button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var count = 1, an = 1, DT = <?= $Settings->default_tax_rate ?>, po_edit = 1,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, shipping = 0, surcharge = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>;
    var potype = 1;
    var deposit_advertisement_showed = false;
    var return_purchase = true;
    var total_payment_discount = 0;
    localStorage.removeItem('retotalreturn');
    $(document).ready(function () {
        <?php if ($inv) { ?>
            localStorage.setItem('reref', '<?= $reference ?>');
            localStorage.setItem('renote', '<?= str_replace(array("\n", "\r"), '', trim($this->sma->decode_html($inv->note))); ?>');
            localStorage.setItem('reitems', JSON.stringify(<?= $inv_items; ?>));
            localStorage.setItem('rediscount', '<?= $inv->order_discount_id ?>');
            localStorage.setItem('retax2', '<?= $inv->order_tax_id ?>');
            localStorage.setItem('return_surcharge', '0');
            localStorage.setItem('reorderdiscountmethod', '<?= $inv->order_discount_method ?>');
            localStorage.setItem('reinvoice_paid', '<?= $inv->paid ?>');
            localStorage.setItem('repayment_status', '<?= $inv->payment_status ?>');
            localStorage.setItem('repurchase_balance', '<?= ($inv->grand_total - ($inv->paid - (($inv->rete_fuente_assumed == 1 ? 0 : $inv->rete_fuente_total) + ($inv->rete_iva_assumed == 1 ? 0 : $inv->rete_iva_total) + ($inv->rete_ica_assumed == 1 ? 0 : $inv->rete_ica_total) + ($inv->rete_other_assumed == 1 ? $inv->rete_other_total : 0) + ($inv->rete_ica_assumed == 1 ? 0 : $inv->rete_bomberil_total) + ($inv->rete_ica_assumed == 1 ? 0 : $inv->rete_autoaviso_total) + $total_ce_rete_amount))) ?>');
            localStorage.removeItem('re_retenciones');
            potype = "<?= $inv->purchase_type ?>";
            <?php if ($inv->rete_fuente_id || $inv->rete_iva_id || $inv->rete_ica_id || $inv->rete_other_id) { ?>
                re_retenciones = {
                    'gtotal' : $('#gtotal').text(),
                    'id_rete_fuente' : '<?= $inv->rete_fuente_id ?>',
                    'id_rete_iva' : '<?= $inv->rete_iva_id ?>',
                    'base_rete_fuente' : '<?= $inv->rete_fuente_base ?>',
                    'base_rete_otros' : '<?= $inv->rete_other_base ?>',
                    'calculated_base_rete_fuente' : '<?= $inv->rete_fuente_base ?>',
                    'calculated_base_rete_otros' : '<?= $inv->rete_other_base ?>',
                    'id_rete_ica' : '<?= $inv->rete_ica_id ?>',
                    'id_rete_otros' : '<?= $inv->rete_other_id ?>',
                    'rete_fuente_assumed' : JSON.parse('<?= $inv->rete_fuente_assumed == 1 ? "true" : "false" ?>'),
                    'rete_iva_assumed' : JSON.parse('<?= $inv->rete_iva_assumed == 1 ? "true" : "false" ?>'),
                    'rete_ica_assumed' : JSON.parse('<?= $inv->rete_ica_assumed == 1 ? "true" : "false" ?>'),
                    'rete_otros_assumed' : JSON.parse('<?= $inv->rete_other_assumed == 1 ? "true" : "false" ?>'),
                    'id_rete_bomberil' : '<?= $inv->rete_bomberil_id ?>',
                    'id_rete_autoaviso' : '<?= $inv->rete_autoaviso_id ?>',
                };
                localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));
            <?php } ?>
        <?php } ?>
        <?php if ($Owner || $Admin) { ?>
            $("#redate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                startDate : end_date
            }).datetimepicker('update', new Date());
            $(document).on('change', '#redate', function (e) {
                localStorage.setItem('redate', $(this).val());
            });

            if (redate = localStorage.getItem('redate')) {
                // $('#redate').val(redate);
            }
        <?php } ?>
        if (reref = localStorage.getItem('reref')) {
            $('#reref').val(reref);
        }
        if (rediscount = localStorage.getItem('rediscount')) {
            $('#rediscount').val(rediscount);
        }
        if (retax2 = localStorage.getItem('retax2')) {
            $('#retax2').val(retax2);
        }
        if (return_surcharge = localStorage.getItem('return_surcharge')) {
            $('#return_surcharge').val(return_surcharge);
        }


        $(document).on('change', '.paid_by', function () {
            var p_val = $(this).val();
            $('#rpaidby').val(p_val);
            if (p_val == 'cash') {
                $('.pcheque_1').hide();
                $('.pcc_1').hide();
                $('.pcash_1').show();
            } else if (p_val == 'CC') {
                $('.pcheque_1').hide();
                $('.pcash_1').hide();
                $('.pcc_1').show();
                $('#pcc_no_1').focus();
            } else if (p_val == 'Cheque') {
                $('.pcc_1').hide();
                $('.pcash_1').hide();
                $('.pcheque_1').show();
                $('#cheque_no_1').focus();
            } else {
                $('.pcheque_1').hide();
                $('.pcc_1').hide();
                $('.pcash_1').hide();
            }
            if (p_val == 'gift_card') {
                $('.gc').show();
                $('.ngc').hide();
                $('#gift_card_no').focus();
            } else {
                $('.ngc').show();
                $('.gc').hide();
                $('#gc_details').html('');
            }
        });

        /* ------------------------------
         * Edit Row Quantity
         ------------------------------- */

        var old_row_qty;
        $(document).on("focus", '.rquantity', function () {
            old_row_qty = $(this).val();
        }).on("change", '.rquantity', function () {
            var row = $(this).closest('tr');
            var new_qty = parseFloat($(this).val()),
                item_id = row.attr('data-item-id');
            if (!is_numeric(new_qty) || (new_qty > reitems[item_id].row.oqty)) {
                $(this).val(old_row_qty);
                bootbox.alert('<?= lang('unexpected_value'); ?>');
                return false;
            }
            if(new_qty > reitems[item_id].row.oqty) {
                bootbox.alert('<?= lang('unexpected_value'); ?>');
                $(this).val(old_row_qty);
                return false;
            }
            reitems[item_id].row.base_quantity = new_qty;
            if(reitems[item_id].row.unit != reitems[item_id].row.base_unit) {
                $.each(reitems[item_id].units, function(){
                    if (this.id == reitems[item_id].row.unit) {
                        reitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                    }
                });
            }
            reitems[item_id].row.qty = new_qty;
            localStorage.setItem('reitems', JSON.stringify(reitems));
            recalcular_re_retenciones();
            loadItems($(this).prop('tabindex'));
        }).on('keydown', '.rquantity', function(e){
            if (e.keyCode == 13) {
                $(this).trigger('change');
                e.stopPropagation();
            }
        });

        $(document).on('click', '.redel', function () {
            var row = $(this).closest('tr');
            var item_id = row.attr('data-item-id');
            delete reitems[item_id];
            row.remove();
            if(reitems.hasOwnProperty(item_id)) { } else {
                localStorage.setItem('reitems', JSON.stringify(reitems));
                loadItems();
                return;
            }
        });
        var old_surcharge;
        $(document).on("focus", '#return_surcharge', function () {
            old_surcharge = $(this).val() ? $(this).val() : '0';
        }).on("change", '#return_surcharge', function () {
            var new_surcharge = $(this).val() ? $(this).val() : '0';
            if (!is_valid_discount(new_surcharge)) {
                $(this).val(new_surcharge);
                bootbox.alert('<?= lang('unexpected_value'); ?>');
                return;
            }
            localStorage.setItem('return_surcharge', JSON.stringify(new_surcharge));
            loadItems();
        });$('#rebiller').trigger('change');
        recalcular_re_retenciones();

        $(document).on('change', '#add_shipping', function(){
            if ($(this).val()) {
                localStorage.setItem('add_shipping', $(this).val());
                loadItems();
            }
        });

        if (localStorage.getItem('reitems')) {
            setTimeout(function() {
                loadItems();
            }, 850);
        }

        get_biller_documentTypes();
    });

    //aca load
    function loadItems(set_focus = null) {

        if (localStorage.getItem('reitems')) {
            if (!po_edit) {
                $('#currency').select2('readonly', true);
            }
            var locked_for_item_qty = false;
            var total = 0;
            var subtotal = 0;
            var subtotal_edit = 0;
            var totaltax = 0;
            var ipoconsumo = 0;
            var count = 1;
            var product_tax = 0;
            var product_tax_2 = 0;
            var invoice_tax = 0;
            var product_discount = 0;
            var order_discount = 0;
            var total_discount = 0;
            var item_order_discount = 0;
            var total_shipping_cost = 0;
            descuento_orden = localStorage.getItem('reorderdiscountmethod');
            var trmrate = 1;
            var posubtotal = 0;
            var edit_item = false;
            if (psbt = localStorage.getItem('resubtotal')) {
                posubtotal = psbt;
            }
            descuento_a_productos = descuento_orden;
            if (descuento_a_productos == 1) {
                if (podiscount = localStorage.getItem('rediscount')) {
                    item_order_discount = podiscount;
                }
            }
            $("#reTable tbody").empty();
            reitems = JSON.parse(localStorage.getItem('reitems'));
            an = site.settings.item_addition == 1 ? 1 : Object.keys(reitems).length;
            sortedItems = (site.settings.item_addition == 1) ? _.sortBy(reitems, function(o){return [parseInt(o.order)];}) : reitems;
            var order_no = new Date().getTime();
            $.each(sortedItems, function () {
                var supportDocument = $('#supportDocument').val();
                var item = this;
                var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
                item.order = item.order ? item.order : order_no++;
                var product_id = item.row.id,
                    item_type = item.row.type,
                    combo_items = item.combo_items,
                    item_oqty = item.row.oqty,
                    item_qty = item.row.qty,
                    item_bqty = item.row.quantity_balance,
                    item_expiry = item.row.expiry,
                    item_tax_method = item.row.tax_method,
                    item_ds = item.row.discount,
                    item_discount = 0,
                    item_option = item.row.option,
                    item_code = item.row.code,
                    shipping_unit_cost = item.row.shipping_unit_cost,
                    item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;"),
                    item_serial = item.row.serial;
                var qty_received = (item.row.received >= 0) ? item.row.received : item.row.qty;
                var item_supplier_part_no = item.row.supplier_part_no ? item.row.supplier_part_no : '';
                if (item.row.new_entry == 1) { item_bqty = item_qty; }
                var unit_cost = item.row.real_unit_cost;
                var product_unit = item.row.unit, base_quantity = item.row.base_quantity;
                var supplier = localStorage.getItem('resupplier'), belong = false;
                var item_ds_2 = 0;
                var units = [];
                var item_unit_selected =  item.row.product_unit_id_selected != null ?  item.row.product_unit_id_selected : item.row.unit,
                    item_cost = item.row.net_unit_cost,
                    unit_cost = item.row.unit_cost,
                    pr_tax = item.tax_rate,
                    pr_tax_val = item.row.item_tax,
                    pr_tax_rate = pr_tax.rate,
                    pr_tax_id = pr_tax.id,
                    pr_tax_2 = item.tax_rate_2,
                    pr_tax_2_val = formatDecimal(item.row.consumption_purchase_tax ? item.row.consumption_purchase_tax : 0),
                    pr_tax_2_rate = item.row.purchase_tax_rate_2_percentage ? item.row.purchase_tax_rate_2_percentage : pr_tax_2_val,
                    pr_tax_2_rate_id = item.row.purchase_tax_rate_2_id,
                    item_ds = item.row.discount,
                    item_discount =  item.row.item_discount;
                if (item.row.expense_tax_rate_id != item.tax_rate.id && potype == 2) {
                    $('#add_pruchase').prop('disabled', true);
                    item.row.expense_diferent_tax_rate_id = true;
                    $('.error_debug').text('El primer impuesto actual del gasto es diferente al configurado').css('display', '');
                } else {
                    $('#add_pruchase').prop('disabled', false);
                    $('.error_debug').css('display', 'none');
                }
                var unit_qty_received = qty_received;
                if(item.row.fup != 1 && product_unit != item.row.base_unit) {
                    $.each(item.units, function(){
                        if (this.id == product_unit) {
                            base_quantity = formatDecimal(unitToBaseQty(item.row.qty, this));
                            unit_qty_received = item.row.unit_received ? item.row.unit_received : formatDecimal(baseToUnitQty(qty_received, this));
                            unit_cost = formatDecimal((parseFloat(item.row.base_unit_cost)*(unitToBaseQty(1, this))));
                        }
                    });
                }
                if (othercurrency = localStorage.getItem('othercurrency')) {
                    rate = localStorage.getItem('othercurrencyrate');
                    if (trm = localStorage.getItem('othercurrencytrm')) {
                        trmrate = rate / trm;
                    }
                }
                if (shipping_unit_cost > 0) {
                    $('.div_add_shipping').fadeIn();
                    if (!localStorage.getItem('add_shipping') || localStorage.getItem('add_shipping') == 0) {
                        shipping_unit_cost = 0;
                    }
                }
                var row_no = item.id;
                var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');

                var sel_opt = '';
                $.each(item.options, function () {
                    if(this.id == item_option) {
                        sel_opt = this.name;
                    }
                });

                tr_html = '<td>'+
                            '<input name="purchase_item_id[]" type="hidden" value="'+item.row.purchase_item_id+'">'+
                            '<input name="original_tax_rate_id[]" type="hidden" value="'+item.row.original_tax_rate_id+'">'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                            '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                            '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                            '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                            '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                            '<input name="part_no[]" type="hidden" class="rpart_no" value="' + item_supplier_part_no + '">'+
                            '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name +(sel_opt != '' ? ' ('+sel_opt+')' : '')+' '+
                            ( edit_item ? '<span class="label label-default">'+item_supplier_part_no+'</span></span> <i class="pull-right fa fa-edit tip edit" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i>' : '')+
                            (item.row.op_pending_quantity !== undefined && item.row.op_pending_quantity > 0 ? ' <i class="fa-solid fa-info-circle"  data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+
                                "En stock : "+formatDecimal((parseFloat(item.row.op_pending_quantity)+parseFloat(item.row.wh_quantity)))+" | "+
                                "Pendientes orden de pedido : "+formatDecimal(item.row.op_pending_quantity)+" | "+
                                "Disponibles : "+formatDecimal(item.row.wh_quantity)+
                            '"'+'"></i>': "")+
                          '</td>';
                if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                    if (item_unit_selected > 0) {
                        unit_data_selected = item.units[item_unit_selected];
                        tr_html += '<td>'+
                                    '<input type="hidden" name="product_unit_id_selected[]" value="'+item_unit_selected+'">'+
                                    '<span>'+unit_data_selected.code+'</span>'+
                                  '</td>'; //nombre unidad

                        item_unit_quantity = item_qty;
                        item_unit_cost = item_cost;


                        if (unit_data_selected.operator == '*') {
                            item_unit_quantity = item_qty / unit_data_selected.operation_value;
                            item_unit_cost = item_cost * unit_data_selected.operation_value;
                        } else if (unit_data_selected.operator == '/'){
                            item_unit_quantity = item_qty * unit_data_selected.operation_value;
                            item_unit_cost = item_cost / unit_data_selected.operation_value;
                        } else if (unit_data_selected.operator == '+'){
                            item_unit_quantity = item_qty - unit_data_selected.operation_value;
                        } else if (unit_data_selected.operator == '-'){
                            item_unit_quantity = item_qty + unit_data_selected.operation_value;
                        }

                        tr_html += '<td>'+
                                    '<span>'+formatQuantity(item_unit_quantity)+'</span>'+
                                  '</td>'; //cantidad de la unidad
                        tr_html += '<td>'+
                                    '<span>'+formatMoney(item_unit_cost)+'</span>'+
                                  '</td>'; // costo de la unidad
                    } else {
                        tr_html += '<td>'+
                                    '<span>Sin datos</span>'+
                                  '</td>'; //cantidad de la unidad
                        tr_html += '<td>'+
                                    '<span>Sin datos</span>'+
                                  '</td>'; //cantidad de la unidad
                        tr_html += '<td>'+
                                    '<span>Sin datos</span>'+
                                  '</td>'; //cantidad de la unidad
                    }
                }

                if (site.settings.product_serial == 1) {
                    tr_html += '<td class="text-right">'+
                                    '<input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'">'+
                                '</td>';
                }
                if (site.settings.product_expiry == 1) {
                    tr_html += '<td>'+
                                  '<input class="form-control date rexpiry" name="expiry[]" type="text" value="' + item_expiry + '" data-id="' + row_no + '" data-item="' + item_id + '" id="expiry_' + row_no + '">'+
                                '</td>';
                }
                tr_html += '<td class="text-right">'+
                                '<span>' + formatMoney(parseFloat(item_cost) + parseFloat(item_discount)) + '</span>'+
                            '</td>';

                if (site.settings.product_discount == 1) {
                    tr_html += '<td class="text-right">'+
                                    '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + (item_ds != 0 ? item_ds : item_ds_2) + '">'+
                                    '<input class="form-control input-sm rdiscount" name="product_discount_val[]" type="hidden" id="discount_val_' + row_no + '" value="' + formatDecimal(0 - (item_discount * item_qty)) + '">'+
                                    '<input class="form-control input-sm rdiscount" name="product_unit_discount_val[]" type="hidden" id="unit_discount_val_' + row_no + '" value="' + formatDecimal(item_discount) + '">'+
                                    '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + formatMoney(trmrate * (0 - (item_discount * item_qty))) + '</span>'+
                               '</td>';
                }

                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rcost" name="net_cost[]" type="hidden" id="cost_' + row_no + '" value="' + item_cost + '">'+
                                '<input name="shipping_unit_cost[]" type="hidden" value="' + shipping_unit_cost + '">'+

                                '<input class="rucost" name="unit_cost[]" type="hidden" value="' + unit_cost + '">'+
                                '<input class="realucost" name="real_unit_cost[]" type="hidden" value="' + item.row.real_unit_cost + '">'+
                                '<span class="text-right scost" id="scost_' + row_no + '">' + formatMoney(trmrate * item_cost) + '</span>'+
                           '</td>';
                if (site.settings.tax1 == 1) {
                    tr_html += '<td class="text-right">'+
                                    '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + pr_tax.id + '">'+
                                    '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax' + row_no + '" value="' + pr_tax_val + '">'+
                                    '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (pr_tax_rate ? '(' + pr_tax_rate + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_val * item_qty)) + '</span>'+
                               '</td>';
                }
                if (site.settings.ipoconsumo == 2 || site.settings.ipoconsumo == 1) {
                    tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax_2" name="product_tax_2[]" type="hidden" id="product_tax_2_' + row_no + '" value="' + pr_tax_2_rate_id + '">'+
                                '<span class="text-right sproduct_tax_2" id="sproduct_tax_2_' + row_no + '">' + (pr_tax_2_rate ? '(' + pr_tax_2_rate + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_2_val * item_qty)) + '</span>'+
                                '<input class="form-control input-sm text-right runit_product_tax_2" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_2_' + row_no + '" value="' + pr_tax_2_val + '">'+
                                '<input class="form-control input-sm text-right runit_product_tax_2_percentage" name="unit_product_tax_2_percentage[]" type="hidden" id="unit_product_tax_2_percentage_' + row_no + '" value="' + pr_tax_2_rate + '">'+
                            '</td>';
                }
                tr_html += '<td class="text-right">'+
                                '<span>' + formatMoney(parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) + '</span>'+
                            '</td>';
                tr_html += '<td class="text-right">'+
                                '<span>' + formatMoney(parseFloat(shipping_unit_cost) * item_qty) + '</span>'+
                            '</td>';

                tr_html += '<td>'+
                                '<input class="form-control text-center" value="' + item_oqty + '" readonly>'+
                           '</td>';

                if (supportDocument == 1 || total_ce_rete_amount > 0) {
                    quantityReadOnly = 'readonly';
                } else {
                    quantityReadOnly = '';
                }

                tr_html += '<td>'+
                                '<input name="quantity_balance[]" type="hidden" class="rbqty" value="' + item_bqty + '">'+
                                '<input class="form-control text-center rquantity" name="quantity[]" type="number" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" value="' + item_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+ quantityReadOnly +'>'+
                                '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                                '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + base_quantity + '">'+
                           '</td>';
                tr_html += '<td class="text-right">'+
                                '<span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(trmrate * ((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val) + parseFloat(shipping_unit_cost)) * parseFloat(item_qty))) + '</span>'+
                            '</td>';
                tr_html += '<td class="text-center">'+
                                (edit_item ? '<i class="fa fa-times tip podel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i>' : '')+
                            '</td>';
                newTr.html(tr_html);
                newTr.prependTo("#reTable");
                total_shipping_cost += formatDecimal(((parseFloat(shipping_unit_cost)) * parseFloat(item_qty)));
                subtotal += formatDecimal(((parseFloat(item_cost)) * parseFloat(item_qty)));
                subtotal_edit += (edit_item ? (formatDecimal(((parseFloat(item_cost)) * parseFloat(item_qty)))) : 0);
                totaltax += formatDecimal(((parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
                ipoconsumo += formatDecimal(((parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
                product_tax += parseFloat(pr_tax_val) * parseFloat(item_qty);
                product_tax_2 += parseFloat(pr_tax_2_val) * parseFloat(item_qty);
                total += formatDecimal(((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
                if (site.settings.overselling == 0 && parseFloat(item_qty) > parseFloat(item.row.wh_quantity)) {
                    $('#row_' + row_no).addClass('danger');
                    locked_for_item_qty = true;
                }
                count += parseFloat(item_qty);
                if (site.settings.item_addition == 1) {
                    an++;
                } else {
                    an--;
                }
                if(!belong)
                    $('#row_' + row_no).addClass('warning');

            });

            var col = 1;
            if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
                col = 4;
            }
            if (site.settings.product_expiry == 1) { col++; }
            if (site.settings.product_serial == 1) { col++; }

            var tfoot = '<tr id="tfoot" class="tfoot active">';
            tfoot += '<th colspan="'+col+'">Total</th>';
            tfoot += '<th class="text-right">'+formatMoney(trmrate * (product_discount + subtotal))+'</th>'; //aca total bruto
            if (site.settings.product_discount == 1) {
                tfoot += '<th class="text-right">'+formatMoney(trmrate * product_discount)+'</th>';
            }
            tfoot += '<th class="text-right" id="subtotalP">'+formatMoney(trmrate * subtotal)+'</th>';
            if (psubtotal = localStorage.getItem('posubtotal')) {
                localStorage.removeItem('posubtotal');
            }
            if (po_edit) {
                localStorage.setItem('posubtotal', subtotal_edit+product_discount);
            } else {
                localStorage.setItem('posubtotal', subtotal+product_discount);
            }
            if (site.settings.tax1 == 1) {
                tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax)+'</th>';
            }
            if (site.settings.ipoconsumo == 2 || site.settings.ipoconsumo == 1) {
                tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax_2)+'</th>';
            }
            tfoot += '<th class="text-right">'+formatMoney(trmrate * (product_tax + product_tax_2 + subtotal))+'</th>'+
                    '<th>'+formatMoney(total_shipping_cost)+'</th>'+
                    '<th></th>'+
                    '<th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>'+
                    '<th class="text-right" id="totalP">'+formatMoney(trmrate * (total + total_shipping_cost))+'</th>'+
                    '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                '</tr>';
            localStorage.setItem('gtotal', total);
            localStorage.setItem('gtotal_apportion', total - product_tax_2);
            $('#reTable tfoot').html(tfoot);

            // Order level discount calculations
            if (descuento_a_productos == 2) {
                if (podiscount = localStorage.getItem('rediscount')) {
                    console.log('Descuento '+podiscount);
                    var ds = podiscount;
                    if (ds.indexOf("%") !== -1) {
                        var pds = ds.split("%");
                        if (!isNaN(pds[0])) {
                            order_discount = formatDecimal(((subtotal * parseFloat(pds[0])) / 100));
                        } else {
                            order_discount = formatDecimal(ds);
                        }
                    } else {
                        order_discount = formatDecimal(ds);
                    }
                }
            }


            // Order level tax calculations
            if (site.settings.tax2 != 0) {
                if (potax2 = localStorage.getItem('retax2')) {
                    $.each(tax_rates, function () {
                        if (this.id == potax2) {
                            if (this.type == 2) {
                                invoice_tax = formatDecimal(this.rate);
                            }
                            if (this.type == 1) {
                                if (descuento_orden == 1) { //Descuento afecta IVA
                                    invoice_tax = formatDecimal((((subtotal - order_discount) * this.rate) / 100));
                                } else if (descuento_orden == 2) {//Descuento NO afecta iva
                                    invoice_tax = formatDecimal((((subtotal + product_discount) * this.rate) / 100));
                                }
                            }
                        }
                    });
                }
            }
            percentage_return =  formatDecimal(((subtotal * 100)) / "<?= $inv->total ?>") / 100;

            total_discount = parseFloat(order_discount + product_discount);
            shipping = formatDecimal("<?= $inv->shipping > 0 ? $inv->shipping : 0 ?>");
            shipping = formatDecimal(shipping * percentage_return);
            total_payment_discount = 0;
            $('input[name="discount_amount[]"]').each(function(index, input_discount){
                prorrated_discount = formatDecimal(formatDecimal($(input_discount).data('originalamount')) * percentage_return);
                $(input_discount).val(prorrated_discount);
                total_payment_discount+=prorrated_discount;
            });
            if (total_shipping_cost > 0 || shipping > 0) {
                $('.div_add_shipping').fadeIn();
                if (!localStorage.getItem('add_shipping') || localStorage.getItem('add_shipping') == 0) {
                    shipping = 0;
                    total_shipping_cost = 0;
                }
            }
            // Totals calculations after item addition
            var gtotal = ((total + invoice_tax) - order_discount) + shipping;
            $('#amount_1').data('maxval', gtotal).prop('readonly', false);
            <?php if ($inv->payment_status == 'paid' || $inv->payment_status == 'partial') { ?>
                total_rete_amount = 0;
                if (re_retenciones = JSON.parse(localStorage.getItem('re_retenciones'))) {
                    total_rete_amount = formatDecimal(re_retenciones.total_rete_amount);
                }
                $('.amount_to_paid').val(formatMoney(gtotal - formatDecimal(total_rete_amount) - total_payment_discount - total_ce_rete_amount));
                $('#amount_1').val(formatDecimal(gtotal - formatDecimal(total_rete_amount) - total_payment_discount - total_ce_rete_amount));
                $('#discounted').val(formatDecimal(total_payment_discount));
                gtotal = gtotal - formatDecimal(total_rete_amount);
            <?php } ?>
            localStorage.setItem('gtotal', gtotal);
            if (!localStorage.getItem('retotalreturn')) {
                localStorage.setItem('retotalreturn', gtotal);
            }
            // gtotal = gtotal - parseFloat(total_rete_amount);
            // $('#ivaamount').text(formatMoney(totaltax));

            $('#total').text(formatMoney(trmrate * total));
            $('#shipping').val((trmrate * shipping));
            $('#tship').text(formatMoney(trmrate * shipping));
            $('#titems').text((an-1)+' ('+(formatQty(parseFloat(count) - 1))+')');
            $('#tds').text(formatMoney(trmrate * order_discount));
            if (site.settings.tax1) {
                $('#ttax1').text(formatMoney(trmrate * product_tax));
            }
            if (site.settings.tax2 != 0) {
                $('#ttax2').text(formatMoney(trmrate * invoice_tax));
            }
            $('#gtotal').text(formatMoney(trmrate * (gtotal + total_shipping_cost)));
            $('#total_ipoconsumo').text(formatMoney(trmrate * ipoconsumo));

            if (re_retenciones = JSON.parse(localStorage.getItem('re_retenciones'))) {
                re_retenciones.gtotal = $('#gtotal').text();
                if (re_retenciones.base_rete_fuente > 0) {
                    base_rete_fuente = re_retenciones.base_rete_fuente * percentage_return;
                    console.log(" >> re_retenciones.base_rete_fuente "+re_retenciones.base_rete_fuente);
                    $('#input_rete_fuente_base').val(base_rete_fuente).trigger('change');
                    re_retenciones.calculated_base_rete_fuente = base_rete_fuente;
                }
                if (re_retenciones.base_rete_otros > 0) {
                    base_rete_otros = re_retenciones.base_rete_otros * percentage_return;
                    $('#input_rete_otros_base').val(base_rete_otros).trigger('change');
                    re_retenciones.calculated_base_rete_otros = base_rete_otros;
                }
                localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));
            }
            set_page_focus(set_focus);
            verify_payment_register(gtotal-total_ce_rete_amount);

            $('[data-toggle="tooltip"]').tooltip();
            if (locked_for_item_qty) {
                $('#add_return').prop('disabled', true);
            }
        }
    }

    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_fuente_option').val('').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_iva_option').val('').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_ica_option').val('').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_otros_option').val('').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    $(document).on('click', '#rete_bomberil', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/BOMB"
            }).done(function(data){
                $('#rete_bomberil_option').html(data);
                $('#rete_bomberil_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_bomberil_option').val('').attr('disabled', true).select();
            $('#rete_bomberil_tax').val('');
            $('#rete_bomberil_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    $(document).on('click', '#rete_autoaviso', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/TABL"
            }).done(function(data){
                $('#rete_autoaviso_option').html(data);
                $('#rete_autoaviso_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_autoaviso_option').val('').attr('disabled', true).select();
            $('#rete_autoaviso_tax').val('');
            $('#rete_autoaviso_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option, #rete_iva_option, #rete_ica_option, #rete_otros_option, #rete_bomberil_option, #rete_autoaviso_option', function(){
        rete_purchase_option_changed($(this).prop('id'), getTrmRate());
    });

    function getReteAmount(apply){
        amount = 0;
        if (apply == "ST") {
            amount = $('#subtotalP').text();
        } else if (apply == "TX") {
            amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = $('#totalP').text();
        } else if (apply == "IC") {
            amount = $('#rete_ica_valor').val();
        }
        if (othercurrency = localStorage.getItem('othercurrency')) {
            othercurrencytrm = localStorage.getItem('othercurrencytrm');
            if (othercurrencytrm > 0) {
                amount = amount * othercurrencytrm;
            }
        }
        return amount;
    }    
    
    $(document).on('change', '#input_rete_fuente_base, #input_rete_otros_base', function(){
        rete_purchase_option_changed($(this).prop('id') == 'input_rete_fuente_base' ? 'rete_fuente_option' : 'rete_otros_option', getTrmRate(), true);
    });

    function setReteTotalAmount(){
        rtotal_rete =
                    ($('#rete_fuente_assumed').is(':checked') ? 0 : formatDecimal($('#rete_fuente_valor').val())) +
                    ($('#rete_iva_assumed').is(':checked') ? 0 : formatDecimal($('#rete_iva_valor').val())) +
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_ica_valor').val())) +
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_bomberil_valor').val())) +
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_autoaviso_valor').val())) +
                    ($('#rete_otros_assumed').is(':checked') ? 0 : formatDecimal($('#rete_otros_valor').val()));
        trmrate = getTrmRate();
        $('#total_rete_amount_show').text(formatMoney(trmrate * rtotal_rete));
        $('#total_rete_amount').text(formatMoney(rtotal_rete));
    }


    // JS re_retenciones

    $(document).on('click', '#updateOrderRete', function () {
        // var ts = $('#order_tax_input').val();
        // $('#sltax2').val(ts);

        $('#ajaxCall').fadeIn();

        rete_prev = JSON.parse(localStorage.getItem('re_retenciones'));
        var re_retenciones = {
            'gtotal' : (rete_prev ? rete_prev.gtotal : $('#gtotal').text()),
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            'rete_fuente_assumed' : $('#rete_fuente_assumed').is(':checked'),
            'rete_iva_assumed' : $('#rete_iva_assumed').is(':checked'),
            'rete_ica_assumed' : $('#rete_ica_assumed').is(':checked'),
            'rete_otros_assumed' : $('#rete_otros_assumed').is(':checked'),
            'bomberil_option' : $('#rete_bomberil_option option:selected').data('percentage'),
            'id_rete_bomberil' : $('#rete_bomberil_option').val(),
            'autoaviso_option' : $('#rete_autoaviso_option option:selected').data('percentage'),
            'id_rete_autoaviso' : $('#rete_autoaviso_option').val(),
            
            'base_rete_fuente' : rete_prev.base_rete_fuente,
            'base_rete_otros' : rete_prev.base_rete_otros,
            'calculated_base_rete_fuente' : $('#input_rete_fuente_base').val(),
            'calculated_base_rete_otros' : $('#input_rete_otros_base').val(),
        };
        re_retenciones.total_discounted = formatMoney(parseFloat(formatDecimal(rete_prev ? rete_prev.gtotal : $('#gtotal').text())) - parseFloat(formatDecimal($('#total_rete_amount').text())));

        localStorage.setItem('re_retenciones', JSON.stringify(re_retenciones));

        setTimeout(function() {
            $('#gtotal').text(re_retenciones.total_discounted);
            $('#ajaxCall').fadeOut();
            loadItems();
        }, 1000);

        general_total_rete = formatDecimal($('#rete_fuente_valor').val()) + formatDecimal($('#rete_iva_valor').val()) + formatDecimal($('#rete_ica_valor').val()) + formatDecimal($('#rete_otros_valor').val());

        if (general_total_rete > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(re_retenciones.total_rete_amount)))
        } else {
            $('#rete_applied').val(0);
        }
        $('#retencion_show').val(re_retenciones.total_rete_amount);
        $('#rtModal').modal('hide');
    });

    $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('re_retenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
    });

    function recalcular_re_retenciones(){

        // $('#rete_applied').val(1);
                // $('#total_rete_amount').text(re_retenciones.total_rete_amount);
                // $('#gtotal_rete_amount').val(parseFloat(formatDecimal(re_retenciones.total_rete_amount)));
                // $('#rete').text(re_retenciones.total_rete_amount);
        $('#add_return').prop('disabled', true);
        re_retenciones = JSON.parse(localStorage.getItem('re_retenciones'));
        if (re_retenciones != null) {
                if (re_retenciones.id_rete_fuente> 0) {
                    if (!$('#rete_fuente').is(':checked')) {
                        $('#rete_fuente').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_iva> 0) {
                    if (!$('#rete_iva').is(':checked')) {
                        $('#rete_iva').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_ica> 0) {
                    if (!$('#rete_ica').is(':checked')) {
                        $('#rete_ica').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_bomberil> 0) {
                    $('#rete_bomberil').prop('disabled', false);
                    if (!$('#rete_bomberil').is(':checked')) {
                        $('#rete_bomberil').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_autoaviso> 0) {
                    $('#rete_autoaviso').prop('disabled', false);
                    if (!$('#rete_autoaviso').is(':checked')) {
                        $('#rete_autoaviso').trigger('click');
                    }
                }
                if (re_retenciones.id_rete_otros> 0) {
                    if (!$('#rete_otros').is(':checked')) {
                        $('#rete_otros').trigger('click');
                    }
                }
                if (re_retenciones.rete_fuente_assumed != false) {
                    if (!$('#rete_fuente_assumed').is(':checked')) {
                        $('#rete_fuente_assumed').trigger('click');
                    }
                }
                if (re_retenciones.rete_iva_assumed != false) {
                    if (!$('#rete_iva_assumed').is(':checked')) {
                        $('#rete_iva_assumed').trigger('click');
                    }
                }
                if (re_retenciones.rete_ica_assumed != false) {
                    if (!$('#rete_ica_assumed').is(':checked')) {
                        $('#rete_ica_assumed').trigger('click');
                    }
                }
                if (re_retenciones.rete_otros_assumed != false) {
                    if (!$('#rete_otros_assumed').is(':checked')) {
                        $('#rete_otros_assumed').trigger('click');
                    }
                }

                setTimeout(function() {
                    $.each($('#rete_fuente_option option'), function(index, value){
                        if(re_retenciones.id_rete_fuente != '' && $(value).val() == re_retenciones.id_rete_fuente){
                            $('#rete_fuente_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_iva_option option'), function(index, value){
                        if(re_retenciones.id_rete_iva != '' && $(value).val() == re_retenciones.id_rete_iva){
                            $('#rete_iva_option').select2('val', $(value).val()).trigger('change');
                        }
                    });

                    $.each($('#rete_ica_option option'), function(index, value){
                        if(re_retenciones.id_rete_ica != '' && $(value).val() == re_retenciones.id_rete_ica){
                            $('#rete_ica_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_otros_option option'), function(index, value){
                        if(re_retenciones.id_rete_otros != '' && $(value).val() == re_retenciones.id_rete_otros){
                            $('#rete_otros_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_bomberil_option option'), function(index, value){
                        if(re_retenciones.id_rete_bomberil != '' && $(value).val() == re_retenciones.id_rete_bomberil){
                            $('#rete_bomberil_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $.each($('#rete_autoaviso_option option'), function(index, value){
                        if(re_retenciones.id_rete_autoaviso != '' && $(value).val() == re_retenciones.id_rete_autoaviso){
                            $('#rete_autoaviso_option').select2('val', $(value).val()).trigger('change');
                        }
                    });
                    $('#rete_otros_option').select2().trigger('change');
                    
                    if (re_retenciones.calculated_base_rete_fuente > 0) {
                        $('#input_rete_fuente_base').val(re_retenciones.calculated_base_rete_fuente).trigger('change');
                    }
                    if (re_retenciones.calculated_base_rete_otros > 0) {
                        $('#input_rete_otros_base').val(re_retenciones.calculated_base_rete_otros).trigger('change');
                    }
                    $('#updateOrderRete').trigger('click');
                    $('#add_return').prop('disabled', false);
                }, 2000);
        } else {
            $('#add_return').prop('disabled', false);
        }
    }

    $('#rtModal').on('hidden.bs.modal', function () {
        // loadItems();
    });

     $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('re_retenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
     });

    function verify_payment_register(gtotal)
    {
        paid = parseFloat(localStorage.getItem('reinvoice_paid'));
        payment_status = localStorage.getItem('repayment_status');
        purchase_balance = parseFloat(localStorage.getItem('repurchase_balance'));
        total_rete_amount = 0;
        if (rete = JSON.parse(localStorage.getItem('re_retenciones'))) {
            total_rete_amount = parseFloat(formatDecimal(rete.total_rete_amount));
        }
        if (payment_status == 'partial') {
            if (gtotal > purchase_balance ) {
                $('#payments').fadeIn();
                $('#amount_1').val(gtotal - purchase_balance);
                $('.amount_to_paid').val(formatMoney(gtotal - purchase_balance));
                if (deposit_payment_method.purchase == true && deposit_advertisement_showed == false) {
                    deposit_advertisement_showed = true;
                    setTimeout(function() {
                        bootbox.confirm({
                            message: '<p style="font-size=150%;">El valor de la devolución <b>('+formatMoney(gtotal)+')</b> supera el saldo de la factura afectada  <b>('+formatMoney(purchase_balance)+')</b>, el valor restante <b>('+formatMoney(gtotal - purchase_balance)+')</b>, ¿Cómo se recibió?',
                            buttons: {
                                confirm: {
                                    label: 'Registrar cómo anticipo',
                                    className: 'btn-success send-submit-sale btn-full'
                                },
                                cancel: {
                                    label: 'Recibido en efectivo',
                                    className: 'btn-danger btn-full'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    $('#paid_by_1').select2('val', 'deposit').trigger('change');
                                } else {
                                    $('#paid_by_1').select2('val', 'cash').trigger('change');
                                }
                            }
                        });
                    }, 800);
                }
            } else {
                $('#payments').fadeOut();
                $('#amount_1').val(0);
                $('.amount_to_paid').val(formatMoney(0));
            }
        } else if (payment_status == 'pending' || payment_status == 'due') {
            $('#amount_1').val(0);
            $('.amount_to_paid').val(formatMoney(0));
        } else if (payment_status == 'paid') {
            $('#payments').fadeIn();
            if (deposit_advertisement_showed == false) {
                deposit_advertisement_showed = true;
                setTimeout(function() {
                    bootbox.confirm({
                        message: 'La factura no tiene saldo, el valor total de la devolución <b>('+formatMoney(gtotal)+')</b>, ¿Cómo se recibió?',
                        buttons: {
                            confirm: {
                                label: 'Registrar cómo anticipo',
                                className: 'btn-success send-submit-sale btn-full'
                            },
                            cancel: {
                                label: 'Recibido en efectivo',
                                className: 'btn-danger btn-full'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $('#paid_by_1').select2('val', 'deposit').trigger('change');
                            } else {
                                $('#paid_by_1').select2('val', 'cash').trigger('change');
                            }
                        }
                    });
                }, 800);
            }
        }
        $('#discounted').val(formatDecimal(total_payment_discount));
        // invoice_paid;
    }

    $(document).on('change', '#paid_by_1', function(){
        paid_by = $(this).val();
        if (paid_by == 'deposit') {
            $('.deposit_div').fadeIn();
            $.ajax({
                url:'<?= admin_url("billers/getBillersDocumentTypes/15/") ?>'+$('#rebiller').val(),
                type:'get',
                dataType:'JSON'
            }).done(function(data){
                response = data;
                $('#deposit_document_type_id').html(response.options).select2();

                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                }
                $('#deposit_document_type_id').trigger('change');
            });
        } else {
            $('.deposit_div').fadeOut();
        }
    });
</script>

<script type="text/javascript">
    $(document).on('change', '#rebiller', function (e) {
        let supportDocument = $('#supportDocument').val();
        const document_type = (supportDocument == '1') ? '<?= DOCUMENTO_SOPORTE_AJUSTE ?>' :  6;
        is_electronic = "<?= $inv->factura_electronica ? '1' : '0' ?>";
        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/") ?>'+document_type+"/"+$('#rebiller').val()+"/NULL/"+is_electronic,
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.repurchasert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        });
    });

    function getTrmRate(){
        trmrate = 1;

        if (othercurrency = localStorage.getItem('othercurrency')) {
            othercurrencytrm = localStorage.getItem('othercurrencytrm');
            othercurrencyrate = localStorage.getItem('othercurrencyrate');
            othercurrencycode = localStorage.getItem('othercurrencycode');

            trmrate = othercurrencyrate / othercurrencytrm;
        }

        return trmrate;
    }

    function get_biller_documentTypes() {
        var biller_id = $('#rebiller').val();
        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/32/") ?>'+biller_id,
            type:'get',
            dataType:'JSON'
        }).done(function(data) {
            response = data;
            $('#payment_reference_no').html(response.options).select2();

            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }
        });
    }

    $(document).on('change', '.paid_by', function() {
        element_codigo_fe_forma_pago = $('.paid_by option:selected').data('code_fe');
        $('#mean_payment_code_fe').val(element_codigo_fe_forma_pago);
    });
</script>