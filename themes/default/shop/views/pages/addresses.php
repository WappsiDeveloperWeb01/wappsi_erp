<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-sm-9 col-md-10">

                        <div class="panel panel-default margin-top-lg">
                            <div class="panel-heading text-bold">
                                <i class="fa fa-map margin-right-sm"></i> <?= lang('my_addresses'); ?>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($this->Settings->indian_gst) { $istates = $this->gst->getIndianStates(); }
                                if (!empty($addresses)) {
                                    echo '<div class="row">';
                                    echo '<div class="col-sm-12 text-bold">'.lang('select_address_to_edit').'</div>';
                                    $r = 1;
                                    foreach ($addresses as $address) {
                                        ?>
                                        <div class="col-sm-6">
                                            <a href="#" class="link-address" data-id="<?= $address->id; ?>">
                                                    <?= $address->sucursal; ?><br>
                                                    <?= $address->direccion; ?><br>
                                                    <?= $address->city; ?>
                                                    <?= $this->Settings->indian_gst && isset($istates[$address->state]) ? $istates[$address->state].' - '.$address->state : $address->state; ?><br>
                                                    <?= $address->postal_code; ?> <?= $address->country; ?><br>
                                                    <?= lang('phone').': '.$address->phone; ?>
                                                    <span class="count"><i><?= $r; ?></i></span>
                                                    <span class="edit"><i class="fa fa-edit"></i></span>
                                                </a>
                                        </div>
                                        <?php
                                        $r++;
                                    }
                                    echo '</div>';
                                }
                                if (count($addresses) < 6) {
                                    echo '<div class="row margin-top-lg">';
                                    echo '<div class="col-sm-12"><a href="#" class="btn btn-primary btn-sm add-address">'.lang('add_address').'</a></div>';
                                    echo '</div>';
                                }
                                if ($this->Settings->indian_gst) {
                                ?>
                                <script>
                                    var istates = <?= json_encode($istates); ?>
                                </script>
                                <?php
                                } else {
                                    echo '<script>var istates = false; </script>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 col-md-2">
                        <?php include('sidebar1.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"></div>

</section>
<script type="text/javascript">
var addresses = <?= !empty($addresses) ? json_encode($addresses) : 'false'; ?>;
    $(document).on('click', '.link-address', function() {
        $('#myModal').load('<?= base_url() ?>edit_address/' + $(this).data('id'), function(){
            $('#myModal').modal('show');
        });
    });

    $(document).on('click', '.add-address', function() {
        $('#myModal').load('<?= base_url() ?>add_address', function(){
            $('#myModal').modal('show');
        });
    });
</script>
