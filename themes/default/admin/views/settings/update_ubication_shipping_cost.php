<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php
$pages = 1;
if (!$view_biller) {
    $pages = ceil($cnt_billers / 7);
}
?>
<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-3">
                            <label>Páginas : </label>
                            <?php if ($pages > 1) : ?>
                                <select id="page_view">
                                    <?php for ($i = 0; $i < $pages; $i++) { ?>
                                        <option value="<?= $i ?>" <?= $i == $page ? 'selected="selected"' : '' ?>>Página <?= $i + 1 ?></option>
                                    <?php } ?>
                                </select>
                            <?php endif ?>
                        </div>
                        <div class="col-lg-9">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?= admin_url('system_settings/ExportProductsPricesGroups') ?>">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('download_xls') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            $cnt = count($billers);
                            $wdth = 75 / $cnt;
                            ?>
                            <div>
                                <?= admin_form_open_multipart('system_settings/update_ubication_shipping_cost'); ?>
                                <table id="tableData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;"><?= lang("city"); ?></th>
                                            <th style="width: 10%;"><?= lang("zone"); ?></th>
                                            <th style="width: 10%;"><?= lang("subzone"); ?></th>
                                            <?php foreach ($billers as $pg) : ?>
                                                <th style="width: <?= $wdth ?>% !important"><?= $pg->name ?></th>
                                            <?php endforeach ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="4" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }
    $(document).ready(function() {
        $.ajax({
            url: "<?= admin_url('system_settings/getUbicationShippingCost/') . $page . '/' . $view_biller ?>"
        }).done(
            function(data) {
                $('#tableData tbody').html(data);

                $('#tableData').DataTable({
                    "processing": false,
                    pageLength: 100,
                    oLanguage: <?php echo $dt_lang; ?>,
                    "aoColumns": [
                        null,
                        null,
                        null,
                        <?php foreach ($billers as $pg) : ?>
                            null,
                        <?php endforeach ?>
                    ]
                });
            }
        );
    });

    $(document).on('blur', '.pamount', function(e) {
        biller_id = $(this).data('billerid');
        ubicationcode = $(this).data('ubicationcode');
        ubicationtype = $(this).data('ubicationtype');
        price = $(this).val();
        if (price > 0) {
            update_price(biller_id, ubicationcode, ubicationtype, price);
        }

    });

    $(document).on('keypress', '.pamount', function(e) {
        if (e.keyCode == 13) {
            biller_id = $(this).data('billerid');
            ubicationcode = $(this).data('ubicationcode');
            ubicationtype = $(this).data('ubicationtype');
            price = $(this).val();
            if (price > 0) {
                update_price(biller_id, ubicationcode, ubicationtype, price);
            }
        }
    });

    function update_price(biller_id, ubicationcode, ubicationtype, price) {
        $('.pamount').attr('readonly', true);

        $.ajax({
            url: "<?= admin_url('system_settings/ajax_update_ubication_shipping_cost') ?>/" + biller_id + "/" + ubicationcode + "/" + ubicationtype + "/" + price
        }).done(function(data) {
            if (data == 1) {
                setTimeout(function() {
                    $('.pamount').attr('readonly', false);
                }, 800);
                // command: toastr.success('Se actualizó el precio modificado', 'Precio actualizado', { onHidden : function(){  } });
            }
        });
    }

    $('#page_view').on('change', function() {
        location.href = '<?= admin_url("system_settings/update_ubication_shipping_cost/") ?>' + $(this).val();
    });
</script>