<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $v = ""; ?>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="row">
                    	<div class="col-lg-12">
							<?php echo form_open('admin/reports/debts_to_pay_report' , 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
                            <input type="hidden" name="action" id="action">
								<div class="row">
									<div class="col-xs-12 col-md-4">
										<div class="form-group">
											<label for="inputEmail3" class="control-label"> Proveedor</label>
											<br>
											<label class="radio-inline">
												<input type="radio" name="valueProveedor" id="provider1" value="Todas" <?= !isset($_POST['valueProveedor']) || (isset($_POST['valueProveedor']) && $_POST['valueProveedor'] == 'Todas') ? 'checked' : '' ?> > Todas
											</label>
											<label class="radio-inline">
												<input type="radio" name="valueProveedor" id="provider2" value="Una"  <?= isset($_POST['valueProveedor']) && $_POST['valueProveedor'] == 'Una' ? 'checked' : '' ?> > Una
											</label>
											<div class="collapse" id="collapseProveedor">
												<select class="form-control " name="Proveedor" id="Proveedor" style="width:100%;" >
													<option value="all" >-- Seleccione Proveedor --</option>
													<?php foreach ($providers as $provider): ?>
														<option value="<?php echo $provider['cod_id']; ?>" <?php echo ((!empty($datos)) AND ($datos['provider']==$provider['cod_id'])) ? 'selected' : ''; ?>><?php echo $provider['nombre']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-4">
										<label for="Sucursal" class="control-label"> <?= lang('branch') ?></label>
										<?php if ($this->Settings->big_data_limit_reports == 1): ?>
											<select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;" required>
												<option value="" > <?= lang('select').' '.lang('branch') ?> </option>
												<?php foreach ($billers as $biller): ?>
													<option value="<?php echo $biller->id; ?>"><?php echo $biller->name; ?></option>
												<?php endforeach; ?>
											</select>
										<?php else: ?>
											<div class="form-group">
												<br>
												<?php if ((!$this->session->userdata('biller_id'))): ?>
													<label class="radio-inline">
														<input type="radio" name="valueSucursal" id="biller1" value="Todas" <?= !isset($_POST['valueSucursal']) || (isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
													</label>
												<?php endif ?>
													
												<label class="radio-inline">
													<input type="radio" name="valueSucursal" id="biller2" value="Una"  <?= ((isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Una') || ($this->session->userdata('biller_id'))) ? 'checked' : '' ?>> <?= lang('aa') ?>
												</label>
											</div>
											
											<?php
			                                $bl[""] = "";
			                                $bldata = [];
			                                foreach ($billers as $biller) {
			                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
			                                    $bldata[$biller->id] = $biller;
			                                }
			                                // exit(var_dump($this->session->userdata('biller_id')));
			                                ?>

			                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
												<div class="collapse" id="collapseBiller">
													<select class="form-control " name="Sucursal" id="Sucursal" style="width:100%;">
														<option value="all" > <?= lang('select').' '.lang('branch') ?> </option>
														<?php foreach ($billers as $biller): ?>
															<option value="<?php echo $biller->id; ?>" <?= (isset($_POST['Sucursal']) && $_POST['Sucursal'] == $biller->id ? "selected" : "") ?> ><?php echo $biller->name; ?></option>
														<?php endforeach; ?>
													</select>
												</div>    
			                                <?php } else { ?>
													<select class="form-control " name="Sucursal" id="Sucursal" style="width:100%;">
			                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
			                                                    $biller = $bldata[$this->session->userdata('biller_id')];
			                                                ?>
			                                                    <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
			                                                <?php endif ?>
			                                            </select>
			                                <?php } ?>
											
											
										<?php endif ?>
									</div>
			                        <div class="col-xs-12 col-md-4 text-right">
			                            <div class="pull-right dropdown">
			                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
			                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
			                                    <li>
			                                        <a class="ExportPdf" id="ExportPdf"> PDF </a>
			                                    </li>
			                                    <li>
			                                        <a class="ExportXls" id="ExportXls"> Excel </a>
			                                    </li>
			                                </ul>
			                            </div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-4">
										<?= lang('select_due_days_date', 'select_due_days_date') ?>
										<select name="select_due_days_date" id="select_due_days_date" class="form-control">
											<option value="2" <?= $select_due_days_date == 2 ? "selected" : "" ?>><?= lang('due_date') ?></option>
											<option value="1" <?= $select_due_days_date == 1 ? "selected" : "" ?>><?= lang('invoice_date') ?></option>
										</select>
									</div>
                                    <div class="form-group col-md-4">
                                      <label><?= lang('cut_off_date') ?></label>
                                      <input type="date" name="end_date" id="end_date" class="form-control" value="<?= isset($_POST['end_date']) && !empty($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d') ?>" >
                                    </div>
								</div>
								<div class="row">
									<div class="form-group col-xs-12 col-md-9">
										<label>
											<input type="checkbox" name="view_summary" <?= isset($_POST['view_summary']) ? 'checked="checked"' : '' ?> >
											<?= lang('view_summary') ?>
										</label>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-3">
										<button type="submit" class="btn btn-success" class="Buscar" id="Buscar" ><?php echo lang('submit');?></button>
									</div>
								</div>
							</form>
						</div>
						<br>

						<?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
							<div class="col-lg-12" >
								<br>
								<div class="table-responsive">
									<table id="TableData" class="table table-stripped" >
										<thead>
											<tr>
												<td class="text-center"> <b><?= lang('ref') ?></b></td>
												<td class="text-center"> <b><?= lang('date').' '.lang('purchase') ?></b></td>
												<td class="text-center"> <b><?= lang('days_30') ?></b></td>
												<td class="text-center"> <b><?= lang('days_60') ?></b></td>
												<td class="text-center"> <b><?= lang('days_90') ?></b></td>
												<td class="text-center"> <b><?= lang('days_120') ?></b></td>
												<td class="text-center"> <b><?= lang('days_more') ?></b></td>
												<td class="text-center"> <b><?= lang('due_date') ?></b></td>
												<td class="text-center"> <b><?= lang('day_arrears') ?></b></td>
												<td class="text-center"> <b><?= lang('value') ?></b></td>
												<td class="text-center"> <b><?= lang('sbalance').' '.lang('paid') ?></b></td>
												<td class="text-center"> <b><?= lang('sbalance').' '.lang('pending') ?></b></td>
											</tr>
										</thead>
										<tbody>
											<?php
												$supplier =  '' ;
												$gran_total_venta = 0;
												$gran_total_pago = 0;
												$gran_total_saldo = 0;
												$total = $val_pago = $Saldo = 0;
												$total_total = $total_val_pago = $total_Saldo = 0;
												$i = 0;
											?>
											<?php foreach ($purchases as $purchase): ?>
												<?php

												$fecha = $select_due_days_date == 1 ? substr($purchase['Fecha'],0,10) : substr($purchase['due_date'],0,10);
											    $fecha_now = date('Y-m-d');
											    $dias	= (strtotime($fecha)-strtotime($fecha_now))/86400;
											    if ($dias < 0) {
											        $dias   = abs($dias);
											        $dias   = floor($dias);
											    } else {
											        $dias  = 0;
											    }


											    $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 = $saldo_mas = 0;
											    $total_30 = $total_60 = $total_90 = $total_120 = $total_mas = 0;

											    $Saldo = number_format($purchase['Saldo'],0,'','');

											    if ($dias <=30)
											    {
											        $saldo_30 = $Saldo;
											        $total_30 += $purchase['Saldo'];
											    }
											    else if ($dias <=60)
											    {
											        $saldo_60 = $Saldo;
											        $total_60 += $purchase['Saldo'];
											    }
											    else if ($dias <=90)
											    {
											        $saldo_90 = $Saldo;
											        $total_90 += $purchase['Saldo'];
											    }
											    elseif ($dias <=120)
											    {
											        $saldo_120 = $Saldo;
											        $total_120 += $purchase['Saldo'];
											    }
											    else
											    {
											        $saldo_mas = $Saldo;
											        $total_mas += $purchase['Saldo'];
											    }
												 ?>
												<?php $i++; ?>
												<?php if ($purchase['cod_companie'] != $supplier): ?>
													<?php if ($i > 1): ?>
														<tr >
															<td ></td>
															<td ></td>
															<td ></td>
															<td ></td>
															<td ></td>
															<td ></td>
															<td ></td>
															<td ></td>
															<th class="text-right" >SubTotal Proveedor </th>
															<th class="text-right">$ <?php echo number_format($total_total,0,'','.');?></th>
															<th class="text-right">$ <?php echo number_format($total_val_pago,0,'','.');?></th>
															<th class="text-right">$ <?php echo number_format($total_Saldo,0,'','.');?></th>
														</tr>
														<?php
															$total_total = $total_val_pago = $total_Saldo = 0;
														?>
													<?php endif; ?>
													<tr class="active">
														<td ><h5><b>Proveedor: <?php echo $purchase['supplier']; ?></b></h5></td>
														<td ></td>
														<td ></td>
														<td ></td>
														<td ></td>
														<td ></td>
														<td ></td>
														<td ></td>
														<td ></td>
														<td ></td>
														<td ></td>
														<td ></td>
													</tr>
													<?php $supplier = $purchase['cod_companie'];?>
												<?php endif; ?>
												<?php if (!isset($_POST['view_summary'])): ?>
													<tr >
														<td scope="row"><?php echo $purchase['Referencia']; ?></td>
														<td class="text-center"><?php echo substr($purchase['Fecha'],0,10); ?></td>
														<td class="text-right"><?= $this->sma->formatMoney($saldo_30) ?></td>
														<td class="text-right"><?= $this->sma->formatMoney($saldo_60) ?></td>
														<td class="text-right"><?= $this->sma->formatMoney($saldo_90) ?></td>
														<td class="text-right"><?= $this->sma->formatMoney($saldo_120) ?></td>
														<td class="text-right"><?= $this->sma->formatMoney($saldo_mas) ?></td>
														<td><?php echo substr($purchase['due_date'],0,10); ?></td>
														<td class="text-right"><?= $dias ?></td>
														<td class="text-right">$ <?php echo number_format($purchase['total'],0,'','.'); ?></td>
														<td class="text-right">$ <?php echo number_format($purchase['val_pago'],0,'','.'); ?></td>
														<td class="text-right">$ <?php echo number_format($purchase['Saldo'],0,'','.'); ?></td>
													</tr>
												<?php endif ?>
												<?php
													$total_total += $purchase['total'];
													$total_val_pago += $purchase['val_pago'];
													$total_Saldo += $purchase['Saldo'];

													$gran_total_venta += $purchase['total'];
													$gran_total_pago += $purchase['val_pago'];
													$gran_total_saldo += $purchase['Saldo'];

												?>
												<?php endforeach; ?>
												<tr >
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<th class="text-right" >SubTotal Proveedor </th>
													<th class="text-right">$ <?php echo number_format($total_total,0,'','.');?></th>
													<th class="text-right">$ <?php echo number_format($total_val_pago,0,'','.');?></th>
													<th class="text-right">$ <?php echo number_format($total_Saldo,0,'','.');?></th>
												</tr>
												<tr >
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<td ></td>
													<th class="text-right" >Total informe </th>
													<th class="text-right">$ <?php echo number_format($gran_total_venta,0,'','.');?></th>
													<th class="text-right">$ <?php echo number_format($gran_total_pago,0,'','.');?></th>
													<th class="text-right">$ <?php echo number_format($gran_total_saldo,0,'','.');?></th>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
						<?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script >


$('#collapseProveedor').collapse('hide');

	$(document).ready(function() {

		if ("<?= isset($_POST['valueProveedor']) && $_POST['valueProveedor'] != 'Todas' ? true : false ?>") {
			$('#collapseProveedor').collapse('show');
		}

		if ("<?= isset($_POST['valueBiller']) && $_POST['valueBiller'] != 'Todas' ? true : false ?>") {
			$('#collapseBiller').collapse('show');
		}

		// $("#provider1").prop("checked", true);
		$('#TableData').DataTable({
            "aaSorting": [],
            "aoColumns": [
	            {
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            },{
	                "bSortable": false
	            }],
			// dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
    		buttons : [{extend:'excel', title:'Menus', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4,5,6]}}],
			oLanguage: <?php echo $dt_lang; ?>
		});
		$("#ExportPdf").click(function() {
			$('#action').val('pdf');
			form_action();
		});
		$("#ExportXls").click(function() {
			$('#action').val('xls');
			form_action();
		});
	} );

function form_action(){
	$('#form_filter').prop('target', '_blank').submit();
	$('#form_filter').prop('target', false);
	$('#action').val('');
}

$('#provider1').on('ifChecked', function(event){
	$("#Proveedor").val('all');
	$('#collapseProveedor').collapse('hide');
});
$('#provider2').on('ifChecked', function(event){
	var Proveedor = $("#Proveedor option:selected").val();
	$('#collapseProveedor').collapse('show');
});

$('#biller1').on('ifChecked', function(event){
	$("#Biller").val('all');
	$('#collapseBiller').collapse('hide');
});
$('#biller2').on('ifChecked', function(event){
	var Biller = $("#Biller option:selected").val();
	$('#collapseBiller').collapse('show');
});
$('#Buscar').on('click', function(e){
    $('#filter_action').val(1);
    $('#form_filter').submit();
});

</script>
