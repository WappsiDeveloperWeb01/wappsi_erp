<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CompaniesModel extends CI_Model
{
    public $tableName = 'companies';

    public function __construct() {
        parent::__construct();
    }

    public function find($filters) {
        $this->db->where($filters);
        return $this->db->get($this->tableName)->row();
    }

    public function create($data) {
        if ($this->db->insert($this->tableName, $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function billerData($filters) {
        $this->db->select("bd.*");
        $this->db->join("biller_data bd", "bd.biller_id = {$this->tableName}.id", "inner");
        $this->db->where($filters);
        return $this->db->get($this->tableName)->row();
    }

    // public function countCompanies($filters = []) {
    //     if ($filters['group']) {
    //         $this->db->where('group_name', $filters['group']);
    //     }
    //     $this->db->from('companies');
    //     return $this->db->count_all_results();
    // }

    // public function getCompanies($filters = []) {
    //     if ($filters['group']) {
    //         $this->db->where('group_name', $filters['group']);
    //     }

    //     if ($filters['name']) {
    //         $this->db->where('name', $filters['name']);
    //     } else {
    //         $this->db->order_by($filters['order_by'][0], $filters['order_by'][1] ? $filters['order_by'][1] : 'asc');
    //         $this->db->limit($filters['limit'], ($filters['start']-1));
    //     }

    //     return $this->db->get("companies")->result();
    // }

    // public function getCompany($filters) {
    //     if (!empty($companies = $this->getCompanies($filters))) {
    //         return array_values($companies)[0];
    //     }
    //     return FALSE;
    // }

    // public function getCompanyUsers($company_id) {
    //     return $this->db->get_where('users', ['company_id' => $company_id])->result();
    // }

}
