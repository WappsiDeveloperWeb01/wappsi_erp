<?php
/** Error reporting */
require_once APPPATH.'../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';


function print_totals_address($excel){
	$excel->getActiveSheet()->mergeCells('A'.$excel->j.':C'.$excel->j.''); 
	$excel->setActiveSheetIndex(0)
						->setCellValue('A'.$excel->j, "Subtotal Sucursal Cliente ".$excel->address)
						->setCellValue('D'.$excel->j, number_format($excel->total_30_address,0,'',''))
						->setCellValue('E'.$excel->j, number_format($excel->total_60_address,0,'',''))
						->setCellValue('F'.$excel->j, number_format($excel->total_90_address,0,'',''))
						->setCellValue('G'.$excel->j, number_format($excel->total_120_address,0,'',''))
						->setCellValue('H'.$excel->j, number_format($excel->total_mas_address,0,'',''))
						->setCellValue('I'.$excel->j, number_format($excel->total_address,0,'','')) ;
	$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
    $excel->total_30_address = $excel->total_60_address = $excel->total_90_address = $excel->total_120_address = $excel->total_mas_address = $excel->total_address = 0;
    $excel->j++;
}
function print_totals_customer($excel){
	$excel->getActiveSheet()->mergeCells('A'.$excel->j.':C'.$excel->j.''); 
	$excel->setActiveSheetIndex(0)
						->setCellValue('A'.$excel->j, "Subtotal Cliente ".$excel->customer)
						->setCellValue('D'.$excel->j, number_format($excel->total_30_customer,0,'',''))
						->setCellValue('E'.$excel->j, number_format($excel->total_60_customer,0,'',''))
						->setCellValue('F'.$excel->j, number_format($excel->total_90_customer,0,'',''))
						->setCellValue('G'.$excel->j, number_format($excel->total_120_customer,0,'',''))
						->setCellValue('H'.$excel->j, number_format($excel->total_mas_customer,0,'',''))
						->setCellValue('I'.$excel->j, number_format($excel->total_customer,0,'','')) ;
	$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
    $excel->total_30_customer = $excel->total_60_customer = $excel->total_90_customer = $excel->total_120_customer = $excel->total_mas_customer = $excel->total_customer = 0;
    $excel->j++;
}
function print_totals_city($excel){
	$excel->getActiveSheet()->mergeCells('A'.$excel->j.':C'.$excel->j.''); 
	$excel->setActiveSheetIndex(0)
						->setCellValue('A'.$excel->j, "Subtotal Ciudad ".$excel->city)
						->setCellValue('D'.$excel->j, number_format($excel->total_30_city,0,'',''))
						->setCellValue('E'.$excel->j, number_format($excel->total_60_city,0,'',''))
						->setCellValue('F'.$excel->j, number_format($excel->total_90_city,0,'',''))
						->setCellValue('G'.$excel->j, number_format($excel->total_120_city,0,'',''))
						->setCellValue('H'.$excel->j, number_format($excel->total_mas_city,0,'',''))
						->setCellValue('I'.$excel->j, number_format($excel->total_city,0,'','')) ;
	$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
    $excel->total_30_city = $excel->total_60_city = $excel->total_90_city = $excel->total_120_city = $excel->total_mas_city = $excel->total_city = 0;
    $excel->j++;
}
function print_totals_state($excel){
	$excel->getActiveSheet()->mergeCells('A'.$excel->j.':C'.$excel->j.''); 
	$excel->setActiveSheetIndex(0)
						->setCellValue('A'.$excel->j, "Subtotal Departamento ".$excel->cstate)
						->setCellValue('D'.$excel->j, number_format($excel->total_30_state,0,'',''))
						->setCellValue('E'.$excel->j, number_format($excel->total_60_state,0,'',''))
						->setCellValue('F'.$excel->j, number_format($excel->total_90_state,0,'',''))
						->setCellValue('G'.$excel->j, number_format($excel->total_120_state,0,'',''))
						->setCellValue('H'.$excel->j, number_format($excel->total_mas_state,0,'',''))
						->setCellValue('I'.$excel->j, number_format($excel->total_state,0,'','')) ;
	$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
    $excel->total_30_state = $excel->total_60_state = $excel->total_90_state = $excel->total_120_state = $excel->total_mas_state = $excel->total_state = 0;
    $excel->j++;
}
function print_totals_seller($excel){
	$excel->getActiveSheet()->mergeCells('A'.$excel->j.':C'.$excel->j.''); 
	$excel->setActiveSheetIndex(0)
						->setCellValue('A'.$excel->j, "Subtotal Vendedor ".$excel->seller)
						->setCellValue('D'.$excel->j, number_format($excel->total_30_seller,0,'',''))
						->setCellValue('E'.$excel->j, number_format($excel->total_60_seller,0,'',''))
						->setCellValue('F'.$excel->j, number_format($excel->total_90_seller,0,'',''))
						->setCellValue('G'.$excel->j, number_format($excel->total_120_seller,0,'',''))
						->setCellValue('H'.$excel->j, number_format($excel->total_mas_seller,0,'',''))
						->setCellValue('I'.$excel->j, number_format($excel->total_seller,0,'','')) ;
	$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
    $excel->total_30_seller = $excel->total_60_seller = $excel->total_90_seller = $excel->total_120_seller = $excel->total_mas_seller = $excel->total_seller = 0;
    $excel->j++;
}
function print_totals_biller($excel){
	$excel->getActiveSheet()->mergeCells('A'.$excel->j.':C'.$excel->j.''); 
	$excel->setActiveSheetIndex(0)
						->setCellValue('A'.$excel->j, "Subtotal Sucursal ".$excel->biller)
						->setCellValue('D'.$excel->j, number_format($excel->total_30_biller,0,'',''))
						->setCellValue('E'.$excel->j, number_format($excel->total_60_biller,0,'',''))
						->setCellValue('F'.$excel->j, number_format($excel->total_90_biller,0,'',''))
						->setCellValue('G'.$excel->j, number_format($excel->total_120_biller,0,'',''))
						->setCellValue('H'.$excel->j, number_format($excel->total_mas_biller,0,'',''))
						->setCellValue('I'.$excel->j, number_format($excel->total_biller,0,'','')) ;
	$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
    $excel->total_30_biller = $excel->total_60_biller = $excel->total_90_biller = $excel->total_120_biller = $excel->total_mas_biller = $excel->total_biller = 0;
    $excel->j++;
}
// Create new PHPExcel object
$excel = new PHPExcel();
// Set document properties
$excel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
$seller_old = 'old' ; 
$saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 = $saldo_mas = 0;
$excel->total_30 = $excel->total_60 = $excel->total_90 = $excel->total_120 = $excel->total_mas = $total = 0;
$excel->total_30_biller = $excel->total_60_biller = $excel->total_90_biller = $excel->total_120_biller = $excel->total_mas_biller = $excel->total_biller = 0;
$excel->total_30_seller = $excel->total_60_seller = $excel->total_90_seller = $excel->total_120_seller = $excel->total_mas_seller = $excel->total_seller = 0;
$excel->total_30_state = $excel->total_60_state = $excel->total_90_state = $excel->total_120_state = $excel->total_mas_state = $excel->total_state = 0;
$excel->total_30_city = $excel->total_60_city = $excel->total_90_city = $excel->total_120_city = $excel->total_mas_city = $excel->total_city = 0;
$excel->total_30_address = $excel->total_60_address = $excel->total_90_address = $excel->total_120_address = $excel->total_mas_address = $excel->total_address = 0;
$excel->total_30_customer = $excel->total_60_customer = $excel->total_90_customer = $excel->total_120_customer = $excel->total_mas_customer = $excel->total_customer = 0;
$excel->biller = $excel->seller = $excel->customer = $excel->cstate = $excel->city = $excel->address = null;
$i = $excel->j = 0;
$excel->j++;
$excel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(30);
$excel->setActiveSheetIndex(0)->getStyle("A1:M1")->getFont()->setBold(true);
$excel->setActiveSheetIndex(0)
			->setCellValue('A'.$excel->j, 'DOCUMENTO');
$letter = 'B';
// if (!$group_data) {
// 	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'CLIENTE');$letter++;
// 	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'CIUDAD');$letter++;
// 	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'VENDEDOR');$letter++;
// }
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'FECHA');$letter++;
if (!$group_data) {
    $excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'CLIENTE');$letter++;
}
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'DIAS EN MORA');$letter++;
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'SALDO 0 - 30');$letter++;
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'SALDO 31 - 60');$letter++;
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'SALDO 61 - 90');$letter++;
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'SALDO 91 - 120');$letter++;
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'SALDO 121 o mas');$letter++;
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'SALDO TOTAL') ;$letter++;
$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, 'FECHA VENCIMIENTO') ;$letter++;
$excel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(16);

$excel->setActiveSheetIndex(0)
			->getDefaultColumnDimension()
			->setWidth(16);
$excel->j++;
foreach ($sales as $sale) 
{
	if ($group_data && $group_by_biller && (!$excel->biller || ($excel->biller && $excel->biller != $sale['Sucursal']))) {  
        if ($excel->total_address > 0 && $group_by_customer_address){
            print_totals_address($excel);
        }
        if ($excel->total_customer > 0 && $group_by_customer){
            print_totals_customer($excel);
        }
        if ($excel->total_city > 0 && $group_by_city){
            print_totals_city($excel);
        }
        if ($excel->total_state > 0 && $group_by_city){ 
            print_totals_state($excel);
        }  
        if ($excel->total_seller > 0 && $group_by_seller){
            print_totals_seller($excel);
        }
        if ($excel->total_biller > 0){ 
            print_totals_biller($excel);
        }
        $excel->biller = $sale['Sucursal']; 
        $excel->seller = $excel->customer = $excel->cstate = $excel->city = $excel->address = null;
        $excel->biller_name = $excel->biller;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$excel->j, "Sucursal : ".$excel->biller);
		$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
        $excel->j++;
    }
    if ($group_data && ($group_by_seller && (!$excel->seller || ($excel->seller && $excel->seller != $sale['nom_seller'])))) { 
        if ($excel->total_address > 0 && $group_by_customer_address){
            print_totals_address($excel);
        }
        if ($excel->total_customer > 0 && $group_by_customer){
            print_totals_customer($excel);
        }
        if ($excel->total_city > 0 && $group_by_city){
            print_totals_city($excel);
        }
        if ($excel->total_state > 0 && $group_by_city){ 
            print_totals_state($excel);
        }  
        if ($excel->total_seller > 0 && $group_by_seller){
            print_totals_seller($excel);
        }
        $excel->seller = $sale['nom_seller']; 
        $excel->customer = $excel->cstate = $excel->city = $excel->address = null;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$excel->j, "Vendedor : ".$excel->seller);
		$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
        $excel->j++;
    }
    if ($group_data && ($group_by_city && (!$excel->cstate || ($excel->cstate && $excel->cstate != $sale['customer_state'])))) { 
        if ($excel->total_address > 0 && $group_by_customer_address){
            print_totals_address($excel);
        }
        if ($excel->total_customer > 0 && $group_by_customer){
            print_totals_customer($excel);
        }
        if ($excel->total_city > 0 && $group_by_city){
            print_totals_city($excel);
        }
        if ($excel->total_state > 0 && $group_by_city){ 
            print_totals_state($excel);
        }  
        $excel->cstate = $sale['customer_state']; 
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$excel->j, "Departamento : ".$excel->cstate);
		$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
        $excel->j++;
    }
    if ($group_data && ($group_by_city && (!$excel->city || ($excel->city && $excel->city != $sale['customer_city'])))) { 
        if ($excel->total_address > 0 && $group_by_customer_address){
            print_totals_address($excel);
        }
        if ($excel->total_customer > 0 && $group_by_customer){
            print_totals_customer($excel);
        }
        if ($excel->total_city > 0 && $group_by_city){
            print_totals_city($excel);
        }
        $excel->city = $sale['customer_city'];
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$excel->j, "Ciudad : ".$excel->city);
		$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
        $excel->j++;
    }
    if ($group_data && ($group_by_customer && (!$excel->customer || ($excel->customer && $excel->customer != $sale['nom_companie'])))) { 
        if ($excel->total_address > 0 && $group_by_customer_address){
            print_totals_address($excel);
        }
        if ($excel->total_customer > 0 && $group_by_customer){
            print_totals_customer($excel);
        }
        $excel->customer = $sale['nom_companie']; 
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$excel->j, "Cliente : ".$excel->customer);
		$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
        $excel->j++;
    }
    if ($group_data && ($group_by_customer_address && (!$excel->address || ($excel->address && $excel->address != $sale['customer_sucursal'])))) { 
        if ($excel->total_address > 0 && $group_by_customer_address){
            print_totals_address($excel);
        }
        $excel->address = $sale['customer_sucursal']; 
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$excel->j, "Sucursal Cliente : ".$excel->address);
		$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
        $excel->j++;
    }

    $fecha = substr(!empty($sale['due_date']) ? $sale['due_date'] : $sale['Fecha_Creacion'],0,10);
    $fecha_now = isset($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d');
    $dias   = (strtotime($fecha_now)-strtotime($fecha))/86400;
    $dias 	= $dias < 0 ? 0 : $dias; 
    $dias 	= floor($dias);
    $Saldo = number_format($sale['Saldo'],0,'','');
    $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 = $saldo_mas = 0;
    if ($dias <=30) 
    {
        $saldo_30 = $Saldo;
        $excel->total_30 += $sale['Saldo'];
    }
    else if ($dias <=60)
    {
        $saldo_60 = $Saldo;
        $excel->total_60 += $sale['Saldo'];
    }
    else if ($dias <=90)
    {
        $saldo_90 = $Saldo;
        $excel->total_90 += $sale['Saldo'];
    }
    elseif ($dias <=120) 
    { 
        $saldo_120 = $Saldo;
        $excel->total_120 += $sale['Saldo'];
    }
    else
    {
        $saldo_mas = $Saldo;
        $excel->total_mas += $sale['Saldo'];
    }
	$total += number_format($sale['Saldo'],0,'','');
	$excel->setActiveSheetIndex(0)
				->setCellValue('A'.$excel->j, $sale['Referencia']);
	$letter = 'B';
	$right_start = $letter;
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $sale['Fecha_Creacion']);$letter++;
    if (!$group_data) {
        $excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $sale['nom_companie']);$letter++;
    }
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, floor($dias));$letter++;
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $saldo_30);$letter++;
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $saldo_60);$letter++;
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $saldo_90);$letter++;
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $saldo_120);$letter++;
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $saldo_mas);$letter++;
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $sale['Saldo']);$letter++;
	$excel->setActiveSheetIndex(0)->setCellValue($letter.$excel->j, $sale['due_date']);$letter++;
	$right_end = $letter;
	$excel->setActiveSheetIndex(0)->getStyle($right_start.$excel->j.':'.$right_end.$excel->j)->getAlignment()->setHorizontal(
		PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
	);
    $excel->j++;
	// $total += $sale['Saldo'];
	$excel->total_30_address += $saldo_30;
	$excel->total_30_customer += $saldo_30;
	$excel->total_30_city += $saldo_30;
	$excel->total_30_state += $saldo_30;
	$excel->total_30_seller += $saldo_30;
	$excel->total_30_biller += $saldo_30;
	$excel->total_60_address += $saldo_60;
	$excel->total_60_customer += $saldo_60;
	$excel->total_60_city += $saldo_60;
	$excel->total_60_state += $saldo_60;
	$excel->total_60_seller += $saldo_60;
	$excel->total_60_biller += $saldo_60;
	$excel->total_90_address += $saldo_90;
	$excel->total_90_customer += $saldo_90;
	$excel->total_90_city += $saldo_90;
	$excel->total_90_state += $saldo_90;
	$excel->total_90_seller += $saldo_90;
	$excel->total_90_biller += $saldo_90;
	$excel->total_120_address += $saldo_120;
	$excel->total_120_customer += $saldo_120;
	$excel->total_120_city += $saldo_120;
	$excel->total_120_state += $saldo_120;
	$excel->total_120_seller += $saldo_120;
	$excel->total_120_biller += $saldo_120;
	$excel->total_mas_address += $saldo_mas;
	$excel->total_mas_customer += $saldo_mas;
	$excel->total_mas_city += $saldo_mas;
	$excel->total_mas_state += $saldo_mas;
	$excel->total_mas_seller += $saldo_mas;
	$excel->total_mas_biller += $saldo_mas;
	$excel->total_address += $sale['Saldo'];
	$excel->total_customer += $sale['Saldo'];
	$excel->total_city += $sale['Saldo'];
	$excel->total_state += $sale['Saldo'];
	$excel->total_seller += $sale['Saldo'];
	$excel->total_biller += $sale['Saldo'];
}
$excel->setActiveSheetIndex(0)->getStyle('A'.$excel->j.':C'.$excel->j)->getFont()->setBold(true);
$excel->setActiveSheetIndex(0)->getStyle('A'.$excel->j.':I'.$excel->j)->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
if ($excel->total_address > 0 && $group_by_customer_address){
    print_totals_address($excel);
}
if ($excel->total_customer > 0 && $group_by_customer){
    print_totals_customer($excel);
}
if ($excel->total_city > 0 && $group_by_city){
    print_totals_city($excel);
}
if ($excel->total_state > 0 && $group_by_city){ 
    print_totals_state($excel);
}  
if ($excel->total_seller > 0 && $group_by_seller){
    print_totals_seller($excel);
}
if ($excel->total_biller > 0){ 
    print_totals_biller($excel);
}
$excel->getActiveSheet()->mergeCells('A'.$excel->j.':C'.$excel->j.''); 
$excel->setActiveSheetIndex(0)
					->setCellValue('A'.$excel->j, "Total ")
					->setCellValue('D'.$excel->j, number_format($excel->total_30,0,'',''))
					->setCellValue('E'.$excel->j, number_format($excel->total_60,0,'',''))
					->setCellValue('F'.$excel->j, number_format($excel->total_90,0,'',''))
					->setCellValue('G'.$excel->j, number_format($excel->total_120,0,'',''))
					->setCellValue('H'.$excel->j, number_format($excel->total_mas,0,'',''))
					->setCellValue('I'.$excel->j, number_format($total,0,'','')) ;
$excel->setActiveSheetIndex(0)->getStyle("A".$excel->j.":M".$excel->j)->getFont()->setBold(true);
$excel->total_30_state = $excel->total_60_state = $excel->total_90_state = $excel->total_120_state = $excel->total_mas_state = $excel->total_state = 0;
$excel->j++;
$excel->setActiveSheetIndex(0)->getStyle('A'.$excel->j.':C'.$excel->j)->getFont()->setBold(true);
$excel->setActiveSheetIndex(0)->getStyle('A'.$excel->j.':I'.$excel->j)->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
$excel->j++;
$excel->setActiveSheetIndex(0)->getStyle('A'.$excel->j.':M'.$excel->j)->getFont()->setBold(true);
$excel->setActiveSheetIndex(0)->getStyle('A'.$excel->j.':H'.$excel->j)->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
$excel->getActiveSheet()->mergeCells('A'.$excel->j.':C'.$excel->j.''); 
$excel->getActiveSheet()->getStyle('D1:D'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
$excel->getActiveSheet()->getStyle('E1:E'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
$excel->getActiveSheet()->getStyle('F1:F'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
$excel->getActiveSheet()->getStyle('G1:G'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
$excel->getActiveSheet()->getStyle('H1:H'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
$excel->getActiveSheet()->getStyle('I1:I'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
$excel->getActiveSheet()->getStyle('J1:J'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
$excel->getActiveSheet()->getStyle('K1:K'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
$excel->getActiveSheet()->getStyle('L1:L'.$excel->j)->getNumberFormat()->setFormatCode('#,##0.00');
// Rename worksheet
$excel->getActiveSheet()->setTitle('CARTERA POR VENCIMIENTOS');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$excel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Informe de cartera por edades '.date('Y-m-d H_i_s').'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
