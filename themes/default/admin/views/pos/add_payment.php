<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_payment'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("pos/add_payment/" . $inv->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">

              <div class="col-sm-12">
                    <table class="table">
                        <tr>
                            <th class="text-right">Factura N°</th>
                            <td class="text-center"><?= $inv->reference_no ?></td>
                            <th class="text-right">Fecha</th>
                            <td class="text-center"><?= $inv->date ?></td>
                            <th class="text-right">Saldo</th>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->grand_total - $inv->paid) ?></td>
                        </tr>
                        <tr>
                            <th class="text-right">Sub Total</th>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->grand_total + $inv->order_discount - $inv->order_tax - $inv->product_tax) ?></td>
                            <th class="text-right">Impuestos</th>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->product_tax) ?></td>
                            <th class="text-right">Total</th>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->grand_total + $inv->order_discount - $inv->order_tax) ?></td>
                        </tr>
                    </table>
                </div>

                <?php if ($Owner || $Admin) { ?>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= lang("date", "date"); ?>
                            <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="date" required="required"'); ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="col-sm-6 form-group">
                    <?= lang("biller", "rcbiller"); ?>
                    <?php
                    $bl[""] = "";
                    foreach ($billers as $biller) {
                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                    }
                    ?>
                    <select name="biller" class="form-control" id="rcbiller" required="required">
                        <?php foreach ($billers as $biller): ?>
                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("reference_no", "document_type_id"); ?>
                        <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                    </div>
                </div>

                <input type="hidden" value="<?php echo $inv->id; ?>" name="sale_id"/>
            </div>

            <?php if (!$rete_applied): ?>
                <div class="form-group">
                    <label onclick="showRetention()" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención</label>
                    <input type="text" name="retencion" id="rete" class="form-control text-right" readonly>
                </div>

                <div class="section-retentions" style="display: none;">
                      <div class="row">
                        <div class="col-sm-2">
                          <label>Retención</label>
                        </div>
                        <div class="col-sm-5">
                          <label>Opción</label>
                        </div>
                        <div class="col-sm-2">
                          <label>Porcentaje</label>
                        </div>
                        <div class="col-sm-3">
                          <label>Valor</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-2">
                          <label>
                            <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                            <option>Seleccione...</option>
                          </select>
                          <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                          <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right" readonly>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-2">
                          <label>
                            <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                            <option>Seleccione...</option>
                          </select>
                          <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                          <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right" readonly>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-2">
                          <label>
                            <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                            <option>Seleccione...</option>
                          </select>
                          <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                          <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right" readonly>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-2">
                          <label>
                            <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                            <option>Seleccione...</option>
                          </select>
                          <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                          <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right" readonly>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-8 text-right">
                          <label>Total : </label>
                        </div>
                        <div class="col-md-4 text-right">
                          <label id="total_rete_amount"> 0.00 </label>
                        </div>
                        <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                      </div>
                </div>
            <?php else: ?>
                <div class="col-sm-12">
                    <label>Retención ya aplicada.</label>
                    <table class="table">
                        <tr>
                            <th class="text-center">Rete Fuente</th>
                            <th class="text-center">Rete IVA</th>
                            <th class="text-center">Rete ICA</th>
                            <th class="text-center">Rete Other</th>
                            <th class="text-center">Total</th>
                        </tr>
                        <tr>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->rete_fuente_total) ?></td>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->rete_iva_total) ?></td>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->rete_ica_total) ?></td>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->rete_other_total) ?></td>
                            <td class="text-center"><?= $this->sma->formatMoney($inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total) ?></td>
                        </tr>
                    </table>
                </div>
            <?php endif ?>

            <div class="clearfix"></div>
            <div id="payments">

                <div class="well well-sm well_1">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="payment">
                                    <div class="form-group">
                                        <?= lang("amount", "amount_1"); ?>
                                        <input name="amount-paid" type="text" id="amount_1"
                                               value="<?= $inv->grand_total - $inv->paid ?>"
                                               class="pa form-control kb-pad amount" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("paying_by", "paid_by_1"); ?>
                                    <select name="paid_by" id="paid_by_1" class="form-control paid_by" required="required">
                                        <?= $this->sma->paid_opts(); ?>
                                        <?= $pos_settings->paypal_pro ? '<option value="ppp">' . lang("paypal_pro") . '</option>' : ''; ?>
                                        <?= $pos_settings->stripe ? '<option value="stripe">' . lang("stripe") . '</option>' : ''; ?>
                                        <?=$pos_settings->authorize ? '<option value="authorize">' . lang("authorize") . '</option>' : '';?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group gc" style="display: none;">
                            <?= lang("gift_card_no", "gift_card_no"); ?>
                            <input name="gift_card_no" type="text" id="gift_card_no" class="pa form-control kb-pad"/>

                            <div id="gc_details"></div>
                        </div>
                        <div class="pcc_1" style="display:none;">
                            <div class="form-group">
                                <input type="text" id="swipe_1" class="form-control swipe"
                                       placeholder="<?= lang('swipe') ?>"/>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="pcc_no" type="text" id="pcc_no_1" class="form-control"
                                               placeholder="<?= lang('cc_no') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <input name="pcc_holder" type="text" id="pcc_holder_1" class="form-control"
                                               placeholder="<?= lang('cc_holder') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select name="pcc_type" id="pcc_type_1" class="form-control pcc_type"
                                                placeholder="<?= lang('card_type') ?>">
                                            <option value="Visa"><?= lang("Visa"); ?></option>
                                            <option value="MasterCard"><?= lang("MasterCard"); ?></option>
                                            <option value="Amex"><?= lang("Amex"); ?></option>
                                            <option value="Discover"><?= lang("Discover"); ?></option>
                                        </select>
                                        <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input name="pcc_month" type="text" id="pcc_month_1" class="form-control"
                                               placeholder="<?= lang('month') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">

                                        <input name="pcc_year" type="text" id="pcc_year_1" class="form-control"
                                               placeholder="<?= lang('year') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-3" id='ppp-stripe'>
                                    <div class="form-group">
                                        <input name="pcc_ccv" type="text" id="pcc_cvv2_1" class="form-control"
                                               placeholder="<?= lang('cvv2') ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pcheque_1" style="display:none;">
                            <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                <input name="cheque_no" type="text" id="cheque_no_1" class="form-control cheque_no"/>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>

            <div class="form-group">
                <?= lang("attachment", "attachment") ?>
                <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>



            <?php if (isset($cost_centers)): ?>
                  <div class="form-group">
                      <?= lang('cost_center', 'cost_center_id') ?>
                      <?php
                      $ccopts[''] = lang('select');
                      foreach ($cost_centers as $cost_center) {
                          $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                      }
                       ?>
                       <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                  </div>
            <?php endif ?>

            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note"'); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_payment', lang('add_payment'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<script type="text/javascript" src="<?= $assets ?>pos/js/parse-track-data.js"></script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(document).on('change', '#gift_card_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "sales/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && data.customer_id != <?=$inv->customer_id?>) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('gift_card_not_for_customer')?>');

                        } else {
                            var due = <?=$inv->grand_total-$inv->paid?>;
                            if (due > data.balance) {
                                $('#amount_1').val(formatDecimal(data.balance));
                            }
                            $('#gc_details').html('<small>Card No: <span style="max-width:60%;float:right;">' + data.card_no + '</span><br>Value: <span style="max-width:60%;float:right;">' + currencyFormat(data.value) + '</span><br>Balance: <span style="max-width:60%;float:right;">' + currencyFormat(data.balance) + '</span></small>');
                            $('#gift_card_no').parent('.form-group').removeClass('has-error');
                        }
                    }
                });
            }
        });
        $(document).on('change', '.paid_by', function () {
            var p_val = $(this).val();
            localStorage.setItem('paid_by', p_val);
            $('#rpaidby').val(p_val);
            if (p_val == 'cash') {
                $('.pcheque_1').hide();
                $('.pcc_1').hide();
                $('.pcash_1').show();
                $('#amount_1').focus();
            } else if (p_val == 'CC' || p_val == 'stripe' || p_val == 'ppp' || p_val == 'authorize') {
                if (p_val == 'CC') {
                    $('#ppp-stripe').hide();
                } else {
                    $('#ppp-stripe').show();
                }
                $('.pcheque_1').hide();
                $('.pcash_1').hide();
                $('.pcc_1').show();
                $('#swipe_1').focus();
            } else if (p_val == 'Cheque') {
                $('.pcc_1').hide();
                $('.pcash_1').hide();
                $('.pcheque_1').show();
                $('#cheque_no_1').focus();
            } else {
                $('.pcheque_1').hide();
                $('.pcc_1').hide();
                $('.pcash_1').hide();
            }
            if (p_val == 'gift_card') {
                $('.gc').show();
                $('#gift_card_no').focus();
            } else {
                $('.gc').hide();
            }
        });
        $('#pcc_no_1').change(function (e) {
            var pcc_no = $(this).val();
            localStorage.setItem('pcc_no_1', pcc_no);
            var CardType = null;
            var ccn1 = pcc_no.charAt(0);
            if (ccn1 == 4)
                CardType = 'Visa';
            else if (ccn1 == 5)
                CardType = 'MasterCard';
            else if (ccn1 == 3)
                CardType = 'Amex';
            else if (ccn1 == 6)
                CardType = 'Discover';
            else
                CardType = 'Visa';

            $('#pcc_type_1').select2("val", CardType);
        });

        $('.swipe').keypress(function (e) {
            var self = $(this);
            var id = 1;
            var TrackData = $(this).val();
            if (e.keyCode == 13) {
                e.preventDefault();

                var p = new SwipeParserObj(TrackData);

                if (p.hasTrack1) {
                    var CardType = null;
                    var ccn1 = p.account.charAt(0);
                    if (ccn1 == 4)
                        CardType = 'Visa';
                    else if (ccn1 == 5)
                        CardType = 'MasterCard';
                    else if (ccn1 == 3)
                        CardType = 'Amex';
                    else if (ccn1 == 6)
                        CardType = 'Discover';
                    else
                        CardType = 'Visa';

                    $('#pcc_no_' + id).val(p.account);
                    $('#pcc_holder_' + id).val(p.account_name);
                    $('#pcc_month_' + id).val(p.exp_month);
                    $('#pcc_year_' + id).val(p.exp_year);
                    $('#pcc_cvv2_' + id).val('');
                    $('#pcc_type_' + id).val(CardType);
                    self.val('');
                }
                else {
                    $('#pcc_no_' + id).val('');
                    $('#pcc_holder_' + id).val('');
                    $('#pcc_month_' + id).val('');
                    $('#pcc_year_' + id).val('');
                    $('#pcc_cvv2_' + id).val('');
                    $('#pcc_type_' + id).val('');
                }

                $('#pcc_cvv2_' + id).focus();
            }

        }).blur(function (e) {
            $(this).val('');
        }).focus(function (e) {
            $(this).val('');
        });
        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());

        $(document).on('change', '#rcbiller', function(){
          $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/14/") ?>'+$('#rcbiller').val(),
            type:'get',
            dataType:'JSON'
          }).done(function(data){
            response = data;

            $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }

            if (response.status == 0) {
              $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
              $('#document_type_id').trigger('change');
          });
        });

        $('#rcbiller').select2('val', '<?= $inv->biller_id; ?>');
        $('#rcbiller').select2('readonly', true);
        $('#rcbiller').trigger('change');
    });

    $('#date').datetimepicker().on('show', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });

    function showRetention(){

        sr = $('.section-retentions');

        if (sr.hasClass('active')) {
            $('.section-retentions').fadeOut().removeClass('active');
            $('#rete_fuente').attr('checked', true).trigger('click');
            $('#rete_iva').attr('checked', true).trigger('click');
            $('#rete_ica').attr('checked', true).trigger('click');
            $('#rete_otros').attr('checked', true).trigger('click');
        } else {
            $('.section-retentions').fadeIn().addClass('active');
        }

    }

// JS Retenciones

    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_fuente_option').select2('val', '').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_iva_option').select2('val', '').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_ica_option').select2('val', '').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_otros_option').select2('val', '').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option', function(){

        prevamnt = $('#rete_fuente_valor').val();
        setReteTotalAmount(prevamnt, '-');

        percentage = $('#rete_fuente_option option:selected').data('percentage');
        apply = $('#rete_fuente_option option:selected').data('apply');
        account = $('#rete_fuente_option option:selected').data('account');

        $('#rete_fuente_account').val(account);

        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));

        // console.log('Base fuente : '+amount);
        $('#rete_fuente_base').val(amount);

        cAmount = amount * (percentage / 100);

        cAmount = Math.round(cAmount);

        $('#rete_fuente_tax').val(percentage);
        $('#rete_fuente_valor').val(cAmount);

        rete_fuente_amount = formatMoney(cAmount);

        setReteTotalAmount(rete_fuente_amount, '+');

        // console.log(apply+", "+amount+" * "+percentage+" = "+cAmount);
    });

    $(document).on('change', '#rete_iva_option', function(){

        prevamnt = $('#rete_iva_valor').val();
        setReteTotalAmount(prevamnt, '-');

        percentage = $('#rete_iva_option option:selected').data('percentage');
        apply = $('#rete_iva_option option:selected').data('apply');
        account = $('#rete_iva_option option:selected').data('account');

        $('#rete_iva_account').val(account);

        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));

        // console.log('Base iva : '+amount);
        $('#rete_iva_base').val(amount);

        cAmount = amount * (percentage / 100);

        cAmount = Math.round(cAmount);

        $('#rete_iva_tax').val(percentage);
        $('#rete_iva_valor').val(cAmount);

        rete_iva_amount = formatMoney(cAmount);

        setReteTotalAmount(rete_iva_amount, '+');

        // console.log(apply+", "+amount+" * "+percentage+" = "+cAmount);
    });

    $(document).on('change', '#rete_ica_option', function(){

        prevamnt = $('#rete_ica_valor').val();
        setReteTotalAmount(prevamnt, '-');

        percentage = $('#rete_ica_option option:selected').data('percentage');
        apply = $('#rete_ica_option option:selected').data('apply');
        account = $('#rete_ica_option option:selected').data('account');

        $('#rete_ica_account').val(account);

        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));

        // console.log('Base ica : '+amount);
        $('#rete_ica_base').val(amount);

        cAmount = amount * (percentage / 100);

        cAmount = Math.round(cAmount);

        $('#rete_ica_tax').val(percentage);
        $('#rete_ica_valor').val(cAmount);

        rete_ica_amount = formatMoney(cAmount);

        setReteTotalAmount(rete_ica_amount, '+');

        // console.log(apply+", "+amount+" * "+percentage+" = "+cAmount);
    });

    $(document).on('change', '#rete_otros_option', function(){

        prevamnt = $('#rete_otros_valor').val();
        setReteTotalAmount(prevamnt, '-');

        percentage = $('#rete_otros_option option:selected').data('percentage');
        apply = $('#rete_otros_option option:selected').data('apply');
        account = $('#rete_otros_option option:selected').data('account');

        $('#rete_otros_account').val(account);

        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));

        // console.log('Base otros : '+amount);
        $('#rete_otros_base').val(amount);

        cAmount = amount * (percentage / 100);

        cAmount = Math.round(cAmount);

        $('#rete_otros_tax').val(percentage);
        $('#rete_otros_valor').val(cAmount);

        rete_otros_amount = formatMoney(cAmount);

        setReteTotalAmount(rete_otros_amount, '+');

        // console.log(apply+", "+amount+" * "+percentage+" = "+cAmount);
    });


    function getReteAmount(apply){

        amount = 0;

        if (apply == "ST") {
            amount = "<?= ($inv->grand_total + $inv->order_discount - $inv->order_tax - $inv->product_tax) ?>";
            // amount = $('#subtotalP').text();
        } else if (apply == "TX") {
            amount = "<?= $inv->product_tax ?>";
            // amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = "<?= ($inv->grand_total + $inv->order_discount - $inv->order_tax) ?>";
            // amount = $('#totalP').text();
        }

        return amount;
    }

    function setReteTotalAmount(amount, action){

        // console.log('Monto : '+amount+' acción : '+action);

        if (action == "+") {
            tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) + formatDecimal(amount);
            tra2 =  formatDecimal(parseFloat(formatDecimal($('#amount_1').val()))) - formatDecimal(amount);
        } else if (action == "-") {
            tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) - formatDecimal(amount);
            tra2 =  formatDecimal(parseFloat(formatDecimal($('#amount_1').val()))) + formatDecimal(amount);
        }

        if (tra < 0) {
            tra = 0;
            $('#rete_applied').val(0);
        } else {
            $('#rete_applied').val(1);
        }

        $('#rete').val(formatMoney(tra));
        $('#amount_1').val(tra2);
        $('#total_rete_amount').text(formatMoney(tra));
    }


// JS Retenciones
</script>
