<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class FinancingInstallmentsPayment_model extends CI_Model
{
    public $tableName = "credit_financing_installments_payment";

	public function __construct()
	{
		parent::__construct();
	}

    public function find($data)
    {
        $this->db->where($data);
        $q = $this->db->get($this->tableName);
        return $q->row();
    }

     public function getIn($field, $data)
    {
        $this->db->select("id,
            installment_id,
            SUM(capital_amount) AS capital_amount,
            SUM(current_interest_amount) AS current_interest_amount,
            SUM(interest_tax) AS interest_tax");
        $this->db->where_in($field, $data);
        $this->db->group_by("installment_id");
        $q = $this->db->get($this->tableName);
        return $q->result();
    }

    public function get($data, $groupPaidBy = false)
    {
        $this->db->select("
            cfi.id,
            SUM(sma_{$this->tableName}.capital_amount) AS capital_amount,
            SUM(sma_{$this->tableName}.current_interest_amount) AS current_interest_amount,
            SUM(sma_{$this->tableName}.interest_tax) AS interest_tax,
            SUM(sma_{$this->tableName}.default_interest) AS default_interest,
            SUM(sma_{$this->tableName}.default_interest_tax) AS default_interest_tax,
            SUM(sma_{$this->tableName}.installment_paid_amount) AS installment_paid_amount,
            CAST(payment_date AS DATE) AS paymentDate,
            CAST(payment_date AS TIME) AS paymentTime,
            cf.id AS credit_id, cf.credit_no,
            cfi.installment_amount,
            cfi.installment_no,
            b.name,
            cf.biller_id,
            sma_{$this->tableName}.created_by,
            sma_{$this->tableName}.document_type_id,
            sma_{$this->tableName}.paid_by,
            pm.name AS paidByName,
            sma_{$this->tableName}.change,
            sma_{$this->tableName}.value_received,
            c.name AS customerName,
            c.vat_no AS customerVatno,
            sma_{$this->tableName}.note,
            sma_{$this->tableName}.sale_id,
            cf.customer_id,
            cfi.installment_due_date,
            sma_{$this->tableName}.default_days,
            SUM(sma_{$this->tableName}.procredito_commission) AS procredito_commission,
            SUM(sma_{$this->tableName}.vat_procredito_commission) AS vat_procredito_commission,
            SUM(sma_{$this->tableName}.procredito_interest) AS procredito_interest,
            SUM(sma_{$this->tableName}.vat_procredito_interest) AS vat_procredito_interest
            ");
        $this->db->where($data);
        $this->db->join("credit_financing_installments cfi", "cfi.id = {$this->tableName}.installment_id");
        $this->db->join("sma_credit_financing cf", "cf.id = cfi.financing_credit_id");
        $this->db->join("sma_companies b", "b.id = {$this->tableName}.biller_id");
        $this->db->join("sma_companies c", "c.id = cf.customer_id");
        $this->db->join("sma_payment_methods pm", "pm.code = {$this->tableName}.paid_by");

        if ($groupPaidBy) {
            $this->db->group_by("installment_id, paid_by");
        } else {
            $this->db->group_by("installment_id");
        }
        $this->db->order_by("payment_date", "DESC");
        $this->db->order_by("cfi.id", "ASC");
        $q = $this->db->get($this->tableName);
        return $q->result();
    }

    public function create($data)
    {
        if ($this->db->insert($this->tableName, $data)) {
            return true;
        }
        return false;
    }

    public function createBatch($data)
    {
        if ($this->db->insert_batch($this->tableName, $data) > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function update($data, $paymentNo)
    {
        $this->db->where("payment_no", $paymentNo);
        if ($this->db->update($this->tableName, $data)) {
            return true;
        }
        return false;
    }
}