<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script>
    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";

    <?php if (isset($_POST['start_date'])): ?>
        start_date = '<?= $_POST['start_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['end_date'])): ?>
        end_date = '<?= $_POST['end_date'] ?>';
    <?php endif ?>
    $(document).ready(function () {
        oTable = $('#dmpData').dataTable({
            "aaSorting": [[1, "desc"], [2, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('products/getproducttransformations/'.($warehouse ? $warehouse->id : '')); ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "start_date",
                    "value": "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->sma->fld($this->filtros_fecha_inicial) ?>"
                }, {
                    "name": "end_date",
                    "value": "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->sma->fld($this->filtros_fecha_final) ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                            {"bSortable": false, "mRender": checkbox},
                            {"mRender": fld},
                            null,
                            null,
                            null,
                            {"mRender": decode_html},
                            {"bSortable": false,"mRender": attachment},
                            {"bSortable": false, "bSearchable": false}
                        ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[2];
                nRow.className = "product_transformation_link";
                return nRow;
            },
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('created_by');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang(' note');?>]", filter_type: "text", data: []},
        ], "footer");
        if (localStorage.getItem('remove_ptrls')) {
            if (localStorage.getItem('ptroriginitems')) {
                localStorage.removeItem('ptroriginitems');
            }
            if (localStorage.getItem('ptrdestinationitems')) {
                localStorage.removeItem('ptrdestinationitems');
            }
            if (localStorage.getItem('ptrref')) {
                localStorage.removeItem('ptrref');
            }
            if (localStorage.getItem('ptrwarehouseorigin')) {
                localStorage.removeItem('ptrwarehouseorigin');
            }
            if (localStorage.getItem('ptrwarehousedestination')) {
                localStorage.removeItem('ptrwarehousedestination');
            }
            if (localStorage.getItem('ptrnote')) {
                localStorage.removeItem('ptrnote');
            }
            if (localStorage.getItem('ptrdate')) {
                localStorage.removeItem('ptrdate');
            }
            if (localStorage.getItem('trnf_companies_id')) {
                localStorage.removeItem('trnf_companies_id');
            }
            if (localStorage.getItem('trnf_document_type_id')) {
                localStorage.removeItem('trnf_document_type_id');
            }
            if (localStorage.getItem('ptrbiller')) {
                localStorage.removeItem('ptrbiller');
            }
            if (localStorage.getItem('ptrdestination_biller')) {
                localStorage.removeItem('ptrdestination_biller');
            }
            localStorage.removeItem('remove_ptrls');
        }
        <?php if ($this->session->userdata('remove_ptrls')) { ?>
            if (localStorage.getItem('ptroriginitems')) {
                localStorage.removeItem('ptroriginitems');
            }
            if (localStorage.getItem('ptrdestinationitems')) {
                localStorage.removeItem('ptrdestinationitems');
            }
            if (localStorage.getItem('ptrref')) {
                localStorage.removeItem('ptrref');
            }
            if (localStorage.getItem('ptrwarehouseorigin')) {
                localStorage.removeItem('ptrwarehouseorigin');
            }
            if (localStorage.getItem('ptrwarehousedestination')) {
                localStorage.removeItem('ptrwarehousedestination');
            }
            if (localStorage.getItem('ptrnote')) {
                localStorage.removeItem('ptrnote');
            }
            if (localStorage.getItem('trnf_companies_id')) {
                localStorage.removeItem('trnf_companies_id');
            }
            if (localStorage.getItem('trnf_document_type_id')) {
                localStorage.removeItem('trnf_document_type_id');
            }
            if (localStorage.getItem('ptrdate')) {
                localStorage.removeItem('ptrdate');
            }
        <?php $this->sma->unset_data('remove_ptrls');}
        ?>
    });
</script>

<div class="wrapper wrapper-content animated fadeInRight no-print">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins border-bottom">
            <div class="ibox-title">
                <h5><?= lang('filter') ?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <?= admin_form_open('products/product_transformations') ?>

                            <div class="col-md-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                <div class="form-group">
                                    <?= lang('date_records_filter', 'date_records_filter_h') ?>
                                    <select name="date_records_filter" id="date_records_filter_h" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>
                            </div>

                            <div class="date_controls_h">
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('start_date', 'start_date_h') ?>
                                    <input type="date" name="start_date" id="start_date_h" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                </div>
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('end_date', 'end_date_h') ?>
                                    <input type="date" name="end_date" id="end_date_h" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit" id="submit-sales-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php if ($Owner || $GP['bulk_actions']) {
        echo admin_form_open('products/adjustment_actions', 'id="action-form"');
    }
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle">
                                    <?= lang('actions') ?> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?= admin_url('products/add_product_transformation') ?>">
                                            <i class="fa fa-plus-circle"></i> <?= lang('add_product_transformation') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                </ul>
                            </div>
                            <?php if (!empty($warehouses)) { ?>
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle">
                                <?= lang("warehouses") ?> <span class="caret"></span>
                                </button>
                                <!-- <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel"> -->
                                <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                                    <li><a href="<?= admin_url('products/product_transformations') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                                    <li class="divider"></li>
                                    <?php
                                    foreach ($warehouses as $warehouse) {
                                        echo '<li><a href="' . admin_url('products/product_transformations/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><?= lang('list_results'); ?></p> -->
                            <h4 class="text_filter"></h4>
                            <div class="table-responsive">
                                <table id="dmpData" class="table table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th class="col-xs-2"><?= lang("date"); ?></th>
                                        <th class="col-xs-2"><?= lang("reference_no"); ?></th>
                                        <th class="col-xs-2"><?= lang("warehouse"); ?></th>
                                        <th class="col-xs-2"><?= lang("created_by"); ?></th>
                                        <th><?= lang("note"); ?></th>
                                        <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                        <th style="min-width:75px; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr class="active">
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th></th><th></th><th></th><th></th><th></th>
                                        <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                        <th style="width:75px; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- /.ibox-content -->
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>
<script type="text/javascript">
    $(document).ready(function(){
        setFilterText();
    });
    function setFilterText(){
        var start_date_text = $('#start_date_h').val();
        var end_date_text = $('#end_date_h').val();
        var text = "Filtros configurados : ";
        coma = false;
        if (start_date !== undefined) {
            text+= coma ? "," : "";
            text+=" Fecha de inicio ("+start_date_text+")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text+= coma ? "," : "";
            text+=" Fecha final ("+end_date_text+")";
            coma = true;
        }
        $('.text_filter').html(text);
    }
   $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_h').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_h').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });
</script>