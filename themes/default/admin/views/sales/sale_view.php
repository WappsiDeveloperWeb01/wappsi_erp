<?php
/***********

FACTURA DE VENTA CON DATOS DE LA EMPRESA, DE ACUERDO A LO PARAMETRIZADO EN EL TIPO DE DOCUMENTO SE DISCRIMINA O NO EL IVA

************/
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        //izquierda
        if (file_exists('assets/uploads/logos/'.$this->logo)) {
            if ($this->biller_logo == 2) {
                $this->Image(base_url().'assets/uploads/logos/'.$this->logo,27,7,28);
            } else {
                $this->Image(base_url().'assets/uploads/logos/'.$this->logo,14,12,55);
            }
        }
        if ($this->show_document_type_header == 1) {
            $this->SetFont('Arial','',$this->fuente);
            $this->setXY(75, 36);
            $this->MultiCell(123, 3,  $this->sma->utf8Decode($this->crop_text(strip_tags($this->sma->decode_html($this->invoice_header)), 145)), 0, 'L');
        }
        $cx = 75;
        $cy = 10;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->MultiCell(75, 3.5,  $this->sma->utf8Decode(($this->biller->company != "-" ? $this->biller->company : $this->biller->name)), '', 'L');
        $cy  = $this->getY();
        $cy+=1.3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->digito_verificacion != "" ? '-'.$this->biller->digito_verificacion : '')."  ".$this->tipo_regimen),'',1,'L');
        if ($this->ciiu_code) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode("Códigos CIIU : ".$this->ciiu_code),'',1,'L');
        }
        if ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode(
                                                ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? 'Auto retenedor de ' : '').
                                                ($this->Settings->fuente_retainer ? 'Fuente' : '').
                                                ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '').'IVA ' : '').
                                                ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '').'ICA ' : '')
                                            ),'',1,'L');
        }
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(isset($this->Settings->url_web) ? $this->Settings->url_web : ''),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+(6));
        $this->RoundedRect(138, 10, 71, 31, 3, '1234', '');
        $cx = 140;
        $cy = 15;
        $this->setXY($cx, $cy);
        $this->MultiCell(40, 5,  $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'FACTURA DE VENTA'), '', 'L');
        $cy +=13;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'L');
        if (!empty($this->factura->cufe)) {
            $this->Image(base_url().'themes/default/admin/assets/images/qr_code/'.$this->factura->reference_no.'.png', 180, 12.5, 26);
        }

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 43, 125, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 48, 125, 27, 3, '4', '');
        $cx = 13;
        $cy = 43;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('Información del cliente'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->phone_negocio),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $altura = 5;
        $adicional_altura = 1;
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(138, 43, 71, 5, 3, '2', 'DF');
        $this->RoundedRect(138, 48, 71, 27, 3, '3', '');
        $cx = 138;
        $cy = 43;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Fecha Factura'),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Total'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, $this->factura->grand_total * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Fecha Vencimiento'),'LBR',1,'C',1);
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Forma de Pago'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".($this->factura->payment_term > 0 ? $this->factura->payment_term : 0)." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->sale_payment_method),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(71, $altura , $this->sma->utf8Decode('Vendedor'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cy += $altura;
        $this->setXY($cx, $cy);
        if ($this->seller) {
            $this->Cell(71, $altura , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');
        } else {
            $this->Cell(71, $altura , $this->sma->utf8Decode('Ventas Varias'),'',1,'C');
        }

        $this->ln(1.7);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->Cell(17,5, $this->sma->utf8Decode('Código'),'TBLR',0,'C',1);
        $this->Cell(79,5, $this->sma->utf8Decode('Descripción de producto o Servicio'),'TBLR',0,'C',1);
        $this->Cell(13,5, $this->sma->utf8Decode('Cant.'),'TBLR',0,'C',1);
        $this->Cell(13,5, $this->sma->utf8Decode('Und.'),'TBLR',0,'C',1);
        if ($this->tax_inc) {
            $unit_price_size = 22.2;
            $this->Cell(17,5, $this->sma->utf8Decode(lang('tax')),'TBLR',0,'C',1);
        } else {
            $unit_price_size = 39.2;
        }
        $this->Cell($unit_price_size,5, $this->sma->utf8Decode('Valor Unitario'),'TBLR',0,'C',1);

        $this->Cell(35.2,5, $this->sma->utf8Decode('Valor total'),'TBLR',1,'C',1);
    }
    function Footer()
    {
        // Print centered page number
        if ($this->description2 != '') {
            $this->SetXY(13, -39.4);
            $this->RoundedRect(13, 238.5, 196, 5, 1, '1234', '');
            $this->Cell(196, 3 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
            $this->RoundedRect(13, 244.5, 196, 21, 3, '1234', '');
            $this->SetXY(14, -33);
            if (isset($this->cost_center) && $this->cost_center) {
                $this->MultiCell(194,3,$this->sma->utf8Decode($this->reduceTextToDescription2( "Centro de costo : ".$this->cost_center->name." (".$this->cost_center->code.") \n".$this->description2)), 0, 'L');
            } else {
                $this->MultiCell(195,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
            }
        } else {
            $this->SetXY(13, -18);
            $this->RoundedRect(13, 260, 196, 5, 1, '1234', '');
            $this->Cell(196, 3 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
        }
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi se lo simplifica     www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Página N° '.$this->PageNo()),'',1,'C');
    }
    function reduceTextToDescription1($text){
        $text=$text;
        if (strlen($text) > 310) {
            $text = substr($text, 0, 305);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
    function reduceTextToDescription2($text){
        if (strlen($text) > 870) {
            $text = substr($text, 0, 865);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
}
$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);
$fuente = 8;
$adicional_fuente = 0;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();
$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->sale_payment_method = $sale_payment_method;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->value_decimals = $value_decimals;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}
$pdf->tipo_regimen = $tipo_regimen;
$pdf->ciiu_code = $ciiu_code;
$pdf->invoice_header = $invoice_header ? $invoice_header : "";
$pdf->show_document_type_header = $show_document_type_header;
$pdf->AddPage();

if ($document_type->invoice_footer != '') {
    $maximo_footer = 220;
} else {
    $maximo_footer = 245;
}

$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);
$taxes = [];
// for ($i=0; $i < 50 ; $i++) {
    foreach ($rows as $item) {
        $pr_quantity = $item->quantity;
        if (isset($item->operator)) {
            if ($item->operator == "*") {
                $pr_quantity = $item->quantity / $item->operation_value;
            } else if ($item->operator == "/") {
                $pr_quantity = $item->quantity * $item->operation_value;
            } else if ($item->operator == "+") {
                $pr_quantity = $item->quantity - $item->operation_value;
            } else if ($item->operator == "-") {
                $pr_quantity = $item->quantity + $item->operation_value;
            }
        }
        $pr_unit_code = $item->product_unit_code;

        $columns = [];
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        $columns[1]['inicio_X'] = $pdf->getX();
        $pdf->Cell(17,5, $this->sma->utf8Decode($show_code == 1 ? $item->product_code : $item->reference),'',0,'C');
        $columns[1]['fin_X'] = $pdf->getX();
        $pr_name =
                    ($item->under_cost_authorized == 1 ? '    ' : '').
                    $item->product_name.
                    (!is_null($item->variant) && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? "( ".$item->variant." )" : '').
                    ($this->Settings->show_brand_in_product_search ? " - ".$item->brand_name : '').
                    (!is_null($item->serial_no) ? " - ".$item->serial_no : '').
                    strip_tags($this->sma->decode_html(($show_product_preferences == 1 && $item->preferences ? ' (' . $this->sma->print_preference_selection($item->preferences) . ')' : '')));
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $columns[2]['inicio_X'] = $pdf->getX();
        $pdf->setXY($pdf->getX(), $pdf->getY()+0.5);
        $unlock_icon_X = $pdf->getX();
        $pdf->MultiCell(79,4, $this->sma->utf8Decode($pr_name),'','L');
        $unlock_icon_Y = $pdf->getY() - 4;
        if ($item->under_cost_authorized == 1) {
            $pdf->Image(base_url().'assets/images/unlock-icon.png',$unlock_icon_X+0.8, $unlock_icon_Y+0.4,2.6);
        }

        $columns[2]['fin_X'] = $cX+79;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+79, $cY);
        $columns[3]['inicio_X'] = $pdf->getX();
        $pdf->Cell(13,5, $this->sma->utf8Decode($this->sma->formatQuantity($pr_quantity, $qty_decimals)),'',0,'C');
        $columns[3]['fin_X'] = $pdf->getX();
        $columns[4]['inicio_X'] = $pdf->getX();
        $pdf->Cell(13,5, $this->sma->utf8Decode($pr_unit_code),'',0,'C');
        $columns[4]['fin_X'] = $pdf->getX();
        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
        }
        if ($tax_inc) {
            $unit_price_size = 22.2;
            $columns[5]['inicio_X'] = $pdf->getX();
            $pdf->Cell(17,5, $this->sma->utf8Decode("% ".$this->sma->formatDecimal($item->tax_rate, 0)),'',0,'R');
            $columns[5]['fin_X'] = $pdf->getX();
        } else {
            $unit_price_size = 39.2;
        }
        if ($view_tax) {
            $columns[6]['inicio_X'] = $pdf->getX();
            $pdf->Cell($unit_price_size,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->unit_price * $trmrate)),'',0,'R');
            $columns[6]['fin_X'] = $pdf->getX();
            $columns[7]['inicio_X'] = $pdf->getX();
            $pdf->Cell(35.2,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->unit_price * $item->quantity * $trmrate)),'',0,'R');
            $columns[7]['fin_X'] = $pdf->getX();
        } else {
            $columns[6]['inicio_X'] = $pdf->getX();
            $pdf->Cell($unit_price_size,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->net_unit_price)),'',0,'R');
            $columns[6]['fin_X'] = $pdf->getX();
            $columns[7]['inicio_X'] = $pdf->getX();
            $pdf->Cell(35.2,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->net_unit_price * $item->quantity)),'',0,'R');
            $columns[7]['fin_X'] = $pdf->getX();
        }
            $fin_fila_X = $pdf->getX();
            $fin_fila_Y = $pdf->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $altura_fila = ($altura_fila < 5 ? 5 : $altura_fila);
            $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
        // $this->sma->print_arrays($columns);
        foreach ($columns as $key => $data) {
            $ancho_column = $data['fin_X'] - $data['inicio_X'];
            $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
        }
        $pdf->ln($altura_fila);
    }
// }

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

    $current_x = $pdf->getX();
    $current_y = $pdf->getY() + 1;
// $pdf->RoundedRect($current_x, $current_y, 111.6, 12.25, 3, '1234', '');
$pdf->ln(1);
$pdf->setX(16);
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(115.6,5, $this->sma->utf8Decode('Valor (En letras)'),'',1,'L');

    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

$pdf->setX(16);
$pdf->SetFont('Arial','',$fuente-1.2);
// $pdf->Cell(115.6,6.125, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total, $currencies_names[$inv->sale_currency])),'',1,'L');
$pdf->MultiCell(113.6, 4, $this->sma->utf8Decode($number_convert->convertir((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate), (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency))), '', 'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->setX(13);

$pdf->setXY($current_x, $current_y+9);

    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->SetFont('Arial','B',$fuente);
    $pdf->Cell(113.4,0, $this->sma->utf8Decode(''),'B',1,'C');
    $pdf->Cell(28.89,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
    $pdf->Cell(28.89,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
    $pdf->Cell(28.89,5, $this->sma->utf8Decode('Valor Impuesto'),'',0,'C');
    $pdf->Cell(28.89,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');
    // $pdf->ln(1);
$pdf->SetFont('Arial','',$fuente);
$total_tax_base = 0;
$total_tax_amount = 0;
foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $total =$arr['base'] + $arr['tax'];
    $pdf->Cell(28.89,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"),'',1,'C');
    $pdf->setXY($current_x + 28.89, $current_y);
    $pdf->setX($current_x + 28.89);
    $pdf->Cell(28.89,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['base'] * $trmrate)),'',1,'C');
    $total_tax_base += $arr['base'] * $trmrate;
    $pdf->setXY($current_x + (28.89 * 2), $current_y);
    $pdf->setX($current_x + (28.89 * 2));
    $pdf->Cell(28.89,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['tax'] * $trmrate)),'',1,'C');
    $total_tax_amount += $arr['tax'] * $trmrate;
    $pdf->setXY($current_x + (28.89 * 3), $current_y);
    $pdf->setX($current_x + 28.89 * 3);
    $pdf->Cell(28.89,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total)),'',1,'C');
}


if ($inv->order_tax > 0) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->Cell(28.9,3, $this->sma->utf8Decode("(SP) ".$this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id]." %" : "0%"))),'',1,'C');
    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)),'',1,'C');
    $total_tax_base +=  $inv->total * $trmrate;

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax * $trmrate)),'',1,'C');
    $total_tax_amount += $inv->order_tax * $trmrate;

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total * $trmrate)),'',1,'C');
}

if ($inv->consumption_sales > 0 && !empty($secundaryTaxes)) {
    foreach ($secundaryTaxes as $secondTax) {
        $current_x = $pdf->getX();
        $current_y = $pdf->getY();

        $pdf->Cell(28.9,3, $this->sma->utf8Decode($secondTax['name']),'',1,'C');
        $pdf->setXY($current_x + 28.9, $current_y);
        $pdf->setX($current_x + 28.9);
        $pdf->Cell(28.9,3, $this->sma->utf8Decode(''),'',1,'C');

        $pdf->setXY($current_x + (28.9 * 2), $current_y);
        $pdf->setX($current_x + (28.9 * 2));
        $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $secondTax["tax"])),'',1,'C');
        $total_tax_amount += $inv->consumption_sales * $trmrate;

        $pdf->setXY($current_x + (28.9 * 3), $current_y);
        $pdf->setX($current_x + 28.9 * 3);
        $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $secondTax["tax"])),'',1,'C');
    }
}

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(28.9,3, $this->sma->utf8Decode("Total"),'',1,'C');
$pdf->setXY($current_x + 28.9, $current_y);
$pdf->setX($current_x + 28.9);
$pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base)),'',1,'C');

$pdf->setXY($current_x + (28.9 * 2), $current_y);
$pdf->setX($current_x + (28.9 * 2));
$pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_amount)),'',1,'C');

$pdf->setXY($current_x + (28.9 * 3), $current_y);
$pdf->setX($current_x + 28.9 * 3);
$pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base + $total_tax_amount)),'',1,'C');
$pdf->SetFont('Arial','',$fuente);

$tax_summary_end = $pdf->getY() + 3;

//derecha

$pdf->setXY($cX_items_finished+117, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$l_column = 44.2;
$r_column = 35.2;

$pdf->cell($l_column, 5, $this->sma->utf8Decode('Total bruto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->total * $trmrate))), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($l_column, 5, $this->sma->utf8Decode('Valor Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax + $inv->product_tax * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención en la fuente '.($inv->rete_fuente_percentage > 0 ? $this->sma->formatQuantity($inv->rete_fuente_percentage, $qty_decimals)." %" : "")), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');

if ($inv->rete_iva_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención al IVA '.($inv->rete_iva_percentage > 0 ? $this->sma->formatQuantity($inv->rete_iva_percentage, $qty_decimals)." %" : "")), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención al ICA '.($inv->rete_ica_percentage > 0 ? $this->sma->formatQuantity($inv->rete_ica_percentage, $qty_decimals)." %" : "")), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_other_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode(lang('rete_other')." ".($inv->rete_other_percentage > 0 ? $this->sma->formatQuantity($inv->rete_other_percentage, $qty_decimals)." %" : "")), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_bomberil_id && $inv->rete_bomberil_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode(lang('rete_bomberil')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+39.2, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_bomberil_total * $trmrate)), 'TBR', 1, 'R');
}


if ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode(lang('rete_autoaviso')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+39.2, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_autoaviso_total * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($l_column, 5, $this->sma->utf8Decode('Descuento'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_discount * $trmrate)), 'TBR', 1, 'R');

if ($inv->shipping > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('  Serv. Transporte'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+2);
$pdf->cell($l_column, 5, $this->sma->utf8Decode('Total'), 'TBLR', 1, 'L', 1);
$pdf->setXY($current_x+$l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate)), 'TBR', 1, 'R', 1);

$pdf->SetFont('Arial','',$fuente);

$inv_total_summary_end = $pdf->getY() + 3;

if ($tax_summary_end > $inv_total_summary_end) {
    $pdf->setXY(13, $tax_summary_end);
} else {
    $pdf->setXY(13, $inv_total_summary_end);
}

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;

$cX = $pdf->getX();
$cY = $pdf->getY();
$pdf->setX(16);
$pdf->SetFont('Arial','B',$fuente);
$pdf->cell(10, 3, $this->sma->utf8Decode('Nota : '), '', 1, 'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->setX(15);
$pdf->setXY($cX+12, $cY);
$pdf->MultiCell(190,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 20, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->SetFont('Arial','B',$fuente);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');
$pdf->SetFont('Arial','',$fuente);

$pdf->setXY($current_x+3, $current_y+15);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+15);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

//derecha
$pdf->setXY($current_x+78.4, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 20, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->SetFont('Arial','B',$fuente);
$pdf->MultiCell(110,4, $this->sma->utf8Decode('Firma autorizada'),0,'L');
$pdf->SetFont('Arial','',$fuente);
if ($signature_root) {
    $pdf->Image($signature_root, $current_x+35,$current_y+3,35);
}

if ($inv->sale_status == 'pending') {
    $pdf->setXY(90, 90);
    $pdf->SetFont('Arial','B',50);
    $pdf->SetTextColor(125,125,125);
    $pdf->RotatedImage(base_url().'assets/images/not_approved.png',20,220,240,18,45);
    $pdf->SetFont('Arial','',$fuente);
    $pdf->SetTextColor(0,0,0);
}

if ($for_email === true) {
    $pdf->Output(FCPATH."files/". $filename .".pdf", "F");
} else {
    if ($download) {
        $pdf->Output($filename.".pdf", "D");
    } else if ($internal_download == TRUE) {
        $pdf->Output("files/electronic_billing/". $filename .".pdf", "F");
    } else {
        $pdf->Output($filename.".pdf", "I");
    }
}