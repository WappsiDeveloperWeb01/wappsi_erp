$(document).ready(function () {
    if (!localStorage.getItem('qaref')) {
        localStorage.setItem('qaref', '');
    }
    ItemnTotals();
    $('.bootbox').on('hidden.bs.modal', function (e) {
        $('#add_item').focus();
    });
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }
    // If there is any item in localStorage
    if (localStorage.getItem('qaitems')) {
        loadItems();
    }
    // clear localStorage and reload
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('qaitems')) {
                    localStorage.removeItem('qaitems');
                }
                if (localStorage.getItem('qaref')) {
                    localStorage.removeItem('qaref');
                }
                if (localStorage.getItem('qawarehouse')) {
                    localStorage.removeItem('qawarehouse');
                }
                if (localStorage.getItem('qanote')) {
                    localStorage.removeItem('qanote');
                }
                if (localStorage.getItem('qadate')) {
                    localStorage.removeItem('qadate');
                }
                if (localStorage.getItem('qa_companies_id')) {
                    localStorage.removeItem('qa_companies_id');
                }
                if (localStorage.getItem('adjustmet_type')) {
                    localStorage.removeItem('adjustmet_type');
                }
                $('#modal-loading').show();
                location.reload();
            }
        });
    });
    // save and load the fields in and/or from localStorage
    $('#qaref').change(function (e) {
        localStorage.setItem('qaref', $(this).val());
    });
    if (qaref = localStorage.getItem('qaref')) {
        $('#qaref').val(qaref);
    }
    if (qa_companies_id = localStorage.getItem('qa_companies_id')) {
        $('#companies_id').select2('val', qa_companies_id);
    }

    
    $('#qawarehouse').change(function (e) {
        localStorage.setItem('qawarehouse', $(this).val());
    });

    $('#adjustmet_type').change(function (e) {
        localStorage.setItem('adjustmet_type', $(this).val());
        $('#adjustmet_type').select2('readonly', true);
        if ($(this).val() == 1) {
            $('.cost').fadeIn();
            $('.avg_cost').fadeOut();
        } else {
            $('.cost').fadeOut();
            $('.avg_cost').fadeIn();
        }
    });

    if (qawarehouse = localStorage.getItem('qawarehouse')) {
        $('#qawarehouse').select2("val", qawarehouse);
    }

    //$(document).on('change', '#qanote', function (e) {
        $('#qanote').redactor('destroy');
        $('#qanote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('qanote', v);
            }
        });
        if (qanote = localStorage.getItem('qanote')) {
            $('#qanote').redactor('set', qanote);
        }

    // prevent default action upon enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });


    /* ----------------------
     * Delete Row Method
     * ---------------------- */

    $(document).on('click', '.qadel', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        delete qaitems[item_id];
        row.remove();
        if(qaitems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('qaitems', JSON.stringify(qaitems));
            loadItems();
            return;
        }
    });

    /* --------------------------
     * Edit Row Quantity Method
     -------------------------- */

    $(document).on("change", '.rquantity', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        qaitems[item_id].row.qty = new_qty;
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
    }).on('keydown', '.rquantity', function(e){
        if (e.keyCode == 13) {
            localStorage.setItem('enter_pressed_for_quantity', true);
            $(this).trigger('change');
            setTimeout(function() {
                localStorage.removeItem('enter_pressed_for_quantity');
            }, 2000);
        }
    });

    $(document).on("change", '.rnetcost', function () {
        var new_cost = parseFloat($(this).val());
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        if (site.settings.tax_method == 0) {
            item_tax = calculateTax(qaitems[item_id].tax_rate, new_cost, 1);
            item_tax_val = item_tax[0];
            new_cost = new_cost + item_tax_val;
        }
        qaitems[item_id].row.cost = new_cost;
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
    });

    $(document).on("change", '.rcost', function () {
        var new_cost = parseFloat($(this).val());
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        var prev_cost = qaitems[item_id].row.cost;
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(prev_cost);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        if (site.settings.tax_method == 1) {
            item_tax = calculateTax(qaitems[item_id].tax_rate, new_cost, 0);
            item_tax_val = item_tax[0];
            new_cost = new_cost - item_tax_val;
        }
        qaitems[item_id].row.cost = new_cost;
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
    });

    $(document).on("change", '.rtype', function () {
        var row = $(this).closest('tr');
        var new_type = $(this).val(),
        item_id = row.attr('data-item-id');
        qaitems[item_id].row.type = new_type;
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
    });

    $(document).on("change", '.rvariant', function () {
        var row = $(this).closest('tr');
        var new_opt = $(this).val(),
        item_id = row.attr('data-item-id');
        qaitems[item_id].row.option = new_opt;
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
    });


    if (qabiller = localStorage.getItem('qabiller')) {
        $('#qabiller').select2("val", qabiller).trigger('change');
    } else {
        $('#qabiller').trigger('change');
    }

    if (adjustmet_type = localStorage.getItem('adjustmet_type')) {
        $('#adjustmet_type').select2('val', adjustmet_type);
        setTimeout(function() {
            $('#adjustmet_type').select2('readonly', true).trigger('change');
        }, 800);
    }

    setTimeout(function() {
        if (max_qpr > 0) {
            command: toastr.warning('Los ajustes se harán de a '+max_qpr+' productos por temas de rendimiento en la plataforma', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
        }

        if (site.settings.product_variant_per_serial == 1) {
            if (!localStorage.getItem('qaitems')) {
                $('#add_item').focus();
            }
        }

    }, 850);

});


/* -----------------------
 * Load Items to table
 ----------------------- */
//aca load

function loadItems() {
    if (localStorage.getItem('qaitems')) {
        _restrict_permission_cost = (is_admin || is_owner || user_permissions.products_cost == 1) ? '' : 'style=" display:none;"';
        count = 1;
        an = 1;
        Ttotal = 0;
        $("#qaTable tbody").empty();
        qaitems = JSON.parse(localStorage.getItem('qaitems'));
        sortedItems = _.sortBy(qaitems, function(o) { return [parseInt(o.order)]; });
        var lock_by_item_quantity = false;
        $.each(sortedItems, function (index, item) {
            if (index == 0 ) {
                count = 1;
                an = 1;
                Ttotal = 0;
            }
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_qty = item.row.qty, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var type = item.row.type !== undefined ? item.row.type : '';
            var adjustmet_type = localStorage.getItem('adjustmet_type') ? localStorage.getItem('adjustmet_type') : $('#adjustmet_type').val();
            item_tax = 0;
            item_net_cost = 0;
            if (adjustmet_type == 1) {
                var item_cost = item.row.cost;
                item_tax = calculateTax(item.tax_rate, item_cost, site.settings.tax_method);
                item_tax_val = item_tax[0];
                if (site.settings.tax_method == 1) {
                    item_net_cost = item_cost;
                    item_cost = formatDecimal(parseFloat(item_cost) + parseFloat(item_tax_val));
                } else {
                    item_net_cost = formatDecimal(parseFloat(item_cost) - parseFloat(item_tax_val));
                }
            } else {
                var item_cost = item.row.avg_cost;
                item_tax = calculateTax(item.tax_rate, item_cost, 0);
                item_tax_val = item_tax[0];
                item_net_cost = item_cost - item_tax_val;
            }
                

            var item_total = item.row.qty * item_cost;

            var opt = $("<select id=\"poption\" name=\"variant\[\]\" class=\"form-control select rvariant\" />");
            if(item.options !== false && site.settings.product_variant_per_serial == 0) {
                $.each(item.options, function () {
                    if (item.row.option == this.id)
                        $("<option />", {value: this.id, text: this.name, selected: 'selected'}).appendTo(opt);
                    else
                        $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                });
            } else {
                $("<option />", {value: 0, text: 'n/a'}).appendTo(opt);
                opt = opt.hide();
            }
            Ttotal += item_total;
            var row_no = item.id;
             var sel_opt = '';
            if(item.options !== false && site.settings.product_variant_per_serial == 0) {
                $.each(item.options, function () {
                    if(this.id == item_option) {
                        sel_opt = this.code;
                    }
                });
            }
            
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                            (item.row.serial_subtraction_option_id !== undefined ? '<input type="hidden" name="serial_subtraction_option_id[]" value="'+item.row.serial_subtraction_option_id+'">' : '')+
                            '<input name="product_name[]" type="hidden" value="' + item_name + '">'+

                            '<span class="sname" id="name_' + row_no + '">' + item_code +' - ' + item_name+' '+ (sel_opt ? ' ('+sel_opt+')' : '')  +'</span>'+
                            (item.row.op_pending_quantity !== undefined && item.row.op_pending_quantity > 0 ? ' <i class="fa-solid fa-info-circle"  data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+
                                "En stock : "+formatDecimal((parseFloat(item.row.op_pending_quantity)+parseFloat(item.row.quantity)))+" | "+
                                "Pendientes orden de pedido : "+formatDecimal(item.row.op_pending_quantity)+" | "+
                                "Disponibles : "+formatDecimal(item.row.quantity)+
                            '"'+'"></i>': "")+
                        '</td>';
            if (site.settings.product_variant_per_serial == 0) {
                tr_html += '<td>'+(opt.get(0).outerHTML)+'</td>';
            } else if (site.settings.product_variant_per_serial == 1) {
                tr_html += '<td>'+(item.row.serialModal_serial)+'</td>';
            }
            tr_html += '<td>'+
                            '<select name="type[]" class="form-contol select rtype" id="pr_type_' + item_id + '" style="width:100%;"><option value="subtraction"'+(type == 'subtraction' ? ' selected' : '')+'>'+type_opt.subtraction+'</option><option value="addition"'+(type == 'addition' ? ' selected' : '')+'>'+type_opt.addition+'</option></select>'+
                        '</td>';

            if (site.settings.set_adjustment_cost == 1) {
                readonly = '';
                if (type == 'subtraction') {
                    readonly = 'readonly="readonly"';
                }
                tr_html += '<td' +_restrict_permission_cost+ '>'+
                                '<input class="form-control rnetcost text-center" name="net_cost[]" value="'+item_net_cost+'"  id="net_cost_' + row_no + '" '+readonly+'>'+
                            '</td>';
                tr_html += '<td' +_restrict_permission_cost+ '>'+
                                '<input class="form-control rcost text-center" name="cost[]" type="text" id="cost_' + row_no + '" value="'+item_cost+'" '+readonly+'>'+
                                '<input name="item_tax[]" type="hidden" value="'+item_tax_val+'">'+
                            '</td>';
            } else {
                tr_html += '<td class="text-right"' +_restrict_permission_cost+  '>' 
                                +formatMoney(item_net_cost)+
                                '<input class="form-control text-center" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="net_cost[]" type="hidden" value="' + item_net_cost + '" readonly>'+
                            '</td>'+
                            '<td class="text-right"' +_restrict_permission_cost+ '>'
                                +formatMoney(item_cost)+
                                '<input class="form-control text-center" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="cost[]" type="hidden" value="' + item_cost + '" readonly>'+
                            '</td>'; //unitario
            }
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + item_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+(item.row.serial_subtraction_option_id !== undefined ? 'readonly' : '')+' >'+
                        '</td>';

            tr_html += '<td class="text-right">'+formatMoney(item_total)+
                            '<input class="form-control text-center" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="total[]" type="hidden" value="' + item_total+ '" readonly>'+
                            '<input name="serialModal_serial[]" type="hidden" value="' + (item.row.serialModal_serial ? item.row.serialModal_serial : '')+ '">'+
                        '</td>'; //total
            if (site.settings.product_serial == 1) {
                tr_html += '<td class="text-right"><input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'"></td>';
            }
            tr_html += '<td class="text-center"><i class="fa fa-times tip qadel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            if (site.settings.product_order == 1) {
                newTr.appendTo("#qaTable");
            } else {
                newTr.prependTo("#qaTable");
            }
            if (site.settings.overselling == 0 && $('#adjustmet_type').val() == 2 && (parseFloat(item_qty) > parseFloat(item.row.quantity))) {
                $('#row_' + row_no).addClass('danger');
                lock_by_item_quantity = true;
            }
            count += parseFloat(item_qty);
            an++;

            $('[data-toggle="tooltip"]').tooltip();
            // if (site.settings.product_variant_per_serial == 1) {
                setTimeout(function() {
                    $('#pr_type_' + item_id).select2('readonly', true);
                }, 800);
            // }

        });
        
        var col = 4;
        if (site.settings.product_variant_per_serial == 1) {
            col++;
        }
        if (site.settings.product_serial == 1) {
                col++;
            }
        if (_restrict_permission_cost != '') col--;  
        var tfoot = '<tr id="tfoot" class="tfoot active"><th colspan="'+col+'">Total ('+(an - 1)+')</th>'+(site.settings.set_adjustment_cost ? '<th' +_restrict_permission_cost+ '></th>' : '')+
                        '<th class="text-center">' + formatQuantity2(parseFloat(count) - 1) + '</th>'+
                        '<th class="text-right">' + formatMoney(parseFloat(Ttotal)) + '</th>';
        if (site.settings.product_serial == 1) { tfoot += '<th></th>'; }
        tfoot +=        '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                    '</tr>';
        $('#qaTable tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        $('#titems').text('Artículos '+(an-1)+' ('+(formatQty(parseFloat(count) - 1))+')');
        set_page_focus(an);
        if (lock_by_item_quantity) {
            $('#add_adjustment_submit').prop('disabled', true);
        } else {
            $('#add_adjustment_submit').prop('disabled', false);
        }
    }
}

/* -----------------------------
 * Add Purchase Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
function add_adjustment_item(item, modal = false, cont = false) {
    if (count == 1) {
        qaitems = {};
    }
    if (item == null)
        return;
    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (qaitems[item_id] !== undefined) {
        var new_qty = parseFloat(qaitems[item_id].row.qty) + 1;
        qaitems[item_id].row.base_quantity = new_qty;
        if(qaitems[item_id].row.unit != qaitems[item_id].row.base_unit) {
            $.each(qaitems[item_id].units, function(){
                if (this.id == qaitems[item_id].row.unit) {
                    qaitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        qaitems[item_id].row.qty = new_qty;

    } else {
        qaitems[item_id] = item;
    }
    qaitems[item_id].order = new Date().getTime();

    if (site.settings.product_variant_per_serial == 1 && modal === false && item.row.attributes == 1) {
        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(qaitems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal').modal('show');
        localStorage.setItem('modalSerial_item', JSON.stringify(qaitems[item_id]));
        delete qaitems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
        }, 850);
    } else {
        localStorage.removeItem('modalSerial_item');
        if (site.settings.product_variant_per_serial == 1 && cont == false) {
            $('#serialModal').modal('hide');
            $('#add_item').val('').focus();
        } else {
            item = qaitems[item_id];
            $.ajax({
                url : site.base_url + "products/get_random_id"
            }).done(function(data){
                item.id = data;
                localStorage.setItem('modalSerial_item', JSON.stringify(item));
                $('#serialModal_serial').val('');
                $('#serialModal_meters').val('');
            });
        }

        if ($('#adjustmet_type').val() == 1) {
            qaitems[item_id].row.type = 'addition';
        } else if ($('#adjustmet_type').val() == 2) {
            qaitems[item_id].row.type = 'subtraction';
        }

        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
        setTimeout(function() {
            if (site.pos_settings.show_variants_and_preferences == 1) {
                if (qaitems[item_id].row.variant_selected === undefined) {
                    $('#row_'+item_id).find('.sname').trigger('click');
                }
            }
        }, 1500);
        return true;
    }

}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}


$(document).on('change', '#companies_id', function(){
    localStorage.setItem('qa_companies_id', $(this).val());
});

$(document).on('click', '.send_serial_modal', function(){
    validar_modal_serial();
});

$(document).on('click', '.continue_serial_modal', function(){
    validar_modal_serial(true);
});


function validar_modal_serial(cont = false){
    if ($('#serialModal_form').valid()) {
        serial = $('#serialModal_serial').val();
        meters = $('#serialModal_meters').val();
        item_id = $('#serialModal_product_id').val();
        item = JSON.parse(localStorage.getItem('modalSerial_item'));
        item.row.serialModal_serial = serial;
        item.row.qty = meters;
        item.row.type = 'addition';
        if ($('#adjustmet_type').val() == 2) {
            option_id = $('#serialModal_option_id').val();
            item.row.serial_subtraction_option_id = option_id;
        }
        add_adjustment_item(item, true, cont);
    }
}

$(document).on('keyup', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal, e);
});
$(document).on('change', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal);
});
$(document).on('keyup', '#serialModal_meters', function(e){
    if (e.keyCode == 13) {
        $('.send_serial_modal').trigger('click');
    }
});

function validate_serial(serialModal, e = null){

    if ( e == null || e != null && e.keyCode == 13) {
        qaitems = JSON.parse(localStorage.getItem('qaitems'));
        if (qaitems) {
            $.each(qaitems, function(index, item){
                if (item.row.serialModal_serial == $('#serialModal_serial').val()) {
                    setTimeout(function() {
                        command: toastr.warning('El serial ingresado ya se registró en este formulario.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                        $('#serialModal_serial').val('').focus();
                        }, 800);
                    return false;
                }
            });
        }
        serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
        if ($('#adjustmet_type').val() == 1) {
            $.ajax({
                url : site.base_url + 'products/get_product_serial_variant/'+serial_item.row.id+'/'+serialModal
            }).done(function(data){
                if (data == '1') {
                    if (!localStorage.getItem('validated_serial')) {
                        localStorage.setItem('validated_serial', true);
                        bootbox.confirm('El serial ingresado ya existe para este producto. ¿Desea registrar más cantidades?', function (result) {
                            if (result) {
                                setTimeout(function() {
                                    $('#serialModal').modal('show');
                                }, 400);
                                setTimeout(function() {
                                    $('#serialModal_meters').focus();
                                    localStorage.removeItem('validated_serial');
                                }, 1000);
                            } else {
                                setTimeout(function() {
                                    $('#serialModal').modal('show');
                                }, 400);
                                setTimeout(function() {
                                    $('#serialModal_serial').val('').focus();
                                    localStorage.removeItem('validated_serial');
                                }, 1000);
                            }
                        });
                    }
                } else {
                    $('#serialModal_meters').focus();
                }
            });
        } else if ($('#adjustmet_type').val() == 2) {
            qawarehouse = $('#qawarehouse').val();
            $.ajax({
                url : site.base_url + 'products/get_product_serial_variant_quantity/'+serial_item.row.id+'/'+serialModal+'/'+qawarehouse,
                dataType : 'JSON',
            }).done(function(data){
                if (data.id  !== undefined) {
                    $('#serialModal_meters').val(data.quantity);
                    $('#serialModal_meters').prop('max', data.quantity);
                    $('#serialModal_option_id').val(data.option_id);
                    $('#serialModal_meters').focus();
                } else {
                    $('#serialModal_serial').val('');
                    $('#serialModal_option_id').val('');
                     command: toastr.warning('El serial indicado no está registrado para este producto', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                }
            });
        }
            
    }


}

$(document).on('click', '#random_num', function(e){
    $('#serialModal_serial').select();
});

$(document).on('click', '.sname', function(e) {
    var row = $(this).closest('tr');
    var qaitems_id = $(row).data('item-id');
    var itemid = row.find('.rid').val();

    if (site.settings.product_clic_action == 1) {
        $('#myModal').modal({remote: site.base_url + 'products/modal_view/' + itemid});
        $('#myModal').modal('show');
    } else if (site.settings.product_clic_action == 2) {
        // if (qaitems[qaitems_id].options && qaitems[qaitems_id].row.variant_selected === undefined) {
        if (qaitems[qaitems_id].options) {
            $('#myModal').modal({remote: site.base_url + 'products/adjustment_product_variants_selection/' + itemid + "/" + qaitems_id + "/"+$('#qawarehouse').val()});
            $('#myModal').modal('show');
        }
    } else if (site.settings.product_clic_action == 3) {
        row.find('.edit').trigger('click');
    }

});