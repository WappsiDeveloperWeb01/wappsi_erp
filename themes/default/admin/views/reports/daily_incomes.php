<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open("reports/getDayliIncomesReport", 'id="form_filter"'); ?>
                            <div class="row">
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <?= lang("biller", "slbiller"); ?>
                                            <select name="biller" class="form-control" id="slbiller" required="required">
                                                <?php if ($billers && count($billers) > 1): ?>
                                                    <option value=""><?= lang('select') ?></option>
                                                <?php endif ?>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" <?= (isset($_POST['biller']) && $_POST['biller'] == $biller->id ? "selected" : "") ?>><?= $biller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <?php
                                        $bl[""] = "";
                                        $bldata = [];
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                            $bldata[$biller->id] = $biller;
                                        }
                                    ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <?= lang("biller", "slbiller"); ?>
                                            <select name="biller" class="form-control" id="slbiller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                ?>
                                                    <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                        <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'class="form-control" id="customer" data-placeholder="' . $this->lang->line("select"). '"'); ?>
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label class="control-label" for="seller"><?= lang("seller"); ?></label>
                                    <?php
                                    $user_seller = false;
                                    $sl = [];
                                    if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
                                        if ($this->session->userdata('company_id')) {
                                            $user_seller = $this->session->userdata('company_id');
                                        }
                                    } else {
                                        $sl[""] = lang('select');
                                    }
                                    foreach ($sellers as $seller) {
                                        if ($user_seller && $user_seller == $seller->id || !$user_seller) {
                                            $sl[$seller->id] = $seller->company != '-' ? $seller->company : $seller->name;
                                        }
                                    }
                                    echo form_dropdown('seller', $sl, (isset($_POST['seller']) ? $_POST['seller'] : $user_seller), 'class="form-control" id="seller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("seller") . '" '.($user_seller ? 'readonly' : ''));
                                    ?>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                        <?php
                                        $us[""] = lang('select');
                                        foreach ($users as $user) {
                                            $us[$user->id] = $user->first_name . " " . $user->last_name;
                                        }
                                        echo form_dropdown('user', $us, (isset($_POST['user']) ? $_POST['user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>
                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                        <div class="col-sm-4 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= lang("start_date", "start_date_dh"); ?>
                                            <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= lang("end_date", "end_date_dh"); ?>
                                            <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <button class="btn btn-primary new-button" id="submit_filter" type="button" title="<?= lang('search') ?>">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        <?php if ($this->session->userdata('biller_id')): ?>
            setTimeout(function() {
                $('#slbiller').select2('val', '<?= $this->session->userdata('biller_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
        $('#form').hide();
        <?php if ($this->input->post('customer')) { ?>
        $('#customer').val(<?= $this->input->post('customer') ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/suggestions/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#customer').val(<?= $this->input->post('customer') ?>);
        <?php } ?>
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
        $(document).on('click', '#submit_filter', function (event) {
            if ($('#form_filter').valid()) {
                $('#form_filter').submit();
            }
        });
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '20%'
        });
        $("#form_filter").validate({
            ignore: []
        });
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
        <?php if ($this->Settings->big_data_limit_reports == 1): ?>
            $('#biller').prop('required', true);
        <?php endif ?>
        $(document).on('change', '#start_date_dh, #end_date_dh, #date_records_filter_dh', function(){
            change_biller_required();
        });
    });

    function change_biller_required()
    {
        var fechaInicioStr = $('#start_date_dh').val();
        var fechaFinStr = $('#end_date_dh').val();
        var fechaInicio = parsearFecha(fechaInicioStr);
        var fechaFin = parsearFecha(fechaFinStr);
        if (fechaInicio && fechaFin) {
            var diferencia = fechaFin - fechaInicio;
            var diasDiferencia = Math.floor(diferencia / (1000 * 60 * 60 * 24));
            if (diasDiferencia > 31) {
                $('#slbiller').prop('required', true);
            } else {
                $('#slbiller').prop('required', false);
            }
        }
    }

    function parsearFecha(fechaStr)
    {
        var partes = fechaStr.split(' ');
        var fechaPartes = partes[0].split('/');
        var horaPartes = partes[1].split(':');
        if (fechaPartes.length === 3 && horaPartes.length === 2) {
            var dia = parseInt(fechaPartes[0]);
            var mes = parseInt(fechaPartes[1]) - 1; // Meses en JavaScript comienzan desde 0
            var anio = parseInt(fechaPartes[2]);
            var hora = parseInt(horaPartes[0]);
            var minutos = parseInt(horaPartes[1]);
            return new Date(anio, mes, dia, hora, minutos);
        } else {
            return null;
        }
    }
</script>