<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php
                    $attrib = array('id' => 'new_form');
                    echo admin_form_open_multipart("products/sequentialCount", $attrib);
                    ?>
                        <div class="row">
                            <?php if ($Owner || $Admin || !$this->session->userdata('biller')) { ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("biller", "biller"); ?>
                                        <?php
                                        $wh[''] = '';
                                        foreach ($billers as $biller) {
                                            $wh[$biller->id] = $biller->name;
                                        }
                                        echo form_dropdown('biller', $wh, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="biller" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" style="width:100%;" ');
                                        ?>
                                    </div>
                                </div>
                            <?php } else {
                                $biller_input = array(
                                    'type' => 'hidden',
                                    'name' => 'biller',
                                    'id' => 'biller',
                                    'value' => $this->session->userdata('biller_id'),
                                    );

                                echo form_input($biller_input);
                            } ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("reference_no", "document_type_id"); ?>
                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                </div>
                            </div>
                            <?php if ($Owner || $Admin) { ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("date", "ectdate"); ?>
                                        <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="ectdate" required="required"'); ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("warehouse", "ectwarehouse"); ?>
                                        <?php
                                        $wh[''] = '';
                                        unset($wh);
                                        foreach ($warehouses as $warehouse) {
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                        echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ($warehouse_id ? $warehouse_id :$Settings->default_warehouse)), 'id="ectwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" '.($warehouse_id ? 'readonly' : '').' style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            <?php } else {
                                $warehouse_input = array(
                                    'type' => 'hidden',
                                    'name' => 'warehouse',
                                    'id' => 'ectwarehouse',
                                    'value' => $this->session->userdata('warehouse_id'),
                                    );

                                echo form_input($warehouse_input);
                            } ?>
                            <div class="col-md-4 form-group">
                                <?= lang('third', 'company_id') ?>
                                 <?php
                                    $cOptions[''] = lang('select_third');
                                    foreach ($companies as $row => $company) {
                                        $cOptions[$company->id] = $company->name;
                                    }

                                     ?>
                                <?= form_dropdown('company_id', $cOptions, '',' id="ectcompany" class="form-control select" required style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="category"><?= lang('category') ?></label>
                                <?php 
                                    $caOptions[''] = lang('select');
                                    foreach ($categories as $row => $category) {
                                        $caOptions[$category->id] = $category->name;
                                    }
                                 ?>
                                <?= form_dropdown('category_id', $caOptions, '',' id="ectcategory" class="form-control select" style="width:100%;"'); ?>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("brands", "brand"); ?>
                                    <?php
                                    $brandsOpts[''] = lang('select');
                                    foreach ($brands as $brand) {
                                        $brandsOpts[$brand->id] = $brand->name;
                                    }
                                    echo form_dropdown('brand[]', $brandsOpts, (isset($_POST['brand']) ? $_POST['brand'] : ''), 'id="ectbrand" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("brand") . '" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row div_products" style="display:none;">
                            <div class="col-md-12 alert alert-danger" role="alert"><?= lang('express_count_warning') ?></div>
                            <div class="col-md-4 form-group">
                                <label>
                                    <?= lang('show_only_affected') ?> 
                                    <br>
                                    <input type="checkbox" name="ectaffected" id="ectaffected">
                                </label>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group table-group">
                                    <label class="table-label"><?= lang("products"); ?> *</label>
                                    <div class="controls table-controls">
                                        <table id="ectTable" class="table items  table-bordered table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th style="width:70% !important;"><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                <th style="width:30% !important;" class="col-md-1"><?= lang("quantity"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 div_new_count" style="display:none;">
                                <div class="panel panel-success">
                                  <div class="panel-heading"><?= lang('new_express_count_header_title') ?></div>
                                  <div class="panel-body">
                                    <?= lang('new_express_count_body_info') ?>
                                    <br>
                                    <br>
                                    <button class="btn btn-success" id="start_express_count" type="button"><?= lang('start_express_count') ?></button>
                                  </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                    <button type="button" class="btn btn-primary div_products" id="sendSC"  style="display:none;"><?= lang('submit') ?></button>
                                    <button type="button" class="btn btn-danger div_products" id="resetSC" onclick="reset_local()" style="display:none;"><?= lang('delete') ?></button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    <?php if (isset($count_id)): ?>
        clear_local_storage();
        localStorage.setItem('ectcount_id', "<?= $count_id ?>");
        localStorage.setItem('ectdate', "<?= $stock_count->date ?>");
        localStorage.setItem('ectbiller', "<?= $stock_count->biller_id ?>");
        localStorage.setItem('ectwarehouse', "<?= $stock_count->warehouse_id ?>");
        localStorage.setItem('ectcompany', "<?= $stock_count->company_id ?>");
        localStorage.setItem('ectdocument_type', "<?= $stock_count->document_type_id ?>");
        localStorage.setItem('ectcategory', "<?= $stock_count->category_id ?>");
        localStorage.setItem('ectbrand', "<?= $stock_count->brand_id ?>");
        localStorage.setItem('ectquantity_changed', 1);
    <?php endif ?>
    $(document).ready(function () {
        $("#new_form").validate({
              ignore: []
          });
        <?php if ($Owner || $Admin) { ?>
            if (!localStorage.getItem('ectdate')) {
                $("#ectdate").datetimepicker({
                    format: site.dateFormats.js_ldate,
                    fontAwesome: true,
                    language: 'sma',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0
                }).datetimepicker('update', new Date());
            }
            $(document).on('change', '#ectdate', function (e) {
                localStorage.setItem('ectdate', $(this).val());
            });
            if (ectdate = localStorage.getItem('ectdate')) {
                $('#ectdate').val(ectdate);
            }
        <?php } ?>
        setTimeout(function() {
            if (ectbiller = localStorage.getItem('ectbiller')) {
                $('#biller').select2('val', ectbiller).trigger('change');
            } else {
                $('#biller').trigger('change');
            }
            if (ectdate = localStorage.getItem('ectdate')) {
                $('#ectdate').val(ectdate);
            }
            if (ectcompany = localStorage.getItem('ectcompany')) {
                $('#ectcompany').select2('val', ectcompany);
            }
            if (ectcategory = localStorage.getItem('ectcategory')) {
                $('#ectcategory').select2('val', ectcategory);
            }
            if (ectbrand = localStorage.getItem('ectbrand')) {
                $('#ectbrand').select2('val', ectbrand);
            }
        }, 850);

        setTimeout(function() {
            if (localStorage.getItem('ectcount_id')) {
                $('#start_express_count').trigger('click');
            } else {
                $('.div_products').fadeOut();
                $('.div_new_count').fadeIn();
            }
        }, 1600);
    });


    $(document).on('change', '#biller', function (e) {
        if ($('#biller').val()) {
            localStorage.setItem('ectbiller', $('#biller').val());
        }
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/64/") ?>'+$('#biller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;
          $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
          if (response.status == 0) {
            $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
          }
          $('#document_type_id').trigger('change');
          setTimeout(function() {
            if (ectwarehouse = localStorage.getItem('ectwarehouse')) {
                $('#ectwarehouse').select2('val', ectwarehouse);
            }
            if (ectdocument_type = localStorage.getItem('ectdocument_type')) {
                $('#document_type_id').select2('val', ectdocument_type);
            }
          }, 850);
        });
    });
    $(document).on('change', '#ectdate', function (e) {
        localStorage.setItem('ectdate', $('#ectdate').val());
    });
    $(document).on('change', '#ectwarehouse', function (e) {
        localStorage.setItem('ectwarehouse', $('#ectwarehouse').val());
    });
    $(document).on('change', '#ectcompany', function (e) {
        localStorage.setItem('ectcompany', $('#ectcompany').val());
    });
    $(document).on('change', '#document_type_id', function (e) {
        localStorage.setItem('ectdocument_type', $('#document_type_id').val());
    });
    $(document).on('change', '#ectcategory', function (e) {
        localStorage.setItem('ectcategory', $('#ectcategory').val());
        if (localStorage.getItem('ectcount_id')) {
            $('#start_express_count').trigger('click');
        }
    });
    $(document).on('change', '#ectbrand', function (e) {
        localStorage.setItem('ectbrand', $('#ectbrand').val());
        if (localStorage.getItem('ectcount_id')) {
            $('#start_express_count').trigger('click');
        }
    });
    $(document).on('ifChecked', '#ectaffected', function (e) {
        localStorage.setItem('ectaffected', $('#ectaffected').is(':checked'));
        $('#start_express_count').trigger('click');
    });
    $(document).on('ifUnchecked', '#ectaffected', function (e) {
        localStorage.setItem('ectaffected', $('#ectaffected').is(':checked'));
        $('#start_express_count').trigger('click');
    });


    $(document).on('click', '#start_express_count', function(){
        $('#loading').fadeIn();
        if ($('#new_form').valid()) {
            set_table_data();
        } else {
            $('#loading').fadeOut();
        }
    });

    var ajax = 0;
    var table;
    function set_table_data(focus = false) {
        if (ajax == 1) {
            table.destroy();
        } else {
            ajax = 1;
        }
        count_id = localStorage.getItem('ectcount_id');
        $.ajax({
            url : site.base_url+"products/get_products_express_count"+(count_id ? "/"+count_id : ""),
            type : "get",
            dataType: "json",
            async: false,
            data : {
                "biller_id":$('#biller').val(),
                "warehouse_id":$('#ectwarehouse').val(),
                "company":$('#ectcompany').val(),
                "document_type":$('#document_type_id').val(),
                "date":$('#ectdate').val(),
                "category_id":$('#ectcategory').val(),
                "brand_id":$('#ectbrand').val(),
                "show_only_affected":$('#ectaffected').is(':checked'),
            }
        }).done(function(data){
            if (data.status) {
                $('#biller').select2('readonly', true);
                $('#ectcompany').select2('readonly', true);
                $('#ectwarehouse').select2('readonly', true);
                $('#document_type_id').select2('readonly', true);
                $('#ectdate').prop('disabled', true);
                if (!count_id) {
                    localStorage.setItem('ectcount_id', data.count_id);
                }
                $('#ectTable tbody').empty().html(data.html);
                if (localStorage.getItem('ectquantity_changed')) {
                    $('#ectcategory').select2('readonly', true);
                    $('#ectbrand').select2('readonly', true);
                }
                setTimeout(function() {
                    $('.div_products').fadeIn();
                    $('.div_new_count').fadeOut();
                }, 850);
                if (data.status_message != "") {
                    header_alert('success', data.status_message);
                }
            } else {
                header_alert('error', data.status_message);
            }
            $('#loading').fadeOut();
        }).fail(function(data){
            alert(data);
        });
        table = $('#ectTable').DataTable({
            aaSorting: false,
            processing: false,
            pageLength: 100,
            responsive: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            oLanguage: <?php echo $dt_lang; ?>
        });
        if (focus) {
            // setTimeout(function() {
                // alert(" focus >> "+focus);
                $('input.ectquantity[tabindex='+focus+']').select();
            // }, 850);
        }
    }

    $(document).on('click', '.ectquantity', function(){
        $(this).select();
    }).on('keypress', function(e){
        if (e.keyCode == 13) {
            $(this).trigger('change');
        }
    });
    $(document).on('change', '.ectquantity', function(){
        $('.ectquantity').prop('readonly', true);
        scid = $(this).data('scid');
        product_id = $(this).data('productid');
        quantity = $(this).val();
        count_id = localStorage.getItem('ectcount_id');
        tabindex = $(this).prop('tabindex');
        $.ajax({
            url : site.base_url+"products/update_express_count/"+count_id,
            type : "get",
            dataType: "json",
            async: false,
            data : {
                "product_id":product_id,
                "count_item_id":scid,
                "quantity":quantity,
                "warehouse_id":$('#ectwarehouse').val(),
            }
        }).done(function(data){
            localStorage.setItem('ectquantity_changed', 1);
            if (data.status) {
                $('#start_express_count').trigger('click');
                set_table_data(tabindex+1);
            }
        });
    });

    function reset_local(){
        Swal.fire({
          title: "¿Está seguro de la acción?",
          text: "Se borrará el conteo y los datos diligenciados",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#d33",
          cancelButtonColor: "#bdbdbd",
          confirmButtonText: "Si, eliminarlo",
          cancelButtonText: "No, cancelar"
        }).then((result) => {
          if (result.isConfirmed) {
            count_id = localStorage.getItem('ectcount_id');
            $.ajax({
                url : site.base_url+"products/delete_express_count/"+count_id,
                type : "get",
                dataType : "json"
            }).done(function(data){
                if (data.status) {
                    clear_local_storage();
                    location.href = site.base_url+"products/add_express_count";
                }
            });     
          }
        });
    }

    function clear_local_storage(){
        if (localStorage.getItem('ectdate')) {
            localStorage.removeItem('ectdate');
        }
        if (localStorage.getItem('ectbiller')) {
            localStorage.removeItem('ectbiller');
        }
        if (localStorage.getItem('ectwarehouse')) {
            localStorage.removeItem('ectwarehouse');
        }
        if (localStorage.getItem('ectcompany')) {
            localStorage.removeItem('ectcompany');
        }
        if (localStorage.getItem('ectdocument_type')) {
            localStorage.removeItem('ectdocument_type');
        }
        if (localStorage.getItem('ectcategory')) {
            localStorage.removeItem('ectcategory');
        }
        if (localStorage.getItem('ectbrand')) {
            localStorage.removeItem('ectbrand');
        }
        if (localStorage.getItem('ectaffected')) {
            localStorage.removeItem('ectaffected');
        }
        if (localStorage.getItem('ectcount_id')) {
            localStorage.removeItem('ectcount_id');
        }
        if (localStorage.getItem('ectquantity_changed')) {
            localStorage.removeItem('ectquantity_changed');
        }
    }

    $(document).on('click', '#sendSC', function(){
        Swal.fire({
          title: "¿Está seguro de la acción?",
          text: "Se finalizará el conteo y se ajustarán las cantidades",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#bdbdbd",
          confirmButtonText: "Si, finalizar",
          cancelButtonText: "No, cancelar"
        }).then((result) => {
          if (result.isConfirmed) {
            count_id = localStorage.getItem('ectcount_id');
            $.ajax({
                url : site.base_url+"products/finish_express_count/"+count_id,
                type : "get",
                dataType : "json"
            }).done(function(data){
                if (data.status) {
                    Swal.fire({
                      title: "Conteo finalizado",
                      text: "Se ha finalizado el conteo y se ajustaron las cantidades",
                      icon: "success"
                    });
                    setTimeout(function() {
                        window.open(site.base_url+'products/print_express_count/'+count_id);
                        clear_local_storage();
                        location.href = site.base_url+"products/stock_counts";
                    }, 1200);
                } else {
                    header_alert('error', 'Error al finalizar, asegúrese de haber modificado al menos un producto o contáctese con soporte.')
                }
            });     
          }
        });
    });
</script>