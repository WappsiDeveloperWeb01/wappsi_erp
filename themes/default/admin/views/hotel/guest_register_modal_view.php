<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
@media print {
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 3px !important;
    }
}
</style>

<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <div class="col-xs-12">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-2x">&times;</i>
                </button>
                <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="imprimir_factura_mayorista();">
                    <i class="fa fa-print"></i> <?= lang('print'); ?>
                </button>
            </div>
            <div id="contenido_factura">
                <div class="row bold">
                    <div class="col-xs-6">
                    <h4 class="bold"><?= lang("reference_no"); ?>: <?= $inv->reference_no; ?></h4>
                    <h4><?= lang("date_in"); ?>: <?= $this->sma->hrld($inv->date_in); ?></h4>
                    <h4><?= lang("date_out"); ?>: <?= $this->sma->hrld($inv->date_out); ?></h4>
                    <h4><?= lang("estimated_date_out"); ?>: <?= $this->sma->hrld($inv->estimated_date_out); ?></h4>
                    <h4><?= lang("nights_no"); ?>: <?= $inv->nights_no; ?></h4>
                    <h4><?= lang("guests_no"); ?>: <?= $inv->guests_no; ?></h4>
                    <h4><?= lang("room"); ?>: <?= $inv->room; ?></h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>

            <div class="row" style="margin-bottom:15px;">
                    <div class="col-xs-12 text-center">
                    	<h1><?= lang('guests_register') ?></h1>
                    </div>

                <div class="col-xs-6">
                    <?php echo $this->lang->line("to"); ?>:<br/>
                    <h3 style="margin-top:10px;"><?= $customer->company ? $customer->company : $customer->name; ?></h3>
                    <?= $customer->company ? "" : "Attn: " . $customer->name ?>
                    <?php
                    if ($customer->vat_no != "-" && $customer->vat_no != "") {
                        if ($customer->tipo_documento == 6) {
                            $vatNo =  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion;
                        } else {
                            $vatNo =  lang("vat_no") . ": " . $customer->vat_no;
                        }
                    }
                     ?>
                    <?= $vatNo ?><br>
                    <span><?= $customer->address ?><br>
                    <?= $customer->postal_code." - ".$customer->city.", ".$customer->state ?><br>
                    <?= $customer->country ?><br>
                    <?= $customer->phone ?><br>
                    <?= $customer->email ?><br>
                    <?php
                    if ($customer->cf1 != "-" && $customer->cf1 != "") {
                        echo "<br>" . lang("ccf1") . ": " . $customer->cf1;
                    }
                    if ($customer->cf2 != "-" && $customer->cf2 != "") {
                        echo "<br>" . lang("ccf2") . ": " . $customer->cf2;
                    }
                    if ($customer->cf3 != "-" && $customer->cf3 != "") {
                        echo "<br>" . lang("ccf3") . ": " . $customer->cf3;
                    }
                    if ($customer->cf4 != "-" && $customer->cf4 != "") {
                        echo "<br>" . lang("ccf4") . ": " . $customer->cf4;
                    }
                    if ($customer->cf5 != "-" && $customer->cf5 != "") {
                        echo "<br>" . lang("ccf5") . ": " . $customer->cf5;
                    }
                    if ($customer->cf6 != "-" && $customer->cf6 != "") {
                        echo "<br>" . lang("ccf6") . ": " . $customer->cf6;
                    }

                    ?>
                </div>

                <div class="col-xs-6">
                    <?php echo $this->lang->line("from"); ?>:
                    <h3 style="margin-top:10px;"><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h3>
                    <span>
                        <?= $biller->address ?><br>
                        <?= $biller->postal_code." - ".$biller->city.", ".$biller->state ?><br>
                        <?= $biller->country ?><br>
                        <?php
                        echo "NIT : ".$this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '');
                         ?><br>
                        <?= $biller->phone ?><br>
                        <?= $biller->email ?><br>
                    </span>

                    <?php

                    if ($biller->cf1 != "-" && $biller->cf1 != "") {
                        echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                    }
                    if ($biller->cf2 != "-" && $biller->cf2 != "") {
                        echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                    }
                    if ($biller->cf3 != "-" && $biller->cf3 != "") {
                        echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                    }
                    if ($biller->cf4 != "-" && $biller->cf4 != "") {
                        echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                    }
                    if ($biller->cf5 != "-" && $biller->cf5 != "") {
                        echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                    }
                    if ($biller->cf6 != "-" && $biller->cf6 != "") {
                        echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                    }
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
	            <div class="col-xs-12 text-center">
	            	<h3><?= lang('guests') ?></h3>
	            </div>
            </div>
            <div class="row">
                <div class="table-responsive" style="min-height: 0px !important;">
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                         <tr>
                            <th><?= lang('no'); ?></th>
                            <th><?= lang('vat_no'); ?></th>
                            <th><?= lang('names'); ?></th>
                            <th><?= lang('surnames'); ?></th>
                            <th><?= lang('nationality'); ?></th>
                            <th><?= lang('phone'); ?></th>
                            <th><?= lang('email'); ?></th>
                            <th><?= lang('birth_date'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
	                        <?php $r = 1;
	                        foreach ($rows as $row):
	                        ?>
	                            <tr>
	                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
	                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $row->vat_no; ?></td>
	                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $row->names; ?></td>
	                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $row->surnames; ?></td>
	                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $row->nationality; ?></td>
	                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $row->phone; ?></td>
	                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $row->email; ?></td>
	                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= date('Y-m-d', strtotime($row->birth_date)); ?></td>
	                            </tr>
	                            <?php
	                            $r++;
	                        endforeach;
	                        ?>
                        </tbody>
                        <tfoot>
	                        <?php
	                        $col = 7;
	                        ?>
	                        <tr>
	                            <td colspan="<?= $col; ?>"
	                                style="text-align:right; font-weight:bold;"><?= lang("total"); ?>
	                            </td>
	                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->total); ?></td>
	                        </tr>

                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 pull-left">
                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p style="border-bottom: 1px solid #666;">&nbsp;</p>

                    <p><?= lang("stamp_sign"); ?></p>
                </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="col-xs-12 text-center">
                <p>
                    <?= lang("created_by"); ?>: <?= $inv->created_by ? $created_by->first_name . ' ' . $created_by->last_name : $customer->name; ?> <br>
                    <?= lang("date"); ?>: <?= $this->sma->hrld($inv->registration_date); ?>
                </p>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    function imprimir_factura_mayorista()
    {
        window.open('<?= admin_url("hotel/register_view/").$inv->id ?>');
    }
</script>