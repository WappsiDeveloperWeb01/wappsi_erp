<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="box">
    <style type="text/css">
        .deleted {
            filter: hue-rotate(213deg) brightness(40%);
        }
    </style>

    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <?= admin_form_open_multipart("shop_settings/slider", array('data-toggle' => 'validator', 'role' => 'form')); ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $sliders = [
                            0 => 1,
                            1 => 1,
                            2 => 1,
                            3 => 1,
                            4 => 1,
                        ];

                        ?>
                        <?php if (count((array)$slider_settings) > 0) : ?>
                            <?php foreach ($slider_settings as $key => $ssetting) :
                                unset($sliders[$key]); ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang('link', 'link'); ?> <?= $key + 1 ?>
                                            <?= form_input('link[' . $key . ']', $ssetting->link, 'class="form-control tip"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang('caption', 'caption'); ?> <?= $key + 1 ?>
                                            <?= form_input('caption[' . $key . ']', $ssetting->caption, 'class="form-control tip"'); ?>
                                        </div>
                                    </div>
                                    <input type="hidden" name="slider_id[<?= $key ?>]" value="<?= $key ?>">
                                    <div class="col-md-12 text-center" style="padding:1% 1% 5% 1%;">
                                        <?php if (isset($ssetting->image)) : ?>
                                            <img src="<?= base_url() . 'assets/uploads/' . $ssetting->image ?>" style="width: 60%;" class="image">

                                        <?php endif ?>
                                        <button data-ssettingid="<?= $key ?>" type="button" class="btn btn-danger btn-outline delete_image"><i class="fa fa-times"></i></button>
                                        <input type="checkbox" class="checkbox_deleted skip" name="deleted[]" value="<?= $key ?>" style="display: none;">
                                    </div>
                                    <hr class="col-md-11">
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>
                        <?php foreach ($sliders as $slider_key => $sl_setted) : ?>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang('link', 'link'); ?> <?= $slider_key + 1 ?>
                                        <?= form_input('link[' . $slider_key . ']', '', 'class="form-control tip"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('caption', 'caption'); ?> <?= $slider_key + 1 ?>
                                        <?= form_input('caption[' . $slider_key . ']', '', 'class="form-control tip"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang('image', 'image'); ?> <?= $slider_key + 1 ?>
                                        <input type="file" data-browse-label="<?= lang('browse'); ?>" name="image[<?= $slider_key ?>]" data-show-upload="false" data-show-preview="false" class="form-control file">
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>

                        <?= form_submit('update', lang('update'), 'class="btn btn-primary"'); ?>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.delete_image', function() {

        index = $('.delete_image').index($(this));
        image = $('.image').eq(index);
        if (image.hasClass('deleted')) {
            image.removeClass('deleted');
            $('.delete_image').eq(index).addClass('btn-outline');
            $('.checkbox_deleted').eq(index).prop('checked', false);
        } else {
            $('.delete_image').eq(index).removeClass('btn-outline');
            $('.checkbox_deleted').eq(index).prop('checked', true);
            image.addClass('deleted');
        }

    });
</script>