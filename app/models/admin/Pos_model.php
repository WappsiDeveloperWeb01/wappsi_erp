<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Pos_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function getSetting()
    {
        $q = $this->db->get('pos_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateSetting($data)
    {
        // ******************************************************** START PROCEDURE STRORE CHANGES IN POS SETTINGS **************************************************
        foreach ($data as $key => $value) {
            if ($key == 'default_section') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.default_section = 999,"' .lang('favorites'). '", IF('.$this->db->dbprefix('pos_settings').'.default_section = 998, "' .lang('promotions'). '",  (SELECT NAME FROM '.$this->db->dbprefix('categories').' WHERE  id = '.$this->db->dbprefix('pos_settings').'.default_section))) AS default_section');
            }if ($key == 'default_customer') {
                $this->db->select('(select NAME from '.$this->db->dbprefix('companies'). ' where id = ' . $this->db->dbprefix('pos_settings').'.default_customer) AS default_customer');
            }if ($key == 'default_biller'){
                $this->db->select('(select NAME from '.$this->db->dbprefix('companies'). ' where id = ' . $this->db->dbprefix('pos_settings').'.default_biller) AS default_biller');
            }if($key == 'display_time'){
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.display_time = 0, "' .lang('no'). '", "' .lang('yes'). '") AS display_time');
            }if($key == 'keyboard'){
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.keyboard = 0, "' .lang('no'). '", "' .lang('yes'). '") AS keyboard');
            }if ($key == 'product_button_color') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'default' THEN '" .lang('default'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'primary' THEN '" .lang('primary')."'
                                        WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'info' THEN '" .lang('info') ."'
                                        WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'warning' THEN '" .lang('warning') ."'
                                        WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'danger' THEN '" .lang('danger') ."'
                                        ELSE {$this->db->dbprefix('pos_settings')}.product_button_color END AS 'product_button_color'  ");
            }if ($key == 'tooltips') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.tooltips = 0, "' .lang('no'). '", "' .lang('yes'). '") AS tooltips');
            }if ($key == 'paypal_pro') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.paypal_pro = 0, "' .lang('no'). '", "' .lang('yes'). '") AS paypal_pro');
            }if ($key == 'stripe') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.stripe = 0, "' .lang('no'). '", "' .lang('yes'). '") AS stripe');
            }if ($key == 'rounding') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.rounding = 0 THEN '" .lang('disable'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.rounding = 1 THEN '" .lang('to_nearest_005'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.rounding = 2 THEN '" .lang('to_nearest_050'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.rounding = 3 THEN '" .lang('to_nearest_number'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.rounding = 4 THEN '" .lang('to_next_number'). "'
                                        ELSE {$this->db->dbprefix('pos_settings')}.rounding END AS 'rounding'  ");
            }if ($key == 'after_sale_page') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.after_sale_page = 0, "' .lang('receipt'). '", "' .lang('pos'). '") AS after_sale_page');
            }if ($key == 'item_order') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.item_order = 0 THEN '" .lang('default'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.item_order = 1 THEN '" .lang('category'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.item_order = 2 THEN '" .lang('brands'). "'
                                        ELSE {$this->db->dbprefix('pos_settings')}.item_order END AS 'item_order' ");
            }if ($key == 'authorize') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.authorize = 0, "' .lang('no'). '", "' .lang('yes'). '") AS authorize');
            }if ($key == 'remote_printing') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.remote_printing = 1 THEN '" .lang('web_browser_print'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.remote_printing = 4 THEN '" .lang('direct_printing_with_google_cloud_print'). "'
                                        ELSE {$this->db->dbprefix('pos_settings')}.remote_printing END AS 'remote_printing'  ");
            }if ($key == 'auto_print') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.auto_print = 0, "' .lang('no'). '", "' .lang('yes'). '") AS auto_print');
            }if ($key == 'customer_details') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.customer_details = 0, "' .lang('no'). '", "' .lang('yes'). '") AS customer_details');
            }if ($key == 'local_printers') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.local_printers = 0, "' .lang('no'). '", "' .lang('yes'). '") AS local_printers');
            }if ($key == 'balance_settings') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.balance_settings = 0 THEN '" .lang('no_balance'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.balance_settings = 1 THEN '" .lang('balance_no_label'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.balance_settings = 2 THEN '" .lang('balance_label'). "'
                                        ELSE {$this->db->dbprefix('pos_settings')}.balance_settings END AS 'balance_settings'  ");
            }if ($key == 'restobar_mode') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.restobar_mode = 0, "' .lang('inactive'). '", "' .lang('active'). '") AS restobar_mode');
            }if ($key == 'table_service') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.table_service = 0, "' .lang('inactive'). '", "' .lang('active'). '") AS table_service');
            }if ($key == 'order_print_mode') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.order_print_mode = 0 THEN '" .lang('to_screen'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.order_print_mode = 1 THEN '" .lang('to_printer'). "'
                                        WHEN {$this->db->dbprefix('pos_settings')}.order_print_mode = 2 THEN '" .lang('to_preparation_area'). "'
                                        ELSE {$this->db->dbprefix('pos_settings')}.order_print_mode END AS 'order_print_mode'  ");
            }if ($key == 'home_delivery_in_invoice') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.home_delivery_in_invoice = 0, "' .lang('no'). '", "' .lang('yes'). '") AS home_delivery_in_invoice');
            }if ($key == 'detail_sale_by_category') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.detail_sale_by_category = 0, "' .lang('no'). '", "' .lang('yes'). '") AS detail_sale_by_category');
            }if ($key == 'show_suspended_bills_automatically') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.show_suspended_bills_automatically = 0, "' .lang('no'). '", "' .lang('yes'). '") AS show_suspended_bills_automatically');
            }if ($key == 'print_voucher_delivery') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.print_voucher_delivery = 0, "' .lang('no'). '", "' .lang('yes'). '") AS print_voucher_delivery');
            }if ($key == 'allow_print_command') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.allow_print_command = 0, "' .lang('no'). '", "' .lang('yes'). '") AS allow_print_command');
            }if ($key == 'express_payment') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.express_payment = 0, "' .lang('no'). '", "' .lang('yes'). '") AS express_payment');
            }if ($key == 'except_tip_restobar') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.except_tip_restobar = 0, "' .lang('no'). '", "' .lang('yes'). '") AS except_tip_restobar');
            }if ($key == 'required_shipping_restobar') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.required_shipping_restobar = 0, "' .lang('no'). '", "' .lang('yes'). '") AS required_shipping_restobar');
            }if ($key == 'mobile_print_automatically_invoice') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.mobile_print_automatically_invoice = 0 THEN '".lang('mobile_print_automatically_invoice_no')."'
                                        WHEN {$this->db->dbprefix('pos_settings')}.mobile_print_automatically_invoice = 1 THEN '".lang('mobile_print_automatically_invoice_yes')."'
                                        WHEN {$this->db->dbprefix('pos_settings')}.mobile_print_automatically_invoice = 2 THEN '".lang('mobile_print_automatically_invoice_yes_wait_time')."'
                                        ELSE {$this->db->dbprefix('pos_settings')}.mobile_print_automatically_invoice END AS 'mobile_print_automatically_invoice'  ");
            }if ($key == 'use_barcode_scanner') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.use_barcode_scanner = 0, "' .lang('no'). '", "' .lang('yes'). '") AS use_barcode_scanner');
            }if ($key == 'show_variants_and_preferences') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.show_variants_and_preferences = 0, "' .lang('no'). '", "' .lang('yes'). '") AS show_variants_and_preferences');
            }if ($key == 'order_to_table_default_restobar') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.order_to_table_default_restobar = 0, "' .lang('no'). '", "' .lang('yes'). '") AS order_to_table_default_restobar');
            }if ($key == 'show_client_modal_on_select') {
                $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.show_client_modal_on_select = 0, "' .lang('no'). '", "' .lang('yes'). '") AS show_client_modal_on_select');
            }else if($key !== 'default_section' && $key !== 'default_customer' && $key !== 'default_biller' && $key !== 'display_time' && $key !== 'keyboard' && $key !== 'product_button_color' && $key !== 'tooltips' && $key !== 'paypal_pro' && $key !== 'stripe' && $key !== 'rounding' && $key !== 'after_sale_page' && $key !== 'item_order' && $key !== 'authorize' && $key !== 'remote_printing' && $key !== 'auto_print' && $key !== 'customer_details' && $key !== 'local_printers' && $key !== 'balance_settings' && $key !== 'restobar_mode' && $key !== 'table_service' && $key !== 'order_print_mode' && $key !== 'home_delivery_in_invoice' && $key !== 'detail_sale_by_category' && $key !== 'show_suspended_bills_automatically' && $key !== 'print_voucher_delivery' && $key !== 'allow_print_command' && $key !== 'express_payment' && $key !== 'except_tip_restobar' && $key !== 'required_shipping_restobar' && $key !== 'mobile_print_automatically_invoice' && $key !== 'use_barcode_scanner' && $key !== 'product_clic_action' && $key !== 'show_variants_and_preferences' && $key !== 'order_to_table_default_restobar' && $key !== 'show_client_modal_on_select' && $key !== 'last_update'){
                $this->db->select($this->db->dbprefix('pos_settings').'.'.$key);
            }
        }
        $this->db->from($this->db->dbprefix('pos_settings'));
        $q1 = $this->db->get();
        $initialData = $q1->row_array();

        // update pos settings
        $this->db->where('pos_id', '1');
        if ($this->db->update('pos_settings', $data)) {
            foreach ($data as $key => $value) {// ejecutamos nuevamente el query para comparar cuales columnas fueron actualizadas
                if ($key == 'default_section') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.default_section = 999,"' .lang('favorites'). '", IF('.$this->db->dbprefix('pos_settings').'.default_section = 998, "' .lang('promotions'). '",  (SELECT NAME FROM '.$this->db->dbprefix('categories').' WHERE  id = '.$this->db->dbprefix('pos_settings').'.default_section))) AS default_section');
                }if ($key == 'default_customer') {
                    $this->db->select('(select NAME from '.$this->db->dbprefix('companies'). ' where id = ' . $this->db->dbprefix('pos_settings').'.default_customer) AS default_customer');
                }if ($key == 'default_biller'){
                    $this->db->select('(select NAME from '.$this->db->dbprefix('companies'). ' where id = ' . $this->db->dbprefix('pos_settings').'.default_biller) AS default_biller');
                }if($key == 'display_time'){
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.display_time = 0, "' .lang('no'). '", "' .lang('yes'). '") AS display_time');
                }if($key == 'keyboard'){
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.keyboard = 0, "' .lang('no'). '", "' .lang('yes'). '") AS keyboard');
                }if ($key == 'product_button_color') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'default' THEN '" .lang('default'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'primary' THEN '" .lang('primary')."'
                                            WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'info' THEN '" .lang('info') ."'
                                            WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'warning' THEN '" .lang('warning') ."'
                                            WHEN {$this->db->dbprefix('pos_settings')}.product_button_color = 'danger' THEN '" .lang('danger') ."'
                                            ELSE {$this->db->dbprefix('pos_settings')}.product_button_color END AS 'product_button_color'  ");
                }if ($key == 'tooltips') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.tooltips = 0, "' .lang('no'). '", "' .lang('yes'). '") AS tooltips');
                }if ($key == 'paypal_pro') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.paypal_pro = 0, "' .lang('no'). '", "' .lang('yes'). '") AS paypal_pro');
                }if ($key == 'stripe') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.stripe = 0, "' .lang('no'). '", "' .lang('yes'). '") AS stripe');
                }if ($key == 'rounding') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.rounding = 0 THEN '" .lang('disable'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.rounding = 1 THEN '" .lang('to_nearest_005'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.rounding = 2 THEN '" .lang('to_nearest_050'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.rounding = 3 THEN '" .lang('to_nearest_number'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.rounding = 4 THEN '" .lang('to_next_number'). "'
                                            ELSE {$this->db->dbprefix('pos_settings')}.rounding END AS 'rounding'  ");
                }if ($key == 'after_sale_page') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.after_sale_page = 0, "' .lang('receipt'). '", "' .lang('pos'). '") AS after_sale_page');
                }if ($key == 'item_order') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.item_order = 0 THEN '" .lang('default'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.item_order = 1 THEN '" .lang('category'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.item_order = 2 THEN '" .lang('brands'). "'
                                            ELSE {$this->db->dbprefix('pos_settings')}.item_order END AS 'item_order' ");
                }if ($key == 'authorize') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.authorize = 0, "' .lang('no'). '", "' .lang('yes'). '") AS authorize');
                }if ($key == 'remote_printing') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.remote_printing = 1 THEN '" .lang('web_browser_print'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.remote_printing = 4 THEN '" .lang('direct_printing_with_google_cloud_print'). "'
                                            ELSE {$this->db->dbprefix('pos_settings')}.remote_printing END AS 'remote_printing'  ");
                }if ($key == 'auto_print') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.auto_print = 0, "' .lang('no'). '", "' .lang('yes'). '") AS auto_print');
                }if ($key == 'customer_details') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.customer_details = 0, "' .lang('no'). '", "' .lang('yes'). '") AS customer_details');
                }if ($key == 'local_printers') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.local_printers = 0, "' .lang('no'). '", "' .lang('yes'). '") AS local_printers');
                }if ($key == 'balance_settings') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.balance_settings = 0 THEN '" .lang('no_balance'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.balance_settings = 1 THEN '" .lang('balance_no_label'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.balance_settings = 2 THEN '" .lang('balance_label'). "'
                                            ELSE {$this->db->dbprefix('pos_settings')}.balance_settings END AS 'balance_settings'  ");
                }if ($key == 'restobar_mode') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.restobar_mode = 0, "' .lang('inactive'). '", "' .lang('active'). '") AS restobar_mode');
                }if ($key == 'table_service') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.table_service = 0, "' .lang('inactive'). '", "' .lang('active'). '") AS table_service');
                }if ($key == 'order_print_mode') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.order_print_mode = 0 THEN '" .lang('to_screen'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.order_print_mode = 1 THEN '" .lang('to_printer'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.order_print_mode = 2 THEN '" .lang('to_preparation_area'). "'
                                            ELSE {$this->db->dbprefix('pos_settings')}.order_print_mode END AS 'order_print_mode'  ");
                }if ($key == 'home_delivery_in_invoice') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.home_delivery_in_invoice = 0, "' .lang('no'). '", "' .lang('yes'). '") AS home_delivery_in_invoice');
                }if ($key == 'detail_sale_by_category') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.detail_sale_by_category = 0, "' .lang('no'). '", "' .lang('yes'). '") AS detail_sale_by_category');
                }if ($key == 'show_suspended_bills_automatically') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.show_suspended_bills_automatically = 0, "' .lang('no'). '", "' .lang('yes'). '") AS show_suspended_bills_automatically');
                }if ($key == 'print_voucher_delivery') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.print_voucher_delivery = 0, "' .lang('no'). '", "' .lang('yes'). '") AS print_voucher_delivery');
                }if ($key == 'allow_print_command') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.allow_print_command = 0, "' .lang('no'). '", "' .lang('yes'). '") AS allow_print_command');
                }if ($key == 'express_payment') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.express_payment = 0, "' .lang('no'). '", "' .lang('yes'). '") AS express_payment');
                }if ($key == 'except_tip_restobar') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.except_tip_restobar = 0, "' .lang('no'). '", "' .lang('yes'). '") AS except_tip_restobar');
                }if ($key == 'required_shipping_restobar') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.required_shipping_restobar = 0, "' .lang('no'). '", "' .lang('yes'). '") AS required_shipping_restobar');
                }if ($key == 'mobile_print_automatically_invoice') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.mobile_print_automatically_invoice = 0 THEN '".lang('mobile_print_automatically_invoice_no')."'
                                            WHEN {$this->db->dbprefix('pos_settings')}.mobile_print_automatically_invoice = 1 THEN '".lang('mobile_print_automatically_invoice_yes')."'
                                            WHEN {$this->db->dbprefix('pos_settings')}.mobile_print_automatically_invoice = 2 THEN '".lang('mobile_print_automatically_invoice_yes_wait_time')."'
                                            ELSE {$this->db->dbprefix('pos_settings')}.mobile_print_automatically_invoice END AS 'mobile_print_automatically_invoice'  ");
                }if ($key == 'use_barcode_scanner') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.use_barcode_scanner = 0, "' .lang('no'). '", "' .lang('yes'). '") AS use_barcode_scanner');
                }if ($key == 'product_clic_action') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('pos_settings')}.product_clic_action = 1 THEN '" .lang('view_product_details'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.product_clic_action = 2 THEN '" .lang('view_product_variants_and_preferences'). "'
                                            WHEN {$this->db->dbprefix('pos_settings')}.product_clic_action = 3 THEN '" .lang('view_edit_product_form'). "'
                                            ELSE {$this->db->dbprefix('pos_settings')}.product_clic_action END AS 'product_clic_action' ");
                }if ($key == 'show_variants_and_preferences') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.show_variants_and_preferences = 0, "' .lang('no'). '", "' .lang('yes'). '") AS show_variants_and_preferences');
                }if ($key == 'order_to_table_default_restobar') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.order_to_table_default_restobar = 0, "' .lang('no'). '", "' .lang('yes'). '") AS order_to_table_default_restobar');
                }if ($key == 'show_client_modal_on_select') {
                    $this->db->select('IF('.$this->db->dbprefix('pos_settings').'.show_client_modal_on_select = 0, "' .lang('no'). '", "' .lang('yes'). '") AS show_client_modal_on_select');
                }else if($key !== 'default_section' && $key !== 'default_customer' && $key !== 'default_biller' && $key !== 'display_time' && $key !== 'keyboard' && $key !== 'product_button_color' && $key !== 'tooltips' && $key !== 'paypal_pro' && $key !== 'stripe' && $key !== 'rounding' && $key !== 'after_sale_page' && $key !== 'item_order' && $key !== 'authorize' && $key !== 'remote_printing' && $key !== 'auto_print' && $key !== 'customer_details' && $key !== 'local_printers' && $key !== 'balance_settings' && $key !== 'restobar_mode' && $key !== 'table_service' && $key !== 'order_print_mode' && $key !== 'home_delivery_in_invoice' && $key !== 'detail_sale_by_category' && $key !== 'show_suspended_bills_automatically' && $key !== 'print_voucher_delivery' && $key !== 'allow_print_command' && $key !== 'express_payment' && $key !== 'except_tip_restobar' && $key !== 'required_shipping_restobar' && $key !== 'mobile_print_automatically_invoice' && $key !== 'use_barcode_scanner' && $key !== 'product_clic_action' && $key !== 'show_variants_and_preferences' && $key !== 'order_to_table_default_restobar' && $key !== 'show_client_modal_on_select' && $key !== 'last_update'){
                    $this->db->select($this->db->dbprefix('pos_settings').'.'.$key);
                }
            }
            $this->db->from($this->db->dbprefix('pos_settings'));
            $q2 = $this->db->get();
            $updateData = $q2->row_array();
            $differences = array_diff_assoc($updateData, $initialData);
            if (!empty($differences)) {
                $changes = '';
                foreach ($differences as $key => $value) {
                    $changes .=  lang($key)  . ' '. lang('from') . ': ' . (($initialData[$key] == "" || is_null($initialData[$key])) ? "Vacío" : $initialData[$key]) . '  ' .lang('to') . ': '  . (($updateData[$key] == "" || is_null($updateData[$key])) ? "Vacío" : $updateData[$key] ) .', ';
                }
                $changes = trim($changes,  ", ");
                $txt_fields_changed = sprintf(lang('user_fields_changed_settings'), $this->session->first_name." ".$this->session->last_name, lang($this->m) . " : " . $changes );
                $this->db->insert('user_activities', [
                            'date' => date('Y-m-d H:i:s'),
                            'type_id' => 1,
                            'user_id' => $this->session->userdata('user_id'),
                            'module_name' => 'pos_settings',
                            'table_name' => 'pos_settings',
                            'record_id' => 1,
                            'description' => $txt_fields_changed,
                        ]);
            }
            return true;
        }
        return false;
    }

    public function products_count($category_id, $subcategory_id = NULL, $brand_id = NULL)
    {
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        if ($brand_id) {
            $this->db->where('brand', $brand_id);
        }
        $this->db->from('products');
        return $this->db->count_all_results();
    }

    public function fetch_products($category_id, $limit, $start, $subcategory_id = NULL, $brand_id = NULL, $biller_id = NULL, $warehouse_id = NULL)
    {

        if ((!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) || $biller_id) {
            if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
                $biller_id = $this->session->userdata('biller_id');
            }
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }

        $this->db->select('
                            products.*
                            '.($warehouse_id ? ', WP.quantity AS quantity' : '').'
                        ');
        $this->db->limit($limit, $start);
        if ($brand_id) {
            $this->db->where('brand', $brand_id);
        } elseif ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        if ($warehouse_id) {
            $this->db->join('warehouses_products WP', 'WP.product_id = products.id AND WP.warehouse_id = '.$warehouse_id, 'left');
            if ($this->Settings->overselling == 0) {
                $this->db->where('WP.quantity >', 0);
            }
        }
        $this->db->where('discontinued', 0);
        $this->db->order_by("products.name", "asc");
        if ($biller_id) {
            $this->db
                ->join('biller_data', 'biller_data.biller_id IN ('.$biller_id.')', 'left');
            $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
        }
        $query = $this->db->get("products");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return false;
    }
    public function registerData($user_id = NULL, $register_id = NULL)
    {
        if ((!$user_id && !$register_id) || $user_id) {
            if (!$user_id) {
                $user_id = $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id');
            }
            $this->db->where('user_id', $user_id);
            $this->db->where('status', 'open');
        } else if ($register_id) {
            $this->db->where('id', $register_id);
        }
        $q = $this->db->get('pos_register', 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function openRegister($data)
    {
        if ($this->db->insert('pos_register', $data)) {
            return true;
        }
        return FALSE;
    }

    public function getOpenRegisters()
    {
        $this->db->select("pos_register.id, date, user_id, cash_in_hand, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name, ' - ', " . $this->db->dbprefix('users') . ".email) as user", FALSE)
            ->join('users', 'users.id=pos_register.user_id', 'left');
        $q = $this->db->get_where('pos_register', array('status' => 'open'));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;

    }

    public function closeRegister($rid, $user_id, $data, $items, $sync)
    {

        if ($sync) {
            $this->db->delete('pos_register_items', ['pos_register_id' => $rid]);
        } else {
            if (!$rid) {
                $rid = $this->session->userdata('register_id');
            }
            if (!$user_id) {
                $user_id = $this->session->userdata('user_id');
            }
        }

        if ($data['transfer_opened_bills'] == -1) {
            $this->db->delete('suspended_bills', array('created_by' => $user_id));
        } elseif ($data['transfer_opened_bills'] != 0) {
            $this->db->update('suspended_bills', array('created_by' => $data['transfer_opened_bills']), array('created_by' => $user_id));
        }
        if ($this->db->update('pos_register', $data, array('id' => $rid, 'user_id' => $user_id))) {
            foreach ($items as $item) {
                $this->db->insert('pos_register_items', $item);
            }
            return true;
        }
        return FALSE;
    }

    public function getUsers()
    {
        $q = $this->db->get_where('users', array('company_id' => NULL));
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
        return FALSE;
    }

    public function getProductsByCode($code)
    {
        $this->db->like('code', $code, 'both')->order_by("code");
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
    }

    public function getWHProduct($code = NULL, $warehouse_id = null, $id = NULL)
    {
        $this->db->select('products.*, warehouses_products.quantity as quantity, categories.id as category_id, categories.name as category_name')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->where('categories.hide != 1')
            ->group_by('products.id');
        if ($code) {
            $this->db->where('products.code', $code);
        }
        if ($id) {
            $this->db->where('products.id', $id);
        }
        $q = $this->db->get("products");
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id, $all = NULL)
    {
        $wpv = "( SELECT option_id, warehouse_id, quantity from {$this->db->dbprefix('warehouses_products_variants')} WHERE product_id = {$product_id}) FWPV";
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, FWPV.quantity as quantity', FALSE)
            ->join($wpv, 'FWPV.option_id=product_variants.id', 'left')
            //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->group_by('product_variants.id');

        if (! $this->Settings->overselling && ! $all) {
            $this->db->where('FWPV.warehouse_id', $warehouse_id);
            $this->db->where('FWPV.quantity >', 0);
        }
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
        return FALSE;
    }

    public function getProductPreferences($product_id){
        $q = $this->db->select('product_preferences.*, preferences_categories.name as prf_cat_name')
                ->join('preferences_categories', 'preferences_categories.id = product_preferences.preference_category_id', 'join')
                ->where('product_id', $product_id)
                ->order_by('preferences_categories.name ASC')
                ->order_by('product_preferences.preference_id ASC')
                ->get('product_preferences')
                ;
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->where('warehouses_products.warehouse_id', $warehouse_id)
            ->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function updateOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addSale($data = array(), $document_type_id = null, $items = array(), $payments = array(), $sid = NULL, $quote_id = NULL, $order = NULL)
    {
        $cost = $this->site->costing($items);
        // $this->sma->print_arrays($cost);
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        } else {
            $reference = $this->site->getReference('pos');
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        if ($duplicated_id = $this->site->check_if_register_exists('sales', $data)) {
            $this->session->set_flashdata('error', lang('process_interrupted_due_to_duplicate_records'));
            if ($duplicated_id > 0) {
                admin_redirect('pos/view/'.$duplicated_id);
            } else {
                admin_redirect('pos');
            }
            return false;
        }
        if ($this->db->insert('sales', $data)) {
            $sale_id = $this->db->insert_id();
            $data['sale_id'] = $sale_id;
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            $item_costing_each = [];
            foreach ($items as $key => $item) {
                if ($item["state_readiness"] != CANCELLED) {
                    $item['sale_id'] = $sale_id;
                    $check_reference_data = [
                        'table' => 'sales', 'record_id' => $sale_id, 'reference' => $data['reference_no'], 'biller_id' => $data['biller_id'], 'document_type_id' => $data['document_type_id']
                        ];
                    if ($new_reference = $this->site->check_reference($check_reference_data)) {
                        $data['reference_no'] = $new_reference;
                    }
                    if ($this->Settings->ipoconsumo && $item['consumption_sales'] == 0) {
                        $product = $this->site->getProductByID($item['product_id']);
                        if ($product->consumption_purchase_tax > 0) {
                            $item['consumption_sales_costing'] = $product->consumption_purchase_tax;
                            $items[$key]['consumption_sales_costing'] = $product->consumption_purchase_tax;
                        }
                    }
                    $product_gift_card_no = $product_gift_card_value = $product_gift_card_expiry = NULL;
                    if (isset($item['product_gift_card_no']) && !empty($item['product_gift_card_no'])) {
                        $product_gift_card_no = $item['product_gift_card_no'];
                        $product_gift_card_value = $item['product_gift_card_value'];
                        $product_gift_card_expiry = $item['product_gift_card_expiry'];
                        $item['product_name'] .= " (".$item['product_gift_card_no'].")";
                    }
                    unset($item['product_gift_card_no']);
                    unset($item['product_gift_card_value']);
                    unset($item['product_gift_card_expiry']);
                    $this->db->insert('sale_items', $item);
                    $sale_item_id = $this->db->insert_id();

                    if ($product_gift_card_no) {
                        $gc_data = array(
                            'card_no' => $product_gift_card_no,
                            'value' => $product_gift_card_value,
                            'customer_id' => $data['customer_id'],
                            'customer' => $data['customer'],
                            'balance' => $product_gift_card_value,
                            'expiry' => $product_gift_card_expiry,
                            'creation_type' => 1,
                            'sale_item_id' => $sale_item_id,
                            'status' => 1,
                            'sale_reference_no' => $data['reference_no'],
                            'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                        );
                        $this->db->insert('gift_cards', $gc_data);
                    }
                    if ($data['sale_status'] == 'completed' && $this->site->getProductByID($item['product_id'])) {

                        $item_costs = $this->site->item_costing($item);
                        // $this->sma->print_arrays($item_costs);
                        $item_costing_each[$item['product_id']] = $item_costs;
                        if (isset($item_costs[0]['purchase_net_unit_cost'])) {
                            $avg_net_unit_cost = $item_costs[0]['purchase_net_unit_cost'];
                            $this->db->update('sale_items', ['avg_net_unit_cost' => $avg_net_unit_cost], ['sale_id' => $sale_id, 'product_id' => $item['product_id']]);
                        }

                        if ($item['product_type'] == 'combo') {
                            $biller_data = $this->get_biller_data_by_biller_id($data['biller_id']);

                            $insert_adjustment = [];
                            $qareferenceBiller = $this->site->getReferenceBiller($data['biller_id'], $biller_data->preparation_adjustment_document_type_id);
                            if($qareferenceBiller){
                                $qareference = $qareferenceBiller;
                            } else {
                                $qareference = $this->site->getReference('qa');
                            }
                            $insert_adjustment['reference_no'] = $qareference;
                            $insert_adjustment['document_type_id'] = $biller_data->preparation_adjustment_document_type_id;
                            $qaref = explode("-", $qareference);
                            $qaconsecutive = $qaref[1];
                            $insert_adjustment['date'] = $data['date'];
                            $insert_adjustment['warehouse_id'] = 1;
                            $insert_adjustment['note'] = 'Ajuste por venta '.$data['reference_no'];
                            $insert_adjustment['created_by'] = $this->session->userdata('user_id');
                            $insert_adjustment['companies_id'] = $data['customer_id'];
                            $insert_adjustment['biller_id'] = $data['biller_id'];
                            $insert_adjustment['ignore_accountant'] = 1;
                            $insert_adjustment['cost_center_id'] = isset($data['cost_center_id']) ? $data['cost_center_id'] : NULL;
                            $this->db->insert('adjustments', $insert_adjustment);
                            $adjustment_id = $this->db->insert_id();
                            $this->site->updateBillerConsecutive($insert_adjustment['document_type_id'], $qaconsecutive+1);
                            $insert_adjustment['grand_total'] = 0;

                        }

                        // $this->sma->print_arrays($item_costs);
                        foreach ($item_costs as $item_cost) {
                            if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                                $item_cost['sale_item_id'] = $sale_item_id;
                                $item_cost['sale_id'] = $sale_id;
                                $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                                unset($item_cost['preparation_id_origin']);
                                if(! isset($item_cost['pi_overselling'])) {
                                    // $this->sma->print_arrays($item_cost);
                                    $this->db->insert('costing', $item_cost);
                                }
                            } else {
                                foreach ($item_cost as $ic) {
                                    $ic['sale_item_id'] = $sale_item_id;
                                    $ic['sale_id'] = $sale_id;
                                    $ic['date'] = date('Y-m-d', strtotime($data['date']));
                                    unset($ic['preparation_id_origin']);
                                    if(! isset($ic['pi_overselling'])) {
                                        $this->db->insert('costing', $ic);
                                    }
                                }
                            }
                            $item_cost_adj = false;
                            if (isset($item_cost[0]) && count($item_cost[0]) == 0 && count($item_cost[1]) > 0 && isset($item_cost[1]['product_id'])) {
                                $item_cost_adj = $item_cost[1];
                            } else if (isset($item_cost[0])) {
                                $item_cost_adj = $item_cost[0];
                            }
                            if ($item['product_type'] == 'combo' && $item_cost_adj) {
                                $data_pi = array(
                                                'adjustment_id' => $adjustment_id,
                                                'product_id' => $item_cost_adj['product_id'],
                                                'warehouse_id' => $item['warehouse_id'],
                                                'quantity' => ($item_cost_adj['quantity'] * $item['quantity']),
                                                'type' => 'subtraction',
                                                'avg_cost' => $item_cost_adj['purchase_unit_cost'],
                                            );
                                $pidata = $this->site->getProductByID($item_cost_adj['product_id']);
                                $product_names[$item_cost_adj['product_id']] = $pidata->name;
                                $insert_adjustment['grand_total'] += $item_cost_adj['purchase_unit_cost'];
                                $insert_adjustment_items[] = $data_pi;
                                $this->db->insert('adjustment_items', $data_pi);

                                $clause = array('product_id' => $data_pi['product_id'], 'option_id' => NULL, 'warehouse_id' => $data_pi['warehouse_id'], 'status' => 'received');
                                $qty = $data_pi['type'] == 'subtraction' ? 0 - $data_pi['quantity'] : 0 + $data_pi['quantity'];
                                $this->site->setPurchaseItem($clause, $qty);
                                $this->site->syncProductQty($data_pi['product_id'], $data_pi['warehouse_id']);
                            }
                        }
                        if ($this->Settings->modulary == 1 && isset($insert_adjustment)) {
                            // $this->site->wappsiContabilidadAjustes($insert_adjustment, $insert_adjustment_items, $product_names);
                        }
                    }
                }
                if ($quote_id && $order) {
                    $qty_del = 0;
                    $this->db->where("quantity_to_bill > ", 0);
                    $condicion = array(
                                        'sale_id' => $quote_id,
                                        'product_id' => $item['product_id']
                                    );
                    if ($this->sma->field_is_filled($item['product_unit_id'])){
                        $condicion['product_unit_id'] = $item['product_unit_id'];
                    }
                    if ($this->sma->field_is_filled($item['option_id'])){
                        $condicion['option_id'] = $item['option_id'];
                    }
                    $order_item = $this->db->where($condicion)->get('order_sale_items')->row_array();
                    if (!$order_item) {
                        // exit(var_dump($this->db->last_query()));
                    }
                    $qty_del = $order_item['quantity_to_bill'] + $order_item['quantity_delivered'];
                    if ($qty_del > $order_item['quantity']) {
                        $qty_del = $order_item['quantity'];
                    }
                    $condicion['id'] = $order_item['id'];
                    $this->db->update('order_sale_items', array('quantity_delivered' => $qty_del, 'quantity_to_bill' => 0), ($condicion));
                }
            }
            $this->site->syncStoreProductsQuantityMovement($items, 2);
            if ($quote_id && !$order) {
                $this->db->update('quotes', array('status' => 'completed', 'destination_reference_no' => $data['reference_no']), array('id' => $quote_id));
            } else if ($quote_id && $order) {
                $this->site->updateOrderSaleStatus($quote_id);
                $this->db->update('order_sales', array('destination_reference_no' => $data['reference_no']), array('id' => $quote_id));
            }

            if ($data['sale_status'] == 'completed') {
                $this->site->syncPurchaseItems($cost);
            }

            $msg = array();
            if (!empty($payments)) {
                $paid = 0;
                foreach ($payments as $pmnt_id => $payment) {
                    if (!empty($payment) && isset($payment['amount']) && $payment['amount'] != 0) {
                        $payment['sale_id'] = $sale_id;

                        if ($payment['paid_by'] == 'retencion') { //SI EL PAGO ES POR LA RETENCIÓN ACTIVAMOS LA BANDERA Y ASIGNAMOS EL RESPECTIVO CONSECUTIVO
                            $payment['reference_no'] = 'retencion';
                            $retencion = 1;
                        } else {
                            $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $payment['document_type_id']);
                            if($referenceBiller){
                                $reference_payment = $referenceBiller;
                            }else{
                                $reference_payment = $this->site->getReference('rc');
                            }
                            $payment['reference_no'] = $reference_payment;
                            $ref = explode("-", $reference_payment);
                            $p_consecutive = $ref[1];
                            $pmnt_data = $this->site->getPaymentMethodByCode($payment['paid_by']);
                            if ($pmnt_data) {
                                $payment['pm_commision_value'] = $pmnt_data->commision_value;
                                $payment['pm_retefuente_value'] = $pmnt_data->retefuente_value;
                                $payment['pm_reteiva_value'] = $pmnt_data->reteiva_value;
                                $payment['pm_reteica_value'] = $pmnt_data->reteica_value;
                            }

                        }

                        if ($payment['paid_by'] == 'ppp') {
                            $card_info = array("number" => $payment['cc_no'], "exp_month" => $payment['cc_month'], "exp_year" => $payment['cc_year'], "cvc" => $payment['cc_cvv2'], 'type' => $payment['cc_type']);
                            $result = $this->paypal($payment['amount'], $card_info);
                            if (!isset($result['error'])) {
                                $payment['transaction_id'] = $result['transaction_id'];
                                $payment['date'] = $this->sma->fld($result['created_at']);
                                $payment['amount'] = $result['amount'];
                                $payment['currency'] = $result['currency'];
                                unset($payment['cc_cvv2']);
                                $this->db->insert('payments', $payment);
                                $this->site->updateReference('pay');
                                $paid += $payment['amount'];
                            } else {
                                $msg[] = lang('payment_failed');
                                if (!empty($result['message'])) {
                                    foreach ($result['message'] as $m) {
                                        $msg[] = '<p class="text-danger">' . $m['L_ERRORCODE'] . ': ' . $m['L_LONGMESSAGE'] . '</p>';
                                    }
                                } else {
                                    $msg[] = lang('paypal_empty_error');
                                }
                            }
                        } elseif ($payment['paid_by'] == 'stripe') {
                            $card_info = array("number" => $payment['cc_no'], "exp_month" => $payment['cc_month'], "exp_year" => $payment['cc_year'], "cvc" => $payment['cc_cvv2'], 'type' => $payment['cc_type']);
                            $result = $this->stripe($payment['amount'], $card_info);
                            if (!isset($result['error'])) {
                                $payment['transaction_id'] = $result['transaction_id'];
                                $payment['date'] = $this->sma->fld($result['created_at']);
                                $payment['amount'] = $result['amount'];
                                $payment['currency'] = $result['currency'];
                                unset($payment['cc_cvv2']);
                                $this->db->insert('payments', $payment);
                                $this->site->updateReference('pay');
                                $paid += $payment['amount'];
                            } else {
                                $msg[] = lang('payment_failed');
                                $msg[] = '<p class="text-danger">' . $result['code'] . ': ' . $result['message'] . '</p>';
                            }
                        } elseif ($payment['paid_by'] == 'authorize') {
                            $authorize_arr = array("x_card_num" => $payment['cc_no'], "x_exp_date" => ($payment['cc_month'].'/'.$payment['cc_year']), "x_card_code" => $payment['cc_cvv2'], 'x_amount' => $payment['amount'], 'x_invoice_num' => $sale_id, 'x_description' => 'Sale Ref '.$data['reference_no'].' and Payment Ref '.$payment['reference_no']);
                            list($first_name, $last_name) = explode(' ', $payment['cc_holder'], 2);
                            $authorize_arr['x_first_name'] = $first_name;
                            $authorize_arr['x_last_name'] = $last_name;
                            $result = $this->authorize($authorize_arr);
                            if (!isset($result['error'])) {
                                $payment['transaction_id'] = $result['transaction_id'];
                                $payment['approval_code'] = $result['approval_code'];
                                $payment['date'] = $this->sma->fld($result['created_at']);
                                unset($payment['cc_cvv2']);
                                $this->db->insert('payments', $payment);
                                $this->site->updateReference('pay');
                                $paid += $payment['amount'];
                            } else {
                                $msg[] = lang('payment_failed');
                                $msg[] = '<p class="text-danger">' . $result['msg'] . '</p>';
                            }
                        } else {

                            if ($payment['paid_by'] == 'gift_card') {
                                $this->db->update('gift_cards', array('balance' => $payment['gc_balance']), array('card_no' => $payment['cc_no']));
                                unset($payment['gc_balance']);
                                $this->site->update_gift_card_balance_status(NULL, $payment['cc_no']);
                            } elseif ($payment['paid_by'] == 'deposit') {
                                unset($payment['cc_cvv2']);
                                $p_arr[] = $payment;
                                $p_arr = $this->site->set_payments_affected_by_deposits($data['customer_id'], $p_arr);
                                $count = 0;
                                foreach ($p_arr as $pmnt2) {
                                    $pmnt2['pos_paid'] = $pmnt2['amount'];
                                    if ($count > 0) {
                                        $this->site->updateBillerConsecutive($pmnt2['document_type_id'], $p_consecutive+1);
                                        // NUEVA REFERENCIA POR REGISTRO DE OTRO DEPÓSITO
                                        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt2['document_type_id']);
                                        if($referenceBiller){
                                            $reference_payment = $referenceBiller;
                                        }else{
                                            $reference_payment = $this->site->getReference('rc');
                                        }
                                        $pmnt2['reference_no'] = $reference_payment;
                                        $ref = explode("-", $reference_payment);
                                        $p_consecutive = $ref[1];
                                        // NUEVA REFERENCIA POR REGISTRO DE OTRO DEPÓSITO
                                    }
                                    $this->db->insert('payments', $pmnt2);
                                    $count++;
                                }
                            }
                            unset($payment['cc_cvv2']);
                            if ($payment['paid_by'] != 'deposit') {
                                if ($asd = $this->db->insert('payments', $payment)) {
                                     if (!isset($retencion)) { //SI EL PAGO NO ES POR RETENCION, AUMENTAMOS EL CONSECUTIVO DE PAGO
                                        $this->site->updateReference('pay');
                                     }
                                } else {
                                    exit(var_dump($this->db->error()));
                                }
                            }
                            $paid += $payment['amount'];
                        }
                        if (isset($p_consecutive) && isset($payment['document_type_id'])) {
                            $this->site->updateBillerConsecutive($payment['document_type_id'], $p_consecutive+1);
                        }
                    }
                    if (isset($pmnt_data)) {
                        $payments[$pmnt_id]['pmnt_data'] = $pmnt_data;
                    }
                }
                $this->site->syncSalePayments($sale_id);
            }

            $this->site->syncQuantity($sale_id);
            if ($sid) {
                $this->deleteBill($sid);
            }

            if ($this->Settings->modulary == 1) { //Si se indicó que el POS es modular con Contabilidad.
                /* LLamado a la función wappsiContabilidad para alimentar las tablas del modulo de contabilidad */
                if($this->site->wappsiContabilidadVentas($sale_id, $data, $items, $payments, $cost, null, $item_costing_each)){
                  // Se realizo el proceso de inserción en las tablas de contabilidad.
                }
            }

            $biller_data = $this->site->getAllCompaniesWithState('biller', $data['biller_id']);
            if ($biller_data->default_customer_id != $data['customer_id']) {
                $this->sma->update_award_points($data);
            }
            $this->site->updateReference('pos');
            return array('sale_id' => $sale_id, 'message' => $msg, 'reference_no' => $data['reference_no']);
        }

        return false;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllBillerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'biller'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getBillerById($biller_id)
    {
        $q = $this->db->get_where('companies', array('id' => $biller_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $biller = $row->name;
            }

            return $biller;
        }
    }

    public function getAllCustomerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'customer'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCompanyByID($id)
    {

        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllProducts()
    {
        $q = $this->db->query('SELECT * FROM products ORDER BY id');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductByID($id)
    {

        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllTaxRates()
    {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getTaxRateByID($id)
    {

        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function updateProductQuantity($product_id, $warehouse_id, $quantity)
    {

        if ($this->addQuantity($product_id, $warehouse_id, $quantity)) {
            return true;
        }

        return false;
    }

    public function addQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($warehouse_quantity = $this->getProductQuantity($product_id, $warehouse_id)) {
            $new_quantity = $warehouse_quantity['quantity'] - $quantity;
            if ($this->updateQuantity($product_id, $warehouse_id, $new_quantity)) {
                $this->site->syncProductQty($product_id, $warehouse_id);
                return TRUE;
            }
        } else {
            if ($this->insertQuantity($product_id, $warehouse_id, -$quantity)) {
                $this->site->syncProductQty($product_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($this->db->update('warehouses_products', array('quantity' => $quantity), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function getItemByID($id)
    {
        $q = $this->db->get_where('sale_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllSales()
    {
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
        return FALSE;
    }

    public function sales_count()
    {
        return $this->db->count_all("sales");
    }

    public function fetch_sales($limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by("id", "desc");
        $query = $this->db->get("sales");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getAllInvoiceItems($sale_id, $product_order = NULL)
    {
        if ($this->pos_settings->item_order == 0) {
            $this->db->select('sale_items.*,
                tax_rates.code as tax_code,
                tax_rates.name as tax_name,
                tax_rates.rate as tax_rate,
                tax_rates.tax_indicator as tax_indicator,
                tr2ic.tax_indicator as tax_indicator2ic,
                tr2ic.code as tax_code2ic,
                CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "")) as variant,
                '.$this->db->dbprefix('product_variants').'.code as variant_code,
                products.details as details,
                products.hsn_code as hsn_code,
                products.second_name as second_name,
                products.code as barcode,
                products.category_id as category_id,
                products.category_id as product_category_id,
                products.tax_method as tax_method,
                brands.name as brand_name,
                units.code as product_unit_code,
                units.operation_value,
                units.operator,
                products.reference as reference,
                main_unit.code as main_unit_code,
                main_unit.id as main_unit_id
            ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('brands', 'brands.id=products.brand', 'left')
            ->join('units', 'sale_items.product_unit_id_selected=units.id', 'left')
            ->join('units main_unit', 'products.unit=main_unit.id', 'left')
            ->join('unit_prices', 'sale_items.product_unit_id_selected = unit_prices.unit_id and unit_prices.id_product = sale_items.product_id', 'left')
            ->join('tax_secondary tr2ic', 'tr2ic.id=sale_items.tax_rate_2_id', 'left')
            ->group_by('sale_items.id');
            if ($product_order == NULL || $product_order == 4) {
                $this->db->order_by('sale_items.id', 'asc');
            } else if ($product_order == 1) {
                $this->db->order_by('products.code', 'asc');
            } else if ($product_order == 2) {
                $this->db->order_by('products.name', 'asc');
            } else if ($product_order == 3) {
                $this->db->order_by('products.name', 'desc');
            } else if ($product_order == 5) {
                if ($this->Settings->product_order == 2) {
                    $this->db->order_by('sale_items.id', 'desc');
                } else {
                    $this->db->order_by('sale_items.id', 'asc');
                }
            }
        } elseif ($this->pos_settings->item_order == 1) {
            $this->db->select('sale_items.*,
                tax_rates.code as tax_code,
                tax_rates.name as tax_name,
                tax_rates.rate as tax_rate,
                tax_rates.tax_indicator as tax_indicator,
                CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "")) as variant,
                categories.id as category_id,
                categories.name as category_name,
                products.details as details,
                products.category_id as product_category_id,
                products.hsn_code as hsn_code,
                products.second_name as second_name,
                products.code as barcode,
                products.reference as reference,
                brands.name as brand_name,
                units.code as product_unit_code,
                units.operation_value,
                units.operator,
                main_unit.code as main_unit_code,
                main_unit.id as main_unit_id
            ')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->join('brands', 'brands.id=products.brand', 'left')
            ->join('units', 'sale_items.product_unit_id_selected=units.id', 'left')
            ->join('units main_unit', 'products.unit=main_unit.id', 'left')
            ->join('unit_prices', 'sale_items.product_unit_id_selected = unit_prices.unit_id and unit_prices.id_product = sale_items.product_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('categories.id', 'asc');
        } elseif ($this->pos_settings->item_order == 2) {
            $this->db->select('sale_items.*,
                tax_rates.code as tax_code,
                tax_rates.name as tax_name,
                tax_rates.rate as tax_rate,
                tax_rates.tax_indicator as tax_indicator,
                product_variants.name as variant,
                brands.id as brand_id,
                brands.name as brand_name,
                products.details as details,
                products.hsn_code as hsn_code,
                products.second_name as second_name,
                products.code as barcode,
                products.reference as reference,
                products.category_id as category_id,
                brands.name as brand_name,
                units.code as product_unit_code,
                units.operation_value,
                units.operator,
                main_unit.code as main_unit_code,
                main_unit.id as main_unit_id
            ')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('brands', 'brands.id=products.brand', 'left')
            ->join('units', 'sale_items.product_unit_id_selected=units.id', 'left')
            ->join('units main_unit', 'products.unit=main_unit.id', 'left')
            ->join('unit_prices', 'sale_items.product_unit_id_selected = unit_prices.unit_id and unit_prices.id_product = sale_items.product_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('brands.id', 'asc');
        }

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
        return FALSE;
    }

    public function get_invoice_products_by_order_in_biller_data($sale_id, $product_order = NULL)
    {
        if ($product_order == "0") {
            $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, product_variants.name as variant, products.details as details, products.hsn_code as hsn_code, products.second_name as second_name, products.code as barcode')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('id', 'asc');
        } elseif ($product_order == "1") {
            $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, product_variants.name as variant, categories.id as category_id, categories.name as category_name, products.details as details, products.hsn_code as hsn_code, products.second_name as second_name, products.code as barcode')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('categories.id', 'asc');
        } elseif ($product_order == "2") {
            $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, product_variants.name as variant, brands.id as brand_id, brands.name as brand_name, products.details as details, products.hsn_code as hsn_code, products.second_name as second_name, products.code as barcode')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('brands', 'brands.id=products.brand', 'left')
            ->group_by('sale_items.id')
            ->order_by('brands.id', 'asc');
        }

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
        return FALSE;
    }

    public function getSuspendedSaleItems($id)
    {
        $q = $this->db->get_where('suspended_items', array('suspend_id' => $id));
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
    }

    public function getSaleItems($id)
    {
        $q = $this->db->get_where('sale_items', array('sale_id' => $id));
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
    }

    public function getSuspendedSales($user_id = NULL)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('suspended_bills.*, tables.numero');
        $this->db->join('tables', 'tables.id = suspended_bills.table_id', 'left');
        $q = $this->db->get_where('suspended_bills', array('created_by' => $user_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getPaymentTermToExpire($days_alert, $customer_id = NULL, $just_validate = false){
        $this->db->where(array('payment_status !=' => 'paid', 'DATEDIFF(due_date, "'.date('Y-m-d').'") <=' => $days_alert));

        if ($customer_id != NULL) {
            $this->db->where('customer_id', $customer_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            if ($just_validate) {
                return $q->num_rows();
            } else {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            }
        } else {
            return FALSE;
        }
    }

    public function getOpenBillByID($id)
    {

        $q = $this->db->get_where('suspended_bills', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    // public function getInvoiceByID($id)
    // {

    //     $q = $this->db->get_where('sales', array('id' => $id), 1);
    //     if ($q->num_rows() > 0) {
    //         return $q->row();
    //     }

    //     return FALSE;
    // }

    public function getInvoiceByID($id, $arr = false)
    {
        $this->db->select('sales.*,
                            addresses.sucursal as negocio,
                            addresses.direccion as direccion_negocio,
                            addresses.city as ciudad_negocio,
                            addresses.state as state_negocio,
                            addresses.country as country_negocio,
                            addresses.phone as phone_negocio,
                            tax_rates.rate as tax_rate,
                            tax_rates.type as type_tax_rate,
                            documents_types.module_invoice_format_id,
                            documents_types.factura_electronica,
                            documents_types.quick_print_format_id')
            ->where(array('sales.id' => $id))
            ->join('documents_types', 'sales.document_type_id = documents_types.id', 'left')
            ->join('addresses', 'addresses.id = sales.address_id', 'left')
            ->join('tax_rates', 'tax_rates.id = sales.order_tax_id', 'left');
        $q = $this->db->get('sales', 1);

        if ($arr == false) {
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        } else {
            if ($q->num_rows() > 0) {
                return $q->row_array();
            }
        }
        return FALSE;
    }

    public function bills_count()
    {
        $this->db->select('suspended_bills.*, companies.company as name_seller')
                ->join('companies', 'companies.id = suspended_bills.seller_id', 'left');
        if (!$this->Owner && !$this->Admin) {
            if ($this->session->userdata('warehouse_id')) {
                $this->db->where('suspended_bills.warehouse_id', $this->session->userdata('warehouse_id'));
            }
        }
        return $this->db->count_all_results("suspended_bills");
    }

    public function bills_count_reference($bill_reference, $order = 3)
    {

        $this->db->select('suspended_bills.*, companies.company as name_seller')
                ->join('companies', 'companies.id = suspended_bills.seller_id', 'left');
        if (!$this->Owner && !$this->Admin) {
            if ($this->session->userdata('warehouse_id')) {
                $this->db->where('suspended_bills.warehouse_id', $this->session->userdata('warehouse_id'));
            }
        }
        if ($bill_reference) {
            $this->db->like('suspend_note',$bill_reference)->or_like('company',$bill_reference);
        }
        if ($order == 1) {
            $this->db->where(' suspended_bills.date LIKE "'.date('Y-m-d').'%" ');
            $this->db->order_by("suspended_bills.id", "desc");
        } else if ($order == 2) {
            $this->db->where(' suspended_bills.date LIKE "'.date('Y-m-d').'%" ');
            $this->db->order_by("suspended_bills.id", "asc");
        } else if ($order == 3) {
            $this->db->order_by("suspended_bills.id", "asc");
        }
        return $this->db->count_all_results("suspended_bills");
    }

    public function fetch_bills_reference($limit, $start, $bill_reference = NULL, $order = 3)
    {

        $this->db->select('suspended_bills.*, companies.company as name_seller, tables.tipo AS table_type')
                ->join('companies', 'companies.id = suspended_bills.seller_id', 'left')
                ->join('tables', "tables.id = suspended_bills.table_id", 'left')
                ->where('(tables.tipo IS NULL OR tables.tipo = "M")');
        if (!$this->Owner && !$this->Admin) {
            if ($this->pos_settings->suspended_sales_limited_users_filter == 1) {
                if ($this->session->userdata('biller_id')) {
                    $this->db->where('suspended_bills.biller_id', $this->session->userdata('biller_id'));
                }
            } else if ($this->pos_settings->suspended_sales_limited_users_filter == 2) {
                if ($this->session->userdata('warehouse_id')) {
                    $this->db->where('suspended_bills.warehouse_id', $this->session->userdata('warehouse_id'));
                }
            } else if ($this->pos_settings->suspended_sales_limited_users_filter == 3) {
                if ($this->session->userdata('biller_id')) {
                    $this->db->where('suspended_bills.biller_id', $this->session->userdata('biller_id'));
                }
                if ($this->session->userdata('warehouse_id')) {
                    $this->db->where('suspended_bills.warehouse_id', $this->session->userdata('warehouse_id'));
                }
            }
        }
        if ($bill_reference) {
            $this->db->like('suspend_note',$bill_reference)->or_like('company',$bill_reference);
        }
        if ($order == 1) {
            $this->db->where(' suspended_bills.date LIKE "'.date('Y-m-d').'%" ');
            $this->db->order_by("suspended_bills.id", "desc");
        } else if ($order == 2) {
            $this->db->where(' suspended_bills.date LIKE "'.date('Y-m-d').'%" ');
            $this->db->order_by("suspended_bills.id", "asc");
        } else if ($order == 3) {
            $this->db->order_by("suspended_bills.id", "asc");
        }

        $this->db->limit($limit, $start);
        $query = $this->db->get("suspended_bills");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function fetch_bills($limit, $start)
    {
        if (!$this->Owner && !$this->Admin) {
            if ($this->session->userdata('warehouse_id')) {
                $this->db->where('warehouse_id', $this->session->userdata('warehouse_id'));
            }
        }
        $this->db->limit($limit, $start);
        $this->db->order_by("id", "asc");
        $query = $this->db->get("suspended_bills");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getTodaySales()
    {
        $sdate = date('Y-m-d 00:00:00');
        $edate = date('Y-m-d 23:59:59');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('sales.date >=', $sdate)->where('payments.date <=', $edate);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getCosting()
    {
        $date = date('Y-m-d');
        $this->db->select('SUM( COALESCE( purchase_unit_cost, 0 ) * quantity ) AS cost, SUM( COALESCE( sale_unit_price, 0 ) * quantity ) AS sales, SUM( COALESCE( purchase_net_unit_cost, 0 ) * quantity ) AS net_cost, SUM( COALESCE( sale_net_unit_price, 0 ) * quantity ) AS net_sales', FALSE)
            ->where('date', $date);

        $q = $this->db->get('costing');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayCCSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cc_slips, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'CC');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayCashSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'cash');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayRefunds()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS returned', FALSE)
            ->join('sales', 'sales.id=payments.return_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayReturns()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total', FALSE)
            ->where('date >=', $date);

        $q = $this->db->get('returns');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayExpenses()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE)
            ->where('date >=', $date);

        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayCashRefunds()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS returned', FALSE)
            ->join('sales', 'sales.id=payments.return_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'cash');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayChSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'Cheque');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayPPPSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'ppp');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayStripeSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'stripe');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayAuthorizeSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'authorize');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterSales($date, $user_id = NULL)
    {

        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('
                            SUM( COALESCE( grand_total, 0 ) - IF( paid = 0, COALESCE( tip_amount, 0 ), 0) ) AS total,
                            SUM( COALESCE( paid, 0 ) ) AS paid',
                        FALSE)
            ->where('sales.date >=', $date)
            ->where('sales.date <=', $this->close_register_end_date)
            ->where('sales.sale_status', 'completed')
            ->where('sales.created_by', $user_id);

        $q = $this->db->get('sales');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }


    public function getRegisterCCSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cc_slips, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')
            ->where('payments.date >=', $date)
            ->where('payments.date <=', $this->close_register_end_date)
            ->where('paid_by', 'CC')
            ->where('payments.created_by', $user_id)
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterCCSalesRC($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cc_slips, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)
            ->where('paid_by', 'CC')
            ->where('payments.created_by', $user_id)
            ->like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturnedCCSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cc_slips, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'CC');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterCashSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'cash')
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterCashSalesRC($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'cash')
            ->like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturnedCashSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'cash');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterState($amount = NULL, $created_by = NULL){
        if ($created_by) {
            $user_register = $this->registerData($created_by);
            $register_open_time = $user_register ? $user_register->date : NULL;
        } else {
            if ($this->Owner || $this->Admin) {
                $user_register = $this->session->userdata('user_id') ? $this->registerData($this->session->userdata('user_id')) : NULL;
                $register_open_time = $user_register ? $user_register->date : NULL;
            } else {
                $register_open_time = $this->session->userdata('register_open_time');
            }
        }
        if ($register_open_time) {
            $this->close_register_end_date = isset($this->close_register_end_date) ? $this->close_register_end_date : date('Y-m-d H:i:s');
            $cash_sales = $this->getRegisterCashSales($register_open_time, $created_by);
            $rc_cash_sales = $this->getRegisterCashSalesRC($register_open_time, $created_by);
            $cash_sales_returned = $this->getRegisterReturnedCashSales($register_open_time, $created_by);
            $returns = $this->getRegisterReturns($register_open_time, $created_by);
            $expenses = $this->getRegisterExpenses($register_open_time, $created_by);
            $customer_deposits = $this->getRegisterDeposits($register_open_time, $created_by);
            $suppliers_deposits = $this->pos_model->getRegisterSupplierDeposits($register_open_time, $created_by);
            $purchase_payments = $this->pos_model->getPurchasesPayments($register_open_time, $created_by);
            $pos_register_movements = $this->pos_model->getRegisterMovements($register_open_time, $created_by);
            $tips_cash = $this->pos_model->getTipsByCash($register_open_time, $created_by);
            $shipping_cash = $this->pos_model->getShippingByCash($register_open_time, $created_by);
            $total_cash = ($rc_cash_sales ? $rc_cash_sales->paid : 0) +
                          ($cash_sales ? $cash_sales->paid : 0 ) +
                          ($cash_sales_returned ? $cash_sales_returned->paid : 0) +
                          ($created_by ? $user_register->cash_in_hand : $this->session->userdata('cash_in_hand')) -
                          ($returns ? $returns->total : 0) +
                          ($expenses ? $expenses->total : 0) +
                          ($customer_deposits ? $customer_deposits->total : 0) +
                          ($suppliers_deposits ? $suppliers_deposits->total : 0) +
                          ($purchase_payments ? $purchase_payments->ppayments : 0) +
                          ($tips_cash && isset($tips_cash['amount']) ? $tips_cash['amount'] : 0) +
                          ($shipping_cash ? $shipping_cash->amount : 0) +
                          ($pos_register_movements ? $pos_register_movements->amount_in : 0) +
                          ($pos_register_movements ? $pos_register_movements->amount_out : 0)
                          ;

            // return $total_cash;
            if ($amount != NULL) {
                $total_cash -= $amount;
            }

            if ($total_cash < 0) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }

    }


    public function getSalesByPaymentOption($popt_code, $date, $user_id = NULL, $detailed = false, $register_date = false)
    {

        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                payments.reference_no as payment_reference,
                                payments.amount as payment_amount,
                                payments.date as payment_date,
                                sales.reference_no as sale_reference,
                                sales.date as sale_date
                                ', FALSE);
        } else {
            $this->db->select('
                sales.id as sale_id,
                COUNT('.$this->db->dbprefix('sales').'.id) AS num_transactions,
                SUM(COALESCE(amount, 0)) AS paid,
                SUM(COALESCE('.$this->db->dbprefix("sales").'.rete_fuente_total, 0) + COALESCE('.$this->db->dbprefix("sales").'.rete_iva_total, 0) + COALESCE('.$this->db->dbprefix("sales").'.rete_ica_total, 0) + COALESCE('.$this->db->dbprefix("sales").'.rete_other_total, 0)) as total_rete
                ', FALSE);
        }
        $this->db->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('type', 'received')
            ->where('payments.date >= ', $date)
            ->where('payments.date <= ', $this->close_register_end_date)
            ->where('sales.date >= ', $date)
            ->where('sales.date <= ', $this->close_register_end_date)
            ->where('paid_by', $popt_code)
            ->where('payments.return_id', NULL)
            ->where('payments.created_by', $user_id);

        if ($detailed) {
            $this->db->group_by('sales.id');
        }

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getRegisterSalesRCByPaymentOption($popt_code, $date, $user_id = NULL, $detailed = false, $register_date = false)
    {

        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }


        if ($detailed) {
            $this->db->select('
                                payments.reference_no as payment_reference,
                                payments.amount as payment_amount,
                                payments.date as payment_date,
                                sales.reference_no as sale_reference,
                                sales.date as sale_date
                                ', FALSE);
        } else {
            $this->db->select('COUNT('.$this->db->dbprefix('payments').'.id) AS num_transactions, SUM(amount - ('.$this->db->dbprefix('payments').'.rete_fuente_total + '.$this->db->dbprefix('payments').'.rete_iva_total + '.$this->db->dbprefix('payments').'.rete_ica_total + '.$this->db->dbprefix('payments').'.rete_other_total)) AS paid,
                SUM('.$this->db->dbprefix("payments").'.rete_fuente_total + '.$this->db->dbprefix("payments").'.rete_iva_total + '.$this->db->dbprefix("payments").'.rete_ica_total + '.$this->db->dbprefix("payments").'.rete_other_total) as total_rete', FALSE);
        }


        $this->db->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('(type = "received" OR type = "cancelled")')
            ->where('payments.date >= ', $date)
            ->where('payments.date <= ', $this->close_register_end_date)
            ->where('paid_by', $popt_code)
            ->where('(sales.date < "'.$date.'" OR sales.date IS NULL)')
            ->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getRegisterReturnedSalesByPaymentOption($popt_code, $date, $user_id = NULL, $detailed = false, $register_date = false)
    {

        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }


        if ($detailed) {
            $this->db->select('
                                payments.reference_no as payment_reference,
                                payments.amount as payment_amount,
                                payments.date as payment_date,
                                sales.reference_no as sale_reference,
                                sales.date as sale_date
                                ', FALSE);
        } else {
            $this->db->select('
                                COUNT('.$this->db->dbprefix('payments').'.id) AS num_transactions,
                               SUM( COALESCE( amount, 0 ) ) AS paid,
                               SUM(
                                    COALESCE('.$this->db->dbprefix("sales").'.rete_fuente_total, 0) +
                                    COALESCE('.$this->db->dbprefix("sales").'.rete_iva_total, 0) +
                                    COALESCE('.$this->db->dbprefix("sales").'.rete_ica_total, 0) +
                                    COALESCE('.$this->db->dbprefix("sales").'.rete_other_total, 0)
                                ) as total_rete
                            ', FALSE);
        }

        $this->db->join('sales', 'sales.id=payments.sale_id', 'inner')->where('type', 'returned')
        ->where('payments.date >=', $date)
        ->where('payments.date <=', $this->close_register_end_date)
        ->where('paid_by', $popt_code);
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getRegisterRefunds($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS returned', FALSE)
            ->join('sales', 'sales.id=payments.return_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date);
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturns($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total', FALSE)
        ->where('date >=', $date)
        ->where('returns.created_by', $user_id);

        $q = $this->db->get('returns');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterCashRefunds($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS returned', FALSE)
            ->join('sales', 'sales.id=payments.return_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'cash');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterExpenses($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                reference,
                                date,
                                COALESCE(amount * -1, 0) as amount,
                                companies.name as supplier_name
                              ', FALSE);
        } else {
            $this->db->select('SUM( COALESCE( amount, 0 ) ) * -1 AS total', FALSE);
        }
        $this->db->join('companies', 'companies.id = expenses.supplier_id', 'left');
        $this->db->where('date >=', $date)
            ->where('date <=', $this->close_register_end_date);
        $this->db->where('expenses.created_by', $user_id);
        $q = $this->db->get('expenses');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getPurchasesPayments($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                payments.reference_no as payment_reference,
                                COALESCE( amount *-1, 0 ) AS payment_amount,
                                payments.date as payment_date,
                                purchases.reference_no as purchase_reference,
                                purchases.date as purchase_date
                                ', FALSE);
        } else {
            $this->db->select('SUM( COALESCE( amount *-1, 0 ) ) AS ppayments', FALSE);
        }
        $this->db->join('purchases', 'purchases.id=payments.purchase_id', 'left')
                 ->where('(type = "sent" OR type = "cancelled" OR purchases.payment_affects_register = 1)')
                 ->where('payments.date >=', $date)
                 ->where('payments.date <=', $this->close_register_end_date)
                 ->where('paid_by', 'cash')
                 ->where('purchase_type', '1');
        $this->db->where('payments.created_by', $user_id);
        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getRegisterDeposits($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }


        if ($detailed) {
            $this->db->select('
                                deposits.reference_no,
                                deposits.amount,
                                deposits.date,
                                companies.name
                                ', FALSE);
        } else {
            $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE);
        }


        $this->db->join('companies', 'companies.id = deposits.company_id');
        $this->db->where('deposits.created_by', $user_id)->where('paid_by', 'cash')
        ->where('date >=', $date)
        ->where('date <=', $this->close_register_end_date)
        ->where('companies.group_name', 'customer');

        $q = $this->db->get('deposits');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getRegisterSupplierDeposits($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }


        if ($detailed) {
            $this->db->select('
                                deposits.reference_no,
                                ( '.$this->db->dbprefix('deposits').'.amount * -1 ) as amount,
                                deposits.date,
                                companies.name
                                ', FALSE);
        } else {
            $this->db->select('SUM( COALESCE( amount * -1, 0 ) ) AS total', FALSE);
        }

        $this->db->join('companies', 'companies.id = deposits.company_id');
        $this->db->where('deposits.created_by', $user_id)->where('paid_by', 'cash')->where('affects_pos_register', '1')
        ->where('date >=', $date)
        ->where('date <=', $this->close_register_end_date)
        ->where('companies.group_name', 'supplier');

        $q = $this->db->get('deposits');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getRegisterDepositsOtherPayments($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE)
            ->where('date >=', $date)
            ->where('date <=', $this->close_register_end_date)
            ;
        $this->db->where('created_by', $user_id)->where('paid_by !=', 'cash');

        $q = $this->db->get('deposits');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterDueReturns($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_due_returns, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('type', 'returned')
            ->where('payments.date >=', $date)
            ->where('payments.date <=', $this->close_register_end_date)
            ->where('paid_by', 'due');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
    public function getRegisterDueSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_due_returns, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('type', 'devolución')
            ->where('payments.date >=', $date)
            ->where('payments.date <=', $this->close_register_end_date)
            ->where('paid_by', 'due');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterChSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')
            ->where('payments.date >=', $date)
            ->where('payments.date <=', $this->close_register_end_date)
            ->where('paid_by', 'Cheque');
            $this->db->where('payments.created_by', $user_id)
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterChSalesRC($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'Cheque');
            $this->db->where('payments.created_by', $user_id)
            ->like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturnedChSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'Cheque');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterGCSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'gift_card');
        $this->db->where('payments.created_by', $user_id)
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterGCSalesRC($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'gift_card');
        $this->db->where('payments.created_by', $user_id)
            ->like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturnedGCSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'gift_card');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterOtherSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'other');
        $this->db->where('payments.created_by', $user_id)
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterOtherSalesRC($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'other');
        $this->db->where('payments.created_by', $user_id)
            ->like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturnedOtherSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'other');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }


    public function getRegisterDepositSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'deposit');
        $this->db->where('payments.created_by', $user_id)
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterDepositSalesRC($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'deposit');
        $this->db->where('payments.created_by', $user_id)
            ->like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturnedDepositSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'deposit');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterPPPSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'ppp');
        $this->db->where('payments.created_by', $user_id)
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturnedPPPSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'ppp');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterStripeSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'stripe');
        $this->db->where('payments.created_by', $user_id)
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterReturnedStripeSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'returned')->where('payments.date >=', $date)->where('paid_by', 'stripe');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterAuthorizeSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >=', $date)->where('paid_by', 'authorize');
        $this->db->where('payments.created_by', $user_id)
            ->not_like($this->db->dbprefix('payments') . '.reference_no', $this->Settings->sma_payment_prefix, 'after');

        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function suspendSale($data = array(), $items = array(), $did = NULL, $table_id = NULL, $order_id = NULL)
    {
        $sData = array(
            'customer_id' => $data['customer_id'],
            'customer' => $data['customer'],
            'count' => $data['total_items'],
            'order_discount_id' => $data['order_discount_id'],
            'order_tax_id' => $data['order_tax_id'],
            'total' => $data['grand_total'],
            'biller_id' => $data['biller_id'],
            'seller_id' => $data['seller_id'],
            'warehouse_id' => $data['warehouse_id'],
            'suspend_note' => $data['suspend_note'],
            'address_id' => $data['address_id'],
            'created_by' => $this->session->userdata('user_id')
        );

        if ($order_id) {
            $sData['origin_order_sale_id'] = $order_id;
        }

        if (!empty($data["restobar_table_id"])) {
            $sData['table_id'] = $data["restobar_table_id"];
        }
        foreach ($items as $key => $item) {
            if (isset($item['product_gift_card_no'])) {
                unset($items[$key]['product_gift_card_no']);
                unset($items[$key]['product_gift_card_value']);
                unset($items[$key]['product_gift_card_expiry']);
            }
        }
        if ($did) {
            if ($this->db->update('suspended_bills', $sData, array('id' => $did)) && $this->db->delete('suspended_items', array('suspend_id' => $did))) {
                $addOn = array('suspend_id' => $did);
                end($addOn);

                foreach ($items as &$var) {
                    $var = array_merge($addOn, $var);
                }

                if ($this->db->insert_batch('suspended_items', $items)) {
                    // $this->sma->print_arrays($items);
                    return TRUE;
                }
            }
        } else {
            $sData['date'] = $data['date'];

            if ($this->db->insert('suspended_bills', $sData)) {
                $suspend_id = $this->db->insert_id();
                $addOn = array('suspend_id' => $suspend_id);
                end($addOn);

                foreach ($items as &$var) {
                    $var = array_merge($addOn, $var);
                }

                if ($this->db->insert_batch('suspended_items', $items)) {
                    return TRUE;
                }
            }

        }
        return FALSE;
    }

    public function deleteBill($id)
    {
        if ($this->db->delete('suspended_items', array('suspend_id' => $id)) && $this->db->delete('suspended_bills', array('id' => $id))) {
            return TRUE;
        }

        return FALSE;
    }

    public function getInvoicePayments($sale_id)
    {
        $q = $this->db->select('payments.*, IF('.$this->db->dbprefix('payment_methods').'.name IS NOT NULL, '.$this->db->dbprefix('payment_methods').'.name, '.$this->db->dbprefix('payments').'.paid_by) as paid_by_name, '.$this->db->dbprefix('payments').'.paid_by as paid_by')
                ->join('payment_methods', 'payment_methods.code = payments.paid_by', 'left')
                ->where('payments.sale_id', $sale_id)
                ->get("payments");
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
        return FALSE;
    }

    function stripe($amount = 0, $card_info = array(), $desc = '')
    {
        $this->load->admin_model('stripe_payments');
        //$card_info = array( "number" => "4242424242424242", "exp_month" => 1, "exp_year" => 2016, "cvc" => "314" );
        //$amount = $amount ? $amount*100 : 3000;
        unset($card_info['type']);
        $amount = $amount * 100;
        if ($amount && !empty($card_info)) {
            $token_info = $this->stripe_payments->create_card_token($card_info);
            if (!isset($token_info['error'])) {
                $token = $token_info->id;
                $data = $this->stripe_payments->insert($token, $desc, $amount, $this->default_currency->code);
                if (!isset($data['error'])) {
                    $result = array('transaction_id' => $data->id,
                        'created_at' => date($this->dateFormats['php_ldate'], $data->created),
                        'amount' => ($data->amount / 100),
                        'currency' => strtoupper($data->currency)
                    );
                    return $result;
                } else {
                    return $data;
                }
            } else {
                return $token_info;
            }
        }
        return false;
    }

    function paypal($amount = NULL, $card_info = array(), $desc = '')
    {
        $this->load->admin_model('paypal_payments');
        //$card_info = array( "number" => "5522340006063638", "exp_month" => 2, "exp_year" => 2016, "cvc" => "456", 'type' => 'MasterCard' );
        //$amount = $amount ? $amount : 30.00;
        if ($amount && !empty($card_info)) {
            $data = $this->paypal_payments->Do_direct_payment($amount, $this->default_currency->code, $card_info, $desc);
            if (!isset($data['error'])) {
                $result = array('transaction_id' => $data['TRANSACTIONID'],
                    'created_at' => date($this->dateFormats['php_ldate'], strtotime($data['TIMESTAMP'])),
                    'amount' => $data['AMT'],
                    'currency' => strtoupper($data['CURRENCYCODE'])
                );
                return $result;
            } else {
                return $data;
            }
        }
        return false;
    }

    public function authorize($authorize_data)
    {
        $this->load->library('authorize_net');
        // $authorize_data = array( 'x_card_num' => '4111111111111111', 'x_exp_date' => '12/20', 'x_card_code' => '123', 'x_amount' => '25', 'x_invoice_num' => '15454', 'x_description' => 'References');
        $this->authorize_net->setData($authorize_data);

        if( $this->authorize_net->authorizeAndCapture() ) {
            $result = array(
                'transaction_id' => $this->authorize_net->getTransactionId(),
                'approval_code' => $this->authorize_net->getApprovalCode(),
                'created_at' => date($this->dateFormats['php_ldate']),
            );
            return $result;
        } else {
            return array('error' => 1, 'msg' => $this->authorize_net->getError());
        }
    }

    public function addPayment($payment = array(), $customer_id = null, $retencion = array())
    {

        //TRATAMIENTO DE PAGO INDIVIDUAL CON RETENCIÓN APLICADA
        if ($retencion != null) {

            $rete_payment = array(
                'date'         => $payment['date'],
                'sale_id'      => $payment['sale_id'],
                'amount'       => $retencion['total_retenciones'],
                'reference_no' => 'retencion',
                'paid_by'      => 'retencion',
                'cheque_no'    => '',
                'cc_no'        => '',
                'cc_holder'    => '',
                'cc_month'     => '',
                'cc_year'      => '',
                'cc_type'      => '',
                'created_by'   => $this->session->userdata('user_id'),
                'type'         => 'received',
                'note'         => 'Retenciones',
            );

            unset($retencion['total_retenciones']);

            // exit(var_dump($data)."<br>".var_dump($retencion)."<br>".var_dump($rete_payment));

            if ($this->db->insert('payments', $rete_payment)) { //SE INSERTA EL PAGO DE RETENCIÓN
                if ($this->db->update('sales', $retencion, array('id' => $payment['sale_id']))) { //SE ACTUALIZAN DATOS DE RETENCIÓN EN LA COMPRA
                    # code...
                } else {
                    exit('Error al actualizar base de datos : '.$this->db->last_query());
                }
            } else {
                return FALSE;
            }

        }
        //TRATAMIENTO DE PAGO INDIVIDUAL CON RETENCIÓN APLICADA

      //$this->sma->print_arrays($payment);
      $data['reference_no'] = "";

      if (isset($payment['sale_id']) && isset($payment['paid_by']) && isset($payment['amount'])) {
        $payment['pos_paid'] = $payment['amount'];
        $inv = $this->getInvoiceByID($payment['sale_id']);
        $paid = $inv->paid + $payment['amount'];
        if ($payment['paid_by'] == 'ppp') {
          $card_info = array("number" => $payment['cc_no'], "exp_month" => $payment['cc_month'], "exp_year" => $payment['cc_year'], "cvc" => $payment['cc_cvv2'], 'type' => $payment['cc_type']);
          $result = $this->paypal($payment['amount'], $card_info);
          if (!isset($result['error'])) {
            $payment['transaction_id'] = $result['transaction_id'];
            $payment['date'] = $this->sma->fld($result['created_at']);
            $payment['amount'] = $result['amount'];
            $payment['currency'] = $result['currency'];
            unset($payment['cc_cvv2']);
            $this->db->insert('payments', $payment);
            $paid += $payment['amount'];
          } else {
            $msg[] = lang('payment_failed');
            if (!empty($result['message'])) {
              foreach ($result['message'] as $m) {
                $msg[] = '<p class="text-danger">' . $m['L_ERRORCODE'] . ': ' . $m['L_LONGMESSAGE'] . '</p>';
              }
            } else {
              $msg[] = lang('paypal_empty_error');
            }
          }
        } elseif ($payment['paid_by'] == 'stripe') {
          $card_info = array("number" => $payment['cc_no'], "exp_month" => $payment['cc_month'], "exp_year" => $payment['cc_year'], "cvc" => $payment['cc_cvv2'], 'type' => $payment['cc_type']);
          $result = $this->stripe($payment['amount'], $card_info);
          if (!isset($result['error'])) {
            $payment['transaction_id'] = $result['transaction_id'];
            $payment['date'] = $this->sma->fld($result['created_at']);
            $payment['amount'] = $result['amount'];
            $payment['currency'] = $result['currency'];
            unset($payment['cc_cvv2']);
            $this->db->insert('payments', $payment);
            $paid += $payment['amount'];
          } else {
            $msg[] = lang('payment_failed');
            $msg[] = '<p class="text-danger">' . $result['code'] . ': ' . $result['message'] . '</p>';
          }

        } elseif ($payment['paid_by'] == 'authorize') {
          $authorize_arr = array("x_card_num" => $payment['cc_no'], "x_exp_date" => ($payment['cc_month'].'/'.$payment['cc_year']), "x_card_code" => $payment['cc_cvv2'], 'x_amount' => $payment['amount'], 'x_invoice_num' => $inv->id, 'x_description' => 'Sale Ref '.$inv->reference_no.' and Payment Ref '.$payment['reference_no']);
          list($first_name, $last_name) = explode(' ', $payment['cc_holder'], 2);
          $authorize_arr['x_first_name'] = $first_name;
          $authorize_arr['x_last_name'] = $last_name;
          $result = $this->authorize($authorize_arr);
          if (!isset($result['error'])) {
            $payment['transaction_id'] = $result['transaction_id'];
            $payment['approval_code'] = $result['approval_code'];
            $payment['date'] = $this->sma->fld($result['created_at']);
            unset($payment['cc_cvv2']);
            $this->db->insert('payments', $payment);
            $paid += $payment['amount'];
          } else {
            $msg[] = lang('payment_failed');
            $msg[] = '<p class="text-danger">' . $result['msg'] . '</p>';
          }

        } else {
          if ($payment['paid_by'] == 'gift_card') {
            $gc = $this->site->getGiftCardByNO($payment['cc_no']);
            $this->db->update('gift_cards', array('balance' => ($gc->balance - $payment['amount'])), array('card_no' => $payment['cc_no']));
            $this->site->update_gift_card_balance_status(NULL, $payment['cc_no']);
          } elseif ($customer_id && $payment['paid_by'] == 'deposit') {
            $customer = $this->site->getCompanyByID($customer_id);
            $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$payment['amount'])), array('id' => $customer_id));
          }
          unset($payment['cc_cvv2']);
          $this->db->insert('payments', $payment);
          $paid += $payment['amount'];
        }
        if (!isset($msg)) {
            if ($this->site->getReference('rc') == $data['reference_no']) {
            $this->site->updateReference('rc');
          }
          $this->site->syncSalePayments($payment['sale_id']);
          if ($this->Settings->modulary == 1) { //Si se indicó que el POS es modular con Contabilidad.
              /* LLamado a la función wappsiContabilidad para pagos de ventas para alimentar las tablas del modulo de contabilidad */
              if($this->site->wappsiContabilidadPagoVentas($payment, $customer_id, $retencion)){}
          }
          return array('status' => 1, 'msg' => '');
        }
        return array('status' => 0, 'msg' => $msg);
      }
      return false;
    }

    public function addPrinter($data = array()) {
        if($this->db->insert('printers', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function updatePrinter($id, $data = array()) {
        if($this->db->update('printers', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deletePrinter($id) {
        if($this->db->delete('printers', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getPrinterByID($id) {
        $q = $this->db->get_where('printers', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllPrinters() {
        $q = $this->db->get('printers');
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductNames($term, $warehouse_id)
    { $limit = $this->Settings->max_num_results_display;
        $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";

        $this->db->select('products.*, FWP.quantity as quantity, categories.id as category_id, categories.name as category_name', FALSE)
            ->join($wp, 'FWP.product_id=products.id', 'left')
            // ->join('warehouses_products FWP', 'FWP.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('products.id');
        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("(products.track_quantity = 0 OR FWP.quantity > 0) AND FWP.warehouse_id = '" . $warehouse_id . "' AND "
                . "({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }
        // $this->db->order_by('products.name ASC');
        $this->db->where('categories.hide !=', '1');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }
    }


    public function getProductUnits($product_id){
        $q = $this->db->get_where('unit_prices', array('product_id' => $product_id));

        if ($q->num_rows() > 0) {
            // foreach (($q->result()) as $row) {
            //     $data[] = $row;
            // }
            // return $data;
            return $q->result();
        }

        return false;

    }

    public function getOpenRegisterByUserId($user_id){
        $or = $this->db->get_where('pos_register', array('user_id' => $user_id, 'status' => 'open'));
        if ($or->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function get_biller_data_by_biller_id($biller_id)
    {
        $this->db->select("*");
        $this->db->from("biller_data");
        $this->db->where("biller_id", $biller_id);
        $result = $this->db->get();

        return $result->row();
    }

    public function update_suspended_item($data, $id)
    {
        $this->db->where('id', $id);
        if ($this->db->update('suspended_items', $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function update_suspended_item_by_suspend_id($data, $suspend_id)
    {
        $this->db->where('suspend_id', $suspend_id);
        if ($this->db->update('suspended_items', $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function update_batch_suspended_item($data)
    {
        if ($this->db->update_batch("suspended_items", $data, "id") > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function get_preparation_orders_by_preparer_user($preparer_id = NULL, $table_num = NULL, $all_items = FALSE)
    {
        $this->db->select("t.numero AS 'numero_mesa',
                            sb.state AS estado_orden,
                            sb.date AS 'fecha_orden',
                            TIME(sb.date) AS 'hora_orden',
                            TIMESTAMPDIFF(MINUTE, date, '". date("Y-m-d H:i:s") ."') AS 'diferencia_minutos',
                            sb.id AS 'id_venta_suspendida',
                            sb.created_by AS 'id_vendedor',
                            u.first_name AS 'nombre_vendedor',
                            u.last_name AS 'apellido_vendedor',
                            p.name AS 'nombre_producto',
                            si.id AS 'id_producto_cuenta',
                            FLOOR(si.quantity) AS 'cantidad_producto',
                            si.state_readiness AS 'estado_producto',
                            si.preferences AS 'preferences',
                            si.browser_readiness_status,
                            c.preparer_id AS 'id_preparador',
                            t.tipo,
                            t.id AS id_mesa");
        $this->db->from("suspended_bills AS sb");
        $this->db->join("suspended_items AS si", "si.suspend_id = sb.id", "inner");
        $this->db->join("tables AS t", "t.id = sb.table_id", "inner");
        $this->db->join("products AS p", "p.id = si.product_id", "inner");
        $this->db->join("categories AS c", "c.id = p.category_id", "inner");
        $this->db->join("users AS u", "u.id = sb.created_by", "inner");
        if (!empty($preparer_id)) {
            $this->db->where("c.preparer_id", $preparer_id);
        }
        if (!empty($table_num)) {
            $this->db->where("sb.id", $table_num);
        }
        if ($all_items === FALSE) {
            $this->db->where("si.state_readiness != ". DISPATCHED);
        }
        $this->db->order_by("sb.date", 'desc');
        $this->db->order_by("p.id", 'asc');

        $response = $this->db->get();

        return $response->result();
    }

    public function get_register_details($register_id = NULL){

        if (!$register_id && isset($this->biller_id) && $this->biller_id) {
            $register = $this->db
                ->select('
                             SUM('.$this->db->dbprefix("pos_register").'.refunds) AS refunds,
                             SUM('.$this->db->dbprefix("pos_register").'.expenses) AS expenses,
                             SUM('.$this->db->dbprefix("pos_register").'.purchases_payments) AS purchases_payments,
                             SUM('.$this->db->dbprefix("pos_register").'.deposits) AS deposits,
                             SUM('.$this->db->dbprefix("pos_register").'.deposits_other_methods) AS deposits_other_methods,
                             SUM('.$this->db->dbprefix("pos_register").'.suppliers_deposits) AS suppliers_deposits,
                             SUM('.$this->db->dbprefix("pos_register").'.tips_cash) AS tips_cash,
                             SUM('.$this->db->dbprefix("pos_register").'.tips_other_methods) AS tips_other_methods,
                             SUM('.$this->db->dbprefix("pos_register").'.tips_due) AS tips_due,
                             SUM('.$this->db->dbprefix("pos_register").'.shipping_cash) AS shipping_cash,
                             SUM('.$this->db->dbprefix("pos_register").'.shipping_other_methods) AS shipping_other_methods,
                             SUM('.$this->db->dbprefix("pos_register").'.shipping_due) AS shipping_due,
                             SUM('.$this->db->dbprefix("pos_register").'.movements_in) AS movements_in,
                             SUM('.$this->db->dbprefix("pos_register").'.movements_out) AS movements_out,
                             SUM('.$this->db->dbprefix("pos_register").'.gc_topups_cash) AS gc_topups_cash,
                             SUM('.$this->db->dbprefix("pos_register").'.gc_topups_other) AS gc_topups_other,
                             SUM('.$this->db->dbprefix("pos_register").'.cash_in_hand) AS cash_in_hand,
                             SUM('.$this->db->dbprefix("pos_register").'.total_return_retention) AS total_return_retention,
                             SUM('.$this->db->dbprefix("pos_register").'.total_rc_retention) AS total_rc_retention,
                             SUM('.$this->db->dbprefix("pos_register").'.total_cheques_submitted) AS total_cheques_submitted,
                             SUM('.$this->db->dbprefix("pos_register").'.total_cc_slips_submitted) AS total_cc_slips_submitted,
                             SUM('.$this->db->dbprefix("pos_register").'.total_cash_submitted) AS total_cash_submitted,
                             SUM('.$this->db->dbprefix("pos_register").'.total_retention) AS total_retention
                        ')
                ->join('users', 'users.id = pos_register.user_id ')
                ->where("( REPLACE(REPLACE({$this->db->dbprefix('users')}.biller_id, '[\"', ''), '\"]', '') = {$this->biller_id} )")
                ->where('pos_register.status', 'close')
                ->where('pos_register.closed_at >=', $this->start_date)
                ->where('pos_register.closed_at <=', $this->end_date)
                ->get('pos_register');
            if ($register->num_rows() > 0) {
                $register = $register->row();
                $return_data['register'] = $register;
                $this->db
                    ->select('
                                payment_methods.code,
                                pos_register_items.pos_register_id,
                                pos_register_items.payment_method_id,
                                pos_register_items.category_id,
                                pos_register_items.payment_counted,
                                pos_register_items.description,
                                pos_register_items.category_quantity,
                                SUM('.$this->db->dbprefix("pos_register_items").'.payments_amount) AS payments_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.sales_amount) AS sales_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.devolutions_amount) AS devolutions_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.total_amount) AS total_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.payments_collections_amount) AS payments_collections_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.sold_gift_cards) AS sold_gift_cards,
                            ')
                    ->join('payment_methods', 'payment_methods.id = pos_register_items.payment_method_id', 'left')
                    ->join('pos_register', 'pos_register.id = pos_register_items.pos_register_id ')
                    ->join('users', 'users.id = pos_register.user_id ')
                    ->where("( REPLACE(REPLACE({$this->db->dbprefix('users')}.biller_id, '[\"', ''), '\"]', '') = {$this->biller_id} )");

                $this->db->where('pos_register.status', 'close');
                if (isset($this->start_date) && $this->start_date) {
                    $this->db->where('pos_register.closed_at >=', $this->start_date);
                    $this->db->where('pos_register.closed_at <=', $this->end_date);
                }

                $register_details_data = $this->db->group_by('pos_register_items.payment_method_id')->get('pos_register_items');
                if ($register_details_data->num_rows() > 0) {
                    $register_details = [];
                    $register_details_counted = [];
                    $register_details_categories = [];
                    $register_details_categories_expenses = [];
                    foreach (($register_details_data->result()) as $row) {
                        if ($row->category_id == NULL && $row->expense_category_id == NULL) {
                            if ($row->payment_counted == 0) {
                                $register_details[] = $row;
                            } else if ($row->payment_counted == 1) {
                                $register_details_counted[] = $row;
                            }
                        } else if ($row->expense_category_id != NULL) {
                            $register_details_categories_expenses[] = $row;
                        } else {
                            $register_details_categories[] = $row;
                        }
                    }
                    $return_data['register_details'] = $register_details;
                    $return_data['register_details_counted'] = $register_details_counted;
                    $return_data['register_details_categories'] = $register_details_categories;
                    $return_data['register_details_categories_expenses'] = $register_details_categories_expenses;
                    return $return_data;
                }
            }

        } else if (!$register_id && isset($this->serial) && $this->serial) {
            $register = $this->db
                ->select('
                             SUM('.$this->db->dbprefix("pos_register").'.refunds) AS refunds,
                             SUM('.$this->db->dbprefix("pos_register").'.expenses) AS expenses,
                             SUM('.$this->db->dbprefix("pos_register").'.purchases_payments) AS purchases_payments,
                             SUM('.$this->db->dbprefix("pos_register").'.deposits) AS deposits,
                             SUM('.$this->db->dbprefix("pos_register").'.deposits_other_methods) AS deposits_other_methods,
                             SUM('.$this->db->dbprefix("pos_register").'.suppliers_deposits) AS suppliers_deposits,
                             SUM('.$this->db->dbprefix("pos_register").'.tips_cash) AS tips_cash,
                             SUM('.$this->db->dbprefix("pos_register").'.tips_other_methods) AS tips_other_methods,
                             SUM('.$this->db->dbprefix("pos_register").'.tips_due) AS tips_due,
                             SUM('.$this->db->dbprefix("pos_register").'.shipping_cash) AS shipping_cash,
                             SUM('.$this->db->dbprefix("pos_register").'.shipping_other_methods) AS shipping_other_methods,
                             SUM('.$this->db->dbprefix("pos_register").'.shipping_due) AS shipping_due,
                             SUM('.$this->db->dbprefix("pos_register").'.movements_in) AS movements_in,
                             SUM('.$this->db->dbprefix("pos_register").'.movements_out) AS movements_out,
                             SUM('.$this->db->dbprefix("pos_register").'.gc_topups_cash) AS gc_topups_cash,
                             SUM('.$this->db->dbprefix("pos_register").'.gc_topups_other) AS gc_topups_other,
                             SUM('.$this->db->dbprefix("pos_register").'.cash_in_hand) AS cash_in_hand,
                             SUM('.$this->db->dbprefix("pos_register").'.total_return_retention) AS total_return_retention,
                             SUM('.$this->db->dbprefix("pos_register").'.total_rc_retention) AS total_rc_retention,
                             SUM('.$this->db->dbprefix("pos_register").'.total_cheques_submitted) AS total_cheques_submitted,
                             SUM('.$this->db->dbprefix("pos_register").'.total_cc_slips_submitted) AS total_cc_slips_submitted,
                             SUM('.$this->db->dbprefix("pos_register").'.total_cash_submitted) AS total_cash_submitted,
                             SUM('.$this->db->dbprefix("pos_register").'.total_retention) AS total_retention
                        ')
                ->join('users', 'users.id = pos_register.user_id ')
                ->where("{$this->db->dbprefix('users')}.user_pc_serial = '{$this->serial}'")
                ->where('pos_register.status', 'close')
                ->where('pos_register.closed_at >=', $this->start_date)
                ->where('pos_register.closed_at <=', $this->end_date)
                ->get('pos_register');
            if ($register->num_rows() > 0) {
                $register = $register->row();
                $return_data['register'] = $register;
                $this->db
                    ->select('
                                payment_methods.code,
                                pos_register_items.pos_register_id,
                                pos_register_items.payment_method_id,
                                pos_register_items.category_id,
                                pos_register_items.payment_counted,
                                pos_register_items.description,
                                pos_register_items.category_quantity,
                                SUM('.$this->db->dbprefix("pos_register_items").'.payments_amount) AS payments_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.sales_amount) AS sales_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.devolutions_amount) AS devolutions_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.total_amount) AS total_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.payments_collections_amount) AS payments_collections_amount,
                                SUM('.$this->db->dbprefix("pos_register_items").'.sold_gift_cards) AS sold_gift_cards,
                            ')
                    ->join('payment_methods', 'payment_methods.id = pos_register_items.payment_method_id', 'left')
                    ->join('pos_register', 'pos_register.id = pos_register_items.pos_register_id ')
                    ->join('users', 'users.id = pos_register.user_id ')
                    ->where("{$this->db->dbprefix('users')}.user_pc_serial = '{$this->serial}'");

                $this->db->where('pos_register.status', 'close');
                if (isset($this->start_date) && $this->start_date) {
                    $this->db->where('pos_register.closed_at >=', $this->start_date);
                    $this->db->where('pos_register.closed_at <=', $this->end_date);
                }

                $register_details_data = $this->db->group_by('pos_register_items.payment_method_id')->get('pos_register_items');
                if ($register_details_data->num_rows() > 0) {
                    $register_details = [];
                    $register_details_counted = [];
                    $register_details_categories = [];
                    $register_details_categories_expenses = [];
                    foreach (($register_details_data->result()) as $row) {
                        if ($row->category_id == NULL && $row->expense_category_id == NULL) {
                            if ($row->payment_counted == 0) {
                                $register_details[] = $row;
                            } else if ($row->payment_counted == 1) {
                                $register_details_counted[] = $row;
                            }
                        } else if ($row->expense_category_id != NULL) {
                            $register_details_categories_expenses[] = $row;
                        } else {
                            $register_details_categories[] = $row;
                        }
                    }
                    $return_data['register_details'] = $register_details;
                    $return_data['register_details_counted'] = $register_details_counted;
                    $return_data['register_details_categories'] = $register_details_categories;
                    $return_data['register_details_categories_expenses'] = $register_details_categories_expenses;
                    return $return_data;
                }
            }
        } else {

            $register = $this->db->get_where('pos_register', array('id' => $register_id));
            $register_details_data = $this->db->select('payment_methods.code, pos_register_items.*')
                ->join('payment_methods', 'payment_methods.id = pos_register_items.payment_method_id', 'left')
                ->where('pos_register_items.pos_register_id', $register_id)
                ->get('pos_register_items');
            if ($register->num_rows() > 0) {
                $register = $register->row();
                $return_data['register'] = $register;
                if ($register_details_data->num_rows() > 0) {
                    $register_details = [];
                    $register_details_counted = [];
                    $register_details_categories = [];
                    $register_details_categories_expenses = [];
                    foreach (($register_details_data->result()) as $row) {
                        if ($row->category_id == NULL && $row->expense_category_id == NULL) {
                            if ($row->payment_counted == 0) {
                                $register_details[] = $row;
                            } else if ($row->payment_counted == 1) {
                                $register_details_counted[] = $row;
                            }
                        } else if ($row->expense_category_id != NULL) {
                            $register_details_categories_expenses[] = $row;
                        } else {
                            $register_details_categories[] = $row;
                        }
                    }
                    $return_data['register_details'] = $register_details;
                    $return_data['register_details_counted'] = $register_details_counted;
                    $return_data['register_details_categories'] = $register_details_categories;
                    $return_data['register_details_categories_expenses'] = $register_details_categories_expenses;
                } else {
                    $return_data['register_details'] = false;
                    $return_data['register_details_counted'] = false;
                    $return_data['register_details_categories'] = false;
                    $return_data['register_details_categories_expenses'] = false;
                }
                return $return_data;
            }
        }
        return false;
    }


    public function getSalesByCategory($category_id = NULL, $date = null, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('C.id,
                            C.name,
                            SUM(SI.quantity) AS total_quantity,
                            SUM(IF(SI.subtotal > 0, SI.subtotal, 0)) as total_sales,
                            SUM(IF(SI.subtotal < 0, SI.subtotal, 0)) as total_devolutions,
                            SUM(S.order_discount) as order_discount')
                ->from('sales S')
                ->join('sale_items SI', 'SI.sale_id = S.id', 'inner')
                ->join('products P', 'SI.product_id = P.id', 'inner')
                ->join('categories C', 'P.category_id = C.id', 'inner')
                ->where('S.date >=', $date)
                ->where('S.date <=', $this->close_register_end_date)
                ->where('S.created_by', $user_id)
                ->group_by('C.id');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function getSalesByProduct($product_id = NULL, $date = null, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('P.id,
                            P.name,
                            P.code,
                            SUM(SI.quantity) AS total_quantity,
                            SUM(IF((SI.subtotal + COALESCE(SI.item_discount, 0)) > 0, (SI.subtotal + COALESCE(SI.item_discount, 0)), 0)) as total_sales,
                            SUM(IF((SI.subtotal + COALESCE(SI.item_discount, 0)) < 0, (SI.subtotal + COALESCE(SI.item_discount, 0)), 0)) as total_devolutions,
                            SUM(  CAST(((COALESCE(S.order_discount, 0)) * ((SI.subtotal)/(S.grand_total + COALESCE(S.order_discount, 0)))) AS DECIMAL(25,4)) + COALESCE(SI.item_discount, 0)  ) as order_discount')
                ->from('sales S')
                ->join('sale_items SI', 'SI.sale_id = S.id', 'inner')
                ->join('products P', 'SI.product_id = P.id', 'inner')
                ->where('S.date >=', $date)
                ->where('S.date <=', $this->close_register_end_date)
                ->where('S.created_by', $user_id)
                ->group_by('P.id');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function get_restobar_table_by_id($table_id){
        $q = $this->db->get_where('tables', ['id' => $table_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function update_restobar_table_status($table_id, $status = 1){
        $this->db->update('tables', ['estado' => $status], ['id' => $table_id]);
    }

    public function change_order_status($data, $suspend_sale_id) {
        $this->db->where("id", $suspend_sale_id);
        if ($this->db->update("suspended_bills", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function change_browser_readiness_state($data, $product_id) {
        $this->db->where("id", $product_id);
        if ($this->db->update("suspended_items", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function get_items_shipped_by_suspended_sale_id($id) {
        $this->db->select("*");
        $this->db->from("suspended_items");
        $this->db->where("suspend_id", $id);
        $this->db->where("state_readiness < ", CANCELLED);
        $response = $this->db->get();

        if ($response->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function get_state_suspended_sale_item($ordered_item_id)
    {
        $this->db->select("state_readiness");
        $this->db->from("suspended_items");
        $this->db->where("id", $ordered_item_id);
        $response = $this->db->get();

        return $response->row();
    }

    public function getProductPreferencesRestobar($preferencesId)
    {
        $this->db->where_in('id', $preferencesId);
        $this->db->from("product_preferences");
        $response = $this->db->get();

        return $response->result();
    }

    public function get_count_suspended_bills(){
        $q = $this->db->get('suspended_bills');
        if ($q->num_rows() > 0) {
            return $q->num_rows();
        } else {
            return 0;
        }
    }

    public function getTipsByCash($date, $user_id = NULL, $sale_tipping = array(), $payment_method_tipping = array())
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('tip_amount AS amount, sales.id as sale_id', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('(type = "received" or type = "returned")')
            ->where('payments.date >= ', $date)
            ->where('payments.date <= ', $this->close_register_end_date)
            ->where('sales.date >= ', $date)
            ->where('sales.date <= ', $this->close_register_end_date)
            ->where('paid_by', 'cash')
            ->where('sales.tip_amount IS NOT NULL')
            ->where('payments.return_id', NULL)
            ->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        $total_tipping = 0;
        $payment_method_tipping = [];
        if ($q && $q->num_rows() > 0) {
            foreach (($q->result()) as $inv) {
                $sale_tipping[$inv->sale_id] = 1;
                $total_tipping += $inv->amount;
                if (!isset($payment_method_tipping['cash'])) {
                    $payment_method_tipping['cash'] = $inv->amount;
                } else {
                    $payment_method_tipping['cash'] += $inv->amount;
                }
            }
            return ['sale_tipping' => $sale_tipping, 'amount' => $total_tipping, 'payment_method_tipping' => $payment_method_tipping];
        }
        return false;
    }

    public function getTipsByOtherPayments($date, $user_id = NULL, $sale_tipping = array(), $payment_method_tipping = array())
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('tip_amount AS amount, sales.id as sale_id, payments.paid_by', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('(type = "received" or type = "returned")')
            ->where('payments.date >= ', $date)
            ->where('payments.date <= ', $this->close_register_end_date)
            ->where('sales.date >= ', $date)
            ->where('sales.date <= ', $this->close_register_end_date)
            ->where('paid_by !=', 'cash')
            ->where('paid_by !=', 'due')
            ->where('sales.tip_amount IS NOT NULL')
            ->where('payments.return_id', NULL)
            ->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        $total_tipping = 0;
        if ($q && $q->num_rows() > 0) {
            foreach (($q->result()) as $inv) {
                if (!isset($sale_tipping[$inv->sale_id])) {
                    $sale_tipping[$inv->sale_id] = 1;
                    $total_tipping += $inv->amount;
                    if (!isset($payment_method_tipping[$inv->paid_by])) {
                        $payment_method_tipping[$inv->paid_by] = $inv->amount;
                    } else {
                        $payment_method_tipping[$inv->paid_by] += $inv->amount;
                    }
                }
            }
            return ['sale_tipping' => $sale_tipping, 'amount' => $total_tipping, 'payment_method_tipping' => $payment_method_tipping];
        }
        return ['sale_tipping' => $sale_tipping, 'payment_method_tipping' => $payment_method_tipping];
    }

    public function getTipsByDue($date, $user_id = NULL, $sale_tipping = array(), $payment_method_tipping = array())
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('tip_amount AS amount, sales.id as sale_id', FALSE)
            ->where('date >= ', $date)
            ->where('date <= ', $this->close_register_end_date)
            ->where('paid', 0)
            ->where('sales.created_by', $user_id);
        $q = $this->db->get('sales');
        $total_tipping = 0;
        if ($q && $q->num_rows() > 0) {
            foreach (($q->result()) as $inv) {
                $sale_tipping[$inv->sale_id] = 1;
                $total_tipping += $inv->amount;
                if (!isset($payment_method_tipping['due'])) {
                    $payment_method_tipping['due'] = $inv->amount;
                } else {
                    $payment_method_tipping['due'] += $inv->amount;
                }
            }
            return ['sale_tipping' => $sale_tipping, 'amount' => $total_tipping, 'payment_method_tipping' => $payment_method_tipping];
        }
        return ['sale_tipping' => $sale_tipping, 'payment_method_tipping' => $payment_method_tipping];
    }

    public function getShippingByCash($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $sale_shipping = [];
        $this->db->select('shipping AS amount, sales.id as sale_id', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('type', 'received')
            ->where('payments.date >= ', $date)
            ->where('payments.date <= ', $this->close_register_end_date)
            ->where('sales.date >= ', $date)
            ->where('sales.date <= ', $this->close_register_end_date)
            ->where('paid_by', 'cash')
            ->where('payments.amount > sales.shipping AND sales.shipping > 0')
            ->where('payments.return_id', NULL)
            ->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        $total_shipping = 0;
        $payment_method_shipping = [];
        if ($q && $q->num_rows() > 0) {
            foreach (($q->result()) as $inv) {
                $sale_shipping[$inv->sale_id] = 1;
                $total_shipping += $inv->amount;
                if (!isset($payment_method_shipping['cash'])) {
                    $payment_method_shipping['cash'] = $inv->amount;
                } else {
                    $payment_method_shipping['cash'] += $inv->amount;
                }
            }
            return ['sale_shipping' => $sale_shipping, 'amount' => $total_shipping, 'payment_method_shipping' => $payment_method_shipping];
        }
        return false;
    }

    public function getShippingByOtherPayments($date, $user_id = NULL, $sale_shipping = array(), $payment_method_shipping = array())
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('shipping AS amount, sales.id as sale_id, payments.paid_by', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('type', 'received')
            ->where('payments.date >= ', $date)
            ->where('payments.date <= ', $this->close_register_end_date)
            ->where('sales.date >= ', $date)
            ->where('sales.date <= ', $this->close_register_end_date)
            ->where('paid_by !=', 'cash')
            ->where('payments.amount > sales.shipping AND sales.shipping > 0')
            ->where('payments.return_id', NULL)
            ->where('payments.created_by', $user_id);
        $q = $this->db->get('payments');
        $total_shipping = 0;
        if ($q && $q->num_rows() > 0) {
            foreach (($q->result()) as $inv) {
                if (!isset($sale_shipping[$inv->sale_id])) {
                    $sale_shipping[$inv->sale_id] = 1;
                    $total_shipping += $inv->amount;
                    if (!isset($payment_method_shipping[$inv->paid_by])) {
                        $payment_method_shipping[$inv->paid_by] = $inv->amount;
                    } else {
                        $payment_method_shipping[$inv->paid_by] += $inv->amount;
                    }
                }
            }
            return ['sale_shipping' => $sale_shipping, 'amount' => $total_shipping, 'payment_method_shipping' => $payment_method_shipping];
        }
        return ['sale_shipping' => $sale_shipping, 'payment_method_shipping' => $payment_method_shipping];
    }

    public function getShippingByDue($date, $user_id = NULL, $sale_shipping = array(), $payment_method_shipping = array())
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('shipping AS amount, sales.id as sale_id', FALSE)
            ->where('date >= ', $date)
            ->where('date <= ', $this->close_register_end_date)
            ->where('paid', 0)
            ->where('sales.created_by', $user_id);
        $q = $this->db->get('sales');
        $total_shipping = 0;
        if ($q && $q->num_rows() > 0) {
            foreach (($q->result()) as $inv) {
                if (!isset($sale_shipping[$inv->sale_id])) {
                    $sale_shipping[$inv->sale_id] = 1;
                    $total_shipping += $inv->amount;
                    if (!isset($payment_method_shipping['due'])) {
                        $payment_method_shipping['due'] = $inv->amount;
                    } else {
                        $payment_method_shipping['due'] += $inv->amount;
                    }
                }
            }
            return ['sale_shipping' => $sale_shipping, 'amount' => $total_shipping, 'payment_method_shipping' => $payment_method_shipping];
        }
        return ['sale_shipping' => $sale_shipping, 'payment_method_shipping' => $payment_method_shipping];
    }

    public function add_movement($data, $destination_data = []){
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $ref = explode("-", $reference);
        $data['reference_no'] = $reference;
        $consecutive = $ref[1];
        if ($this->db->insert('pos_register_movements', $data)) {
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            $this->site->wappsiCOntabilidadMovimientosCaja($data);
            if (count($destination_data) > 0) {
                $referenceBiller = $this->site->getReferenceBiller($destination_data['biller_id'], $destination_data['document_type_id']);
                if($referenceBiller){
                    $reference = $referenceBiller;
                }
                $ref = explode("-", $reference);
                $destination_data['reference_no'] = $reference;
                $consecutive = $ref[1];
                if ($this->db->insert('pos_register_movements', $destination_data)) {
                    $this->site->updateBillerConsecutive($destination_data['document_type_id'], $consecutive+1);
                    $this->site->wappsiCOntabilidadMovimientosCaja($destination_data);
                }
            }

            return $reference;
        }
        return false;
    }

    public function get_register_movement($id){
        $q = $this->db->get_where('pos_register_movements', ['id' => $id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterMovements($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }


        if ($detailed) {
            $this->db->select('
                                reference_no,
                                date,
                                COALESCE( IF(movement_type = 1, amount     , 0) )  AS amount_in,
                                COALESCE( IF(movement_type = 2  or movement_type = 3, amount * -1, 0) ) AS amount_out,
                                date
                            ', FALSE);
        } else {
            $this->db->select('SUM( COALESCE( IF(movement_type = 1, amount     , 0), 0 ) ) AS amount_in,
                               SUM( COALESCE( IF(movement_type = 2 or movement_type = 3, amount * -1, 0), 0 ) ) AS amount_out, ', FALSE);
        }


        $this->db->where('created_by', $user_id)->where('date >=', $date)->where('date <=', $this->close_register_end_date);

        $q = $this->db->get('pos_register_movements');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function get_preparation_area_by_id($pid){
        $q = $this->db->get_where('preparation_areas', ['id' => $pid]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function update_inv_payment_method($id, $data, $customer_id, $ret_com){
        $inv = $this->getInvoiceByID($id);
        $txt_note = 'Pago editado por el usuario '.$this->session->userdata('first_name').' '.$this->session->userdata('last_name').', fecha edición '.date('Y-m-d H:i:s');
        $pm_commision_value = $ret_com['pm_commision_value'];
        $pm_commision_value_info = $ret_com['pm_commision_value_info'];
        $pm_commision_changed = $ret_com['pm_commision_changed'];
        $pm_retefuente_value = $ret_com['pm_retefuente_value'];
        $pm_retefuente_value_info = $ret_com['pm_retefuente_value_info'];
        $pm_retefuente_changed = $ret_com['pm_retefuente_changed'];
        $pm_reteiva_value = $ret_com['pm_reteiva_value'];
        $pm_reteiva_value_info = $ret_com['pm_reteiva_value_info'];
        $pm_reteiva_changed = $ret_com['pm_reteiva_changed'];
        $pm_reteica_value = $ret_com['pm_reteica_value'];
        $pm_reteica_value_info = $ret_com['pm_reteica_value_info'];
        $pm_reteica_changed = $ret_com['pm_reteica_changed'];
        foreach ($data as $payment_id => $paid_by) {
            $p_d = $this->db->get_where('payments', ['id'=>$payment_id])->row();
            $this->db->update('sales', ['payment_method'=>$paid_by], ['id'=>$p_d->sale_id]);
            if ($p_d->paid_by != $paid_by) {
                $this->db->insert('user_activities', [
                            'date' => date('Y-m-d H:i:s'),
                            'type_id' => 1,
                            'table_name' => 'payments',
                            'record_id' => $payment_id,
                            'user_id' => $this->session->userdata('user_id'),
                            'module_name' => $this->m,
                            'description' => $this->session->userdata('username').' cambio la forma de la factura '.$inv->reference_no.', el pago '.$p_d->reference_no.' cambió de '.$p_d->paid_by.' a '.$paid_by,
                        ]);
            }
            $data_retcom = [
                                'pm_commision_value' => NULL,
                                'pm_retefuente_value' => NULL,
                                'pm_reteiva_value' => NULL,
                                'pm_reteica_value' => NULL,
                            ];
            if (isset($pm_commision_value_info[$payment_id]) && !empty($pm_commision_value_info[$payment_id])) {
                if (isset($pm_commision_changed[$payment_id]) && $pm_commision_changed[$payment_id] == 1 && isset($pm_commision_value[$payment_id]) && $pm_commision_value[$payment_id] > 0) {
                    $data_retcom['pm_commision_value'] = $pm_commision_value[$payment_id];
                } else {
                    $data_retcom['pm_commision_value'] = $pm_commision_value_info[$payment_id];
                }
            }
            if (isset($pm_retefuente_value_info[$payment_id]) && !empty($pm_retefuente_value_info[$payment_id])) {
                if (isset($pm_retefuente_changed[$payment_id]) && $pm_retefuente_changed[$payment_id] == 1 && isset($pm_retefuente_value[$payment_id]) && $pm_retefuente_value[$payment_id] > 0) {
                    $data_retcom['pm_retefuente_value'] = $pm_retefuente_value[$payment_id];
                } else {
                    $data_retcom['pm_retefuente_value'] = $pm_retefuente_value_info[$payment_id];
                }
            }
            if (isset($pm_reteiva_value_info[$payment_id]) && !empty($pm_reteiva_value_info[$payment_id])) {
                if (isset($pm_reteiva_changed[$payment_id]) && $pm_reteiva_changed[$payment_id] == 1 && isset($pm_reteiva_value[$payment_id]) && $pm_reteiva_value[$payment_id] > 0) {
                    $data_retcom['pm_reteiva_value'] = $pm_reteiva_value[$payment_id];
                } else {
                    $data_retcom['pm_reteiva_value'] = $pm_reteiva_value_info[$payment_id];
                }
            }
            if (isset($pm_reteica_value_info[$payment_id]) && !empty($pm_reteica_value_info[$payment_id])) {
                if (isset($pm_reteica_changed[$payment_id]) && $pm_reteica_changed[$payment_id] == 1 && isset($pm_reteica_value[$payment_id]) && $pm_reteica_value[$payment_id] > 0) {
                    $data_retcom['pm_reteica_value'] = $pm_reteica_value[$payment_id];
                } else {
                    $data_retcom['pm_reteica_value'] = $pm_reteica_value_info[$payment_id];
                }
            }
            $this->db->update('payments', $data_retcom, ['id'=>$payment_id]);
            $prev_pmnt = $this->db->get_where('payments', ['id' => $payment_id])->row();
            if (!empty($prev_pmnt->affected_deposit_id)) {
                $affected_deposit = $this->db->get_where('deposits', ['id' => $prev_pmnt->affected_deposit_id])->row();
            }
            if ($paid_by == 'Credito') {
                $this->db->update('payments', ['amount' => 0, 'note' => $txt_note, 'affected_deposit_id' => NULL], ['id' => $payment_id]);
                if ($prev_pmnt->paid_by == 'deposit') {
                    $this->db->update('deposits', ['balance' => ($affected_deposit->balance + $prev_pmnt->amount)], ['id' => $prev_pmnt->affected_deposit_id]);
                }
            } else if ($paid_by == 'deposit') {
                if ($prev_pmnt->paid_by != 'deposit') {
                    $new_affected_deposit = $this->db->select('*')
                                                 ->from('deposits')
                                                 ->where('balance >=', $prev_pmnt->amount)
                                                 ->where('company_id', $customer_id)
                                                 ->order_by('id ASC')
                                                 ->limit(1)
                                                 ->get();
                    if ($new_affected_deposit->num_rows() > 0) {
                        $new_affected_deposit = $new_affected_deposit->row();
                        $this->db->update('deposits', ['balance' => ($new_affected_deposit->balance - $prev_pmnt->amount)], ['id' => $new_affected_deposit->id]);
                        $this->db->update('payments', ['paid_by' => $paid_by, 'note' => $txt_note, 'affected_deposit_id' => $new_affected_deposit->id], ['id' => $payment_id]);
                    } else {
                        exit('error');
                    }
                }
            } else {
                $this->db->update('payments', ['paid_by' => $paid_by, 'note' => $txt_note, 'affected_deposit_id' => NULL], ['id' => $payment_id]);
                if ($prev_pmnt->paid_by == 'deposit') {
                    $this->db->update('deposits', ['balance' => ($affected_deposit->balance + $prev_pmnt->amount)], ['id' => $prev_pmnt->affected_deposit_id]);
                }
            }
        }
        return true;
    }

    public function get_all_preparation_order()
    {
        $this->db->select("date(date) as date, TIME(date) as time, CONCAT(first_name, ' ', last_name) as nombre, tipo, numero, product_name, state_readiness");
        $this->db->join('suspended_items', 'suspended_items.suspend_id = suspended_bills.id', 'inner');
        $this->db->join('tables', 'tables.id = suspended_bills.table_id', 'inner');
        $this->db->join('products', 'products.id = suspended_items.product_id', 'inner');
        $this->db->join('categories', 'categories.id = products.category_id', 'inner');
        $this->db->join('users', 'users.id = categories.preparer_id', 'inner');
        $this->db->where('DATE(date) = CURDATE()');
        $query = $this->db->get('suspended_bills');

        return $query->result();
    }

    public function select_unprinted_documents($userDocumentType)
    {
        $this->db->select("sales.id,
            date,
            reference_no,
            biller,
            customer,
            grand_total,
            printed,
            factura_electronica,
            cufe");
        $this->db->from("sales");
        $this->db->where("printed", "0");
        $this->db->where("CAST(date AS DATE) between date_sub(CURDATE(), interval 2 DAY) AND CURDATE()");
        $this->db->join("documents_types dt", "dt.id = sales.document_type_id");
        if (!empty($userDocumentType)) {
            $this->db->where_in("document_type_id", $userDocumentType);
        }

        $query1 = $this->db->get_compiled_select();

        $this->db->select("id,
            date,
            reference_no,
            biller,
            customer,
            grand_total,
            printed,
            0,
            cufe");
        $this->db->from("sales");
        $this->db->where("printed", "1");
        $this->db->where("CAST(date AS DATE) between date_sub(CURDATE(), interval 2 DAY) AND CURDATE()");
        if (!empty($userDocumentType)) {
            $this->db->where_in("document_type_id", $userDocumentType);
        }

        $this->db->order_by("printed", "ASC");
        $this->db->order_by("date", "DESC");
        $this->db->limit("20");
        $query2 = $this->db->get_compiled_select();

        $query = $this->db->query($query1 ." UNION ". $query2);

        return $query->result();
    }

    public function update_print_status($document_id)
    {
        $this->db->where("id", $document_id);
        if ($this->db->update("sales", ["printed" => 1])) {
            return TRUE;
        }

        return FALSE;
    }

    public function get_invoice_pos_register_status($inv){
        $q = $this->db->get_where('pos_register', ['date <= ' => $inv->date, 'user_id' => $inv->created_by, 'status' => 'open']);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function recontabilizar_movimiento_caja($id){
        $q = $this->db->get_where('pos_register_movements', ['id' => $id]);
        if ($q->num_rows() > 0) {
            $mv = $q->row_array();
            $this->site->wappsiCOntabilidadMovimientosCaja($mv);
        }
    }

    public function getRegisterPurchasesExpenses($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                purchases.reference_no,
                                purchases.date,
                                COALESCE(tbl_payment.total_paid, 0) * -1 as amount,
                                companies.name as supplier_name
                              ', FALSE);
        } else {
            $this->db->select('SUM( COALESCE( tbl_payment.total_paid, 0 ) ) * -1 AS total', FALSE);
        }
        $this->db->join('(
                        SELECT purchase_id, SUM(amount) AS total_paid FROM '.$this->db->dbprefix('payments').' WHERE purchase_id IS NOT NULL AND paid_by = "cash" AND type = "sent" GROUP BY purchase_id
                        ) AS tbl_payment', 'tbl_payment.purchase_id = purchases.id', 'left')
                ->join('companies', 'companies.id = purchases.supplier_id');
        $this->db->where('purchases.purchase_type', 2);
        $this->db->where('purchases.date >=', $date)->where('purchases.date <=', $this->close_register_end_date);
        $this->db->where('purchases.created_by', $user_id);
        $q = $this->db->get('purchases');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getRegisterPurchasesExpensesPayments($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                purchases.reference_no,
                                purchases.date,
                                COALESCE(amount * -1, 0  ) as amount,
                                companies.name as supplier_name
                              ', FALSE);
        } else {
            $this->db->select('SUM( COALESCE( '.$this->db->dbprefix("payments").'.amount, 0 ) ) * -1 AS total', FALSE);
        }
        $this->db->join('purchases', 'purchases.id = payments.purchase_id', 'left')
                ->join('companies', 'companies.id = purchases.supplier_id');
        $this->db->where('payments.purchase_id IS NOT NULL');
        $this->db->where('payments.paid_by', 'cash');
        $this->db->where('payments.type', 'sent');
        $this->db->where('purchases.date <', $date);
        $this->db->where('purchases.purchase_type =', 2);
        $this->db->where('payments.date >=', $date);
        $this->db->where('payments.date <=', $this->close_register_end_date);
        $this->db->where('payments.created_by', $user_id);
        $q = $this->db->get('payments');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function get_gift_card_topups($date, $user_id = NULL, $detailed = false, $register_date = false, $paid_by = 'cash')
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                gift_card_topups.reference_no as payment_reference,
                                COALESCE( '.$this->db->dbprefix('gift_card_topups').', 0 ) AS payment_amount,
                                gift_card_topups.date as payment_date,
                                gift_cards.card_no
                                ', FALSE);
        } else {
            $this->db->select('SUM( COALESCE( '.$this->db->dbprefix('gift_card_topups').'.amount, 0 ) ) AS payment_amount', FALSE);
        }
        $this->db->join('gift_cards', 'gift_cards.id=gift_card_topups.card_id', 'left')
                 ->where('gift_card_topups.paid_by IS NOT NULL')
                 ->where('gift_card_topups.date >=', $date)
                 ->where('gift_card_topups.date <=', $this->close_register_end_date)
                 ;
        if ($paid_by == 'cash') {
            $this->db->where('gift_card_topups.paid_by', $paid_by);
        } else {
            $this->db->where('gift_card_topups.paid_by !=', 'cash');
        }
        $this->db->where('gift_card_topups.created_by', $user_id);
        $q = $this->db->get('gift_card_topups');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                // foreach (($q->result()) as $row) {
                //     $data[] = $row;
                // }
                // return $data;
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }
    public function convert_to_order($data, $products, $convert_order_doc_type, $convert_order_data, $did = NULL){
        $sale_id = false;
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $convert_order_doc_type);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];

        $osl_data = array(
                'date' => $data['date'],
                'sale_status' => 'pending',
                'reference_no' => $data['reference_no'],
                'customer_id' => $convert_order_data['convert_order_as'] == 2 ? $convert_order_data['convert_order_employer_id'] : $data['customer_id'],
                'customer' => $convert_order_data['convert_order_as'] == 2 ? $convert_order_data['convert_order_employer_name'] : $data['customer'],
                'biller_id' => $data['biller_id'],
                'biller' => $data['biller'],
                'warehouse_id' => $data['warehouse_id'],
                'note' => $data['note'],
                'staff_note' => $data['staff_note'],
                'total' => $data['total'],
                'product_discount' => $data['product_discount'],
                'order_discount_id' => $data['order_discount_id'],
                'order_discount' => $data['order_discount'],
                'total_discount' => $data['total_discount'],
                'product_tax' => $data['product_tax'],
                'order_tax_id' => $data['order_tax_id'],
                'order_tax' => $data['order_tax'],
                'total_tax' => $data['total_tax'],
                'shipping' => $data['shipping'],
                'grand_total' => $data['grand_total'],
                'total_items' => $data['total_items'],
                'paid' => $data['paid'],
                'seller_id' => $data['seller_id'],
                'payment_method' => NULL,
                'address_id' => $convert_order_data['convert_order_as'] == 2 ? $convert_order_data['convert_order_employer_branch'] : $data['address_id'],
                'created_by' => $data['created_by'],
                'hash' => $data['hash'],
                'payment_term' => $data['payment_term'],
                'document_type_id' => $convert_order_doc_type,
                'contact_id' => $convert_order_data['convert_order_as'] == 2 ? $data['customer_id'] : NULL
            );
        if ($this->db->insert('order_sales', $osl_data)) {
            $sale_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($convert_order_doc_type, $consecutive+1);
            foreach ($products as $product) {
                $osl_product = array(
                        'product_id' => $product['product_id'],
                        'sale_id' => $sale_id,
                        'product_code' => $product['product_code'],
                        'product_name' => $product['product_name'],
                        'product_type' => $product['product_type'],
                        'option_id' => $product['option_id'],
                        'net_unit_price' => $product['net_unit_price'],
                        'unit_price' => $product['unit_price'],
                        'quantity' => $product['quantity'],
                        'product_unit_id' => $product['product_unit_id'],
                        'product_unit_code' => $product['product_unit_code'],
                        'unit_quantity' => $product['unit_quantity'],
                        'warehouse_id' => $product['warehouse_id'],
                        'item_tax' => $product['item_tax'],
                        'tax_rate_id' => $product['tax_rate_id'],
                        'tax' => $product['tax'],
                        'item_tax_2' => $product['item_tax_2'],
                        'tax_rate_2_id' => $product['tax_rate_2_id'],
                        'tax_2' => $product['tax_2'],
                        'discount' => $product['discount'],
                        'item_discount' => $product['item_discount'],
                        'subtotal' => $product['subtotal'],
                        'serial_no' => $product['serial_no'],
                        'real_unit_price' => $product['real_unit_price'],
                        'quantity_to_bill' => $product['quantity'],
                        'price_before_tax' => $product['price_before_tax'],
                        'preferences' => $product['preferences'],
                    );
                $this->db->insert('order_sale_items', $osl_product);
            }
        }
        return $sale_id;
    }
    public function getTableInSaleSuspended($tableId)
    {
        $this->db->where('table_id', $tableId);
        $q = $this->db->get('suspended_bills');

        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }

    public function get_pending_pa_orders($user_id, $sale_id = false){
        $data_1 = $data_2 = false;
        $this->db->select('
                            sales.date,
                            sales.reference_no,
                            sales.customer,
                            seller.name as seller,
                            sale_items.*')
                 ->join('sales', 'sales.id = sale_items.sale_id', 'inner')
                 ->join('products', 'products.id = sale_items.product_id', 'inner')
                 ->join('categories', 'categories.id = products.category_id', 'inner')
                 ->join('users', 'users.id = categories.preparer_id', 'inner')
                 ->join('companies seller', 'seller.id = sales.seller_id', 'inner')
                 ->where('users.id', $user_id)
                 ->where('sales.pos', 1)
                 ->where('sale_items.state_readiness', 1);
        if ($sale_id == false) {
            $this->db->group_by('sales.id');
        } else {
            $this->db->where('sales.id', $sale_id);
        }
        $q = $this->db->order_by('sales.date asc')->get('sale_items');
        if ($q->num_rows() > 0) {
            $data_1 = $q->result();
        }

        $this->db->select('
                            suspended_bills.date,
                            suspended_bills.suspend_note as reference_no,
                            suspended_bills.customer,
                            suspended_items.suspend_id as sale_id,
                            seller.name as seller,
                            suspended_items.*')
                 ->join('suspended_bills', 'suspended_bills.id = suspended_items.suspend_id', 'inner')
                 ->join('products', 'products.id = suspended_items.product_id', 'inner')
                 ->join('categories', 'categories.id = products.category_id', 'inner')
                 ->join('users', 'users.id = categories.preparer_id', 'inner')
                 ->join('companies seller', 'seller.id = suspended_bills.seller_id', 'inner')
                 ->where('users.id', $user_id)
                 ->where('(suspended_items.state_readiness = 1 OR suspended_items.state_readiness = 3)');
        if ($sale_id == false) {
            $this->db->group_by('suspended_bills.id');
        } else {
            $this->db->where('suspended_bills.id', $sale_id);
        }
        $q = $this->db->order_by('suspended_bills.date asc')->get('suspended_items');
        if ($q->num_rows() > 0) {
            $data_2 = $q->result();
        }
        $resultado = $data_2 ? ($data_1 ? array_merge($data_1, $data_2) : $data_2) : $data_1;
        return $resultado;
    }

    public function get_pos_register_movements($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                pos_register_movements.*
                                ', FALSE);
        } else {
            $this->db->select('
                                pos_register_movements.*
                                ', FALSE);
        }

        $this->db->where('date >= ', $date)
                ->where('date <= ', $this->close_register_end_date)
                 ->where('created_by', $user_id);

        $q = $this->db->get('pos_register_movements');
        if ($q && $q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function getPaymentsCollectionsByPaymentOption($popt_code, $date, $user_id = NULL, $detailed = false, $register_date = false)
    {

        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                payment_collection.reference_no as payment_reference,
                                payment_collection.amount as payment_amount,
                                payment_collection.date as payment_date,
                                companies.name as customer_name
                                ', FALSE);
        } else {
            $this->db->select('
                COUNT('.$this->db->dbprefix('payment_collection').'.id) AS num_transactions,
                SUM(COALESCE(amount, 0)) AS paid
                ', FALSE);
        }
        $this->db->join('companies', 'companies.id = payment_collection.customer_id', 'left')
            ->where('payment_collection.date >= ', $date)
            ->where('payment_collection.date <= ', $this->close_register_end_date)
            ->where('paid_by', $popt_code)
            ->where('payment_collection.created_by', $user_id);

        $q = $this->db->get('payment_collection');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function getGiftCardsSoldByPaymentOption($popt_code, $date, $user_id = NULL, $detailed = false, $register_date = false)
    {

        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                gift_cards.card_no as card_no,
                                gift_cards.value as value,
                                gift_cards.date as date,
                                companies.name as customer_name
                                ', FALSE);
        } else {
            $this->db->select('
                COUNT('.$this->db->dbprefix('gift_cards').'.id) AS num_transactions,
                SUM(COALESCE(value, 0)) AS paid
                ', FALSE);
        }
        $this->db->join('companies', 'companies.id = gift_cards.customer_id', 'left')
            ->where('gift_cards.creation_type', 4)
            ->where('gift_cards.status', 1)
            ->where('gift_cards.date >= ', $date)
            ->where('gift_cards.date <= ', $this->close_register_end_date)
            ->where('paid_by', $popt_code)
            ->where('gift_cards.created_by', $user_id);

        $q = $this->db->get('gift_cards');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }
    public function getGiftCardsReturnedByPaymentOption($popt_code, $date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                gift_cards.card_no as card_no,
                                gift_cards.value as value,
                                gift_cards.date as date,
                                companies.name as customer_name
                                ', FALSE);
        } else {
            $this->db->select('
                COUNT('.$this->db->dbprefix('gift_cards').'.id) AS num_transactions,
                SUM(COALESCE((value * -1), 0)) AS paid
                ', FALSE);
        }
        $this->db->join('companies', 'companies.id = gift_cards.customer_id', 'left')
            ->where('gift_cards.creation_type', 4)
            ->where('gift_cards.status', 5)
            ->where('gift_cards.return_date >= ', $date)
            ->where('gift_cards.return_date <= ', $this->close_register_end_date)
            ->where('paid_by', $popt_code)
            ->where('gift_cards.return_by', $user_id);

        $q = $this->db->get('gift_cards');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }
    public function return_sale_pos_for_fe($sale_id, $documentTypeReturn)
    {
        $inv = $this->site->getSaleByID($sale_id);
        $inv_items = $this->getAllInvoiceItems($sale_id);
        $payment = $products = $si_return = [];
        $data_document_type_id = $documentTypeReturn;

        $data = array(
                        'date' => date('Y-m-d H:i:s'),
                        'sale_id' => $inv->id,
                        'address_id' => $inv->address_id,
                        'customer_id' => $inv->customer_id,
                        'customer' => $inv->customer,
                        'biller_id' => $inv->biller_id,
                        'biller' => $inv->biller,
                        'warehouse_id' => $inv->warehouse_id,
                        'note' => $inv->note,
                        'total' => 0-$inv->total,
                        'product_discount' => $inv->product_discount,
                        'order_discount_id' => $inv->order_discount_id,
                        'order_discount' => $inv->order_discount,
                        'total_discount' => $inv->total_discount,
                        'product_tax' => 0-$inv->product_tax,
                        'order_tax_id' => $inv->order_tax_id,
                        'order_tax' => $inv->order_tax,
                        'total_tax' => $inv->total_tax,
                        'surcharge' => $inv->surcharge,
                        'grand_total' => 0-$inv->grand_total,
                        'created_by' => $inv->created_by,
                        'return_sale_ref' => $inv->return_sale_ref,
                        'sale_status' => 'returned',
                        'pos' => $inv->pos,
                        'seller_id' => $inv->seller_id,
                        'document_type_id' => $inv->document_type_id,
                        'shipping' => $inv->shipping,
                        'payment_status' => $inv->payment_status == 'pending' || $inv->payment_status == 'due' ? 'paid' : 'partial',
                        'rete_fuente_percentage' => $inv->rete_fuente_percentage,
                        'rete_fuente_total' => $inv->rete_fuente_total,
                        'rete_fuente_account' => $inv->rete_fuente_account,
                        'rete_fuente_base' => $inv->rete_fuente_base,
                        'rete_iva_percentage' => $inv->rete_iva_percentage,
                        'rete_iva_total' => $inv->rete_iva_total,
                        'rete_iva_account' => $inv->rete_iva_account,
                        'rete_iva_base' => $inv->rete_iva_base,
                        'rete_ica_percentage' => $inv->rete_ica_percentage,
                        'rete_ica_total' => $inv->rete_ica_total,
                        'rete_ica_account' => $inv->rete_ica_account,
                        'rete_ica_base' => $inv->rete_ica_base,
                        'rete_other_percentage' => $inv->rete_other_percentage,
                        'rete_other_total' => $inv->rete_other_total,
                        'rete_other_account' => $inv->rete_other_account,
                        'rete_other_base' => $inv->rete_other_base,
                        'rete_fuente_id' => $inv->rete_fuente_id,
                        'rete_iva_id' => $inv->rete_iva_id,
                        'rete_ica_id' => $inv->rete_ica_id,
                        'rete_other_id' => $inv->rete_other_id,
                        'document_type_id' => $data_document_type_id,
                        'return_sale_ref' => $inv->reference_no,
                        'tip_amount' => 0-$inv->tip_amount,
                    );
        if ($inv->payment_status == 'paid' || $inv->payment_status == 'partial') {
            $pay_ref = $this->site->getReference('pay');
            $payments = $this->getPaymentsForSale($sale_id);
            foreach ($payments as $spayment) {
                $data_payment = array(
                    'date' => date('Y-m-d H:i:s'),
                    'reference_no' =>  strpos($spayment->reference_no, '-') !== false ? $pay_ref : $spayment->reference_no,
                    'amount' => 0-$spayment->amount,
                    'paid_by' => $spayment->paid_by,
                    'cheque_no' => $spayment->cheque_no,
                    'cc_no' => $spayment->cc_no,
                    'cc_holder' => $spayment->cc_holder,
                    'cc_month' => $spayment->cc_month,
                    'cc_year' => $spayment->cc_year,
                    'cc_type' => $spayment->cc_type,
                    'created_by' => $spayment->created_by,
                    'type' => 'returned',
                    'comm_base' => 0-$spayment->comm_base,
                    'comm_amount' => 0-$spayment->comm_amount,
                    'comm_perc' => $spayment->comm_perc,
                    'seller_id' => $spayment->seller_id,
                    'mean_payment_code_fe' => $spayment->mean_payment_code_fe
                );
                $payment[] = $data_payment;
            }
            if ($inv->payment_status == 'partial') {
                $pay_ref = $this->site->getReference('pay');
                $data_payment = array(
                    'date' => date('Y-m-d H:i:s'),
                    'reference_no' => $pay_ref,
                    'amount' => $inv->paid - $inv->grand_total,
                    'paid_by' => 'due',
                    'cheque_no' => NULL,
                    'cc_no' => NULL,
                    'cc_holder' => NULL,
                    'cc_month' => NULL,
                    'cc_year' => NULL,
                    'cc_type' => NULL,
                    'created_by' => $this->session->userdata('user_id'),
                    'type' => 'returned',
                    'mean_payment_code_fe' => "ZZZ"
                );
                $payment[] = $data_payment;
            }
        } else {
            $pay_ref = $this->site->getReference('pay');
            $data_payment = array(
                'date' => date('Y-m-d H:i:s'),
                'reference_no' => $pay_ref,
                'amount' => $data['grand_total'],
                'paid_by' => 'due',
                'cheque_no' => NULL,
                'cc_no' => NULL,
                'cc_holder' => NULL,
                'cc_month' => NULL,
                'cc_year' => NULL,
                'cc_type' => NULL,
                'created_by' => $this->session->userdata('user_id'),
                'type' => 'returned',
                'mean_payment_code_fe' => "ZZZ"
            );
            $payment[] = $data_payment;
        }

        foreach ($inv_items as $item) {
            $product = array(
                'product_id' => $item->product_id,
                'product_code' => $item->product_code,
                'product_name' => $item->product_name,
                'product_type' => $item->product_type,
                'option_id' => $item->option_id,
                'net_unit_price' => $item->net_unit_price,
                'unit_price' => $item->unit_price,
                'quantity' => 0-$item->quantity,
                'product_unit_id' => $item->product_unit_id,
                'product_unit_code' => $item->product_unit_code,
                'unit_quantity' => 0-$item->quantity,
                'warehouse_id' => $item->warehouse_id,
                'item_tax' => 0-$item->item_tax,
                'tax_rate_id' => $item->tax_rate_id,
                'tax' => $item->tax,
                'discount' => $item->discount,
                'item_discount' => 0-$item->item_discount,
                'subtotal' => 0-$item->subtotal,
                'serial_no' => $item->serial_no,
                'real_unit_price' => $item->real_unit_price,
                'price_before_tax' => $item->price_before_tax,
            );

            $products[] = $product;

            $si_return[] = array(
                'id' => $item->id,
                'sale_id' => $sale_id,
                'product_id' => $item->product_id,
                'option_id' => $item->option_id,
                'quantity' => $item->quantity,
                'warehouse_id' => $inv->warehouse_id,
            );
        }
        $this->session->unset_userdata('detal_post_processing_model');
        $this->sales_model->addSale($data, $products, $payment, $si_return);
        $this->site->syncSalePayments($sale_id);
    }

    public function getPaymentsForSale($sale_id, $arr = false)
    {
        $this->db->select('payments.*,
            (
                COALESCE('.$this->db->dbprefix('payments').'.rete_fuente_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_iva_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_ica_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_other_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_autoaviso_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_bomberil_total, 0)
            ) as rc_total_retention
            , users.first_name, users.last_name, type, payment_methods.name')
            ->join('users', 'users.id=payments.created_by', 'left')
            ->join('payment_methods', 'payment_methods.code=payments.paid_by', 'left')
            ;
        // $this->db->where('(paid_by != "due" AND paid_by != "retencion" AND paid_by != "discount")');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                foreach (($q->result_array()) as $row) {
                    $row['sale_id'] = $sale_id;
                    $data[] = $row;
                }
            }
            return $data;
        }
        return FALSE;
    }

    public function getPaidInstallmentsByPaymentOption($popt_code, $date, $user_id = NULL, $detailed = false, $register_date = false)
    {

        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if ($detailed) {
            $this->db->select('
                                gift_cards.card_no as card_no,
                                gift_cards.value as value,
                                gift_cards.payment_date as date,
                                companies.name as customer_name
                                ', FALSE);
        } else {
            $this->db->select('
                COUNT('.$this->db->dbprefix('credit_financing_installments_payment').'.id) AS num_transactions,
                SUM(COALESCE(installment_paid_amount, 0)) AS paid
                ', FALSE);
        }
        $this->db
            ->where('credit_financing_installments_payment.payment_date >= ', $date)
            ->where('credit_financing_installments_payment.payment_date <= ', $this->close_register_end_date)
            ->where('paid_by', $popt_code)
            ->where('credit_financing_installments_payment.created_by', $user_id);

        $q = $this->db->get('credit_financing_installments_payment');
        if ($q && $q->num_rows() > 0) {
            if ($detailed) {
                return $q->result();
            } else {
                return $q->row();
            }
        }
        return false;
    }

    public function mark_all_sales_as_printed($electronic = 0)
    {
        $sql = "UPDATE 
                sma_sales
            INNER JOIN 
                sma_documents_types  ON sma_documents_types.id = sma_sales.document_type_id
            SET 
                sma_sales.printed = 1
            WHERE 
                sma_documents_types.factura_electronica = $electronic
                    AND sma_sales.pos = 1
                    AND sma_sales.printed != 1";

        $this->db->query($sql);

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function get_price_minimun($id){
        $this->db->where('product_prices.product_id', $id);
        $this->db->where('price_groups.minimun_list', '1');
        $this->db->join('price_groups', 'price_groups.id = product_prices.price_group_id', 'inner');
        $q = $this->db->get('product_prices');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getExpensesByCategory($date, $user_id = NULL, $detailed = false, $register_date = false)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('
                            expense_categories.id as id,
                            COUNT('.$this->db->dbprefix('expenses').'.id) AS num_transactions,
                            SUM( COALESCE( amount, 0 ) ) * -1 AS total,
                            expense_categories.name as expense_name
                            ', FALSE);
        $this->db->join('companies', 'companies.id = expenses.supplier_id', 'left')
                ->join('expense_categories', 'expense_categories.id = expenses.category_id', 'left');
        $this->db->where('date >=', $date)
            ->where('date <=', $this->close_register_end_date);
        $this->db->where('expenses.created_by', $user_id)
        ->group_by('expenses.category_id');
        $q = $this->db->get('expenses');
        if ($q && $q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }
}