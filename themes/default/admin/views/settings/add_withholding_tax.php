<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?></h3>
        </div>
        <?= admin_form_open("system_settings/save_withholding_tax", ["id"=>"add_withholding_tax_form"]); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= form_label(lang("withholding_tax_type")); ?>
                <?php $topts = [
                                ""=>lang('select'), 
                                "OTRA"=>lang('rete_other'), 
                                "IVA"=>"ReteIVA", 
                                "ICA"=>"ReteICA", 
                                "FUENTE"=>"ReteFuente",
                                "BOMB"=>"Tasa Bomberil",
                                "TABL"=>"Avisos y Tableros",
                                ]; ?>
                <?= form_dropdown(["name"=>"type", "id"=>"type", "class"=>"form-control select2", "required"=>TRUE], $topts); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("apply_on")); ?>
                <?php $topts = [""=>lang('select'), "ST"=>lang('subtotal'), "TO"=>lang('total'), "TX"=>lang('tax'), "IC"=>"Total ICA"]; ?>
                <?= form_dropdown(["name"=>"apply", "id"=>"apply", "class"=>"form-control select2", "required"=>TRUE], $topts); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("withholding_tax_description"), "description"); ?>
                <?= form_input(["name"=> "description", "id"=>"description", "class"=>"form-control", "required"=>TRUE]); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("withholding_tax_minimum_base"), "min_base"); ?>
                <?= form_input(["name"=> "min_base", "id"=>"min_base", "value"=>0, "class"=>"form-control", "type"=>"number", "required"=>TRUE, "min"=>"0"]); ?>
            </div>

            <div class="form-group" id="container_withholding_percentage_type">
                <?= form_label(lang("withholding_tax_percentage"), 'percentage'); ?>
                <?= form_input(["name"=> "percentage", "id"=>"percentage", "class"=>"form-control", "type"=>"number", "required"=>TRUE]); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("withholding_tax_transaction_type")); ?>
                <?php $aopts = [""=>lang("select"), "S"=>"Ventas", "P"=>"Compras"] ?>
                <?= form_dropdown(["name"=>"affects", "id"=>"affects", "class"=>"form-control select2", "required"=>TRUE], $aopts); ?>
            </div>

            <?php if ($this->Settings->modulary == YES): ?>
                <div class="form-group">
                    <?= form_label(lang("withholding_tax_accounting_account", "account_id")); ?>
                    <?php
                        $lopts[''] = lang('select');

                        foreach ($ledgers as $ledger) {
                            $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                        }
                    ?>
                    <?= form_dropdown(["name"=>"account_id", "id"=>"account_id", "class"=>"form-control select2", "required"=>TRUE], $lopts); ?>
                </div>

                <div class="form-group">
                    <?= form_label(lang("assumed_account_id", "assumed_account_id")); ?>
                    <?= form_dropdown(["name"=>"assumed_account_id", "id"=>"assumed_account_id", "class"=>"form-control select2"], $lopts); ?>
                </div>
            <?php endif ?>
        </div>
        <div class="modal-footer">
            <?= form_button("withholding_tax_add", lang("save"), 'class="btn btn-primary" id="withholding_tax_add"'); ?>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script>

    $("#add_withholding_tax_form").validate({ ignore: [] });
    $(document).on('click', '#withholding_tax_add', function() {
        if ($('#add_withholding_tax_form').valid()) {
            $('#add_withholding_tax_form').submit();
        }
    });

    $(document).on('change', '#type', function(){
        type = $(this);
        if (type.val() == 'IVA') {
            $('#apply').select2('val', 'TX').select2('readonly', true);
            // $('#affects').select2('val', '').select2('readonly', false);
        } else if (type.val() == 'ICA' || type.val() == 'FUENTE') {
            $('#apply').select2('val', 'ST').select2('readonly', true);
            // $('#affects').select2('val', '').select2('readonly', false);
        } else if (type.val() == 'BOMB' || type.val() == 'TABL') {
            $('#apply').select2('val', 'IC').select2('readonly', true);
            // $('#affects').select2('val', 'S').select2('readonly', true);
        } else {
            $('#apply').select2('val', '').select2('readonly', false);
            // $('#affects').select2('val', '').select2('readonly', false);
        }
    });
</script>
