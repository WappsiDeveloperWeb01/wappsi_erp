<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-content">
                    <?= admin_form_open("documentsReception/saveSettings"); ?>
                        <?= form_hidden('id', (!empty($settings)) ? $settings->id : '') ?>

                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('username') ?></label>
                                    <?php $username = (!empty($settings)) ? $settings->username : ''; ?>
                                    <?= form_input(['class'=>'form-control', 'type'=>'email', 'name'=>'username', 'id'=>'username', 'required'=>true], $username); ?>
                                    <small>Correo registrado para la recepción de facturas (DIAN)</small>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('work_environment') ?></label>
                                    <?php
                                        $workEnvironment = (!empty($settings)) ? $settings->work_environment : '';
                                        $workEnvironmentOpt = [
                                            $TESTING => $this->lang->line('testing'),
                                            $PRODUCTION => $this->lang->line('production'),
                                        ];
                                    ?>
                                    <?= form_dropdown(['class'=>'form-control', 'type'=>'email', 'name'=>'work_environment', 'id'=>'work_environment', 'required'=>true], $workEnvironmentOpt, $workEnvironment) ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('hostname') ?></label>
                                    <?php $hostname = (!empty($settings)) ? $settings->hostname : ''; ?>
                                    <?= form_input(['class'=>'form-control', 'name'=>'hostname', 'id'=>'hostname', 'required'=>true], $hostname) ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('hostUserName') ?></label>
                                    <?php $hostUserName = (!empty($settings)) ? $settings->hostUserName : ''; ?>
                                    <?= form_input(['class'=>'form-control', 'name'=>'hostUserName', 'id'=>'hostUserName', 'required'=>true], $hostUserName) ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('hostPassword') ?></label>
                                    <?php $hostPassword = (!empty($settings)) ? $settings->hostPassword : ''; ?>
                                    <div class="input-group">
                                        <?= form_input(['class'=>'form-control', 'name'=>'hostPassword', 'id'=>'hostPassword', 'type'=>'password', 'required'=>true]) ?>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-group-addon" type="button" id="seePasswordButton"><i class="fa fa-eye"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('hostnameDatabase') ?></label>
                                    <?php $hostnameDatabase = (!empty($settings)) ? $settings->hostnameDatabase : ''; ?>
                                    <?= form_input(['class'=>'form-control', 'name'=>'hostnameDatabase', 'id'=>'hostnameDatabase', 'required'=>true], $hostnameDatabase) ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <?= form_submit(['class'=>'btn btn-success pull-right'], 'Guardar'); ?>
                            </div>
                        </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('mousedown', '#seePasswordButton', function() { seePassword(); });
        $(document).on('mouseup', '#seePasswordButton', function() { notSeePassword(); });
    });

    function seePassword()
    {
        $('#hostPassword').prop('type', 'text');
    }

    function notSeePassword()
    {
        $('#hostPassword').prop('type', 'password');
    }
</script>