<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('update_status'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("system_settings/update_warehouse_status/" . $warehouse->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= lang('warehouse'); ?>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed <div class="col-lg-12"> table-borderless" style="margin-bottom:0;">
                        <tbody>
                            <tr>
                                <td><?= lang('code'); ?></td>
                                <td><?= $warehouse->code; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('name'); ?></td>
                                <td><?= $warehouse->name; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-group">
                <?= lang('status', 'status'); ?>
                <?php
                $opts = array('1' => lang('active'), '0' => lang('inactive'));
                ?>
                <?= form_dropdown('status', $opts, $warehouse->status, 'class="form-control" id="status" required="required" style="width:100%;"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('update', lang('update'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
