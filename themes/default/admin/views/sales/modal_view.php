<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
@media print {
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 3px !important;
    }
}
</style>

<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <div class="col-xs-12">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-2x">&times;</i>
                </button>
                <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="imprimir_factura_mayorista();">
                    <i class="fa fa-print"></i> <?= lang('print'); ?>
                </button>
            </div>
            <div id="contenido_factura">
                <div class="row bold">


                    <div class="col-xs-6">
                        <?php if ($logo) { ?>
                            <div class="text-center" style="margin-bottom:20px;">
                                <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                                     alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-6">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?php
                        $fecha_exp = strtotime("+".($inv->payment_term > 0 ? $inv->payment_term : 0)." day", strtotime($inv->date));
                        $fecha_exp = date('d/m/Y', $fecha_exp);
                         ?>
                        <?php if ($inv->sale_status != 'returned'): ?>
                         <?= lang("expiration_date"); ?>: <?= $fecha_exp; ?><br>
                        <?php endif ?>
                        <?= lang("sale_no_ref"); ?>: <?= $inv->reference_no; ?><br>
                        <?php if (!empty($inv->return_sale_ref)) {
                            echo lang("return_ref").': '.$inv->return_sale_ref;
                            if ($inv->return_id) {
                                echo ' <a data-target="#myModal2" data-toggle="modal" href="'.admin_url('sales/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                            } else {
                                echo '<br>';
                            }
                        } ?>
                        <?= lang("sale_status"); ?>: <?= lang($inv->sale_status); ?><br>
                        <?= lang("payment_status"); ?>: <?= lang($inv->payment_status); ?><br>
                        <?= lang("paid_by"); ?>: <?= lang($sale_main_payment_method); ?>
                        <?php if ($sale_payments_method != ''): ?>
                            <br><?= lang('payment_methods') ?>: <?= $sale_payments_method ?>
                        <?php endif ?>
                        <?php if ($inv->sale_origin): ?>
                            <br><?= lang('origin') ?> : <?= lang($inv->sale_origin) ?>
                            <br><?= lang('reference_origin') ?> : <?= $inv->sale_origin_reference_no ?>
                        <?php endif ?>
                        <?php if (!empty($inv->warehouse_id)) : ?>
                            <br><?= lang("warehouse"); ?>: <?= lang($warehouse->name); ?>
                        <?php endif ?>
                        <?php if (!empty($invoice_header)) : ?>
                            <br><?= $invoice_header; ?>
                        <?php endif ?>

                    </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>

            <div class="row" style="margin-bottom:15px;">

                <?php if ($Settings->invoice_view == 1) { ?>
                    <div class="col-xs-12 text-center">
                        <h1><?= $document_type ? $document_type->nombre :  lang('sale_invoice'); ?></h1>
                    </div>
                <?php } ?>

                <div class="col-xs-6">
                    <?php echo $this->lang->line("to"); ?>:<br/>
                    <h3 style="margin-top:10px;"><?= $customer->company ? $customer->company : $customer->name; ?></h3>
                    <?= $customer->company ? "" : "Attn: " . $customer->name ?>
                    <?php
                    if ($customer->vat_no != "-" && $customer->vat_no != "") {
                        if ($customer->tipo_documento == 6) {
                            $vatNo =  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion;
                        } else {
                            $vatNo =  lang("vat_no") . ": " . $customer->vat_no;
                        }
                    }
                     ?>
                    <?= $vatNo ?><br>

                    <?php if ($address): ?>
                        <span><?= $address->direccion ?><br>
                        <?= $address->postal_code." - ".$address->city.", ".$address->state ?><br>
                        <?= $address->country ?><br>
                        <?= $address->phone ?><br>
                    <?php else: ?>
                        <span><?= $customer->address ?><br>
                        <?= $customer->postal_code." - ".$customer->city.", ".$customer->state ?><br>
                        <?= $customer->country ?><br>
                        <?= $customer->phone ?><br>
                    <?php endif ?>
                        <?= $customer->email ?><br>

                    <?php

                    if ($customer->cf1 != "-" && $customer->cf1 != "") {
                        echo "<br>" . lang("ccf1") . ": " . $customer->cf1;
                    }
                    if ($customer->cf2 != "-" && $customer->cf2 != "") {
                        echo "<br>" . lang("ccf2") . ": " . $customer->cf2;
                    }
                    if ($customer->cf3 != "-" && $customer->cf3 != "") {
                        echo "<br>" . lang("ccf3") . ": " . $customer->cf3;
                    }
                    if ($customer->cf4 != "-" && $customer->cf4 != "") {
                        echo "<br>" . lang("ccf4") . ": " . $customer->cf4;
                    }
                    if ($customer->cf5 != "-" && $customer->cf5 != "") {
                        echo "<br>" . lang("ccf5") . ": " . $customer->cf5;
                    }
                    if ($customer->cf6 != "-" && $customer->cf6 != "") {
                        echo "<br>" . lang("ccf6") . ": " . $customer->cf6;
                    }

                    ?>
                </div>

                <div class="col-xs-6">
                    <?php echo $this->lang->line("from"); ?>:
                    <h3 style="margin-top:10px;"><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h3>
                    <span>
                        <?= $biller->address ?><br>
                        <?= $biller->postal_code." - ".$biller->city.", ".$biller->state ?><br>
                        <?= $biller->country ?><br>
                        <?php
                        echo "NIT : ".$this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '');
                         ?><br>
                        <?= $biller->phone ?><br>
                        <?= $biller->email ?><br>
                    </span>

                    <?php

                    if ($biller->cf1 != "-" && $biller->cf1 != "") {
                        echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                    }
                    if ($biller->cf2 != "-" && $biller->cf2 != "") {
                        echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                    }
                    if ($biller->cf3 != "-" && $biller->cf3 != "") {
                        echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                    }
                    if ($biller->cf4 != "-" && $biller->cf4 != "") {
                        echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                    }
                    if ($biller->cf5 != "-" && $biller->cf5 != "") {
                        echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                    }
                    if ($biller->cf6 != "-" && $biller->cf6 != "") {
                        echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                    }
                    ?>
                </div>

                <div class="clearfix"></div>

                <?php if ($seller): ?>
                    <div class="col-xs-12" style="padding-top: 1%;">
                        <p><b><?php echo lang('seller'); ?> :</b> <?= $seller->company != '-' ? $seller->company : $seller->name; ?></p>
                    </div>
                <?php endif ?>
                <?php if ($affiliate): ?>
                    <div class="col-xs-12" style="padding-top: 1%;">
                        <p><b><?php echo lang('affiliate'); ?> :</b> <?= $affiliate->company != '-' ? $affiliate->company : $affiliate->name; ?></p>
                    </div>
                <?php endif ?>

                <div class="clearfix"></div>

                <?php if ($inv->sale_currency != $this->Settings->default_currency): ?>
                    <div class="col-xs-6 no-print">
                        <label><?= lang('choose_sale_currency_view') ?></label>
                        <div class="row">
                            <div class="col-xs-6">
                                <select name="view_currency" id="view_currency" class="form-control">
                                    <option value="<?= $inv->sale_currency ?>"><?= $inv->sale_currency ?></option>
                                    <option value="<?= $this->Settings->default_currency ?>"><?= $this->Settings->default_currency ?></option>
                                </select>
                            </div>
                            <div class="col-xs-6 inv_currency_trm">
                                <input type="text" name="inv_currency_trm" class="form-control text-right" value="TRM : <?= $inv->sale_currency_trm ?>" readonly>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if (isset($cost_center) && $cost_center): ?>
                    <hr class="col-xs-11">
                    <div class="col-xs-12">
                        <label><?= lang('cost_center') ?></label>
                        <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                    </div>
                <?php endif ?>

            </div>

            <div class="inv_currency">
                <div class="table-responsive" style="min-height: 0px !important;">
                    <table class="table table-bordered table-hover print-table order-table">

                        <thead>

                        <tr>
                            <th><?= lang("no"); ?></th>
                            <th><?= lang("description"); ?></th>
                            <?php if ($Settings->indian_gst) { ?>
                                <th><?= lang("hsn_code"); ?></th>
                            <?php } ?>
                            <th><?= lang('gross_net_unit_price') ?></th>
                            <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                <th><?= lang("discount") ?></th>
                            <?php endif ?>
                            <th><?= lang("price_x_discount"); ?></th>
                            <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                <th><?= lang("tax") ?></th>
                            <?php endif ?>
                            <?php if ($Settings->ipoconsumo): ?>
                                <th><?= lang("second_product_tax") ?></th>
                            <?php endif ?>
                            <th><?= lang("price_x_tax"); ?></th>
                            <th><?= lang("quantity"); ?></th>
                            <th><?= lang("subtotal"); ?></th>
                        </tr>

                        </thead>

                        <tbody>

                        <?php $r = 1;

                        // var_dump($rows);
                        $tax_summary = array();

                        $tax_1 = 0;
                        // $tax_2 = 0;

                        foreach ($rows as $row):

                            $netunitpricetrm = $this->sma->formatDecimalNoRound($trmrate * $row->net_unit_price, 6);
                            $tax_1 += $row->item_tax;
                            // $tax_2 += $row->item_tax_2;

                        ?>
                            <tr>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    <?= $this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : ''; ?>
                                    <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                    <?= $row->preferences ? '<br>' . $this->sma->print_preference_selection($row->preferences) : ''; ?>
                                </td>
                                <?php if ($Settings->indian_gst) { ?>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                <?php } ?>
                                <td style="text-align:right; width:120px;">
                                    <?= $this->sma->formatMoney($trmrate * $row->price_before_tax); ?>
                                </td>
                                <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                    <td style="text-align:right; width:120px;">
                                        <?= $this->sma->get_percentage_from_amount($row->discount, $row->price_before_tax); ?>
                                    </td>
                                <?php endif ?>
                                <td style="text-align:right; width:100px;">
                                    <?= $trmrate != 1 ? $this->sma->formatMoney($netunitpricetrm) :  $this->sma->formatMoney($row->net_unit_price); ?>
                                </td>
                                <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                    <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                <?php endif ?>
                                <?php if ($Settings->ipoconsumo): ?>
                                    <td style="text-align:right; width:120px;"><?= $row->tax_2; ?></td>
                                <?php endif ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->unit_price); ?></td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                        if ($return_rows) {
                            echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                            foreach ($return_rows as $row):

                            $netunitpricetrm = $this->sma->formatDecimalNoRound($trmrate * $row->net_unit_price, 6);
                            ?>
                                <tr class="warning">
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                        <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                        <?= $row->details ? '<br>' . $row->details : ''; ?>
                                        <?= $this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : ''; ?>
                                        <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                    </td>
                                    <?php if ($Settings->indian_gst) { ?>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                    <?php } ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->price_before_tax); ?></td>
                                    <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->get_percentage_from_amount($row->discount, $row->price_before_tax); ?></td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:100px;"><?= $trmrate != 1 ? $this->sma->formatMoney($netunitpricetrm) :  $this->sma->formatMoney($row->net_unit_price); ?></td>
                                    <?php
                                    // if ($Settings->tax1 && $inv->product_tax > 0) {
                                    //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($trmrate * $row->item_tax) . '</td>';
                                    // }
                                    // if ($Settings->product_discount && $inv->product_discount != 0) {
                                    //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($trmrate * $row->item_discount) . '</td>';
                                    // }
                                    ?>
                                    <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                        <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                    <?php endif ?>
                                    <?php if ($Settings->ipoconsumo): ?>
                                        <td style="text-align:right; width:120px;"><?= $row->tax_2; ?></td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->unit_price); ?></td>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <?php
                        $col = $Settings->indian_gst ? 7 : 6;
                        if ($Settings->product_discount && $inv->product_discount != 0) {
                            $col++;
                        }
                        if ($Settings->tax1 && $inv->product_tax > 0) {
                            $col++;
                        }
                        if ($Settings->ipoconsumo) {
                            $col++;
                        }
                        if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                            $tcol = $col - 2;
                        } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                            $tcol = $col - 1;
                        } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                            $tcol = $col - 1;
                        } else {
                            $tcol = $col;
                        }
                        ?>
                        <?php
                        if ($return_sale) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $return_sale->grand_total) . '</td></tr>';
                        }
                        if ($inv->surcharge != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $inv->surcharge) . '</td></tr>';
                        }
                        ?>

                        <?php if ($Settings->indian_gst) {
                            if ($inv->cgst > 0) {
                                $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                                echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('cgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                            }
                            if ($inv->sgst > 0) {
                                $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                                echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('sgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                            }
                            if ($inv->igst > 0) {
                                $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                                echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('igst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                            }
                        } ?>

                        <?php if ($inv->order_discount != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($trmrate * ($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount)) . '</td></tr>';
                        }
                        ?>
                        <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * ($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax)) . '</td></tr>';
                        }
                        ?>
                        <?php if ($inv->shipping != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $inv->shipping) . '</td></tr>';
                        }

                        $total_retencion = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total + $inv->rete_autoaviso_total + $inv->rete_bomberil_total;
                        ?>
                        <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                            </td>
                            <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * ($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total)); ?></td>
                        </tr>
                            <?php if ($total_retencion > 0): ?>
                                <?php if ($inv->grand_total > 0): ?>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("retention"); ?>
                                            <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"> - <?= $this->sma->formatMoney($trmrate * $total_retencion); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                            <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * (($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - $total_retencion)); ?></td>
                                    </tr>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("retention"); ?>
                                            <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"> <?= $this->sma->formatMoney($total_retencion); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                            <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) + $total_retencion); ?></td>
                                    </tr>
                                <?php endif ?>
                            <?php endif ?>
                        <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; font-weight:bold;"><?= lang("paid"); ?>
                                <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                            </td>

                            <?php if ($inv->grand_total > 0): ?>
                                <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * (($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid) - $total_retencion)); ?></td>
                            <?php else: ?>
                                <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($trmrate * (($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid) + $total_retencion)*-1)); ?></td>
                            <?php endif ?>
                        </tr>
                        <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * (($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid))); ?></td>
                        </tr>

                        </tfoot>
                    </table>
                </div>
                <?php
                $inv_currency = new stdClass();
                $inv_currency->currency = $inv->sale_currency;
                $inv_currency->rate = 1 / ($inv->sale_currency_trm != 0 ? $inv->sale_currency_trm : 1);
                 ?>
                <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax), false, $inv_currency) : ''; ?>
            </div>

            <div class="default_currency" style="display: none;">
                <div class="table-responsive" style="min-height: 0px !important;">
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                         <tr>
                            <th><?= lang("no"); ?></th>
                            <th><?= lang("description"); ?></th>
                            <?php if ($Settings->indian_gst) { ?>
                                <th><?= lang("hsn_code"); ?></th>
                            <?php } ?>
                            <th><?= lang('gross_net_unit_price') ?></th>
                            <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                <th><?= lang("discount") ?></th>
                            <?php endif ?>
                            <th><?= lang("net_unit_price"); ?></th>
                            <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                <th><?= lang("tax") ?></th>
                            <?php endif ?>
                            <th><?= lang("unit_price"); ?></th>
                            <th><?= lang("quantity"); ?></th>
                            <th><?= lang("subtotal"); ?></th>
                        </tr>

                        </thead>

                        <tbody>

                        <?php $r = 1;

                        // var_dump($rows);

                        foreach ($rows as $row):
                            $trmrate = 1;
                        ?>
                            <tr>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    <?= $this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : ''; ?>
                                    <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                </td>
                                <?php if ($Settings->indian_gst) { ?>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                <?php } ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->price_before_tax); ?></td>
                                <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->get_percentage_from_amount($row->discount, $row->price_before_tax); ?></td>
                                <?php endif ?>
                                <td style="text-align:right; width:100px;"><?= $trmrate != 1 ? $netunitpricetrm :  $this->sma->formatMoney($row->net_unit_price); ?></td>
                                <?php
                                // if ($Settings->tax1 && $inv->product_tax > 0) {
                                //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($trmrate * $row->item_tax) . '</td>';
                                // }
                                // if ($Settings->product_discount && $inv->product_discount != 0) {
                                //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($trmrate * $row->item_discount) . '</td>';
                                // }
                                ?>
                                <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                    <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                <?php endif ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->unit_price); ?></td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                        if ($return_rows) {
                            echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                            foreach ($return_rows as $row):
                            ?>
                                <tr class="warning">
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                        <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                        <?= $row->details ? '<br>' . $row->details : ''; ?>
                                        <?= $this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : ''; ?>
                                        <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                    </td>
                                    <?php if ($Settings->indian_gst) { ?>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                    <?php } ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->price_before_tax); ?></td>
                                    <?php if ($Settings->product_discount && $inv->product_discount != 0): ?>
                                        <td style="text-align:right; width:120px;"><?= $this->sma->get_percentage_from_amount($row->discount, $row->price_before_tax); ?></td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:100px;"><?= $trmrate != 1 ? $netunitpricetrm :  $this->sma->formatMoney($row->net_unit_price); ?></td>
                                    <?php
                                    // if ($Settings->tax1 && $inv->product_tax > 0) {
                                    //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($trmrate * $row->item_tax) . '</td>';
                                    // }
                                    // if ($Settings->product_discount && $inv->product_discount != 0) {
                                    //     echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($trmrate * $row->item_discount) . '</td>';
                                    // }
                                    ?>
                                    <?php if ($Settings->tax1 && $inv->product_tax > 0): ?>
                                        <td style="text-align:right; width:120px;"><?= $row->tax; ?></td>
                                    <?php endif ?>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->unit_price); ?></td>
                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <?php
                        $col = $Settings->indian_gst ? 7 : 6;
                        if ($Settings->product_discount && $inv->product_discount != 0) {
                            $col++;
                        }
                        if ($Settings->tax1 && $inv->product_tax > 0) {
                            $col++;
                        }
                        if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                            $tcol = $col - 2;
                        } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                            $tcol = $col - 1;
                        } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                            $tcol = $col - 1;
                        } else {
                            $tcol = $col;
                        }
                        ?>
                        <!-- <?php if ($inv->grand_total != $inv->total) { ?>
                            <tr>
                                <td colspan="<?= $tcol; ?>"
                                    style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                    <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                </td>
                                <?php
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_tax+$return_sale->product_tax) : $inv->product_tax) . '</td>';
                                }
                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_discount+$return_sale->product_discount) : $inv->product_discount) . '</td>';
                                }
                                ?>
                                <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax)); ?></td>
                            </tr>
                        <?php } ?> -->
                        <?php
                        if ($return_sale) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->grand_total) . '</td></tr>';
                        }
                        if ($inv->surcharge != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->surcharge) . '</td></tr>';
                        }
                        ?>

                        <?php if ($Settings->indian_gst) {
                            if ($inv->cgst > 0) {
                                $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                                echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('cgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                            }
                            if ($inv->sgst > 0) {
                                $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                                echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('sgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                            }
                            if ($inv->igst > 0) {
                                $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                                echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('igst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                            }
                        } ?>

                        <?php if ($inv->order_discount != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount) . '</td></tr>';
                        }
                        ?>
                        <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</td></tr>';
                        }
                        ?>
                        <?php if ($inv->shipping != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                        }

                        $total_retencion = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total;
                        ?>
                        <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                            </td>
                            <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total); ?></td>
                        </tr>
                            <?php if ($total_retencion > 0): ?>
                                <?php if ($inv->grand_total > 0): ?>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("retention"); ?>
                                            <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"> - <?= $this->sma->formatMoney($total_retencion); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                            <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - $total_retencion); ?></td>
                                    </tr>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("retention"); ?>
                                            <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"> <?= $this->sma->formatMoney($total_retencion); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="<?= $col; ?>"
                                            style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                            <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) + $total_retencion); ?></td>
                                    </tr>
                                <?php endif ?>
                            <?php endif ?>
                        <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; font-weight:bold;"><?= lang("paid"); ?>
                                <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid) - $total_retencion); ?></td>
                        </tr>
                        <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                <span class="currency_text">(<?= $inv->sale_currency ? $inv->sale_currency : $this->Settings->default_currency; ?>)</span>
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid)); ?></td>
                        </tr>

                        </tfoot>
                    </table>
                </div>
                <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax)) : ''; ?>
            </div>


            <?php if ($inv->resolucion): ?>
                <div class="well well-sm">
                    <div><?= $inv->resolucion ?></div>
                </div>
            <?php endif ?>

            <p class="text-center"><b><?= isset($tipo_regimen->description) ? lang($tipo_regimen->description) : '' ?></b></p>

            <?php if ($invoice_footer): ?>
                <div class="well well-sm">
                    <div><?= $invoice_footer ?></div>
                </div>
            <?php endif ?>

            <div class="row">
                <div class="col-xs-12">
                    <?php
                        if ($inv->note || $inv->note != "") { ?>
                            <div class="well well-sm">
                                <p class="bold"><?= lang("note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->note); ?></div>
                            </div>
                        <?php
                        }
                        if ($inv->staff_note || $inv->staff_note != "") { ?>
                            <div class="well well-sm staff_note">
                                <p class="bold"><?= lang("staff_note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->staff_note); ?></div>
                            </div>
                        <?php } ?>

                </div>

                <!-- <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) { ?>
                <div class="col-xs-5 pull-left">
                    <div class="well well-sm">
                        <?=
                        '<p>'.lang('this_sale').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                        .'<br>'.
                        lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>';?>
                    </div>
                </div>
                <?php } ?> -->


            </div>


            <div class="row">
                <div class="col-xs-4 pull-left">
                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p style="border-bottom: 1px solid #666;">&nbsp;</p>

                    <p><?= lang("stamp_sign"); ?></p>
                    <p>
                        <?= lang("created_by"); ?>: <?= $inv->created_by ? $created_by->first_name . ' ' . $created_by->last_name : $customer->name; ?> <br>
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                    </p>
                </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="col-xs-12 text-center">
                <?php if ($inv->updated_by) { ?>
                <p>
                    <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                    <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                </p>
                <?php } ?>
            </div>
        </div>

    </div>
</div>


<?php if (!$Supplier || !$Customer) { ?>
    <div class="buttons row div_buttons_modal div_buttons_modal_lg">
        <div class="col-sm-3 col-xs-6">
            <a href='#' class='po btn btn-outline btn-primary' title='' data-placement="top" data-content="
                        <p> <?= lang('where_to_duplicate') ?> </p>
                        <a class='btn btn-primary' href='<?= admin_url('sales/add/?sale_id=').$inv->id ?>'> <?= lang('detal_sale') ?> </a>
                        <a class='btn btn-primary' href='<?= admin_url('pos/index/?duplicate=').$inv->id ?>'> <?= lang('pos_sale') ?> </a>
                        <button class='btn po-close'> <?= lang('cancel') ?> </button>" rel='popover' style="width: 100%;">
                        <i class='fa fa-download'></i>
                <?= lang('duplicate') ?>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6">
            <a href="<?= admin_url('sales/add_delivery/' . $inv->id) ?>" class="tip btn btn-outline btn-primary" title="<?= lang('add_delivery') ?>" data-toggle="modal" data-target="#myModal2" style="width: 100%;">
                <i class="fa fa-truck"></i>
                <span class="hidden-sm hidden-xs"><?= lang('delivery') ?></span>
            </a>
        </div>
        <?php if ($inv->attachment) { ?>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= admin_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-outline btn-primary" title="<?= lang('attachment') ?>" style="width: 100%;">
                    <i class="fa fa-chain"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                </a>
            </div>
        <?php } ?>
        <div class="col-sm-3 col-xs-6">
            <a href="<?= admin_url('sales/email/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal2" class="tip btn btn-outline btn-primary" title="<?= lang('email') ?>" style="width: 100%;">
                <i class="fa fa-envelope-o"></i>
                <span class="hidden-sm hidden-xs"><?= lang('email') ?></span>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6">
            <a href="<?= admin_url("sales/saleView/").$inv->id."/FALSE/TRUE" ?>" class="tip btn btn-outline btn-primary" title="<?= lang('download_pdf') ?>" style="width: 100%;">
                <i class="fa fa-download"></i>
                <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
            </a>
        </div>
        <?php if ( ! $inv->sale_id) { ?>
        <!-- <div class="col-sm-3 col-xs-6">
            <a href="<?= admin_url('sales/edit/' . $inv->id) ?>" class="tip btn btn-outline btn-warning sledit" title="<?= lang('edit') ?>">
                <i class="fa fa-edit"></i>
                <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
            </a>
        </div> -->
        <!-- <div class="col-sm-3 col-xs-6">
            <a href="#" class="tip btn btn-danger bpo" title="<b><?= $this->lang->line("delete_sale") ?></b>"
                data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('sales/delete/' . $inv->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                data-html="true" data-placement="top">
                <i class="fa fa-trash-o"></i>
                <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
            </a>
        </div> -->
        <?php } ?>
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });

    $('#view_currency').on('change', function(){
        actual_currency = $(this).val();
        default_currency = site.settings.default_currency;

        $('.currency_text').text("("+actual_currency+")");

        if (actual_currency != default_currency) {
            $('.inv_currency').css('display', '');
            $('.inv_currency_trm').css('display', '');
            $('.default_currency').css('display', 'none');
        } else {
            $('.inv_currency').css('display', 'none');
            $('.inv_currency_trm').css('display', 'none');
            $('.default_currency').css('display', '');
        }

    });
</script>

<script>
    $(document).ready(function(){
       $(document).on('click', '#boton_imprimir_anticipo', function() { imprimir_factura_mayorista(); });
    });

    function imprimir_factura_mayorista()
    {
        <?php if ($document_type->module_invoice_format_id == 0) { ?>
            var divToPrint=document.getElementById('contenido_factura');
            var newWin=window.open('','Print-Window');
            var header = $('head').html();
            newWin.document.open();
            newWin.document.write('<head>'+header+'</head><body onload="window.print()" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');
            newWin.document.close();
            setTimeout(function(){newWin.close();},1000);
        <?php } else { ?>
            window.open('<?= admin_url("sales/saleView/").$inv->id ?>');
        <?php } ?>
    }
</script>
