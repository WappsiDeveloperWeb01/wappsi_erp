<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$show_cost = true;
if (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) {
    $show_cost = false;
}
 ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <a type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/view_product_transformation_format/'.$inv[0]->origin_reference_no) ?>" target="_blank">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </a>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row bold">
                        <div class="col-xs-5">
                        <h3><?= $document_type->nombre ?></h3>
                        <h3><?= $inv[0]->origin_reference_no ?></h3>
                        <p class="bold">
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv[0]->date); ?><br>
                            <?= lang("warehouse"); ?>: <?= $warehouse->name; ?><br>
                            <?= lang("created_by"); ?>: <?= $created_by->first_name." ".$created_by->last_name; ?><br>
                            <?= lang("employee"); ?>: <?= $employee->company; ?><br>
                            <?php if ($inv[0]->origin_reference_no): ?>
                                <?= lang("origin"); ?>: <?= $inv[0]->origin_reference_no; ?><br>
                            <?php endif ?>
                        </p>
                        </div>
                        <div class="col-xs-7 text-right order_barcodes">

                        </div>
                        <div class="clearfix"></div>
                        <?php if (isset($cost_center) && $cost_center): ?>
                            <div class="col-xs-6">
                                <p class="bold">
                                    <?= lang("cost_center"); ?> : <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                                </p>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <hr class="col-md-11">

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <label><?= lang('adjustment_reference_no') ?></label>
                            <h3><?= $inv[0]->reference_no ?></h3>
                        </div>
                        <div class="col-md-12">
                            <label><?= lang('warehouse_origin') ?></label>
                            <h3><?= $warehouses[$inv[0]->warehouse_id]->name ?></h3>
                        </div>
                        <div class="col-md-12 table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><?= lang('no') ?></th>
                                        <th><?= lang('code') ?></th>
                                        <th><?= lang('product') ?></th>
                                        <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                                            <th><?= lang('serial_no') ?></th>
                                        <?php endif ?>
                                        <?php if ($show_cost): ?>
                                            <th><?= lang('unit_cost') ?></th>
                                        <?php endif ?>
                                        <th><?= lang('quantity') ?></th>
                                        <?php if ($show_cost): ?>
                                            <th><?= lang('cost') ?></th>
                                        <?php endif ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total_cost = 0; $cnt = 1;?>
                                    <?php foreach ($rows[$inv[0]->id] as $item): ?>
                                        <?php 
                                        if ($item->type == 'addition') {
                                            continue;
                                        }
                                         ?>
                                        <tr>
                                            <td><?= $cnt ?></td>
                                            <td><?= $item->code ?></td>
                                            <td><?= $item->name ?></td>
                                            <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                                                <td><?= $item->serial_no ?></td>
                                            <?php endif ?>
                                            <?php if ($show_cost): ?>
                                                <td><?= $this->sma->formatMoney($item->avg_cost) ?></td>
                                            <?php endif ?>
                                            <td><?= $this->sma->formatQuantity($item->quantity) ?></td>
                                            <?php if ($show_cost): ?>
                                                <td><?= $this->sma->formatMoney($item->avg_cost * $item->quantity) ?></td>
                                            <?php endif ?>
                                            <?php $total_cost += $item->avg_cost * $item->quantity; ?>
                                        </tr>
                                        <?php $cnt++; ?>
                                    <?php endforeach ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="<?= $show_cost ? 5 : 3 ?>">Total</th>
                                        <th class="text-right"><?= $this->sma->formatMoney($total_cost) ?></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <label><?= lang('adjustment_reference_no') ?></label>
                            <h3><?= isset($inv[1]->reference_no) ? $inv[1]->reference_no : $inv[0]->reference_no ?></h3>
                        </div>
                        <div class="col-md-12">
                            <label><?= lang('warehouse_destination') ?></label>
                            <h3><?= isset($inv[1]->warehouse_id) ? $warehouses[$inv[1]->warehouse_id]->name : $warehouses[$inv[0]->warehouse_id]->name ?></h3>
                        </div>
                        <div class="col-md-12 table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><?= lang('no') ?></th>
                                        <th><?= lang('code') ?></th>
                                        <th><?= lang('product') ?></th>
                                        <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                                            <th><?= lang('serial_no') ?></th>
                                        <?php endif ?>
                                        <?php if ($show_cost): ?>
                                            <th><?= lang('unit_cost') ?></th>
                                        <?php endif ?>
                                        <th><?= lang('quantity') ?></th>
                                        <?php if ($show_cost): ?>
                                            <th><?= lang('cost') ?></th>
                                        <?php endif ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total_cost = 0; $cnt = 1;?>
                                    <?php 
                                    $items = isset($inv[1]) ? $rows[$inv[1]->id] : $rows[$inv[0]->id];
                                     ?>
                                    <?php foreach ($items as $item): ?>
                                        <?php 
                                        if ($item->type == 'subtraction') {
                                            continue;
                                        }
                                         ?>
                                        <tr>
                                            <td><?= $cnt ?></td>
                                            <td><?= $item->code ?></td>
                                            <td><?= $item->name ?></td>
                                            <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                                                <td><?= $item->serial_no ?></td>
                                            <?php endif ?>
                                            <?php if ($show_cost): ?>
                                                <td><?= $this->sma->formatMoney($item->avg_cost) ?></td>
                                            <?php endif ?>
                                            <td><?= $this->sma->formatQuantity($item->quantity) ?></td>
                                            <?php if ($show_cost): ?>
                                                <td><?= $this->sma->formatMoney($item->avg_cost * $item->quantity) ?></td>
                                            <?php endif ?>
                                            <?php $total_cost += $item->avg_cost * $item->quantity; ?>
                                        </tr>
                                        <?php $cnt++; ?>
                                    <?php endforeach ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="<?= $show_cost ? 5 : 3 ?>">Total</th>
                                        <th class="text-right"><?= $this->sma->formatMoney($total_cost) ?></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 border no-right">
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p style="border-bottom: 1px solid #666;">&nbsp;</p>
                  <p style="text-align: center;">Recibido por : </p>
                </div>
            </div>
        </div>
    </div>
</div>
