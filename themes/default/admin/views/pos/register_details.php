
<?php
    $definir_color = function($amount){
        if ($amount > 0) {
            $color = "color:green;";
        } else if ($amount < 0) {
            $color = "color:#ef5b50;";
        } else {
            $color = "";
        }
        return $color;
    }
?>
<style type="text/css">
    .amount_cash {
        font-weight: 700;
    }
</style>
<?php if ($modal) { ?>
    <div class="modal-dialog modal-xs"  <?= isset($cron_job) ? "style='border:1px solid #DDD; padding:10px; margin:10px 0;'" : "" ?>>
        <div class="modal-content"  id="closeRegisterPrint">
            <div class="modal-header">
                
            <?php if (!isset($cron_job)): ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
            <?php endif ?>
<?php
    } else {
?>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Responsable cierre : <?= $close_user->first_name." ".$close_user->last_name; ?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            .text-center { text-align: center ; }
            table { width: 100% ; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
            table > tbody > tr > th ,
            table > tbody > tr > td ,
            table > tbody > tr > td > h4,
            table > tbody > tr > td > h4 > span
            {
                font-size: 14px !important;
            }
            table > tbody > tr > th ,
            table > tbody > tr > td
             {
                padding-left:3.5px !important;
            }
        </style>
    </head>

    <body id="closeRegisterPrint">
    <div id="wrapper">
        <div id="receiptData">

<?php } ?>
            <h4 class="modal-title" id="myModalLabel"><?= lang('close_register') . ' (' . $this->sma->hrld($register->date) . ' - ' . $this->sma->hrld($register->closed_at) . ')'; ?></h4>
            <h4>Responsable cierre : <?= $close_user->first_name." ".$close_user->last_name; ?></h4>
            <?php if ($close_user->user_pc_serial): ?>
                <h4><?= lang('user_pc_serial') ?> : <?= $close_user->user_pc_serial; ?></h4>
            <?php endif ?>
            <h4>Base de caja : <span class="amount_cash" style="<?= $definir_color($register->cash_in_hand) ?>"><?= $this->sma->formatMoney($register->cash_in_hand); ?></span></h4>

            <h4 class="without_close" style="display: none;"></h4>
            <?php if (!isset($cron_job)): ?>
                <label class="no-print">
                    <input type="checkbox" name="tirilla" class="tirilla skip"> Tirilla
                </label>
            <?php endif ?>
        </div>
<?php if ($modal) { ?>
        <div class="modal-body">
<?php
    } else {
?>
        <div class="">
<?php } ?>
            <table width="100%" class="stable tableView">
                <tr>
                    <th style="width: 50%;">Concepto</th>
                    <th style="width: 10%; text-align: center;">Pagos</th>
                    <th style="width: 13.3%; text-align: center;">Venta</th>
                    <th style="width: 13.3%; text-align: center;">Devolución</th>
                    <th style="width: 13.4%; text-align: center;">Total</th>
                </tr>
                <?php $total_cash = 0;
                      $total_sales = 0;
                      $total_returns = 0;
                      $total_payments_collections = 0;
                      $total_sold_gift_cards = 0;
                      $total_paid_installments = 0;
                    if (isset($register_details) && $register_details): ?>
                        <?php foreach ($register_details as $popt): ?>
                                <?php
                                $total_payments_collections += ($popt->payments_collections_amount > 0 ? $popt->payments_collections_amount : 0);
                                $total_sold_gift_cards += (($popt->sold_gift_cards > 0 ? $popt->sold_gift_cards : 0) + ($popt->returned_gift_cards != 0 ? $popt->returned_gift_cards : 0));
                                $total_paid_installments += ($popt->paid_installments > 0 ? $popt->paid_installments : 0);
                                $RC_paid = $popt->payments_amount;
                                $Sales_paid = $popt->sales_amount;
                                $Returned_paid = $popt->devolutions_amount;
                                $Total_paid = $RC_paid + $Sales_paid + $Returned_paid;
                                $total_sales += $Sales_paid;
                                $total_returns += $Returned_paid;
                                $display_tr = '';
                                if ($popt->code == "cash") {
                                    $color_RC = $definir_color($RC_paid);
                                    $color_Sales = $definir_color($Sales_paid);
                                    $color_Returned = $definir_color($Returned_paid);
                                    $color_total = $definir_color($Total_paid);
                                    $total_cash += $Total_paid;
                                } else {
                                    $color_RC = "";
                                    $color_Sales = "";
                                    $color_Returned = "";
                                    $color_total = "";
                                }
                                if ($RC_paid == 0 && $Sales_paid == 0 && $Returned_paid == 0) {
                                    $display_tr = "style='display:none;'";
                                }
                                ?>
                            <tr <?= $display_tr ?>>
                                <td style="border-bottom: 1px solid #DDD;"><h4><?= $popt->description ?></h4></td>
                                <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_RC ?>"><?= $this->sma->formatMoney($RC_paid) ?></span></td>
                                <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_Sales ?>"><h4><span><?= $this->sma->formatMoney($Sales_paid) ?></span></h4></td>
                                <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_Returned ?>"><h4><span><?= $this->sma->formatMoney($Returned_paid); ?></span></h4></td>
                                <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_total ?>"><h4><span><?= $this->sma->formatMoney($Total_paid) ?></span></h4></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif;
                    if (isset($register_details_counted) && $register_details_counted): ?>
                    <tr>
                        <th colspan="5" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;"><h4>DE CONTADO : </h4></th>
                    </tr>
                    <?php foreach ($register_details_counted as $popt): ?>
                            <?php
                            $total_payments_collections += ($popt->payments_collections_amount > 0 ? $popt->payments_collections_amount : 0);
                            $total_sold_gift_cards += (($popt->sold_gift_cards > 0 ? $popt->sold_gift_cards : 0) + ($popt->returned_gift_cards != 0 ? $popt->returned_gift_cards : 0));
                            $total_paid_installments += ($popt->paid_installments > 0 ? $popt->paid_installments : 0);
                            $RC_paid = $popt->payments_amount;
                            $Sales_paid = $popt->sales_amount;
                            $Returned_paid = $popt->devolutions_amount;
                            $Total_paid = $RC_paid + $Sales_paid + $Returned_paid;
                            $total_sales += $Sales_paid;
                            $total_returns += $Returned_paid;
                            $display_tr = '';
                            if ($popt->code == "cash") {
                                $color_RC = $definir_color($RC_paid);
                                $color_Sales = $definir_color($Sales_paid);
                                $color_Returned = $definir_color($Returned_paid);
                                $color_total = $definir_color($Total_paid);
                                $total_cash += $Total_paid;
                            } else {
                                $color_RC = "";
                                $color_Sales = "";
                                $color_Returned = "";
                                $color_total = "";
                            }
                            if ($RC_paid == 0 && $Sales_paid == 0 && $Returned_paid == 0) {
                                $display_tr = "style='display:none;'";
                            }
                            ?>
                        <tr <?= $display_tr ?>>
                            <td style="border-bottom: 1px solid #DDD;"><h4><?= $popt->description ?></h4></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_RC ?>"><?= $this->sma->formatMoney($RC_paid) ?></span></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_Sales ?>"><h4><span><?= $this->sma->formatMoney($Sales_paid) ?></span></h4></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_Returned ?>"><h4><span><?= $this->sma->formatMoney($Returned_paid); ?></span></h4></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_total ?>"><h4><span><?= $this->sma->formatMoney($Total_paid) ?></span></h4></td>
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
                <?php 
                $tr_display = $total_sales > 0 || $total_returns  != 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td width="300px;" style="font-weight:bold;"><h4><?= lang('total_sales'); ?></h4></td>
                    <td></td>
                    <td style="text-align:right;"><h4><span><?= $this->sma->formatMoney($total_sales); ?></span></h4></td>
                    <td width="200px;" style="font-weight:bold;text-align:right;"><h4>
                            <span><?= $this->sma->formatMoney($total_returns); ?></span>
                        </h4></td>
                    <td style="text-align:right;">
                        <h4>
                            <span><?= $this->sma->formatMoney($total_sales + $total_returns) ?></span>
                        </h4>
                    </td>
                </tr>
                <?php 
                $tr_display = $register->refunds != 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-top: 1px solid #DDD;"><h4><?= lang('returns'); ?></h4></td>
                    <td style="text-align:right; border-top: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-top: 1px solid #DDD;"></td>
                    <td style="text-align:right;border-top: 1px solid #DDD;"><h4>
                            <span class="amount_cash" style="<?= $definir_color($register->refunds) ?>"><?= $this->sma->formatMoney($register->refunds ? "-".$register->refunds : '0.00'); ?></span>
                        </h4></td>
                    <td style="text-align:right;border-top: 1px solid #DDD;"></td>
                </tr>
                <?php if (isset($register_details_categories_expenses) && $register_details_categories_expenses): ?>
                    <tr>
                        <th colspan="5"><?= lang('expenses_detail_by_category') ?></th>
                    </tr>
                    <?php foreach ($register_details_categories_expenses as $popt): ?>
                        <tr>
                            <td style="border-bottom: 1px solid #DDD;"  colspan="3"><h4><?= $popt->description ?></h4></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_total ?>"><h4><span><?= $this->sma->formatMoney($popt->total_amount) ?></span></h4></td>
                            <td></td>
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
                <?php 
                $tr_display = $register->expenses != 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style=""><h4><?= (isset($register_details_categories_expenses) && $register_details_categories_expenses ? lang('total')." " : "").lang('expenses'); ?></h4></td>
                    <td style="text-align:right;"></td>
                    <td style="text-align:right;"></td>
                    <td style="text-align:right;"><h4>
                            <span class="amount_cash" style="<?= $definir_color($register->expenses) ?>"><?= $this->sma->formatMoney($register->expenses); ?></span>
                        </h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->purchases_payments != 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('ppayments'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"><h4>
                            <span class="amount_cash" style="<?= $definir_color($register->purchases_payments) ?>"><?= $this->sma->formatMoney($register->purchases_payments); ?></span>
                        </h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->deposits > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('deposits_registered'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($register->deposits) ?>"><?= $this->sma->formatMoney($register->deposits ? $register->deposits : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->deposits_other_methods > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('deposits_registered_other_payments'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($register->deposits_other_methods ? $register->deposits_other_methods : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->suppliers_deposits  != 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('deposits_paid'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($register->suppliers_deposits) ?>"><?= $this->sma->formatMoney($register->suppliers_deposits ? $register->suppliers_deposits : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->tips_cash > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('tips_by_cash'), lang('tips')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($register->tips_cash) ?>"><?= $this->sma->formatMoney($register->tips_cash ? $register->tips_cash : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->tips_other_methods > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('tips_by_other_payments'), lang('tip')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($register->tips_other_methods ? $register->tips_other_methods : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->tips_due > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('tips_by_due'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($register->tips_due ? $register->tips_due : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->shipping_cash > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('shipping_by_cash'), lang('shipping')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($register->shipping_cash) ?>"><?= $this->sma->formatMoney($register->shipping_cash ? $register->shipping_cash : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->shipping_other_methods > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('shipping_by_other_payments'), lang('shipping')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($register->shipping_other_methods ? $register->shipping_other_methods : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->shipping_due > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('shipping_by_due'), lang('shipping')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($register->shipping_due ? $register->shipping_due : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->movements_in > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('shipping_by_cash'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($register->movements_in) ?>"><?= $this->sma->formatMoney($register->movements_in ? $register->movements_in : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->movements_out  != 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('shipping_by_cash'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($register->movements_out) ?>"><?= $this->sma->formatMoney($register->movements_out ? $register->movements_out : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->gc_topups_cash > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4>Recargas tarjeta regalo (Efectivo) </h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($register->gc_topups_cash) ?>"><?= $this->sma->formatMoney($register->gc_topups_cash ? $register->gc_topups_cash : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php 
                $tr_display = $register->gc_topups_other > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td style="border-bottom: 1px solid #DDD;"><h4>Recargas tarjeta regalo </h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class=""><?= $this->sma->formatMoney($register->gc_topups_other ? $register->gc_topups_other : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                </tr>
                <?php if ($total_payments_collections > 0): ?>
                    <tr>
                        <th colspan="6" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;"><h4><?= lang('payments_collections') ?></h4></th>
                    </tr>
                    <?php if (isset($register_details) && $register_details): ?>
                        <?php foreach ($register_details as $popt): ?>
                            <?php if ($popt->payments_collections_amount == 0) {
                                continue;
                            } ?>
                            <?php 
                                if ($popt->code == 'cash') {
                                $total_cash += ($popt->payments_collections_amount > 0 ? $popt->payments_collections_amount : 0);
                            }
                             ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popt->payments_collections_amount)  : '' ?>"><?= $this->sma->formatMoney($popt->payments_collections_amount) ?></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td><input type="hidden" name="gc_topups_other" value="<?= $popt->payments_collections_amount ?>"></td>
                                </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    <?php if (isset($register_details_counted) && $register_details_counted): ?>
                        <?php foreach ($register_details_counted as $popt): ?>
                            <?php if ($popt->payments_collections_amount == 0) {
                                continue;
                            } ?>
                            <?php 
                                if ($popt->code == 'cash') {
                                $total_cash += ($popt->payments_collections_amount > 0 ? $popt->payments_collections_amount : 0);
                            }
                             ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popt->payments_collections_amount)."font-weight: 800;"  : '' ?>"><?= $this->sma->formatMoney($popt->payments_collections_amount) ?></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td><input type="hidden" name="gc_topups_other" value="<?= $popt->payments_collections_amount ?>"></td>
                                </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                <?php endif ?>
                
                <?php if ($total_sold_gift_cards != 0): ?>
                    <tr>
                        <th colspan="6" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;"><h4><?= sprintf(lang('sold_gift_cards'), lang('gift_cards')) ?></h4></th>
                    </tr>
                    <?php if (isset($register_details) && $register_details): ?>
                        <?php foreach ($register_details as $popt): ?>
                            <?php if ($popt->sold_gift_cards == 0 && $popt->returned_gift_cards == 0) {
                                continue;
                            } ?>
                            <?php 
                                if ($popt->code == 'cash') {
                                $total_cash += (($popt->sold_gift_cards > 0 ? $popt->sold_gift_cards : 0) + ($popt->returned_gift_cards != 0 ? $popt->returned_gift_cards : 0));
                            }
                             ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popt->sold_gift_cards)  : '' ?>"><?= $this->sma->formatMoney($popt->sold_gift_cards) ?></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popt->returned_gift_cards)  : '' ?>"><?= $this->sma->formatMoney($popt->returned_gift_cards) ?></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td><input type="hidden" name="gc_topups_other" value="<?= $popt->sold_gift_cards ?>"></td>
                                </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    <?php if (isset($register_details_counted) && $register_details_counted): ?>
                        <?php foreach ($register_details_counted as $popt): ?>
                            <?php if ($popt->sold_gift_cards == 0 && $popt->returned_gift_cards == 0) {
                                continue;
                            } ?>
                            <?php 
                                if ($popt->code == 'cash') {
                                $total_cash += (($popt->sold_gift_cards > 0 ? $popt->sold_gift_cards : 0) + ($popt->returned_gift_cards != 0 ? $popt->returned_gift_cards : 0));
                            }
                             ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popt->sold_gift_cards)."font-weight: 800;"  : '' ?>"><?= $this->sma->formatMoney($popt->sold_gift_cards) ?></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popt->returned_gift_cards)."font-weight: 800;"  : '' ?>"><?= $this->sma->formatMoney($popt->returned_gift_cards) ?></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td><input type="hidden" name="gc_topups_other" value="<?= $popt->sold_gift_cards ?>"></td>
                                </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                <?php endif ?>
                
                
                <?php if ($total_paid_installments > 0): ?>
                    <tr>
                        <th colspan="6" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;"><h4><?= lang('payment_financing_fee') ?></h4></th>
                    </tr>
                    <?php if (isset($register_details) && $register_details): ?>
                        <?php foreach ($register_details as $popt): ?>
                            <?php if ($popt->paid_installments == 0) {
                                continue;
                            } ?>
                            <?php 
                                if ($popt->code == 'cash') {
                                $total_cash += ($popt->paid_installments > 0 ? $popt->paid_installments : 0);
                            }
                             ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popt->paid_installments)  : '' ?>"><?= $this->sma->formatMoney($popt->paid_installments) ?></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td><input type="hidden" name="gc_topups_other" value="<?= $popt->paid_installments ?>"></td>
                                </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    <?php if (isset($register_details_counted) && $register_details_counted): ?>
                        <?php foreach ($register_details_counted as $popt): ?>
                            <?php if ($popt->paid_installments == 0) {
                                continue;
                            } ?>
                            <?php 
                                if ($popt->code == 'cash') {
                                $total_cash += ($popt->paid_installments > 0 ? $popt->paid_installments : 0);
                            }
                             ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popt->paid_installments)."font-weight: 800;"  : '' ?>"><?= $this->sma->formatMoney($popt->paid_installments) ?></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td><input type="hidden" name="gc_topups_other" value="<?= $popt->paid_installments ?>"></td>
                                </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                <?php endif ?>
                
                
                <?php 

                $total_cash_amount = 
                                        $register->cash_in_hand + 
                                        $total_cash - 
                                        ($register->refunds ? $register->refunds : 0) + 
                                        ($register->expenses ? $register->expenses : 0) + 
                                        $register->deposits + 
                                        ($register->purchases_payments ? $register->purchases_payments : 0) +
                                        ($register->suppliers_deposits ? $register->suppliers_deposits : 0) +
                                        ($register->tips_cash ? $register->tips_cash : 0) +
                                        ($register->shipping_cash ? $register->shipping_cash : 0) +
                                        ($register->movements_in ? $register->movements_in : 0) +
                                        ($register->movements_out ? $register->movements_out : 0)+
                                        ($register->gc_topups_cash ? $register->gc_topups_cash : 0)
                                    ;
                $tr_display = $total_cash_amount > 0 ? '' : 'style="display:none;"';
                 ?>
                <tr <?= $tr_display ?>>
                    <td width="300px;" style="font-weight:bold;"><h4><strong><?= lang('total_cash'); ?></strong>:</h4></td>
                    <td></td>
                    <td style="text-align:right;"><h4>
                            <span class="amount_cash" style="<?= $definir_color($total_cash_amount) ?>"><?= $this->sma->formatMoney($total_cash_amount); ?></span>
                        </h4></td>
                    <td style="text-align:right;"></td>
                    <td></td>
                </tr>
                
                <?php if (isset($register_details_categories) && $register_details_categories): ?>
                    <tr>
                        <th colspan="5" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;"><h4><?= $this->pos_settings->detail_sale_by_category == 1 ? 'Ventas por categoría' : 'Ventas por producto' ?></h4></th>
                    </tr>
                        <tr>
                            <th class="" colspan="3">Nombre</th>
                            <th class="text-right">Cantidad</th>
                            <th class="text-right">Total</th>
                        </tr>
                    <?php foreach ($register_details_categories as $popt): ?>
                        <tr>
                            <td style="border-bottom: 1px solid #DDD;"  colspan="3"><h4><?= $popt->description ?></h4></td>
                            <?php
                            $RC_paid = $popt->payments_amount;
                            $Sales_paid = $popt->sales_amount;
                            $Returned_paid = $popt->devolutions_amount;
                            $Total_paid = $RC_paid + $Sales_paid + $Returned_paid;
                            $total_sales += $Sales_paid;
                            $total_returns += $Returned_paid;
                            if ($popt->code == "cash") {
                                $color_RC = $definir_color($RC_paid);
                                $color_Sales = $definir_color($Sales_paid);
                                $color_Returned = $definir_color($Returned_paid);
                                $color_total = $definir_color($Total_paid);
                                $total_cash += $Total_paid;
                            } else {
                                $color_RC = "";
                                $color_Sales = "";
                                $color_Returned = "";
                                $color_total = "";
                            }
                            ?>
                            <td style="text-align:center; border-bottom: 1px solid #DDD; <?= $color_Returned ?>"><h4><span><?= $this->sma->formatQuantity($popt->category_quantity); ?></span></h4></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_total ?>"><h4><span><?= $this->sma->formatMoney($Total_paid) ?></span></h4></td>
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
                <?php 
                $display_tr = '';
                if ($register->total_retention == 0 && $register->total_return_retention == 0 && $register->total_rc_retention == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?>>
                    <td width="300px;" style="font-weight:bold; border-bottom: 1px solid #EEE;"><h4>Retenciones:</h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;"><?= $this->sma->formatMoney($register->total_rc_retention) ?></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;">
                        
                            <h4><span><?= $this->sma->formatMoney($register->total_retention) ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;" width="200px;" style="font-weight:bold;text-align:right; border-bottom: 1px solid #EEE;"><h4><span>
                                <?=  $this->sma->formatMoney($register->total_return_retention); ?>
                            </span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;">
                    </td>
                </tr>
            </table>
            <hr>
            <div class="row">
                <div class="form-group col-sm-6  no-print">
                    <?= lang("total_cheques", "total_cheques_submitted"); ?>
                    <?= form_hidden('total_cheques', $register->total_cheques_submitted); ?>
                    <?= form_input('total_cheques_submitted', $register->total_cheques_submitted, 'class="form-control input-tip" id="total_cheques_submitted" readonly'); ?>
                </div>
                <div class="form-group col-sm-6  no-print">
                    <?= lang("total_cc_slips", "total_cc_slips_submitted"); ?>
                    <?= form_hidden('total_cc_slips', $register->total_cc_slips_submitted); ?>
                    <?= form_input('total_cc_slips_submitted', $register->total_cc_slips_submitted, 'class="form-control input-tip" id="total_cc_slips_submitted" readonly'); ?>
                </div>
                <div class="form-group col-sm-12">
                    <table class="table">
                        <?php 
                        $diff_cash = $register->total_cash_submitted - $total_cash_amount;
                         ?>
                        <tr>
                            <th><?= lang('total_cash') ?></th>
                            <td><?= $this->sma->formatMoney($total_cash_amount) ?></td>
                        </tr>
                        <tr>
                            <th><?= lang('total_cash_submitted') ?></th>
                            <td><?= $this->sma->formatMoney($register->total_cash_submitted) ?></td>
                        </tr>
                        <tr>
                            <th>Diferencia</th>
                            <td style="<?= $definir_color($diff_cash) ?>;"><?= $this->sma->formatMoney($diff_cash) ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <?php if (!isset($cron_job)): ?>
                <div class="form-group no-print">
                    <label for="note"><?= lang("note"); ?></label>
                    <div class="controls"> <?= form_textarea('note', $register->note, 'class="form-control" id="note" style="margin-top: 10px; height: 100px;" readonly'); ?> </div>
                </div>
            <?php endif ?>

        <?php if (!isset($cron_job)): ?>
            <div class="modal-footer no-print">
                <button type="button" class="btn btn-wappsi-pagar" onclick="imprimir_factura_pos();">
                    <i class="fa fa-print"></i> <?= lang('print'); ?>
                </button>
            </div>
        <?php endif ?>
        
    <?php
    if( ! $modal) {
        ?>
        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
        <?php
    }
    ?>

<?php if($modal) { ?>

            </div>
        </div>
    </div>
<?= $modal_js ?>
<?php } else { ?>
        </body>
    </html>
<?php } ?>

</div>
<script type="text/javascript">
    $(document).ready(function () {
        // close_register
        $("input[name='close_register']").click(function( event ){
            event.preventDefault();
            window.onafterprint = function(e){
                $(window).off('mousemove', window.onafterprint);
                console.log('Print Dialog Closed..');
                $('.bv-form').submit();
            };
            window.print();
            setTimeout(function(){
                $(window).one('mousemove', window.onafterprint);
            }, 1);
        });

        $(document).on('click', '.po', function (e) {
            e.preventDefault();
            $('.po').popover({
                html: true,
                placement: 'left',
                trigger: 'manual'
            }).popover('show').not(this).popover('hide');
            return false;
        });
        $(document).on('click', '.po-close', function () {
            $('.po').popover('hide');
            return false;
        });
        $(document).on('click', '.po-delete', function (e) {
            var row = $(this).closest('tr');
            e.preventDefault();
            $('.po').popover('hide');
            var link = $(this).attr('href');
            $.ajax({
                type: "get", url: link,
                success: function (data) {
                    row.remove();
                    addAlert(data, 'success');
                },
                error: function (data) {
                    addAlert('Failed', 'danger');
                }
            });
            return false;
        });
        <?php if (!$modal): ?>
            setTimeout(function() {
                imprimir_factura_pos();
            }, 1200);
        <?php endif ?>
        
    });

    $(document).on('change', '.tirilla', function(){
        check = $(this);

        if (check.is(':checked')) {
            $('.tableView').css('display', 'none');
            $('.tablePrint').css('display', '');
            $('.modal-dialog h4').css('font-size', '95%');
        } else {
            $('.tableView').css('display', '');
            $('.tablePrint').css('display', 'none');
            $('.modal-dialog h4').css('font-size', '');
        }

    });


    function imprimir_factura_pos()
    {
        $('.without_close').css('display', '');
      var divToPrint=document.getElementById('closeRegisterPrint');
      var newWin=window.open('','Print-Window');
      var header = $('head').html();
      newWin.document.open();
      newWin.document.write('<head>'+header+'</head><body onload="window.print()" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');
      newWin.document.close();
      setTimeout(function(){
        newWin.close();
        $('.without_close').css('display', 'none');
          <?php if (!$modal): ?>
              location.href = '<?= admin_url('dashboard') ?>';
          <?php endif ?>
        },300);
    }
</script>


<?php 
    unset($def_color);
 ?>