<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
if ($this->session->userdata('detal_post_processing')) {
    $this->session->unset_userdata('detal_post_processing');
}
?>
<script type="text/javascript">
    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";
        customer = null;
        filtered = null;
        filtered_ini_date  = null;
    if (localStorage.getItem('sl_filter_filtered_ini_date')) {
        filtered_ini_date = localStorage.getItem('sl_filter_filtered_ini_date');
    }
    <?php if (isset($_POST['start_date'])): ?>
        start_date = '<?= $_POST['start_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['end_date'])): ?>
        end_date = '<?= $_POST['end_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['filtered'])): ?>
        filtered = '<?= $_POST['filtered'] ?>';
        if (filtered_ini_date === undefined) {
            filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        }
    <?php endif ?>
</script>
<script>
    var oTable;
    if ($(window).width() < 1000) {
        var nums = [[10, 25], [10, 25]];
    } else {
        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
    }
    $(document).ready(function ()
    {
        var oTable = $('#SLData').dataTable({
            "aaSorting": [[3, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength":  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=admin_url('hotel/get_guest_registers'); ?>',
            fnServerData: function (sSource, aoData, fnCallback)
            {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                }, {
                    "name": "start_date",
                    "value": start_date
                }, {
                    "name": "end_date",
                    "value": end_date
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex)
            {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "guest_register_link";
                return nRow;
            },
            "aoColumns": [
                {"mRender": checkbox},
                null,
                null,
                {"mRender": fld},
                {"mRender": fld},
                {"mRender": fld},
                null,
                {"mRender": formatDecimal},
                {"mRender": formatDecimal},
                {"mRender": guest_register_status},
                {"mRender": currencyFormat},
                null,
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0;
                for (var i = 0; i < aaData.length; i++) {
                    if (aaData[aiDisplay[i]] !== undefined) {
                        gtotal += parseFloat(aaData[aiDisplay[i]][10]);
                    }
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[10].innerHTML = currencyFormat(parseFloat(gtotal));
            }
        }).fnSetFilteringDelay().dtFilter([
            // {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
        ], "footer");

        if (localStorage.getItem('remove_slls') || localStorage.getItem('remove_resl')) {
            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
            localStorage.removeItem('remove_slls');
        }

        <?php if ($this->session->userdata('remove_slls') || $this->session->userdata('remove_resl')) {?>
            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
        <?php
            $this->sma->unset_data('remove_slls');
        }
        ?>
    });

</script>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading no-printable">
    <div class="col-lg-8">
        <h2><?=lang('guest_registers');?></h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<!-- /Header -->


<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <?= admin_form_open('hotel/guest_registers', ['id'=>'sales_filter']) ?>
                                <div class="col-sm-4">
                                    <?= lang('reference_no', 'detal_reference_no') ?>
                                    <select name="detal_reference_no" id="detal_reference_no" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($documents_types): ?>
                                            <?php foreach ($documents_types as $dt): ?>
                                                <option value="<?= $dt->id ?>" data-dtprefix="<?= $dt->sales_prefix ?>" <?= isset($_POST['detal_reference_no']) && $_POST['detal_reference_no'] == $dt->id ? 'selected="selected"' : '' ?>><?= $dt->nombre." (".$dt->sales_prefix.")" ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                    <?php
                                    $biller_selected = '';
                                    $biller_readonly = false;
                                    if ($this->session->userdata('biller_id')) {
                                        $biller_selected = $this->session->userdata('biller_id');
                                        $biller_readonly = true;
                                    }

                                    $bl[""] = lang('select');
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                    <?php
                                    echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="filter_customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                                    ?>
                                </div>
                                <?php if ($this->Owner || $this->Admin): ?>
                                    <div class="col-sm-4">
                                        <label><?= lang('user') ?></label>
                                        <select name="filter_user" id="filter_user" class="form-control">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php if ($users): ?>
                                                <?php foreach ($users as $user): ?>
                                                    <option value="<?= $user->id ?>"><?= $user->first_name." ".$user->last_name ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                <?php endif ?>
                                <hr class="col-sm-11">

                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>

                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                        <div class="col-sm-4 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('start_date', 'start_date') ?>
                                        <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                    </div>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('end_date', 'end_date') ?>
                                        <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $this->sma->fld($_POST['end_date']) : $this->filtros_fecha_final ?>" class="form-control datetime">
                                    </div>
                                </div>

                                <div class="col-sm-12" style="margin-top: 2%;">
                                    <input type="hidden" name="filtered" value="1">
                                    <button type="submit" id="submit-sales-filter_dh" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                    <button type="button" onclick="window.location.href = site.base_url+'sales';" class="btn btn-danger"><span class="fa fa-times"></span> <?= lang('reset') ?></button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
    	    echo admin_form_open('sales/sale_actions', 'id="action-form"');
    	}
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <?php if ($this->Owner || $this->Admin || $this->GP['hotel-add_guest_register'] || $this->GP['bulk_actions']): ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <?php if ($this->Owner || $this->Admin || $this->GP['hotel-add_guest_register']): ?>
                                    <li>
                                        <a href="<?=admin_url('hotel/add_guest_register')?>">
                                            <i class="fa fa-plus-circle"></i> <?=lang('add_guest_register')?>
                                        </a>
                                    </li>
                                    <?php endif ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><?//=lang('list_results');?></p> -->
                            <div class="table-responsive">
                                <h4 class="text_filter"></h4>
                                <table id="SLData" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("customer"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("date_in"); ?></th>
                                        <th><?= lang("estimated_date_out"); ?></th>
                                        <th><?= lang("date_out"); ?></th>
                                        <th><?= lang("room"); ?></th>
                                        <th><?= lang("guests_no"); ?></th>
                                        <th><?= lang("nights_no"); ?></th>
                                        <th><?= lang("status"); ?></th>
                                        <th><?= lang("total"); ?></th>
                                        <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("customer"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("date_in"); ?></th>
                                        <th><?= lang("estimated_date_out"); ?></th>
                                        <th><?= lang("date_out"); ?></th>
                                        <th><?= lang("room"); ?></th>
                                        <th><?= lang("guests_no"); ?></th>
                                        <th><?= lang("nights_no"); ?></th>
                                        <th><?= lang("status"); ?></th>
                                        <th><?= lang("total"); ?></th>
                                        <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $Admin || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">

    $(document).ready(function () {
        if (customer != '') {
                $('#filter_customer').val(customer).select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site.base_url+"customers/getCustomer/" + $(element).val(),
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site.base_url + "customers/suggestions",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: lang.no_match_found}]};
                            }
                        }
                    }
                });

            $('#filter_customer').trigger('change');
        } else {
            $('#filter_customer').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        }
        if (filtered !== undefined) {
            setTimeout(function() {
                console.log('biller '+biller);
                $('#start_date').val(start_date);
                $('#end_date').val(end_date);
                $('#biller').select2('val', biller);
                $('#filter_user').select2('val', filter_user);
                // $('.collapse-link').click();
            }, 900);
        }

        setTimeout(function() {
            setFilterText();
        }, 1500);
        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>
    });

function calcularMinutos(start_date, end_date)
{
    var fecha1 = new Date(start_date);
    var fecha2 = new Date(end_date);
    var diff = fecha2.getTime() - fecha1.getTime();
    var minutos = diff/(1000 * 60);
    return minutos;
}

function setFilterText(){
    var reference_text = $('#detal_reference_no option:selected').data('dtprefix');
    var biller_text = $('#biller option:selected').text();
    var customer_text = $('#filter_customer').select2('data') !== null ? $('#filter_customer').select2('data').text : '';
    var sale_status_text = $('#sale_status option:selected').text();
    var payment_status_text = $('#payment_status option:selected').text();
    var start_date_text = $('#start_date_dh').val();
    var end_date_text = $('#end_date_dh').val();
    var text = "Filtros configurados : ";
    coma = false;
    if (detal_reference_no != '' && detal_reference_no !== undefined) {
        text+=" Tipo documento ("+reference_text+")";
        coma = true;
    }
    if (biller != '' && biller !== undefined) {
        text+= coma ? "," : "";
        text+=" Sucursal ("+biller_text+")";
        coma = true;
    }
    if (customer != '' && customer !== undefined) {
        text+= coma ? "," : "";
        text+=" Cliente ("+customer_text+")";
        coma = true;
    }
    if (start_date != '' && start_date !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha de inicio ("+start_date_text+")";
        coma = true;
    }
    if (end_date != '' && end_date !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha final ("+end_date_text+")";
        coma = true;
    }
    $('.text_filter').html(text);
}
$(document).ready(function(){
    setTimeout(function() {
        <?php if (!isset($_POST['date_records_filter'])): ?>
            $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            $('#sales_filter').submit();
        <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
            $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
        <?php endif ?>
    }, 150);
});

</script>