<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_product_color'); ?></h4>
        </div>
        <?= admin_form_open("system_settings/updateColor/$color->id", ['data-toggle' => 'validator', 'role' => 'form']); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label for="name"><?= $this->lang->line("name"); ?></label>
                <div class="controls">
                    <?= form_input('name', $color->name, 'class="form-control" id="name" required="required"'); ?>
                </div>
            </div>

            <div class="form-group">
                <?= form_label($this->lang->line("status"), 'status') ?>
                <div class="controls">
                    <label class="status_switch">
                        <input name="status" type="checkbox" class="skip" <?= ($color->status == 1 ? 'checked' : '') ?>>
                        <span class="status_slider"></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_button(['name'=>'saveColor', 'id'=>'saveColor', 'type'=>'submit', 'class'=>'btn btn-primary new-button', 'data-toogle-second'=>'tooltip', 'title'=>$this->lang->line('save'), 'content'=>'<i class="fas fa-check"></i>']); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>

<?= $modal_js ?>

<script>
    $(document).ready(function () {
        $('[data-toggle-second="tooltip"]').tooltip();
    });
</script>