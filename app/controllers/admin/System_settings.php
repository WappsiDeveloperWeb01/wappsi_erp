<?php defined('BASEPATH') OR exit('No direct script access allowed');

class system_settings extends MY_Controller
{

    private $integration = [];

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->admin_model('pos_model');
        $this->pos_settings = $this->pos_model->getSetting();
        $this->lang->admin_load('settings', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('Restobar_model');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('WithholdingTax_model');
        $this->load->admin_model('TypesMovement_model');
        $this->load->admin_model('CommercialDiscount_model');
        $this->load->admin_model('InvoiceNotes_model');
        $this->load->admin_model('ElectronicBillingInstance_model');
        $this->load->admin_model('products_model');
        $this->load->admin_model('Payment_method');
        $this->load->admin_model('TaxRates_model');

        $this->load->integration_model('ProductVariation');
        $this->load->integration_model('Product');
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif';
        $this->allowed_file_size = '1024';
        $this->not_accounting = [38, 12, 7, 8, 9, 10, 19, 13, 20, 32, 23, 39];
    }

    public function index()
    {
        $this->sma->checkPermissions();
        $this->load->library('gst');
        $this->form_validation->set_rules('site_name', lang('site_name'), 'trim|required');
        $this->form_validation->set_rules('dateformat', lang('dateformat'), 'trim|required');
        $this->form_validation->set_rules('timezone', lang('timezone'), 'trim|required');
        $this->form_validation->set_rules('mmode', lang('maintenance_mode'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('iwidth', lang('image_width'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('iheight', lang('image_height'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('twidth', lang('thumbnail_width'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('theight', lang('thumbnail_height'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('display_all_products', lang('display_all_products'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('watermark', lang('watermark'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('currency', lang('default_currency'), 'trim|required');
        $this->form_validation->set_rules('email', lang('default_email'), 'trim|required');
        $this->form_validation->set_rules('language', lang('language'), 'trim|required');
        $this->form_validation->set_rules('warehouse', lang('default_warehouse'), 'trim|required');
        $this->form_validation->set_rules('biller', lang('default_biller'), 'trim|required');
        $this->form_validation->set_rules('tax_rate', lang('product_tax'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('tax_rate2', lang('invoice_tax'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('quote_prefix', lang('quote_prefix'), 'trim');
        $this->form_validation->set_rules('purchase_prefix', lang('purchase_prefix'), 'trim');
        $this->form_validation->set_rules('transfer_prefix', lang('transfer_prefix'), 'trim');
        $this->form_validation->set_rules('delivery_prefix', lang('delivery_prefix'), 'trim');
        $this->form_validation->set_rules('payment_prefix', lang('payment_prefix'), 'trim');
        $this->form_validation->set_rules('return_prefix', lang('return_prefix'), 'trim');
        $this->form_validation->set_rules('expense_prefix', lang('expense_prefix'), 'trim');
        $this->form_validation->set_rules('detect_barcode', lang('detect_barcode'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('theme', lang('theme'), 'trim|required');
        $this->form_validation->set_rules('rows_per_page', lang('rows_per_page'), 'trim|required');
        $this->form_validation->set_rules('accounting_method', lang('accounting_method'), 'trim|required');
        $this->form_validation->set_rules('product_serial', lang('product_serial'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('product_discount', lang('product_discount'), 'trim|required');
        $this->form_validation->set_rules('bc_fix', lang('bc_fix'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('busqueda_avanzada', lang('advanced_search'), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('hide_products_in_zero_price', lang('hide_products_in_zero_price'), 'greater_than_equal_to[0]');

        if ($this->input->post('protocol') == 'smtp') {
            $this->form_validation->set_rules('smtp_host', lang('smtp_host'), 'required');
            $this->form_validation->set_rules('smtp_user', lang('smtp_user'), 'required');
            $this->form_validation->set_rules('smtp_pass', lang('smtp_pass'), 'required');
            $this->form_validation->set_rules('smtp_port', lang('smtp_port'), 'required');
        }

        if ($this->input->post('protocol') == 'sendmail') {
            $this->form_validation->set_rules('mailpath', lang('mailpath'), 'required');
        }

        $this->form_validation->set_rules('decimals', lang('decimals'), 'trim|required');
        $this->form_validation->set_rules('decimals_sep', lang('decimals_sep'), 'trim|required');
        $this->form_validation->set_rules('thousands_sep', lang('thousands_sep'), 'trim|required');

        if ($this->Settings->indian_gst) {
            $this->form_validation->set_rules('state', lang('state'), 'trim|required');
        }

        $this->form_validation->set_rules('type_person', lang('label_type_person'), 'trim|required');
        $this->form_validation->set_rules('document_type', lang('label_document_type'), 'trim|required');
        $this->form_validation->set_rules('document_number', lang('label_document_number'), 'trim|required');

        if ($this->input->post('type_person') == NATURAL_PERSON) {
            $this->form_validation->set_rules('first_name', lang('label_first_name'), 'trim|required');
            $this->form_validation->set_rules('first_lastname', lang('label_first_lastname'), 'trim|required');
        } else {
            $this->form_validation->set_rules('business_name', lang('label_business_name'), 'trim|required');
        }

        $this->form_validation->set_rules('country', lang('label_country'), 'trim|required');
        $this->form_validation->set_rules('state', lang('label_state'), 'trim|required');
        $this->form_validation->set_rules('city', lang('label_city'), 'trim|required');
        $this->form_validation->set_rules('address', lang('label_address'), 'trim|required');
        $this->form_validation->set_rules('locality', lang('label_locality'), 'trim|required');

        if ($this->input->post("electronic_billing") == 1) {
            $this->form_validation->set_rules('technology_provider', lang('technology_provider'), 'trim|required');
            $this->form_validation->set_rules('work_environment', lang('work_environment'), 'trim|required');

            if ($this->input->post("technology_provider") == 1 || $this->input->post("technology_provider") == 2) {
                $this->form_validation->set_rules("fe_user", lang("electronic_billing_user"), "trim|required");
                $this->form_validation->set_rules("fe_password", lang("electronic_billing_password"), "trim|required");
            }
        }

        if ($this->Owner) {
            $this->form_validation->set_rules('protocol', lang('email_protocol'), 'trim|required');
            $this->form_validation->set_rules("days_before_current_date", lang("period_days_allowed_before_current_date"), "trim|required");
            $this->form_validation->set_rules("days_after_current_date", lang("period_days_allowed_after_current_date"), "trim|required");
        }

        if ($this->input->post("financing_module")) {
            $this->form_validation->set_rules("payment_method_for_credit_financing", lang("payment_method_for_credit_financing"), "trim|required");
            $this->form_validation->set_rules("quota_calculation_method_credit_financing", lang("quota_calculation_method_credit_financing"), "trim|required");
            $this->form_validation->set_rules("concepts_for_current_interest", lang("concepts_for_current_interest"), "trim|required");
            $this->form_validation->set_rules("concepts_for_default_interest", lang("concepts_for_default_interest"), "trim|required");
            $this->form_validation->set_rules("concept_for_procredito_commission", lang("concept_for_procredito_commission"), "trim|required");
            $this->form_validation->set_rules("concept_for_procredito_interest", lang("concept_for_procredito_interest"), "trim|required");
            $this->form_validation->set_rules("days_for_notification", lang("days_for_notification"), "trim|required");
            $this->form_validation->set_rules("days_for_report", lang("days_for_report"), "trim|required");
        }

        if ($this->form_validation->run() == true) {
            $language = $this->input->post('language');

            if ((file_exists(APPPATH.'language'.DIRECTORY_SEPARATOR.$language.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'sma_lang.php') && is_dir(APPPATH.DIRECTORY_SEPARATOR.'language'.DIRECTORY_SEPARATOR.$language)) || $language == 'english') {
                $lang = $language;
            } else {
                $this->session->set_flashdata('error', lang('language_x_found'));
                admin_redirect("system_settings");
                $lang = 'english';
            }

            $tax1 = ($this->input->post('tax_rate') != 0) ? 1 : 0;
            $tax2 = ($this->input->post('tax_rate2') != 0) ? 1 : 0;
            $tax3 = ($this->input->post('purchase_tax_rate') != 0) ? 1 : 0;
            $ciiu_codes = $this->input->post('ciiu_code') ? $this->input->post('ciiu_code') : "";
            $data = array('site_name' => DEMO ? 'Stock Manager Advance' : $this->input->post('site_name'),
                'rows_per_page' => $this->input->post('rows_per_page'),
                'dateformat' => $this->input->post('dateformat'),
                'timezone' => DEMO ? 'Asia/Kuala_Lumpur' : $this->input->post('timezone'),
                'mmode' => trim($this->input->post('mmode')),
                'iwidth' => $this->input->post('iwidth'),
                'iheight' => $this->input->post('iheight'),
                'twidth' => $this->input->post('twidth'),
                'theight' => $this->input->post('theight'),
                'watermark' => $this->input->post('watermark'),
                'accounting_method' => $this->input->post('accounting_method'),
                'default_email' => DEMO ? 'noreply@sma.tecdiary.my' : $this->input->post('email'),
                'language' => $lang,
                'default_warehouse' => $this->input->post('warehouse'),
                'default_tax_rate' => $this->input->post('tax_rate'),
                'default_tax_rate2' => $this->input->post('tax_rate2'),
                'quote_prefix' => $this->input->post('quote_prefix'),
                'quote_purchase_prefix' => $this->input->post('quote_purchase_prefix'),
                'order_sale_prefix' => $this->input->post('order_sale_prefix'),
                'purchase_prefix' => $this->input->post('purchase_prefix'),
                'transfer_prefix' => $this->input->post('transfer_prefix'),
                'delivery_prefix' => $this->input->post('delivery_prefix'),
                'payment_prefix' => $this->input->post('payment_prefix'),
                'ppayment_prefix' => $this->input->post('ppayment_prefix'),
                'qa_prefix' => $this->input->post('qa_prefix'),
                'return_prefix' => $this->input->post('return_prefix'),
                'returnp_prefix' => $this->input->post('returnp_prefix'),
                'expense_prefix' => $this->input->post('expense_prefix'),
                'auto_detect_barcode' => trim($this->input->post('detect_barcode')),
                'theme' => trim($this->input->post('theme')),
                'product_serial' => $this->input->post('product_serial'),
                'customer_group' => $this->input->post('customer_group'),
                'product_expiry' => $this->input->post('product_expiry'),
                'product_discount' => $this->input->post('product_discount'),
                'default_currency' => $this->input->post('currency'),
                'bc_fix' => $this->input->post('bc_fix'),
                'tax1' => $tax1,
                'tax2' => $tax2,
                'tax3' => $tax3,
                'cashier_close' => $this->input->post('cashier_close'),
                'reference_format' => $this->input->post('reference_format'),
                'racks' => $this->input->post('racks'),
                'attributes' => $this->input->post('attributes'),
                'restrict_calendar' => $this->input->post('restrict_calendar'),
                'captcha' => $this->input->post('captcha'),
                'item_addition' => $this->input->post('item_addition'),
                'decimals' => $this->input->post('decimals'),
                'decimals_sep' => $this->input->post('decimals_sep'),
                'thousands_sep' => $this->input->post('thousands_sep'),
                'default_biller' => $this->input->post('biller'),
                'invoice_view' => $this->input->post('invoice_view'),
                'rtl' => $this->input->post('rtl'),
                'each_spent' => $this->input->post('each_spent') ? $this->input->post('each_spent') : NULL,
                'ca_point' => $this->input->post('ca_point') ? $this->input->post('ca_point') : NULL,
                'each_sale' => $this->input->post('each_sale') ? $this->input->post('each_sale') : NULL,
                'sa_point' => $this->input->post('sa_point') ? $this->input->post('sa_point') : NULL,
                'sac' => $this->input->post('sac'),
                'qty_decimals' => $this->input->post('qty_decimals'),
                'display_all_products' => $this->input->post('display_all_products'),
                'display_symbol' => $this->input->post('display_symbol'),
                'symbol' => $this->input->post('symbol'),
                'remove_expired' => $this->input->post('remove_expired'),
                'barcode_separator' => $this->input->post('barcode_separator'),
                'set_focus' => $this->input->post('set_focus'),
                'disable_editing' => $this->input->post('disable_editing'),
                'price_group' => $this->input->post('price_group'),
                'barcode_img' => $this->input->post('barcode_renderer'),
                'update_cost' => $this->input->post('update_cost'),
                'apis' => $this->input->post('apis'),
                'pdf_lib' => $this->input->post('pdf_lib'),
                'state' => $this->input->post('state'),
                'show_alert_sale_expired' => $this->input->post('show_alert_sale_expired'),
                'alert_sale_expired' => $this->input->post('alert_sale_expired'),
                'purchase_tax_rate' => $this->input->post('purchase_tax_rate'),
                'sma_payment_prefix' => $this->input->post('sma_payment_prefix'),
                'deposit_prefix' => $this->input->post('deposit_prefix'),
                'resolucion_porc_aviso' => $this->input->post('resolucion_porc_aviso'),
                'resolucion_dias_aviso' => $this->input->post('resolucion_dias_aviso'),
                'prioridad_precios_producto' => $this->input->post('prioridad_precios_producto'),
                'descuento_orden' => $this->input->post('descuento_orden'),
                'allow_change_sale_iva' => $this->input->post('allow_change_sale_iva'),
                'tax_rate_traslate' => $this->input->post('tax_rate_traslate'),
                'get_companies_check_digit' => $this->input->post('get_companies_check_digit'),
                'rounding' => 1,
                'purchase_payment_affects_cash_register' => $this->input->post('purchase_payment_affects_cash_register'),
                'cost_center_selection' => $this->input->post('modulary') ? $this->input->post('cost_center_selection') : 2,
                "allow_advanced_search" => $this->input->post("busqueda_avanzada"),
                'tipo_regimen' => $this->input->post('type_vat_regime'),
                'tipo_persona' => $this->input->post('type_person'),
                'tipo_documento' => $this->input->post('document_type'),
                'numero_documento' => $this->input->post('document_number'),
                'digito_verificacion' => $this->input->post("digito_verificacion"),
                'matricula_mercantil' => $this->input->post('commercial_register'),
                'pais' => $this->input->post('country'),
                'departamento' => $this->input->post('state'),
                'ciudad' => $this->input->post('city'),
                'city_code' => $this->input->post('city_code'),
                'direccion' => $this->input->post('address'),
                'localidad' => $this->input->post('locality'),
                'phone' => $this->input->post('phone'),
                "postal_code" => $this->input->post("postal_code"),
                'fe_user' => ($this->input->post('fe_user') ? $this->input->post('fe_user') : NULL),
                'fe_password' => ($this->input->post('fe_password') ? $this->input->post('fe_password') : NULL),
                'wappsi_print_format' => ($this->input->post('wappsi_print_format') ? YES : NOT),
                'hide_products_in_zero_price' => $this->input->post('hide_products_in_zero_price'),
                'tax_method' => $this->input->post('tax_method'),
                'customer_default_country' => $this->input->post('customer_default_country'),
                'customer_default_state' => $this->input->post('customer_default_state'),
                'customer_default_city' => $this->input->post('customer_default_city'),
                'set_adjustment_cost' => $this->input->post('set_adjustment_cost'),
                'default_records_filter' => $this->input->post('default_records_filter'),
                'show_brand_in_product_search' => $this->input->post('show_brand_in_product_search'),
                'precios_por_unidad_presentacion' => $this->input->post('precios_por_unidad_presentacion'),
                'lock_cost_field_in_product' => $this->input->post('lock_cost_field_in_product'),
                'fuente_retainer' => $this->input->post('fuente_retainer') ? 1 : 0,
                'iva_retainer' => $this->input->post('iva_retainer') ? 1 : 0,
                'ica_retainer' => $this->input->post('ica_retainer') ? 1 : 0,
                'export_to_csv_each_new_customer' => $this->input->post('export_to_csv_each_new_customer'),
                'max_num_results_display' => $this->input->post('max_num_results_display'),
                'management_consecutive_suppliers' => $this->input->post('management_consecutive_suppliers'),
                'url_web' => $this->input->post('url_web'),
                'commision_payment_method' => $this->input->post('commision_payment_method'),
                'default_expense_id_to_pay_commisions' => $this->input->post('default_expense_id_to_pay_commisions'),
                'profitability_margin_validation' => $this->input->post('profitability_margin_validation'),
                'users_can_view_price_groups' => $this->input->post('users_can_view_price_groups'),
                'users_can_view_warehouse_quantitys' => $this->input->post('users_can_view_warehouse_quantitys'),
                'prorate_shipping_cost' => $this->input->post('prorate_shipping_cost'),
                'product_order' => $this->input->post('product_order'),
                'keep_seller_from_user' => $this->input->post('keep_seller_from_user'),
                'invoice_values_greater_than_zero' => $this->input->post('invoice_values_greater_than_zero'),
                'product_variant_language' => $this->input->post('product_variant_language'),
                'product_variants_language' => $this->input->post('product_variants_language'),
                'ipoconsumo' => $this->input->post('ipoconsumo'),
                'except_category_taxes' => $this->input->post('except_category_taxes'),
                'category_tax_exception_start_date' => $this->input->post('category_tax_exception_start_date'),
                'category_tax_exception_end_date' => $this->input->post('category_tax_exception_end_date'),
                'category_tax_exception_tax_rate_id' => $this->input->post('category_tax_exception_tax_rate_id'),
                'customer_territorial_decree_tax_rate_id' => $this->input->post('customer_territorial_decree_tax_rate_id'),
                'default_return_payment_method' => $this->input->post('default_return_payment_method'),
                'send_electronic_invoice_to' => $this->input->post('send_electronic_invoice_to'),
                'manual_payment_reference' => $this->input->post('manual_payment_reference'),
                'self_withholding_percentage' => $this->input->post('self_withholding_percentage'),
                'item_discount_apply_to' => $this->input->post('item_discount_apply_to'),
                'gift_card_language' => $this->input->post('gift_card_language'),
                'gift_cards_language' => $this->input->post('gift_cards_language'),
                'great_contributor' => $this->input->post('great_contributor'),
                'product_search_show_quantity' => $this->input->post('product_search_show_quantity'),
                'suspended_sales_to_retail' => $this->input->post('suspended_sales_to_retail'),
                'payments_methods_retcom' => $this->input->post('payments_methods_retcom'),
                'product_default_exempt_tax_rate' => $this->input->post('product_default_exempt_tax_rate'),
                'ciiu_code' => $ciiu_codes,
                'production_order_one_reference_limit' => $this->input->post('production_order_one_reference_limit'),
                'system_start_date' => $this->input->post('system_start_date'),
                'set_product_variant_suffix' => $this->input->post('set_product_variant_suffix'),
                'wms_picking_filter_biller' => $this->input->post('wms_picking_filter_biller'),
                'enable_customer_tax_exemption' => $this->input->post('enable_customer_tax_exemption'),
                'rete_other_language' => $this->input->post('rete_other_language'),
                'product_crud_validate_cost' => $this->input->post('product_crud_validate_cost'),
                'alert_zero_cost_sale' => $this->input->post('alert_zero_cost_sale'),
                'customer_branch_language' => $this->input->post('customer_branch_language'),
                'customer_branches_language' => $this->input->post('customer_branches_language'),
                'barcode_reader_exact_search' => $this->input->post('barcode_reader_exact_search'),
                'which_cost_margin_validation' => $this->input->post('which_cost_margin_validation'),
                'max_minutes_random_pin_code' => $this->input->post('max_minutes_random_pin_code'),
                'cron_job_db_backup_partitions' => $this->input->post('cron_job_db_backup_partitions'),
                'customer_default_payment_type' => $this->input->post('customer_default_payment_type'),
                'customer_default_payment_term' => $this->input->post('customer_default_payment_term'),
                'customer_default_credit_limit' => $this->input->post('customer_default_credit_limit'),
                'management_order_sale_delivery_time' => $this->input->post('management_order_sale_delivery_time'),
                'delivery_day_max_orders' => $this->input->post('delivery_day_max_orders'),
                'product_preferences_management' => $this->input->post('product_preferences_management'),
                'product_transformation_validate_quantity' => $this->input->post('product_transformation_validate_quantity'),
                'gift_card_product_id' => $this->input->post('gift_card_product_id'),
                'ca_point_value' => $this->input->post('ca_point_value'),
                'update_prices_from_purchases' => $this->input->post('update_prices_from_purchases'),
                'update_prices_by_margin_formula' => $this->input->post('update_prices_by_margin_formula'),
                'product_clic_action'   => $this->input->post('product_clic_action'),
                'pos_register_hide_closing_detail'   => $this->input->post('pos_register_hide_closing_detail'),
                'transformation_product_reference_restriction'   => $this->input->post('transformation_product_reference_restriction'),
                'order_sale_notification' => $this->input->post('order_sale_notification'),
                'pos_order_preparation_notification'=> $this->input->post('pos_order_preparation_notification'),
                'repeat_pos_order_preparation_notification'  => $this->input->post('repeat_pos_order_preparation_notification') ? 1 : 0,
                'repeat_order_sale_notification' => $this->input->post('repeat_order_sale_notification') ? 1 : 0,
                'loaded_suspended_sale_validate_prices' => $this->input->post('loaded_suspended_sale_validate_prices'),
                'validate_um_min_factor_qty' => $this->input->post('validate_um_min_factor_qty'),
                'days_to_new_product' => $this->input->post('days_to_new_product'),
                'update_product_tax_from_purchase' => $this->input->post('update_product_tax_from_purchase'),
                'order_sales_conversion' => $this->input->post('order_sales_conversion'),
                'product_seller_management' => $this->input->post('product_seller_management'),
                'aiu_management' => $this->input->post('aiu_management'),
                'aiu_perc_admin' => $this->input->post('aiu_perc_admin'),
                'aiu_perc_imprev' => $this->input->post('aiu_perc_imprev'),
                'aiu_perc_utilidad' => $this->input->post('aiu_perc_utilidad'),
                'variant_code_search' => $this->input->post('variant_code_search'),
                'purchases_products_supplier_code' => $this->input->post('purchases_products_supplier_code'),
                'brand_language' => $this->input->post('brand_language'),
                'brands_language' => $this->input->post('brands_language'),
                'category_language' => $this->input->post('category_language'),
                'categories_language' => $this->input->post('categories_language'),
                'subcategory_language' => $this->input->post('subcategory_language'),
                'subcategories_language' => $this->input->post('subcategories_language'),
                'subsubcategory_language' => $this->input->post('subsubcategory_language'),
                'subsubcategories_language' => $this->input->post('subsubcategories_language'),
                'block_warranty_by_warranty_days' => $this->input->post('block_warranty_by_warranty_days'),
                'warranty_warehouse' => $this->input->post('warranty_warehouse'),
                'warranty_supplier_warehouse' => $this->input->post('warranty_supplier_warehouse'),
                'cost_to_profit_and_validation' => $this->input->post('cost_to_profit_and_validation'),
                'payment_collection_language' => $this->input->post('payment_collection_language'),
                'payments_collections_language' => $this->input->post('payments_collections_language'),
                'images_from_tpro' => $this->input->post('images_from_tpro'),
                'default_zebra_print' => $this->input->post('default_zebra_print'),
                'payment_collection_webservice' => $this->input->post('payment_collection_webservice'),
                'force_default_client_in_branch' => $this->input->post('force_default_client_in_branch'),
                'allow_returns_date_before_current_date' => $this->input->post('allow_returns_date_before_current_date'),
                'exclude_overselling_on_sale_orders' => $this->input->post('exclude_overselling_on_sale_orders') ? 1 : 0,
                'companies_for_adjustments_transfers' => $this->input->post('companies_for_adjustments_transfers'),
                'days_count_birthday_discount_application' => $this->input->post('days_count_birthday_discount_application'),
                'days_count_birthday_discount' => $this->input->post('days_count_birthday_discount'),
                'automatic_loading_of_existence_in_transformation' => $this->input->post('automatic_loading_of_existence_in_transformation'),
                'financing_module' => $this->input->post('financing_module') ? 1 : 0,
                'payment_method_for_credit_financing' => $this->input->post('payment_method_for_credit_financing'),
                'enable_for' => $this->input->post('enable_for'),
                'current_interest_percentage_credit_financing' => $this->input->post('current_interest_percentage_credit_financing'),
                'default_interest_percentage_credit_financing' => $this->input->post('default_interest_percentage_credit_financing'),
                'insurance_percentage_credit_financing' => $this->input->post('insurance_percentage_credit_financing'),
                'quota_calculation_method_credit_financing' => $this->input->post('quota_calculation_method_credit_financing'),
                'credit_financing_language' => $this->input->post('credit_financing_language'),
                'vat_default_interest' => $this->input->post('vat_default_interest'),
                'withholding_validate_total_base' => $this->input->post('withholding_validate_total_base'),
                'highlight_products_without_quantities_in_edit_sales' => $this->input->post('highlight_products_without_quantities_in_edit_sales'),
                'generate_automatic_invoice' => $this->input->post('generate_automatic_invoice') ? 1 : 0,
                'concepts_for_current_interest' => $this->input->post('concepts_for_current_interest'),
                'concepts_for_default_interest' => $this->input->post('concepts_for_default_interest'),
                'concept_for_procredito_commission' => $this->input->post('concept_for_procredito_commission'),
                'concept_for_procredito_interest' => $this->input->post('concept_for_procredito_interest'),
                'allow_zero_price_products' => $this->input->post('allow_zero_price_products'),
                'management_weight_in_variants' => $this->input->post('management_weight_in_variants'),
                'management_order_sale_delivery_day' => $this->input->post('management_order_sale_delivery_day'),
                'tip_language' => $this->input->post('tip_language'),
                'exclusive_discount_group_customers' => $this->input->post('exclusive_discount_group_customers'),
                'days_for_notification' => $this->input->post('days_for_notification'),
                'days_for_report' => $this->input->post('days_for_report'),
                'test_id'   => $this->input->post('test_id'),
                'handle_jewerly_products'   => $this->input->post('handle_jewerly_products'),
                'control_customer_credit'   => $this->input->post('control_customer_credit'),
                'affiliate_management'   => $this->input->post('affiliate_management'),
                'recurring_sale_management'   => $this->input->post('recurring_sale_management'),
                'include_pending_order_quantity_stock'   => $this->input->post('include_pending_order_quantity_stock'),
                'shipping_language'   => $this->input->post('shipping_language'),
                'restobar_table_language'   => $this->input->post('restobar_table_language'),
                'osdt_recurring_sales'   => $this->input->post('osdt_recurring_sales'),
            );

            if ($this->Owner) {
                $data['detail_due_sales'] = $this->input->post('detail_due_sales') ? 1 : 0;
                $data['detail_partial_sales'] = $this->input->post('detail_partial_sales') ? 1 : 0;
                $data['detail_paid_sales'] = $this->input->post('detail_paid_sales') ? 1 : 0;
                $data['detail_products_expirated'] = $this->input->post('detail_products_expirated') ? 1 : 0;
                $data['detail_products_promo_expirated'] = $this->input->post('detail_products_promo_expirated') ? 1 : 0;
                $data['detail_sales_per_biller'] = $this->input->post('detail_sales_per_biller') ? 1 : 0;
                $data['detail_pos_registers'] = $this->input->post('detail_pos_registers') ? 1 : 0;
                $data['detail_zeta_report'] = $this->input->post('detail_zeta_report') ? 1 : 0;
                $data['dropbox_key'] = $this->input->post('dropbox_key');
                $data['automatic_update'] = $this->input->post('automatic_update');
                $data['overselling'] = $this->input->post('restrict_sale');
                $data['annual_closure'] = $this->input->post('annual_closure');
                $data['product_variant_per_serial'] = $this->input->post('product_variant_per_serial');
                $data['electronic_billing'] = $this->input->post("electronic_billing");
                $data['fe_technology_provider'] = ($this->input->post("electronic_billing") == 1) ? $this->input->post('technology_provider') : "";
                $data['fe_work_environment'] = ($this->input->post("electronic_billing") == 1) ? $this->input->post("work_environment") : "";
                $data['copy_mail_to_sender'] = ($this->input->post("electronic_billing") == 1) ? ($this->input->post("copy_mail_to_sender") ? 1 : 0) : 0;
                $data["days_before_current_date"]=$this->input->post("days_before_current_date");
                $data["days_after_current_date"]=$this->input->post("days_after_current_date");
                $data["add_individual_attachments"]=$this->input->post("add_individual_attachments");
                $data["block_movements_from_another_year"]=$this->input->post("block_movements_from_another_year");
                $data["disable_product_price_under_cost"] = $this->input->post("disable_product_price_under_cost");
                $data["purchase_send_advertisement"] = $this->input->post("purchase_send_advertisement");
                $data["ignore_purchases_edit_validations"] = $this->input->post("ignore_purchases_edit_validations");
                $data["cron_job_db_backup"] = $this->input->post("cron_job_db_backup") ? 1 : 0;
                $data["cron_job_images_backup"] = $this->input->post("cron_job_images_backup") ? 1 : 0;
                $data["cron_job_ebilling_backup"] = $this->input->post("cron_job_ebilling_backup") ? 1 : 0;
                $data["cron_job_send_mail"] = $this->input->post("cron_job_send_mail") ? 1 : 0;
                $data['protocol'] = DEMO ? 'mail' : $this->input->post('protocol');
                $data['mailpath'] = $this->input->post('mailpath');
                $data['smtp_host'] = $this->input->post('smtp_host');
                $data['smtp_user'] = $this->input->post('smtp_user');
                $data['smtp_port'] = $this->input->post('smtp_port');
                $data['big_data_limit_reports'] = $this->input->post('big_data_limit_reports');
                $data['years_database_management'] = $this->input->post('years_database_management');
                $data['purchase_datetime_management'] = $this->input->post('purchase_datetime_management');
                $data['email_new_order_sale_notification'] = $this->input->post('email_new_order_sale_notification');
                $data['hotel_module_management'] = $this->input->post('hotel_module_management');
                $data['smtp_crypto'] = $this->input->post('smtp_crypto') ? $this->input->post('smtp_crypto') : NULL;
                $data['documents_reception'] = $this->input->post('documents_reception') ? 1 : 0;
                $data['supportingDocument'] = $this->input->post('supportingDocument') ? 1 : 0;
                $data['electronic_payroll'] = $this->input->post('electronic_payroll') ? 1 : 0;
                $data['wappsi_customer_address_id'] = $this->input->post('wappsi_customer_address_id');
                $data['data_synchronization_to_store'] = $this->input->post('data_synchronization_to_store');
                $data['show_pos_movements_close_register'] = $this->input->post('show_pos_movements_close_register');
                $data['imports_module'] = $this->input->post('imports_module');
                $data['adjustments_document'] = $this->input->post('adjustments_document');
                $data['transfer_document'] = $this->input->post('transfer_document');

                $data['a_id_marca'] = $this->input->post('a_id_marca');
                $data['a_schemacia'] = $this->input->post('a_schemacia');
                $data['cid_token'] = $this->input->post('cid_token');
                $data['url_comercio'] = $this->input->post('url_comercio');
                $data['a_login'] = $this->input->post('a_login');
                $data['a_password'] = $this->input->post('a_password');
                $data['c_sucursal'] = $this->input->post('c_sucursal');

                if (md5($this->Settings->smtp_pass) != $this->input->post('smtp_pass')) {
                    $data['smtp_pass'] = $this->input->post('smtp_pass');
                }
                if (md5($this->Settings->dropbox_secret) != $this->input->post('dropbox_secret')) {
                    $data['dropbox_secret'] = $this->input->post('dropbox_secret');
                }
                if (md5($this->Settings->dropbox_token) != $this->input->post('dropbox_token')) {
                    $data['dropbox_token'] = $this->input->post('dropbox_token');
                }
                $data['electronic_hits_validation'] = $this->input->post('electronic_hits_validation') ? 1 : 0;
                $data['detailed_budget_by_seller']  = $this->input->post('detailed_budget_by_seller') ? 1 : 0;
                $data['detailed_budget_by_category']  = $this->input->post('detailed_budget_by_category') ? 1 : 0;
                $data['manage_purchases_evaluation']  = $this->input->post('manage_purchases_evaluation');
            }
            $modulary = 0;
            if ($this->Owner) {
                $modulary = $this->input->post('modulary') ? $this->input->post('modulary') : 0;
                $data['modulary'] = $modulary;
            }

            if ($this->input->post('default_cost_center')) {
                $data['default_cost_center'] = $this->input->post('default_cost_center');
            } else {
                if ($modulary && $this->Settings->modulary == 1 && $data['cost_center_selection'] != 2) {
                    $this->form_validation->set_rules('default_cost_center', lang('default_cost_center'), 'required');
                }

                $data['default_cost_center'] = NULL;
            }

            if($this->input->post('type_person') == NATURAL_PERSON) {
                $data['primer_nombre'] = $this->input->post('first_name');
                $data['segundo_nombre'] = $this->input->post('second_name');
                $data['primer_apellido'] = $this->input->post('first_lastname');
                $data['segundo_apellido'] = $this->input->post('second_lastname');
                $data['razon_social'] = $data['primer_nombre'] . (!empty($data['segundo_nombre']) ? ' '. $data['segundo_nombre'] : '') . ' '. $data['primer_apellido'] . (!empty($data['segundo_apellido']) ? ' '. $data['segundo_apellido'] : '');
                $data['nombre_comercial'] = $this->input->post('trade_name');
            } else {
                $data['razon_social'] = $this->input->post('business_name');
                $data['nombre_comercial'] = $this->input->post('business_name');
            }

            $arr_tax_rate_traslate = [];

            if ($tax_rate_traslate = $this->input->post('tax_rate_traslate')) {
                if ($tax_rate_traslate == 1) {
                    $trt1 = array(
                                'type' => 'tax_rate_traslate',
                                'entity' => 'n/a',
                                'payment_ledger_id' => $this->input->post('tax_rate_traslate_payment_ledger_id'),
                                'receipt_ledger_id' => $this->input->post('tax_rate_traslate_receipt_ledger_id')
                                );

                    $arr_tax_rate_traslate[] = $trt1;

                    $trt2 = array(
                                'type' => 'trm_difference',
                                'entity' => 'n/a',
                                'payment_ledger_id' => $this->input->post('trm_difference_payment_ledger_id'),
                                'receipt_ledger_id' => $this->input->post('trm_difference_receipt_ledger_id')
                                );

                    $arr_tax_rate_traslate[] = $trt2;

                    if ($trts_ids = $this->input->post('trt_id')) {
                        foreach ($trts_ids as $row => $trt_id) {
                            $arr_tax_rate_traslate[$row]['id'] = $trt_id;
                        }
                    }
                }
            }

            if ($_FILES['digital_signature']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path . 'signatures/';
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = 1024;
                $config['max_height'] = 400;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('digital_signature')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/index");
                }

                $file = $this->upload->file_name;
                $data['digital_signature'] = $file;
            }
            if ($this->Settings->wappsi_customer_address_id != $this->input->post('wappsi_customer_address_id')) {
                $this->db->update('settings', ['db_global_hostname_saved'=>NULL], ['setting_id'=>1]);
            }

            $type_obligations_update = $this->input->post("types_obligations");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateSetting($data, $arr_tax_rate_traslate, $type_obligations_update)) {
            if (!DEMO && TIMEZONE != $data['timezone']) {
                if (!$this->write_index($data['timezone'])) {
                    $this->session->set_flashdata('error', lang('setting_updated_timezone_failed'));
                    admin_redirect('system_settings');
                }
            }
            $this->site->delete_types_customer_obligations(1, TRANSMITTER);
            $data_types_customer_obligations = [];
            if ($this->input->post("types_obligations")) {
                foreach ($this->input->post("types_obligations") as $type_obligation) {
                    $data_types_customer_obligations[] = [
                        "customer_id" => 1,
                        "types_obligations_id" =>$type_obligation,
                        "relation"=>TRANSMITTER
                    ];
                }
                $this->settings_model->insert_type_customer_obligations($data_types_customer_obligations);
            }

            $this->session->set_flashdata('message', lang('setting_updated'));
            admin_redirect("system_settings");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['settings'] = $this->settings_model->getSettings();
            $audios = $this->getSoundsList();
            $this->data['audio_files'] = $audios['audio_files'];
            $this->data['currencies'] = $this->settings_model->getAllCurrencies();
            $this->data['date_formats'] = $this->settings_model->getDateFormats();
            $this->data['tax_rates'] = $this->settings_model->getAllTaxRates();
            $this->data['customer_groups'] = $this->settings_model->getAllCustomerGroups();
            $this->data['price_groups'] = $this->settings_model->getAllPriceGroups();
            $this->data['warehouses'] = $this->settings_model->getAllWarehouses();
            $this->data['ledgers'] = $this->site->getAllLedgers();
            $this->data['tax_rate_traslate'] = $this->settings_model->getTaxRateTraslate();
            $this->data['document_types'] = $this->site->get_document_type();
            $this->data['countries'] = $this->site->getCountries();
            $this->data["types_person"] = $this->site->get_types_person();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $this->data["types_obligations"] = $this->site->get_types_obligations();
            $this->data["types_customer_obligations"] = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $this->data["technology_providers"] = $this->settings_model->get_technology_providers();
            $this->data["expense_categories"] = $this->settings_model->get_all_expense_categories();
            $this->data["gift_card_product"] = $this->site->getProductByID($this->Settings->gift_card_product_id);
            $this->data['documentsTypes'] = $this->site->getDocumentsTypeByBillerId($this->input->post('biller_id'), [2, 4, 27], YES);
            $this->data['adjustmentsDocument'] = $this->site->get_document_types_by_module(11);
            $this->data['transferDocument'] = $this->site->get_document_types_by_module(12);
            $this->data['osdt_documents_types'] = $this->site->get_document_types_by_module(8);
            $this->data['groups'] = $this->site->getAllGroups();
            $this->data['paymentMethodForCreditFinancing'] = $this->Payment_method->get(['state_sale' => 1, 'code <>' => 'cash']);

            $Accountingtablas = $this->db->list_tables();
            $AccountingtablasConfigCount = array();
            foreach ($Accountingtablas as $tabla) {
              $pos = strpos($tabla, "settings_con");
              $pos_validate = str_replace("sma_settings_con", "", $tabla);
              if ($pos !== false && $pos_validate != "") {
                $AccountingtablasConfigCount[] = str_replace("sma_settings", "", $tabla);
              }
            }
            $this->data["contabilidad_años"] = $AccountingtablasConfigCount;
            // $this->data["technology_provider_configuration"] = $this->settings_model->get_technology_provider_configuration();
            if ($this->Settings->modulary) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }

            $this->page_construct('settings/index', ['page_title' => lang('system_settings')], $this->data);
        }
    }

    public function paypal()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('active', $this->lang->line('activate'), 'trim');
        $this->form_validation->set_rules('account_email', $this->lang->line('paypal_account_email'), 'trim|valid_email');
        if ($this->input->post('active')) {
            $this->form_validation->set_rules('account_email', $this->lang->line('paypal_account_email'), 'required');
        }
        $this->form_validation->set_rules('fixed_charges', $this->lang->line('fixed_charges'), 'trim');
        $this->form_validation->set_rules('extra_charges_my', $this->lang->line('extra_charges_my'), 'trim');
        $this->form_validation->set_rules('extra_charges_other', $this->lang->line('extra_charges_others'), 'trim');

        if ($this->form_validation->run() == true) {

            $data = array('active' => $this->input->post('active'),
                'account_email' => $this->input->post('account_email'),
                'fixed_charges' => $this->input->post('fixed_charges'),
                'extra_charges_my' => $this->input->post('extra_charges_my'),
                'extra_charges_other' => $this->input->post('extra_charges_other')
            );
        }

        if ($this->form_validation->run() == true && $this->settings_model->updatePaypal($data)) {
            $this->session->set_flashdata('message', $this->lang->line('paypal_setting_updated'));
            admin_redirect("system_settings/paypal");
        } else {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

            $this->data['paypal'] = $this->settings_model->getPaypalSettings();

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('paypal_settings')));
            $meta = array('page_title' => lang('paypal_settings'), 'bc' => $bc);
            $this->page_construct('settings/paypal', $meta, $this->data);
        }
    }

    public function skrill()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('active', $this->lang->line('activate'), 'trim');
        $this->form_validation->set_rules('account_email', $this->lang->line('paypal_account_email'), 'trim|valid_email');
        if ($this->input->post('active')) {
            $this->form_validation->set_rules('account_email', $this->lang->line('paypal_account_email'), 'required');
        }
        $this->form_validation->set_rules('fixed_charges', $this->lang->line('fixed_charges'), 'trim');
        $this->form_validation->set_rules('extra_charges_my', $this->lang->line('extra_charges_my'), 'trim');
        $this->form_validation->set_rules('extra_charges_other', $this->lang->line('extra_charges_others'), 'trim');

        if ($this->form_validation->run() == true) {

            $data = array('active' => $this->input->post('active'),
                'account_email' => $this->input->post('account_email'),
                'fixed_charges' => $this->input->post('fixed_charges'),
                'extra_charges_my' => $this->input->post('extra_charges_my'),
                'extra_charges_other' => $this->input->post('extra_charges_other')
            );
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateSkrill($data)) {
            $this->session->set_flashdata('message', $this->lang->line('skrill_setting_updated'));
            admin_redirect("system_settings/skrill");
        } else {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

            $this->data['skrill'] = $this->settings_model->getSkrillSettings();

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('skrill_settings')));
            $meta = array('page_title' => lang('skrill_settings'), 'bc' => $bc);
            $this->page_construct('settings/skrill', $meta, $this->data);
        }
    }

    public function change_logo()
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            $this->sma->md();
        }
        $this->load->helper('security');
        $this->form_validation->set_rules('site_logo', lang("site_logo"), 'xss_clean');
        $this->form_validation->set_rules('login_logo', lang("login_logo"), 'xss_clean');
        $this->form_validation->set_rules('biller_logo_square', lang("biller_logo_square"), 'xss_clean');
        $this->form_validation->set_rules('biller_logo_rectangular', lang("biller_logo_rectangular"), 'xss_clean');
        $this->form_validation->set_rules('logo_app', lang("logo_app"), 'xss_clean');
        $this->form_validation->set_rules('logo_symbol', lang("logo_symbol"), 'xss_clean');
        $this->form_validation->set_rules('entry_image_1', lang("entry_image_1"), 'xss_clean');
        $this->form_validation->set_rules('entry_image_2', lang("entry_image_2"), 'xss_clean');
        $this->form_validation->set_rules('entry_image_2', lang("entry_image_3"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $this->getFileUpload('site_logo', 300, 80);
            $this->getFileUpload('biller_logo_rectangular', 600, 160);
            $this->getFileUpload('biller_logo_square', 300, 300);
            $this->getFileUpload('logo_app', 300, 80);
            $this->getFileUpload('logo_symbol', 40, 30);

            $this->getFileUpload('entry_image_1', 1773, 1426);
            $this->getFileUpload('entry_image_2', 1773, 1426);
            $this->getFileUpload('entry_image_3', 1773, 1426);

            $this->getFileUpload('logo_zebra_32_25_variants', 300, 104);

            $this->session->set_flashdata('message', lang('logo_uploaded'));
            redirect($_SERVER["HTTP_REFERER"]);

        } elseif ($this->input->post('upload_logo')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/change_logo', $this->data);
        }
    }

    private function getFileUpload($fileName, $maxWidth, $maxHeight)
    {
        if ($_FILES[$fileName]['size'] > 0) {
            $this->load->library('upload');
            $config['upload_path'] = $this->upload_path . 'logos/';
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $maxWidth;
            $config['max_height'] = $maxHeight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            //$config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($fileName)) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $url = $this->upload->file_name;
            if ($fileName != 'biller_logo_square' && $fileName != 'biller_logo_rectangular') {
                if ($fileName == 'site_logo') {
                    $columnName = 'logo';
                } else {
                    $columnName = $fileName;
                }

                $this->db->update('settings', array($columnName => $url), array('setting_id' => 1));
            }
        }
    }

    public function write_index($timezone)
    {
        $this->sma->checkPermissions();

        $template_path = './assets/config_dumps/index.php';
        $output_path = SELF;
        $index_file = file_get_contents($template_path);
        $new = str_replace("%TIMEZONE%", $timezone, $index_file);
        $handle = fopen($output_path, 'w+');
        @chmod($output_path, 0777);

        if (is_writable($output_path)) {
            if (fwrite($handle, $new)) {
                @chmod($output_path, 0644);
                return true;
            } else {
                @chmod($output_path, 0644);
                return false;
            }
        } else {
            @chmod($output_path, 0644);
            return false;
        }
    }

    public function updates()
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->form_validation->set_rules('purchase_code', lang("purchase_code"), 'required');
        $this->form_validation->set_rules('envato_username', lang("envato_username"), 'required');
        if ($this->form_validation->run() == true) {
            $this->db->update('settings', array('purchase_code' => $this->input->post('purchase_code', TRUE), 'envato_username' => $this->input->post('envato_username', TRUE)), array('setting_id' => 1));
            admin_redirect('system_settings/updates');
        } else {
            $fields = array('version' => $this->Settings->version, 'code' => $this->Settings->purchase_code, 'username' => $this->Settings->envato_username, 'site' => base_url());
            $this->load->helper('update');
            $protocol = is_https() ? 'https://' : 'http://';
            $updates = get_remote_contents($protocol.'api.tecdiary.com/v1/update/', $fields);
            $this->data['updates'] = json_decode($updates);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('updates')));
            $meta = array('page_title' => lang('updates'), 'bc' => $bc);
            $this->page_construct('settings/updates', $meta, $this->data);
        }
    }

    public function install_update($file, $m_version, $version)
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->load->helper('update');
        save_remote_file($file . '.zip');
        $this->sma->unzip('./files/updates/' . $file . '.zip');
        if ($m_version) {
            $this->load->library('migration');
            if (!$this->migration->latest()) {
                $this->session->set_flashdata('error', $this->migration->error_string());
                admin_redirect("system_settings/updates");
            }
        }
        $this->db->update('settings', array('version' => $version, 'update' => 0), array('setting_id' => 1));
        unlink('./files/updates/' . $file . '.zip');
        $this->session->set_flashdata('success', lang('update_done'));
        admin_redirect("system_settings/updates");
    }

    public function backups()
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->data['files'] = glob('./files/backups/*.zip', GLOB_BRACE);
        $this->data['dbs'] = glob('./files/backups/*.txt', GLOB_BRACE);
        krsort($this->data['files']); krsort($this->data['dbs']);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('backups')));
        $meta = array('page_title' => lang('backups'), 'bc' => $bc);
        $this->page_construct('settings/backups', $meta, $this->data);
    }

    public function backup_database()
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->load->dbutil();
        $prefs = array(
            'format' => 'txt',
            'filename' => 'sma_db_backup.sql'
        );
        $back = $this->dbutil->backup($prefs);
        $backup =& $back;
        $db_name = 'db-backup-on-' . date("Y-m-d-H-i-s") . '.txt';
        $save = './files/backups/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);
        $this->session->set_flashdata('messgae', lang('db_saved'));
        admin_redirect("system_settings/backups");
    }

    public function backup_files()
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $name = 'file-backup-' . date("Y-m-d-H-i-s");
        $this->sma->zip("./", './files/backups/', $name);
        $this->session->set_flashdata('messgae', lang('backup_saved'));
        admin_redirect("system_settings/backups");
        exit();
    }

    public function restore_database($dbfile)
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $file = file_get_contents('./files/backups/' . $dbfile . '.txt');
        // $this->db->conn_id->multi_query($file);
        mysqli_multi_query($this->db->conn_id, $file);
        $this->db->conn_id->close();
        admin_redirect('logout/db');
    }

    public function download_database($dbfile)
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->load->library('zip');
        $this->zip->read_file('./files/backups/' . $dbfile . '.txt');
        $name = $dbfile . '.zip';
        $this->zip->download($name);
        exit();
    }

    public function download_backup($zipfile)
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $this->load->helper('download');
        force_download('./files/backups/' . $zipfile . '.zip', NULL);
        exit();
    }

    public function restore_backup($zipfile)
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        $file = './files/backups/' . $zipfile . '.zip';
        $this->sma->unzip($file, './');
        $this->session->set_flashdata('success', lang('files_restored'));
        admin_redirect("system_settings/backups");
        exit();
    }

    public function delete_database($dbfile)
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        unlink('./files/backups/' . $dbfile . '.txt');
        $this->session->set_flashdata('messgae', lang('db_deleted'));
        admin_redirect("system_settings/backups");
    }

    public function delete_backup($zipfile)
    {
        $this->sma->checkPermissions();
        if (DEMO) {
            $this->session->set_flashdata('warning', lang('disabled_in_demo'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            admin_redirect("welcome");
        }
        unlink('./files/backups/' . $zipfile . '.zip');
        $this->session->set_flashdata('messgae', lang('backup_deleted'));
        admin_redirect("system_settings/backups");
    }

    public function email_templates($template = "credentials")
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('mail_body', lang('mail_message'), 'trim|required');
        $this->load->helper('file');
        $temp_path = is_dir('./themes/' . $this->theme . 'email_templates/');
        $theme = $temp_path ? $this->theme : 'default';
        if ($this->form_validation->run() == true) {
            $data = $_POST["mail_body"];
            if (write_file('./themes/' . $this->theme . 'email_templates/' . $template . '.html', $data)) {
                $this->session->set_flashdata('message', lang('message_successfully_saved'));
                admin_redirect('system_settings/email_templates#' . $template);
            } else {
                $this->session->set_flashdata('error', lang('failed_to_save_message'));
                admin_redirect('system_settings/email_templates#' . $template);
            }
        } else {

            $this->data['credentials'] = file_get_contents('./themes/' . $this->theme . 'email_templates/credentials.html');
            $this->data['sale'] = file_get_contents('./themes/' . $this->theme . 'email_templates/sale.html');
            $this->data['pos_sale'] = file_get_contents('./themes/' . $this->theme . 'email_templates/pos_sale.html');
            $this->data['order_sale'] = file_get_contents('./themes/' . $this->theme . 'email_templates/order_sale.html');
            $this->data['staff_order_sale'] = file_get_contents('./themes/' . $this->theme . 'email_templates/staff_order_sale.html');
            $this->data['quote'] = file_get_contents('./themes/' . $this->theme . 'email_templates/quote.html');
            $this->data['quote_purchase'] = file_get_contents('./themes/' . $this->theme . 'email_templates/quote_purchase.html');
            $this->data['purchase'] = file_get_contents('./themes/' . $this->theme . 'email_templates/purchase.html');
            $this->data['transfer'] = file_get_contents('./themes/' . $this->theme . 'email_templates/transfer.html');
            $this->data['payment'] = file_get_contents('./themes/' . $this->theme . 'email_templates/payment.html');
            $this->data['forgot_password'] = file_get_contents('./themes/' . $this->theme . 'email_templates/forgot_password.html');
            $this->data['activate_email'] = file_get_contents('./themes/' . $this->theme . 'email_templates/activate_email.html');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('email_templates')));
            $meta = array('page_title' => lang('email_templates'), 'bc' => $bc);
            $this->page_construct('settings/email_templates', $meta, $this->data);
        }
    }

    public function create_group()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('group_name', lang('group_name'), 'required|alpha_dash|is_unique[groups.name]');

        if ($this->form_validation->run() == TRUE) {
            $data = array(
                            'name' => strtolower($this->input->post('group_name')),
                            'description' => $this->input->post('description'),
                            'relationed_companies' => $this->input->post('relationed_companies'),
                        );
        } elseif ($this->input->post('create_group')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/user_groups");
        }

        if ($this->form_validation->run() == TRUE && ($new_group_id = $this->settings_model->addGroup($data))) {
            $this->session->set_flashdata('message', lang('group_added'));
            admin_redirect("system_settings/permissions/" . $new_group_id);

        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['group_name'] = array(
                'name' => 'group_name',
                'id' => 'group_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('group_name'),
            );
            $this->data['description'] = array(
                'name' => 'description',
                'id' => 'description',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('description'),
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/create_group', $this->data);
        }
    }

    public function edit_group($id)
    {
        $this->sma->checkPermissions();

        if (!$id || empty($id)) {
            admin_redirect('system_settings/user_groups');
        }

        $group = $this->settings_model->getGroupByID($id);

        $this->form_validation->set_rules('group_name', lang('group_name'), 'required|alpha_dash');

        if ($this->form_validation->run() === TRUE) {
            $data = array(
                            'name' => strtolower($this->input->post('group_name')),
                            'description' => $this->input->post('description'),
                            'relationed_companies' => $this->input->post('relationed_companies'),
                        );
            $group_update = $this->settings_model->updateGroup($id, $data);

            if ($group_update) {
                $this->session->set_flashdata('message', lang('group_udpated'));
            } else {
                $this->session->set_flashdata('error', lang('attempt_failed'));
            }
            admin_redirect("system_settings/user_groups");
        } else {


            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['group'] = $group;

            $this->data['group_name'] = array(
                'name' => 'group_name',
                'id' => 'group_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('group_name', $group->name),
            );
            $this->data['group_description'] = array(
                'name' => 'group_description',
                'id' => 'group_description',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('group_description', $group->description),
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_group', $this->data);
        }
    }

    public function permissions($id = NULL)
    {
        $this->sma->checkPermissions();

        $setting = new Settings_model();

        $this->form_validation->set_rules('group', lang("group"), 'is_natural_no_zero');
        if ($this->form_validation->run() == true) {
            $data = array(
                'products-index' => $this->input->post('products-index'),
                'products-edit' => $this->input->post('products-edit'),
                'products-add' => $this->input->post('products-add'),
                'products-delete' => $this->input->post('products-delete'),
                'products-cost' => $this->input->post('products-cost'),
                'products-price' => $this->input->post('products-price'),
                'products-quantity' => $this->input->post('products-quantity'),
                'products-view_graph' => $this->input->post('products-view_graph'),
                'products-view_sales' => $this->input->post('products-view_sales'),
                'products-view_quotes' => $this->input->post('products-view_quotes'),
                'products-view_purchases' => $this->input->post('products-view_purchases'),
                'products-view_transfers' => $this->input->post('products-view_transfers'),
                'products-view_adjustments' => $this->input->post('products-view_adjustments'),
                'customers-index' => $this->input->post('customers-index'),
                'customers-edit' => $this->input->post('customers-edit'),
                'customers-add' => $this->input->post('customers-add'),
                'customers-delete' => $this->input->post('customers-delete'),
                'suppliers-index' => $this->input->post('suppliers-index'),
                'suppliers-edit' => $this->input->post('suppliers-edit'),
                'suppliers-add' => $this->input->post('suppliers-add'),
                'suppliers-delete' => $this->input->post('suppliers-delete'),
                'sales-index' => $this->input->post('sales-index'),
                'sales-edit' => $this->input->post('sales-edit'),
                'sales-add' => $this->input->post('sales-add'),
                'sales-delete' => $this->input->post('sales-delete'),
                'sales-email' => $this->input->post('sales-email'),
                'sales-pdf' => $this->input->post('sales-pdf'),
                'sales-deliveries' => $this->input->post('sales-deliveries'),
                'sales-edit_delivery' => $this->input->post('sales-edit_delivery'),
                'sales-add_delivery' => $this->input->post('sales-add_delivery'),
                'sales-delete_delivery' => $this->input->post('sales-delete_delivery'),
                'sales-email_delivery' => $this->input->post('sales-email_delivery'),
                'sales-pdf_delivery' => $this->input->post('sales-pdf_delivery'),
                'sales-gift_cards' => $this->input->post('sales-gift_cards'),
                'sales-edit_gift_card' => $this->input->post('sales-edit_gift_card'),
                'sales-add_gift_card' => $this->input->post('sales-add_gift_card'),
                'sales-delete_gift_card' => $this->input->post('sales-delete_gift_card'),
                'sales-print_server' => $this->input->post('sales-print_server'),
                'quotes-index' => $this->input->post('quotes-index'),
                'quotes-edit' => $this->input->post('quotes-edit'),
                'quotes-add' => $this->input->post('quotes-add'),
                'quotes-delete' => $this->input->post('quotes-delete'),
                'quotes-email' => $this->input->post('quotes-email'),
                'quotes-pdf' => $this->input->post('quotes-pdf'),
                'purchases-index' => $this->input->post('purchases-index'),
                'purchases-edit' => $this->input->post('purchases-edit'),
                'purchases-add' => $this->input->post('purchases-add'),
                'purchases-delete' => $this->input->post('purchases-delete'),
                'purchases-email' => $this->input->post('purchases-email'),
                'purchases-pdf' => $this->input->post('purchases-pdf'),
                'transfers-index' => $this->input->post('transfers-index'),
                'transfers-edit' => $this->input->post('transfers-edit'),
                'transfers-add' => $this->input->post('transfers-add'),
                'transfers-delete' => $this->input->post('transfers-delete'),
                'transfers-email' => $this->input->post('transfers-email'),
                'transfers-pdf' => $this->input->post('transfers-pdf'),
                'sales-return_sales' => $this->input->post('sales-return_sales'),
                'sales-return_sales_fe' => $this->input->post('sales-return_sales_fe'),
                'reports-quantity_alerts' => $this->input->post('reports-quantity_alerts'),
                'reports-expiry_alerts' => $this->input->post('reports-expiry_alerts'),
                'reports-products' => $this->input->post('reports-products'),
                'reports-daily_sales' => $this->input->post('reports-daily_sales'),
                'reports-monthly_sales' => $this->input->post('reports-monthly_sales'),
                'reports-payments' => $this->input->post('reports-payments'),
                'reports-sales' => $this->input->post('reports-sales'),
                'reports-purchases' => $this->input->post('reports-purchases'),
                'reports-customers' => $this->input->post('reports-customers'),
                'reports-suppliers' => $this->input->post('reports-suppliers'),
                'sales-payments' => $this->input->post('sales-payments'),
                'purchases-payments' => $this->input->post('purchases-payments'),
                'purchases-expenses' => $this->input->post('purchases-expenses'),
                'products-adjustments' => $this->input->post('products-adjustments'),
                'bulk_actions' => $this->input->post('bulk_actions'),
                'customers-list_deposits' => $this->input->post('customers-list_deposits'),
                'customers-add_deposit' => $this->input->post('customers-add_deposit'),
                'suppliers-list_deposits' => $this->input->post('suppliers-list_deposits'),
                'suppliers-add_deposit' => $this->input->post('suppliers-add_deposit'),
                // 'customers-delete_deposit' => $this->input->post('customers-delete_deposit'),
                'products-barcode' => $this->input->post('products-barcode'),
                'purchases-return_purchases' => $this->input->post('purchases-return_purchases'),
                'reports-expenses' => $this->input->post('reports-expenses'),
                'reports-daily_purchases' => $this->input->post('reports-daily_purchases'),
                'reports-monthly_purchases' => $this->input->post('reports-monthly_purchases'),
                'products-stock_count' => $this->input->post('products-stock_count'),
                'edit_price' => $this->input->post('edit_price'),
                'returns-index' => $this->input->post('returns-index'),
                'returns-edit' => $this->input->post('returns-edit'),
                'returns-add' => $this->input->post('returns-add'),
                'returns-delete' => $this->input->post('returns-delete'),
                'returns-email' => $this->input->post('returns-email'),
                'returns-pdf' => $this->input->post('returns-pdf'),
                'reports-tax' => $this->input->post('reports-tax'),
                'reports-payment_term_expired' => $this->input->post('reports-payment_term_expired'),
                'payments-add' => $this->input->post('payments-add'),
                'payments-index' => $this->input->post('payments-index'),
                'system_settings-categories' => $this->input->post('system_settings-categories'),
                'system_settings-add_category' => $this->input->post('system_settings-add_category'),
                'system_settings-edit_category' => $this->input->post('system_settings-edit_category'),
                'system_settings-delete_category' => $this->input->post('system_settings-delete_category'),
                'system_settings-units' => $this->input->post('system_settings-units'),
                'system_settings-add_unit' => $this->input->post('system_settings-add_unit'),
                'system_settings-edit_unit' => $this->input->post('system_settings-edit_unit'),
                'system_settings-delete_unit' => $this->input->post('system_settings-delete_unit'),
                'system_settings-brands' => $this->input->post('system_settings-brands'),
                'system_settings-add_brand' => $this->input->post('system_settings-add_brand'),
                'system_settings-edit_brand' => $this->input->post('system_settings-edit_brand'),
                'system_settings-delete_brand' => $this->input->post('system_settings-delete_brand'),
                'system_settings-tax_rates' => $this->input->post('system_settings-tax_rates'),
                'system_settings-add_tax_rate' => $this->input->post('system_settings-add_tax_rate'),
                'system_settings-edit_tax_rate' => $this->input->post('system_settings-edit_tax_rate'),
                'system_settings-delete_tax_rate' => $this->input->post('system_settings-delete_tax_rate'),
                'system_settings-payment_methods' => $this->input->post('system_settings-payment_methods'),
                'system_settings-add_payment_method' => $this->input->post('system_settings-add_payment_method'),
                'system_settings-edit_payment_method' => $this->input->post('system_settings-edit_payment_method'),
                'system_settings-delete_payment_method' => $this->input->post('system_settings-delete_payment_method'),
                'sales-fe_index' => $this->input->post('sales-fe_index'),
                'reports-best_sellers' => $this->input->post('reports-best_sellers'),
                'reports-valued_products' => $this->input->post('reports-valued_products'),
                'reports-adjustments' => $this->input->post('reports-adjustments'),
                'reports-categories' => $this->input->post('reports-categories'),
                'reports-brands' => $this->input->post('reports-brands'),
                'reports-users' => $this->input->post('reports-users'),
                'reports-portfolio' => $this->input->post('reports-portfolio'),
                'reports-portfolio_report' => $this->input->post('reports-portfolio_report'),
                'reports-debts_to_pay_report' => $this->input->post('reports-debts_to_pay_report'),
                'reports-load_zeta' => $this->input->post('reports-load_zeta'),
                'reports-load_bills' => $this->input->post('reports-load_bills'),
                'reports-load_rentabilidad_doc' => $this->input->post('reports-load_rentabilidad_doc'),
                'reports-load_rentabilidad_customer' => $this->input->post('reports-load_rentabilidad_customer'),
                'reports-load_rentabilidad_producto' => $this->input->post('reports-load_rentabilidad_producto'),
                'shop_settings-index' => $this->input->post('shop_settings-index'),
                'shop_settings-slider' => $this->input->post('shop_settings-slider'),
                'shop_settings-pages' => $this->input->post('shop_settings-pages'),
                'shop_settings-add_page' => $this->input->post('shop_settings-add_page'),
                'shop_settings-edit_page' => $this->input->post('shop_settings-edit_page'),
                'shop_settings-delete_page' => $this->input->post('shop_settings-delete_page'),
                'shop_settings-sms_settings' => $this->input->post('shop_settings-sms_settings'),
                'shop_settings-send_sms' => $this->input->post('shop_settings-send_sms'),
                'shop_settings-sms_log' => $this->input->post('shop_settings-sms_log'),
                'sales-orders' => $this->input->post('sales-orders'),
                'sales-add_order' => $this->input->post('sales-add_order'),
                'sales-edit_order' => $this->input->post('sales-edit_order'),
                'products-production_orders' => $this->input->post('products-production_orders'),
                'products-add_production_order' => $this->input->post('products-add_production_order'),
                'payments-pindex' => $this->input->post('payments-pindex'),
                'payments-padd' => $this->input->post('payments-padd'),
                'system_settings-update_products_group_prices' => $this->input->post('system_settings-update_products_group_prices'),
                'products-edit_group_prices' => $this->input->post('products-edit_group_prices'),
                'pos-pos_register_movements' => $this->input->post('pos-pos_register_movements'),
                'pos-pos_register_add_movement' => $this->input->post('pos-pos_register_add_movement'),
                'payments-cancel' => $this->input->post('payments-cancel'),
                'payments-pcancel' => $this->input->post('payments-pcancel'),
                'products-product_transformations' => $this->input->post('products-product_transformations'),
                'products-add_product_transformation' => $this->input->post('products-add_product_transformation'),
                'products-add_product_transformation' => $this->input->post('products-add_product_transformation'),
                'pos_print_server' => $this->input->post('pos_print_server'),
                'returns-credit_note_other_concepts' => $this->input->post('returns-credit_note_other_concepts'),
                'production_order-index' => $this->input->post('production_order-index'),
                'production_order-add' => $this->input->post('production_order-add'),
                'production_order-edit' => $this->input->post('production_order-edit'),
                'production_order-cutting_orders' => $this->input->post('production_order-cutting_orders'),
                'production_order-add_cutting_order' => $this->input->post('production_order-add_cutting_order'),
                'production_order-assemble_orders' => $this->input->post('production_order-assemble_orders'),
                'production_order-add_assemble_order' => $this->input->post('production_order-add_assemble_order'),
                'production_order-packing_orders' => $this->input->post('production_order-packing_orders'),
                'production_order-add_packing_order' => $this->input->post('production_order-add_packing_order'),
                'production_order-change_cutting_order_items_status' => $this->input->post('production_order-change_cutting_order_items_status'),
                'production_order-change_assemble_order_items_status' => $this->input->post('production_order-change_assemble_order_items_status'),
                'production_order-change_packing_order_items_status' => $this->input->post('production_order-change_packing_order_items_status'),
                'production_order-production_order_report' => $this->input->post('production_order-production_order_report'),
                'reports-collection_commissions' => $this->input->post('reports-collection_commissions'),
                'sales-pay_commissions' => $this->input->post('sales-pay_commissions'),
                'purchases-return_other_concepts' => $this->input->post('purchases-return_other_concepts'),
                'payroll_management-index' => $this->input->post('payroll_management-index'),
                'payroll_management-add' => $this->input->post('payroll_management-add'),
                'payroll_management-delete' => $this->input->post('payroll_management-delete'),
                'payroll_management-send' => $this->input->post('payroll_management-send'),
                'payroll_management-update' => $this->input->post('payroll_management-update'),
                'payroll_management-approve' => $this->input->post('payroll_management-approve'),
                'payroll_management-add_novelty' => $this->input->post('payroll_management-add_novelty'),
                'documentsReception-index' => $this->input->post('documentsReception-index'),
                'documentsReception-claim' => $this->input->post('documentsReception-claim'),
                'documentsReception-acknowledgment' => $this->input->post('documentsReception-acknowledgment'),
                'documentsReception-reception_good_service' => $this->input->post('documentsReception-reception_good_service'),
                'documentsReception-express_acceptance' => $this->input->post('documentsReception-express_acceptance'),
                'supportingDocument-index' => $this->input->post('supportingDocument-index'),
                'supportingDocument-add' => $this->input->post('supportingDocument-add'),
                'supportingDocument-edit' => $this->input->post('supportingDocument-edit'),
                'supportingDocument-delete' => $this->input->post('supportingDocument-delete'),
                'supportingDocument-adjustmentNote' => $this->input->post('supportingDocument-adjustmentNote'),
                'expense_categories-index' => $this->input->post('expense_categories-index'),
                'expense_categories-add' => $this->input->post('expense_categories-add'),
                'expense_categories-edit' => $this->input->post('expense_categories-edit'),
                'products-view_kardex' => $this->input->post('products-view_kardex'),

                'hotel-guests' => $this->input->post('hotel-guests'),
                'hotel-add_guest' => $this->input->post('hotel-add_guest'),
                'hotel-edit_guest' => $this->input->post('hotel-edit_guest'),
                'hotel-guest_registers' => $this->input->post('hotel-guest_registers'),
                'hotel-add_guest_register' => $this->input->post('hotel-add_guest_register'),
                'hotel-edit_guest_register' => $this->input->post('hotel-edit_guest_register'),
                'hotel-close_guest_register' => $this->input->post('hotel-close_guest_register'),

                'pos-order_preparation' => $this->input->post('pos-order_preparation'),
                'pos-see_order_preparation' => $this->input->post('pos-see_order_preparation'),
                'pos-change_payment_method' => $this->input->post('pos-change_payment_method'),
                'returns-add_past_years_sale_return' => $this->input->post('returns-add_past_years_sale_return'),

                'system_settings-user_activities' => $this->input->post('system_settings-user_activities'),
                'auth-users' => $this->input->post('auth-users'),
                'auth-create_user' => $this->input->post('auth-create_user'),
                'auth-profile' => $this->input->post('auth-profile'),

                'system_settings-variants' => $this->input->post('system_settings-variants'),
                'system_settings-colors' => $this->input->post('system_settings-colors'),
                'system_settings-materials' => $this->input->post('system_settings-materials'),

                'system_settings-addMaterial' => $this->input->post('system_settings-addMaterial'),
                'system_settings-editMaterial' => $this->input->post('system_settings-editMaterial'),
                'system_settings-addColor' => $this->input->post('system_settings-addColor'),
                'system_settings-editColor' => $this->input->post('system_settings-editColor'),
                'system_settings-add_variant' => $this->input->post('system_settings-add_variant'),
                'system_settings-edit_variant' => $this->input->post('system_settings-edit_variant'),
                'system_settings-delete_variant' => $this->input->post('system_settings-delete_variant'),
                'warranty-index' => $this->input->post("warranty-index"),
                'warranty-add' => $this->input->post("warranty-add"),
                'warranty-edit' => $this->input->post("warranty-edit"),
                'warranty-delete' => $this->input->post("warranty-delete"),

                'payments_collections-index' => $this->input->post("payments_collections-index"),
                'payments_collections-add' => $this->input->post("payments_collections-add"),
                'pos-close_register' => $this->input->post("pos-close_register"),
                'pos-delete_table_order' => $this->input->post("pos-delete_table_order"),
                'purchases-add_items_by_csv' => $this->input->post("purchases-add_items_by_csv"),
                'system_settings-get_products_units_xls' => $this->input->post("system_settings-get_products_units_xls"),
                'reports-register' => $this->input->post('reports-register'),
                'purchases-delete_purchase' => $this->input->post('purchases-delete_purchase'),
                'purchases-edition_delete_item' => $this->input->post('purchases-edition_delete_item'),
                'products-import_csv' => $this->input->post('products-import_csv'),
                'reports-biller_register' => $this->input->post('reports-biller_register'),
                'reports-serial_register' => $this->input->post('reports-serial_register'),

                'financing-index' => $this->input->post('financing-index'),
                'financing-add' => $this->input->post('financing-add'),
                'financing-edit' => $this->input->post('financing-edit'),
                'financing-delete' => $this->input->post('financing-delete'),
                'financing-make_collections' => $this->input->post('financing-make_collections'),
                'financing-make_collections_reported' => $this->input->post('financing-make_collections_reported'),
                'financing-installment_edit' => $this->input->post('financing-installment_edit'),

                'budget-index' => $this->input->post('budget-index'),
                'sales_reports-budget_execution' => $this->input->post('sales_reports-budget_execution'),
                'reports-transfers' => $this->input->post('reports-transfers'),

                "products-sync_pro_store_products"  => $this->input->post("products-sync_pro_store_products"),
                "products-sync_pro_store_prices_quantities" => $this->input->post("products-sync_pro_store_prices_quantities"),
                "products-hide_products_without_photos" => $this->input->post("products-hide_products_without_photos"),

                'transfers-orders' => $this->input->post('transfers-orders'),
                'transfers-add_order' => $this->input->post('transfers-add_order'),
                'debit_notes-add' => $this->input->post('debit_notes-add'),
                'returns-add_past_years_purchase_return' => $this->input->post('returns-add_past_years_purchase_return'),
                'products-view_order_sales' => $this->input->post('products-view_order_sales'),
                'reports-daily_incomes' => $this->input->post('reports-daily_incomes'),
            );
            if (POS) {
                $data['pos-index'] = $this->input->post('pos-index');
                $data['pos-sales'] = $this->input->post('pos-sales');
                $data['pos-add_wholesale'] = $this->input->post('pos-add_wholesale');
            }
        }
        if ($this->form_validation->run() == true && $setting->updatePermissions($id, $data)) {
            $this->session->set_flashdata('message', lang("group_permissions_updated"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['id'] = $id;
            $permissions = $setting->getGroupPermissions($id);
            if (!$permissions) {
                $setting->create_new_permissions_for_group($id);
                $permissions = $setting->getGroupPermissions($id);
            }
            $this->data['p'] = $permissions;
            $this->data['group'] = $setting->getGroupByID($id);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('group_permissions')));
            $meta = array('page_title' => lang('group_permissions'), 'bc' => $bc);
            $this->page_construct('settings/permissions', $meta, $this->data);
        }
    }

    public function user_groups()
    {
        $this->sma->checkPermissions();

        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('error', lang("access_denied"));
            admin_redirect('auth');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['groups'] = $this->settings_model->getGroups();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('groups')));
        $meta = array('page_title' => lang('groups'), 'bc' => $bc);
        $this->page_construct('settings/user_groups', $meta, $this->data);
    }

    public function delete_group($id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->settings_model->checkGroupUsers($id)) {
            $this->session->set_flashdata('error', lang("group_x_b_deleted"));
            admin_redirect("system_settings/user_groups");
        }

        if ($this->settings_model->deleteGroup($id)) {
            $this->session->set_flashdata('message', lang("group_deleted"));
            admin_redirect("system_settings/user_groups");
        }
    }

    public function currencies()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('currencies')));
        $meta = array('page_title' => lang('currencies'), 'bc' => $bc);
        $this->page_construct('settings/currencies', $meta, $this->data);
    }

    public function getCurrencies()
    {
        $this->sma->checkPermissions();

        $this->load->library('datatables');
        $this->datatables
            ->select("id, code, name, rate, symbol")
            ->from("currencies")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('system_settings/edit_currency/$1') . "' class='tip' title='" . lang("edit_currency") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_currency") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_currency/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }

    public function add_currency()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('code', lang("currency_code"), 'trim|is_unique[currencies.code]|required');
        $this->form_validation->set_rules('name', lang("name"), 'required');
        $this->form_validation->set_rules('rate', lang("exchange_rate"), 'required|numeric');

        if ($this->form_validation->run() == true) {
            $data = array('code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'rate' => $this->input->post('rate'),
                'symbol' => $this->input->post('symbol'),
                'auto_update' => $this->input->post('auto_update') ? $this->input->post('auto_update') : 0,
            );
        } elseif ($this->input->post('add_currency')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/currencies");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addCurrency($data)) { //check to see if we are creating the customer
            $this->session->set_flashdata('message', lang("currency_added"));
            admin_redirect("system_settings/currencies");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['page_title'] = lang("new_currency");
            $this->load_view($this->theme . 'settings/add_currency', $this->data);
        }
    }

    public function edit_currency($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('code', lang("currency_code"), 'trim|required');
        $cur_details = $this->settings_model->getCurrencyByID($id);
        if ($this->input->post('code') != $cur_details->code) {
            $this->form_validation->set_rules('code', lang("currency_code"), 'required|is_unique[currencies.code]');
        }
        $this->form_validation->set_rules('name', lang("currency_name"), 'required');
        $this->form_validation->set_rules('rate', lang("exchange_rate"), 'required|numeric');

        if ($this->form_validation->run() == true) {

            $data = array('code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'rate' => $this->input->post('rate'),
                'symbol' => $this->input->post('symbol'),
                'auto_update' => $this->input->post('auto_update') ? $this->input->post('auto_update') : 0,
            );
        } elseif ($this->input->post('edit_currency')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/currencies");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateCurrency($id, $data)) { //check to see if we are updateing the customer
            $this->session->set_flashdata('message', lang("currency_updated"));
            admin_redirect("system_settings/currencies");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['currency'] = $this->settings_model->getCurrencyByID($id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_currency', $this->data);
        }
    }

    public function delete_currency($id = NULL)
    {
        $this->sma->checkPermissions();
        if ($this->settings_model->currencyHasMovements($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("currency_has_movements")));
        }

        if ($this->settings_model->deleteCurrency($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("currency_deleted")));
        }
    }

    public function currency_actions()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->deleteCurrency($id);
                    }
                    $this->session->set_flashdata('message', lang("currencies_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('currencies'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('rate'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->settings_model->getCurrencyByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->rate);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'currencies_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function categories()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('categories')));
        $meta = array('page_title' => lang('categories'), 'bc' => $bc);
        $this->page_construct('settings/categories', $meta, $this->data);
    }

    public function getCategories()
    {
        // $this->sma->checkPermissions();

        $print_barcode = anchor('admin/products/print_barcodes/?category=$1', '<i class="fa fa-print"></i> '.lang('print_barcodes'), 'class="tip"');

        $this->load->library('datatables');
        $this->datatables
            ->select("
                            {$this->db->dbprefix('categories')}.id as id,
                            {$this->db->dbprefix('categories')}.image,
                            {$this->db->dbprefix('categories')}.code,
                            {$this->db->dbprefix('categories')}.name,
                            {$this->db->dbprefix('categories')}.slug,
                            c.name as parent,
                            s.name as sub,
                            {$this->db->dbprefix('categories')}.synchronized_store")
            ->from("categories")
            ->join("categories c", 'c.id=categories.parent_id', 'left')
            ->join("categories s", 's.id=categories.subcategory_id', 'left')
            ->group_by('categories.id')
            ->add_column("Actions", "<div class=\"text-center\">
                                        <div class=\"btn-group text-left\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs new-button new-button-sm dropdown-toggle\" data-toggle=\"dropdown\">
                                                <i class='fas fa-ellipsis-v fa-lg'></i>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li>
                                                    ".$print_barcode."
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('system_settings/edit_category/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title=''><i class=\"fa fa-edit\"></i> " . sprintf(lang('edit_category'), lang('category')) . "</a>
                                                </li>
                                                <li>
                                                    <a href='#' class='tip po' title='<b></b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_category/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> " . sprintf(lang('delete_category'), lang('category')) . "</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>", "id");

        echo $this->datatables->generate();
    }

    public function add_category()
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('code', sprintf(lang('category_code'), lang('category')), 'trim|is_unique[categories.code]|required');
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('slug', lang("slug"), 'required|is_unique[categories.slug]|alpha_dash');
        $this->form_validation->set_rules('userfile', lang("category_image"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'parent_id' => $this->input->post('parent'),
                'printer_id' => $this->input->post('printer'),
                'preparer_id' => $this->input->post('user_preparation'),
                'hide' => $this->input->post('hide_category_products') ? 1 : 0,
                'except_category_taxes' => $this->input->post('except_category_taxes') ? 1 : 0,
                'profitability_margin' => $this->input->post('profitability_margin'),
                'profitability_margin_price_base' => $this->input->post('profitability_margin_price_base'),
                'suggested_rate' => $this->input->post('tax_rate'),
                'subcategory_id' => $this->input->post('subcategory'),
                'hide_ecommerce' => $this->input->post('hide_ecommerce') ? 1 : 0,
                'code_consecutive' => $this->input->post('code_consecutive'),
                );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('add_category')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/categories");
        }

        if ($this->form_validation->run() == true && $categoryId = $this->settings_model->addCategory($data)) {
            $syncCreateCategory = $this->syncCreatedCategory($categoryId);

            $this->session->set_flashdata('message', lang("category_added"));

            if (isset($syncCreateCategory->warning) && !empty($syncCreateCategory->warning)) {
                $this->session->set_flashdata('warning', lang("category_added") .  $syncCreateCategory->warning);
            }
            if (isset($syncCreateCategory->message) && !empty($syncCreateCategory->message)) {
                $this->session->set_flashdata('message', lang("category_added") .  $syncCreateCategory->message);
            }

            admin_redirect("system_settings/categories");
        } else {

            $printers = false;
            $users_preparations = false;

            if ($this->pos_settings->restobar_mode) {
                if ($this->pos_settings->order_print_mode == 1) {
                    $printers[] = lang('select');
                    $printers_d = $this->site->getAllPrinters();
                    foreach ($printers_d as $printer) {
                        $printers[$printer->id] = $printer->title;
                    }
                } else if ($this->pos_settings->order_print_mode == 0) {
                    $users_preparations[] = lang('select');
                    $group_n = $this->site->getUserGroupByName('preparation');
                    $preparers = $this->site->getUsersByGroupId($group_n->id);
                    foreach ($preparers as $preparer) {
                        $users_preparations[$preparer->id] = $preparer->first_name." ".$preparer->last_name;
                    }
                }
            }

            $this->data['printers'] = $printers;
            $this->data['users_preparations'] = $users_preparations;
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['categories'] = $this->settings_model->getParentCategories();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['tax_rates'] = $this->settings_model->getAllTaxRates();
            $this->load_view($this->theme . 'settings/add_category', $this->data);

        }
    }

    private function syncCreatedCategory($id)
    {
        $_message = '';
        $_errorMessage = '';
        $response = [];
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            $category = $this->settings_model->getCategoryByID($id);

            if ($category->hide_ecommerce == YES) {
                return (object) $response;
            }

            if (!empty($integrations) && !empty($category)) {
                $this->load->integration_model('Category');
                foreach ($integrations as $integration) {
                    $Category = new Category();
                    $Category->open($integration);
                    $categoryExt = $Category->find(["id_wappsi"=>$category->id]);

                    if (empty($categoryExt)) {
                        $categoryLevel = $this->getCategoryLevel($category, $Category);
                        $slug = !empty($category->slug) ? $category->slug : $this->sma->slug($category->name);
                        $data = [
                            "parent_id" => $categoryLevel->parent_id,
                            "level" => $categoryLevel->level,
                            "name" => $category->name,
                            "order_level" => 0,
                            "featured" => 1,
                            "slug" => $slug,
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s"),
                            "id_wappsi" => $category->id,
                        ];
                        if ($categoryId = $Category->create($data)) {
                            $this->settings_model->updateCategory($id, ["synchronized_store" => YES]);
                            $this->addAttributeCategory($integration, $categoryId);
                            $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                        } else {
                            $_errorMessage .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                        }
                    }

                    $Category->close();
                }
                if (!empty($_message)) {
                    $response["message"] = " Sincronizado en: ". rtrim($_message, ", ");
                }
                if (!empty($_errorMessage)) {
                    $response["warning"] = " No fue posible sincronizar en: ". rtrim($_errorMessage, ", ");
                }
            }
        }
        return (object) $response;
    }

    private function getCategoryLevel($data, $Category)
    {
        if (empty($data->parent_id)) {
            $categoryLevel = ["parent_id" => 0, "level" =>0];
        } else if (empty($data->subcategory_id)) {
            $category = $Category->find(["id_wappsi"=>$data->parent_id]);
            $categoryLevel = ["parent_id" => $category->id, "level" =>1];
        } else {
            $category = $Category->find(["id_wappsi"=>$data->subcategory_id]);
            $categoryLevel = ["parent_id" => $category->id, "level" =>2];
        }
        return (object) $categoryLevel;
    }

    public function addAttributeCategory($integration, $categoryId)
    {
        $this->load->integration_model('AttributeCategory');
        $this->load->integration_model('AttributeIntegration');

        $AttributeCategory = new AttributeCategory();
        $AttributeCategory->open($integration);

        $Attribute = new AttributeIntegration();
        $Attribute->open($integration);
        $attributes = $Attribute->getAll();
        if (!empty($attributes)) {
            foreach ($attributes as $attribute) {
                $AttributeCategory->create([
                    "category_id" => $categoryId,
                    "attribute_id" => $attribute->id,
                ]);
            }
        }
    }

    public function edit_category($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('code', sprintf(lang('category_code'), lang('category')), 'trim|required');
        $pr_details = $this->settings_model->getCategoryByID($id);
        if ($this->input->post('code') != $pr_details->code) {
            $this->form_validation->set_rules('code', sprintf(lang('category_code'), lang('category')), 'required|is_unique[categories.code]');
        }
        $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
        if ($this->input->post('slug') != $pr_details->slug) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash|is_unique[categories.slug]');
        }
        $this->form_validation->set_rules('name', lang("category_name"), 'required|min_length[3]');
        $this->form_validation->set_rules('userfile', lang("category_image"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'parent_id' => $this->input->post('parent'),
                'printer_id' => $this->input->post('printer'),
                'preparer_id' => $this->input->post('user_preparation'),
                'preparation_area_id' => $this->input->post('preparation_area'),
                'hide' => $this->input->post('hide_category_products') ? 1 : 0,
                'except_category_taxes' => $this->input->post('except_category_taxes') ? 1 : 0,
                'profitability_margin' => $this->input->post('profitability_margin'),
                'profitability_margin_price_base' => $this->input->post('profitability_margin_price_base'),
                'suggested_rate' => $this->input->post('tax_rate'),
                'subcategory_id' => $this->input->post('subcategory'),
                'hide_ecommerce' => $this->input->post('hide_ecommerce') ? 1 : 0,
                );
            $code_consecutive = $this->input->post('code_consecutive');
            if ($code_consecutive != NULL) {
                $data['code_consecutive'] = $code_consecutive;
            }

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('edit_category')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/categories");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateCategory($id, $data)) {
            $syncUpdatedCategory = $this->syncUpdatedCategory($id);

            if (!empty($syncUpdatedCategory)) {
                $this->session->set_flashdata('warning', lang("category_updated") . " $syncUpdatedCategory");
            } else {
                $this->session->set_flashdata('message', lang("category_updated"));
            }

            admin_redirect("system_settings/categories");
        } else {

            $printers = false;
            $users_preparations = false;
            $preparation_areas = false;

            if ($this->pos_settings->restobar_mode) {
                if ($this->pos_settings->order_print_mode == 1) {
                    $printers[] = lang('select');
                    $printers_d = $this->site->getAllPrinters();
                    foreach ($printers_d as $printer) {
                        $printers[$printer->id] = $printer->title;
                    }
                } else if ($this->pos_settings->order_print_mode == 0 || $this->pos_settings->order_print_mode == 3) {
                    $users_preparations[] = lang('select');
                    $group_n = $this->site->getUserGroupByName('preparation');
                    $preparers = $this->site->getUsersByGroupId($group_n->id);
                    if ($preparers) {
                        foreach ($preparers as $preparer) {
                            $users_preparations[$preparer->id] = $preparer->first_name." ".$preparer->last_name;
                        }
                    }
                } else if ($this->pos_settings->order_print_mode == 2 ) {
                    $preparation_areas[] = lang('select');
                    $preparation_areas_data = $this->site->get_preparation_areas();
                    foreach ($preparation_areas_data as $pa) {
                        $preparation_areas[$pa->id] = $pa->name;
                    }
                }
            }

            $this->data['printers'] = $printers;
            $this->data['users_preparations'] = $users_preparations;
            $this->data['preparation_areas'] = $preparation_areas;
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['category'] = $this->settings_model->getCategoryByID($id);
            $this->data['categories'] = $this->settings_model->getParentCategories();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['tax_rates'] = $this->settings_model->getAllTaxRates();
            $this->load_view($this->theme . 'settings/edit_category', $this->data);

        }
    }

    private function syncUpdatedCategory($id)
    {
        $_message = '';
        $_errorMessage = '';
        $response = [];
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            $category = $this->settings_model->getCategoryByID($id);

            if ($category->hide_ecommerce == YES) {
                return (object) $response;
            }

            if (!empty($integrations) && !empty($category)) {
                $this->load->integration_model('Category');
                foreach ($integrations as $integration) {
                    $Category = new Category();
                    $Category->open($integration);

                    $categoryExt = $Category->find(["id_wappsi"=>$category->id]);
                    if (!empty($categoryExt)) {
                        $categoryLevel = $this->getCategoryLevel($category, $Category);
                        $data = [
                            "parent_id" => $categoryLevel->parent_id,
                            "level" => $categoryLevel->level,
                            "name" => $category->name,
                            "slug" => $category->slug,
                            "updated_at" => date("Y-m-d H:i:s"),
                        ];
                        if (!$Category->update($data, $categoryExt->id)) {
                            $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                        }
                    } else {
                        $syncCreateCategory = $this->syncCreatedCategory($id);

                        if (isset($syncCreateCategory->warning) && !empty($syncCreateCategory->warning)) {
                            $_message .= $syncCreateCategory->warning;
                        }
                    }
                    $Category->close();
                }
                if (!empty($_message)) {
                    $_message = "No fue posible sincronizar en: $_message";
                }
            }
        }
        return trim($_message, ', ');
    }

    public function delete_category($id = NULL)
    {
        $this->sma->checkPermissions();
        $cat = $this->site->getCategoryByID($id);
        if ($cat->parent_id && !$cat->subcategory_id) {//subcategory
            if ($this->site->getSubCategoriesSecondLevel($id)) {
                $this->sma->send_json(array('error' => 1, 'msg' => lang("category_has_subsubcategory")));
            }
            if ($this->site->getProductsBySubCategory($id)) {
                $this->sma->send_json(array('error' => 1, 'msg' => sprintf(lang("subcategory_has_products"), lang('category'))));
            }
        } else if ($cat->subcategory_id) {//sub subcategory
            if ($this->site->getProductsBySubSubCategory($id)) {
                $this->sma->send_json(array('error' => 1, 'msg' => sprintf(lang("subsubcategory_has_products"), lang('category'))));
            }
        } else {
            if ($this->site->getSubCategories($id)) {
                $this->sma->send_json(array('error' => 1, 'msg' => lang("category_has_subcategory")));
            }
            if ($this->site->getProductsByCategory($id)) {
                $this->sma->send_json(array('error' => 1, 'msg' => sprintf(lang("category_has_products"), lang('category'))));
            }
        }
        if ($this->settings_model->deleteCategory($id)) {
            $syncDeletedCategory = $this->syncDeletedCategory($id);
            if (!empty($syncDeletedCategory)) {
                $this->session->set_flashdata('warning', lang("category_deleted") . " $syncDeletedCategory");
            } else {
                $this->session->set_flashdata('message', lang("category_deleted"));
            }
            $this->sma->send_json(array('error' => 0, 'msg' => lang("category_deleted")));
        }
    }

    public function category_actions()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');
        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $warning = false;
                    foreach ($_POST['val'] as $id) {
                        if ($this->site->getProductsByCategory($id)) {
                            $warning = true;
                            continue;
                        } else {
                            $this->settings_model->deleteCategory($id);
                            $this->syncDeletedCategory($id);
                        }
                    }
                    if ($warning) {
                        $this->session->set_flashdata('warning', lang("category_deleted_product_warning"));
                    } else {
                        $this->session->set_flashdata('message', lang("categories_deleted"));
                    }
                    admin_redirect('system_settings/categories');
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('categories'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('image'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('parent_actegory'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->settings_model->getCategoryByID($id);
                        $parent_actegory = '';
                        if ($sc->parent_id) {
                            $pc = $this->settings_model->getCategoryByID($sc->parent_id);
                            $parent_actegory = $pc->code;
                        }
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sc->image);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $parent_actegory);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'categories_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else if ($this->input->post('form_action') == 'fix_slug') {
                    $this->db->query("UPDATE {$this->db->dbprefix('categories')} C
                                        INNER JOIN (SELECT id, slug, LOWER(REPLACE(name, ' ', '-')) AS new_slug FROM {$this->db->dbprefix('categories')}
                                    WHERE slug IS NULL) tbl ON tbl.id = C.id
                                    SET C.slug = tbl.new_slug
                                    WHERE C.slug IS NULL");
                $this->session->set_flashdata('message', lang("slug_fixed"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function syncDeletedCategory($id)
    {
        $_message = '';
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Category');
                foreach ($integrations as $integration) {
                    $Category = new Category();
                    $Category->open($integration);

                    $categoryExt = $Category->find(["id_wappsi"=>$id]);
                    if (!empty($categoryExt)) {
                        if ($Category->delete($categoryExt->id)) {
                            $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                        }
                        $Category->close();
                    }
                }
                if (!empty($_message)) {
                    $_message = "No fue posible sincronizar en: $_message";
                }
            }
        }
        return trim($_message, ', ');
    }

    public function syncCategoriesStore()
    {
        $ids = $this->input->post('ids');
        $warningMessage = '';
        $response = [];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $category = $this->settings_model->getCategoryByID($id);

                if ($category->synchronized_store == 0) {
                    if (!empty((int) $category->parent_id)) {
                        $syncCreatedCategory = $this->syncCreatedCategory($category->parent_id);
                    }

                    if (!empty((int) $category->subcategory_id)) {
                        $syncCreatedCategory = $this->syncCreatedCategory($category->subcategory_id);
                    }

                    $syncCreatedCategory = $this->syncCreatedCategory($id);

                    if (isset($syncCreatedCategory->warning) && !empty($syncCreatedCategory->warning)) {
                        $warningMessage .= $syncCreatedCategory->warning;
                    }
                }
            }

            if (!empty($warningMessage)) {
                $response['warning'] = $warningMessage;
            }

            echo json_encode($response);
        }
    }

    public function tax_rates()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        // $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('tax_rates')));
        // $meta = array('page_title' => lang('tax_rates'), 'bc' => $bc);
        $this->page_construct('settings/tax_rates', ['page_title' => lang('tax_rates')], $this->data);
    }

    public function getTaxRates()
    {
        $this->load->library('datatables');

        $delete = "<a href='#' class='tip po' rel='popover'
            data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_tax_rate/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\">
                <i class=\"fa fa-trash-o\"></i> ". lang("delete") ."
        </a>";

        $actions = '<div class="btn-group text-left">
            <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                <i class="fas fa-ellipsis-v fa-lg"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>
                    <a href="'.admin_url('system_settings/edit_tax_rate/$1') . '" class="tip" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-edit"></i> ' . lang("edit") . '
                    </a>
                </li>
                <li>
                    '. $delete .'
                </li>
            </ul>
        </div>';

        $this->datatables
            ->select("id, name, code, rate, type, sync")
            ->from("tax_rates")
            ->add_column("Actions", $actions, "id");


        echo $this->datatables->generate();
    }

    public function add_tax_rate()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('code_fe', lang("code_fe"), "trim|required");
        $this->form_validation->set_rules('name', lang("name"), 'trim|is_unique[tax_rates.name]|required');
        $this->form_validation->set_rules('type', lang("type"), 'required');
        $this->form_validation->set_rules('rate', lang("tax_rate"), 'required|numeric');

        if ($this->form_validation->run() == TRUE) {
            $data = [
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'type' => $this->input->post('type'),
                'rate' => $this->input->post('rate'),
                'tax_indicator' => $this->input->post('tax_indicator'),
                "code_fe" => $this->input->post("code_fe"),
                'excluded' => ($this->input->post('excluded')) ? 1 : 0
            ];
        } elseif ($this->input->post('add_tax_rate')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/tax_rates");
        }

        if ($this->form_validation->run() == TRUE && $id = $this->settings_model->addTaxRate($data)) {
            $this->validateSync();
            
            $integrations = $this->settings_model->getAllIntegrations();
            foreach ($integrations as $integration) {
                $TaxRates_model = new TaxRates_model();
                $tax = $TaxRates_model->find($id);

                $this->syncTax($tax, $integration);
            }
            
            $this->session->set_flashdata('message', lang("tax_rate_added"));
            admin_redirect("system_settings/tax_rates");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['tax_codes_fe'] = $this->settings_model->get_tax_code_fe();

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/add_tax_rate', $this->data);
        }
    }

    public function edit_tax_rate($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('code_fe', lang("code_fe"), "trim|required");
        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        $tax_details = $this->settings_model->getTaxRateByID($id);
        if ($this->input->post('name') != $tax_details->name) {
            $this->form_validation->set_rules('name', lang("name"), 'required|is_unique[tax_rates.name]');
        }
        $this->form_validation->set_rules('type', lang("type"), 'required');
        $this->form_validation->set_rules('rate', lang("tax_rate"), 'required|numeric');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'type' => $this->input->post('type'),
                'rate' => $this->input->post('rate'),
                'tax_indicator' => $this->input->post('tax_indicator'),
                "code_fe" => $this->input->post("code_fe"),
                'excluded' => ($this->input->post('excluded')) ? 1 : 0
            );
        } elseif ($this->input->post('edit_tax_rate')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/tax_rates");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateTaxRate($id, $data)) { //check to see if we are updateing the customer
            $this->validateSync();
            
            $integrations = $this->settings_model->getAllIntegrations();
            foreach ($integrations as $integration) {
                $TaxRates_model = new TaxRates_model();
                $tax = $TaxRates_model->find($id);

                $this->syncTax($tax, $integration);
            }

            $this->session->set_flashdata('message', lang("tax_rate_updated"));
            admin_redirect("system_settings/tax_rates");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['id'] = $id;
            $this->data['tax_rate'] = $this->settings_model->getTaxRateByID($id);
            $this->data['tax_codes_fe'] = $this->settings_model->get_tax_code_fe();

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_tax_rate', $this->data);
        }
    }

    public function delete_tax_rate($id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->settings_model->deleteTaxRate($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("tax_rate_deleted")));
        } else {

            $this->sma->send_json(array('error' => 1, 'msg' => lang("tax_rate_not_deleted")));
        }
    }

    public function tax_actions()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->deleteTaxRate($id);
                    }
                    $this->session->set_flashdata('message', lang("tax_rates_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('tax_rates'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('tax_rate'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('type'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $tax = $this->settings_model->getTaxRateByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $tax->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $tax->code);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $tax->rate);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, ($tax->type == 1) ? lang('percentage') : lang('fixed'));
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'tax_rates_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function validateSync() 
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (empty($integrations)) {
                $this->session->set_flashdata('error', "No se encontraron datos de sincronización");
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', "La sincronización no está activada");
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function syncTaxes()
    {
        $this->validateSync();
        
        $integrations = $this->settings_model->getAllIntegrations();
        foreach ($integrations as $integration) {
            $TaxRates_model = new TaxRates_model();
            $taxes = $TaxRates_model->all();
            if (!empty($taxes)) {
                foreach ($taxes as $tax) {
                    $sync = $this->syncTax($tax, $integration);
                    if (!$sync) {
                        $errorMessage = "$tax->name,";
                    } else {
                        $TaxRates_model->update(["sync" => YES], $tax->id);
                    }
                }
            }
        }

        if (isset($errorMessage)) {
            $this->session->set_flashdata('error', "Los impuestos ". trim($errorMessage, ","). " No pudieron ser sincronizados");
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->session->set_flashdata('message', "Sincronización realizada con éxito");
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function syncTax($tax, $integration) 
    {
        $this->load->integration_model('Taxes');
        $Taxes = new Taxes();
        $Taxes->open($integration);

        $taxStore = $Taxes->find(["id_wappsi" => $tax->id]);
        $data = [
            "name" => ucwords(strtolower($tax->name)),
            "id_wappsi" => $tax->id
        ];
        if (empty($taxStore)) {
            $response = $Taxes->create($data);
        } else {
            $response = $Taxes->update($data, $taxStore->id);
        }

        $Taxes->close();

        return $response;
    }

    public function customer_groups()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('customer_groups')));
        $meta = array('page_title' => lang('customer_groups'), 'bc' => $bc);
        $this->page_construct('settings/customer_groups', $meta, $this->data);
    }

    public function getCustomerGroups()
    {
        $this->sma->checkPermissions();

        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, percent")
            ->from("customer_groups")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('system_settings/edit_customer_group/$1') . "' class='tip' title='" . lang("edit_customer_group") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_customer_group") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_customer_group/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }

    public function add_customer_group()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('name', lang("group_name"), 'trim|is_unique[customer_groups.name]|required');
        $this->form_validation->set_rules('percent', lang("group_percentage"), 'required|numeric');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'percent' => $this->input->post('percent'),
                'units' => json_encode($this->input->post('units')),
            );
        } elseif ($this->input->post('add_customer_group')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/customer_groups");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addCustomerGroup($data)) {
            $this->session->set_flashdata('message', lang("customer_group_added"));
            admin_redirect("system_settings/customer_groups");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['units'] = $this->site->get_all_units();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/add_customer_group', $this->data);
        }
    }

    public function edit_customer_group($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('name', lang("group_name"), 'trim|required');
        $pg_details = $this->settings_model->getCustomerGroupByID($id);
        if ($this->input->post('name') != $pg_details->name) {
            $this->form_validation->set_rules('name', lang("group_name"), 'required|is_unique[tax_rates.name]');
        }
        $this->form_validation->set_rules('percent', lang("group_percentage"), 'required|numeric');

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'percent' => $this->input->post('percent'),
                'units' => json_encode($this->input->post('units')),
            );
        } elseif ($this->input->post('edit_customer_group')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/customer_groups");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateCustomerGroup($id, $data)) {
            $this->session->set_flashdata('message', lang("customer_group_updated"));
            admin_redirect("system_settings/customer_groups");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['units'] = $this->site->get_all_units();
            $this->data['customer_group'] = $this->settings_model->getCustomerGroupByID($id);

            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_customer_group', $this->data);
        }
    }

    public function delete_customer_group($id = NULL)
    {
        $this->sma->checkPermissions();
        //Validación de clientes existentes en grupos a eliminar.

        if ($this->settings_model->getCustomersByGroups($id) !== FALSE) { //Si hay clientes en el grupo a borrar, se indica el error.
            $this->sma->send_json(array('error' => 1, 'msg' => 'No se puede eliminar el grupo por que hay clientes relacionados a éste.'));
        } else {

            if ($this->Settings->customer_group == $id) {
                $this->sma->send_json(array('error' => 1, 'msg' => 'No se puede eliminar el grupo por que es el asignado por defecto.'));
            } else {
                if ($this->settings_model->deleteCustomerGroup($id)) { //Si NO hay clientes, se borra el grupo.
                    $this->sma->send_json(array('error' => 0, 'msg' => lang("customer_group_deleted")));
                }
            }

        }
    }

    public function customer_group_actions()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->deleteCustomerGroup($id);
                    }
                    $this->session->set_flashdata('message', lang("customer_groups_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('tax_rates'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('group_name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('group_percentage'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $pg = $this->settings_model->getCustomerGroupByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $pg->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $pg->percent);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'customer_groups_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_customer_group_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function warehouses()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('warehouses')));
        $meta = array('page_title' => lang('warehouses'), 'bc' => $bc);
        $this->page_construct('settings/warehouses', $meta, $this->data);
    }

    public function getWarehouses()
    {
        $this->sma->checkPermissions();

        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('warehouses')}.id as id,
                        map,
                        code,
                        {$this->db->dbprefix('warehouses')}.name as name,
                        {$this->db->dbprefix('warehouses')}.type as type,
                        {$this->db->dbprefix('price_groups')}.name as price_group,
                        phone,
                        email,
                        address,
                        status,
                if(status = 1, 'fas fa-user-check', 'fas fa-user-slash') as faicon,
                if(status = 1, '".lang('deactivate_warehouse')."', '".lang('activate_warehouse')."') as detailaction,
                if(status = 1, '" . admin_url('system_settings/deactivate_warehouse/') . "', '" . admin_url('system_settings/activate_warehouse/') . "') as linkaction
                    ")
            ->from("warehouses")
            ->join('price_groups', 'price_groups.id=warehouses.price_group_id', 'left')
            ->add_column("Actions", "<div class=\"text-center\">
                                        <div class=\"btn-group text-left\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                                                Acciones
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li>
                                                    <a href='" . admin_url('system_settings/edit_warehouse/$1') . "' class='tip' title='' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-edit\"></i>
                                                        ".lang("edit_warehouse")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='#' class='po' title='<b></b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='$4$1'>" . lang('i_m_sure') . "</a><button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"$2\"></i>$3</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                        ", "id, faicon, detailaction, linkaction");
        $this->datatables->unset_column('faicon');
        $this->datatables->unset_column('detailaction');
        $this->datatables->unset_column('linkaction');
        echo $this->datatables->generate();
    }

    public function add_warehouse()
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('code', lang("code"), 'trim|is_unique[warehouses.code]|required');
        $this->form_validation->set_rules('name', lang("name"), 'required');
        $this->form_validation->set_rules('address', lang("address"), 'required');
        $this->form_validation->set_rules('userfile', lang("map_image"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = 'assets/uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = '2000';
                $config['max_height'] = '2000';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $error);
                    admin_redirect("system_settings/warehouses");
                }

                $map = $this->upload->file_name;

                $this->load->helper('file');
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/' . $map;
                $config['new_image'] = 'assets/uploads/thumbs/' . $map;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 76;
                $config['height'] = 76;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
            } else {
                $map = NULL;
            }
            $data = array('code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'price_group_id' => $this->input->post('price_group'),
                'type' => $this->input->post('type'),
                'map' => $map,
                'preffix' => $this->input->post('preffix'),
                'area' => $this->input->post('area'),
                'street' => $this->input->post('street'),
                'module' => $this->input->post('module'),
                'level' => $this->input->post('level'),
                'suffix' => $this->input->post('suffix'),
            );
        } elseif ($this->input->post('add_warehouse')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/warehouses");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addWarehouse($data)) {
            $this->session->set_flashdata('message', lang("warehouse_added"));
            admin_redirect("system_settings/warehouses");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['price_groups'] = $this->settings_model->getAllPriceGroups();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/add_warehouse', $this->data);
        }
    }

    public function edit_warehouse($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('code', lang("code"), 'trim|required');
        $wh_details = $this->settings_model->getWarehouseByID($id);
        if ($this->input->post('code') != $wh_details->code) {
            $this->form_validation->set_rules('code', lang("code"), 'required|is_unique[warehouses.code]');
        }
        $this->form_validation->set_rules('address', lang("address"), 'required');
        $this->form_validation->set_rules('map', lang("map_image"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $data = array('code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'price_group_id' => $this->input->post('price_group'),
                'status' => $this->input->post('status'),
                'type' => $this->input->post('type'),
                'preffix' => $this->input->post('preffix'),
                'area' => $this->input->post('area'),
                'street' => $this->input->post('street'),
                'module' => $this->input->post('module'),
                'level' => $this->input->post('level'),
                'suffix' => $this->input->post('suffix'),
            );

            if ($_FILES['userfile']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = '2000';
                $config['max_height'] = '2000';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $error);
                    admin_redirect("system_settings/warehouses");
                }

                $data['map'] = $this->upload->file_name;

                $this->load->helper('file');
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/' . $data['map'];
                $config['new_image'] = 'assets/uploads/thumbs/' . $data['map'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 76;
                $config['height'] = 76;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
            }
        } elseif ($this->input->post('edit_warehouse')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/warehouses");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateWarehouse($id, $data)) { //check to see if we are updateing the customer
            $this->session->set_flashdata('message', lang("warehouse_updated"));
            admin_redirect("system_settings/warehouses");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['warehouse'] = $this->settings_model->getWarehouseByID($id);
            $this->data['price_groups'] = $this->settings_model->getAllPriceGroups();
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_warehouse', $this->data);
        }
    }

    public function deactivate_warehouse($id = NULL)
    {
        $this->sma->checkPermissions();
        if ($this->settings_model->deleteWarehouse($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("warehouse_deleted")));
        }
    }

    public function activate_warehouse($id = NULL)
    {
        $this->sma->checkPermissions();
        if ($this->settings_model->activeWarehouse($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("warehouse_activated")));
        }
    }

    public function warehouse_actions()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');
        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->deleteWarehouse($id);
                    }
                    $this->session->set_flashdata('message', lang("warehouses_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                if ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('warehouses'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('city'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $wh = $this->settings_model->getWarehouseByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $wh->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $wh->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $wh->address);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $wh->city);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'warehouses_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_warehouse_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function variants()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => $this->Settings->product_variants_language));
        $meta = array('page_title' => $this->Settings->product_variants_language, 'bc' => $bc);
        $this->page_construct('settings/variants', $meta, $this->data);
    }

    public function getVariants()
    {

        $this->load->library('datatables');
        $this->datatables->select("id");
        $this->datatables->select("name");
        if ($this->Settings->management_weight_in_variants == '1') {
            $this->datatables->select('weight');
        }
        $this->datatables->select("synchronized_store");

        $this->datatables
            ->from("variants")
            ->add_column("Actions", "<div class='btn-group text-left'>
                <button type='button' class='btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle' data-toggle='dropdown' data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                    <i class='fas fa-ellipsis-v fa-lg'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li>
                        <a href='" . admin_url('system_settings/edit_variant/$1') . "' class='tip' data-toggle='modal' data-target='#myModal'>
                            <i class=\"fa fa-edit\"></i> " . lang("edit")." ".$this->Settings->product_variant_language . "
                        </a>
                    </li>
                    <li>
                        <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_variant/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>
                            <i class=\"fa fa-trash-o\"></i> " .lang('delete')." ". $this->Settings->product_variant_language . "
                        </a>
                    </li>
                </ul>
            </div>", "id");

        echo $this->datatables->generate();
    }

    public function add_variant()
    {
        $this->sma->checkPermissions('add_variant');

        $this->form_validation->set_rules('name', lang("name"), 'trim|is_unique[variants.name]|required');

        if ($this->form_validation->run() == true) {
            $data['name'] = $this->input->post('name');
            $data['weight'] = $this->input->post('weight');
        } elseif ($this->input->post('add_variant')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/variants");
        }

        if ($this->form_validation->run() == true && $variantId = $this->settings_model->addVariant($data)) {
            $syncCreatedVariants = $this->syncCreatedVariants($variantId);

            if (isset($syncCreatedVariants->warning) && !empty($syncCreatedVariants->warning)) {
                $this->session->set_flashdata('warning', lang("variant_added") .  $syncCreatedVariants->warning);
            }
            if (isset($syncCreatedVariants->message) && !empty($syncCreatedVariants->message)) {
                $this->session->set_flashdata('message', lang("variant_added") .  $syncCreatedVariants->message);
            }

            admin_redirect("system_settings/variants");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/add_variant', $this->data);
        }
    }

    private function syncCreatedVariants($id)
    {
        $_message = '';
        $_errorMessage = '';
        $response = [];
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            $variant = $this->settings_model->getVariantByID($id);
            if (!empty($integrations) && !empty($variant)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $data = [
                        "attribute_id" => 1,
                        "name" => $variant->name,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s"),
                        "id_wappsi" => $variant->id,
                    ];
                    if ($AttributeValue->create($data)) {
                        $this->settings_model->updateVariant($id, ["synchronized_store" => YES]);
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }else {
                        $_errorMessage .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $AttributeValue->close();
                }
                if (!empty($_message)) {
                    $response["message"] = "Sincronizado en: ". rtrim($_message, ", ");
                }
                if (!empty($_errorMessage)) {
                    $response["warning"] = " No fue posible sincronizar en: ". rtrim($_errorMessage, ", ");
                }
            }
        }
        return (object) $response;
    }

    public function edit_variant($id = NULL)
    {
        $this->sma->checkPermissions('edit_variant');

        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        $tax_details = $this->settings_model->getVariantByID($id);
        if (mb_strtolower($this->input->post('name')) != mb_strtolower($tax_details->name)) {
            $this->form_validation->set_rules('name', lang("name"), 'required|is_unique[variants.name]');
        }

        if ($this->form_validation->run() == true) {
            $data['name'] = $this->input->post('name');
            $data['weight'] = $this->input->post('weight');

            if ($_FILES['variant_image']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('variant_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/edit/" . $id);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $config = NULL;
            }
        } elseif ($this->input->post('edit_variant')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/variants");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateVariant($id, $data)) {
            $syncUpdatedVariant = $this->syncUpdatedVariant($id);
            if (!empty($syncUpdatedVariant)) {
                $this->session->set_flashdata('warning', lang("variant_updated") . " $syncUpdatedVariant");
            } else {
                $this->session->set_flashdata('message', lang("variant_updated"));
            }
            admin_redirect("system_settings/variants");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['variant'] = $tax_details;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_variant', $this->data);
        }
    }

    private function syncUpdatedVariant($id)
    {
        $_message = '';
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            $variant = $this->settings_model->getVariantByID($id);
            if (!empty($integrations) && !empty($variant)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $variantExt = $AttributeValue->find(["id_wappsi"=>$variant->id]);
                    $data = [
                        "name" => $variant->name,
                        "updated_at" => date("Y-m-d H:i:s"),
                    ];
                    if (!$AttributeValue->update($data, $variantExt->id)) {
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $AttributeValue->close();
                }
                if (!empty($_message)) {
                    $_message = "No fue posible sincronizar en: $_message";
                }
            }
        }
        return trim($_message, ', ');
    }

    public function delete_variant($id = NULL)
    {
        $this->sma->checkPermissions();
        if ($this->site->variantHasProducts($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("variant_has_products")));
        }
        if ($this->settings_model->deleteVariant($id)) {
            $syncDeletedVariant = $this->syncDeletedVariant($id);
            $this->sma->send_json(array('error' => 0, 'msg' => lang("variant_deleted") . " $syncDeletedVariant"));
            $this->sma->send_json(array('error' => 0, 'msg' => lang("variant_deleted")));
        }
    }

    private function syncDeletedVariant($id)
    {
        $_message = '';
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);

                    $variantExt = $AttributeValue->find(["id_wappsi"=>$id]);
                    if ($AttributeValue->delete($variantExt->id)) {
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $AttributeValue->close();
                }
                if (!empty($_message)) {
                    $_message = "No fue posible sincronizar en: $_message";
                }
            }
        }
        return trim($_message, ', ');
    }

    public function syncVariantsStore()
    {
        $ids = $this->input->post('ids');
        $warningMessage = '';
        $response = [];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $variant = $this->settings_model->getVariantByID($id);
                if ($variant->synchronized_store == NOT) {
                    $syncCreatedVariants = $this->syncCreatedVariants($id);

                    if (isset($syncCreatedVariants->warning) && !empty($syncCreatedVariants->warning)) {
                        $warningMessage .= $syncCreatedVariants->warning;
                    }
                }
            }

            if (!empty($warningMessage)) {
                $response['warning'] = $warningMessage;
            }

            echo json_encode($response);
        }
    }

    public function colors()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('settings/colors', ['page_title' => $this->lang->line("product_colors")], $this->data);
    }

    public function getColors($xls=null)
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, status, synchronized_store")
            ->from("colors")
            ->add_column("Actions", "<div class='btn-group text-left'>
                <button type='button' class='btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle' data-toggle='dropdown' data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                    <i class='fas fa-ellipsis-v fa-lg'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li>".
                        ($this->Admin || $this->Owner || $this->GP['system_settings-editColor']  ? "<a href='" . admin_url('system_settings/editColor/$1') . "' class='tip' data-toggle='modal' data-target='#myModal'>
                                                    <i class=\"fa fa-edit\"></i> " . lang("edit") ."
                                                </a>" : "").
                        ($this->Admin || $this->Owner || $this->GP['system_settings-deleteColor']  ? "<a href='" . admin_url('system_settings/deleteColor/$1') . "' class='tip' data-toggle='modal' data-target='#myModal'>
                                                    <i class=\"fa fa-trash\"></i> " . lang("delete") ."
                                                </a>" : "").
                    "</li>
                </ul>
            </div>", "id");
        if ($xls) {
            $data = $this->datatables->get_display_result_2();
            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('colors');
            $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
            $this->excel->getActiveSheet()->SetCellValue('B1', lang('status'));
            $this->excel->getActiveSheet()->SetCellValue('C1', lang('synchronized_store'));
            $row = 2;
            foreach ($data as $data_row) {
                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->name);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, ($data_row->status == 0) ? lang('inactive') : lang('active') );
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, ($data_row->synchronized_store == 0) ? lang('no') : lang('yes'));
                $row++;
            }
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $filename = 'Colors report';
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
        }else{
            echo $this->datatables->generate();
        }
    }

    public function addColor()
    {
        $this->sma->checkPermissions('addColor');

        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'settings/add_color', $this->data);
    }

    public function createColor()
    {
        $settings = new Settings_model();

        $this->form_validation->set_rules('name', lang("name"), 'trim|is_unique[colors.name]|required');

        if ($this->form_validation->run() == true) {
            $data = array('name' => ($this->input->post('name')));

            if ($colorId = $settings->createColor($data)) {
                $syncCreatedColors = $this->syncCreatedColors($colorId);
                if (!empty($syncCreatedColors)) {
                    if (isset($syncCreatedColors->warning) && !empty($syncCreatedColors->warning)) {
                        $this->session->set_flashdata('warning', lang("saved_color") .  $syncCreatedColors->warning);
                    }
                    if (isset($syncCreatedColors->message) && !empty($syncCreatedColors->message)) {
                        $this->session->set_flashdata('message', lang("saved_color") .  $syncCreatedColors->message);
                    }
                } else {
                    $this->session->set_flashdata('message', lang("saved_color"));
                }
            } else {
                $this->session->set_flashdata('error', lang("unsaved_color"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("system_settings/colors");
    }

    private function syncCreatedColors($id)
    {
        $_message = '';
        $_errorMessage = '';
        $response = [];
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $settings = new Settings_model();
            $integrations = $settings->getAllIntegrations();
            $color = $settings->findColor(['id'=>$id]);
            if (!empty($integrations) && !empty($color)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $data = [
                        "attribute_id" => 2,
                        "name" => $color->name,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s"),
                        "id_wappsi" => $id,
                    ];
                    if ($AttributeValue->create($data)) {
                        $settings->updateColor(["synchronized_store" => YES], $id);
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    } else {
                        $_errorMessage .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $AttributeValue->close();
                }
                if (!empty($_message)) {
                    $response["message"] = " Sincronizado en: ". rtrim($_message, ", ");
                }
                if (!empty($_errorMessage)) {
                    $response["warning"] = " No fue posible sincronizar en: ". rtrim($_errorMessage, ", ");
                }
            }
        }
        return (object) $response;
    }

    public function editColor($id)
    {
        $this->sma->checkPermissions('editColor');

        $settings = new Settings_model();

        $this->data['color'] = $settings->findColor(['id'=>$id]);
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'settings/edit_color', $this->data);
    }

    public function updateColor($id)
    {
        $settings = new Settings_model();

        $this->form_validation->set_rules('name', lang("name"), 'trim|required');

        if ($this->form_validation->run() == true) {
            $color = $settings->findColor(['name'=>mb_strtoupper($this->input->post('name'))], $id);
            if (!empty($color)) {
                $this->session->set_flashdata('error', "El nombre debe ser un valor único");
                admin_redirect("system_settings/colors");
            }

            $data = ['name' => mb_strtoupper($this->input->post('name')), "status"=>($this->input->post("status") ? 1 : 0)];

            if ($settings->updateColor($data, $id)) {
                $syncUpdatedColor = $this->syncUpdatedColor($id);
                if (!empty($syncUpdatedColor)) {
                    $this->session->set_flashdata('warning', lang("updated_color") . " $syncUpdatedColor");
                } else {
                    $this->session->set_flashdata('message', lang("updated_color"));
                }
            } else {
                $this->session->set_flashdata('error', lang("color_not_updated"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("system_settings/colors");
    }

    private function syncUpdatedColor($id)
    {
        $_message = '';
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $settings = new Settings_model();

            $integrations = $settings->getAllIntegrations();
            $color = $settings->findColor(['id'=>$id]);
            if (!empty($integrations) && !empty($color)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $colorExt = $AttributeValue->find(["id_wappsi"=>$color->id]);
                    $data = [
                        "name" => $color->name,
                        "updated_at" => date("Y-m-d H:i:s"),
                    ];
                    if (!$AttributeValue->update($data, $colorExt->id)) {
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $AttributeValue->close();
                }
                if (!empty($_message)) {
                    $_message = "No fue posible sincronizar en: $_message";
                }
            }
        }
        return trim($_message, ', ');
    }

    public function syncColorsStore()
    {
        $ids = $this->input->post('ids');
        $warningMessage = '';
        $response = [];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $color = $this->settings_model->findColor(['id'=>$id]);
                if ($color->synchronized_store == NOT) {
                    $syncCreatedColors = $this->syncCreatedColors($id);

                    if (isset($syncCreatedColors->warning) && !empty($syncCreatedColors->warning)) {
                        $warningMessage .= $syncCreatedColors->warning;
                    }
                }
            }

            if (!empty($warningMessage)) {
                $response['warning'] = $warningMessage;
            }

            echo json_encode($response);
        }
    }

    public function materials()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('settings/materials', ['page_title' => $this->lang->line("product_materials")], $this->data);
    }

    public function getMaterials($xls=NULL)
    {

        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, status, synchronized_store")
            ->from("materials")
            ->add_column("Actions", "<div class='btn-group text-left'>
                <button type='button' class='btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle' data-toggle='dropdown' data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                    <i class='fas fa-ellipsis-v fa-lg'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li>
                        ".
                        ($this->Admin || $this->Owner || $this->GP['system_settings-editMaterial'] ? "<a href='" . admin_url('system_settings/editMaterial/$1') . "' class='tip' data-toggle='modal' data-target='#myModal'>
                                                    <i class=\"fa fa-edit\"></i> " . lang("edit") ."
                                                </a>" : "").
                    "</li>
                </ul>
            </div>", "id");

        if ($xls) {
            $data = $this->datatables->get_display_result_2();
            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('colors');
            $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
            $this->excel->getActiveSheet()->SetCellValue('B1', lang('status'));
            $this->excel->getActiveSheet()->SetCellValue('C1', lang('synchronized_store'));
            $row = 2;
            foreach ($data as $data_row) {
                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->name);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, ($data_row->status == 0) ? lang('inactive') : lang('active') );
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, ($data_row->synchronized_store == 0) ? lang('no') : lang('yes'));
                $row++;
            }
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $filename = 'Materils report';
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
        }else{
            echo $this->datatables->generate();
        }
    }

    public function addMaterial()
    {
        $this->sma->checkPermissions('addMaterial');

        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'settings/add_material', $this->data);
    }

    public function createMaterial()
    {
        $settings = new Settings_model();

        $this->form_validation->set_rules('name', lang("name"), 'trim|is_unique[materials.name]|required');

        if ($this->form_validation->run() == true) {
            $data = array('name' => mb_strtoupper($this->input->post('name')));

            if ($materialId = $settings->createMaterial($data)) {
                $syncCreatedMaterial = $this->syncCreatedMaterial($materialId);
                if (isset($syncCreatedMaterial->warning) && !empty($syncCreatedMaterial->warning)) {
                    $this->session->set_flashdata('warning', lang("saved_material") .  $syncCreatedMaterial->warning);
                }
                if (isset($syncCreatedMaterial->message) && !empty($syncCreatedMaterial->message)) {
                    $this->session->set_flashdata('message', lang("saved_material") .  $syncCreatedMaterial->message);
                }
            } else {
                $this->session->set_flashdata('error', lang("unsaved_material"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("system_settings/materials");
    }

    private function syncCreatedMaterial($id)
    {
        $_message = '';
        $_errorMessage = '';
        $response = [];
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $settings = new Settings_model();
            $integrations = $settings->getAllIntegrations();
            $material = $settings->findMaterial(['id'=>$id]);
            if (!empty($integrations) && !empty($material)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $data = [
                        "attribute_id" => 3,
                        "name" => $material->name,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s"),
                        "id_wappsi" => $id,
                    ];
                    if ($AttributeValue->create($data)) {
                        $this->settings_model->updateMaterial(["synchronized_store" => YES], $id);
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    } else {
                        $_errorMessage .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $AttributeValue->close();
                }
                if (!empty($_message)) {
                    $response["message"] = " Sincronizado en: ". rtrim($_message, ", ");
                }
                if (!empty($_errorMessage)) {
                    $response["warning"] = " No fue posible sincronizar en: ". rtrim($_errorMessage, ", ");
                }
            }
        }
        return (object) $response;
    }

    public function editMaterial($id)
    {
        $this->sma->checkPermissions('editMaterial');

        $settings = new Settings_model();

        $this->data['material'] = $settings->findMaterial(['id'=>$id]);
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'settings/edit_material', $this->data);
    }

    public function updateMAterial($id)
    {
        $settings = new Settings_model();

        $this->form_validation->set_rules('name', lang("name"), 'trim|required');

        if ($this->form_validation->run() == true) {
            $material = $settings->findMaterial(['name'=>mb_strtoupper($this->input->post('name'))], $id);
            if (!empty($material)) {
                $this->session->set_flashdata('error', "El nombre debe ser un valor único");
                admin_redirect("system_settings/materials");
            }

            $data = ['name' => mb_strtoupper($this->input->post('name')), "status"=>($this->input->post("status") ? 1 : 0)];

            if ($settings->updateMaterial($data, $id)) {
                $syncUpdatedMaterial = $this->syncUpdatedMaterial($id);
                if (!empty($syncUpdatedMaterial)) {
                    $this->session->set_flashdata('warning', lang("updated_material") . " $syncUpdatedMaterial");
                } else {
                    $this->session->set_flashdata('message', lang("updated_material"));
                }
            } else {
                $this->session->set_flashdata('error', lang("material_not_updated"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("system_settings/materials");
    }

    private function syncUpdatedMaterial($id)
    {
        $_message = '';
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $settings = new Settings_model();

            $integrations = $settings->getAllIntegrations();
            $material = $settings->findMaterial(['id'=>$id]);
            if (!empty($integrations) && !empty($material)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $materialExt = $AttributeValue->find(["id_wappsi"=>$material->id]);
                    $data = [
                        "name" => $material->name,
                        "updated_at" => date("Y-m-d H:i:s"),
                    ];
                    if (!$AttributeValue->update($data, $materialExt->id)) {
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $AttributeValue->close();
                }
                if (!empty($_message)) {
                    $_message = "No fue posible sincronizar en: $_message";
                }
            }
        }
        return trim($_message, ', ');
    }

    public function syncMaterialsStore()
    {
        $ids = $this->input->post('ids');
        $warningMessage = '';
        $response = [];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $material = $this->settings_model->findMaterial(['id'=>$id]);
                if ($material->synchronized_store == NOT) {
                    $syncCreatedMaterial = $this->syncCreatedMaterial($id);

                    if (isset($syncCreatedMaterial->warning) && !empty($syncCreatedMaterial->warning)) {
                        $warningMessage .= $syncCreatedMaterial->warning;
                    }
                }
            }

            if (!empty($warningMessage)) {
                $response['warning'] = $warningMessage;
            }

            echo json_encode($response);
        }
    }

    public function expense_categories()
    {
        $this->sma->checkPermissions('index', null, 'expense_categories');

        // $this->sma->print_arrays($this->GP);

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('expense_categories')));
        $meta = array('page_title' => lang('categories'), 'bc' => $bc);
        $this->page_construct('settings/expense_categories', $meta, $this->data);
    }

    public function getExpenseCategories()
    {
        $this->sma->checkPermissions('index', null, 'expense_categories');

        $this->load->library('datatables');
        $this->datatables->select("id, code, name");
        $this->datatables->from("expense_categories");
        if ($this->Owner || $this->Admin) {
            $this->datatables->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('system_settings/edit_expense_category/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_expense_category") . "'><i class=\"fa fa-edit\"></i></a> </div>", "id");
        } else {
            if ($this->GP['expense_categories-edit']) {
                $this->datatables->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('system_settings/edit_expense_category/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_expense_category") . "'><i class=\"fa fa-edit\"></i></a> </div>", "id");
            }
        }

        echo $this->datatables->generate();
    }

    public function add_expense_category()
    {
        $this->sma->checkPermissions('add', null, 'expense_categories');

        $this->form_validation->set_rules('code', sprintf(lang('category_code'), lang('category')), 'trim|is_unique[categories.code]|required');
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        // $this->form_validation->set_rules('tax_rate_id', lang("tax_1"), 'required');
        // $this->form_validation->set_rules('tax_rate_2_id', lang("tax_2"), 'required');

        if ($this->Settings->modulary) {
            $this->form_validation->set_rules('ledger_id', lang("ledger"), 'required');

            if ($tax1 = $this->site->getTaxRateByID($this->input->post('tax_rate_id'))) {
                if ($tax1->rate > 0 && $tax1->type == 1 || $tax1->type == 2) {
                    $this->form_validation->set_rules('tax_ledger_id', lang("tax_1_ledger"), 'required');
                }
            }

            if ($tax2 = $this->site->getTaxRateByID($this->input->post('tax_rate_2_id'))) {
                if ($tax2->rate > 0 && $tax2->type == 1 || $tax2->type == 2) {
                    $this->form_validation->set_rules('tax_2_ledger_id', lang("tax_2_ledger"), 'required');
                }
            }
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'tax_rate_id' => $this->input->post('tax_rate_id'),
                'tax_rate_2_id' => $this->input->post('tax_rate_2_id'),
                'expense_import' => $this->input->post('expense_import') ? 1 : 0,
            );

            if ($ledger_id = $this->input->post('ledger_id')) {
                $data['ledger_id'] = $ledger_id;
            }
            if ($creditor_ledger_id = $this->input->post('creditor_ledger_id')) {
                $data['creditor_ledger_id'] = $creditor_ledger_id;
            }
            if ($tax_ledger_id = $this->input->post('tax_ledger_id')) {
                $data['tax_ledger_id'] = $tax_ledger_id;
            }
            if ($tax_2_ledger_id = $this->input->post('tax_2_ledger_id')) {
                $data['tax_2_ledger_id'] = $tax_2_ledger_id;
            }

        } elseif ($this->input->post('add_expense_category')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/expense_categories");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addExpenseCategory($data)) {
            $this->session->set_flashdata('message', lang("expense_category_added"));
            admin_redirect("system_settings/expense_categories");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            if ($this->Settings->modulary) {
                $this->data['ledgers'] = $this->site->getAllLedgers();
            }
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->load_view($this->theme . 'settings/add_expense_category', $this->data);
        }
    }

    public function edit_expense_category($id = NULL)
    {
        $this->sma->checkPermissions('edit', null, 'expense_categories');

        $this->form_validation->set_rules('code', sprintf(lang('category_code'), lang('category')), 'trim|required');
        $category = $this->settings_model->getExpenseCategoryByID($id);
        if ($this->input->post('code') != $category->code) {
            $this->form_validation->set_rules('code', sprintf(lang('category_code'), lang('category')), 'required|is_unique[expense_categories.code]');
        }
        $this->form_validation->set_rules('name', lang("category_name"), 'required|min_length[3]');

        if ($this->Settings->modulary) {
            $this->form_validation->set_rules('ledger_id', lang("ledger"), 'required');
        }

        if ($this->Settings->modulary) {
            $this->form_validation->set_rules('ledger_id', lang("ledger"), 'required');

            if ($tax1 = $this->site->getTaxRateByID($this->input->post('tax_rate_id'))) {
                if ($tax1->rate > 0 && $tax1->type == 1 || $tax1->type == 2) {
                    $this->form_validation->set_rules('tax_ledger_id', lang("tax_1_ledger"), 'required');
                }
            }

            if ($tax2 = $this->site->getTaxRateByID($this->input->post('tax_rate_2_id'))) {
                if ($tax2->rate > 0 && $tax2->type == 1 || $tax2->type == 2) {
                    $this->form_validation->set_rules('tax_2_ledger_id', lang("tax_2_ledger"), 'required');
                }
            }

        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'tax_rate_id' => $this->input->post('tax_rate_id'),
                'tax_rate_2_id' => $this->input->post('tax_rate_2_id'),
                'tax_ledger_id' => $this->input->post('tax_ledger_id'),
                'tax_2_ledger_id' => $this->input->post('tax_2_ledger_id'),
                'expense_import' => $this->input->post('expense_import') ? 1 : 0,
            );

            if ($ledger_id = $this->input->post('ledger_id')) {
                $data['ledger_id'] = $ledger_id;
            }
            if ($creditor_ledger_id = $this->input->post('creditor_ledger_id')) {
                $data['creditor_ledger_id'] = $creditor_ledger_id;
            }

        } elseif ($this->input->post('edit_expense_category')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/expense_categories");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateExpenseCategory($id, $data)) {
            $this->session->set_flashdata('message', lang("expense_category_updated"));
            admin_redirect("system_settings/expense_categories");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['category'] = $category;
            $this->data['modal_js'] = $this->site->modal_js();
            if ($this->Settings->modulary) {
                $this->data['ledgers'] = $this->site->getAllLedgers();
            }
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->load_view($this->theme . 'settings/edit_expense_category', $this->data);
        }
    }

    public function delete_expense_category($id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->settings_model->hasExpenseCategoryRecord($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("category_has_expenses")));
        }

        if ($this->settings_model->deleteExpenseCategory($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("expense_category_deleted")));
        }
    }

    public function expense_category_actions()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->deleteExpenseCategory($id);
                    }
                    $this->session->set_flashdata('message', lang("categories_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('categories'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->settings_model->getCategoryByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->name);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'expense_categories_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function import_categories()
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/categories");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('code', 'name', 'image', 'pcode');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $value = $this->site->cleanRowCsvImport($value);
                    if(!$final[] = array_combine($keys, $value)){//VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key+1)));
                        admin_redirect("system_settings/brands");
                        break;
                    }
                }

                foreach ($final as $csv_ct) {
                    if (!$this->settings_model->getCategoryByCode(trim($csv_ct['code']))) {
                        $pcode = trim($csv_ct['pcode']);
                        if (!empty($pcode)) {
                            if ($pcategory = $this->settings_model->getCategoryByCode($pcode)) {
                                $data[] = array(
                                    'code' => trim($csv_ct['code']),
                                    'name' => trim($csv_ct['name']),
                                    'image' => trim($csv_ct['image']),
                                    'parent_id' => $pcategory->id,
                                    );
                            }
                        } else {
                            $data[] = array(
                                'code' => trim($csv_ct['code']),
                                'name' => trim($csv_ct['name']),
                                'image' => trim($csv_ct['image']),
                                );
                        }
                    }
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && $this->settings_model->addCategories($data)) {
            $this->session->set_flashdata('message', lang("categories_added"));
            admin_redirect('system_settings/categories');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'settings/import_categories', $this->data);

        }
    }

    public function import_subcategories()
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/categories");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('code', 'name', 'category_code', 'image');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }

                $rw = 2;
                foreach ($final as $csv_ct) {
                    if ( ! $this->settings_model->getSubcategoryByCode(trim($csv_ct['code']))) {
                        if ($parent_actegory = $this->settings_model->getCategoryByCode(trim($csv_ct['category_code']))) {
                            $data[] = array(
                                'code' => trim($csv_ct['code']),
                                'name' => trim($csv_ct['name']),
                                'image' => trim($csv_ct['image']),
                                'category_id' => $parent_actegory->id,
                                );
                        } else {
                            $this->session->set_flashdata('error', lang("check_category_code") . " (" . $csv_ct['category_code'] . "). " . lang("category_code_x_exist") . " " . lang("line_no") . " " . $rw);
                            admin_redirect("system_settings/categories");
                        }
                    }
                    $rw++;
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && $this->settings_model->addSubCategories($data)) {
            $this->session->set_flashdata('message', lang("subcategories_added"));
            admin_redirect('system_settings/categories');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'settings/import_subcategories', $this->data);

        }
    }

    public function import_expense_categories()
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/expense_categories");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('code', 'name');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }

                foreach ($final as $csv_ct) {
                    if ( ! $this->settings_model->getExpenseCategoryByCode(trim($csv_ct['code']))) {
                        $data[] = array(
                            'code' => trim($csv_ct['code']),
                            'name' => trim($csv_ct['name']),
                            );
                    }
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && $this->settings_model->addExpenseCategories($data)) {
            $this->session->set_flashdata('message', lang("categories_added"));
            admin_redirect('system_settings/expense_categories');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'settings/import_expense_categories', $this->data);

        }
    }

    public function units()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('units')));
        $meta = array('page_title' => lang('units'), 'bc' => $bc);
        $this->page_construct('settings/units', $meta, $this->data);
    }

    public function getUnits()
    {
        // $this->sma->checkPermissions();

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('units')}.id as id, {$this->db->dbprefix('units')}.code, {$this->db->dbprefix('units')}.name, b.name as base_unit, {$this->db->dbprefix('units')}.operator, {$this->db->dbprefix('units')}.operation_value, pg.name as pg_name", FALSE)
            ->from("units")
            ->join("units b", 'b.id=units.base_unit', 'left')
            ->join("price_groups pg", 'pg.id=units.price_group_id', 'left')
            ->group_by('units.id')
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('system_settings/edit_unit/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_unit") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_unit") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_unit/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    public function add_unit()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('code', lang("unit_code"), 'trim|is_unique[units.code]|required');
        $this->form_validation->set_rules('name', lang("unit_name"), 'trim|is_unique[units.name]|required');
        if ($this->input->post('base_unit')) {
            $this->form_validation->set_rules('operator', lang("operator"), 'required');
            $this->form_validation->set_rules('operation_value', lang("operation_value"), 'trim|required');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'base_unit' => $this->input->post('base_unit') ? $this->input->post('base_unit') : NULL,
                'operator' => $this->input->post('base_unit') ? $this->input->post('operator') : NULL,
                'operation_value' => $this->input->post('operation_value') ? $this->input->post('operation_value') : NULL,
                'price_group_id' => $this->input->post('price_group_id') ? $this->input->post('price_group_id') : NULL,
                );

        } elseif ($this->input->post('add_unit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/units");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addUnit($data)) {
            $this->session->set_flashdata('message', lang("unit_added"));
            admin_redirect("system_settings/units");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['base_units'] = $this->site->getAllBaseUnits();
            $this->data['modal_js'] = $this->site->modal_js();
            if ($this->Settings->prioridad_precios_producto == 10) {
                $this->data['price_groups'] = $this->site->getAllPriceGroups();
            }
            $this->load_view($this->theme . 'settings/add_unit', $this->data);

        }
    }

    public function edit_unit($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('code', lang("code"), 'trim|required');
        $unit_details = $this->site->getUnitByID($id);
        if ($this->input->post('code') != $unit_details->code) {
            $this->form_validation->set_rules('code', lang("code"), 'required|is_unique[units.code]');
        }
        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        if ($this->input->post('base_unit')) {
            $this->form_validation->set_rules('operator', lang("operator"), 'required');
            $this->form_validation->set_rules('operation_value', lang("operation_value"), 'trim|required');
        }

        $prevname = $this->input->post('prevname');
        $actualname = $this->input->post('name');
        if ($actualname && $actualname != $prevname) {
            $this->form_validation->set_rules('name', lang("name"), 'required|is_unique[units.name]');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'base_unit' => $this->input->post('base_unit') ? $this->input->post('base_unit') : NULL,
                'operator' => $this->input->post('base_unit') ? $this->input->post('operator') : NULL,
                'operation_value' => $this->input->post('operation_value') ? $this->input->post('operation_value') : NULL,
                'price_group_id' => $this->input->post('price_group_id') ? $this->input->post('price_group_id') : NULL,
                );

        } elseif ($this->input->post('edit_unit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/units");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateUnit($id, $data)) {
            $this->session->set_flashdata('message', lang("unit_updated"));
            admin_redirect("system_settings/units");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['unit'] = $unit_details;
            $this->data['base_units'] = $this->site->getAllBaseUnits();
            $this->data['can_edit'] = $this->site->validate_all_product_unit_movements($id);
            if ($this->Settings->prioridad_precios_producto == 10) {
                $this->data['price_groups'] = $this->site->getAllPriceGroups();
            }
            $this->load_view($this->theme . 'settings/edit_unit', $this->data);

        }
    }

    public function delete_unit($id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->settings_model->getUnitChildren($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("unit_has_subunit")));
        }

        if ($this->settings_model->get_unit_movements($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("unit_has_movements")));
        }

        if ($this->settings_model->deleteUnit($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("unit_deleted")));
        }
    }

    public function unit_actions()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        // $this->settings_model->deleteUnit($id);
                    }
                    $this->session->set_flashdata('message', lang("units_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('categories'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('base_unit'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('operator'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('operation_value'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('price_group'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $unit = $this->site->getUnitByID($id);
                        $pg = $this->site->getPriceGroupByID($unit->price_group_id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $unit->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $unit->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $unit->base_unit);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $unit->operator);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $unit->operation_value);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $pg ? $pg->name : "");
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'units_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function price_groups()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('price_groups')));
        $meta = array('page_title' => lang('price_groups'), 'bc' => $bc);
        $this->page_construct('settings/price_groups', $meta, $this->data);
    }

    public function w_units()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => 'Unidades'));
        $meta = array('page_title' => 'Unidades', 'bc' => $bc);
        $this->page_construct('settings/w_units', $meta, $this->data);
    }

    public function getWappsiUnits()
    {
        $this->sma->checkPermissions();
        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, operation_value")
            ->from("units")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('system_settings/product_units/$1') . "' class='tip' title='" . lang("group_product_prices") . "'><i class=\"fa fa-eye\"></i></a> </div>", "id");
            //->unset_column('id');
        echo $this->datatables->generate();
    }
    // Termina Wappsi codigo
    public function getPriceGroups()
    {
        $this->sma->checkPermissions();

        $this->load->library('datatables');

        $see = "<a href='" . admin_url('system_settings/group_product_prices/$1') . "' class='tip'>
            <i class='fa fa-eye'></i> " . lang("see") . "
        </a>";
        $edit = "<a href='" . admin_url('system_settings/edit_price_group/$1') . "' class='tip' data-toggle='modal' data-target='#myModal'>
            <i class='fa fa-edit'></i> " . lang("edit") . "
        </a>";
        $delete = "<a href='#' class='tip po' title='<b>" . lang("delete_price_group") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_price_group/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>
            <i class='fa fa-trash-o'></i> " . lang("delete") . "
        </a>";

        $action = '<div class="btn-group text-left">
            <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                <i class="fas fa-ellipsis-v fa-lg"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>' . $see . '</li>
                <li>' . $edit . '</li>
                <li>' . $delete . '</li>
            </ul>
        </div>';

        $this->datatables
            ->select("id, name, IF(price_group_base = 1, 'Base', 'No base') as price_group_base")
            ->from("price_groups")
            ->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function add_price_group()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('name', lang("group_name"), 'trim|is_unique[price_groups.name]|required|alpha_numeric_spaces');

        if ($this->form_validation->run() == true) {
            $data = array(
                        'name' => $this->input->post('name'),
                        'price_group_base' => $this->input->post('price_group_base') ? 1 : 0,
                        );
        } elseif ($this->input->post('add_price_group')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/price_groups");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addPriceGroup($data)) {
            $this->session->set_flashdata('message', lang("price_group_added"));
            admin_redirect("system_settings/price_groups");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/add_price_group', $this->data);
        }
    }

    public function edit_price_group($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('name', lang("group_name"), 'trim|required|alpha_numeric_spaces');
        $pg_details = $this->settings_model->getPriceGroupByID($id);
        if ($this->input->post('name') != $pg_details->name) {
            $this->form_validation->set_rules('name', lang("group_name"), 'required|is_unique[price_groups.name]');
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                            'name' => $this->input->post('name'),
                            'price_group_base' => $this->input->post('price_group_base') ? 1 : 0,
                            'minimun_list'     => $this->input->post('minimun_list') ? 1 :0,
                        );
        } elseif ($this->input->post('edit_price_group')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/price_groups");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updatePriceGroup($id, $data)) {
            $this->session->set_flashdata('message', lang("price_group_updated"));
            admin_redirect("system_settings/price_groups");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['price_group'] = $pg_details;
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_price_group', $this->data);
        }
    }

    public function delete_price_group($id = NULL)
    {
        $this->sma->checkPermissions();
        //Validación de clientes relacionados al grupo de precios a borrar.
        if ($this->settings_model->getCustomerByPriceGroups($id) !== FALSE) {
            $this->sma->send_json(array('error' => 1, 'msg' => 'No se puede eliminar el grupo de precios por que hay clientes relacionados a éste.'));
        }
        if ($this->Settings->price_group == $id) {
            $this->sma->send_json(array('error' => 1, 'msg' => 'No se puede eliminar el grupo de precios por es el asignado por defecto.'));
        }
        if ($this->settings_model->get_products_price_group_by_id($id) !== FALSE) {
            $this->sma->send_json(array('error' => 1, 'msg' => 'No se puede eliminar el grupo de precios por que hay productos relacionados a éste.'));
        }
        if ($this->settings_model->deletePriceGroup($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("price_group_deleted")));
        }
    }

    public function product_group_price_actions($group_id)
    {
        $this->sma->checkPermissions();
        if (!$group_id) {
            $this->session->set_flashdata('error', lang('no_price_group_selected'));
            admin_redirect('system_settings/price_groups');
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'update_price') {

                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->setProductPriceForPriceGroup($id, $group_id, $this->input->post('price'.$id));
                        $group_price_data = $this->settings_model->getPriceGroupByID($group_id);
                        if ($group_price_data->price_group_base == 1) {
                            $this->settings_model->updateProductPrice($id, $this->input->post('price'.$id));
                        }
                    }
                    $this->session->set_flashdata('message', lang("products_group_price_updated"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'delete') {

                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->deleteProductGroupPrice($id, $group_id);
                    }
                    $this->session->set_flashdata('message', lang("products_group_price_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('tax_rates'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('price'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('group_name'));
                    $row = 2;
                    $group = $this->settings_model->getPriceGroupByID($group_id);
                    foreach ($_POST['val'] as $id) {
                        $pgp = $this->settings_model->getProductGroupPriceByPID($id, $group_id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $pgp->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $pgp->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $pgp->price);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $group->name);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'price_groups_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_price_group_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function product_unit_price_actions($unit_id = NULL)
    {
        $this->sma->checkPermissions();
        //$this->sma->print_arrays($operation_value);

        $operation_value = $this->input->post('cantidad');

        if (!$unit_id) {
            $this->session->set_flashdata('error', lang('no_price_group_selected'));
            admin_redirect('system_settings/price_groups');
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'update_price') {

                    // Actualización de multiples registros de precios por unidad

                    foreach ($_POST['val'] as $id) {
                        // exit($this->input->post('price'.$id));
                        //$this->settings_model->setProductPriceForPriceGroup($id, $unit_id, $this->input->post('price'.$id));
                        //$this->settings_model->setProductPriceForUnit($id, $unit_id, $this->input->post('price'.$id),$this->input->post('cantidad'.$id));
                        $this->settings_model->setProductPriceForUnit($id, $unit_id, $this->input->post('price'.$id), $operation_value);
                    }
                    $this->session->set_flashdata('message', lang("products_group_price_updated"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'delete') {

                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->deleteProductGroupPrice($id, $unit_id);
                    }
                    $this->session->set_flashdata('message', lang("products_group_price_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('tax_rates'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('price'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('group_name'));
                    $row = 2;
                    $group = $this->settings_model->getPriceGroupByID($unit_id);
                    foreach ($_POST['val'] as $id) {
                        $pgp = $this->settings_model->getProductGroupPriceByPID($id, $unit_id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $pgp->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $pgp->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $pgp->price);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $group->name);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'price_groups_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    return create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_price_group_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function product_units($unit_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$unit_id) {
            $this->session->set_flashdata('error', 'No se ha seleccionado ninguna unidad');
            admin_redirect('system_settings/w_units');
        }
        $this->data['unit'] = $this->settings_model->getUnitByID($unit_id);
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')),  array('link' => admin_url('system_settings/w_units'), 'page' => 'Unidades'), array('link' => '#', 'page' => 'Precios por unidad de medida'));
        $meta = array('page_title' => 'Precios por unidad de medida', 'bc' => $bc);
        $this->page_construct('settings/product_units', $meta, $this->data);
    }

    public function wGetProducts($unit_id = NULL, $opertion_value = NULL)
    {
        $this->sma->checkPermissions();
        if (!$unit_id) {
            $this->session->set_flashdata('error', lang('no_price_group_selected'));
            admin_redirect('system_settings/w_units');
        }

        if(!$opertion_value){
          $opertion_value = 1;
        }

        $pp = "(

        SELECT
            {$this->db->dbprefix('unit_prices')}.id_product as product_id,
            {$this->db->dbprefix('unit_prices')}.code as product_code,
            {$this->db->dbprefix('unit_prices')}.valor_unitario as valor,
            {$this->db->dbprefix('unit_prices')}.cantidad as cantidad,
            {$this->db->dbprefix('unit_prices')}.unit_id as unit_id
        FROM {$this->db->dbprefix('unit_prices')}
        WHERE unit_id = {$unit_id} ) PP";

        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('products')}.id as id,
                        {$this->db->dbprefix('products')}.code as product_code,
                        {$this->db->dbprefix('products')}.name as product_name,
                        CONCAT({$this->db->dbprefix('products')}.id, '__',
                            COALESCE(IF(main_unit.id IS NOT NULL,
                                COALESCE({$this->db->dbprefix('products')}.price, 0),
                                PP.valor
                            ) ".
                                ($this->Settings->ipoconsumo && $this->Settings->precios_por_unidad_presentacion == 2 ?
                                    " + IF(
                                            {$this->db->dbprefix('products')}.tax_method = 0,
                                                (COALESCE({$this->db->dbprefix('products')}.consumption_sale_tax, 0) * COALESCE({$this->db->dbprefix('units')}.operation_value, 1))
                                            , 0
                                        ) "
                                    :
                                    ""
                                ).", 0)
                        ) as valor
                    ")
            ->from("products")
            ->join($pp, 'PP.product_id = products.id', 'left')
            ->join('units', 'units.id = PP.unit_id', 'left')
            ->join('units as main_unit', 'main_unit.id = products.unit AND main_unit.id = '.$unit_id, 'left')
            ->add_column("Actions", "<div class=\"text-center\"><button class=\"btn btn-primary btn-xs form-submit\" type=\"button\"><i class=\"fa fa-check\"></i></button></div>", "id");

        echo $this->datatables->generate();
    }

    public function update_product_unit_price($unit_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$unit_id) {
            $this->sma->send_json(array('status' => 0));
        }
        $product_id = $this->input->post('product_id', TRUE);
        $price = $this->input->post('price', TRUE);
        $cantidad = $this->input->post('cantidad', TRUE);


        $product = $this->site->getProductByID($product_id);
        $unit_data = $this->site->getUnitByID($unit_id);
        $prev_price = $this->site->getProductUnitPrice($product_id, $unit_id);

        // desarrollando aqui

        if (!empty($product_id) && !empty($price) && !empty($cantidad)) {
            if ($this->settings_model->setProductPriceForUnit($product_id, $unit_id, $price, $cantidad)) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 1,
                    'table_name' => 'product_prices',
                    'record_id' => $product_id,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => 'group_product_prices',
                    'description' => $this->session->first_name." ".$this->session->last_name." Cambió el precio del producto ".$product->name." para la unidad ".$unit_data->name." de ".$this->sma->formatMoney($prev_price ? $prev_price->valor_unitario : $product->price)." a ".$this->sma->formatMoney($price),
                ]);
                $this->sma->send_json(array('status' => 1));
            }
        }
        $this->sma->send_json(array('status' => 0));
    }

    public function group_product_prices($group_id = NULL)
    {
        $this->sma->checkPermissions();

        if (!$group_id) {
            $this->session->set_flashdata('error', lang('no_price_group_selected'));
            admin_redirect('system_settings/price_groups');
        }

        $this->data['price_group'] = $this->settings_model->getPriceGroupByID($group_id);
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $this->page_construct('settings/group_product_prices', ['page_title' => lang('group_product_prices')." (".$this->data['price_group']->name.")"], $this->data);
    }

    public function getProductPrices($group_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$group_id) {
            $this->session->set_flashdata('error', lang('no_price_group_selected'));
            admin_redirect('system_settings/price_groups');
        }

        $pp = "(
                SELECT
                    {$this->db->dbprefix('product_prices')}.product_id as product_id,
                    {$this->db->dbprefix('product_prices')}.price as price
                FROM {$this->db->dbprefix('product_prices')}
                WHERE price_group_id = {$group_id} ) PP";

        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('products')}.id as id,
                        {$this->db->dbprefix('products')}.code as product_code,
                        {$this->db->dbprefix('products')}.name as product_name,
                        (PP.price ".($this->Settings->ipoconsumo ? " + COALESCE({$this->db->dbprefix('products')}.consumption_sale_tax)" : "").") as price
                    ")
            ->from("products")
            ->where('products.discontinued', 0)
            ->join($pp, 'PP.product_id=products.id', 'left')
            ->edit_column("price", "$1__$2", 'id, price')
            ->add_column("Actions", "<div class=\"text-center\"><button class=\"btn btn-primary btn-xs form-submit\" type=\"button\"><i class=\"fa fa-check\"></i></button></div>", "id");

        echo $this->datatables->generate();
    }

    public function update_unit_prices_csv($unit_id = NULL)
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('message', lang("disabled_in_demo"));
                admin_redirect('welcome');
            }

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/product_units/".$unit_id);
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                //code,unit_id,cantidad,valor_unitario
                $keys = array('code', 'unit_id', 'cantidad', 'valor_unitario');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if ($product = $this->site->getProductByCode(trim($csv_pr['code']))) {
                    $data[] = array(
                        'id_product' => $product->id,
                        'unit_id' => $unit_id,
                        'cantidad' => $csv_pr['cantidad'],
                        'valor_unitario' => $csv_pr['valor_unitario'],
                        'code' => $csv_pr['code']
                        );


                    // $this->sma->print_arrays($product);
                    //$this->sma->print_arrays($data);







                    } else {
                        $this->session->set_flashdata('message', lang("check_product_code") . " (" . $csv_pr['code'] . "). " . lang("code_x_exist") . " " . lang("line_no") . " :P " . $rw);
                        admin_redirect("system_settings/product_units/".$unit_id);
                    }
                    $rw++;
                }
            }

        } elseif ($this->input->post('update_price')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/product_units/".$unit_id);
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            $this->settings_model->updateUnitPrices($data);
            $this->session->set_flashdata('message', lang("price_updated"));
            // $this->session->set_flashdata('message', $aux);
            admin_redirect("system_settings/product_units/".$unit_id);
        } else {

            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );

            $this->data['unit'] = $this->settings_model->getUnitByID($unit_id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'settings/update_unit_price', $this->data);

        }
    }

    public function update_product_group_price($group_id = NULL)
    {
        $this->sma->checkPermissions();

        if (!$group_id) {
            $this->sma->send_json(array('status' => 0));
        }

        $product_id = $this->input->post('product_id', TRUE);
        $price = $this->input->post('price', TRUE);

        $product = $this->site->getProductByID($product_id);
        $price_group = $this->site->getPriceGroupByID($group_id);
        $prev_price = $this->site->getProductGroupPrice($product_id, $group_id);

        if (!empty($product_id) && !empty($price)) {
            if ($this->settings_model->setProductPriceForPriceGroup($product_id, $group_id, $price)) {
                $this->syncProductPrice($product_id, $price, $price_group);

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 1,
                    'table_name' => 'product_prices',
                    'record_id' => $product_id,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => 'group_product_prices',
                    'description' => $this->session->first_name." ".$this->session->last_name." Cambió el precio del producto ".$product->name." para la lista de precios ".$price_group->name." de ".$this->sma->formatMoney($prev_price->price)." a ".$this->sma->formatMoney($price),
                ]);
                $this->sma->send_json(array('status' => 1));
            }
        }

        $this->sma->send_json(array('status' => 0));
    }

    public function update_prices_csv($group_id = NULL)
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('message', lang("disabled_in_demo"));
                admin_redirect('welcome');
            }

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/group_product_prices/".$group_id);
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'price');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if ($product = $this->site->getProductByCode(trim($csv_pr['code']))) {
                    $data[] = array(
                        'product_id' => $product->id,
                        'price' => $csv_pr['price'],
                        'price_group_id' => $group_id
                        );
                    } else {
                        $this->session->set_flashdata('message', lang("check_product_code") . " (" . $csv_pr['code'] . "). " . lang("code_x_exist") . " " . lang("line_no") . " " . $rw);
                        admin_redirect("system_settings/group_product_prices/".$group_id);
                    }
                    $rw++;
                }
            }

        } elseif ($this->input->post('update_price')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/group_product_prices/".$group_id);
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            $this->settings_model->updateGroupPrices($data);
            $this->session->set_flashdata('message', lang("price_updated"));
            admin_redirect("system_settings/group_product_prices/".$group_id);
        } else {

            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['group'] = $this->site->getPriceGroupByID($group_id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'settings/update_price', $this->data);

        }
    }

    private function syncProductPrice($productId, $price, $priceGroup)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();

            if (!empty($integrations)) {
                foreach ($integrations as $integration) {
                    $this->integration = $integration;

                    $TaxRatesModel = new TaxRates_model();
                    $ProductModel = new products_model();

                    $Product = new Product();
                    $Product->open($this->integration);
                    $storeProduct = $Product->find(['id_wappsi' => $productId]);
                    if (!empty($storeProduct)) {
                        $product = $ProductModel->getProductByID($productId);
                        $tax = $TaxRatesModel->find($product->tax_rate);

                        if ($priceGroup->price_group_base == YES) {
                            $newPrice = ($tax->type == 1) ? $price / (1 + ($tax->rate / 100)) : $price;
                            $Product->update([
                                'lowest_price' => $newPrice,
                                'highest_price' => $newPrice
                            ], $storeProduct->id);
                        }

                        $ProductVariation = new ProductVariation();
                        $ProductVariation->open($this->integration);
                        $productsVariations = $ProductVariation->get(['product_id' => $storeProduct->id]);

                        if (!empty($productsVariations)) {
                            foreach ($productsVariations as $variation) {
                                $variationWappsi = $ProductModel->findProductVariant(['id'=>$variation->id_wappsi, 'product_id'=>$productId]);

                                $priceGroupPrice = ($tax->type == 1) ? $price / (1 + ($tax->rate / 100)) : $price;

                                if (!empty($variationWappsi) && $variationWappsi->price > 0) {
                                    $priceTotal = $priceGroupPrice + $variationWappsi->price;
                                } else {
                                    $priceTotal = $priceGroupPrice;
                                }

                                $ProductVariation->update(['price' => $priceTotal], $variation->id);
                            }
                        } else {
                            return 'No existe variaciones para este producto';
                        }
                    } else {
                        return 'No está sincronizado el producto en la tienda';
                    }
                }
            }
        }
    }

    public function brands()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('brands')));
        $meta = array('page_title' => lang('brands'), 'bc' => $bc);

        $this->page_construct('settings/brands', $meta, $this->data);
    }

    public function getBrands()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, image, code, name, slug, synchronized_store")
            ->from("brands")
            ->add_column("Actions", "<div class='btn-group text-left'>
                <button type='button' class='btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle' data-toggle='dropdown' data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                    <i class='fas fa-ellipsis-v fa-lg'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li>
                        <a href='" . admin_url('system_settings/edit_brand/$1') . "' data-toggle='modal' data-target='#myModal' class='tip'>
                            <i class=\"fa fa-edit\"></i> " . sprintf(lang('edit_brand'), lang('brand')) . "
                        </a>
                    </li>
                    <li>
                        <a href='#' class='tip po' title='<b>" . sprintf(lang('delete_brand'), lang('brand')) . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_brand/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>
                            <i class=\"fa fa-trash-o\"></i> ". sprintf(lang('delete_brand'), lang('brand')) ."
                        </a>
                    </li>
                </ul>
            </div>", "id");

        echo $this->datatables->generate();
    }

    public function add_brand()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('name', lang("brand_name"), 'trim|required|is_unique[brands.name]|alpha_numeric_spaces');
        $this->form_validation->set_rules('slug', lang("slug"), 'trim|required|is_unique[brands.slug]|alpha_dash');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'code_consecutive' => $this->input->post('code_consecutive'),
                'hide_ecommerce'   => $this->input->post('hide_ecommerce') ? 1 : 0,
                );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                $this->image_lib->clear();
            }

        } elseif ($this->input->post('add_brand')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/brands");
        }

        if ($this->form_validation->run() == true && $brandId = $this->settings_model->addBrand($data)) {
            $syncCreatedBrand = $this->syncCreatedBrand($brandId);

            $this->session->set_flashdata('message', lang("brand_added"));

            if (isset($syncCreatedBrand->warning) && !empty($syncCreatedBrand->warning)) {
                $this->session->set_flashdata('warning', lang("brand_added") .  $syncCreatedBrand->warning);
            }
            if (isset($syncCreatedBrand->message) && !empty($syncCreatedBrand->message)) {
                $this->session->set_flashdata('message', lang("brand_added") .  $syncCreatedBrand->message);
            }

            admin_redirect("system_settings/brands");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load_view($this->theme . 'settings/add_brand', $this->data);
        }
    }

    private function syncCreatedBrand($id)
    {
        $_errorMessage = '';
        $_message = '';
        $response = [];
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            $brand = $this->site->getBrandByID($id);

            if ($brand->hide_ecommerce == YES) {
                return (object) $response;
            }

            if (!empty($integrations) && !empty($brand)) {
                $this->load->integration_model('Brand');
                foreach ($integrations as $integration) {
                    $Brand = new Brand();
                    $Brand->open($integration);
                    $slug = !empty($brand->slug) ? $brand->slug : $this->sma->slug($brand->name);
                    $data = [
                        "name" => $brand->name,
                        "slug" => $slug,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s"),
                        "id_wappsi" => $brand->id,
                    ];
                    if ($Brand->create($data)) {
                        $this->settings_model->updateBrand($id, ["synchronized_store" => YES]);
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    } else {
                        $_errorMessage .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $Brand->close();
                }
                if (!empty($_message)) {
                    $response["message"] = " Sincronizado en: ". rtrim($_message, ", ");
                }
                if (!empty($_errorMessage)) {
                    $response["warning"] = " No fue posible sincronizar en: ". rtrim($_errorMessage, ", ");
                }
            }
        }
        return (object) $response;
    }

    public function edit_brand($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('name', lang("brand_name"), 'trim|required|alpha_numeric_spaces');
        $brand_details = $this->site->getBrandByID($id);
        if ($this->input->post('name') != $brand_details->name) {
            $this->form_validation->set_rules('name', lang("brand_name"), 'required|is_unique[brands.name]');
        }
        $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
        if ($this->input->post('slug') != $brand_details->slug) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash|is_unique[brands.slug]');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'hide_ecommerce'   => $this->input->post('hide_ecommerce') ? 1 : 0,
                );
            $code_consecutive = $this->input->post('code_consecutive');
            if ($code_consecutive != NULL) {
                $data['code_consecutive'] = $code_consecutive;
            }

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                $this->image_lib->clear();
            }

        } elseif ($this->input->post('edit_brand')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/brands");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateBrand($id, $data)) {
            $syncUpdatedBrand = $this->syncUpdatedBrand($id);
            if (!empty($syncUpdatedBrand)) {
                $this->session->set_flashdata('warning', lang("brand_updated") . " $syncUpdatedBrand");
            } else {
                $this->session->set_flashdata('message', lang("brand_updated"));
            }


            $this->session->set_flashdata('message', lang("brand_updated"));
            admin_redirect("system_settings/brands");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['brand'] = $brand_details;
            $this->load_view($this->theme . 'settings/edit_brand', $this->data);

        }
    }

    private function syncUpdatedBrand($id)
    {
        $_message = '';
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            $brand = $this->site->getBrandByID($id);
            if (!empty($integrations) && !empty($brand)) {
                $this->load->integration_model('Brand');
                foreach ($integrations as $integration) {
                    $Brand = new Brand();
                    $Brand->open($integration);
                    $brandExt = $Brand->find(["id_wappsi"=>$brand->id]);
                    $data = [
                        "name" => $brand->name,
                        "slug" => $brand->slug,
                        "updated_at" => date("Y-m-d H:i:s"),
                    ];
                    if (!$Brand->update($data, $brandExt->id)) {
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $Brand->close();
                }
                if (!empty($_message)) {
                    $_message = "No fue posible sincronizar en: $_message";
                }
            }
        }
        return trim($_message, ', ');
    }

    public function delete_brand($id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->settings_model->brandHasProducts($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("brand_has_products")));
        }

        if ($this->settings_model->deleteBrand($id)) {
            $syncDeletedVariant = $this->syncDeletedVariant($id);
            $this->sma->send_json(array('error' => 0, 'msg' => lang("brand_deleted") . " $syncDeletedVariant"));
        }
    }

    private function syncDeletedBrand($id)
    {
        $_message = '';
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Brand');
                foreach ($integrations as $integration) {
                    $Brand = new Brand();
                    $Brand->open($integration);

                    $brandExt = $Brand->find(["id_wappsi"=>$id]);
                    if (!$Brand->delete($brandExt->id)) {
                        $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                    }
                    $Brand->close();
                }
                if (!empty($_message)) {
                    $_message = "No fue posible sincronizar en: $_message";
                }
            }
        }
        return trim($_message, ', ');
    }

    public function syncBrandsStore()
    {
        $brandIds = $this->input->post('brandIds');
        $warningMessage = '';
        $response = [];

        if (!empty($brandIds)) {
            foreach ($brandIds as $brandId) {
                $brand = $this->site->getBrandByID($brandId);
                if ($brand->synchronized_store == 0) {
                    $syncCreatedBrand = $this->syncCreatedBrand($brandId);

                    if (isset($syncCreatedBrand->warning) && !empty($syncCreatedBrand->warning)) {
                        $warningMessage .= $syncCreatedBrand->warning;
                    }
                }
            }

            if (!empty($warningMessage)) {
                $response['warning'] = $warningMessage;
            }

            echo json_encode($response);
        }
    }

    public function import_brands()
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/brands");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('name', 'code', 'image');
                $final = array();

                // echo var_dump($keys);
                foreach ($arrResult as $key => $value) {
                // echo var_dump($value);
                    if(!$final[] = array_combine($keys, $value)){//VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key+1)));
                        admin_redirect("system_settings/brands");
                        break;
                    }
                }


                foreach ($final as $csv_ct) {
                    if ( ! $this->settings_model->getBrandByName(trim($csv_ct['name']))) {
                        $data[] = array(
                            'code' => trim($csv_ct['code']),
                            'name' => trim($csv_ct['name']),
                            'image' => trim($csv_ct['image']),
                            'last_update' => date('Y-m-d H:i:s'),
                            );
                    }
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && !empty($data) && $this->settings_model->addBrands($data)) {
            $this->session->set_flashdata('message', lang("brands_added"));
            admin_redirect('system_settings/brands');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'settings/import_brands', $this->data);

        }
    }

    public function brand_actions()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->deleteBrand($id);
                    }
                    $this->session->set_flashdata('message', lang("brands_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('brands'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('image'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $brand = $this->site->getBrandByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $brand->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $brand->code);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $brand->image);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'brands_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else if ($this->input->post('form_action') == 'fix_slug') {
                    $this->db->query("UPDATE {$this->db->dbprefix('brands')} C
                                        INNER JOIN (SELECT id, slug, LOWER(REPLACE(name, ' ', '-')) AS new_slug FROM {$this->db->dbprefix('brands')}
                                    WHERE slug IS NULL) tbl ON tbl.id = C.id
                                    SET C.slug = tbl.new_slug
                                    WHERE C.slug IS NULL");
                $this->session->set_flashdata('message', lang("slug_fixed"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function payment_methods()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('payment_methods')));
        $meta = array('page_title' => lang('payment_methods'), 'bc' => $bc);
        $this->page_construct('settings/payment_methods', $meta, $this->data);
    }

    public function getPaymentMethods()
    {
        if ($this->site->wappsiContabilidadVerificacion() > 0) {
            $cs = $this->session->userdata('accounting_module');
            $l_table = 'sma_ledgers_con'.$cs;
            $table = 'sma_payment_methods_con'.$cs;
            $rl = $this->db->dbprefix.'rl';
            $pl = $this->db->dbprefix.'pl';

            $this->load->library('datatables');
            $this->datatables
                ->select("payment_methods.id as id, payment_methods.name, payment_methods.code as code, state_sale, ".$rl.".name as receipt_ledger, state_purchase, ".$pl.".name as payment_ledger")
                ->from("payment_methods")
                ->join($table, 'payment_methods.code = '.$table.'.type', 'left')
                ->join($l_table.' AS '.$rl, $rl.'.id = '.$table.'.receipt_ledger_id', 'left')
                ->join($l_table.' AS '.$pl, $pl.'.id = '.$table.'.payment_ledger_id', 'left')
                ->order_by("id asc");
            $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                            <a href='" . admin_url('system_settings/edit_payment_method/$1') . "' class='tip' title='" . lang("edit_payment_method") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a>
                                            <a href='#' class='tip po' title='<b>" . lang("delete_payment_method") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_payment_method/$1/$2') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>
                                        </div>", "id, code");
        } else {
            $this->load->library('datatables');
            $this->datatables
                ->select("id, name, code, state_sale, 'N/A' as receipt_ledger_id, state_purchase, 'N/A' as payment_ledger_id ")
                ->from("payment_methods")
                ->order_by("id asc");
            $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                            <a href='" . admin_url('system_settings/edit_payment_method/$1') . "' class='tip' title='" . lang("edit_payment_method") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a>
                                        </div>", "id");
        }


        echo $this->datatables->generate();
    }

    public function add_payment_method()
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('code', lang("code"), 'trim|is_unique[payment_methods.code]|required');
        $this->form_validation->set_rules('name', lang("name"), 'required');
        // $this->form_validation->set_rules('state_sale', lang("state_sale"), 'required');
        // $this->form_validation->set_rules('state_purchase', lang("state_purchase"), 'required');

        if ($this->Settings->modulary) {
            if ($this->input->post('state_sale')) {
                $this->form_validation->set_rules('receipt_ledger_id', lang("receipt_ledger"), 'required');
            }
            if ($this->input->post('state_purchase')) {
                $this->form_validation->set_rules('payment_ledger_id', lang("payment_ledger"), 'required');
            }
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                'code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'state_sale' => $this->input->post('state_sale') ? 1 : 0,
                'state_purchase' => $this->input->post('state_purchase') ? 1 : 0,
                'cash_payment' => $this->input->post('cash_payment') ? 1 : 0,
                'due_payment' => $this->input->post('due_payment') ? 1 : 0,
                'code_fe' => $this->input->post('code_fe'),
                'commision_value' => $this->input->post('commision_value'),
                'commision_ledger_id' => $this->input->post('commision_ledger_id'),
                'retefuente_value' => $this->input->post('retefuente_value'),
                'retefuente_ledger_id' => $this->input->post('retefuente_ledger_id'),
                'reteiva_value' => $this->input->post('reteiva_value'),
                'reteiva_ledger_id' => $this->input->post('reteiva_ledger_id'),
                'reteica_value' => $this->input->post('reteica_value'),
                'reteica_ledger_id' => $this->input->post('reteica_ledger_id'),
                'icon' => $this->input->post('icon'),
            );
            if ($this->Settings->modulary) {
                $data_conta = array(
                    'type' => $this->input->post('code'),
                    'receipt_ledger_id' => $this->input->post('receipt_ledger_id'),
                    'payment_ledger_id' => $this->input->post('payment_ledger_id')
                );
            }

            if ($this->input->post('billers[]')) {
                $data['billers'] = json_encode($this->input->post('billers[]'));
            } else {
                $data['billers'] = NULL;
            }

            if ($this->input->post('commision_value') && !$this->input->post('commision_ledger_id')) {
                $this->session->set_flashdata('error', lang("commision_ledger_id"));
                admin_redirect("system_settings/payment_methods");
            }

            if ($this->input->post('retefuente_value') && !$this->input->post('retefuente_ledger_id')) {
                $this->session->set_flashdata('error', lang("retefuente_ledger_id"));
                admin_redirect("system_settings/payment_methods");
            }

            if ($this->input->post('reteiva_value') && !$this->input->post('reteiva_ledger_id')) {
                $this->session->set_flashdata('error', lang("reteiva_ledger_id"));
                admin_redirect("system_settings/payment_methods");
            }

            if ($this->input->post('reteica_value') && !$this->input->post('reteica_ledger_id')) {
                $this->session->set_flashdata('error', lang("reteica_ledger_id"));
                admin_redirect("system_settings/payment_methods");
            }

        } elseif ($this->input->post('add_payment_method')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/payment_methods");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addPaymentMethod($data, (isset($data_conta) ? $data_conta : null))) {
            $this->session->set_flashdata('message', lang("payment_method_added"));
            admin_redirect("system_settings/payment_methods");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['price_groups'] = $this->settings_model->getAllPriceGroups();
            $this->data['payment_mean_codes'] = $this->settings_model->get_payments_mean_codes();
            $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            if ($this->Settings->modulary) {
                $this->data['ledgers'] = $this->site->getAllLedgers();
            }
            $icons = $this->getIconsList();
            $this->data['icon_files'] = $icons['icon_files'];
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/add_payment_method', $this->data);
        }
    }

    public function edit_payment_method($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->form_validation->set_rules('code', lang("code"), 'trim|required');
        $pm_details = $this->settings_model->getPaymentMethodById($id);

        if ($this->Settings->modulary && !$this->input->post('biller_accountant_parametrization')) {
            if ($this->input->post('state_sale')) {
                $this->form_validation->set_rules('receipt_ledger_id', lang("receipt_ledger"), 'required');
            }
        }
        if ($this->Settings->modulary && $this->input->post('state_purchase')) {
            $this->form_validation->set_rules('payment_ledger_id', lang("payment_ledger"), 'required');
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'state_sale' => $this->input->post('state_sale') ? 1 : 0,
                'state_purchase' => $this->input->post('state_purchase') ? 1 : 0,
                'cash_payment' => $this->input->post('cash_payment') ? 1 : 0,
                'due_payment' => $this->input->post('due_payment') ? 1 : 0,
                'code_fe' => $this->input->post('code_fe'),
                'commision_value' => $this->input->post('commision_value'),
                'commision_ledger_id' => $this->input->post('commision_ledger_id'),
                'retefuente_value' => $this->input->post('retefuente_value'),
                'retefuente_ledger_id' => $this->input->post('retefuente_ledger_id'),
                'reteiva_value' => $this->input->post('reteiva_value'),
                'reteiva_ledger_id' => $this->input->post('reteiva_ledger_id'),
                'reteica_value' => $this->input->post('reteica_value'),
                'reteica_ledger_id' => $this->input->post('reteica_ledger_id'),
                'supplier_id' => $this->input->post('supplier_id'),
                'icon' => $this->input->post('icon'),
            );

            if ($this->input->post('billers[]')) {
                $data['billers'] = json_encode($this->input->post('billers[]'));
            } else {
                $data['billers'] = NULL;
            }

            if ($this->input->post('code') == 'cash') {
                $data['biller_accountant_parametrization'] = $this->input->post('biller_accountant_parametrization') ? 1 : 0;
            }

            if ($this->input->post('commision_value') && !$this->input->post('commision_ledger_id')) {
                $this->session->set_flashdata('error', lang("commision_ledger_id"));
                admin_redirect("system_settings/payment_methods");
            }

            if ($this->input->post('retefuente_value') && !$this->input->post('retefuente_ledger_id')) {
                $this->session->set_flashdata('error', lang("retefuente_ledger_id"));
                admin_redirect("system_settings/payment_methods");
            }

            if ($this->input->post('reteiva_value') && !$this->input->post('reteiva_ledger_id')) {
                $this->session->set_flashdata('error', lang("reteiva_ledger_id"));
                admin_redirect("system_settings/payment_methods");
            }

            if ($this->input->post('reteica_value') && !$this->input->post('reteica_ledger_id')) {
                $this->session->set_flashdata('error', lang("reteica_ledger_id"));
                admin_redirect("system_settings/payment_methods");
            }

            if ($this->Settings->modulary) {
                $data_conta = array(
                    'id' => $this->input->post('pm_id'),
                    'type' => $this->input->post('code'),
                    'receipt_ledger_id' => $this->input->post('receipt_ledger_id'),
                    'payment_ledger_id' => $this->input->post('payment_ledger_id')
                );
            }
        } elseif ($this->input->post('edit_payment_method')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/payment_methods");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updatePaymentMethod($id, $data, (isset($data_conta) ? $data_conta : null))) { //check to see if we are updateing the customer
            $this->session->set_flashdata('message', lang("payment_method_updated"));
            admin_redirect("system_settings/payment_methods");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['payment_method'] = $this->settings_model->getPaymentMethodById($id);
            $this->data['payment_mean_codes'] = $this->settings_model->get_payments_mean_codes();
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $icons = $this->getIconsList();
            $this->data['icon_files'] = $icons['icon_files'];
            $billers_selected = json_decode($this->data['payment_method']->billers);
            $bs_arr = [];
            if ($billers_selected) {
                foreach ($billers_selected as $bs_key => $bs_id) {
                    $bs_arr[$bs_id] = 1;
                }
            }
            $this->data['billers_selected'] = $bs_arr;
            if ($this->Settings->modulary) {
                $this->data['payment_method_parameter'] = $this->site->getPaymentMethodParameter($this->data['payment_method']->code);
                $this->data['ledgers'] = $this->site->getAllLedgers();
            }
            $this->load_view($this->theme . 'settings/edit_payment_method', $this->data);
        }
    }

    public function delete_payment_method($id = NULL, $code = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->settings_model->paymentMethodHasPayments($code)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("payment_method_has_payments")));
        }

        if ($this->settings_model->deletePaymentMethod($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("payment_method_deleted")));
        }
    }

    public function compatibilities()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $meta = ['page_title' => lang('compatibilities')];

        $this->page_construct('settings/compatibilities', $meta, $this->data);
    }

    public function get_compatibilities()
    {
        $this->sma->checkPermissions();
        $this->load->library('datatables');
        $this->datatables
            ->select("compatibilidades.id AS id, brands.name AS nombre_marca, nombre, marca_nombre")
            ->join("brands", "brands.id = compatibilidades.marca_id", "INNER")
            ->from("compatibilidades")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('system_settings/edit_compatibility/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("compatibilities_edit_compatibility") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("compatibilities_delete_compatibility") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_compatibility/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    public function add_compatibility()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('marca', lang("compatibilities_brand"), 'trim|required');
        $this->form_validation->set_rules('nombre', lang("compatibilities_name"), 'trim|required');


        if ($this->form_validation->run() == TRUE)
        {
            $existing_compatibility = $this->settings_model->get_compatibility_by_name($this->input->post("nombre"));
            if (! empty($existing_compatibility)) {
                $this->session->set_flashdata('error', lang("compatibilities_duplicate"));
                admin_redirect("system_settings/compatibilities");
            }

            $data = [
                'marca_id' => $this->input->post('marca'),
                'nombre' => $this->input->post('nombre'),
                'marca_nombre' => $this->input->post('nombre_marca') ." ". $this->input->post('nombre'),
            ];
        }

        if ($this->form_validation->run() == true && $this->settings_model->add_compatibility($data)) {
            $this->session->set_flashdata('message', lang("compatibilities_compatibility_added"));
            admin_redirect("system_settings/compatibilities");
        }
        else
        {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['brands'] = $this->settings_model->get_brands();
            $this->load_view($this->theme . 'settings/add_compatibility', $this->data);
        }
    }

    public function edit_compatibility($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('marca', lang("compatibilities_brand"), 'trim|required');
        $this->form_validation->set_rules('nombre', lang("compatibilities_name"), 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $existing_compatibility = $this->settings_model->get_compatibility_by_name($this->input->post("nombre"), $id);

            if (! empty($existing_compatibility))
            {
                $this->session->set_flashdata('error', lang("compatibilities_duplicate"));
                admin_redirect("system_settings/compatibilities");
            }

            $data = [
                'marca_id' => $this->input->post('marca'),
                'nombre' => $this->input->post('nombre'),
                'marca_nombre' => $this->input->post('nombre_marca') ." ". $this->input->post('nombre'),
            ];
        }

        if ($this->form_validation->run() == TRUE && $this->settings_model->update_compatibility($id, $data)) {
            $this->session->set_flashdata('message', lang("compatibilities_compatibility_update"));
            admin_redirect("system_settings/compatibilities");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();

            $compatibility_details = $this->settings_model->get_compatibility_by_id($id);
            $this->data['compatibility'] = $compatibility_details;
            $this->data['brands'] = $this->settings_model->get_brands();
            $this->load_view($this->theme . 'settings/edit_compatibility', $this->data);
        }
    }

    public function compatibility_actions()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == TRUE)
        {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->settings_model->delete_compatibility($id);
                    }
                    $this->session->set_flashdata('message', lang("compatibilities_compatibility_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('compatibilities'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('compatibilities_brand'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('compatibilities_name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('compatibilities_full_name'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $compatibility = $this->settings_model->get_compatibility_by_id($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $compatibility->marca_id);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $compatibility->nombre);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $compatibility->marca_nombre);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'Compatibilities_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function delete_compatibility($id = NULL)
    {
        $this->sma->checkPermissions();
        if ($this->settings_model->delete_compatibility($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("compatibilities_compatibility_deleted")));
        }
    }

    public function load_states_by_country_id($country_id)
    {
        $this->sma->checkPermissions();
        $states = $this->site->getStates($country_id);
        $options = '<option value="">'. lang('select') .'</option>';

        foreach ($states as $state)
        {
            $options .= '<option value="'. $state->DEPARTAMENTO .'" data-code="'. $state->CODDEPARTAMENTO .'">'. $state->DEPARTAMENTO .'</option>';
        }

        echo  $options;
    }

    public function load_cities_by_state_id($state_id)
    {
        $this->sma->checkPermissions();
        $cities = $this->site->get_cities_by_state_id($state_id);
        $options = '<option value="">'. lang('select') .'</option>';

        foreach ($cities as $city)
        {
            if ($city->CODIGO != 17068000)
            {
                $options .= '<option value="'. $city->DESCRIPCION .'" data-code="'. $city->CODIGO .'">'. $city->DESCRIPCION .'</option>';
            }
        }

        echo $options;
    }

    public function restobar()
    {
        $this->sma->checkPermissions();
        $branch_id = $this->input->post('id_sucursal');
        $area_id = $this->input->post('id_area');

        $branches = $this->Restobar_model->get_branches();


        if (! empty($branch_id)) {
            $branch_id = $branch_id;
        } else {
            $branch_id = $branches[0]->id;
        }

        $branch_areas = $this->Restobar_model->get_branch_areas($branch_id);

        if (!empty($area_id)) {
            $area_id = $area_id;

            foreach ($branch_areas as $area) {
                if ($area->id == $area_id) {
                    $area_name = $area->nombre;
                }
            }
        } else {
            $area_id = $branch_areas[0]->id;
            $area_name = $branch_areas[0]->nombre;
        }

        $tables = $this->Restobar_model->get_tables($area_id);

        $this->data['tables'] = $tables;
        $this->data['branches'] = $branches;
        $this->data['branch_id'] = $branch_id;
        $this->data['branch_areas'] = $branch_areas;
        $this->data['area_id'] = $area_id;
        $this->data['area_name'] = $area_name;

        $this->page_construct('settings/restobar', ['page_title' => lang('system_settings')], $this->data);
    }

    public function get_table_by_id()
    {
        $this->sma->checkPermissions();
        $table_id = $this->input->post('id_mesa');

        $table_data = $this->Restobar_model->get_table_by_id($table_id);

        echo json_encode($table_data);
    }

    public function get_existing_table_number()
    {
        $this->sma->checkPermissions();
        $area_id = $this->input->post('id_area');
        $table_id = $this->input->post('id_mesa');
        $table_number = $this->input->post('numero_mesa');

        $existing_table = $this->Restobar_model->get_table_by_number($table_number, $area_id, $table_id);

        if (empty($existing_table)) {
            echo json_encode(FALSE);
        } else {
            echo json_encode(TRUE);
        }
    }

    public function save_table()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('numero_mesa', lang("restobar_label_table_number"), 'trim|required');
        $this->form_validation->set_rules('estados_mesa', lang("restobar_label_table_state"), 'trim|required');
        $this->form_validation->set_rules('numero_asientos_mesa', lang("restobar_label_number_table_seats"), 'trim|required|greater_than_equal_to[1]');
        $this->form_validation->set_rules('forma_mesa', lang("restobar_label_table_shape"), 'trim|required');
        $this->form_validation->set_rules('posicion_x_mesa', lang("restobar_label_x_position_table"), 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('posicion_y_mesa', lang("restobar_label_y_position_table"), 'trim|required|greater_than_equal_to[0]');

        if ($this->input->post('forma_mesa') == SQUARE) {
            $this->form_validation->set_rules('tamano_mesa', lang("restobar_label_table_size"), 'trim|required|greater_than_equal_to[1]');
        } else if ($this->input->post('forma_mesa') == CIRCLE || $this->input->post('forma_mesa') == ELLIPSE) {
            $this->form_validation->set_rules('radio_mesa', lang("restobar_label_table_radius"), 'trim|required|greater_than_equal_to[1]');
        } else {
            $this->form_validation->set_rules('ancho_mesa', lang("restobar_label_table_width"), 'trim|required|greater_than_equal_to[1]');
            $this->form_validation->set_rules('alto_mesa', lang("restobar_label_table_height"), 'trim|required|greater_than_equal_to[1]');
        }

        $existing_table = $this->Restobar_model->get_table_by_number($this->input->post('numero_mesa'), $this->input->post('id_area'));
        if (! empty($existing_table)) {
            echo json_encode([
                "response" => 0,
                "message" => "El número de mesa ingresado ya se encuentra registrado."
            ]);
            exit();
        }

        if(strtolower($this->input->post("area_name")) == "domicilio") {
            $tipo = "D";
        } else if(strtolower($this->input->post("area_name")) == "llevar") {
            $tipo = "LL";
        } else {
            $tipo = "M";
        }

        $image_path = $this->input->post('forma_mesa')."_".$this->input->post('numero_asientos_mesa');

        if ($this->form_validation->run()) {
            $table_data = [
                "numero"=>$this->input->post('numero_mesa'),
                "estado"=>$this->input->post('estados_mesa'),
                "forma_mesa"=>$this->input->post('forma_mesa'),
                "numero_asientos"=>$this->input->post('numero_asientos_mesa'),
                "imagen"=>$image_path,
                "posicion_x"=>$this->input->post('posicion_x_mesa'),
                "posicion_y"=>$this->input->post('posicion_y_mesa'),
                "area_id"=>$this->input->post('id_area'),
                "tipo" => $tipo
            ];

            if ($this->input->post('forma_mesa') == SQUARE) {
                $table_data["tamano"] = $this->input->post('tamano_mesa');
            } else if ($this->input->post('forma_mesa') == CIRCLE || $this->input->post('forma_mesa') == ELLIPSE) {
                $table_data["radio"] = $this->input->post('radio_mesa');
            } else {
                $table_data["ancho"] = $this->input->post('ancho_mesa');
                $table_data["alto"] = $this->input->post('alto_mesa');
            }

            $saved_table = $this->Restobar_model->insert_table($table_data);
            if ($saved_table === FALSE) {
                echo json_encode([
                    "response" => 0,
                    "message" => "No fue posible crear la mesa."
                ]);
            } else {
                echo json_encode([
                    "response" => 1,
                    "message" => "La mesa fue creada exitosamente"
                ]);
            }
        } else {
            echo json_encode([
                "response" => 0,
                "message" => validation_errors()
            ]);
        }
    }

    public function edit_table()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('numero_mesa', lang("restobar_label_table_name"), 'trim|required');
        $this->form_validation->set_rules('estados_mesa', lang("restobar_label_table_state"), 'trim|required');
        $this->form_validation->set_rules('numero_asientos_mesa', lang("restobar_label_number_table_seats"), 'trim|required|greater_than_equal_to[1]');
        $this->form_validation->set_rules('forma_mesa', lang("restobar_label_table_shape"), 'trim|required');
        $this->form_validation->set_rules('posicion_x_mesa', lang("restobar_label_x_position_table"), 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('posicion_y_mesa', lang("restobar_label_y_position_table"), 'trim|required|greater_than_equal_to[0]');

        if ($this->input->post('forma_mesa') == SQUARE) {
            $this->form_validation->set_rules('tamano_mesa', lang("restobar_label_table_size"), 'trim|required|greater_than_equal_to[1]');
        } else if ($this->input->post('forma_mesa') == CIRCLE || $this->input->post('forma_mesa') == ELLIPSE) {
            $this->form_validation->set_rules('radio_mesa', lang("restobar_label_table_radius"), 'trim|required|greater_than_equal_to[1]');
        } else {
            $this->form_validation->set_rules('ancho_mesa', lang("restobar_label_table_width"), 'trim|required|greater_than_equal_to[1]');
            $this->form_validation->set_rules('alto_mesa', lang("restobar_label_table_height"), 'trim|required|greater_than_equal_to[1]');
        }

        $table_id = $this->input->post('id_mesa');
        if (empty($table_id)) {
            echo json_encode([
                'response'=>0,
                'message'=>'No es posible actualizar la mesa.'
            ]);
            exit();
        }

        $existing_table = $this->Restobar_model->get_table_by_number($this->input->post('numero_mesa'), $this->input->post('id_area'), $this->input->post('id_mesa'));
        if (! empty($existing_table)) {
            echo json_encode([
                "response" => 0,
                "message" => "El número de mesa ingresado ya se encuentra registrado."
            ]);
            exit();
        }

        $image_path = $this->input->post('forma_mesa')."_".$this->input->post('numero_asientos_mesa');

        if ($this->form_validation->run()) {
            $table_data = [
                "numero"=>$this->input->post('numero_mesa'),
                "estado"=>$this->input->post('estados_mesa'),
                "forma_mesa"=>$this->input->post('forma_mesa'),
                "numero_asientos"=>$this->input->post('numero_asientos_mesa'),
                "imagen"=>$image_path,
                "alto"=>$this->input->post('alto_mesa'),
                "posicion_x"=>$this->input->post('posicion_x_mesa'),
                "posicion_y"=>$this->input->post('posicion_y_mesa'),
                'area_id'=>$this->input->post('id_area')
            ];

            if ($this->input->post('forma_mesa') == SQUARE) {
                $table_data["tamano"] = $this->input->post('tamano_mesa');
            } else if ($this->input->post('forma_mesa') == CIRCLE || $this->input->post('forma_mesa') == ELLIPSE) {
                $table_data["radio"] = $this->input->post('radio_mesa');
            } else {
                $table_data["ancho"] = $this->input->post('ancho_mesa');
                $table_data["alto"] = $this->input->post('alto_mesa');
            }

            $edited_table = $this->Restobar_model->update_table($table_data, $table_id);
            if ($edited_table === FALSE) {
                echo json_encode([
                    "response" => 0,
                    "message" => "No fue posible actualizar la mesa."
                ]);
            } else {
                echo json_encode([
                    "response" => 1,
                    "message" => "La mesa fue actualizada exitosamente"
                ]);
            }
        } else {
            echo json_encode([
                "response" => 0,
                "message" => validation_errors()
            ]);
        }
    }

    public function edit_table_position_data()
    {
        $this->sma->checkPermissions();
        $table_id = $this->input->post('id_mesa');
        $table_position_data = [
            'posicion_X'=>$this->input->post('posicion_x_mesa'),
            'posicion_Y'=>$this->input->post('posicion_y_mesa'),
            'rotacion'=>$this->input->post('rotacion_mesa')
        ];
        $updated_table_position_data = $this->Restobar_model->update_table($table_position_data, $table_id);
        echo json_encode($updated_table_position_data);
    }

    public function validate_products_tax_methods()
    {

        $tax_method = $this->input->get('tax_method');

        $response = array(
                        'different_defined_method' => false,
                        'product_counting' => 0
                        );

        if ($tax_method != 2) {
            $different_defined_method = $this->site->getProductsByTaxMethod($tax_method, true);
            if ($different_defined_method) {
                $response = array(
                        'different_defined_method' => true,
                        'product_counting' => count($different_defined_method)
                        );
            }
        }

        exit(json_encode($response));
    }

    public function get_account_parameter_method()
    {
        $settings_con = $this->site->getSettingsCon();
        if ($settings_con) {
            echo $settings_con->account_parameter_method;
        } else {
            echo 0;
        }
    }

    public function update_products_group_prices($page = 0)
    {
        $this->data['page'] = $page;
        $this->data['cnt_price_groups'] = count($this->site->getAllPriceGroups());
        $this->data['price_groups'] = $this->site->getAllPriceGroups($page, true);
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller', false, true);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')),  array('link' => admin_url('system_settings/price_groups'), 'page' => lang('price_groups')), array('link' => '#', 'page' => lang('update_products_group_prices')));
        $meta = array('page_title' => lang('update_products_group_prices'), 'bc' => $bc);
        $this->page_construct('settings/update_products_group_prices', $meta, $this->data);
    }

    public function getProductsPricesGroups($page)
    {
        $biller_id = $this->input->get('biller_id');
        $products = $this->site->getAllProducts(FALSE, $biller_id);
        $price_groups = $this->site->getAllPriceGroups($page, true);
        $products_price_groups = $this->site->getAllProductsPriceGroups();
        $html = "";
        foreach ($products as $product) {
            $html.="<tr>
                        <th>".$product->code."</th>
                        <th>".$product->name."</th>";
            foreach ($price_groups as $pg) {
                $class = '';
                if ($pg->price_group_base == 1) {
                    $class = 'bold';
                }
                $html.='<td>
                            <input name="product_price_group['.$product->id.']['.$pg->id.']" data-productid="'.$product->id.'" data-pricegroupid="'.$pg->id.'" type="text" class="only_number form-control text-right pamount '.$class.'" '.(isset($products_price_groups[$product->id][$pg->id]) ? 'value="'.($this->sma->formatDecimals($products_price_groups[$product->id][$pg->id])).'"' : '').' style="width:100%;" '.(isset($products_price_groups[$product->id][$pg->id]) ? '' : ' ').'>
                        </td>';
            }
            $html.="</tr>";
        }
        echo $html;
    }

    public function ExportProductsPricesGroups()
    {
        $products = $this->site->getAllProducts();
        $price_groups = $this->site->getAllPriceGroups();
        $products_price_groups = $this->site->getAllProductsPriceGroups();
        $html = "";

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('product_quantity_alerts'));
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
        $letter = 'C';
        foreach ($price_groups as $pg) {
            $this->excel->getActiveSheet()->SetCellValue($letter.'1', $pg->name);
            $letter++;
        }

        $row = 2;
        foreach ($products as $product) {
            $letter = 'C';
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, $product->code);
            $this->excel->getActiveSheet()->SetCellValue('B'.$row, $product->name);

            foreach ($price_groups as $pg) {
                $this->excel->getActiveSheet()->SetCellValue($letter.$row, (isset($products_price_groups[$product->id][$pg->id]) ? $products_price_groups[$product->id][$pg->id] : 0));
                $letter++;
            }
            $row++;
        }
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $this->excel->getActiveSheet()->getStyle('C1:'.$letter.$row)->getNumberFormat()->setFormatCode("#,##0.00");

        for ($i='C'; $i <= $letter ; $i++) {
            $this->excel->getActiveSheet()->getColumnDimension($i)->setWidth(20);
        }
        // $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'product_quantity_alerts';
        $this->load->helper('excel');
        create_excel($this->excel, $filename);

        echo $html;
    }

    public function updateProductPriceGroup($pr_id, $prg_id, $price = 0)
    {
        if ($this->settings_model->update_product_price_group($pr_id, $prg_id, $price)) {
            $pg_data = $this->site->getPriceGroupByID($prg_id);
            $this->syncProductPrice($pr_id, $price, $pg_data);
            $txt_fields_changed = sprintf(lang('user_fields_changed'), $this->session->first_name." ".$this->session->last_name, lang('update_products_group_prices'), $pr_id);
            $pr_data = $this->site->getProductByID($pr_id);
            $txt_fields_changed.= lang('product')." ".$pr_data->code." ". lang('price_group')." ".$pg_data->name." = ".$this->sma->formatMoney($price);
            $this->db->insert('user_activities', [
                            'date' => date('Y-m-d H:i:s'),
                            'type_id' => 1,
                            'table_name' => 'products',
                            'record_id' => $pr_id,
                            'user_id' => $this->session->userdata('user_id'),
                            'module_name' => $this->m,
                            'description' => $txt_fields_changed,
                        ]);

            exit('1');
        }
        exit('0');
    }

    public function document_types()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('document_types')));
        $meta = array('page_title' => lang('document_types'), 'bc' => $bc);
        $this->page_construct('settings/document_types', $meta, $this->data);
    }

    public function getDocumentTypes()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, nombre, num_resolucion, sales_prefix, sales_consecutive, inicio_resolucion, fin_resolucion, factura_electronica")
            ->from("documents_types")
            ->add_column("Actions", "<div class=\"text-center\">
                                        <a href='" . admin_url('system_settings/edit_document_type/$1') . "' data-toggle='modal' data-target='#myModal' class='tip btn btn-primary new-button new-button-sm' data-toggle-second='tooltip' data-placement='top' title='" . lang("edit_document_type") . "'>
                                            <i class=\"fa fa-edit\"></i>
                                        </a>
                                        <a href='#' class='tip po btn btn-danger new-button new-button-sm' data-toggle-second='tooltip' title='". lang("delete_document_type") ."' data-content=\"<p>" . lang('r_u_sure') . "</p>
                                            <a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_document_type/$1') . "'>" . lang('i_m_sure') . "
                                            </a>
                                            <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>
                                            <i class=\"fa fa-trash-o\"></i>
                                        </a>
                                    </div>", "id");

        echo $this->datatables->generate();
    }

    public function add_document_type()
    {
        $this->sma->checkPermissions();

        $module = $this->input->post('module');
        $factura_electronica = $this->input->post('factura_electronica');
        $factura_contigencia = $this->input->post("contingency_invoice");
        $work_environment = $this->input->post("work_environment");

        $this->form_validation->set_rules('module', lang('document_type_module'), 'required');
        $this->form_validation->set_rules('nombre_doctype', lang('document_type_nombre'), 'required');
        $this->form_validation->set_rules('sales_prefix', lang('document_type_sales_prefix'), 'trim|required');
        $this->form_validation->set_rules('sales_consecutive', lang('document_type_sales_consecutive'), 'required');

        if ($module == 1 || $module == 2) {
            if ($this->input->post('save_resolution_in_sale') == 1) {
                $this->form_validation->set_rules('num_resolucion', lang('document_type_num_resolucion'), 'trim|required');
                $this->form_validation->set_rules('palabra_resolucion', lang('document_type_palabra_resolucion'), 'trim|required');
                $this->form_validation->set_rules('inicio_resolucion', lang('document_type_inicio_resolucion'), 'trim|required');
                $this->form_validation->set_rules('fin_resolucion', lang('document_type_fin_resolucion'), 'trim|required');
                $this->form_validation->set_rules('emision_resolucion', lang('document_type_emision_resolucion'), 'trim|required');
                $this->form_validation->set_rules('vencimiento_resolucion', lang('document_type_vencimiento_resolucion'), 'trim|required');
            }

            if ($this->Settings->electronic_billing == 1 && $factura_electronica == 1) {
                $this->form_validation->set_rules('factura_electronica', lang('document_type_factura_electronica'), 'required');
                $this->form_validation->set_rules('work_environment', lang('document_type_work_enviroment'), 'required');

                if ($factura_contigencia == 0) {
                    if ($this->Settings->fe_technology_provider == CADENA) {
                        $this->form_validation->set_rules('clave_tecnica', lang('document_type_technical_key'), 'trim|required');
                    }
                }

                if ($work_environment == TEST) {
                    if ($this->Settings->fe_technology_provider == CADENA) {
                        $this->form_validation->set_rules('testid', lang('document_type_testid'), 'trim|required');
                    }
                }
            }
        }

        if ($this->Settings->fe_technology_provider == BPM  && $factura_electronica == YES) {
            if (in_array($module, ['1','2','3','4','26','27','35','52'])) {
                $this->form_validation->set_rules('instance', lang('instance'), 'required');
            }
        }

        if ($this->Settings->fe_technology_provider == DELCOP) {
            if ($factura_electronica == 1) {
                if (!($module == 48 || $module == 49 || $module == 50 || $module == 51)) {
                    $this->form_validation->set_rules('transaction_id', lang('document_type_transaction_id'), 'required');
                }
            }
        }

        if ($this->Settings->fe_technology_provider == SIMBA  && $factura_electronica == YES) {
            if (in_array($module, ['1','2','3','4','26','27'])) {
                $this->form_validation->set_rules('mode', lang('mode'), 'required');
            }
        }

        if ($this->form_validation->run() == TRUE) {
            $id = $this->input->post('id');
            $nombre = $this->input->post('nombre_doctype');
            $num_resolucion = $this->input->post('num_resolucion');
            $palabra_resolucion = $this->input->post('palabra_resolucion');
            $sales_prefix = $this->input->post('sales_prefix');
            $sales_consecutive = $this->input->post('sales_consecutive');
            $inicio_resolucion = $this->input->post('inicio_resolucion');
            $fin_resolucion = $this->input->post('fin_resolucion');
            $emision_resolucion = $this->input->post('emision_resolucion');
            $vencimiento_resolucion = $this->input->post('vencimiento_resolucion');
            $clave_tecnica = $this->input->post("clave_tecnica");
            $invoice_footer = $this->input->post('invoice_footer');
            $module_invoice_format_id = $this->input->post('module_invoice_format_id');
            $word_type_sale = $this->input->post('word_type_sale');
            $testid = $this->input->post("testid");
            $fe_transaction_id = $this->input->post("transaction_id");
            $save_resolution_in_sale = $this->input->post("save_resolution_in_sale");
            $invoice_header = $this->input->post("invoice_header");
            $add_consecutive_left_zeros = $this->input->post("add_consecutive_left_zeros");
            $quick_print_format_id = $this->input->post('quick_print_format_id');
            $instance = $this->input->post('instance');
            $language = $this->input->post('language');
            $inicio_resolucion_wappsi = $this->input->post('inicio_resolucion_wappsi') ?? NULL;
            $mode = $this->input->post('mode') ?? 0;
            $highlighted_text = $this->input->post("highlighted_text");

            $data = [
                'module' => $module,
                'nombre' => $nombre,
                'factura_electronica' => $factura_electronica,
                "mode" => $mode,
                "factura_contingencia" => $factura_contigencia,
                'num_resolucion' => $num_resolucion,
                'inicio_resolucion_wappsi' => $inicio_resolucion_wappsi,
                'palabra_resolucion' => $palabra_resolucion,
                'sales_prefix' => $sales_prefix,
                'sales_consecutive' => $sales_consecutive,
                'inicio_resolucion' => $inicio_resolucion,
                'fin_resolucion' => $fin_resolucion,
                'emision_resolucion' => $emision_resolucion,
                'vencimiento_resolucion' => $vencimiento_resolucion,
                "clave_tecnica" => $clave_tecnica,
                "fe_work_environment" => $work_environment,
                "fe_testid" => $testid,
                'invoice_footer' => $invoice_footer,
                'module_invoice_format_id' => $module_invoice_format_id,
                'word_type_sale' => $word_type_sale,
                "fe_transaction_id" => $fe_transaction_id,
                "save_resolution_in_sale" => $save_resolution_in_sale,
                "invoice_header" => $invoice_header,
                "add_consecutive_left_zeros" => $add_consecutive_left_zeros,
                "quick_print_format_id" => $quick_print_format_id,
                "language" => $language,
                "electronic_billing_instance_id" => $instance,
                "highlighted_text" => $highlighted_text,
            ];


            if (in_array($module, $this->not_accounting)) {
                $data['not_accounting'] = 1;
            }

            if ($this->validateInstance() == false) {
                $this->session->set_flashdata('error', 'La instancia ya tiene asignado el módulo seleccionado. Por favor seleccione otra instancia.');
                admin_redirect("system_settings/document_types");
            }

        } elseif ($this->input->post('add')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/document_types");
        }

        if ($this->form_validation->run() == TRUE && $this->settings_model->add_document_type($data)) {
            $this->session->set_flashdata('message', lang("document_type_added"));
            admin_redirect("system_settings/document_types");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['documents_types_js'] = $this->site->documents_types_js();

            if ($this->Settings->fe_technology_provider == BPM) {
                $this->data['instances'] = $this->ElectronicBillingInstance_model->getAll();
            }

            $this->load_view($this->theme . 'settings/add_document_type', $this->data);
        }
    }

    public function edit_document_type($id = NULL)
    {
        $this->sma->checkPermissions();

        $module = $this->input->post('module');
        $factura_electronica = $this->input->post('factura_electronica');
        $factura_contigencia = $this->input->post('contingency_invoice');
        $work_environment = $this->input->post("work_environment");

        $this->form_validation->set_rules('module', lang('document_type_module'), 'required');
        $this->form_validation->set_rules('nombre_doctype', lang('document_type_nombre'), 'trim|required');
        $this->form_validation->set_rules('sales_prefix', lang('document_type_sales_prefix'), 'required');
        $module = $this->input->post('module');

        if ($module == 1 || $module == 2) {
            if ($this->input->post('save_resolution_in_sale') == 1) {
                $this->form_validation->set_rules('num_resolucion', lang('document_type_num_resolucion'), 'trim|required');
                $this->form_validation->set_rules('palabra_resolucion', lang('document_type_palabra_resolucion'), 'trim|required');
                $this->form_validation->set_rules('inicio_resolucion', lang('document_type_inicio_resolucion'), 'trim|required');
                $this->form_validation->set_rules('fin_resolucion', lang('document_type_fin_resolucion'), 'trim|required');
                $this->form_validation->set_rules('emision_resolucion', lang('document_type_emision_resolucion'), 'trim|required');
                $this->form_validation->set_rules('vencimiento_resolucion', lang('document_type_vencimiento_resolucion'), 'trim|required');
            }

            if ($this->Settings->electronic_billing == 1 && $factura_electronica == 1) {
                $this->form_validation->set_rules('factura_electronica', lang('document_type_factura_electronica'), 'required');
                $this->form_validation->set_rules('work_environment', lang('document_type_work_enviroment'), 'required');

                if ($factura_contigencia == 0) {
                    if ($this->Settings->fe_technology_provider == CADENA) {
                        $this->form_validation->set_rules('clave_tecnica', lang('document_type_technical_key'), 'trim|required');
                    }
                }

                if ($work_environment == TEST) {
                    if ($this->Settings->fe_technology_provider == CADENA) {
                        $this->form_validation->set_rules('testid', lang('document_type_testid'), 'trim|required');
                    }
                }
            }
        }

        if ($this->Settings->fe_technology_provider == BPM  && $factura_electronica == YES) {
            if (in_array($module, ['1','2','3','4','26','27','35','52'])) {
                $this->form_validation->set_rules('instance', lang('instance'), 'required');
            }
        }

        if ($this->Settings->fe_technology_provider == DELCOP) {
            if ($factura_electronica == 1) {
                if ($module == 48 || $module == 49 || $module == 50 || $module == 51) {
                } else {
                    $this->form_validation->set_rules('transaction_id', lang('document_type_transaction_id'), 'required');
                }
            }
        }

        if ($this->Settings->fe_technology_provider == SIMBA  && $factura_electronica == YES) {
            if (in_array($module, ['1','2','3','4','26','27'])) {
                $this->form_validation->set_rules('mode', lang('mode'), 'required');
            }
        }

        if ($this->form_validation->run() == TRUE) {
            $id = $this->input->post('id');
            $nombre = $this->input->post('nombre_doctype');
            $num_resolucion = $this->input->post('num_resolucion');
            $palabra_resolucion = $this->input->post('palabra_resolucion');
            $sales_prefix = $this->input->post('sales_prefix');
            $inicio_resolucion = $this->input->post('inicio_resolucion');
            $fin_resolucion = $this->input->post('fin_resolucion');
            $emision_resolucion = $this->input->post('emision_resolucion');
            $vencimiento_resolucion = $this->input->post('vencimiento_resolucion');
            $clave_tecnica = $this->input->post("clave_tecnica");
            $invoice_footer = $this->input->post('invoice_footer');
            $module_invoice_format_id = $this->input->post('module_invoice_format_id');
            $word_type_sale = $this->input->post('word_type_sale');
            $testid = $this->input->post("testid");
            $fe_transaction_id = $this->input->post("transaction_id");
            $save_resolution_in_sale = $this->input->post("save_resolution_in_sale");
            $prev_sales_prefix = $this->input->post("prev_sales_prefix");
            $invoice_header = $this->input->post("invoice_header");
            $add_consecutive_left_zeros = $this->input->post("add_consecutive_left_zeros");
            $invoice_format_logo = $this->input->post("invoice_format_logo");
            $quick_print_format_id = $this->input->post('quick_print_format_id');
            $language = $this->input->post('language');
            //invoice_format
            $print_copies = $this->input->post('print_copies');
            $copies_watermark = $this->input->post('copies_watermark');
            $instance = $this->input->post('instance');
            $inicio_resolucion_wappsi = $this->input->post('inicio_resolucion_wappsi') ?? NULL;
            $mode = $this->input->post('mode') ?? 0;
            $version = $this->input->post("version");
            $version_date = $this->input->post("version_date");
            $visible = $this->input->post("visible") ? 1 : 0;
            $highlighted_text = $this->input->post("highlighted_text");

            $inv_format  = [
                'print_copies' => $print_copies,
                'copies_watermark' => $copies_watermark,
                'logo' => $invoice_format_logo,
                "version" => $version,
                "version_date" => $version_date,
                "visible" => $visible,
            ];
            $data = [
                'module' => $module,
                'nombre' => $nombre,
                'factura_electronica' => $factura_electronica,
                "mode" => $mode,
                'factura_contingencia' => $factura_contigencia,
                'num_resolucion' => $num_resolucion,
                'palabra_resolucion' => $palabra_resolucion,
                'sales_prefix' => $sales_prefix,
                'inicio_resolucion' => $inicio_resolucion,
                'fin_resolucion' => $fin_resolucion,
                'inicio_resolucion_wappsi' => $inicio_resolucion_wappsi,
                'emision_resolucion' => $emision_resolucion,
                'vencimiento_resolucion' => $vencimiento_resolucion,
                "clave_tecnica" => $clave_tecnica,
                "fe_work_environment" => $work_environment,
                "fe_testid" => $testid,
                'invoice_footer' => $invoice_footer,
                'module_invoice_format_id' => $module_invoice_format_id,
                'word_type_sale' => $word_type_sale,
                "fe_transaction_id" => $fe_transaction_id,
                "save_resolution_in_sale" => $save_resolution_in_sale,
                "invoice_header" => $invoice_header,
                "add_consecutive_left_zeros" => $add_consecutive_left_zeros,
                'quick_print_format_id' => $quick_print_format_id,
                'language' => $language,
                "electronic_billing_instance_id" => $instance,
                "highlighted_text" => $highlighted_text,
            ];

            if (in_array($module, $this->not_accounting)) {
                $data['not_accounting'] = 1;
            }


            if ($this->validateInstance() == false) {
                $this->session->set_flashdata('error', 'La instancia ya tiene asignado el módulo seleccionado. Por favor seleccione otra instancia.');
                admin_redirect("system_settings/document_types");
            }
        } elseif ($this->input->post('id')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/document_types");
        }

        if ($this->form_validation->run() == TRUE && $this->settings_model->update_document_type($id, $data, $prev_sales_prefix, $inv_format)) {
            $this->session->set_flashdata('message', lang("document_type_updated"));
            admin_redirect("system_settings/document_types");
        } else {
            $document_type = $this->site->getDocumentTypeById($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['documents_types_js'] = $this->site->documents_types_js();
            $this->data['document_type'] = $document_type;

            if ($this->Settings->fe_technology_provider == BPM) {
                $this->data['instances'] = $this->ElectronicBillingInstance_model->getAll();
            }

            $this->load_view($this->theme . 'settings/edit_document_type', $this->data);
        }
    }

    public function add_document_type_automatic()
    {
        $resolution_data_WS = $this->getResolutionsWS();
        $resolutions_data = $this->site->getDocumentTypeById();
        if ($resolution_data_WS->statusCode == 200) {
            $resolution_data_create = $this->build_resolution_data_create($resolution_data_WS, $resolutions_data);
            if (!empty($resolution_data_create)) {
                $document_types_added = $this->settings_model->add_document_types($resolution_data_create);

                if (!empty($document_types_added)) {
                    $this->session->set_flashdata("message", lang("document_types_added_successfully"));
                } else {
                    $this->session->set_flashdata("message", lang("document_types_already_updated"));
                }
            } else  {
                $this->session->set_flashdata("message", lang("document_types_already_updated"));
            }
            admin_redirect("system_settings/document_types");
        } else {
            $this->session->set_flashdata('error', $resolution_data_WS->errorCause);
            admin_redirect("system_settings/document_types");
        }
    }

    public function getResolutionsWS()
    {
        if ($this->Settings->fe_technology_provider == BPM) {
            $resolutionData = $this->bpmWsGetResolutions();
        } else if ($this->Settings->fe_technology_provider == CADENA || $this->Settings->fe_technology_provider == DELCOP) {
            $resolutionData = $this->cadenaWsGetResolutions();
        }

        echo json_encode($resolutionData);
    }

    public function bpmWsGetResolutions()
    {
        $resolutionData = [];
        $module = $this->input->post('module');
        $resolutionNumber = $this->input->post('resolutionNumber');

        $technologyProviderConfiguration = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

        if ($module == FACTURA_POS || $module == FACTURA_DETAL) {
            $tipoDocumento = 'FA';
        } elseif ($module == NOTA_CREDITO_POS || $module == NOTA_CREDITO_DETAL) {
            $tipoDocumento = 'NC';
        } elseif ($module == NOTA_DEBITO_POS || $module == NOTA_DEBITO_DETAL) {
            $tipoDocumento = 'ND';
        }

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $technologyProviderConfiguration->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "Controller": "Documentos",
                "Action" : "GetNumeracion",
                "ParamValues" : {
                    "TokenIdentificador": "'. $technologyProviderConfiguration->security_token .'",
                    "TipoDocumento": "'. $tipoDocumento .'"
                }
            }',
            CURLOPT_HTTPHEADER => [ 'Content-Type: application/json' ],
        ]);

        $response = json_decode(curl_exec($curl));

        curl_close($curl);

        $resolutionsList = $response->Data->DataResult;
        foreach ($resolutionsList as $resolution) {
            if ($resolution->NumeroResolucion == $resolutionNumber) {
                $resolutionData['number'] = $resolution->NumeroResolucion;
                $resolutionData['prefix'] = $resolution->Prefijo;
                $resolutionData['fromNumber'] = $resolution->RangoInicial;
                $resolutionData['toNumber'] = $resolution->RangoFinal;
                $fechaInicial = date_create($resolution->VigenciaFechaInicial);
                $resolutionData['validDateTimeFrom'] = $fechaInicial->format('Y-m-d');
                $fechaFinal = date_create($resolution->VigenciaFechaFinal);
                $resolutionData['validDateTimeTo'] = $fechaFinal->format('Y-m-d');
                break;
            }
        }

        return $resolutionData;
    }

    public function cadenaWsGetResolutions()
    {
        $resolutionData = '';
        $resolutionNumber = $this->input->post('resolutionNumber');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apivp.efacturacadena.com/v1/vp/consulta/rango-numeracion?softwareCode=49fab599-4556-4828-a30b-852a910c5bb1&accountCodeVendor=890930534&accountCode=". $this->Settings->numero_documento,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($response);

        $resolutionsList = $response->numberingRangelist;

        foreach ($resolutionsList as $resolution) {
            if ($resolution->resolutionNumber == $resolutionNumber) {
                $resolutionData = $resolution;
                break;
            }
        }

        return  $resolutionData;
    }

    private function build_resolution_data_create($resolution_data_WS = [], $resolutions_data = [])
    {
        $resolution_data_create = [];

        foreach ($resolution_data_WS->numberingRangelist as $data_WS) {
            $existing_resolution_number = $this->settings_model->existing_document_type_by_resolution_number($data_WS->resolutionNumber);

            if ($existing_resolution_number === FALSE) {
                $existing_prefix = $this->settings_model->existing_document_type_by_prefix($data_WS->prefix);

                $resolution_data_create[] = [
                    "module" => 2,
                    "nombre" => "Factura de Venta",
                    "factura_electronica" => 1,
                    "factura_contingencia" => 0,
                    "num_resolucion" => $data_WS->resolutionNumber,
                    "palabra_resolucion" => ($existing_prefix === TRUE) ? 1 : 2,
                    "sales_prefix" => $data_WS->prefix,
                    "sales_consecutive" => 1,
                    "inicio_resolucion" => $data_WS->fromNumber,
                    "fin_resolucion" => $data_WS->toNumber,
                    "emision_resolucion" => $data_WS->validDateTimeFrom,
                    "vencimiento_resolucion" => $data_WS->validDateTimeTo,
                    "clave_tecnica" => $data_WS->technicalKey,
                    "fe_work_environment" => 0,
                    "word_type_sale" => "Facturación Electrónica de Venta"
                ];

                break;
            }
        }

        return $resolution_data_create;
    }

    public function get_invoice_formats($module)
    {
        $invoice_formats = $this->site->getInvoiceFormats($module);

        $options = $quick_print_options = '<option value="NULL">'.lang('invoice_format_default').'</option>';
        if ($invoice_formats) {
            foreach ($invoice_formats as $inv_form) {
                $options .= '<option value="'.$inv_form->id.'">'.$inv_form->format_name.' ('.$inv_form->format_url.')</option>';

                if ($inv_form->quick_print == YES){
                    $quick_print_options .= '<option value="'.$inv_form->id.'">'.$inv_form->format_name.' ('.$inv_form->format_url.')</option>';
                }
            }
        }

        echo json_encode([
            'options'=>$options,
            'quick_print_options'=>$quick_print_options
        ]);
    }

    public function delete_document_type($id)
    {

        if ($this->settings_model->get_type_document_movements($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("document_type_has_movements")));
        }

        if ($this->settings_model->delete_document_type($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("document_type_deleted")));
        }
    }

    public function close_year($action = NULL)
    {
        if ($action == NULL) {
            if ($this->Settings->years_database_management == 1) {
                $this->session->set_flashdata('error', 'No se puede acceder a este módulo');
                admin_redirect('system_settings');
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('close_year')));
            $meta = array('page_title' => lang('close_year'), 'bc' => $bc);
            $this->data['warehouses'] = $this->site->getAllWarehouses(null, false);
            $this->data['products_with_negative_quantitys'] = $this->settings_model->get_product_negative_quantity();
            $this->data['open_pos_registers'] = $this->settings_model->get_open_pos_registers();
            $this->page_construct('settings/close_year', $meta, $this->data);
        } else {
            if ($action == 'close_year') {
                echo json_encode($this->settings_model->create_new_year_database());
            } else if ($action == 'inventory_transfer') {
                $step_1 = $this->settings_model->transfer_inventory_to_new_year();
                if (!$step_1['error']) {
                    $step_2 = $this->settings_model->transfer_inventory_with_variants_to_new_year($step_1['cnt_reference']);
                    echo json_encode($step_2);
                } else {
                    echo json_encode($step_1);
                }
            } else if ($action == 'cxc_transfer') {
                echo json_encode($this->settings_model->transfer_cxc());
            } else if ($action == 'cxp_transfer') {
                echo json_encode($this->settings_model->transfer_cxp());
            } else if ($action == 'deposit_transfer') {
                echo json_encode($this->settings_model->deposit_transfer());
            }
        }
    }

    public function update_products_unit_prices($page = 0)
    {
        $this->data['cnt_units'] = count($this->site->get_all_units());
        $this->data['units'] = $this->site->get_all_units($page, true);
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['page'] = $page;
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')),  array('link' => admin_url('system_settings/price_groups'), 'page' => lang('price_groups')), array('link' => '#', 'page' => lang('update_products_unit_prices')));
        $meta = array('page_title' => lang('update_products_unit_prices'), 'bc' => $bc);
        $this->page_construct('settings/update_products_unit_prices', $meta, $this->data);
    }

    public function getProductsUnitPrices($page = 0)
    {
        $biller_id = $this->input->get('biller_id');
        $products = $this->site->getAllProducts(FALSE, $biller_id);
        $units = $this->site->get_all_units($page, true);
        $products_units = $this->site->getAllProductsUnitPrices($page);
        $html = "";
        foreach ($products as $product) {
            $html.="<tr>
                        <th>".$product->code."</th>
                        <th>".$product->name."</th>";
            foreach ($units as $pg) {
                $class='';
                if ($pg->id == $product->unit) {
                    $class = 'bold';
                }

                $html.='<td>
                            <input name="product_unit_price['.$product->id.']['.$pg->id.']" data-productid="'.$product->id.'" data-unitpriceid="'.$pg->id.'" type="text" class="only_number form-control text-right pamount '.$class.'" '.(isset($products_units[$product->id][$pg->id]) ? 'value="'.($this->sma->formatDecimals($products_units[$product->id][$pg->id])).'"' : '').' style="width:100%;" '.(isset($products_units[$product->id][$pg->id]) ? '' : 'readonly').'>
                        </td>';
            }
            $html.="</tr>";
        }

        echo $html;
    }

    public function updateProductUnitPrice($pr_id, $prg_id, $price)
    {
        if ($this->settings_model->update_product_unit_price($pr_id, $prg_id, $price)) {
            $txt_fields_changed = sprintf(lang('user_fields_changed'), $this->session->first_name." ".$this->session->last_name, lang('update_products_unit_prices'), $pr_id);
            $pg_data = $this->site->getUnitByID($prg_id);
            $pr_data = $this->site->getProductByID($pr_id);
            $txt_fields_changed.= lang('product')." ".$pr_data->code." ". lang('unit')." ".$pg_data->name." = ".$this->sma->formatMoney($price);
            $this->db->insert('user_activities', [
                            'date' => date('Y-m-d H:i:s'),
                            'type_id' => 1,
                            'table_name' => 'products',
                            'record_id' => $pr_id,
                            'user_id' => $this->session->userdata('user_id'),
                            'module_name' => $this->m,
                            'description' => $txt_fields_changed,
                        ]);
            exit('1');
        }
        exit('0');
    }

    public function view_console()
    {
        $this_ip = $_SERVER['REMOTE_ADDR'];
        $salidas = shell_exec('netstat -lt');
        exit(var_dump($salidas."</br>".$this_ip));
    }

    public function validate_new_unit_name($unit_name){
        if ($this->settings_model->get_unit_by_name($unit_name)) {
            exit(false);
        }
        exit(true);
    }

    public function update_price_margin()
    {

        $this->form_validation->set_rules('cost_type', lang("cost_type"), 'required');
        $this->form_validation->set_rules('price_group_type', lang("price_group_type"), 'required');
        // $this->form_validation->set_rules('category[]', lang("categories"), 'required');
        // $this->form_validation->set_rules('subcategory[]', lang("subcategories"), 'required');

        if ($this->form_validation->run() == true) {

            if (!$this->input->post('category[]') && !$this->input->post('subcategory[]')) {
                $this->session->set_flashdata('error', 'Seleccione al menos una categoría o subcategoría');
                admin_redirect('system_settings/update_price_margin');
            }
            $price_rounding = $this->input->post('price_rounding');
            $cost_type = $this->input->post('cost_type');
            $price_group_type = $this->input->post('price_group_type');
            $categories = $this->input->post('category[]');
            $subcategories_inp = $this->input->post('subcategory[]');
            $subcategories = null;
            if ($subcategories_inp) {
                foreach ($categories as $key => $category_id) {
                    if (isset($subcategories_inp[$category_id])) {
                        unset($categories[$key]);
                    }
                }
                foreach ($subcategories_inp as $key => $cat_arr) {
                    foreach ($cat_arr as $subcat) {
                        $subcategories[] = $subcat;
                    }
                }
            }
        }

        if ($this->form_validation->run() == true && $this->settings_model->update_prices_margins($cost_type, $price_group_type, $price_rounding, $categories, $subcategories)) {
                $this->session->set_flashdata('message', 'Actualizado correctamente');
            admin_redirect('system_settings/update_price_margin');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $categories = $this->site->getAllCategories();
            $subcategories = false;
            if ($categories) {
                foreach ($categories as $cat) {
                    $subcategories[$cat->id] = $this->site->getSubCategories($cat->id);
                }
            }

            $this->data['categories'] = $categories;
            $this->data['subcategories'] = $subcategories;

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')),  array('link' => admin_url('system_settings/update_price_margin'), 'page' => lang('price_groups')), array('link' => '#', 'page' => lang('update_price_margin')));
            $meta = array('page_title' => lang('update_price_margin'), 'bc' => $bc);
            $this->page_construct('settings/update_price_margin', $meta, $this->data);
        }

    }

    public function validate_document_type_prefix($prefix)
    {
        $validate = $this->settings_model->validate_document_type_prefix($prefix);
        echo json_encode(['response' => $validate]);
    }

    public function update_products_hybrid_prices($page = 0)
    {
        $this->data['page'] = $page;
        $this->data['cnt_price_groups'] = count($this->site->getAllPriceGroups());
        $this->data['price_groups'] = $this->site->getAllPriceGroups($page, true);
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')),  array('link' => admin_url('system_settings/price_groups'), 'page' => lang('price_groups')), array('link' => '#', 'page' => lang('update_products_hybrid_prices')));
        $meta = array('page_title' => lang('update_products_hybrid_prices'), 'bc' => $bc);
        $this->page_construct('settings/update_products_hybrid_prices', $meta, $this->data);
    }

    public function getProductsHybridPrices($page = 0)
    {
        $products = $this->site->getAllProducts();
        $units = $this->site->get_all_units($page, false);
        $price_groups = $this->site->getAllPriceGroups($page, false);
        $products_units = $this->site->get_all_product_hybrid_prices();
        $html = "";
        foreach ($products as $product) {
            foreach ($units as $unit) {
                if (!isset($products_units[$product->id][$unit->id])) {
                    continue;
                }
                $html.="<tr>
                            <th>".$product->code."</th>
                            <th>".$product->name."</th>
                            <th>".$unit->name."</th>";
                foreach ($price_groups as $pg) {
                    $class='';

                    $html.='<td>
                                <input
                                    name="product_unit_price['.$product->id.']['.$pg->id.']"
                                    data-productid="'.$product->id.'"
                                    data-unitpriceid="'.$unit->id.'"
                                    data-pricegroupid="'.$pg->id.'"
                                    type="text"
                                    class="only_number form-control text-right pamount '.$class.'"
                                    '.(isset($products_units[$product->id][$unit->id][$pg->id]) ? 'value="'.($this->sma->formatDecimal($products_units[$product->id][$unit->id][$pg->id])).'"' : '').'
                                style="width:100%;">
                            </td>';
                }
                $html.="</tr>";
            }
        }

        echo $html;
    }

    public function updateProductHybridPrice($pr_id, $prg_id, $unit_id, $price)
    {
        $product = $this->site->getProductByID($pr_id);
        $unit_data = $this->site->getUnitByID($unit_id);
        $prev_price = $this->site->getProductUnitPrice($pr_id, $unit_id, $prg_id);
        $ppv = $prev_price ? $prev_price->valor_unitario : $product->price;
        if ($this->site->update_product_hybrid_price($pr_id, $prg_id, $unit_id, $price)) {
            if ($ppv != $price) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 1,
                    'table_name' => 'unit_prices',
                    'record_id' => $pr_id,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => 'update_products_hybrid_prices',
                    'description' => $this->session->first_name." ".$this->session->last_name." Cambió el precio del producto ".$product->name." para la unidad ".$unit_data->name." de ".$this->sma->formatMoney($ppv)." a ".$this->sma->formatMoney($price),
                ]);
            }
            exit('1');
        }
        exit('0');
    }

    public function product_preferences()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('settings/product_preferences', ['page_title' => lang('product_preferences')], $this->data);
    }

    public function getProductPreferencesCategories()
    {
        $this->sma->checkPermissions();

        $this->load->library('datatables');
        $this->datatables
            ->select("preferences_categories.id, preferences_categories.name, preferences.name as preference_name")
            ->from("preferences")
            ->join("preferences_categories", 'preferences_categories.id = preferences.category_id')
            ->add_column("Actions", '<div class="btn-group text-left">
                <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                    <i class="fas fa-ellipsis-v fa-lg"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="' . admin_url('system_settings/edit_product_preference_category/$1') .'" data-toggle="modal" data-target="#myModal" class="tip">
                            <i class="fa fa-edit"></i> '. lang("edit_expense_category") .'
                        </a>
                    </li>
                </ul>
            </div>', "preferences_categories.id");

        echo $this->datatables->generate();
    }

    public function add_product_preference_category()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('preferences_name[]', lang("enter_preferences"), 'required');
        if ($this->form_validation->run() == true) {
            $name = $this->input->post('name');
            $preferences = $this->input->post('preferences_name');
        } elseif ($this->input->post('add_product_preference_category')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/product_preferences");
        }

        if ($this->form_validation->run() == true && $preferenceCategoryId = $this->settings_model->addPreferencesCategory($name, $preferences)) {
            $this->session->set_flashdata('message', lang("expense_category_added"));
            admin_redirect("system_settings/product_preferences");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/add_product_preference_category', $this->data);
        }
    }

    public function edit_product_preference_category($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('name', lang("category_name"), 'required|min_length[3]');
        $this->form_validation->set_rules('preferences_name[]', lang("enter_preferences"), 'required');
        $category = $this->settings_model->getProductPreferenceCategory($id);
        $category_preferences = $this->settings_model->getCategoryPreferences($id);
        if ($this->form_validation->run() == true) {
            $name = $this->input->post('name');
            $selection_limit = $this->input->post('selection_limit');
            $required = $this->input->post('required');
            $preferences = $this->input->post('preferences_name');
            $preferences_id = $this->input->post('preferences_id');
            $delete_preferences = $this->input->post('delete_preference_id');

            $data = [
                        'name' => $name,
                        'selection_limit' => $selection_limit,
                        'required' => $required,
                        'preferences' => $preferences,
                        'preferences_id' => $preferences_id,
                        'delete_preferences' => $delete_preferences,
                    ];

        } elseif ($this->input->post('edit_product_preference_category')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/product_preferences");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updatePreferencesCategory($id, $data)) {
            $this->session->set_flashdata('message', lang("expense_category_updated"));
            admin_redirect("system_settings/product_preferences");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['category'] = $category;
            $this->data['category_preferences'] = $category_preferences;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_product_preference_category', $this->data);
        }
    }

    public function ubication()
    {
        $this->sma->checkPermissions();

        $country = $this->input->post("countryId");
        $state = $this->input->post("stateId");
        $status = $this->input->post("status");
        $this->data["statusFilter"] = 0;
        
        $this->data["countries"] = $this->settings_model->getAllCountries();
        if (!empty($country)) {
            $this->data["states"] = $this->settings_model->getAllStates(["PAIS" => $country]);
            if (!empty($state)) {
                $this->data["cities"] = $this->settings_model->getAllCities(["CODDEPARTAMENTO" => $state]);
            }
        }

        if ($status != '') {
            $this->data["statusFilter"] = 1;
        }

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('settings/ubication', ['page_title' => lang('ubication')], $this->data);
    }

    public function getCountries()
    {
        $this->sma->checkPermissions();

        $synchronized = $this->input->post("synchronized");
        $countryId = $this->input->post("countryId");
        $stateId = $this->input->post("stateId");
        $cityId = $this->input->post("cityId");
        $status = $this->input->post("status");

        $data = [
            "synchronized" => $synchronized,
            "countryId" => $countryId,
            "stateId" => $stateId,
            "cityId" => $cityId,
            "status" => $status
        ];

        echo $this->settings_model->get_data_for_list_ubication($data);
    }

    public function add_ubication($type)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['countries'] = $this->companies_model->getCountries();
        $this->data['currencies'] = $this->site->getAllCurrencies();
        $this->data['type'] = $type;
        $this->data['modal_js'] = $this->site->modal_js();

        $this->load_view($this->theme . 'settings/add_ubication', $this->data);
    }
    
    public function add_ubication_ajax($type)
    {
        $SettingsModel = new Settings_model();
        $data = [
            'postal_code' => $this->input->get('postal_code'),
            'name' => $this->input->get('name'),
            'country' => $this->input->get('country_code'),
            'state' => $this->input->get('state_code'),
            'city' => $this->input->get('city_code'),
            'zone' => $this->input->get('zone_code'),
            'codigo_iso' => $this->input->get('codigo_iso'),
            'currency' => $this->input->get('currency'),
        ];
        if ($type == 3) {
            $data['postal_code'] = $this->input->get('state_code').$this->input->get('postal_code');
        }

        $ubicationCreated = $SettingsModel->add_ubication($type, $data);
        if ($ubicationCreated == true) {
            $response = ["success" => 1];

            $synchronized = $this->syncUbication($type, $data);
            if ($synchronized == false) {
                $response["synchronized"] = $synchronized;
            }

            echo json_encode($response);
        } else {
            echo json_encode(['success'=>0]);
        }
    }

    public function edit_ubication($type, $id)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('postal_code', lang("postal_code"), 'required');
        $this->form_validation->set_rules('name', lang("name"), 'required');

        if ($this->form_validation->run() == true) {
            $data = [
                'postal_code' => $this->input->post('postal_code'),
                'name' => $this->input->post('name'),
                'country' => $this->input->post('country_code'),
                'state' => $this->input->post('state_code'),
                'city' => $this->input->post('city_code'),
                'zone' => $this->input->post('zone_code'),
                'codigo_iso' => $this->input->post('codigo_iso'),
                'currency' => $this->input->post('currency'),
            ];
        } elseif ($this->input->post('add_ubication')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/ubication");
        }

        if ($this->form_validation->run() == true && $this->settings_model->edit_ubication($type, $data)) {
            $responseSynchronized = "";

            $ubicationSynchronized = $this->syncUbication($type, $data, "edit");
            if ($ubicationSynchronized == FALSE) {
                $responseSynchronized = " No fue posible sincronizar en Tienda Pro";
            }

            $this->session->set_flashdata('message', lang("ubication_updated") . $responseSynchronized);
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $country = null;
            $state = null;
            $city = null;
            $zone = null;
            $subzone = null;

            if ($type == 1) {
                $country = $this->site->get_country_by_id($id);

                $this->data['postal_code'] = $country->CODIGO;
                $this->data['name'] = $country->NOMBRE;
                $this->data['codigo_iso'] = $country->codigo_iso;
                $this->data['MONEDA'] = $country->MONEDA;
            } else if ($type == 2) {
                $state = $this->site->get_state_by_id($id);
                $country = $this->site->get_country_by_id($state->PAIS);

                $this->data['postal_code'] = $state->CODDEPARTAMENTO;
                $this->data['name'] = $state->DEPARTAMENTO;
            } else if ($type == 3) {
                $city = $this->site->get_city_by_id($id);
                $state = $this->site->get_state_by_id($city->CODDEPARTAMENTO);
                $country = $this->site->get_country_by_id($city->PAIS);

                $this->data['postal_code'] = $city->CODIGO;
                $this->data['name'] = $city->DESCRIPCION;
            } else if ($type == 4) {
                $zone = $this->site->get_zone_by_id($id);
                $city = $this->site->get_city_by_id($zone->codigo_ciudad);
                $state = $this->site->get_state_by_id($city->CODDEPARTAMENTO);
                $country = $this->site->get_country_by_id($city->PAIS);

                $this->data['postal_code'] = $zone->zone_code;
                $this->data['name'] = $zone->zone_name;
            } else if ($type == 5) {
                $subzone = $this->site->get_subzone_by_id($id);
                $zone = $this->site->get_zone_by_id($subzone->zone_code);
                $city = $this->site->get_city_by_id($zone->codigo_ciudad);
                $state = $this->site->get_state_by_id($city->CODDEPARTAMENTO);
                $country = $this->site->get_country_by_id($city->PAIS);

                $this->data['postal_code'] = $subzone->subzone_code;
                $this->data['name'] = $subzone->subzone_name;
            }

            $this->data['country'] = $country;
            $this->data['state'] = $state;
            $this->data['city'] = $city;
            $this->data['zone'] = $zone;
            $this->data['subzone'] = $subzone;

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['type'] = $type;
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'settings/edit_ubication', $this->data);
        }
    }

    public function delete_ubication($type, $id)
    {
        if (!$this->existsUbicationsInCompanies($type, $id)) {
            $response = false;

            if ($type == 5) {
                $response = $this->settings_model->deleteSubzone(["subzone_code" => $id]);
            } else if ($type == 4) {
                if ($this->settings_model->deleteSubzone(["zone_code" => $id])) {
                    $response = $this->settings_model->deleteZone(["zone_code" => $id]);
                }
            } else if ($type == 3) {
                if ($this->settings_model->deleteSubzone(["city_code" => $id])) {
                    if ($this->settings_model->deleteZone(["codigo_ciudad" => $id])) {
                        $response = $this->settings_model->deleteCity(["CODIGO" => $id]);
                    }
                }
            } else if ($type == 2) {
                if ($this->settings_model->deleteSubzone(["state_code" => $id])) {
                    if ($this->settings_model->deleteZone(["state_code" => $id])) {
                        if ($this->settings_model->deleteCity(["CODDEPARTAMENTO" => $id])) {
                            $response = $this->settings_model->deleteState(["CODDEPARTAMENTO" => $id]);
                        }
                    }
                }
            } else if ($type == 1) {
                if ($this->settings_model->deleteSubzone(["country_code" => $id])) {
                    if ($this->settings_model->deleteZone(["country_code" => $id])) {
                        if ($this->settings_model->deleteCity(["PAIS" => $id])) {
                            if ($this->settings_model->deleteState(["PAIS" => $id])) {
                                $response = $this->settings_model->deleteCountry(["CODIGO" => $id]);
                            }
                        }
                    }
                }
            }
        
            if ($response) {
                $responseSynchronized = "";
                if ($this->syncDeleteUbication($type, $id) == false) {
                    $responseSynchronized = " No fue posible sincronizar en Tienda Pro";
                }

                $this->sma->send_json(['error' => 0, 'msg' => lang('ubication_deleted') . $responseSynchronized]);
            } else {
                $this->sma->send_json(['error' => 1, 'msg' => lang('ubication_cant_be_deleted_error')]);
            }
        } else {
            $this->sma->send_json(['error' => 1, 'msg' => lang('ubication_cant_be_deleted_company')]);
        }
    }

    public function existsUbicationsInCompanies($type, $id)
    {
        $condiciones = [];
        $condiciones_delete = [];

        if ($type == 1) {
            $country = $this->site->get_country_by_id($id);
            $condiciones['country'] = $country->NOMBRE;
            $condiciones_delete['CODIGO'] = $id;
            $table = 'countries';
        } else if ($type == 2) {
            $state = $this->site->get_state_by_id($id);
            $condiciones['state'] = $state->DEPARTAMENTO;
            $condiciones_delete['CODDEPARTAMENTO'] = $id;
            $table = 'states';
        } else if ($type == 3) {
            $city = $this->site->get_city_by_id($id);
            $condiciones['city'] = $city->DESCRIPCION;
            $condiciones_delete['CODIGO'] = $id;
            $table = 'cities';
        } else if ($type == 4) {
            $zone = $this->site->get_zone_by_id($id);
            $condiciones['location'] = $zone->zone_name;
            $condiciones_delete['zone_code'] = $id;
            $table = 'zones';
        } else if ($type == 5) {
            $subzone = $this->site->get_subzone_by_id($id);
            $condiciones['subzone'] = $subzone->subzone_name;
            $condiciones_delete['subzone_code'] = $id;
            $table = 'subzones';
        }

        $q = $this->db->get_where('companies', $condiciones);
        if ($q->num_rows() > 0) {
            return true;
        }

        return false;
    }

    public function update_ubication_shipping_cost($page = 0, $view_biller = null)
    {
        $this->data['page'] = $page;
        $this->data['view_biller'] = $view_biller;
        $this->data['cnt_billers'] = count($this->site->get_billers_for_shipping_cost());
        $this->data['billers'] = $this->site->get_billers_for_shipping_cost($page, true, $view_biller);
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')),  array('link' => admin_url('system_settings/price_groups'), 'page' => lang('price_groups')), array('link' => '#', 'page' => lang('update_ubication_shipping_cost')));
        $meta = array('page_title' => lang('update_ubication_shipping_cost'), 'bc' => $bc);
        $this->page_construct('settings/update_ubication_shipping_cost', $meta, $this->data);
    }

    public function getUbicationShippingCost($page, $view_biller = null)
    {
        $ubication_costs = $this->site->get_all_ubication_shipping_costs();
        $ubications = $this->site->get_ubication_for_cost_shipping();
        $billers = $this->site->get_billers_for_shipping_cost($page, true, $view_biller);
        $html = "";
        foreach ($ubications as $ubication) {
            $cost = false;
            if (isset($ubication->subzone_code)) {
                $ubication_code = $ubication->subzone_code;
                $ubication_type = 5;
                $zone = $this->site->get_zone_by_id($ubication->zone_code);
                $city = $this->site->get_city_by_id($zone->codigo_ciudad);
                $cost = $ubication_costs[$city->CODIGO][$zone->zone_code][$ubication->subzone_code];
                $html.="<tr>
                            <th>".$city->DESCRIPCION."</th>
                            <th>".$zone->zone_name."</th>
                            <th>".$ubication->subzone_name."</th>";
            } else if (isset($ubication->zone_code)) {
                $ubication_code = $ubication->zone_code;
                $ubication_type = 4;
                $city = $this->site->get_city_by_id($ubication->codigo_ciudad);
                $cost = $ubication_costs[$city->CODIGO][$ubication->zone_code];
                $html.="<tr>
                            <th>".$city->DESCRIPCION."</th>
                            <th>".$ubication->zone_name."</th>
                            <th></th>";
            } else if (isset($ubication->CODIGO)) {
                $ubication_code = $ubication->CODIGO;
                $ubication_type = 3;
                $cost = $ubication_costs[$ubication->CODIGO];
                $html.="<tr>
                            <th>".$ubication->DESCRIPCION."</th>
                            <th></th>
                            <th></th>";
            }
            foreach ($billers as $pg) {

                if (isset($ubication->subzone_code)) {
                    $cost = $ubication_costs[$pg->id][$city->CODIGO][$zone->zone_code][$ubication->subzone_code];
                } else if (isset($ubication->zone_code)) {
                    $cost = $ubication_costs[$pg->id][$city->CODIGO][$ubication->zone_code];
                } else if (isset($ubication->CODIGO)) {
                    $cost = $ubication_costs[$pg->id][$ubication->CODIGO];
                }

                $html.='<td>
                            <input name="ubication_shipping_cost['.$ubication_code.']['.$pg->id.']" data-ubicationcode="'.$ubication_code.'" data-ubicationtype="'.$ubication_type.'" data-billerid="'.$pg->id.'" type="text" class="only_number form-control text-right pamount" '.($cost ? 'value="'.$cost.'"' : '').'  style="width:100%;">
                        </td>';
            }

            $html.="</tr>";
        }
        echo $html;
    }

    public function ajax_update_ubication_shipping_cost($biller_id, $ubicationcode, $ubicationtype, $price)
    {

        if ($this->settings_model->update_ubication_shipping_cost($biller_id, $ubicationcode, $ubicationtype, $price)) {
            exit('1');
        }
        exit('0');
    }

    public function get_webservices_url()
    {
        $technology_provider = $this->input->post('technology_provider');
        $work_environment = $this->input->post('work_environment');

        $technology_provider_configuration = $this->settings_model->get_technology_provider_configuration($technology_provider, $work_environment);

        if (!empty($technology_provider_configuration)) {
            $response_ajax = [
                "response" => TRUE,
                "data" => $technology_provider_configuration
            ];
        } else {
            $response_ajax = [
                "response" => FALSE,
                "message" => "No existe URL para el proveedor seleccionado"
            ];
        }

        echo json_encode($response_ajax);
    }

    public function configuration_concept_notes()
    {
        $meta = array('page_title' => lang('configuration_concept_notes'));
        $this->page_construct('settings/configuration_concept_notes', $meta, $this->data);
    }

    public  function get_notes_concepts()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("debit_credit_notes_concepts.id as id,
                    debit_credit_notes_concepts.type,
                    dian_code,
                    dian_name,
                    description,
                    IF({$this->db->dbprefix('tax_rates')}.id IS NULL, 'NA', {$this->db->dbprefix('tax_rates')}.name),
                    tax_amount_ledger_account,
                    tax_base_ledger_account,
                    IF(view = 1, '<i class=\"fa fa-check text-success\">', '<i class=\"fa fa-ban text-danger\">')")
            ->from("debit_credit_notes_concepts")
            ->join("tax_rates", "tax_rates.id = debit_credit_notes_concepts.tax_id", "left")
            ->add_column("Actions", '<div class="text-center">
                                        <a href='. admin_url('system_settings/edit_note_concepts/$1'). ' class="btn btn-primary btn-xs new-button new-button-sm editBudgetButton" data-toggle="modal" data-target="#myModal" title="'.lang("edit_document_type"). '">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    </div>', "id")
            ->order_by('debit_credit_notes_concepts.type', 'ASC')
            ->order_by('debit_credit_notes_concepts.id', 'ASC');

        echo $this->datatables->generate();
    }

    public function add_note_concepts()
    {
        $this->form_validation->set_rules('type', lang("concept_notes_type"), 'required');
        $this->form_validation->set_rules('dian_code', lang("concept_notes_dian_code"), 'required');
        $this->form_validation->set_rules('description', lang("concept_notes_description"), 'required');

        if ($this->input->post("tax") && $this->site->wappsiContabilidadVerificacion() > 0) {
            $this->form_validation->set_rules('tax_amount_ledger_account', lang("concept_notes_tax_amount_ledger_account"), 'required');
            $this->form_validation->set_rules('tax_base_ledger_account', lang("concept_notes_tax_base_ledger_account"), 'required');
        }

        if ($this->form_validation->run() == true) {
            $data = [
                "type" => $this->input->post('type'),
                "dian_code" => $this->input->post("dian_code"),
                "dian_name" => $this->input->post("dian_description"),
                "description" => $this->input->post("description"),
                "tax_id" => $this->input->post("tax"),
                "view" => $this->input->post("view"),
                "applied_selfretention" => ($this->input->post("applied_selfretention")) ? 1 : 0,
            ];

            if ($this->input->post("tax") && $this->site->wappsiContabilidadVerificacion() > 0) {
                $data["tax_amount_ledger_account"] = $this->input->post("tax_amount_ledger_account");
                $data["tax_base_ledger_account"] = $this->input->post("tax_base_ledger_account");
            }

            if ($this->settings_model->save_note_concept($data)) {
                $this->session->set_flashdata('message', "Concepto de Nota guardado exitosamente");
                admin_redirect("system_settings/configuration_concept_notes");
            } else {
                $this->session->set_flashdata('error', "No fue posible guardar el concepto");
                admin_redirect("system_settings/configuration_concept_notes");
            }
        } else {
            $this->data["error"] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data["dian_note_concepts"] = $this->settings_model->get_dian_note_concept("NC");
            $this->data["taxes"] = $this->settings_model->getAllTaxRates();
            if($this->site->wappsiContabilidadVerificacion() > 0) {
                $this->data["tax_amount_ledger_account"] = $this->settings_model->get_ledgers_con();
            }

            $this->load_view($this->theme . 'settings/add_note_concepts', $this->data);
        }
    }

    public function edit_note_concepts($id)
    {
        $this->form_validation->set_rules('type', lang("concept_notes_type"), 'required');
        $this->form_validation->set_rules('dian_code', lang("concept_notes_dian_code"), 'required');
        $this->form_validation->set_rules('description', lang("concept_notes_description"), 'required');

        if ($this->input->post("tax") && $this->site->wappsiContabilidadVerificacion() > 0) {
            $this->form_validation->set_rules('tax_amount_ledger_account', lang("concept_notes_tax_amount_ledger_account"), 'required');
            $this->form_validation->set_rules('tax_base_ledger_account', lang("concept_notes_tax_base_ledger_account"), 'required');
        }

        if ($this->form_validation->run() == true) {
            $data = [
                "type" => $this->input->post('type'),
                "dian_code" => $this->input->post("dian_code"),
                "dian_name" => $this->input->post("dian_description"),
                "description" => $this->input->post("description"),
                "tax_id" => $this->input->post("tax"),
                "view" => $this->input->post("view"),
                "applied_selfretention" => ($this->input->post("applied_selfretention")) ? 1 : 0,
            ];

            if ($this->input->post("tax") && $this->site->wappsiContabilidadVerificacion() > 0) {
                $data["tax_amount_ledger_account"] = $this->input->post("tax_amount_ledger_account");
                $data["tax_base_ledger_account"] = $this->input->post("tax_base_ledger_account");
            } else {
                $data["tax_amount_ledger_account"] = NULL;
                $data["tax_base_ledger_account"] = NULL;
            }

            if ($this->settings_model->update_note_concept($data, $this->input->post("note_concept_id"))) {
                $this->session->set_flashdata('message', "Concepto de Nota actualizado exitosamente");
                admin_redirect("system_settings/configuration_concept_notes");
            } else {
                $this->session->set_flashdata('error', "No fue posible actualizar el concepto");
                admin_redirect("system_settings/configuration_concept_notes");
            }
        } else {
            $this->data["error"] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

            $note_concept_data = $this->settings_model->get_note_concept_by_id($id);
            $this->data["note_concept_data"] = $note_concept_data;
            $this->data["dian_note_concepts"] = $this->settings_model->get_dian_note_concept($note_concept_data->type);
            $this->data["taxes"] = $this->settings_model->getAllTaxRates();
            if($this->site->wappsiContabilidadVerificacion() > 0) {
                $this->data["tax_amount_ledger_account"] = $this->settings_model->get_ledgers_con();
            }

            $this->load_view($this->theme . 'settings/edit_note_concepts', $this->data);
        }
    }

    public function get_dian_note_concept()
    {
        $type = $this->input->post("type");
        $dian_note_concepts = $this->settings_model->get_dian_note_concept($type);

        $dian_code_options = '<option value="">'. lang("select") .'</option>';

        foreach ($dian_note_concepts as $dian_code) {
            $dian_code_options .= '<option value="'. $dian_code->code .'">'. $dian_code->name .'</option>';
        }

        echo $dian_code_options;
    }

    public function update_warehouse_status($id)
    {

        $this->form_validation->set_rules('status', lang("status"), 'required');
        if ($this->form_validation->run() == true) {
            $status = $this->input->post('status');
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'system_settings/warehouses');
        }
        if ($this->form_validation->run() == true && $this->settings_model->update_warehouse_status($id, $status)) {
            $this->session->set_flashdata('message', lang('status_updated').", ".$msg);
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'system_settings/warehouses');
        } else {
            $this->data['warehouse'] = $this->settings_model->getWarehouseByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme.'settings/update_warehouse_status', $this->data);
        }
    }

    public function warehouse_view($id)
    {
        $this->sma->checkPermissions('index', true);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['warehouse'] = $this->settings_model->getWarehouseByID($id);
        // exit(var_dump($this->data["custom_fields"]));
        $this->load_view($this->theme.'settings/warehouse_view', $this->data);
    }

    public function cost_centers()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('cost_centers')));
        $meta = array('page_title' => lang('cost_centers'), 'bc' => $bc);
        $this->page_construct('settings/cost_centers', $meta, $this->data);
    }

    public function getCostCenters()
    {
        if($this->site->wappsiContabilidadVerificacion() > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'cost_centers_con'.$contabilidadSufijo;

            $this->load->library('datatables');
            $this->datatables
                ->select("id, code, name")
                ->from($tabla)
                ->add_column("Actions", "<div class=\"text-center\">
                                            <a href='" . admin_url('system_settings/edit_cost_center/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_cost_center") . "'>
                                                <i class=\"fa fa-edit\"></i>
                                            </a>
                                        </div>", "id");
            echo $this->datatables->generate();
        }
    }

    public function add_cost_center()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('name', lang('name'), 'required');
        $this->form_validation->set_rules('code', lang('code'), 'required');
        if ($this->form_validation->run() == TRUE) {
            $code = $this->input->post('code');
            $name = $this->input->post('name');

            $data = [
                'code' => $code,
                'name' => $name,
            ];

        } elseif ($this->input->post('add')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/cost_centers");
        }

        if ($this->form_validation->run() == TRUE && $this->settings_model->add_cost_center($data)) {
            $this->session->set_flashdata('message', lang("cost_center_added"));
            admin_redirect("system_settings/cost_centers");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['documents_types_js'] = $this->site->documents_types_js();
            $this->load_view($this->theme . 'settings/add_cost_center', $this->data);
        }
    }

    public function edit_cost_center($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('code', lang('code'), 'required');
        $this->form_validation->set_rules('name', lang('name'), 'required');

        if ($this->form_validation->run() == TRUE) {
            $code = $this->input->post('code');
            $name = $this->input->post('name');

            $data = [
                'code' => $code,
                'name' => $name,
            ];
        } elseif ($this->input->post('id')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/cost_centers");
        }

        if ($this->form_validation->run() == TRUE && $this->settings_model->update_cost_center($id, $data)) {
            $this->session->set_flashdata('message', lang("cost_center_updated"));
            admin_redirect("system_settings/cost_centers");
        } else {
            $cost_center = $this->site->getCostCenterByid($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['cost_center'] = $cost_center;
            $this->load_view($this->theme . 'settings/edit_cost_center', $this->data);
        }
    }

    public function get_update_files(){


        if ($this->Settings->automatic_update == 0) {
            $this->session->set_flashdata('error', 'Actualización automática desactivada');
            admin_redirect("sales");
        }

        if ($version_data = $this->site->get_restful_service_data()) {
            $error_messages = "";
            if ($version_data->current_version) {
                foreach ($version_data->versions as $key => $vnumber) {
                    // exit(var_dump($vnumber));
                    if ($vnumber != $this->Settings->version) {
                        $ftpHost   = '162.214.192.15';
                        $ftpUsername = 'henry@wappsi281.com';
                        $ftpPassword = 'Hp97532468';
                        $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");
                        if(ftp_login($connId, $ftpUsername, $ftpPassword)){
                            $this->db->update('settings', ['update'=>1], ['setting_id'=>1]);
                            $mode = ftp_pasv($connId, TRUE);
                            $localFilePath  = $vnumber.'.zip';
                            $remoteFilePath = $vnumber.'.zip';
                            $ruta_mover = "";
                            $this->db->db_debug = false;
                            if(ftp_size($connId, $remoteFilePath) > 0){
                                ftp_get($connId, $localFilePath, $remoteFilePath, FTP_BINARY);
                                $zip = new ZipArchive(); //Se Instancia con la Clase de ZIP
                                if ($x = $zip->open($localFilePath)) {
                                    for ($i = 0; $i < $zip->numFiles; $i++) { //Obtenemos el número de documentos que hay en el ZIP y recorremos.
                                        $nm = $zip->getNameIndex($i); //nombre completo del archivo
                                        if (strpos($nm, "scripts.sql") === false) {
                                            if (is_file(FCPATH.$nm)) {
                                                unlink(FCPATH.$nm);
                                                if (!copy("zip://".$remoteFilePath."#".$nm, $ruta_mover.$nm)) {
                                                    $error_messages .= "Error al extraer ".$nm.";";
                                                }
                                                chmod(FCPATH.$nm, 0777);
                                            }
                                        } else if (strpos($nm, "scripts.sql") !== false) {
                                            if (!$zip->extractTo("sql_update/".$ruta_mover, array($nm))) {
                                               $error_messages .= "Error al extraer ".$nm.";";
                                            }
                                            $sql = file_get_contents("sql_update/".$ruta_mover.$nm);
                                            $sql_arr = explode(";", $sql);
                                            foreach ($sql_arr as $key => $script) {
                                                $script = str_replace("\n", " ", $script);
                                                $script.=";";
                                                if (!empty($script)) {
                                                    if (strpos($script, '(año)') !== false) {
                                                        if ($this->site->wappsiContabilidadVerificacion() > 0) {
                                                            $script = str_replace('(año)', $this->session->userdata('accounting_module'), $script);
                                                        } else {
                                                            continue;
                                                        }
                                                    }
                                                    if (!$this->db->query($script)) {
                                                        $error = $this->db->error();
                                                        if ($error['code'] != 1060) {
                                                            $error_messages .= "Error code : ".$error['code'].", message : ".$error['message'].";";
                                                        }
                                                    }
                                                }
                                            }
                                            unlink(FCPATH."/sql_update/".$nm);
                                        }
                                    }
                                }
                                unlink(FCPATH.$localFilePath);
                            }else{
                                $error_messages .= "Error code : FTP get_file, Message : Error al obtener el archivo $localFilePath;";
                            }
                            ftp_close($connId);
                            $this->db->db_debug = true;
                        }else{
                            $this->session->set_flashdata('error', 'Error al loguear en el FTP');
                            admin_redirect('calendar?u=1');
                        }
                    }
                }
            }

            $this->db->update('settings', ['update'=>0, 'version'=> ($version_data ? $version_data->current_version : $this->Settings->version)], ['setting_id'=>1]);
            if ($error_messages != "") {
                $this->db->insert('automatic_update_logs', ['description' => $error_messages." >> ".($version_data ? $version_data->current_version : $this->Settings->version)]);
                $this->session->set_flashdata('error', $error_messages);
                admin_redirect('calendar?u=2');
            } else {
                $this->session->set_flashdata('message', 'Plataforma actualizada correctamente');
                admin_redirect('calendar?u=1');
            }

        } else {
            $this->session->set_flashdata('error', 'La URL del WEBSERVICE es incorrecto o el WEBSERVICE arrojó error');
            admin_redirect('calendar?u=1');
        }

    }

    public function paccount_settings()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('paccount_settings')));
        $meta = array('page_title' => lang('paccount_settings'), 'bc' => $bc);
        $this->page_construct('settings/paccount_settings', $meta, $this->data);
    }

    public function getPAccounts()
    {
        if($this->site->wappsiContabilidadVerificacion() > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'accounts_con'.$contabilidadSufijo;
            $tabla_ledgers = 'ledgers_con'.$contabilidadSufijo;

            $this->load->library('datatables');
            $actions = '<div class="btn-group text-left">
                <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                    <i class="fas fa-ellipsis-v fa-lg"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="'. admin_url('system_settings/edit_paccount/$1') .'" data-toggle="modal" data-target="#myModal" class="tip"><i class="fa fa-edit"></i> '. lang("edit_paccount") .'</a>
                    </li>
                    <li class="divider"></li>
                </ul>
            </div>';

            $this->datatables->select("
                                        ".$this->db->dbprefix($tabla).".type,
                                        typemov,
                                        tax_rates.name,
                                        ledgers.code ledger_code,
                                        ledgers.name ledger_name,
                                        ".$this->db->dbprefix($tabla).".id as id
                                    ")
                ->from($tabla)
                ->join($tabla_ledgers." as ledgers", "ledgers.id = ".$this->db->dbprefix($tabla).".ledger_id")
                ->join('tax_rates', "tax_rates.id = ".$this->db->dbprefix($tabla).".id_tax", 'left')
                ->add_column("Actions", $actions, "id")
                ->order_by($this->db->dbprefix($tabla).".type", 'asc');
            echo $this->datatables->generate();
        }
    }

    public function add_paccount()
    {
        $this->form_validation->set_rules('typemov', lang('parameter_type'), 'required');
        $this->form_validation->set_rules('ledger_id', lang('ledger'), 'required');
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                        'type' => $this->input->post('type'),
                        'typemov' => $this->input->post('typemov'),
                        'id_tax' => $this->input->post('tax_id'),
                        'id_category' => $this->input->post('category_id'),
                        'ledger_id' => $this->input->post('ledger_id'),
                        );
            $update_existing = $this->input->post('update_existing');
        } elseif ($this->input->post('add')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/paccount_settings");
        }

        if ($this->form_validation->run() == TRUE) {
            if ($this->settings_model->add_paccount($data, $update_existing)) {
                $this->session->set_flashdata('message', lang("paccount_added"));
            }
            admin_redirect("system_settings/paccount_settings");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['ledgers'] = $this->site->getAllLedgers();
            $settings_con = $this->site->getSettingsCon();
            if ($settings_con->account_parameter_method == 1) {
                $this->data['tax_rates'] = $this->settings_model->getAllTaxRates();
            } else if ($settings_con->account_parameter_method == 2) {
                $this->data['categories'] = $this->settings_model->getParentCategories();
            }
            $this->load_view($this->theme . 'settings/add_paccount', $this->data);
        }
    }

    public function edit_paccount($id)
    {
        $this->form_validation->set_rules('typemov', lang('parameter_type'), 'required');
        $this->form_validation->set_rules('ledger_id', lang('ledger'), 'required');
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                        'type' => $this->input->post('type'),
                        'typemov' => $this->input->post('typemov'),
                        'id_tax' => $this->input->post('tax_id'),
                        'id_category' => $this->input->post('category_id'),
                        'ledger_id' => $this->input->post('ledger_id'),
                        );

        } elseif ($this->input->post('add')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/paccount_settings");
        }

        if ($this->form_validation->run() == TRUE) {
            if ($this->settings_model->update_paccount($id, $data)) {
                $this->session->set_flashdata('message', lang("paccount_updated"));
            }
            admin_redirect("system_settings/paccount_settings");
        } else {
            $this->data['paccount'] = $this->settings_model->get_paccount_by_id($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['ledgers'] = $this->site->getAllLedgers();
            $settings_con = $this->site->getSettingsCon();
            if ($settings_con->account_parameter_method == 1) {
                $this->data['tax_rates'] = $this->settings_model->getAllTaxRates();
            } else if ($settings_con->account_parameter_method == 2) {
                $this->data['categories'] = $this->settings_model->getParentCategories();
            }
            $this->load_view($this->theme . 'settings/edit_paccount', $this->data);
        }
    }

    public function add_multiple_tables_restobar($branch_id)
    {
        $this->data["tables"] = $this->Restobar_model->get_consecutive_table_number($branch_id);
        $this->load_view($this->theme.'settings/add_multiple_tables_restobar', $this->data);
    }

    public function save_multiple_table()
    {
        $table_m = $this->input->post('table_M');
        $table_d = $this->input->post('table_D');
        $table_ll = $this->input->post('table_LL');

        if (empty($table_m) && empty($table_d) && emtpy($table_ll))
        {
            $this->session->set_flashdata('error', "Por ingrese al menos la cantidad de mesas a crear en alguna de las areas.");
            admin_redirect('system_settings/add_multiple_tables_restobar');
        } else
        {
            $table_data = $this->build_table_data($table_m, $table_d, $table_ll);

            $saved_table = $this->Restobar_model->insert_batch_table($table_data);
            if ($saved_table === FALSE) {
                $this->session->set_flashdata('error', 'No fue posible crear las mesas.');
            } else {
                $this->session->set_flashdata('message', 'Las mesas fueron creadas exitosamente.');
            }

            admin_redirect('system_settings/restobar');
        }
    }

    private function build_table_data($table_m, $table_d, $table_ll)
    {
        $table_data = [];

        if (!empty($table_m)) {
            $table_number = $this->input->post('current_number_M');
            $position_x = 540;
            for ($i = 0; $i < $table_m; $i++) {
                $table_number++;

                $table_data[] = [
                    "numero"=>$table_number,
                    "estado"=>1,
                    "forma_mesa"=>1,
                    "numero_asientos"=>4,
                    "imagen"=>'1_4',
                    "tamano"=>60,
                    "posicion_x"=>$position_x,
                    "posicion_y"=>290,
                    "area_id"=>$this->input->post('table_area_M'),
                    "tipo" => "M"
                ];

                $position_x+=10;
            }
        }

        if (!empty($table_d)) {
            $table_number = $this->input->post('current_number_D');
            $position_x = 540;
            for ($i = 0; $i < $table_d; $i++) {
                $table_number++;

                $table_data[] = [
                    "numero"=>$table_number,
                    "estado"=>1,
                    "forma_mesa"=>1,
                    "numero_asientos"=>4,
                    "imagen"=>'1_4',
                    "tamano"=>60,
                    "posicion_x"=>$position_x,
                    "posicion_y"=>290,
                    "area_id"=>$this->input->post('table_area_D'),
                    "tipo" => "D"
                ];

                $position_x+=10;
            }
        }

        if (!empty($table_ll)) {
            $table_number = $this->input->post('current_number_LL');
            $position_x = 540;
            for ($i = 0; $i < $table_ll; $i++) {
                $table_number++;

                $table_data[] = [
                    "numero"=>$table_number,
                    "estado"=>1,
                    "forma_mesa"=>1,
                    "numero_asientos"=>4,
                    "imagen"=>'1_4',
                    "tamano"=>60,
                    "posicion_x"=>$position_x,
                    "posicion_y"=>290,
                    "area_id"=>$this->input->post('table_area_LL'),
                    "tipo" => "LL"
                ];

                $position_x+=10;
            }
        }

        return $table_data;
    }

    public function update_product_prices_per_margin(){

        if ($this->input->post('choose_how_to_calculate_prices')) {
            $data['how_to_calculate_prices'] = $this->input->post('choose_how_to_calculate_prices');
            $data['categories'] = $this->input->post('categories');
            $data['margin_percent'] = $this->input->post('margin_percent');
            $data['formula'] = $this->input->post('formula');
            $data['price_groups'] = $this->input->post('price_groups');
            $data['rounding'] = $this->input->post('rounding_options');
            $data['cost_option'] = $this->input->post('cost_option');
        }

        if ($this->input->post('choose_how_to_calculate_prices') && $html = $this->settings_model->update_prices_per_margin($data)) {
            echo json_encode(['error' => 0, 'msg' => $html]);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['categories'] = $this->settings_model->getParentCategories();
            $this->data['price_groups'] = $this->settings_model->getAllPriceGroups();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')),  array('link' => admin_url('system_settings/w_units'), 'page' => lang('update_product_prices_per_margin')), array('link' => '#', 'page' => lang('update_product_prices_per_margin')));
            $meta = array('page_title' => lang('update_product_prices_per_margin'), 'bc' => $bc);
            $this->page_construct('settings/update_product_prices_per_margin', $meta, $this->data);
        }

    }

    function ciiu_suggestions($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $result = $this->settings_model->get_ciiu_codes($term, $limit);
        if ($a) {
            $this->sma->send_json($result);
        }
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    function get_ciiu_code($id = NULL)
    {
        $ids = explode(',', urldecode($id));
        $rows = $this->site->get_ciiu_code_by_ids($ids);
        $ciius = [];
        foreach ($rows as $ciiu) {
            $ciius[] = array('id' => $ciiu->id, 'text' => $ciiu->code." - ".$ciiu->description, 'value' => $ciiu->description);
        }
        $this->sma->send_json($ciius);
    }

    public function withholding_tax() {
        $withholding_tax_list = $this->WithholdingTax_model->get();

        $this->data["withholding_tax_list"] = $withholding_tax_list;
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct("settings/withholding_tax", ["page_title"=>lang("withholding_tax")], $this->data);
    }

    public function get_withholding_tax_datatables()
    {
        $this->sma->checkPermissions();

        $this->load->library('datatables');
        $this->datatables
            ->select("id,
                    description,
                    CAST(min_base AS DECIMAL(12, 2)) AS minimum_base,
                    CAST(percentage AS DECIMAL(12, 2)),
                    IF(apply = 'ST', 'Subtotal', 'impuesto') AS basis_calculation,
                    IF(affects = 'S', 'Ventas', 'Compras') AS transaction_type,
                    type")
            ->from("withholdings")
            ->add_column("Actions", "<div class=\"text-center\">
                                        <a href='" . admin_url('system_settings/edit_withholding_tax/$1') . "' class='tip btn btn-primary new-button new-button-sm' data-toggle-second=\"tooltip\" data-placement=\"top\" title='" . lang("edit") . "' data-toggle='modal' data-target='#myModal'>
                                            <i class=\"fa fa-pencil\"></i>
                                        </a>
                                        <a href='#' class='tip po btn btn-danger new-button new-button-sm' data-toggle-second=\"tooltip\" data-placement=\"top\" title='". lang("delete") ."' data-content=\"<p>" . lang('r_u_sure') . "</p>
                                            <a class='btn btn-danger po-delete' href='" . admin_url('system_settings/remove_withholding_tax/$1') . "'>" . lang('i_m_sure') . "</a>
                                            <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>
                                            <i class=\"fa fa-trash-o\"></i>
                                        </a>
                                    </div>", "id");
        echo $this->datatables->generate();
    }

    public function add_withholding_tax()
    {
        $this->sma->checkPermissions();

        if ($this->Settings->modulary) { $this->data['ledgers'] = $this->site->getAllLedgers(); }

        $this->data["page_title"] = lang("withholding_tax_add");
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'settings/add_withholding_tax', $this->data);
    }

    public function edit_withholding_tax($id)
    {
        $this->sma->checkPermissions();

        $this->data["withholding_tax_data"] = $this->WithholdingTax_model->get_by_id($id);
        if ($this->Settings->modulary) { $this->data['ledgers'] = $this->site->getAllLedgers(); }

        $this->data["page_title"] = lang("withholding_tax_edit");
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'settings/edit_withholding_tax', $this->data);
    }

    public function save_withholding_tax()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules("type", lang("withholding_tax_type"), "trim|required");
        $this->form_validation->set_rules("description", lang("withholding_tax_description"), "trim|required");
        $this->form_validation->set_rules("min_base", lang("withholding_tax_minimum_base"), "trim|required|greater_than_equal_to[0]");
        $this->form_validation->set_rules("percentage", lang("withholding_tax_percentage"), "trim|required");
        $this->form_validation->set_rules("affects", lang("withholding_tax_transaction_type"), "trim|required");
        $this->form_validation->set_rules("account_id", lang("withholding_tax_accounting_account"), "trim|callback_modulary_check");

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "description"=>$this->input->post('description'),
                "min_base"=>$this->input->post('min_base'),
                "percentage"=>$this->input->post('percentage'),
                "affects"=>$this->input->post("affects"),
                "type"=>$this->input->post("type"),
                "apply"=>$this->input->post("apply"),
                "assumed_account_id"=>$this->input->post('assumed_account_id'),
            ];
            if ($this->Settings->modulary == YES) {
                $data["account_id"] = $this->input->post("account_id");
            }

            $this->validateWithHodingsIfExists();

            if ($this->WithholdingTax_model->insert($data)) {
                $this->session->set_flashdata('message', lang("withholding_tax_saved"));
            } else {
                $this->session->set_flashdata('error', lang("withholding_tax_saved"));
            }

            admin_redirect("system_settings/withholding_tax");
        } else {
            $this->session->set_flashdata("error", validation_errors());
            admin_redirect("system_settings/withholding_tax");
        }
    }

    public function update_withholding_tax()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules("type", lang("withholding_tax_type"), "trim|required");
        $this->form_validation->set_rules("description", lang("withholding_tax_description"), "trim|required");
        $this->form_validation->set_rules("min_base", lang("withholding_tax_minimum_base"), "trim|required|greater_than_equal_to[0]");
        $this->form_validation->set_rules("percentage", lang("withholding_tax_percentage"), "trim|required");
        $this->form_validation->set_rules("affects", lang("withholding_tax_transaction_type"), "trim|required");
        $this->form_validation->set_rules("account_id", lang("withholding_tax_accounting_account"), "trim|callback_modulary_check");

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "description"=>$this->input->post('description'),
                "min_base"=>$this->input->post('min_base'),
                "percentage"=>$this->input->post('percentage'),
                "apply"=> $this->input->post("apply"),
                "affects"=>$this->input->post("affects"),
                "type"=>$this->input->post("type"),
                "assumed_account_id"=>$this->input->post("assumed_account_id"),
            ];
            if ($this->Settings->modulary == YES) {
                $data["account_id"] = $this->input->post("account_id");
            }

            $this->validateWithHodingsIfExists($this->input->post("id"));

            if ($this->WithholdingTax_model->update($data, $this->input->post("id"))) {
                $this->session->set_flashdata('message', lang("withholding_tax_updated"));
            } else {
                $this->session->set_flashdata('error', lang("withholding_tax_not_updated"));
            }

            admin_redirect("system_settings/withholding_tax");
        } else {
            $this->session->set_flashdata("error", validation_errors());
            admin_redirect("system_settings/withholding_tax");
        }
    }

    private function validateWithHodingsIfExists($id = NULL)
    {
        $preData = [
            "percentage"=>$this->input->post('percentage'),
            "affects"=>$this->input->post("affects"),
            "type"=>$this->input->post("type"),
            "apply"=>$this->input->post("apply"),
        ];
        if ($this->Settings->modulary == YES) {
            $preData["account_id"] = $this->input->post("account_id");
        }
        if (!empty($this->WithholdingTax_model->get_by_data($preData, $id))) {
            $this->session->set_flashdata('error', lang("Ya existe un retención con los datos ingresados"));
            admin_redirect("system_settings/withholding_tax");
        }
    }

    public function percentages_check($percentage)
    {
        $type = $this->input->post("type");

        if ($type != "ICA") {
            if ($type == "IVA") {
                $rates_array = [15, 100];
            } else {
                $rates_array = [0.1, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 6.0, 7.0, 10.0, 11.0, 20.0];
            }

            if (in_array($percentage, $rates_array) == FALSE) {
                $this->form_validation->set_message("percentages_check", "El {field} no coincide con los porcentajes dados por la DIAN");
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return TRUE;
        }
    }

    public function modulary_check($account_id)
    {
        if ($this->Settings->modulary == YES) {
            if (empty($account_id)) {
                $this->form_validation->set_message("modulary_check", "El campo {field} es obligatorio");
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return TRUE;
        }
    }

    public function remove_withholding_tax($id)
    {
        $withholding_tax_data = $this->WithholdingTax_model->get_by_id($id);

        $movement_existing = $this->WithholdingTax_model->movement_existing_tax_withholding($id, $withholding_tax_data->type);

        if ($movement_existing != NULL) {
            $this->sma->send_json(["error" => 1, "msg" => lang("withholding_tax_movement_existing")]);
        }

        if ($this->WithholdingTax_model->delete($id)) {
            $this->sma->send_json(array("error" => 0, "msg" => lang("withholding_tax_removed")));
        } else {
            $this->sma->send_json(["error" => 1, "msg" => lang("withholding_tax_not_removed")]);
        }
    }

    public function types_movement()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct("settings/types_movement", ["page_title"=>lang("types_movement")], $this->data);
    }

    public function get_types_movement_datatables()
    {
        $this->sma->checkPermissions();

        if ($this->site->wappsiContabilidadVerificacion()) {
            $sufijo_contabilidad = $this->session->userdata('accounting_module');
        }

        $this->load->library('datatables');
        $this->datatables
            ->select("movement_type_con". $sufijo_contabilidad.".id AS id,
                    movement_type_con". $sufijo_contabilidad.".movement AS movement,
                    l.name AS accounting_account_name,
                    lc.name AS counterparty_account_name,
                    dt.nombre AS document_type_name")
            ->from("movement_type_con". $sufijo_contabilidad)
            ->join("ledgers_con". $sufijo_contabilidad ." l", "l.id = movement_type_con". $sufijo_contabilidad.".ledger_id", "inner")
            ->join("ledgers_con". $sufijo_contabilidad ." lc", "lc.id=movement_type_con". $sufijo_contabilidad.".ledger_id", "inner")
            ->join("documents_types dt", "dt.id=movement_type_con". $sufijo_contabilidad.".document_type_id", "inner")
            ->add_column("Actions", "<div class=\"text-center\">
                                        <a href='" . admin_url('system_settings/edit_types_movement/$1') . "' class='tip' title='" . lang("types_movement_edit") . "' data-toggle='modal' data-target='#myModal'>
                                            <i class=\"fa fa-edit\"></i>
                                        </a>
                                        <a href='#' class='tip po' title='<b>" . lang("type_movement_remove") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p>
                                            <a class='btn btn-danger po-delete' href='" . admin_url('system_settings/remove_types_movement/$1') . "'>" . lang('i_m_sure') . "</a>
                                            <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>
                                            <i class=\"fa fa-trash-o\"></i>
                                        </a>
                                    </div>", "id");

        echo $this->datatables->generate();
    }

    public function add_types_movement()
    {
        $this->sma->checkPermissions();

        $this->data['ledgers'] = $this->site->getAllLedgers();
        $this->data["document_types"] = $this->TypesMovement_model->get_document_types();

        $this->data["page_title"] = lang("types_movement_add");
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'settings/add_types_movement', $this->data);
    }

    public function save_types_movement()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules("movement", lang("types_movement_type"), "trim|required");
        $this->form_validation->set_rules("ledger_id", lang("types_movement_accounting_account_name"), "trim|required");
        $this->form_validation->set_rules("offsetting_account", lang("types_movement_counterparty_account_name"), "trim|required");
        $this->form_validation->set_rules("document_type_id", lang("types_movement_document_type_name"), "trim|required");

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "movement"=>$this->input->post('movement'),
                "ledger_id"=>$this->input->post('ledger_id'),
                "offsetting_account"=>$this->input->post('offsetting_account'),
                "document_type_id"=>$this->input->post("document_type_id")
            ];

            if ($this->TypesMovement_model->insert($data)) {
                $this->session->set_flashdata('message', lang("types_movement_saved"));
            } else {
                $this->session->set_flashdata('error', lang("types_movement_not_saved"));
            }

            admin_redirect("system_settings/types_movement");
        } else {
            $this->session->set_flashdata("error", validation_errors());
            admin_redirect("system_settings/types_movement");
        }
    }

    public function edit_types_movement($id)
    {
        $this->sma->checkPermissions();

        $type_movement_data =  $this->TypesMovement_model->get_by_id($id);
        $existing_movement_type = $this->TypesMovement_model->get_existing_move_type($id, [$type_movement_data->ledger_id, $type_movement_data->offsetting_account]);


        $this->data['ledgers'] = $this->site->getAllLedgers();
        $this->data["type_movement_data"] = $type_movement_data;
        $this->data["document_types"] = $this->TypesMovement_model->get_document_types();
        $this->data["existing_movement_type"] = empty($existing_movement_type) ? FALSE : TRUE;

        $this->data["page_title"] = lang("types_movement_edit");
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'settings/edit_types_movement', $this->data);
    }

    public function update_types_movement()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules("movement", lang("types_movement_type"), "trim|required");
        $this->form_validation->set_rules("ledger_id", lang("types_movement_accounting_account_name"), "trim|required");
        $this->form_validation->set_rules("offsetting_account", lang("types_movement_counterparty_account_name"), "trim|required");
        $this->form_validation->set_rules("document_type_id", lang("types_movement_document_type_name"), "trim|required");

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "movement"=>$this->input->post('movement'),
                "ledger_id"=>$this->input->post('ledger_id'),
                "offsetting_account"=>$this->input->post('offsetting_account'),
                "document_type_id"=>$this->input->post("document_type_id")
            ];

            if ($this->TypesMovement_model->update($data, $this->input->post("id"))) {
                $this->session->set_flashdata('message', lang("types_movement_updated"));
            } else {
                $this->session->set_flashdata('error', lang("types_movement_not_updated"));
            }

            admin_redirect("system_settings/types_movement");
        } else {
            $this->session->set_flashdata("error", validation_errors());
            admin_redirect("system_settings/types_movement");
        }
    }

    public function remove_types_movement($id)
    {
        $type_movement_data = $this->TypesMovement_model->get_by_id($id);

        $movement_existing = $this->TypesMovement_model->get_existing_move_type($id, [$type_movement_data->ledger_id, $type_movement_data->offsetting_account]);

        if (empty($movement_existing)) {
            $this->sma->send_json(["error" => 1, "msg" => lang("type_movement_existing")]);
        }

        if ($this->TypesMovement_model->delete($id)) {
            $this->sma->send_json(array("error" => 0, "msg" => lang("type_movement_removed")));
        } else {
            $this->sma->send_json(["error" => 1, "msg" => lang("type_movement_not_removed")]);
        }
    }

    public function commercial_discounts()
    {
        $this->data["error"] = validation_errors() ? validation_errors() : $this->session->flashdata("error");
        $this->page_construct("settings/commercial_discounts", ["page_title"=>lang("commercial_discounts")], $this->data);
    }

    public function get_commercial_discounts_datatables()
    {
        $this->sma->checkPermissions();

        if ($this->site->wappsiContabilidadVerificacion()) {
            $sufijo_contabilidad = $this->session->userdata('accounting_module');
        }

        $this->load->library('datatables');
        $this->datatables
            ->select("collection_discounts.id AS id,
                    collection_discounts.apply_to,
                    IF (ls.code IS NOT NULL, CONCAT(ls.code, ' - ', ls.name), NULL) AS sales_accounting_account_code,
                    IF (lp.code IS NOT NULL, CONCAT(lp.code, ' - ', lp.name), NULL) AS purchases_accounting_account_code,
                    collection_discounts.name AS commercial_discounts_name")
            ->from("collection_discounts")
            ->join("sma_ledgers_con". $sufijo_contabilidad ." ls", "ls.id = collection_discounts.ledger_id", "left")
            ->join("sma_ledgers_con". $sufijo_contabilidad ." lp", "lp.id = collection_discounts.purchase_ledger_id", "left")
            ->add_column("Actions", "<div class=\"text-center\">
                                        <a href='" . admin_url('system_settings/edit_commercial_discount/$1') . "' class='tip' title='" . lang("types_movement_edit") . "' data-toggle='modal' data-target='#myModal'>
                                            <i class=\"fa fa-edit\"></i>
                                        </a>
                                    </div>", "id")
            ;


        echo $this->datatables->generate();
    }

    public function edit_commercial_discount($id)
    {
        $this->sma->checkPermissions();

        $this->data['ledgers'] = $this->site->getAllLedgers();
        $this->data['commercial_discounts_data'] =  $this->CommercialDiscount_model->get_by_id($id);

        $this->data["page_title"] = lang("commercial_discounts_edit");
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'settings/edit_commercial_discount', $this->data);
    }

    public function update_commercial_discount()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules("ledger_id", lang("commercial_discounts_sales_accounting_account"), "trim|required");
        $this->form_validation->set_rules("purchase_ledger_id", lang("commercial_discounts_purchase_accounting_account"), "trim|required");

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "ledger_id"=>$this->input->post('ledger_id'),
                "purchase_ledger_id"=>$this->input->post('purchase_ledger_id')
            ];

            if ($this->CommercialDiscount_model->update($data, $this->input->post("id"))) {
                $this->session->set_flashdata('message', lang("commercial_discounts_updated"));
            } else {
                $this->session->set_flashdata('error', lang("commercial_discounts_not_updated"));
            }

            admin_redirect("system_settings/commercial_discounts");
        } else {
            $this->session->set_flashdata("error", validation_errors());
            admin_redirect("system_settings/commercial_discounts");
        }
    }

    public function getIconsList()
    {
        $this->load->helper('directory');
        $dirname = "assets/payment_methods_icons";
        $ext = array("jpg", "png", "jpeg", "gif");
        $icon_files = array();
        if ($handle = opendir($dirname)) {
            while (false !== ($file = readdir($handle)))
                for ($i = 0; $i < sizeof($ext); $i++)
                    if (stristr($file, "." . $ext[$i])) {
                        $icon_files[] = $file;
                    }
            closedir($handle);
        }
        sort($icon_files);
        return ['icon_files' => $icon_files];
    }

    public function invoice_notes()
    {
        $this->data["error"] = validation_errors() ? validation_errors() : $this->session->flashdata("error");
        $this->page_construct("settings/invoice_notes", ["page_title"=>lang("invoice_notes")], $this->data);
    }

    public function get_invoice_notes_datatables()
    {
        $this->sma->checkPermissions();

        $this->load->library('datatables');
        $this->datatables
            ->select("invoice_notes.id AS id,
                    invoice_notes.description,
                    invoice_notes.state,
                    invoice_notes.module")
            ->from("invoice_notes")
            ->add_column("Actions", "<div class=\"text-center\">
                                        <a href='" . admin_url('system_settings/edit_invoice_note/$1') . "' class='tip' title='" . lang("invoice_notes_edit") . "' data-toggle='modal' data-target='#myModal'>
                                            <i class=\"fa fa-edit\"></i>
                                        </a>
                                    </div>", "id")
            ;


        echo $this->datatables->generate();
    }

    public function add_invoice_notes()
    {
        $this->sma->checkPermissions();

        $this->data["page_title"] = lang("invoice_notes_add");
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'settings/add_invoice_notes', $this->data);
    }

    public function save_invoice_notes()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules("description", lang("types_movement_type"), "trim|required|max_length[500]");
        $this->form_validation->set_rules("status", lang("types_movement_accounting_account_name"), "trim|required");
        $this->form_validation->set_rules("module", lang("types_movement_counterparty_account_name"), "trim|required");

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "description"=>$this->input->post('description'),
                "state"=>$this->input->post('status'),
                "module"=>$this->input->post('module')
            ];

            if ($this->InvoiceNotes_model->insert($data)) {
                $this->session->set_flashdata('message', lang("invoice_notes_saved"));
            } else {
                $this->session->set_flashdata('error', lang("invoice_notes_not_saved"));
            }

            admin_redirect("system_settings/invoice_notes");
        } else {
            $this->session->set_flashdata("error", validation_errors());
            admin_redirect("system_settings/invoice_notes");
        }
    }

    public function edit_invoice_note($id)
    {
        $this->sma->checkPermissions();

        $invoice_note_data =  $this->InvoiceNotes_model->get_by_id($id);

        $this->data["invoice_note_data"] = $invoice_note_data;

        $this->data["page_title"] = lang("invoice_notes_edit");
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'settings/edit_invoice_note', $this->data);
    }

    public function update_invoice_notes()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules("description", lang("types_movement_type"), "trim|required|max_length[500]");
        $this->form_validation->set_rules("status", lang("types_movement_accounting_account_name"), "trim|required");
        $this->form_validation->set_rules("module", lang("types_movement_counterparty_account_name"), "trim|required");

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "description"=>$this->input->post('description'),
                "state"=>$this->input->post('status'),
                "module"=>$this->input->post('module')
            ];

            if ($this->InvoiceNotes_model->update($data, $this->input->post("id"))) {
                $this->session->set_flashdata('message', lang("invoice_notes_updated"));
            } else {
                $this->session->set_flashdata('error', lang("invoice_notes_not_updated"));
            }

            admin_redirect("system_settings/invoice_notes");
        } else {
            $this->session->set_flashdata("error", validation_errors());
            admin_redirect("system_settings/invoice_notes");
        }
    }

    public function auditor()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules("send_auditor", lang("send_auditor"), "required");
        if ($this->form_validation->run() == TRUE) {
            $data = [
                        'start_date' => $this->input->post('start_date')." 00:00:00",
                        'end_date' => $this->input->post('end_date')." 23:59:59",
                        'documents_types' => $this->input->post('documents_types'),

                        'audit_invalid_date' => $this->input->post('audit_invalid_date'),
                        'audit_skipped_consecutives' => $this->input->post('audit_skipped_consecutives'),
                        'audit_header_detail' => $this->input->post('audit_header_detail'),
                        'audit_tax_retention_id_relation' => $this->input->post('audit_tax_retention_id_relation'),
                        'audit_tax_corresponds_product' => $this->input->post('audit_tax_corresponds_product'),
                        'audit_header_document_type_relation' => $this->input->post('audit_header_document_type_relation'),
                        'audit_company_relationed_exists' => $this->input->post('audit_company_relationed_exists'),
                        'audit_cost_center_relation' => $this->input->post('audit_cost_center_relation'),
                        'audit_products_relation_in_movements' => $this->input->post('audit_products_relation_in_movements'),
                        'audit_products_option_relation_in_movements' => $this->input->post('audit_products_option_relation_in_movements'),
                        'audit_invoice_retentions_payments' => $this->input->post('audit_invoice_retentions_payments'),
                        'audit_invoice_paid_value' => $this->input->post('audit_invoice_paid_value'),
                        'audit_returns_payments' => $this->input->post('audit_returns_payments'),
                        'audit_pending_invoices' => $this->input->post('audit_pending_invoices'),
                        'audit_invoices_commision' => $this->input->post('audit_invoices_commision'),
                        'audit_product_cost_and_avg_cost' => $this->input->post('audit_product_cost_and_avg_cost'),
                        'audit_product_cost_and_price_base' => $this->input->post('audit_product_cost_and_price_base'),
                        'audit_fe_invoice_pending_dian' => $this->input->post('audit_fe_invoice_pending_dian'),
                        'audit_sale_price_avg_costing' => $this->input->post('audit_sale_price_avg_costing'),
                        'audit_products_without_movements' => $this->input->post('audit_products_without_movements'),
                        'audit_credit_customers_direct_payments' => $this->input->post('audit_credit_customers_direct_payments'),
                        'audit_payments_concepts_ledger_id' => $this->input->post('audit_payments_concepts_ledger_id'),
                        'audit_invoice_subtotal_tax_grand_total' => $this->input->post('audit_invoice_subtotal_tax_grand_total'),
                        'audit_invoices_negative_balance' => $this->input->post('audit_invoices_negative_balance'),
                        'audit_document_types_modulary_parametrization' => $this->input->post('audit_document_types_modulary_parametrization'),
                        'audit_movement_accounted' => $this->input->post('audit_movement_accounted'),
                        'audit_total_movement_payment_method' => $this->input->post('audit_total_movement_payment_method'),
                        'audit_retention_and_discounts_accounted' => $this->input->post('audit_retention_and_discounts_accounted'),
                        'audit_movement_accounting_third' => $this->input->post('audit_movement_accounting_third'),
                        'audit_movement_accounting_cost_center' => $this->input->post('audit_movement_accounting_cost_center'),
                        'audit_movement_accounting_duplicated' => $this->input->post('audit_movement_accounting_duplicated'),
                        'audit_invoice_pending_accounted' => $this->input->post('audit_invoice_pending_accounted'),
                        'audit_entries_debit_credit_total' => $this->input->post('audit_entries_debit_credit_total'),
                        'audit_entries_pending' => $this->input->post('audit_entries_pending'),
                        'audit_entries_invalid_ledger' => $this->input->post('audit_entries_invalid_ledger'),
                        'audit_tax_modulary_parametrization' => $this->input->post('audit_tax_modulary_parametrization'),
                        'audit_payment_methods_parametrization' => $this->input->post('audit_payment_methods_parametrization'),
                        'audit_expense_categories_modulary_parametrization' => $this->input->post('audit_expense_categories_modulary_parametrization'),
                        'audit_quantity_adjustments_modulary_parametrization' => $this->input->post('audit_quantity_adjustments_modulary_parametrization'),
                        'audit_movement_type_modulary_parametrization' => $this->input->post('audit_movement_type_modulary_parametrization'),
                    ];
        }
        if ($this->form_validation->run() == TRUE && $reponse = $this->settings_model->audit_system($data)) {
            $this->load->library('excel');
            $num = 0;
            foreach ($reponse as $type_audit => $arr) {
                $this->excel->createSheet($num);
                $this->excel->setActiveSheetIndex($num);
                $this->excel->getActiveSheet()->setTitle('Resultados '.($num+1));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang($type_audit));
                $this->excel->getActiveSheet()->SetCellValue('A2', 'Tabla');
                $this->excel->getActiveSheet()->SetCellValue('B2', 'Tabla Detalle');
                $this->excel->getActiveSheet()->SetCellValue('C2', 'Mensaje');
                $this->excel->getActiveSheet()->getStyle("A1:C1")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle("A2:C2")->getFont()->setBold(true);
                $row = 3;
                foreach ($arr as $key => $text) {
                    $arr_text = explode("/", $text);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $arr_text[0]);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $arr_text[1]);
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, $arr_text[2]);
                    $row++;
                }
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(120);
                $num++;
            }
            $filename = 'report_auditoria';
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
            admin_redirect('system_settings/auditor');
        }
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('auditor')));
        $meta = array('page_title' => lang('auditor'), 'bc' => $bc);
        $this->page_construct('settings/auditor', $meta, $this->data);
    }

    function warehouse_suggestions($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $result = $this->settings_model->getWarehouseSuggestions($term, $limit);
        if ($a) {
            $this->sma->send_json($result);
        }
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    function validate_paccount_exists()
    {
        $data['type'] = $this->input->get('type');
        $data['typemov'] = $this->input->get('typemov');
        $data['id_tax'] = $this->input->get('id_tax');
        $data['id_category'] = $this->input->get('id_category');
        echo json_encode(['response' => $this->settings_model->validate_paccount_exists($data)]);
    }

    public function getSoundsList()
    {
        $this->load->helper('directory');
        $dirname = 'themes/default/admin/assets/sounds';
        $ext = array("mp3", "wav");
        $audio_files = array();
        // exit(var_dump($dirname));
        if ($handle = opendir($dirname)) {
            while (false !== ($file = readdir($handle)))
                for ($i = 0; $i < sizeof($ext); $i++)
                    if (stristr($file, "." . $ext[$i])) {
                        $audio_files[] = $file;
                    }
            closedir($handle);
        }
        sort($audio_files);
        return ['audio_files' => $audio_files];
    }

    public function get_subcategories($category_id, $not_this_parent = NULL)
    {
        if ($scats = $this->site->getSubCategories($category_id, $not_this_parent)) {
            $html = "<option value=''>".lang('select')."</option>";
            foreach ($scats as $scat) {
                $html .= "<option value='".$scat->id."'>".$scat->name."</option>";
            }
            echo json_encode(['html'=>$html]);
        } else {

            echo json_encode(['html'=>"<option value=''>".lang('no_register')."</option>"]);
        }
    }

    public function update_product_unit_prices_general()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('update_product_unit_prices_general')));
        $meta = array('page_title' => lang('update_product_unit_prices_general'), 'bc' => $bc);
        $this->page_construct('settings/update_product_unit_prices_general', $meta, $this->data);
    }

    public function get_products_units_html() 
    {
        $results = $this->site->get_all_products_units();
        if (count($results) == 0) {
            exit('<table class="table table-bordered"><tbody><tr><td>No se encontraron datos</td></tr></tbody></table>');
        }
        // Obtener el número máximo de unidades asignadas a cualquier producto
        $max_units = 0;
        $up_setted=[];
        foreach ($results as $row) {
            $unit_id = $row->unit_id;
            if (!isset($units[$row->product_id])) {
                $units[$row->product_id] = array();
            }
            $units[$row->product_id][] = $row;
            $max_units = max($max_units, count($units[$row->product_id]));
            if (count($units[$row->product_id]) >= 6) {
                // $this->sma->print_arrays($units[$row->product_id]);
            }
        }
        $max_units++;
        $width = $this->sma->formatQuantity(75 / ($max_units))."%";
        // Construir la tabla HTML
        $html = '<table class="table table-bordered" id="units_data">';
        $html .= '<thead><tr><th style="width:15% !important;">Producto</th><th style="width:10% !important;">Costo</th>';
        for ($i = 1; $i <= $max_units; $i++) {
            $html .= '<th style="width:'.$width.' !important;">Unidad '.$i.'</th>';
        }
        $html .= '</tr></thead><tbody>';
        $printed_products = [];
        foreach ($results as $row) {
            if (!isset($printed_products[$row->product_id])) {
                $printed_products[$row->product_id] = 1;
            } else {
                continue;
            }
            $product_id = $row->product_id;
            $product_name = $row->product_name;
            $product_code = $row->product_code;
            $margin = $this->sma->formatDecimals(((($row->main_price - $row->main_cost) / ($row->main_price > 0 ? $row->main_price : 1)) * 100), 2);
            $html .= '<tr><td>'.$product_code." ".$product_name.'</td><td>'.$this->sma->formatDecimals($row->main_cost).'</td>';
            $html .= '<td>
                        <input type="text" class="form-control only_number unit_price"  data-cost="'.$row->main_cost.'" data-idproduct="'.$row->product_id.'" data-idunit="'.$row->main_unit_id.'" data-pgid="'.$row->main_u_pgid.'" data-unitbase="1" data-oldprice="'.$this->sma->formatDecimals($row->main_price).'" value="'.$this->sma->formatDecimals($row->main_price).'">
                        <br><b>'.$row->main_u_name.' <span class="text_margin">('.$margin.' %)</span>'.'</b>
                    </td>';
            for ($i = 1; $i < $max_units; $i++) {
                $unit_price = '-';
                $unit_name = '';
                if (isset($units[$product_id][$i-1])) {
                    $unit_price = $units[$product_id][$i-1]->unit_price;
                    $unit_name = $units[$product_id][$i-1]->unit_name;
                    $unit_id = $units[$product_id][$i-1]->unit_id;
                    $margin = $this->sma->formatDecimals(((($unit_price - $row->main_cost) / ($unit_price > 0 ? $unit_price : 1)) * 100), 2);
                    $html .= '<td>
                                <input type="text" class="form-control only_number unit_price" data-cost="'.$row->main_cost.'"  data-idproduct="'.$row->product_id.'" data-pgid="'.$row->unit_pgid.'" data-idunit="'.$unit_id.'"  data-unitbase="0" value="'.$this->sma->formatDecimals($unit_price).'">
                                <br><b>'.$unit_name.' <span class="text_margin">('.$margin.' %)</span>'.'</b>
                            </td>';
                } else {
                    $html .= '<td><input type="text" disabled="true"> </td>';
                }
            }
            $html .= '</tr>';
        }
        $html .= '</tbody></table>';
        exit($html);
    }

    public function get_products_units_xls() 
    {
        $results = $this->site->get_all_products_units();
        if (count($results) == 0) {
            exit('<table class="table table-bordered"><tbody><tr><td>No se encontraron datos</td></tr></tbody></table>');
        }
        // Obtener el número máximo de unidades asignadas a cualquier producto
        $max_units = 0;
        $up_setted=[];
        foreach ($results as $row) {
            $unit_id = $row->unit_id;
            if (!isset($units[$row->product_id])) {
                $units[$row->product_id] = array();
            }
            $units[$row->product_id][] = $row;
            $max_units = max($max_units, count($units[$row->product_id]));
            if (count($units[$row->product_id]) >= 6) {
                // $this->sma->print_arrays($units[$row->product_id]);
            }
        }
        $max_units++;

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('product_units'));
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('cost'));
        $letter = 'D';
        for ($i = 1; $i <= $max_units; $i++) {
            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('unit_name')." ".$i);
            $letter++;
            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('operation_value')." ".$i);
            $letter++;
            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('unit_price')." ".$i);
            $letter++;
        }


        $printed_products = [];
        $nrow = 2;
        foreach ($results as $row) {
            if (!isset($printed_products[$row->product_id])) {
                $printed_products[$row->product_id] = 1;
            } else {
                continue;
            }
            $product_id = $row->product_id;
            $product_name = $row->product_name;
            $product_code = $row->product_code;
            $margin = $this->sma->formatDecimals(((($row->main_price - $row->main_cost) / ($row->main_price > 0 ? $row->main_price : 1)) * 100), 2);
            $this->excel->getActiveSheet()->SetCellValue('A'. $nrow, $product_code);
            $this->excel->getActiveSheet()->SetCellValue('B'. $nrow, $product_name);
            $this->excel->getActiveSheet()->SetCellValue('C'. $nrow, $this->sma->formatDecimals($row->main_cost));
            $this->excel->getActiveSheet()->SetCellValue('D'. $nrow, $row->main_u_name);
            $this->excel->getActiveSheet()->SetCellValue('E'. $nrow, ($row->main_operation_value > 0 ? $row->main_operation_value : 1));
            $this->excel->getActiveSheet()->SetCellValue('F'. $nrow, $this->sma->formatDecimals($row->main_price));
            $letter = 'G';
            for ($i = 1; $i < $max_units; $i++) {
                $unit_price = '-';
                $unit_name = '';
                if (isset($units[$product_id][$i-1])) {
                    $unit_price = $units[$product_id][$i-1]->unit_price;
                    $unit_operation_value = $units[$product_id][$i-1]->unit_operation_value;
                    $unit_name = $units[$product_id][$i-1]->unit_name;
                    $unit_id = $units[$product_id][$i-1]->unit_id;
                    $margin = $this->sma->formatDecimals(((($unit_price - $row->main_cost) / ($unit_price > 0 ? $unit_price : 1)) * 100), 2);
                    $this->excel->getActiveSheet()->SetCellValue($letter. $nrow, $unit_name);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter. $nrow, ($unit_operation_value > 0 ? $unit_operation_value : 1));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter. $nrow, $this->sma->formatDecimals($unit_price));
                    $letter++;
                } else {
                    $this->excel->getActiveSheet()->SetCellValue($letter. $nrow, ' - ');
                    $letter++;
                }
            }
            $nrow++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
        $this->excel->getActiveSheet()->getStyle('C1:C'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('E1:E'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('F1:F'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('H1:H'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('I1:I'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('K1:K'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('L1:L'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('N1:N'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('D1:D'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('Q1:Q'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('R1:R'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('T1:T'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getActiveSheet()->getStyle('U1:U'.$nrow)->getNumberFormat()->setFormatCode("#,##0.00");
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = lang('product_units');
        $this->load->helper('excel');
        create_excel($this->excel, $filename);

    }

    public function update_product_unit_price_general()
    {
        $product_id = $this->input->post('product_id');
        $unit_id = $this->input->post('unit_id');
        $price = $this->input->post('price');
        $old_price = $this->input->post('old_price');
        $unit_base = $this->input->post('unit_base');
        $price_group_id = $this->input->post('price_group_id');

        if ($this->db->update('unit_prices', ['valor_unitario'=>$price], ['id_product'=>$product_id, 'unit_id'=>$unit_id])) {
            if ($unit_base) {
                $pg_base = $this->db->get_where('price_groups', ['price_group_base'=>1])->row();
                $this->db->update('product_prices', ['price'=>$price], ['product_id'=> $product_id, 'price_group_id'=>$pg_base->id]);
            }
            if ($unit_base && !$this->db->update('products', ['price'=>$price], ['id'=>$product_id])) {
                exit('false');
            }
            if ($price_group_id && !$this->db->update('product_prices', ['price'=>$price], ['product_id'=>$product_id, 'price_group_id' => $price_group_id])) {
                exit('false');
            }

            $product_data = $this->db->get_where('products', ['id'=>$product_id])->row();
            $unit_data = $this->db->get_where('units', ['id'=>$unit_id])->row();
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 1,
                'table_name' => 'products',
                'record_id' => $product_id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' cambió el precio del producto ('.$product_data->name.') para la unidad ('.$unit_data->name.') de '.$old_price.' a '.$price,
            ]);
            exit('true');
        }
        exit('false');
    }

    public function get_invoice_format()
    {
        $invoice_format_id = $this->input->post('invoice_format_id');
        $dt_data = $this->site->get_invoice_format($invoice_format_id);
        if ($dt_data) {
            echo json_encode(['data'=>$dt_data]);
        } else {
            echo json_encode(['data'=>false]);
        }
    }

    // public function user_activities()
    // {
    //     $this->sma->checkPermissions();
    //     $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
    //     $this->data['users'] = $this->site->get_all_users();
    //     $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('user_activities')));
    //     $meta = array('page_title' => lang('user_activities'), 'bc' => $bc);
    //     $this->page_construct('settings/user_activities', $meta, $this->data);
    // }

    public function getUserActivities($xls = NULL)
    {
        $start_date = $this->input->get('start_date') ?  $this->sma->fld($this->input->get('start_date')) : date('Y-01-01 00:00:00');
        $end_date = $this->input->get('end_date') ?  $this->sma->fld($this->input->get('end_date')) : date('Y-m-d H:i:s');
        $user = $this->input->get('user');
        $type = $this->input->get('type');
        $module = $this->input->get('module');
        $table = $this->input->get('table');
        $description = $this->input->get('description');
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        user_activities.id,
                        user_activities.date,
                        CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user,
                        user_activities.type_id,
                        user_activities.module_name,
                        user_activities.table_name,
                        user_activities.description
                    ")
            ->from("user_activities")
            ->join('users', 'users.id=user_activities.user_id', 'left');

        if ($start_date) {
            $this->datatables->where('date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('date <=', $end_date);
        }
        if ($user) {
            $this->datatables->where('user_id', $user);
        }
        if ($type) {
            $this->datatables->where('type_id', $type);
        }
        if ($module) {
            $this->datatables->where('module_name', $module);
        }
        if ($table) {
            $this->datatables->where('table_name', $table);
        }
        if ($description) {
            $desc_Arr = $this->sma->obtener_array_comillas($description);
            foreach ($desc_Arr as $key => $value) {
                $this->datatables->like('description', $value);
            }
        }

        if ($xls) {
            $data = $this->datatables->get_display_result_2();
            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('user_activities');
            $this->excel->getActiveSheet()->SetCellValue('A1', lang('id'));
            $this->excel->getActiveSheet()->SetCellValue('B1', lang('date'));
            $this->excel->getActiveSheet()->SetCellValue('C1', lang('user'));
            $this->excel->getActiveSheet()->SetCellValue('D1', lang('type'));
            $this->excel->getActiveSheet()->SetCellValue('E1', lang('module'));
            $this->excel->getActiveSheet()->SetCellValue('F1', lang('table'));
            $this->excel->getActiveSheet()->SetCellValue('G1', lang('description'));
            $row = 2;
            foreach ($data as $data_row) {
                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->id);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->date);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->user);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->type_id);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->module_name);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->table_name);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->description);
                $row++;
            }
            for ($i='A'; $i <= 'G'; $i++) {
                $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
            }
            $filename = 'user_activities_'.date('Y_m_d_H_i_s');
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
        } else {
            echo $this->datatables->generate();
        }
    }

    public function ua_view($id)
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['ua'] = $this->site->get_user_activity_by_id($id);
        // exit(var_dump($this->data["custom_fields"]));
        $this->load_view($this->theme.'settings/ua_view', $this->data);
    }

    public function valid_unique_code(){
        $code = $this->input->get('code');
        $module = $this->input->get('module');
        $id = $this->input->get('id');
        $this->db->where('code', $code);
        if ($id !== "false") {
            $this->db->where('id !=', $id);
        }

        if ($module == 'brand') {
            $q = $this->db->get('brands');
        } else if ($module == 'category') {
            $q = $this->db->get('categories');
        } else if ($module == 'variant') {
            $q = $this->db->get('product_variants');
        } else if ($module == 'product') {
            $q = $this->db->get('products');
        }

        if ($q->num_rows() > 0) {
            echo json_encode(['exists'=>true]);
            exit();
        }
        echo json_encode(['exists'=>false]);
        exit();
    }

    public function get_code_consecutive(){
        $module = $this->input->get('module');
        $code = $this->site->getCodeConsecutive($module);
        echo json_encode(['code'=>$code]);
        exit();
    }

    public function instances()
    {
        $this->sma->checkPermissions();

        $settings = new Settings_model();

        $this->data["instances"] = $settings->getAllInstances();

        $this->page_construct('settings/instances', ['page_title' => lang('instances')], $this->data);
    }

    public function getAllInstances()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, security_token, technology_provider_id")
            ->from("electronic_billing_instances")
            ->add_column("Actions", "<div class='btn-group text-left'>
                <button type='button' class='btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle' data-toggle='dropdown' data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                    <i class='fas fa-ellipsis-v fa-lg'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li>
                        <a href='" . admin_url('system_settings/editInstance/$1') . "' data-toggle='modal' data-target='#myModal' class='tip'>
                            <i class=\"fa fa-edit\"></i> " . lang("edit_instance") . "
                        </a>
                    </li>
                </ul>
            </div>", "id");

        echo $this->datatables->generate();
    }

    public function addIntances()
    {
        $this->sma->checkPermissions();

        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'settings/add_instance', $this->data);
    }

    public function createInstance()
    {
        $this->sma->checkPermissions();
        $settings = new Settings_model();

        $this->form_validation->set_rules('name', lang("brand_name"), 'trim|required');
        $this->form_validation->set_rules('security_token', lang("security_token_bpm"), 'trim|required|is_unique[electronic_billing_instances.security_token]');

        if ($this->form_validation->run() == true) {
            $data = [
                'name' => $this->input->post('name'),
                'security_token' => $this->input->post('security_token'),
                'technology_provider_id' => BPM
            ];

            if ($settings->createInstance($data)) {
                $this->session->set_flashdata('message', lang("saved_instance"));
            } else {
                $this->session->set_flashdata('error', lang("unsaved_instance"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("system_settings/instances");
    }

    public function editInstance($id)
    {
        $this->sma->checkPermissions();

        $settings = new Settings_model();

        $this->data['instance'] = $settings->getInstance(['id'=>$id]);
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'settings/edit_instance', $this->data);
    }

    public function updateInstance($id)
    {
        $this->sma->checkPermissions();
        $settings = new Settings_model();

        $this->form_validation->set_rules('name', lang("brand_name"), 'trim|required');
        $this->form_validation->set_rules('security_token', lang("security_token_bpm"), 'trim|required');

        $instance = $settings->getInstance(['security_token' => $this->input->post('security_token')], $id);
        if (!empty($instance)) {
            $this->session->set_flashdata('error', lang("El token ingresado ya se encuentra registrado"));
            admin_redirect("system_settings/instances");
        }

        if ($this->form_validation->run() == true) {
            $data = [
                'name' => $this->input->post('name'),
                'security_token' => $this->input->post('security_token')
            ];

            if ($settings->updtateInstance($data, $id)) {
                $this->session->set_flashdata('message', lang("saved_instance"));
            } else {
                $this->session->set_flashdata('error', lang("unsaved_instance"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("system_settings/instances");
    }

    public function validateInstance()
    {
        $settings = new Settings_model();

        if ($this->Settings->fe_technology_provider == BPM  && $this->input->post('factura_electronica') == YES) {
            $instance = $this->input->post('instance');
            $module = $this->input->post('module');
            $prefix = $this->input->post('sales_prefix');
            if ($module == 1 || $module == 2) {
                $module = [1, 2];
            } else {
                $module = [$module];
            }

            $instanceDocumentType = $settings->getInstanceDocumentType(["instance"=>$instance], $module);
            if (!empty($instanceDocumentType)) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public function tags()
    {
        $this->sma->checkPermissions();
        $this->page_construct('settings/tags', ['page_title' => lang('tags')], $this->data);
    }

    public function getAllTags()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, module, description, type, color, status")
            ->from("tags")
            ->add_column("Actions", "<div class='btn-group text-left'>
                <button type='button' class='btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle' data-toggle='dropdown' data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                    <i class='fas fa-ellipsis-v fa-lg'></i>
                </button>
                <ul class='dropdown-menu pull-right' role='menu'>
                    <li>
                        <a href='" . admin_url('system_settings/editTag/$1') . "' data-toggle='modal' data-target='#myModal' class='tip'>
                            <i class=\"fa fa-edit\"></i> " . lang("edit_tag") . "
                        </a>
                    </li>
                </ul>
            </div>", "id");

        echo $this->datatables->generate();
    }

    public function addTag()
    {
        $this->data['colors'] = [ "#fe193d", "#fe4b00", "#ffa001", "#67bb02", "#00cc71",
            "#0095ff", "#6a77e7", "#b660ea", "#c6c6c6", "#3f3f3f"
        ];
        $this->data['modules'] = [
            '' => $this->lang->line("select"),
            1 => "Producto",
            2 => "Vental Detal",
            3 => "Vental POS",
            4 => "Compra",
            5 => "Documento de Soporte",
            6 => "Nota débito",
            7 => "Nota crédito de Venta",
            8 => "Nota crédito de Compra",
            9 => "Nota crédito por Otros conceptos Venta",
            10 => "Nota crédito por Otros conceptos Compra",
            11 => "Cotización Venta",
            12 => "Orden de pedido",
        ];
        $this->data['types'] = [1 => "Normal", 2 => "Urgente"];
        $this->data['status'] = [1 => "Activo", 2 => "Inactivo"];
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'settings/add_tag', $this->data);
    }

    public function createTag()
    {
        $this->sma->checkPermissions();
        $settings = new Settings_model();

        $this->form_validation->set_rules('module', lang("module"), 'trim|required');
        $this->form_validation->set_rules('type', lang("type"), 'trim|required');
        $this->form_validation->set_rules('color', lang("color"), 'trim|required');
        $this->form_validation->set_rules('status', lang("status"), 'trim|required');
        $this->form_validation->set_rules('description', lang("description"), 'trim|required|is_unique[tags.description]');

        if ($this->form_validation->run() == true) {
            $data = [
                'module' => $this->input->post('module'),
                'type' => $this->input->post('type'),
                'color' =>  $this->input->post('color'),
                'status' =>  $this->input->post('status'),
                'description' =>  $this->input->post('description'),
            ];
            if ($settings->createTag($data)) {
                $this->session->set_flashdata('message', lang("saved_tag"));
            } else {
                $this->session->set_flashdata('error', lang("unsaved_tag"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("system_settings/tags");
    }

    public function editTag($id)
    {
        $settings = new Settings_model();
        $this->data['colors'] = [ "#fe193d", "#fe4b00", "#ffa001", "#67bb02", "#00cc71",
            "#0095ff", "#6a77e7", "#b660ea", "#c6c6c6", "#3f3f3f"
        ];

        $this->data['modules'] = [
            '' => $this->lang->line("select"),
            1 => "Producto",
            2 => "Vental Detal",
            3 => "Vental POS",
            4 => "Compra",
            5 => "Documento de Soporte",
            6 => "Nota débito",
            7 => "Nota crédito de Venta",
            8 => "Nota crédito de Compra",
            9 => "Nota crédito por Otros conceptos Venta",
            10 => "Nota crédito por Otros conceptos Compra",
            11 => "Cotización Venta",
            12 => "Orden de pedido",
        ];
        $this->data['types'] = [1 => "Normal", 2 => "Urgente"];
        $this->data['status'] = [1 => "Activo", 2 => "Inactivo"];
        $this->data['tag'] = $settings->findTag(['id'=>$id]);
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'settings/edit_tag', $this->data);
    }

    public function updateTag($id)
    {
        $settings = new Settings_model();

        $this->form_validation->set_rules('module', lang("module"), 'trim|required');
        $this->form_validation->set_rules('type', lang("type"), 'trim|required');
        $this->form_validation->set_rules('color', lang("color"), 'trim|required');
        $this->form_validation->set_rules('status', lang("status"), 'trim|required');
        $this->form_validation->set_rules('description', lang("description"), 'trim|required');

        $tag = $settings->findTag(['description' => $this->input->post('description')], $id);
        if (!empty($instance)) {
            $this->session->set_flashdata('error', lang("La etiqueta ingresada ya se encuentra registrada"));
            admin_redirect("system_settings/tags");
        }

        if ($this->form_validation->run() == true) {
            $data = [
                'module' => $this->input->post('module'),
                'type' => $this->input->post('type'),
                'color' =>  $this->input->post('color'),
                'status' =>  $this->input->post('status'),
                'description' =>  $this->input->post('description'),
            ];

            if ($settings->updateTag($data, $id)) {
                $this->session->set_flashdata('message', lang("updated_tag"));
            } else {
                $this->session->set_flashdata('error', lang("tag_not_updated"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("system_settings/tags");
    }

    private function getJsonRegisterSimba($technology_provider_configuration)
    {
        $json = [];
        $json["NIT"]        = $technology_provider_configuration->nit_wappsi;
        $json["Token"]      = $technology_provider_configuration->token_wappsi;
        $json["COR_PPAL"]   = $technology_provider_configuration->correo_wappsi;
        $json["NEW_PASS"]   = "";
        $json["NUM_IDEN"]   = $this->Settings->numero_documento;
        $json["TIP_IDEN"]   = $this->Settings->tipo_documento;
        $json['DIG_VERI']   = $this->Settings->digito_verificacion;
        $json['NOM_TERC']   = ($this->Settings->razon_social && $this->Settings->razon_social != '') ? $this->Settings->razon_social : $this->Settings->nombre_comercial;
        $json['DES_TERC']   = ($this->Settings->razon_social && $this->Settings->razon_social != '') ? $this->Settings->razon_social : $this->Settings->nombre_comercial;
        $json['TEL_PPAL']   = $this->Settings->phone;
        $json['DIR_PPAL']   = $this->Settings->direccion;
        return json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    public function syncSimba()
    {
        $fe_provider = $this->input->post('fe_provider');
        $fe_envinroment = $this->input->post('fe_environment');
        $technology_provider_configuration = $this->settings_model->get_technology_provider_configuration($fe_provider, $fe_envinroment);
        if (!empty($technology_provider_configuration)) {
            $json = $this->getJsonRegisterSimba($technology_provider_configuration);
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://fe2.simba.co/api_nomina/RegistroConvenio',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $json,
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            ]);
            $response = curl_exec($curl);
            curl_close($curl);
            log_message('debug', 'FE - SIMBA registro convenio: '. $response);
            // $response = '{"TOK_EMPR": "ff", "Msg": "EXITO"}'; // <- para pruebas
            try {
                $dataResponse = json_decode($response, false);
                if ($dataResponse === null) {
                    echo json_encode([
                        "response" => FALSE,
                        "message" => "Error en el proceso"
                    ]);
                }
                if($dataResponse->Msg != 'EXITO'){
                    $this->db->update('settings', ['reg_conv_simba_res' => $dataResponse->Msg], ['setting_id' => 1]);
                    echo json_encode([
                        "response" => FALSE,
                        "message" => "Error: " .$dataResponse->Msg
                    ]);
                }else {
                    $token = $dataResponse->TOK_EMPR;
                    $this->db->update('technology_provider_configuration', ['security_token' => $token], ['technology_provider_id' => 3]);
                    $this->db->update('settings', ['reg_conv_simba_res' => $dataResponse->Msg, 'reg_conv_simba_status' => 1], ['setting_id' => 1]);
                    echo json_encode([
                        "response" => TRUE,
                        "message" => "Asociación realizada con exito"
                    ]);
                }
            } catch (Exception $e) {
                log_message('debug','FE SIMBA registration: '. $e->getMessage());
            }
        }
    }

    public function HabilitarFE()
    {
        $technology_provider_configuration = $this->settings_model->get_technology_provider_configuration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
        $textId = $this->input->post('setTestId');
        $json = [];
        $json['NIT'] = $this->Settings->numero_documento;
        $json['Token'] = $technology_provider_configuration->security_token;
        $json['SetTestId'] = $textId;
        $peticion = json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://fe2.simba.co/api_nomina/api/FacturaElectronica/HabilitarFE',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $peticion,
            CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
        ]);
        $response = curl_exec($curl);
        curl_close($curl);
        // $response = '{"Error": "true", "Msg": "Procesado Correctamente."}'; // <- para pruebas
        log_message('debug', 'FE - SIMBA Habilitar FE: '. $response);
        try {
            $dataResponse = json_decode($response, false);
            if ($dataResponse === null) {
                echo json_encode([
                    "response" => FALSE,
                    "message" => "Error en el proceso"
                ]);
            }
            if($dataResponse->Msg != 'Procesado Correctamente.'){
                $this->db->update('settings', ['qualification_simba_res' => $dataResponse->Msg], ['setting_id' => 1]);
                echo json_encode([
                    "response" => FALSE,
                    "message" => "Error: " .$dataResponse->Msg
                ]);
            }else {
                $this->db->update('settings', ['qualification_simba_res' => $dataResponse->Msg, 'qualification_simba_status' => 1], ['setting_id' => 1]);
                echo json_encode([
                    "response" => TRUE,
                    "message" => "Habilitación realizada con exito"
                ]);
            }
        } catch (Exception $e) {
            log_message('debug','FE SIMBA Habilitar FE: '. $e->getMessage());
        }
    }

    public function HabilitarPoFE()
    {
        $technology_provider_configuration = $this->settings_model->get_technology_provider_configuration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
        $textId = $this->input->post('setTestId');
        $json = [];
        $json['NIT'] = $this->Settings->numero_documento;
        $json['Token'] = $technology_provider_configuration->security_token;
        $json['SetTestId'] = $textId;
        $peticion = json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://fe2.simba.co/api_nomina/api/FacturaElectronica/HabilitarPOS',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $peticion,
            CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
        ]);
        $response = curl_exec($curl);
        curl_close($curl);
        // $response = '{"Error": "true", "Msg": "Procesado Correctamente."}'; // <- para pruebas
        log_message('debug', 'FE - SIMBA Habilitar FE: '. $response);
        try {
            $dataResponse = json_decode($response, false);
            if ($dataResponse === null) {
                echo json_encode([
                    "response" => FALSE,
                    "message" => "Error en el proceso"
                ]);
            }
            if($dataResponse->Msg != 'Procesado Correctamente.'){
                $this->db->update('settings', ['quali_simba_res_pofe' => $dataResponse->Msg], ['setting_id' => 1]);
                echo json_encode([
                    "response" => FALSE,
                    "message" => "Error: " .$dataResponse->Msg
                ]);
            }else {
                $this->db->update('settings', ['quali_simba_res_pofe' => $dataResponse->Msg, 'quali_simba_status_pofe' => 1], ['setting_id' => 1]);
                echo json_encode([
                    "response" => TRUE,
                    "message" => "Habilitación realizada con exito"
                ]);
            }
        } catch (Exception $e) {
            log_message('debug','FE SIMBA Habilitar FE: '. $e->getMessage());
        }
    }

    public function getNumbering()
    {
        $technology_provider_configuration = $this->settings_model->get_technology_provider_configuration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
        $jsonNumbering = [];
        $jsonNumbering['NIT'] = $this->Settings->numero_documento;
        $jsonNumbering['Token'] = $technology_provider_configuration->security_token;
        $peticionNumbering = json_encode($jsonNumbering, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
         
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://fe2.simba.co/api_nomina/api/FacturaElectronica/GetNumberingRange',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $peticionNumbering,
            CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
        ]);
        $responseNumberting = curl_exec($curl);
        curl_close($curl);
        log_message('debug', 'FE - SIMBA GetNumbering FE: '. $responseNumberting);
        //$responseNumberting = '{"Error": "true", "Msg": "ff."}'; // <- para pruebas
        try {
            $dataResponseNumbering = json_decode($responseNumberting, false);
            if ($dataResponseNumbering === null) {
                echo json_encode([
                    "response" => FALSE,
                    "message" => "Error en el proceso"
                ]);
            }else {
                $this->db->update('settings', ['getNumbertin_simba_res' => $dataResponseNumbering->Msg, 'getNumbertin_simba_status' => 1], ['setting_id' => 1]);
                echo json_encode([
                    "response" => TRUE,
                    "message" => $dataResponseNumbering->Msg,
                ]);
            }
        } catch (Exception $e) {
            log_message('debug','FE SIMBA Habilitar FE: '. $e->getMessage());
        }
    }


    public function deleteColor($id)
    {
        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        if ($this->form_validation->run() == true) {
            if ($this->Settings->data_synchronization_to_store == ACTIVE) {
                $this->load->integration_model('AttributeValue');
                $AttributeValue = new AttributeValue();
                $integrations = $this->settings_model->getAllIntegrations();
                if (!empty($integrations)) {
                    foreach ($integrations as $integration) {
                        $AttributeValue->open($integration);
                        if (!$AttributeValue->check_attribute_value_assigned($id)) {
                            $AttributeValue->close();
                            $this->session->set_flashdata('error', lang('cant_delete_color_is_assigned_in_tienda_pro'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        $AttributeValue->close();
                    }
                }
            }
            if (!$this->settings_model->validateColorAssigned($id)) {
                $this->session->set_flashdata('error', lang('cant_delete_color_is_assigned'));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }
        if ($this->form_validation->run() == true && $this->settings_model->deleteColor($id)) {
            if ($this->Settings->data_synchronization_to_store == ACTIVE) {
                $color = $this->settings_model->findColor(['id'=>$id]);
                if (!empty($integrations) && !empty($color)) {
                    foreach ($integrations as $integration) {
                        $AttributeValue->open($integration);
                        $AttributeValue->delete_id_wappsi($id);
                        $AttributeValue->close();
                    }
                }
            }
            $this->session->set_flashdata('message', lang('color_deleted'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->sma->checkPermissions('deleteColor');
            $settings = new Settings_model();
            $this->data['color'] = $this->settings_model->findColor(['id'=>$id]);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'settings/delete_color', $this->data);
        }
    }

    public function getState()
    {
        $SettingsModel = new Settings_model();

        $countryId = $this->input->post("countryId");
        $states = $SettingsModel->getAllStates(["PAIS" => $countryId]);
        
        if (!empty($states)) {
            $options = '<option value="">'. lang("alls") .'</option>';
            foreach ($states as $state) {
                $name = ucwords(strtolower($state->DEPARTAMENTO));
                $options .= "<option value=\"$state->CODDEPARTAMENTO\">$name</option>";
            }
        }
        echo $options;
    }

    public function getCities()
    {
        $SettingsModel = new Settings_model();

        $stateId = $this->input->post("stateId");
        $cities = $SettingsModel->getAllCities(["CODDEPARTAMENTO" => $stateId]);
        
        if (!empty($cities)) {
            $options = '<option value="">'. lang("alls") .'</option>';
            foreach ($cities as $city) {
                $name = ucwords(strtolower($city->DESCRIPCION));
                $options .= "<option value=\"$city->CODIGO\">$name</option>";
            }
        }
        echo $options;
    }

    public function activeInactive()
    {
        $SettingsModel = new Settings_model();
        $status = $this->input->post("status");
        $ubications = json_decode($this->input->post("ubications"));

        if (!empty($ubications)) {
            foreach ($ubications as $ubication) {
                $id = $ubication->code;
                $type = $ubication->type;

                if ($type == 'city') {
                    if ($SettingsModel->updateCity(["status" => $status], $id) == false) {
                        echo json_encode(false);
                        exit();
                    }
                }

                if ($type == 'states') {
                    if ($SettingsModel->updateState(["status" => $status], $id) == true) {
                        if ($status == 0) {
                            $SettingsModel->updateCities(["status" => $status], ["CODDEPARTAMENTO" => $id]);
                        }
                    } else {
                        echo json_encode(false);
                        exit();
                    }
                }

                if ($type == 'country') {
                    if ($SettingsModel->updateCountry(["status" => $status], $id)) {
                        if ($status == 0) {
                            $SettingsModel->updateStates(["status" => $status], ["PAIS" => $id]);
                            $SettingsModel->updateCities(["status" => $status], ["PAIS" => $id]);
                        }
                    } else {
                        echo json_encode(false);
                        exit();
                    }
                }
            }
        }

        echo json_encode(true);
    }

    public function activateInactivateUbication()
    {
        $SettingsModel = new Settings_model();

        $id = $this->input->post("id");
        $type = $this->input->post("type");
        $activate = $this->input->post("activate");
        $active = ($activate == "true") ? ACTIVE : INACTIVE;

        if ($type == "city") {
            $result = $SettingsModel->updateCity(["status" => $active], $id);
        } else if ($type == "state") {
            $result = $SettingsModel->updateState(["status" => $active], $id);
            if ($result) {
                $result = $SettingsModel->updateCities(["status" => $active], ["CODDEPARTAMENTO" => $id]);
            }
        } else {
            $result = $SettingsModel->updateCountry(["status" => $active], $id);
            if ($result) {
                $result = $SetttingModel->updateStates(["status" => $active], ["PAIS" => $id]);
                if ($result) {
                    $result = $SetttingModel->updateCities(["status" => $active], ["PAIS" => $id]);
                }
            }
        }

        echo json_encode($result);
    }

    public function syncUbication($type, $data, $process = "create")
    {
        $synchronized = null;

        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $SettingsModel = new Settings_model();
            $integrations = $SettingsModel->getAllIntegrations();

            $synchronized = false;

            if (!empty($integrations)) {
                foreach ($integrations as $integration) {
                    if ($type == 1) {
                        $country = $SettingsModel->findCountry(["CODIGO" => $data["postal_code"]]);
                        $synchronized = $this->syncCountry($integration, $country, $process);
                    }
                    if ($type == 2) {
                        $state = $SettingsModel->findState(["CODDEPARTAMENTO" => $data["postal_code"]]);
                        $synchronized = $this->syncState($integration, $state, $process);
                    }
                    if ($type == 3) {
                        $city = $SettingsModel->findCity(["CODIGO" => $data["postal_code"]]);
                        $synchronized = $this->syncCity($integration, $city, $process);
                    }
                    if ($type == 4) {
                        $zone = $SettingsModel->findZone(["zone_code" => $data["postal_code"]]);
                        $synchronized = $this->syncZone($integration, $zone, $process);
                    }
                    if ($type == 5) {
                        $subzone = $SettingsModel->findSubzone(["subzone_code" => $data["postal_code"]]);
                        $synchronized = $this->syncSubzone($integration, $subzone, $process);
                    }

                }
            }
        }

        return $synchronized;
    }

    public function syncDeleteUbication($type, $code)
    {
        $synchronized = null;

        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $SettingsModel = new Settings_model();
            $integrations = $SettingsModel->getAllIntegrations();

            $synchronized = false;

            if (!empty($integrations)) {
                foreach ($integrations as $integration) {
                    if ($type == 5) {
                        $synchronized = $this->syncSubzone($integration, $code, "delete", $type);
                    } else if ($type == 4) {
                        if ($this->syncSubzone($integration, $code, "delete", $type)) {
                            $synchronized = $this->syncZone($integration, $code, "delete", $type);
                        }
                    } else if ($type == 3) {
                        if ($this->syncSubzone($integration, $code, "delete", $type)) {
                            if ($this->syncZone($integration, $code, "delete", $type)) {
                                $synchronized = $this->syncCity($integration, $code, "delete", $type);
                            }
                        }
                    } else if ($type == 2) {
                        if ($this->syncSubzone($integration, $code, "delete", $type)) {
                            if ($this->syncZone($integration, $code, "delete", $type)) {
                                if ($this->syncCity($integration, $code, "delete", $type)) {
                                    $synchronized = $this->syncState($integration, $code, "delete", $type);
                                }
                            }
                        }
                    } else {
                        if ($this->syncSubzone($integration, $code, "delete", $type)) {
                            if ($this->syncZone($integration, $code, "delete", $type)) {
                                if ($this->syncCity($integration, $code, "delete", $type)) {
                                    if ($this->syncState($integration, $code, "delete", $type)) {
                                        $synchronized = $this->syncCountry($integration, $code, "delete", $type);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $synchronized;
    }

    public function syncUbications()
    {
        $response = false;
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $SettingsModel = new Settings_model();
            $integrations = $SettingsModel->getAllIntegrations();

            if (!empty($integrations)) {
                foreach ($integrations as $integration) {
                    $this->syncDeleteUbications($integration);

                    $response = $this->syncCountries($integration);
                    if ($response) {
                        $response = $this->syncStates($integration);

                        if ($response) {
                            $response = $this->syncCities($integration); 

                            if ($response) {
                                $response = $this->syncZones($integration); 
                            }
                        }   
                    }
                }
            }
        }

        echo json_encode($response);
    }

    private function syncDeleteUbications($integration)
    {
        if ($this->syncDeleteCountries($integration)) {
            if ($this->syncDeleteStates($integration)) {
                if ($this->syncDeleteCities($integration)) {
                    if ($this->syncDeleteZones($integration)) {
                        if ($this->syncDeleteSubzones($integration)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public function syncDeleteCountries($integration) 
    {
        $this->load->integration_model('Country');

        $Country = new Country();
        $Country->open($integration);

        $response = $Country->truncate();
        $Country->close();

        return $response;
    }

    public function syncDeleteStates($integration) 
    {
        $this->load->integration_model('State');

        $State = new State();
        $State->open($integration);

        $response = $State->truncate();
        $State->close();

        return $response;
    }

    public function syncDeleteCities($integration) 
    {
        $this->load->integration_model('City');

        $City = new City();
        $City->open($integration);

        $response = $City->truncate();
        $City->close();

        return $response;
    }

    public function syncDeleteZones($integration) 
    {
        $this->load->integration_model('CityZone');

        $CityZone = new CityZone();
        $CityZone->open($integration);

        $response = $CityZone->truncate();
        $CityZone->close();

        return $response;
    }

    public function syncDeleteSubZones($integration) 
    {
        $this->load->integration_model('SubZone');

        $SubZone = new SubZone();
        $SubZone->open($integration);

        $response = $SubZone->truncate();
        $SubZone->close();

        return $response;
    }

    public function syncCountries($integration)
    {
        $response = false;
        $SettingsModel = new Settings_model();
        $countries = $SettingsModel->getAllCountries();

        if (!empty($countries)) {
            foreach ($countries as $country) {
                $response = $this->syncCountry($integration, $country);
            }
        }

        return $response;
    }

    public function syncCountry($integration, $country, $process = "create", $type = false)
    {
        $this->load->integration_model('Country');

        $Country = new Country();
        $Country->open($integration);

        if ($process == "create") {
            $data = [
                "code" => $country->codigo_iso,
                "name" => ucfirst(strtolower($country->NOMBRE)),
                "id_wappsi" => $country->CODIGO,
            ];
            $response = $Country->create($data);
        } 

        if ($process == "edit") {
            $countryStore = $Country->find(["id_wappsi" => $country->CODIGO]);
            $data = [
                "code" => $country->codigo_iso,
                "name" => ucfirst(strtolower($country->NOMBRE)),
                "status" => $country->status,
            ];
            $response = $Country->update($data, $countryStore->id);
        } 
        
        if ($process == "delete") {
            $countryStore = $Country->find(["id_wappsi" => $country]);
            $data = ["id" => $countryStore->id];

            $response = $Country->delete($data);
        }

        $Country->close();

        return $response;
    }
    
    public function syncStates($integration)
    {
        $response = false;
        $SettingsModel = new Settings_model();
        $states = $SettingsModel->getAllStates();

        if (!empty($states)) {
            foreach ($states as $state) {
                $response = $this->syncState($integration, $state);
            }
        }

        return $response;
    }

    public function syncState($integration, $state, $process = "create", $type = false)
    {
        $this->load->integration_model('State');
        $State = new State();
        $State->open($integration);
        
        $this->load->integration_model('Country');
        $Country = new Country();
        $Country->open($integration);

        if ($process == "create") {
            $countryStore = $Country->find(["id_wappsi" => $state->PAIS]);
    
            $data = [
                "name" => ucfirst(strtolower($state->DEPARTAMENTO)),
                "country_id" => $countryStore->id,
                "statesCode" => $state->CODDEPARTAMENTO,
                "id_wappsi" => $state->CODDEPARTAMENTO,
            ];

            $response = $State->create($data);
        }

        if ($process == "edit") {
            $countryStore = $Country->find(["id_wappsi" => $state->PAIS]);
            $stateStore = $State->find(["id_wappsi" => $state->CODDEPARTAMENTO]);

            $data = [
                "name" => ucfirst(strtolower($state->DEPARTAMENTO)),
                "country_id" => $countryStore->id,
                "staus" => $state->status,
            ];

            $response = $State->update($data, $stateStore->id);
        }

        if ($process == "delete") {
            if ($type == 1) {
                $countryStore = $Country->find(["id_wappsi" => $state]);
                $data = ["country_id" => $countryStore->id];
            } else {
                $stateStore = $State->find(["id_wappsi" => $state]);
                $data = ["id" => $stateStore->id];
            }

            $response = $State->delete($data);
        }

        $State->close();
        $Country->close();

        return $response;
    }

    public function syncCities($integration)
    {
        $response = false;
        $SettingsModel = new Settings_model();
        $cities = $SettingsModel->getAllCities();

        if (!empty($cities)) {
            foreach ($cities as $city) {
                $response = $this->syncCity($integration, $city);
            }
        }

        return $response;
    }

    public function syncCity($integration, $city, $process = "create", $type = false)
    {
        $this->load->integration_model('City');
        $City = new City();
        $City->open($integration);

        $this->load->integration_model('State');
        $State = new State();
        $State->open($integration);

        $this->load->integration_model('Country');
        $Country = new Country();
        $Country->open($integration);
        
        if ($process == "create") {
            $stateStore = $State->find(["id_wappsi" => $city->CODDEPARTAMENTO]);
            $countryStore = $Country->find(["id_wappsi" => $city->PAIS]);
    
            $data = [
                "name" => ucfirst(strtolower($city->DESCRIPCION)),
                "postalCode" => $city->CODIGO,
                "state_id" => $stateStore->id,
                "country_id" => $countryStore->id,
                "id_wappsi" => $city->CODIGO,
            ];
            $response = $City->create($data);
        }
        
        if ($process == "edit") {
            $countryStore = $Country->find(["id_wappsi" => $city->PAIS]);
            $stateStore = $State->find(["id_wappsi" => $city->CODDEPARTAMENTO]);
            $cityStore = $City->find(["id_wappsi" => $city->CODIGO]);

            $data = [
                "name" => ucfirst(strtolower($city->DESCRIPCION)),
                "state_id" => $stateStore->id,
                "country_id" => $countryStore->id,
                "status" => $city->status
            ];

            $response = $City->update($data, $cityStore->id);
        }

        if ($process == "delete") {
            if ($type == 1) {
                $countryStore = $Country->find(["id_wappsi" => $city]);
                $data = ["country_id" => $countryStore->id];
            } else if ($type == 2) {
                $stateStore = $State->find(["id_wappsi" => $city]);
                $data = ["state_id" => $stateStore->id];
            } else {
                $cityStore = $City->find(["id_wappsi" => $city]);
                $data = ["id" => $cityStore->id];
            }

            $response = $City->delete($data);
        }
        
        $City->close();
        $State->close();
        $Country->close();

        return $response;
    }

    public function syncZones($integration)
    {
        $response = false;
        $SettingsModel = new Settings_model();
        $zones = $SettingsModel->getAllZones();

        if (!empty($zones)) {
            foreach ($zones as $zone) {
                $response = $this->syncZone($integration, $zone);
            }
        } else {
            $response = true;
        }

        return $response;
    }

    public function syncZone($integration, $zone, $process = "create", $type = false) 
    {
        $this->load->integration_model('CityZone');
        $CityZone = new CityZone();
        $CityZone->open($integration);

        $this->load->integration_model('City');
        $City = new City();
        $City->open($integration);

        $this->load->integration_model('State');
        $State = new State();
        $State->open($integration);

        $this->load->integration_model('Country');
        $Country = new Country();
        $Country->open($integration);

        if ($process == "create") {
            $countryStore = $Country->find(["id_wappsi" => $zone->country_code]);
            $stateStore = $State->find(["id_wappsi" => $zone->state_code]);
            $cityStore = $City->find(["id_wappsi" => $zone->codigo_ciudad]);
    
            $data = [
                "name" => ucfirst(strtolower($zone->zone_name)),
                "id_city" => $cityStore->id,
                "id_state" => $stateStore->id,
                "id_country" => $countryStore->id,
                "id_wappsi" => $zone->zone_code,
            ];
            $response = $CityZone->create($data);
        }

        if ($process == "edit") {
            $countryStore = $Country->find(["id_wappsi" => $zone->country_code]);
            $stateStore = $State->find(["id_wappsi" => $zone->state_code]);
            $cityStore = $City->find(["id_wappsi" => $zone->codigo_ciudad]);
            $cityZoneStore = $CityZone->find(["id_wappsi" => $zone->zone_code]);

            $data = [
                "name" => ucfirst(strtolower($zone->zone_name)),
                "id_city" => $cityStore->id,
                "id_state" => $stateStore->id,
                "id_country" => $countryStore->id,
                "id_wappsi" => $zone->zone_code,
            ];

            $response = $CityZone->update($data, $cityZoneStore->id);
        }

        if ($process == "delete") {
            if ($type == 1) {
                $countryStore = $Country->find(["id_wappsi" => $zone]);
                $data = ["id_country" => $countryStore->id];
            } else if ($type == 2) {
                $stateStore = $State->find(["id_wappsi" => $zone]);
                $data = ["id_state" => $stateStore->id];
            } else if ($type == 3) {
                $cityStore = $City->find(["id_wappsi" => $zone]);
                $data = ["id_city" => $cityStore->id];
            } else {
                $cityZoneStore = $CityZone->find(["id_wappsi" => $zone]);
                $data = ["id" => $cityZoneStore->id];
            }

            $response = $CityZone->delete($data);
        }

        $CityZone->close();
        $City->close();
        $State->close();
        $Country->close();

        return $response;
    }

    public function syncSubzone($integration, $subZone, $process = "create", $type = false)
    {
        $this->load->integration_model('SubZone');
        $SubZone = new SubZone();
        $SubZone->open($integration);

        $this->load->integration_model('CityZone');
        $CityZone = new CityZone();
        $CityZone->open($integration);

        $this->load->integration_model('City');
        $City = new City();
        $City->open($integration);

        $this->load->integration_model('State');
        $State = new State();
        $State->open($integration);
        
        $this->load->integration_model('Country');
        $Country = new Country();
        $Country->open($integration);

        if ($process == "create") {
            $countryStore = $Country->find(["id_wappsi" => $subZone->country_code]);
            $stateStore = $State->find(["id_wappsi" => $subZone->state_code]);
            $cityStore = $City->find(["id_wappsi" => $subZone->city_code]);
            $cityZoneStore = $CityZone->find(["id_wappsi" => $subZone->zone_code]);
    
            $data = [
                "name" => ucfirst(strtolower($subZone->subzone_name)),
                "id_city_zones" => $cityZoneStore->id,
                "id_city" => $cityStore->id,
                "id_state" => $stateStore->id,
                "id_country" => $countryStore->id,
                "id_wappsi" => $subZone->subzone_code,
            ];
            $response = $SubZone->create($data);
        }
        
        if ($process == "edit") {
            $countryStore = $Country->find(["id_wappsi" => $subZone->country_code]);
            $stateStore = $State->find(["id_wappsi" => $subZone->state_code]);
            $cityStore = $City->find(["id_wappsi" => $subZone->city_code]);
            $cityZoneStore = $CityZone->find(["id_wappsi" => $subZone->zone_code]);
            $subZoneStore = $SubZone->find(["id_wappsi" => $subZone->subzone_code]);
    
            $data = [
                "name" => ucfirst(strtolower($subZone->subzone_name)),
                "status" => $subZone->status,
                "id_city_zones" => $cityZoneStore->id,
                "id_city" => $cityStore->id,
                "id_state" => $stateStore->id,
                "id_country" => $countryStore->id,
            ];
            $response = $SubZone->update($data, $subZoneStore->id);
        }

        if ($process == "delete") {
            if ($type == 1) {
                $countryStore = $Country->find(["id_wappsi" => $subZone]);
                $data = ["id_country" => $countryStore->id];
            } else if ($type == 2) {
                $stateStore = $State->find(["id_wappsi" => $subZone]);
                $data = ["id_state" => $stateStore->id];
            } else if ($type == 3) {
                $cityStore = $City->find(["id_wappsi" => $subZone]);
                $data = ["id_city" => $cityStore->id];
            } else if ($type == 4) {
                $cityZoneStore = $CityZone->find(["id_wappsi" => $subZone]);
                $data = ["id_city_zones" => $cityZoneStore->id];
            } else {
                $subZoneStore = $SubZone->find(["id_wappsi" => $subZone]);
                $data = ["id" => $subZoneStore->id];
            }
            $response = $SubZone->delete($data);
        }

        $SubZone->close();
        $CityZone->close();
        $City->close();
        $State->close();
        $Country->close();

        return $response;
    }
}