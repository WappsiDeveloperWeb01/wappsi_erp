<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentsTypes_model extends CI_Model
{
    private $tableName = 'documents_types';

	public function __construct()
	{
		parent::__construct();
	}

    public function getReference($documentTypeId)
    {
        $documentType = $this->getDocumentType(["id" => $documentTypeId]);
        $referenceNo = $documentType->sales_prefix ."-". $documentType->sales_consecutive;
        return (object) ["referenceNo" => $referenceNo, "consecutive" => $documentType->sales_consecutive];
    }

    public function get_credit_note_type_document($branch_id, $module, $is_electronic_invoice = NULL)
    {
    	$this->db->select("documents_types.id AS document_type_id, sales_prefix AS document_type_prefix, factura_electronica AS is_electronic_document, module");
    	$this->db->join("biller_documents_types", "biller_documents_types.document_type_id = documents_types.id", "inner");
    	$this->db->where("biller_id", $branch_id);

        if (!empty($is_electronic_invoice)) {
            $this->db->where("documents_types.factura_electronica", $is_electronic_invoice);
        }

        if (!empty($module)) {
            if ($module == FACTURA_DETAL) {
                $this->db->where("module", NOTA_CREDITO_DETAL);
            } else if ($module == FACTURA_POS) {
                $this->db->where("module", NOTA_CREDITO_POS);
            }
        } else {
    	   $this->db->where_in("module", [NOTA_CREDITO_POS, NOTA_CREDITO_DETAL]);
        }

    	$response = $this->db->get("documents_types");

        return $response->result();
    }

    public function get_debit_note_type_document($branch_id, $module)
    {
        $this->db->select("documents_types.id AS document_type_id, sales_prefix AS document_type_prefix, factura_electronica AS is_electronic_document, module");
        $this->db->from("documents_types");
        $this->db->join("biller_documents_types", "biller_documents_types.document_type_id = documents_types.id", "inner");
        $this->db->where("biller_id", $branch_id);
        $this->db->where_in("module", $module);

        $response = $this->db->get();

        return $response->result();
    }

    public function getDocumentType($data)
    {
        $this->db->where($data);
        $response = $this->db->get("documents_types");

        return $response->row();
    }

    public function getDocumentTypeBiller($data)
    {
        $this->db->select("{$this->tableName}.*");
        $this->db->where($data);
        $this->db->join("biller_documents_types bdt", "bdt.document_type_id = {$this->tableName}.id");
        $response = $this->db->get($this->tableName);

        return $response->row();
    }


    public function getDocumentsTypeBiller($data)
    {
        $this->db->select("{$this->tableName}.*");
        $this->db->join("biller_documents_types bdt", "bdt.document_type_id = {$this->tableName}.id");
        $this->db->where($data);

        if (isset($data["u.id"]) && !empty($data["u.id"])) {
            $this->db->join("users u", "
            u.document_type_id          = {$this->db->dbprefix($this->tableName)}.id OR
            u.pos_document_type_id      = {$this->db->dbprefix($this->tableName)}.id OR
            u.fe_pos_document_type_id   = {$this->db->dbprefix($this->tableName)}.id");
        }

        $response = $this->db->get($this->tableName);

        return $response->result();
    }

    // public function getAdjustmentDocumentType()
    // {
    //     $this->db->where("module", 57);
    //     $response = $this->db->get("documents_types");

    //     return $response->row();
    // }

    // public function getTrasferDocumentType()
    // {
    //     $this->db->where("module", 58);
    //     $response = $this->db->get("documents_types");

    //     return $response->row();
    // }

    public function updateConsecutiveDocumentType($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("documents_types", $data)) {
            return TRUE;
        }

        return FALSE;
    }

}

/* End of file DocumentTypes_model.php */
/* Location: .//opt/lampp/htdocs/wappsi/app/models/admin/DocumentTypes_model.php */