<?php

/***********

FACTURA DE VENTA CON DATOS DE LA EMPRESA, DE ACUERDO A LO PARAMETRIZADO EN EL TIPO DE DOCUMENTO SE DISCRIMINA O NO EL IVA

 ************/

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath . 'vendor/fpdf/fpdf.php';
require_once $apppath . 'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        //izquierda
        if (file_exists('assets/uploads/logos/' . $this->logo)) {
            if ($this->biller_logo == 2) {
                $this->Image(base_url() . 'assets/uploads/logos/' . $this->logo, 27, 7, 28);
            } else {
                $this->Image(base_url() . 'assets/uploads/logos/' . $this->logo, 14, 12, 55);
            }
        }

        if ($this->show_document_type_header == 1) {
            $this->SetFont('Arial', '', $this->fuente);
            $this->setXY(13, 36);
            $this->MultiCell(123, 3,  $this->sma->utf8Decode($this->crop_text($this->invoice_header, 170)), 0, 'L');
        }

        $cx = 75;
        $cy = 10;
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->MultiCell(75, 3.5,  $this->sma->utf8Decode(mb_strtoupper($this->biller->company != '-' ? $this->biller->company : $this->biller->name)), '', 'L');
        $cy  = $this->getY();
        $cy += 1.3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $this->Cell(72, 3, $this->sma->utf8Decode('Nit : ' . $this->biller->vat_no . ($this->biller->tipo_documento == 6 ? "-" . $this->biller->digito_verificacion : '') . "  " . $this->tipo_regimen), '', 1, 'L');
        if ($this->ciiu_code) {
            $cy += 3;
            $this->setXY($cx, $cy);
            // $this->Cell(72, 3 , $this->sma->utf8Decode("Código CIIU : ".$this->ciiu_code->code),'',1,'L');
        }
        if ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
            $cy += 3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3, $this->sma->utf8Decode(
                ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? 'Auto retenedor de ' : '') .
                    ($this->Settings->fuente_retainer ? 'Fuente' : '') .
                    ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '') . 'IVA ' : '') .
                    ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '') . 'ICA ' : '')
            ), '', 1, 'L');
        }
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3, $this->sma->utf8Decode($this->biller->address), '', 1, 'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3, $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city)) . " - " . ucfirst(mb_strtolower($this->biller->state))), '', 1, 'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3, $this->sma->utf8Decode('Teléfonos : ' . $this->biller->phone), '', 1, 'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3, $this->sma->utf8Decode('Correo : ' . $this->biller->email), '', 1, 'L');

        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3, $this->sma->utf8Decode(isset($this->Settings->url_web) ? $this->Settings->url_web : ''), '', 1, 'L');

        //derecha
        $this->SetFont('Arial', 'B', $this->fuente + (6));
        $this->RoundedRect(138, 10, 71, 31, 3, '1234', '');
        $cx = 140;
        $cy = 15;
        $this->setXY($cx, $cy);
        $this->MultiCell(40, 5,  $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'FACTURA DE VENTA'), '', 'L');
        $cy += 13;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10, $this->sma->utf8Decode($this->factura->reference_no), '', 1, 'L');

        //izquierda
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->setFillColor($this->color_R, $this->color_G, $this->color_B);
        $this->RoundedRect(13, 43, 125, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 48, 125, 27, 3, '4', '');
        $cx = 13;
        $cy = 43;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5, $this->sma->utf8Decode('Información del cliente'), 'B', 1, 'C');
        $cx += 3;
        $cy += 8;
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->setXY($cx, $cy);
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name . ($this->customer->second_name != '' ? ' ' . $this->customer->second_name : '') . ($this->customer->first_lastname != '' ? ' ' . $this->customer->first_lastname : '') . ($this->customer->second_lastname != '' ? ' ' . $this->customer->second_lastname : '');
        }
        $this->MultiCell(115, 3, $this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_l, 3, $this->sma->utf8Decode("Nit/Cc : "), '', 0, 'L');
        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_r, 3, $this->sma->utf8Decode($this->customer->vat_no . ($this->customer->digito_verificacion != '' ? '-' . $this->customer->digito_verificacion : '')), '', 1, 'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_l, 3, $this->sma->utf8Decode("Sucursal : "), '', 0, 'L');
        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_r, 3, $this->sma->utf8Decode($this->factura->negocio), '', 1, 'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_l, 3, $this->sma->utf8Decode("Dirección : "), '', 0, 'L');
        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_r, 3, $this->sma->utf8Decode($this->factura->direccion_negocio . ", " . (ucfirst(mb_strtolower($this->factura->ciudad_negocio)))), '', 1, 'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_l, 3, $this->sma->utf8Decode("Teléfonos : "), '', 0, 'L');
        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_r, 3, $this->sma->utf8Decode($this->factura->phone_negocio), '', 0, 'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_l, 3, $this->sma->utf8Decode("Correo : "), '', 0, 'L');
        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $this->Cell($columns_r, 3, $this->sma->utf8Decode($this->customer->email), '', 1, 'L');
        if ($this->factura->contact_name) {
            $cy += 3;
            $this->setXY($cx, $cy);
            $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
            $this->Cell($columns_l, 3, $this->sma->utf8Decode("Contacto : "), '', 0, 'L');
            $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
            $this->Cell($columns_r, 3, $this->sma->utf8Decode($this->factura->contact_name), '', 1, 'L');
        }
        //derecha
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $altura = 5;
        $adicional_altura = 1;
        $this->setFillColor($this->color_R, $this->color_G, $this->color_B);
        $this->RoundedRect(138, 43, 71, 5, 3, '2', 'DF');
        $this->RoundedRect(138, 48, 71, 27, 3, '3', '');
        $cx = 138;
        $cy = 43;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura, $this->sma->utf8Decode('Fecha Factura'), 'BR', 1, 'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura, $this->sma->utf8Decode('Total'), 'B', 1, 'C');

        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura + $adicional_altura, $this->sma->utf8Decode($this->factura->date), 'BR', 1, 'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura + $adicional_altura, $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, $this->factura->grand_total * $this->trmrate)), 'B', 1, 'C');
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->setFillColor($this->color_R, $this->color_G, $this->color_B);
        $cy += 5 + $adicional_altura;
        $cx -= 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura, $this->sma->utf8Decode('Fecha Vencimiento'), 'LBR', 1, 'C', 1);
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura, $this->sma->utf8Decode('Medio de Pago'), 'LBR', 1, 'C', 1);
        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+" . $this->factura->payment_term . " day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(35.5, $altura + $adicional_altura, $this->sma->utf8Decode($fecha_exp), 'BR', 1, 'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->payment_method),'B',1,'C');
        $this->setFillColor($this->color_R, $this->color_G, $this->color_B);
        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $cy += 5 + $adicional_altura;
        $cx -= 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(71, $altura, $this->sma->utf8Decode('Vendedor'), 'TLBR', 1, 'C', 1);
        $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
        $cy += $altura;
        $this->setXY($cx, $cy);
        if ($this->seller) {
            $this->Cell(71, $altura, $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))), '', 1, 'C');
        } else {
            $this->Cell(71, $altura, $this->sma->utf8Decode('Ventas Varias'), '', 1, 'C');
        }

        $this->ln(1.7);

        $this->SetFont('Arial', 'B', $this->fuente + $this->adicional_fuente);
        $this->setFillColor($this->color_R, $this->color_G, $this->color_B);
        $this->Cell(17, 5, $this->sma->utf8Decode('Código'), 'TBLR', 0, 'C', 1);
        $this->Cell(79, 5, $this->sma->utf8Decode('Descripción de producto o Servicio'), 'TBLR', 0, 'C', 1);
        $this->Cell(8.5, 5, $this->sma->utf8Decode('Cant.'), 'TBLR', 0, 'C', 1);
        $this->Cell(8.5, 5, $this->sma->utf8Decode('U.M.'), 'TBLR', 0, 'C', 1);
        if ($this->tax_inc) {
            $unit_price_size = 27.2;
            $this->Cell(17, 5, $this->sma->utf8Decode('Impuesto'), 'TBLR', 0, 'C', 1);
        } else {
            $unit_price_size = 44.2;
        }
        $this->Cell($unit_price_size, 5, $this->sma->utf8Decode('Valor Unitario'), 'TBLR', 0, 'C', 1);

        $this->Cell(39.2, 5, $this->sma->utf8Decode('Valor total'), 'TBLR', 1, 'C', 1);
    }

    function Footer()
    {
        // Print centered page number
        if ($this->description2 != '') {
            // $this->SetXY(15, -70);
            $this->SetFont('Arial', '', $this->fuente + $this->adicional_fuente);
            $this->SetXY(14, -24);
            $this->RoundedRect(13, 254, 196, 10, 0.8, '1234', '');
            $this->Cell(194, 4, $this->sma->utf8Decode($this->factura->resolucion), '', 1, 'C');
            // $this->Cell(194, 4 , $this->sma->utf8Decode('CUFE: '. $this->factura->cufe),'',1,'C');
        } else {
            $this->SetXY(14, -23);
            $this->RoundedRect(13, 255, 196, 10, 1, '1234', '');
            $this->Cell(194, 4, $this->sma->utf8Decode($this->factura->resolucion), '', 1, 'C');
            // $this->Cell(194, 4 , $this->sma->utf8Decode("CUFE : ".$this->factura->cufe),'',1,'C');
        }


        $this->SetXY(13, -14);
        $img_x = $this->getX() + 125;
        $img_y = $this->getY() + 1;
        $this->Cell(125, 7, $this->sma->utf8Decode('Impreso por Wappsi © ' . date('Y') . ' Web Apps Innovation SAS NIT 901.090.070-9     www.wappsi.com'), '', 1, 'L');
        $this->Image(base_url() . 'assets/images/cadena_logo.jpeg', $img_x, $img_y, 45);
        $this->SetXY(195, -14);
        $this->Cell(7, 7, $this->sma->utf8Decode('Página N° ' . $this->PageNo()), '', 1, 'C');
    }

    function reduceTextToDescription1($text)
    {
        $text = $text;
        if (strlen($text) > 980) {
            $text = substr($text, 0, 975);
            $text .= "...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text)
    {
        if (strlen($text) > 870) {
            $text = substr($text, 0, 865);
            $text .= "...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function crop_text($text, $length = 50)
    {
        if (strlen($text) > $length) {
            $text = substr($text, 0, ($length - 5));
            $text .= "...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op = 'S';
        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m', ($x + $r) * $k, ($hp - $y) * $k));

        $xc = $x + $w - $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - $y) * $k));
        if (strpos($corners, '2') === false)
            $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $y) * $k));
        else
            $this->_Arc($xc + $r * $MyArc, $yc - $r, $xc + $r, $yc - $r * $MyArc, $xc + $r, $yc);

        $xc = $x + $w - $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $yc) * $k));
        if (strpos($corners, '3') === false)
            $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - ($y + $h)) * $k));
        else
            $this->_Arc($xc + $r, $yc + $r * $MyArc, $xc + $r * $MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x + $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - ($y + $h)) * $k));
        if (strpos($corners, '4') === false)
            $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - ($y + $h)) * $k));
        else
            $this->_Arc($xc - $r * $MyArc, $yc + $r, $xc - $r, $yc + $r * $MyArc, $xc - $r, $yc);

        $xc = $x + $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $yc) * $k));
        if (strpos($corners, '1') === false) {
            $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $y) * $k));
            $this->_out(sprintf('%.2F %.2F l', ($x + $r) * $k, ($hp - $y) * $k));
        } else
            $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf(
            '%.2F %.2F %.2F %.2F %.2F %.2F c ',
            $x1 * $this->k,
            ($h - $y1) * $this->k,
            $x2 * $this->k,
            ($h - $y2) * $this->k,
            $x3 * $this->k,
            ($h - $y3) * $this->k
        ));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 1.5;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R, $color_G, $color_B);
$pdf->biller = $biller;

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->ciiu_code = $ciiu_code;
$pdf->invoice_header = $invoice_header ? $invoice_header : "";
$pdf->show_document_type_header = $show_document_type_header;
$pdf->value_decimals = $value_decimals;
$pdf->payment_method = $payment_method->name;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();

if ($document_type->invoice_footer != '') {
    $maximo_footer = 220;
} else {
    $maximo_footer = 245;
}


$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial', '', $fuente + $plus_font_size);


$taxes = [];
// for ($i=0; $i < 50 ; $i++) {
foreach ($rows as $item) {
    $columns = [];
    if ($pdf->getY() > $maximo_footer) {
        $pdf->AddPage();
    }
    $inicio_fila_X = $pdf->getX();
    $inicio_fila_Y = $pdf->getY();
    $columns[1]['inicio_X'] = $pdf->getX();
    $pdf->Cell(17, 5, $this->sma->utf8Decode($show_code == 1 ? $item->product_code : $item->reference), '', 0, 'C');
    $columns[1]['fin_X'] = $pdf->getX();
    $pr_name =  $item->product_name .''.
        (!is_null($item->variant) ? "( " . $item->variant . " )" : '') .
        ($this->Settings->show_brand_in_product_search ? " - " . $item->brand_name : '') .
        (!is_null($item->serial_no) ? " - " . $item->serial_no : '') .
        strip_tags($this->sma->decode_html(($show_product_preferences == 1 && $item->preferences ? ' (' . $this->sma->print_preference_selection($item->preferences) . ')' : '')));
    $cX = $pdf->getX();
    $cY = $pdf->getY();
    $inicio_altura_fila = $cY;
    $columns[2]['inicio_X'] = $pdf->getX();
    $pdf->setXY($cX, $cY + 0.5);
    $pdf->MultiCell(79, 3, $this->sma->utf8Decode($pr_name), '', 'L');
    $columns[2]['fin_X'] = $cX + 79;
    $fin_altura_fila = $pdf->getY();
    $pdf->setXY($cX + 79, $cY);
    $columns[3]['inicio_X'] = $pdf->getX();
    $qty = $item->quantity;
    $price = $item->unit_price;
    if (isset($item->operator)) {
        if ($item->operator == "*") {
            $qty = $item->quantity / $item->operation_value;
            $price = $item->unit_price * $item->operation_value;
        } else if ($item->operator == "/") {
            $qty = $item->quantity * $item->operation_value;
            $price = $item->unit_price / $item->operation_value;
        } else if ($item->operator == "+") {
            $qty = $item->quantity - $item->operation_value;
            $price = $item->unit_price - $item->operation_value;
        } else if ($item->operator == "-") {
            $qty = $item->quantity + $item->operation_value;
            $price = $item->unit_price + $item->operation_value;
        }
    }
    $pdf->Cell(8.5, 5, $this->sma->utf8Decode($this->sma->formatQuantity($qty, $qty_decimals)), '', 0, 'C');
    $columns[3]['fin_X'] = $pdf->getX();

    $columns[4]['inicio_X'] = $pdf->getX();
    $pdf->Cell(8.5, 5, $this->sma->utf8Decode($item->product_unit_code), '', 0, 'C');
    $columns[4]['fin_X'] = $pdf->getX();
    if (!isset($taxes[$item->tax_rate_id])) {
        $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
        $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
    } else {
        $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
        $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
    }
    if ($tax_inc) {
        $unit_price_size = 27.2;
        $columns[5]['inicio_X'] = $pdf->getX();
        $pdf->Cell(17, 5, $this->sma->utf8Decode(isset($taxes_details[$item->tax_rate_id]) ? $taxes_details[$item->tax_rate_id] : "Exento"), '', 0, 'R');
        $columns[5]['fin_X'] = $pdf->getX();
    } else {
        $unit_price_size = 44.2;
    }
    if ($view_tax) {

        $columns[6]['inicio_X'] = $pdf->getX();
        $pdf->Cell($unit_price_size, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $price * $trmrate)), '', 0, 'R');
        $columns[6]['fin_X'] = $pdf->getX();
        $columns[7]['inicio_X'] = $pdf->getX();
        $pdf->Cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($item->unit_price * $item->quantity) * $trmrate)), '', 0, 'R');
        $columns[7]['fin_X'] = $pdf->getX();
    } else {
        $columns[6]['inicio_X'] = $pdf->getX();
        $pdf->Cell($unit_price_size, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($item->net_unit_price * $trmrate))), '', 0, 'R');
        $columns[6]['fin_X'] = $pdf->getX();
        $columns[7]['inicio_X'] = $pdf->getX();
        $pdf->Cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($item->net_unit_price * $item->quantity) * $trmrate)), '', 0, 'R');
        $columns[7]['fin_X'] = $pdf->getX();
    }
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = $fin_altura_fila - $inicio_altura_fila;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    // $this->sma->print_arrays($columns);
    foreach ($columns as $key => $data) {
        $altura_fila = $altura_fila >= 5 ? $altura_fila : 5;
        $ancho_column = $data['fin_X'] - $data['inicio_X'];
        $pdf->Cell($ancho_column, $altura_fila, $this->sma->utf8Decode(''), 'LBR', 0, 'R');
    }
    $pdf->ln($altura_fila);
}
// }

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;
// $pdf->RoundedRect($current_x, $current_y, 111.6, 12.25, 3, '1234', '');
$pdf->ln(1);
$pdf->setX(16);
$pdf->SetFont('Arial', 'B', $fuente);
$pdf->Cell(115.6, 5, $this->sma->utf8Decode('VALOR (En letras)'), '', 1, 'L');

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(16);
$pdf->SetFont('Arial', '', $fuente);
$pdf->MultiCell(111.6, 4, $this->sma->utf8Decode($number_convert->convertir(($inv->grand_total * $trmrate), ($this->Settings->default_currency))), '', 'L');
$pdf->SetFont('Arial', '', $fuente);
$pdf->setX(13);

$pdf->setXY($current_x, $current_y + 9);


$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->SetFont('Arial', 'B', $fuente);
$pdf->Cell(113.4, 0, $this->sma->utf8Decode(''), 'B', 1, 'C');
$pdf->Cell(28.89, 5, $this->sma->utf8Decode('Tipo Impuesto'), '', 0, 'C');
$pdf->Cell(28.89, 5, $this->sma->utf8Decode('Valor Base'), '', 0, 'C');
$pdf->Cell(28.89, 5, $this->sma->utf8Decode('Valor Impuesto'), '', 0, 'C');
$pdf->Cell(28.89, 5, $this->sma->utf8Decode('Valor Total'), '', 1, 'C');

$pdf->SetFont('Arial', '', $fuente);
$total_tax_base = 0;
$total_tax_amount = 0;

foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $total = $arr['base'] + $arr['tax'];
    $pdf->Cell(28.89, 3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"), '', 1, 'C');
    $pdf->setXY($current_x + 28.89, $current_y);
    $pdf->setX($current_x + 28.89);
    $pdf->Cell(28.89, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['base'] * $trmrate)), '', 1, 'C');
    $total_tax_base += $arr['base'] * $trmrate;
    $pdf->setXY($current_x + (28.89 * 2), $current_y);
    $pdf->setX($current_x + (28.89 * 2));
    $pdf->Cell(28.89, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['tax'] * $trmrate)), '', 1, 'C');
    $total_tax_amount += $arr['tax'] * $trmrate;
    $pdf->setXY($current_x + (28.89 * 3), $current_y);
    $pdf->setX($current_x + 28.89 * 3);
    $pdf->Cell(28.89, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total)), '', 1, 'C');
}


if ($inv->order_tax > 0) {

    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $pdf->Cell(28.9, 3, $this->sma->utf8Decode("(SP) " . $this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id] . " %" : "0%"))), '', 1, 'C');
    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)), '', 1, 'C');
    $total_tax_base +=  $inv->total * $trmrate;

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax * $trmrate)), '', 1, 'C');
    $total_tax_amount += $inv->order_tax * $trmrate;

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total * $trmrate)), '', 1, 'C');
}


$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->SetFont('Arial', 'B', $fuente);
$pdf->Cell(28.9, 3, $this->sma->utf8Decode("Total"), '', 1, 'C');
$pdf->setXY($current_x + 28.9, $current_y);
$pdf->setX($current_x + 28.9);
$pdf->Cell(28.9, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base)), '', 1, 'C');

$pdf->setXY($current_x + (28.9 * 2), $current_y);
$pdf->setX($current_x + (28.9 * 2));
$pdf->Cell(28.9, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_amount)), '', 1, 'C');

$pdf->setXY($current_x + (28.9 * 3), $current_y);
$pdf->setX($current_x + 28.9 * 3);
$pdf->Cell(28.9, 3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base + $total_tax_amount)), '', 1, 'C');
$pdf->SetFont('Arial', '', $fuente);

$tax_summary_end = $pdf->getY() + 3;
//derecha

$pdf->setXY($cX_items_finished + 113, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$l_column = 44.2;
$r_column = 39.2;

$pdf->cell($l_column, 5, $this->sma->utf8Decode('Total bruto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x + $l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->total * $trmrate))), 'TBR', 1, 'R');

$current_y += 5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($l_column, 5, $this->sma->utf8Decode('Valor Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x + $l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax + $inv->product_tax * $trmrate)), 'TBR', 1, 'R');

$current_y += 5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención en la fuente ' . ($inv->rete_fuente_percentage > 0 ? $this->sma->formatQuantity($inv->rete_fuente_percentage, $qty_decimals) . " %" : "")), 'TBLR', 1, 'L');
$pdf->setXY($current_x + $l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');

if ($inv->rete_iva_total > 0) {
    $current_y += 5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención al IVA ' . ($inv->rete_iva_percentage > 0 ? $this->sma->formatQuantity($inv->rete_iva_percentage, $qty_decimals) . " %" : "")), 'TBLR', 1, 'L');
    $pdf->setXY($current_x + $l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_total > 0) {
    $current_y += 5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención al ICA ' . ($inv->rete_ica_percentage > 0 ? $this->sma->formatQuantity($inv->rete_ica_percentage, $qty_decimals) . " %" : "")), 'TBLR', 1, 'L');
    $pdf->setXY($current_x + $l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_other_total > 0) {
    $current_y += 5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode(lang('rete_other') . " " . ($inv->rete_other_percentage > 0 ? $this->sma->formatQuantity($inv->rete_other_percentage, $qty_decimals) . " %" : "")), 'TBLR', 1, 'L');
    $pdf->setXY($current_x + $l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
}

// if ($inv->rete_bomberil_total > 0) {
//     $current_y+=5;
//     $pdf->setXY($current_x, $current_y);
//     $pdf->cell($l_column, 5, $this->sma->utf8Decode(lang('rete_bomberil')." ".($inv->rete_bomberil_percentage > 0 ? $this->sma->formatQuantity($inv->rete_bomberil_percentage, $qty_decimals)." %" : "")), 'TBLR', 1, 'L');
//     $pdf->setXY($current_x+$l_column, $current_y);
//     $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_bomberil_total * $trmrate)), 'TBR', 1, 'R');
// }

// if ($inv->rete_autoaviso_total > 0) {
//     $current_y+=5;
//     $pdf->setXY($current_x, $current_y);
//     $pdf->cell($l_column, 5, $this->sma->utf8Decode(lang('rete_autoaviso')." ".($inv->rete_autoaviso_percentage > 0 ? $this->sma->formatQuantity($inv->rete_autoaviso_percentage, $qty_decimals)." %" : "")), 'TBLR', 1, 'L');
//     $pdf->setXY($current_x+$l_column, $current_y);
//     $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->rete_autoaviso_total * $trmrate)), 'TBR', 1, 'R');
// }

$current_y += 5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($l_column, 5, $this->sma->utf8Decode('Descuento'), 'TBLR', 1, 'L');
$pdf->setXY($current_x + $l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_discount * $trmrate)), 'TBR', 1, 'R');

if ($inv->shipping > 0) {
    $current_y += 5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Domicilio'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x + $l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y += 5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial', 'B', $fuente + 2);
$pdf->cell($l_column, 5, $this->sma->utf8Decode('Total'), 'TBLR', 1, 'L', 1);
$pdf->setXY($current_x + $l_column, $current_y);
$pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total) * $trmrate)), 'TBR', 1, 'R', 1);

$pdf->SetFont('Arial', '', $fuente);

$inv_total_summary_end = $pdf->getY() + 3;

if ($tax_summary_end > $inv_total_summary_end) {
    $pdf->setXY(13, $tax_summary_end);
} else {
    $pdf->setXY(13, $inv_total_summary_end);
}

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY() + 11;

$cX = $pdf->getX();
$cY = $pdf->getY();
$pdf->setX(16);
$pdf->SetFont('Arial', 'B', $fuente);
$pdf->cell(10, 3, $this->sma->utf8Decode('Nota : '), '', 1, 'L');
$pdf->SetFont('Arial', '', $fuente);
$pdf->setX(15);
$pdf->setXY($cX + 12, $cY);
$pdf->MultiCell(183, 3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)), 0, 'L');

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 20, 3, '1234', '');

$footer_note_x = $pdf->getX();
$pdf->setX($current_x + 3);
$pdf->SetFont('Arial', 'B', $fuente);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');
$pdf->SetFont('Arial', '', $fuente);

$pdf->setXY($current_x + 3, $current_y + 15);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x + 39.2, $current_y + 15);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

//derecha
$pdf->setXY($current_x + 78.4, $current_y);
$current_x = $pdf->getX() + 2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 20, 3, '1234', '');
$pdf->setX($current_x + 3);
$pdf->SetFont('Arial', 'B', $fuente);
$pdf->MultiCell(110, 4, $this->sma->utf8Decode('Firma autorizada'), 0, 'L');
$pdf->SetFont('Arial', '', $fuente);

if ($signature_root) {
    $pdf->Image($signature_root, $current_x + 35, $current_y + 3, 35);
}

$pdf->setXY($footer_note_x + 1, $current_y + 23);
$square_initial_y = $pdf->getY() - 1;
$pdf->SetFont('Arial', '', $pdf->fuente + 0.5);
if (isset($pdf->cost_center) && $pdf->cost_center) {
    $pdf->MultiCell(195, 3, $this->sma->utf8Decode($pdf->reduceTextToDescription2("Centro de costo : " . $pdf->cost_center->name . " (" . $pdf->cost_center->code . ") \n" . $pdf->description2)), 0, 'L');
} else {
    $pdf->MultiCell(194.5, 3, $this->sma->utf8Decode($pdf->reduceTextToDescription2($pdf->description2)), 0, 'L');
}
$square_end_y = $pdf->getY();
$square_height = $square_end_y - $square_initial_y;
$pdf->RoundedRect($footer_note_x, $square_initial_y, 196, $square_height + 1, 3, '1234', '');
if ($for_email === true) {
    $pdf->Output(FCPATH . "files/" . $inv->reference_no . ".pdf", "F");
} else {
    $pdf->Output($inv->reference_no . ".pdf", "I");
}
