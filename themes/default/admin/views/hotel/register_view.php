<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Registro de Huéspedes'));
        //izquierda
        if ($this->biller_logo == 2) {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,15,8.5,29);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,14,17,30);
        }
        $cx = 55;
        $cy = 8;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->MultiCell(75, 3.5,  $this->sma->utf8Decode(($this->biller->company != "-" ? $this->biller->company : $this->biller->name)), '', 'L');
        $cy  = $this->getY();
        $cy+=1.3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->digito_verificacion != "" ? '-'.$this->biller->digito_verificacion : '')."  ".$this->tipo_regimen),'',1,'L');
        $cy+=4;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address),'',1,'L');
        $cy+=4;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=4;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=4;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');
        $cy+=4;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(isset($this->Settings->url_web) ? $this->Settings->url_web : ''),'',1,'L');

        //derecha
        // $this->RoundedRect(138, 10, 71, 31, 3, '1234', '');
        $cx = 130;
        $cy = 8;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente+5);
        $this->setXY($cx, $cy);
        $this->Cell(77, 4, $this->sma->utf8Decode($this->inv->reference_no),'',1,'L');
        $cy +=5;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(30.5, 4, $this->sma->utf8Decode('Fecha Registro :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(46.5, 4, $this->sma->utf8Decode($this->inv->date_in),0,1,'L');
        $cy +=4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(30.5, 4, $this->sma->utf8Decode('Fecha est. salida :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(46.5, 4, $this->sma->utf8Decode($this->inv->estimated_date_out),0,1,'L');
        $cy +=4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(30.5, 4, $this->sma->utf8Decode('N° noches :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(46.5, 4, $this->sma->utf8Decode($this->inv->nights_no),0,1,'L');
        $cy +=4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(30.5, 4, $this->sma->utf8Decode('N° húespedes :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(46.5, 4, $this->sma->utf8Decode($this->inv->guests_no),0,1,'L');
        $cy +=4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(30.5, 4, $this->sma->utf8Decode('Habitación :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(46.5, 4, $this->sma->utf8Decode($this->inv->room),0,1,'L');
        $cy +=4;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(30.5, 4, $this->sma->utf8Decode('Total :'),0,0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(46.5, 4, $this->sma->utf8Decode($this->sma->formatMoney($this->inv->total)),0,1,'L');


        $this->ln(4);
        $this->Cell(196, 0, $this->sma->utf8Decode(''),1,1,'L');
        $this->SetFont('Arial','B',$this->fuente+6);
        $this->setY($this->getY()+2);
        $this->Cell(196, 5 , $this->sma->utf8Decode('Registro de Entrada'),'',1,'C');
        $cy += $this->getY();
        $this->setXY(13, $cy+2);

        //izquierda
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cx = 13;
        $cy = 48;
        $this->setXY($cx, $cy);
        $this->Cell(196, 5 , $this->sma->utf8Decode('Información del cliente'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        } else {
            $customer_name = $this->customer->company;
        }
        $this->MultiCell(98,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 19;
        $columns_r = 79;
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->address),'',1,'L');
        ////
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cx = 111;
        $cy = 48;
        $this->setXY($cx, $cy);
        $cx += 3;
        $cy += 12;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->phone),'',0,'L');
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->email),'',1,'L');
    }
    function Footer()
    {
        $this->SetXY(7, -33);
        $this->SetXY(7, -7);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -7);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }
}
$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);
$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();
$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->biller_logo = $biller_logo;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->tipo_regimen = $tipo_regimen;
$pdf->inv = $inv;
$pdf->customer = $customer;
$pdf->main_guest = $rows[0];
$pdf->AddPage();
$maximo_footer = 235;
$plus_font_size = 0;
// unset($rows[0]);
$pdf->ln(5);

$pdf->SetFont('Arial','B',$fuente+4);
$pdf->Cell(196, 5, $this->sma->utf8Decode("Huéspedes"),'B',1,'L');
$pdf->SetFont('Arial','B',$fuente+2);
$pdf->Cell(47.32, 5, $this->sma->utf8Decode("Nombre"),'TLB',0,'L');
$pdf->Cell(27, 5, $this->sma->utf8Decode("Tipo Doc."),'TLB',0,'L');
$pdf->Cell(25, 5, $this->sma->utf8Decode("Nit/Cc"),'TLB',0,'L');
$pdf->Cell(32.66, 5, $this->sma->utf8Decode("Email"),'TLB',0,'L');
$pdf->Cell(30   , 5, $this->sma->utf8Decode("Teléfono"),'TLB',0,'L');
$pdf->Cell(33.98, 5, $this->sma->utf8Decode("Firma"),'TLBR',1,'L');
$pdf->SetFont('Arial','',$fuente);
foreach ($rows as $row) {
    $pdf->Cell(47.32, 5, $this->sma->utf8Decode($row->names.($row->surnames ? " ".$row->surnames : "")),'TLB',0,'L');
    $pdf->Cell(27, 5, $this->sma->utf8Decode($pdf->crop_text(lang($row->tipo_doc), 30)),'TLB',0,'L');
    $pdf->Cell(25, 5, $this->sma->utf8Decode($row->vat_no),'TLB',0,'L');
    $pdf->Cell(32.66, 5, $this->sma->utf8Decode($row->email),'TLB',0,'L');
    $pdf->Cell(30  , 5, $this->sma->utf8Decode($row->phone),'TLB',0,'L');
    $pdf->Cell(33.98, 5, $this->sma->utf8Decode(''),'TLBR',1,'L');
}
$cy = $pdf->getY()+2;
$pdf->RoundedRect(13, $cy, 98, 20, 3, '1234', '');
$pdf->setXY(15, $cy+15);
$pdf->Cell(94, 5, $this->sma->utf8Decode('Firma huésped principal'),'T',0,'L');
if ($pdf->getY() > 185.5) {
    $pdf->AddPage();
}
$pdf->setXY(13, 185.5);
$pdf->Cell(196, 0, $this->sma->utf8Decode(''),1,1,'L');

$pdf->ln(1.5);
$pdf->SetFont('Arial','B',$pdf->fuente+6);
$pdf->Cell(196, 5 , $this->sma->utf8Decode('Registro de Salida'),'',1,'C');

$pdf->ln(3);
$pdf->SetFont('Arial','B',$pdf->fuente+3);
$pdf->Cell(30, 5, $this->sma->utf8Decode('Fecha de salida :'),'',0,'L');
$pdf->SetFont('Arial','',$pdf->fuente+3);
$pdf->Cell(66, 5, $this->sma->utf8Decode(''),'B',0,'L');
$pdf->SetFont('Arial','B',$pdf->fuente+3);
$pdf->Cell(30, 5, $this->sma->utf8Decode('Total a pagar :'),'',0,'L');
$pdf->SetFont('Arial','',$pdf->fuente+3);
$pdf->Cell(66, 5, $this->sma->utf8Decode(''),'B',1,'L');
$pdf->ln(3);

$pdf->SetFont('Arial','',$pdf->fuente+3);

$cy = $pdf->getY()+2;
$pdf->RoundedRect(13, $cy, 98, 25, 3, '1234', '');
$pdf->setXY(15, $cy+15);
$pdf->Cell(94, 5, $this->sma->utf8Decode('Autorizo el cargo de los servicios solicitados a la cuenta'),'T',1,'L');
$cy = $pdf->getY();
$pdf->setXY(15, $cy);
$pdf->Cell(94, 5, $this->sma->utf8Decode('Firma huésped principal'),'',0,'L');


$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();
$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->Output($inv->reference_no.".pdf", "I");
$pdf->ln();