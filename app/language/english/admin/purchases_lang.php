<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Purchases
 *
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */


$lang['add_purchase']                       = "Add Purchase";
$lang['edit_purchase']                      = "Edit Purchase";
$lang['delete_purchase']                    = "Delete Purchase";
$lang['delete_purchases']                   = "Delete Purchases";
$lang['purchase_added']                     = "Purchase successfully added";
$lang['purchase_updated']                   = "Purchase successfully updated";
$lang['purchase_deleted']                   = "Purchase successfully deleted";
$lang['purchases_deleted']                  = "Purchases successfully deleted";
$lang['ref_no']                             = "Reference No";
$lang['purchase_details']                   = "Purchase Details";
$lang['email_purchase']                     = "Email Purchase";
$lang['purchase_quantity']                  = "Purchase Quantity";
$lang['please_select_warehouse']            = "Please select warehouse";
$lang['purchase_by_csv']                    = "Add Purchase by CSV";
$lang['received']                           = "Received";
$lang['more_options']                       = "More Options";
$lang['add_standard_product']               = "Add Standard Product";
$lang['product_code_is_required']           = "Product Code is required";
$lang['product_name_is_required']           = "Product Name is required";
$lang['product_category_is_required']       = "Product Category is required";
$lang['product_unit_is_required']           = "Product Unit is required";
$lang['product_cost_is_required']           = "Product Cost is required";
$lang['product_price_is_required']          = "Product Price is required";
$lang['ordered']                            = "Ordered";
$lang['tax_rate_name']                      = "Tax Rate Name";
$lang['no_purchase_selected']               = "No purchase selected. Please select at least one purchase.";
$lang['view_payments']                      = "View Payments";
$lang['add_payment']                        = "Add Payment";
$lang['payment_reference_no']               = "Payment Reference No";
$lang['edit_payment']                       = "Edit Payment";
$lang['delete_payment']                     = "Delete Payment";
$lang['delete_payments']                    = "Delete Payments";
$lang['payment_added']                      = "Payment successfully added";
$lang['payment_updated']                    = "Payment successfully updated";
$lang['payment_deleted']                    = "Payment successfully deleted";
$lang['payments_deleted']                   = "Payments successfully deleted";
$lang['paid_by']                            = "Paid by";
$lang['payment_reference']                  = "Payment Reference";
$lang['view_purchase_details']              = "View Purchase Details";
$lang['purchase_no']                        = "Purchase Number";
$lang['balance']                            = "Balance";
$lang['product_option']                     = "Product Option";
$lang['payment_sent']                       = "Payment Sent";
$lang['payment_note']                       = "Payment Note";
$lang['payment_received']                   = "Payment Received";
$lang['purchase_status']                    = "Purchase Status";
$lang['purchase_x_edited_older_than_x_days']= "Purchase can't be edited as this purchase is older than %d days.";
$lang['pr_not_found']                       = "No product found ";
$lang['line_no']                            = "Line Number";
$lang['expense']                            = "Expense";
$lang['edit_expense']                       = "Edit Expense";
$lang['delete_expense']                     = "Delete Expense";
$lang['delete_expenses']                    = "Delete Expenses";
$lang['expense_added']                      = "Expense successfully added";
$lang['expense_updated']                    = "Expense successfully updated";
$lang['expense_deleted']                    = "Expense successfully deleted";
$lang['reference']                          = "Reference";
$lang['expenses_deleted']                   = "Expenses successfully deleted";
$lang['expense_note']                       = "Expense Note";
$lang['no_expense_selected']                = "No expense selected. Please select at least one expense.";
$lang['please_select_supplier']             = "Please select supplier";
$lang['unit_cost']                          = "Unit Cost";
$lang['product_expiry_date_issue']          = "Product expiry date has issue";
$lang['received_more_than_ordered']         = "Received quantity was more than ordered";
$lang['purchase_order']                     = "Purchase order";
$lang['payment_term']                       = "Payment Term";
$lang['payment_term_tip']                   = "Please type the number of days (integer) only";
$lang['due_on']                             = "Due on";
$lang['paid_amount']                        = "Paid Amount";
$lang['return_purchase']                    = "Return Purchase";
$lang['return_surcharge']                   = "Return Surcharge";
$lang['return_amount']                      = "Return Amount";
$lang['purchase_reference']                 = "Purchase Reference";
$lang['return_purchase_no']                 = "Return Purchase No";
$lang['view_return']                        = "View Return";
$lang['return_purchase_deleted']            = "Return purchase successfully deleted";
$lang['purchase_status_x_received']         = "Purchase status is not received";
$lang['total_before_return']                = "Total Before Return";
$lang['return_amount']                      = "Return Amount";
$lang['return_items']                       = "Return Items";
$lang['surcharge']                          = "Surcharges";
$lang['returned_items']                     = "Returned Items";
$lang['return_quantity']                    = "Returned Quantity";
$lang['seller']                             = "Sller";
$lang['users']                              = "Users";
$lang['return_note']                        = "Return Note";
$lang['return_purchase_added']              = "Return purchase successfully added";
$lang['return_has_been_added']              = "Few items had been returned for this purchase";
$lang['return_tip']                         = "Please edit the return quantity below. You can remove the item or set the return quantity to zero if it is not being returned";
$lang['return_surcharge']                   = "Return Surcharge";
$lang['payment_returned']                   = "Payment Returned";
$lang['payment_note']                       = "Payment Note";
$lang['payment_status']                     = "Payment Status";
$lang['view_return_details']                = "View Return Details";
$lang['adjust_payments']                    = "Please adjust payments for the purchase manually";
$lang['purchase_x_action']                  = "This action can not be performed for purchase with a return record";
$lang['purchase_already_returned']          = "Purchase already have return record";
$lang['purchase_is_returned']               = "Purchase has return record";
$lang['calculate_unit_cost']                = "Calculate Unit Cost";
$lang['purchase_already_paid']              = "Payment status is already paid for the purchase";
$lang['payment_sent']                       = "Payment Sent";
$lang['update_supplier_email']              = "Please update supplier email address";

//NO TRADUCIDO

$lang['first_3_are_d_other_optional'] = 'First 3 are required, other are optional.';
$lang['attachment'] = 'Attachment';
$lang['stamp_sign'] = 'Stamp $ Signature';
$lang['purchase_x_edited_older_than_3_months'] = 'You can edit purchases within 3 months.';
$lang['subtotal_before_tax'] = 'Subtotal before taxes';
$lang['order_discount_method'] = 'Order discount method';
$lang['order_discount_method_each_product'] = 'Apply discount to each item';
$lang['order_discount_method_order_total'] = 'Apply discount to order total';
$lang['order_discount_to_products'] = 'Apply discount to items';
$lang['order_discount_to_products_yes'] = 'Apply order discount to items';
$lang['order_discount_to_products_no'] = 'Apply order discount globally';

$lang['purchase_expense_not_inserted']				= "The expense created through the purchase could not be generated.";

$lang['payment_affects_register'] = "Does the payment to this purchase affect the cash register?";
$lang['affects_register'] = "Yes, it does affect the cash register.";
$lang['no_affects_register'] = "No, it does not affect the cash register.";

$lang['affects_register_note'] = "Payments on this purchase affect the cash register";

$lang['not_enough_cash_register'] = "Cash in cash register is not enough for payment";

$lang['trm_difference'] = 'Value difference because of different TRM';
$lang['trm_difference_operator'] = 'Negative/Positive difference';

$lang['expense_can_not_be_returned'] = 'It is not possible to register a refund on the purchase because it is of the Expense type';

$lang['affectation_not_indicated'] = 'Affectation to cash register not indicated';

$lang['purchase_pending'] = 'The purchase was pending, so it did not affect the inventory';
$lang['purchase_pending_not_accounted'] = ' and was not counted';

$lang['cannot_edit_purchase_with_payment'] = 'You cannot edit this purchase because you already have registered payments';
$lang['cannot_edit_purchase_completed'] = 'Unable to edit the purchase because it has been completed';
$lang['cannot_edit_purchase_diferent_month'] = 'Unable to edit the purchase because it is not of this month';
$lang['cannot_edit_purchase_expense'] = 'Unable to edit a purchase type expense';
$lang['purchase_edited_at_by'] = 'This purchase was edited at %s by the user %s';

$lang['cannot_register_payment_purchase_pending'] = 'A purchase payment cannot be registered because it is in pending status.';