<?php

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

//$this->SetTextColor( 255, 0, 0); color rojo
//$this->SetTextColor( 0, 0, 0); color negro

// var_dump($POST);

class PDF extends FPDF
  {

    function Header()
    {

        $this->SetTitle($this->sma->utf8Decode('Informe de cartera por vendedor y cliente'));

        $this->SetFont('Arial','B', $this->fontsize_title);

        $this->setY(12);

        $cx = $this->getX();
        $cy = $this->getY();

        if ($this->print_logo && isset($this->Logo)) {
            $this->Image($this->Logo,22,11,30,0);
        }
        
        $this->setXY($cx, $cy+5);

        $this->setXY($cx+76.06, $cy);

        $cx = $this->getX();
        $cy = $this->getY();

        $this->SetFont('Arial','B', $this->fontsize_title+4);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Informe de Cartera por Vendedor y Cliente'), '', 1, 'C');
        $this->setXY($cx, $cy+5);
        $this->SetFont('Arial','B', $this->fontsize_title+2);
        if (isset($this->biller_name)) {
            $this->cell(111.97, 5, $this->sma->utf8Decode("Sucursal : ".$this->biller_name), '', 1, 'C');
        }

        $this->setXY($cx, $cy+5);
        // $this->cell(111.97, 5, $this->sma->utf8Decode('Por edades (cambiar)'), '', 0, 'C');
        $this->setXY($cx+111.97, $cy);

        $cx = $this->getX();
        $cy = $this->getY();
        $this->cell(76.06, 5, $this->sma->utf8Decode("Fecha : ".date('d/m/Y H:i:s')), '', 1, 'L');
        $this->setXY($cx, $cy+5);
        $this->SetFont('Arial','B', $this->fontsize_title+2);
        if (isset($this->seller_name)) {
            $this->cell(76.06, 5, $this->sma->utf8Decode("Vendedor : ".$this->seller_name), '', 0, 'L');
        }
        $this->SetFont('Arial','B', $this->fontsize_title);

        $this->ln();

        $this->setFillColor(200,200,200);
        $current_x = $this->getX();
        $current_y = $this->getY();
        $this->RoundedRect($current_x, $current_y, 265.78, 5, 1, '1234', 'FD');
        $this->cell(19   , 5, $this->sma->utf8Decode('Número'), '', 0, 'C');
        $this->cell(20.08, 5, $this->sma->utf8Decode('Fecha factura'), 'L', 0, 'C');
        $this->cell(24.53, 5, $this->sma->utf8Decode('Valor Bruto factura'), 'L', 0, 'C');
        $this->cell(15.08, 5, $this->sma->utf8Decode('% Dscto'), 'L', 0, 'C');
        $this->cell(18.08, 5, $this->sma->utf8Decode('Val. Dscto(-)'), 'L', 0, 'C');
        $this->cell(22.53, 5, $this->sma->utf8Decode('Subtotal (+)'), 'L', 0, 'C');
        $this->cell(22   , 5, $this->sma->utf8Decode('Retenciones (-)'), 'L', 0, 'C');
        $this->cell(22.08, 5, $this->sma->utf8Decode('Abonos (-)'), 'L', 0, 'C');
        $this->cell(22.08, 5, $this->sma->utf8Decode('Nota crédito (-)'), 'L', 0, 'C');
        $this->cell(22.08, 5, $this->sma->utf8Decode('Valor Impuesto (+)'), 'L', 0, 'C');
        $this->cell(20.08, 5, $this->sma->utf8Decode('Fecha Vence'), 'L', 0, 'C');
        $this->cell(15.08, 5, $this->sma->utf8Decode('Días'), 'L', 0, 'C');
        $this->cell(23.08, 5, $this->sma->utf8Decode('Saldo en factura'), 'L', 1, 'C');

    }

    function Footer()
    {
        // $this->cell(76.06, 5, $this->sma->utf8Decode($this->biller_name), '', 1, 'C');
        // $this->cell(76.06, 5, $this->sma->utf8Decode('Nit. '.$this->biller_vat_no.($this->biller_digito_verificacion != 0 ? $this->biller_digito_verificacion : "")), '', 0, 'C');
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina '.$this->PageNo()." de ".'{nb}'),'',1,'C');

    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

}


$pdf = new PDF('L', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);

$pdf->fontsize_body = 7.5;
$pdf->fontsize_title = 7.5;

$pdf->settings = $settings;
$pdf->print_logo = $print_logo;

$sellers = [];
$customers = [];
$addresses = [];
$cities = [];
$states = [];
$total_balance=0;
$customer_balance = 0;
$address_balance = 0;
$customer_total_subtotal = 0;
$address_total_subtotal = 0;
$state_balance = 0;
$city_balance = 0;
$seller_balance = 0;

$last_biller = null;
// exit(json_encode($data));

if (!$group_by_biller) {
    $pdf->biller_name = 'Todas las sucursales';
    $pdf->seller_name = 'Todos los vendedores';
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    $pdf->Logo = base_url('assets/uploads/logos/'.$default_biller->logo);
}

if (!$group_by_seller) {
    $pdf->AddPage();
}

foreach ($data as $sale) {

    if ($group_by_biller && ($last_biller == null || ($last_biller != null && $last_biller != $sale->biller_id))) {
        $last_biller = $sale->biller_id;
        $pdf->biller_name = $sale->biller_name;
        $pdf->SetFont('Arial','', $pdf->fontsize_body);
        $pdf->Logo = base_url('assets/uploads/logos/'.$sale->biller_logo);
    }

    if ($group_by_seller && !isset($sellers[$sale->seller_id])) {

        $pdf->SetFont('Arial','B', $pdf->fontsize_title);

        if ($address_balance != 0 && $view_customer_address_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($address_total_subtotal)), 'B', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : "), 'B', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($address_balance)), 'B', 1, 'R');
            $address_balance = 0;
            $pdf->ln(5);
        }
        if ($customer_balance != 0 && $view_customer_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($customer_total_subtotal)), '', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($customer_balance)), '', 1, 'R');
            $customer_total_subtotal = 0;
            $customer_balance = 0;
        }
        if ($city_balance != 0 && $view_city_total) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total de la Ciudad ".(mb_strtoupper(end($cities)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($city_balance)), 'T', 1, 'R');
            $city_balance = 0;
        }
        if ($state_balance != 0 && $view_city_total) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total del Dpto ".(mb_strtoupper(end($states)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($state_balance)), 'T', 1, 'R');
            $state_balance = 0;
        }
        if ($seller_balance != 0) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total del vendedor ".(mb_strtoupper(end($sellers)))." : "), 'T', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($seller_balance)), 'T', 1, 'R');
            $seller_balance = 0;
        }

        $pdf->SetFont('Arial','', $pdf->fontsize_body);

        $sellers[$sale->seller_id] = $sale->seller_name;
        $pdf->seller_name = $sellers[$sale->seller_id];
        $pdf->biller_name = $sale->biller_name;
        $pdf->biller_vat_no = $sale->biller_vat_no;
        $pdf->biller_digito_verificacion = $sale->biller_digito_verificacion;
        $pdf->AddPage();
        $customers = [];
        $states = [];
        $addresses = [];
        $cities = [];
    } 

    if (!isset($states[mb_strtolower($sale->address_state)])) {
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);

        if ($address_balance != 0 && $view_customer_address_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($address_total_subtotal)), 'B', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : "), 'B', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($address_balance)), 'B', 1, 'R');
            $address_total_subtotal = 0;
            $address_balance = 0;
            $pdf->ln(5);
        }
        if ($customer_balance != 0 && $view_customer_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($customer_total_subtotal)), '', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($customer_balance)), '', 1, 'R');
            $customer_total_subtotal = 0;
            $customer_balance = 0;
        }
        if ($city_balance != 0 && $view_city_total) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total de la Ciudad ".(mb_strtoupper(end($cities)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($city_balance)), 'T', 1, 'R');
            $city_balance = 0;
        }
        if ($state_balance != 0 && $view_city_total) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total del Dpto ".(mb_strtoupper(end($states)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($state_balance)), 'T', 1, 'R');
            $state_balance = 0;
        }
        unset($cities);
        unset($customers);
        unset($addresses);
        $states[mb_strtolower($sale->address_state)] = $sale->address_state;
        $pdf->cell(265, 0.8, $this->sma->utf8Decode(''), 'T', 1, 'L', 1);
        $pdf->cell(265, 4, $this->sma->utf8Decode((mb_strtoupper($sale->address_state))), 'T', 1, 'L');
    }

    if (!isset($cities[mb_strtolower($sale->address_city)])) {
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);

        if ($address_balance != 0 && $view_customer_address_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($address_total_subtotal)), 'B', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : "), 'B', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($address_balance)), 'B', 1, 'R');
            $address_total_subtotal = 0;
            $address_balance = 0;
            $pdf->ln(5);
        }
        if ($customer_balance != 0 && $view_customer_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($customer_total_subtotal)), '', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($customer_balance)), '', 1, 'R');
            $customer_total_subtotal = 0;
            $customer_balance = 0;
        }
        if ($city_balance != 0 && $view_city_total) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total de la Ciudad ".(mb_strtoupper(end($cities)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($city_balance)), 'T', 1, 'R');
            $city_balance = 0;
        }
        $pdf->cell(265, 0.4, $this->sma->utf8Decode(''), 'T', 1, 'L', 1);
        $cities[mb_strtolower($sale->address_city)] = $sale->address_city;
        $pdf->cell(265, 4, $this->sma->utf8Decode((mb_strtoupper($sale->address_city))), '', 1, 'L');

        unset($customers);
        unset($addresses);

        $customers[$sale->customer_id] = $sale->customer_name;
        $pdf->cell(88.33, 4, $this->sma->utf8Decode(mb_strtoupper($sale->customer_name)), '', 0, 'L');
        $pdf->cell(88.33, 4, $this->sma->utf8Decode("NIT/CC : ".$sale->customer_vat_no.($sale->customer_digito_verificacion != 0 ? "-".$sale->customer_digito_verificacion : "")), '', 0, 'L');
        $pdf->cell(50.33, 4, $this->sma->utf8Decode("Lista : ".(!empty($sale->price_group_name) ? ucfirst(mb_strtolower($sale->price_group_name)) : "Sin lista de precio asignada")), '', 0, 'L');
        $pdf->cell(38.33, 4, $this->sma->utf8Decode('Anticipos : '.$this->sma->formatMoney($sale->customer_deposit_amount)), '', 1, 'L');

        $pdf->SetFont('Arial','', $pdf->fontsize_body);
    }

    if (!isset($customers[$sale->customer_id])) {
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);
        if ($address_balance != 0 && $view_customer_address_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($address_total_subtotal)), 'B', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : "), 'B', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($address_balance)), 'B', 1, 'R');
            $address_balance = 0;
            $address_total_subtotal = 0;
            $pdf->ln(5);
        }
        if ($customer_balance != 0 && $view_customer_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($customer_total_subtotal)), '', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($customer_balance)), '', 1, 'R');
            $customer_balance = 0;
        }

        $pdf->ln(2.5);
        $customer_total_subtotal = 0;
        $customers[$sale->customer_id] = $sale->customer_name;
        $pdf->cell(88.33, 4, $this->sma->utf8Decode(mb_strtoupper($sale->customer_name)), '', 0, 'L');
        $pdf->cell(88.33, 4, $this->sma->utf8Decode("NIT/CC : ".$sale->customer_vat_no.($sale->customer_digito_verificacion != 0 ? "-".$sale->customer_digito_verificacion : "")), '', 0, 'L');
        $pdf->cell(50.33, 4, $this->sma->utf8Decode("Lista : ".(!empty($sale->price_group_name) ? ucfirst(mb_strtolower($sale->price_group_name)) : "Sin lista de precio asignada")), '', 0, 'L');
        $pdf->cell(38.33, 4, $this->sma->utf8Decode('Anticipos : '.$this->sma->formatMoney($sale->customer_deposit_amount)), '', 1, 'L');
        $pdf->SetFont('Arial','', $pdf->fontsize_body);

    }

    if (!isset($addresses[$sale->address_id])) {

        $pdf->SetFont('Arial','B', $pdf->fontsize_title);
        if ($address_balance != 0 && $view_customer_address_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($customer_total_subtotal)), '', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($address_balance)), '', 1, 'R');
            $address_balance = 0;
            $pdf->ln(5);
        }
        $address_total_subtotal = 0;
        $addresses[$sale->address_id] = $sale->address_name;
        $pdf->cell(88.33, 4, $this->sma->utf8Decode("Sucursal : ".ucfirst(mb_strtolower($sale->address_name))), '', 0, 'L');
        $pdf->cell(88.33, 4, $this->sma->utf8Decode(ucfirst(mb_strtolower($sale->address_direccion)).", ".ucfirst(mb_strtolower($sale->address_city))), '', 0, 'L');
        $pdf->cell(88.33, 4, $this->sma->utf8Decode("Télefono : ".$sale->address_phone), '', 1, 'L');
        $pdf->SetFont('Arial','', $pdf->fontsize_body);

    }

    $pdf->cell(19   , 3, $this->sma->utf8Decode($sale->reference_no), '', 0, 'L');
    $pdf->cell(20.08, 3, $this->sma->utf8Decode(date('Y-m-d', strtotime($sale->date))), '', 0, 'L');
    $pdf->cell(24.53, 3, $this->sma->utf8Decode($sma->formatMoney($sale->grand_total)), '', 0, 'R');
    $pdf->cell(15.08, 3, $this->sma->utf8Decode($sma->formatDecimal($sale->percentage_discount)), '', 0, 'C');
    $pdf->cell(18.08, 3, $this->sma->utf8Decode($sma->formatMoney($sale->total_discount)), '', 0, 'R');
    $pdf->cell(22.53, 3, $this->sma->utf8Decode($sma->formatMoney($sale->sub_total)), '', 0, 'R');
    $pdf->cell(22   , 3, $this->sma->utf8Decode($sma->formatMoney( ($sale->rete_fuente_total + $sale->rete_iva_total + $sale->rete_ica_total + $sale->rete_other_total) )), '', 0, 'R');
    $pdf->cell(22.08, 3, $this->sma->utf8Decode($sma->formatMoney($sale->paid)), '', 0, 'R');
    $pdf->cell(22.08, 3, $this->sma->utf8Decode($sma->formatMoney($sale->credit_note)), '', 0, 'R');
    $pdf->cell(22.08, 3, $this->sma->utf8Decode($sma->formatMoney($sale->total_tax)), '', 0, 'R');
    $pdf->cell(20.08, 3, $this->sma->utf8Decode(date('Y-m-d', strtotime($sale->expiration_date))), '', 0, 'L');

    if ($expiration_days_from == 1) {
        $fecha = substr($sale->date,0,10);
    } else if ($expiration_days_from == 2) {
        $fecha = substr($sale->due_date,0,10);
    }

    $fecha_now = date('Y-m-d');
    $dias   = (strtotime($fecha)-strtotime($fecha_now))/86400;
    $dias   = abs($dias); 
    $dias   = floor($dias);

    // $pdf->cell(15.08, 3, $this->sma->utf8Decode($sale->payment_term), '', 0, 'C');
    $pdf->cell(15.08, 3, $this->sma->utf8Decode($dias), '', 0, 'C');
    $pdf->cell(23.08, 3, $this->sma->utf8Decode($sma->formatMoney($sale->balance)), '', 1, 'R');

    $customer_balance += $sale->balance;
    $customer_total_subtotal += $sale->sub_total;
    $address_balance += $sale->balance;
    $address_total_subtotal += $sale->sub_total;
    $state_balance += $sale->balance;
    $city_balance += $sale->balance;
    $seller_balance += $sale->balance;
    $total_balance += $sale->balance;
    if ($sale === end($data)) {
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);
        if ($address_balance != 0 && $view_customer_address_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($address_total_subtotal)), '', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total de la Sucursal ".ucwords(mb_strtolower($sale->address_name))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($address_balance)), '', 1, 'R');
            $address_balance = 0;
        }
        if ($customer_balance != 0 && $view_customer_total) {
            $pdf->cell(119.13, 4, $this->sma->utf8Decode($sma->formatMoney($customer_total_subtotal)), '', 0, 'R');
            $pdf->cell(115.89, 4, $this->sma->utf8Decode("Saldo total del Cliente ".mb_strtoupper($sale->customer_name)." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($customer_balance)), '', 1, 'R');
            $customer_balance = 0;
        }
        if ($city_balance != 0 && $view_city_total) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total de la Ciudad ".mb_strtoupper($sale->address_city)." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($city_balance)), '', 1, 'R');
            $city_balance = 0;
        }
        if ($state_balance != 0 && $view_city_total) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total del Dpto ".(mb_strtoupper(end($states)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($state_balance)), 'T', 1, 'R');
            $state_balance = 0;
        }
        if ($seller_balance != 0) {
            $pdf->cell(235.02, 4, $this->sma->utf8Decode("Saldo total del vendedor ".(mb_strtoupper(end($sellers)))." : "), '', 0, 'R');
            $pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($seller_balance)), 'T', 1, 'R');
            $seller_balance = 0;
        }
    }
}

$pdf->cell(235.02, 4, $this->sma->utf8Decode("Gran total del informe : "), 'T', 0, 'R');
$pdf->cell(30.8, 4, $this->sma->utf8Decode($sma->formatMoney($total_balance)), 'T', 1, 'R');

$descargar = false;

if ($descargar) {
  $pdf->Output("INFORME_CARTERA.pdf", "D");
} else {
  $pdf->Output("INFORME_CARTERA.pdf", "I");
}