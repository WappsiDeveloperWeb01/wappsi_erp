<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
$v = "";
if ($this->input->post('product')) {
    $v .= "&product=" . $this->input->post('product');
}
if ($this->input->post('category')) {
    $v .= "&category=" . $this->input->post('category');
}
if ($this->input->post('brand')) {
    $v .= "&brand=" . $this->input->post('brand');
}
if ($this->input->post('subcategory')) {
    $v .= "&subcategory=" . $this->input->post('subcategory');
}
if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
if ($this->input->post('price_group')) {
    $v .= "&price_group=" . $this->input->post('price_group');
}
if ($this->input->post('report_date')) {
    $v .= "&report_date=" . $this->input->post('report_date');
}
if ($this->input->post('include_products_discontinued')) {
    $v .= "&include_products_discontinued=" . $this->input->post('include_products_discontinued');
}
if ($this->input->post('variants')) {
    $v .= "&variants=" . $this->input->post('variants');
}
?>
<script>
    $(document).ready(function() {
        function spb(x) {
            v = x.split('__');
            return '(' + formatQuantity2(v[0]) + ') <strong>' + formatMoney(v[1]) + '</strong>';
        }
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
            var oTable = $('#PrRData').dataTable({
                "aaSorting": [
                    [0, "desc"],
                    [1, "desc"]
                ],
                "aLengthMenu": [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
                ],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true,
                'bServerSide': true,
                'sAjaxSource': '<?= admin_url('reports/getValuedProductsReport/?v=1' . $v) ?>',
                'fnServerData': function(sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'data': aoData,
                        'success': fnCallback
                    });
                },
                'fnRowCallback': function(nRow, aData, iDisplayIndex) {

                },
                "aoColumns": [
                    <?php if(isset($_POST['variants'])){ ?>
                        { "bSortable": false }, // codigo
                        { "bSortable": false }, // nombre
                        { "bSortable": false }, // codigo talla
                        { "bSortable": false }, // nombre talla
                        { "bSortable": false, "mRender" : function(data, type, row){
                            return data != null ? formatStatusVariant(data) : formatStatusVariant(1);
                        }   }, // estado
                        { "bSortable": false }, // tarifa impuesto
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        },  // cantidad
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        }, // costo unitario con iva
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        }, // costo promedio con iva
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        }, // precio unitario con iva
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        }, // total costo promedio con iva
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        }, // total costo promedio sin iva
                    <?php } else { ?>
                        { "bSortable": false },
                        { "bSortable": false },
                        { "bSortable": false, "mRender": formatStatus },
                        { "bSortable": false },
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        },
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        },
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        },
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        },
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        },
                        {
                            "mRender": formatQuantity,
                            "bSortable": false,
                            "bSearchable": false
                        },
                    <?php } ?>
                ],
                buttons: [{
                    extend: 'excel',
                    title: 'Afiliados',
                    className: 'btnExportarExcel',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12]
                    }
                }],
                "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                    var tt1 = tt2 = tt3 = tt4 = tt5 = tt6 = 0;
                    <?php if(isset($_POST['variants'])){?>
                        for (var i = 0; i < aaData.length; i++) {
                            tt1 += parseFloat(aaData[aiDisplay[i]][6]);
                            tt2 += parseFloat(aaData[aiDisplay[i]][7]);
                            tt3 += parseFloat(aaData[aiDisplay[i]][8]);
                            tt4 += parseFloat(aaData[aiDisplay[i]][9]);
                            tt5 += parseFloat(aaData[aiDisplay[i]][10]);
                            tt6 += parseFloat(aaData[aiDisplay[i]][11]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[6].innerHTML = '<div class="text-right">'+formatQuantity2(tt1)+'</div>';
                        nCells[7].innerHTML = '<div class="text-right">'+formatQuantity2(tt2)+'</div>';
                        nCells[8].innerHTML = '<div class="text-right">'+formatQuantity2(tt3)+'</div>';
                        nCells[9].innerHTML = '<div class="text-right">' + formatQuantity2(tt4) + '</div>';
                        nCells[10].innerHTML = '<div class="text-right">' + formatQuantity2(tt5) + '</div>';
                        nCells[11].innerHTML = '<div class="text-right">' + formatQuantity2(tt6) + '</div>';
                    <?php } else { ?>
                        for (var i = 0; i < aaData.length; i++) {
                            tt1 += parseFloat(aaData[aiDisplay[i]][4]);
                            tt2 += parseFloat(aaData[aiDisplay[i]][5]);
                            tt3 += parseFloat(aaData[aiDisplay[i]][6]);
                            tt4 += parseFloat(aaData[aiDisplay[i]][7]);
                            tt5 += parseFloat(aaData[aiDisplay[i]][8]);
                            tt6 += parseFloat(aaData[aiDisplay[i]][9]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[4].innerHTML = '<div class="text-right">'+formatQuantity2(tt1)+'</div>';
                        nCells[5].innerHTML = '<div class="text-right">'+formatQuantity2(tt2)+'</div>';
                        nCells[6].innerHTML = '<div class="text-right">'+formatQuantity2(tt3)+'</div>';
                        nCells[7].innerHTML = '<div class="text-right">' + formatQuantity2(tt4) + '</div>';
                        nCells[8].innerHTML = '<div class="text-right">' + formatQuantity2(tt5) + '</div>';
                        nCells[9].innerHTML = '<div class="text-right">' + formatQuantity2(tt6) + '</div>';
                    <?php }?>
                }
            }).fnSetFilteringDelay().dtFilter([
                <?php if (isset($_POST['variants'])) { ?>
                    {
                        column_number: 0,
                        filter_default_label: "[<?= lang('product_code'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?= lang('product_name'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 2,
                        filter_default_label: "[<?= lang('code')." ".lang('variant') ; ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 3,
                        filter_default_label: "[<?= lang('variant')." ".lang('variant') ; ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 4,
                        filter_default_label: "[<?= lang('status'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 5,
                        filter_default_label: "[<?= lang('tax_rate'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 6,
                        filter_default_label: "[<?= lang('quantity'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 7,
                        filter_default_label: "[<?= lang('unit_cost_tax'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 8,
                        filter_default_label: "[<?= lang('avg_cost_tax'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 9,
                        filter_default_label: "[<?= lang('unit_price_tax'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 10,
                        filter_default_label: "[<?= lang('total_avg_cost'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 11,
                        filter_default_label: "[<?= lang('total_actual_price'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                <?php } else { ?>
                    {
                        column_number: 0,
                        filter_default_label: "[<?= lang('product_code'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?= lang('product_name'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 2,
                        filter_default_label: "[<?= lang('status'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 3,
                        filter_default_label: "[<?= lang('tax_rate'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 4,
                        filter_default_label: "[<?= lang('quantity'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 5,
                        filter_default_label: "[<?= lang('unit_cost_tax'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 6,
                        filter_default_label: "[<?= lang('avg_cost_tax'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 7,
                        filter_default_label: "[<?= lang('unit_price_tax'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 8,
                        filter_default_label: "[<?= lang('total_avg_cost'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 9,
                        filter_default_label: "[<?= lang('total_actual_price'); ?>]",
                        filter_type: "text",
                        data: []
                    },
                <?php } ?>
            ], "footer");
        <?php endif ?>

    });

    $(document).ready(function() {
        $('#form').hide();
        $('.toggle_down').click(function() {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function() {
            $("#form").slideUp();
            return false;
        });
    });

    $(document).ready(function() {
        // $('#category').select2({allowClear: true, placeholder: "<?= lang('select'); ?>", minimumResultsForSearch: 7}).select2('destroy');
        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>").select2({
            allowClear: true,
            placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
            data: [{
                id: '',
                text: '<?= sprintf(lang('select_category_to_load'), lang('category')) ?>'
            }]
        });
        $('#category').change(function() {
            $('#subcategory').val('');
            var v = $(this).val();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function(scdata) {
                        if (scdata != null) {
                            $("#subcategory").select2('destroy').select2({
                                allowClear: true,
                                placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
                                data: scdata
                            });
                        } else {
                            $("#subcategory").select2('destroy').select2({
                                allowClear: true,
                                placeholder: "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>",
                                data: [{
                                    id: '',
                                    text: '<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>'
                                }]
                            });
                        }
                    },
                    error: function() {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                    }
                });
            } else {
                $("#subcategory").val('').select2({
                    allowClear: true,
                    placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
                    data: [{
                        id: '',
                        text: '<?= sprintf(lang('select_category_to_load'), lang('category')) ?>'
                    }]
                });
            }
        });
        <?php if (isset($_POST['category']) && !empty($_POST['category'])) { ?>
            $.ajax({
                type: "get",
                async: false,
                url: "<?= admin_url('products/getSubCategories') ?>/" + <?= $_POST['category'] ?>,
                dataType: "json",
                success: function(scdata) {
                    if (scdata != null) {
                        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_subcategory'), lang('subcategory')) ?>").select2({
                            allowClear: true,
                            placeholder: "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>",
                            data: scdata
                        });
                    }
                }
            });
        <?php } ?>
    });
</script>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open("reports/valued_products", 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("product", "suggest_product"); ?>
                                        <?php echo form_input('sproduct', (isset($_POST['sproduct']) ? $_POST['sproduct'] : ""), 'class="form-control" id="suggest_product"'); ?>
                                        <input type="hidden" name="product" value="<?= isset($_POST['product']) ? $_POST['product'] : "" ?>" id="report_product_id" />
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("category", "category") ?>
                                        <?php
                                        $cat[''] = lang('select') . ' ' . lang('category');
                                        foreach ($categories as $category) {
                                            $cat[$category->id] = $category->name;
                                        }
                                        echo form_dropdown('category', $cat, (isset($_POST['category']) ? $_POST['category'] : ''), 'class="form-control select" id="category" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("subcategory", "subcategory") ?>
                                        <div class="controls" id="subcat_data">
                                            <?php
                                                echo form_input('subcategory', (isset($_POST['subcategory']) ? $_POST['subcategory'] : ''), 'class="form-control" id="subcategory"  placeholder="' . sprintf(lang('select_category_to_load'), lang('category')) . '"');
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("brand", "brand") ?>
                                        <?php
                                        $bt[''] = lang('select');
                                        foreach ($brands as $brand) {
                                            $bt[$brand->id] = $brand->name;
                                        }
                                        echo form_dropdown('brand', $bt, (isset($_POST['brand']) ? $_POST['brand'] : ''), 'class="form-control select" id="brand" placeholder="' . lang("select") . " " . lang("brand") . '" style="width:100%"')
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                        <?php $warehouseId = (isset($_POST["warehouse"])) ? $_POST["warehouse"] : ''; ?>
                                        <?php $warehouseId = (empty($warehouseId)) ? $this->session->userdata('warehouse_id') : $warehouseId; ?>
                                        <?php
                                        $wh[""] = lang('select');
                                        foreach ($warehouses as $warehouse) {
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                        ?>
                                        <?= form_dropdown('warehouse', $wh, $warehouseId, 'class="form-control" id="warehouse" data-placeholder="'.$this->lang->line("select").'"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("price_group"); ?></label>
                                        <?php
                                        $prg = [];
                                        $price_group_base = null;
                                        foreach ($price_groups as $price_group) {
                                            $prg[$price_group->id] = $price_group->name;
                                            if ($price_group->price_group_base == 1) {
                                                $price_group_base = $price_group->id;
                                            }
                                        }
                                        echo form_dropdown('price_group', $prg, (isset($_POST['price_group']) ? $_POST['price_group'] : $price_group_base), 'class="form-control" id="price_group" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("price_group") . '"');
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("cut-off_date", "end_date"); ?>
                                        <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : date('d/m/Y')), 'class="form-control date" id="end_date" autocomplete="off"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-2 form-group">
                                    <label>Fecha a tener en cuenta</label>
                                    <br>
                                    <label style="width: 100%;">
                                        <input type="radio" name="report_date" value="2" <?= isset($_POST['report_date']) && $_POST['report_date'] == 2 || !isset($_POST['report_date']) ? 'checked' : '' ?>>
                                        Fecha real de registro <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('report_valued_products_real_date') ?>"></i>
                                    </label>
                                    <br>
                                    <label style="width: 100%;">
                                        <input type="radio" name="report_date" value="1" <?= isset($_POST['report_date']) && $_POST['report_date'] == 1 ? 'checked' : '' ?>>
                                        Fecha del documento <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="<?= lang('report_valued_products_movement_date') ?>"></i>
                                    </label>
                                </div>
                                <div class="col-sm-12 col-md-2">
									<div class="form-group">
										<div class="controls" id="include_products_discontinued" style="margin-top: 15px">
											<?= form_checkbox('include_products_discontinued', 'include_products_discontinued', (isset($_POST['include_products_discontinued']) ? 'checked' : ''));?>
										    <?= lang('include_products_discontinued', 'include_products_discontinued'); ?>
										</div>
									</div>
								</div>
                                <div class="col-sm-12 col-md-2">
									<div class="form-group">
										<div class="controls" id="variants" style="margin-top: 15px">
											<?= form_checkbox('variants', 'variants', (isset($_POST['variants']) ? 'checked' : '')); ?>
										    <?= lang('variants', 'variants'); ?>
										</div>
									</div>
								</div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="controls">
                                            <button type="submit" name="submit_report" id="submit_filter", class="btn btn-primary new-button" data-toggle="tooltip" title="<?= $this->lang->line("submit") ?>">
                                                <i class="fa fa-check"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <!-- <p class="introtext"><?php //echo lang('enter_info');
                                                    ?></p> -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-right dropdown">
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                        <li class="dropdown">
                                            <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                                <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?>
                                            </a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                                <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <h3>Fecha de corte : <?= !empty($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d') ?></h3>
                                    <table id="PrRData" class="table  table-bordered table-condensed table-hover dfTable reports-table" style="margin-bottom:5px;">
                                        <thead>
                                            <tr class="active">
                                                <th><?= lang("product_code"); ?></th>
                                                <th><?= lang("product_name"); ?></th>
                                            <?php if(isset($_POST['variants'])) { ?>
                                                <th><?= lang("code")." ".lang('variant')  ?></th>
                                                <th><?= lang("name")." ".lang('variant')  ?></th>
                                            <?php } ?>
                                                <th><?= lang("status"); ?></th>
                                                <th><?= lang("tax_rate"); ?></th>
                                                <th><?= lang("quantity"); ?></th>
                                                <th><?= lang("unit_cost_tax"); ?></th>
                                                <th><?= lang("avg_cost_tax"); ?></th>
                                                <th><?= lang("unit_price_tax"); ?></th>
                                                <th><?= lang("total_avg_cost_tax"); ?></th>
                                                <th><?= lang("total_avg_cost_no_tax"); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                            <tr class="active">
                                                <th></th>
                                                <th></th>
                                                <?php if(isset($_POST['variants'])) { ?>
                                                    <th></th>
                                                    <th></th>
                                                <?php } ?>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        preLoadUserData()

        $('#pdf').click(function(event) {
            event.preventDefault();
            window.location.href = "<?= admin_url('reports/getValuedProductsReport/pdf/0/?v=1' . $v) ?>";
            return false;
        });
        $('#xls').click(function(event) {
            event.preventDefault();
            window.location.href = "<?= admin_url('reports/getValuedProductsReport/0/xls/?v=1' . $v) ?>";
            return false;
        });
        $('#image').click(function(event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function(canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
        $('#submit_filter').on('click', function(e) {
            // e.preventDefault();
            $('#filter_action').val(1);
            $('#sales_filter').submit();
        });
        $(document).on('change', '#end_date', function() {
            bootbox.alert('Al seleccionar una fecha de corte diferente a la actual, los costos y precios cambian al del último movimiento de la fecha seleccionada');
        });
        $('[data-toggle="tooltip"]').tooltip();
        $(document).on('change', '#warehouse', function(){
            if ($(this).val()) {
                header_alert('warning', '<?= lang('valued_products_report_wh_alert') ?>', 6);
            }
        })


    });

    function preLoadUserData() {
        <?php if ($this->session->userdata('warehouse_id')) { ?>
            setTimeout(function() {
                $('#warehouse').select2('readonly', true).trigger('change');
            }, 850);
        <?php } ?>
    }

    function formatStatusVariant(x) {
        if (x == '0') {
            return '<div class="text-center"><span class="discontinued_status label label-warning">'+lang['inactive']+'</span></div>';
        } else if(x == '1') {
            return '<div class="text-center"><span class="discontinued_status label label-success">'+lang['active']+'</span></div>';
        }
    }

    function formatStatus(x) {
        if (x == '0') {
            return '<div class="text-center"><span class="discontinued_status label label-success">'+lang['active']+'</span></div>';
        } else if (x == '1') {
            return '<div class="text-center"><span class="discontinued_status label label-warning">'+lang['inactive']+'</span></div>';
        }
    }
</script>