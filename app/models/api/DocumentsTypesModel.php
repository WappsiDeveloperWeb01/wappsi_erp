<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentsTypesModel extends CI_Model
{
    public $tableName = 'documents_types';

    public function __construct() {
        parent::__construct();
    }

    public function find($filters) {
        $this->db->where($filters);
        return $this->db->get($this->tableName)->row();
    }

    public function create($data) {
        if ($this->db->insert($this->tableName, $data)) {
            return true;
        }
        return false;
    }

    public function update($data, $filters) {
        $this->db->where($filters);
        if ($this->db->update($this->tableName, $data)) {
            return true;
        }
        return false;
    }

    public function getBybiller($filters) {
        $this->db->select("{$this->tableName}.id, sales_prefix, sales_consecutive");
        $this->db->join("biller_data bd", "bd.order_sales_document_type_default = {$this->tableName}.id", "inner");
        $this->db->where($filters);
        return $this->db->get($this->tableName)->row();
    }
}